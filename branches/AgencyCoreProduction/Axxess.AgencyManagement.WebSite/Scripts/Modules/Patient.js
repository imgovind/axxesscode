﻿if (typeof Patient == "undefined") var Patient = new Object();
$.extend(Patient, {
    RebindList: function() {
        U.RebindTGrid($("#List_Patient_Grid"));
        U.RebindTGrid($("#List_PatientPending_Grid"));
        U.RebindTGrid($("#List_Referral"));
    },
    InitNew: function() {
        var $triage = $("input.Triage");
        $triage.click(function() {
            $triage.removeAttr("checked");
            $(this).prop("checked", true);
        });
        U.PhoneAutoTab("New_Patient_HomePhone");
        U.PhoneAutoTab("New_Patient_MobilePhone");
        U.PhoneAutoTab("New_Patient_PharmacyPhone");
        U.InitEditTemplate("Patient");
        Template.OnChangeInit();
        U.PhoneAutoTab("New_Patient_EmergencyContactPhonePrimary");
        U.PhoneAutoTab("New_Patient_EmergencyContactPhoneAlternate");
        U.ShowIfChecked($("#New_Patient_DMEOther"), $("#New_Patient_OtherDME"));
        U.ShowIfChecked($("#New_Patient_PaymentSource"), $("#New_Patient_OtherPaymentSource"));
        U.InitNewTemplate("Patient", function() {
            if ($("#New_Patient_Status").val() == "1") {
                Patient.Rebind();
                UserInterface.ShowPatientPrompt();
            }
        });
        $("#window_newpatient .Physicians").PhysicianInput();
    },
    InitEdit: function() {
        U.PhoneAutoTab("Edit_Patient_HomePhone");
        U.PhoneAutoTab("Edit_Patient_MobilePhone");
        U.PhoneAutoTab("Edit_Patient_PharmacyPhone");
        U.InitEditTemplate("Patient");
        Template.OnChangeInit();
        $("#window_editpatient .Physicians").PhysicianInput();
        $("#EditPatient_NewPhysician").click(function() {
            var PhysId = $("#EditPatient_PhysicianSelector").next("input[type=hidden]").val();
            $("#EditPatient_PhysicianSelector").AjaxAutocomplete("reset");
            if (U.IsGuid(PhysId)) Patient.AddPhysician(PhysId, $("#Edit_Patient_Id").val());
            else U.Growl("Please Select a Physician", "error");
        });
        U.InitEditTemplate("Patient");
    },
    Verify: function(medicareNumber, lastName, firstName, dob, gender) {
        U.PostUrl("Patient/Verify", { medicareNumber: medicareNumber, lastName: lastName, firstName: firstName, dob: dob, gender: gender }, function(result) { alert(result) });
    },
    InitPatientReadmitted: function() {
        U.InitTemplate($("#readmitForm"), function() {
            UserInterface.CloseModal();
            Patient.Charts._patientId = "";
            Patient.Rebind();
        }, "Patient Re-admitted successfully.")
    },
    InitChangePatientStatus: function() {
        U.InitTemplate($("#changePatientStatusForm"), function() {
            UserInterface.CloseModal();
            Patient.Charts._patientId = "";
            Patient.Rebind();
        }, "Patient status updated successfully.")
    },
    InitDischargePatient: function() {
        U.InitTemplate($("#dischargePatientForm"), function() {
            UserInterface.CloseModal();
            Patient.Charts._patientId = "";
            U.FilterResults("Patient");
        }, "Patient is discharged successfully.")
    },
    InitActivatePatient: function() {
        U.InitTemplate($("#activatePatientForm"), function() {
            UserInterface.CloseModal();
            Patient.Charts._patientId = "";
            U.FilterResults("Patient");
        }, "Patient is activated successfully.")
    },
    InitNewPhoto: function() {
        $("#changePatientPhotoForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("Patient photo updated successfully.", "success");
                            Patient.Charts.LoadInfoAndActivity(Patient.Charts._patientId);
                            UserInterface.CloseModal();
                        } else alert(result.responseText);
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            U.Growl("Patient photo updated successfully.", "success");
                            Patient.Charts.LoadInfoAndActivity(Patient.Charts._patientId);
                            UserInterface.CloseModal();
                        } else alert(result.responseText);
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    RemovePhoto: function(patientId) {
        if (confirm("Are you sure you want to remove this photo?")) {
            U.PostUrl("/Patient/RemovePhoto", "patientId=" + patientId, function(result) {
                if (result.isSuccessful) {
                    U.Growl("Patient photo has been removed successfully.", "success");
                    Patient.Charts.LoadInfoAndActivity(Patient.Charts._patientId);
                    UserInterface.CloseModal();
                } else U.Growl(result.errorMessage, "error");
            })
        }
    },
    InitNewDocument: function(patientId) {
        $("#UploadDocument_Form").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if ($.trim(result.responseText) == "Success") {
                            U.Growl("Patient document uploaded.", "success");
                            U.RebindTGrid($("#List_PatientDocuments"));
                            UserInterface.CloseModal();
                        } else alert(result.responseText);
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == "Success") {
                            U.Growl("Patient document uploaded successfully.", "success");
                            U.RebindTGrid($("#List_PatientDocuments"));
                            UserInterface.CloseModal();
                        } else alert(result.responseText);
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    InitEditDocument: function(patientDocument) {
        $("#EditDocument_Form").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.errorMessage.toLowerCase().indexOf("not") == -1) {
                            U.Growl("Patient document updated successfully.", "success");
                            U.RebindTGrid($("#List_PatientDocuments"));
                            UserInterface.CloseModal();
                        } else alert(result.errorMessage);
                    },
                    error: function(result) {
                        if (result.errorMessage.toLowerCase().indexOf("success") == -1) {
                            U.Growl("Patient document updated successfully.", "success");
                            U.RebindTGrid($("#List_PatientDocuments"));
                            UserInterface.CloseModal();
                        } else alert(result.responseText);
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    DeleteDocument: function(patientId, documentId) {
        U.Delete("Document", "Patient/Document/Delete", { patientId: patientId, documentId: documentId }, function() {
            U.RebindTGrid($("#List_PatientDocuments"))
        })
    },
    PatientRowDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue-note")) var c = "blue-note";
            if ($(this).hasClass("red-note")) var c = "red-note";
            if ($(this).html().length) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).html(), ($(this).hasClass("blue-note") ? "blue" : "") + ($(this).hasClass("red-note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() { return $(this).html() }
                });
            } else $(this).hide();
        })
    },
    LoadEditPatient: function(id) {
        Acore.Open("editpatient", "Patient/EditPatientContent", function() {
            Patient.InitEdit();
            Lookup.LoadAdmissionSources();
            Lookup.LoadRaces();
            Lookup.LoadUsers();
            Lookup.LoadReferralSources();
        }, { patientId: id });
    },
    LoadEditAuthorization: function(patientId, id) {
        Acore.Open("editauthorization", "Authorization/Edit", function() {
            Patient.InitEditAuthorization(patientId);
        }, { patientId: patientId, Id: id });
    },
    InitNewOrder: function() {
        Template.OnChangeInit();
        $("#New_Order_PhysicianDropDown").PhysicianInput();
        $("#newOrderForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            var patientId = $("select[name=PatientId]", form).val();
                            Schedule.Center.RefreshSchedule(patientId);
                            Patient.Charts.Activities.Refresh(patientId);
                            Agency.RebindOrders();
                            UserInterface.CloseWindow("neworder");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    SubmitOrder: function(control, page) {
        var form = control.closest("form").validate();
        if (form.valid()) {
            var options = {
                dataType: "json",
                beforeSubmit: function(values, form, options) {},
                success: function(result) {
                    if (result.isSuccessful) {
                        var patientId = form.find("[name=PatientId]").val();
                        if (control.html() == "Save") U.Growl("The Physician Order has been saved successfully.", "success");
                        if (control.html() == "Save &amp; Close") {
                            UserInterface.CloseAndRefresh(page, patientId);
                            U.Growl("The Physician Order has been saved successfully.", "success");
                        }
                        if (control.html() == "Complete") {
                            UserInterface.CloseAndRefresh(page, patientId);
                            U.Growl("The Physician Order has been saved and completed successfully.", "success");
                        }
                    } else U.Growl(result.errorMessage, "error");
                }
            };
            form.ajaxSubmit(options);
            return false;
        } else U.ValidationError(control);
    },
    InitNewFaceToFaceEncounter: function(r, t, x, e) {
        $("#newFaceToFaceEncounterForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            var patientId = $("select[name=PatientId]", e).val();
                            Schedule.Center.RefreshSchedule(patientId);
                            Patient.Charts.Activities.Refresh(patientId);
                            Agency.RebindOrders();
                            UserInterface.CloseWindow("newfacetofaceencounter");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    InitNewCommunicationNote: function() {
        Template.OnChangeInit();
        $("textarea[maxcharacters]").limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings(".charsRemaining").text("You have " + remaining + " characters remaining");
                if (remaining > 0) $(this).css("background-color", "white");
            },
            onLimit: function() {
                $(this).css("background-color", "#ecbab3");
            }
        });
        $("#newCommunicationNoteForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function() { },
                    success: function(result) {
                        U.UnBlockButton($(form).find(".blocked-button"));
                        if (result.isSuccessful) {
                            U.Growl("New Communication Note saved successfully.", "success");
                            UserInterface.CloseWindow("newcommnote");
                            var patientId = $("select[name=PatientId]", form).val();
                            Patient.Charts.Activities.Refresh(patientId);
                        } else U.Growl(result.errorMessage, "error");
                    }

                };
                $(form).ajaxSubmit(options);
                return false;
            },
            invalidHandler: function(form, validator) {
                U.UnBlockButton($(this).find(".blocked-button"));
            }
        })
    },
    InitNewAuthorization: function() {
        $(".numeric").numeric();
        $("textarea[maxcharacters]").limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings(".charsRemaining").text("You have " + remaining + " characters remaining");
                if (remaining > 0) $(this).css("background-color", "white");
            },
            onLimit: function() {
                $(this).css("background-color", "#ecbab3");
            }
        });
        $("#newAuthorizationForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        U.Growl(result.errorMessage, "success");
                        if (result.isSuccessful) {
                            var patientId = $("#New_Authorization_PatientId").val();
                            UserInterface.CloseWindow("newauthorization");
                            Patient.RebindAuthorizationGrid(patientId);
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    InitEditAuthorization: function(patientId) {
        $(".numeric").numeric();
        $("textarea[maxcharacters]").limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings(".charsRemaining").text("You have " + remaining + " characters remaining");
                if (remaining > 0) $(this).css("background-color", "white");
            },
            onLimit: function() {
                $(this).css("background-color", "#ecbab3");
            }
        });
        $("#editAuthorizationForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow("editauthorization");
                            Patient.RebindAuthorizationGrid(patientId);
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    InitEditOrder: function() {
        Template.OnChangeInit();
        $("#Edit_Order_PhysicianDropDown").PhysicianInput();
    },
    Rebind: function() {
        Patient.Charts._isRebind = true;
        U.RebindTGrid($("#PatientSelectionGrid"));
        U.RebindTGrid($("#List_Patient_Grid"));
    },
    LoadNewOrder: function(patientId) {
        Acore.Open("neworder", "Order/New", function() { Patient.InitNewOrder() }, { patientId: patientId });
    },
    DeleteOrder: function(id, patientId, episodeId) {
        if (confirm("Are you sure you want to delete this task?"))
            U.PostUrl("Patient/DeleteOrder", { id: id, patientId: patientId, episodeId: episodeId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    var grid = $("#List_PatientOrdersHistory").data("tGrid");
                    if (grid != null) grid.rebind({ patientId: patientId });
                    var grid = $("#List_EpisodeOrders").data("tGrid");
                    if (grid != null) grid.rebind({ patientId: patientId, episodeId: episodeId });
                    if ($("#Billing_CenterContent").val() != null && $("#Billing_CenterContent").val() != undefined) Billing.ReLoadUnProcessedClaim("#Billing_CenterContentFinal", $("#Billing_FinalCenter_BranchCode").val(), $("#Billing_FinalCenter_InsuranceId").val(), "FinalGrid", "Final");
                } else U.Growl(result.errorMessage, "error");
            })
    },
    LoadNewCommunicationNote: function(patientId) {
        Acore.Open("newcommnote", "CommunicationNote/New", function() { Patient.InitNewCommunicationNote() }, { patientId: patientId });
    },
    LoadNewAuthorization: function(patientId) {
        Acore.Open("newauthorization", "Authorization/New", function() { Patient.InitNewAuthorization() }, { patientId: patientId });
    },
    LoadNewInfectionReport: function(patientId) {
        Acore.Open("newinfectionreport", "Infection/New", function() { InfectionReport.InitNew() }, { patientId: patientId });
    },
    LoadNewIncidentReport: function(patientId) {
        Acore.Open("newincidentreport", "Incident/New", function() { IncidentReport.InitNew() }, { patientId: patientId });
    },
    LoadUserAccess: function(patientId) {
        Acore.Open("patientuseraccess", "Patient/UserAccess", function() {
            $("#UserAccess_Form").Validate({
                Success: function() { U.RebindTGrid($("#List_PatientUserAccess")) }
            })
        }, { patientId: patientId })
    },
    LoadAddUserAccess: function() {
        $("#AddUserAccess_Form").Validate({
            Success: function() {
                U.RebindTGrid($("#List_PatientUserAccess"));
                UserInterface.CloseModal();
            }
        })
    },
    AddPhysRow: function(el) {
        $(el).closest(".column").find(".row.hidden").first().removeClass("hidden").find("input:first").blur()
    },
    RemPhysRow: function(el) {
        $(el).closest(".row").addClass("hidden").appendTo($(el).closest(".column")).find("select").val("00000000-0000-0000-0000-000000000000")
    },
    InitMedicationProfileSnapshot: function(assessmentType) {
        U.PhoneAutoTab("MedProfile_PharmacyPhone");
        $("#window_medicationprofilesnapshot .Physicians").PhysicianInput();
        $("#newMedicationProfileSnapShotForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) {
                        if (assessmentType != undefined) {
                            var assessmentId = $(assessmentType + "_Id").val();
                            if (assessmentId != undefined && assessmentId != "") values.push({ name: "AssociatedAssessment", value: assessmentId });
                        }
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            UserInterface.CloseWindow('medicationprofilesnapshot');
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    LoadMedicationProfileSnapshot: function(patientId, assessmentType) {
        Acore.Open("medicationprofilesnapshot", "Patient/MedicationProfileSnapShot", function() { Patient.InitMedicationProfileSnapshot(assessmentType) }, { patientId: patientId });
    },
    LoadMedicationProfile: function(patientId) {
        Acore.Open("medicationprofile", "Patient/MedicationProfile", function() { }, { patientId: patientId });
    },
    LoadMedicationProfileSnapshotHistory: function(patientId) {
        Acore.Open("medicationprofilesnapshothistory", "Patient/MedicationProfileSnapShotHistory", function() { }, { patientId: patientId });
    },
    LoadPatientCommunicationNotes: function(patientId) {
        Acore.Open("patientcommunicationnoteslist", "Patient/PatientCommunicationNotesView", function() { }, { patientId: patientId });
    },
    OnMedicationProfileEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find(".MedicationType").data("tDropDownList").select(function(dataItem) {
                return dataItem.Value == e.dataItem["MedicationType"].Value;
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest("td").prevAll().length == 7 && e.dataItem["MedicationCategory"] != "DC") $(this).html("");
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest("td").prevAll().length == 6 && (e.dataItem["Classification"] == null || e.dataItem["Classification"] == "")) $(this).find("input[name=MedicationClassification]").val("");
            });
        }
    },
    OnPlanofCareMedicationEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find(".MedicationType").data("tDropDownList").select(function(dataItem) {
                return dataItem.Value == e.dataItem["MedicationType"].Value;
            })
        }
    },
    OnMedicationProfileRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem.MedicationCategory == "DC") {
            $(e.row).removeClass("t-alt").addClass("red");
            e.row.cells[7].innerHTML = dataItem.DCDateFormated;
        } else e.row.cells[7].innerHTML = "<a class='t-grid-action t-button t-state-default' href='javascript:void(0)' onclick='" + dataItem.DischargeUrl + "'>D/C</a>";
        if (dataItem.DrugId != null && dataItem.DrugId.length > 0) {
            var medline = "http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.69&mainSearchCriteria.v.c=" + dataItem.DrugId;
            e.row.cells[2].innerHTML = "<a href='" + medline + "' target='_blank'>" + dataItem.MedicationDosage + "</a>";
        }
        e.row.cells[1].innerHTML = dataItem.StartDateSortable;
    },
    OnMedicationProfileSnapShotRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem != undefined) e.row.cells[1].innerHTML = dataItem.StartDateSortable;
    },
    OnMedProfileSnapShotHistoryRowDataBound: function(e) {
        var dataItem = e.dataItem;
        e.row.cells[1].innerHTML = dataItem.SignedDateFormatted;
    },
    OnDataBound: function(e) {
        var id = $("#medicationProfileHistroyId").val();
        var medicatonProfileGridDC = $("#MedicatonProfileGridDC").data("tGrid");
        medicatonProfileGridDC.rebind({ medId: id, medicationCategry: "DC", assessmentType: "" });
    },
    DischargeMed: function(medId, patientId, assessmentType) {
        $("#Discharge_Medication_Container").load("Patient/MedicationDischarge", function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "error") U.Growl("Could not load the Medication Discharge page.", "error");
            else if (textStatus == "success")
                U.ShowDialog("#Discharge_Medication_Container", function() {
                    if (assessmentType != undefined || assessmentType != null && assessmentType.length > 0) Patient.InitDischargeMedication(medId, patientId, function() { Patient.RefreshOasisMedProfile(medId, assessmentType) });
                    else Patient.InitDischargeMedication(medId, patientId, function() { Patient.RefreshMedProfile(medId) });
                })
        })
    },
    InitDischargeMedication: function(medId, patientId, callback) {
        $("#dischargeMedicationProfileForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    data: { medId: medId, patientId: patientId },
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            UserInterface.CloseModal();
                            if (callback != undefined && typeof (callback) == "function") callback();
                            U.Growl(result.errorMessage, "success");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    RefreshMedProfile: function(medId) {
        var medicatonProfileGridActive = $("#MedicatonProfileGridActive").data("tGrid");
        medicatonProfileGridActive.rebind({ medId: medId, medicationCategry: "Active", assessmentType: "" });
        var medicatonProfileGridDC = $("#MedicatonProfileGridDC").data("tGrid");
        medicatonProfileGridDC.rebind({ medId: medId, medicationCategry: "DC", assessmentType: "" });
    },
    RefreshOasisMedProfile: function(medId, assessmentType) {
        var oasisMedGrid = $("#Oasis" + assessmentType + "MedicationGrid").data("tGrid");
        if (oasisMedGrid != null) oasisMedGrid.rebind({ medId: medId, medicationCategry: "Active", assessmentType: assessmentType });
    },
    Delete: function(patientId) {
        if (confirm("Are you sure you want to delete this patient?"))
            U.PostUrl("Patient/Delete", { patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    Patient.LoadPateintListContent("#Patient_List_MainContainer", "#List_Patient_BranchId", "#List_Patient_Status", "DisplayName-ASC");
                    Patient.Rebind();
                    U.RebindTGrid($("#ScheduleCenter_PatientSelectionGrid"));
                } else U.Growl(result.errorMessage, "error");
            })
    },
    InitAdmit: function(patientId) {
        $("#newAdmitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    clearForm: false,
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl("Patient admission successful.", "success");
                            UserInterface.CloseWindow("admitpatient");
                            UserInterface.CloseModal();
                            Patient.Rebind();
                            U.RebindTGrid($("#ScheduleCenter_PatientSelectionGrid"));
                            U.RebindTGrid($("#List_Patient_NonAdmit_Grid"));
                            U.RebindTGrid($("#List_PatientPending_Grid"));
                            UserInterface.ShowPatientPrompt();
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        })
    },
    InitNonAdmit: function() {
        $("#NonAdmit_Patient_Date").datepicker("hide");
        $("#newNonAdmitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    clearForm: false,
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl("Patient non-admission successful.", "success");
                            UserInterface.CloseWindow("nonadmitpatient");
                            UserInterface.CloseModal();
                            U.RebindTGrid($("#List_PatientPending_Grid"));
                            Patient.Rebind();
                            U.RebindTGrid($("#ScheduleCenter_PatientSelectionGrid"));
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        })
    },
    LoadEditCommunicationNote: function(patientId, id) {
        Acore.Open("editcommunicationnote", "Patient/EditCommunicationNote", function() {
            $("#editCommunicationNoteForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: "json",
                        beforeSubmit: function(values, form, options) { },
                        success: function(result) {
                            if (result.isSuccessful) {
                                U.Growl(resultObject.errorMessage, "success");
                                UserInterface.CloseWindow("editcommunicationnote");
                                Schedule.Center.RefreshSchedule(patientId);
                                Patient.Charts.Activities.Refresh(patientId);
                            } else U.Growl(result.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            })
        }, { patientId: patientId, Id: id })
    },
    LoadEditEmergencyContact: function(patientId, id) {
        Acore.Open("editemergencycontact", "Patient/EditEmergencyContactContent", function() {
            U.PhoneAutoTab("Edit_EmergencyContact_PrimaryPhoneArray");
            U.PhoneAutoTab("Edit_EmergencyContact_AlternatePhoneArray");
            $("#editEmergencyContactForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: "json",
                        beforeSubmit: function(values, form, options) { },
                        success: function(result) {
                            if (result.isSuccessful) {
                                U.Growl("Edit emergency contact is successful.", "success");
                                var emergencyContact = $("#Edit_patient_EmergencyContact_Grid").data("tGrid");
                                if (emergencyContact != null) emergencyContact.rebind({ PatientId: patientId });
                                UserInterface.CloseWindow("editemergencycontact");
                            } else U.Growl(result.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            })
        }, { patientId: patientId, Id: id })
    },
    LoadNewEmergencyContact: function(id) {
        Acore.Open("newemergencycontact", "Patient/NewEmergencyContactContent", function() {
            U.PhoneAutoTab("New_EmergencyContact_PrimaryPhoneArray");
            U.PhoneAutoTab("New_EmergencyContact_AlternatePhoneArray");
            $("#newEmergencyContactForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: "json",
                        beforeSubmit: function(values, form, options) { },
                        success: function(result) {
                            if (result.isSuccessful) {
                                U.Growl("New emergency contact added successful.", "success");
                                var emergencyContact = $("#Edit_patient_EmergencyContact_Grid").data("tGrid");
                                if (emergencyContact != null) emergencyContact.rebind({ PatientId: id });
                                UserInterface.CloseWindow("newemergencycontact");
                            } else U.Growl(result.errorMessage, "error");
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            })
        }, { patientId: id })
    },
    DeleteEmergencyContact: function(id, patientId) {
        if (confirm("Are you sure you want to delete"))
            U.PostUrl("Patient/DeleteEmergencyContact", { id: id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    var emergencyContact = $("#Edit_patient_EmergencyContact_Grid").data("tGrid");
                    if (emergencyContact != null) emergencyContact.rebind({ PatientId: patientId });
                }
            })
    },
    DeleteCommunicationNote: function(Id, patientId) {
        if (confirm("Are you sure you want to delete this task?"))
            U.PostUrl("Patient/DeleteCommunicationNote", { Id: Id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    var communicationNoteGrid = $("#List_CommunicationNote").data("tGrid");
                    if (communicationNoteGrid != null) {
                        U.Growl(result.errorMessage, "success");
                        communicationNoteGrid.rebind();
                    }
                } else U.Growl(result.errorMessage, "error");
            })
    },
    AddPhysician: function(id, patientId) {
        U.TGridAjax("Patient/AddPatientPhysicain", { id: id, patientId: patientId }, $("#EditPatient_PhysicianGrid"));
        $("#EditPatient_PhysicianSelector").AjaxAutocomplete("reset").next("input[type=hidden]").val("");
    },
    DeletePhysician: function(id, patientId) {
        if (confirm("Are you sure you want to delete this physician?")) U.TGridAjax("Patient/DeletePhysicianContact", { id: id, patientId: patientId }, $("#EditPatient_PhysicianGrid"));
    },
    DeleteAuthorization: function(patientId, id) {
        if (confirm("Are you sure you want to delete this authorization?"))
            U.PostUrl("Patient/DeleteAuthorization", { Id: id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    Patient.RebindAuthorizationGrid(patientId);
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            })
    },
    SetPrimaryPhysician: function(id, patientId) {
        if (confirm("Are you sure you want to set this physician as primary?")) {
            U.TGridAjax("Physician/SetPrimary", { id: id, patientId: patientId }, $("#EditPatient_PhysicianGrid"));
            Patient.Rebind();
        }
    },
    OnSocChange: function() {
        var date = $("#New_Patient_StartOfCareDate").datepicker("getDate");
        $("#New_Patient_EpisodeStartDate").datepicker("setDate", date)
        $("#New_Patient_EpisodeStartDate").datepicker("option", "mindate", date);
    },
    UpdateOrderStatus: function(eventId, patientId, episodeId, orderType, actionType) {
        var reason = "";
        if (actionType == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() { if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide() });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown("slow");
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("Patient/UpdateOrderStatus", { eventId: eventId, patientId: patientId, episodeId: episodeId, orderType: orderType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Schedule.Center.RefreshSchedule(patientId);
                        Patient.Charts.Activities.Refresh(patientId);
                        User.RebindScheduleList();
                        U.Growl(data.errorMessage, "success");
                    } else U.Growl(data.errorMessage, "error");
                });
            }
        } else
            U.PostUrl("Patient/UpdateOrderStatus", { eventId: eventId, patientId: patientId, episodeId: episodeId, orderType: orderType, actionType: actionType, reason: reason }, function(data) {
                if (data.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Schedule.Center.RefreshSchedule(patientId);
                    Patient.Charts.Activities.Refresh(patientId);
                    User.RebindScheduleList();
                    U.Growl(data.errorMessage, "success");
                } else U.Growl(data.errorMessage, "error");
            });
    },
    ProcessCommunicationNote: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#print-return-reason").is(":hidden")) {
                $("#print-controls li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very-hidden");
                $("#print-return-reason").slideDown("slow");
            } else {
                reason = $("#print-return-reason textarea").val();
                U.PostUrl("Patient/ProcessCommunicationNotes", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Schedule.Center.RefreshSchedule(patientId);
                        Patient.Charts.Activities.Refresh(patientId);
                        User.RebindScheduleList();
                        U.Growl(result.errorMessage, "success");
                    } else U.Growl(result.errorMessage, "error");
                });
            }
        } else
            U.PostUrl("Patient/ProcessCommunicationNotes", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Schedule.Center.RefreshSchedule(patientId);
                    Patient.Charts.Activities.Refresh(patientId);
                    User.RebindScheduleList();
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            });
    },
    RebindOrdersHistory: function() {
        var grid = $("#List_PatientOrdersHistory").data("tGrid");
        if (grid != null) grid.rebind({ patientId: $("#PatientOrdersHistory_PatientId").val(), StartDate: $("#PatientOrdersHistory_StartDate-input").val(), EndDate: $("#PatientOrdersHistory_EndDate-input").val() });
        var $exportLink = $("#PatientOrdersHistory_ExportLink");
        var href = $exportLink.attr("href");
        href = href.replace(/patientId=([^&]*)/, "patientId=" + $("#PatientOrdersHistory_PatientId").val());
        href = href.replace(/StartDate=([^&]*)/, "StartDate=" + $("#PatientOrdersHistory_StartDate-input").val());
        href = href.replace(/EndDate=([^&]*)/, "EndDate=" + $("#PatientOrdersHistory_EndDate-input").val());
        $exportLink.attr("href", href);
    },
    LoadPatientAdmissionPeriods: function(Id) {
        Acore.Open("patientmanageddates", "Patient/AdmissionPeriod", function() { }, { patientId: Id });
    },
    LoadInsuranceContent: function(contentId, patientId, insuranceId, action, insuranceType) {
        $(contentId).load("Patient/InsuranceInfoContent", { PatientId: patientId, InsuranceId: insuranceId, Action: action, InsuranceType: insuranceType }, function() { })
    },
    LoadPateintListContent: function(contentId, branchId, statusId, SortParams) {
        $(contentId).empty().addClass("loading").load("Patient/ListContent", { BranchId: $(branchId).val(), Status: $(statusId).val(), SortParams: SortParams }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "error") U.Growl("Could not load patient list. Try again.", "error");
            else if (textStatus == "success") {
                $(contentId).removeClass("loading");
                var $exportLink = $("#List_Patient_ExportLink");
                var href = $exportLink.attr("href");
                if (href != null && href != undefined) {
                    href = href.replace(/BranchId=([^&]*)/, "BranchId=" + $(branchId).val());
                    href = href.replace(/Status=([^&]*)/, "Status=" + $(statusId).val());
                    $exportLink.attr("href", href);
                }
            }
        })
    },
    LoadPatientAdmissionContent: function(contentId, patientId) {
        $(contentId).empty().addClass("loading").load("Patient/AdmissionPeriodContent", { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == "error") U.Growl("Could not load this page. Try again.", "error");
            else if (textStatus == "success") $(contentId).removeClass("loading");
        })
    },
    DeletePatientAdmission: function(patientId, Id) {
        if (confirm("Are you sure you want to delete this admission period?"))
            U.PostUrl("Patient/DeletePatientAdmission", { patientId: patientId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    Patient.LoadPatientAdmissionContent("#PatientAdmissionPeriodContainer", patientId);
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            })
    },
    InitAdmissionPatient: function(patientId, type) {
        U.PhoneAutoTab(type + "_Admission_HomePhone");
        U.PhoneAutoTab(type + "_Admission_MobilePhone");
        U.PhoneAutoTab(type + "_Admission_PharmacyPhone");
        $("#window_" + type.toLowerCase() + "patientadmission .Physicians").PhysicianInput();
        $("#" + type.toLowerCase() + "PatientAdmissionForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: "json",
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.Growl(result.errorMessage, "success");
                            Patient.LoadPatientAdmissionContent("#PatientAdmissionPeriodContainer", patientId);
                            UserInterface.CloseWindow(type.toLowerCase() + "patientadmission");
                        } else U.Growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    MarkPatientAdmissionCurrent: function(patientId, Id) {
        if (confirm("Are you sure you want to mark this admission active? This will update this admission with the current patient information."))
            U.PostUrl("Patient/MarkPatientAdmissionCurrent", { patientId: patientId, Id: Id }, function(result) {
                if (result.isSuccessful) {
                    Patient.LoadPatientAdmissionContent("#PatientAdmissionPeriodContainer", patientId);
                    U.Growl(result.errorMessage, "success");
                } else U.Growl(result.errorMessage, "error");
            })
    },
    RestoreDeleted: function(patientId) {
        if (confirm("Are you sure you want to restore this patient?"))
            U.PostUrl("Patient/RestoreDeleted", { PatientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    U.Growl(result.errorMessage, "success");
                    U.RebindDataGridContent("List_Patient_Deleted", "Patient/DeletedPatientContent", { BranchId: $("#List_Patient_Deleted_BranchId").val() }, "DisplayName-ASC");
                    Patient.Rebind();
                    U.RebindTGrid($("#ScheduleCenter_PatientSelectionGrid"));
                } else U.Growl(result.errorMessage, "error");
            })
    },

    RebindAuthorizationGrid: function(patientId) {
        var authorizationGrid = $("#List_Authorizations").data("tGrid");
        if (authorizationGrid != null) {
            var selectUrl = authorizationGrid.ajax.selectUrl;
            var match = $("#List_Authorizations").data("tGrid").ajax.selectUrl.match("patientId=(.*)");
            if (match.length > 0 && match[1] == patientId) authorizationGrid.rebind({ patientId: patientId });
        }
    },
    UserAccess: {
        OnRowBound: function(e) {
            var row = e.row;
            if ($("input", row.innerHTML).attr("value").indexOf("00000000-0000-0000-0000-000000000000") === -1) $("input", row).attr("checked", "checked");
        },
        AddUserAccess: function(data, e) {
            U.PostUrl("Patient/AddUserAccess", data, function(dataResult) {
                var patientUserId = dataResult.PatientId;
                $(e).attr("value", patientUserId);
                console.log("saved");
            })
        },
        RemoveUserAccess: function(data) {
            if (typeof data !== "object") data = { patientUserId: data };
            U.PostUrl("Patient/RemoveSingleUserAccess", data, function(dataResult) {
                U.RebindTGrid($("#List_PatientUserAccess"));
            })
        },
        UserClick: function(e) {
            if ($(e).is(":checked")) {
                var patientId = $("#UserAccessPatientId")[0].defaultValue;
                var userId = $(e).parent().parent().children(":first")[0].innerHTML;
                var data = { patientId: patientId, userId: userId };
                Patient.UserAccess.AddUserAccess(data, e);
            } else {
                var patientUserId = $(e).attr("value");
                var data = { patientUserId: patientUserId };
                Patient.UserAccess.RemoveUserAccess(data);
            }
        },
        SubmitAddUser: function() {
            $("#AddUserAccess_Form").submit();
            U.RebindTGrid($("#List_PatientUserAccess"));
            UserInterface.CloseModal();
        }
    },
    PatientAccess: {
        UserClick: function(e) {
            if ($(e).is(":checked")) {
                var userId = $("#PatientAccessUserId")[0].defaultValue
                var patientId = $(e).parent().parent().children(":first")[0].innerHTML;
                var data = { patientId: patientId, userId: userId };
                Patient.UserAccess.AddUserAccess(data, e);
            } else {
                var patientUserId = $(e).attr("value");
                var data = { patientUserId: patientUserId };
                Patient.UserAccess.RemoveUserAccess(data);
            }
        }
    }
});