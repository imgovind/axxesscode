﻿var UAPInsulinPrepAdminVisit = function() { BaseVisit.call(this); this.WindowId = "UAPInsulinPrepAdminVisit"; };
Visit.Extend(UAPInsulinPrepAdminVisit, "UAPInsulinPrepAdminVisit", function(Prefix, e) {
    U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
    U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
    U.ShowIfChecked($(Prefix + "FFBS"), $(Prefix + "FFBSMore"));
    U.ShowIfChecked($(Prefix + "NFFBS"), $(Prefix + "NFFBSMore"));
    U.ShowIfChecked($(Prefix + "AsepticTech1"), $(Prefix + "AsepticTech1More"));
    U.ShowIfChecked($(Prefix + "AsepticTechSlide1"), $(Prefix + "AsepticTechSlide1More"));
    U.ShowIfChecked($(Prefix + "PADueTo6"), $(Prefix + "PADueToOther"));
    U.ShowIfChecked($(Prefix + "Supplies5"), $(Prefix + "SuppliesOther"));
});

var UAPWoundCareVisit = function() { BaseVisit.call(this); this.WindowId = "UAPWoundCareVisit"; };
Visit.Extend(UAPWoundCareVisit, "UAPWoundCareVisit", function(Prefix, e) {
    U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
    U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
    U.ShowIfChecked($(Prefix + "MedicationApplied7"), $(Prefix + "MedicationAppliedOther"));
    U.ShowIfChecked($(Prefix + "DressingApplied10"), $(Prefix + "DressingAppliedOther"));
    U.ShowIfChecked($(Prefix + "DressingSecured3"), $(Prefix + "DressingSecuredOther"));
    U.ShowIfChecked($(Prefix + "Supplies11"), $(Prefix + "SuppliesOther"));
});

$.extend(Visit, {
    UAPInsulinPrepAdminVisit: new UAPInsulinPrepAdminVisit(),
    UAPWoundCareVisit: new UAPWoundCareVisit()
});