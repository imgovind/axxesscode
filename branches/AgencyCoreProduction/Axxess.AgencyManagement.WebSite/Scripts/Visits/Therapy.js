﻿var STDischargeSummary = function() { BaseVisit.call(this); this.WindowId = "STDischargeSummary"; };
Visit.Extend(STDischargeSummary, "STDischargeSummary", function(Prefix, e) {
    U.ShowIfChecked($(Prefix + "ServiceProvidedOther"), $(Prefix + "ServiceProvidedOtherValue"));
    U.ShowIfSelectEquals($(Prefix + "NotificationDate"), "3", $(Prefix + "NotificationDateOther"));
    U.ShowIfSelectEquals($(Prefix + "ReasonForDC"), "9", $(Prefix + "ReasonForDCOther"));
});

var OTVisit = function() { BaseVisit.call(this); this.WindowId = "OTVisit"; };
Visit.Extend(OTVisit, "OTVisit");

var COTAVisit = function() { BaseVisit.call(this); this.WindowId = "COTAVisit"; };
Visit.Extend(COTAVisit, "COTAVisit");

var OTDischarge = function() { BaseVisit.call(this); this.WindowId = "OTDischarge"; };
Visit.Extend(OTDischarge, "OTDischarge");

var OTDischargeSummary = function() { BaseVisit.call(this); this.WindowId = "OTDischargeSummary"; };
Visit.Extend(OTDischargeSummary, "OTDischargeSummary", function(Prefix, e) {
    U.ShowIfChecked($(Prefix + "ServiceProvidedOther"), $(Prefix + "ServiceProvidedOtherValue"));
    U.ShowIfSelectEquals($(Prefix + "NotificationDate"), "3", $(Prefix + "NotificationDateOther"));
    U.ShowIfSelectEquals($(Prefix + "ReasonForDC"), "9", $(Prefix + "ReasonForDCOther"));
});

var OTEvaluation = function() { BaseVisit.call(this); this.WindowId = "OTEvaluation"; };
Visit.Extend(OTEvaluation, "OTEvaluation");

var OTPlanOfCare = function() { BaseVisit.call(this); this.WindowId = "OTPlanOfCare"; };
Visit.Extend(OTPlanOfCare, "OTPlanOfCare");

var OTMaintenance = function() { BaseVisit.call(this); this.WindowId = "OTMaintenance"; };
Visit.Extend(OTMaintenance, "OTMaintenance");

var OTReEvaluation = function() { BaseVisit.call(this); this.WindowId = "OTReEvaluation"; };
Visit.Extend(OTReEvaluation, "OTReEvaluation");

var OTReassessment = function() { BaseVisit.call(this); this.WindowId = "OTReassessment"; };
Visit.Extend(OTReassessment, "OTReassessment");

var OTSupervisoryVisit = function() { BaseVisit.call(this); this.WindowId = "OTSupervisoryVisit"; };
Visit.Extend(OTSupervisoryVisit, "OTSupervisoryVisit");

var PTVisit = function() { BaseVisit.call(this); this.WindowId = "PTVisit"; };
Visit.Extend(PTVisit, "PTVisit");

var PTAVisit = function() { BaseVisit.call(this); this.WindowId = "PTAVisit"; };
Visit.Extend(PTAVisit, "PTAVisit");

var PTDischarge = function() { BaseVisit.call(this); this.WindowId = "PTDischarge"; };
Visit.Extend(PTDischarge, "PTDischarge");

var PTEvaluation = function() { BaseVisit.call(this); this.WindowId = "PTEvaluation"; };
Visit.Extend(PTEvaluation, "PTEvaluation");

var PTPlanOfCare = function() { BaseVisit.call(this); this.WindowId = "PTPlanOfCare"; };
Visit.Extend(PTPlanOfCare, "PTPlanOfCare");

var PTMaintenance = function() { BaseVisit.call(this); this.WindowId = "PTMaintenance"; };
Visit.Extend(PTMaintenance, "PTMaintenance");

var PTReEvaluation = function() { BaseVisit.call(this); this.WindowId = "PTReEvaluation"; };
Visit.Extend(PTReEvaluation, "PTReEvaluation");

var PTReassessment = function() { BaseVisit.call(this); this.WindowId = "PTReassessment"; };
Visit.Extend(PTReassessment, "PTReassessment");

var PTSupervisoryVisit = function() { BaseVisit.call(this); this.WindowId = "PTSupervisoryVisit"; };
Visit.Extend(PTSupervisoryVisit, "PTSupervisoryVisit");

var PTDischargeSummary = function() { BaseVisit.call(this); this.WindowId = "PTDischargeSummary"; };
Visit.Extend(PTDischargeSummary, "PTDischargeSummary", function(Prefix, e) {
    U.ShowIfChecked($(Prefix + "ServiceProvidedOther"), $(Prefix + "ServiceProvidedOtherValue"));
    U.ShowIfSelectEquals($(Prefix + "NotificationDate"), "3", $(Prefix + "NotificationDateOther"));
    U.ShowIfSelectEquals($(Prefix + "ReasonForDC"), "9", $(Prefix + "ReasonForDCOther"));
});

var STVisit = function() { BaseVisit.call(this); this.WindowId = "STVisit"; };
Visit.Extend(STVisit, "STVisit");

var STDischarge = function() { BaseVisit.call(this); this.WindowId = "STDischarge"; };
Visit.Extend(STDischarge, "STDischarge", function(Prefix, e) {
    U.ShowIfChecked($(Prefix + "GenericHomeboundReason9"), $(Prefix + "GenericHomeboundReasonOther"));
    U.ShowIfRadioEquals(Type + "_GenericOrdersForEvaluationOnly", "0", $(Prefix + "GenericIfNoOrdersAreSpan"));
    U.ShowIfRadioEquals(Type + "_GenericIsSSE", "1", $(Prefix + "GenericGenericSSESpecifySpan"));
    U.ShowIfRadioEquals(Type + "_GenericIsVideoFluoroscopy", "1", $(Prefix + "GenericVideoFluoroscopySpecify"));
    U.ShowIfChecked($(Prefix + "GenericLiquids2"), $(Prefix + "GenericLiquidsThick"));
    U.ShowIfChecked($(Prefix + "GenericLiquids3"), $(Prefix + "GenericLiquidsOther"));
    U.ShowIfChecked($(Prefix + "GenericReferralFor4"), $(Prefix + "GenericReferralForOther"));
    U.ShowIfChecked($(Prefix + "GenericDischargeDiscussedWith4"), $(Prefix + "GenericDischargeDiscussedWithOther"));
    U.ShowIfChecked($(Prefix + "GenericCareCoordination7"), $(Prefix + "GenericCareCoordinationOther"));
});

var STEvaluation = function() { BaseVisit.call(this); this.WindowId = "STEvaluation"; };
Visit.Extend(STEvaluation, "STEvaluation", function(Prefix, e) {
    U.ShowIfChecked($(Prefix + "GenericHomeboundReason9"), $(Prefix + "GenericHomeboundReasonOther"));
    U.ShowIfRadioEquals(Type + "_GenericOrdersForEvaluationOnly", "0", $(Prefix + "GenericIfNoOrdersAreSpan"));
    U.ShowIfRadioEquals(Type + "_GenericIsSSE", "1", $(Prefix + "GenericGenericSSESpecifySpan"));
    U.ShowIfRadioEquals(Type + "_GenericIsVideoFluoroscopy", "1", $(Prefix + "GenericVideoFluoroscopySpecify"));
    U.ShowIfChecked($(Prefix + "GenericLiquids2"), $(Prefix + "GenericLiquidsThick"));
    U.ShowIfChecked($(Prefix + "GenericLiquids3"), $(Prefix + "GenericLiquidsOther"));
    U.ShowIfChecked($(Prefix + "GenericReferralFor4"), $(Prefix + "GenericReferralForOther"));
    U.ShowIfChecked($(Prefix + "GenericDischargeDiscussedWith4"), $(Prefix + "GenericDischargeDiscussedWithOther"));
    U.ShowIfChecked($(Prefix + "GenericCareCoordination7"), $(Prefix + "GenericCareCoordinationOther"));
});

var STPlanOfCare = function() { BaseVisit.call(this); this.WindowId = "STPlanOfCare"; };
Visit.Extend(STPlanOfCare, "STPlanOfCare");

var STMaintenance = function() { BaseVisit.call(this); this.WindowId = "STMaintenance"; };
Visit.Extend(STMaintenance, "STMaintenance");

var STReEvaluation = function() { BaseVisit.call(this); this.WindowId = "STReEvaluation"; };
Visit.Extend(STReEvaluation, "STReEvaluation");

var STReassessment = function() { BaseVisit.call(this); this.WindowId = "STReassessment"; };
Visit.Extend(STReassessment, "STReassessment", function(Prefix, e) {
    U.ShowIfChecked($(Prefix + "GenericHomeboundReason9"), $(Prefix + "GenericHomeboundReasonOther"));
    U.ShowIfRadioEquals(Type + "_GenericOrdersForEvaluationOnly", "0", $(Prefix + "GenericIfNoOrdersAreSpan"));
    U.ShowIfRadioEquals(Type + "_GenericIsSSE", "1", $(Prefix + "GenericGenericSSESpecifySpan"));
    U.ShowIfRadioEquals(Type + "_GenericIsVideoFluoroscopy", "1", $(Prefix + "GenericVideoFluoroscopySpecify"));
    U.ShowIfChecked($(Prefix + "GenericLiquids2"), $(Prefix + "GenericLiquidsThick"));
    U.ShowIfChecked($(Prefix + "GenericLiquids3"), $(Prefix + "GenericLiquidsOther"));
    U.ShowIfChecked($(Prefix + "GenericReferralFor4"), $(Prefix + "GenericReferralForOther"));
    U.ShowIfChecked($(Prefix + "GenericDischargeDiscussedWith4"), $(Prefix + "GenericDischargeDiscussedWithOther"));
    U.ShowIfChecked($(Prefix + "GenericCareCoordination7"), $(Prefix + "GenericCareCoordinationOther"));
});

$.extend(Visit, {
    OTVisit: new OTVisit(),
    COTAVisit: new COTAVisit(),
    OTDischarge: new OTDischarge(),
    OTEvaluation: new OTEvaluation(),
    OTPlanOfCare: new OTPlanOfCare(),
    OTMaintenance: new OTMaintenance(),
    OTReEvaluation: new OTReEvaluation(),
    OTReassessment: new OTReassessment(),
    OTSupervisoryVisit: new OTSupervisoryVisit(),
    OTDischargeSummary: new OTDischargeSummary(),
    PTVisit: new PTVisit(),
    PTAVisit: new PTAVisit(),
    PTDischarge: new PTDischarge(),
    PTEvaluation: new PTEvaluation(),
    PTPlanOfCare: new PTPlanOfCare(),
    PTMaintenance: new PTMaintenance(),
    PTReEvaluation: new PTReEvaluation(),
    PTReassessment: new PTReassessment(),
    PTSupervisoryVisit: new PTSupervisoryVisit(),
    PTDischargeSummary: new PTDischargeSummary(),
    STVisit: new STVisit(),
    STDischarge: new STDischarge(),
    STEvaluation: new STEvaluation(),
    STPlanOfCare: new STPlanOfCare(),
    STMaintenance: new STMaintenance(),
    STReEvaluation: new STReEvaluation(),
    STReassessment: new STReassessment(),
    STDischargeSummary: new STDischargeSummary()
});