﻿var PASCarePlan = function() { BaseVisit.call(this); this.WindowId = "PASCarePlan"; };
Visit.Extend(PASCarePlan, "PASCarePlan", function(Prefix, e) {
    U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
    U.ShowIfChecked($(Prefix + "IsDiet"), $(Prefix + "Diet"));
    U.ShowIfChecked($(Prefix + "Allergies"), $(Prefix + "AllergiesDescription"));
    U.ShowIfChecked($(Prefix + "FunctionLimitationsB"), $(Prefix + "FunctionLimitationsOther"));
    U.ShowIfChecked($(Prefix + "ActivitiesPermitted12"), $(Prefix + "ActivitiesPermittedOther"));
});

var PASVisit = function() { BaseVisit.call(this); this.WindowId = "PASVisit"; };
Visit.Extend(PASVisit, "PASVisit", function(Prefix, e) {
    U.ShowIfSelectEquals($(Prefix + "HomeboundStatus"), "8", $(Prefix + "HomeboundStatusOther"));
    U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
    U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
});

var PASTravel = function() { BaseVisit.call(this); this.WindowId = "PASTravel"; };
Visit.Extend(PASTravel, "PASTravel");

$.extend(Visit, {
    PASCarePlan: new PASCarePlan(),
    PASVisit: new PASVisit(),
    PASTravel: new PASTravel()
});