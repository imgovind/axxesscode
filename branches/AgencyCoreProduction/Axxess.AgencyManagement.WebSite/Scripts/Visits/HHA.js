﻿var HHAideVisit = function() { BaseVisit.call(this); this.WindowId = "HHAideVisit"; };
Visit.Extend(HHAideVisit, "HHAideVisit", function(Prefix, e) {
    U.ShowIfSelectEquals($(Prefix + "HomeboundStatus"), "8", $(Prefix + "HomeboundStatusOther"));
    U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(Prefix + "IsVitalSignParameterMore"));
    U.HideIfChecked($(Prefix + "IsVitalSigns"), $(Prefix + "IsVitalSignsMore"));
});

var HHACarePlan = function() { BaseVisit.call(this); this.WindowId = "hhaCarePlan"; };
Visit.Extend(HHACarePlan, "HHACarePlan", function(Prefix, e) {
    U.HideIfChecked($(Prefix + "IsVitalSignParameter"), $(".vitalsigns", e));
    U.ShowIfChecked($(Prefix + "IsDiet"), $(Prefix + "Diet"));
    U.ShowIfChecked($(Prefix + "Allergies"), $(Prefix + "AllergiesDescription"));
    U.ShowIfChecked($(Prefix + "FunctionLimitationsB"), $(Prefix + "FunctionLimitationsOther"));
    U.ShowIfChecked($(Prefix + "ActivitiesPermitted12"), $(Prefix + "ActivitiesPermittedOther"));
});

var HomeMakerNote = function() { BaseVisit.call(this); this.WindowId = "HomeMakerNote"; };
Visit.Extend(HomeMakerNote, "HomeMakerNote");

var HHASVisit = function() { BaseVisit.call(this); this.WindowId = "hhasVisit"; };
Visit.Extend(HHASVisit, "HHASVisit");

$.extend(Visit, {
    HHACarePlan: new HHACarePlan(),
    HHAideVisit: new HHAideVisit(),
    HomeMakerNote: new HomeMakerNote(),
    HHASVisit: new HHASVisit()
});