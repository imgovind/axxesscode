﻿var DriverOrTransportationNote = function() { BaseVisit.call(this); this.WindowId = "transportationnote"; };
Visit.Extend(DriverOrTransportationNote, "DriverOrTransportationNote");

var MSWVisit = function() { BaseVisit.call(this); this.WindowId = "mswVisit"; };
Visit.Extend(MSWVisit, "MSWVisit");

var MSWProgressNote = function() { BaseVisit.call(this); this.WindowId = "mswProgressNote"; };
Visit.Extend(MSWProgressNote, "MSWProgressNote", function(Prefix, e) {
    U.ShowIfChecked($(Prefix + "GenericVisitGoals8"), $(Prefix + "GenericVisitGoalsOther"));
    U.ShowIfChecked($(Prefix + "GenericEmotionalStatus10"), $(Prefix + "GenericEmotionalStatusOther"));
    U.ShowIfChecked($(Prefix + "GenericPlannedInterventions11"), $(Prefix + "GenericPlannedInterventionsOther"));
});

var MSWAssessment = function() { BaseVisit.call(this); this.WindowId = "MSWAssessment"; };
Visit.Extend(MSWAssessment, "MSWAssessment", function(Prefix, e) {
    Visit.MSWEvaluationAssessment.InitContent(Prefix, e);
});

var MSWDischarge = function() { BaseVisit.call(this); this.WindowId = "MSWDischarge"; };
Visit.Extend(MSWDischarge, "MSWDischarge", function(Prefix, e) {
    Visit.MSWEvaluationAssessment.InitContent(Prefix, e);
});

var MSWEvaluationAssessment = function() { BaseVisit.call(this); this.WindowId = "MSWEvaluationAssessment"; };
Visit.Extend(MSWEvaluationAssessment, "MSWEvaluationAssessment", function(Prefix, e) {
    U.ShowIfChecked($(Prefix + "GenericLivingSituation10"), $(Prefix + "GenericLivingSituationOther"));
    U.ShowIfChecked($(Prefix + "GenericReasonForReferral11"), $(Prefix + "GenericReasonForReferralOther"));
    U.ShowIfChecked($(Prefix + "GenericEmotionalStatus10"), $(Prefix + "GenericEmotionalStatusOther"));
    U.ShowIfChecked($(Prefix + "GenericPlannedInterventions11"), $(Prefix + "GenericPlannedInterventionsOther"));
    U.ShowIfChecked($(Prefix + "GenericGoals8"), $(Prefix + "GenericGoalsOther"));
});

$.extend(Visit, {
    MSWEvaluationAssessment: new MSWEvaluationAssessment(),
    MSWAssessment: new MSWAssessment(),
    MSWDischarge: new MSWDischarge(),
    MSWVisit: new MSWVisit(),
    MSWProgressNote: new MSWProgressNote(),
    DriverOrTransportationNote: new DriverOrTransportationNote()
});