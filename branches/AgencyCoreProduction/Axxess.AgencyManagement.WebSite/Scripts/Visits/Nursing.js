﻿var SNVisit = function() { BaseVisit.call(this); this.WindowId = "snVisit"; };
Visit.Extend(SNVisit, "SNVisit", function(Prefix, e) {
    $(Prefix + "GenericBloodSugarSiteText", e).Autocomplete({ source: ["Thumb", "Index", "Middle", "Forearm", "1st  digit", "2nd  digit", "3rd  digit", "4th digit", "5th digit"] });
    U.HideIfChecked($(Prefix + "GenericIsIVApplied1", e), $(Prefix + "IVContainer", e));
    U.HideIfChecked($(Prefix + "GenericIsDiabeticCareApplied1", e), $(Prefix + "DiabeticCareContainer", e));
    U.HideIfChecked($(Prefix + "GenericIsPhlebotomyApplied1", e), $(Prefix + "GenericPhlebotomyContainer", e));
    $("select.Templates,.template-text > select", e).TemplateSelect();
});

var SNDiabeticDailyVisit = function() { BaseVisit.call(this); this.WindowId = "snDiabeticDailyVisit"; };
Visit.Extend(SNDiabeticDailyVisit, "SNDiabeticDailyVisit", function(Prefix, e) {
    U.ChangeToRadio(Prefix + "GenericCardiovascularRadialPosition");
    U.ChangeToRadio(Prefix + "GenericCardiovascularPedalPosition");
    U.ChangeToRadio(Prefix + "GenericEdemaNonPittingPosition");
    U.ChangeToRadio(Prefix + "GenericRespiratoryBreathSoundsPosition");
    U.ChangeToRadio(Prefix + "GenericBloodSugarSite");
    U.HideOptions();
});

var SNPediatricVisit = function() { BaseVisit.call(this); this.WindowId = "snPediatricVisit"; };
Visit.Extend(SNPediatricVisit, "SNPediatricVisit", function(Prefix, e) {
    $(Prefix + "GenericBloodSugarSiteText").Autocomplete({ source: ["Thumb", "Index", "Middle", "Forearm", "1st  digit", "2nd  digit", "3rd  digit", "4th digit", "5th digit"] });
    U.HideIfChecked($(Prefix + "GenericIsIVApplied1"), $(Prefix + "IVContainer"));
    U.HideIfChecked($(Prefix + "GenericIsDiabeticCareApplied1"), $(Prefix + "DiabeticCareContainer"));
    U.HideIfChecked($(Prefix + "GenericIsPhlebotomyApplied1"), $(Prefix + "GenericPhlebotomyContainer"));
});

var SNPediatricAssessment = function() { BaseVisit.call(this); this.WindowId = "snPediatricAssessment"; };
Visit.Extend(SNPediatricAssessment, "SNPediatricAssessment", function(Prefix, e) {
    $(Prefix + "GenericBloodSugarSiteText").Autocomplete({ source: ["Thumb", "Index", "Middle", "Forearm", "1st  digit", "2nd  digit", "3rd  digit", "4th digit", "5th digit"] });
    U.HideIfChecked($(Prefix + "GenericIsIVApplied1"), $(Prefix + "IVContainer"));
    U.HideIfChecked($(Prefix + "GenericIsDiabeticCareApplied1"), $(Prefix + "DiabeticCareContainer"));
    U.HideIfChecked($(Prefix + "GenericIsPhlebotomyApplied1"), $(Prefix + "GenericPhlebotomyContainer"));
});

var SNLabs = function() { BaseVisit.call(this); this.WindowId = "snLabs"; };
Visit.Extend(SNLabs, "SNLabs");

var LVNSVisit = function() { BaseVisit.call(this); this.WindowId = "lvnsVisit"; };
Visit.Extend(LVNSVisit, "LVNSVisit");

var SNPsychVisit = function() { BaseVisit.call(this); this.WindowId = "snPsychVisit"; };
Visit.Extend(SNPsychVisit, "SNPsychVisit");

var SNPsychAssessment = function() { BaseVisit.call(this); this.WindowId = "snPsychAssessment"; };
Visit.Extend(SNPsychAssessment, "SNPsychAssessment");

var SixtyDaySummary = function() { BaseVisit.call(this); this.WindowId = "sixtyDaySummary"; };
Visit.Extend(SixtyDaySummary, "SixtyDaySummary", function(Prefix, e) {
    U.ShowIfSelectEquals($(Prefix + "NotificationDate"), "3", $(Prefix + "NotificationDateOther"));
    U.ShowIfChecked($(Prefix + "HomeboundStatusOtherCheck"), $(Prefix + "HomeboundStatusOther"));
    U.ShowIfChecked($(Prefix + "ServiceProvidedOther"), $(Prefix + "ServiceProvidedOtherValue"));
    U.ShowIfChecked($(Prefix + "RecommendedServiceOther"), $(Prefix + "RecommendedServiceOtherValue"));
});

var TransferSummary = function() { BaseVisit.call(this); this.WindowId = "transferSummary"; };
Visit.Extend(TransferSummary, "TransferSummary", function(Prefix, e) {
    U.ShowIfChecked($(Prefix + "ServiceProvidedOther"), $(Prefix + "ServiceProvidedOtherValue"));
    U.ShowIfChecked($(Prefix + "FunctionLimitationsB"), $(Prefix + "FunctionLimitationsOther"));
    var innerType = this.Type;
    $(Prefix + "Physician").change(function() {
        if ($(Prefix + "Physician").val() != "") {
            Physician.GetPhone(innerType, "Physician");
        }
    });
});

var CoordinationOfCare = function() { BaseVisit.call(this); this.WindowId = "coordinationofcare"; };
Visit.Extend(CoordinationOfCare, "CoordinationOfCare", function(Prefix, e) {
    Visit.TransferSummary.InitContent(Prefix, e);
});

var InitialSummaryOfCare = function() { BaseVisit.call(this); this.WindowId = "ISOC"; };
Visit.Extend(InitialSummaryOfCare, "InitialSummaryOfCare");

var DischargeSummary = function() { BaseVisit.call(this); this.WindowId = "dischargeSummary"; };
Visit.Extend(DischargeSummary, "DischargeSummary", function(Prefix, e) {
    U.ShowIfSelectEquals($(Prefix + "NotificationDate"), "3", $(Prefix + "NotificationDateOther"));
    U.ShowIfChecked($(Prefix + "DischargeInstructionsGivenTo4"), $(Prefix + "DischargeInstructionsGivenToOther"));
    U.ShowIfChecked($(Prefix + "ServiceProvidedOther"), $(Prefix + "ServiceProvidedOtherValue"));
});

$.extend(Visit, {
    SNVisit: new SNVisit(),
    SNDiabeticDailyVisit: new SNDiabeticDailyVisit(),
    SNPediatricVisit: new SNPediatricVisit(),
    SNPediatricAssessment: new SNPediatricAssessment(),
    SNLabs: new SNLabs(),
    LVNSVisit: new LVNSVisit(),
    SNPsychVisit: new SNPsychVisit(),
    SNPsychAssessment: new SNPsychAssessment(),
    SixtyDaySummary: new SixtyDaySummary(),
    TransferSummary: new TransferSummary(),
    CoordinationOfCare: new CoordinationOfCare(),
    InitialSummaryOfCare: new InitialSummaryOfCare(),
    DischargeSummary: new DischargeSummary()
});