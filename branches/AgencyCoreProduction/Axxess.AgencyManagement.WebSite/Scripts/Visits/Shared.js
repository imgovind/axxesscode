﻿var BaseVisit = function() {
    if (!this.Type) this.Type = "";
    this.WindowId = "";
    this.ContentUrl = "";
};
BaseVisit.prototype = {
    Init: function(r, t, x, e) {
        this.Type = $("input[name=Type]", e).val();
        var prefix = "#" + this.Type + "_",
            $form = $("form", e),
            innerType = this.Type,
            input = { EpisodeId: $(prefix + "EpisodeId", $form).val(), PatientId: $(prefix + "PatientId", $form).val(), EventId: $(prefix + "EventId", $form).val() };
        if (!this.ContentUrl) this.ContentUrl = "Schedule/" + this.Type + "Content";
        Template.OnChangeInit();
        $(".load-disciplinetask", $form).bind("click", function() { Schedule.Task.LoadTasks($(this), { LocationId: $("input[name=LocationId]", $form).val(), DisciplineTask: $("[name=DisciplineTask]", $form).val() }); });
        //Initializes the Physician picker
        var physicianInput = $(".Physicians", $form);
        if (physicianInput.length) physicianInput.PhysicianInput();
        if ($(prefix + "TimeIn").length != 0) Schedule.CheckTimeInOut(this.Type, true);
        $form.Validate({
            PreprocessForm: function() {
                var buttonVal = $form.data("Button");
                $("[name=button]", $form).val(buttonVal);
                return !Schedule.CheckTimeInOut(prefix, buttonVal == "Save");
            },
            Success: function() {
                // If the form was autosaved, we don't want to do Growl messages or refresh the UI.
                if ($form.data('autosaved')) {
                    $form.data({
                        autosaved: false,
                        changed: false
                    });
                    return;
                }
                var patientId = $("#" + innerType + "_PatientId", $form).val();
                switch ($form.data("Button")) {
                    case "Save":
                        UserInterface.Refresh(patientId);
                        break;
                    case "Complete":
                    case "Approve":
                    case "Return":
                        UserInterface.CloseAndRefresh(innerType, patientId);
                        break;
                }
            },
            Fail: function() {
                if ($form.data('autosaved')) {
                    $form.data({
                        autosaved: false
                    });
                    return;
                }
            }
        });
        Visit.Shared.InitPreviousNotes($form, input, this.InitContent, this.ContentUrl, prefix, this.Type);
        this.InitContent(prefix, e);
    },
    InitContent: function(Prefix, e) {

    },
    Load: function(EpisodeId, PatientId, EventId) {
        Acore.Open(this.WindowId, { episodeId: EpisodeId, patientId: PatientId, eventId: EventId });
    },
    Print: function(EpisodeId, PatientId, EventId, QA) {
        Visit.Shared.Print(EpisodeId, PatientId, EventId, QA, this.Type);
    }
};

var Visit = {
    Extend: function(childClass, visitReference, initContentFunction) {
        childClass.prototype = new BaseVisit();
        childClass.prototype.constructor = childClass;
        childClass.prototype.Init = function(r, t, x, e) {
            BaseVisit.prototype.Init.call(Visit[visitReference], r, t, x, e);
        };
        childClass.prototype.Type = visitReference;
        if (typeof initContentFunction == "function") childClass.prototype.InitContent = initContentFunction;
    },
    Shared: {
        PreviousNoteList: function(input, currentItem, callback) {
            U.PostUrl("/Schedule/Previous", input, function(data) {
                var options = [];
                $.each(data, function(index, itemData) { options[index] = new Option(itemData.Text, itemData.Value, false, false); });
                if (typeof callback == "function") callback(currentItem, options);
            });
        },
        InitPreviousNotes: function(form, baseInput, initFunction, contentUrl, prefix, type) {
            $(".load-previousnotes", form).click(function() {
                var $this = $(this);
                var previousNoteInput = $.extend({}, baseInput, { DisciplineTask: $("[name=DisciplineTask]", form).val(), EventDate: $("input[name=EventDate]", form).val() });
                Visit.Shared.PreviousNoteList(previousNoteInput, $this.closest("div.buttons").next(".previousnotes-list"),
                function(currentSelect, options) {
                    currentSelect.empty();
                    currentSelect.append(options);
                    currentSelect.show();
                    $this.closest("div.buttons").hide(); //.closest("span").hide();
                });
            });
            $(".previousnotes-list", form).change(function() {
                var $this = $(this);
                var noteId = $this.val();
                if (noteId && noteId != U.GuidEmpty) {
                    window.setTimeout(function() {
                        if (confirm("Are you sure you want to load from the previous note If you load the previous note it will over write any data in this document.")) {
                            $(prefix + "Content").Load(contentUrl, { patientId: baseInput.PatientId, noteId: noteId, type: type }, function(r, t, x, e) { initFunction(e); });
                        }
                    }, 0);
                }
            });
        },
        //        Submit: function(Button, Completing, Type) {
        //            var $form = Button.closest("form");
        //            if (Completing) $(".complete-required", "#window_" + Type).addClass("required");
        //            else $(".complete-required", "#window_" + Type).removeClass("required");
        //            $("#" + Type + "_Button").val(Button.text());
        //            $form.validate();
        //            if ($form.valid()) {
        //                $form.ajaxSubmit({
        //                    dataType: "json",
        //                    data: { AuditSilence: $form.data('autosaved') },
        //                    beforeSubmit: Visit.Shared.BeforeSubmit,
        //                    success: function(Result) { Visit.Shared.AfterSubmit(Result, Button.text(), Type, $form) },
        //                    error: function(Result) { Visit.Shared.AfterSubmit(Result, Button.text(), Type, $form) }
        //                })
        //            } else U.ValidationError(Button);
        //        },
        //        BeforeSubmit: function() { },
        //        AfterSubmit: function(Result, Command, Type, $form) {
        //            // If the form was autosaved, we don't want to do Growl messages or refresh the UI.
        //            if ($form.data('autosaved')) {
        //                $form.data({
        //                    autosaved: false,
        //                    changed: false
        //                });
        //                return;
        //            }
        //            var patientId = $("#" + Type + "_PatientId").val();
        //            if (Result.isSuccessful) switch (Command) {
        //                case "Save":
        //                    U.Growl(Result.errorMessage, "success");
        //                    UserInterface.Refresh(patientId);
        //                    break;
        //                case "Complete":
        //                case "Approve":
        //                case "Return":
        //                    U.Growl(Result.errorMessage, "success");
        //                    UserInterface.CloseAndRefresh(Type, patientId);
        //                    break;
        //            } else {
        //                if (Result.errorMessage) {
        //                    U.Growl(Result.errorMessage, "error");
        //                }
        //                else {
        //                    U.Growl("A problem occured while saving the document. Please check your connection and try again, if the problem persists, contact Axxess for assistance.", "error");
        //                }

        //            };
        //        },
        Load: function(Options) { if (Options && Options.Type) { Acore.Open(Options.Type, Options.Data, Options.Url); } },
        Print: function(EpisodeId, PatientId, EventId, QA, Type, Load) {
            if (Load == undefined) Load = function() {
                Visit[Type].Load(EpisodeId, PatientId, EventId);
                UserInterface.CloseModal();
            };
            var Arguments = {
                Url: Type + "/View/" + EpisodeId + "/" + PatientId + "/" + EventId,
                PdfUrl: "Schedule/" + Type + "Pdf",
                PdfData: {
                    episodeId: EpisodeId,
                    patientId: PatientId,
                    eventId: EventId
                }
            };
            if (QA) $.extend(Arguments, {
                ReturnClick: function() { Schedule.ProcessNote("Return", EpisodeId, PatientId, EventId) },
                Buttons: [
                    {
                        Text: "Edit",
                        Click: function() {
                            if (typeof Load === "function") {
                                try {
                                    Load();
                                } catch (e) {
                                    U.Growl("Unable to open note for editing.  Please contact Axxess for assistance.", "error");
                                    console.log("Error in edit function on load for " + Type + ": " + e.toString());
                                    $.error(e)
                                }
                            }
                        }
                    }, {
                        Text: "Approve",
                        Click: function() {
                            Schedule.ProcessNote("Approve", EpisodeId, PatientId, EventId)
                        }
                    }
                ]
            });
            Acore.OpenPrintView(Arguments);
        }
    }
};