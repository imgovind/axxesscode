﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;

using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class IllinoisAgencyScript
    {
        private static ISheet sheet;
        private static HSSFWorkbook workBook;
        private static string input = Path.Combine(App.Root, "Files\\IllinoisAgencies.txt");
        private static string output = Path.Combine(App.Root, string.Format("Files\\IllinoisAgencies_{0}.xls", DateTime.Now.Ticks.ToString()));
        public static void Run()
        {
            Initialize();

            using (var fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
            {
                using (var streamReader = new StreamReader(fileStream))
                {
                    int i = 1;
                    while (streamReader.Peek() >= 0)
                    {
                        var line = streamReader.ReadLine();
                        if (line.IsNotNullOrEmpty())
                        {
                            //var lineArray = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            //if (lineArray != null && lineArray.Length > 0)
                            //{
                            //    lineArray.ForEach(word =>
                            //    {
                            //        if (word.Length > 2 && word.ToLower().Contains("fax"))
                            //        {
                            //            Row excelRow = sheet.CreateRow(i);

                            //            var startIndex = line.ToLower().IndexOf("fax") + word.Length;
                            //            if (line.Length >= startIndex + 15)
                            //            {
                            //                var faxNumber = line.Substring(startIndex, 16);
                            //                excelRow.CreateCell(0).SetCellValue(faxNumber.Trim().ToPhoneDB().ToDigitsOnly());
                            //            }

                            //            i++;
                            //        }
                            //    });
                            //}

                            var lineArray = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            if (lineArray != null && lineArray.Length > 0)
                            {
                                lineArray.ForEach(word =>
                                {
                                    if (word.Length > 3 && word.Contains("@"))
                                    {
                                        var excelRow = sheet.CreateRow(i);
                                        excelRow.CreateCell(0).SetCellValue(word.ToLower());

                                        i++;
                                    }
                                });
                            }
                        }
                    }
                }
                Write();
            }
        }

        private static void Initialize()
        {
            workBook = new HSSFWorkbook();

            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            workBook.SummaryInformation = si;

            sheet = workBook.CreateSheet("California State Agencies");

            var headerFont = workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            var headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Email Addresses");
            headerRow.RowStyle = headerStyle;
        }

        private static void Write()
        {
            sheet.AutoSizeColumn(0);

            using (FileStream fileStream = new FileStream(output, FileMode.OpenOrCreate, FileAccess.Write))
            {
                workBook.Write(fileStream);
            }
        }
    }
}
