﻿using System;
using System.IO;
using System.Data;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using System.Threading;

namespace Axxess.ConsoleApp.Tests
{
    public static class NPIUpdate
    {
        private static string input = @"C:\\Users\\Administrator\\Downloads\\NPIUPDATE\\NPPES_Data_Dissemination_Aug_2013\npidata_20050523-20130811.csv";
        private static string output = Path.Combine(App.Root, string.Format("npidata_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            int counter = 1;
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                var start = DateTime.Now;
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (var csvReader = new CsvReader(fileStream))
                        {
                            if (csvReader != null)
                            {
                                csvReader.ReadHeaderRecord();
                                foreach (var record in csvReader.DataRecords)
                                {
                                    if (record.GetValue(1).IsNotNullOrEmpty())
                                    {
                                        var npiData = new NpiData();
                                        npiData.Id = record.GetValue(0);
                                        npiData.EntityTypeCode = record.GetValue(1);
                                        npiData.ProviderOrganizationName = record.GetValue(4);
                                        npiData.ProviderFirstName = record.GetValue(6);
                                        npiData.ProviderMiddleName = record.GetValue(7);
                                        npiData.ProviderLastName = record.GetValue(5);
                                        npiData.ProviderCredentialText = record.GetValue(10);
                                        npiData.ProviderFirstLineBusinessMailingAddress = record.GetValue(20);
                                        npiData.ProviderSecondLineBusinessMailingAddress = record.GetValue(21);
                                        npiData.ProviderBusinessMailingAddressCityName = record.GetValue(22);
                                        npiData.ProviderBusinessMailingAddressStateName = record.GetValue(23);
                                        npiData.ProviderBusinessMailingAddressPostalCode = record.GetValue(24);
                                        npiData.ProviderBusinessMailingAddressCountryCode = record.GetValue(25);
                                        npiData.ProviderBusinessMailingAddressTelephoneNumber = record.GetValue(26);
                                        npiData.ProviderBusinessMailingAddressFaxNumber = record.GetValue(27);
                                        npiData.ProviderFirstLineBusinessPracticeLocationAddress = record.GetValue(28);
                                        npiData.ProviderSecondLineBusinessPracticeLocationAddress = record.GetValue(29);
                                        npiData.ProviderBusinessPracticeLocationAddressCityName = record.GetValue(30);
                                        npiData.ProviderBusinessPracticeLocationAddressStateName = record.GetValue(31);
                                        npiData.ProviderBusinessPracticeLocationAddressPostalCode = record.GetValue(32);
                                        npiData.ProviderBusinessPracticeLocationAddressCountryCode = record.GetValue(33);
                                        npiData.ProviderBusinessPracticeLocationAddressTelephoneNumber = record.GetValue(34);
                                        npiData.ProviderBusinessPracticeLocationAddressFaxNumber = record.GetValue(35);

                                        Console.WriteLine("Inserting Row {0}: {1}", counter.ToString(), npiData.ToString());
                                        var sql = new PhysicianScript(npiData).ToString();
                                        using (var cmd = new FluentCommand<int>(sql))
                                        {
                                            cmd.SetConnection("AxxessLookupConnectionString");
                                            if (cmd.AsNonQuery() > 0)
                                            {
                                                Console.WriteLine("Inserted Row {0}: {1}", counter.ToString(), npiData.ToString());
                                            }
                                            else
                                            {
                                                Console.WriteLine("Failed to insert Row {0}: {1}", counter.ToString(), npiData.ToString());
                                                textWriter.WriteLine("Failed to insert Row {0}: {1}", counter.ToString(), npiData.ToString());
                                            }
                                        }
                                        Console.WriteLine();
                                        counter++;
                                    }
                                }
                            }
                        }
                    }
                    var timeSpan = (DateTime.Now - start).TotalMinutes;

                    Console.WriteLine(string.Format("NPI _ Total Time spent:{0} mins ", timeSpan.ToString()));

                }
                catch (Exception ex)
                {
                    textWriter.WriteLine(ex.ToString());
                }
            }
        }
    }
}
