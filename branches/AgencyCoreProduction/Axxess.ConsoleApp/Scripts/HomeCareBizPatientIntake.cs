﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;
    using Kent.Boogaart.KBCsv;
    using Axxess.Core.Extension;

    public static class HomeCareBizPatientIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\daystar.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\daystar_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(DataFormat format)
        {
            using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
            {
                if (format == DataFormat.XLX)
                {
                    ExcelLoad(fileStream);
                }
                else
                {
                    CsvLoad(fileStream);
                }
            }
        }

        private static void ExcelLoad(FileStream fileStream)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                {
                    if (excelReader != null && excelReader.IsValid)
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        DataTable dataTable = excelReader.AsDataSet().Tables[0];
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                var patientData = new PatientData();
                                patientData.AgencyId = "3a6836e4-e258-4c14-9508-71fa0f2a73ba";
                                patientData.AgencyLocationId = "70ff1333-e248-4e6c-bc62-eab17d8e06e6";
                                patientData.Ethnicity = "3;";
                                patientData.MedicareNumber = "";
                                patientData.PatientNumber = dataRow.GetValue(0);
                                patientData.FirstName = dataRow.GetValue(1).ToTitleCase();
                                patientData.MiddleInitial = dataRow.GetValue(2);
                                patientData.LastName = dataRow.GetValue(3).ToTitleCase();
                                patientData.Gender = dataRow.GetValue(4) == "1" ? "Male" : "Female";
                                patientData.BirthDate = dataRow.GetValue(5).ToMySqlDateString();
                                patientData.AddressLine1 = dataRow.GetValue(6).ToTitleCase();
                                patientData.AddressCity = dataRow.GetValue(8).ToTitleCase();
                                patientData.AddressState = dataRow.GetValue(9);
                                patientData.AddressZipCode = dataRow.GetValue(10);
                                patientData.Phone = dataRow.GetValue(11);
                                patientData.StartofCareDate = dataRow.GetValue(12).ToMySqlDate(0);

                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                textWriter.Write(textWriter.NewLine);
                            }
                        }
                    }
                }
            }
        }

        private static void CsvLoad(FileStream fileStream)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (var csvReader = new CsvReader(fileStream))
                {
                    if (csvReader != null)
                    {
                        csvReader.ReadHeaderRecord();
                        foreach (var record in csvReader.DataRecords)
                        {
                            var patientData = new PatientData();
                            patientData.AgencyId = "3a6836e4-e258-4c14-9508-71fa0f2a73ba";
                            patientData.AgencyLocationId = "70ff1333-e248-4e6c-bc62-eab17d8e06e6";
                            patientData.PatientNumber = record.GetValue(13);
                            patientData.Ethnicity = "3;";
                            patientData.Gender = record.GetValue(12).IsEqual("2") ? "Female" : "Male";
                            patientData.MedicareNumber = "";
                            patientData.FirstName = record.GetValue(0).ToTitleCase();
                            patientData.LastName = record.GetValue(2).ToTitleCase();
                            patientData.MiddleInitial = record.GetValue(1);
                            patientData.BirthDate = record.GetValue(10).ToMySqlDateString();
                            patientData.AddressLine1 = record.GetValue(3).ToTitleCase();
                            patientData.AddressLine2 = "";
                            patientData.AddressCity = record.GetValue(4).ToTitleCase();
                            patientData.AddressState = record.GetValue(5);
                            patientData.AddressZipCode = record.GetValue(6);
                            patientData.StartofCareDate = record.GetValue(8).ToMySqlDateString();
                            patientData.Phone = record.GetValue(7);
                            if (record.GetValue(39).IsNotNullOrEmpty())
                            {
                                patientData.Comments += string.Format("Primary Diagnosis Code: {0}. ", record.GetValue(39));
                            }
                            if (record.GetValue(40).IsNotNullOrEmpty())
                            {
                                patientData.Comments += string.Format("Other Diagnosis Code 1: {0}. ", record.GetValue(40));
                            }
                            if (record.GetValue(41).IsNotNullOrEmpty())
                            {
                                patientData.Comments += string.Format("Other Diagnosis Code 2: {0}. ", record.GetValue(41));
                            }
                            if (record.GetValue(42).IsNotNullOrEmpty())
                            {
                                patientData.Comments += string.Format("Other Diagnosis Code 3: {0}. ", record.GetValue(42));
                            }
                            if (record.GetValue(43).IsNotNullOrEmpty())
                            {
                                patientData.Comments += string.Format("Other Diagnosis Code 4: {0}. ", record.GetValue(43));
                            }
                            if (record.GetValue(44).IsNotNullOrEmpty())
                            {
                                patientData.Comments += string.Format("Other Diagnosis Code 5: {0}. ", record.GetValue(44));
                            }

                            textWriter.WriteLine(new PatientScript(patientData).ToString());
                            textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                            textWriter.Write(textWriter.NewLine);
                        }
                    }
                }
            }
        }
    }
}
