﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class SynergyIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\gateway.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\gateway_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        var patientData = new PatientData();
                                        patientData.AgencyId = "146a3b4b-e47a-499f-93a3-2f1af8de04d7";
                                        patientData.AgencyLocationId = "62b1d5fb-c718-4274-b2ef-df43666a2098";
                                        patientData.Gender = "Male";
                                        patientData.MiddleInitial = "";
                                        patientData.PatientNumber = dataRow.GetValue(5);
                                        patientData.MedicareNumber = dataRow.GetValue(1);
                                        patientData.Phone = dataRow.GetValue(4).ToPhoneDB();

                                        if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                        {
                                            patientData.BirthDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(6))).ToString("yyyy-M-d");
                                        }

                                        if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                        {
                                            patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(7))).ToString("yyyy-M-d");
                                        }

                                        if (dataRow.GetValue(0).IsNotNullOrEmpty())
                                        {
                                            var nameArray = dataRow.GetValue(0).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length > 1)
                                            {
                                                patientData.FirstName = nameArray[1].Trim();
                                                patientData.LastName = nameArray[0].Trim();
                                            }
                                        }

                                        if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                        {
                                            var addressArray = dataRow.GetValue(2).Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries);
                                            if (addressArray != null && addressArray.Length > 1)
                                            {
                                                patientData.AddressLine1 = addressArray[0].Replace("'", "");
                                                patientData.AddressLine2 = "";

                                                var locationArray = addressArray[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                if (locationArray != null && locationArray.Length > 2)
                                                {
                                                    locationArray = locationArray.Reverse();
                                                    patientData.AddressZipCode = locationArray[0];
                                                    patientData.AddressState = locationArray[1];

                                                    if (locationArray.Length > 3)
                                                    {
                                                        int cityNameCount = 2;
                                                        var city = new string[locationArray.Length - cityNameCount];

                                                        do
                                                        {
                                                            city[cityNameCount - 2] = locationArray[cityNameCount];
                                                            cityNameCount++;
                                                        } while (cityNameCount < locationArray.Length);

                                                        if (city.Length > 0)
                                                        {
                                                            city = city.Reverse();
                                                            city.ForEach(name =>
                                                            {
                                                                patientData.AddressCity += string.Format("{0} ", name.Replace(",", ""));
                                                            });
                                                            patientData.AddressCity = patientData.AddressCity.Trim();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        patientData.AddressCity = locationArray[2].Replace(",", "");
                                                    }
                                                }
                                            }
                                        }

                                        if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                        {
                                            var status = dataRow.GetValue(8).ToLower();
                                            if (status.Contains("admitted"))
                                            {
                                                patientData.PatientStatusId = "1";
                                            }
                                            else
                                            {
                                                patientData.PatientStatusId = "3";
                                            }
                                        }

                                        if (dataRow.GetValue(9).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("DX Code: {0}. ", dataRow.GetValue(9));
                                        }

                                        if (dataRow.GetValue(10).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("Physician/NPI: {0} {1}. "
                                                , dataRow.GetValue(10)
                                                , dataRow.GetValue(11).ToPhone());
                                        }

                                        if (dataRow.GetValue(15).IsNotNullOrEmpty())
                                        {
                                            patientData.Comments += string.Format("Current Insurance: {0}. ", dataRow.GetValue(15));
                                        }

                                        patientData.Comments = patientData.Comments.Trim();

                                        textWriter.WriteLine(new PatientScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                        textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());

                                        textWriter.Write(textWriter.NewLine);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
