﻿namespace Axxess.ConsoleApp.Tests
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    using SubSonic.Repository;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Repositories;

  public static class PhysicianOrderPhysicianLoad
    {
        private static string output = Path.Combine(App.Root, string.Format("Files\\PhysicianOrder_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static readonly IPatientRepository patientRepository = new PatientRepository(new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None));
        private static readonly IPhysicianRepository physicianRepository = new PhysicianRepository(new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None));
        public static void Run()
        {
            var physicianOrders = patientRepository.GetAllPhysicianOrders();
            if (physicianOrders != null && physicianOrders.Count > 0)
            {
                using (TextWriter textWriter = new StreamWriter(output, true))
                {
                    foreach (var order in physicianOrders)
                    {
                        if (!order.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(order.PhysicianId, order.AgencyId);
                            if (physician != null)
                            {
                                var xmlData = new StringBuilder(physician.ToXml());
                                var data = xmlData.Replace("'", @"\'");

                                textWriter.WriteLine(string.Format("UPDATE `physicianorders` SET `PhysicianData` = '{0}'   WHERE   `Id` = '{1}' AND `AgencyId` = '{2}' ;", data, order.Id, order.AgencyId));
                                textWriter.Write(textWriter.NewLine);

                            }
                        }
                    }
                }
            }
        }
    }
}
