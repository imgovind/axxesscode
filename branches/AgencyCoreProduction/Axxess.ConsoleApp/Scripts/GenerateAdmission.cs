﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    using SubSonic.Repository;
    using System.Text.RegularExpressions;

   public static class GenerateAdmission
    {
       private static string output = Path.Combine(App.Root, string.Format("Files\\GenerateAdmission_{0}.txt", DateTime.Now.Ticks.ToString()));
       private static readonly IPatientRepository patientRepository = new PatientRepository( new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None));
       private static readonly IEpisodeRepository episodeRepository = new EpisodeRepository(new SimpleRepository("AgencyManagementConnectionString", SimpleRepositoryOptions.None));
       public static void Run()
       {
           var patients = patientRepository.All();
           if (patients != null && patients.Count > 0)
           {
               using (TextWriter textWriter = new StreamWriter(output, true))
               {
                   foreach (var patient in patients)
                   {
                       var admissionId= Guid.NewGuid();
                       patient.AdmissionId = admissionId;
                       var xmlData = new StringBuilder(patient.ToXml());
                       var patientData = xmlData.Replace("'", @"\'");
                       textWriter.WriteLine(string.Format("INSERT INTO `patientadmissiondates`(`Id`,`AgencyId`,`PatientId`,`StartOfCareDate`,`DischargedDate`,`PatientData`, `Status`,`IsActive`,`IsDeprecated`,`Created`,`Modified`,`Reason`)  VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}');", admissionId, patient.AgencyId, patient.Id, patient.StartofCareDate.ToString("yyyy-MM-dd"), patient.DischargeDate.ToString("yyyy-MM-dd"), patientData, patient.Status, "1", "0", DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), string.Empty));
                       textWriter.Write(textWriter.NewLine);
                       textWriter.WriteLine(string.Format("UPDATE `patients` SET `AdmissionId` = '{0}'   WHERE   `Id` = '{1}' AND `AgencyId` = '{2}' ;", admissionId, patient.Id, patient.AgencyId));
                       textWriter.Write(textWriter.NewLine);

                       var episodes = episodeRepository.GetPatientAllEpisodesWithNoException(patient.AgencyId, patient.Id);
                       if (episodes != null && episodes.Count > 0)
                       {
                           foreach (var episode in episodes)
                           {
                               textWriter.WriteLine(string.Format("UPDATE `patientepisodes` SET `AdmissionId` = '{0}'   WHERE   `Id` = '{1}' AND `AgencyId` = '{2}' ;", admissionId, episode.Id, episode.AgencyId));
                           }
                       }

                       textWriter.Write(textWriter.NewLine);
                       textWriter.Write(textWriter.NewLine);
                   }
               }
           }
       }
    }

}
