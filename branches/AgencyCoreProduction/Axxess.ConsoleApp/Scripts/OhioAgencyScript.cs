﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;

using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class OhioAgencyScript
    {
        private static ISheet sheet;
        private static HSSFWorkbook workBook;
        private static string input = Path.Combine(App.Root, "Files\\Ohio_Home_Health_Agency.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\OhioAgencies_{0}.xls", DateTime.Now.Ticks.ToString()));
        public static void Run()
        {
            Initialize();

            using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                {
                    if (excelReader != null && excelReader.IsValid)
                    {
                        excelReader.IsFirstRowAsColumnNames = false;
                        DataTable dataTable = excelReader.AsDataSet().Tables[0];
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            int i = 1;
                            int rowCounter = 0;
                            int dataCounter = 0;
                            IRow excelRow = null;

                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!dataRow.IsEmpty())
                                {
                                    switch (dataCounter)
                                    {
                                        case 0:
                                            excelRow = sheet.CreateRow(i);
                                            excelRow.CreateCell(0).SetCellValue(dataRow.GetValue(2)); // Agency Name
                                            Console.WriteLine("{0}", dataRow.GetValue(2));
                                            break;
                                        case 3:
                                            excelRow.CreateCell(1).SetCellValue(dataRow.GetValue(7).Replace("Fax #: ", "").ToPhoneDB()); // Fax #: (614) 794-0326
                                            Console.WriteLine("{0}", dataRow.GetValue(7));
                                            break;
                                        case 4:
                                            if (dataRow.GetValue(2).IsNotNullOrEmpty() && !dataRow.GetValue(2).Contains("not available"))
                                            {
                                                var email = dataRow.GetValue(2).Replace("E-Mail Address: ", "");
                                                excelRow.CreateCell(2).SetCellValue(email); // Email Address
                                            }
                                            Console.WriteLine("{0}", dataRow.GetValue(2));
                                            Console.WriteLine();
                                            i++;
                                            break;
                                    }
                                    dataCounter++;
                                }
                                else
                                {
                                    dataCounter = 0;
                                }
                                rowCounter++;
                            }

                            Write();
                        }
                    }
                }
            }
        }
        private static void Initialize()
        {
            workBook = new HSSFWorkbook();

            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            workBook.SummaryInformation = si;

            sheet = workBook.CreateSheet("Ohio State Agencies");

            var headerFont = workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            var headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Agency Name");
            headerRow.CreateCell(1).SetCellValue("Fax Number");
            headerRow.CreateCell(2).SetCellValue("Email Address");
            headerRow.RowStyle = headerStyle;
        }

        private static void Write()
        {
            int columnCounter = 0;
            do
            {
                sheet.AutoSizeColumn(columnCounter);
                columnCounter++;
            }
            while (columnCounter < 3);

            using (FileStream fileStream = new FileStream(output, FileMode.OpenOrCreate, FileAccess.Write))
            {
                workBook.Write(fileStream);
            }
        }
    }
}
