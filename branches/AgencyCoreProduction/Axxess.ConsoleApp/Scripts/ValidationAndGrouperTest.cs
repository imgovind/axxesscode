﻿using System;

using Axxess.Api;

namespace Axxess.ConsoleApp.Tests
{
    public static class ValidationAndGrouperTest
    {
        private static void ValidationAndGrouper(string oasisDataString)
        {
            Console.WriteLine("OASIS Data String: {0}", oasisDataString);
            Console.WriteLine("OASIS Data String Length: {0}", oasisDataString.Length);

            try
            {
                Console.WriteLine("Validating ...");
                var validationAgent = new ValidationAgent();
                var result = validationAgent.ValidateAssessment(oasisDataString);

                Console.WriteLine("Found {0} Errors", result.Count);
                if (result != null && result.Count <= 2)
                {
                    Console.WriteLine("Grouper ...");
                    var grouperAgent = new GrouperAgent();
                    Axxess.Api.Contracts.Hipps hipps = grouperAgent.GetHippsCode(oasisDataString);
                    Console.WriteLine("HIPPS code: {0}", hipps.Code);
                    Console.WriteLine("HIPPS version: {0}", hipps.Version);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void Run()
        {
            //ValidationAndGrouper("B1                    C-072009    02.00                                                                                                                                         20090615                                                                                                                     2014010104                                                                                             296.80   401.9    300.00   715.89                    0001                                                                   00         00       NA              01      01  0100                                   01  01                                                                                                                                                             02                                                                                    0000                                                                                                                         01   0                                                00    00  0                0201  0103                                    NA                                                                                                                                                                                                                                                                                                                                       %");
            ValidationAndGrouper("B1          01        C-072009    02.003646497171.0  7005031                                                               747416               TXN         OHI00013            20101113        1CAROLYN      WAKEFIELD            TX75014      459749306A  04597493060              119451130 214270175400012014010104       010000000000                                                                          780.3302 401.9 03 716.8903 300.0202 272.2 02 298.0 020001                                                                   01         00       NA              03      01  0000                                   01  01                                                                                                                                                   181113356402                                                                                    0000                                                                                                                         03   0                                                00    00  0                0201  010100000     00000                     NA                                                                                                                                                                                                                                                                                                                                       %");
        }
    }
}
