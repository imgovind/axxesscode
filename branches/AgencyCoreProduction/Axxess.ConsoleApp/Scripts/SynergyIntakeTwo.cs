﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;

    using Excel;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;

    using Axxess.Core.Extension;

    public static class SynergyIntakeTwo
    {
        private static ISheet sheet;
        private static HSSFWorkbook workBook;
        private static string input = Path.Combine(App.Root, "Files\\crescent.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\crescent_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string npioutput = Path.Combine(App.Root, string.Format("Files\\crescent_Physician_{0}.xls", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            Initialize();
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                int dataCounter = 1;
                                PatientData patientData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        if (dataCounter == 1)
                                        {
                                            patientData = new PatientData();
                                            patientData.AgencyId = "263740dc-4732-4a41-8fbf-089b8c431e72";
                                            patientData.AgencyLocationId = "756d89d5-1269-4185-9268-27f04604a874";
                                            patientData.Gender = "Male";
                                            if (dataRow.GetValue(1).IsNotNullOrEmpty())
                                            {
                                                var indexOf = dataRow.GetValue(1).IndexOf(" (");
                                                var fullName = dataRow.GetValue(1).Substring(0, indexOf + 1);

                                                var nameArray = fullName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (nameArray != null && nameArray.Length > 1)
                                                {
                                                    patientData.LastName = nameArray[0].Trim();
                                                    patientData.FirstName = nameArray[1].Trim();
                                                }
                                            }
                                            patientData.MiddleInitial = "";

                                            if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                            {
                                                var addressArray = dataRow.GetValue(14).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                                if (addressArray != null)
                                                {
                                                    if (addressArray.Length == 2)
                                                    {
                                                        var twoWordCityArray = addressArray[0].Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries);
                                                        if (twoWordCityArray != null && twoWordCityArray.Length == 2)
                                                        {
                                                            patientData.AddressLine1 = twoWordCityArray[0].Replace("'", "");
                                                            patientData.AddressLine2 = "";
                                                            patientData.AddressCity = twoWordCityArray[1].Replace("'", "");
                                                        }
                                                        else
                                                        {
                                                            var oneWordCityArray = addressArray[0].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (oneWordCityArray != null && oneWordCityArray.Length > 1)
                                                            {
                                                                patientData.AddressCity = oneWordCityArray.Reverse()[0].Replace("'", "");

                                                                oneWordCityArray = oneWordCityArray.Reverse();
                                                                int addressLine1Count = 0;
                                                                var addressLine1Array = new StringBuilder();
                                                                do
                                                                {
                                                                    addressLine1Array.AppendFormat("{0} ", oneWordCityArray[addressLine1Count]);
                                                                    addressLine1Count++;
                                                                } while (addressLine1Count < oneWordCityArray.Length - 1);

                                                                patientData.AddressLine1 = addressLine1Array.ToString().TrimEnd();
                                                                patientData.AddressLine2 = "";
                                                            }
                                                        }
                                                        var locationArray = addressArray[1].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                        if (locationArray != null && locationArray.Length == 2)
                                                        {
                                                            patientData.AddressState = locationArray[0];
                                                            patientData.AddressZipCode = locationArray[1].Substring(0, 5);
                                                        }
                                                    }
                                                }
                                            }

                                            patientData.Phone = dataRow.GetValue(25).ToPhoneDB();
                                            patientData.MedicareNumber = dataRow.GetValue(28);
                                            patientData.BirthDate = "0001-01-01";
                                            dataCounter++;
                                        }
                                        else if (dataCounter == 2)
                                        {
                                            patientData.PatientNumber = dataRow.GetValue(15);
                                            if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                            {
                                                patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(4))).ToString("yyyy-M-d");
                                            }

                                            if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            {
                                                var status = dataRow.GetValue(7);

                                                if (status.IsEqual("30"))
                                                {
                                                    patientData.PatientStatusId = "1";
                                                }
                                                else if (status.IsEqual("1") || status.IsEqual("01"))
                                                {
                                                    patientData.PatientStatusId = "2";
                                                }
                                                else if (status.IsEqual("2") || status.IsEqual("02"))
                                                {
                                                    patientData.IsHosptialized = true;
                                                    patientData.PatientStatusId = "1";
                                                }
                                                else
                                                {
                                                    patientData.PatientStatusId = "3";
                                                }
                                            }

                                            if (dataRow.GetValue(19).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Primary DX: {0}. ", dataRow.GetValue(19));
                                            }

                                            if (dataRow.GetValue(23).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Physician Information: {0}. ", dataRow.GetValue(23));
                                            }

                                            if (dataRow.GetValue(30).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Clinician Information: {0}. ", dataRow.GetValue(7));
                                            }

                                            dataCounter = 1;

                                            patientData.Comments = patientData.Comments.Trim();

                                            textWriter.WriteLine(new PatientScript(patientData).ToString());
                                            textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                            textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());

                                            textWriter.Write(textWriter.NewLine);
                                        }
                                    }
                                }
                                //Write();
                            }
                        }
                    }
                }
            }
        }

        private static void Initialize()
        {
            workBook = new HSSFWorkbook();

            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            workBook.SummaryInformation = si;

            sheet = workBook.CreateSheet("NPI");

            var headerFont = workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            var headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("PatientGUID");
            headerRow.CreateCell(1).SetCellValue("PatientHIC");
            headerRow.CreateCell(2).SetCellValue("PatientLastName");
            headerRow.CreateCell(3).SetCellValue("PatientFirstName");
            headerRow.CreateCell(4).SetCellValue("PhysicianNPI");
            headerRow.CreateCell(5).SetCellValue("Physiciantitle");
            headerRow.CreateCell(6).SetCellValue("Physicianlname");
            headerRow.CreateCell(7).SetCellValue("Physicianfname");
            headerRow.CreateCell(8).SetCellValue("Physicianname");
            headerRow.CreateCell(9).SetCellValue("Physicianaddr");
            headerRow.CreateCell(10).SetCellValue("Physiciancity");
            headerRow.CreateCell(11).SetCellValue("Physicianstate");
            headerRow.CreateCell(12).SetCellValue("Physicianzip");
            headerRow.CreateCell(13).SetCellValue("Physicianphone");
            headerRow.CreateCell(14).SetCellValue("Physicianfax");
            headerRow.RowStyle = headerStyle;
        }

        private static void Write()
        {
            int columnCounter = 0;
            do
            {
                sheet.AutoSizeColumn(columnCounter);
                columnCounter++;
            }
            while (columnCounter < 15);

            using (FileStream fileStream = new FileStream(npioutput, FileMode.OpenOrCreate, FileAccess.Write))
            {
                workBook.Write(fileStream);
            }
        }
    }
}
