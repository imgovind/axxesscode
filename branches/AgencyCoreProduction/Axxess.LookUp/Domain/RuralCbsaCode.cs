﻿namespace Axxess.LookUp.Domain
{
    using System.Text;

    public class RuralCbsaCode : ICbsaCode
    {
        public int Id { get; set; }
        public string CBSA { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public double WITwoSeven { get; set; }
        public double WITwoEight { get; set; }
        public double WITwoNine { get; set; }
        public double WITwoTen { get; set; }
        public double WITwoEleven { get; set; }
        public double WITwoTwelve { get; set; }
        public double WITwoThirteen { get; set; }
        public double WITwoFourteen { get; set; }

        public override string ToString()
        {
            return new StringBuilder()
                .AppendFormat("Id: {0} ", Id)
                .AppendFormat("CBSA: {0} ", CBSA)
                .AppendFormat("StateCode: {0} ", StateCode)
                .AppendFormat("StateName: {0} ", StateName)
                .AppendFormat("2011: {0} ", WITwoEleven)
                .AppendFormat("2012: {0} ", WITwoTwelve)
                .AppendFormat("2013: {0}", WITwoThirteen)
                .ToString();
        }
    }
}
