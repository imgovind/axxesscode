﻿namespace Axxess.LookUp.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;

    [Serializable]
    public class DisciplineTask
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public int DisciplineId { get; set; }
        public bool IsBillable { get; set; }
        public bool IsMultiple { get; set; }
        public string RevenueCode { get; set; }
        public string GCode { get; set; }
        public int Unit { get; set; }
        public double Rate { get; set; }

        public int Version { get; set; }
        public int DefaultStatus { get; set; }
        public int Table { get; set; }
        public bool NeedOrderNumber { get; set; }
        public bool NeedPhysician { get; set; }


        [SubSonicIgnore]
        public string TableName { get; set; }
    }
}
