﻿namespace Axxess.LookUp.Domain
{
    public class MedicareProvider
    {
        public int Id { get; set; }
        public string ProviderNumber { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
    }
}
