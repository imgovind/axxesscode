﻿namespace Axxess.Membership.Domain
{
    using System;
    using System.Collections.Generic;

    public class Setting
    {
        public Guid Id { get; set; }
        public Guid ApplicationId { get; set; }
        public string Name { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public Dictionary<byte, string> Data { get; set; }
    }
}
