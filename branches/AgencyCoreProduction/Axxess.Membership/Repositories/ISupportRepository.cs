﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    public interface ISupportRepository
    {
        bool AddImpersonationLink(ImpersonationLink link);
        ImpersonationLink GetImpersonationLink(Guid linkId,Guid agencyId);
        bool UpdateImpersonationLink(ImpersonationLink link);
    }
}
