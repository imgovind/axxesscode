﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class LoginRepository : ILoginRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public LoginRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region ILoginRepository Members

        public bool Add(Login login)
        {
            if (login != null)
            {
                login.Id = Guid.NewGuid();
                login.Created = DateTime.Now;
                database.Add<Login>(login);
                return true;
            }
            return false;
        }

        public Login Find(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            return database.Single<Login>(l => l.Id == id);
        }

        public Login Find(string emailAddress)
        {
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            return database.Single<Login>(l => l.EmailAddress == emailAddress.Trim());
        }

        public bool Update(Login login)
        {
            if (login != null)
            {
                database.Update<Login>(login);
                return true;
            }
            return false;
        }

        public bool Delete(Guid loginId)
        {
            Check.Argument.IsNotEmpty(loginId, "loginId");

            bool result = false;
            var login = database.Single<Login>(l => l.Id == loginId);

            if (login != null)
            {
                login.IsActive = false;
                database.Update<Login>(login);
                result = true;
            }
            return result;
        }

        public ICollection<Login> GetAll()
        {
            return database.All<Login>().ToList().AsReadOnly();
        }

        public IList<Login> GetAllByRole(Roles role)
        {
            return database.Find<Login>(l => l.Role == role.ToString() && l.IsActive == true).ToList();
        }

        public ICollection<Login> AutoComplete(string searchString)
        {
            return database.Find<Login>(u => u.EmailAddress.StartsWith(searchString)).ToList();
        }

        public string GetLoginDisplayName(Guid loginId)
        {
            var name = string.Empty;

            if (!loginId.IsEmpty())
            {
                var login = Find(loginId);
                if (login != null)
                {
                    name = login.DisplayName;
                }
            }

            return name;
        }

        public string GetLoginEmailAddress(Guid loginId)
        {
            var name = string.Empty;

            if (!loginId.IsEmpty())
            {
                var login = Find(loginId);
                if (login != null)
                {
                    name = login.EmailAddress;
                }
            }

            return name;
        }

        public bool IsSignatureCorrect(Guid loginId, string signature)
        {
            if (signature.IsNotNullOrEmpty())
            {
                var login = database.Single<Login>(l => l.Id == loginId); 
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(signature, login.SignatureHash, login.SignatureSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsPasswordCorrect(Guid loginId, string password)
        {
            if (password.IsNotNullOrEmpty())
            {
                var login = database.Single<Login>(l => l.Id == loginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(password, login.PasswordHash, login.PasswordSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        public List<Login> GetLoginsForEmail(List<Guid> Ids)
        {
            var list = new List<Login>();

            var script = string.Format(@"SELECT 
                                Id as Id ,
                                EmailAddress as EmailAddress ,
                                IsActive as  IsActive 
                                    FROM 
                                        logins 
                                            WHERE 
                                                Id IN ({0}) ", Ids.Select(Id => string.Format("'{0}'", Id)).ToArray().Join(", "));
            using (var cmd = new FluentCommand<Login>(script))
            {
                list = cmd.SetConnection("AxxessMembershipConnectionString")
                .SetMap(reader => new Login
                {
                    Id = reader.GetGuid("Id"),
                    EmailAddress = reader.GetStringNullable("EmailAddress"),
                    IsActive = reader.GetBoolean("IsActive")
                }).AsList();
            }
            return list;
        }

        #endregion

        #region DDE Credentials Members

        public DdeCredential GetDdeCredential(Guid id)
        {
            return database.Single<DdeCredential>(d => d.Id == id);
        }

        public IList<DdeCredential> GetDdeCredentials()
        {
            return database.All<DdeCredential>().ToList();
        }

        public bool UpdateDdeCredential(DdeCredential ddeCredential)
        {
            if (ddeCredential != null)
            {
                database.Update<DdeCredential>(ddeCredential);
                return true;
            }
            return false;
        }

        #endregion
    }
}
