﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using SubSonic.Repository;

    public class ApplicationRepository : IApplicationRepository
    {
        #region Constructor

        //private ICoreDataContext db;

        //public ApplicationRepository(ICoreDataContext dataContext)
        //{
        //    Check.Argument.IsNotNull(dataContext, "dataContext");
        //    this.db = dataContext;
        //}

        #endregion

        #region IApplicationRepository Members

        //public Guid Add(string name)
        //{
        //    Check.Argument.IsNotEmpty(name, "name");

        //    Application newApp = new Application()
        //    {
        //        Id = Guid.NewGuid(),
        //        Name = name,
        //        Created = DateTime.Now,
        //        Modified = DateTime.Now
        //    };
        //    db.Insert<Application>(newApp);
        //    db.SubmitChanges();
        //    return newApp.Id;
        //}
        
        //public bool Update(Guid id, string name)
        //{
        //    Check.Argument.IsNotEmpty(id, "id");
        //    Check.Argument.IsNotEmpty(name, "name");

        //    bool result = false;

        //    Application app = db.ApplicationsDataSource.SingleOrDefault(a => a.Id == id);

        //    if (app != null)
        //    {
        //        app.Name = name;
        //        app.Modified = DateTime.Now;
        //        db.SubmitChanges();
        //        result = true;
        //    }

        //    return result;
        //}

        //public bool Delete(Guid applicationId)
        //{
        //    Check.Argument.IsNotEmpty(applicationId, "applicationId");

        //    bool result = false;
        //    Application app = db.ApplicationsDataSource.SingleOrDefault(a => a.Id == applicationId);
        //    if (app != null)
        //    {
        //        db.DeleteAll(db.UserApplicationsDataSource.Where(ua => ua.ApplicationId == app.Id));
        //        db.DeleteAll(db.SettingsDataSource.Where(s => s.ApplicationId == app.Id));
        //        db.DeleteAll(db.AuditsDataSource.Where(a => a.ApplicationId == app.Id));
        //        db.DeleteAll(db.ErrorsDataSource.Where(e => e.Host.ApplicationId == app.Id));
        //        db.DeleteAll(db.HostsDataSource.Where(h => h.ApplicationId == app.Id));
        //        db.Delete(app);
        //        db.SubmitChanges();
        //        result = true;
        //    }
        //    return result;
        //}

        //public IApplication Find(Guid id)
        //{
        //    Check.Argument.IsNotEmpty(id, "id");

        //    return db.ApplicationsDataSource
        //        .SingleOrDefault(a => a.Id == id);
        //}

        //public IApplication Find(string applicationName)
        //{
        //    Check.Argument.IsNotNull(applicationName, "applicationName");

        //    return db.ApplicationsDataSource
        //        .SingleOrDefault(a => a.Name == applicationName);
        //}

        //public ICollection<IApplication> GetApplicationEntryOnTimeInterval(DateTime startDate, DateTime endDate)
        //{
        //    Check.Argument.IsNotInvalidDate(startDate, "startDate");
        //    Check.Argument.IsNotInvalidDate(endDate, "endDate");

        //    return db.ApplicationsDataSource
        //        .Where(a => (a.Created >= startDate && a.Created <= endDate))
        //        .OrderBy(a => a.Created)
        //        .Cast<IApplication>()
        //        .ToList();
        //}

        //public ICollection<IApplication> GetAppliactionsModifiedInTimeInterval(DateTime startDate, DateTime endDate)
        //{
        //    Check.Argument.IsNotInvalidDate(startDate, "startDate");
        //    Check.Argument.IsNotInvalidDate(endDate, "endDate");
        //    return db.ApplicationsDataSource
        //        .Where(a => (a.Created >= startDate && a.Created <= endDate))
        //        .OrderBy(a => a.Created)
        //        .Cast<IApplication>()
        //        .ToList();
        //}

        //public ICollection<IApplication> GetAll()
        //{
        //    return db.ApplicationsDataSource
        //        .Cast<IApplication>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        //public ICollection<IApplication> GetUserApps(Guid userId)
        //{
        //    Check.Argument.IsNotEmpty(userId, "userId");

        //    var query = from app in db.ApplicationsDataSource
        //                join userApps in db.UserApplicationsDataSource
        //                on app.Id equals userApps.ApplicationId
        //                where userApps.UserId == userId
        //                select app;

        //    return query.Cast<IApplication>()
        //        .ToList()
        //        .AsReadOnly();
        //}

        #endregion
    }
}
