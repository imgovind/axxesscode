﻿namespace Axxess.Membership.Repositories
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core.Infrastructure;

    public interface ILoginRepository
    {
        Login Find(Guid id);
        bool Add(Login login);
        bool Delete(Guid loginId);
        ICollection<Login> GetAll();
        Login Find(string emailAddress);
        ICollection<Login> AutoComplete(string searchString);
        bool Update(Login login);
        bool IsSignatureCorrect(Guid loginId, string signature);
        bool IsPasswordCorrect(Guid loginId, string password);
        string GetLoginDisplayName(Guid loginId);
        string GetLoginEmailAddress(Guid loginId);
        List<Login> GetLoginsForEmail(List<Guid> Ids);

        IList<Login> GetAllByRole(Roles role);

        DdeCredential GetDdeCredential(Guid id);
        IList<DdeCredential> GetDdeCredentials();
        bool UpdateDdeCredential(DdeCredential ddeCredential);
    }
}
