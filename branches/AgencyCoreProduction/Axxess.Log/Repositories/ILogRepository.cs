﻿
namespace Axxess.Log.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Log.Domain;

    public interface ILogRepository
    {
        bool AddTaskAudit(TaskAudit audit);
        bool UpdateTaskAudit(TaskAudit audit);
        TaskAudit GetTaskAudit(Guid agencyId, Guid patientId, Guid entityId, int disciplineTaskId);

        bool AddGeneralAudit(AppAudit appAudit);
        bool UpdateGeneralAudit(AppAudit appAudit);
        AppAudit GetGeneralAudit(Guid agencyId, string logDomain, Guid domainId, string entityId, int Id);
        List<AppAudit> GetGeneralAudits(Guid agencyId, string logDomain, string logType, Guid domainId, string entityId);
        List<AppAudit> GetMedicationAudits(Guid agencyId, string logDomain, Guid domainId);
        List<TaskAudit> GetTaskAudits(Guid agencyId, string entityIds);
        bool UpdateOrAddTaskAudits(Guid agencyId, string script);
    }
}
