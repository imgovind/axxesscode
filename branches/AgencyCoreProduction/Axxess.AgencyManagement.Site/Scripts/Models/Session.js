﻿var Session = {
    Init: function() {

        $.blockUI.defaults.css = {};

        $('#EndSession').click(function() {
            $.idleTimeout.options.onTimeout.call(this);
        });

        $('#ContinueSession').click(function() {
            $.unblockUI();
            return false;
        });

        // cache a reference to the countdown element so we don't have to query the DOM for it each second.
        var $countdown = $("#dialog-countdown");

        // start the idle timer plugin
        $.idleTimeout('#SessionDialog', 'div#SessionDialog button:first', {
            idleAfter: 300,
            pollingInterval: 30,
            keepAliveURL: '/Ping',
            failedRequests: 2,
            serverResponseEquals: 'OK',
            onTimeout: function() {
                window.location = "/Logout";
            },
            onIdle: function() {
                //$.blockUI.defaults.css = {};
                //$.blockUI({ message: $('#SessionDialog') });
                U.showDialog("#SessionDialog");
            },
            onCountdown: function(counter) {
                $countdown.html(counter); // update the counter
            },
            onResume: function() {
                // the dialog closes.  nothing else needs to be done
            }
        });
    }
}