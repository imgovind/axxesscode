﻿var SOC = {
    _SOCId: "",
    _EpisodeId: "",
    _patientId: "",

    GetId: function() {
        return SOC._patientId;
    },
    SetId: function(patientId) {
        SOC._patientId = patientId;
    },
    GetSOCId: function() {
        return SOC._SOCId;
    },
    SetSOCId: function(SOCId) {
        SOC._SOCId = SOCId;
    },
    GetSOCEpisodeId: function() {
        return SOC._EpisodeId;
    },
    SetSOCEpisodeId: function(EpisodeId) {
        SOC._EpisodeId = EpisodeId;
    },
    Init: function() {
        $("input[name=StartOfCare_M1000InpatientFacilitiesNone][type=checkbox]").click(function() {

            if ($(this).is(':checked')) {
                $("#soc_M1005").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });

                $("#soc_M1012").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#soc_M1010").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#soc_M1005").find(':input').each(function() { $(this).val(''); });
                $('input[name=StartOfCare_M1005InpatientDischargeDateUnknown]').attr('checked', false);
                $("#soc_M1010").find(':input').each(function() { $(this).val(''); });
                $("#soc_M1012").find(':input').each(function() { $(this).val(''); });


            }
            else {
                $("#soc_M1005").unblock();
                $("#soc_M1010").unblock();
                $("#soc_M1012").unblock();
            }
        });

        $("input[name=StartOfCare_M1300PressureUlcerAssessment]").click(function() {
            if ($(this).val() == "00") {

                $("#soc_M1302").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=StartOfCare_M1302RiskDevelopingPressureUlcers ]").attr('checked', false);

            }
            else {
                $("#soc_M1302").unblock();
            }
        });

        $("input[name=StartOfCare_M1306UnhealedPressureUlcers]").click(function() {
            if ($(this).val() == 0) {



                $("#soc_M1308").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#soc_M13010_12_14").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#soc_M1320").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#StartOfCare_M1307NonEpithelializedStageTwoUlcerDate").val('');
                $("#soc_M1308").find(':input').each(function() { $(this).val(''); });
                $("#soc_M13010_12_14").find(':input').each(function() { $(this).val(''); });
                $("input[name=StartOfCare_M1320MostProblematicPressureUlcerStatus ]").attr('checked', false);
            }
            else if ($(this).val() == 1) {
                $("#soc_M1308").unblock();
                $("#soc_M13010_12_14").unblock();
                $("#soc_M1320").unblock();
            }
        });

        $("input[name=StartOfCare_M1330StasisUlcer]").click(function() {
            if ($(this).val() == "00") {
                $("#soc_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=StartOfCare_M1332CurrentNumberStasisUlcer ]").attr('checked', false);
                $("input[name=StartOfCare_M1334StasisUlcerStatus ]").attr('checked', false);
            }
            else {
                $("#soc_M1332AndM1334").unblock();
            }
        });


        $("input[name=StartOfCare_M1340SurgicalWound]").click(function() {
            if ($(this).val() == "00") {
                $("#soc_M1342").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });

                $("input[name=StartOfCare_M1342SurgicalWoundStatus]").attr('checked', false);

            }
            else {
                $("#soc_M1342").unblock();
            }
        });

        $("input[name=StartOfCare_M1610UrinaryIncontinence]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "02") {
                $("#soc_M1615").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("input[name=StartOfCare_M1615UrinaryIncontinenceOccur]").attr('checked', false);
            }
            else {
                $("#soc_M1615").unblock();
            }
        });


        $("input[name=StartOfCare_M2000DrugRegimenReview]").click(function() {
            if ($(this).val() == "00" || $(this).val() == "01") {

                $("#soc_M2002").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=StartOfCare_M2002MedicationFollowup ]").attr('checked', false);
                $("#soc_M2010").unblock();
                $("#soc_M2020").unblock();
                $("#soc_M2030").unblock();
            }
            else if ($(this).val() == "NA") {

                $("#soc_M2002").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#soc_M2010").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#soc_M2020").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#soc_M2030").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("input[name=StartOfCare_M2002MedicationFollowup ]").attr('checked', false);
                $("input[name=StartOfCare_M2010PatientOrCaregiverHighRiskDrugEducation ]").attr('checked', false);
                $("input[name=StartOfCare_M2020ManagementOfOralMedications ]").attr('checked', false);
                $("input[name=StartOfCare_M2030ManagementOfInjectableMedications ]").attr('checked', false);

            }
            else {
                $("#soc_M2002").unblock();
                $("#soc_M2010").unblock();
                $("#soc_M2020").unblock();
                $("#soc_M2030").unblock();
            }
        });
        Oasis.BradenScaleOnchange('StartOfCare', '#socBradenScale');

        $("input[name=StartOfCare_GenericNutritionalHealth]").click(function() {
            Oasis.CalculateNutritionScore('StartOfCare');
        });

    },
    HandlerHelper: function(form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {

                    var actionType = control.val();
                    if (actionType == "Save/Continue") {

                        Oasis.NextTab("#socTabs.tabs");
                    }
                    else if (actionType == "Save/Exit") {
                        Oasis.Close(control);
                        Oasis.RebindActivity();
                    }
                    else if (actionType == "Save") {

                    }

                }
                else {
                    alert(resultObject.errorMessage);
                }
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }

    ,
    FormSubmit: function(control) {

        var form = control.closest("form");
        form.validate();
        SOC.HandlerHelper(form, control);
    },
    Validate: function(id) {
        OasisValidation.Validate(id, "StartOfCare");
    }
    ,
    NewStartOfCare: function() {
        var id = Oasis.GetId();
        var data = 'id=' + id;
        $("#soc").clearForm();
        $("#soc div").unblock();
        Oasis.BlockAssessmentType();
        $.ajax({
            url: '/Patient/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var patient = eval(result);
                var patientName = (patient.FirstName !== null ? patient.FirstName : "") + " " + (patient.LastName != null ? patient.LastName : "");
                $("#StartOfCareTitle").text("New Start Of Care - " + patientName);
                $("input[name='StartOfCare_Id']").val("");
                $("input[name='StartOfCare_PatientGuid']").val(id);
                $("input[name='StartOfCare_Action']").val('New');
                $("#StartOfCare_M0020PatientIdNumber").val(patient.PatientIdNumber);
                $("#StartOfCare_M0030SocDate").val(patient.StartOfCareDateFormatted);
                $("#StartOfCare_M0040FirstName").val(patient.FirstName);
                $("#StartOfCare_M0040MI").val(patient.MiddleInitial);
                $("#StartOfCare_M0040LastName").val(patient.LastName);
                $("#StartOfCare_M0050PatientState").val(patient.AddressStateCode);
                $("#StartOfCare_M0060PatientZipCode").val(patient.AddressZipCode);
                $("#StartOfCare_M0063PatientMedicareNumber").val(patient.MedicareNumber);
                $("#StartOfCare_M0064PatientSSN").val(patient.SSN);
                $("#StartOfCare_M0065PatientMedicaidNumber").val(patient.MedicaidNumber);
                $("#StartOfCare_M0066PatientDoB").val(patient.DOBFormatted);
                $('input[name=StartOfCare_M0069Gender][value=' + patient.Gender.toString() + ']').attr('checked', true);
                $("input[name='StartOfCare_M0100AssessmentType'][value='01']").attr('checked', true);
                if (patient.EthnicRace !== null) {
                    var EthnicRaceArray = (patient.EthnicRace).split(';');
                    var i = 0;
                    for (i = 0; i < EthnicRaceArray.length; i++) {

                        if (EthnicRaceArray[i] == 1) {
                            $('input[name=StartOfCare_M0140RaceAMorAN][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 2) {
                            $('input[name=StartOfCare_M0140RaceAsia][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 3) {
                            $('input[name=StartOfCare_M0140RaceBalck][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 4) {
                            $('input[name=StartOfCare_M0140RaceHispanicOrLatino][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 5) {
                            $('input[name=StartOfCare_M0140RaceNHOrPI][value=1]').attr('checked', true);
                        }
                        if (EthnicRaceArray[i] == 6) {
                            $('input[name=StartOfCare_M0140RaceWhite][value=1]').attr('checked', true);
                        }
                    }
                }
                if (patient.PaymentSource !== null) {
                    var PaymentSourceArray = (patient.PaymentSource).split(';');
                    var i = 0;
                    for (i = 0; i < PaymentSourceArray.length; i++) {
                        if (PaymentSourceArray[i] == 0) {
                            $('input[name=StartOfCare_M0150PaymentSourceNone][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 1) {
                            $('input[name=StartOfCare_M0150PaymentSourceMCREFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 2) {
                            $('input[name=StartOfCare_M0150PaymentSourceMCREHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 3) {
                            $('input[name=StartOfCare_M0150PaymentSourceMCAIDFFS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 4) {
                            $('input[name=StartOfCare_M0150PaymentSourceMACIDHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 5) {
                            $('input[name=StartOfCare_M0150PaymentSourceWRKCOMP][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 6) {
                            $('input[name=StartOfCare_M0150PaymentSourceTITLPRO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 7) {
                            $('input[name=StartOfCare_M0150PaymentSourceOTHGOVT][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 8) {
                            $('input[name=StartOfCare_M0150PaymentSourcePRVINS][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 9) {
                            $('input[name=StartOfCare_M0150PaymentSourcePRVHMO][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 10) {
                            $('input[name=StartOfCare_M0150PaymentSourceSelfPay][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 11) {
                            $('input[name=StartOfCare_M0150PaymentSourceUnknown][value=1]').attr('checked', true);
                        }
                        if (PaymentSourceArray[i] == 12) {
                            $('input[name=StartOfCare_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                            $("#StartOfCare_M0150PaymentSourceOther").val(patient.OtherPaymentSource);
                        }
                    }
                }
                Oasis.ClearRows($("#suppliesTable"));
                Oasis.addTableRow('#suppliesTable');
            }
        });
    }
    ,
    loadSoc: function(id, patientId, assessmentType) {
        $('#socResult').load('Oasis/Soc', { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#socResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#soc');
            }
            else if (textStatus == "success") {

                JQD.open_window('#soc');
                $("#socTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
                $("#socTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');

                $("#socTabs.tabs").bind("tabsselect", { Id: id, PatientId: patientId, AssessmentType: assessmentType }, function(event, ui) {
                    SOC.loadSOCParts(event, ui);
                });
                SOC.Init();
                Oasis.Init();

            }
        }
);
    }
    ,
    loadSOCParts: function(event, ui) {

        $($(ui.tab).attr('href')).load('Oasis/SocCategory', { Id: event.data.Id, PatientId: event.data.PatientId, AssessmentType: event.data.AssessmentType, Category: $(ui.tab).attr('href') }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $($(ui.tab).attr('href')).html('<p>There was an error making the AJAX request</p>');

            }
            else if (textStatus == "success") {
                SOC.Init();
                Oasis.Init();
            }
        }
);
    }
    ,
    EditStartOfCare: function(id, patientId, assessmentType) {
        $("#soc").clearForm();
        Oasis.BlockAssessmentType();
        var data = 'Id=' + id + "&assessmentType=" + assessmentType;
        $.ajax({
            url: '/Oasis/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                fillStartOfCare(result);
            }
        });
        fillStartOfCare = function(result) {
            var firstName = result["M0040FirstName"] != null && result["M0040FirstName"] != undefined ? result["M0040FirstName"].Answer : "";
            var lastName = result["M0040LastName"] != null && result["M0040LastName"] != undefined ? result["M0040LastName"].Answer : "";
            $("#StartOfCareTitle").text("Edit Start Of Care - " + firstName + " " + lastName);
            $("input[name='StartOfCare_Id']").val(id);
            SOC.SetSOCId(id);
            SOC.SetId(patientId);
            $("input[name='StartOfCare_Action']").val('Edit');
            $("input[name='StartOfCare_PatientGuid']").val(patientId);
            $("#StartOfCare_M0010CertificationNumber").val(result["M0010CertificationNumber"] != null && result["M0010CertificationNumber"] != undefined ? result["M0010CertificationNumber"].Answer : "");
            $("#StartOfCare_M0014BranchState").val(result["M0014BranchState"] != null && result["M0014BranchState"] != undefined ? result["M0014BranchState"].Answer : "");
            $("#StartOfCare_M0016BranchId").val(result["M0016BranchId"] != null && result["M0016BranchId"] != undefined ? result["M0016BranchId"].Answer : "");
            var nationalProviderIdUK = result["M0018NationalProviderIdUnknown"];
            if (nationalProviderIdUK != null && nationalProviderIdUK != undefined) {
                if (nationalProviderIdUK.Answer == 1) {

                    $('input[name=StartOfCare_M0018NationalProviderIdUnknown][value=1]').attr('checked', true);
                    $("#StartOfCare_M0018NationalProviderId").val(" ");
                }
                else {
                    $('input[name=StartOfCare_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                    $("#StartOfCare_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
                }
            }
            else {
                $('input[name=StartOfCare_M0018NationalProviderIdUnknown][value=1]').attr('checked', false);
                $("#StartOfCare_M0018NationalProviderId").val(result["M0018NationalProviderId"] != null && result["M0018NationalProviderId"] != undefined ? result["M0018NationalProviderId"].Answer : "");
            }
            $("#StartOfCare_M0020PatientIdNumber").val(result["M0020PatientIdNumber"] != null && result["M0020PatientIdNumber"] != undefined ? result["M0020PatientIdNumber"].Answer : "");
            $("#StartOfCare_M0030SocDate").val(result["M0030SocDate"] != null && result["M0030SocDate"] != undefined ? result["M0030SocDate"].Answer : "");
            var rOCDateNotApplicable = result["M0032ROCDateNotApplicable"];
            if (rOCDateNotApplicable != null && rOCDateNotApplicable != undefined) {
                if (rOCDateNotApplicable.Answer == 1) {

                    $('input[name=StartOfCare_M0032ROCDateNotApplicable][value=1]').attr('checked', true);
                    $("#StartOfCare_M0032ROCDate").val(" ");
                }
                else {
                    $('input[name=StartOfCare_M0032ROCDateNotApplicable][value=1]').attr('checked', false);
                    $("#StartOfCare_M0032ROCDate").val(result["M0032ROCDate"] != null && result["M0032ROCDate"] != undefined ? result["M0032ROCDate"].Answer : "");
                }
            }
            $("#StartOfCare_M0040FirstName").val(firstName);
            $("#StartOfCare_M0040MI").val(result["M0040MI"] != null && result["M0040MI"] != undefined ? result["M0040MI"].Answer : "");
            $("#StartOfCare_M0040LastName").val(lastName);
            $("#StartOfCare_M0050PatientState").val(result["M0050PatientState"] != null && result["M0050PatientState"] != undefined ? result["M0050PatientState"].Answer : "");
            $("#StartOfCare_M0060PatientZipCode").val(result["M0060PatientZipCode"] != null && result["M0060PatientZipCode"] != undefined ? result["M0060PatientZipCode"].Answer : "");
            $("#StartOfCare_GenericEpisodeStartDate").val(result["GenericEpisodeStartDate"] != null && result["GenericEpisodeStartDate"] != undefined ? result["GenericEpisodeStartDate"].Answer : "");

            var patientMedicareNumberUK = result["M0063PatientMedicareNumberUnknown"];
            if (patientMedicareNumberUK != null && patientMedicareNumberUK != undefined) {
                if (patientMedicareNumberUK.Answer == 1) {

                    $('input[name=StartOfCare_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', true);
                    $("#StartOfCare_M0063PatientMedicareNumber").val(" ");
                }
                else {
                    $('input[name=StartOfCare_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                    $("#StartOfCare_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
                }
            }
            else {
                $('input[name=StartOfCare_M0063PatientMedicareNumberUnknown][value=1]').attr('checked', false);
                $("#StartOfCare_M0063PatientMedicareNumber").val(result["M0063PatientMedicareNumber"] != null && result["M0063PatientMedicareNumber"] != undefined ? result["M0063PatientMedicareNumber"].Answer : "");
            }
            var patientSSNUK = result["M0064PatientSSNUnknown"];
            if (patientSSNUK != null && patientSSNUK != undefined) {
                if (patientSSNUK.Answer == 1) {

                    $('input[name=StartOfCare_M0064PatientSSNUnknown][value=1]').attr('checked', true);
                    $("#StartOfCare_M0064PatientSSN").val(" ");
                }
                else {
                    $('input[name=StartOfCare_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                    $("#StartOfCare_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
                }
            }
            else {
                $('input[name=StartOfCare_M0064PatientSSNUnknown][value=1]').attr('checked', false);
                $("#StartOfCare_M0064PatientSSN").val(result["M0064PatientSSN"] != null && result["M0064PatientSSN"] != undefined ? result["M0064PatientSSN"].Answer : "");
            }
            var patientMedicaidNumberUK = result["M0065PatientMedicaidNumberUnknown"];
            if (patientMedicaidNumberUK != null && patientMedicaidNumberUK != undefined) {
                if (patientMedicaidNumberUK.Answer == 1) {

                    $('input[name=StartOfCare_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', true);
                    $("#StartOfCare_M0065PatientMedicaidNumber").val(" ");
                }
                else {
                    $('input[name=StartOfCare_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                    $("#StartOfCare_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
                }
            }
            else {
                $('input[name=StartOfCare_M0065PatientMedicaidNumberUnknown][value=1]').attr('checked', false);
                $("#StartOfCare_M0065PatientMedicaidNumber").val(result["M0065PatientMedicaidNumber"] != null && result["M0065PatientMedicaidNumber"] != undefined ? result["M0065PatientMedicaidNumber"].Answer : "");
            }
            $("#StartOfCare_M0066PatientDoB").val(result["M0066PatientDoB"] != null && result["M0066PatientDoB"] != undefined ? result["M0066PatientDoB"].Answer : "");
            $('input[name=StartOfCare_M0069Gender][value=' + (result["M0069Gender"] != null && result["M0069Gender"] != undefined ? result["M0069Gender"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M0080DisciplinePerson][value=' + (result["M0080DisciplinePerson"] != null && result["M0080DisciplinePerson"] != undefined ? result["M0080DisciplinePerson"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_M0090AssessmentCompleted").val(result["M0090AssessmentCompleted"] != null && result["M0090AssessmentCompleted"] != undefined ? result["M0090AssessmentCompleted"].Answer : "");
            $("input[name='StartOfCare_M0100AssessmentType'][value='01']").attr('checked', true);
            var physicianOrderedDateNotApplicable = result["M0102PhysicianOrderedDateNotApplicable"];
            if (physicianOrderedDateNotApplicable != null && physicianOrderedDateNotApplicable != undefined) {
                if (physicianOrderedDateNotApplicable.Answer == 1) {

                    $('input[name=StartOfCare_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', true);
                    $("#StartOfCare_M0102PhysicianOrderedDate").val(" ");
                }
                else {
                    $('input[name=StartOfCare_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                    $("#StartOfCare_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
                }
            }
            else {
                $('input[name=StartOfCare_M0102PhysicianOrderedDateNotApplicable][value=1]').attr('checked', false);
                $("#StartOfCare_M0102PhysicianOrderedDate").val(result["M0102PhysicianOrderedDate"] != null && result["M0102PhysicianOrderedDate"] != undefined ? result["M0102PhysicianOrderedDate"].Answer : "");
            }

            $("#StartOfCare_M0104ReferralDate").val(result["M0104ReferralDate"] != null && result["M0104ReferralDate"] != undefined ? result["M0104ReferralDate"].Answer : "");

            $('input[name=StartOfCare_M0110EpisodeTiming][value=' + (result["M0110EpisodeTiming"] != null && result["M0110EpisodeTiming"] != undefined ? result["M0110EpisodeTiming"].Answer : "") + ']').attr('checked', true);


            $('input[name=StartOfCare_M0140RaceAMorAN][value=' + (result["M0140RaceAMorAN"] != null && result["M0140RaceAMorAN"] != undefined ? result["M0140RaceAMorAN"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0140RaceAsia][value=' + (result["M0140RaceAsia"] != null && result["M0140RaceAsia"] != undefined ? result["M0140RaceAsia"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0140RaceBalck][value=' + (result["M0140RaceBalck"] != null && result["M0140RaceBalck"] != undefined ? result["M0140RaceBalck"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0140RaceHispanicOrLatino][value=' + (result["M0140RaceHispanicOrLatino"] != null && result["M0140RaceHispanicOrLatino"] != undefined ? result["M0140RaceHispanicOrLatino"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0140RaceNHOrPI][value=' + (result["M0140RaceNHOrPI"] != null && result["M0140RaceNHOrPI"] != undefined ? result["M0140RaceNHOrPI"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0140RaceWhite][value=' + (result["M0140RaceWhite"] != null && result["M0140RaceWhite"] != undefined ? result["M0140RaceWhite"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourceNone][value=' + (result["M0150PaymentSourceNone"] != null && result["M0150PaymentSourceNone"] != undefined ? result["M0150PaymentSourceNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourceMCREFFS][value=' + (result["M0150PaymentSourceMCREFFS"] != null && result["M0150PaymentSourceMCREFFS"] != undefined ? result["M0150PaymentSourceMCREFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourceMCREHMO][value=' + (result["M0150PaymentSourceMCREHMO"] != null && result["M0150PaymentSourceMCREHMO"] != undefined ? result["M0150PaymentSourceMCREHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourceMCAIDFFS][value=' + (result["M0150PaymentSourceMCAIDFFS"] != null && result["M0150PaymentSourceMCAIDFFS"] != undefined ? result["M0150PaymentSourceMCAIDFFS"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourceMACIDHMO][value=' + (result["M0150PaymentSourceMACIDHMO"] != null && result["M0150PaymentSourceMACIDHMO"] != undefined ? result["M0150PaymentSourceMACIDHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourceWRKCOMP][value=' + (result["M0150PaymentSourceWRKCOMP"] != null && result["M0150PaymentSourceWRKCOMP"] != undefined ? result["M0150PaymentSourceWRKCOMP"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourceTITLPRO][value=' + (result["M0150PaymentSourceTITLPRO"] != null && result["M0150PaymentSourceTITLPRO"] != undefined ? result["M0150PaymentSourceTITLPRO"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourceOTHGOVT][value=' + (result["M0150PaymentSourceOTHGOVT"] != null && result["M0150PaymentSourceOTHGOVT"] != undefined ? result["M0150PaymentSourceOTHGOVT"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourcePRVINS][value=' + (result["M0150PaymentSourcePRVINS"] != null && result["M0150PaymentSourcePRVINS"] != undefined ? result["M0150PaymentSourcePRVINS"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourcePRVHMO][value=' + (result["M0150PaymentSourcePRVHMO"] != null && result["M0150PaymentSourcePRVHMO"] != undefined ? result["M0150PaymentSourcePRVHMO"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M0150PaymentSourceSelfPay][value=' + (result["M0150PaymentSourceSelfPay"] != null && result["M0150PaymentSourceSelfPay"] != undefined ? result["M0150PaymentSourceSelfPay"].Answer : "") + ']').attr('checked', true);

            var paymentSourceOtherSRS = result["M0150PaymentSourceOtherSRS"];
            if (paymentSourceOtherSRS != null && paymentSourceOtherSRS != undefined) {
                if (paymentSourceOtherSRS.Answer == 1) {

                    $('input[name=StartOfCare_M0150PaymentSourceOtherSRS][value=1]').attr('checked', true);
                    $("#StartOfCare_M0150PaymentSourceOther").val(result["M0150PaymentSourceOther"] != null && result["M0150PaymentSourceOther"] != undefined ? result["M0150PaymentSourceOther"].Answer : "");

                }
                else {
                    $('input[name=StartOfCare_M0150PaymentSourceOtherSRS][value=1]').attr('checked', false);

                }
            }

            $('input[name=StartOfCare_M0150PaymentSourceUnknown][value=' + (result["M0150PaymentSourceUnknown"] != null && result["M0150PaymentSourceUnknown"] != undefined ? result["M0150PaymentSourceUnknown"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1000InpatientFacilitiesLTC][value=' + (result["M1000InpatientFacilitiesLTC"] != null && result["M1000InpatientFacilitiesLTC"] != undefined ? result["M1000InpatientFacilitiesLTC"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1000InpatientFacilitiesSNF][value=' + (result["M1000InpatientFacilitiesSNF"] != null && result["M1000InpatientFacilitiesSNF"] != undefined ? result["M1000InpatientFacilitiesSNF"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1000InpatientFacilitiesIPPS][value=' + (result["M1000InpatientFacilitiesIPPS"] != null && result["M1000InpatientFacilitiesIPPS"] != undefined ? result["M1000InpatientFacilitiesIPPS"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1000InpatientFacilitiesLTCH][value=' + (result["M1000InpatientFacilitiesLTCH"] != null && result["M1000InpatientFacilitiesLTCH"] != undefined ? result["M1000InpatientFacilitiesLTCH"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1000InpatientFacilitiesIRF][value=' + (result["M1000InpatientFacilitiesIRF"] != null && result["M1000InpatientFacilitiesIRF"] != undefined ? result["M1000InpatientFacilitiesIRF"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1000InpatientFacilitiesPhych][value=' + (result["M1000InpatientFacilitiesPhych"] != null && result["M1000InpatientFacilitiesPhych"] != undefined ? result["M1000InpatientFacilitiesPhych"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1000InpatientFacilitiesOTHR][value=' + (result["M1000InpatientFacilitiesOTHR"] != null && result["M1000InpatientFacilitiesOTHR"] != undefined ? result["M1000InpatientFacilitiesOTHR"].Answer : "") + ']').attr('checked', true);

            var inpatientFacilitiesOTHR = result["M1000InpatientFacilitiesOTHR"];
            if (inpatientFacilitiesOTHR != null && inpatientFacilitiesOTHR != undefined) {
                if (inpatientFacilitiesOTHR.Answer == 1) {

                    $('input[name=StartOfCare_M1000InpatientFacilitiesOTHR][value=1]').attr('checked', true);
                    $("#StartOfCare_M1000InpatientFacilitiesOther").val(result["M1000InpatientFacilitiesOther"] != null && result["M1000InpatientFacilitiesOther"] != undefined ? result["M1000InpatientFacilitiesOther"].Answer : "");

                }
                else {
                    $('input[name=StartOfCare_M1000InpatientFacilitiesOTHR][value=1]').attr('checked', false);

                }
            }
            $('input[name=StartOfCare_M1000InpatientFacilitiesNone][value=' + (result["M1000InpatientFacilitiesNone"] != null && result["M1000InpatientFacilitiesNone"] != undefined ? result["M1000InpatientFacilitiesNone"].Answer : "") + ']').attr('checked', true);
            var inpatientFacilitiesNone = result["M1000InpatientFacilitiesNone"];

            if (inpatientFacilitiesNone == 1) {

                $("#soc_M1005").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });

                $("#soc_M1012").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
                $("#soc_M1010").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });

            }
            else {
                var inpatientDischargeDateUnknown = result["M1005InpatientDischargeDateUnknown"];
                if (inpatientDischargeDateUnknown != null && inpatientDischargeDateUnknown != undefined && inpatientDischargeDateUnknown.Answer != null) {
                    if (inpatientDischargeDateUnknown.Answer == 1) {
                        $('input[name=StartOfCare_M1005InpatientDischargeDateUnknown][value=1]').attr('checked', true);
                    }
                    else {
                        $('input[name=StartOfCare_M1005InpatientDischargeDateUnknown][value=1]').attr('checked', false);
                        $("#StartOfCare_M1005InpatientDischargeDate").val(result["M1005InpatientDischargeDate"] != null && result["M1005InpatientDischargeDate"] != undefined ? result["M1005InpatientDischargeDate"].Answer : "");
                    }
                }
                var inpatientFacilityDiagnosisCode1 = result["M1010InpatientFacilityDiagnosisCode1"];
                if (inpatientFacilityDiagnosisCode1 != null && inpatientFacilityDiagnosisCode1 != undefined && inpatientFacilityDiagnosisCode1.Answer != null) {
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosisCode1]').val(result["M1010InpatientFacilityDiagnosisCode1"] != null && result["M1010InpatientFacilityDiagnosisCode1"] != undefined ? result["M1010InpatientFacilityDiagnosisCode1"].Answer : "");
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosis1]').val(result["M1010InpatientFacilityDiagnosis1"] != null && result["M1010InpatientFacilityDiagnosis1"] != undefined ? result["M1010InpatientFacilityDiagnosis1"].Answer : "");
                }

                var inpatientFacilityDiagnosisCode2 = result["M1010InpatientFacilityDiagnosisCode2"];
                if (inpatientFacilityDiagnosisCode2 != null && inpatientFacilityDiagnosisCode2 != undefined && inpatientFacilityDiagnosisCode2.Answer != null) {
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosisCode2]').val(result["M1010InpatientFacilityDiagnosisCode2"] != null && result["M1010InpatientFacilityDiagnosisCode2"] != undefined ? result["M1010InpatientFacilityDiagnosisCode2"].Answer : "");
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosis2]').val(result["M1010InpatientFacilityDiagnosis2"] != null && result["M1010InpatientFacilityDiagnosis2"] != undefined ? result["M1010InpatientFacilityDiagnosis2"].Answer : "");
                }

                var inpatientFacilityDiagnosisCode3 = result["M1010InpatientFacilityDiagnosisCode3"];
                if (inpatientFacilityDiagnosisCode3 != null && inpatientFacilityDiagnosisCode3 != undefined && inpatientFacilityDiagnosisCode3.Answer != null) {
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosisCode3]').val(result["M1010InpatientFacilityDiagnosisCode3"] != null && result["M1010InpatientFacilityDiagnosisCode3"] != undefined ? result["M1010InpatientFacilityDiagnosisCode3"].Answer : "");
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosis3]').val(result["M1010InpatientFacilityDiagnosis3"] != null && result["M1010InpatientFacilityDiagnosis3"] != undefined ? result["M1010InpatientFacilityDiagnosis3"].Answer : "");
                }

                var inpatientFacilityDiagnosisCode4 = result["M1010InpatientFacilityDiagnosisCode4"];
                if (inpatientFacilityDiagnosisCode4 != null && inpatientFacilityDiagnosisCode4 != undefined && inpatientFacilityDiagnosisCode4.Answer != null) {
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosisCode4]').val(result["M1010InpatientFacilityDiagnosisCode4"] != null && result["M1010InpatientFacilityDiagnosisCode4"] != undefined ? result["M1010InpatientFacilityDiagnosisCode4"].Answer : "");
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosis4]').val(result["M1010InpatientFacilityDiagnosis4"] != null && result["M1010InpatientFacilityDiagnosis4"] != undefined ? result["M1010InpatientFacilityDiagnosis4"].Answer : "");
                }

                var inpatientFacilityDiagnosisCode5 = result["M1010InpatientFacilityDiagnosisCode5"];
                if (inpatientFacilityDiagnosisCode5 != null && inpatientFacilityDiagnosisCode5 != undefined && inpatientFacilityDiagnosisCode5.Answer != null) {
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosisCode5]').val(result["M1010InpatientFacilityDiagnosisCode5"] != null && result["M1010InpatientFacilityDiagnosisCode5"] != undefined ? result["M1010InpatientFacilityDiagnosisCode5"].Answer : "");
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosis5]').val(result["M1010InpatientFacilityDiagnosis5"] != null && result["M1010InpatientFacilityDiagnosis5"] != undefined ? result["M1010InpatientFacilityDiagnosis5"].Answer : "");
                }

                var inpatientFacilityDiagnosisCode6 = result["M1010InpatientFacilityDiagnosisCode6"];
                if (inpatientFacilityDiagnosisCode6 != null && inpatientFacilityDiagnosisCode6 != undefined && inpatientFacilityDiagnosisCode6.Answer != null) {
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosisCode6]').val(result["M1010InpatientFacilityDiagnosisCode6"] != null && result["M1010InpatientFacilityDiagnosisCode6"] != undefined ? result["M1010InpatientFacilityDiagnosisCode6"].Answer : "");
                    $('input[name=StartOfCare_M1010InpatientFacilityDiagnosis6]').val(result["M1010InpatientFacilityDiagnosis6"] != null && result["M1010InpatientFacilityDiagnosis6"] != undefined ? result["M1010InpatientFacilityDiagnosis6"].Answer : "");
                }


                var inpatientFacilityProcedureCode1 = result["M1012InpatientFacilityProcedureCode1"];
                if (inpatientFacilityProcedureCode1 != null && inpatientFacilityProcedureCode1 != undefined && inpatientFacilityProcedureCode1.Answer != null) {
                    $('input[name=StartOfCare_M1012InpatientFacilityProcedureCode1]').val(result["M1012InpatientFacilityProcedureCode1"] != null && result["M1012InpatientFacilityProcedureCode1"] != undefined ? result["M1012InpatientFacilityProcedureCode1"].Answer : "");
                    $('input[name=StartOfCare_M1012InpatientFacilityProcedure1]').val(result["M1012InpatientFacilityProcedure1"] != null && result["M1012InpatientFacilityProcedure1"] != undefined ? result["M1012InpatientFacilityProcedure1"].Answer : "");
                }

                var inpatientFacilityProcedureCode2 = result["M1012InpatientFacilityProcedureCode2"];
                if (inpatientFacilityProcedureCode2 != null && inpatientFacilityProcedureCode2 != undefined && inpatientFacilityProcedureCode2.Answer != null) {
                    $('input[name=StartOfCare_M1012InpatientFacilityProcedureCode2]').val(result["M1012InpatientFacilityProcedureCode2"] != null && result["M1012InpatientFacilityProcedureCode2"] != undefined ? result["M1012InpatientFacilityProcedureCode2"].Answer : "");
                    $('input[name=StartOfCare_M1012InpatientFacilityProcedure2]').val(result["M1012InpatientFacilityProcedure2"] != null && result["M1012InpatientFacilityProcedure2"] != undefined ? result["M1012InpatientFacilityProcedure2"].Answer : "");
                }
                var inpatientFacilityProcedureCode3 = result["M1012InpatientFacilityProcedureCode3"];
                if (inpatientFacilityProcedureCode3 != null && inpatientFacilityProcedureCode3 != undefined && inpatientFacilityProcedureCode3.Answer != null) {
                    $('input[name=StartOfCare_M1012InpatientFacilityProcedureCode3]').val(result["M1012InpatientFacilityProcedureCode3"] != null && result["M1012InpatientFacilityProcedureCode3"] != undefined ? result["M1012InpatientFacilityProcedureCode3"].Answer : "");
                    $('input[name=StartOfCare_M1012InpatientFacilityProcedure3]').val(result["M1012InpatientFacilityProcedure3"] != null && result["M1012InpatientFacilityProcedure3"] != undefined ? result["M1012InpatientFacilityProcedure3"].Answer : "");
                }
                var inpatientFacilityProcedureCode4 = result["M1012InpatientFacilityProcedureCode4"];
                if (inpatientFacilityProcedureCode4 != null && inpatientFacilityProcedureCode4 != undefined && inpatientFacilityProcedureCode4.Answer != null) {
                    $('input[name=StartOfCare_M1012InpatientFacilityProcedureCode4]').val(result["M1012InpatientFacilityProcedureCode4"] != null && result["M1012InpatientFacilityProcedureCode4"] != undefined ? result["M1012InpatientFacilityProcedureCode4"].Answer : "");
                    $('input[name=StartOfCare_M1012InpatientFacilityProcedure4]').val(result["M1012InpatientFacilityProcedure4"] != null && result["M1012InpatientFacilityProcedure4"] != undefined ? result["M1012InpatientFacilityProcedure4"].Answer : "");
                }

                $('input[name=StartOfCare_M1012InpatientFacilityProcedureCodeNotApplicable][value=' + (result["M1012InpatientFacilityProcedureCodeNotApplicable"] != null && result["M1012InpatientFacilityProcedureCodeNotApplicable"] != undefined ? result["M1012InpatientFacilityProcedureCodeNotApplicable"].Answer : "") + ']').attr('checked', true);

                $('input[name=StartOfCare_M1012InpatientFacilityProcedureCodeUnknown][value=' + (result["M1012InpatientFacilityProcedureCodeUnknown"] != null && result["M1012InpatientFacilityProcedureCodeUnknown"] != undefined ? result["M1012InpatientFacilityProcedureCodeUnknown"].Answer : "") + ']').attr('checked', true);

            }

            var medicalRegimenDiagnosisCode1 = result["M1016MedicalRegimenDiagnosisCode1"];
            if (medicalRegimenDiagnosisCode1 != null && medicalRegimenDiagnosisCode1 != undefined && medicalRegimenDiagnosisCode1.Answer != null) {
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosisCode1]').val(result["M1016MedicalRegimenDiagnosisCode1"] != null && result["M1016MedicalRegimenDiagnosisCode1"] != undefined ? result["M1016MedicalRegimenDiagnosisCode1"].Answer : "");
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosis1]').val(result["M1016MedicalRegimenDiagnosis1"] != null && result["M1016MedicalRegimenDiagnosis1"] != undefined ? result["M1016MedicalRegimenDiagnosis1"].Answer : "");
            }
            var medicalRegimenDiagnosisCode2 = result["M1016MedicalRegimenDiagnosisCode2"];
            if (medicalRegimenDiagnosisCode2 != null && medicalRegimenDiagnosisCode2 != undefined && medicalRegimenDiagnosisCode2.Answer != null) {
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosisCode2]').val(result["M1016MedicalRegimenDiagnosisCode2"] != null && result["M1016MedicalRegimenDiagnosisCode2"] != undefined ? result["M1016MedicalRegimenDiagnosisCode2"].Answer : "");
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosis2]').val(result["M1016MedicalRegimenDiagnosis2"] != null && result["M1016MedicalRegimenDiagnosis2"] != undefined ? result["M1016MedicalRegimenDiagnosis2"].Answer : "");
            }
            var medicalRegimenDiagnosisCode3 = result["M1016MedicalRegimenDiagnosisCode3"];
            if (medicalRegimenDiagnosisCode3 != null && medicalRegimenDiagnosisCode3 != undefined && medicalRegimenDiagnosisCode3.Answer != null) {
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosisCode3]').val(result["M1016MedicalRegimenDiagnosisCode3"] != null && result["M1016MedicalRegimenDiagnosisCode3"] != undefined ? result["M1016MedicalRegimenDiagnosisCode3"].Answer : "");
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosis3]').val(result["M1016MedicalRegimenDiagnosis3"] != null && result["M1016MedicalRegimenDiagnosis3"] != undefined ? result["M1016MedicalRegimenDiagnosis3"].Answer : "");
            }
            var medicalRegimenDiagnosisCode4 = result["M1016MedicalRegimenDiagnosisCode4"];
            if (medicalRegimenDiagnosisCode4 != null && medicalRegimenDiagnosisCode4 != undefined && medicalRegimenDiagnosisCode4.Answer != null) {
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosisCode4]').val(result["M1016MedicalRegimenDiagnosisCode4"] != null && result["M1016MedicalRegimenDiagnosisCode4"] != undefined ? result["M1016MedicalRegimenDiagnosisCode4"].Answer : "");
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosis4]').val(result["M1016MedicalRegimenDiagnosis4"] != null && result["M1016MedicalRegimenDiagnosis4"] != undefined ? result["M1016MedicalRegimenDiagnosis4"].Answer : "");
            }
            var medicalRegimenDiagnosisCode5 = result["M1016MedicalRegimenDiagnosisCode5"];
            if (medicalRegimenDiagnosisCode5 != null && medicalRegimenDiagnosisCode5 != undefined && medicalRegimenDiagnosisCode5.Answer != null) {
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosisCode5]').val(result["M1016MedicalRegimenDiagnosisCode5"] != null && result["M1016MedicalRegimenDiagnosisCode5"] != undefined ? result["M1016MedicalRegimenDiagnosisCode5"].Answer : "");
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosis5]').val(result["M1016MedicalRegimenDiagnosis5"] != null && result["M1016MedicalRegimenDiagnosis5"] != undefined ? result["M1016MedicalRegimenDiagnosis5"].Answer : "");
            }
            var medicalRegimenDiagnosisCode6 = result["M1016MedicalRegimenDiagnosisCode6"];
            if (medicalRegimenDiagnosisCode6 != null && medicalRegimenDiagnosisCode6 != undefined && medicalRegimenDiagnosisCode6.Answer != null) {
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosisCode6]').val(result["M1016MedicalRegimenDiagnosisCode6"] != null && result["M1016MedicalRegimenDiagnosisCode6"] != undefined ? result["M1016MedicalRegimenDiagnosisCode6"].Answer : "");
                $('input[name=StartOfCare_M1016MedicalRegimenDiagnosis6]').val(result["M1016MedicalRegimenDiagnosis6"] != null && result["M1016MedicalRegimenDiagnosis6"] != undefined ? result["M1016MedicalRegimenDiagnosis6"].Answer : "");
            }
            $('input[name=StartOfCare_M1016MedicalRegimenDiagnosisNotApplicable][value=' + (result["M1016MedicalRegimenDiagnosisNotApplicable"] != null && result["M1016MedicalRegimenDiagnosisNotApplicable"] != undefined ? result["M1016MedicalRegimenDiagnosisNotApplicable"].Answer : "") + ']').attr('checked', true);

            var ICD9M = result["M1020ICD9M"];
            if (ICD9M != null && ICD9M != undefined && ICD9M.Answer != null) {
                $("#StartOfCare_M1020ICD9M").val(result["M1020ICD9M"] != null && result["M1020ICD9M"] != undefined ? result["M1020ICD9M"].Answer : "");
                $("#StartOfCare_M1020PrimaryDiagnosis").val(result["M1020PrimaryDiagnosis"] != null && result["M1020PrimaryDiagnosis"] != undefined ? result["M1020PrimaryDiagnosis"].Answer : "");
                $("#StartOfCare_M1020SymptomControlRating").val(result["M1020SymptomControlRating"] != null && result["M1020SymptomControlRating"] != undefined ? result["M1020SymptomControlRating"].Answer : "");
            }
            var ICD9MA3 = result["M1024ICD9MA3"];
            if (ICD9MA3 != null && ICD9MA3 != undefined && ICD9MA3.Answer != null) {
                $("#StartOfCare_M1024ICD9MA3").val(result["M1024ICD9MA3"] != null && result["M1024ICD9MA3"] != undefined ? result["M1024ICD9MA3"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesA3").val(result["M1024PaymentDiagnosesA3"] != null && result["M1024PaymentDiagnosesA3"] != undefined ? result["M1024PaymentDiagnosesA3"].Answer : "");
            }
            var ICD9MA4 = result["M1024ICD9MA4"];
            if (ICD9MA4 != null && ICD9MA4 != undefined && ICD9MA4.Answer != null) {
                $("#StartOfCare_M1024ICD9MA4").val(result["M1024ICD9MA4"] != null && result["M1024ICD9MA4"] != undefined ? result["M1024ICD9MA4"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesA4").val(result["M1024PaymentDiagnosesA4"] != null && result["M1024PaymentDiagnosesA4"] != undefined ? result["M1024PaymentDiagnosesA4"].Answer : "");
            }

            var ICD9M1 = result["M1022ICD9M1"];
            if (ICD9M1 != null && ICD9M1 != undefined && ICD9M1.Answer != null) {
                $("#StartOfCare_M1022ICD9M1").val(result["M1022ICD9M1"] != null && result["M1022ICD9M1"] != undefined ? result["M1022ICD9M1"].Answer : "");
                $("#StartOfCare_M1022PrimaryDiagnosis1").val(result["M1022PrimaryDiagnosis1"] != null && result["M1022PrimaryDiagnosis1"] != undefined ? result["M1022PrimaryDiagnosis1"].Answer : "");
                $("#StartOfCare_M1022OtherDiagnose1Rating").val(result["M1022OtherDiagnose1Rating"] != null && result["M1022OtherDiagnose1Rating"] != undefined ? result["M1022OtherDiagnose1Rating"].Answer : "");
            }
            var ICD9MB3 = result["M1024ICD9MB3"];
            if (ICD9MB3 != null && ICD9MB3 != undefined && ICD9MB3.Answer != null) {
                $("#StartOfCare_M1024ICD9MB3").val(result["M1024ICD9MB3"] != null && result["M1024ICD9MB3"] != undefined ? result["M1024ICD9MB3"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesB3").val(result["M1024PaymentDiagnosesB3"] != null && result["M1024PaymentDiagnosesB3"] != undefined ? result["M1024PaymentDiagnosesB3"].Answer : "");
            }
            var ICD9MB4 = result["M1024ICD9MB4"];
            if (ICD9MB4 != null && ICD9MB4 != undefined && ICD9MB4.Answer != null) {
                $("#StartOfCare_M1024ICD9MB4").val(result["M1024ICD9MB4"] != null && result["M1024ICD9MB4"] != undefined ? result["M1024ICD9MB4"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesB4").val(result["M1024PaymentDiagnosesB4"] != null && result["M1024PaymentDiagnosesB4"] != undefined ? result["M1024PaymentDiagnosesB4"].Answer : "");
            }


            var ICD9M2 = result["M1022ICD9M2"];
            if (ICD9M2 != null && ICD9M2 != undefined && ICD9M2.Answer != null) {
                $("#StartOfCare_M1022ICD9M2").val(result["M1022ICD9M2"] != null && result["M1022ICD9M2"] != undefined ? result["M1022ICD9M2"].Answer : "");
                $("#StartOfCare_M1022PrimaryDiagnosis2").val(result["M1022PrimaryDiagnosis2"] != null && result["M1022PrimaryDiagnosis2"] != undefined ? result["M1022PrimaryDiagnosis2"].Answer : "");
                $("#StartOfCare_M1022OtherDiagnose2Rating").val(result["M1022OtherDiagnose2Rating"] != null && result["M1022OtherDiagnose2Rating"] != undefined ? result["M1022OtherDiagnose2Rating"].Answer : "");
            }
            var ICD9MC3 = result["M1024ICD9MC3"];
            if (ICD9MC3 != null && ICD9MC3 != undefined && ICD9MC3.Answer != null) {
                $("#StartOfCare_M1024ICD9MC3").val(result["M1024ICD9MC3"] != null && result["M1024ICD9MC3"] != undefined ? result["M1024ICD9MC3"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesC3").val(result["M1024PaymentDiagnosesC3"] != null && result["M1024PaymentDiagnosesC3"] != undefined ? result["M1024PaymentDiagnosesC3"].Answer : "");
            }
            var ICD9MC4 = result["M1024ICD9MC4"];
            if (ICD9MC4 != null && ICD9MC4 != undefined && ICD9MC4.Answer != null) {
                $("#StartOfCare_M1024ICD9MC4").val(result["M1024ICD9MC4"] != null && result["M1024ICD9MC4"] != undefined ? result["M1024ICD9MC4"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesC4").val(result["M1024PaymentDiagnosesC4"] != null && result["M1024PaymentDiagnosesC4"] != undefined ? result["M1024PaymentDiagnosesC4"].Answer : "");
            }

            var ICD9M3 = result["M1022ICD9M3"];
            if (ICD9M3 != null && ICD9M3 != undefined && ICD9M3.Answer != null) {
                $("#StartOfCare_M1022ICD9M3").val(result["M1022ICD9M3"] != null && result["M1022ICD9M3"] != undefined ? result["M1022ICD9M3"].Answer : "");
                $("#StartOfCare_M1022PrimaryDiagnosis3").val(result["M1022PrimaryDiagnosis3"] != null && result["M1022PrimaryDiagnosis3"] != undefined ? result["M1022PrimaryDiagnosis3"].Answer : "");
                $("#StartOfCare_M1022OtherDiagnose3Rating").val(result["M1022OtherDiagnose3Rating"] != null && result["M1022OtherDiagnose3Rating"] != undefined ? result["M1022OtherDiagnose3Rating"].Answer : "");
            }
            var ICD9MD3 = result["M1024ICD9MD3"];
            if (ICD9MD3 != null && ICD9MD3 != undefined && ICD9MD3.Answer != null) {
                $("#StartOfCare_M1024ICD9MD3").val(result["M1024ICD9MD3"] != null && result["M1024ICD9MD3"] != undefined ? result["M1024ICD9MD3"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesD3").val(result["M1024PaymentDiagnosesD3"] != null && result["M1024PaymentDiagnosesD3"] != undefined ? result["M1024PaymentDiagnosesD3"].Answer : "");
            }
            var ICD9MD4 = result["M1024ICD9MD4"];
            if (ICD9MD4 != null && ICD9MD4 != undefined && ICD9MD4.Answer != null) {
                $("#StartOfCare_M1024ICD9MD4").val(result["M1024ICD9MD4"] != null && result["M1024ICD9MD4"] != undefined ? result["M1024ICD9MD4"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesD4").val(result["M1024PaymentDiagnosesD4"] != null && result["M1024PaymentDiagnosesD4"] != undefined ? result["M1024PaymentDiagnosesD4"].Answer : "");
            }

            var ICD9M4 = result["M1022ICD9M4"];
            if (ICD9M4 != null && ICD9M4 != undefined && ICD9M4.Answer != null) {
                $("#StartOfCare_M1022ICD9M4").val(result["M1022ICD9M4"] != null && result["M1022ICD9M4"] != undefined ? result["M1022ICD9M4"].Answer : "");
                $("#StartOfCare_M1022PrimaryDiagnosis4").val(result["M1022PrimaryDiagnosis4"] != null && result["M1022PrimaryDiagnosis4"] != undefined ? result["M1022PrimaryDiagnosis4"].Answer : "");
                $("#StartOfCare_M1022OtherDiagnose4Rating").val(result["M1022OtherDiagnose4Rating"] != null && result["M1022OtherDiagnose4Rating"] != undefined ? result["M1022OtherDiagnose4Rating"].Answer : "");
            }
            var ICD9ME3 = result["M1024ICD9ME3"];
            if (ICD9ME3 != null && ICD9ME3 != undefined && ICD9ME3.Answer != null) {
                $("#StartOfCare_M1024ICD9ME3").val(result["M1024ICD9ME3"] != null && result["M1024ICD9ME3"] != undefined ? result["M1024ICD9ME3"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesE3").val(result["M1024PaymentDiagnosesE3"] != null && result["M1024PaymentDiagnosesE3"] != undefined ? result["M1024PaymentDiagnosesE3"].Answer : "");
            }
            var ICD9ME4 = result["M1024ICD9ME4"];
            if (ICD9ME4 != null && ICD9ME4 != undefined && ICD9ME4.Answer != null) {
                $("#StartOfCare_M1024ICD9ME4").val(result["M1024ICD9ME4"] != null && result["M1024ICD9ME4"] != undefined ? result["M1024ICD9ME4"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesE4").val(result["M1024PaymentDiagnosesE4"] != null && result["M1024PaymentDiagnosesE4"] != undefined ? result["M1024PaymentDiagnosesE4"].Answer : "");
            }

            var ICD9M5 = result["M1022ICD9M5"];
            if (ICD9M5 != null && ICD9M5 != undefined && ICD9M5.Answer != null) {
                $("#StartOfCare_M1022ICD9M5").val(result["M1022ICD9M5"] != null && result["M1022ICD9M5"] != undefined ? result["M1022ICD9M5"].Answer : "");
                $("#StartOfCare_M1022PrimaryDiagnosis5").val(result["M1022PrimaryDiagnosis5"] != null && result["M1022PrimaryDiagnosis5"] != undefined ? result["M1022PrimaryDiagnosis5"].Answer : "");
                $("#StartOfCare_M1022OtherDiagnose5Rating").val(result["M1022OtherDiagnose5Rating"] != null && result["M1022OtherDiagnose5Rating"] != undefined ? result["M1022OtherDiagnose5Rating"].Answer : "");
            }
            var ICD9MF3 = result["M1024ICD9MF3"];
            if (ICD9MF3 != null && ICD9MF3 != undefined && ICD9MF3.Answer != null) {
                $("#StartOfCare_M1024ICD9MF3").val(result["M1024ICD9MF3"] != null && result["M1024ICD9MF3"] != undefined ? result["M1024ICD9MF3"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesF3").val(result["M1024PaymentDiagnosesF3"] != null && result["M1024PaymentDiagnosesF3"] != undefined ? result["M1024PaymentDiagnosesF3"].Answer : "");
            }
            var ICD9MF4 = result["M1024ICD9MF4"];
            if (ICD9MF4 != null && ICD9MF4 != undefined && ICD9MF4.Answer != null) {
                $("#StartOfCare_M1024ICD9MF4").val(result["M1024ICD9MF4"] != null && result["M1024ICD9MF4"] != undefined ? result["M1024ICD9MF4"].Answer : "");
                $("#StartOfCare_M1024PaymentDiagnosesF4").val(result["M1024PaymentDiagnosesF4"] != null && result["M1024PaymentDiagnosesF4"] != undefined ? result["M1024PaymentDiagnosesF4"].Answer : "");
            }


            $('input[name=StartOfCare_M1018ConditionsPriorToMedicalRegimenUI][value=' + (result["M1018ConditionsPriorToMedicalRegimenUI"] != null && result["M1018ConditionsPriorToMedicalRegimenUI"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenUI"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1018ConditionsPriorToMedicalRegimenCATH][value=' + (result["M1018ConditionsPriorToMedicalRegimenCATH"] != null && result["M1018ConditionsPriorToMedicalRegimenCATH"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenCATH"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1018ConditionsPriorToMedicalRegimenPain][value=' + (result["M1018ConditionsPriorToMedicalRegimenPain"] != null && result["M1018ConditionsPriorToMedicalRegimenPain"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenPain"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1018ConditionsPriorToMedicalRegimenDECSN][value=' + (result["M1018ConditionsPriorToMedicalRegimenDECSN"] != null && result["M1018ConditionsPriorToMedicalRegimenDECSN"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenDECSN"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1018ConditionsPriorToMedicalRegimenDisruptive][value=' + (result["M1018ConditionsPriorToMedicalRegimenDisruptive"] != null && result["M1018ConditionsPriorToMedicalRegimenDisruptive"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenDisruptive"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1018ConditionsPriorToMedicalRegimenMemLoss][value=' + (result["M1018ConditionsPriorToMedicalRegimenMemLoss"] != null && result["M1018ConditionsPriorToMedicalRegimenMemLoss"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenMemLoss"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1018ConditionsPriorToMedicalRegimenNone][value=' + (result["M1018ConditionsPriorToMedicalRegimenNone"] != null && result["M1018ConditionsPriorToMedicalRegimenNone"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1018ConditionsPriorToMedicalRegimenNA][value=' + (result["M1018ConditionsPriorToMedicalRegimenNA"] != null && result["M1018ConditionsPriorToMedicalRegimenNA"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenNA"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1018ConditionsPriorToMedicalRegimenUK][value=' + (result["M1018ConditionsPriorToMedicalRegimenUK"] != null && result["M1018ConditionsPriorToMedicalRegimenUK"] != undefined ? result["M1018ConditionsPriorToMedicalRegimenUK"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1030HomeTherapiesInfusion][value=' + (result["M1030HomeTherapiesInfusion"] != null && result["M1030HomeTherapiesInfusion"] != undefined ? result["M1030HomeTherapiesInfusion"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1030HomeTherapiesParNutrition][value=' + (result["M1030HomeTherapiesParNutrition"] != null && result["M1030HomeTherapiesParNutrition"] != undefined ? result["M1030HomeTherapiesParNutrition"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1030HomeTherapiesEntNutrition][value=' + (result["M1030HomeTherapiesEntNutrition"] != null && result["M1030HomeTherapiesEntNutrition"] != undefined ? result["M1030HomeTherapiesEntNutrition"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1030HomeTherapiesNone][value=' + (result["M1030HomeTherapiesNone"] != null && result["M1030HomeTherapiesNone"] != undefined ? result["M1030HomeTherapiesNone"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1032HospitalizationRiskRecentDecline][value=' + (result["M1032HospitalizationRiskRecentDecline"] != null && result["M1032HospitalizationRiskRecentDecline"] != undefined ? result["M1032HospitalizationRiskRecentDecline"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1032HospitalizationRiskMultipleHosp][value=' + (result["M1032HospitalizationRiskMultipleHosp"] != null && result["M1032HospitalizationRiskMultipleHosp"] != undefined ? result["M1032HospitalizationRiskMultipleHosp"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1032HospitalizationRiskHistoryOfFall][value=' + (result["M1032HospitalizationRiskHistoryOfFall"] != null && result["M1032HospitalizationRiskHistoryOfFall"] != undefined ? result["M1032HospitalizationRiskHistoryOfFall"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1032HospitalizationRiskMedications][value=' + (result["M1032HospitalizationRiskMedications"] != null && result["M1032HospitalizationRiskMedications"] != undefined ? result["M1032HospitalizationRiskMedications"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1032HospitalizationRiskFrailty][value=' + (result["M1032HospitalizationRiskFrailty"] != null && result["M1032HospitalizationRiskFrailty"] != undefined ? result["M1032HospitalizationRiskFrailty"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1032HospitalizationRiskOther][value=' + (result["M1032HospitalizationRiskOther"] != null && result["M1032HospitalizationRiskOther"] != undefined ? result["M1032HospitalizationRiskOther"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1032HospitalizationRiskNone][value=' + (result["M1032HospitalizationRiskNone"] != null && result["M1032HospitalizationRiskNone"] != undefined ? result["M1032HospitalizationRiskNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1034OverallStatus][value=' + (result["M1034OverallStatus"] != null && result["M1034OverallStatus"] != undefined ? result["M1034OverallStatus"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1036RiskFactorsSmoking][value=' + (result["M1036RiskFactorsSmoking"] != null && result["M1036RiskFactorsSmoking"] != undefined ? result["M1036RiskFactorsSmoking"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1036RiskFactorsObesity][value=' + (result["M1036RiskFactorsObesity"] != null && result["M1036RiskFactorsObesity"] != undefined ? result["M1036RiskFactorsObesity"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1036RiskFactorsAlcoholism][value=' + (result["M1036RiskFactorsAlcoholism"] != null && result["M1036RiskFactorsAlcoholism"] != undefined ? result["M1036RiskFactorsAlcoholism"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1036RiskFactorsDrugs][value=' + (result["M1036RiskFactorsDrugs"] != null && result["M1036RiskFactorsDrugs"] != undefined ? result["M1036RiskFactorsDrugs"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1036RiskFactorsNone][value=' + (result["M1036RiskFactorsNone"] != null && result["M1036RiskFactorsNone"] != undefined ? result["M1036RiskFactorsNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1036RiskFactorsUnknown][value=' + (result["M1036RiskFactorsUnknown"] != null && result["M1036RiskFactorsUnknown"] != undefined ? result["M1036RiskFactorsUnknown"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1100LivingSituation][value=' + (result["M1100LivingSituation"] != null && result["M1100LivingSituation"] != undefined ? result["M1100LivingSituation"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1200Vision][value=' + (result["M1200Vision"] != null && result["M1200Vision"] != undefined ? result["M1200Vision"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1210Hearing][value=' + (result["M1210Hearing"] != null && result["M1210Hearing"] != undefined ? result["M1210Hearing"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1220VerbalContent][value=' + (result["M1220VerbalContent"] != null && result["M1220VerbalContent"] != undefined ? result["M1220VerbalContent"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1230SpeechAndOral][value=' + (result["M1230SpeechAndOral"] != null && result["M1230SpeechAndOral"] != undefined ? result["M1230SpeechAndOral"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1240FormalPainAssessment][value=' + (result["M1240FormalPainAssessment"] != null && result["M1240FormalPainAssessment"] != undefined ? result["M1240FormalPainAssessment"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1242PainInterferingFrequency][value=' + (result["M1242PainInterferingFrequency"] != null && result["M1242PainInterferingFrequency"] != undefined ? result["M1242PainInterferingFrequency"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1300PressureUlcerAssessment][value=' + (result["M1300PressureUlcerAssessment"] != null && result["M1300PressureUlcerAssessment"] != undefined ? result["M1300PressureUlcerAssessment"].Answer : "") + ']').attr('checked', true);
            var pressureUlcerAssessment = $('input[name=StartOfCare_M1300PressureUlcerAssessment]:checked').val();
            if (pressureUlcerAssessment == "00") {

                $("#soc_M1302").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
            }
            else {

                $('input[name=StartOfCare_M1302RiskDevelopingPressureUlcers][value=' + (result["M1302RiskDevelopingPressureUlcers"] != null && result["M1302RiskDevelopingPressureUlcers"] != undefined ? result["M1302RiskDevelopingPressureUlcers"].Answer : "") + ']').attr('checked', true);

            }

            $('input[name=StartOfCare_M1306UnhealedPressureUlcers][value=' + (result["M1306UnhealedPressureUlcers"] != null && result["M1306UnhealedPressureUlcers"] != undefined ? result["M1306UnhealedPressureUlcers"].Answer : "") + ']').attr('checked', true);
            var unhealedPressureUlcers = $('input[name=StartOfCare_M1306UnhealedPressureUlcers]:checked').val();
            if (unhealedPressureUlcers == 0) {


                $("#soc_M1308").block(
                                {
                                    message: '',
                                    overlayCSS: {
                                        "cursor": "default"
                                    }
                                });
                $("#soc_M13010_12_14").block(
                                {
                                    message: '',
                                    overlayCSS: {
                                        "cursor": "default"
                                    }
                                });
                $("#soc_M1320").block(
                                {
                                    message: '',
                                    overlayCSS: {
                                        "cursor": "default"
                                    }
                                });
            }
            else if (unhealedPressureUlcers == 1) {
                $("#StartOfCare_M1307NonEpithelializedStageTwoUlcerDate").val(result["M1307NonEpithelializedStageTwoUlcerDate"] != null && result["M1307NonEpithelializedStageTwoUlcerDate"] != undefined ? result["M1307NonEpithelializedStageTwoUlcerDate"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedStageTwoUlcerCurrent").val(result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageTwoUlcerCurrent"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedStageTwoUlcerAdmission").val(result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageTwoUlcerAdmission"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedStageThreeUlcerCurrent").val(result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageThreeUlcerCurrent"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedStageThreeUlcerAdmission").val(result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageThreeUlcerAdmission"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedStageFourUlcerCurrent").val(result["M1308NumberNonEpithelializedStageFourUlcerCurrent"] != null && result["M1308NumberNonEpithelializedStageFourUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedStageFourUlcerCurrent"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedStageIVUlcerAdmission").val(result["M1308NumberNonEpithelializedStageIVUlcerAdmission"] != null && result["M1308NumberNonEpithelializedStageIVUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedStageIVUlcerAdmission"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedUnstageableIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedUnstageableIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerAdmission"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIUlcerCurrent"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent").val(result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"] != null && result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"].Answer : "");
                $("#StartOfCare_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission").val(result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"] != null && result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"] != undefined ? result["M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"].Answer : "");
                $("#StartOfCare_M1310PressureUlcerLength").val(result["M1310PressureUlcerLength"] != null && result["M1310PressureUlcerLength"] != undefined ? result["M1310PressureUlcerLength"].Answer : "");
                $("#StartOfCare_M1312PressureUlcerWidth").val(result["M1312PressureUlcerWidth"] != null && result["M1312PressureUlcerWidth"] != undefined ? result["M1312PressureUlcerWidth"].Answer : "");
                $("#StartOfCare_M1314PressureUlcerDepth").val(result["M1314PressureUlcerDepth"] != null && result["M1314PressureUlcerDepth"] != undefined ? result["M1314PressureUlcerDepth"].Answer : "");
                $('input[name=StartOfCare_M1320MostProblematicPressureUlcerStatus][value=' + (result["M1320MostProblematicPressureUlcerStatus"] != null && result["M1320MostProblematicPressureUlcerStatus"] != undefined ? result["M1320MostProblematicPressureUlcerStatus"].Answer : "") + ']').attr('checked', true);

                $("#soc_M1308").unblock();
                $("#soc_M13010_12_14").unblock();
                $("#soc_M1320").unblock();
            }

            $('input[name=StartOfCare_M1322CurrentNumberStageIUlcer][value=' + (result["M1322CurrentNumberStageIUlcer"] != null && result["M1322CurrentNumberStageIUlcer"] != undefined ? result["M1322CurrentNumberStageIUlcer"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1324MostProblematicUnhealedStage][value=' + (result["M1324MostProblematicUnhealedStage"] != null && result["M1324MostProblematicUnhealedStage"] != undefined ? result["M1324MostProblematicUnhealedStage"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1330StasisUlcer][value=' + (result["M1330StasisUlcer"] != null && result["M1330StasisUlcer"] != undefined ? result["M1330StasisUlcer"].Answer : "") + ']').attr('checked', true);
            var stasisUlcer = $('input[name=StartOfCare_M1300PressureUlcerAssessment]:checked').val();
            if (stasisUlcer == "00" || stasisUlcer == "03") {
                $("#soc_M1332AndM1334").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=StartOfCare_M1332CurrentNumberStasisUlcer][value=' + (result["M1332CurrentNumberStasisUlcer"] != null && result["M1332CurrentNumberStasisUlcer"] != undefined ? result["M1332CurrentNumberStasisUlcer"].Answer : "") + ']').attr('checked', true);
                $('input[name=StartOfCare_M1334StasisUlcerStatus][value=' + (result["M1334StasisUlcerStatus"] != null && result["M1334StasisUlcerStatus"] != undefined ? result["M1334StasisUlcerStatus"].Answer : "") + ']').attr('checked', true);
                $("#soc_M1332AndM1334").unblock();
            }

            $('input[name=StartOfCare_M1340SurgicalWound][value=' + (result["M1340SurgicalWound"] != null && result["M1340SurgicalWound"] != undefined ? result["M1340SurgicalWound"].Answer : "") + ']').attr('checked', true);
            var surgicalWound = $('input[name=StartOfCare_M1340SurgicalWound]:checked').val();
            if (surgicalWound == "00" || surgicalWound == "02") {
                $("#soc_M1342").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=StartOfCare_M1342SurgicalWoundStatus][value=' + (result["M1342SurgicalWoundStatus"] != null && result["M1342SurgicalWoundStatus"] != undefined ? result["M1342SurgicalWoundStatus"].Answer : "") + ']').attr('checked', true);
                $("#soc_M1342").unblock();
            }
            $('input[name=StartOfCare_M1350SkinLesionOpenWound][value=' + (result["M1350SkinLesionOpenWound"] != null && result["M1350SkinLesionOpenWound"] != undefined ? result["M1350SkinLesionOpenWound"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1400PatientDyspneic][value=' + (result["M1400PatientDyspneic"] != null && result["M1400PatientDyspneic"] != undefined ? result["M1400PatientDyspneic"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1410HomeRespiratoryTreatmentsOxygen][value=' + (result["M1410HomeRespiratoryTreatmentsOxygen"] != null && result["M1410HomeRespiratoryTreatmentsOxygen"] != undefined ? result["M1410HomeRespiratoryTreatmentsOxygen"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1410HomeRespiratoryTreatmentsVentilator][value=' + (result["M1410HomeRespiratoryTreatmentsVentilator"] != null && result["M1410HomeRespiratoryTreatmentsVentilator"] != undefined ? result["M1410HomeRespiratoryTreatmentsVentilator"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1410HomeRespiratoryTreatmentsContinuous][value=' + (result["M1410HomeRespiratoryTreatmentsContinuous"] != null && result["M1410HomeRespiratoryTreatmentsContinuous"] != undefined ? result["M1410HomeRespiratoryTreatmentsContinuous"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1410HomeRespiratoryTreatmentsNone][value=' + (result["M1410HomeRespiratoryTreatmentsNone"] != null && result["M1410HomeRespiratoryTreatmentsNone"] != undefined ? result["M1410HomeRespiratoryTreatmentsNone"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1600UrinaryTractInfection][value=' + (result["M1600UrinaryTractInfection"] != null && result["M1600UrinaryTractInfection"] != undefined ? result["M1600UrinaryTractInfection"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1610UrinaryIncontinence][value=' + (result["M1610UrinaryIncontinence"] != null && result["M1610UrinaryIncontinence"] != undefined ? result["M1610UrinaryIncontinence"].Answer : "") + ']').attr('checked', true);
            var urinaryIncontinence = $('input[name=StartOfCare_M1610UrinaryIncontinence]:checked').val();
            if (urinaryIncontinence == "00" || urinaryIncontinence == "02") {
                $("#soc_M1615").block(
                {
                    message: '',
                    overlayCSS: {
                        "cursor": "default"
                    }
                });
            }
            else {
                $('input[name=StartOfCare_M1615UrinaryIncontinenceOccur][value=' + (result["M1615UrinaryIncontinenceOccur"] != null && result["M1615UrinaryIncontinenceOccur"] != undefined ? result["M1615UrinaryIncontinenceOccur"].Answer : "") + ']').attr('checked', true);
                $("#soc_M1615").unblock();
            }
            $('input[name=StartOfCare_M1620BowelIncontinenceFrequency][value=' + (result["M1620BowelIncontinenceFrequency"] != null && result["M1620BowelIncontinenceFrequency"] != undefined ? result["M1620BowelIncontinenceFrequency"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1630OstomyBowelElimination][value=' + (result["M1630OstomyBowelElimination"] != null && result["M1630OstomyBowelElimination"] != undefined ? result["M1630OstomyBowelElimination"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1700CognitiveFunctioning][value=' + (result["M1700CognitiveFunctioning"] != null && result["M1700CognitiveFunctioning"] != undefined ? result["M1700CognitiveFunctioning"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1710WhenConfused][value=' + (result["M1710WhenConfused"] != null && result["M1710WhenConfused"] != undefined ? result["M1710WhenConfused"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1720WhenAnxious][value=' + (result["M1720WhenAnxious"] != null && result["M1720WhenAnxious"] != undefined ? result["M1720WhenAnxious"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1730DepressionScreening][value=' + (result["M1730DepressionScreening"] != null && result["M1730DepressionScreening"] != undefined ? result["M1730DepressionScreening"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_M1730DepressionScreening]:checked').val() == '01') {
                $('input[name=StartOfCare_M1730DepressionScreeningInterest][value=' + (result["M1730DepressionScreeningInterest"] != null && result["M1730DepressionScreeningInterest"] != undefined ? result["M1730DepressionScreeningInterest"].Answer : "") + ']').attr('checked', true);
                $('input[name=StartOfCare_M1730DepressionScreeningHopeless][value=' + (result["M1730DepressionScreeningHopeless"] != null && result["M1730DepressionScreeningHopeless"] != undefined ? result["M1730DepressionScreeningHopeless"].Answer : "") + ']').attr('checked', true);
            }

            $('input[name=StartOfCare_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1740CognitiveBehavioralPsychiatricSymptomsImpDes][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsImpDes"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1740CognitiveBehavioralPsychiatricSymptomsVerbal][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsVerbal"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1740CognitiveBehavioralPsychiatricSymptomsPhysical][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsPhysical"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1740CognitiveBehavioralPsychiatricSymptomsSIB][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsSIB"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsSIB"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsSIB"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1740CognitiveBehavioralPsychiatricSymptomsDelusional][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsDelusional"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1740CognitiveBehavioralPsychiatricSymptomsNone][value=' + (result["M1740CognitiveBehavioralPsychiatricSymptomsNone"] != null && result["M1740CognitiveBehavioralPsychiatricSymptomsNone"] != undefined ? result["M1740CognitiveBehavioralPsychiatricSymptomsNone"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1745DisruptiveBehaviorSymptomsFrequency][value=' + (result["M1745DisruptiveBehaviorSymptomsFrequency"] != null && result["M1745DisruptiveBehaviorSymptomsFrequency"] != undefined ? result["M1745DisruptiveBehaviorSymptomsFrequency"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1750PsychiatricNursingServicing][value=' + (result["M1750PsychiatricNursingServicing"] != null && result["M1750PsychiatricNursingServicing"] != undefined ? result["M1750PsychiatricNursingServicing"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1800Grooming][value=' + (result["M1800Grooming"] != null && result["M1800Grooming"] != undefined ? result["M1800Grooming"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1810CurrentAbilityToDressUpper][value=' + (result["M1810CurrentAbilityToDressUpper"] != null && result["M1810CurrentAbilityToDressUpper"] != undefined ? result["M1810CurrentAbilityToDressUpper"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1820CurrentAbilityToDressLower][value=' + (result["M1820CurrentAbilityToDressLower"] != null && result["M1820CurrentAbilityToDressLower"] != undefined ? result["M1820CurrentAbilityToDressLower"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1830CurrentAbilityToBatheEntireBody][value=' + (result["M1830CurrentAbilityToBatheEntireBody"] != null && result["M1830CurrentAbilityToBatheEntireBody"] != undefined ? result["M1830CurrentAbilityToBatheEntireBody"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1840ToiletTransferring][value=' + (result["M1840ToiletTransferring"] != null && result["M1840ToiletTransferring"] != undefined ? result["M1840ToiletTransferring"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1845ToiletingHygiene][value=' + (result["M1845ToiletingHygiene"] != null && result["M1845ToiletingHygiene"] != undefined ? result["M1845ToiletingHygiene"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1850Transferring][value=' + (result["M1850Transferring"] != null && result["M1850Transferring"] != undefined ? result["M1850Transferring"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1860AmbulationLocomotion][value=' + (result["M1860AmbulationLocomotion"] != null && result["M1860AmbulationLocomotion"] != undefined ? result["M1860AmbulationLocomotion"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1870FeedingOrEating][value=' + (result["M1870FeedingOrEating"] != null && result["M1870FeedingOrEating"] != undefined ? result["M1870FeedingOrEating"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1880AbilityToPrepareLightMeal][value=' + (result["M1880AbilityToPrepareLightMeal"] != null && result["M1880AbilityToPrepareLightMeal"] != undefined ? result["M1880AbilityToPrepareLightMeal"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1890AbilityToUseTelephone][value=' + (result["M1890AbilityToUseTelephone"] != null && result["M1890AbilityToUseTelephone"] != undefined ? result["M1890AbilityToUseTelephone"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_M1900SelfCareFunctioning][value=' + (result["M1900SelfCareFunctioning"] != null && result["M1900SelfCareFunctioning"] != undefined ? result["M1900SelfCareFunctioning"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1900Ambulation][value=' + (result["M1900Ambulation"] != null && result["M1900Ambulation"] != undefined ? result["M1900Ambulation"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1900Transfer][value=' + (result["M1900Transfer"] != null && result["M1900Transfer"] != undefined ? result["M1900Transfer"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1900HouseHoldTasks][value=' + (result["M1900HouseHoldTasks"] != null && result["M1900HouseHoldTasks"] != undefined ? result["M1900HouseHoldTasks"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M1910FallRiskAssessment][value=' + (result["M1910FallRiskAssessment"] != null && result["M1910FallRiskAssessment"] != undefined ? result["M1910FallRiskAssessment"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2000DrugRegimenReview][value=' + (result["M2000DrugRegimenReview"] != null && result["M2000DrugRegimenReview"] != undefined ? result["M2000DrugRegimenReview"].Answer : "") + ']').attr('checked', true);
            var drugRegimenReview = $('input[name=StartOfCare_M2000DrugRegimenReview]:checked').val();
            if (drugRegimenReview == "00" || drugRegimenReview == "01") {

                $("#soc_M2002").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $('input[name=StartOfCare_M2010PatientOrCaregiverHighRiskDrugEducation][value=' + (result["M2010PatientOrCaregiverHighRiskDrugEducation"] != null && result["M2010PatientOrCaregiverHighRiskDrugEducation"] != undefined ? result["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer : "") + ']').attr('checked', true);
                $('input[name=StartOfCare_M2020ManagementOfOralMedications][value=' + (result["M2020ManagementOfOralMedications"] != null && result["M2020ManagementOfOralMedications"] != undefined ? result["M2020ManagementOfOralMedications"].Answer : "") + ']').attr('checked', true);
                $('input[name=StartOfCare_M2030ManagementOfInjectableMedications][value=' + (result["M2030ManagementOfInjectableMedications"] != null && result["M2030ManagementOfInjectableMedications"] != undefined ? result["M2030ManagementOfInjectableMedications"].Answer : "") + ']').attr('checked', true);
            }
            else if (drugRegimenReview == "NA") {

                $("#soc_M2002").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#soc_M2010").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#soc_M2020").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });
                $("#soc_M2030").block(
                {
                    message: '',
                    overlayCSS: {

                        "cursor": "default"
                    }
                });


            }
            else {
                $('input[name=StartOfCare_M2002MedicationFollowup][value=' + (result["M2002MedicationFollowup"] != null && result["M2002MedicationFollowup"] != undefined ? result["M2002MedicationFollowup"].Answer : "") + ']').attr('checked', true);
                $('input[name=StartOfCare_M2010PatientOrCaregiverHighRiskDrugEducation][value=' + (result["M2010PatientOrCaregiverHighRiskDrugEducation"] != null && result["M2010PatientOrCaregiverHighRiskDrugEducation"] != undefined ? result["M2010PatientOrCaregiverHighRiskDrugEducation"].Answer : "") + ']').attr('checked', true);
                $('input[name=StartOfCare_M2020ManagementOfOralMedications][value=' + (result["M2020ManagementOfOralMedications"] != null && result["M2020ManagementOfOralMedications"] != undefined ? result["M2020ManagementOfOralMedications"].Answer : "") + ']').attr('checked', true);
                $('input[name=StartOfCare_M2030ManagementOfInjectableMedications][value=' + (result["M2030ManagementOfInjectableMedications"] != null && result["M2030ManagementOfInjectableMedications"] != undefined ? result["M2030ManagementOfInjectableMedications"].Answer : "") + ']').attr('checked', true);
            }

            $('input[name=StartOfCare_M2040PriorMedicationOral][value=' + (result["M2040PriorMedicationOral"] != null && result["M2040PriorMedicationOral"] != undefined ? result["M2040PriorMedicationOral"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2040PriorMedicationInject][value=' + (result["M2040PriorMedicationInject"] != null && result["M2040PriorMedicationInject"] != undefined ? result["M2040PriorMedicationInject"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2100ADLAssistance][value=' + (result["M2100ADLAssistance"] != null && result["M2100ADLAssistance"] != undefined ? result["M2100ADLAssistance"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2100IADLAssistance][value=' + (result["M2100IADLAssistance"] != null && result["M2100IADLAssistance"] != undefined ? result["M2100IADLAssistance"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2100MedicationAdministration][value=' + (result["M2100MedicationAdministration"] != null && result["M2100MedicationAdministration"] != undefined ? result["M2100MedicationAdministration"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2100MedicalProcedures][value=' + (result["M2100MedicalProcedures"] != null && result["M2100MedicalProcedures"] != undefined ? result["M2100MedicalProcedures"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2100ManagementOfEquipment][value=' + (result["M2100ManagementOfEquipment"] != null && result["M2100ManagementOfEquipment"] != undefined ? result["M2100ManagementOfEquipment"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2100SupervisionAndSafety][value=' + (result["M2100SupervisionAndSafety"] != null && result["M2100SupervisionAndSafety"] != undefined ? result["M2100SupervisionAndSafety"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2100FacilitationPatientParticipation][value=' + (result["M2100FacilitationPatientParticipation"] != null && result["M2100FacilitationPatientParticipation"] != undefined ? result["M2100FacilitationPatientParticipation"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2110FrequencyOfADLOrIADLAssistance][value=' + (result["M2110FrequencyOfADLOrIADLAssistance"] != null && result["M2110FrequencyOfADLOrIADLAssistance"] != undefined ? result["M2110FrequencyOfADLOrIADLAssistance"].Answer : "") + ']').attr('checked', true);


            var therapyNeed = result["M2200TherapyNeed"];
            if (therapyNeed !== null && therapyNeed != undefined) {
                if (therapyNeed.Answer == 1) {

                    $('input[name=StartOfCare_M2200TherapyNeed][value=1]').attr('checked', true);
                    $("#StartOfCare_M2200NumberOfTherapyNeed").val("");
                }
                else {
                    $('input[name=StartOfCare_M2200TherapyNeed][value=1]').attr('checked', false);
                    $("#StartOfCare_M2200NumberOfTherapyNeed").val(result["M2200NumberOfTherapyNeed"] != null && result["M2200NumberOfTherapyNeed"] != undefined ? result["M2200NumberOfTherapyNeed"].Answer : "");
                }
            }

            $('input[name=StartOfCare_M2250PatientParameters][value=' + (result["M2250PatientParameters"] != null && result["M2250PatientParameters"] != undefined ? result["M2250PatientParameters"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2250DiabeticFoot][value=' + (result["M2250DiabeticFoot"] != null && result["M2250DiabeticFoot"] != undefined ? result["M2250DiabeticFoot"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2250FallsPrevention][value=' + (result["M2250FallsPrevention"] != null && result["M2250FallsPrevention"] != undefined ? result["M2250FallsPrevention"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2250DepressionPrevention][value=' + (result["M2250DepressionPrevention"] != null && result["M2250DepressionPrevention"] != undefined ? result["M2250DepressionPrevention"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2250MonitorMitigatePainIntervention][value=' + (result["M2250MonitorMitigatePainIntervention"] != null && result["M2250MonitorMitigatePainIntervention"] != undefined ? result["M2250MonitorMitigatePainIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2250PressureUlcerIntervention][value=' + (result["M2250PressureUlcerIntervention"] != null && result["M2250PressureUlcerIntervention"] != undefined ? result["M2250PressureUlcerIntervention"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_M2250PressureUlcerTreatment][value=' + (result["M2250PressureUlcerTreatment"] != null && result["M2250PressureUlcerTreatment"] != undefined ? result["M2250PressureUlcerTreatment"].Answer : "") + ']').attr('checked', true);

            $('input[name=StartOfCare_485Allergies][value=' + (result["485Allergies"] != null && result["485Allergies"] != undefined ? result["485Allergies"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_485AllergiesDescription").val(result["485AllergiesDescription"] != null && result["485AllergiesDescription"] != undefined ? result["485AllergiesDescription"].Answer : "");

            $("#StartOfCare_GenericPulseApical").val(result["GenericPulseApical"] != null && result["GenericPulseApical"] != undefined ? result["GenericPulseApical"].Answer : "");
            $('input[name=StartOfCare_GenericPulseApicalRegular][value=' + (result["GenericPulseApicalRegular"] != null && result["GenericPulseApicalRegular"] != undefined ? result["GenericPulseApicalRegular"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_GenericPulseRadial").val(result["GenericPulseRadial"] != null && result["GenericPulseRadial"] != undefined ? result["GenericPulseRadial"].Answer : "");
            $('input[name=StartOfCare_GenericPulseRadialRegular][value=' + (result["GenericPulseRadialRegular"] != null && result["GenericPulseRadialRegular"] != undefined ? result["GenericPulseRadialRegular"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_GenericHeight").val(result["GenericHeight"] != null && result["GenericHeight"] != undefined ? result["GenericHeight"].Answer : "");
            $("#StartOfCare_GenericWeight").val(result["GenericWeight"] != null && result["GenericWeight"] != undefined ? result["GenericWeight"].Answer : "");

            $("#StartOfCare_GenericBPLeftLying").val(result["GenericBPLeftLying"] != null && result["GenericBPLeftLying"] != undefined ? result["GenericBPLeftLying"].Answer : "");
            $("#StartOfCare_GenericBPLeftSitting").val(result["GenericBPLeftSitting"] != null && result["GenericBPLeftSitting"] != undefined ? result["GenericBPLeftSitting"].Answer : "");
            $("#StartOfCare_GenericBPLeftStanding").val(result["GenericBPLeftStanding"] != null && result["GenericBPLeftStanding"] != undefined ? result["GenericBPLeftStanding"].Answer : "");

            $("#StartOfCare_GenericBPRightLying").val(result["GenericBPRightLying"] != null && result["GenericBPRightLying"] != undefined ? result["GenericBPRightLying"].Answer : "");
            $("#StartOfCare_GenericBPRightSitting").val(result["GenericBPRightSitting"] != null && result["GenericBPRightSitting"] != undefined ? result["GenericBPRightSitting"].Answer : "");
            $("#StartOfCare_GenericBPRightStanding").val(result["GenericBPRightStanding"] != null && result["GenericBPRightStanding"] != undefined ? result["GenericBPRightStanding"].Answer : "");

            $("#StartOfCare_GenericTemp").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#StartOfCare_GenericResp").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");
            $('input[name=StartOfCare_GenericVitalSignsActualStated][value=' + (result["GenericVitalSignsActualStated"] != null && result["GenericVitalSignsActualStated"] != undefined ? result["GenericVitalSignsActualStated"].Answer : "") + ']').attr('checked', true);


            $("#StartOfCare_GenericTempGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#StartOfCare_GenericTempLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#StartOfCare_GenericPulseGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#StartOfCare_GenericPulseLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#StartOfCare_GenericRespirationGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#StartOfCare_GenericRespirationLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#StartOfCare_GenericSystolicBPGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#StartOfCare_GenericSystolicBPLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#StartOfCare_GenericDiastolicBPGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#StartOfCare_GenericDiastolicBPLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");


            $("#StartOfCare_Generic02SatLessThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");


            $("#StartOfCare_GenericFastingBloodSugarGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#StartOfCare_GenericFastingBloodSugarLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#StartOfCare_GenericRandomBloddSugarGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#StartOfCare_GenericRandomBloddSugarLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");

            $("#StartOfCare_GenericWeightGreaterThan").val(result["GenericTemp"] != null && result["GenericTemp"] != undefined ? result["GenericTemp"].Answer : "");
            $("#StartOfCare_GenericWeightLessThan").val(result["GenericResp"] != null && result["GenericResp"] != undefined ? result["GenericResp"].Answer : "");


            var medicalHistory = result["GenericMedicalHistory"];
            if (medicalHistory !== null && medicalHistory != undefined && medicalHistory.Answer != null) {
                var medicalHistoryArray = (medicalHistory.Answer).split(',');
                var i = 0;
                for (i = 0; i < medicalHistoryArray.length; i++) {
                    $('input[name=StartOfCare_GenericMedicalHistory][value=' + medicalHistoryArray[i] + ']').attr('checked', true);
                    if (medicalHistoryArray[i] == 10) {
                        $("#StartOfCare_GenericMedicalHistoryCancerType").val((result["GenericMedicalHistoryCancerType"] !== null && result["GenericMedicalHistoryCancerType"] !== undefined ? result["GenericMedicalHistoryCancerType"].Answer : " "));
                        $('input[name=StartOfCare_GenericMedicalHistoryCancerRemission][value=' + (result["GenericMedicalHistoryCancerRemission"] != null && result["GenericMedicalHistoryCancerRemission"] != undefined ? result["GenericMedicalHistoryCancerRemission"].Answer : "") + ']').attr('checked', true);
                    }
                    if (medicalHistoryArray[i] == 11) {
                        $("#StartOfCare_GenericMedicalHistoryOsteoarthritisSites").val((result["GenericMedicalHistoryOsteoarthritisSites"] !== null && result["GenericMedicalHistoryOsteoarthritisSites"] !== undefined ? result["GenericMedicalHistoryOsteoarthritisSites"].Answer : " "));

                    }

                    if (medicalHistoryArray[i] == 16) {
                        $("#StartOfCare_GenericMedicalHistoryJointReplacementLocation").val((result["GenericMedicalHistoryJointReplacementLocation"] !== null && result["GenericMedicalHistoryJointReplacementLocation"] !== undefined ? result["GenericMedicalHistoryJointReplacementLocation"].Answer : " "));

                    }

                    if (medicalHistoryArray[i] == 30) {
                        $("#StartOfCare_GenericMedicalHistoryLiverGallBladderDesc").val((result["GenericMedicalHistoryLiverGallBladderDesc"] !== null && result["GenericMedicalHistoryLiverGallBladderDesc"] !== undefined ? result["GenericMedicalHistoryLiverGallBladderDesc"].Answer : " "));

                    }
                    if (medicalHistoryArray[i] == 35) {
                        $("#StartOfCare_GenericMedicalHistorySubstanceAbuseDesc").val((result["GenericMedicalHistorySubstanceAbuseDesc"] !== null && result["GenericMedicalHistorySubstanceAbuseDesc"] !== undefined ? result["GenericMedicalHistorySubstanceAbuseDesc"].Answer : " "));

                    }
                    if (medicalHistoryArray[i] == 36) {
                        $("#StartOfCare_GenericMedicalHistoryMentalDisorderDesc").val((result["GenericMedicalHistoryMentalDisorderDesc"] !== null && result["GenericMedicalHistoryMentalDisorderDesc"] !== undefined ? result["GenericMedicalHistoryMentalDisorderDesc"].Answer : " "));

                    }
                    if (medicalHistoryArray[i] == 41) {
                        $("#StartOfCare_GenericMedicalHistoryOtherDetails").val((result["GenericMedicalHistoryOtherDetails"] !== null && result["GenericMedicalHistoryOtherDetails"] !== undefined ? result["GenericMedicalHistoryOtherDetails"].Answer : " "));

                    }
                    if (medicalHistoryArray[i] == 60) {
                        $("#StartOfCare_GenericMedicalHistoryHepatitisDetails").val((result["GenericMedicalHistoryHepatitisDetails"] !== null && result["GenericMedicalHistoryHepatitisDetails"] !== undefined ? result["GenericMedicalHistoryHepatitisDetails"].Answer : " "));

                    }

                    if (medicalHistoryArray[i] == 61) {
                        $("#StartOfCare_GenericMedicalHistoryInfectiousDiseaseDetails").val((result["GenericMedicalHistoryInfectiousDiseaseDetails"] !== null && result["GenericMedicalHistoryInfectiousDiseaseDetails"] !== undefined ? result["GenericMedicalHistoryInfectiousDiseaseDetails"].Answer : " "));

                    }

                    if (medicalHistoryArray[i] == 62) {
                        $("#StartOfCare_GenericMedicalHistoryTobaccoDependenceType").val((result["GenericMedicalHistoryTobaccoDependenceType"] !== null && result["GenericMedicalHistoryTobaccoDependenceType"] !== undefined ? result["GenericMedicalHistoryTobaccoDependenceType"].Answer : " "));
                        $("#StartOfCare_GenericMedicalHistoryTobaccoDependenceAmt").val((result["GenericMedicalHistoryTobaccoDependenceAmt"] !== null && result["GenericMedicalHistoryTobaccoDependenceAmt"] !== undefined ? result["GenericMedicalHistoryTobaccoDependenceAmt"].Answer : " "));
                        $("#StartOfCare_GenericMedicalHistoryTobaccoDependenceTypeLength").val((result["GenericMedicalHistoryTobaccoDependenceTypeLength"] !== null && result["GenericMedicalHistoryTobaccoDependenceTypeLength"] !== undefined ? result["GenericMedicalHistoryTobaccoDependenceTypeLength"].Answer : " "));

                    }

                    if (medicalHistoryArray[i] == 65) {
                        $("#StartOfCare_GenericMedicalHistoryExtraDetails").val((result["GenericMedicalHistoryExtraDetails"] !== null && result["GenericMedicalHistoryExtraDetails"] !== undefined ? result["GenericMedicalHistoryExtraDetails"].Answer : " "));

                    }
                    if (medicalHistoryArray[i] == 66) {
                        $("#StartOfCare_GenericMedicalHistoryPastSuguriesDetails").val((result["GenericMedicalHistoryPastSuguriesDetails"] !== null && result["GenericMedicalHistoryPastSuguriesDetails"] !== undefined ? result["GenericMedicalHistoryPastSuguriesDetails"].Answer : " "));

                    }

                }
            }

            $('input[name=StartOfCare_485Pnemonia][value=' + (result["485Pnemonia"] != null && result["485Pnemonia"] != undefined ? result["485Pnemonia"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_485Pnemonia]:checked').val() == 'Yes') {
                $("#StartOfCare_485PnemoniaDate").val((result["485PnemoniaDate"] !== null && result["485PnemoniaDate"] !== undefined ? result["485PnemoniaDate"].Answer : " "));

            }

            $('input[name=StartOfCare_485Flu][value=' + (result["485Flu"] != null && result["485Flu"] != undefined ? result["485Flu"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_485Flu]:checked').val() == 'Yes') {
                $("#StartOfCare_485FluDate").val((result["485FluDate"] !== null && result["485FluDate"] !== undefined ? result["485FluDate"].Answer : " "));

            }

            $('input[name=StartOfCare_485Tetanus][value=' + (result["485Tetanus"] != null && result["485Tetanus"] != undefined ? result["485Tetanus"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_485Tetanus]:checked').val() == 'Yes') {
                $("#StartOfCare_485TetanusDate").val((result["485TetanusDate"] !== null && result["485TetanusDate"] !== undefined ? result["485TetanusDate"].Answer : " "));

            }

            $('input[name=StartOfCare_485TB][value=' + (result["485TB"] != null && result["485TB"] != undefined ? result["485TB"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_485TB]:checked').val() == 'Yes') {
                $("#StartOfCare_485TBDate").val((result["485TBDate"] !== null && result["485TBDate"] !== undefined ? result["485TBDate"].Answer : " "));

            }

            $('input[name=StartOfCare_485TBExposure][value=' + (result["485TBExposure"] != null && result["485TBExposure"] != undefined ? result["485TBExposure"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_485TBExposure]:checked').val() == 'Yes') {
                $("#StartOfCare_485TBExposureDate").val((result["485TBExposureDate"] !== null && result["485TBExposureDate"] !== undefined ? result["485TBExposureDate"].Answer : " "));

            }

            $('input[name=StartOfCare_485HepatitisB][value=' + (result["485HepatitisB"] != null && result["485HepatitisB"] != undefined ? result["485HepatitisB"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_485HepatitisB]:checked').val() == 'Yes') {
                $("#StartOfCare_485HepatitisBDate").val((result["485HepatitisBDate"] !== null && result["485HepatitisBDate"] !== undefined ? result["485HepatitisBDate"].Answer : " "));

            }

            $("#StartOfCare_485AdditionalImmunization1Name").val((result["485AdditionalImmunization1Name"] !== null && result["485AdditionalImmunization1Name"] !== undefined ? result["485AdditionalImmunization1Name"].Answer : " "));
            $('input[name=StartOfCare_485AdditionalImmunization1][value=' + (result["485AdditionalImmunization1"] != null && result["485AdditionalImmunization1"] != undefined ? result["485AdditionalImmunization1"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_485AdditionalImmunization1]:checked').val() == 'Yes') {
                $("#StartOfCare_485AdditionalImmunization1Date").val((result["485AdditionalImmunization1Date"] !== null && result["485AdditionalImmunization1Date"] !== undefined ? result["485AdditionalImmunization1Date"].Answer : " "));

            }

            $("#StartOfCare_485AdditionalImmunization2Name").val((result["485AdditionalImmunization2Name"] !== null && result["485AdditionalImmunization2Name"] !== undefined ? result["485AdditionalImmunization2Name"].Answer : " "));
            $('input[name=StartOfCare_485AdditionalImmunization2][value=' + (result["485AdditionalImmunization2"] != null && result["485AdditionalImmunization2"] != undefined ? result["485AdditionalImmunization2"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_485AdditionalImmunization2]:checked').val() == 'Yes') {
                $("#StartOfCare_485AdditionalImmunization2Date").val((result["485AdditionalImmunization2Date"] !== null && result["485AdditionalImmunization2Date"] !== undefined ? result["485AdditionalImmunization2Date"].Answer : " "));

            }

            $("#StartOfCare_485ImmunizationComments").val((result["485ImmunizationComments"] !== null && result["485ImmunizationComments"] !== undefined ? result["485ImmunizationComments"].Answer : " "));

            $("#StartOfCare_485LastCholesterolLevel").val((result["485LastCholesterolLevel"] !== null && result["485LastCholesterolLevel"] !== undefined ? result["485LastCholesterolLevel"].Answer : " "));
            $("#StartOfCare_485LastMammogram").val((result["485LastMammogram"] !== null && result["485LastMammogram"] !== undefined ? result["485LastMammogram"].Answer : " "));
            $('input[name=StartOfCare_485PerformMonthlySelfBreastExams][value=' + (result["485PerformMonthlySelfBreastExams"] != null && result["485PerformMonthlySelfBreastExams"] != undefined ? result["485PerformMonthlySelfBreastExams"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_485LastPapSmear").val((result["485LastPapSmear"] !== null && result["485LastPapSmear"] !== undefined ? result["485LastPapSmear"].Answer : " "));
            $("#StartOfCare_485LastPSA").val((result["485LastPSA"] !== null && result["485LastPSA"] !== undefined ? result["485LastPSA"].Answer : " "));
            $("#StartOfCare_485LastProstateExam").val((result["485LastProstateExam"] !== null && result["485LastProstateExam"] !== undefined ? result["485LastProstateExam"].Answer : " "));
            $("#StartOfCare_485LastColonoscopy").val((result["485LastColonoscopy"] !== null && result["485LastColonoscopy"] !== undefined ? result["485LastColonoscopy"].Answer : " "));

            var riskAssessmentIntervention = result["485RiskAssessmentIntervention"];
            if (riskAssessmentIntervention !== null && riskAssessmentIntervention != undefined && riskAssessmentIntervention.Answer != null) {
                var riskAssessmentInterventionArray = (riskAssessmentIntervention.Answer).split(',');
                var i = 0;
                for (i = 0; i < riskAssessmentInterventionArray.length; i++) {
                    $('input[name=StartOfCare_485RiskAssessmentIntervention][value=' + riskAssessmentInterventionArray[i] + ']').attr('checked', true);
                    if (riskAssessmentInterventionArray[i] == 4) {
                        $("#StartOfCare_485AdministerVaccinationDetails").val((result["485AdministerVaccinationDetails"] !== null && result["485AdministerVaccinationDetails"] !== undefined ? result["485AdministerVaccinationDetails"].Answer : " "));
                    }
                    if (riskAssessmentInterventionArray[i] == 5) {
                        $("#StartOfCare_485AdministerPneumococcalVaccineDetails").val((result["485AdministerPneumococcalVaccineDetails"] !== null && result["485AdministerPneumococcalVaccineDetails"] !== undefined ? result["485AdministerPneumococcalVaccineDetails"].Answer : " "));

                    }
                }
            }

            $("#StartOfCare_485RiskInterventionTemplates").val(result["485RiskInterventionTemplates"] != null && result["485RiskInterventionTemplates"] != undefined ? result["485RiskInterventionTemplates"].Answer : "");
            $("#StartOfCare_485RiskInterventionComments").val((result["485RiskInterventionComments"] !== null && result["485RiskInterventionComments"] !== undefined ? result["485RiskInterventionComments"].Answer : " "));

            var riskAssessmentGoals = result["485RiskAssessmentGoals"];
            if (riskAssessmentGoals !== null && riskAssessmentGoals != undefined && riskAssessmentGoals.Answer != null) {
                var riskAssessmentGoalsArray = (riskAssessmentGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < riskAssessmentGoalsArray.length; i++) {
                    $('input[name=StartOfCare_485RiskAssessmentGoals][value=' + riskAssessmentGoalsArray[i] + ']').attr('checked', true);
                    if (riskAssessmentGoalsArray[i] == 2) {
                        $("#StartOfCare_485VerbalizeEmergencyPlanPerson").val((result["485VerbalizeEmergencyPlanPerson"] !== null && result["485VerbalizeEmergencyPlanPerson"] !== undefined ? result["485VerbalizeEmergencyPlanPerson"].Answer : " "));
                        $("#StartOfCare_485VerbalizeEmergencyPlanDate").val((result["485VerbalizeEmergencyPlanDate"] !== null && result["485VerbalizeEmergencyPlanDate"] !== undefined ? result["485VerbalizeEmergencyPlanDate"].Answer : " "));
                    }

                }
            }

            $("#StartOfCare_485RiskGoalTemplates").val(result["485RiskGoalTemplates"] != null && result["485RiskGoalTemplates"] != undefined ? result["485RiskGoalTemplates"].Answer : "");
            $("#StartOfCare_485RiskGoalComments").val((result["485RiskGoalComments"] !== null && result["485RiskGoalComments"] !== undefined ? result["485RiskGoalComments"].Answer : " "));

            $('input[name=StartOfCare_485Prognosis][value=' + (result["485Prognosis"] != null && result["485Prognosis"] != undefined ? result["485Prognosis"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_485AdvancedDirectives][value=' + (result["485AdvancedDirectives"] != null && result["485AdvancedDirectives"] != undefined ? result["485AdvancedDirectives"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_485AdvancedDirectivesIntent][value=' + (result["485AdvancedDirectivesIntent"] != null && result["485AdvancedDirectivesIntent"] != undefined ? result["485AdvancedDirectivesIntent"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_485AdvancedDirectivesIntent]:checked').val() == 'Other') {
                $("#StartOfCare_485AdvancedDirectivesIntentOther").val((result["485AdvancedDirectivesIntentOther"] !== null && result["485AdvancedDirectivesIntentOther"] !== undefined ? result["485AdvancedDirectivesIntentOther"].Answer : " "));
            }

            $('input[name=StartOfCare_485AdvancedDirectivesCopyOnFile][value=' + (result["485AdvancedDirectivesCopyOnFile"] != null && result["485AdvancedDirectivesCopyOnFile"] != undefined ? result["485AdvancedDirectivesCopyOnFile"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_485AdvancedDirectivesWrittenAndVerbal][value=' + (result["485AdvancedDirectivesWrittenAndVerbal"] != null && result["485AdvancedDirectivesWrittenAndVerbal"] != undefined ? result["485AdvancedDirectivesWrittenAndVerbal"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericPatientDNR][value=' + (result["GenericPatientDNR"] != null && result["GenericPatientDNR"] != undefined ? result["GenericPatientDNR"].Answer : "") + ']').attr('checked', true);

            var functionLimitations = result["485FunctionLimitations"];
            if (functionLimitations !== null && functionLimitations != undefined && functionLimitations.Answer != null) {
                var functionLimitationsArray = (functionLimitations.Answer).split(',');
                var i = 0;
                for (i = 0; i < functionLimitationsArray.length; i++) {
                    $('input[name=StartOfCare_485FunctionLimitations][value=' + functionLimitationsArray[i] + ']').attr('checked', true);

                    if (functionLimitationsArray[i] == "B") {

                        $("#StartOfCare_485FunctionLimitationsOther").val((result["485FunctionLimitationsOther"] !== null && result["485FunctionLimitationsOther"] !== undefined ? result["485FunctionLimitationsOther"].Answer : " "));
                    }
                }
            }

            $('input[name=StartOfCare_M1100LivingSituation][value=' + (result["M1100LivingSituation"] != null && result["M1100LivingSituation"] != undefined ? result["M1100LivingSituation"].Answer : "") + ']').attr('checked', true);
            var ADLTypeOfAssistance = result["GenericADLTypeOfAssistance"];
            if (ADLTypeOfAssistance !== null && ADLTypeOfAssistance != undefined && ADLTypeOfAssistance.Answer != null) {
                var ADLTypeOfAssistanceArray = (ADLTypeOfAssistance.Answer).split(',');
                var i = 0;
                for (i = 0; i < ADLTypeOfAssistanceArray.length; i++) {
                    $('input[name=StartOfCare_GenericADLTypeOfAssistance][value=' + ADLTypeOfAssistanceArray[i] + ']').attr('checked', true);
                }
            }

            var IADLTypeOfAssistance = result["GenericIADLTypeOfAssistance"];
            if (IADLTypeOfAssistance !== null && IADLTypeOfAssistance != undefined && IADLTypeOfAssistance.Answer != null) {
                var IADLTypeOfAssistanceArray = (IADLTypeOfAssistance.Answer).split(',');
                var i = 0;
                for (i = 0; i < IADLTypeOfAssistanceArray.length; i++) {
                    $('input[name=StartOfCare_GenericIADLTypeOfAssistance][value=' + IADLTypeOfAssistanceArray[i] + ']').attr('checked', true);
                }
            }

            var psychoSupportTypeOfAssistance = result["GenericPsychoSupportTypeOfAssistance"];
            if (psychoSupportTypeOfAssistance !== null && psychoSupportTypeOfAssistance != undefined && psychoSupportTypeOfAssistance.Answer != null) {
                var psychoSupportTypeOfAssistanceArray = (psychoSupportTypeOfAssistance.Answer).split(',');
                var i = 0;
                for (i = 0; i < psychoSupportTypeOfAssistanceArray.length; i++) {
                    $('input[name=StartOfCare_GenericPsychoSupportTypeOfAssistance][value=' + psychoSupportTypeOfAssistanceArray[i] + ']').attr('checked', true);
                }
            }

            var medicalApptTypeOfAssistance = result["GenericMedicalApptTypeOfAssistance"];
            if (medicalApptTypeOfAssistance !== null && medicalApptTypeOfAssistance != undefined && medicalApptTypeOfAssistance.Answer != null) {
                var medicalApptTypeOfAssistanceArray = (medicalApptTypeOfAssistance.Answer).split(',');
                var i = 0;
                for (i = 0; i < medicalApptTypeOfAssistanceArray.length; i++) {
                    $('input[name=StartOfCare_GenericMedicalApptTypeOfAssistance][value=' + medicalApptTypeOfAssistanceArray[i] + ']').attr('checked', true);
                }
            }

            var financeManagmentTypeOfAssistance = result["GenericFinanceManagmentTypeOfAssistance"];
            if (financeManagmentTypeOfAssistance !== null && financeManagmentTypeOfAssistance != undefined && financeManagmentTypeOfAssistance.Answer != null) {
                var financeManagmentTypeOfAssistanceArray = (financeManagmentTypeOfAssistance.Answer).split(',');
                var i = 0;
                for (i = 0; i < financeManagmentTypeOfAssistanceArray.length; i++) {
                    $('input[name=StartOfCare_GenericFinanceManagmentTypeOfAssistance][value=' + financeManagmentTypeOfAssistanceArray[i] + ']').attr('checked', true);
                }
            }

            $("#StartOfCare_GenericTypeOfAssistanceComments").val((result["GenericTypeOfAssistanceComments"] !== null && result["GenericTypeOfAssistanceComments"] !== undefined ? result["GenericTypeOfAssistanceComments"].Answer : " "));
            $("#StartOfCare_GenericOrgProvidingAssistanceNames").val((result["GenericOrgProvidingAssistanceNames"] !== null && result["GenericOrgProvidingAssistanceNames"] !== undefined ? result["GenericOrgProvidingAssistanceNames"].Answer : " "));

            $('input[name=StartOfCare_GenericCommunityResourceInfoNeeded][value=' + (result["GenericCommunityResourceInfoNeeded"] != null && result["GenericCommunityResourceInfoNeeded"] != undefined ? result["GenericCommunityResourceInfoNeeded"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericAbilityHandleFinance][value=' + (result["GenericAbilityHandleFinance"] != null && result["GenericAbilityHandleFinance"] != undefined ? result["GenericAbilityHandleFinance"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericAlteredAffect][value=' + (result["GenericAlteredAffect"] != null && result["GenericAlteredAffect"] != undefined ? result["GenericAlteredAffect"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_GenericAlteredAffectComments").val((result["GenericAlteredAffectComments"] !== null && result["GenericAlteredAffectComments"] !== undefined ? result["GenericAlteredAffectComments"].Answer : " "));
            $('input[name=StartOfCare_GenericSuicidalIdeation][value=' + (result["GenericSuicidalIdeation"] != null && result["GenericSuicidalIdeation"] != undefined ? result["GenericSuicidalIdeation"].Answer : "") + ']').attr('checked', true);
            var suspected = result["GenericSuspected"];
            if (suspected !== null && suspected != undefined && suspected.Answer != null) {
                var suspectedArray = (suspected.Answer).split(',');
                var i = 0;
                for (i = 0; i < suspectedArray.length; i++) {
                    $('input[name=StartOfCare_GenericSuspected][value=' + suspectedArray[i] + ']').attr('checked', true);
                }
            }

            $("#StartOfCare_GenericMSWIndicatedForWhat").val((result["GenericMSWIndicatedForWhat"] !== null && result["GenericMSWIndicatedForWhat"] !== undefined ? result["GenericMSWIndicatedForWhat"].Answer : " "));
            $('input[name=StartOfCare_GenericMSWIndicatedFor][value=' + (result["GenericMSWIndicatedFor"] != null && result["GenericMSWIndicatedFor"] != undefined ? result["GenericMSWIndicatedFor"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericCoordinatorNotified][value=' + (result["GenericCoordinatorNotified"] != null && result["GenericCoordinatorNotified"] != undefined ? result["GenericCoordinatorNotified"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericNoHazardsIdentified][value=' + (result["GenericNoHazardsIdentified"] != null && result["GenericNoHazardsIdentified"] != undefined ? result["GenericNoHazardsIdentified"].Answer : "") + ']').attr('checked', true);
            var hazardsIdentified = result["GenericHazardsIdentified"];
            if (hazardsIdentified !== null && hazardsIdentified != undefined && hazardsIdentified.Answer != null) {
                var hazardsIdentifiedArray = (hazardsIdentified.Answer).split(',');
                var i = 0;
                for (i = 0; i < hazardsIdentifiedArray.length; i++) {
                    $('input[name=StartOfCare_GenericHazardsIdentified][value=' + hazardsIdentifiedArray[i] + ']').attr('checked', true);

                }
            }

            $("#StartOfCare_GenericOtherHazards").val((result["GenericOtherHazards"] !== null && result["GenericOtherHazards"] !== undefined ? result["GenericOtherHazards"].Answer : " "));
            $("#StartOfCare_GenericHazardsComments").val((result["GenericHazardsComments"] !== null && result["GenericHazardsComments"] !== undefined ? result["GenericHazardsComments"].Answer : " "));

            $('input[name=StartOfCare_GenericUsingOxygen][value=' + (result["GenericUsingOxygen"] != null && result["GenericUsingOxygen"] != undefined ? result["GenericUsingOxygen"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericNoSmokingSigns][value=' + (result["GenericNoSmokingSigns"] != null && result["GenericNoSmokingSigns"] != undefined ? result["GenericNoSmokingSigns"].Answer : "") + ']').attr('checked', true);
            var noSmokingSignsPerson = result["GenericNoSmokingSignsPerson"];
            if (noSmokingSignsPerson !== null && noSmokingSignsPerson != undefined && noSmokingSignsPerson.Answer != null) {
                var noSmokingSignsPersonArray = (noSmokingSignsPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < noSmokingSignsPersonArray.length; i++) {
                    $('input[name=StartOfCare_GenericNoSmokingSignsPerson][value=' + noSmokingSignsPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=StartOfCare_GenericSmokeWithOxygenInUser][value=' + (result["GenericSmokeWithOxygenInUser"] != null && result["GenericSmokeWithOxygenInUser"] != undefined ? result["GenericSmokeWithOxygenInUser"].Answer : "") + ']').attr('checked', true);
            var smokeWithOxygenInUserPerson = result["GenericSmokeWithOxygenInUserPerson"];
            if (smokeWithOxygenInUserPerson !== null && smokeWithOxygenInUserPerson != undefined && smokeWithOxygenInUserPerson.Answer != null) {
                var smokeWithOxygenInUserPersonArray = (smokeWithOxygenInUserPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < smokeWithOxygenInUserPersonArray.length; i++) {
                    $('input[name=StartOfCare_GenericSmokeWithOxygenInUserPerson][value=' + smokeWithOxygenInUserPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=StartOfCare_GenericSmokeDetectorsWorking][value=' + (result["GenericSmokeDetectorsWorking"] != null && result["GenericSmokeDetectorsWorking"] != undefined ? result["GenericSmokeDetectorsWorking"].Answer : "") + ']').attr('checked', true);
            var smokeDetectorsWorkingPerson = result["GenericSmokeDetectorsWorkingPerson"];
            if (smokeDetectorsWorkingPerson !== null && smokeDetectorsWorkingPerson != undefined && smokeDetectorsWorkingPerson.Answer != null) {
                var smokeDetectorsWorkingPersonArray = (smokeDetectorsWorkingPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < smokeDetectorsWorkingPersonArray.length; i++) {
                    $('input[name=StartOfCare_GenericSmokeDetectorsWorkingPerson][value=' + smokeDetectorsWorkingPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=StartOfCare_GenericFireExtinguisherWorking][value=' + (result["GenericFireExtinguisherWorking"] != null && result["GenericFireExtinguisherWorking"] != undefined ? result["GenericFireExtinguisherWorking"].Answer : "") + ']').attr('checked', true);
            var fireExtinguisherWorkingPerson = result["GenericFireExtinguisherWorkingPerson"];
            if (fireExtinguisherWorkingPerson !== null && fireExtinguisherWorkingPerson != undefined && fireExtinguisherWorkingPerson.Answer != null) {
                var fireExtinguisherWorkingPersonArray = (fireExtinguisherWorkingPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < fireExtinguisherWorkingPersonArray.length; i++) {
                    $('input[name=StartOfCare_GenericFireExtinguisherWorkingPerson][value=' + fireExtinguisherWorkingPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=StartOfCare_GenericOxygenCyclindersSafe][value=' + (result["GenericOxygenCyclindersSafe"] != null && result["GenericOxygenCyclindersSafe"] != undefined ? result["GenericOxygenCyclindersSafe"].Answer : "") + ']').attr('checked', true);
            var oxygenCyclindersSafePerson = result["GenericOxygenCyclindersSafePerson"];
            if (oxygenCyclindersSafePerson !== null && oxygenCyclindersSafePerson != undefined && oxygenCyclindersSafePerson.Answer != null) {
                var oxygenCyclindersSafePersonArray = (oxygenCyclindersSafePerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < oxygenCyclindersSafePersonArray.length; i++) {
                    $('input[name=StartOfCare_GenericOxygenCyclindersSafePerson][value=' + oxygenCyclindersSafePersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=StartOfCare_GenericElectricalCordsIntact][value=' + (result["GenericElectricalCordsIntact"] != null && result["GenericElectricalCordsIntact"] != undefined ? result["GenericElectricalCordsIntact"].Answer : "") + ']').attr('checked', true);
            var electricalCordsIntactPerson = result["GenericElectricalCordsIntactPerson"];
            if (electricalCordsIntactPerson !== null && electricalCordsIntactPerson != undefined && electricalCordsIntactPerson.Answer != null) {
                var electricalCordsIntactPersonArray = (electricalCordsIntactPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < electricalCordsIntactPersonArray.length; i++) {
                    $('input[name=StartOfCare_GenericElectricalCordsIntactPerson][value=' + electricalCordsIntactPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=StartOfCare_GenericEvacuationPlan][value=' + (result["GenericEvacuationPlan"] != null && result["GenericEvacuationPlan"] != undefined ? result["GenericEvacuationPlan"].Answer : "") + ']').attr('checked', true);
            var evacuationPlanPerson = result["GenericEvacuationPlanPerson"];
            if (evacuationPlanPerson !== null && evacuationPlanPerson != undefined && evacuationPlanPerson.Answer != null) {
                var evacuationPlanPersonArray = (evacuationPlanPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < evacuationPlanPersonArray.length; i++) {
                    $('input[name=StartOfCare_GenericEvacuationPlanPerson][value=' + evacuationPlanPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=StartOfCare_GenericAerosolsSafeWhenOxygenInUse][value=' + (result["GenericAerosolsSafeWhenOxygenInUse"] != null && result["GenericAerosolsSafeWhenOxygenInUse"] != undefined ? result["GenericAerosolsSafeWhenOxygenInUse"].Answer : "") + ']').attr('checked', true);
            var aerosolsSafeWhenOxygenInUsePerson = result["GenericAerosolsSafeWhenOxygenInUsePerson"];
            if (aerosolsSafeWhenOxygenInUsePerson !== null && aerosolsSafeWhenOxygenInUsePerson != undefined && aerosolsSafeWhenOxygenInUsePerson.Answer != null) {
                var aerosolsSafeWhenOxygenInUsePersonArray = (aerosolsSafeWhenOxygenInUsePerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < aerosolsSafeWhenOxygenInUsePersonArray.length; i++) {
                    $('input[name=StartOfCare_GenericAerosolsSafeWhenOxygenInUsePerson][value=' + aerosolsSafeWhenOxygenInUsePersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=StartOfCare_GenericPetroleumNearOxygen][value=' + (result["GenericPetroleumNearOxygen"] != null && result["GenericPetroleumNearOxygen"] != undefined ? result["GenericPetroleumNearOxygen"].Answer : "") + ']').attr('checked', true);
            var petroleumNearOxygenPerson = result["GenericPetroleumNearOxygenPerson"];
            if (petroleumNearOxygenPerson !== null && petroleumNearOxygenPerson != undefined && petroleumNearOxygenPerson.Answer != null) {
                var petroleumNearOxygenPersonArray = (petroleumNearOxygenPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < petroleumNearOxygenPersonArray.length; i++) {
                    $('input[name=StartOfCare_GenericPetroleumNearOxygenPerson][value=' + petroleumNearOxygenPersonArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=StartOfCare_GenericWaterbasedBodyLotion][value=' + (result["GenericWaterbasedBodyLotion"] != null && result["GenericWaterbasedBodyLotion"] != undefined ? result["GenericWaterbasedBodyLotion"].Answer : "") + ']').attr('checked', true);
            var waterbasedBodyLotionPerson = result["GenericWaterbasedBodyLotionPerson"];
            if (waterbasedBodyLotionPerson !== null && waterbasedBodyLotionPerson != undefined && waterbasedBodyLotionPerson.Answer != null) {
                var waterbasedBodyLotionPersonArray = (waterbasedBodyLotionPerson.Answer).split(',');
                var i = 0;
                for (i = 0; i < waterbasedBodyLotionPersonArray.length; i++) {
                    $('input[name=StartOfCare_GenericWaterbasedBodyLotionPerson][value=' + waterbasedBodyLotionPersonArray[i] + ']').attr('checked', true);

                }
            }

            var safetyMeasures = result["485SafetyMeasures"];
            if (safetyMeasures !== null && safetyMeasures != undefined && safetyMeasures.Answer != null) {
                var safetyMeasuresArray = (safetyMeasures.Answer).split(',');
                var i = 0;
                for (i = 0; i < safetyMeasuresArray.length; i++) {
                    $('input[name=StartOfCare_485SafetyMeasures][value=' + safetyMeasuresArray[i] + ']').attr('checked', true);

                }
            }

            $("#StartOfCare_485OtherSafetyMeasures").val((result["485OtherSafetyMeasures"] !== null && result["485OtherSafetyMeasures"] !== undefined ? result["485OtherSafetyMeasures"].Answer : " "));
            $("#StartOfCare_485TriageRiskCode").val((result["485TriageRiskCode"] !== null && result["485TriageRiskCode"] !== undefined ? result["485TriageRiskCode"].Answer : " "));
            $("#StartOfCare_485DisasterCode").val((result["485DisasterCode"] !== null && result["485DisasterCode"] !== undefined ? result["485DisasterCode"].Answer : " "));
            $("#StartOfCare_485SafetyMeasuresComments").val((result["485SafetyMeasuresComments"] !== null && result["485SafetyMeasuresComments"] !== undefined ? result["485SafetyMeasuresComments"].Answer : " "));

            $("#StartOfCare_GenericPrimaryLanguage").val((result["GenericPrimaryLanguage"] !== null && result["GenericPrimaryLanguage"] !== undefined ? result["GenericPrimaryLanguage"].Answer : " "));
            $('input[name=StartOfCare_GenericCulturalPractices][value=' + (result["GenericCulturalPractices"] != null && result["GenericCulturalPractices"] != undefined ? result["GenericCulturalPractices"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_GenericCulturalPracticesDetails").val((result["GenericCulturalPracticesDetails"] !== null && result["GenericCulturalPracticesDetails"] !== undefined ? result["GenericCulturalPracticesDetails"].Answer : " "));
            $('input[name=StartOfCare_GenericReligionImportant][value=' + (result["GenericReligionImportant"] != null && result["GenericReligionImportant"] != undefined ? result["GenericReligionImportant"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_GenericReligiousPreference").val((result["GenericReligiousPreference"] !== null && result["GenericReligiousPreference"] !== undefined ? result["GenericReligiousPreference"].Answer : " "));

            var useOfInterpreter = result["GenericUseOfInterpreter"];
            if (useOfInterpreter !== null && useOfInterpreter != undefined && useOfInterpreter.Answer != null) {
                var useOfInterpreterArray = (useOfInterpreter.Answer).split(',');
                var i = 0;
                for (i = 0; i < useOfInterpreterArray.length; i++) {
                    $('input[name=StartOfCare_GenericUseOfInterpreter][value=' + useOfInterpreterArray[i] + ']').attr('checked', true);
                    if (useOfInterpreterArray[i] == 4) {
                        $("#StartOfCare_GenericUseOfInterpreterOtherDetails").val((result["GenericUseOfInterpreterOtherDetails"] !== null && result["GenericUseOfInterpreterOtherDetails"] !== undefined ? result["GenericUseOfInterpreterOtherDetails"].Answer : " "));

                    }
                }
            }

            $("#StartOfCare_GenericPrimaryEmotionalSupport").val((result["GenericPrimaryEmotionalSupport"] !== null && result["GenericPrimaryEmotionalSupport"] !== undefined ? result["GenericPrimaryEmotionalSupport"].Answer : " "));
            $('input[name=StartOfCare_GenericIsHomeBound][value=' + (result["GenericIsHomeBound"] != null && result["GenericIsHomeBound"] != undefined ? result["GenericIsHomeBound"].Answer : "") + ']').attr('checked', true);
            var homeBoundReason = result["GenericHomeBoundReason"];
            if (homeBoundReason !== null && homeBoundReason != undefined && homeBoundReason.Answer != null) {
                var homeBoundReasonArray = (homeBoundReason.Answer).split(',');
                var i = 0;
                for (i = 0; i < homeBoundReasonArray.length; i++) {
                    $('input[name=StartOfCare_GenericHomeBoundReason][value=' + homeBoundReasonArray[i] + ']').attr('checked', true);
                    if (homeBoundReasonArray[i] == 6) {
                        $("#StartOfCare_GenericOtherHomeBoundDetails").val((result["GenericOtherHomeBoundDetails"] !== null && result["GenericOtherHomeBoundDetails"] !== undefined ? result["GenericOtherHomeBoundDetails"].Answer : " "));

                    }
                }
            }

            var eyes = result["GenericEyes"];
            if (eyes !== null && eyes != undefined && eyes.Answer != null) {
                var eyesArray = (eyes.Answer).split(',');
                var i = 0;
                for (i = 0; i < eyesArray.length; i++) {
                    $('input[name=StartOfCare_GenericEyes][value=' + eyesArray[i] + ']').attr('checked', true);
                    if (eyesArray[i] == 13) {
                        $("#StartOfCare_GenericEyesOtherDetails").val((result["GenericEyesOtherDetails"] !== null && result["GenericEyesOtherDetails"] !== undefined ? result["GenericEyesOtherDetails"].Answer : " "));

                    }
                }
            }
            $("#StartOfCare_GenericEyesLastEyeExamDate").val((result["GenericEyesLastEyeExamDate"] !== null && result["GenericEyesLastEyeExamDate"] !== undefined ? result["GenericEyesLastEyeExamDate"].Answer : " "));

            var ears = result["GenericEars"];
            if (ears !== null && ears != undefined && ears.Answer != null) {
                var earsArray = (ears.Answer).split(',');
                var i = 0;
                for (i = 0; i < earsArray.length; i++) {
                    $('input[name=StartOfCare_GenericEars][value=' + earsArray[i] + ']').attr('checked', true);
                    if (earsArray[i] == 2) {

                        var earsHearingImpairedPosition = result["GenericEarsHearingImpairedPosition"];
                        if (earsHearingImpairedPosition !== null && earsHearingImpairedPosition != undefined && earsHearingImpairedPosition.Answer != null) {
                            var earsHearingImpairedPositionArray = (earsHearingImpairedPosition.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < earsHearingImpairedPositionArray.length; j++) {
                                $('input[name=StartOfCare_GenericEarsHearingImpairedPosition][value=' + earsHearingImpairedPositionArray[j] + ']').attr('checked', true);
                            }
                        }
                    }
                    if (earsArray[i] == 6) {

                        var earsHearingAidsPosition = result["GenericEarsHearingAidsPosition"];
                        if (earsHearingAidsPosition !== null && earsHearingAidsPosition != undefined && earsHearingAidsPosition.Answer != null) {
                            var earsHearingAidsPositionArray = (earsHearingAidsPosition.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < earsHearingAidsPositionArray.length; j++) {
                                $('input[name=StartOfCare_GenericEarsHearingAidsPosition][value=' + earsHearingAidsPositionArray[j] + ']').attr('checked', true);
                            }
                        }
                    }

                }
            }

            var nose = result["GenericNose"];
            if (nose !== null && nose != undefined && nose.Answer != null) {
                var noseArray = (nose.Answer).split(',');
                var i = 0;
                for (i = 0; i < noseArray.length; i++) {
                    $('input[name=StartOfCare_GenericNose][value=' + noseArray[i] + ']').attr('checked', true);
                    if (noseArray[i] == 4) {
                        $("#StartOfCare_GenericNoseBleedsFrequency").val((result["GenericNoseBleedsFrequency"] !== null && result["GenericNoseBleedsFrequency"] !== undefined ? result["GenericNoseBleedsFrequency"].Answer : " "));

                    }
                    if (noseArray[i] == 5) {
                        $("#StartOfCare_GenericNoseOtherDetails").val((result["GenericNoseOtherDetails"] !== null && result["GenericNoseOtherDetails"] !== undefined ? result["GenericNoseOtherDetails"].Answer : " "));

                    }
                }
            }

            var sensoryStatusIntervention = result["485SensoryStatusIntervention"];
            if (sensoryStatusIntervention !== null && sensoryStatusIntervention != undefined && sensoryStatusIntervention.Answer != null) {
                var sensoryStatusInterventionArray = (sensoryStatusIntervention.Answer).split(',');
                var i = 0;
                for (i = 0; i < sensoryStatusInterventionArray.length; i++) {
                    $('input[name=StartOfCare_485SensoryStatusIntervention][value=' + sensoryStatusInterventionArray[i] + ']').attr('checked', true);
                    if (sensoryStatusInterventionArray[i] == 1) {
                        $("#StartOfCare_485AdministerEarMedicationOrder").val((result["485AdministerEarMedicationOrder"] !== null && result["485AdministerEarMedicationOrder"] !== undefined ? result["485AdministerEarMedicationOrder"].Answer : " "));
                    }
                    if (sensoryStatusInterventionArray[i] == 2) {
                        $("#StartOfCare_485InstillOphathalmicMedicationOrders").val((result["485InstillOphathalmicMedicationOrders"] !== null && result["485InstillOphathalmicMedicationOrders"] !== undefined ? result["485InstillOphathalmicMedicationOrders"].Answer : " "));

                    }
                    if (sensoryStatusInterventionArray[i] == 3) {
                        $("#StartOfCare_485STEvaluateSensoryStatusFrequency").val((result["485STEvaluateSensoryStatusFrequency"] !== null && result["485STEvaluateSensoryStatusFrequency"] !== undefined ? result["485STEvaluateSensoryStatusFrequency"].Answer : " "));
                        $("#StartOfCare_485STEvaluateSensoryStatusEvalDate").val((result["485STEvaluateSensoryStatusEvalDate"] !== null && result["485STEvaluateSensoryStatusEvalDate"] !== undefined ? result["485STEvaluateSensoryStatusEvalDate"].Answer : " "));

                    }
                }
            }

            $("#StartOfCare_485SensoryStatusOrderTemplates").val(result["485SensoryStatusOrderTemplates"] != null && result["485SensoryStatusOrderTemplates"] != undefined ? result["485SensoryStatusOrderTemplates"].Answer : "");
            $("#StartOfCare_485SensoryStatusInterventionComments").val((result["485SensoryStatusInterventionComments"] !== null && result["485SensoryStatusInterventionComments"] !== undefined ? result["485SensoryStatusInterventionComments"].Answer : " "));

            $("#StartOfCare_485SensoryStatusGoalTemplates").val(result["485SensoryStatusGoalTemplates"] != null && result["485SensoryStatusGoalTemplates"] != undefined ? result["485SensoryStatusGoalTemplates"].Answer : "");
            $("#StartOfCare_485SensoryStatusGoalComments").val((result["485SensoryStatusGoalComments"] !== null && result["485SensoryStatusGoalComments"] !== undefined ? result["485SensoryStatusGoalComments"].Answer : " "));

            $("#StartOfCare_GenericPainOnSetDate").val(result["GenericPainOnSetDate"] != null && result["GenericPainOnSetDate"] != undefined ? result["GenericPainOnSetDate"].Answer : "");
            $("#StartOfCare_GenericLocationOfPain").val((result["GenericLocationOfPain"] !== null && result["GenericLocationOfPain"] !== undefined ? result["GenericLocationOfPain"].Answer : " "));
            $("#StartOfCare_GenericIntensityOfPain").val((result["GenericIntensityOfPain"] !== null && result["GenericIntensityOfPain"] !== undefined ? result["GenericIntensityOfPain"].Answer : " "));
            $("#StartOfCare_GenericDurationOfPain").val((result["GenericDurationOfPain"] !== null && result["GenericDurationOfPain"] !== undefined ? result["GenericDurationOfPain"].Answer : " "));

            $("#StartOfCare_GenericQualityOfPain").val(result["GenericQualityOfPain"] != null && result["GenericQualityOfPain"] != undefined ? result["GenericQualityOfPain"].Answer : "");
            $("#StartOfCare_GenericWhatMakesPainWorse").val((result["GenericWhatMakesPainWorse"] !== null && result["GenericWhatMakesPainWorse"] !== undefined ? result["GenericWhatMakesPainWorse"].Answer : " "));
            $("#StartOfCare_GenericWhatMakesPainBetter").val((result["GenericWhatMakesPainBetter"] !== null && result["GenericWhatMakesPainBetter"] !== undefined ? result["GenericWhatMakesPainBetter"].Answer : " "));
            $("#StartOfCare_GenericReliefRatingOfPain").val((result["GenericReliefRatingOfPain"] !== null && result["GenericReliefRatingOfPain"] !== undefined ? result["GenericReliefRatingOfPain"].Answer : " "));

            $("#StartOfCare_GenericPainMedication").val(result["GenericPainMedication"] != null && result["GenericPainMedication"] != undefined ? result["GenericPainMedication"].Answer : "");
            $("#StartOfCare_GenericMedicationEffectiveness").val((result["GenericMedicationEffectiveness"] !== null && result["GenericMedicationEffectiveness"] !== undefined ? result["GenericMedicationEffectiveness"].Answer : " "));
            $("#StartOfCare_GenericMedicationAdverseEffects").val((result["GenericMedicationAdverseEffects"] !== null && result["GenericMedicationAdverseEffects"] !== undefined ? result["GenericMedicationAdverseEffects"].Answer : " "));
            $("#StartOfCare_GenericPatientPainGoal").val((result["GenericPatientPainGoal"] !== null && result["GenericPatientPainGoal"] !== undefined ? result["GenericPatientPainGoal"].Answer : " "));

            var painIntervention = result["485PainInterventions"];
            if (painIntervention !== null && painIntervention != undefined && painIntervention.Answer != null) {
                var painInterventionArray = (painIntervention.Answer).split(',');
                var i = 0;
                for (i = 0; i < painInterventionArray.length; i++) {
                    $('input[name=StartOfCare_485PainInterventions][value=' + painInterventionArray[i] + ']').attr('checked', true);
                    if (painInterventionArray[i] == 5) {
                        $("#StartOfCare_485PainTooGreatLevel").val((result["485PainTooGreatLevel"] !== null && result["485PainTooGreatLevel"] !== undefined ? result["485PainTooGreatLevel"].Answer : " "));
                    }

                }
            }

            $("#StartOfCare_485PainOrderTemplates").val((result["485PainOrderTemplates"] !== null && result["485PainOrderTemplates"] !== undefined ? result["485PainOrderTemplates"].Answer : " "));
            $("#StartOfCare_485PainInterventionComments").val((result["485PainInterventionComments"] !== null && result["485PainInterventionComments"] !== undefined ? result["485PainInterventionComments"].Answer : " "));

            var painGoals = result["485PainGoals"];
            if (painGoals !== null && painGoals != undefined && painGoals.Answer != null) {
                var painGoalsArray = (painGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < painGoalsArray.length; i++) {
                    $('input[name=StartOfCare_485PainGoals][value=' + painGoalsArray[i] + ']').attr('checked', true);
                    if (painGoalsArray[i] == 1) {
                        $("#StartOfCare_485PatientVerbalizeMedUseDate").val((result["485PatientVerbalizeMedUseDate"] !== null && result["485PatientVerbalizeMedUseDate"] !== undefined ? result["485PatientVerbalizeMedUseDate"].Answer : " "));
                    }
                    if (painGoalsArray[i] == 2) {
                        $("#StartOfCare_485AchievePainLevelLessThan").val((result["485AchievePainLevelLessThan"] !== null && result["485AchievePainLevelLessThan"] !== undefined ? result["485AchievePainLevelLessThan"].Answer : " "));
                        $("#StartOfCare_485AchievePainLevelWeeks").val((result["485AchievePainLevelWeeks"] !== null && result["485AchievePainLevelWeeks"] !== undefined ? result["485AchievePainLevelWeeks"].Answer : " "));
                    }

                }
            }
            $("#StartOfCare_485PainGoalTemplates").val((result["485PainGoalTemplates"] !== null && result["485PainGoalTemplates"] !== undefined ? result["485PainGoalTemplates"].Answer : ""));
            $("#StartOfCare_485PainGoalComments").val((result["485PainGoalComments"] !== null && result["485PainGoalComments"] !== undefined ? result["485PainGoalComments"].Answer : ""));

            $("#StartOfCare_485BradenScaleSensory").val((result["485BradenScaleSensory"] !== null && result["485BradenScaleSensory"] !== undefined ? result["485BradenScaleSensory"].Answer : ""));
            $("#StartOfCare_485BradenScaleMoisture").val((result["485BradenScaleMoisture"] !== null && result["485BradenScaleMoisture"] !== undefined ? result["485BradenScaleMoisture"].Answer : ""));
            $("#StartOfCare_485BradenScaleActivity").val(result["485BradenScaleActivity"] != null && result["485BradenScaleActivity"] != undefined ? result["485BradenScaleActivity"].Answer : "");
            $("#StartOfCare_485BradenScaleMobility").val((result["485BradenScaleMobility"] !== null && result["485BradenScaleMobility"] !== undefined ? result["485BradenScaleMobility"].Answer : ""));
            $("#StartOfCare_485BradenScaleNutrition").val((result["485BradenScaleNutrition"] !== null && result["485BradenScaleNutrition"] !== undefined ? result["485BradenScaleNutrition"].Answer : ""));
            $("#StartOfCare_485BradenScaleFriction").val((result["485BradenScaleFriction"] !== null && result["485BradenScaleFriction"] !== undefined ? result["485BradenScaleFriction"].Answer : ""));
            Oasis.BradenScaleScore('StartOfCare');
            // $("#StartOfCare_485BradenScaleTotal").val((result["485BradenScaleTotal"] !== null && result["485BradenScaleTotal"] !== undefined ? result["485BradenScaleTotal"].Answer : ""));

            $('input[name=StartOfCare_GenericSkinTurgor][value=' + (result["GenericSkinTurgor"] != null && result["GenericSkinTurgor"] != undefined ? result["GenericSkinTurgor"].Answer : "") + ']').attr('checked', true);

            var skinColor = result["GenericSkinColor"];
            if (skinColor !== null && skinColor != undefined && skinColor.Answer != null) {
                var skinColorArray = (skinColor.Answer).split(',');
                var i = 0;
                for (i = 0; i < skinColorArray.length; i++) {
                    $('input[name=StartOfCare_GenericSkinColor][value=' + skinColorArray[i] + ']').attr('checked', true);

                }
            }

            var skin = result["GenericSkin"];
            if (skin !== null && skin != undefined && skin.Answer != null) {
                var skinArray = (skin.Answer).split(',');
                var i = 0;
                for (i = 0; i < skinArray.length; i++) {
                    $('input[name=StartOfCare_GenericSkin][value=' + skinArray[i] + ']').attr('checked', true);

                }
            }

            $('input[name=StartOfCare_GenericInstructedControlInfections][value=' + (result["GenericInstructedControlInfections"] != null && result["GenericInstructedControlInfections"] != undefined ? result["GenericInstructedControlInfections"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericNails][value=' + (result["GenericNails"] != null && result["GenericNails"] != undefined ? result["GenericNails"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericPressureRelievingDevice][value=' + (result["GenericPressureRelievingDevice"] != null && result["GenericPressureRelievingDevice"] != undefined ? result["GenericPressureRelievingDevice"].Answer : "") + ']').attr('checked', true);

            $("#StartOfCare_GenericPressureRelievingDeviceType").val((result["GenericPressureRelievingDeviceType"] !== null && result["GenericPressureRelievingDeviceType"] !== undefined ? result["GenericPressureRelievingDeviceType"].Answer : " "));
            $("#StartOfCare_GenericIntegumentaryStatusComments").val((result["GenericIntegumentaryStatusComments"] !== null && result["GenericIntegumentaryStatusComments"] !== undefined ? result["GenericIntegumentaryStatusComments"].Answer : " "));

            $("#StartOfCare_GenericWoundLocation1").val((result["GenericWoundLocation1"] !== null && result["GenericWoundLocation1"] !== undefined ? result["GenericWoundLocation1"].Answer : " "));
            $("#StartOfCare_GenericWoundLocation2").val((result["GenericWoundLocation2"] !== null && result["GenericWoundLocation2"] !== undefined ? result["GenericWoundLocation2"].Answer : " "));
            $("#StartOfCare_GenericWoundLocation3").val(result["GenericWoundLocation3"] != null && result["GenericWoundLocation3"] != undefined ? result["GenericWoundLocation3"].Answer : "");
            $("#StartOfCare_GenericWoundLocation4").val((result["GenericWoundLocation4"] !== null && result["GenericWoundLocation4"] !== undefined ? result["GenericWoundLocation4"].Answer : " "));
            $("#StartOfCare_GenericWoundLocation5").val((result["GenericWoundLocation5"] !== null && result["GenericWoundLocation5"] !== undefined ? result["GenericWoundLocation5"].Answer : " "));

            $("#StartOfCare_GenericWoundOnsetDate1").val((result["GenericWoundOnsetDate1"] !== null && result["GenericWoundOnsetDate1"] !== undefined ? result["GenericWoundOnsetDate1"].Answer : " "));
            $("#StartOfCare_GenericWoundOnsetDate2").val((result["GenericWoundOnsetDate2"] !== null && result["GenericWoundOnsetDate2"] !== undefined ? result["GenericWoundOnsetDate2"].Answer : " "));
            $("#StartOfCare_GenericWoundOnsetDate3").val((result["GenericWoundOnsetDate3"] !== null && result["GenericWoundOnsetDate3"] !== undefined ? result["GenericWoundOnsetDate3"].Answer : " "));
            $("#StartOfCare_GenericWoundOnsetDate4").val((result["GenericWoundOnsetDate4"] !== null && result["GenericWoundOnsetDate4"] !== undefined ? result["GenericWoundOnsetDate4"].Answer : " "));
            $("#StartOfCare_GenericWoundOnsetDate5").val(result["GenericWoundOnsetDate5"] != null && result["GenericWoundOnsetDate5"] != undefined ? result["GenericWoundOnsetDate5"].Answer : "");

            $("#StartOfCare_GenericWoundSize1").val((result["GenericWoundSize1"] !== null && result["GenericWoundSize1"] !== undefined ? result["GenericWoundSize1"].Answer : " "));
            $("#StartOfCare_GenericWoundSize2").val((result["GenericWoundSize2"] !== null && result["GenericWoundSize2"] !== undefined ? result["GenericWoundSize2"].Answer : " "));
            $("#StartOfCare_GenericWoundSize3").val((result["GenericWoundSize3"] !== null && result["GenericWoundSize3"] !== undefined ? result["GenericWoundSize3"].Answer : " "));
            $("#StartOfCare_GenericWoundSize4").val((result["GenericWoundSize4"] !== null && result["GenericWoundSize4"] !== undefined ? result["GenericWoundSize4"].Answer : " "));
            $("#StartOfCare_GenericWoundSize5").val((result["GenericWoundSize5"] !== null && result["GenericWoundSize5"] !== undefined ? result["GenericWoundSize5"].Answer : " "));

            $("#StartOfCare_GenericWoundDrainage1").val((result["GenericWoundDrainage1"] !== null && result["GenericWoundDrainage1"] !== undefined ? result["GenericWoundDrainage1"].Answer : " "));
            $("#StartOfCare_GenericWoundDrainage2").val(result["GenericWoundDrainage2"] != null && result["GenericWoundDrainage2"] != undefined ? result["GenericWoundDrainage2"].Answer : "");
            $("#StartOfCare_GenericWoundDrainage3").val((result["GenericWoundDrainage3"] !== null && result["GenericWoundDrainage3"] !== undefined ? result["GenericWoundDrainage3"].Answer : " "));
            $("#StartOfCare_GenericWoundDrainage4").val((result["GenericWoundDrainage4"] !== null && result["GenericWoundDrainage4"] !== undefined ? result["GenericWoundDrainage4"].Answer : " "));
            $("#StartOfCare_GenericWoundDrainage5").val((result["GenericWoundDrainage5"] !== null && result["GenericWoundDrainage5"] !== undefined ? result["GenericWoundDrainage5"].Answer : " "));

            $("#StartOfCare_GenericWoundOdor1").val((result["GenericWoundOdor1"] !== null && result["GenericWoundOdor1"] !== undefined ? result["GenericWoundOdor1"].Answer : " "));
            $("#StartOfCare_GenericWoundOdor2").val((result["GenericWoundOdor2"] !== null && result["GenericWoundOdor2"] !== undefined ? result["GenericWoundOdor2"].Answer : " "));
            $("#StartOfCare_GenericWoundOdor3").val((result["GenericWoundOdor3"] !== null && result["GenericWoundOdor3"] !== undefined ? result["GenericWoundOdor3"].Answer : " "));
            $("#StartOfCare_GenericWoundOdor4").val(result["GenericWoundOdor4"] != null && result["GenericWoundOdor4"] != undefined ? result["GenericWoundOdor4"].Answer : "");
            $("#StartOfCare_GenericWoundOdor5").val((result["GenericWoundOdor5"] !== null && result["GenericWoundOdor5"] !== undefined ? result["GenericWoundOdor5"].Answer : " "));

            $("#StartOfCare_GenericWoundEtiology1").val((result["GenericWoundEtiology1"] !== null && result["GenericWoundEtiology1"] !== undefined ? result["GenericWoundEtiology1"].Answer : " "));
            $("#StartOfCare_GenericWoundEtiology2").val((result["GenericWoundEtiology2"] !== null && result["GenericWoundEtiology2"] !== undefined ? result["GenericWoundEtiology2"].Answer : " "));
            $("#StartOfCare_GenericWoundEtiology3").val((result["GenericWoundEtiology3"] !== null && result["GenericWoundEtiology3"] !== undefined ? result["GenericWoundEtiology3"].Answer : " "));
            $("#StartOfCare_GenericWoundEtiology4").val((result["GenericWoundEtiology4"] !== null && result["GenericWoundEtiology4"] !== undefined ? result["GenericWoundEtiology4"].Answer : " "));
            $("#StartOfCare_GenericWoundEtiology5").val((result["GenericWoundEtiology5"] !== null && result["GenericWoundEtiology5"] !== undefined ? result["GenericWoundEtiology5"].Answer : " "));

            $("#StartOfCare_GenericWoundStage1").val(result["GenericWoundStage1"] != null && result["GenericWoundStage1"] != undefined ? result["GenericWoundStage1"].Answer : "");
            $("#StartOfCare_GenericWoundStage2").val((result["GenericWoundStage2"] !== null && result["GenericWoundStage2"] !== undefined ? result["GenericWoundStage2"].Answer : " "));
            $("#StartOfCare_GenericWoundStage3").val((result["GenericWoundStage3"] !== null && result["GenericWoundStage3"] !== undefined ? result["GenericWoundStage3"].Answer : " "));
            $("#StartOfCare_GenericWoundStage4").val((result["GenericWoundStage4"] !== null && result["GenericWoundStage4"] !== undefined ? result["GenericWoundStage4"].Answer : " "));
            $("#StartOfCare_GenericWoundStage5").val((result["GenericWoundStage5"] !== null && result["GenericWoundStage5"] !== undefined ? result["GenericWoundStage5"].Answer : " "));

            $("#StartOfCare_GenericWoundUndermining1").val((result["GenericWoundUndermining1"] !== null && result["GenericWoundUndermining1"] !== undefined ? result["GenericWoundUndermining1"].Answer : " "));
            $("#StartOfCare_GenericWoundUndermining2").val((result["GenericWoundUndermining2"] !== null && result["GenericWoundUndermining2"] !== undefined ? result["GenericWoundUndermining2"].Answer : " "));
            $("#StartOfCare_GenericWoundUndermining3").val(result["GenericWoundUndermining3"] != null && result["GenericWoundUndermining3"] != undefined ? result["GenericWoundUndermining3"].Answer : "");
            $("#StartOfCare_GenericWoundUndermining4").val((result["GenericWoundUndermining4"] !== null && result["GenericWoundUndermining4"] !== undefined ? result["GenericWoundUndermining4"].Answer : " "));
            $("#StartOfCare_GenericWoundUndermining5").val((result["GenericWoundUndermining5"] !== null && result["GenericWoundUndermining5"] !== undefined ? result["GenericWoundUndermining5"].Answer : " "));

            $("#StartOfCare_GenericWoundInflamation1").val((result["GenericWoundInflamation1"] !== null && result["GenericWoundInflamation1"] !== undefined ? result["GenericWoundInflamation1"].Answer : " "));
            $("#StartOfCare_GenericWoundInflamation2").val((result["GenericWoundInflamation2"] !== null && result["GenericWoundInflamation2"] !== undefined ? result["GenericWoundInflamation2"].Answer : " "));
            $("#StartOfCare_GenericWoundInflamation3").val((result["GenericWoundInflamation3"] !== null && result["GenericWoundInflamation3"] !== undefined ? result["GenericWoundInflamation3"].Answer : " "));
            $("#StartOfCare_GenericWoundInflamation4").val((result["GenericWoundInflamation4"] !== null && result["GenericWoundInflamation4"] !== undefined ? result["GenericWoundInflamation4"].Answer : " "));
            $("#StartOfCare_GenericWoundInflamation5").val(result["GenericWoundInflamation5"] != null && result["GenericWoundInflamation5"] != undefined ? result["GenericWoundInflamation5"].Answer : "");

            $("#StartOfCare_GenericWoundComment").val((result["GenericWoundComment"] !== null && result["GenericWoundComment"] !== undefined ? result["GenericWoundComment"].Answer : " "));

            var integumentaryInterventions = result["485IntegumentaryInterventions"];
            if (integumentaryInterventions !== null && integumentaryInterventions != undefined && integumentaryInterventions.Answer != null) {
                var integumentaryInterventionsArray = (integumentaryInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < integumentaryInterventionsArray.length; i++) {
                    $('input[name=StartOfCare_485IntegumentaryInterventions][value=' + integumentaryInterventionsArray[i] + ']').attr('checked', true);
                    if (integumentaryInterventionsArray[i] == 1) {
                        $("#StartOfCare_485InstructTurningRepositionPerson").val((result["485InstructTurningRepositionPerson"] !== null && result["485InstructTurningRepositionPerson"] !== undefined ? result["485InstructTurningRepositionPerson"].Answer : " "));
                    }
                    if (integumentaryInterventionsArray[i] == 2) {
                        $("#StartOfCare_485InstructFloatHeelsPerson").val((result["485InstructFloatHeelsPerson"] !== null && result["485InstructFloatHeelsPerson"] !== undefined ? result["485InstructFloatHeelsPerson"].Answer : " "));
                    }
                    if (integumentaryInterventionsArray[i] == 3) {
                        $("#StartOfCare_485InstructReduceFrictionPerson").val((result["485InstructReduceFrictionPerson"] !== null && result["485InstructReduceFrictionPerson"] !== undefined ? result["485InstructReduceFrictionPerson"].Answer : " "));
                    }
                    if (integumentaryInterventionsArray[i] == 4) {
                        $("#StartOfCare_485InstructPadBonyProminencesPerson").val((result["485InstructPadBonyProminencesPerson"] !== null && result["485InstructPadBonyProminencesPerson"] !== undefined ? result["485InstructPadBonyProminencesPerson"].Answer : " "));
                    }
                    if (integumentaryInterventionsArray[i] == 5) {
                        $("#StartOfCare_485InstructWoundCarePerson").val((result["485InstructWoundCarePerson"] !== null && result["485InstructWoundCarePerson"] !== undefined ? result["485InstructWoundCarePerson"].Answer : " "));
                        $("#StartOfCare_485InstructWoundCarePersonFrequency").val((result["485InstructWoundCarePersonFrequency"] !== null && result["485InstructWoundCarePersonFrequency"] !== undefined ? result["485InstructWoundCarePersonFrequency"].Answer : " "));
                    }
                    if (integumentaryInterventionsArray[i] == 6) {
                        $("#StartOfCare_485InstructWoundOtherDetails").val((result["485InstructWoundOtherDetails"] !== null && result["485InstructWoundOtherDetails"] !== undefined ? result["485InstructWoundOtherDetails"].Answer : " "));
                    }

                    if (integumentaryInterventionsArray[i] == 8) {
                        $("#StartOfCare_485AssessWoundDressingChangeScale").val((result["485AssessWoundDressingChangeScale"] !== null && result["485AssessWoundDressingChangeScale"] !== undefined ? result["485AssessWoundDressingChangeScale"].Answer : " "));

                    }
                    if (integumentaryInterventionsArray[i] == 9) {
                        $("#StartOfCare_485InstructSignsWoundInfectionPerson").val((result["485InstructSignsWoundInfectionPerson"] !== null && result["485InstructSignsWoundInfectionPerson"] !== undefined ? result["485InstructWoundOtherDetails"].Answer : " "));
                        $("#StartOfCare_485InstructSignsWoundInfectionScale").val((result["485InstructSignsWoundInfectionScale"] !== null && result["485InstructSignsWoundInfectionScale"] !== undefined ? result["485InstructSignsWoundInfectionScale"].Answer : " "));
                    }

                }
            }

            $("#StartOfCare_485IntegumentaryOrderTemplates").val((result["485IntegumentaryOrderTemplates"] !== null && result["485IntegumentaryOrderTemplates"] !== undefined ? result["485IntegumentaryOrderTemplates"].Answer : " "));
            $("#StartOfCare_485IntegumentaryInterventionComments").val((result["485IntegumentaryInterventionComments"] !== null && result["485IntegumentaryInterventionComments"] !== undefined ? result["485IntegumentaryInterventionComments"].Answer : " "));

            var integumentaryGoals = result["485IntegumentaryGoals"];
            if (integumentaryGoals !== null && integumentaryGoals != undefined && integumentaryGoals.Answer != null) {
                var integumentaryGoalsArray = (integumentaryGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < integumentaryGoalsArray.length; i++) {
                    $('input[name=StartOfCare_485IntegumentaryGoals][value=' + integumentaryGoalsArray[i] + ']').attr('checked', true);
                    if (integumentaryGoalsArray[i] == 1) {
                        $("#StartOfCare_485HealWithoutComplicationDate").val((result["485HealWithoutComplicationDate"] !== null && result["485HealWithoutComplicationDate"] !== undefined ? result["485HealWithoutComplicationDate"].Answer : " "));
                    }
                    if (integumentaryGoalsArray[i] == 3) {
                        $("#StartOfCare_485WoundSizeDecreasePercent").val((result["485WoundSizeDecreasePercent"] !== null && result["485WoundSizeDecreasePercent"] !== undefined ? result["485WoundSizeDecreasePercent"].Answer : " "));
                        $("#StartOfCare_485WoundSizeDecreasePercentDate").val((result["485WoundSizeDecreasePercentDate"] !== null && result["485WoundSizeDecreasePercentDate"] !== undefined ? result["485WoundSizeDecreasePercentDate"].Answer : " "));
                    }

                }
            }
            $("#StartOfCare_485IntegumentaryGoalTemplates").val((result["485IntegumentaryGoalTemplates"] !== null && result["485IntegumentaryGoalTemplates"] !== undefined ? result["485IntegumentaryGoalTemplates"].Answer : " "));
            $("#StartOfCare_485IntegumentaryGoalComments").val((result["485IntegumentaryGoalComments"] !== null && result["485IntegumentaryGoalComments"] !== undefined ? result["485IntegumentaryGoalComments"].Answer : " "));

            var respiratoryCondition = result["GenericRespiratoryCondition"];
            if (respiratoryCondition !== null && respiratoryCondition != undefined && respiratoryCondition.Answer != null) {
                var respiratoryConditionArray = (respiratoryCondition.Answer).split(',');
                var i = 0;
                for (i = 0; i < respiratoryConditionArray.length; i++) {
                    $('input[name=StartOfCare_GenericRespiratoryCondition][value=' + respiratoryConditionArray[i] + ']').attr('checked', true);
                    if (respiratoryConditionArray[i] == 2) {
                        var respiratorySounds = result["GenericRespiratorySounds"];
                        if (respiratorySounds !== null && respiratorySounds != undefined && respiratorySounds.Answer != null) {
                            var respiratorySoundsArray = (respiratorySounds.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < respiratorySoundsArray.length; j++) {
                                $('input[name=StartOfCare_GenericRespiratorySounds][value=' + respiratorySoundsArray[j] + ']').attr('checked', true);
                                if (respiratorySoundsArray[j] == 1) {
                                    $("#StartOfCare_GenericLungSoundsCTAText").val((result["GenericLungSoundsCTAText"] !== null && result["GenericLungSoundsCTAText"] !== undefined ? result["GenericLungSoundsCTAText"].Answer : " "));
                                }
                                if (respiratorySoundsArray[j] == 2) {
                                    $("#StartOfCare_GenericLungSoundsRalesText").val((result["GenericLungSoundsRalesText"] !== null && result["GenericLungSoundsRalesText"] !== undefined ? result["GenericLungSoundsRalesText"].Answer : " "));
                                }
                                if (respiratorySoundsArray[j] == 3) {
                                    $("#StartOfCare_GenericLungSoundsRhonchiText").val((result["GenericLungSoundsRhonchiText"] !== null && result["GenericLungSoundsRhonchiText"] !== undefined ? result["GenericLungSoundsRhonchiText"].Answer : " "));
                                }

                                if (respiratorySoundsArray[j] == 4) {
                                    $("#StartOfCare_GenericLungSoundsWheezesText").val((result["GenericLungSoundsWheezesText"] !== null && result["GenericLungSoundsWheezesText"] !== undefined ? result["GenericLungSoundsWheezesText"].Answer : " "));
                                }
                                if (respiratorySoundsArray[j] == 5) {
                                    $("#StartOfCare_GenericLungSoundsCracklesText").val((result["GenericLungSoundsCracklesText"] !== null && result["GenericLungSoundsCracklesText"] !== undefined ? result["GenericLungSoundsCracklesText"].Answer : " "));
                                }

                                if (respiratorySoundsArray[j] == 6) {
                                    $("#StartOfCare_GenericLungSoundsDiminishedText").val((result["GenericLungSoundsDiminishedText"] !== null && result["GenericLungSoundsDiminishedText"] !== undefined ? result["GenericLungSoundsDiminishedText"].Answer : " "));
                                }

                                if (respiratorySoundsArray[j] == 7) {
                                    $("#StartOfCare_GenericLungSoundsAbsentText").val((result["GenericLungSoundsAbsentText"] !== null && result["GenericLungSoundsAbsentText"] !== undefined ? result["GenericLungSoundsAbsentText"].Answer : " "));
                                }
                                if (respiratorySoundsArray[j] == 8) {
                                    $("#StartOfCare_GenericLungSoundsStridorText").val((result["GenericLungSoundsStridorText"] !== null && result["GenericLungSoundsStridorText"] !== undefined ? result["GenericLungSoundsStridorText"].Answer : " "));
                                }
                            }
                        }
                    }
                    if (respiratoryConditionArray[i] == 3) {
                        $("#StartOfCare_GenericSputumAmount").val((result["GenericSputumAmount"] !== null && result["GenericSputumAmount"] !== undefined ? result["GenericSputumAmount"].Answer : " "));
                        $("#StartOfCare_GenericSputumColorConsistencyOdor").val((result["GenericSputumColorConsistencyOdor"] !== null && result["GenericSputumColorConsistencyOdor"] !== undefined ? result["GenericSputumColorConsistencyOdor"].Answer : " "));
                    }

                    if (respiratoryConditionArray[i] == 4) {
                        $("#StartOfCare_Generic02AtText").val((result["Generic02AtText"] !== null && result["Generic02AtText"] !== undefined ? result["Generic02AtText"].Answer : " "));
                        $("#StartOfCare_GenericLPMVia").val((result["GenericLPMVia"] !== null && result["GenericLPMVia"] !== undefined ? result["GenericLPMVia"].Answer : " "));
                    }
                    if (respiratoryConditionArray[i] == 5) {
                        $("#StartOfCare_Generic02SatText").val((result["Generic02SatText"] !== null && result["Generic02SatText"] !== undefined ? result["Generic02SatText"].Answer : " "));
                        $("#StartOfCare_Generic02SatList").val((result["Generic02SatList"] !== null && result["Generic02SatList"] !== undefined ? result["Generic02SatList"].Answer : " "));
                    }

                    if (respiratoryConditionArray[i] == 6) {
                        $("#StartOfCare_GenericNebulizerText").val((result["GenericNebulizerText"] !== null && result["GenericNebulizerText"] !== undefined ? result["GenericNebulizerText"].Answer : " "));
                    }
                    if (respiratoryConditionArray[i] == 7) {
                        $("#StartOfCare_GenericCoughList").val((result["GenericCoughList"] !== null && result["GenericCoughList"] !== undefined ? result["GenericCoughList"].Answer : " "));
                        $("#StartOfCare_GenericRespiratoryComments").val((result["GenericRespiratoryComments"] !== null && result["GenericRespiratoryComments"] !== undefined ? result["GenericRespiratoryComments"].Answer : " "));
                    }

                }
            }
            var respiratoryInterventions = result["485RespiratoryInterventions"];
            if (respiratoryInterventions !== null && respiratoryInterventions != undefined && respiratoryInterventions.Answer != null) {
                var respiratoryInterventionsArray = (respiratoryInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < respiratoryInterventionsArray.length; i++) {
                    $('input[name=StartOfCare_485RespiratoryInterventions][value=' + respiratoryInterventionsArray[i] + ']').attr('checked', true);
                    if (respiratoryInterventionsArray[i] == 1) {
                        $("#StartOfCare_485InstructPulmonaryToiletFrequency").val((result["485InstructPulmonaryToiletFrequency"] !== null && result["485InstructPulmonaryToiletFrequency"] !== undefined ? result["485InstructPulmonaryToiletFrequency"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 2) {
                        $("#StartOfCare_485PerformPulmonaryToiletFrequency").val((result["485PerformPulmonaryToiletFrequency"] !== null && result["485PerformPulmonaryToiletFrequency"] !== undefined ? result["485PerformPulmonaryToiletFrequency"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 3) {
                        $("#StartOfCare_485InstructNebulizerUsePerson").val((result["485InstructNebulizerUsePerson"] !== null && result["485InstructNebulizerUsePerson"] !== undefined ? result["485InstructNebulizerUsePerson"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 4) {
                        $("#StartOfCare_485AssessOxySaturationFrequency").val((result["485AssessOxySaturationFrequency"] !== null && result["485AssessOxySaturationFrequency"] !== undefined ? result["485AssessOxySaturationFrequency"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 5) {
                        $("#StartOfCare_485AssessOxySatOnOxyAt").val((result["485AssessOxySatOnOxyAt"] !== null && result["485AssessOxySatOnOxyAt"] !== undefined ? result["485AssessOxySatOnOxyAt"].Answer : " "));
                        $("#StartOfCare_485AssessOxySatLPM").val((result["485AssessOxySatLPM"] !== null && result["485AssessOxySatLPM"] !== undefined ? result["485AssessOxySatLPM"].Answer : " "));
                        $("#StartOfCare_485AssessOxySatOnOxyFrequency").val((result["485AssessOxySatOnOxyFrequency"] !== null && result["485AssessOxySatOnOxyFrequency"] !== undefined ? result["485AssessOxySatOnOxyFrequency"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 6) {
                        $("#StartOfCare_485InstructSobFactorsPerson").val((result["485InstructSobFactorsPerson"] !== null && result["485InstructSobFactorsPerson"] !== undefined ? result["485InstructSobFactorsPerson"].Answer : " "));
                        $("#StartOfCare_485InstructSobFactorsTemp").val((result["485InstructSobFactorsTemp"] !== null && result["485InstructSobFactorsTemp"] !== undefined ? result["485InstructSobFactorsTemp"].Answer : " "));
                    }

                    if (respiratoryInterventionsArray[i] == 7) {
                        $("#StartOfCare_485InstructAvoidSmokingPerson").val((result["485InstructAvoidSmokingPerson"] !== null && result["485InstructAvoidSmokingPerson"] !== undefined ? result["485InstructAvoidSmokingPerson"].Answer : " "));

                    }
                    if (respiratoryInterventionsArray[i] == 10) {
                        $("#StartOfCare_485InstructNebulizerTreatmentType").val((result["485InstructNebulizerTreatmentType"] !== null && result["485InstructNebulizerTreatmentType"] !== undefined ? result["485InstructNebulizerTreatmentType"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 11) {
                        $("#StartOfCare_485InstructProperUseOfType").val((result["485InstructProperUseOfType"] !== null && result["485InstructProperUseOfType"] !== undefined ? result["485InstructProperUseOfType"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 13) {
                        $("#StartOfCare_485RecognizePulmonaryDysfunctionPerson").val((result["485RecognizePulmonaryDysfunctionPerson"] !== null && result["485RecognizePulmonaryDysfunctionPerson"] !== undefined ? result["485RecognizePulmonaryDysfunctionPerson"].Answer : " "));
                    }
                    if (respiratoryInterventionsArray[i] == 14) {
                        $("#StartOfCare_485OxySaturationLessThanPercent").val((result["485OxySaturationLessThanPercent"] !== null && result["485OxySaturationLessThanPercent"] !== undefined ? result["485OxySaturationLessThanPercent"].Answer : " "));
                    }
                }
            }

            $("#StartOfCare_485RespiratoryOrderTemplates").val((result["485RespiratoryOrderTemplates"] !== null && result["485RespiratoryOrderTemplates"] !== undefined ? result["485RespiratoryOrderTemplates"].Answer : " "));
            $("#StartOfCare_485RespiratoryInterventionComments").val((result["485RespiratoryInterventionComments"] !== null && result["485RespiratoryInterventionComments"] !== undefined ? result["485RespiratoryInterventionComments"].Answer : " "));
            var respiratoryGoals = result["485RespiratoryGoals"];
            if (respiratoryGoals !== null && respiratoryGoals != undefined && respiratoryGoals.Answer != null) {
                var respiratoryGoalsArray = (respiratoryGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < respiratoryGoalsArray.length; i++) {
                    $('input[name=StartOfCare_485RespiratoryGoals][value=' + respiratoryGoalsArray[i] + ']').attr('checked', true);
                    if (respiratoryGoalsArray[i] == 3) {
                        $("#StartOfCare_485VerbalizeFactorsSobDate").val((result["485VerbalizeFactorsSobDate"] !== null && result["485VerbalizeFactorsSobDate"] !== undefined ? result["485VerbalizeFactorsSobDate"].Answer : " "));
                    }
                    if (respiratoryGoalsArray[i] == 4) {
                        $("#StartOfCare_485DemonstrateLipBreathingDate").val((result["485DemonstrateLipBreathingDate"] !== null && result["485DemonstrateLipBreathingDate"] !== undefined ? result["485DemonstrateLipBreathingDate"].Answer : " "));
                    }
                    if (respiratoryGoalsArray[i] == 5) {
                        $("#StartOfCare_485VerbalizeEnergyConserveDate").val((result["485VerbalizeEnergyConserveDate"] !== null && result["485VerbalizeEnergyConserveDate"] !== undefined ? result["485VerbalizeEnergyConserveDate"].Answer : " "));
                    }
                    if (respiratoryGoalsArray[i] == 6) {
                        $("#StartOfCare_485VerbalizeSafeOxyManagementPerson").val((result["485VerbalizeSafeOxyManagementPerson"] !== null && result["485VerbalizeSafeOxyManagementPerson"] !== undefined ? result["485VerbalizeSafeOxyManagementPerson"].Answer : " "));
                        $("#StartOfCare_485VerbalizeSafeOxyManagementPersonDate").val((result["485VerbalizeSafeOxyManagementPersonDate"] !== null && result["485VerbalizeSafeOxyManagementPersonDate"] !== undefined ? result["485VerbalizeSafeOxyManagementPersonDate"].Answer : " "));
                    }
                    if (respiratoryGoalsArray[i] == 7) {
                        $("#StartOfCare_485DemonstrateNebulizerUseDate").val((result["485DemonstrateNebulizerUseDate"] !== null && result["485DemonstrateNebulizerUseDate"] !== undefined ? result["485DemonstrateNebulizerUseDate"].Answer : " "));
                    }
                    if (respiratoryGoalsArray[i] == 8) {
                        $("#StartOfCare_485DemonstrateProperUseOfType").val((result["485DemonstrateProperUseOfType"] !== null && result["485DemonstrateProperUseOfType"] !== undefined ? result["485DemonstrateProperUseOfType"].Answer : " "));
                        $("#StartOfCare_485DemonstrateProperUseOfTypeDate").val((result["485DemonstrateProperUseOfTypeDate"] !== null && result["485DemonstrateProperUseOfTypeDate"] !== undefined ? result["485DemonstrateProperUseOfTypeDate"].Answer : " "));
                    }

                }
            }

            $("#StartOfCare_485RespiratoryGoalTemplates").val((result["485RespiratoryGoalTemplates"] !== null && result["485RespiratoryGoalTemplates"] !== undefined ? result["485RespiratoryGoalTemplates"].Answer : " "));
            $("#StartOfCare_485RespiratoryGoalComments").val((result["485RespiratoryGoalComments"] !== null && result["485RespiratoryGoalComments"] !== undefined ? result["485RespiratoryGoalComments"].Answer : " "));

            var endocrineWithinNormalLimits = result["GenericEndocrineWithinNormalLimits"];
            if (endocrineWithinNormalLimits != null && endocrineWithinNormalLimits != undefined && endocrineWithinNormalLimits.Answer != null) {
                if (endocrineWithinNormalLimits.Answer == "1") {
                    $('input[name=StartOfCare_GenericEndocrineWithinNormalLimits][value="1"]').attr('checked', true);
                }
                else {
                    $('input[name=StartOfCare_GenericEndocrineWithinNormalLimits][value="1"]').attr('checked', false);

                }
            }
            $('input[name=StartOfCare_GenericPatientDiabetic][value=' + (result["GenericPatientDiabetic"] != null && result["GenericPatientDiabetic"] != undefined ? result["GenericPatientDiabetic"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericInsulinDependent][value=' + (result["GenericInsulinDependent"] != null && result["GenericInsulinDependent"] != undefined ? result["GenericInsulinDependent"].Answer : "") + ']').attr('checked', true);
            if ($('input[name=StartOfCare_GenericInsulinDependent]:checked').val() == '1') {
                $("#StartOfCare_GenericInsulinDependencyDuration").val((result["GenericInsulinDependencyDuration"] !== null && result["GenericInsulinDependencyDuration"] !== undefined ? result["GenericInsulinDependencyDuration"].Answer : " "));

            }
            $('input[name=StartOfCare_GenericDrawUpInsulinDose][value=' + (result["GenericDrawUpInsulinDose"] != null && result["GenericDrawUpInsulinDose"] != undefined ? result["GenericDrawUpInsulinDose"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericAdministerOwnInsulin][value=' + (result["GenericAdministerOwnInsulin"] != null && result["GenericAdministerOwnInsulin"] != undefined ? result["GenericAdministerOwnInsulin"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericOralHypoglycemicAgent][value=' + (result["GenericOralHypoglycemicAgent"] != null && result["GenericOralHypoglycemicAgent"] != undefined ? result["GenericOralHypoglycemicAgent"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericGlucometerUseIndependent][value=' + (result["GenericGlucometerUseIndependent"] != null && result["GenericGlucometerUseIndependent"] != undefined ? result["GenericGlucometerUseIndependent"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericCareGiverDrawUpInsulin][value=' + (result["GenericCareGiverDrawUpInsulin"] != null && result["GenericCareGiverDrawUpInsulin"] != undefined ? result["GenericCareGiverDrawUpInsulin"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericCareGiverGlucometerUse][value=' + (result["GenericCareGiverGlucometerUse"] != null && result["GenericCareGiverGlucometerUse"] != undefined ? result["GenericCareGiverGlucometerUse"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericCareGiverInspectLowerExtremities][value=' + (result["GenericCareGiverInspectLowerExtremities"] != null && result["GenericCareGiverInspectLowerExtremities"] != undefined ? result["GenericCareGiverInspectLowerExtremities"].Answer : "") + ']').attr('checked', true);

            $("#StartOfCare_485RespiratoryInterventionComments").val((result["485RespiratoryInterventionComments"] !== null && result["485RespiratoryInterventionComments"] !== undefined ? result["485RespiratoryInterventionComments"].Answer : " "));
            var patientEdocrineProblem = result["GenericPatientEdocrineProblem"];
            if (patientEdocrineProblem !== null && patientEdocrineProblem != undefined && patientEdocrineProblem.Answer != null) {
                var patientEdocrineProblemArray = (patientEdocrineProblem.Answer).split(',');
                var i = 0;
                for (i = 0; i < patientEdocrineProblemArray.length; i++) {
                    $('input[name=StartOfCare_GenericPatientEdocrineProblem][value=' + patientEdocrineProblemArray[i] + ']').attr('checked', true);

                }
            }
            $("#StartOfCare_GenericBloodSugarLevelText").val((result["GenericBloodSugarLevelText"] !== null && result["GenericBloodSugarLevelText"] !== undefined ? result["485RespiratoryGoalTemplates"].Answer : " "));
            $('input[name=StartOfCare_GenericBloodSugarLevel][value=' + (result["GenericBloodSugarLevel"] != null && result["GenericBloodSugarLevel"] != undefined ? result["GenericBloodSugarLevel"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_GenericBloodSugarCheckedBy").val((result["GenericBloodSugarCheckedBy"] !== null && result["GenericBloodSugarCheckedBy"] !== undefined ? result["GenericBloodSugarCheckedBy"].Answer : " "));
            $("#StartOfCare_GenericBloodSugarSite").val((result["GenericBloodSugarSite"] !== null && result["GenericBloodSugarSite"] !== undefined ? result["GenericBloodSugarSite"].Answer : " "));
            $("#StartOfCare_GenericEndocrineComments").val((result["GenericEndocrineComments"] !== null && result["GenericEndocrineComments"] !== undefined ? result["GenericEndocrineComments"].Answer : " "));

            var endocrineInterventions = result["485EndocrineInterventions"];
            if (endocrineInterventions !== null && endocrineInterventions != undefined && endocrineInterventions.Answer != null) {
                var endocrineInterventionsArray = (endocrineInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < endocrineInterventionsArray.length; i++) {
                    $('input[name=StartOfCare_485EndocrineInterventions][value=' + endocrineInterventionsArray[i] + ']').attr('checked', true);
                    if (endocrineInterventionsArray[i] == 1) {
                        $("#StartOfCare_485InstructDiabeticManagementPerson").val((result["485InstructDiabeticManagementPerson"] !== null && result["485InstructDiabeticManagementPerson"] !== undefined ? result["485InstructDiabeticManagementPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 2) {
                        $("#StartOfCare_485InstructInspectFeetDailyPerson").val((result["485InstructInspectFeetDailyPerson"] !== null && result["485InstructInspectFeetDailyPerson"] !== undefined ? result["485InstructInspectFeetDailyPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 3) {
                        $("#StartOfCare_485InstructWashFeetWarmPerson").val((result["485InstructWashFeetWarmPerson"] !== null && result["485InstructWashFeetWarmPerson"] !== undefined ? result["485InstructWashFeetWarmPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 4) {
                        $("#StartOfCare_485InstructMoisturizerPerson").val((result["485InstructMoisturizerPerson"] !== null && result["485InstructMoisturizerPerson"] !== undefined ? result["485InstructMoisturizerPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 6) {
                        $("#StartOfCare_485InstructNailCarePerson").val((result["485InstructNailCarePerson"] !== null && result["485InstructNailCarePerson"] !== undefined ? result["485InstructNailCarePerson"].Answer : " "));

                    }
                    if (endocrineInterventionsArray[i] == 7) {
                        $("#StartOfCare_485InstructNeverWalkBareFootPerson").val((result["485InstructNeverWalkBareFootPerson"] !== null && result["485InstructNeverWalkBareFootPerson"] !== undefined ? result["485InstructNeverWalkBareFootPerson"].Answer : " "));

                    }

                    if (endocrineInterventionsArray[i] == 8) {
                        $("#StartOfCare_485InstructElevateFeetPerson").val((result["485InstructElevateFeetPerson"] !== null && result["485InstructElevateFeetPerson"] !== undefined ? result["485InstructElevateFeetPerson"].Answer : " "));

                    }
                    if (endocrineInterventionsArray[i] == 9) {
                        $("#StartOfCare_485InstructProtectFeetPerson").val((result["485InstructProtectFeetPerson"] !== null && result["485InstructProtectFeetPerson"] !== undefined ? result["485InstructProtectFeetPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 10) {
                        $("#StartOfCare_485InstructNeverCutCornsPerson").val((result["485InstructNeverCutCornsPerson"] !== null && result["485InstructNeverCutCornsPerson"] !== undefined ? result["485InstructNeverCutCornsPerson"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 12) {
                        $("#StartOfCare_485GiveJuiceIfBloodSugarLevel").val((result["485GiveJuiceIfBloodSugarLevel"] !== null && result["485GiveJuiceIfBloodSugarLevel"] !== undefined ? result["485GiveJuiceIfBloodSugarLevel"].Answer : " "));
                        $("#StartOfCare_485GiveJuiceIfBloodSugarLevelRemains").val((result["485GiveJuiceIfBloodSugarLevelRemains"] !== null && result["485GiveJuiceIfBloodSugarLevelRemains"] !== undefined ? result["485GiveJuiceIfBloodSugarLevelRemains"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 13) {
                        $("#StartOfCare_485PrepareAdministerInsulinType").val((result["485PrepareAdministerInsulinType"] !== null && result["485PrepareAdministerInsulinType"] !== undefined ? result["485PrepareAdministerInsulinType"].Answer : " "));
                        $("#StartOfCare_485PrepareAdministerInsulinFrequency").val((result["485PrepareAdministerInsulinFrequency"] !== null && result["485PrepareAdministerInsulinFrequency"] !== undefined ? result["485PrepareAdministerInsulinFrequency"].Answer : " "));
                    }
                    if (endocrineInterventionsArray[i] == 15) {
                        $("#StartOfCare_485PrefillInsulinSyringesType").val((result["485PrefillInsulinSyringesType"] !== null && result["485PrefillInsulinSyringesType"] !== undefined ? result["485PrefillInsulinSyringesType"].Answer : " "));
                        $("#StartOfCare_485PrefillInsulinSyringesTypeFrequency").val((result["485PrefillInsulinSyringesTypeFrequency"] !== null && result["485PrefillInsulinSyringesTypeFrequency"] !== undefined ? result["485PrefillInsulinSyringesTypeFrequency"].Answer : " "));
                    }

                }
            }

            $("#StartOfCare_485EndocrineInterventionTemplates").val((result["485EndocrineInterventionTemplates"] !== null && result["485EndocrineInterventionTemplates"] !== undefined ? result["485EndocrineInterventionTemplates"].Answer : " "));
            $("#StartOfCare_485EndocrineInterventionComments").val((result["485EndocrineInterventionComments"] !== null && result["485EndocrineInterventionComments"] !== undefined ? result["485EndocrineInterventionComments"].Answer : " "));

            var endocrineGoals = result["485EndocrineGoals"];
            if (endocrineGoals !== null && endocrineGoals != undefined && endocrineGoals.Answer != null) {
                var endocrineGoalsArray = (endocrineGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < endocrineGoalsArray.length; i++) {
                    $('input[name=StartOfCare_485EndocrineGoals][value=' + endocrineGoalsArray[i] + ']').attr('checked', true);
                    if (endocrineGoalsArray[i] == 1) {
                        $("#StartOfCare_485FastingBloodSugarBetween").val((result["485FastingBloodSugarBetween"] !== null && result["485FastingBloodSugarBetween"] !== undefined ? result["485FastingBloodSugarBetween"].Answer : " "));
                        $("#StartOfCare_485FastingBloodSugarAnd").val((result["485FastingBloodSugarAnd"] !== null && result["485FastingBloodSugarAnd"] !== undefined ? result["485FastingBloodSugarAnd"].Answer : " "));
                    }
                    if (endocrineGoalsArray[i] == 2) {
                        $("#StartOfCare_485RandomBloodSugarBetween").val((result["485RandomBloodSugarBetween"] !== null && result["485RandomBloodSugarBetween"] !== undefined ? result["485RandomBloodSugarBetween"].Answer : " "));
                        $("#StartOfCare_485RandomBloodSugarAnd").val((result["485RandomBloodSugarAnd"] !== null && result["485RandomBloodSugarAnd"] !== undefined ? result["485RandomBloodSugarAnd"].Answer : " "));
                    }
                    if (endocrineGoalsArray[i] == 4) {
                        $("#StartOfCare_485GlucometerUseIndependencePerson").val((result["485GlucometerUseIndependencePerson"] !== null && result["485GlucometerUseIndependencePerson"] !== undefined ? result["485GlucometerUseIndependencePerson"].Answer : " "));
                        $("#StartOfCare_485GlucometerUseIndependenceDate").val((result["485GlucometerUseIndependenceDate"] !== null && result["485GlucometerUseIndependenceDate"] !== undefined ? result["485GlucometerUseIndependenceDate"].Answer : " "));
                    }
                    if (endocrineGoalsArray[i] == 5) {
                        $("#StartOfCare_485VerbalizeSkinConditionUnderstandingPerson").val((result["485VerbalizeSkinConditionUnderstandingPerson"] !== null && result["485VerbalizeSkinConditionUnderstandingPerson"] !== undefined ? result["485VerbalizeSkinConditionUnderstandingPerson"].Answer : " "));

                    }
                    if (endocrineGoalsArray[i] == 6) {
                        $("#StartOfCare_485IndependentInsulinAdministrationPerson").val((result["485IndependentInsulinAdministrationPerson"] !== null && result["485IndependentInsulinAdministrationPerson"] !== undefined ? result["485IndependentInsulinAdministrationPerson"].Answer : " "));
                        $("#StartOfCare_485IndependentInsulinAdministrationDate").val((result["485IndependentInsulinAdministrationDate"] !== null && result["485IndependentInsulinAdministrationDate"] !== undefined ? result["485IndependentInsulinAdministrationDate"].Answer : " "));
                    }
                    if (endocrineGoalsArray[i] == 7) {
                        $("#StartOfCare_485VerbalizeProperFootCarePerson").val((result["485VerbalizeProperFootCarePerson"] !== null && result["485VerbalizeProperFootCarePerson"] !== undefined ? result["485VerbalizeProperFootCarePerson"].Answer : " "));
                        $("#StartOfCare_485VerbalizeProperFootCareDate").val((result["485VerbalizeProperFootCareDate"] !== null && result["485VerbalizeProperFootCareDate"] !== undefined ? result["485VerbalizeProperFootCareDate"].Answer : " "));
                    }
                }
            }

            $("#StartOfCare_485EndocrineGoalTemplates").val((result["485EndocrineGoalTemplates"] !== null && result["485EndocrineGoalTemplates"] !== undefined ? result["485EndocrineGoalTemplates"].Answer : " "));
            $("#StartOfCare_485EndocrineGoalComments").val((result["485EndocrineGoalComments"] !== null && result["485EndocrineGoalComments"] !== undefined ? result["485EndocrineGoalComments"].Answer : " "));

            var cardioVascular = result["GenericCardioVascular"];
            if (cardioVascular !== null && cardioVascular != undefined && cardioVascular.Answer != null) {
                var cardioVascularArray = (cardioVascular.Answer).split(',');
                var i = 0;
                for (i = 0; i < cardioVascularArray.length; i++) {
                    $('input[name=StartOfCare_GenericCardioVascular][value=' + cardioVascularArray[i] + ']').attr('checked', true);
                    if (cardioVascularArray[i] == 2) {
                        $("#StartOfCare_GenericDizzinessDesc").val((result["GenericDizzinessDesc"] !== null && result["GenericDizzinessDesc"] !== undefined ? result["GenericDizzinessDesc"].Answer : " "));
                    }
                    if (cardioVascularArray[i] == 3) {
                        $("#StartOfCare_GenericChestPainDesc").val((result["GenericChestPainDesc"] !== null && result["GenericChestPainDesc"] !== undefined ? result["GenericChestPainDesc"].Answer : " "));
                    }
                    if (cardioVascularArray[i] == 4) {
                        $("#StartOfCare_GenericEdemaDesc1").val((result["GenericEdemaDesc1"] !== null && result["GenericEdemaDesc1"] !== undefined ? result["GenericEdemaDesc1"].Answer : " "));
                        $("#StartOfCare_GenericEdemaList1").val((result["GenericEdemaList1"] !== null && result["GenericEdemaList1"] !== undefined ? result["GenericEdemaList1"].Answer : " "));
                        $("#StartOfCare_GenericEdemaDesc2").val((result["GenericEdemaDesc2"] !== null && result["GenericEdemaDesc2"] !== undefined ? result["GenericEdemaDesc2"].Answer : " "));
                        $("#StartOfCare_GenericEdemaList2").val((result["GenericEdemaList2"] !== null && result["GenericEdemaList2"] !== undefined ? result["GenericEdemaList2"].Answer : " "));
                        $("#StartOfCare_GenericEdemaDesc3").val((result["GenericEdemaDesc3"] !== null && result["GenericEdemaDesc3"] !== undefined ? result["GenericEdemaDesc3"].Answer : " "));
                        $("#StartOfCare_GenericEdemaList3").val((result["GenericEdemaList3"] !== null && result["GenericEdemaList3"] !== undefined ? result["GenericEdemaList3"].Answer : " "));
                    }
                    if (cardioVascularArray[i] == 5) {
                        var pittingEdemaType = result["GenericPittingEdemaType"];
                        if (pittingEdemaType !== null && pittingEdemaType != undefined && pittingEdemaType.Answer != null) {
                            var pittingEdemaTypeArray = (pittingEdemaType.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < pittingEdemaTypeArray.length; j++) {
                                $('input[name=StartOfCare_GenericPittingEdemaType][value=' + pittingEdemaTypeArray[j] + ']').attr('checked', true);
                            }
                        }

                    }
                    if (cardioVascularArray[i] == 6) {
                        var heartSoundsType = result["GenericHeartSoundsType"];
                        if (heartSoundsType !== null && heartSoundsType != undefined && heartSoundsType.Answer != null) {
                            var heartSoundsTypeArray = (heartSoundsType.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < heartSoundsTypeArray.length; j++) {
                                $('input[name=StartOfCare_GenericHeartSoundsType][value=' + heartSoundsTypeArray[j] + ']').attr('checked', true);
                            }
                        }
                    }
                    if (cardioVascularArray[i] == 7) {
                        $("#StartOfCare_GenericNeckVeinDistentionDesc").val((result["GenericNeckVeinDistentionDesc"] !== null && result["GenericNeckVeinDistentionDesc"] !== undefined ? result["GenericNeckVeinDistentionDesc"].Answer : " "));

                    }

                    if (cardioVascularArray[i] == 8) {
                        $("#StartOfCare_GenericPeripheralPulsesDesc").val((result["GenericPeripheralPulsesDesc"] !== null && result["GenericPeripheralPulsesDesc"] !== undefined ? result["GenericPeripheralPulsesDesc"].Answer : " "));

                    }
                    if (cardioVascularArray[i] == 9) {
                        $('input[name=StartOfCare_GenericCapRefillLessThan3][value=' + (result["GenericCapRefillLessThan3"] != null && result["GenericCapRefillLessThan3"] != undefined ? result["GenericCapRefillLessThan3"].Answer : "") + ']').attr('checked', true);
                    }

                }
            }
            $("#StartOfCare_GenericPacemakerDate").val((result["GenericPacemakerDate"] !== null && result["GenericPacemakerDate"] !== undefined ? result["GenericPacemakerDate"].Answer : " "));
            $("#StartOfCare_GenericAICDDate").val((result["GenericAICDDate"] !== null && result["GenericAICDDate"] !== undefined ? result["GenericAICDDate"].Answer : " "));
            $("#StartOfCare_GenericCardiovascularComments").val((result["GenericCardiovascularComments"] !== null && result["GenericCardiovascularComments"] !== undefined ? result["GenericCardiovascularComments"].Answer : " "));

            var cardioVascular = result["485CardiacStatusInterventions"];
            if (cardioVascular !== null && cardioVascular != undefined && cardioVascular.Answer != null) {
                var cardioVascularArray = (cardioVascular.Answer).split(',');
                var i = 0;
                for (i = 0; i < cardioVascularArray.length; i++) {
                    $('input[name=StartOfCare_485CardiacStatusInterventions][value=' + cardioVascularArray[i] + ']').attr('checked', true);
                    if (cardioVascularArray[i] == 1) {
                        $('input[name=StartOfCare_485WeightSelfMonitorGain][value=' + (result["485WeightSelfMonitorGain"] != null && result["485WeightSelfMonitorGain"] != undefined ? result["485WeightSelfMonitorGain"].Answer : "") + ']').attr('checked', true);
                        $("#StartOfCare_485WeightSelfMonitorGainDay").val((result["485WeightSelfMonitorGainDay"] !== null && result["485WeightSelfMonitorGainDay"] !== undefined ? result["485WeightSelfMonitorGainDay"].Answer : " "));
                        $("#StartOfCare_485WeightSelfMonitorGainWeek").val((result["485WeightSelfMonitorGainWeek"] !== null && result["485WeightSelfMonitorGainWeek"] !== undefined ? result["485WeightSelfMonitorGainWeek"].Answer : " "));
                    }
                    if (cardioVascularArray[i] == 3) {
                        $("#StartOfCare_485InstructRecognizeCardiacDysfunctionPerson").val((result["485InstructRecognizeCardiacDysfunctionPerson"] !== null && result["485InstructRecognizeCardiacDysfunctionPerson"] !== undefined ? result["485InstructRecognizeCardiacDysfunctionPerson"].Answer : " "));
                    }
                    if (cardioVascularArray[i] == 7) {
                        $("#StartOfCare_485NoBloodPressureArm").val((result["485NoBloodPressureArm"] !== null && result["485NoBloodPressureArm"] !== undefined ? result["485NoBloodPressureArm"].Answer : " "));

                    }
                }
            }
            $("#StartOfCare_485CardiacOrderTemplates").val((result["485CardiacOrderTemplates"] !== null && result["485CardiacOrderTemplates"] !== undefined ? result["485CardiacOrderTemplates"].Answer : " "));
            $("#StartOfCare_485CardiacInterventionComments").val((result["485CardiacInterventionComments"] !== null && result["485CardiacInterventionComments"] !== undefined ? result["485CardiacInterventionComments"].Answer : " "));

            var cardiacStatusGoals = result["485CardiacStatusGoals"];
            if (cardiacStatusGoals !== null && cardiacStatusGoals != undefined && cardiacStatusGoals.Answer != null) {
                var cardiacStatusGoalsArray = (cardiacStatusGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < cardiacStatusGoalsArray.length; i++) {
                    $('input[name=StartOfCare_485CardiacStatusGoals][value=' + cardiacStatusGoalsArray[i] + ']').attr('checked', true);
                    if (cardiacStatusGoalsArray[i] == 1) {
                        $("#StartOfCare_485WeightMaintainedMin").val((result["485WeightMaintainedMin"] !== null && result["485WeightMaintainedMin"] !== undefined ? result["485WeightMaintainedMin"].Answer : " "));
                        $("#StartOfCare_485WeightMaintainedMax").val((result["485WeightMaintainedMax"] !== null && result["485WeightMaintainedMax"] !== undefined ? result["485WeightMaintainedMax"].Answer : " "));
                    }
                    if (cardiacStatusGoalsArray[i] == 5) {
                        $("#StartOfCare_485VerbalizeCardiacSymptomsPerson").val((result["485VerbalizeCardiacSymptomsPerson"] !== null && result["485VerbalizeCardiacSymptomsPerson"] !== undefined ? result["485VerbalizeCardiacSymptomsPerson"].Answer : " "));
                        $("#StartOfCare_485VerbalizeCardiacSymptomsDate").val((result["485VerbalizeCardiacSymptomsDate"] !== null && result["485VerbalizeCardiacSymptomsDate"] !== undefined ? result["485VerbalizeCardiacSymptomsDate"].Answer : " "));
                    }
                    if (cardiacStatusGoalsArray[i] == 6) {
                        $("#StartOfCare_485VerbalizeEdemaRelieverPerson").val((result["485VerbalizeEdemaRelieverPerson"] !== null && result["485VerbalizeEdemaRelieverPerson"] !== undefined ? result["485VerbalizeEdemaRelieverPerson"].Answer : " "));
                        $("#StartOfCare_485VerbalizeEdemaRelieverDate").val((result["485VerbalizeEdemaRelieverDate"] !== null && result["485VerbalizeEdemaRelieverDate"] !== undefined ? result["485VerbalizeEdemaRelieverDate"].Answer : " "));

                    }
                }
            }
            $("#StartOfCare_485CardiacGoalTemplates").val((result["485CardiacGoalTemplates"] !== null && result["485CardiacGoalTemplates"] !== undefined ? result["485CardiacGoalTemplates"].Answer : " "));
            $("#StartOfCare_485CardiacGoalComments").val((result["485CardiacGoalComments"] !== null && result["485CardiacGoalComments"] !== undefined ? result["485CardiacGoalComments"].Answer : " "));

            var gU = result["GenericGU"];
            if (gU !== null && gU != undefined && gU.Answer != null) {
                var gUArray = (gU.Answer).split(',');
                var i = 0;
                for (i = 0; i < gUArray.length; i++) {
                    $('input[name=StartOfCare_GenericGU][value=' + gUArray[i] + ']').attr('checked', true);
                    if (gUArray[i] == 10) {
                        $("#StartOfCare_GenericGUCatheterList").val((result["GenericGUCatheterList"] !== null && result["GenericGUCatheterList"] !== undefined ? result["GenericGUCatheterList"].Answer : " "));
                        $("#StartOfCare_GenericGUCatheterLastChanged").val((result["GenericGUCatheterLastChanged"] !== null && result["GenericGUCatheterLastChanged"] !== undefined ? result["GenericGUCatheterLastChanged"].Answer : " "));
                        $("#StartOfCare_GenericGUCatheterFrequency").val((result["GenericGUCatheterFrequency"] !== null && result["GenericGUCatheterFrequency"] !== undefined ? result["GenericGUCatheterFrequency"].Answer : " "));
                        $("#StartOfCare_GenericGUCatheterAmount").val((result["GenericGUCatheterAmount"] !== null && result["GenericGUCatheterAmount"] !== undefined ? result["GenericGUCatheterAmount"].Answer : " "));
                    }
                    if (gUArray[i] == 11) {
                        var gUUrine = result["GenericGUUrine"];
                        if (gUUrine !== null && gUUrine != undefined && gUUrine.Answer != null) {
                            var gUUrineArray = (gUUrine.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < gUUrineArray.length; j++) {
                                $('input[name=StartOfCare_GenericGUUrine][value=' + gUUrineArray[j] + ']').attr('checked', true);
                                if (gUUrineArray[j] == 5) {
                                    $("#StartOfCare_GenericGUOtherText").val((result["GenericGUOtherText"] !== null && result["GenericGUOtherText"] !== undefined ? result["GenericGUOtherText"].Answer : " "));
                                }
                            }
                        }
                    }
                    if (gUArray[i] == 12) {
                        $('input[name=StartOfCare_GenericGUNormal][value=' + (result["GenericGUNormal"] != null && result["GenericGUNormal"] != undefined ? result["GenericGUNormal"].Answer : "") + ']').attr('checked', true);
                        $('input[name=StartOfCare_GenericGUClinicalAssessment][value=' + (result["GenericGUClinicalAssessment"] != null && result["GenericGUClinicalAssessment"] != undefined ? result["GenericGUClinicalAssessment"].Answer : "") + ']').attr('checked', true);
                    }

                }
            }
            var digestive = result["GenericDigestive"];
            if (digestive !== null && digestive != undefined && digestive.Answer != null) {
                var digestiveArray = (digestive.Answer).split(',');
                var i = 0;
                for (i = 0; i < digestiveArray.length; i++) {
                    $('input[name=StartOfCare_GenericDigestive][value=' + digestiveArray[i] + ']').attr('checked', true);
                    if (digestiveArray[i] == 8) {
                        $('input[name=StartOfCare_GenericDigestiveBowelSoundsType][value=' + (result["GenericDigestiveBowelSoundsType"] != null && result["GenericDigestiveBowelSoundsType"] != undefined ? result["GenericDigestiveBowelSoundsType"].Answer : "") + ']').attr('checked', true);
                    }
                    if (digestiveArray[i] == 9) {
                        $("#StartOfCare_GenericDigestiveAbdGirthLength").val((result["GenericDigestiveAbdGirthLength"] !== null && result["GenericDigestiveAbdGirthLength"] !== undefined ? result["GenericDigestiveAbdGirthLength"].Answer : " "));
                    }
                    if (digestiveArray[i] == 10) {
                        $("#StartOfCare_GenericDigestiveLastBMDate").val((result["GenericDigestiveLastBMDate"] !== null && result["GenericDigestiveLastBMDate"] !== undefined ? result["GenericDigestiveLastBMDate"].Answer : " "));
                        $('input[name=StartOfCare_GenericDigestiveLastBMAsper][value=' + (result["GenericDigestiveLastBMAsper"] != null && result["GenericDigestiveLastBMAsper"] != undefined ? result["GenericDigestiveLastBMAsper"].Answer : "") + ']').attr('checked', true);
                        var digestiveLastBM = result["GenericDigestiveLastBM"];
                        if (digestiveLastBM !== null && digestiveLastBM != undefined && digestiveLastBM.Answer != null) {
                            var digestiveLastBMArray = (digestiveLastBM.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < digestiveLastBMArray.length; j++) {
                                $('input[name=StartOfCare_GenericDigestiveLastBM][value=' + digestiveLastBMArray[j] + ']').attr('checked', true);
                                if (digestiveLastBMArray[j] == 2) {
                                    var digestiveLastBMAbnormalStool = result["GenericDigestiveLastBMAbnormalStool"];
                                    if (digestiveLastBMAbnormalStool !== null && digestiveLastBMAbnormalStool != undefined && digestiveLastBMAbnormalStool.Answer != null) {
                                        var digestiveLastBMAbnormalStoolArray = (digestiveLastBMAbnormalStool.Answer).split(',');
                                        var k = 0;
                                        for (k = 0; k < digestiveLastBMAbnormalStoolArray.length; k++) {
                                            $('input[name=StartOfCare_GenericDigestiveLastBMAbnormalStool][value=' + digestiveLastBMAbnormalStoolArray[k] + ']').attr('checked', true);

                                        }
                                    }
                                }
                                if (digestiveLastBMArray[j] == 3) {
                                    $('input[name=StartOfCare_GenericDigestiveLastBMConstipationType][value=' + (result["GenericDigestiveLastBMConstipationType"] != null && result["GenericDigestiveLastBMConstipationType"] != undefined ? result["GenericDigestiveLastBMAsper"].Answer : "") + ']').attr('checked', true);
                                }
                                if (digestiveLastBMArray[j] == 4) {
                                    $("#StartOfCare_GenericDigestiveLaxEnemaUseDesc").val((result["GenericDigestiveLaxEnemaUseDesc"] !== null && result["GenericDigestiveLaxEnemaUseDesc"] !== undefined ? result["GenericDigestiveLaxEnemaUseDesc"].Answer : " "));
                                }
                                if (digestiveLastBMArray[j] == 5) {
                                    $('input[name=StartOfCare_GenericDigestiveHemorrhoidsType][value=' + (result["GenericDigestiveHemorrhoidsType"] != null && result["GenericDigestiveHemorrhoidsType"] != undefined ? result["GenericDigestiveHemorrhoidsType"].Answer : "") + ']').attr('checked', true);
                                }
                            }
                        }
                    }
                    if (digestiveArray[i] == 11) {
                        $("#StartOfCare_GenericDigestiveOstomyType").val((result["GenericDigestiveOstomyType"] !== null && result["GenericDigestiveOstomyType"] !== undefined ? result["GenericDigestiveOstomyType"].Answer : " "));
                        var digestiveOstomy = result["GenericDigestiveOstomy"];
                        if (digestiveOstomy !== null && digestiveOstomy != undefined && digestiveOstomy.Answer != null) {
                            var digestiveOstomyArray = (digestiveOstomy.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < digestiveOstomyArray.length; j++) {
                                $('input[name=StartOfCare_GenericDigestiveOstomy][value=' + digestiveOstomyArray[j] + ']').attr('checked', true);

                                if (digestiveOstomyArray[j] == 1) {
                                    $("#StartOfCare_GenericDigestiveStomaType").val((result["GenericDigestiveStomaType"] !== null && result["GenericDigestiveStomaType"] !== undefined ? result["GenericDigestiveStomaType"].Answer : " "));
                                }
                                if (digestiveOstomyArray[j] == 2) {
                                    $("#StartOfCare_GenericDigestiveStoolAppearanceDesc").val((result["GenericDigestiveStoolAppearanceDesc"] !== null && result["GenericDigestiveStoolAppearanceDesc"] !== undefined ? result["GenericDigestiveStoolAppearanceDesc"].Answer : " "));
                                }
                                if (digestiveOstomyArray[j] == 3) {
                                    $("#StartOfCare_GenericDigestiveSurSkinType").val((result["GenericDigestiveSurSkinType"] !== null && result["GenericDigestiveSurSkinType"] !== undefined ? result["GenericDigestiveSurSkinType"].Answer : " "));
                                    $('input[name=StartOfCare_GenericDigestiveSurSkinIntact][value=' + (result["GenericDigestiveSurSkinIntact"] != null && result["GenericDigestiveSurSkinIntact"] != undefined ? result["GenericDigestiveSurSkinIntact"].Answer : "") + ']').attr('checked', true);
                                }
                            }
                        }
                    }

                }
            }
            $("#StartOfCare_GenericGUDigestiveComments").val((result["GenericGUDigestiveComments"] !== null && result["GenericGUDigestiveComments"] !== undefined ? result["GenericGUDigestiveComments"].Answer : " "));
            $('input[name=StartOfCare_GenericPatientOnDialysis][value=' + (result["GenericPatientOnDialysis"] != null && result["GenericPatientOnDialysis"] != undefined ? result["GenericPatientOnDialysis"].Answer : "") + ']').attr('checked', true);

            var dialysis = result["GenericDialysis"];
            if (dialysis !== null && dialysis != undefined && dialysis.Answer != null) {
                var dialysisArray = (dialysis.Answer).split(',');
                var i = 0;
                for (i = 0; i < dialysisArray.length; i++) {
                    $('input[name=StartOfCare_GenericDialysis][value=' + dialysisArray[i] + ']').attr('checked', true);
                    if (dialysisArray[i] == 2) {
                        $("#StartOfCare_GenericAVGraftSite").val((result["GenericAVGraftSite"] !== null && result["GenericAVGraftSite"] !== undefined ? result["GenericAVGraftSite"].Answer : " "));
                    }
                    if (dialysisArray[i] == 3) {
                        $("#StartOfCare_GenericVenousCatheterSite").val((result["GenericVenousCatheterSite"] !== null && result["GenericVenousCatheterSite"] !== undefined ? result["GenericVenousCatheterSite"].Answer : " "));
                    }
                    if (dialysisArray[i] == 9) {
                        $("#StartOfCare_GenericDialysisOtherDesc").val((result["GenericDialysisOtherDesc"] !== null && result["GenericDialysisOtherDesc"] !== undefined ? result["GenericDialysisOtherDesc"].Answer : " "));
                    }
                }
            }
            $("#StartOfCare_GenericDialysisCenter").val((result["GenericDialysisCenter"] !== null && result["GenericDialysisCenter"] !== undefined ? result["GenericDialysisCenter"].Answer : " "));
            $("#StartOfCare_GenericDialysisCenterPhone").val((result["GenericDialysisCenterPhone"] !== null && result["GenericDialysisCenterPhone"] !== undefined ? result["GenericDialysisCenterPhone"].Answer : " "));
            $("#StartOfCare_GenericDialysisCenterContact").val((result["GenericDialysisCenterContact"] !== null && result["GenericDialysisCenterContact"] !== undefined ? result["GenericDialysisCenterContact"].Answer : " "));


            var eliminationStatusInterventions = result["485EliminationStatusInterventions"];
            if (eliminationStatusInterventions !== null && eliminationStatusInterventions != undefined && eliminationStatusInterventions.Answer != null) {
                var eliminationStatusInterventionsArray = (eliminationStatusInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < eliminationStatusInterventionsArray.length; i++) {
                    $('input[name=StartOfCare_485EliminationStatusInterventions][value=' + eliminationStatusInterventionsArray[i] + ']').attr('checked', true);
                    if (eliminationStatusInterventionsArray[i] == 2) {
                        $("#StartOfCare_485InstructUTISymptomsPerson").val((result["485InstructUTISymptomsPerson"] !== null && result["485InstructUTISymptomsPerson"] !== undefined ? result["485InstructUTISymptomsPerson"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 3) {
                        $("#StartOfCare_485ChangeFoleyCatheterWith").val((result["485ChangeFoleyCatheterWith"] !== null && result["485ChangeFoleyCatheterWith"] !== undefined ? result["485ChangeFoleyCatheterWith"].Answer : " "));
                        $("#StartOfCare_485ChangeFoleyCatheterQuantity").val((result["485ChangeFoleyCatheterQuantity"] !== null && result["485ChangeFoleyCatheterQuantity"] !== undefined ? result["485ChangeFoleyCatheterQuantity"].Answer : " "));
                        $("#StartOfCare_485ChangeFoleyCatheterFrequency").val((result["485ChangeFoleyCatheterFrequency"] !== null && result["485ChangeFoleyCatheterFrequency"] !== undefined ? result["485ChangeFoleyCatheterFrequency"].Answer : " "));
                        $("#StartOfCare_485ChangeFoleyCatheterDate").val((result["485ChangeFoleyCatheterDate"] !== null && result["485ChangeFoleyCatheterDate"] !== undefined ? result["485ChangeFoleyCatheterDate"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 4) {
                        $("#StartOfCare_485ChangeSuprapubicTubeWith").val((result["485ChangeSuprapubicTubeWith"] !== null && result["485ChangeSuprapubicTubeWith"] !== undefined ? result["485ChangeSuprapubicTubeWith"].Answer : " "));
                        $("#StartOfCare_485ChangeSuprapubicTubeQuantity").val((result["485ChangeSuprapubicTubeQuantity"] !== null && result["485ChangeSuprapubicTubeQuantity"] !== undefined ? result["485ChangeSuprapubicTubeQuantity"].Answer : " "));
                        $("#StartOfCare_485ChangeSuprapubicTubeFrequency").val((result["485ChangeSuprapubicTubeFrequency"] !== null && result["485ChangeSuprapubicTubeFrequency"] !== undefined ? result["485ChangeSuprapubicTubeFrequency"].Answer : " "));
                        $("#StartOfCare_485ChangeSuprapubicTubeDate").val((result["485ChangeSuprapubicTubeDate"] !== null && result["485ChangeSuprapubicTubeDate"] !== undefined ? result["485ChangeSuprapubicTubeDate"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 7) {
                        $("#StartOfCare_485EliminationStatusInterventions").val((result["485EliminationStatusInterventions"] !== null && result["485EliminationStatusInterventions"] !== undefined ? result["485EliminationStatusInterventions"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 8) {
                        $("#StartOfCare_485AllowFoleyVisitsAmount").val((result["485AllowFoleyVisitsAmount"] !== null && result["485AllowFoleyVisitsAmount"] !== undefined ? result["485AllowFoleyVisitsAmount"].Answer : " "));

                    }
                    if (eliminationStatusInterventionsArray[i] == 9) {
                        $("#StartOfCare_485InstructOstomyManagementDesc").val((result["485InstructOstomyManagementDesc"] !== null && result["485InstructOstomyManagementDesc"] !== undefined ? result["485InstructOstomyManagementDesc"].Answer : " "));

                    }

                    if (eliminationStatusInterventionsArray[i] == 10) {
                        $("#StartOfCare_485PerformOstomyCareDesc").val((result["485PerformOstomyCareDesc"] !== null && result["485PerformOstomyCareDesc"] !== undefined ? result["485PerformOstomyCareDesc"].Answer : " "));

                    }
                    if (eliminationStatusInterventionsArray[i] == 11) {
                        $("#StartOfCare_485DigitalDisimpactDays").val((result["485DigitalDisimpactDays"] !== null && result["485DigitalDisimpactDays"] !== undefined ? result["485DigitalDisimpactDays"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 12) {
                        $("#StartOfCare_485InstructMeasureIntakeOutputPerson").val((result["485InstructMeasureIntakeOutputPerson"] !== null && result["485InstructMeasureIntakeOutputPerson"] !== undefined ? result["485InstructMeasureIntakeOutputPerson"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 14) {
                        $("#StartOfCare_485AdministerEnemaType").val((result["485AdministerEnemaType"] !== null && result["485AdministerEnemaType"] !== undefined ? result["485AdministerEnemaType"].Answer : " "));
                        $("#StartOfCare_485AdministerEnemaDays").val((result["485AdministerEnemaDays"] !== null && result["485AdministerEnemaDays"] !== undefined ? result["485AdministerEnemaDays"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 15) {
                        $("#StartOfCare_485InstructConstipationSignsPerson").val((result["485InstructConstipationSignsPerson"] !== null && result["485InstructConstipationSignsPerson"] !== undefined ? result["485InstructConstipationSignsPerson"].Answer : " "));
                    }
                    if (eliminationStatusInterventionsArray[i] == 16) {
                        $("#StartOfCare_485InstructAcidRefluxFoodsPerson").val((result["485InstructAcidRefluxFoodsPerson"] !== null && result["485InstructAcidRefluxFoodsPerson"] !== undefined ? result["485InstructAcidRefluxFoodsPerson"].Answer : " "));
                    }
                }
            }
            $("#StartOfCare_485EliminationInterventionTemplates").val((result["485EliminationInterventionTemplates"] !== null && result["485EliminationInterventionTemplates"] !== undefined ? result["485EliminationInterventionTemplates"].Answer : " "));
            $("#StartOfCare_485EliminationInterventionComments").val((result["485EliminationInterventionComments"] !== null && result["485EliminationInterventionComments"] !== undefined ? result["485EliminationInterventionComments"].Answer : " "));

            var eliminationStatusGoals = result["485EliminationStatusGoals"];
            if (eliminationStatusGoals !== null && eliminationStatusGoals != undefined && eliminationStatusGoals.Answer != null) {
                var eliminationStatusGoalsArray = (eliminationStatusGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < eliminationStatusGoalsArray.length; i++) {
                    $('input[name=StartOfCare_485EliminationStatusGoals][value=' + eliminationStatusGoalsArray[i] + ']').attr('checked', true);
                    if (eliminationStatusGoalsArray[i] == 4) {
                        $("#StartOfCare_485IndependentOstomyManagementPerson").val((result["485IndependentOstomyManagementPerson"] !== null && result["485IndependentOstomyManagementPerson"] !== undefined ? result["485IndependentOstomyManagementPerson"].Answer : " "));
                        $("#StartOfCare_485IndependentOstomyManagementDate").val((result["485IndependentOstomyManagementDate"] !== null && result["485IndependentOstomyManagementDate"] !== undefined ? result["485IndependentOstomyManagementDate"].Answer : " "));
                    }
                    if (eliminationStatusGoalsArray[i] == 6) {
                        $("#StartOfCare_485VerbalizeAcidRefluxFoodPerson").val((result["485VerbalizeAcidRefluxFoodPerson"] !== null && result["485VerbalizeAcidRefluxFoodPerson"] !== undefined ? result["485VerbalizeAcidRefluxFoodPerson"].Answer : " "));
                        $("#StartOfCare_485VerbalizeAcidRefluxFoodDate").val((result["485VerbalizeAcidRefluxFoodDate"] !== null && result["485VerbalizeAcidRefluxFoodDate"] !== undefined ? result["485VerbalizeAcidRefluxFoodDate"].Answer : " "));
                    }
                    if (eliminationStatusGoalsArray[i] == 7) {
                        $("#StartOfCare_485VerbalizeReduceAcidRefluxDate").val((result["485VerbalizeReduceAcidRefluxDate"] !== null && result["485VerbalizeReduceAcidRefluxDate"] !== undefined ? result["485VerbalizeReduceAcidRefluxDate"].Answer : " "));
                    }
                }
            }
            $("#StartOfCare_485EliminationGoalTemplates").val((result["485EliminationGoalTemplates"] !== null && result["485EliminationGoalTemplates"] !== undefined ? result["485EliminationGoalTemplates"].Answer : " "));
            $("#StartOfCare_485EliminationGoalComments").val((result["485EliminationGoalComments"] !== null && result["485EliminationGoalComments"] !== undefined ? result["485EliminationGoalComments"].Answer : " "));

            var nutrition = result["GenericNutrition"];
            if (nutrition !== null && nutrition != undefined && nutrition.Answer != null) {
                var nutritionArray = (nutrition.Answer).split(',');
                var i = 0;
                for (i = 0; i < nutritionArray.length; i++) {
                    $('input[name=StartOfCare_GenericNutrition][value=' + nutritionArray[i] + ']').attr('checked', true);
                    if (nutritionArray[i] == 4) {
                        $('input[name=StartOfCare_GenericNutritionWeightGainLoss][value=' + (result["GenericNutritionWeightGainLoss"] != null && result["GenericNutritionWeightGainLoss"] != undefined ? result["GenericNutritionWeightGainLoss"].Answer : "") + ']').attr('checked', true);
                        $("#StartOfCare_GenericNutritionWeightAmount").val((result["GenericNutritionWeightAmount"] !== null && result["GenericNutritionWeightAmount"] !== undefined ? result["GenericNutritionWeightAmount"].Answer : " "));
                        $("#StartOfCare_GenericNutritionWeightAmountIn").val((result["GenericNutritionWeightAmountIn"] !== null && result["GenericNutritionWeightAmountIn"] !== undefined ? result["GenericNutritionWeightAmountIn"].Answer : " "));
                    }
                    if (nutritionArray[i] == 6) {
                        $('input[name=StartOfCare_GenericNutritionDietAdequate][value=' + (result["GenericNutritionDietAdequate"] != null && result["GenericNutritionDietAdequate"] != undefined ? result["GenericNutritionDietAdequate"].Answer : "") + ']').attr('checked', true);
                        var nutritionDiet = result["GenericNutritionDiet"];
                        if (nutritionDiet !== null && nutritionDiet != undefined && nutritionDiet.Answer != null) {
                            var nutritionDietArray = (nutritionDiet.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < nutritionDietArray.length; j++) {
                                $('input[name=StartOfCare_GenericNutritionDiet][value=' + nutritionDietArray[j] + ']').attr('checked', true);

                            }
                        }
                    }

                    if (nutritionArray[i] == 7) {
                        $("#StartOfCare_GenericNutritionResidualCheckedAmount").val((result["GenericNutritionResidualCheckedAmount"] !== null && result["GenericNutritionResidualCheckedAmount"] !== undefined ? result["GenericNutritionResidualCheckedAmount"].Answer : " "));
                        var nutritionResidualProblem = result["GenericNutritionResidualProblem"];
                        if (nutritionResidualProblem !== null && nutritionResidualProblem != undefined && nutritionResidualProblem.Answer != null) {
                            var nutritionResidualProblemArray = (nutritionResidualProblem.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < nutritionResidualProblemArray.length; j++) {
                                $('input[name=StartOfCare_GenericNutritionResidualProblem][value=' + nutritionResidualProblemArray[j] + ']').attr('checked', true);

                                if (nutritionResidualProblemArray[j] == 7) {
                                    $("#StartOfCare_GenericNutritionOtherDetails").val((result["GenericNutritionOtherDetails"] !== null && result["GenericNutritionOtherDetails"] !== undefined ? result["GenericNutritionOtherDetails"].Answer : " "));
                                }
                            }
                        }
                    }

                }
            }
            $("#StartOfCare_GenericNutritionComments").val((result["GenericNutritionComments"] !== null && result["GenericNutritionComments"] !== undefined ? result["GenericNutritionComments"].Answer : " "));

            var nutritionalHealth = result["GenericNutritionalHealth"];
            if (nutritionalHealth !== null && nutritionalHealth != undefined && nutritionalHealth.Answer != null) {
                var nutritionalHealthArray = (nutritionalHealth.Answer).split(',');
                var j = 0;
                for (j = 0; j < nutritionalHealthArray.length; j++) {
                    $('input[name=StartOfCare_GenericNutritionalHealth][value=' + nutritionalHealthArray[j] + ']').attr('checked', true);

                }
            }
            Oasis.CalculateNutritionScore('StartOfCare');
            $("#StartOfCare_GenericNutritionalStatusComments").val((result["GenericNutritionComments"] !== null && result["GenericNutritionComments"] !== undefined ? result["GenericNutritionComments"].Answer : " "));
            var nutritionDiffect = result["GenericNutritionDiffect"];
            if (nutritionDiffect !== null && nutritionDiffect != undefined && nutritionDiffect.Answer != null) {
                var nutritionDiffectArray = (nutritionDiffect.Answer).split(',');
                var j = 0;
                for (j = 0; j < nutritionDiffectArray.length; j++) {
                    $('input[name=StartOfCare_GenericNutritionDiffect][value=' + nutritionDiffectArray[j] + ']').attr('checked', true);

                }
            }
            $("#StartOfCare_GenericMealsPreparedBy").val((result["GenericNutritionComments"] !== null && result["GenericNutritionComments"] !== undefined ? result["GenericNutritionComments"].Answer : " "));

            var requirements = result["485NutritionalReqs"];
            if (requirements !== null && requirements != undefined && requirements.Answer != null) {
                var requirementsArray = (requirements.Answer).split(',');
                var i = 0;
                for (i = 0; i < requirementsArray.length; i++) {
                    $('input[name=StartOfCare_485NutritionalReqs][value=' + requirementsArray[i] + ']').attr('checked', true);
                    if (requirementsArray[i] == 1) {
                        $("#StartOfCare_485NutritionalReqsSodiumAmount").val((result["485NutritionalReqsSodiumAmount"] !== null && result["485NutritionalReqsSodiumAmount"] !== undefined ? result["485NutritionalReqsSodiumAmount"].Answer : " "));
                    }
                    if (requirementsArray[i] == 3) {
                        $("#StartOfCare_485NutritionalReqsCalorieADADietAmount").val((result["485NutritionalReqsCalorieADADietAmount"] !== null && result["485NutritionalReqsCalorieADADietAmount"] !== undefined ? result["485NutritionalReqsCalorieADADietAmount"].Answer : " "));
                    }
                    if (requirementsArray[i] == 5) {
                        $("#StartOfCare_485NutritionalReqsHighProteinAmount").val((result["485NutritionalReqsHighProteinAmount"] !== null && result["485NutritionalReqsHighProteinAmount"] !== undefined ? result["485NutritionalReqsHighProteinAmount"].Answer : " "));

                    }
                    if (requirementsArray[i] == 6) {
                        $("#StartOfCare_485NutritionalReqsLowProteinAmount").val((result["485NutritionalReqsLowProteinAmount"] !== null && result["485NutritionalReqsLowProteinAmount"] !== undefined ? result["485NutritionalReqsLowProteinAmount"].Answer : " "));
                    }
                    if (requirementsArray[i] == 7) {
                        $('input[name=StartOfCare_485NutritionalReqsCarbLevel][value=' + (result["485NutritionalReqsCarbLevel"] != null && result["485NutritionalReqsCarbLevel"] != undefined ? result["485NutritionalReqsCarbLevel"].Answer : "") + ']').attr('checked', true);
                    }
                    if (requirementsArray[i] == 10) {
                        $("#StartOfCare_485NutritionalReqsSupplementType").val((result["485NutritionalReqsSupplementType"] !== null && result["485NutritionalReqsSupplementType"] !== undefined ? result["485NutritionalReqsSupplementType"].Answer : " "));

                    }
                    if (requirementsArray[i] == 13) {
                        $("#StartOfCare_485NutritionalReqsFluidResAmount").val((result["485NutritionalReqsFluidResAmount"] !== null && result["485NutritionalReqsFluidResAmount"] !== undefined ? result["485NutritionalReqsFluidResAmount"].Answer : " "));
                    }
                    if (requirementsArray[i] == 14) {
                        $("#StartOfCare_485NutritionalReqsPhyDietOtherName").val((result["485NutritionalReqsPhyDietOtherName"] !== null && result["485NutritionalReqsPhyDietOtherName"] !== undefined ? result["485NutritionalReqsPhyDietOtherName"].Answer : " "));
                    }

                    if (requirementsArray[i] == 19) {
                        $("#StartOfCare_485NutritionalReqsEnteralDesc").val((result["485NutritionalReqsEnteralDesc"] !== null && result["485NutritionalReqsEnteralDesc"] !== undefined ? result["485NutritionalReqsEnteralDesc"].Answer : " "));
                        $("#StartOfCare_485NutritionalReqsEnteralAmount").val((result["485NutritionalReqsEnteralAmount"] !== null && result["485NutritionalReqsEnteralAmount"] !== undefined ? result["485NutritionalReqsEnteralAmount"].Answer : " "));
                        var enteralNutrition = result["StartOfCare_485NutritionalReqsEnteral"];
                        if (enteralNutrition !== null && enteralNutrition != undefined && enteralNutrition.Answer != null) {
                            var enteralNutritionArray = (enteralNutrition.Answer).split(',');
                            var j = 0;
                            for (j = 0; j < enteralNutritionArray.length; j++) {
                                $('input[name=StartOfCare_StartOfCare_485NutritionalReqsEnteral][value=' + enteralNutritionArray[j] + ']').attr('checked', true);
                                if (enteralNutritionArray[j] == 1) {
                                    $("#StartOfCare_485NutritionalReqsEnteralPumpType").val((result["485NutritionalReqsEnteralPumpType"] !== null && result["485NutritionalReqsEnteralPumpType"] !== undefined ? result["485NutritionalReqsEnteralPumpType"].Answer : " "));
                                }
                            }
                        }

                    }

                    if (requirementsArray[i] == 20) {
                        $("#StartOfCare_485NutritionalReqsTPNAmount").val((result["485NutritionalReqsTPNAmount"] !== null && result["485NutritionalReqsTPNAmount"] !== undefined ? result["485NutritionalReqsTPNAmount"].Answer : " "));
                        $("#StartOfCare_485NutritionalReqsTPNVia").val((result["485NutritionalReqsTPNVia"] !== null && result["485NutritionalReqsTPNVia"] !== undefined ? result["485NutritionalReqsTPNVia"].Answer : " "));
                    }
                }
            }

            var nutritionInterventions = result["485NutritionInterventions"];
            if (nutritionInterventions !== null && nutritionInterventions != undefined && nutritionInterventions.Answer != null) {
                var nutritionInterventionsArray = (nutritionInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < nutritionInterventionsArray.length; i++) {
                    $('input[name=StartOfCare_485NutritionInterventions][value=' + nutritionInterventionsArray[i] + ']').attr('checked', true);
                    if (nutritionInterventionsArray[i] == 1) {
                        $("#StartOfCare_485InstructOnDietPerson").val((result["485InstructOnDietPerson"] !== null && result["485InstructOnDietPerson"] !== undefined ? result["485InstructOnDietPerson"].Answer : " "));
                        $("#StartOfCare_485InstructOnDietDesc").val((result["485InstructOnDietDesc"] !== null && result["485InstructOnDietDesc"] !== undefined ? result["485InstructOnDietDesc"].Answer : " "));
                    }
                    if (nutritionInterventionsArray[i] == 3) {
                        $("#StartOfCare_485InstructKeepDietLogPerson").val((result["485InstructKeepDietLogPerson"] !== null && result["485InstructKeepDietLogPerson"] !== undefined ? result["485InstructKeepDietLogPerson"].Answer : " "));

                    }
                    if (nutritionInterventionsArray[i] == 4) {
                        $("#StartOfCare_485InstructPromoteOralIntakePerson").val((result["485InstructPromoteOralIntakePerson"] !== null && result["485InstructPromoteOralIntakePerson"] !== undefined ? result["485InstructPromoteOralIntakePerson"].Answer : " "));
                    }
                    if (nutritionInterventionsArray[i] == 5) {
                        $("#StartOfCare_485InstructParenteralNutritionPerson").val((result["485InstructParenteralNutritionPerson"] !== null && result["485InstructParenteralNutritionPerson"] !== undefined ? result["485InstructParenteralNutritionPerson"].Answer : " "));
                        $("#StartOfCare_485InstructParenteralNutritionInclude").val((result["485InstructParenteralNutritionInclude"] !== null && result["485InstructParenteralNutritionInclude"] !== undefined ? result["485InstructParenteralNutritionInclude"].Answer : " "));
                    }
                    if (nutritionInterventionsArray[i] == 6) {
                        $("#StartOfCare_485InstructEnteralNutritionPerson").val((result["485InstructEnteralNutritionPerson"] !== null && result["485InstructEnteralNutritionPerson"] !== undefined ? result["485InstructEnteralNutritionPerson"].Answer : " "));
                        $("#StartOfCare_485InstructEnteralNutritionInclude").val((result["485InstructEnteralNutritionInclude"] !== null && result["485InstructEnteralNutritionInclude"] !== undefined ? result["485InstructEnteralNutritionInclude"].Answer : " "));

                    }
                    if (nutritionInterventionsArray[i] == 7) {
                        $("#StartOfCare_485InstructCareOfTubePerson").val((result["485InstructCareOfTubePerson"] !== null && result["485InstructCareOfTubePerson"] !== undefined ? result["485InstructCareOfTubePerson"].Answer : " "));
                        $("#StartOfCare_485InstructCareOfTubeDesc").val((result["485InstructCareOfTubeDesc"] !== null && result["485InstructCareOfTubeDesc"] !== undefined ? result["485InstructCareOfTubeDesc"].Answer : " "));

                    }
                    if (nutritionInterventionsArray[i] == 8) {
                        $("#StartOfCare_485ChangeTubeEveryType").val((result["485ChangeTubeEveryType"] !== null && result["485ChangeTubeEveryType"] !== undefined ? result["485ChangeTubeEveryType"].Answer : " "));
                        $("#StartOfCare_485ChangeTubeEveryFreq").val((result["485ChangeTubeEveryFreq"] !== null && result["485ChangeTubeEveryFreq"] !== undefined ? result["485ChangeTubeEveryFreq"].Answer : " "));
                        $("#StartOfCare_485ChangeTubeEveryDate").val((result["485ChangeTubeEveryDate"] !== null && result["485ChangeTubeEveryDate"] !== undefined ? result["485ChangeTubeEveryDate"].Answer : " "));

                    }

                    if (nutritionInterventionsArray[i] == 9) {
                        $("#StartOfCare_485IrrigateTubeWithType").val((result["485IrrigateTubeWithType"] !== null && result["485IrrigateTubeWithType"] !== undefined ? result["485IrrigateTubeWithType"].Answer : " "));
                        $("#StartOfCare_485IrrigateTubeWithAmount").val((result["485IrrigateTubeWithAmount"] !== null && result["485IrrigateTubeWithAmount"] !== undefined ? result["485IrrigateTubeWithAmount"].Answer : " "));
                        $("#StartOfCare_485IrrigateTubeWithDesc").val((result["485IrrigateTubeWithDesc"] !== null && result["485IrrigateTubeWithDesc"] !== undefined ? result["485IrrigateTubeWithDesc"].Answer : " "));
                        $("#StartOfCare_485IrrigateTubeWithEvery").val((result["485IrrigateTubeWithEvery"] !== null && result["485IrrigateTubeWithEvery"] !== undefined ? result["485IrrigateTubeWithEvery"].Answer : " "));
                        $("#StartOfCare_485IrrigateTubeWithAsNeeded").val((result["485IrrigateTubeWithAsNeeded"] !== null && result["485IrrigateTubeWithAsNeeded"] !== undefined ? result["485IrrigateTubeWithAsNeeded"].Answer : " "));
                        $('input[name=StartOfCare_485IrrigateTubeWithFreq][value=' + (result["485IrrigateTubeWithFreq"] != null && result["485IrrigateTubeWithFreq"] != undefined ? result["485IrrigateTubeWithFreq"].Answer : "") + ']').attr('checked', true);
                    }
                    if (nutritionInterventionsArray[i] == 10) {
                        $("#StartOfCare_485InstructFreeWaterPerson").val((result["485InstructFreeWaterPerson"] !== null && result["485InstructFreeWaterPerson"] !== undefined ? result["485InstructFreeWaterPerson"].Answer : " "));
                        $("#StartOfCare_485InstructFreeWaterAmount").val((result["485InstructFreeWaterAmount"] !== null && result["485InstructFreeWaterAmount"] !== undefined ? result["485InstructFreeWaterAmount"].Answer : " "));
                        $("#StartOfCare_485InstructFreeWaterEvery").val((result["485InstructFreeWaterEvery"] !== null && result["485InstructFreeWaterEvery"] !== undefined ? result["485InstructFreeWaterEvery"].Answer : " "));
                    }
                }
            }
            $("#StartOfCare_485NutritionOrderTemplates").val((result["485NutritionOrderTemplates"] !== null && result["485NutritionOrderTemplates"] !== undefined ? result["485NutritionOrderTemplates"].Answer : " "));
            $("#StartOfCare_485NutritionComments").val((result["485NutritionComments"] !== null && result["485NutritionComments"] !== undefined ? result["485NutritionComments"].Answer : " "));

            var nutritionGoals = result["485NutritionGoals"];
            if (nutritionGoals !== null && nutritionGoals != undefined && nutritionGoals.Answer != null) {
                var nutritionGoalsArray = (nutritionGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < nutritionGoalsArray.length; i++) {
                    $('input[name=StartOfCare_485NutritionGoals][value=' + nutritionGoalsArray[i] + ']').attr('checked', true);
                    if (nutritionGoalsArray[i] == 1) {
                        $("#StartOfCare_485MaintainDietComplianceType").val((result["485MaintainDietComplianceType"] !== null && result["485MaintainDietComplianceType"] !== undefined ? result["485MaintainDietComplianceType"].Answer : " "));
                    }
                    if (nutritionGoalsArray[i] == 2) {
                        $("#StartOfCare_485DemonstrateDietCompliancePerson").val((result["485DemonstrateDietCompliancePerson"] !== null && result["485DemonstrateDietCompliancePerson"] !== undefined ? result["485DemonstrateDietCompliancePerson"].Answer : " "));
                    }
                    if (nutritionGoalsArray[i] == 3) {
                        $("#StartOfCare_485DemonstrateEnteralNutritionPerson").val((result["485DemonstrateEnteralNutritionPerson"] !== null && result["485DemonstrateEnteralNutritionPerson"] !== undefined ? result["485DemonstrateEnteralNutritionPerson"].Answer : " "));
                        $("#StartOfCare_485DemonstrateEnteralNutritionDate").val((result["485DemonstrateEnteralNutritionDate"] !== null && result["485DemonstrateEnteralNutritionDate"] !== undefined ? result["485DemonstrateEnteralNutritionDate"].Answer : " "));
                    }
                    if (nutritionGoalsArray[i] == 4) {
                        $("#StartOfCare_485DemonstrateParenteralNutritionPerson").val((result["485DemonstrateParenteralNutritionPerson"] !== null && result["485DemonstrateParenteralNutritionPerson"] !== undefined ? result["485DemonstrateParenteralNutritionPerson"].Answer : " "));
                        $("#StartOfCare_485DemonstrateParenteralNutritionDate").val((result["485DemonstrateParenteralNutritionDate"] !== null && result["485DemonstrateParenteralNutritionDate"] !== undefined ? result["485DemonstrateParenteralNutritionDate"].Answer : " "));
                    }
                    if (nutritionGoalsArray[i] == 5) {
                        $("#StartOfCare_485DemonstrateCareOfTubePerson").val((result["485DemonstrateCareOfTubePerson"] !== null && result["485DemonstrateCareOfTubePerson"] !== undefined ? result["485DemonstrateCareOfTubePerson"].Answer : " "));
                        $("#StartOfCare_485DemonstrateCareOfTubeType").val((result["485DemonstrateCareOfTubeType"] !== null && result["485DemonstrateCareOfTubeType"] !== undefined ? result["485DemonstrateCareOfTubeType"].Answer : " "));
                        $("#StartOfCare_485DemonstrateCareOfTubeDate").val((result["485DemonstrateCareOfTubeDate"] !== null && result["485DemonstrateCareOfTubeDate"] !== undefined ? result["485DemonstrateCareOfTubeDate"].Answer : " "));
                    }
                }
            }
            $("#StartOfCare_485NutritionGoalTemplates").val((result["485NutritionGoalTemplates"] !== null && result["485NutritionGoalTemplates"] !== undefined ? result["485NutritionGoalTemplates"].Answer : " "));
            $("#StartOfCare_485NutritionGoalComments").val((result["485NutritionGoalComments"] !== null && result["485NutritionGoalComments"] !== undefined ? result["485NutritionGoalComments"].Answer : " "));

            var neurologicalOriented = result["GenericNeurologicalOriented"];
            if (neurologicalOriented !== null && neurologicalOriented != undefined && neurologicalOriented.Answer != null) {
                var neurologicalOrientedArray = (neurologicalOriented.Answer).split(',');
                var j = 0;
                for (j = 0; j < neurologicalOrientedArray.length; j++) {
                    $('input[name=StartOfCare_GenericNeurologicalOriented][value=' + neurologicalOrientedArray[j] + ']').attr('checked', true);
                }
            }
            var neurologicalStatus = result["GenericNeurologicalStatus"];
            if (neurologicalStatus !== null && neurologicalStatus != undefined && neurologicalStatus.Answer != null) {
                var neurologicalStatusArray = (neurologicalStatus.Answer).split(',');
                var j = 0;
                for (j = 0; j < neurologicalStatusArray.length; j++) {
                    $('input[name=StartOfCare_GenericNeurologicalStatus][value=' + neurologicalStatusArray[j] + ']').attr('checked', true);
                    if (neurologicalStatusArray[j] == 5) {
                        $("#StartOfCare_GenericNeurologicalTremorsLocation").val((result["GenericNeurologicalTremorsLocation"] !== null && result["GenericNeurologicalTremorsLocation"] !== undefined ? result["GenericNeurologicalTremorsLocation"].Answer : " "));
                    }
                }
            }

            var psychosocial = result["GenericPsychosocial"];
            if (psychosocial !== null && psychosocial != undefined && psychosocial.Answer != null) {
                var psychosocialArray = (psychosocial.Answer).split(',');
                var j = 0;
                for (j = 0; j < psychosocialArray.length; j++) {
                    $('input[name=StartOfCare_GenericPsychosocial][value=' + psychosocialArray[j] + ']').attr('checked', true);
                }
            }

            $("#StartOfCare_GenericNeuroEmoBehaviorComments").val((result["GenericNeuroEmoBehaviorComments"] !== null && result["GenericNeuroEmoBehaviorComments"] !== undefined ? result["GenericNeuroEmoBehaviorComments"].Answer : " "));

            var behaviorInterventions = result["485BehaviorInterventions"];
            if (behaviorInterventions !== null && behaviorInterventions != undefined && behaviorInterventions.Answer != null) {
                var behaviorInterventionsArray = (behaviorInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < behaviorInterventionsArray.length; i++) {
                    $('input[name=StartOfCare_485BehaviorInterventions][value=' + behaviorInterventionsArray[i] + ']').attr('checked', true);
                    if (behaviorInterventionsArray[i] == 4) {
                        $("#StartOfCare_485InstructSeizurePrecautionPerson").val((result["485InstructSeizurePrecautionPerson"] !== null && result["485InstructSeizurePrecautionPerson"] !== undefined ? result["485InstructSeizurePrecautionPerson"].Answer : " "));
                    }
                    if (behaviorInterventionsArray[i] == 6) {
                        $('input[name=StartOfCare_485MSWProvideServiceNumberVisits][value=' + (result["485MSWProvideServiceNumberVisits"] != null && result["485MSWProvideServiceNumberVisits"] != undefined ? result["485MSWProvideServiceNumberVisits"].Answer : "") + ']').attr('checked', true);
                        $("#StartOfCare_485MSWProvideServiceVisitAmount").val((result["485MSWProvideServiceVisitAmount"] !== null && result["485MSWProvideServiceVisitAmount"] !== undefined ? result["485MSWProvideServiceVisitAmount"].Answer : " "));
                    }
                    if (behaviorInterventionsArray[i] == 7) {
                        $('input[name=StartOfCare_485MSWLongTermPlanningVisits][value=' + (result["485MSWLongTermPlanningVisits"] != null && result["485MSWLongTermPlanningVisits"] != undefined ? result["485MSWLongTermPlanningVisits"].Answer : "") + ']').attr('checked', true);
                        $("#StartOfCare_485MSWLongTermPlanningVisitAmount").val((result["485MSWLongTermPlanningVisitAmount"] !== null && result["485MSWLongTermPlanningVisitAmount"] !== undefined ? result["485MSWLongTermPlanningVisitAmount"].Answer : " "));
                    }
                    if (behaviorInterventionsArray[i] == 8) {
                        $('input[name=StartOfCare_485MSWCommunityAssistanceVisits][value=' + (result["485MSWCommunityAssistanceVisits"] != null && result["485MSWCommunityAssistanceVisits"] != undefined ? result["485MSWCommunityAssistanceVisits"].Answer : "") + ']').attr('checked', true);
                        $("#StartOfCare_485MSWCommunityAssistanceVisitAmount").val((result["485MSWCommunityAssistanceVisitAmount"] !== null && result["485MSWCommunityAssistanceVisitAmount"] !== undefined ? result["485MSWCommunityAssistanceVisitAmount"].Answer : " "));
                    }
                }
            }
            $("#StartOfCare_485BehaviorOrderTemplates").val((result["485BehaviorOrderTemplates"] !== null && result["485BehaviorOrderTemplates"] !== undefined ? result["485BehaviorOrderTemplates"].Answer : " "));
            $("#StartOfCare_485BehaviorComments").val((result["485BehaviorComments"] !== null && result["485BehaviorComments"] !== undefined ? result["485BehaviorComments"].Answer : " "));

            var behaviorGoals = result["485BehaviorGoals"];
            if (behaviorGoals !== null && behaviorGoals != undefined && behaviorGoals.Answer != null) {
                var behaviorGoalsArray = (behaviorGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < behaviorGoalsArray.length; i++) {
                    $('input[name=StartOfCare_485BehaviorGoals][value=' + behaviorGoalsArray[i] + ']').attr('checked', true);
                    if (behaviorGoalsArray[i] == 2) {
                        $("#StartOfCare_485VerbalizeSeizurePrecautionsPerson").val((result["485VerbalizeSeizurePrecautionsPerson"] !== null && result["485VerbalizeSeizurePrecautionsPerson"] !== undefined ? result["485VerbalizeSeizurePrecautionsPerson"].Answer : " "));
                    }
                }
            }

            var mentalStatus = result["485MentalStatus"];
            if (mentalStatus !== null && mentalStatus != undefined && mentalStatus.Answer != null) {
                var mentalStatusArray = (mentalStatus.Answer).split(',');
                var ms = 0;
                for (ms = 0; ms < mentalStatusArray.length; ms++) {
                    $('input[name=StartOfCare_485MentalStatus][value=' + mentalStatusArray[ms] + ']').attr('checked', true);
                    if (mentalStatusArray[ms] == 8) {
                        $("#StartOfCare_485MentalStatusOther").val((result["485MentalStatusOther"] !== null && result["485MentalStatusOther"] !== undefined ? result["485MentalStatusOther"].Answer : " "));
                    }
                }
            }

            $("#StartOfCare_485BehaviorGoalTemplates").val((result["485BehaviorGoalTemplates"] !== null && result["485BehaviorGoalTemplates"] !== undefined ? result["485BehaviorGoalTemplates"].Answer : " "));
            $("#StartOfCare_485BehaviorGoalComments").val((result["485BehaviorGoalComments"] !== null && result["485BehaviorGoalComments"] !== undefined ? result["485BehaviorGoalComments"].Answer : " "));

            var activitiesPermitted = result["485ActivitiesPermitted"];
            if (activitiesPermitted !== null && activitiesPermitted != undefined && activitiesPermitted.Answer != null) {
                var activitiesPermittedArray = (activitiesPermitted.Answer).split(',');
                var j = 0;
                for (j = 0; j < activitiesPermittedArray.length; j++) {
                    $('input[name=StartOfCare_485ActivitiesPermitted][value=' + activitiesPermittedArray[j] + ']').attr('checked', true);
                    if (activitiesPermittedArray[j] == 11) {
                        $("#StartOfCare_485ActivitiesPermittedOther").val((result["485ActivitiesPermittedOther"] !== null && result["485ActivitiesPermittedOther"] !== undefined ? result["485ActivitiesPermittedOther"].Answer : " "));
                    }
                }
            }

            var musculoskeletal = result["GenericMusculoskeletal"];
            if (musculoskeletal !== null && musculoskeletal != undefined && musculoskeletal.Answer != null) {
                var musculoskeletalArray = (musculoskeletal.Answer).split(',');
                var j = 0;
                for (j = 0; j < musculoskeletalArray.length; j++) {
                    $('input[name=StartOfCare_GenericMusculoskeletal][value=' + musculoskeletalArray[j] + ']').attr('checked', true);
                    if (musculoskeletalArray[j] == 4) {
                        $("#StartOfCare_GenericLimitedMobilityLocation").val((result["GenericLimitedMobilityLocation"] !== null && result["GenericLimitedMobilityLocation"] !== undefined ? result["GenericLimitedMobilityLocation"].Answer : " "));
                    }
                    if (musculoskeletalArray[j] == 5) {
                        $("#StartOfCare_GenericJointPainLocation").val((result["GenericJointPainLocation"] !== null && result["GenericJointPainLocation"] !== undefined ? result["GenericJointPainLocation"].Answer : " "));
                    }
                    if (musculoskeletalArray[j] == 7) {
                        $('input[name=StartOfCare_GenericGripStrengthEqual][value=' + (result["GenericGripStrengthEqual"] != null && result["GenericGripStrengthEqual"] != undefined ? result["GenericGripStrengthEqual"].Answer : "") + ']').attr('checked', true);
                        $("#StartOfCare_GenericGripStrengthDesc").val((result["GenericGripStrengthDesc"] !== null && result["GenericGripStrengthDesc"] !== undefined ? result["GenericGripStrengthDesc"].Answer : " "));
                    }
                }
            }

            var boundType = result["GenericBoundType"];
            if (boundType !== null && boundType != undefined && boundType.Answer != null) {
                var boundTypeArray = (boundType.Answer).split(',');
                var j = 0;
                for (j = 0; j < boundTypeArray.length; j++) {
                    $('input[name=StartOfCare_GenericBoundType][value=' + boundTypeArray[j] + ']').attr('checked', true);
                    if (boundTypeArray[j] == 3) {
                        $("#StartOfCare_GenericContractureLocation").val((result["GenericContractureLocation"] !== null && result["GenericContractureLocation"] !== undefined ? result["GenericContractureLocation"].Answer : " "));
                    }
                    if (boundTypeArray[j] == 4) {
                        $("#StartOfCare_GenericParalysisLocation").val((result["GenericParalysisLocation"] !== null && result["GenericParalysisLocation"] !== undefined ? result["GenericParalysisLocation"].Answer : " "));
                        $('input[name=StartOfCare_GenericParalysisDominant][value=' + (result["GenericParalysisDominant"] != null && result["GenericParalysisDominant"] != undefined ? result["GenericParalysisDominant"].Answer : "") + ']').attr('checked', true);
                    }
                    if (boundTypeArray[j] == 5) {

                        $("#StartOfCare_GenericAssistiveDeviceType").val((result["GenericAssistiveDeviceType"] !== null && result["GenericAssistiveDeviceType"] !== undefined ? result["GenericAssistiveDeviceType"].Answer : " "));
                    }
                }
            }
            $("#StartOfCare_GenericMusculoskeletalComments").val((result["GenericMusculoskeletalComments"] !== null && result["GenericMusculoskeletalComments"] !== undefined ? result["GenericMusculoskeletalComments"].Answer : " "));

            var nursingInterventions = result["485NursingInterventions"];
            if (nursingInterventions !== null && nursingInterventions != undefined && nursingInterventions.Answer != null) {
                var nursingInterventionsArray = (nursingInterventions.Answer).split(',');
                var j = 0;
                for (j = 0; j < nursingInterventionsArray.length; j++) {
                    $('input[name=StartOfCare_485NursingInterventions][value=' + nursingInterventionsArray[j] + ']').attr('checked', true);
                    if (nursingInterventionsArray[j] == 1) {
                        $("#StartOfCare_485PhysicalTherapyFreq").val((result["485PhysicalTherapyFreq"] !== null && result["485PhysicalTherapyFreq"] !== undefined ? result["485PhysicalTherapyFreq"].Answer : " "));
                        $("#StartOfCare_485PhysicalTherapyDate").val((result["485PhysicalTherapyDate"] !== null && result["485PhysicalTherapyDate"] !== undefined ? result["485PhysicalTherapyDate"].Answer : " "));
                    }
                    if (nursingInterventionsArray[j] == 2) {
                        $("#StartOfCare_485OccupationalTherapyFreq").val((result["485OccupationalTherapyFreq"] !== null && result["485OccupationalTherapyFreq"] !== undefined ? result["485OccupationalTherapyFreq"].Answer : " "));
                        $("#StartOfCare_485OccupationalTherapyDate").val((result["485OccupationalTherapyDate"] !== null && result["485OccupationalTherapyDate"] !== undefined ? result["485OccupationalTherapyDate"].Answer : " "));
                    }
                    if (nursingInterventionsArray[j] == 3) {

                        $("#StartOfCare_485HomeHealthAideFreq").val((result["485HomeHealthAideFreq"] !== null && result["485HomeHealthAideFreq"] !== undefined ? result["485HomeHealthAideFreq"].Answer : " "));
                    }
                    if (nursingInterventionsArray[j] == 6) {

                        $("#StartOfCare_485InstructRomExcercisePerson").val((result["485InstructRomExcercisePerson"] !== null && result["485InstructRomExcercisePerson"] !== undefined ? result["485InstructRomExcercisePerson"].Answer : " "));
                    }
                }
            }

            $("#StartOfCare_485ADLOrderTemplates").val((result["485ADLOrderTemplates"] !== null && result["485ADLOrderTemplates"] !== undefined ? result["485ADLOrderTemplates"].Answer : " "));
            $("#StartOfCare_485ADLComments").val((result["485ADLComments"] !== null && result["485ADLComments"] !== undefined ? result["485ADLComments"].Answer : " "));

            var nursingGoals = result["485NursingGoals"];
            if (nursingGoals !== null && nursingGoals != undefined && nursingGoals.Answer != null) {
                var nursingGoalsArray = (nursingGoals.Answer).split(',');
                var j = 0;
                for (j = 0; j < nursingGoalsArray.length; j++) {
                    $('input[name=StartOfCare_485NursingGoals][value=' + nursingGoalsArray[j] + ']').attr('checked', true);
                    if (nursingGoalsArray[j] == 4) {
                        $("#StartOfCare_485DemonstrateROMExcercisePerson").val((result["485DemonstrateROMExcercisePerson"] !== null && result["485DemonstrateROMExcercisePerson"] !== undefined ? result["485DemonstrateROMExcercisePerson"].Answer : " "));
                    }
                }
            }
            $("#StartOfCare_485ADLGoalTemplates").val((result["485ADLGoalTemplates"] !== null && result["485ADLGoalTemplates"] !== undefined ? result["485ADLGoalTemplates"].Answer : " "));
            $("#StartOfCare_485ADLGoalComments").val((result["485ADLGoalComments"] !== null && result["485ADLGoalComments"] !== undefined ? result["485ADLGoalComments"].Answer : " "));

            $('input[name=StartOfCare_GenericAge65Plus][value=' + (result["GenericAge65Plus"] != null && result["GenericAge65Plus"] != undefined ? result["GenericAge65Plus"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericHypotensionDiagnosis][value=' + (result["GenericHypotensionDiagnosis"] != null && result["GenericHypotensionDiagnosis"] != undefined ? result["GenericHypotensionDiagnosis"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericPriorFalls][value=' + (result["GenericPriorFalls"] != null && result["GenericPriorFalls"] != undefined ? result["GenericPriorFalls"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericFallIncontinence][value=' + (result["GenericFallIncontinence"] != null && result["GenericFallIncontinence"] != undefined ? result["GenericFallIncontinence"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericVisualImpairment][value=' + (result["GenericVisualImpairment"] != null && result["GenericVisualImpairment"] != undefined ? result["GenericVisualImpairment"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericImpairedFunctionalMobility][value=' + (result["GenericImpairedFunctionalMobility"] != null && result["GenericImpairedFunctionalMobility"] != undefined ? result["GenericImpairedFunctionalMobility"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericEnvHazards][value=' + (result["GenericEnvHazards"] != null && result["GenericEnvHazards"] != undefined ? result["GenericEnvHazards"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericPolyPharmacy][value=' + (result["GenericPolyPharmacy"] != null && result["GenericPolyPharmacy"] != undefined ? result["GenericPolyPharmacy"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericPainAffectingFunction][value=' + (result["GenericPainAffectingFunction"] != null && result["GenericPainAffectingFunction"] != undefined ? result["GenericPainAffectingFunction"].Answer : "") + ']').attr('checked', true);
            $('input[name=StartOfCare_GenericCognitiveImpairment][value=' + (result["GenericCognitiveImpairment"] != null && result["GenericCognitiveImpairment"] != undefined ? result["GenericCognitiveImpairment"].Answer : "") + ']').attr('checked', true);

            var instructInterventions = result["485InstructInterventions"];
            if (instructInterventions !== null && instructInterventions != undefined && instructInterventions.Answer != null) {
                var instructInterventionsArray = (instructInterventions.Answer).split(',');
                var j = 0;
                for (j = 0; j < instructInterventionsArray.length; j++) {
                    $('input[name=StartOfCare_485InstructInterventions][value=' + instructInterventionsArray[j] + ']').attr('checked', true);
                    if (instructInterventionsArray[j] == 4) {
                        $("#StartOfCare_485InstructRemoveRugsPerson").val((result["485InstructRemoveRugsPerson"] !== null && result["485InstructRemoveRugsPerson"] !== undefined ? result["485InstructRemoveRugsPerson"].Answer : " "));
                    }
                    if (instructInterventionsArray[j] == 5) {
                        $("#StartOfCare_485InstructRemoveClutterPerson").val((result["485InstructRemoveClutterPerson"] !== null && result["485InstructRemoveClutterPerson"] !== undefined ? result["485InstructRemoveClutterPerson"].Answer : " "));
                    }
                    if (instructInterventionsArray[j] == 6) {

                        $("#StartOfCare_485InstructContactForDizzinessPerson").val((result["485InstructContactForDizzinessPerson"] !== null && result["485InstructContactForDizzinessPerson"] !== undefined ? result["485InstructContactForDizzinessPerson"].Answer : " "));
                    }
                    if (instructInterventionsArray[j] == 10) {

                        $("#StartOfCare_485InstructAdequateLightingPerson").val((result["485InstructAdequateLightingPerson"] !== null && result["485InstructAdequateLightingPerson"] !== undefined ? result["485InstructAdequateLightingPerson"].Answer : " "));
                    }
                    if (instructInterventionsArray[j] == 11) {

                        $("#StartOfCare_485InstructContactForFallPerson").val((result["485InstructContactForFallPerson"] !== null && result["485InstructContactForFallPerson"] !== undefined ? result["485InstructContactForFallPerson"].Answer : " "));
                    }
                }
            }

            $("#StartOfCare_485IADLOrderTemplates").val((result["485IADLOrderTemplates"] !== null && result["485IADLOrderTemplates"] !== undefined ? result["485IADLOrderTemplates"].Answer : " "));
            $("#StartOfCare_485IADLComments").val((result["485IADLComments"] !== null && result["485IADLComments"] !== undefined ? result["485IADLComments"].Answer : " "));

            var instructGoals = result["485InstructGoals"];
            if (instructGoals !== null && instructGoals != undefined && instructGoals.Answer != null) {
                var instructGoalsArray = (instructGoals.Answer).split(',');
                var j = 0;
                for (j = 0; j < instructGoalsArray.length; j++) {
                    $('input[name=StartOfCare_485InstructGoals][value=' + instructGoalsArray[j] + ']').attr('checked', true);
                    if (instructGoalsArray[j] == 3) {
                        $("#StartOfCare_485VerbalizeAnnualEyeExamPerson").val((result["485VerbalizeAnnualEyeExamPerson"] !== null && result["485VerbalizeAnnualEyeExamPerson"] !== undefined ? result["485VerbalizeAnnualEyeExamPerson"].Answer : " "));
                        $("#StartOfCare_485VerbalizeAnnualEyeExamDate").val((result["485VerbalizeAnnualEyeExamDate"] !== null && result["485VerbalizeAnnualEyeExamDate"] !== undefined ? result["485VerbalizeAnnualEyeExamDate"].Answer : " "));
                    }
                    if (instructGoalsArray[j] == 4) {

                        $("#StartOfCare_485RemoveClutterFromPathPerson").val((result["485RemoveClutterFromPathPerson"] !== null && result["485RemoveClutterFromPathPerson"] !== undefined ? result["485RemoveClutterFromPathPerson"].Answer : " "));
                        $("#StartOfCare_485RemoveClutterFromPathDate").val((result["485RemoveClutterFromPathDate"] !== null && result["485RemoveClutterFromPathDate"] !== undefined ? result["485RemoveClutterFromPathDate"].Answer : " "));
                    }
                    if (instructGoalsArray[j] == 5) {

                        $("#StartOfCare_485RemoveThrowRugsAndSecurePerson").val((result["485RemoveThrowRugsAndSecurePerson"] !== null && result["485RemoveThrowRugsAndSecurePerson"] !== undefined ? result["485RemoveThrowRugsAndSecurePerson"].Answer : " "));
                        $("#StartOfCare_485RemoveThrowRugsAndSecureDate").val((result["485RemoveThrowRugsAndSecureDate"] !== null && result["485RemoveThrowRugsAndSecureDate"] !== undefined ? result["485RemoveThrowRugsAndSecureDate"].Answer : " "));
                    }

                }
            }
            $("#StartOfCare_485IADLGoalTemplates").val((result["485IADLGoalTemplates"] !== null && result["485IADLGoalTemplates"] !== undefined ? result["485IADLGoalTemplates"].Answer : " "));
            $("#StartOfCare_485IADLGoalComments").val((result["485IADLGoalComments"] !== null && result["485IADLGoalComments"] !== undefined ? result["485IADLGoalComments"].Answer : " "));
            var dME = result["485DME"];
            if (dME !== null && dME != undefined && dME.Answer != null) {
                var dMEArray = (dME.Answer).split(',');
                var j = 0;
                for (j = 0; j < dMEArray.length; j++) {
                    $('input[name=StartOfCare_485DME][value=' + dMEArray[j] + ']').attr('checked', true);
                }
            }
            $("#StartOfCare_485DMEComments").val((result["485DMEComments"] !== null && result["485DMEComments"] !== undefined ? result["485DMEComments"].Answer : " "));

            var supplies = result["485Supplies"];
            if (supplies !== null && supplies != undefined && supplies.Answer != null) {
                var suppliesArray = (supplies.Answer).split(',');
                var j = 0;
                for (j = 0; j < suppliesArray.length; j++) {
                    $('input[name=StartOfCare_GenericSupplies][value=' + suppliesArray[j] + ']').attr('checked', true);
                }
            }
            $("#StartOfCare_485SuppliesComment").val((result["485SuppliesComment"] !== null && result["485SuppliesComment"] !== undefined ? result["485SuppliesComment"].Answer : " "));

            $("#StartOfCare_GenericDMEProviderName").val((result["GenericDMEProviderName"] !== null && result["GenericDMEProviderName"] !== undefined ? result["GenericDMEProviderName"].Answer : " "));
            $("#StartOfCare_GenericDMEProviderAddress").val((result["GenericDMEProviderAddress"] !== null && result["GenericDMEProviderAddress"] !== undefined ? result["GenericDMEProviderAddress"].Answer : " "));
            $("#StartOfCare_GenericDMEProviderPhone").val((result["GenericDMEProviderPhone"] !== null && result["GenericDMEProviderPhone"] !== undefined ? result["GenericDMEProviderPhone"].Answer : " "));
            $("#StartOfCare_GenericDMESuppliesProvided").val((result["GenericDMESuppliesProvided"] !== null && result["GenericDMESuppliesProvided"] !== undefined ? result["GenericDMESuppliesProvided"].Answer : " "));

            var newMedications = result["485NewMedicationsLS"];
            if (newMedications !== null && newMedications != undefined && newMedications.Answer != null) {
                var newMedicationsArray = (newMedications.Answer).split(',');
                var j = 0;
                for (j = 0; j < newMedicationsArray.length; j++) {
                    $('input[name=StartOfCare_485NewMedicationsLS][value=' + newMedicationsArray[j] + ']').attr('checked', true);
                }
            }
            $("#StartOfCare_485NewMedicationsStartDate1").val((result["485NewMedicationsStartDate1"] !== null && result["485NewMedicationsStartDate1"] !== undefined ? result["485NewMedicationsStartDate1"].Answer : " "));
            $("#StartOfCare_485NewMedicationsDosage1").val((result["485NewMedicationsDosage1"] !== null && result["485NewMedicationsDosage1"] !== undefined ? result["485NewMedicationsDosage1"].Answer : " "));
            $("#StartOfCare_485NewMedicationsClassification1").val((result["485NewMedicationsClassification1"] !== null && result["485NewMedicationsClassification1"] !== undefined ? result["485NewMedicationsClassification1"].Answer : " "));
            $("#StartOfCare_485NewMedicationsFrequency1").val((result["485NewMedicationsFrequency1"] !== null && result["485NewMedicationsFrequency1"] !== undefined ? result["485NewMedicationsFrequency1"].Answer : " "));

            $("#StartOfCare_485NewMedicationsStartDate2").val((result["485NewMedicationsStartDate2"] !== null && result["485NewMedicationsStartDate2"] !== undefined ? result["485NewMedicationsStartDate2"].Answer : " "));
            $("#StartOfCare_485NewMedicationsDosage2").val((result["485NewMedicationsDosage2"] !== null && result["485NewMedicationsDosage2"] !== undefined ? result["485NewMedicationsDosage2"].Answer : " "));
            $("#StartOfCare_485NewMedicationsClassification2").val((result["485NewMedicationsClassification2"] !== null && result["485NewMedicationsClassification2"] !== undefined ? result["485NewMedicationsClassification2"].Answer : " "));
            $("#StartOfCare_485NewMedicationsFrequency2").val((result["485NewMedicationsFrequency2"] !== null && result["485NewMedicationsFrequency2"] !== undefined ? result["485NewMedicationsFrequency2"].Answer : " "));

            $("#StartOfCare_485NewMedicationsStartDate3").val((result["485NewMedicationsStartDate3"] !== null && result["485NewMedicationsStartDate3"] !== undefined ? result["485NewMedicationsStartDate3"].Answer : " "));
            $("#StartOfCare_485NewMedicationsDosage3").val((result["485NewMedicationsDosage3"] !== null && result["485NewMedicationsDosage3"] !== undefined ? result["485NewMedicationsDosage3"].Answer : " "));
            $("#StartOfCare_485NewMedicationsClassification3").val((result["485NewMedicationsClassification3"] !== null && result["485NewMedicationsClassification3"] !== undefined ? result["485NewMedicationsClassification3"].Answer : " "));
            $("#StartOfCare_485NewMedicationsFrequency3").val((result["485NewMedicationsFrequency3"] !== null && result["485NewMedicationsFrequency3"] !== undefined ? result["485NewMedicationsFrequency3"].Answer : " "));

            $("#StartOfCare_GenericMedRecTime").val((result["GenericMedRecTime"] !== null && result["GenericMedRecTime"] !== undefined ? result["GenericMedRecTime"].Answer : " "));
            $("#StartOfCare_GenericMedRecMedication").val((result["GenericMedRecMedication"] !== null && result["GenericMedRecMedication"] !== undefined ? result["GenericMedRecMedication"].Answer : " "));
            $("#StartOfCare_GenericMedRecDose").val((result["GenericMedRecDose"] !== null && result["GenericMedRecDose"] !== undefined ? result["GenericMedRecDose"].Answer : " "));
            $("#StartOfCare_GenericMedRecRoute").val((result["GenericMedRecRoute"] !== null && result["GenericMedRecRoute"] !== undefined ? result["GenericMedRecRoute"].Answer : " "));
            $("#StartOfCare_GenericMedRecFrequency").val((result["GenericMedRecFrequency"] !== null && result["GenericMedRecFrequency"] !== undefined ? result["GenericMedRecFrequency"].Answer : " "));
            $("#StartOfCare_GenericMedRecPRN").val((result["GenericMedRecPRN"] !== null && result["GenericMedRecPRN"] !== undefined ? result["GenericMedRecPRN"].Answer : " "));
            $("#StartOfCare_GenericMedRecLocation").val((result["GenericMedRecLocation"] !== null && result["GenericMedRecLocation"] !== undefined ? result["GenericMedRecLocation"].Answer : " "));
            $("#StartOfCare_GenericMedRecResponse").val((result["GenericMedRecResponse"] !== null && result["GenericMedRecResponse"] !== undefined ? result["GenericMedRecResponse"].Answer : " "));
            $("#StartOfCare_GenericMedRecComments").val((result["GenericMedRecComments"] !== null && result["GenericMedRecComments"] !== undefined ? result["GenericMedRecComments"].Answer : " "));

            $('input[name=StartOfCare_GenericIVAccess][value=' + (result["GenericIVAccess"] != null && result["GenericIVAccess"] != undefined ? result["GenericIVAccess"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_GenericIVAccessType").val((result["GenericIVAccessType"] !== null && result["GenericIVAccessType"] !== undefined ? result["GenericIVAccessType"].Answer : " "));
            $("#StartOfCare_GenericIVAccessDate").val((result["GenericIVAccessDate"] !== null && result["GenericIVAccessDate"] !== undefined ? result["GenericIVAccessDate"].Answer : " "));
            $("#StartOfCare_GenericIVAccessDressingChange").val((result["GenericIVAccessDressingChange"] !== null && result["GenericIVAccessDressingChange"] !== undefined ? result["GenericIVAccessDressingChange"].Answer : " "));


            var medicationInterventions = result["485MedicationInterventions"];
            if (medicationInterventions !== null && medicationInterventions != undefined && medicationInterventions.Answer != null) {
                var medicationInterventionsArray = (medicationInterventions.Answer).split(',');
                var i = 0;
                for (i = 0; i < medicationInterventionsArray.length; i++) {
                    $('input[name=StartOfCare_485MedicationInterventions][value=' + medicationInterventionsArray[i] + ']').attr('checked', true);
                    if (medicationInterventionsArray[i] == 3) {
                        $("#StartOfCare_485DetermineFrequencEachMedPerson").val((result["485DetermineFrequencEachMedPerson"] !== null && result["485DetermineFrequencEachMedPerson"] !== undefined ? result["485DetermineFrequencEachMedPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 4) {
                        $("#StartOfCare_485AssessIndicationEachMedPerson").val((result["485AssessIndicationEachMedPerson"] !== null && result["485AssessIndicationEachMedPerson"] !== undefined ? result["485AssessIndicationEachMedPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 6) {
                        $("#StartOfCare_485AssessOpenMedContainersPerson").val((result["485AssessOpenMedContainersPerson"] !== null && result["485AssessOpenMedContainersPerson"] !== undefined ? result["485AssessOpenMedContainersPerson"].Answer : " "));
                    }
                    if (medicationInterventionsArray[i] == 7) {
                        $("#StartOfCare_485InstructMedicationRegimen").val((result["485InstructMedicationRegimen"] !== null && result["485InstructMedicationRegimen"] !== undefined ? result["485InstructMedicationRegimen"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 10) {
                        $("#StartOfCare_485AssessAdminInjectMedsPerson").val((result["485AssessAdminInjectMedsPerson"] !== null && result["485AssessAdminInjectMedsPerson"] !== undefined ? result["485AssessAdminInjectMedsPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 12) {
                        $("#StartOfCare_485InstructHighRiskMedsPerson").val((result["485InstructHighRiskMedsPerson"] !== null && result["485InstructHighRiskMedsPerson"] !== undefined ? result["485InstructHighRiskMedsPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 13) {
                        $("#StartOfCare_485InstructSignsSymptomsIneffectiveDrugPerson").val((result["485InstructSignsSymptomsIneffectiveDrugPerson"] !== null && result["485InstructSignsSymptomsIneffectiveDrugPerson"] !== undefined ? result["485InstructSignsSymptomsIneffectiveDrugPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 14) {
                        $("#StartOfCare_485InstructMedSideEffectsPerson").val((result["485InstructMedSideEffectsPerson"] !== null && result["485InstructMedSideEffectsPerson"] !== undefined ? result["485InstructMedSideEffectsPerson"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 15) {
                        $("#StartOfCare_485InstructMedReactionsPerson").val((result["485InstructMedReactionsPerson"] !== null && result["485InstructMedReactionsPerson"] !== undefined ? result["485InstructMedReactionsPerson"].Answer : " "));

                    }

                    if (medicationInterventionsArray[i] == 16) {
                        $("#StartOfCare_485AdministerIVType").val((result["485AdministerIVType"] !== null && result["485AdministerIVType"] !== undefined ? result["485AdministerIVType"].Answer : " "));
                        $("#StartOfCare_485AdministerIVRate").val((result["485AdministerIVRate"] !== null && result["485AdministerIVRate"] !== undefined ? result["485AdministerIVRate"].Answer : " "));
                        $("#StartOfCare_485AdministerIVVia").val((result["485AdministerIVVia"] !== null && result["485AdministerIVVia"] !== undefined ? result["485AdministerIVVia"].Answer : " "));
                        $("#StartOfCare_485AdministerIVEvery").val((result["485AdministerIVEvery"] !== null && result["485AdministerIVEvery"] !== undefined ? result["485AdministerIVEvery"].Answer : " "));
                    }
                    if (medicationInterventionsArray[i] == 17) {
                        $("#StartOfCare_485InstructAdministerIVPerson").val((result["485InstructAdministerIVPerson"] !== null && result["485InstructAdministerIVPerson"] !== undefined ? result["485InstructAdministerIVPerson"].Answer : " "));
                        $("#StartOfCare_485InstructAdministerIVRate").val((result["485InstructAdministerIVRate"] !== null && result["485InstructAdministerIVRate"] !== undefined ? result["485InstructAdministerIVRate"].Answer : " "));
                        $("#StartOfCare_485InstructAdministerIVVia").val((result["485InstructAdministerIVVia"] !== null && result["485InstructAdministerIVVia"] !== undefined ? result["485InstructAdministerIVVia"].Answer : " "));
                        $("#StartOfCare_485InstructAdministerIVEvery").val((result["485InstructAdministerIVEvery"] !== null && result["485InstructAdministerIVEvery"] !== undefined ? result["485InstructAdministerIVEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 18) {
                        $("#StartOfCare_485ChangePeripheralIVGauge").val((result["485ChangePeripheralIVGauge"] !== null && result["485ChangePeripheralIVGauge"] !== undefined ? result["485ChangePeripheralIVGauge"].Answer : " "));
                        $("#StartOfCare_485ChangePeripheralIVWidth").val((result["485ChangePeripheralIVWidth"] !== null && result["485ChangePeripheralIVWidth"] !== undefined ? result["485ChangePeripheralIVWidth"].Answer : " "));
                    }
                    if (medicationInterventionsArray[i] == 19) {
                        $("#StartOfCare_485FlushPeripheralIVWith").val((result["485FlushPeripheralIVWith"] !== null && result["485FlushPeripheralIVWith"] !== undefined ? result["485FlushPeripheralIVWith"].Answer : " "));
                        $("#StartOfCare_485FlushPeripheralIVOf").val((result["485FlushPeripheralIVOf"] !== null && result["485FlushPeripheralIVOf"] !== undefined ? result["485FlushPeripheralIVOf"].Answer : " "));
                        $("#StartOfCare_485FlushPeripheralIVEvery").val((result["485FlushPeripheralIVEvery"] !== null && result["485FlushPeripheralIVEvery"] !== undefined ? result["485FlushPeripheralIVEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 20) {
                        $("#StartOfCare_485InstructFlushPerpheralIVPerson").val((result["485InstructFlushPerpheralIVPerson"] !== null && result["485InstructFlushPerpheralIVPerson"] !== undefined ? result["485InstructFlushPerpheralIVPerson"].Answer : " "));
                        $("#StartOfCare_485InstructFlushPerpheralIVWith").val((result["485InstructFlushPerpheralIVWith"] !== null && result["485InstructFlushPerpheralIVWith"] !== undefined ? result["485InstructFlushPerpheralIVWith"].Answer : " "));
                        $("#StartOfCare_485InstructFlushPerpheralIVOf").val((result["485InstructFlushPerpheralIVOf"] !== null && result["485InstructFlushPerpheralIVOf"] !== undefined ? result["485InstructFlushPerpheralIVOf"].Answer : " "));
                        $("#StartOfCare_485InstructFlushPerpheralIVEvery").val((result["485InstructFlushPerpheralIVEvery"] !== null && result["485InstructFlushPerpheralIVEvery"] !== undefined ? result["485InstructFlushPerpheralIVEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 21) {
                        $("#StartOfCare_485ChangeCentralLineEvery").val((result["485ChangeCentralLineEvery"] !== null && result["485ChangeCentralLineEvery"] !== undefined ? result["485ChangeCentralLineEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 22) {
                        $("#StartOfCare_485InstructChangeCentralLinePerson").val((result["485InstructChangeCentralLinePerson"] !== null && result["485InstructChangeCentralLinePerson"] !== undefined ? result["485InstructChangeCentralLinePerson"].Answer : " "));
                        $("#StartOfCare_485InstructChangeCentralLineEvery").val((result["485InstructChangeCentralLineEvery"] !== null && result["485InstructChangeCentralLineEvery"] !== undefined ? result["485InstructChangeCentralLineEvery"].Answer : " "));
                    }
                    if (medicationInterventionsArray[i] == 23) {
                        $("#StartOfCare_485FlushCentralLineWith").val((result["485FlushCentralLineWith"] !== null && result["485FlushCentralLineWith"] !== undefined ? result["485FlushCentralLineWith"].Answer : " "));
                        $("#StartOfCare_485FlushCentralLineOf").val((result["485FlushCentralLineOf"] !== null && result["485FlushCentralLineOf"] !== undefined ? result["485FlushCentralLineOf"].Answer : " "));
                        $("#StartOfCare_485FlushCentralLineEvery").val((result["485FlushCentralLineEvery"] !== null && result["485FlushCentralLineEvery"] !== undefined ? result["485FlushCentralLineEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 24) {
                        $("#StartOfCare_485InstructFlushCentralLinePerson").val((result["485InstructFlushCentralLinePerson"] !== null && result["485InstructFlushCentralLinePerson"] !== undefined ? result["485InstructFlushCentralLinePerson"].Answer : " "));
                        $("#StartOfCare_485InstructFlushCentralLineWith").val((result["485InstructFlushCentralLineWith"] !== null && result["485InstructFlushCentralLineWith"] !== undefined ? result["485InstructFlushCentralLineWith"].Answer : " "));
                        $("#StartOfCare_485InstructFlushCentralLineOf").val((result["485InstructFlushCentralLineOf"] !== null && result["485InstructFlushCentralLineOf"] !== undefined ? result["485InstructFlushCentralLineOf"].Answer : " "));
                        $("#StartOfCare_485InstructFlushCentralLineEvery").val((result["485InstructFlushCentralLineEvery"] !== null && result["485InstructFlushCentralLineEvery"] !== undefined ? result["485InstructFlushCentralLineEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 25) {
                        $("#StartOfCare_485AccessPortType").val((result["485AccessPortType"] !== null && result["485AccessPortType"] !== undefined ? result["485AccessPortType"].Answer : " "));
                        $("#StartOfCare_485AccessPortTypeEvery").val((result["485AccessPortTypeEvery"] !== null && result["485AccessPortTypeEvery"] !== undefined ? result["485AccessPortTypeEvery"].Answer : " "));
                        $("#StartOfCare_485AccessPortTypeWith").val((result["485AccessPortTypeWith"] !== null && result["485AccessPortTypeWith"] !== undefined ? result["485AccessPortTypeWith"].Answer : " "));
                        $("#StartOfCare_485AccessPortTypeOf").val((result["485AccessPortTypeOf"] !== null && result["485AccessPortTypeOf"] !== undefined ? result["485AccessPortTypeOf"].Answer : " "));
                        $("#StartOfCare_485AccessPortTypeFrequency").val((result["485AccessPortTypeFrequency"] !== null && result["485AccessPortTypeFrequency"] !== undefined ? result["485AccessPortTypeFrequency"].Answer : " "));
                    }
                    if (medicationInterventionsArray[i] == 26) {
                        $("#StartOfCare_485ChangePortDressingType").val((result["485ChangePortDressingType"] !== null && result["485ChangePortDressingType"] !== undefined ? result["485ChangePortDressingType"].Answer : " "));
                        $("#StartOfCare_485ChangePortDressingEvery").val((result["485ChangePortDressingEvery"] !== null && result["485ChangePortDressingEvery"] !== undefined ? result["485ChangePortDressingEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 27) {
                        $("#StartOfCare_485InstructPortDressingPerson").val((result["485InstructPortDressingPerson"] !== null && result["485InstructPortDressingPerson"] !== undefined ? result["485InstructPortDressingPerson"].Answer : " "));
                        $("#StartOfCare_485InstructPortDressingType").val((result["485InstructPortDressingType"] !== null && result["485InstructPortDressingType"] !== undefined ? result["485InstructPortDressingType"].Answer : " "));
                        $("#StartOfCare_485InstructPortDressingEvery").val((result["485InstructPortDressingEvery"] !== null && result["485InstructPortDressingEvery"] !== undefined ? result["485InstructPortDressingEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 28) {
                        $("#StartOfCare_485ChangeIVTubingEvery").val((result["485ChangeIVTubingEvery"] !== null && result["485ChangeIVTubingEvery"] !== undefined ? result["485ChangeIVTubingEvery"].Answer : " "));

                    }
                    if (medicationInterventionsArray[i] == 29) {
                        $("#StartOfCare_485InstructInfectionSignsSymptomsPerson").val((result["485InstructInfectionSignsSymptomsPerson"] !== null && result["485InstructInfectionSignsSymptomsPerson"] !== undefined ? result["485InstructInfectionSignsSymptomsPerson"].Answer : " "));
                    }
                }
            }
            $("#StartOfCare_485MedicationInterventionTemplates").val((result["485MedicationInterventionTemplates"] !== null && result["485MedicationInterventionTemplates"] !== undefined ? result["485MedicationInterventionTemplates"].Answer : " "));
            $("#StartOfCare_485MedicationInterventionComments").val((result["485MedicationInterventionComments"] !== null && result["485MedicationInterventionComments"] !== undefined ? result["485MedicationInterventionComments"].Answer : " "));

            var medicationGoals = result["485MedicationGoals"];
            if (medicationGoals !== null && medicationGoals != undefined && medicationGoals.Answer != null) {
                var medicationGoalsArray = (medicationGoals.Answer).split(',');
                var i = 0;
                for (i = 0; i < medicationGoalsArray.length; i++) {
                    $('input[name=StartOfCare_485MedicationGoals][value=' + medicationGoalsArray[i] + ']').attr('checked', true);
                    if (medicationGoalsArray[i] == 2) {
                        $("#StartOfCare_485MedManagementIndependentPerson").val((result["485MedManagementIndependentPerson"] !== null && result["485MedManagementIndependentPerson"] !== undefined ? result["485MedManagementIndependentPerson"].Answer : " "));
                        $("#StartOfCare_485MedManagementIndependentDate").val((result["485MedManagementIndependentDate"] !== null && result["485MedManagementIndependentDate"] !== undefined ? result["485MedManagementIndependentDate"].Answer : " "));

                    }
                    if (medicationGoalsArray[i] == 3) {
                        $("#StartOfCare_485VerbalizeMedRegimenUnderstandingPerson").val((result["485VerbalizeMedRegimenUnderstandingPerson"] !== null && result["485VerbalizeMedRegimenUnderstandingPerson"] !== undefined ? result["485VerbalizeMedRegimenUnderstandingPerson"].Answer : " "));
                        $("#StartOfCare_485VerbalizeMedRegimenUnderstandingDate").val((result["485VerbalizeMedRegimenUnderstandingDate"] !== null && result["485VerbalizeMedRegimenUnderstandingDate"] !== undefined ? result["485VerbalizeMedRegimenUnderstandingDate"].Answer : " "));

                    }

                    if (medicationGoalsArray[i] == 4) {
                        $("#StartOfCare_485MedAdminIndependentPerson").val((result["485MedAdminIndependentPerson"] !== null && result["485MedAdminIndependentPerson"] !== undefined ? result["485MedAdminIndependentPerson"].Answer : " "));
                        $("#StartOfCare_485MedAdminIndependentWith").val((result["485MedAdminIndependentWith"] !== null && result["485MedAdminIndependentWith"] !== undefined ? result["485MedAdminIndependentWith"].Answer : " "));
                        $("#StartOfCare_485MedAdminIndependentDate").val((result["485MedAdminIndependentDate"] !== null && result["485MedAdminIndependentDate"] !== undefined ? result["485MedAdminIndependentDate"].Answer : " "));
                    }
                    if (medicationGoalsArray[i] == 5) {
                        $("#StartOfCare_485MedSetupIndependentPerson").val((result["485MedSetupIndependentPerson"] !== null && result["485MedSetupIndependentPerson"] !== undefined ? result["485MedSetupIndependentPerson"].Answer : " "));
                        $("#StartOfCare_485MedSetupIndependentDate").val((result["485MedSetupIndependentDate"] !== null && result["485MedSetupIndependentDate"] !== undefined ? result["485MedSetupIndependentDate"].Answer : " "));

                    }
                    if (medicationGoalsArray[i] == 6) {
                        $("#StartOfCare_485VerbalizeEachMedIndicationPerson").val((result["485VerbalizeEachMedIndicationPerson"] !== null && result["485VerbalizeEachMedIndicationPerson"] !== undefined ? result["485VerbalizeEachMedIndicationPerson"].Answer : " "));
                        $("#StartOfCare_485VerbalizeEachMedIndicationDate").val((result["485VerbalizeEachMedIndicationDate"] !== null && result["485VerbalizeEachMedIndicationDate"] !== undefined ? result["485VerbalizeEachMedIndicationDate"].Answer : " "));
                    }
                    if (medicationGoalsArray[i] == 7) {
                        $("#StartOfCare_485CorrectDoseIdentifyPerson").val((result["485CorrectDoseIdentifyPerson"] !== null && result["485CorrectDoseIdentifyPerson"] !== undefined ? result["485CorrectDoseIdentifyPerson"].Answer : " "));
                        $("#StartOfCare_485CorrectDoseIdentifyDate").val((result["485CorrectDoseIdentifyDate"] !== null && result["485CorrectDoseIdentifyDate"] !== undefined ? result["485CorrectDoseIdentifyDate"].Answer : " "));

                    }
                    if (medicationGoalsArray[i] == 9) {
                        $("#StartOfCare_485DemonstrateCentralLineFlushPerson").val((result["485DemonstrateCentralLineFlushPerson"] !== null && result["485DemonstrateCentralLineFlushPerson"] !== undefined ? result["485DemonstrateCentralLineFlushPerson"].Answer : " "));
                    }
                    if (medicationGoalsArray[i] == 10) {
                        $("#StartOfCare_485DemonstratePeripheralIVLineFlushPerson").val((result["485DemonstratePeripheralIVLineFlushPerson"] !== null && result["485DemonstratePeripheralIVLineFlushPerson"] !== undefined ? result["485DemonstratePeripheralIVLineFlushPerson"].Answer : " "));

                    }
                    if (medicationGoalsArray[i] == 11) {
                        $("#StartOfCare_485DemonstrateSterileDressingTechniquePerson").val((result["485DemonstrateSterileDressingTechniquePerson"] !== null && result["485DemonstrateSterileDressingTechniquePerson"] !== undefined ? result["485DemonstrateSterileDressingTechniquePerson"].Answer : " "));
                        $("#StartOfCare_485DemonstrateSterileDressingTechniqueType").val((result["485DemonstrateSterileDressingTechniqueType"] !== null && result["485DemonstrateSterileDressingTechniqueType"] !== undefined ? result["485DemonstrateSterileDressingTechniqueType"].Answer : " "));
                    }

                    if (medicationGoalsArray[i] == 12) {
                        $("#StartOfCare_485DemonstrateAdministerIVPerson").val((result["485DemonstrateAdministerIVPerson"] !== null && result["485DemonstrateAdministerIVPerson"] !== undefined ? result["485DemonstrateAdministerIVPerson"].Answer : " "));
                        $("#StartOfCare_485DemonstrateAdministerIVType").val((result["485DemonstrateAdministerIVType"] !== null && result["485DemonstrateAdministerIVType"] !== undefined ? result["485DemonstrateAdministerIVType"].Answer : " "));
                        $("#StartOfCare_485DemonstrateAdministerIVRate").val((result["485DemonstrateAdministerIVRate"] !== null && result["485DemonstrateAdministerIVRate"] !== undefined ? result["485DemonstrateAdministerIVRate"].Answer : " "));
                        $("#StartOfCare_485DemonstrateAdministerIVVia").val((result["485DemonstrateAdministerIVVia"] !== null && result["485DemonstrateAdministerIVVia"] !== undefined ? result["485DemonstrateAdministerIVVia"].Answer : " "));
                        $("#StartOfCare_485DemonstrateAdministerIVEvery").val((result["485DemonstrateAdministerIVEvery"] !== null && result["485DemonstrateAdministerIVEvery"] !== undefined ? result["485DemonstrateAdministerIVEvery"].Answer : " "));
                    }
                }
            }
            $("#StartOfCare_485MedicationGoalTemplates").val((result["485MedicationGoalTemplates"] !== null && result["485MedicationGoalTemplates"] !== undefined ? result["485MedicationGoalTemplates"].Answer : " "));
            $("#StartOfCare_485MedicationGoalComments").val((result["485MedicationGoalComments"] !== null && result["485MedicationGoalComments"] !== undefined ? result["485MedicationGoalComments"].Answer : " "));

            $("#StartOfCare_485SNFrequency").val((result["485SNFrequency"] !== null && result["485SNFrequency"] !== undefined ? result["485SNFrequency"].Answer : " "));
            $("#StartOfCare_485PTFrequency").val((result["485PTFrequency"] !== null && result["485PTFrequency"] !== undefined ? result["485PTFrequency"].Answer : " "));
            $("#StartOfCare_485OTFrequency").val((result["485OTFrequency"] !== null && result["485OTFrequency"] !== undefined ? result["485OTFrequency"].Answer : " "));
            $("#StartOfCare_485STFrequency").val((result["485STFrequency"] !== null && result["485STFrequency"] !== undefined ? result["485STFrequency"].Answer : " "));
            $("#StartOfCare_485MSWFrequency").val((result["485MSWFrequency"] !== null && result["485MSWFrequency"] !== undefined ? result["485MSWFrequency"].Answer : " "));
            $("#StartOfCare_485HHAFrequency").val((result["485HHAFrequency"] !== null && result["485HHAFrequency"] !== undefined ? result["485HHAFrequency"].Answer : " "));
            $('input[name=StartOfCare_485Dietician][value=' + (result["485Dietician"] != null && result["485Dietician"] != undefined ? result["485Dietician"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_485OrdersDisciplineInterventionTemplates").val((result["485OrdersDisciplineInterventionTemplates"] !== null && result["485OrdersDisciplineInterventionTemplates"] !== undefined ? result["485OrdersDisciplineInterventionTemplates"].Answer : " "));
            $("#StartOfCare_485OrdersDisciplineInterventionComments").val((result["485OrdersDisciplineInterventionComments"] !== null && result["485OrdersDisciplineInterventionComments"] !== undefined ? result["485OrdersDisciplineInterventionComments"].Answer : " "));

            var rehabilitationPotential = result["485RehabilitationPotential"];
            if (rehabilitationPotential !== null && rehabilitationPotential != undefined && rehabilitationPotential.Answer != null) {
                var rehabilitationPotentialArray = (rehabilitationPotential.Answer).split(',');
                var j = 0;
                for (j = 0; j < rehabilitationPotentialArray.length; j++) {
                    $('input[name=StartOfCare_485RehabilitationPotential][value=' + rehabilitationPotentialArray[j] + ']').attr('checked', true);

                }
            }
            $("#StartOfCare_485AchieveGoalsTemplates").val((result["485AchieveGoalsTemplates"] !== null && result["485AchieveGoalsTemplates"] !== undefined ? result["485AchieveGoalsTemplates"].Answer : " "));
            $("#StartOfCare_485AchieveGoalsComments").val((result["485AchieveGoalsComments"] !== null && result["485AchieveGoalsComments"] !== undefined ? result["485AchieveGoalsComments"].Answer : " "));

            var dischargePlans = result["485DischargePlans"];
            if (dischargePlans !== null && dischargePlans != undefined && dischargePlans.Answer != null) {
                var dischargePlansArray = (dischargePlans.Answer).split(',');
                var j = 0;
                for (j = 0; j < dischargePlansArray.length; j++) {
                    $('input[name=StartOfCare_485DischargePlans][value=' + dischargePlansArray[j] + ']').attr('checked', true);

                }
            }
            $("#StartOfCare_485DischargePlanTemplates").val((result["485DischargePlanTemplates"] !== null && result["485DischargePlanTemplates"] !== undefined ? result["485DischargePlanTemplates"].Answer : " "));
            $("#StartOfCare_485DischargePlanComments").val((result["485DischargePlanComments"] !== null && result["485DischargePlanComments"] !== undefined ? result["485DischargePlanComments"].Answer : " "));

            var patientStrengths = result["485PatientStrengths"];
            if (patientStrengths !== null && patientStrengths != undefined && patientStrengths.Answer != null) {
                var patientStrengthsArray = (patientStrengths.Answer).split(',');
                var j = 0;
                for (j = 0; j < patientStrengthsArray.length; j++) {
                    $('input[name=StartOfCare_485PatientStrengths][value=' + patientStrengthsArray[j] + ']').attr('checked', true);

                }
            }
            $("#StartOfCare_485PatientStrengthOther").val((result["485PatientStrengthOther"] !== null && result["485PatientStrengthOther"] !== undefined ? result["485PatientStrengthOther"].Answer : " "));

            var conclusions = result["485Conclusions"];
            if (conclusions !== null && conclusions != undefined && conclusions.Answer != null) {
                var conclusionsArray = (conclusions.Answer).split(',');
                var j = 0;
                for (j = 0; j < conclusionsArray.length; j++) {
                    $('input[name=StartOfCare_485Conclusions][value=' + conclusionsArray[j] + ']').attr('checked', true);
                }
            }
            $("#StartOfCare_485ConclusionOther").val((result["485ConclusionOther"] !== null && result["485ConclusionOther"] !== undefined ? result["485ConclusionOther"].Answer : " "));

            $("#StartOfCare_485SkilledInterventionTemplate").val((result["485SkilledInterventionTemplate"] !== null && result["485SkilledInterventionTemplate"] !== undefined ? result["485SkilledInterventionTemplate"].Answer : " "));
            $("#StartOfCare_485SkilledInterventionComments").val((result["485SkilledInterventionComments"] !== null && result["485SkilledInterventionComments"] !== undefined ? result["485SkilledInterventionComments"].Answer : " "));

            var response = result["485SIResponse"];
            if (response != null && response != undefined) {
                if (response.Answer == 1) {

                    $('input[name=StartOfCare_485SIResponse][value=1]').attr('checked', true);

                    var sIVerbalizedUnderstandingPT = result["485SIVerbalizedUnderstandingPT"];
                    if (sIVerbalizedUnderstandingPT != null && sIVerbalizedUnderstandingPT != undefined) {
                        if (sIVerbalizedUnderstandingPT.Answer == 1) {

                            $('input[name=StartOfCare_485SIVerbalizedUnderstandingPT][value=1]').attr('checked', true);
                            $("#StartOfCare_485SIVerbalizedUnderstandingPTPercent").val((result["485SIVerbalizedUnderstandingPTPercent"] !== null && result["485SIVerbalizedUnderstandingPTPercent"] !== undefined ? result["485SIVerbalizedUnderstandingPTPercent"].Answer : " "));
                        }
                        else {
                            $('input[name=StartOfCare_485SIVerbalizedUnderstandingPT][value=1]').attr('checked', false);
                        }
                    }

                    var sIVerbalizedUnderstandingCG = result["485SIVerbalizedUnderstandingCG"];
                    if (sIVerbalizedUnderstandingCG != null && sIVerbalizedUnderstandingCG != undefined) {
                        if (sIVerbalizedUnderstandingCG.Answer == 1) {

                            $('input[name=StartOfCare_485SIVerbalizedUnderstandingCG][value=1]').attr('checked', true);
                            $("#StartOfCare_485SIVerbalizedUnderstandingCGPercent").val((result["485SIVerbalizedUnderstandingCGPercent"] !== null && result["485SIVerbalizedUnderstandingCGPercent"] !== undefined ? result["485SIVerbalizedUnderstandingCGPercent"].Answer : " "));
                        }
                        else {
                            $('input[name=StartOfCare_485SIVerbalizedUnderstandingCG][value=1]').attr('checked', false);
                        }
                    }
                    var sIReturnDemonstrationPT = result["485SIReturnDemonstrationPT"];
                    if (sIReturnDemonstrationPT != null && sIReturnDemonstrationPT != undefined) {
                        if (sIReturnDemonstrationPT.Answer == 1) {

                            $('input[name=StartOfCare_485SIReturnDemonstrationPT][value=1]').attr('checked', true);
                            $("#StartOfCare_485SIReturnDemonstrationPTPercent").val((result["485SIReturnDemonstrationPTPercent"] !== null && result["485SIReturnDemonstrationPTPercent"] !== undefined ? result["485SIReturnDemonstrationPTPercent"].Answer : " "));
                        }
                        else {
                            $('input[name=StartOfCare_485SIReturnDemonstrationPT][value=1]').attr('checked', false);
                        }
                    }
                    var sIReturnDemonstrationCG = result["485SIReturnDemonstrationCG"];
                    if (sIReturnDemonstrationCG != null && sIReturnDemonstrationCG != undefined) {
                        if (sIReturnDemonstrationCG.Answer == 1) {

                            $('input[name=StartOfCare_485SIReturnDemonstrationCG][value=1]').attr('checked', true);
                            $("#StartOfCare_485SIReturnDemonstrationCGPercent").val((result["485SIReturnDemonstrationCGPercent"] !== null && result["485SIReturnDemonstrationCGPercent"] !== undefined ? result["485SIReturnDemonstrationCGPercent"].Answer : " "));
                        }
                        else {
                            $('input[name=StartOfCare_485SIReturnDemonstrationCG][value=1]').attr('checked', false);
                        }
                    }
                    var sIFurtherTeachReqPT = result["485SIFurtherTeachReqPT"];
                    if (sIFurtherTeachReqPT != null && sIFurtherTeachReqPT != undefined) {
                        if (sIFurtherTeachReqPT.Answer == 1) {

                            $('input[name=StartOfCare_485SIFurtherTeachReqPT][value=1]').attr('checked', true);

                        }
                        else {
                            $('input[name=StartOfCare_485SIFurtherTeachReqPT][value=1]').attr('checked', false);
                        }
                    }
                    var sIFurtherTeachReqCG = result["485SIFurtherTeachReqCG"];
                    if (sIFurtherTeachReqCG != null && sIFurtherTeachReqCG != undefined) {
                        if (sIFurtherTeachReqCG.Answer == 1) {

                            $('input[name=StartOfCare_485SIFurtherTeachReqCG][value=1]').attr('checked', true);

                        }
                        else {
                            $('input[name=StartOfCare_485SIFurtherTeachReqCG][value=1]').attr('checked', false);
                        }
                    }
                    $("#StartOfCare_485SIResponseComments").val((result["485SIResponseComments"] !== null && result["485SIResponseComments"] !== undefined ? result["485SIResponseComments"].Answer : " "));
                }
                else {
                    $('input[name=StartOfCare_485SIResponse][value=1]').attr('checked', false);
                }
            }
            $("#StartOfCare_485TeachingToolUsed").val((result["485TeachingToolUsed"] !== null && result["485TeachingToolUsed"] !== undefined ? result["485TeachingToolUsed"].Answer : " "));
            $("#StartOfCare_485ProgressToGoals").val((result["485ProgressToGoals"] !== null && result["485ProgressToGoals"] !== undefined ? result["485ProgressToGoals"].Answer : " "));
            $("#StartOfCare_485ConferencedWith").val((result["485ConferencedWith"] !== null && result["485ConferencedWith"] !== undefined ? result["485ConferencedWith"].Answer : " "));
            $("#StartOfCare_485ConferencedWithName").val((result["485ConferencedWithName"] !== null && result["485ConferencedWithName"] !== undefined ? result["485ConferencedWithName"].Answer : " "));
            $("#StartOfCare_485SkilledInterventionRegarding").val((result["485SkilledInterventionRegarding"] !== null && result["485SkilledInterventionRegarding"] !== undefined ? result["485SkilledInterventionRegarding"].Answer : " "));
            $("#StartOfCare_485SkilledInterventionPhysicianContacted").val((result["485SkilledInterventionPhysicianContacted"] !== null && result["485SkilledInterventionPhysicianContacted"] !== undefined ? result["485SkilledInterventionPhysicianContacted"].Answer : " "));
            $("#StartOfCare_485SkilledInterventionOrderChanges").val((result["485SkilledInterventionOrderChanges"] !== null && result["485SkilledInterventionOrderChanges"] !== undefined ? result["485SkilledInterventionOrderChanges"].Answer : " "));
            $("#StartOfCare_485SkilledInterventionNextVisit").val((result["485SkilledInterventionNextVisit"] !== null && result["485SkilledInterventionNextVisit"] !== undefined ? result["485SkilledInterventionNextVisit"].Answer : " "));
            $("#StartOfCare_485SkilledInterventionNextPhysicianVisit").val((result["485SkilledInterventionNextPhysicianVisit"] !== null && result["485SkilledInterventionNextPhysicianVisit"] !== undefined ? result["485SkilledInterventionNextPhysicianVisit"].Answer : " "));
            $("#StartOfCare_485SkilledInterventionDischargePlanning").val((result["485SkilledInterventionDischargePlanning"] !== null && result["485SkilledInterventionDischargePlanning"] !== undefined ? result["485SkilledInterventionDischargePlanning"].Answer : " "));

            $('input[name=StartOfCare_485SkilledInterventionWrittenNotice][value=' + (result["485SkilledInterventionWrittenNotice"] != null && result["485SkilledInterventionWrittenNotice"] != undefined ? result["485SkilledInterventionWrittenNotice"].Answer : "") + ']').attr('checked', true);
            $("#StartOfCare_485SkilledInterventionWrittenNoticeDate").val((result["485SkilledInterventionWrittenNoticeDate"] !== null && result["485SkilledInterventionWrittenNoticeDate"] !== undefined ? result["485SkilledInterventionWrittenNoticeDate"].Answer : " "));
            var supply = result["GenericSupply"];
            if (supply != null && supply.Answer != null) {
                var jsonData = supply.Answer;
                var data = eval("(" + jsonData + ")");
                if (data != null && data.Supply != null) {
                    if (data.Supply.length > 0) {
                        Oasis.addToSupplyTable(data.Supply, $("#suppliesTable"));
                    }
                    else {
                        Oasis.ClearRows($("#suppliesTable"));
                        Oasis.addTableRow('#suppliesTable');
                    }
                }
                else {
                    Oasis.ClearRows($("#suppliesTable"));
                    Oasis.addTableRow('#suppliesTable');
                }
            } else {
                Oasis.ClearRows($("#suppliesTable"));
                Oasis.addTableRow('#suppliesTable');
            }
        };
    }

}





 
  
