﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="abs" id="bar_bottom">
	<a class="float_left" href="#" id="show_desktop" title="Show Desktop">
		<img src="/images/icons/icon_22_desktop.png" />
	</a>
	<ul id="dock">
		<li id="icon_dock_computer">
			<a href="#window_computer">
				<img src="/images/icons/icon_22_computer.png" />
				Computer
			</a>
		</li>
		<li id="icon_dock_drive">
			<a href="#window_drive">
				<img src="/images/icons/icon_22_drive.png" />
				Hard Drive
			</a>
		</li>
		<li id="icon_dock_disc">
			<a href="#window_disc">
				<img src="/images/icons/icon_22_disc.png" />
				Audio CD
			</a>
		</li>
		<li id="icon_dock_network">
			<a href="#window_network">
				<img src="/images/icons/icon_22_network.png" />
				Network
			</a>
		</li>
	</ul>
</div>