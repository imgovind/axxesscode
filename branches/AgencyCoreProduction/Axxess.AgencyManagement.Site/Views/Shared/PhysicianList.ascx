﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="existingPhysicianList" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left">Physician List</span> <span class="float_right"><a href="javascript:void(0);"
                class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                    href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form">
            <div class="submitoasisCont">            
               
                    <!--[if !IE]>start table_wrapper<![endif]-->
                    <%= Html.Telerik().Grid<AgencyPhysician>()
                        .Name("ExistingPhysicianGrid")
                        .ToolBar(commnds => commnds.Custom().HtmlAttributes(new { @id = "add_New_Physician", @href = "javascript:void(0);", @onclick = "Patient.NewPhysicianContact(); JQD.open_window('#newPhysicianContact');" }).Text("Add New Physician"))                                                                                                    
                        .Columns(columns =>
		                {
                            columns.Bound(p => p.Created).Format("{0:MM/dd/yyyy}").Title("Date Created");
                            columns.Bound(p => p.Modified).Format("{0:MM/dd/yyyy}").Title("Date Modified");
                            columns.Bound(p => p.DisplayName);                            
                            columns.Bound(p => p.PhoneWork);           
                            columns.Bound(p =>p.Id)
                                   .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Patient.EditPhysicianContact('<#=Id#>'); JQD.open_window('#editPhysicianContact');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Referral.Delete($(this),'<#=Id#>');\" class=\"deleteReferral\">Delete</a>")
                                   .Title("Action").Width(180);
                        })
                        .DataBinding(dataBinding => dataBinding.Ajax().Select("PhysicianList", "Agency"))
                        .ClientEvents(events => events.OnDataBound("Patient.NewPhysician"))
                        .Pageable(paging => paging.PageSize(13))
                        .Sortable()
                        .Scrollable(scrolling => scrolling.Enabled(true).Height(369))
                    %>
                    <!--[if !IE]>end table_wrapper<![endif]-->
              
               
            </div>
        </div>
        <div class="abs window_bottom">
            List of physicians
        </div>
    </div>
   
</div>
