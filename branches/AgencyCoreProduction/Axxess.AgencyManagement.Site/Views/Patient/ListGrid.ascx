﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<Patient>>" %>
<div id="PatientSelectionGridContainer" class="sideBottom">
    <%Html.Telerik().Grid<Patient>()
                                                .Name("PatientSelectionGrid")
                                                .Columns(columns =>
                                                {
                                                    columns.Bound(p => p.LastName);
                                                    columns.Bound(p => p.FirstName);
                                                    columns.Bound(p => p.PatientIdNumber).Title("Patient #").HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);
                                                    columns.Bound(p => p.Id).HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);
                                                })
                                                .DataBinding(dataBinding => dataBinding.Ajax().Select("All", "Patient", new { statusId = 0, paymentSourceId = 0, name = string.Empty }))
                                                .Sortable()
                                                .Selectable()
                                                .Scrollable()
                                                .Footer(false)
                                                .ClientEvents(events => events.OnDataBound(
                                                    () =>
                                                    {%>
    function () { if($('#PatientSelectionGrid .t-grid-content tbody tr').length ==0){
    Patient.NoPatientBind('<%=Guid.Empty %>'); } else { $('#PatientSelectionGrid .t-grid-content tbody tr:has(td):first').click(); } }
    <% }
                                                ).OnRowSelected("Patient.OnPatientRowSelected")
                                                    .OnLoad("Patient.CalculateGridHeight()")).Render();                                                
                                                                                                
    %>
</div>
