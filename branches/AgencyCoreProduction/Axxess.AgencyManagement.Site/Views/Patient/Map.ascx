﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="patientmap" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left">Patient Map</span><span class="float_right"><a href="javascript:void(0);"
                class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                    href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content">
            <div id="patient_map" style="clear: both; width: 500px; height: 300px"></div>
        </div>
        <div class="abs window_bottom">
        </div>
    </div>
</div>
