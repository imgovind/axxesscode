﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("New", "Patient", FormMethod.Post, new { @id = "newPatientForm" }))%>
<%  { %>
<div id="newPatientValidaton" class="marginBreak" style="display: none">
</div>
<div class="marginBreak">
    <b>Patient Demographics</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="FirstName">
                            First Name (M0040):&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("FirstName","", new { @id = "txtNew_Patient_FirstName", @class = "text  input_wrapper required", @maxlength = "100", @tabindex = "1" })%>
                        </div>
                    </div>
                    <div class="row ">
                        <label for="MiddleInitial">
                            MI:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("MiddleInitial", "", new { @id = "txtNew_Patient_MiddleInitial", @class = "text input_wrapper", @style = "width:20px;", @tabindex = "2" })%>
                    </div>
                    <div class="row">
                        <label for="LastName">
                            Last Name:&nbsp;&nbsp;&nbsp;</label>
                        <div class="inputs">
                            <%=Html.TextBox("LastName","", new { @id = "txtNew_Patient_LastName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "3" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="Gender">
                            Gender (M0069):</label>
                        <div class="inputs">
                            <%=Html.RadioButton("Gender", "Female", new { @id = "", @class = "required", @tabindex = "4" })%>Female
                            <%=Html.RadioButton("Gender", "Male", new { @id = "", @class = "required", @tabindex = "4" })%>Male
                        </div>
                    </div>
                    <div class="row">
                        <label for="DOB">
                            Date of Birth (M0066):</label>
                        <div class="inputs">
                            <%= Html.Telerik().DatePicker()
                                          .Name("DOB")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new {@id = "txtNew_Patient_DOB", @class = "text required  date", @tabindex = "5" })
                                                                                
                            %>
                        </div>
                    </div>
                    <div class="row">
                        <label for="MaritalStatus">
                            Marital Status:</label>
                        <div class="inputs">
                            <select id="txtNew_Patient_MaritalStatus" name="MaritalStatus" class="input_wrapper"
                                tabindex="7">
                                <option selected="selected" value="0">** Select **</option>
                                <option value="1">Married </option>
                                <option value="2">Divorce </option>
                                <option value="3">Widowed</option>
                                <option value="4">Single </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="PatientID">
                            Patient ID(M0020):</label>
                        <div class="inputs">
                            <%=Html.TextBox("PatientIdNumber","", new { @id = "txtNew_Patient_PatientID", @class = "text required input_wrapper", @maxlength = "20", @tabindex = "8" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="MedicareNumber">
                            Medicare Number (M0063):</label>
                        <div class="inputs">
                            <%=Html.TextBox("MedicareNumber", "", new { @id = "txtNew_Patient_MedicareNumber", @class = "text input_wrapper", @maxlength = "11", @tabindex = "9" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="MedicaidNumber">
                            Medicaid Number(M0065):</label>
                        <div class="inputs">
                            <%=Html.TextBox("MedicaidNumber", "", new { @id = "txtNew_Patient_MedicaidNumber", @class = "text digits input_wrapper", @maxlength = "9", @tabindex = "10" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="SSN">
                            SSN (M0064):</label>
                        <div class="inputs">
                            <%=Html.TextBox("SSN","", new { @id = "txtNew_Patient_SSN", @class = "text digits input_wrapper", @maxlength = "9", @tabindex = "11" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="SOC">
                            Start of Care Date (M0030):</label>
                        <div class="inputs">
                            <%= Html.Telerik().DatePicker()
                                        .Name("StartOfCareDate")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new { @id = "txtNew_Patient_StartOfCareDate", @class = "text required date", @tabindex = "12" })
                            %>
                        </div>
                    </div>
                    <div class="row">
                        <label for="ESD">
                            Episode Start Date :</label>
                        <div class="inputs">
                            <%= Html.Telerik().DatePicker()
                                        .Name("EpisodeStartDate")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new { @id = "txtNew_Patient_EpisodeStartDate", @class = "text required date", @tabindex = "13" })
                            %>
                        </div>
                    </div>
                    <div class="row">
                        <label for="FirstName">
                            &nbsp;&nbsp;&nbsp; Assign to Clinician :&nbsp;&nbsp;&nbsp;</label>
                        <select style="width: 150px;" class="Users input_wrapper required selectDropDown"
                            tabindex="17" name="UserId" id="txtNew_Patient_Assign">
                            <option value="0" selected>** Select Employee **</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="margin">
    <div class="row485">
        <table cellspacing="0" cellpadding="0" border="0">
            <thead>
                <tr>
                    <th colspan='4'>
                        <label for="EthnicRaces">
                            Race/Ethnicity (M0140):</label>
                        <label class="error" for="EthnicRaces" generated="true">
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input type="checkbox" value="0" name="EthnicRaces" class="required" />
                        American Indian or Alaska Native
                    </td>
                    <td>
                        <input type="checkbox" value="1" name="EthnicRaces" class="required" />
                        Asian
                    </td>
                    <td>
                        <input type="checkbox" value="2" name="EthnicRaces" class="required" />
                        Black or African-American
                    </td>
                    <td>
                        <input type="checkbox" value="3" name="EthnicRaces" class="required" />
                        Hispanic or Latino
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" value="4" name="EthnicRaces" class="required" />
                        Native Hawaiian or Pacific Islander
                    </td>
                    <td>
                        <input type="checkbox" value="5" name="EthnicRaces" class="required" />
                        White
                    </td>
                    <td colspan="2">
                        <input type="checkbox" value="6" name="EthnicRaces" class="required" />
                        Unknown
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="margin">
    <div class="row485">
        <table cellspacing="0" cellpadding="0" border="0">
            <thead>
                <tr>
                    <th colspan='4'>
                        <label for="EthnicRaces">
                            Payment Source (M0150):</label>(Mark all that apply)
                        <label class="error" for="PaymentSources" generated="true">
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input type="checkbox" value="0" name="PaymentSources" class="required" />
                        None; no charge for current services
                    </td>
                    <td>
                        <input type="checkbox" value="1" name="PaymentSources" class="required" />
                        Medicare (traditional fee-for-service)
                    </td>
                    <td>
                        <input type="checkbox" value="2" name="PaymentSources" class="required" />
                        Medicare (HMO/ managed care)
                    </td>
                    <td>
                        <input type="checkbox" value="3" name="PaymentSources" class="required" />
                        Medicaid (traditional fee-for-service)
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" value="4" name="PaymentSources" class="required" />
                        Medicaid (HMO/ managed care)
                    </td>
                    <td>
                        <input type="checkbox" value="5" name="PaymentSources" class="required" />
                        Workers' compensation
                    </td>
                    <td>
                        <input type="checkbox" value="6" name="PaymentSources" class="required" />
                        Title programs (e.g., Titile III,V, or XX)
                    </td>
                    <td>
                        <input type="checkbox" value="7" name="PaymentSources" class="required" />
                        Other government (e.g.,CHAMPUS,VA,etc)
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" value="8" name="PaymentSources" class="required" />
                        Private insurance
                    </td>
                    <td>
                        <input type="checkbox" value="9" name="PaymentSources" class="required" />
                        Private HMO/ managed care
                    </td>
                    <td>
                        <input type="checkbox" value="10" name="PaymentSources" class="required" />
                        Self-pay
                    </td>
                    <td>
                        <input type="checkbox" value="11" name="PaymentSources" class="required" />
                        Unknown
                    </td>
                </tr>
                <tr>
                    <td colspan='4'>
                        <input type="checkbox" id="AddNewPatient_PaymentSource" value="12" name="PaymentSources"
                            class="required" />
                        Other (specify) &nbsp;&nbsp;&nbsp;
                        <%=Html.TextBox("OtherPaymentSource", "", new { @id = "txtAdd_Patient_OtherPaymentSource", @class = "text", @style = "display:none;" })%>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="marginBreak">
    <b>Patient Address</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="AddressLine1">
                            Address Line 1:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressLine1", "", new { @id = "txtNew_Patient_AddressLine1", @class = "text required input_wrapper", @tabindex = "14" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine2">
                            Address Line 2:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressLine2", "", new { @id = "txtNew_Patient_AddressLine2", @class = "text input_wrapper", tabindex = "15" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressCity">
                            City:</label>
                        <div class="inputs">
                            <%=Html.TextBox("AddressCity", "", new { @id = "txtNew_Patient_AddressCity", @class = "text required input_wrapper", @tabindex = "16" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressStateCode">
                            State (M0050), Zip(M0060):</label>
                        <div class="inputs">
                            <select style="width: 119px;" class="AddressStateCode input_wrapper required selectDropDown"
                                tabindex="17" name="AddressStateCode" id="txtNew_Patient_AddressStateCode">
                                <option value="0" selected>** Select State **</option>
                            </select>
                            &nbsp;
                            <%=Html.TextBox("AddressZipCode", "", new { @id = "txtNew_Patient_AddressZipCode", @class = "text required digits isValidUSZip", @tabindex = "18", @style = "margin: 0px; padding: 0px; width: 54px;", @size = "5", @maxlength = "5" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="PhoneHome">
                            Home Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <input type="text" class="autotext required digits" style="width: 49.5px; padding: 0px;
                                    margin: 0px;" name="PatientHomePhoneArray" id="txtNew_Patient_HomePhone1" maxlength="3"
                                    size="3" tabindex="19" /></span> - <span class="input_wrappermultible">
                                        <input type="text" class="autotext required digits" style="width: 49px; padding: 0px;
                                            margin: 0px;" name="PatientHomePhoneArray" id="txtNew_Patient_HomePhone2" maxlength="3"
                                            size="3" tabindex="20" /></span> - <span class="input_wrappermultible">
                                                <input type="text" class="autotext required digits" style="width: 49.5px; padding: 0px;
                                                    margin: 0px;" name="PatientHomePhoneArray" id="txtNew_Patient_HomePhone3" maxlength="4"
                                                    size="5" tabindex="21" /></span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="PhoneMobile">
                            Mobile Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                    name="PatientMobilePhoneArray" id="txtNew_Patient_MobilePhone1" maxlength="3"
                                    size="3" tabindex="22" /></span> - <span class="input_wrappermultible">
                                        <input type="text" class="autotext digits" style="width: 49px; padding: 0px; margin: 0px;"
                                            name="PatientMobilePhoneArray" id="txtNew_Patient_MobilePhone2" maxlength="3"
                                            size="3" tabindex="23" /></span> - <span class="input_wrappermultible">
                                                <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                    name="PatientMobilePhoneArray" id="txtNew_Patient_MobilePhone3" maxlength="4"
                                                    size="5" tabindex="24" /></span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressCity">
                            Email:</label>
                        <div class="inputs">
                            <%=Html.TextBox("Email", "", new { @id = "txtNew_Patient_Email", @class = "text input_wrapper", @tabindex = "25" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="halfRow">
    <div class="marginBreak">
        <b>Case Manager</b>
        <div class="rowBreak">
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="PharmacyName">
                                Name :&nbsp;&nbsp;&nbsp;</label>
                            <%=Html.TextBox("CaseManager", "", new { @id = "txtNew_Patient_CaseManager", @class = "text input_wrapper", @maxlength = "20", @tabindex = "26" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="halfRow">
    <div class="marginBreak">
        <b>Pharmacy</b>
        <div class="rowBreak">
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="PharmacyName">
                                Name :&nbsp;&nbsp;&nbsp;</label>
                            <%=Html.TextBox("PharmacyName", "", new { @id = "txtNew_Patient_PharmacyName", @class = "text input_wrapper", @maxlength = "20", @tabindex = "26" })%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentDivider">
                <div class="patientfieldset">
                    <div class="fix">
                        <div class="row">
                            <label for="PhoneHome">
                                Phone:</label>
                            <div class="inputs">
                                <span class="input_wrappermultible">
                                    <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                        name="PharmacyPhoneArray" id="txtNew_Patient_PharmacyPhone1" maxlength="3" size="3"
                                        tabindex="27" /></span> - <span class="input_wrappermultible">
                                            <input type="text" class="autotext digits" style="width: 49px; padding: 0px; margin: 0px;"
                                                name="PharmacyPhoneArray" id="txtNew_Patient_PharmacyPhone2" maxlength="3" size="3"
                                                tabindex="28" /></span> - <span class="input_wrappermultible">
                                                    <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                        name="PharmacyPhoneArray" id="txtNew_Patient_PharmacyPhone3" maxlength="4" size="5"
                                                        tabindex="29" /></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="marginBreak">
    <b>Insurance</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="PrimaryInsurance">
                            Primary:&nbsp;&nbsp;&nbsp;</label>
                        <select name="PrimaryInsurance" id="txtNew_Patient_PrimaryInsurance" class="input_wrapper"
                            tabindex="30">
                            <option value="0">** Select **</option>
                            <option value="1">Advantra Freedom</option>
                            <option value="2">AETNA HEALTH INC.</option>
                            <option value="3">CARE IMPROVEMENT PLUS OF TEXAS INSURANCE</option>
                            <option value="4">FIRST HEALTH LIFE AND HEALTH INSUR</option>
                            <option value="5">Palmetto GBA</option>
                            <option value="6">Well Care HEALT INSURANCE OF ARIZON</option>
                            <option value="7">WELLCARE OF TEXAS, INC.</option>
                        </select>
                    </div>
                    <div class="row">
                        <label for="SecondaryInsurance">
                            Secondary:&nbsp;&nbsp;&nbsp;</label>
                        <select name="SecondaryInsurance" id="txtNew_Patient_SecondaryInsurance" class="input_wrapper"
                            tabindex="31">
                            <option value="0">** Select **</option>
                            <option value="1">Advantra Freedom</option>
                            <option value="2">AETNA HEALTH INC.</option>
                            <option value="3">CARE IMPROVEMENT PLUS OF TEXAS INSURANCE</option>
                            <option value="4">FIRST HEALTH LIFE AND HEALTH INSUR</option>
                            <option value="5">Palmetto GBA</option>
                            <option value="6">Well Care HEALT INSURANCE OF ARIZON</option>
                            <option value="7">WELLCARE OF TEXAS, INC.</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="TertiaryInsurance">
                            Tertiary:&nbsp;&nbsp;&nbsp;</label>
                        <select name="TertiaryInsurance" id="txtNew_Patient_TertiaryInsurance" class="input_wrapper"
                            tabindex="32">
                            <option value="0">** Select **</option>
                            <option value="1">Advantra Freedom</option>
                            <option value="2">AETNA HEALTH INC.</option>
                            <option value="3">CARE IMPROVEMENT PLUS OF TEXAS INSURANCE</option>
                            <option value="4">FIRST HEALTH LIFE AND HEALTH INSUR</option>
                            <option value="5">Palmetto GBA</option>
                            <option value="6">Well Care HEALT INSURANCE OF ARIZON</option>
                            <option value="7">WELLCARE OF TEXAS, INC.</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="marginBreak">
    <b>Primary Emergency Contact</b>
    <div class="rowBreak">
        <!--[if !IE]>start section content top<![endif]-->
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row" style="vertical-align: middle;">
                        <label for="FirstName">
                            First Name:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("EmergencyContact.FirstName", "", new { @id = "txtNew_Patient_EmergencyContactFirstName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "33" })%>
                    </div>
                    <div class="row">
                        <label for="LastName">
                            Last Name:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("EmergencyContact.LastName", "", new { @id = "txtNew_Patient_EmergencyContactLastName", @class = "text input_wrapper required", @maxlength = "100", @tabindex = "34" })%>
                    </div>
                    <div class="row">
                        <label for="PhoneHome">
                            Primary Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <input type="text" class="autotext digits required" style="width: 49.5px; padding: 0px;
                                    margin: 0px;" name="EmergencyContact.EmergencyContactPrimaryPhoneArray" id="txtNew_Patient_EmergencyContactPrimaryPhoneArray1"
                                    maxlength="3" size="3" tabindex="35" /></span> - <span class="input_wrappermultible required">
                                        <input type="text" class="autotext digits required" style="width: 49px; padding: 0px;
                                            margin: 0px;" name="EmergencyContact.EmergencyContactPrimaryPhoneArray" id="txtNew_Patient_EmergencyContactPrimaryPhoneArray2"
                                            maxlength="3" size="3" tabindex="36" /></span> - <span class="input_wrappermultible ">
                                                <input type="text" class="autotext digits required" style="width: 49.5px; padding: 0px;
                                                    margin: 0px;" name="EmergencyContact.EmergencyContactPrimaryPhoneArray" id="txtNew_Patient_EmergencyContactPrimaryPhoneArray3"
                                                    maxlength="4" size="5" tabindex="37" /></span>
                        </div>
                    </div>
                    <div class="row">
                        <label for="PhoneMobile">
                            Alt Phone:</label>
                        <div class="inputs">
                            <span class="input_wrappermultible">
                                <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                    name="EmergencyContact.EmergencyContactAltPhoneArray" id="txtNew_Patient_EmergencyContactAltPhoneArray1"
                                    maxlength="3" size="3" tabindex="38" /></span> - <span class="input_wrappermultible">
                                        <input type="text" class="autotext digits" style="width: 49px; padding: 0px; margin: 0px;"
                                            name="EmergencyContact.EmergencyContactAltPhoneArray" id="txtNew_Patient_EmergencyContactAltPhoneArray2"
                                            maxlength="3" size="3" tabindex="39" /></span> - <span class="input_wrappermultible">
                                                <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                    name="EmergencyContact.EmergencyContactAltPhoneArray" id="txtNew_Patient_EmergencyContactAltPhoneArray3"
                                                    maxlength="4" size="5" tabindex="40" /></span>
                        </div>
                    </div>
                    <div class="row" style="vertical-align: middle;">
                        <label for="Email">
                            Email:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("EmergencyContact.EmailAddress", "", new { @id = "txtNew_Patient_EmergencyContactEmail", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "41" })%>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="Relationship">
                            Relationship:</label>
                        <div class="inputs">
                            <%=Html.TextBox("EmergencyContact.Relationship", "", new { @id = "txtNew_Patient_Relationship", @class = "text input_wrapper required", @tabindex = "42" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine1">
                            Address Line 1:</label>
                        <div class="inputs">
                            <%=Html.TextBox("EmergencyContact.AddressLine1", "", new { @id = "txtNew_Patient_EmergencyContactAddressLine1", @class = "text input_wrapper required", @tabindex = "43" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressLine2">
                            Address Line 2:</label>
                        <div class="inputs">
                            <%=Html.TextBox("EmergencyContact.AddressLine2", "", new { @id = "txtNew_Patient_EmergencyContactAddressLine2", @class = "text input_wrapper", tabindex = "44" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressCity">
                            City:</label>
                        <div class="inputs">
                            <%=Html.TextBox("EmergencyContact.AddressCity", "", new { @id = "txtNew_Patient_EmergencyContactAddressCity", @class = "text input_wrapper required", @tabindex = "45" })%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="AddressStateCode">
                            State, Zip Code :</label>
                        <div class="inputs">
                            <select id="txtNew_Patient_EmergencyContactAddressStateCode" name="EmergencyContact.AddressState"
                                class="AddressStateCode input_wrapper required" style="width: 119px;" tabindex="46">
                                <option selected value="0">** Select State **</option>
                            </select>
                            &nbsp;
                            <%=Html.TextBox("EmergencyContact.AddressZipCode", "", new { @id = "txtNew_Patient_EmergencyContactAddressZipCode", @class = "text digits isValidUSZip ", @tabindex = "47", @style = "width: 54px; padding: 0px; margin: 0px;", @size = "5", @maxlength = "5" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="marginBreak">
    <b>Primary Physician</b>
    <div class="rowTable">
        <table border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>
                        <label>
                            Select from the list below:</label>
                        <select style="width: 150px;" class="Physicians input_wrapper " tabindex="17" name="AgencyPhysicians"
                            id="txtNew_Patient_PhysicianDropDown">
                            <option value="0" selected="selected">** Select Physician **</option>
                        </select>
                    </th>
                    <th>
                        <label>
                            Search by NPI Number:</label>
                        <input type="text" name="txtNew_NpiNumber" id="txtNew_NpiNumber" maxlength="10" />
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2">
                        <div class="rowBreak">
                            <!--[if !IE]>start section content top<![endif]-->
                            <div class="row">
                                <div class="contentDivider">
                                    <div class="patientfieldset">
                                        <div class="fix">
                                            <div class="row">
                                                <label for="FirstName">
                                                    First Name:&nbsp;&nbsp;&nbsp;</label>
                                                <%=Html.TextBox("PhysicianContact.FirstName", "", new { @id = "txtNew_Patient_FirstNamePhysicianContact", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "48" })%>
                                            </div>
                                            <div class="row">
                                                <label for="LastName">
                                                    Last Name:&nbsp;&nbsp;&nbsp;</label>
                                                <%=Html.TextBox("PhysicianContact.LastName", "", new { @id = "txtNew_Patient_LastNamePhysicianContact", @class = "text input_wrapper required", @maxlength = "20", @tabindex = "49" })%>
                                            </div>
                                            <div class="row">
                                                <label for="PhoneHome">
                                                    Primary Phone:</label>
                                                <div class="inputs">
                                                    <span class="input_wrappermultible">
                                                        <input type="text" class="autotext digits required" style="width: 49.5px; padding: 0px;
                                                            margin: 0px;" name="PhysicianContact.PhysicianContactPrimaryPhoneArray" id="txtNew_Patient_PhysicianContactPrimaryPhoneArray1"
                                                            maxlength="3" size="3" tabindex="50" /></span> - <span class="input_wrappermultible">
                                                                <input type="text" class="autotext digits required" style="width: 49px; padding: 0px;
                                                                    margin: 0px;" name="PhysicianContact.PhysicianContactPrimaryPhoneArray" id="txtNew_Patient_PhysicianContactPrimaryPhoneArray2"
                                                                    maxlength="3" size="3" tabindex="51" /></span> - <span class="input_wrappermultible">
                                                                        <input type="text" class="autotext digits required" style="width: 49.5px; padding: 0px;
                                                                            margin: 0px;" name="PhysicianContact.PhysicianContactPrimaryPhoneArray" id="txtNew_Patient_PhysicianContactPrimaryPhoneArray3"
                                                                            maxlength="4" size="5" tabindex="52" /></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label for="Fax">
                                                    Fax:</label>
                                                <div class="inputs">
                                                    <span class="input_wrappermultible">
                                                        <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                            name="PhysicianContact.PhysicianContactAltPhoneArray" id="txtNew_Patient_PhysicianContactFax1"
                                                            maxlength="3" size="3" tabindex="53" /></span> - <span class="input_wrappermultible">
                                                                <input type="text" class="autotext digits" style="width: 49px; padding: 0px; margin: 0px;"
                                                                    name="PhysicianContact.PhysicianContactAltPhoneArray" id="txtNew_Patient_PhysicianContactFax2"
                                                                    maxlength="3" size="3" tabindex="54" /></span> - <span class="input_wrappermultible">
                                                                        <input type="text" class="autotext digits" style="width: 49.5px; padding: 0px; margin: 0px;"
                                                                            name="PhysicianContact.PhysicianContactAltPhoneArray" id="txtNew_Patient_PhysicianContactFax3"
                                                                            maxlength="4" size="5" tabindex="55" /></span>
                                                </div>
                                            </div>
                                            <div class="row" style="vertical-align: middle;">
                                                <label for="FirstName">
                                                    Email:&nbsp;&nbsp;&nbsp;</label>
                                                <%=Html.TextBox("PhysicianContact.EmailAddress", "", new { @id = "txtNew_Patient_PhysicianContactEmail", @class = "text email input_wrapper", @maxlength = "100", @tabindex = "56" })%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contentDivider">
                                    <div class="patientfieldset">
                                        <div class="fix">
                                            <div class="row">
                                                <label for="Relationship">
                                                    NPI No:</label>
                                                <div class="inputs">
                                                    <%=Html.TextBox("PhysicianContact.NPI", "", new { @id = "txtNew_Patient_PhysicianContactNPINo", @class = "text input_wrapper digits", @maxlength = "10", @tabindex = "57" })%>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label for="AddressLine1">
                                                    Address Line 1:</label>
                                                <div class="inputs">
                                                    <%=Html.TextBox("PhysicianContact.MailingAddressLine1", "", new { @id = "txtNew_Patient_PhysicianContactAddressLine1", @class = "text input_wrapper required", @tabindex = "58" })%>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label for="AddressLine2">
                                                    Address Line 2:</label>
                                                <div class="inputs">
                                                    <%=Html.TextBox("PhysicianContact.MailingAddressLine2", "", new { @id = "txtNew_Patient_PhysicianContactAddressLine2", @class = "text input_wrapper ", tabindex = "59" })%>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label for="AddressCity">
                                                    City:</label>
                                                <div class="inputs">
                                                    <%=Html.TextBox("PhysicianContact.MailingAddressCity", "", new { @id = "txtNew_Patient_PhysicianContactAddressCity", @class = "text input_wrapper required", @tabindex = "60" })%>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label for="AddressStateCode">
                                                    State, Zip Code :</label>
                                                <div class="inputs">
                                                    <select name="PhysicianContact.MailingAddressStateCode" class="AddressStateCode input_wrapper required"
                                                        style="width: 119px;" tabindex="61" id="txtNew_Patient_PhysicianContactAddressStateCode">
                                                        <option selected value="0">** Select State **</option>
                                                    </select>
                                                    &nbsp;
                                                    <%=Html.TextBox("PhysicianContact.MailingAddressZipCode", "", new { @id = "txtNew_Patient_PhysicianContactAddressZipCode", @class = "text digits isValidUSZip", @tabindex = "62", @style = "width: 54px; padding: 0px; margin: 0px;", @size = "5", @maxlength = "5" })%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="marginBreak">
    <b>Referral</b>
    <div class="rowBreak">
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="AdmissionSource">
                            Admission Source:&nbsp;&nbsp;&nbsp;</label>
                        <select name="AdmissionSource" id="txtNew_Patient_AdmissionSource" class="input_wrapper AdmissionSource"
                            tabindex="64">
                            <option value="0" selected="selected">** Select **</option>
                        </select>
                    </div>
                    <div class="row">
                        <label for="ReferralDate">
                            Referral Date:</label>
                        <div class="inputs">
                            <%= Html.Telerik().DatePicker()
                                        .Name("PatientReferralDate")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new { @id = "txtNew_Patient_PatientReferralDate", @class = "text date", @tabindex = "65" })
                                                                                
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contentDivider">
            <div class="patientfieldset">
                <div class="fix">
                    <div class="row">
                        <label for="FirstName">
                            FirstName:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("OtherSourceFirstName", "", new { @id = "txtNew_Patient_OtherSourceFirstName", @class = "text input_wrapper", @maxlength = "20", @tabindex = "67" })%>
                    </div>
                    <div class="row">
                        <label for="FirstName">
                            LastName:&nbsp;&nbsp;&nbsp;</label>
                        <%=Html.TextBox("OtherSourceLastName", "", new { @id = "txtNew_Patient_OtherSourceLastName", @class = "text input_wrapper", @maxlength = "20", @tabindex = "68" })%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="buttons buttonfix">
        <ul>
            <li>
                <input name="" type="submit" value="Save" /></li>
            <li>
                <input name="" type="button" value="Cancel" onclick="Patient.Close($(this));" /></li>
            <li>
                <input name="" type="reset" value="Reset" /></li>
        </ul>
    </div>
</div>
<%} %>
