﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="editDischarge" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span id="DischargeFromAgencyTitle" class="float_left">Edit Discharge from Agency
            
            
            </span><span class="float_right"><a href="javascript:void(0);" class="window_min"></a>
                <a href="javascript:void(0);" class="window_resize"></a><a href="javascript:void(0);"
                    class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form oasisAssWindowContent">
            <div class="oasisAssWindowContainer">
                <div id="editDischargeTabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
                    <ul>
                        <li><a href="#editClinicalRecord_discharge">Clinical Record Items</a></li>
                        <li><a href="#editRiskassessment_discharge">Risk Assessment</a></li>
                        <li><a href="#editSensorystatus_discharge">Sensory Status</a></li>
                        <li><a href="#editIntegumentarystatus_discharge">Integumentary Status</a></li>
                        <li><a href="#editRespiratorystatus_discharge">Respiratory Status</a></li>
                        <li><a href="#editCardiacstatus_discharge">Cardiac Status</a></li>
                        <li><a href="#editEliminationstatus_discharge">Elimination Status</a></li>
                        <li><a href="#editBehaviourialstatus_discharge">Neuro/Behaviourial Status</a></li>
                        <li><a href="#editAdl_discharge">ADL/IADLs</a></li>
                        <li><a href="#editMedications_discharge">Medications</a></li>
                        <li><a href="#editCaremanagement_discharge">Care Management</a></li>
                        <li><a href="#editEmergentcare_discharge">Emergent Care</a></li>
                        <li><a href="#editDischargeOnlyAdd_discharge">Discharge</a></li>
                    </ul>
                    <div style="width: 179px;">
                        <input id="dischargeValidation" type="button" value="Validate" onclick="Discharge.Validate(); JQD.open_window('#validation');" /></div>
                    <div id="editClinicalRecord_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyDemographicsForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment","DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0010) CMS Certification Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_M0010CertificationNumber" name="DischargeFromAgency_M0010CertificationNumber"
                                        type="text" class="text" value="" />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0014) Branch State:</div>
                                </div>
                                <div class="right marginOasis">
                                    <select class="AddressStateCode" name="DischargeFromAgency_M0014BranchState" id="DischargeFromAgency_M0014BranchState">
                                        <option value=" " selected>** Select State **</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0016) Branch ID Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_M0016BranchId" name="DischargeFromAgency_M0016BranchId"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0018) National Provider Identifier (NPI)</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_M0018NationalProviderId" name="DischargeFromAgency_M0018NationalProviderId"
                                        type="text" class="text" /><br />
                                    <input type="hidden" name="DischargeFromAgency_M0018NationalProviderIdUnknown" value=" " />
                                    <input type="checkbox" id="DischargeFromAgency_M0018NationalProviderIdUnknown" name="DischargeFromAgency_M0018NationalProviderIdUnknown"
                                        value="1" />&nbsp;UK – Unknown or Not Available</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0020) Patient ID Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_M0020PatientIdNumber" name="DischargeFromAgency_M0020PatientIdNumber"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0030) Start of Care Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_M0030SocDate" name="DischargeFromAgency_M0030SocDate"
                                        type="text" class="text" /></div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        Episode Start Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_GenericEpisodeStartDate" name="DischargeFromAgency_GenericEpisodeStartDate"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0032) Resumption of Care Date:
                                    </div>
                                </div>
                                <div class="padding">
                                    <input id="DischargeFromAgency_M0032ROCDate" name="DischargeFromAgency_M0032ROCDate"
                                        type="text" class="text" />
                                    <br />
                                    <input type="hidden" name="DischargeFromAgency_M0032ROCDateNotApplicable" value="" />
                                    <input id="DischargeFromAgency_M0032ROCDateNotApplicable" name="DischargeFromAgency_M0032ROCDateNotApplicable"
                                        type="checkbox" value="1" />&nbsp;NA - Not Applicable
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0040) Patient Name:</div>
                                </div>
                                <div class="right marginOasis">
                                    <div class="padding">
                                        Suffix :
                                        <input id="DischargeFromAgency_M0040Suffix" name="DischargeFromAgency_M0040Suffix"
                                            style="width: 20px;" type="text" class="text" />
                                        &nbsp; First :
                                        <input id="DischargeFromAgency_M0040FirstName" name="DischargeFromAgency_M0040FirstName"
                                            type="text" class="text" />
                                        <br />
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MI :
                                        <input id="DischargeFromAgency_M0040MI" name="DischargeFromAgency_M0040MI" style="width: 20px;"
                                            type="text" class="text" />&nbsp; &nbsp;Last:
                                        <input id="DischargeFromAgency_M0040LastName" name="DischargeFromAgency_M0040LastName"
                                            type="text" class="text" />
                                    </div>
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0050) Patient State of Residence:</div>
                                </div>
                                <div class="right marginOasis">
                                    <select class="AddressStateCode" name="DischargeFromAgency_M0050PatientState" id="DischargeFromAgency_M0050PatientState">
                                        <option value="0" selected>** Select State **</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0060) Patient Zip Code:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_M0060PatientZipCode" name="DischargeFromAgency_M0060PatientZipCode"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0063) Medicare Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_M0063PatientMedicareNumber" name="DischargeFromAgency_M0063PatientMedicareNumber"
                                        type="text" class="text" /><br />
                                    <input name="DischargeFromAgency_M0063PatientMedicareNumberUnknown" type="hidden"
                                        value=" " />
                                    <input id="DischargeFromAgency_M0063PatientMedicareNumberUnknown" name="DischargeFromAgency_M0063PatientMedicareNumberUnknown"
                                        type="checkbox" />&nbsp;NA – No Medicare</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0064) Social Security Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_M0064PatientSSN" name="DischargeFromAgency_M0064PatientSSN"
                                        type="text" class="text" /><br />
                                    <input name="DischargeFromAgency_M0064PatientSSNUnknown" type="hidden" value=" " />
                                    <input id="DischargeFromAgency_M0064PatientSSNUnknown" name="DischargeFromAgency_M0064PatientSSNUnknown"
                                        type="checkbox" />&nbsp;UK – Unknown or Not Available</div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0065) Medicaid Number:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_M0065PatientMedicaidNumber" name="DischargeFromAgency_M0065PatientMedicaidNumber"
                                        type="text" class="text" /><br />
                                    <input name="DischargeFromAgency_M0065PatientMedicaidNumberUnknown" type="hidden"
                                        value=" " />
                                    <input id="DischargeFromAgency_M0065PatientMedicaidNumberUnknown" name="DischargeFromAgency_M0065PatientMedicaidNumberUnknown"
                                        type="checkbox" />&nbsp;NA – No Medicaid</div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0066) Birth Date:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input id="DischargeFromAgency_M0066PatientDoB" name="DischargeFromAgency_M0066PatientDoB"
                                        type="text" class="text" /></div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow  title">
                                    <div class="padding">
                                        (M0069) Gender:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input name="DischargeFromAgency_M0069Gender" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M0069Gender" type="radio" value="1" />Male
                                    <input name="DischargeFromAgency_M0069Gender" type="radio" value="2" />Female
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0080) Discipline of Person Completing Assessment:
                                    </div>
                                </div>
                                <div class="padding">
                                    <%=Html.Hidden("DischargeFromAgency_M0080DisciplinePerson", " ", new { @id = "" })%>
                                    <%=Html.RadioButton("DischargeFromAgency_M0080DisciplinePerson", "1", new { @id = "" })%>&nbsp;1
                                    - RN
                                    <%=Html.RadioButton("DischargeFromAgency_M0080DisciplinePerson", "2", new { @id = "" })%>&nbsp;2
                                    - PT
                                    <%=Html.RadioButton("DischargeFromAgency_M0080DisciplinePerson", "3", new { @id = "" })%>&nbsp;3
                                    - SLP/ST
                                    <%=Html.RadioButton("DischargeFromAgency_M0080DisciplinePerson", "4", new { @id = "" })%>&nbsp;4
                                    - OT
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="padding">
                                        (M0090) Date Assessment Completed:</div>
                                </div>
                                <div class="right marginOasis">
                                    <input name="DischargeFromAgency_M0090AssessmentCompleted" id="DischargeFromAgency_M0090AssessmentCompleted"
                                        type="text" class="text" /></div>
                            </div>
                        </div>
                        <div class="rowOasis assessmentType">
                            <div class="insiderow title">
                                <div class="padding">
                                    (M0100) This Assessment is Currently Being Completed for the Following Reason:</div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow margin">
                                    <u>Start/Resumption of Care</u></div>
                                <div class="insiderow margin">
                                    <input name="DischargeFromAgency_M0100AssessmentType" type="radio" value="01" />&nbsp;1
                                    – Start of care—further visits planned<br />
                                    <input name="DischargeFromAgency_M0100AssessmentType" type="radio" value="03" />&nbsp;3
                                    – Resumption of care (after inpatient stay)<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow">
                                    <u>Follow-Up</u></div>
                                <div class="insiderow">
                                    <input name="DischargeFromAgency_M0100AssessmentType" type="radio" value="04" />&nbsp;4
                                    – Recertification (follow-up) reassessment [ Go to M0110 ]<br />
                                    <input name="DischargeFromAgency_M0100AssessmentType" type="radio" value="05" />&nbsp;5
                                    – Other follow-up [ Go to M0110 ]<br />
                                </div>
                            </div>
                            <div class="insideColFull">
                                <div class="insiderow margin">
                                    <u>Transfer to an Inpatient Facility</u></div>
                                <div class="insiderow margin">
                                    <input name="DischargeFromAgency_M0100AssessmentType" type="radio" value="06" />&nbsp;6
                                    – Transferred to an inpatient facility—patient not discharged from agency [ Go to
                                    M1040]<br />
                                    <input name="DischargeFromAgency_M0100AssessmentType" type="radio" value="07" />&nbsp;7
                                    – Transferred to an inpatient facility—patient discharged from agency [ Go to M1040
                                    ]<br />
                                </div>
                            </div>
                            <div class="insideColFull">
                                <div class="insiderow margin">
                                    <u>Discharge from Agency — Not to an Inpatient Facility</u></div>
                                <div class="insiderow margin">
                                    <input name="DischargeFromAgency_M0100AssessmentType" type="radio" value="08" />&nbsp;8
                                    – Death at home [ Go to M0903 ]<br />
                                    <input name="DischargeFromAgency_M0100AssessmentType" type="radio" value="09" />&nbsp;9
                                    – Discharge from agency [ Go to M1040 ]<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow title">
                                <div class="padding">
                                    (M0140) Race/Ethnicity: (Mark all that apply.)</div>
                            </div>
                            <div class="insideCol">
                                <div class="padding">
                                    <input type="hidden" name="DischargeFromAgency_M0140RaceAMorAN" value="" />
                                    <input name="DischargeFromAgency_M0140RaceAMorAN" type="checkbox" value="1" />&nbsp;1
                                    - American Indian or Alaska Native<br />
                                    <input type="hidden" name="DischargeFromAgency_M0140RaceAsia" value="" />
                                    <input name="DischargeFromAgency_M0140RaceAsia" type="checkbox" value="1" />&nbsp;2
                                    - Asian<br />
                                    <input type="hidden" name="DischargeFromAgency_M0140RaceBalck" value="" />
                                    <input name="DischargeFromAgency_M0140RaceBalck" type="checkbox" value="1" />&nbsp;3
                                    - Black or African-American<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="padding">
                                    <input type="hidden" name="DischargeFromAgency_M0140RaceHispanicOrLatino" value="" />
                                    <input name="DischargeFromAgency_M0140RaceHispanicOrLatino" type="checkbox" value="1" />&nbsp;4
                                    - Hispanic or Latino<br />
                                    <input type="hidden" name="DischargeFromAgency_M0140RaceNHOrPI" value="" />
                                    <input name="DischargeFromAgency_M0140RaceNHOrPI" type="checkbox" value="1" />&nbsp;5
                                    - Native Hawaiian or Pacific Islander<br />
                                    <input type="hidden" name="DischargeFromAgency_M0140RaceWhite" value="" />
                                    <input name="DischargeFromAgency_M0140RaceWhite" type="checkbox" value="1" />&nbsp;6
                                    - White
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull title">
                                <div class="padding">
                                    (M0150) Current Payment Sources for Home Care: (Mark all that apply.)</div>
                            </div>
                            <div class="insideCol ">
                                <div class="padding">
                                    <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceNone" value="" />
                                    <input name="DischargeFromAgency_M0150PaymentSourceNone" type="checkbox" value="1" />&nbsp;0
                                    - None; no charge for current services<br />
                                    <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceMCREFFS" value="" />
                                    <input name="DischargeFromAgency_M0150PaymentSourceMCREFFS" type="checkbox" value="1" />&nbsp;1
                                    - Medicare (traditional fee-for-service)<br />
                                    <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceMCREHMO" value="" />
                                    <input name="DischargeFromAgency_M0150PaymentSourceMCREHMO" type="checkbox" value="1" />&nbsp;2
                                    - Medicare (HMO/managed care/Advantage plan)<br />
                                    <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceMCAIDFFS" value="" />
                                    <input name="DischargeFromAgency_M0150PaymentSourceMCAIDFFS" type="checkbox" value="1" />&nbsp;3
                                    - Medicaid (traditional fee-for-service)<br />
                                    <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceMACIDHMO" value="" />
                                    <input name="DischargeFromAgency_M0150PaymentSourceMACIDHMO" type="checkbox" value="1" />&nbsp;4
                                    - Medicaid (HMO/managed care)
                                    <br />
                                    <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceWRKCOMP" value="" />
                                    <input name="DischargeFromAgency_M0150PaymentSourceWRKCOMP" type="checkbox" value="1" />&nbsp;5
                                    - Workers' compensation<br />
                                    <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceTITLPRO" value="" />
                                    <input name="DischargeFromAgency_M0150PaymentSourceTITLPRO" type="checkbox" value="1" />&nbsp;6
                                    - Title programs (e.g., Title III, V, or XX)<br />
                                </div>
                            </div>
                            <div class="insideCol adjust">
                                <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceOTHGOVT" value="" />
                                <input name="DischargeFromAgency_M0150PaymentSourceOTHGOVT" type="checkbox" value="1" />&nbsp;7
                                - Other government (e.g., TriCare, VA, etc.)<br />
                                <input type="hidden" name="DischargeFromAgency_M0150PaymentSourcePRVINS" value="" />
                                <input name="DischargeFromAgency_M0150PaymentSourcePRVINS" type="checkbox" value="1" />&nbsp;8
                                - Private insurance<br />
                                <input type="hidden" name="DischargeFromAgency_M0150PaymentSourcePRVHMO" value="" />
                                <input name="DischargeFromAgency_M0150PaymentSourcePRVHMO" type="checkbox" value="1" />&nbsp;9
                                - Private HMO/managed care<br />
                                <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceSelfPay" value="" />
                                <input name="DischargeFromAgency_M0150PaymentSourceSelfPay" type="checkbox" value="1" />&nbsp;10
                                - Self-pay<br />
                                <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceOtherSRS" value="" />
                                <input name="DischargeFromAgency_M0150PaymentSourceOtherSRS" type="checkbox" value="1" />&nbsp;11
                                - Other (specify)&nbsp;&nbsp;&nbsp;<input type="text" name="DischargeFromAgency_M0150PaymentSourceOther"
                                    id="DischargeFromAgency_M0150PaymentSourceOther" /><br />
                                <input type="hidden" name="DischargeFromAgency_M0150PaymentSourceUnknown" value="" />
                                <input name="DischargeFromAgency_M0150PaymentSourceUnknown" type="checkbox" value="1" />&nbsp;UK
                                - Unknown<br />
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editRiskassessment_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyRiskAssessmentForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", " ")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1040) Influenza Vaccine: Did the patient receive the influenza vaccine from your
                                        agency for this year’s influenza season (October 1 through March 31) during this
                                        episode of care?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1040InfluenzaVaccine" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1040InfluenzaVaccine" type="radio" value="00" />&nbsp;0
                                    - No<br />
                                    <input name="DischargeFromAgency_M1040InfluenzaVaccine" id="DischargeFromAgency_M1040InfluenzaVaccineYes"
                                        type="radio" value="01" />&nbsp;1 - Yes [ Go to M1050 ]<br />
                                    <input name="DischargeFromAgency_M1040InfluenzaVaccine" id="DischargeFromAgency_M1040InfluenzaVaccineNA"
                                        type="radio" value="NA" />&nbsp;NA - Does not apply because entire episode of
                                    care (SOC/ROC to Transfer/Discharge) is outside this influenza season. [ Go to M1050
                                    ]<br />
                                </div>
                            </div>
                            <div class="insideCol" id="discharge_M1045">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1045) Reason Influenza Vaccine not received: If the patient did not receive the
                                        influenza vaccine from your agency during this episode of care, state reason:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason" type="radio"
                                        value="01" />&nbsp;1 - Received from another health care provider (e.g., physician)<br />
                                    <input name="DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason" type="radio"
                                        value="02" />&nbsp;2 - Received from your agency previously during this year’s
                                    flu season<br />
                                    <input name="DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason" type="radio"
                                        value="03" />&nbsp;3 - Offered and declined<br />
                                    <input name="DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason" type="radio"
                                        value="04" />&nbsp;4 - Assessed and determined to have medical contraindication(s)<br />
                                    <input name="DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason" type="radio"
                                        value="05" />&nbsp;5 - Not indicated; patient does not meet age/condition guidelines
                                    for influenza vaccine<br />
                                    <input name="DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason" type="radio"
                                        value="06" />&nbsp;6 - Inability to obtain vaccine due to declared shortage<br />
                                    <input name="DischargeFromAgency_M1045InfluenzaVaccineNotReceivedReason" type="radio"
                                        value="07" />&nbsp;7 - None of the above
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1050) Pneumococcal Vaccine: Did the patient receive pneumococcal polysaccharide
                                        vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge)?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1050PneumococcalVaccine" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1050PneumococcalVaccine" type="radio" value="0" />&nbsp;0
                                    - No<br />
                                    <input name="DischargeFromAgency_M1050PneumococcalVaccine" type="radio" value="1" />&nbsp;1
                                    - Yes [ Go to M1500 at TRN; Go to M1230 at DC ]
                                </div>
                            </div>
                            <div class="insideCol" id="discharge_M1055">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1055) Reason PPV not received: If patient did not receive the pneumococcal polysaccharide
                                        vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge),
                                        state reason:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1055PPVNotReceivedReason" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1055PPVNotReceivedReason" type="radio" value="01" />&nbsp;1
                                    - Patient has received PPV in the past<br />
                                    <input name="DischargeFromAgency_M1055PPVNotReceivedReason" type="radio" value="02" />&nbsp;2
                                    - Offered and declined<br />
                                    <input name="DischargeFromAgency_M1055PPVNotReceivedReason" type="radio" value="03" />&nbsp;3
                                    - Assessed and determined to have medical contraindication(s)<br />
                                    <input name="DischargeFromAgency_M1055PPVNotReceivedReason" type="radio" value="04" />&nbsp;4
                                    - Not indicated; patient does not meet age/condition guidelines for PPV<br />
                                    <input name="DischargeFromAgency_M1055PPVNotReceivedReason" type="radio" value="05" />&nbsp;5
                                    - None of the above
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editSensorystatus_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencySensoryStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1230) Speech and Oral (Verbal) Expression of Language (in patient's own language):
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1230SpeechAndOral" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1230SpeechAndOral" type="radio" value="00" />&nbsp;0
                                    - Expresses complex ideas, feelings, and needs clearly, completely, and easily in
                                    all situations with no observable impairment.<br />
                                    <input name="DischargeFromAgency_M1230SpeechAndOral" type="radio" value="01" />&nbsp;1
                                    - Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional
                                    errors in word choice, grammar or speech intelligibility; needs minimal prompting
                                    or assistance).<br />
                                    <input name="DischargeFromAgency_M1230SpeechAndOral" type="radio" value="02" />&nbsp;2
                                    - Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance,
                                    errors in word choice, organization or speech intelligibility). Speaks in phrases
                                    or short sentences.<br />
                                    <input name="DischargeFromAgency_M1230SpeechAndOral" type="radio" value="03" />&nbsp;3
                                    - Has severe difficulty expressing basic ideas or needs and requires maximal assistance
                                    or guessing by listener. Speech limited to single words or short phrases.<br />
                                    <input name="DischargeFromAgency_M1230SpeechAndOral" type="radio" value="04" />&nbsp;4
                                    - Unable to express basic needs even with maximal prompting or assistance but is
                                    not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).<br />
                                    <input name="DischargeFromAgency_M1230SpeechAndOral" type="radio" value="05" />&nbsp;5
                                    - Patient nonresponsive or unable to speak.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1242) Frequency of Pain Interfering with patient's activity or movement:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1242PainInterferingFrequency" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1242PainInterferingFrequency" type="radio" value="00" />&nbsp;0
                                    - Patient has no pain<br />
                                    <input name="DischargeFromAgency_M1242PainInterferingFrequency" type="radio" value="01" />&nbsp;1
                                    - Patient has pain that does not interfere with activity or movement<br />
                                    <input name="DischargeFromAgency_M1242PainInterferingFrequency" type="radio" value="02" />&nbsp;2
                                    - Less often than daily<br />
                                    <input name="DischargeFromAgency_M1242PainInterferingFrequency" type="radio" value="03" />&nbsp;3
                                    - Daily, but not constantly<br />
                                    <input name="DischargeFromAgency_M1242PainInterferingFrequency" type="radio" value="04" />&nbsp;4
                                    - All of the time
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editIntegumentarystatus_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyIntegumentaryStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1306) Does this patient have at least one Unhealed Pressure Ulcer at Stage II
                                        or Higher or designated as "unstageable"?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1306UnhealedPressureUlcers" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1306UnhealedPressureUlcers" type="radio" value="0" />&nbsp;0
                                    - No [ Go to M1322 ]<br />
                                    <input name="DischargeFromAgency_M1306UnhealedPressureUlcers" type="radio" value="1" />&nbsp;1
                                    - Yes
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="discharge_M1307">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1307) The Oldest Non-epithelialized Stage II Pressure Ulcer that is present at
                                        discharge
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1307NonEpithelializedStageTwoUlcer" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M1307NonEpithelializedStageTwoUlcer" type="radio"
                                        value="01" />&nbsp;1 - Was present at the most recent SOC/ROC assessment<br />
                                    <input name="DischargeFromAgency_M1307NonEpithelializedStageTwoUlcer" type="radio"
                                        value="02" />&nbsp;2 - Developed since the most recent SOC/ROC assessment: record
                                    date pressure ulcer first identified:&nbsp;
                                    <input id="DischargeFromAgency_M1307NonEpithelializedStageTwoUlcerDate" name="DischargeFromAgency_M1307NonEpithelializedStageTwoUlcerDate"
                                        type="text" />
                                    <br />
                                    <input name="DischargeFromAgency_M1307NonEpithelializedStageTwoUlcer" type="radio"
                                        value="NA" />&nbsp;NA - No non-epithelialized Stage II pressure ulcers are present
                                    at discharge<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="discharge_M1308">
                            <div class="insideColFull">
                                <div class="insideColFull title">
                                    <div class="margin">
                                        (M1308) Current Number of Unhealed (non-epithelialized) Pressure Ulcers at Each
                                        Stage: (Enter “0” if none; excludes Stage I pressure ulcers)
                                    </div>
                                </div>
                                <div class="insideColFull">
                                    <div class="margin">
                                        <table class="agency-data-table" id="Table2">
                                            <thead>
                                                <tr>
                                                    <th>
                                                    </th>
                                                    <th>
                                                        Column 1<br />
                                                        Complete at SOC/ROC/FU & D/C
                                                    </th>
                                                    <th>
                                                        Column 2<br />
                                                        Complete at FU & D/C
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        Stage description – unhealed pressure ulcers
                                                    </th>
                                                    <th>
                                                        Number Currently Present
                                                    </th>
                                                    <th>
                                                        Number of those listed in Column 1 that were present on admission (most recent SOC
                                                        / ROC)
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        a. Stage II: Partial thickness loss of dermis presenting as a shallow open ulcer
                                                        with red pink wound bed, without slough. May also present as an intact or open/ruptured
                                                        serum-filled blister.
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedStageTwoUlcerCurrent"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedStageTwoUlcerCurrent" type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedStageTwoUlcerAdmission"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedStageTwoUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        b. Stage III: Full thickness tissue loss. Subcutaneous fat may be visible but bone,
                                                        tendon, or muscles are not exposed. Slough may be present but does not obscure the
                                                        depth of tissue loss. May include undermining and tunneling.
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedStageThreeUlcerCurrent"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedStageThreeUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedStageThreeUlcerAdmission"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedStageThreeUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        c. Stage IV: Full thickness tissue loss with visible bone, tendon, or muscle. Slough
                                                        or eschar may be present on some parts of the wound bed. Often includes undermining
                                                        and tunneling.
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedStageFourUlcerCurrent"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedStageFourUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedStageIVUlcerAdmission"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedStageIVUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        d.1 Unstageable: Known or likely but unstageable due to non-removable dressing or
                                                        device
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIUlcerCurrent"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIUlcerAdmission"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        d.2 Unstageable: Known or likely but unstageable due to coverage of wound bed by
                                                        slough and/or eschar.
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        d.3 Unstageable: Suspected deep tissue injury in evolution.
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIIUlcerCurrent"
                                                            type="text" />
                                                    </td>
                                                    <td>
                                                        <input id="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"
                                                            name="DischargeFromAgency_M1308NumberNonEpithelializedUnstageableIIIUlcerAdmission"
                                                            type="text" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="discharge_M13010_12_14">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        Directions for M1310, M1312, and M1314: If the patient has one or more unhealed
                                        (non-epithelialized) Stage III or IV pressure ulcers, identify the Stage III or
                                        IV pressure ulcer with the largest surface dimension (length x width) and record
                                        in centimeters. If no Stage III or Stage IV pressure ulcers, go to M1320.
                                    </div>
                                </div>
                                <div class="margin">
                                    <div class="insiderow">
                                        (M1310) Pressure Ulcer Length: Longest length "head-to-toe" &nbsp;&nbsp;<input id="DischargeFromAgency_M1310PressureUlcerLength"
                                            name="DischargeFromAgency_M1310PressureUlcerLength" type="text" />
                                    </div>
                                    <div class="insiderow">
                                        (M1312) Pressure Ulcer Width: Width of the same pressure ulcer; greatest width perpendicular
                                        to the length &nbsp;&nbsp;<input id="DischargeFromAgency_M1312PressureUlcerWidth"
                                            name="DischargeFromAgency_M1312PressureUlcerWidth" type="text" />
                                    </div>
                                    <div class="insiderow">
                                        (M1314) Pressure Ulcer Depth: Depth of the same pressure ulcer; from visible surface
                                        to the deepest area &nbsp;&nbsp;<input id="DischargeFromAgency_M1314PressureUlcerDepth"
                                            name="DischargeFromAgency_M1314PressureUlcerDepth" type="text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol" id="discharge_M1320">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1320) Status of Most Problematic (Observable) Pressure Ulcer:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1320MostProblematicPressureUlcerStatus" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M1320MostProblematicPressureUlcerStatus" type="radio"
                                        value="00" />&nbsp;0 - Newly epithelialized<br />
                                    <input name="DischargeFromAgency_M1320MostProblematicPressureUlcerStatus" type="radio"
                                        value="01" />&nbsp;1 - Fully granulating<br />
                                    <input name="DischargeFromAgency_M1320MostProblematicPressureUlcerStatus" type="radio"
                                        value="02" />&nbsp;2 - Early/partial granulation<br />
                                    <input name="DischargeFromAgency_M1320MostProblematicPressureUlcerStatus" type="radio"
                                        value="03" />&nbsp;3 - Not healing<br />
                                    <input name="DischargeFromAgency_M1320MostProblematicPressureUlcerStatus" type="radio"
                                        value="NA" />&nbsp;NA - No observable pressure ulcer
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1322) Current Number of Stage I Pressure Ulcers: Intact skin with non-blanchable
                                        redness of a localized area usually over a bony prominence. The area may be painful,
                                        firm, soft, warmer or cooler as compared to adjacent tissue.
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M1322CurrentNumberStageIUlcer" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M1322CurrentNumberStageIUlcer" type="radio" value="00" />&nbsp;0
                                        <br />
                                        <input name="DischargeFromAgency_M1322CurrentNumberStageIUlcer" type="radio" value="01" />&nbsp;1
                                        <br />
                                        <input name="DischargeFromAgency_M1322CurrentNumberStageIUlcer" type="radio" value="02" />&nbsp;2<br />
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M1322CurrentNumberStageIUlcer" type="radio" value="03" />&nbsp;3<br />
                                        <input name="DischargeFromAgency_M1322CurrentNumberStageIUlcer" type="radio" value="04" />&nbsp;4
                                        or more
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1324) Stage of Most Problematic Unhealed (Observable) Pressure Ulcer:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1324MostProblematicUnhealedStage" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M1324MostProblematicUnhealedStage" type="radio"
                                        value="01" />&nbsp;1 - Stage I<br />
                                    <input name="DischargeFromAgency_M1324MostProblematicUnhealedStage" type="radio"
                                        value="02" />&nbsp;2 - Stage II<br />
                                    <input name="DischargeFromAgency_M1324MostProblematicUnhealedStage" type="radio"
                                        value="03" />&nbsp;3 - Stage III<br />
                                    <input name="DischargeFromAgency_M1324MostProblematicUnhealedStage" type="radio"
                                        value="04" />&nbsp;4 - Stage IV<br />
                                    <input name="DischargeFromAgency_M1324MostProblematicUnhealedStage" type="radio"
                                        value="NA" />&nbsp;NA - No observable pressure ulcer or unhealed pressure ulcer
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1330) Does this patient have a Stasis Ulcer?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1330StasisUlcer" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1330StasisUlcer" type="radio" value="00" />&nbsp;0
                                    - No [ Go to M1340 ]
                                    <br />
                                    <input name="DischargeFromAgency_M1330StasisUlcer" type="radio" value="01" />&nbsp;1
                                    - Yes, patient has BOTH observable and unobservable stasis ulcers
                                    <br />
                                    <input name="DischargeFromAgency_M1330StasisUlcer" type="radio" value="02" />&nbsp;2
                                    - Yes, patient has observable stasis ulcers ONLY<br />
                                    <input name="DischargeFromAgency_M1330StasisUlcer" type="radio" value="03" />&nbsp;3
                                    - Yes, patient has unobservable stasis ulcers ONLY (known but not observable due
                                    to non-removable dressing) [ Go to M1340 ]
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="discharge_M1332AndM1334">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1332) Current Number of (Observable) Stasis Ulcer(s):
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1332CurrentNumberStasisUlcer" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1332CurrentNumberStasisUlcer" type="radio" value="01" />&nbsp;1
                                    - One<br />
                                    <input name="DischargeFromAgency_M1332CurrentNumberStasisUlcer" type="radio" value="02" />&nbsp;2
                                    - Two<br />
                                    <input name="DischargeFromAgency_M1332CurrentNumberStasisUlcer" type="radio" value="03" />&nbsp;3
                                    - Three<br />
                                    <input name="DischargeFromAgency_M1332CurrentNumberStasisUlcer" type="radio" value="04" />&nbsp;4
                                    - Four or more<br />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1334) Status of Most Problematic (Observable) Stasis Ulcer:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1334StasisUlcerStatus" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1334StasisUlcerStatus" type="radio" value="00" />&nbsp;0
                                    - Newly epithelialized
                                    <br />
                                    <input name="DischargeFromAgency_M1334StasisUlcerStatus" type="radio" value="01" />&nbsp;1
                                    - Fully granulating
                                    <br />
                                    <input name="DischargeFromAgency_M1334StasisUlcerStatus" type="radio" value="02" />&nbsp;2
                                    - Early/partial granulation<br />
                                    <input name="DischargeFromAgency_M1334StasisUlcerStatus" type="radio" value="03" />&nbsp;3
                                    - Not healing
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1340) Does this patient have a Surgical Wound?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1340SurgicalWound" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1340SurgicalWound" type="radio" value="00" />&nbsp;0
                                    - No [ Go to M1350 ]<br />
                                    <input name="DischargeFromAgency_M1340SurgicalWound" type="radio" value="01" />&nbsp;1
                                    - Yes, patient has at least one (observable) surgical wound<br />
                                    <input name="DischargeFromAgency_M1340SurgicalWound" type="radio" value="02" />&nbsp;2
                                    - Surgical wound known but not observable due to non-removable dressing [ Go to
                                    M1350 ]
                                </div>
                            </div>
                            <div class="insideCol" id="discharge_M1342">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1342) Status of Most Problematic (Observable) Surgical Wound:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1342SurgicalWoundStatus" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1342SurgicalWoundStatus" type="radio" value="00" />&nbsp;0
                                    - Newly epithelialized
                                    <br />
                                    <input name="DischargeFromAgency_M1342SurgicalWoundStatus" type="radio" value="01" />&nbsp;1
                                    - Fully granulating
                                    <br />
                                    <input name="DischargeFromAgency_M1342SurgicalWoundStatus" type="radio" value="02" />&nbsp;2
                                    - Early/partial granulation<br />
                                    <input name="DischargeFromAgency_M1342SurgicalWoundStatus" type="radio" value="03" />&nbsp;3
                                    - Not healing
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1350) Does this patient have a Skin Lesion or Open Wound, excluding bowel ostomy,
                                        other than those described above that is receiving intervention by the home health
                                        agency?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1350SkinLesionOpenWound" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1350SkinLesionOpenWound" type="radio" value="0" />&nbsp;0
                                    - No &nbsp;&nbsp;
                                    <input name="DischargeFromAgency_M1350SkinLesionOpenWound" type="radio" value="1" />&nbsp;1
                                    - Yes
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" class="SaveContinue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editRespiratorystatus_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyRespiratoryStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1400) When is the patient dyspneic or noticeably Short of Breath?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1400PatientDyspneic" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1400PatientDyspneic" type="radio" value="00" />&nbsp;0
                                    - Patient is not short of breath<br />
                                    <input name="DischargeFromAgency_M1400PatientDyspneic" type="radio" value="01" />&nbsp;1
                                    - When walking more than 20 feet, climbing stairs<br />
                                    <input name="DischargeFromAgency_M1400PatientDyspneic" type="radio" value="02" />&nbsp;2
                                    - With moderate exertion (e.g., while dressing, using commode or bedpan, walking
                                    distances less than 20 feet)<br />
                                    <input name="DischargeFromAgency_M1400PatientDyspneic" type="radio" value="03" />&nbsp;3
                                    - With minimal exertion (e.g., while eating, talking, or performing other ADLs)
                                    or with agitation<br />
                                    <input name="DischargeFromAgency_M1400PatientDyspneic" type="radio" value="04" />&nbsp;4
                                    - At rest (during day or night)<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1410) Respiratory Treatments utilized at home: (Mark all that apply.)
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsOxygen" value="1"
                                        type="checkbox" />&nbsp;1 - Oxygen (intermittent or continuous)<br />
                                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsVentilator" value=" "
                                        type="hidden" />
                                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsVentilator" value="1"
                                        type="checkbox" />&nbsp;2 - Ventilator (continually or at night)<br />
                                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsContinuous" value=" "
                                        type="hidden" />
                                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsContinuous" value="1"
                                        type="checkbox" />&nbsp;3 - Continuous / Bi-level positive airway pressure<br />
                                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsNone" value=" " type="hidden" />
                                    <input name="DischargeFromAgency_M1410HomeRespiratoryTreatmentsNone" value="1" type="checkbox" />&nbsp;4
                                    - None of the above
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editCardiacstatus_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyCardiacStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1500) Symptoms in Heart Failure Patients: If patient has been diagnosed with heart
                                        failure, did the patient exhibit symptoms indicated by clinical heart failure guidelines
                                        (including dyspnea, orthopnea, edema, or weight gain) at any point since the previous
                                        OASIS assessment?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1500HeartFailureSymptons" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1500HeartFailureSymptons" type="radio" value="00" />&nbsp;0
                                    - No [ Go to M2004 at TRN; Go to M1600 at DC ]<br />
                                    <input name="DischargeFromAgency_M1500HeartFailureSymptons" type="radio" value="01" />&nbsp;1
                                    - Yes<br />
                                    <input name="DischargeFromAgency_M1500HeartFailureSymptons" type="radio" value="02" />&nbsp;2
                                    - Not assessed [Go to M2004 at TRN; Go to M1600 at DC ]<br />
                                    <input name="DischargeFromAgency_M1500HeartFailureSymptons" type="radio" value="NA" />&nbsp;NA
                                    - Patient does not have diagnosis of heart failure [Go to M2004 at TRN; Go to M1600
                                    at DC ]<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="discharge_M1510">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1510) Heart Failure Follow-up: If patient has been diagnosed with heart failure
                                        and has exhibited symptoms indicative of heart failure since the previous OASIS
                                        assessment, what action(s) has (have) been taken to respond? (Mark all that apply.)
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupNoAction" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupNoAction" type="checkbox"
                                        value="1" />&nbsp;0 - No action taken<br />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupPhysicianCon" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupPhysicianCon" type="checkbox"
                                        value="1" />&nbsp;1 - Patient’s physician (or other primary care practitioner)
                                    contacted the same day<br />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupAdvisedEmg" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupAdvisedEmg" type="checkbox"
                                        value="1" />&nbsp;2 - Patient advised to get emergency treatment (e.g., call
                                    911 or go to emergency room)<br />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupParameters" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupParameters" type="checkbox"
                                        value="1" />&nbsp;3 - Implemented physician-ordered patient-specific established
                                    parameters for treatment<br />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupInterventions" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupInterventions" type="checkbox"
                                        value="1" />&nbsp;4 - Patient education or other clinical interventions<br />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupChange" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1510HeartFailureFollowupChange" type="checkbox"
                                        value="1" />&nbsp;5 - Obtained change in care plan orders (e.g., increased monitoring
                                    by agency, change in visit frequency, telehealth, etc.)
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editEliminationstatus_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyEliminationStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1600) Has this patient been treated for a Urinary Tract Infection in the past
                                        14 days?
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M1600UrinaryTractInfection" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M1600UrinaryTractInfection" type="radio" value="00" />&nbsp;0
                                        - No<br />
                                        <input name="DischargeFromAgency_M1600UrinaryTractInfection" type="radio" value="01" />&nbsp;1
                                        - Yes<br />
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M1600UrinaryTractInfection" type="radio" value="NA" />&nbsp;NA
                                        - Patient on prophylactic treatment<br />
                                        <input name="DischargeFromAgency_M1600UrinaryTractInfection" type="radio" value="UK" />&nbsp;UK
                                        - Unknown [Omit “UK” option on DC]
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1610) Urinary Incontinence or Urinary Catheter Presence:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1610UrinaryIncontinence" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1610UrinaryIncontinence" type="radio" value="00" />&nbsp;0
                                    - No incontinence or catheter (includes anuria or ostomy for urinary drainage) [
                                    Go to M1620 ]<br />
                                    <input name="DischargeFromAgency_M1610UrinaryIncontinence" type="radio" value="01" />&nbsp;1
                                    - Patient is incontinent<br />
                                    <input name="DischargeFromAgency_M1610UrinaryIncontinence" type="radio" value="02" />&nbsp;2
                                    - Patient requires a urinary catheter (i.e., external, indwelling, intermittent,
                                    suprapubic) [ Go to M1620 ]
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="discharge_M1615">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1615) When does Urinary Incontinence occur?
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M1615UrinaryIncontinenceOccur" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M1615UrinaryIncontinenceOccur" type="radio" value="00" />&nbsp;0
                                        - Timed-voiding defers incontinence<br />
                                        <input name="DischargeFromAgency_M1615UrinaryIncontinenceOccur" type="radio" value="01" />&nbsp;1
                                        - Occasional stress incontinence<br />
                                        <input name="DischargeFromAgency_M1615UrinaryIncontinenceOccur" type="radio" value="02" />&nbsp;2
                                        - During the night only<br />
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M1615UrinaryIncontinenceOccur" type="radio" value="03" />&nbsp;3
                                        - During the day only<br />
                                        <input name="DischargeFromAgency_M1615UrinaryIncontinenceOccur" type="radio" value="04" />&nbsp;4
                                        - During the day and night
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insiderow">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1620) Bowel Incontinence Frequency:
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M1620BowelIncontinenceFrequency" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M1620BowelIncontinenceFrequency" type="radio" value="00" />&nbsp;0
                                        - Very rarely or never has bowel incontinence<br />
                                        <input name="DischargeFromAgency_M1620BowelIncontinenceFrequency" type="radio" value="01" />&nbsp;1
                                        - Less than once weekly<br />
                                        <input name="DischargeFromAgency_M1620BowelIncontinenceFrequency" type="radio" value="02" />&nbsp;2
                                        - One to three times weekly<br />
                                        <input name="DischargeFromAgency_M1620BowelIncontinenceFrequency" type="radio" value="03" />&nbsp;3
                                        - Four to six times weekly<br />
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M1620BowelIncontinenceFrequency" type="radio" value="04" />&nbsp;4
                                        - On a daily basis<br />
                                        <input name="DischargeFromAgency_M1620BowelIncontinenceFrequency" type="radio" value="05" />&nbsp;5
                                        - More often than once daily<br />
                                        <input name="DischargeFromAgency_M1620BowelIncontinenceFrequency" type="radio" value="NA" />&nbsp;NA
                                        - Patient has ostomy for bowel elimination<br />
                                        <input name="DischargeFromAgency_M1620BowelIncontinenceFrequency" type="radio" value="UK" />&nbsp;UK
                                        - Unknown [Omit “UK” option on FU, DC]
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editBehaviourialstatus_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyBehaviourialStatusForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1700) Cognitive Functioning: Patient's current (day of assessment) level of alertness,
                                        orientation, comprehension, concentration, and immediate memory for simple commands.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1700CognitiveFunctioning" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1700CognitiveFunctioning" type="radio" value="00" />&nbsp;0
                                    - Alert/oriented, able to focus and shift attention, comprehends and recalls task
                                    directions independently.<br />
                                    <input name="DischargeFromAgency_M1700CognitiveFunctioning" type="radio" value="01" />&nbsp;1
                                    - Requires prompting (cuing, repetition, reminders) only under stressful or unfamiliar
                                    conditions.<br />
                                    <input name="DischargeFromAgency_M1700CognitiveFunctioning" type="radio" value="02" />&nbsp;2
                                    - Requires assistance and some direction in specific situations (e.g., on all tasks
                                    involving shifting of attention), or consistently requires low stimulus environment
                                    due to distractibility.<br />
                                    <input name="DischargeFromAgency_M1700CognitiveFunctioning" type="radio" value="03" />&nbsp;3
                                    - Requires considerable assistance in routine situations. Is not alert and oriented
                                    or is unable to shift attention and recall directions more than half the time.<br />
                                    <input name="DischargeFromAgency_M1700CognitiveFunctioning" type="radio" value="04" />&nbsp;4
                                    - Totally dependent due to disturbances such as constant disorientation, coma, persistent
                                    vegetative state, or delirium.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1710) When Confused (Reported or Observed Within the Last 14 Days):
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1710WhenConfused" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1710WhenConfused" type="radio" value="0" />&nbsp;0
                                    - Never<br />
                                    <input name="DischargeFromAgency_M1710WhenConfused" type="radio" value="1" />&nbsp;1
                                    - In new or complex situations only<br />
                                    <input name="DischargeFromAgency_M1710WhenConfused" type="radio" value="2" />&nbsp;2
                                    - On awakening or at night only<br />
                                    <input name="DischargeFromAgency_M1710WhenConfused" type="radio" value="3" />&nbsp;3
                                    - During the day and evening, but not constantly<br />
                                    <input name="DischargeFromAgency_M1710WhenConfused" type="radio" value="4" />&nbsp;4
                                    - Constantly<br />
                                    <input name="DischargeFromAgency_M1710WhenConfused" type="radio" value="NA" />&nbsp;NA
                                    - Patient nonresponsive
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1720) When Anxious (Reported or Observed Within the Last 14 Days):
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1720WhenAnxious" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1720WhenAnxious" type="radio" value="0" />&nbsp;0
                                    - None of the time<br />
                                    <input name="DischargeFromAgency_M1720WhenAnxious" type="radio" value="1" />&nbsp;1
                                    - Less often than daily<br />
                                    <input name="DischargeFromAgency_M1720WhenAnxious" type="radio" value="2" />&nbsp;2
                                    - Daily, but not constantly<br />
                                    <input name="DischargeFromAgency_M1720WhenAnxious" type="radio" value="3" />&nbsp;3
                                    - All of the time<br />
                                    <input name="DischargeFromAgency_M1720WhenAnxious" type="radio" value="NA" />&nbsp;NA
                                    - Patient nonresponsive
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1740) Cognitive, behavioral, and psychiatric symptoms that are demonstrated at
                                        least once a week (Reported or Observed): (Mark all that apply.)
                                    </div>
                                </div>
                                <div class="padding">
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"
                                        value="" type="hidden" />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsMemDeficit"
                                        value="1" type="checkbox" />&nbsp;1 - Memory deficit: failure to recognize familiar
                                    persons/places, inability to recall events of past 24 hours, significant memory
                                    loss so that supervision is required<br />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsImpDes"
                                        value="" type="hidden" />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsImpDes"
                                        value="1" type="checkbox" />&nbsp;2 - Impaired decision-making: failure to perform
                                    usual ADLs or IADLs, inability to appropriately stop activities, jeopardizes safety
                                    through actions<br />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsVerbal"
                                        value="" type="hidden" />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsVerbal"
                                        value="1" type="checkbox" />&nbsp;3 - Verbal disruption: yelling, threatening,
                                    excessive profanity, sexual references, etc.<br />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsPhysical"
                                        value="" type="hidden" />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsPhysical"
                                        value="1" type="checkbox" />&nbsp;4 - Physical aggression: aggressive or combative
                                    to self and others (e.g., hits self, throws objects, punches, dangerous maneuvers
                                    with wheelchair or other objects)<br />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsSIB"
                                        value="" type="hidden" />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsSIB"
                                        value="1" type="checkbox" />&nbsp;5 - Disruptive, infantile, or socially inappropriate
                                    behavior (excludes verbal actions)<br />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsDelusional"
                                        value="" type="hidden" />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsDelusional"
                                        value="1" type="checkbox" />&nbsp;6 - Delusional, hallucinatory, or paranoid
                                    behavior<br />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsNone"
                                        value="" type="hidden" />
                                    <input name="DischargeFromAgency_M1740CognitiveBehavioralPsychiatricSymptomsNone"
                                        value="1" type="checkbox" />&nbsp;7 - None of the above behaviors demonstrated
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1745) Frequency of Disruptive Behavior Symptoms (Reported or Observed) Any physical,
                                        verbal, or other disruptive/dangerous symptoms that are injurious to self or others
                                        or jeopardize personal safety.
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency" type="radio"
                                            value="00" />&nbsp;0 - Never<br />
                                        <input name="DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency" type="radio"
                                            value="01" />&nbsp;1 - Less than once a month<br />
                                        <input name="DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency" type="radio"
                                            value="02" />&nbsp;2 - Once a month<br />
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency" type="radio"
                                            value="03" />&nbsp;3 - Several times each month<br />
                                        <input name="DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency" type="radio"
                                            value="04" />&nbsp;4 - Several times a week<br />
                                        <input name="DischargeFromAgency_M1745DisruptiveBehaviorSymptomsFrequency" type="radio"
                                            value="05" />&nbsp;5 - At least daily
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editAdl_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyADLForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1800) Grooming: Current ability to tend safely to personal hygiene needs (i.e.,
                                        washing face and hands, hair care, shaving or make up, teeth or denture care, fingernail
                                        care).
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1800Grooming" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1800Grooming" type="radio" value="00" />&nbsp;0
                                    - Able to groom self unaided, with or without the use of assistive devices or adapted
                                    methods.<br />
                                    <input name="DischargeFromAgency_M1800Grooming" type="radio" value="01" />&nbsp;1
                                    - Grooming utensils must be placed within reach before able to complete grooming
                                    activities.<br />
                                    <input name="DischargeFromAgency_M1800Grooming" type="radio" value="02" />&nbsp;2
                                    - Someone must assist the patient to groom self.<br />
                                    <input name="DischargeFromAgency_M1800Grooming" type="radio" value="03" />&nbsp;3
                                    - Patient depends entirely upon someone else for grooming needs.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1810) Current Ability to Dress Upper Body safely (with or without dressing aids)
                                        including undergarments, pullovers, front-opening shirts and blouses, managing zippers,
                                        buttons, and snaps:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1810CurrentAbilityToDressUpper" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1810CurrentAbilityToDressUpper" type="radio" value="00" />&nbsp;0
                                    - Able to get clothes out of closets and drawers, put them on and remove them from
                                    the upper body without assistance.<br />
                                    <input name="DischargeFromAgency_M1810CurrentAbilityToDressUpper" type="radio" value="01" />&nbsp;1
                                    - Able to dress upper body without assistance if clothing is laid out or handed
                                    to the patient.<br />
                                    <input name="DischargeFromAgency_M1810CurrentAbilityToDressUpper" type="radio" value="02" />&nbsp;2
                                    - Someone must help the patient put on upper body clothing.<br />
                                    <input name="DischargeFromAgency_M1810CurrentAbilityToDressUpper" type="radio" value="03" />&nbsp;3
                                    - Patient depends entirely upon another person to dress the upper body.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1820) Current Ability to Dress Lower Body safely (with or without dressing aids)
                                        including undergarments, slacks, socks or nylons, shoes:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1820CurrentAbilityToDressLower" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1820CurrentAbilityToDressLower" type="radio" value="00" />&nbsp;0
                                    - Able to obtain, put on, and remove clothing and shoes without assistance.<br />
                                    <input name="DischargeFromAgency_M1820CurrentAbilityToDressLower" type="radio" value="01" />&nbsp;1
                                    - Able to dress lower body without assistance if clothing and shoes are laid out
                                    or handed to the patient.<br />
                                    <input name="DischargeFromAgency_M1820CurrentAbilityToDressLower" type="radio" value="02" />&nbsp;2
                                    - Someone must help the patient put on undergarments, slacks, socks or nylons, and
                                    shoes.<br />
                                    <input name="DischargeFromAgency_M1820CurrentAbilityToDressLower" type="radio" value="03" />&nbsp;3
                                    - Patient depends entirely upon another person to dress lower body.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing
                                        face, washing hands, and shampooing hair).
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="00" />&nbsp;0 - Able to bathe self in shower or tub independently, including
                                    getting in and out of tub/shower.<br />
                                    <input name="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="01" />&nbsp;1 - With the use of devices, is able to bathe self in shower
                                    or tub independently, including getting in and out of the tub/shower.<br />
                                    <input name="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="02" />&nbsp;2 - Able to bathe in shower or tub with the intermittent
                                    assistance of another person:<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) for intermittent supervision or encouragement
                                    or reminders, OR
                                    <br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) to get in and out of the shower or
                                    tub, OR<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c) for washing difficult to reach areas.<br />
                                    <input name="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="03" />&nbsp;3 - Able to participate in bathing self in shower or tub,
                                    but requires presence of another person throughout the bath for assistance or supervision.<br />
                                    <input name="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="04" />&nbsp;4 - Unable to use the shower or tub, but able to bathe self
                                    independently with or without the use of devices at the sink, in chair, or on commode.<br />
                                    <input name="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="05" />&nbsp;5 - Unable to use the shower or tub, but able to participate
                                    in bathing self in bed, at the sink, in bedside chair, or on commode, with the assistance
                                    or supervision of another person throughout the bath.<br />
                                    <input name="DischargeFromAgency_M1830CurrentAbilityToBatheEntireBody" type="radio"
                                        value="06" />&nbsp;6 - Unable to participate effectively in bathing and is bathed
                                    totally by another person.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside
                                        commode safely and transfer on and off toilet/commode.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1840ToiletTransferring" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1840ToiletTransferring" type="radio" value="00" />&nbsp;0
                                    - Able to get to and from the toilet and transfer independently with or without
                                    a device.<br />
                                    <input name="DischargeFromAgency_M1840ToiletTransferring" type="radio" value="01" />&nbsp;1
                                    - When reminded, assisted, or supervised by another person, able to get to and from
                                    the toilet and transfer.<br />
                                    <input name="DischargeFromAgency_M1840ToiletTransferring" type="radio" value="02" />&nbsp;2
                                    - Unable to get to and from the toilet but is able to use a bedside commode (with
                                    or without assistance).<br />
                                    <input name="DischargeFromAgency_M1840ToiletTransferring" type="radio" value="03" />&nbsp;3
                                    - Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal
                                    independently.<br />
                                    <input name="DischargeFromAgency_M1840ToiletTransferring" type="radio" value="04" />&nbsp;4
                                    - Is totally dependent in toileting.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1845) Toileting Hygiene: Current ability to maintain perineal hygiene safely,
                                        adjust clothes and/or incontinence pads before and after using toilet, commode,
                                        bedpan, urinal. If managing ostomy, includes cleaning area around stoma, but not
                                        managing equipment.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1845ToiletingHygiene" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1845ToiletingHygiene" type="radio" value="00" />&nbsp;0
                                    - Able to manage toileting hygiene and clothing management without assistance.<br />
                                    <input name="DischargeFromAgency_M1845ToiletingHygiene" type="radio" value="01" />&nbsp;1
                                    - Able to manage toileting hygiene and clothing management without assistance if
                                    supplies/implements are laid out for the patient.<br />
                                    <input name="DischargeFromAgency_M1845ToiletingHygiene" type="radio" value="02" />&nbsp;2
                                    - Someone must help the patient to maintain toileting hygiene and/or adjust clothing.<br />
                                    <input name="DischargeFromAgency_M1845ToiletingHygiene" type="radio" value="03" />&nbsp;3
                                    - Patient depends entirely upon another person to maintain toileting hygiene.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1850) Transferring: Current ability to move safely from bed to chair, or ability
                                        to turn and position self in bed if patient is bedfast.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1850Transferring" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1850Transferring" type="radio" value="00" />&nbsp;0
                                    - Able to independently transfer.<br />
                                    <input name="DischargeFromAgency_M1850Transferring" type="radio" value="01" />&nbsp;1
                                    - Able to transfer with minimal human assistance or with use of an assistive device.<br />
                                    <input name="DischargeFromAgency_M1850Transferring" type="radio" value="03" />&nbsp;3
                                    - Unable to transfer self and is unable to bear weight or pivot when transferred
                                    by another person.<br />
                                    <input name="DischargeFromAgency_M1850Transferring" type="radio" value="04" />&nbsp;4
                                    - Bedfast, unable to transfer but is able to turn and position self in bed.<br />
                                    <input name="DischargeFromAgency_M1850Transferring" type="radio" value="05" />&nbsp;5
                                    - Bedfast, unable to transfer and is unable to turn and position self.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing
                                        position, or use a wheelchair, once in a seated position, on a variety of surfaces.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1860AmbulationLocomotion" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1860AmbulationLocomotion" type="radio" value="00" />&nbsp;0
                                    - Able to independently walk on even and uneven surfaces and negotiate stairs with
                                    or without railings (i.e., needs no human assistance or assistive device).<br />
                                    <input name="DischargeFromAgency_M1860AmbulationLocomotion" type="radio" value="01" />&nbsp;1
                                    - With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able
                                    to independently walk on even and uneven surfaces and negotiate stairs with or without
                                    railings.<br />
                                    <input name="DischargeFromAgency_M1860AmbulationLocomotion" type="radio" value="02" />&nbsp;2
                                    - Requires use of a two-handed device (e.g., walker or crutches) to walk alone on
                                    a level surface and/or requires human supervision or assistance to negotiate stairs
                                    or steps or uneven surfaces.<br />
                                    <input name="DischargeFromAgency_M1860AmbulationLocomotion" type="radio" value="03" />&nbsp;3
                                    - Able to walk only with the supervision or assistance of another person at all
                                    times.<br />
                                    <input name="DischargeFromAgency_M1860AmbulationLocomotion" type="radio" value="04" />&nbsp;4
                                    - Chairfast, unable to ambulate but is able to wheel self independently.<br />
                                    <input name="DischargeFromAgency_M1860AmbulationLocomotion" type="radio" value="05" />&nbsp;5
                                    - Chairfast, unable to ambulate and is unable to wheel self.<br />
                                    <input name="DischargeFromAgency_M1860AmbulationLocomotion" type="radio" value="06" />&nbsp;6
                                    - Bedfast, unable to ambulate or be up in a chair.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1870) Feeding or Eating: Current ability to feed self meals and snacks safely.
                                        Note: This refers only to the process of eating, chewing, and swallowing, not preparing
                                        the food to be eaten.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1870FeedingOrEating" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1870FeedingOrEating" type="radio" value="00" />&nbsp;0
                                    - Able to independently feed self.<br />
                                    <input name="DischargeFromAgency_M1870FeedingOrEating" type="radio" value="01" />&nbsp;1
                                    - Able to feed self independently but requires:<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) meal set-up; OR
                                    <br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) intermittent assistance or supervision
                                    from another person; OR<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c) a liquid, pureed or ground meat diet.<br />
                                    <input name="DischargeFromAgency_M1870FeedingOrEating" type="radio" value="02" />&nbsp;2
                                    - Unable to feed self and must be assisted or supervised throughout the meal/snack.<br />
                                    <input name="DischargeFromAgency_M1870FeedingOrEating" type="radio" value="03" />&nbsp;3
                                    - Able to take in nutrients orally and receives supplemental nutrients through a
                                    nasogastric tube or gastrostomy.<br />
                                    <input name="DischargeFromAgency_M1870FeedingOrEating" type="radio" value="04" />&nbsp;4
                                    - Unable to take in nutrients orally and is fed nutrients through a nasogastric
                                    tube or gastrostomy.<br />
                                    <input name="DischargeFromAgency_M1870FeedingOrEating" type="radio" value="05" />&nbsp;5
                                    - Unable to take in nutrients orally or by tube feeding.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1880) Current Ability to Plan and Prepare Light Meals (e.g., cereal, sandwich)
                                        or reheat delivered meals safely:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1880AbilityToPrepareLightMeal" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1880AbilityToPrepareLightMeal" type="radio" value="00" />&nbsp;0
                                    - (a) Able to independently plan and prepare all light meals for self or reheat
                                    delivered meals; OR<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) Is physically,
                                    cognitively, and mentally able to prepare light meals on a regular basis but has
                                    not routinely performed light meal preparation in the past (i.e., prior to this
                                    home care admission).<br />
                                    <input name="DischargeFromAgency_M1880AbilityToPrepareLightMeal" type="radio" value="01" />&nbsp;1
                                    - Unable to prepare light meals on a regular basis due to physical, cognitive, or
                                    mental limitations.<br />
                                    <input name="DischargeFromAgency_M1880AbilityToPrepareLightMeal" type="radio" value="02" />&nbsp;2
                                    - Unable to prepare any light meals or reheat any delivered meals.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M1890) Ability to Use Telephone: Current ability to answer the phone safely, including
                                        dialing numbers, and effectively using the telephone to communicate.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M1890AbilityToUseTelephone" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M1890AbilityToUseTelephone" type="radio" value="00" />&nbsp;0
                                    - Able to dial numbers and answer calls appropriately and as desired.<br />
                                    <input name="DischargeFromAgency_M1890AbilityToUseTelephone" type="radio" value="01" />&nbsp;1
                                    - Able to use a specially adapted telephone (i.e., large numbers on the dial, teletype
                                    phone for the deaf) and call essential numbers.<br />
                                    <input name="DischargeFromAgency_M1890AbilityToUseTelephone" type="radio" value="02" />&nbsp;2
                                    - Able to answer the telephone and carry on a normal conversation but has difficulty
                                    with placing calls.<br />
                                    <input name="DischargeFromAgency_M1890AbilityToUseTelephone" type="radio" value="03" />&nbsp;3
                                    - Able to answer the telephone only some of the time or is able to carry on only
                                    a limited conversation.<br />
                                    <input name="DischargeFromAgency_M1890AbilityToUseTelephone" type="radio" value="04" />&nbsp;4
                                    - Unable to answer the telephone at all but can listen if assisted with equipment.<br />
                                    <input name="DischargeFromAgency_M1890AbilityToUseTelephone" type="radio" value="05" />&nbsp;5
                                    - Totally unable to use the telephone.<br />
                                    <input name="DischargeFromAgency_M1890AbilityToUseTelephone" type="radio" value="NA" />&nbsp;NA
                                    - Patient does not have a telephone.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editMedications_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyMedicationsForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2004) Medication Intervention: If there were any clinically significant medication
                                        issues since the previous OASIS assessment, was a physician or the physician-designee
                                        contacted within one calendar day of the assessment to resolve clinically significant
                                        medication issues, including reconciliation?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M2004MedicationIntervention" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M2004MedicationIntervention" type="radio" value="00" />&nbsp;0
                                    - No<br />
                                    <input name="DischargeFromAgency_M2004MedicationIntervention" type="radio" value="01" />&nbsp;1
                                    - Yes<br />
                                    <input name="DischargeFromAgency_M2004MedicationIntervention" type="radio" value="NA" />&nbsp;NA
                                    - No clinically significant medication issues identified since the previous OASIS
                                    assessment
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2015) Patient/Caregiver Drug Education Intervention: Since the previous OASIS
                                        assessment, was the patient/caregiver instructed by agency staff or other health
                                        care provider to monitor the effectiveness of drug therapy, drug reactions, and
                                        side effects, and how and when to report problems that may occur?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention"
                                        type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention"
                                        type="radio" value="00" />&nbsp;0 - No<br />
                                    <input name="DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention"
                                        type="radio" value="01" />&nbsp;1 - Yes<br />
                                    <input name="DischargeFromAgency_M2015PatientOrCaregiverDrugEducationIntervention"
                                        type="radio" value="NA" />&nbsp;NA - Patient not taking any drugs
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2020) Management of Oral Medications: Patient's current ability to prepare and
                                        take all oral medications reliably and safely, including administration of the correct
                                        dosage at the appropriate times/intervals. Excludes injectable and IV medications.
                                        (NOTE: This refers to ability, not compliance or willingness.)
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M2020ManagementOfOralMedications" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M2020ManagementOfOralMedications" type="radio" value="00" />&nbsp;0
                                    - Able to independently take the correct oral medication(s) and proper dosage(s)
                                    at the correct times.<br />
                                    <input name="DischargeFromAgency_M2020ManagementOfOralMedications" type="radio" value="01" />&nbsp;1
                                    - Able to take medication(s) at the correct times if:<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) individual dosages are prepared in
                                    advance by another person; OR<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) another person develops a drug diary
                                    or chart.<br />
                                    <input name="DischargeFromAgency_M2020ManagementOfOralMedications" type="radio" value="02" />&nbsp;2
                                    - Able to take medication(s) at the correct times if given reminders by another
                                    person at the appropriate times<br />
                                    <input name="DischargeFromAgency_M2020ManagementOfOralMedications" type="radio" value="03" />&nbsp;3
                                    - Unable to take medication unless administered by another person.<br />
                                    <input name="DischargeFromAgency_M2020ManagementOfOralMedications" type="radio" value="NA" />&nbsp;NA
                                    - No oral medications prescribed.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2030) Management of Injectable Medications: Patient's current ability to prepare
                                        and take all prescribed injectable medications reliably and safely, including administration
                                        of correct dosage at the appropriate times/intervals. Excludes IV medications.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M2030ManagementOfInjectableMedications" type="hidden"
                                        value=" " />
                                    <input name="DischargeFromAgency_M2030ManagementOfInjectableMedications" type="radio"
                                        value="00" />&nbsp;0 - Able to independently take the correct medication(s)
                                    and proper dosage(s) at the correct times.<br />
                                    <input name="DischargeFromAgency_M2030ManagementOfInjectableMedications" type="radio"
                                        value="01" />&nbsp;1 - Able to take injectable medication(s) at the correct
                                    times if:<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) individual syringes are prepared in
                                    advance by another person; OR<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) another person develops a drug diary
                                    or chart.<br />
                                    <input name="DischargeFromAgency_M2030ManagementOfInjectableMedications" type="radio"
                                        value="02" />&nbsp;2 - Able to take medication(s) at the correct times if given
                                    reminders by another person based on the frequency of the injection<br />
                                    <input name="DischargeFromAgency_M2030ManagementOfInjectableMedications" type="radio"
                                        value="03" />&nbsp;3 - Unable to take injectable medication unless administered
                                    by another person.<br />
                                    <input name="DischargeFromAgency_M2030ManagementOfInjectableMedications" type="radio"
                                        value="NA" />&nbsp;NA - No injectable medications prescribed.
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editCaremanagement_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyCareManagementForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2100) Types and Sources of Assistance: Determine the level of caregiver ability
                                        and willingness to provide assistance for the following activities, if assistance
                                        is needed. (Check only one box in each row.)
                                    </div>
                                </div>
                                <div class="margin">
                                    <table class="agency-data-table" id="Table6">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Type of Assistance
                                                </th>
                                                <th>
                                                    No assistance needed in this area
                                                </th>
                                                <th>
                                                    Caregiver(s) currently provide assistance
                                                </th>
                                                <th>
                                                    Caregiver(s) need training/ supportive services to provide assistance
                                                </th>
                                                <th>
                                                    Caregiver(s) not likely to provide assistance
                                                </th>
                                                <th>
                                                    Unclear if Caregiver(s) will provide assistance
                                                </th>
                                                <th>
                                                    Assistance needed, but no Caregiver(s) available
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    a. ADL assistance (e.g., transfer/ ambulation, bathing, dressing, toileting, eating/feeding)
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ADLAssistance" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2100ADLAssistance" type="radio" value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ADLAssistance" type="radio" value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ADLAssistance" type="radio" value="02" />&nbsp;2
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ADLAssistance" type="radio" value="03" />&nbsp;3
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ADLAssistance" type="radio" value="04" />&nbsp;4
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ADLAssistance" type="radio" value="05" />&nbsp;5
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    b. IADL assistance (e.g., meals, housekeeping, laundry, telephone, shopping, finances)
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100IADLAssistance" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2100IADLAssistance" value="00" type="radio" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100IADLAssistance" value="01" type="radio" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100IADLAssistance" value="02" type="radio" />&nbsp;2
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100IADLAssistance" value="03" type="radio" />&nbsp;3
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100IADLAssistance" value="04" type="radio" />&nbsp;4
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100IADLAssistance" value="05" type="radio" />&nbsp;5
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    c. Medication administration (e.g., oral, inhaled or injectable)
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicationAdministration" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2100MedicationAdministration" value="00" type="radio" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicationAdministration" value="01" type="radio" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicationAdministration" value="02" type="radio" />&nbsp;2
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicationAdministration" value="03" type="radio" />&nbsp;3
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicationAdministration" value="04" type="radio" />&nbsp;4
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicationAdministration" value="05" type="radio" />&nbsp;5
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    d. Medical procedures/ treatments (e.g., changing wound dressing)
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicalProcedures" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2100MedicalProcedures" value="00" type="radio" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicalProcedures" value="01" type="radio" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicalProcedures" value="02" type="radio" />&nbsp;2
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicalProcedures" value="03" type="radio" />&nbsp;3
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicalProcedures" value="04" type="radio" />&nbsp;4
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100MedicalProcedures" value="05" type="radio" />&nbsp;5
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    e. Management of Equipment (includes oxygen, IV/infusion equipment, enteral/ parenteral
                                                    nutrition, ventilator therapy equipment or supplies)
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ManagementOfEquipment" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2100ManagementOfEquipment" value="00" type="radio" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ManagementOfEquipment" value="01" type="radio" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ManagementOfEquipment" value="02" type="radio" />&nbsp;2
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ManagementOfEquipment" value="03" type="radio" />&nbsp;3
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ManagementOfEquipment" value="04" type="radio" />&nbsp;4
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100ManagementOfEquipment" value="05" type="radio" />&nbsp;5
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    f. Supervision and safety (e.g., due to cognitive impairment
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100SupervisionAndSafety" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2100SupervisionAndSafety" value="00" type="radio" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100SupervisionAndSafety" value="01" type="radio" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100SupervisionAndSafety" value="02" type="radio" />&nbsp;2
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100SupervisionAndSafety" value="03" type="radio" />&nbsp;3
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100SupervisionAndSafety" value="04" type="radio" />&nbsp;4
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100SupervisionAndSafety" value="05" type="radio" />&nbsp;5
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    g. Advocacy or facilitation of patient's participation in appropriate medical care
                                                    (includes transporta-tion to or from appointments)
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100FacilitationPatientParticipation" type="hidden"
                                                        value=" " />
                                                    <input name="DischargeFromAgency_M2100FacilitationPatientParticipation" value="00"
                                                        type="radio" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100FacilitationPatientParticipation" value="01"
                                                        type="radio" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100FacilitationPatientParticipation" value="02"
                                                        type="radio" />&nbsp;2
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100FacilitationPatientParticipation" value="03"
                                                        type="radio" />&nbsp;3
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100FacilitationPatientParticipation" value="04"
                                                        type="radio" />&nbsp;4
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2100FacilitationPatientParticipation" value="05"
                                                        type="radio" />&nbsp;5
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2110) How Often does the patient receive ADL or IADL assistance from any caregiver(s)
                                        (other than home health agency staff)?
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance" type="radio"
                                            value="01" />&nbsp;1 - At least daily<br />
                                        <input name="DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance" type="radio"
                                            value="02" />&nbsp;2 - Three or more times per week<br />
                                        <input name="DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance" type="radio"
                                            value="03" />&nbsp;3 - One to two times per week<br />
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance" type="radio"
                                            value="04" />&nbsp;4 - Received, but less often than weekly<br />
                                        <input name="DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance" type="radio"
                                            value="05" />&nbsp;5 - No assistance received<br />
                                        <input name="DischargeFromAgency_M2110FrequencyOfADLOrIADLAssistance" type="radio"
                                            value="UK" />&nbsp;UK - Unknown [Omit “UK” option on DC]<br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editEmergentcare_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyTherapyNeedPlanOfCareForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2300) Emergent Care: Since the last time OASIS data were collected, has the patient
                                        utilized a hospital emergency department (includes holding/observation)?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M2300EmergentCare" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M2300EmergentCare" type="radio" value="00" />&nbsp;0
                                    - No [ Go to M2400 ]<br />
                                    <input name="DischargeFromAgency_M2300EmergentCare" type="radio" value="01" />&nbsp;1
                                    - Yes, used hospital emergency department WITHOUT hospital admission<br />
                                    <input name="DischargeFromAgency_M2300EmergentCare" type="radio" value="02" />&nbsp;2
                                    - Yes, used hospital emergency department WITH hospital admission<br />
                                    <input name="DischargeFromAgency_M2300EmergentCare" type="radio" value="UK" />&nbsp;UK
                                    - Unknown [ Go to M2400 ]
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis" id="discharge_M2310">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2310) Reason for Emergent Care: For what reason(s) did the patient receive emergent
                                        care (with or without hospitalization)? (Mark all that apply.)
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareMed" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareMed" type="checkbox" value="1" />&nbsp;1
                                        - Improper medication administration, medication side effects, toxicity, anaphylaxis<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareFall" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareFall" type="checkbox"
                                            value="1" />&nbsp;2 - Injury caused by fall<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareResInf" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareResInf" type="checkbox"
                                            value="1" />&nbsp;3 - Respiratory infection (e.g., pneumonia, bronchitis)<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareOtherResInf" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareOtherResInf" type="checkbox"
                                            value="1" />&nbsp;4 - Other respiratory problem<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareHeartFail" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareHeartFail" type="checkbox"
                                            value="1" />&nbsp;5 - Heart failure (e.g., fluid overload)<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareCardiac" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareCardiac" type="checkbox"
                                            value="1" />&nbsp;6 - Cardiac dysrhythmia (irregular heartbeat)<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareMyocardial" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareMyocardial" type="checkbox"
                                            value="1" />&nbsp;7 - Myocardial infarction or chest pain<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareHeartDisease" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareHeartDisease" type="checkbox"
                                            value="1" />&nbsp;8 - Other heart disease<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareStroke" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareStroke" type="checkbox"
                                            value="1" />&nbsp;9 - Stroke (CVA) or TIA<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareHypo" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareHypo" type="checkbox"
                                            value="1" />&nbsp;10 - Hypo/Hyperglycemia, diabetes out of control
                                    </div>
                                </div>
                                <div class="insideCol">
                                    <div class="margin">
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareGI" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareGI" type="checkbox" value="1" />&nbsp;11
                                        - GI bleeding, obstruction, constipation, impaction<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareDehMal" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareDehMal" type="checkbox"
                                            value="1" />&nbsp;12 - Dehydration, malnutrition<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareUrinaryInf" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareUrinaryInf" type="checkbox"
                                            value="1" />&nbsp;13 - Urinary tract infection<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareIV" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareIV" type="checkbox" value="1" />&nbsp;14
                                        - IV catheter-related infection or complication<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareWoundInf" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareWoundInf" type="checkbox"
                                            value="1" />&nbsp;15 - Wound infection or deterioration<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareUncontrolledPain" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareUncontrolledPain" type="checkbox"
                                            value="1" />&nbsp;16 - Uncontrolled pain<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareMental" type="hidden"
                                            value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareMental" type="checkbox"
                                            value="1" />&nbsp;17 - Acute mental/behavioral health problem<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareDVT" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareDVT" type="checkbox" value="1" />&nbsp;18
                                        - Deep vein thrombosis, pulmonary embolus<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareOther" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareOther" type="checkbox"
                                            value="1" />&nbsp;19 - Other than above reasons<br />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareUK" type="hidden" value=" " />
                                        <input name="DischargeFromAgency_M2310ReasonForEmergentCareUK" type="checkbox" value="1" />&nbsp;UK
                                        - Reason unknown<br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save/Continue" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                    <div id="editDischargeOnlyAdd_discharge" class="general abs">
                        <% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisDischargeFromAgencyOrdersForm" }))%>
                        <%  { %>
                        <%= Html.Hidden("DischargeFromAgency_Id", "")%>
                        <%= Html.Hidden("DischargeFromAgency_Action", "Edit")%>
                        <%= Html.Hidden("DischargeFromAgency_PatientGuid", "")%>
                        <%= Html.Hidden("assessment", "DischargeFromAgency")%>
                        <div class="rowOasis">
                            <div class="insideColFull">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2400) Intervention Synopsis: (Check only one box in each row.) Since the previous
                                        OASIS assessment, were the following interventions BOTH included in the physician-ordered
                                        plan of care AND implemented?
                                    </div>
                                </div>
                                <div class="margin">
                                    <table class="agency-data-table" id="Table7">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Plan / Intervention
                                                </th>
                                                <th>
                                                    No
                                                </th>
                                                <th>
                                                    Yes
                                                </th>
                                                <th colspan="2">
                                                    Not Applicable
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    a. Diabetic foot care including monitoring for the presence of skin lesions on the
                                                    lower extremities and patient/caregiver education on proper foot care
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400DiabeticFootCare" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2400DiabeticFootCare" type="radio" value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400DiabeticFootCare" type="radio" value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400DiabeticFootCare" type="radio" value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Patient is not diabetic or is bilateral amputee
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    b. Falls prevention interventions
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400FallsPreventionInterventions" type="hidden"
                                                        value=" " />
                                                    <input name="DischargeFromAgency_M2400FallsPreventionInterventions" type="radio"
                                                        value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400FallsPreventionInterventions" type="radio"
                                                        value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400FallsPreventionInterventions" type="radio"
                                                        value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for
                                                    falls since the last OASIS assessment
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    c. Depression intervention(s) such as medication, referral for other treatment,
                                                    or a monitoring plan for current treatment
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400DepressionIntervention" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2400DepressionIntervention" type="radio" value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400DepressionIntervention" type="radio" value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400DepressionIntervention" type="radio" value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Formal assessment indicates patient did not meet criteria for depression AND patient
                                                    did not have diagnosis of depression since the last OASIS assessment
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    d. Intervention(s) to monitor and mitigate pain
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400PainIntervention" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2400PainIntervention" type="radio" value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400PainIntervention" type="radio" value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400PainIntervention" type="radio" value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Formal assessment did not indicate pain since the last OASIS assessment
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    e. Intervention(s) to prevent pressure ulcers
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400PressureUlcerIntervention" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2400PressureUlcerIntervention" type="radio" value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400PressureUlcerIntervention" type="radio" value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400PressureUlcerIntervention" type="radio" value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Formal assessment indicates the patient was not at risk of pressure ulcers since
                                                    the last OASIS assessment
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    f. Pressure ulcer treatment based on principles of moist wound healing
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400PressureUlcerTreatment" type="hidden" value=" " />
                                                    <input name="DischargeFromAgency_M2400PressureUlcerTreatment" type="radio" value="00" />&nbsp;0
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400PressureUlcerTreatment" type="radio" value="01" />&nbsp;1
                                                </td>
                                                <td>
                                                    <input name="DischargeFromAgency_M2400PressureUlcerTreatment" type="radio" value="NA" />&nbsp;na
                                                </td>
                                                <td>
                                                    Dressings that support the principles of moist wound healing not indicated for this
                                                    patient’s pressure ulcers OR patient has no pressure ulcers with need for moist
                                                    wound healing
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2410) To which Inpatient Facility has the patient been admitted?
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M2410TypeOfInpatientFacility" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M2410TypeOfInpatientFacility" type="radio" value="01" />&nbsp;1
                                    - Hospital [ Go to M2430 ]<br />
                                    <input name="DischargeFromAgency_M2410TypeOfInpatientFacility" type="radio" value="02" />&nbsp;2
                                    - Rehabilitation facility [ Go to M0903 ]<br />
                                    <input name="DischargeFromAgency_M2410TypeOfInpatientFacility" type="radio" value="03" />&nbsp;3
                                    - Nursing home [ Go to M2440 ]<br />
                                    <input name="DischargeFromAgency_M2410TypeOfInpatientFacility" type="radio" value="04" />&nbsp;4
                                    - Hospice [ Go to M0903 ]<br />
                                    <input name="DischargeFromAgency_M2410TypeOfInpatientFacility" type="radio" value="NA" />&nbsp;NA
                                    - No inpatient facility admission [Omit "NA" option on TRN]<br />
                                </div>
                            </div>
                            <div class="insideCol" id="discharge_M2420">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M2420) Discharge Disposition: Where is the patient after discharge from your agency?
                                        (Choose only one answer.)
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M2420DischargeDisposition" type="hidden" value=" " />
                                    <input name="DischargeFromAgency_M2420DischargeDisposition" type="radio" value="01" />&nbsp;1
                                    - Patient remained in the community (without formal assistive services)<br />
                                    <input name="DischargeFromAgency_M2420DischargeDisposition" type="radio" value="02" />&nbsp;2
                                    - Patient remained in the community (with formal assistive services)<br />
                                    <input name="DischargeFromAgency_M2420DischargeDisposition" type="radio" value="03" />&nbsp;3
                                    - Patient transferred to a non-institutional hospice<br />
                                    <input name="DischargeFromAgency_M2420DischargeDisposition" type="radio" value="04" />&nbsp;4
                                    - Unknown because patient moved to a geographic location not served by this agency<br />
                                    <input name="DischargeFromAgency_M2420DischargeDisposition" type="radio" value="UK" />&nbsp;UK
                                    - Other unknown<br />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasis">
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M0903) Date of Last (Most Recent) Home Visit:
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M0903LastHomeVisitDate" id="DischargeFromAgency_M0903LastHomeVisitDate"
                                        type="text" />
                                </div>
                            </div>
                            <div class="insideCol">
                                <div class="insiderow title">
                                    <div class="margin">
                                        (M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer,
                                        or death (at home) of the patient.
                                    </div>
                                </div>
                                <div class="margin">
                                    <input name="DischargeFromAgency_M0906DischargeDate" id="DischargeFromAgency_M0906DischargeDate"
                                        type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="rowOasisButtons">
                            <ul>
                                <li style="float: left">
                                    <input type="button" value="Save" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                                <li style="float: left">
                                    <input type="button" value="Save/Exit" onclick="Discharge.FormSubmit($(this),'Edit');" /></li>
                            </ul>
                        </div>
                        <%  } %>
                    </div>
                </div>
            </div>
        </div>
        <div class="abs window_bottom">
            Patient Landing Screen
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
<% Html.Telerik()
       .ScriptRegistrar()
       .Scripts(script => script.Add("/Models/Discharge.js"))
       .OnDocumentReady(() =>
        {%>
Discharge.Init();
<%}); 
%>
