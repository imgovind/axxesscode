﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisTransferInPatientDischargedCardiacForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("TransferInPatientDischarged_Id", Model.Id)%>
<%= Html.Hidden("TransferInPatientDischarged_Action", "Edit")%>
<%= Html.Hidden("TransferInPatientDischarged_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "TransferInPatientDischarged")%>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="margin">
                (M1500) Symptoms in Heart Failure Patients: If patient has been diagnosed with heart
                failure, did the patient exhibit symptoms indicated by clinical heart failure guidelines
                (including dyspnea, orthopnea, edema, or weight gain) at any point since the previous
                OASIS assessment?
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("TransferInPatientDischarged_M1500HeartFailureSymptons", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientDischarged_M1500HeartFailureSymptons", "00", data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "00" ? true : false, new { @id = "" })%>
            &nbsp;0 - No [ Go to M2004 at TRN; Go to M1600 at DC ]<br />
            <%=Html.RadioButton("TransferInPatientDischarged_M1500HeartFailureSymptons", "01", data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Yes<br />
            <%=Html.RadioButton("TransferInPatientDischarged_M1500HeartFailureSymptons", "02", data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Not assessed [Go to M2004 at TRN; Go to M1600 at DC ]<br />
            <%=Html.RadioButton("TransferInPatientDischarged_M1500HeartFailureSymptons", "NA", data.ContainsKey("M1500HeartFailureSymptons") && data["M1500HeartFailureSymptons"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - Patient does not have diagnosis of heart failure [Go to M2004 at TRN; Go to M1600
            at DC ]<br />
        </div>
    </div>
</div>
<div class="rowOasis" id="transfer_M1510">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="margin">
                (M1510) Heart Failure Follow-up: If patient has been diagnosed with heart failure
                and has exhibited symptoms indicative of heart failure since the previous OASIS
                assessment, what action(s) has (have) been taken to respond? (Mark all that apply.)
            </div>
        </div>
        <div class="margin">
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupNoAction" type="hidden"
                value=" " />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupNoAction" type="checkbox"
                value="1" '<% if(data.ContainsKey("M1510HeartFailureFollowupNoAction") && data["M1510HeartFailureFollowupNoAction"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;0
            - No action taken<br />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupPhysicianCon" type="hidden"
                value=" " />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupPhysicianCon" type="checkbox"
                value="1" '<% if(data.ContainsKey("M1510HeartFailureFollowupPhysicianCon") && data["M1510HeartFailureFollowupPhysicianCon"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;1
            - Patient’s physician (or other primary care practitioner) contacted the same day<br />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupAdvisedEmg" type="hidden"
                value=" " />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupAdvisedEmg" type="checkbox"
                value="1" '<% if(data.ContainsKey("M1510HeartFailureFollowupAdvisedEmg") && data["M1510HeartFailureFollowupAdvisedEmg"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;2
            - Patient advised to get emergency treatment (e.g., call 911 or go to emergency
            room)<br />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupParameters" type="hidden"
                value=" " />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupParameters" type="checkbox"
                value="1" '<% if(data.ContainsKey("M1510HeartFailureFollowupParameters") && data["M1510HeartFailureFollowupParameters"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;3
            - Implemented physician-ordered patient-specific established parameters for treatment<br />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupInterventions"
                type="hidden" value=" " />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupInterventions"
                type="checkbox" value="1" '<% if(data.ContainsKey("M1510HeartFailureFollowupInterventions") && data["M1510HeartFailureFollowupInterventions"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;4
            - Patient education or other clinical interventions<br />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupChange" type="hidden"
                value=" " />
            <input name="TransferInPatientDischarged_M1510HeartFailureFollowupChange" type="checkbox"
                value="1" '<% if(data.ContainsKey("M1510HeartFailureFollowupChange") && data["M1510HeartFailureFollowupChange"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;5
            - Obtained change in care plan orders (e.g., increased monitoring by agency, change
            in visit frequency, telehealth, etc.)
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="TransferForDischarge.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="TransferForDischarge.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
