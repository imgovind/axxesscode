﻿namespace Axxess.Scheduled.HomeHealthGold
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class PostReportResponse
    {
        [DataMember]
        public Result result { get; set; }
    }
}
