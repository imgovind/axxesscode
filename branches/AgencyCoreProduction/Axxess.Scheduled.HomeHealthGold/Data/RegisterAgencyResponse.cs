﻿namespace Axxess.Scheduled.HomeHealthGold
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class RegisterAgencyResponse
    {
        public Value value { get; set; }
        public Result result { get; set; }
    }

    [DataContract]
    public class Result
    {
        [DataMember]
        public string msg { get; set; }
        [DataMember]
        public bool success { get; set; }
    }

    [Serializable]
    public class Value
    {
        public Guid agencyId { get; set; }
        public string password { get; set; }
        public string agencyIdHash { get; set; }
    }
}
