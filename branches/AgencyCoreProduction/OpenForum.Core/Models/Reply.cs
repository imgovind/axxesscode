﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using OpenForum.Core.DataAccess;

using SubSonic.SqlGeneration.Schema;

namespace OpenForum.Core.Models
{
    [Serializable]
    public class Reply
    {
        public Reply()
        {
            CreatedDate = DateTime.Now;
        }

        public Reply(Post post)
            : this()
        {
            PostId = post.Id;
        }

        public int Id { get; set; }
        public int PostId { get; set; }
        public string CreatedBy { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime CreatedDate { get; private set; }

        private string _body { get; set; }
        public string Body
        {
            get { return _body; }
            set { _body = HtmlHelper.FixUp(value); }
        }

        [SubSonicIgnore]
        public ForumUser User { get; set; }

        protected void OnValidate(ChangeAction action)
        {
            Validate();
        }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Body))
            {
                throw new OpenForumException("Please provide text for your reply.");
            }
            else
            {
                HtmlHelper.Validate(Body);
            }
        }
    }
}
