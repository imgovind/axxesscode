﻿namespace Axxess.Scheduled.AbilityEligibility
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Diagnostics;
    using System.Collections.Generic;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    class Program
    {
        #region Private Members

        private static TextWriter textWriter = null;
        private const int PATIENT_MIGRATION_THREAD_COUNT = 20;
        private static string output = Path.Combine(App.Root, string.Format("AbilityLog_{0}.txt", DateTime.Now.Ticks.ToString()));

        #endregion

        #region Main Methods

        static void Main(string[] args)
        {
            Bootstrapper.Run();
            Run();
            Console.ReadLine();
        }

        public static void Run()
        {
            using (textWriter = new StreamWriter(output, true))
            {
                try
                {
                    var startTime = DateTime.Now;
                    var stopWatch = new Stopwatch();
                    stopWatch.Start();

                    var agencies = Database.GetAgencies();
                    if (agencies != null && agencies.Count > 0)
                    {
                        agencies.ForEach(agency =>
                        {
                            Console.WriteLine("Verifying Agency: {0}", agency.Name);
                            var patients = Database.GetPatients(agency.Id);
                            if (patients != null && patients.Count > 0)
                            {
                                var patientVerifications = new List<PatientVerification>(patients.Count);
                                if (patients.Count < PATIENT_MIGRATION_THREAD_COUNT)
                                {
                                    ManualResetEvent[] manualEvents = new ManualResetEvent[patients.Count];
                                    for (int i = 0; i < patients.Count; i++)
                                    {
                                        Console.WriteLine("Verifying Patient: {0}", patients[i].DisplayName);
                                        manualEvents[i] = new ManualResetEvent(false);
                                        PatientVerification patientVerification = new PatientVerification(patients[i], agency.MedicareProviderNumber, manualEvents[i]);
                                        patientVerifications.Add(patientVerification);
                                        ThreadPool.QueueUserWorkItem(patientVerification.ThreadPoolCallback, i);
                                    }

                                    WaitHandle.WaitAll(manualEvents);
                                    patientVerifications.ForEach(patientVerification =>
                                    {
                                        if (patientVerification.ErrorMessage.IsNotNullOrEmpty())
                                        {
                                            textWriter.WriteLine(patientVerification.ErrorMessage);
                                            textWriter.WriteLine();
                                        }
                                    });
                                }
                                else
                                {
                                    var counter = 0;
                                    var chunk = patients.Count;
                                    do
                                    {
                                        var patientChunk = patients.Skip(counter * PATIENT_MIGRATION_THREAD_COUNT).Take(PATIENT_MIGRATION_THREAD_COUNT).ToList();
                                        if (patientChunk.Count > 0)
                                        {
                                            ManualResetEvent[] manualEvents = new ManualResetEvent[patientChunk.Count];
                                            for (int i = 0; i < patientChunk.Count; i++)
                                            {
                                                Console.WriteLine("Verifying Patient: {0}", patientChunk[i].DisplayName);
                                                manualEvents[i] = new ManualResetEvent(false);
                                                PatientVerification patientVerification = new PatientVerification(patientChunk[i], agency.MedicareProviderNumber, manualEvents[i]);
                                                patientVerifications.Add(patientVerification);
                                                ThreadPool.QueueUserWorkItem(patientVerification.ThreadPoolCallback, i);
                                            }

                                            WaitHandle.WaitAll(manualEvents);

                                            chunk -= PATIENT_MIGRATION_THREAD_COUNT;
                                            counter++;
                                        }
                                    } while (chunk > 0);

                                    patientVerifications.ForEach(patientVerification =>
                                    {
                                        if (patientVerification.ErrorMessage.IsNotNullOrEmpty())
                                        {
                                            textWriter.WriteLine(patientVerification.ErrorMessage);
                                            textWriter.WriteLine();
                                        }
                                    });
                                }
                            }
                            Console.WriteLine("Verification Complete for {0}.", agency.Name);
                            Console.WriteLine("========================================================================");
                            Console.WriteLine();
                        });
                    }

                    stopWatch.Stop();
                    Console.WriteLine("Started @: {0}", startTime.ToString("MM/dd/yyyy @ hh:mm:ss tt"));
                    Console.WriteLine("Finished @: {0}", DateTime.Now.ToString("MM/dd/yyyy @ hh:mm:ss tt"));
                    Console.WriteLine("Script Completed in {0} minutes or {1} hour(s)", stopWatch.Elapsed.TotalMinutes, Convert.ToDouble(stopWatch.Elapsed.TotalMinutes / 60));
                }
                catch (Exception ex)
                {
                    textWriter.WriteLine(ex.ToString());
                }
                Console.ReadLine();
            }
        }

        #endregion
    }
}
