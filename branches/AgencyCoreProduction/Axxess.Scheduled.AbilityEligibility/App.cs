﻿namespace Axxess.Scheduled.AbilityEligibility
{
    using System;

    public static class App
    {
        public static string Root = AppDomain.CurrentDomain.BaseDirectory.ToLower().Replace("\\bin\\debug", "");
    }
}
