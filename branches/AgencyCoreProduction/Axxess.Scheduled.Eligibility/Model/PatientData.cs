﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Extension;

namespace Axxess.Scheduled.Eligibility.Model
{
    public class PatientMessageData
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string FirstName { get; set; }
        public string PatientIdNumber { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string MedicareNumber { get; set; }
        public string PlanNumber { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Payor { get { return InsuranceName != null && InsuranceName != "" ? string.Format("{0}{1}{2}",this.InsuranceName,  this.AddressLineOne.IsNotNullOrEmpty() ? "<br/>" + this.AddressLineOne : "", this.AddressLineTwo.IsNotNullOrEmpty() ? "<br/>"  + this.AddressLineTwo : "") : ""; } }
        public string InsuranceName { get; set; }
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get { return string.Format("{0}{1}", this.City, this.State.IsNotNullOrEmpty() ? ", " + this.State : ""); } }
        public string City { get; set; }
        public string State { get; set; }
        public string NPI { get; set; }

        public string DisplayNameWithMi
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase(), (this.MiddleInitial.IsNotNullOrEmpty() ? " " + this.MiddleInitial.ToUpperCase() + "." : string.Empty));
            }
        }
    }

    public class PatientEligibilityData
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid EligibilityId { get; set; }
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public DateTime DateofBirth { get; set; }
        public string MedicareNumber { get; set; }
        public string Gender { get; set; }
        public DateTime LastEligibilityCheckDate { get; set; }
        public string PrimaryInsurance { get; set; }
        public string SecondaryInsurance { get; set; }
        public string TertiaryInsurance { get; set; }
        public string NPI { get; set; }
        public string PreviousResult { get; set; }
        public string DisplayNameWithMi
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase(), (this.MiddleInitial.IsNotNullOrEmpty() ? " " + this.MiddleInitial.ToUpperCase() + "." : string.Empty));
            }
        }
    }
}
