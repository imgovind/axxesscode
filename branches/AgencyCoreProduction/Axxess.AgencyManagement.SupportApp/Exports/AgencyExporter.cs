﻿namespace Axxess.AgencyManagement.SupportApp.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class AgencyExporter : BaseExporter
    {
        private IList<Agency> agencies;
        public AgencyExporter(IList<Agency> agencies)
            : base()
        {
            this.agencies = agencies;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agencies";
            base.workBook.SummaryInformation = si;
        }

        protected override void Write()
        {
            var sheet = base.workBook.CreateSheet("Agencies");

            var headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            var headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Tax Id");
            headerRow.CreateCell(2).SetCellValue("NPI");
            headerRow.CreateCell(3).SetCellValue("Contact Person");
            headerRow.CreateCell(4).SetCellValue("Contact Email");
            headerRow.CreateCell(5).SetCellValue("Contact Phone");
            headerRow.CreateCell(6).SetCellValue("Package");
            headerRow.CreateCell(7).SetCellValue("Agreement Signed");
            headerRow.CreateCell(8).SetCellValue("State");
            headerRow.CreateCell(9).SetCellValue("Zip Code");
            headerRow.CreateCell(10).SetCellValue("CBSA");
            headerRow.CreateCell(11).SetCellValue("Provider");
            headerRow.CreateCell(12).SetCellValue("Created");

            headerRow.RowStyle = headerStyle;

            if (this.agencies.Count > 0)
            {
                int i = 1;
                this.agencies.ForEach(a =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.Name.ToTitleCase());
                    dataRow.CreateCell(1).SetCellValue(a.TaxId);
                    dataRow.CreateCell(2).SetCellValue(a.NationalProviderNumber);
                    dataRow.CreateCell(3).SetCellValue(a.ContactPersonDisplayName.ToTitleCase());
                    dataRow.CreateCell(4).SetCellValue(a.ContactPersonEmail);
                    dataRow.CreateCell(5).SetCellValue(a.ContactPersonPhone.ToPhone());
                    dataRow.CreateCell(6).SetCellValue(a.PackageDescription);
                    dataRow.CreateCell(7).SetCellValue(a.IsAgreementSigned ? "true" : "false");
                    dataRow.CreateCell(8).SetCellValue(a.MainLocation != null ? a.MainLocation.AddressStateCode : "");
                    dataRow.CreateCell(9).SetCellValue(a.MainLocation != null ? a.MainLocation.AddressZipCode : "");
                    dataRow.CreateCell(10).SetCellValue(a.MainLocation != null ? a.MainLocation.CBSA : "");
                    dataRow.CreateCell(11).SetCellValue(a.MainLocation != null ? a.MedicareProviderNumber : "");
                    dataRow.CreateCell(12).SetCellValue(a.Created.ToString("MM/dd/yyy"));
                    i++;
                });
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
            sheet.AutoSizeColumn(12);
        }
    }
}
