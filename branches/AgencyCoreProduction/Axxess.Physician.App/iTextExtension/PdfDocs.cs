﻿namespace Axxess.Physician.App.iTextExtension {
    using System;
    public class PdfDocs {
        public static PdfDoc PlanOfCare485          = new PdfDoc(AppSettings.PlanOfCare485Pdf);
        public static PdfDoc PlanOfCare487          = new PdfDoc(AppSettings.PlanOfCare487Pdf);
        public static PdfDoc PhysicianOrder         = new PdfDoc(AppSettings.PhysicianOrderPdf);
        public static PdfDoc PhysFaceToFace         = new PdfDoc(AppSettings.PhysFaceToFacePdf, AppSettings.PhysFaceToFaceXml);
        public static PdfDoc MSWEval                = new PdfDoc(AppSettings.MSWEvalPdf, AppSettings.MSWEvalXml);
        public static PdfDoc OTEval                 = new PdfDoc(AppSettings.OTEvalPdf, AppSettings.OTEvalXml);
        public static PdfDoc OTEval2                = new PdfDoc(AppSettings.OTEvalPdf, AppSettings.OTEvalXml2);
        public static PdfDoc OTEval3                = new PdfDoc(AppSettings.OTEvalPdf, AppSettings.OTEvalXml3);
        public static PdfDoc OTEval4                = new PdfDoc(AppSettings.OTEvalPdf, AppSettings.OTEvalXml4);
        public static PdfDoc PTEval                 = new PdfDoc(AppSettings.PTEvalPdf, AppSettings.PTEvalXml);
        public static PdfDoc PTEval2                = new PdfDoc(AppSettings.PTEvalPdf, AppSettings.PTEvalXml2);
        public static PdfDoc PTEval3                = new PdfDoc(AppSettings.PTEvalPdf, AppSettings.PTEvalXml3);
        public static PdfDoc PTEval4                = new PdfDoc(AppSettings.PTEvalPdf, AppSettings.PTEvalXml4);
        public static PdfDoc PTEval5                = new PdfDoc(AppSettings.PTEvalPdf, AppSettings.PTEvalXml5);
        public static PdfDoc PTPOC                  = new PdfDoc(AppSettings.POCPdf, AppSettings.PTPOCXml);
        public static PdfDoc OTPOC                  = new PdfDoc(AppSettings.POCPdf, AppSettings.OTPOCXml);
        public static PdfDoc OTPOC2                 = new PdfDoc(AppSettings.POCPdf, AppSettings.OTPOCXml2);
        public static PdfDoc STPOC                  = new PdfDoc(AppSettings.POCPdf, AppSettings.STPOCXml);
        public static PdfDoc STEval                 = new PdfDoc(AppSettings.STEvalPdf, AppSettings.STEvalXml);
        public static PdfDoc STEval3                = new PdfDoc(AppSettings.STEvalPdf, AppSettings.STEvalXml3);
        public static PdfDoc STDischarge2           = new PdfDoc(AppSettings.STEvalPdf, AppSettings.STDischargeXml2);
        public static PdfDoc PTDischarge            = new PdfDoc(AppSettings.TherapyPdf, AppSettings.PTDischargeXml);
        public static PdfDoc PTDischarge2           = new PdfDoc(AppSettings.TherapyPdf, AppSettings.PTDischargeXml2);
        public static PdfDoc SixtyDaySummary        = new PdfDoc(AppSettings.SixtyDaySummaryPdf, AppSettings.SixtyDaySummaryXml);
        public static PdfDoc PTReassessment         = new PdfDoc(AppSettings.TherapyPdf, AppSettings.PTReassessmentXml);
        public static PdfDoc OTReassessment         = new PdfDoc(AppSettings.TherapyPdf, AppSettings.OTReassessmentXml);
        public static PdfDoc PsychAssessment2       = new PdfDoc(AppSettings.PsychAssessmentPdf, AppSettings.PsychAssessmentXml2);
    }
}
