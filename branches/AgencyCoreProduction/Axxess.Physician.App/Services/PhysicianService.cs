﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement;

    public class PhysicianService : IPhysicianService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;

        public PhysicianService(IAgencyManagementDataProvider agencyManagmentDataProvider, IOasisCDataProvider oasisCDataProvider)
        {
            Check.Argument.IsNotNull(oasisCDataProvider, "oasisCDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.planofCareRepository = oasisCDataProvider.PlanofCareRepository;
            this.patientRepository = agencyManagmentDataProvider.PatientRepository;
            this.physicianRepository = agencyManagmentDataProvider.PhysicianRepository;
            this.agencyRepository = agencyManagmentDataProvider.AgencyRepository;
        }
        
        #endregion

        #region IPhysicianService Members

        public List<FaceToFaceEncounter>GetFaceToFaceEncounters()
        {
            var faceToFaceEncounter = patientRepository.GetFaceToFaceEncountersByPhysician(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderSavedByPhysician);
            faceToFaceEncounter.ForEach(ffe =>
            {
                var patient = patientRepository.GetPatientOnly(ffe.PatientId, ffe.AgencyId);
                ffe.DisplayName = patient != null ? patient.DisplayName : string.Empty;
                var agency = AgencyEngine.Get(ffe.AgencyId);
                ffe.AgencyDisplayName = agency != null ? agency.Name : string.Empty;
                ffe.PrintUrl = Url.Print(ffe.Id, ffe.PatientId, ffe.EpisodeId, ffe.AgencyId, OrderType.FaceToFaceEncounter, true);
            });
            return faceToFaceEncounter;
        }

        public List<CarePlanOversight> GetPhysicianCPO()
        {
            var cpos = physicianRepository.GetCPO(Current.LoginId);
            if (cpos != null)
            {
                cpos.OrderBy(c => c.LogDateFormatted)
                    .ForEach(cpo =>
                    {
                        cpo.AgencyName = agencyRepository.GetAgencyOnly(cpo.AgencyId).Name;
                        cpo.PatientName = patientRepository.GetPatientNameById(cpo.PatientId, cpo.AgencyId);
                    });
            }
            return cpos;
        }

        public bool SaveCPO(CarePlanOversight cpo)
        {
            if (patientRepository.UpdateCPO(cpo))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Order> GetOrders()
        {
            var orders = new List<Order>();

            var physicianOrders = patientRepository.GetPhysicianOrdersByPhysician(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderSentToPhysicianElectronically);
            physicianOrders.ForEach(po =>
            {
                var patient = patientRepository.GetPatientOnly(po.PatientId, po.AgencyId);
                orders.Add(new Order
                {
                    Id = po.Id,
                    Status = po.Status,
                    AgencyId = po.AgencyId,
                    Number = po.OrderNumber,
                    Type = OrderType.PhysicianOrder,
                    PrintUrl = Url.Print(po.Id, po.PatientId, po.EpisodeId, po.AgencyId, OrderType.PhysicianOrder, false),
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    AgencyName = AgencyEngine.Get(po.AgencyId).Name,
                    SentDate = po.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = po.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var planofCareOrders = planofCareRepository.GetByPhysicianId(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderSentToPhysicianElectronically);
            planofCareOrders.ForEach(poc =>
            {
                var patient = patientRepository.GetPatientOnly(poc.PatientId, poc.AgencyId);
                orders.Add(new Order
                {
                    Id = poc.Id,
                    Type = OrderType.HCFA485,
                    Number = poc.OrderNumber,
                    Status = poc.Status,
                    AgencyId = poc.AgencyId,
                    PrintUrl = Url.Print(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485, false),
                    AgencyName = AgencyEngine.Get(poc.AgencyId).Name,
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = poc.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var planofCareStandAloneOrders = planofCareRepository.GetStandAloneByPhysicianId(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderSentToPhysicianElectronically);
            planofCareStandAloneOrders.ForEach(poc =>
            {
                var patient = patientRepository.GetPatientOnly(poc.PatientId, poc.AgencyId);
                orders.Add(new Order
                {
                    Id = poc.Id,
                    Type = OrderType.HCFA485StandAlone,
                    Number = poc.OrderNumber,
                    Status = poc.Status,
                    AgencyId = poc.AgencyId,
                    AgencyName = AgencyEngine.Get(poc.AgencyId).Name,
                    PrintUrl = Url.Print(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485StandAlone, false),
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = poc.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var evalOrders = patientRepository.GetEvalOrdersByPhysician(Current.PhysicianIdentifiers,new int[] {(int)ScheduleStatus.EvalSentToPhysicianElectronically,(int)ScheduleStatus.OrderSentToPhysicianElectronically});
            evalOrders.ForEach(eval =>
            {
                var patient = patientRepository.GetPatientOnly(eval.PatientId, eval.AgencyId);
                orders.Add(new Order
                {
                    Id = eval.Id,
                    Type = GetOrderType(eval.NoteType),
                    //Text = GetDisciplineType(eval.NoteType).GetDescription(),
                    Number = eval.OrderNumber,
                    Status = eval.Status,
                    AgencyId = eval.AgencyId,
                    AgencyName = AgencyEngine.Get(eval.AgencyId).Name,
                    PrintUrl = Url.Print(eval.Id, eval.PatientId, eval.EpisodeId, eval.AgencyId, GetOrderType(eval.NoteType), false),
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    SentDate = eval.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = GetNoteVisitDate(eval.Note).ToShortDateString(),//eval.SentDate.ToShortDateString().ToZeroFilled(),
                    ReceivedDate = DateTime.Today
                });
            });

            List<FaceToFaceEncounter> facetofaceOrder = patientRepository.GetFaceToFaceEncountersByPhysician(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderSavedByPhysician);
            
            facetofaceOrder.ForEach(ftf =>
                {
                    var patient = patientRepository.GetPatientOnly(ftf.PatientId, ftf.AgencyId);
                    orders.Add(new Order
                    {
                        Id = ftf.Id,
                        Type = OrderType.FaceToFaceEncounter,
                        Number = ftf.OrderNumber,
                        Status = ftf.Status,
                        AgencyId = ftf.AgencyId,
                        AgencyName = AgencyEngine.Get(ftf.AgencyId).Name,
                        PrintUrl = Url.Print(ftf.Id, ftf.PatientId, ftf.EpisodeId, ftf.AgencyId, OrderType.FaceToFaceEncounter, false),
                        PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                        SentDate = ftf.SentDate.ToShortDateString().ToZeroFilled(),
                        CreatedDate = ftf.RequestDate.ToShortDateString(),
                        ReceivedDate = DateTime.Today
                    });
                });

            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        private DateTime GetNoteVisitDate(string note)
        {
            XElement rootElement = XElement.Parse(note);
            IDictionary<string, NotesQuestion> dict = new Dictionary<string, NotesQuestion>();
            foreach (XElement el in rootElement.Elements())
            {
                if (el.Attribute("Name") != null && el.Attribute("Answer") != null && el.Attribute("Type") != null)
                {
                    dict.Add(el.Attribute("Name").Value, new NotesQuestion { Name = el.Attribute("Name").Value, Answer = el.Attribute("Answer").Value, Type = el.Attribute("Type").Value });
                }
            }
            if (dict != null && dict.ContainsKey("VisitDate"))
            {
                return dict["VisitDate"].Answer.ToDateTime();
            }
            else
                return DateTime.MinValue;
        }

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTEvaluation":
                        orderType = OrderType.PtEvaluation;
                        break;
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "PTReassessment":
                        orderType = OrderType.PTReassessment;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "OTReassessment":
                        orderType = OrderType.OTReassessment;
                        break;
                    case "OTDischarge":
                        orderType = OrderType.OTDischarge;
                        break;
                    case "STDischarge":
                        orderType = OrderType.STDischarge;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                    case "PTDischarge":
                        orderType = OrderType.PTDischarge;
                        break;
                    case "SixtyDaySummary":
                        orderType = OrderType.SixtyDaySummary;
                        break;
                    case "MSWEvaluationAssessment":
                        orderType = OrderType.MSWEvaluation;
                        break;
                    case "MSWDischarge":
                        orderType = OrderType.MSWDischarge;
                        break;
                    case "PTPlanOfCare":
                        orderType = OrderType.PTPlanOfCare;
                        break;
                    case "OTPlanOfCare":
                        orderType = OrderType.OTPlanOfCare;
                        break;
                    case "STPlanOfCare":
                        orderType = OrderType.STPlanOfCare;
                        break;
                    case "SNPsychAssessment":
                        orderType = OrderType.SNPsychAssessment;
                        break;

                }
            }
            return orderType;
        }

        public string GetReturnComments(Guid agencyId, Guid eventId, Guid episodeId, Guid patientId)
        {
            string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, agencyId);
            List<ReturnComment> NewComments = patientRepository.GetReturnComments(agencyId, episodeId, eventId);
            foreach (ReturnComment comment in NewComments)
            {
                if (comment.IsDeprecated) continue;
                if (CommentString.IsNotNullOrEmpty()) CommentString += "<hr/>";
                if (comment.UserId == Current.User.Id) CommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                CommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", UserEngine.GetName(comment.UserId, agencyId), comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return CommentString;
        }

        public bool AddReturnComments(Guid eventId, Guid episodeId, string comment, Guid agencyId)
        {
            return patientRepository.AddReturnComment(new ReturnComment
            {
                AgencyId = agencyId,
                Comments = comment,
                Created = DateTime.Now,
                EpisodeId = episodeId,
                EventId = eventId,
                Modified = DateTime.Now,
                UserId = Current.User.Id
            });
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                    case "PTDischarge":
                        task = DisciplineTasks.PTDischarge;
                        break;
                }
            }
            return task;
        }

        public List<Order> GetOrdersCompleted(DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();

            var physicianOrders = patientRepository.GetPhysicianOrdersByPhysicianAndDate(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, startDate, endDate);
            physicianOrders.ForEach(po =>
            {
                var patient = patientRepository.GetPatientOnly(po.PatientId, po.AgencyId);
                orders.Add(new Order
                {
                    Id = po.Id,
                    Status = po.Status,
                    AgencyId = po.AgencyId,
                    Number = po.OrderNumber,
                    Type = OrderType.PhysicianOrder,
                    PrintUrl = Url.View(po.Id, po.PatientId, po.EpisodeId, po.AgencyId, OrderType.PhysicianOrder, true),
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    AgencyName = AgencyEngine.Get(po.AgencyId).Name,
                    SentDate = po.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = po.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var planofCareOrders = planofCareRepository.GetByPhysicianIdAndDate(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, startDate, endDate);
            planofCareOrders.ForEach(poc =>
            {
                var patient = patientRepository.GetPatientOnly(poc.PatientId, poc.AgencyId);
                orders.Add(new Order
                {
                    Id = poc.Id,
                    Type = OrderType.HCFA485,
                    Number = poc.OrderNumber,
                    Status = poc.Status,
                    AgencyId = poc.AgencyId,
                    PrintUrl = Url.View(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485, true),
                    AgencyName = AgencyEngine.Get(poc.AgencyId).Name,
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = poc.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var planofCareStandAloneOrders = planofCareRepository.GetStandAloneByPhysicianIdAndDate(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, startDate, endDate);
            planofCareStandAloneOrders.ForEach(poc =>
            {
                var patient = patientRepository.GetPatientOnly(poc.PatientId, poc.AgencyId);
                orders.Add(new Order
                {
                    Id = poc.Id,
                    Type = OrderType.HCFA485StandAlone,
                    Number = poc.OrderNumber,
                    Status = poc.Status,
                    AgencyId = poc.AgencyId,
                    AgencyName = AgencyEngine.Get(poc.AgencyId).Name,
                    PrintUrl = Url.View(poc.Id, poc.PatientId, poc.EpisodeId, poc.AgencyId, OrderType.HCFA485StandAlone, true),
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                    CreatedDate = poc.OrderDateFormatted,
                    ReceivedDate = DateTime.Today
                });
            });

            var facetofaceOrders = patientRepository.GetFaceToFaceEncountersByPhysicianAndDate(Current.PhysicianIdentifiers, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, startDate, endDate);
            facetofaceOrders.ForEach(ffe =>
            {
                var patient = patientRepository.GetPatientOnly(ffe.PatientId, ffe.AgencyId);
                orders.Add(new Order
                {
                    Id = ffe.Id,
                    Type = OrderType.FaceToFaceEncounter,
                    Number = ffe.OrderNumber,
                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                    Status = ffe.Status,
                    AgencyId = ffe.AgencyId,
                    AgencyName = AgencyEngine.Get(ffe.AgencyId).Name,
                    PrintUrl = Url.View(ffe.Id, ffe.PatientId, ffe.EpisodeId, ffe.AgencyId, OrderType.FaceToFaceEncounter, true),
                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy"),
                    ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate,
                    SendDate = ffe.RequestDate
                });
            });

            var evalOrders = patientRepository.GetEvalOrdersByPhysician(Current.PhysicianIdentifiers, new int[]{ (int)ScheduleStatus.EvalReturnedWPhysicianSignature, (int)ScheduleStatus.OrderReturnedWPhysicianSignature});
            evalOrders.ForEach(eval =>
            {
                var orderDate = GetNoteVisitDate(eval.Note);
                if (orderDate >= startDate && orderDate <= endDate)
                {
                    var patient = patientRepository.GetPatientOnly(eval.PatientId, eval.AgencyId);
                    orders.Add(new Order
                    {
                        Id = eval.Id,
                        Type = GetOrderType(eval.NoteType),
                        //Text = GetDisciplineType(eval.NoteType).GetDescription(),
                        Number = eval.OrderNumber,
                        Status = eval.Status,
                        AgencyId = eval.AgencyId,
                        AgencyName = AgencyEngine.Get(eval.AgencyId).Name,
                        PrintUrl = Url.View(eval.Id, eval.PatientId, eval.EpisodeId, eval.AgencyId, GetOrderType(eval.NoteType), true),
                        PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                        SentDate = eval.SentDate.ToShortDateString().ToZeroFilled(),
                        CreatedDate = orderDate.ToShortDateString(),//eval.SentDate.ToShortDateString().ToZeroFilled(),
                        ReceivedDate = DateTime.Today
                    });
                }
            });
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

       

        public bool UpdatePhysicianOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;

            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            PhysicianOrder physicianOrder = null;

            if (!orderId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(agencyId, episodeId, patientId, orderId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(agencyId, scheduleEvent.UserId, patientId, orderId);
                }

                physicianOrder = patientRepository.GetOrder(orderId, patientId, agencyId);
                if (physicianOrder != null)
                {
                    if (actionType == "Approve")
                    {
                        physicianOrder.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        physicianOrder.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                        physicianOrder.PhysicianSignatureDate = DateTime.Now;
                        physicianOrder.ReceivedDate = DateTime.Now;
                        if (patientRepository.UpdateOrder(physicianOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        physicianOrder.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                        physicianOrder.Modified = DateTime.Now;
                        if (patientRepository.UpdateOrder(physicianOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(agencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(agencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(agencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateFaceToFaceEncounter(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;

            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            FaceToFaceEncounter facetoFace = null;

            if (!orderId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(agencyId, episodeId, patientId, orderId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(agencyId, scheduleEvent.UserId, patientId, orderId);
                }

                facetoFace = patientRepository.GetFaceToFaceEncounter(orderId, patientId, agencyId);
                if (facetoFace != null)
                {
                    if (actionType == "Approve")
                    {
                        facetoFace.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        facetoFace.SignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                        facetoFace.SignatureDate = DateTime.Now;
                        facetoFace.ReceivedDate = DateTime.Now;
                        if (patientRepository.UpdateFaceToFaceEncounter(facetoFace))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        facetoFace.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                        facetoFace.Modified = DateTime.Now;
                        if (patientRepository.UpdateFaceToFaceEncounter(facetoFace))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(agencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(agencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(agencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateFaceToFaceEncounter(FaceToFaceEncounter order)
        {
            var result = false;
            FaceToFaceEncounter existingFacetoFace = patientRepository.GetFaceToFaceEncounter(order.Id, order.PatientId, order.AgencyId);

            if (existingFacetoFace != null)
            {
                existingFacetoFace.Status = order.Status;
                existingFacetoFace.Modified = DateTime.Now;
                existingFacetoFace.Services = order.Services;
                existingFacetoFace.ServicesOther = order.ServicesOther;
                existingFacetoFace.Certification = order.Certification;
                existingFacetoFace.EncounterDate = order.EncounterDate;
                existingFacetoFace.MedicalReason = order.MedicalReason;
                existingFacetoFace.ClinicalFinding = order.ClinicalFinding;

                if (order.ServicesArray != null && order.ServicesArray.Count > 0)
                {
                    existingFacetoFace.Services = order.ServicesArray.ToArray().AddColons();
                }

                if (existingFacetoFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature)
                {
                    existingFacetoFace.ReceivedDate = DateTime.Now;
                    existingFacetoFace.SignatureDate = DateTime.Now;
                    existingFacetoFace.SignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                }
                else
                {
                    existingFacetoFace.SignatureText = string.Empty;
                }

                if (patientRepository.UpdateFaceToFaceEncounter(existingFacetoFace))
                {
                    var scheduleEvent = patientRepository.GetSchedule(existingFacetoFace.AgencyId, existingFacetoFace.EpisodeId, existingFacetoFace.PatientId, existingFacetoFace.Id);
                    if (scheduleEvent != null)
                    {
                        scheduleEvent.Status = existingFacetoFace.Status.ToString();
                        if (patientRepository.UpdateEpisode(existingFacetoFace.AgencyId, scheduleEvent))
                        {
                            var userEvent = userRepository.GetEvent(existingFacetoFace.AgencyId, scheduleEvent.UserId, existingFacetoFace.PatientId, existingFacetoFace.Id);
                            if (userEvent != null)
                            {
                                userEvent.Status = scheduleEvent.Status;
                                userEvent.ReturnReason = scheduleEvent.ReturnReason;
                                userRepository.UpdateEvent(existingFacetoFace.AgencyId, userEvent);
                                result = true;
                            }
                            else
                            {
                                userRepository.AddUserEvent(existingFacetoFace.AgencyId, scheduleEvent.PatientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool UpdateEvalOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid evalId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;

            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            PatientVisitNote evalOrder = null;

            if (!evalId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(agencyId, episodeId, patientId, evalId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(agencyId, scheduleEvent.UserId, patientId, evalId);
                }
                bool poc = false;
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTPlanOfCare || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTPlanOfCare || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STPlanOfCare)
                {
                    poc = true;
                }
                evalOrder = patientRepository.GetVisitNote(agencyId, patientId, evalId);
                if (evalOrder != null)
                {
                    if (actionType == "Approve")
                    {
                        evalOrder.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                        evalOrder.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                        evalOrder.PhysicianSignatureDate = DateTime.Now;
                        evalOrder.ReceivedDate = DateTime.Now;
                        if (patientRepository.UpdateVisitNote(evalOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                if (poc)
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                }
                                else
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString();
                                }
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    if (poc)
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                    }
                                    else
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString();
                                    }
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        evalOrder.Status = ((int)ScheduleStatus.EvalReturnedByPhysician);
                        evalOrder.Modified = DateTime.Now;
                        if (patientRepository.UpdateVisitNote(evalOrder))
                        {
                            if (scheduleEvent != null)
                            {
                                if (poc)
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                }
                                else
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.EvalReturnedByPhysician).ToString();
                                }
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    if (poc)
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    }
                                    else
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.EvalReturnedByPhysician).ToString();
                                    }
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    if (shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(agencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(agencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(agencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        #endregion
    }
}
