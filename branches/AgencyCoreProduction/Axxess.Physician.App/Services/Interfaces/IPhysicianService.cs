﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Axxess.AgencyManagement.Domain;

    public interface IPhysicianService
    {
        List<Order> GetOrders();
        List<Order> GetOrdersCompleted(DateTime startDate, DateTime endDate);
        List<FaceToFaceEncounter> GetFaceToFaceEncounters();
        bool AddReturnComments(Guid eventId, Guid episodeId, string comment, Guid agencyId);
        bool UpdatePhysicianOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string action, string reason);
        bool UpdateFaceToFaceEncounter(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId, string action, string reason);
        bool UpdateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceOrder);
        bool UpdateEvalOrder(Guid agencyId, Guid episodeId, Guid patientId, Guid evalId, string action, string reason);
        List<CarePlanOversight> GetPhysicianCPO();
        bool SaveCPO(CarePlanOversight cpo);
        string GetReturnComments(Guid agencyId, Guid eventId, Guid episodeId, Guid patientId);
    }
}
