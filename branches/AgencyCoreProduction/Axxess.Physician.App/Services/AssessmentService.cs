﻿namespace Axxess.Physician.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Physician.App.ViewData;
    using Axxess.Physician.App.Extensions;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    public class AssessmentService : IAssessmentService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IOasisCDataProvider oasisDataProvider;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPhysicianRepository physicianRepository;

        public AssessmentService(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(oasisDataProvider, "dataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.oasisDataProvider = oasisDataProvider;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region IAssessmentService Members

        public VisitNoteViewData GetVisitNotePrint(Guid agencyId, Guid episodeId, Guid patientId, Guid orderId) {
            var note = new VisitNoteViewData();
            note.Agency = agencyRepository.GetWithBranches(agencyId);
            var patientvisitNote = patientRepository.GetVisitNote(agencyId, episodeId, patientId, orderId);
            if (patientvisitNote != null) {
                note.SignatureText = patientvisitNote.SignatureText;
                note.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                note.PhysicianSignatureText = patientvisitNote.PhysicianSignatureText;
                note.PhysicianSignatureDate = patientvisitNote.PhysicianSignatureDate;
                note.Questions = patientvisitNote.ToDictionary();
                var scheduledEvent = patientRepository.GetSchedule(agencyId, episodeId, patientId, orderId);
                if (scheduledEvent != null)
                {
                    note.DisciplineTask = scheduledEvent.DisciplineTask;
                }
                var selectedEpisodeId = note.Questions != null && note.Questions.ContainsKey("SelectedEpisodeId") && note.Questions["SelectedEpisodeId"].Answer.IsNotNullOrEmpty() ? note.Questions["SelectedEpisodeId"].Answer.ToGuid() : Guid.Empty;
                if (!selectedEpisodeId.IsEmpty()) {
                    var episode = patientRepository.GetEpisodeById(agencyId, selectedEpisodeId, patientId);
                    if (episode != null) {
                        note.EndDate = episode.EndDate;
                        note.StartDate = episode.StartDate;
                    }
                } else {
                    var episode = patientRepository.GetEpisodeById(agencyId, episodeId, patientId);
                    if (episode != null) {
                        note.EndDate = episode.EndDate;
                        note.StartDate = episode.StartDate;
                        var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == orderId);
                        if (evnt != null) note.VisitDate = evnt.VisitDate.IsNotNullOrEmpty() && evnt.VisitDate.IsValidDate() ? evnt.VisitDate : evnt.EventDate;
                    }
                }
                note.PatientId = patientvisitNote.PatientId;
                note.EpisodeId = patientvisitNote.EpisodeId;
                note.EventId = patientvisitNote.Id;
                note.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                note.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                note.Version = patientvisitNote.Version;
            }
            else note.Questions = new Dictionary<string, NotesQuestion>();
            var patient = patientRepository.Get(patientId, agencyId);
            note.Patient = patient;
            if (patient != null) {
                //if (note.Questions != null && note.Questions.ContainsKey("Physician") && note.Questions["Physician"].Answer.IsNotNullOrEmpty() && note.Questions["Physician"].Answer.IsGuid()) {
                //    var physician = physicianRepository.Get(note.Questions["Physician"].Answer.ToGuid(), agencyId);
                //    if (physician != null) {
                //        note.PhysicianId = physician.Id;
                //        note.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                //    }
                //} else {
                //    if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0) {
                //        var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                //        if (physician != null) {
                //            note.PhysicianId = physician.Id;
                //            note.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                //        }
                //    }
                //}
                AgencyPhysician physician = patientvisitNote.PhysicianData.ToObject<AgencyPhysician>();
                if (physician != null)
                {
                    note.PhysicianId = physician.Id;
                    note.PhysicianDisplayName = physician.DisplayName;
                }
                else if (!patientvisitNote.PhysicianId.IsEmpty())
                {
                    physician = physicianRepository.Get(patientvisitNote.PhysicianId, agencyId);
                    if (physician != null)
                    {
                        note.PhysicianDisplayName = physician.DisplayName;
                    }
                }
                if (note.Type != null && (note.Type == DisciplineTasks.PTPlanOfCare.ToString() || note.Type == DisciplineTasks.OTPlanOfCare.ToString() || note.Type == DisciplineTasks.STPlanOfCare.ToString()
                    || note.Type == DisciplineTasks.SNPsychAssessment.ToString()))
                {
                    note.Questions.Add("OrderNumber", new NotesQuestion { Name = "OrderNumber", Answer = patientvisitNote.OrderNumber.ToString() });
                }
            }
            return note;
        }

        public Assessment GetEpisodeAssessment(Guid agencyId, Guid episodeId, Guid patientId, DateTime eventDate) {
            Assessment assessment = null;
            var episode = patientRepository.GetEpisode(agencyId, episodeId, patientId);
            if (episode != null && episode.IsActive == true && episode.IsDischarged == false && episode.Schedule.IsNotNullOrEmpty()) {
                var assessmentEvent = episode.Schedule.ToObject<List<ScheduleEvent>>()
                    .Where(e => e.IsDeprecated == false && e.IsMissedVisit == false
                       && e.EventDate.IsValidDate() && (e.EventDate.ToDateTime().Date <= eventDate.Date)
                       && e.EventDate.ToDateTime().Date <= episode.EndDate.AddDays(-5).Date
                       && (e.IsStartofCareAssessment() || e.IsResumptionofCareAssessment()))
                    .OrderByDescending(e => e.EventDateSortable).FirstOrDefault();
                if (assessmentEvent != null) assessment = oasisDataProvider.OasisAssessmentRepository.Get(assessmentEvent.EventId, assessmentEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), agencyId);
                else if (episode.HasPrevious && episode.PreviousEpisode != null && episode.PreviousEpisode.IsActive == true && episode.PreviousEpisode.IsDischarged == false && episode.PreviousEpisode.Schedule.IsNotNullOrEmpty()) {
                    var previousEpisodeRecertEvent = episode.PreviousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                        .Where(e => e.IsDeprecated == false && e.IsMissedVisit == false
                           && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= episode.PreviousEpisode.EndDate.AddDays(-5).Date
                           && e.EventDate.ToDateTime().Date <= episode.PreviousEpisode.EndDate.Date && e.IsRecertificationAssessment())
                        .OrderByDescending(e => e.EventDateSortable).FirstOrDefault();
                    if (previousEpisodeRecertEvent != null) assessment = oasisDataProvider.OasisAssessmentRepository.Get(previousEpisodeRecertEvent.EventId, previousEpisodeRecertEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), agencyId);
                    else {
                        var previousEpisodeRocEvent = episode.PreviousEpisode.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(e => e.IsDeprecated == false && e.IsMissedVisit == false
                               && e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date >= episode.PreviousEpisode.EndDate.AddDays(-5).Date
                               && e.EventDate.ToDateTime().Date <= episode.PreviousEpisode.EndDate.Date && e.IsResumptionofCareAssessment())
                            .OrderByDescending(e => e.EventDateSortable).FirstOrDefault();
                        if (previousEpisodeRocEvent != null) assessment = oasisDataProvider.OasisAssessmentRepository.Get(previousEpisodeRocEvent.EventId, previousEpisodeRocEvent.DisciplineTask.ToName<DisciplineTasks>(DisciplineTasks.NoDiscipline), agencyId);
                    }
                }
            }
            return assessment;
        }

        private Assessment GetAssessment(Guid assessmentId, string assessmentType, Guid agencyId)
        {
            return oasisDataProvider.OasisAssessmentRepository.Get(assessmentId, assessmentType, agencyId);
        }

        public PlanofCare GetPlanofCare(Guid agencyId, Guid episodeId, Guid patientId, Guid planofcareId)
        {
            return oasisDataProvider.PlanofCareRepository.Get(agencyId, episodeId, patientId, planofcareId);
        }

        public IDictionary<string, Question> GetAllergies(Guid assessmentId, string AssessmentType, Guid agencyId)
        {
            var allergies = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId, AssessmentType, agencyId);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();
                if (questions.ContainsKey("485Allergies") && questions["485Allergies"] != null)
                {
                    allergies.Add("485Allergies", questions["485Allergies"]);
                }
                if (questions.ContainsKey("485AllergiesDescription") && questions["485AllergiesDescription"] != null)
                {
                    allergies.Add("485AllergiesDescription", questions["485AllergiesDescription"]);
                }
            }
            return allergies;
        }

        public IDictionary<string, Question> GetDiagnoses(Guid assessmentId, string AssessmentType, Guid agencyId)
        {
            var diagnosis = new Dictionary<string, Question>();
            var assessment = this.GetAssessment(assessmentId, AssessmentType, agencyId);
            if (assessment != null)
            {
                var questions = assessment.ToDictionary();

                if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                {
                    diagnosis.Add("M1020PrimaryDiagnosis", questions["M1020PrimaryDiagnosis"]);
                }
                if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
                {
                    diagnosis.Add("M1020ICD9M", questions["M1020ICD9M"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis1", questions["M1022PrimaryDiagnosis1"]);
                }

                if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
                {
                    diagnosis.Add("M1022ICD9M1", questions["M1022ICD9M1"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis2", questions["M1022PrimaryDiagnosis2"]);
                }

                if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
                {
                    diagnosis.Add("M1022ICD9M2", questions["M1022ICD9M2"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis3", questions["M1022PrimaryDiagnosis3"]);
                }

                if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
                {
                    diagnosis.Add("M1022ICD9M3", questions["M1022ICD9M3"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis4", questions["M1022PrimaryDiagnosis4"]);
                }

                if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
                {
                    diagnosis.Add("M1022ICD9M4", questions["M1022ICD9M4"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis5", questions["M1022PrimaryDiagnosis5"]);
                }

                if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
                {
                    diagnosis.Add("M1022ICD9M5", questions["M1022ICD9M5"]);
                }

            }
            return diagnosis;
        }

        public bool UpdatePlanofCare(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, string actionType, string reason)
        {
            var result = false;
            var shouldUpdateEpisode = false;

            var userEvent = new UserEvent();
            var scheduleEvent = new ScheduleEvent();
            PlanofCare planofCare = null;

            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                scheduleEvent = patientRepository.GetSchedule(agencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    userEvent = userRepository.GetEvent(agencyId, scheduleEvent.UserId, patientId, eventId);
                }

                var planofCareStandAlone = oasisDataProvider.PlanofCareRepository.GetStandAlone(agencyId, episodeId, patientId, eventId);
                if (planofCareStandAlone != null)
                {
                    if (actionType == "Approve")
                    {
                        planofCareStandAlone.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                        planofCareStandAlone.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                        planofCareStandAlone.PhysicianSignatureDate = DateTime.Now;
                        planofCareStandAlone.ReceivedDate = DateTime.Now;
                        if (oasisDataProvider.PlanofCareRepository.UpdateStandAlone(planofCareStandAlone))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                    else if (actionType == "Return")
                    {
                        planofCareStandAlone.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                        if (oasisDataProvider.PlanofCareRepository.UpdateStandAlone(planofCareStandAlone))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                            }
                        }
                    }
                }
                else
                {
                    planofCare = oasisDataProvider.PlanofCareRepository.Get(agencyId, episodeId, patientId, eventId);
                    if (planofCare != null)
                    {
                        if (actionType == "Approve")
                        {
                            planofCare.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                            planofCare.PhysicianSignatureText = string.Format("Electronically Signed by: {0}", Current.DisplayName);
                            planofCare.PhysicianSignatureDate = DateTime.Now;
                            planofCare.ReceivedDate = DateTime.Now;
                            if (oasisDataProvider.PlanofCareRepository.Update(planofCare))
                            {
                                if (scheduleEvent != null)
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                    scheduleEvent.ReturnReason = string.Empty;
                                    if (userEvent != null)
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                                        userEvent.ReturnReason = string.Empty;
                                    }
                                    shouldUpdateEpisode = true;
                                }
                            }
                        }
                        else if (actionType == "Return")
                        {
                            planofCare.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                            if (oasisDataProvider.PlanofCareRepository.Update(planofCare))
                            {
                                if (scheduleEvent != null)
                                {
                                    scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                    scheduleEvent.ReturnReason = reason;
                                    if (userEvent != null)
                                    {
                                        userEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview).ToString();
                                        userEvent.ReturnReason = reason;
                                    }
                                    shouldUpdateEpisode = true;
                                }
                            }
                        }
                    }
                }

                if (shouldUpdateEpisode)
                {
                    if (patientRepository.UpdateEpisode(agencyId, scheduleEvent))
                    {
                        if (userEvent != null)
                        {
                            if (userRepository.UpdateEvent(agencyId, userEvent))
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            userRepository.AddUserEvent(agencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                            result = true;
                        }
                    }
                }
            }

            return result;
        }

        #endregion
    }
}
