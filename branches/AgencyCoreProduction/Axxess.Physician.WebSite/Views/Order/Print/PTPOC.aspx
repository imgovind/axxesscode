﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null&&Model.Agency!=null) location = Model.Agency.GetMainOffice(); %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPT Plan Of Care" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("EpsPeriod") ? data["EpsPeriod"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EOrder Number:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("OrderNumber") %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EPT Plan Of Care%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
</script>
<% if (data.AnswerOrEmptyString("IsDMEApply").Equals("1"))
   { %>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "DME");
</script>   
<%}
   else
   { %>
<script type="text/javascript">
     printview.addsection(
        printview.col(6,
            printview.span("Available:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMEAvailable").Clean() %>",0,1) +
            printview.span("Needs:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMENeeds").Clean() %>",0,1) +
            printview.span("Suggestion:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMESuggestion").Clean() %>",0,1)),
            "DME");
</script>
<%} %>
<%if (Model.DisciplineTask==44 || Model.DisciplineTask==137)
              {if(data.AnswerOrEmptyString("IsMedicalApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
                printview.checkbox("N/A", true),
                "Medical Diagnosis");
</script>
              <%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("PT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
</script>
<%}
  }
  else if (Model.DisciplineTask==47 || Model.DisciplineTask==135)
              { %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("OT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Diagnosis");
</script>
<%}
  else if (Model.DisciplineTask == 51 || Model.DisciplineTask == 136)
  {
      if (data.AnswerOrEmptyString("IsMedicalApply").Equals("1"))
      { %>
<script type="text/javascript">
    printview.addsection(
                printview.checkbox("N/A", true),
                "Medical Diagnosis");
</script>
              <%}
      else
      { %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("ST Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
</script>
<%}
  } %>

<%  var noteDiscipline = Model.Type; %>
<%  string[] genericTreatmentPlan = data.AnswerArray("POCGenericTreatmentPlan"); %>
<% if(data.ContainsKey("IsTreatmentApply") && data.AnswerOrEmptyString("IsTreatmentApply").Equals("1")){%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Treatment Plan");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.span("PT Frequency & Duration:<%=data.AnswerOrEmptyString("POCGenericFrequencyAndDuration").Clean() %>")+
        printview.col(3,
            printview.checkbox("Therapeutic exercise",<%= genericTreatmentPlan.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Bed Mobility Training",<%= genericTreatmentPlan.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Transfer Training",<%= genericTreatmentPlan.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Balance Training",<%= genericTreatmentPlan.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Gait Training",<%= genericTreatmentPlan.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Neuromuscular re-education",<%= genericTreatmentPlan.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Functional mobility training",<%= genericTreatmentPlan.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective use of adaptive/assist device",<%= genericTreatmentPlan.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Teach safe stair climbing skills",<%= genericTreatmentPlan.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Teach fall prevention/safety",<%= genericTreatmentPlan.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("Establish/upgrade home exercise program",<%= genericTreatmentPlan.Contains("11").ToString().ToLower() %>) +
            printview.checkbox("Pt/caregiver education/training",<%= genericTreatmentPlan.Contains("12").ToString().ToLower() %>) +
            printview.checkbox("Proprioceptive training",<%= genericTreatmentPlan.Contains("13").ToString().ToLower() %>) +
            printview.checkbox("Postural control training",<%= genericTreatmentPlan.Contains("14").ToString().ToLower() %>) +
            printview.checkbox("Teach energy conservation techniques",<%= genericTreatmentPlan.Contains("15").ToString().ToLower() %>) +
            printview.checkbox("Relaxation technique",<%= genericTreatmentPlan.Contains("16").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective breathing technique",<%= genericTreatmentPlan.Contains("17").ToString().ToLower() %>) +
            printview.checkbox("Teach hip precaution",<%= genericTreatmentPlan.Contains("18").ToString().ToLower() %>) +
            printview.checkbox("Electrical stimulation",<%= genericTreatmentPlan.Contains("19").ToString().ToLower() %>) +
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan19BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan19Duration").Clean() %>",0,1)+
            printview.checkbox("Ultrasound",<%= genericTreatmentPlan.Contains("20").ToString().ToLower() %>) +
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan20BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan20Duration").Clean() %>",0,1)+
            printview.checkbox("TENS",<%= genericTreatmentPlan.Contains("21").ToString().ToLower() %>) +
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan21BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan21Duration").Clean() %>",0,1)+
            printview.checkbox("Prosthetic training",<%= genericTreatmentPlan.Contains("22").ToString().ToLower() %>) +
            printview.checkbox("Pulse oximetry PRN",<%= genericTreatmentPlan.Contains("23").ToString().ToLower() %>) +
            printview.span("&#160;"))+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("POCGenericTreatmentPlanOther").Clean() %>",0,1),
            "Treatment Plan");
</script>
<%} %>
 <% if (data.AnswerOrEmptyString("IsModalitiesApply").Equals("1"))
     {%>
  <script type="text/javascript">
      printview.addsection(
        printview.checkbox("N/A", true),
        "Modalities");
  </script>
  <%}
     else
     { %>
  <script type="text/javascript">
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericModalitiesComment").Clean() %>",0,3),"Modalities");
  </script>
  <%} %>
<%  var type=Model.Type; %>
<%  string[] genericPTGoals = data.AnswerArray("POCGenericPTGoals"); %>
<% if(data.ContainsKey("IsGoalsApply") && data.AnswerOrEmptyString("IsGoalsApply").Equals("1")){%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "PT Goals");
</script>    
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
    <%if(genericPTGoals.Contains("1")){ %>
        printview.checkbox("Patient will demonstrate ability to perform home exercise program within <%= data.AnswerOrEmptyString("POCGenericPTGoals1Weeks").Clean() %> weeks",<%= genericPTGoals.Contains("1").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("2")){ %>
        printview.checkbox("Demonstrate effective pain management utilizing <%= data.AnswerOrEmptyString("POCGenericPTGoals2Utilize").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericPTGoals2Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("2").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("3")){ %>
        printview.checkbox("Patient will be able to perform sit to supine/supine to sit with <%= data.AnswerOrEmptyString("POCGenericPTGoals3Assist").Clean() %> assist with safe and effective technique within <%= data.AnswerOrEmptyString("POCGenericPTGoals3Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("3").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("4")){ %>
        printview.checkbox("Improve bed mobility to independent within <%= data.AnswerOrEmptyString("POCGenericPTGoals4Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("4").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("5")){ %>
        printview.checkbox("Improve transfers to <%= data.AnswerOrEmptyString("POCGenericPTGoals5Assist").Clean() %> assist using <%= data.AnswerOrEmptyString("POCGenericPTGoals5Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericPTGoals5Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("5").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("6")){ %>
        printview.checkbox("Independent with safe transfer skills within <%= data.AnswerOrEmptyString("POCGenericPTGoals6Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("6").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("7")){ %>
        printview.checkbox("Patient will improve <%= data.AnswerOrEmptyString("POCGenericPTGoals7Skill").Clean() %> transfer skill to <%= data.AnswerOrEmptyString("POCGenericPTGoals7Assist").Clean() %> assist using <%= data.AnswerOrEmptyString("POCGenericPTGoals7Within").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericPTGoals7Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("7").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("8")){ %>
        printview.checkbox("Patient to be independent with safety issues in <%= data.AnswerOrEmptyString("POCGenericPTGoals8Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("8").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("9")){ %>
        printview.checkbox("Patient will be able to negotiate stairs with <%= data.AnswerOrEmptyString("POCGenericPTGoals9Assist").Clean() %> device with <%= data.AnswerOrEmptyString("POCGenericPTGoals9Within").Clean() %> assist within <%= data.AnswerOrEmptyString("POCGenericPTGoals9Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("9").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("10")){ %>
        printview.checkbox("Patient will be able to ambulate using <%= data.AnswerOrEmptyString("POCGenericPTGoals10Device").Clean() %> device at least <%= data.AnswerOrEmptyString("POCGenericPTGoals10Feet").Clean() %> feet with <%= data.AnswerOrEmptyString("POCGenericPTGoals10Within").Clean() %> assist with safe and effective gait pattern",<%= genericPTGoals.Contains("10").ToString().ToLower() %>)+ 
        printview.span("  on even/uneven surface within <%= data.AnswerOrEmptyString("POCGenericPTGoals10Weeks").Clean() %> weeks.",0,1)+ 
            
        <%} %>
        <%if(genericPTGoals.Contains("11")){ %>
        printview.checkbox("Independent with ambulation without device indoor/outdoor at least <%= data.AnswerOrEmptyString("POCGenericPTGoals11Feet").Clean() %> feet to allow community access within <%= data.AnswerOrEmptyString("POCGenericPTGoals11Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("11").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("12")){ %>
        printview.checkbox("Patient will be able to ambulate using <%= data.AnswerOrEmptyString("POCGenericPTGoals12Device").Clean() %> device at least <%= data.AnswerOrEmptyString("POCGenericPTGoals12Feet").Clean() %> feet on <%= data.AnswerOrEmptyString("POCGenericPTGoals12Within").Clean() %> surface to be able to perform ADL within <%= data.AnswerOrEmptyString("POCGenericPTGoals12Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("12").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("13")){ %>
        printview.checkbox("Improve strength of <%= data.AnswerOrEmptyString("POCGenericPTGoals13Of").Clean() %> to <%= data.AnswerOrEmptyString("POCGenericPTGoals13To").Clean() %> grade to improve <%= data.AnswerOrEmptyString("POCGenericPTGoals13Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericPTGoals13Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("13").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("14")){ %>
        printview.checkbox("Increase muscle strength of <%= data.AnswerOrEmptyString("POCGenericPTGoals14Of").Clean() %> to <%= data.AnswerOrEmptyString("POCGenericPTGoals14To").Clean() %> grade to improve gait pattern/stability and decrease fall risk within <%= data.AnswerOrEmptyString("POCGenericPTGoals14Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("14").ToString().ToLower() %>)+
        <%} %>
        <%if(genericPTGoals.Contains("15")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("POCGenericPTGoals15To").Clean() %> to improve postural control and balance within <%= data.AnswerOrEmptyString("POCGenericPTGoals15Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("15").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("16")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("POCGenericPTGoals16To").Clean() %> to improve postural control during bed mobility and transfer within <%= data.AnswerOrEmptyString("POCGenericPTGoals16Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("16").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("17")){ %>
        printview.checkbox("Patient will increase ROM of <%= data.AnswerOrEmptyString("POCGenericPTGoals17Joint").Clean() %> joint to <%= data.AnswerOrEmptyString("POCGenericPTGoals17Degree").Clean() %> degree of <%= data.AnswerOrEmptyString("POCGenericPTGoals17In").Clean() %> in <%= data.AnswerOrEmptyString("POCGenericPTGoals17Weeks").Clean() %> weeks to <%= data.AnswerOrEmptyString("POCGenericPTGoals17IncreaseROMTo").Clean() %>.",<%= genericPTGoals.Contains("17").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("18")){ %>
        printview.checkbox("Demonstrate safe and effective use of prosthesis/brace/splint within <%= data.AnswerOrEmptyString("POCGenericPTGoals18Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("18").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("19")){ %>
        printview.checkbox("Demonstrate safe and effective use of <%= data.AnswerOrEmptyString("POCGenericPTGoals19Within").Clean() %> DME within <%= data.AnswerOrEmptyString("POCGenericPTGoals19Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("19").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("20")){ %>
        printview.checkbox("Patient will have increase in Tinetti Performance Oriented Mobility Assessment score to <%= data.AnswerOrEmptyString("POCGenericPTGoals20Within").Clean() %> over 28 to reduce",<%= genericPTGoals.Contains("20").ToString().ToLower() %>)+ 
        printview.span("  fall risk within <%= data.AnswerOrEmptyString("POCGenericPTGoals20Weeks").Clean() %> weeks.",0,1)+
        <%} %>
        <%if(genericPTGoals.Contains("21")){ %>
        printview.checkbox("Patient will have improved <%= data.AnswerOrEmptyString("POCGenericPTGoals21Improve").Clean() %> standardized test score to improve <%= data.AnswerOrEmptyString("POCGenericPTGoals21Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericPTGoals21Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("21").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericPTGoals.Contains("22")){ %>
        printview.checkbox("Patient will have increase in Timed Up and Go score to <%= data.AnswerOrEmptyString("POCGenericPTGoals22Seconds").Clean() %> seconds to reduce fall risk and improve mobility within <%= data.AnswerOrEmptyString("POCGenericPTGoals22Weeks").Clean() %> weeks.",<%= genericPTGoals.Contains("22").ToString().ToLower() %>)+
        <%} %>
        <%if(type.Contains("PT")){ %>
        printview.span("Additional goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericPTGoalsComments").Clean() %>",0,2)+
        printview.span("PT Short Term Goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericPTShortTermGoalsComments").Clean() %>",0,2)+
        printview.span("PT Long Term Goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericPTLongTermGoalsComments").Clean() %>",0,2)+
        printview.col(2,
            printview.checkbox("Patient",<%= data.AnswerOrEmptyString("POCPTGoalsDesired").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver desired outcomes:<%=data.AnswerOrEmptyString("POCPTGoalsDesiredOutcomes").Clean() %>",<%= data.AnswerOrEmptyString("POCPTGoalsDesired").Contains("1").ToString().ToLower() %>)),
        "PT Goals");
        <%}else if(type.Contains("OT")){ %>
        printview.span("Additional goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericPTGoalsComments").Clean() %>",0,2)+
        printview.span("Rehab Potential:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericRehabPotential").Clean() %>",0,2),
        "OT Goals");
        <%} %>
</script>
<%} %>
<% if (data.AnswerOrEmptyString("IsRecommendationApply").Equals("1"))
   {%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Other Discipline Recommendation");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.checkbox("OT", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("MSW", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("ST", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Podiatrist", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.span("Other", true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendationOther").Clean() %>",0,1)) +
            printview.span("Reason", true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendationReason").Clean() %>",0,1) ,
        "Other Discipline Recommendation");
 </script>       
 <%} %>
 <% if (data.AnswerOrEmptyString("IsRehabApply").Equals("1"))
   {%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Rehab");
</script>
<%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.span("Rehab Diagnosis:<%=data.AnswerOrEmptyString("POCGenericDisciplineRehabDiagnosis").Clean() %>",true)+
        printview.span("Rehab Potential:",true)+
        printview.col(3,
            printview.checkbox("Good",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("0").ToString().ToLower() %>)+
            printview.checkbox("Fair",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Poor",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("2").ToString().ToLower() %>))+
        printview.span("<%=data.AnswerOrEmptyString("POCOtherRehabPotential").Clean() %>",0,1),
        "Rehab");
            
</script>
 <%} %>
 <% if (data.AnswerOrEmptyString("IsDCPlanApply").Equals("1"))
     {%>
  <script type="text/javascript">
      printview.addsection(
        printview.checkbox("N/A", true),
        "Discharge Plan");
  </script>
  <%}
     else
     { %>
  <script type="text/javascript">
    printview.addsection(
        printview.span("Patient to be discharged to the care of:",true)+
        printview.col(3,
            printview.checkbox("Physician",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("2").ToString().ToLower() %>)+
            printview.checkbox("Selfcare",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("3").ToString().ToLower() %>))+
        printview.span("Discharge Plans:",true)+
        printview.col(2,
            printview.checkbox("Discharge when caregiver willing and able to manage all aspects of patient’s care",<%= data.AnswerOrEmptyString("POCDCPlanPlans").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Discharge when goals met.",<%= data.AnswerOrEmptyString("POCDCPlanPlans").Contains("2").ToString().ToLower() %>))+
        printview.span("<%= data.AnswerOrEmptyString("POCDCPlanAdditional").Clean() %>",0,3),"Discharge Plan");
  </script>
  <%} %>
 <% if (data.AnswerOrEmptyString("IsSkilledCareApply").Equals("1"))
    { %>
 <script type="text/javascript">
     printview.addsection(
        printview.checkbox("N/A", true),
        "Skilled Care Provided");
  </script>
  <%}
    else
    { %>
    <script type="text/javascript">
    printview.addsection(
        printview.span("Training Topics:<%= data.AnswerOrEmptyString("POCSkilledCareTrainingTopics").Clean() %>",0,3)+
        printview.span("Trained:",true)+
        printview.col(2,
            printview.checkbox("Patient",<%= data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("1").ToString().ToLower() %>)) +
        printview.span("Treatment Performed: <%= data.AnswerOrEmptyString("POCSkilledCareTreatmentPerformed").Clean() %>",0,3)+
        printview.span("Patient Response: <%= data.AnswerOrEmptyString("POCSkilledCarePatientResponse").Clean() %>",0,3),
    "Skilled Care Provided");
  </script>
  <%} %>
 <% if (data.AnswerOrEmptyString("IsCareApply").Equals("1"))
    {%>
  <script type="text/javascript">
      printview.addsubsection(
        printview.checkbox("N/A", true),
        "Care Coordination", 2);
  </script>
 <%}
    else
    { %>
  <script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericCareCoordination").Clean() %>",0,3),"Care Coordination");
  </script>
  <%} %>
  <% if (data.AnswerOrEmptyString("IsSafetyIssueApply").Equals("1"))
     {%>
  <script type="text/javascript">
      printview.addsubsection(
        printview.checkbox("N/A", true),
        "Safety Issues/Instruction/Education");
  </script>
  <%}
     else
     { %>
  <script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericSafetyIssue").Clean() %>",0,3),"Safety Issues/Instruction/Education");
  </script>
  <%} %>
 <script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.checkbox("Patient ",<%= data.AnswerOrEmptyString("POCNotificationUnderstands").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCNotificationUnderstands").Contains("1").ToString().ToLower() %>)) +
        printview.span("understands diagnosis/prognosis and agrees with Goals/Time frame and Plan of Care (POC)",true)+
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("POCNotificationPeopleAnswer").Contains("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("POCNotificationPeopleAnswer").Contains("0").ToString().ToLower() %>))+
        printview.checkbox("Physician notified and agrees with POC, frequency and duration. Comments (if any):<%=data.AnswerOrEmptyString("POCNotificationPhysicianComment").Clean() %>",<%= data.AnswerOrEmptyString("POCNotificationPhysician").Contains("0").ToString().ToLower() %>),
    "Notification");
</script>
  <script type="text/javascript">
      printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
</script>
</body>
</html>