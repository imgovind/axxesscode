﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %><%
var data = Model != null && Model.Questions!=null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name + " | " : "" %>SN Psychiatric Assessment<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body>
<%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
<script type="text/javascript">

    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%224%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22  colspan=%223%22%3E" +
        "SN Psychiatric Assessment" +
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%227%22%3E%3Cspan class=%22tricol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name: %3C/strong%3E" +
        "<%= Model.Patient.LastName.Clean()%>, <%= Model.Patient.FirstName.Clean()%> <%= Model.Patient.MiddleInitial.Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EDOB: %3C/strong%3E" +
        "<%= Model.Patient.DOBFormatted.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR: %3C/strong%3E" +
        "<%= Model.Patient.PatientIdNumber.Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Range: %3C/strong%3E" +
        "<%= string.Format("{0} - {1}", Model.StartDate.ToShortDateString().ToZeroFilled(), Model.EndDate.ToShortDateString().ToZeroFilled()).Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ELast Physician Visit Date: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("LastVisitDate") %>" +
         "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician: %3C/strong%3E" +
        "<%= data != null && Model.PhysicianDisplayName.IsNotNullOrEmpty() ? Model.PhysicianDisplayName.Clean() : string.Empty%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis").Clean()%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary DX: %3C/strong%3E" +
        "<%= data.AnswerOrEmptyString("PrimaryDiagnosis1").Clean()%>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.addsubsection(<%  var possibleAnswers = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% if (data.AnswerOrEmptyString("GenericIsAppearanceApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsAppearanceApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Appearance"
<%}else{ %>
printview.span("Facial Expressions",true) + 
printview.col(2,
    printview.span("Sad:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceFacial1", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Expressionless:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceFacial2", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Hostile:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceFacial3", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Worried:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceFacial4", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Avoids Gaze:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceFacial5", possibleAnswers) %>")) +
printview.span("Dress",true) + 
printview.col(2,
    printview.span("Meticulous:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceDress1", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Clothing, Hygiene Poor:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceDress2", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Eccentric:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceDress3", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Seductive:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceDress4", possibleAnswers) %>")) +
printview.col(2,
    printview.span("Exposed:",false) +
    printview.span("<%= data.AnswerForDropDown("GenericAppearanceDress5", possibleAnswers) %>")) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericAppearanceComment").Clean()%>"),
    "Appearance"
<% } %>
,4);
    printview.addsubsection(<%  var possibleAnswers1 = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions = new List<string>() { "Increased Amount", "Decreased Amount", "Agitation", "Tics", "Tremor", "Peculiar Posturing", "Unusual Gait", "Repetitive Acts" }; %>
<% if (data.AnswerOrEmptyString("GenericIsMotorActivityApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsMotorActivityApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Motor Activity"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericMotorActivity" + count, possibleAnswers1)%>")) +
    <% count++; %>
    <% } %> 
    printview.span("Comments:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericMotorActivityComment").Clean()%>"),
    "Motor Activity"
<% } %>
);
    printview.addsubsection(<%  var possibleAnswers2 = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions1 = new List<string>() { "Excessive Amount", "Reduced Amount", "Speech", "Slowed", "Loud", "Soft", "Mute", "Slurred", "Stuttering" }; %>
<% if (data.AnswerOrEmptyString("GenericIsSpeechApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsSpeechApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Speech"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions1) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericSpeech" + count, possibleAnswers2) %>")) +
    <% count++; %>
    <% } %> 
    printview.span("Comments:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericSpeechComment").Clean()%>"),
    "Speech"
<% } %>
);
    printview.addsubsection(<%  var possibleAnswers3 = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions2 = new List<string>() { "Blocking", "Circumstantial", "Tangential", "Perseveration", "Flight of Ideas", "Loose Association", "Indecisive" }; %>
<% if (data.AnswerOrEmptyString("GenericIsFlowOfThoughtApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsFlowOfThoughtApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Flow of Thought"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions2) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericFlowOfThought" + count, possibleAnswers3) %>")) +
    <% count++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericFlowOfThoughtComment").Clean()%>"),
"Flow of Thought"
<% } %>);
    printview.addsubsection(<%  var possibleAnswers4 = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions3 = new List<string>() { "Anxious", "Inappropriate Affect", "Flat Affect", "Elevated Mood", "Depressed Mood", "Labile Mood" }; %>
<% if (data.AnswerOrEmptyString("GenericIsMoodAffectAssessmentApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsMoodAffectAssessmentApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Mood and Affect"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions3) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericMoodAffectAssessment" + count, possibleAnswers4) %>")) +
    <% count++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericMoodAffectAssessmentComment").Clean()%>"),
"Mood and Affect"
<% } %>
,4);
    printview.addsubsection(<%  var possibleAnswers5 = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var orientationQuestions = new List<string>() { "Time", "Place", "Person" }; %>
<% var memoryQuestions = new List<string>() { "Clouding of Consciousness", "Inability to Concentrate", "Amnesia", "Poor Recent Memory", "Poor Remote Memory", "Conflabulation" }; %>
<% if (data.AnswerOrEmptyString("GenericIsSensoriumApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsSensoriumApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Sensorium"
<%}else{ %>
    printview.span("Orientation Impaired",true) + 
    <% int orientationCount = 1; %>
    <% foreach(string question in orientationQuestions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericSensoriumOrientation" + orientationCount, possibleAnswers5)%>")) +
    <% orientationCount++; %>
    <% } %> 
    printview.span("Memory",true) + 
    <% int memoryCount = 1; %>
    <% foreach(string question in memoryQuestions) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericSensoriumMemory" + memoryCount, possibleAnswers5)%>")) +
    <% memoryCount++; %>
    <% } %> 
    printview.span("Comments:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericSensoriumComment").Clean()%>"),
    "Sensorium"
<% } %>);
    printview.addsubsection(<% var possibleAnswers6 = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions4 = new List<string>() { "Above Normal", "Below Normal", "Paucity of Knowledge", "Vocabulary Poor", "Serial Sevens Done Poorly", "Poor Abstraction" }; %>
<% if (data.AnswerOrEmptyString("GenericIsIntellectApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsIntellectApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Intellect"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions4) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericIntellect" + count, possibleAnswers6) %>")) +
    <% count++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericIntellectComment").Clean()%>"),
"Intellect"
<% } %>);
    printview.addsubsection(<%  var possibleAnswers7 = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions5 = new List<string>() { "Poor Insight", "Poor Judgment", "Unrealistic Regarding Degree of Illness", "Doesn't Know Why He Is Here", "Unmotivated for Treatment", "Unrealistic Regarding Goals" }; %>
<% if (data.AnswerOrEmptyString("GenericIsInsightAndJudgmentApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsInsightAndJudgmentApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Insight and Judgment"
<%}else{ %>
<% int count = 1; %>
    <% foreach(string question in questions5) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericInsightAndJudgment" + count, possibleAnswers7) %>")) +
    <% count++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericInsightAndJudgmentComment").Clean()%>"),
"Insight and Judgment"
<% } %>);
    printview.addsubsection(<%  var possibleAnswers8 = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions6 = new List<string>() { "Angry Outbursts", "Irritable", "Impulsive", "Hostile", "Silly", "Sensitive", "Apathetic", "Withdrawn", "Evasive", "Passive" }; %>
<% var questions7 = new List<string>() { "Aggressive", "Naive", "Overly Dramatic", "Manipulative", "Dependent", "Uncooperative", "Demanding", "Negativistic", "Callous", "Mood Swings" }; %>
<% if (data.AnswerOrEmptyString("GenericIsInterviewBehaviorApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsInterviewBehaviorApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true), "Interview Behavior"
<%}else{ %>
   <%-- printview.col(2,--%>
    <% int count = 1; %>
    <% foreach(string question in questions6) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericInterviewBehavior" + count, possibleAnswers8)%>")) +
    <% count++; %>
    <% } %> 
    
    <% int memoryCount = 1; %>
    <% foreach (string question in questions7)
       { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericInterviewBehavior" + count, possibleAnswers8)%>")) +
    <% count++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericInterviewBehaviorComment").Clean()%>"),
"Interview Behavior"
<% } %>, 2);
    printview.addspliltablesubsection(<%  var possibleAnswers9 = new Dictionary<string, string>(){ {"0", ""}, {"1", "Mild"}, {"2", "Moderate"}, {"3", "Severe"} }; %>
<% var questions8 = new List<string>() { "Suicidal Thoughts", "Suicidal Plans", "Assaultive Ideas", "Homicidal Thoughts",
       "Homicidal Plans", "Antisocial Attitudes", "Suspiciousness", "Poverty of Content", "Phobias", "Obsessions",
       "Compulsions", "Feelings of Unreality", "Feels Persecuted", "Thoughts of Running Away", "Somatic Complaints",
       "Ideas of Guilt", "Ideas of Hopelessness" }; %>
<% var questions12 = new List<string>() { "Ideas of Worthlessness", "Excessive Religiosity", "Sexual Preoccupation", "Blames Others" }; %>
<% var questions13 = new List<string>() { "Auditory", "Visual", "Other" }; %>
<% var questions14 = new List<string>() { "Of Persection", "Of Grandeur", "Of Reference", "Of Influcence", "Somatic", "Are Systematized", "Other" }; %>
<% if (data.AnswerOrEmptyString("GenericIsContentOfThoughtApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsContentOfThoughtApplied").ToString() == "1") {%>
printview.checkbox("WNL for Patient",true),"Content of Thought"
<%}else{ %>

    <% int count = 1; %>
    <% foreach(string question in questions8) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericContentOfThought" + count, possibleAnswers9)%>")) +
    <% count++; %>
    <% if(count == questions.Count/2) { %> printview.split() +  <% } %>
    <% } %> 
    printview.split() + 
    <% foreach (string question in questions12)
       { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericContentOfThought" + count, possibleAnswers9)%>")) +
    <% count++; %>
    <% } %> 
        
    printview.span("Illusions", true) +
    printview.col(2,
        printview.span("Present:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericContentOfThoughtIllusions", possibleAnswers9)%>")) +
     printview.span("Hallucinations", true) +
    <% int hallucinationsCount = 1; %>
    <% foreach(string question in questions13) { %>
    printview.col(2,
        printview.span("<%= question %>:", false) +
        printview.span("<%= data.AnswerForDropDown("GenericContentOfThoughtHallucinations" + hallucinationsCount, possibleAnswers9)%>")) +
    <% hallucinationsCount++; %>
    <% } %> 
    printview.split() + 
    printview.span("Delusions", true) +
    <% int delusionsCount = 1; %>
    <% foreach(string question in questions14) { %>
    printview.col(2,
        printview.span("<%= question %>:",false) +
        printview.span("<%= data.AnswerForDropDown("GenericContentOfThoughtDelusions" + delusionsCount, possibleAnswers9)%>")) +
    <% delusionsCount++; %>
    <% } %> 
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericContentOfThoughtComment").Clean()%>"), 
"Content of Thought"
<% } %>);
    
    printview.addsection(<%  string[] interventions = data.AnswerArray("GenericPsychiatricInterventions"); %>
printview.col(2,
    <% if(interventions.Contains("1")) { %>
        printview.checkbox("Suicidal and safety precautions",<%= interventions.Contains("1").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("2")) { %>
        printview.checkbox("Relaxation, imagery and deep breathing exercises",<%= interventions.Contains("2").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("3")) { %>
        printview.checkbox("Problem solving, positive coping, decision making and stress management technique",<%= interventions.Contains("3").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("4")) { %>
        printview.checkbox("Recognition of s/sx complications of crisis and when to call MD",<%= interventions.Contains("4").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("5")) { %>
        printview.checkbox("Reality/congruent thinking techniques",<%= interventions.Contains("5").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("6")) { %>
        printview.checkbox("Anger management",<%= interventions.Contains("6").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("7")) { %>
        printview.checkbox("Emergency and crisis intervention",<%= interventions.Contains("7").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("8")) { %>
        printview.checkbox("Recognition of thoughts and verbally express painful ones",<%= interventions.Contains("8").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("9")) { %>
        printview.checkbox("Recognition of cardiovascular and neurological side effects of medication",<%= interventions.Contains("9").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("10")) { %>
        printview.checkbox("Ability to focus thoughts on feelings and verbally express painful ones",<%= interventions.Contains("10").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("11")) { %>
        printview.checkbox("Positive feedback to reality and realistic feelings",<%= interventions.Contains("11").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("12")) { %>
        printview.checkbox("Importance of supportive thearpy, reality testing, and positive feedback, validation and confrontation",<%= interventions.Contains("12").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("13")) { %>
        printview.checkbox("Relaxation and stress management techniques",<%= interventions.Contains("13").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("14")) { %>
        printview.checkbox("Relationship between feelings and behavior, impulse control behaviors",<%= interventions.Contains("14").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("15")) { %>
        printview.checkbox("Entry back into community and importance of interacting with others in the environment",<%= interventions.Contains("15").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("16")) { %>
        printview.checkbox("Grieving process and bereavement counseling",<%= interventions.Contains("16").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("17")) { %>
        printview.checkbox("Calming techniques for agitation",<%= interventions.Contains("17").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("18")) { %>
        printview.checkbox("Instructs time planning skills to prevent being overwhelmed",<%= interventions.Contains("18").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("19")) { %>
        printview.checkbox("Instruct recognition of exacerbation of illness, hallucinations, and delusions, inappropriate thought patterns and disorganization",<%= interventions.Contains("19").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("20")) { %>
        printview.checkbox("Instruct exploration of painful or anxious feelings and/or identifying ambivalent feelings",<%= interventions.Contains("20").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("21")) { %>
        printview.checkbox("Instruct importance of providing positive reinforcement for positive actions",<%= interventions.Contains("21").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("22")) { %>
        printview.checkbox("Instruct need for concrete realites and focus on thoughts",<%= interventions.Contains("22").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("23")) { %>
        printview.checkbox("Instruct need for supportive psychotherapist",<%= interventions.Contains("23").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("24")) { %>
        printview.checkbox("Instruct recognition of illness, mood swings, hyperactivity, delusions, euphoria's, grandiosity, depression and/or despondence",<%= interventions.Contains("24").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("25")) { %>
        printview.checkbox("Instruct in s/sx of lithium toxicity",<%= interventions.Contains("26").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("26")) { %>
        printview.checkbox("Instruct positive coping skills to deal with disease and symptoms",<%= interventions.Contains("26").ToString().ToLower() %>) +
    <% } %>
    <% if(interventions.Contains("27")) { %>
        printview.checkbox("Instruct recognition of illness, mood swings, hyperactivity, delusions, euphoria's, grandiosity, depression and/or despondence",<%= interventions.Contains("27").ToString().ToLower() %>)) +
    <% } else { %> "")+ <% } %>

printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericPsychiatricInterventionsComment").Clean()%>"),
"Interventions");
    printview.addsection(<%  string[] goals = data.AnswerArray("GenericGoals"); %>
<%  string[] isGoalsApplied = data.AnswerArray("GenericIsGoalsApplied"); %>
<% if(data.AnswerOrEmptyString("GenericIsGoalsApplied").IsNotNullOrEmpty() && data.AnswerOrEmptyString("GenericIsGoalsApplied").ToString()=="1"){ %>
printview.checkbox("N/A",true),"Goals"
<%}else{ %>
printview.col(2,
    <% if(goals.Contains("1")) { %>
        printview.checkbox("Make daily social contacts as evidenced by: <%= data.AnswerOrDefault("GenericGoals1Weeks", "__________").Clean()%> wks.",<%= goals.Contains("1").ToString().ToLower() %>) +
    <% } %>
    <% if(goals.Contains("2")) { %>
        printview.checkbox("Exhibit stable weight, nutrition, hydration status w/weight gain of <%= data.AnswerOrDefault("GenericGoals2Weight", "__________").Clean()%> lbs by <%= data.AnswerOrDefault("GenericGoals2Weeks", "__________").Clean()%> wks.",<%= goals.Contains("2").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("3")) { %>
        printview.checkbox("Improve interpersonal relationships by <%= data.AnswerOrDefault("GenericGoals3Weeks", "__________").Clean()%> wks.",<%= goals.Contains("3").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("4")) { %>
        printview.checkbox("Demonstrate coping strategies by <%= data.AnswerOrDefault("GenericGoals4Weeks", "__________").Clean()%> wks.",<%= goals.Contains("4").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("5")) { %>
        printview.checkbox("Decrease neurotic behavior by <%= data.AnswerOrDefault("GenericGoals5Weeks", "__________").Clean()%> wks.",<%= goals.Contains("5").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("6")) { %>
        printview.checkbox("Verbalize a decrease in depression by <%= data.AnswerOrDefault("GenericGoals6Weeks", "__________").Clean()%> wks.",<%= goals.Contains("6").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("7")) { %>
        printview.checkbox("Verbalize absense of suicidal ideation, intent and plan by <%= data.AnswerOrDefault("GenericGoals7Weeks", "__________").Clean()%> wks.",<%= goals.Contains("7").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("8")) { %>
        printview.checkbox("Will not harm self as evidenced by: <%= data.AnswerOrDefault("GenericGoals8Weeks", "__________").Clean()%> wks.",<%= goals.Contains("8").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("9")) { %>
        printview.checkbox("Verbalize absense of violent ideation by <%= data.AnswerOrDefault("GenericGoals9Weeks", "__________").Clean()%> wks.",<%= goals.Contains("9").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("10")) { %>
        printview.checkbox("Verbalize s/sx suicidality, crisis intervention, when to call physician/911 by  <%= data.AnswerOrDefault("GenericGoals10Weeks", "__________").Clean()%> wks.",<%= goals.Contains("10").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("11")) { %>
        printview.checkbox("Exhibit elevated mood as evidenced by:  <%= data.AnswerOrDefault("GenericGoals11Weeks", "__________").Clean()%> wks.",<%= goals.Contains("11").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("12")) { %>
        printview.checkbox("Demonstrate 2 coping skills as evidenced by: <%= data.AnswerOrDefault("GenericGoals12Weeks", "__________").Clean()%> wks.",<%= goals.Contains("12").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("13")) { %>
        printview.checkbox("Make daily social contacts as evidenced by: <%= data.AnswerOrDefault("GenericGoals13Weeks", "__________").Clean()%> wks.",<%= goals.Contains("13").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("14")) { %>
        printview.checkbox("Exhibit goal directed thoughts as evidence by: <%= data.AnswerOrDefault("GenericGoals14Weeks", "__________").Clean()%> wks.",<%= goals.Contains("14").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("15")) { %>
        printview.checkbox("Maintain stable weight, nutrition, hydration status w/weight gain of <%= data.AnswerOrDefault("GenericGoals15Weight", "__________").Clean()%> lbs by <%= data.AnswerOrDefault("GenericGoals15Weeks", "__________").Clean()%> wks.",<%= goals.Contains("15").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("16")) { %>
        printview.checkbox("Achieve sx control of CV & CP status w/meds & relaxation skills AEB by <%= data.AnswerOrDefault("GenericGoals16Weeks", "__________").Clean()%> wks.",<%= goals.Contains("16").ToString().ToLower() %>) +
    <% } %>
    <% if(goals.Contains("17")) { %>
        printview.checkbox("Achieve GI/GU managements as evidence by: <%= data.AnswerOrDefault("GenericGoals17Weeks", "__________").Clean()%> wks.",<%= goals.Contains("17").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("18")) { %>
        printview.checkbox("Verbalize bowel management as evidence by: <%= data.AnswerOrDefault("GenericGoals18Weeks", "__________").Clean()%> wks.",<%= goals.Contains("18").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("19")) { %>
        printview.checkbox("Verbalize and achieve symptom control of sleep disturbance of <%= data.AnswerOrDefault("GenericGoals19Noc", "__________").Clean()%> h/noc by <%= data.AnswerOrDefault("GenericGoals19Weeks", "__________").Clean()%> wks.",<%= goals.Contains("19").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("20")) { %>
        printview.checkbox("Exhibit control of anxiety w/med & relaxation skills AEB by <%= data.AnswerOrDefault("GenericGoals20Weeks", "__________").Clean()%> wks.",<%= goals.Contains("20").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("21")) { %>
        printview.checkbox("Exhibit control of thought disorder as evidenced by <%= data.AnswerOrDefault("GenericGoals21Weeks", "__________").Clean()%> wks.",<%= goals.Contains("21").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("22")) { %>
        printview.checkbox("Achieve mobility/safety management as evidenced by <%= data.AnswerOrDefault("GenericGoals22Weeks", "__________").Clean()%> wks.",<%= goals.Contains("22").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("23")) { %>
        printview.checkbox("Achieve maximum level of self-care as evidenced by <%= data.AnswerOrDefault("GenericGoals23Weeks", "__________").Clean()%> wks.",<%= goals.Contains("23").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("24")) { %>
        printview.checkbox("Verbalized medications, use schedule & side effecs and patient will take as ordered by <%= data.AnswerOrDefault("GenericGoals24Weeks", "__________").Clean()%> wks.",<%= goals.Contains("24").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("25")) { %>
        printview.checkbox("Verbalize adequate knowledge of disease process & know when to notify physician by <%= data.AnswerOrDefault("GenericGoals25Weeks", "__________").Clean()%> wks.",<%= goals.Contains("25").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("26")) { %>
        printview.checkbox("97 Verbalize goal-directed thoughts, really based orientation & congruent thinking by <%= data.AnswerOrDefault("GenericGoals26Weeks", "__________").Clean()%> wks.",<%= goals.Contains("26").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("27")) { %>
        printview.checkbox("Exhibit decreased hyperactivity & safe behaviors as evidence by <%= data.AnswerOrDefault("GenericGoals27Weeks", "__________").Clean()%> wks.",<%= goals.Contains("27").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("28")) { %>
        printview.checkbox("Refrain from boastful/delusional behaviors & from interrupting conversations by <%= data.AnswerOrDefault("GenericGoals28Weeks", "__________").Clean()%> wks.",<%= goals.Contains("28").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("29")) { %>
        printview.checkbox("Demonstrate positive coping mechanisms & verbalize 2 realistic goals by <%= data.AnswerOrDefault("GenericGoals29Weeks", "__________").Clean()%> wks.",<%= goals.Contains("29").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("30")) { %>
        printview.checkbox("Experience no untoward ECT complications by <%= data.AnswerOrDefault("GenericGoals30Weeks", "__________").Clean()%> wks.",<%= goals.Contains("30").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("31")) { %>
        printview.checkbox("Verbalize 2 management techniques of disease by <%= data.AnswerOrDefault("GenericGoals31Weeks", "__________").Clean()%> wks.",<%= goals.Contains("31").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("32")) { %>
        printview.checkbox("Verbalize 2 s/sx of illness by <%= data.AnswerOrDefault("GenericGoals32Weeks", "__________").Clean()%> wks.",<%= goals.Contains("32").ToString().ToLower() %>)+
    <% } %>
    <% if(goals.Contains("33")) { %>
        printview.checkbox("Other: <%= data.AnswerOrDefault("GenericGoalsOther", "__________").Clean()%>",<%= goals.Contains("33").ToString().ToLower() %>)) +
    <% } else { %> "")+ <% } %>
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericGoalsComment").Clean() %>"),
"Goals"
<%} %>);
    printview.addsection(printview.col(3,
    printview.checkbox("Fair",<%= data.AnswerOrEmptyString("GenericRehabPotential").Equals("0").ToString().ToLower()%>) + 
    printview.checkbox("Good",<%= data.AnswerOrEmptyString("GenericRehabPotential").Equals("1").ToString().ToLower()%>) + 
    printview.checkbox("Excellent",<%= data.AnswerOrEmptyString("GenericRehabPotential").Equals("2").ToString().ToLower()%>)),
"Rehab Potential");
    
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("GenericComments").Clean() %>", false), "Comments/Narrative");
    printview.addsection(<%  string[] homeBoundReason = data.AnswerArray("HomeBoundReason"); %>
printview.col(1,
    printview.checkbox("Exhibits considerable &#38; taxing effort to leave home",<%= homeBoundReason.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Requires the assistance of another to get up and moving safely",<%= homeBoundReason.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Severe Dyspnea",<%= homeBoundReason.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Unable to safely leave home unassisted",<%= homeBoundReason.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("Unsafe to leave home due to cognitive or psychiatric impairments",<%= homeBoundReason.Contains("6").ToString().ToLower() %>) +
    printview.checkbox("Unable to leave home due to medical restriction(s)",<%= homeBoundReason.Contains("7").ToString().ToLower() %>) +
    printview.checkbox("Other <%= data.AnswerOrEmptyString("OtherHomeBoundDetails")%>",<%= homeBoundReason.Contains("8").ToString().ToLower() %>)) +
printview.col(2,
    printview.span("Home Environment",true) +
    printview.span("<%= (data.AnswerOrEmptyString("HomeEnvironment").Equals("1") ? "No issues identified" : string.Empty) + (data.AnswerOrEmptyString("HomeEnvironment").Equals("2") ? "Lack of Finances" : string.Empty) + (data.AnswerOrEmptyString("HomeEnvironment").Equals("3") ? "Lack of CG/Family Support" : string.Empty) + (data.AnswerOrEmptyString("HomeEnvironment").Equals("4") ? "Poor Home Environment" : string.Empty) + (data.AnswerOrEmptyString("HomeEnvironment").Equals("5") ? "Cluttered/Soiled Living Conditions" : string.Empty) + (data.AnswerOrEmptyString("HomeEnvironment").Equals("6") ? "Other" : string.Empty) %>")),
"Homebound Status");
    
    printview.addsection(
        "%3Ctable class=%22fixed%22%3E" +
            "%3Ctbody%3E" +
                "%3Ctr%3E" +
                    "%3Ctd colspan=%223%22%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EClinician Signature%3C/strong%3E" +
                            "<%= Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E%3Ctd%3E" +
                        "%3Cspan%3E" +
                            "%3Cstrong%3EDate%3C/strong%3E" +
                            "<%= Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate : string.Empty %>" +
                        "%3C/span%3E" +
                    "%3C/td%3E" +
                "%3C/tr%3E" +
            "%3C/tbody%3E" +
        "%3C/table%3E");
    
    printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
</script>

</body>
</html>