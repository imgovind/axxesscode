﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<%  if (location == null) location = Model.Agency.GetMainOffice(); %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group
            .Add("print.css")
        .Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
    <%  Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.7.1.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>

    <script type="text/javascript">
    printview.cssclass = "largerfont";
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3E" +
        "<%= Model.TypeName %>" + 
        "%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EPatient Name:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : ""%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("VisitDate").Clean() %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("EpsPeriod") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("TimeIn")%>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("TimeOut") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Mileage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("AssociatedMileage") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ESurcharge:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("Surcharge") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EOrder Number:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data.AnswerOrEmptyString("OrderNumber") %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EPhysician:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.PhysicianDisplayName.Clean() %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        "<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EOT Plan Of Care%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
    </script>

    <% if (data.AnswerOrEmptyString("POCIsDMEApply").Equals("1"))
   { %>

<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "DME");
</script>

<%}
   else
   { %>

<script type="text/javascript">
     printview.addsection(
        printview.col(6,
            printview.span("Available:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMEAvailable").Clean() %>",0,1) +
            printview.span("Needs:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMENeeds").Clean() %>",0,1) +
            printview.span("Suggestion:",true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDMESuggestion").Clean() %>",0,1)),
            "DME");
</script>

<%} %>
    <%if (Model.DisciplineTask==44 || Model.DisciplineTask==137)
              {if(data.AnswerOrEmptyString("IsMedicalApply").Equals("1")){ %>
<script type="text/javascript">
    printview.addsection(
                printview.checkbox("N/A", true),
                "Medical Diagnosis");
</script>
              <%}else{ %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("PT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
</script>
<%}
  }
  else if (Model.DisciplineTask==47 || Model.DisciplineTask==135)
              { %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("OT Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Diagnosis");
</script>
<%}
  else if (Model.DisciplineTask == 51 || Model.DisciplineTask == 136)
              { %>
<script type="text/javascript">
    printview.addsection(
        printview.col(4,
        printview.span("Medical Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCMedicalDiagnosisDate").Clean() %>",0,1) +
        printview.span("ST Diagnosis:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericTherapyDiagnosis").Clean() %>",0,1) +
        printview.span("Onset:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCTherapyDiagnosisDate").Clean() %>",0,1)) +
        printview.span("Comment",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericMedicalDiagnosisComment").Clean() %>",0,1),
        "Medical Diagnosis");
</script>
<%} %>
    <%  var noteDiscipline = Model.Type; %>
<%  string[] genericTreatmentPlan = data.AnswerArray("POCGenericTreatmentPlan"); %>
<% if(data.ContainsKey("IsTreatmentApply") && data.AnswerOrEmptyString("IsTreatmentApply").Equals("1")){%>
<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Treatment Plan");
</script>
<%}else{ %>
<script type="text/javascript">
            printview.addsection(
            printview.span("OT Frequency & Duration:<%=data.AnswerOrEmptyString("POCGenericFrequencyAndDuration").Clean() %>")+
            printview.col(3,
            printview.checkbox("Therapeutic exercise",<%= genericTreatmentPlan.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Bed Mobility Training",<%= genericTreatmentPlan.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Transfer Training",<%= genericTreatmentPlan.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Balance Training",<%= genericTreatmentPlan.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Gait Training",<%= genericTreatmentPlan.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Neuromuscular re-education",<%= genericTreatmentPlan.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Functional mobility training",<%= genericTreatmentPlan.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective use of adaptive/assist device",<%= genericTreatmentPlan.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("Teach safe stair climbing skills",<%= genericTreatmentPlan.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Teach fall prevention/safety",<%= genericTreatmentPlan.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("Establish/upgrade home exercise program",<%= genericTreatmentPlan.Contains("11").ToString().ToLower() %>) +
            printview.checkbox("Pt/caregiver education/training",<%= genericTreatmentPlan.Contains("12").ToString().ToLower() %>) +
            printview.checkbox("Proprioceptive training",<%= genericTreatmentPlan.Contains("13").ToString().ToLower() %>) +
            printview.checkbox("Postural control training",<%= genericTreatmentPlan.Contains("14").ToString().ToLower() %>) +
            printview.checkbox("Teach energy conservation techniques",<%= genericTreatmentPlan.Contains("15").ToString().ToLower() %>) +
            printview.checkbox("Relaxation technique",<%= genericTreatmentPlan.Contains("16").ToString().ToLower() %>) +
            printview.checkbox("Teach safe and effective breathing technique",<%= genericTreatmentPlan.Contains("17").ToString().ToLower() %>) +
            printview.checkbox("Teach hip precaution",<%= genericTreatmentPlan.Contains("18").ToString().ToLower() %>) +
            printview.checkbox("Electrical stimulation",<%= genericTreatmentPlan.Contains("19").ToString().ToLower() %>) +
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("GenericTreatmentPlan19BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("GenericTreatmentPlan19Duration").Clean() %>",0,1)+
            printview.checkbox("Ultrasound",<%= genericTreatmentPlan.Contains("20").ToString().ToLower() %>) +
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan20BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan20Duration").Clean() %>",0,1)+
            printview.checkbox("TENS",<%= genericTreatmentPlan.Contains("21").ToString().ToLower() %>) +
            printview.span("Body Parts: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan21BodyParts").Clean() %>",0,1)+
            printview.span("Duration: <%= data.AnswerOrEmptyString("POCGenericTreatmentPlan21Duration").Clean() %>",0,1)+
            printview.checkbox("Prosthetic training",<%= genericTreatmentPlan.Contains("22").ToString().ToLower() %>) +
            printview.checkbox("Pulse oximetry PRN",<%= genericTreatmentPlan.Contains("23").ToString().ToLower() %>) +
            printview.span("&#160;"))+
            printview.span("Other",true)+
            printview.span("<%= data.AnswerOrEmptyString("POCGenericTreatmentPlanOther").Clean() %>",0,1),
            "Treatment Plan");
</script>
<%} %>
    <% if (data.AnswerOrEmptyString("POCIsModalitiesApply").Equals("1"))
   {%>

<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Modalities");
</script>

<%}
   else
   { %>

<script type="text/javascript">
    printview.addsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericModalitiesComment").Clean() %>",0,3),"Modalities");
</script>

<%} %>
    <%  string[] genericOTGoals = data.AnswerArray("POCGenericOTGoals"); %>
<script type="text/javascript">
    printview.addsection(
        <%if(genericOTGoals.Contains("1")){ %>
        printview.checkbox("Patient will be able to perform upper body dressing with <%= data.AnswerOrEmptyString("POCGenericOTGoals1With").Clean() %> assist  utilizing <%= data.AnswerOrEmptyString("POCGenericOTGoals1With").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericOTGoals1Weeks").Clean() %> weeks",<%= genericOTGoals.Contains("1").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("2")){ %>
        printview.checkbox("Patient will be able to perform lower body dressing with <%= data.AnswerOrEmptyString("POCGenericOTGoals2With").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("POCGenericOTGoals2Utilize").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericOTGoals2Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("2").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("3")){ %>
        printview.checkbox("Patient will be able to perform toilet hygiene with <%= data.AnswerOrEmptyString("POCGenericOTGoals3Assist").Clean() %> assist within <%= data.AnswerOrEmptyString("POCGenericOTGoals3Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("3").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("4")){ %>
        printview.checkbox("Patient will be able to perform grooming with <%= data.AnswerOrEmptyString("POCGenericOTGoals4Assist").Clean() %> assist within <%= data.AnswerOrEmptyString("POCGenericOTGoals4Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("4").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("5")){ %>
        printview.checkbox("Patient will be able to perform <%= data.AnswerOrEmptyString("POCGenericOTGoals5With").Clean() %> (task)  with <%= data.AnswerOrEmptyString("POCGenericOTGoals5Assist").Clean() %> assist using <%= data.AnswerOrEmptyString("POCGenericOTGoals5Within").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericOTGoals5Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("5").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("6")){ %>
        printview.checkbox("Improve strength of <%= data.AnswerOrEmptyString("POCGenericOTGoals6Of").Clean() %> to <%= data.AnswerOrEmptyString("POCGenericOTGoals6To").Clean() %> grade to improve <%= data.AnswerOrEmptyString("POCGenericOTGoals6Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericOTGoals6Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("6").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("7")){ %>
        printview.checkbox("Increase muscle strength of <%= data.AnswerOrEmptyString("POCGenericOTGoals7Skill").Clean() %> to <%= data.AnswerOrEmptyString("POCGenericOTGoals7Assist").Clean() %> grade to improve <%= data.AnswerOrEmptyString("POCGenericOTGoals7Within").Clean() %> (task) within <%= data.AnswerOrEmptyString("POCGenericOTGoals7Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("7").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("8")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("POCGenericOTGoals8To").Clean() %> to improve postural control and balance necessary to perform <%= data.AnswerOrEmptyString("POCGenericOTGoals8Within").Clean() %> (task) within <%= data.AnswerOrEmptyString("POCGenericOTGoals8Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("8").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("9")){ %>
        printview.checkbox("Increase trunk muscle strength to <%= data.AnswerOrEmptyString("POCGenericOTGoals9Within").Clean() %> to improve postural control during bed mobility and transfer within <%= data.AnswerOrEmptyString("POCGenericOTGoals9Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("9").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("10")){ %>
        printview.checkbox("Patient will increase ROM of <%= data.AnswerOrEmptyString("POCGenericOTGoals10Device").Clean() %> joint to <%= data.AnswerOrEmptyString("POCGenericOTGoals10Feet").Clean() %> degree of <%= data.AnswerOrEmptyString("POCGenericOTGoals10Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericOTGoals10Weeks").Clean() %> weeks to improve <%= data.AnswerOrEmptyString("POCGenericOTGoals10DegreeOf").Clean() %> (task).",<%= genericOTGoals.Contains("10").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("11")){ %>
        printview.checkbox("Patient/caregiver will be able to perform HEP safely and effectively within <%= data.AnswerOrEmptyString("POCGenericOTGoals11Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("11").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("12")){ %>
        printview.checkbox("Patient will learn <%= data.AnswerOrEmptyString("POCGenericOTGoals12Device").Clean() %> techniques to <%= data.AnswerOrEmptyString("POCGenericOTGoals12Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericOTGoals12Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("12").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("13")){ %>
        printview.checkbox("Patient will improve <%= data.AnswerOrEmptyString("POCGenericOTGoals13Of").Clean() %> standardized test score to <%= data.AnswerOrEmptyString("POCGenericOTGoals13To").Clean() %> to improve <%= data.AnswerOrEmptyString("POCGenericOTGoals13Within").Clean() %> within <%= data.AnswerOrEmptyString("POCGenericOTGoals13Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("13").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("14")){ %>
        printview.checkbox("Patient/caregiver will demonstrate proper use of brace/splint within <%= data.AnswerOrEmptyString("POCGenericOTGoals14Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("14").ToString().ToLower() %>)+
        <%} %>
        <%if(genericOTGoals.Contains("15")){ %>
        printview.checkbox("Patient/caregiver will demonstrate proper use of wheelchair with <%= data.AnswerOrEmptyString("POCGenericOTGoals15To").Clean() %> assist within <%= data.AnswerOrEmptyString("POCGenericOTGoals15Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("15").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("16")){ %>
        printview.checkbox("Patient will be able to perform upper body bathing with <%= data.AnswerOrEmptyString("POCGenericOTGoals16With").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("POCGenericOTGoals16To").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericOTGoals16Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("16").ToString().ToLower() %>)+ 
        <%} %>
        <%if(genericOTGoals.Contains("17")){ %>
        printview.checkbox("Patient will be able to perform lower body bathing with <%= data.AnswerOrEmptyString("POCGenericOTGoals17Joint").Clean() %> assist utilizing <%= data.AnswerOrEmptyString("POCGenericOTGoals17Degree").Clean() %> device within <%= data.AnswerOrEmptyString("POCGenericOTGoals17Weeks").Clean() %> weeks.",<%= genericOTGoals.Contains("17").ToString().ToLower() %>)+ 
        <%} %>
        printview.span("Frequency:<%= data.AnswerOrEmptyString("POCGenericOTGoalsFrequency").Clean() %>")+
        printview.span("Duration:<%= data.AnswerOrEmptyString("POCGenericOTGoalsDuration").Clean() %>")+
        printview.span("Additional goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericOTGoalsComments").Clean() %>",0,2)+
        printview.span("OT Short Term Goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericOTShortTermGoalsComments").Clean() %>",0,2)+
        printview.span("OT Long Term Goals:",true)+
        printview.span("<%= data.AnswerOrEmptyString("POCGenericOTLongTermGoalsComments").Clean() %>",0,2)+
        printview.col(2,
            printview.checkbox("Patient",<%= data.AnswerOrEmptyString("POCOTGoalsDesired").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver desired outcomes:<%=data.AnswerOrEmptyString("POCOTGoalsDesiredOutcomes").Clean() %>",<%= data.AnswerOrEmptyString("POCOTGoalsDesired").Contains("1").ToString().ToLower() %>)),
        "OT Goals");
        
</script>
    
    <% if (data.AnswerOrEmptyString("POCIsRecommendationApply").Equals("1"))
       {%>

    <script type="text/javascript">
        printview.addsection(
        printview.checkbox("N/A", true),
        "Other Discipline Recommendation");
    </script>

    <%}
       else
       { %>

    <script type="text/javascript">
    printview.addsection(
        printview.col(6,
            printview.checkbox("PT", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("1").ToString().ToLower() %>) +
            printview.checkbox("MSW", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("2").ToString().ToLower() %>) +
            printview.checkbox("ST", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Podiatrist", <%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendation").Split(',').Contains("4").ToString().ToLower() %>) +
            printview.span("Other", true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendationOther").Clean() %>",0,1)) +
            printview.span("Reason", true) +
            printview.span("<%= data.AnswerOrEmptyString("POCGenericDisciplineRecommendationReason").Clean() %>",0,1) ,
        "Other Discipline Recommendation");
    </script>

    <%} %>
    
    <% if (data.AnswerOrEmptyString("IsRehabApply").Equals("1"))
       {%>

    <script type="text/javascript">
        printview.addsection(
        printview.checkbox("N/A", true),
        "Rehab");
    </script>

    <%}
       else
       { %>

    <script type="text/javascript">
    printview.addsection(
        printview.span("Rehab Diagnosis:<%=data.AnswerOrEmptyString("POCGenericDisciplineRehabDiagnosis").Clean() %>",true)+
        printview.span("Rehab Potential:",true)+
        printview.col(3,
            printview.checkbox("Good",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("0").ToString().ToLower() %>)+
            printview.checkbox("Fair",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Poor",<%= data.AnswerOrEmptyString("POCRehabPotential").Contains("2").ToString().ToLower() %>))+
        printview.span("<%=data.AnswerOrEmptyString("POCOtherRehabPotential").Clean() %>",0,1),
        "Rehab");
            
    </script>

    <%} %>
    
    <% if (data.AnswerOrEmptyString("POCIsDCPlanApply").Equals("1"))
   {%>

<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Discharge Plan");
</script>

<%}
   else
   { %>

<script type="text/javascript">
    printview.addsection(
        printview.span("Patient to be discharged to the care of:",true)+
        printview.col(3,
            printview.checkbox("Physician",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("2").ToString().ToLower() %>)+
            printview.checkbox("Selfcare",<%= data.AnswerOrEmptyString("POCDCPlanCareOf").Contains("3").ToString().ToLower() %>))+
        printview.span("Discharge Plans:",true)+
        printview.col(2,
            printview.checkbox("Discharge when caregiver willing and able to manage all aspects of patient’s care",<%= data.AnswerOrEmptyString("POCDCPlanPlans").Contains("1").ToString().ToLower() %>)+
            printview.checkbox("Discharge when goals met.",<%= data.AnswerOrEmptyString("POCDCPlanPlans").Contains("2").ToString().ToLower() %>))+
        printview.span("<%= data.AnswerOrEmptyString("POCDCPlanAdditional").Clean() %>",0,3),"Discharge Plan");
</script>

<%} %>
    <% if (data.AnswerOrEmptyString("POCIsSkilledCareApply").Equals("1"))
   { %>

<script type="text/javascript">
    printview.addsection(
        printview.checkbox("N/A", true),
        "Skilled Care Provided");
</script>

<%}
   else
   { %>

<script type="text/javascript">
    printview.addsection(
        printview.span("Training Topics:<%= data.AnswerOrEmptyString("POCSkilledCareTrainingTopics").Clean() %>",0,3)+
        printview.span("Trained:",true)+
        printview.col(2,
        printview.checkbox("Patient",<%= data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("0").ToString().ToLower() %>) +
        printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCSkilledCareTrained").Contains("1").ToString().ToLower() %>)) +
        printview.span("Treatment Performed: <%= data.AnswerOrEmptyString("POCSkilledCareTreatmentPerformed").Clean() %>",0,3)+
        printview.span("Patient Response: <%= data.AnswerOrEmptyString("POCSkilledCarePatientResponse").Clean() %>",0,3),
    "Skilled Care Provided");
</script>

<%} %>

    <% if (data.AnswerOrEmptyString("POCIsCareApply").Equals("1"))
   {%>

<script type="text/javascript">
    printview.addsubsection(
        printview.checkbox("N/A", true),
        "Care Coordination", 2);
</script>

<%}
   else
   { %>

<script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericCareCoordination").Clean() %>",0,3),"Care Coordination");
</script>

<%} %>
    <% if (data.AnswerOrEmptyString("POCIsSafetyIssueApply").Equals("1"))
   {%>

<script type="text/javascript">
    printview.addsubsection(
        printview.checkbox("N/A", true),
        "Safety Issues/Instruction/Education");
</script>

<%}
   else
   { %>

<script type="text/javascript">
    printview.addsubsection(printview.span("<%= data.AnswerOrEmptyString("POCGenericSafetyIssue").Clean() %>",0,3),"Safety Issues/Instruction/Education");
</script>

<%} %>
    
    <script type="text/javascript">
    printview.addsection(
        printview.col(2,
            printview.checkbox("Patient ",<%= data.AnswerOrEmptyString("POCNotificationUnderstands").Contains("0").ToString().ToLower() %>) +
            printview.checkbox("Caregiver",<%= data.AnswerOrEmptyString("POCNotificationUnderstands").Contains("1").ToString().ToLower() %>)) +
        printview.span("understands diagnosis/prognosis and agrees with Goals/Time frame and Plan of Care (POC)",true)+
            printview.col(2,
                printview.checkbox("Yes",<%= data.AnswerOrEmptyString("POCNotificationPeopleAnswer").Contains("1").ToString().ToLower() %>) +
                printview.checkbox("No",<%= data.AnswerOrEmptyString("POCNotificationPeopleAnswer").Contains("0").ToString().ToLower() %>))+
        printview.checkbox("Physician notified and agrees with POC, frequency and duration. Comments (if any):<%=data.AnswerOrEmptyString("POCNotificationPhysicianComment").Clean() %>",<%= data.AnswerOrEmptyString("POCNotificationPhysician").Contains("0").ToString().ToLower() %>),
    "Notification");
</script>

    <script type="text/javascript">
        printview.addsection(
        printview.col(2,
            printview.span("Physician Signature", 1) +
            printview.span("Date", 1) +
            printview.span("<%= Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText.Clean() : string.Empty %>", 0, 1) +
            printview.span("<%= Model.PhysicianSignatureDate.IsValid() ? Model.PhysicianSignatureDate.ToShortDateString().Clean() : string.Empty %>", 0, 1)));
    </script>

</body>
</html>
