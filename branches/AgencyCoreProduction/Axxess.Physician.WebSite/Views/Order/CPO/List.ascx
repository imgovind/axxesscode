﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<CarePlanOversights>>" %>
<span class="wintitle">Care Plan Oversight list</span>
<div class="wrapper">
<div id="cpoListContent"><% Html.RenderPartial("~/Views/Order/CPO/Content.ascx", Model); %></div>    

    <%--<div class="buttons">
        <ul>
            <li>
                <a href="javascript:void(0);" onclick="cpo.SubmitCpoBill($('#CPO_BillList').val());">Bill Patient</a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="cpo.RebindList();" >Refresh</a>
            </li>
        </ul>
    </div>--%>
</div>
