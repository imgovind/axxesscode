﻿(function($) {
    $.extend($.fn, {
        Shade: function(option) {
            return this.each(function() {
                if (option == "remove") $(".ui-shade", this).remove();
                else $(this).append(
                    $("<div>").addClass("ui-shade")
                )
            })
        }
    })
})(jQuery);