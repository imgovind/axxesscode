﻿(function($) {
    $.extend($.fn, {
        ClearForm: function() {
            return this.each(function() {
                $(":input", this).each(function() {
                    $(this).val($(this).attr("default") ? $(this).attr("default") : "")
                })
            })
        }
    })
})(jQuery);