﻿namespace Axxess.OasisC.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;

    using Axxess.OasisC.Enums;
    using Axxess.Core.Enums;


    public static class AssessmentTypeFactory
    {
        public static int ParseString(string value)
        {
            return value.IsNotNullOrEmpty() && Enum.IsDefined(typeof(AssessmentType), value) ? (int)Enum.Parse(typeof(AssessmentType), value) : 0;
        }

        public static List<int> NextEpisodeAssessments()
        {
            var list = new List<int>();
            list.Add((int)AssessmentType.Recertification);
            list.Add((int)AssessmentType.ResumptionOfCare);
            return list;
        }

        public static List<int> NonCMSAssessments()
        {
            var list = new List<int>();
            list.Add((int)AssessmentType.PlanOfCare485);
            list.Add((int)AssessmentType.NonOasisDischarge);
            list.Add((int)AssessmentType.NonOasisRecertification);
            list.Add((int)AssessmentType.NonOasisStartOfCare);
            return list;
        }

        public static List<int> GeneratePlanOfCareAssessments(DateTime eventDate, DateTime endDate)
        {
            var list = new List<int>();
            list.Add((int)AssessmentType.StartOfCare);
            list.Add((int)AssessmentType.ResumptionOfCare);
            if (eventDate.IsValid() && eventDate.Date >= endDate.AddDays(-5).Date && eventDate.Date <= endDate.Date)
            {
                list.Add((int)AssessmentType.Recertification);
            }
            list.AddRange(GeneratePlanOfCareNonOasis());
            return list;
        }

        public static List<int> GeneratePlanOfCareNonOasis()
        {
            var list = new List<int>();
            list.Add((int)AssessmentType.NonOasisStartOfCare);
            list.Add((int)AssessmentType.NonOasisRecertification);
            return list;
        }

        public static List<int> VitalSignAssessments()
        {
            var list = new List<int>();
            list.Add((int)AssessmentType.StartOfCare);
            list.Add((int)AssessmentType.ResumptionOfCare);
            list.Add((int)AssessmentType.Recertification);
            list.Add((int)AssessmentType.NonOasisStartOfCare);
            list.Add((int)AssessmentType.NonOasisRecertification);
            return list;
        }

        public static AssessmentType ToAssessmentTypeFromTaskName(string disciplineTaskName)
        {
            var type = AssessmentType.None;
            switch (disciplineTaskName)
            {
                case "StartOfCare":
                case "OASISCStartOfCare":
                case "OASISCStartOfCarePT":
                case "OASISCStartOfCareOT":
                    type = AssessmentType.StartOfCare;
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionOfCare":
                case "OASISCResumptionOfCareOT":
                case "OASISCResumptionOfCarePT":
                    type = AssessmentType.ResumptionOfCare;
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    type = AssessmentType.FollowUp;
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    type = AssessmentType.Recertification;
                    break;
                case "TransferInPatientNotDischarged":
                case "TransferNotDischarged":
                case "Transfer":
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                    type = AssessmentType.TransferInPatientNotDischarged;
                    break;
                case "TransferInPatientDischarged":
                case "TransferDischarge":
                case "TransferDischarged":
                case "OASISCTransferDischarge":
                    type = AssessmentType.TransferInPatientDischarged;
                    break;
                case "DischargeFromAgencyDeath":
                case "Death":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    type = AssessmentType.DischargeFromAgencyDeath;
                    break;
                case "DischargeFromAgency":
                case "Discharge":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    type = AssessmentType.DischargeFromAgency;
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    type = AssessmentType.NonOasisDischarge;
                    break;
                case "SNAssessment":
                case "NonOASISStartOfCare":
                case "NonOASISStartofCare":
                case "NonOasisStartOfCare":
                case "NonOasisStartofCare":
                    type = AssessmentType.NonOasisStartOfCare;
                    break;
                case "SNAssessmentRecert":
                case "NonOASISRecertification":
                case "NonOasisRecertification":
                    type = AssessmentType.NonOasisRecertification;
                    break;
                default:
                    break;
            }
            return type;
        }

        public static AssessmentType ToAssessmentType(DisciplineTasks task)
        {
            return ToAssessmentTypeFromTaskName(task.ToString());
        }

        public static DisciplineTasks ToDisciplineTask(AssessmentType assessmentType)
        {
            var task = DisciplineTasks.NoDiscipline;
            switch (assessmentType)
            {
                case AssessmentType.StartOfCare:
                    task = DisciplineTasks.OASISCStartofCare;
                    break;
                case AssessmentType.ResumptionOfCare:
                    task = DisciplineTasks.OASISCResumptionofCare;
                    break;
                case AssessmentType.FollowUp:
                    task = DisciplineTasks.OASISCFollowUp;
                    break;
                case AssessmentType.Recertification:
                    task = DisciplineTasks.OASISCRecertification;
                    break;
                case AssessmentType.TransferInPatientNotDischarged:
                    task = DisciplineTasks.OASISCTransfer;
                    break;
                case AssessmentType.TransferInPatientDischarged:
                    task = DisciplineTasks.OASISCTransferDischarge;
                    break;
                case AssessmentType.DischargeFromAgencyDeath:
                    task = DisciplineTasks.OASISCDeath;
                    break;
                case AssessmentType.DischargeFromAgency:
                    task = DisciplineTasks.OASISCDischarge;
                    break;
                case AssessmentType.NonOasisDischarge:
                    task = DisciplineTasks.NonOASISDischarge;
                    break;
                case AssessmentType.NonOasisStartOfCare:
                    task = DisciplineTasks.NonOASISStartofCare;
                    break;
                case AssessmentType.NonOasisRecertification:
                    task = DisciplineTasks.NonOASISRecertification;
                    break;
                default:
                    break;
            }
            return task;
        }

        public static string AssessmentTypeNumber(string disciplineTaskName)
        {
            string number = "00";
            switch (disciplineTaskName)
            {
                case "StartOfCare":
                case "OASISCStartOfCare":
                case "OASISCStartOfCarePT":
                case "OASISCStartOfCareOT":
                    number = "01";
                    break;
                case "ResumptionOfCare":
                case "OASISCResumptionOfCare":
                case "OASISCResumptionOfCareOT":
                case "OASISCResumptionOfCarePT":
                    number = "03";
                    break;
                case "FollowUp":
                case "OASISCFollowUp":
                case "OASISCFollowUpOT":
                case "OASISCFollowUpPT":
                    number = "05";
                    break;
                case "Recertification":
                case "OASISCRecertification":
                case "OASISCRecertificationOT":
                case "OASISCRecertificationPT":
                    number = "04";
                    break;
                case "OASISCTransfer":
                case "OASISCTransferPT":
                case "OASISCTransferOT":
                case "Transfer":
                case "TransferNotDischarged":
                    number = "06";
                    break;
                case "OASISCTransferDischarge":
                case "TransferDischarge":
                case "TransferDischarged":
                    number = "07";
                    break;
                case "Death":
                case "OASISCDeath":
                case "OASISCDeathOT":
                case "OASISCDeathPT":
                    number = "08";
                    break;
                case "Discharge":
                case "OASISCDischarge":
                case "OASISCDischargeOT":
                case "OASISCDischargePT":
                    number = "09";
                    break;
                case "NonOASISStartOfCare":
                case "NonOASISStartofCare":
                case "NonOasisStartOfCare":
                case "NonOasisStartofCare":
                    number = "10";
                    break;
                case "NonOasisRecertification":
                case "NonOASISRecertification":
                    number = "10";
                    break;
                case "NonOasisDischarge":
                case "NonOASISDischarge":
                    number = "10";
                    break;
                default:
                    break;
            }
            return number;
        }


    }
}
