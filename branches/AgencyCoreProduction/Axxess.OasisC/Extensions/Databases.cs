﻿namespace Axxess.OasisC.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Enums;
    using Domain;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;
    

    public static class Databases
    {
//        public static void InsertAny(this SimpleRepository repository, IAssessment assessment)
//        {
//            switch (assessment.Type)
//            {
//                case AssessmentType.StartOfCare:
//                    repository.Add<StartOfCareAssessment>((StartOfCareAssessment)assessment);
//                    break;
//                case AssessmentType.DischargeFromAgency:
//                    repository.Add<DischargeFromAgencyAssessment>((DischargeFromAgencyAssessment)assessment);
//                    break;
//                case AssessmentType.DischargeFromAgencyDeath:
//                    repository.Add<DeathAtHomeAssessment>((DeathAtHomeAssessment)assessment);
//                    break;
//                case AssessmentType.FollowUp:
//                    repository.Add<FollowUpAssessment>((FollowUpAssessment)assessment);
//                    break;
//                case AssessmentType.Recertification:
//                    repository.Add<RecertificationAssessment>((RecertificationAssessment)assessment);
//                    break;
//                case AssessmentType.ResumptionOfCare:
//                    repository.Add<ResumptionofCareAssessment>((ResumptionofCareAssessment)assessment);
//                    break;
//                case AssessmentType.TransferInPatientDischarged:
//                    repository.Add<TransferDischargeAssessment>((TransferDischargeAssessment)assessment);
//                    break;
//                case AssessmentType.TransferInPatientNotDischarged:
//                    repository.Add<TransferNotDischargedAssessment>((TransferNotDischargedAssessment)assessment);
//                    break;
//                case AssessmentType.NonOasisStartOfCare:
//                    repository.Add<NonOasisStartOfCareAssessment>((NonOasisStartOfCareAssessment)assessment);
//                    break;
//                case AssessmentType.NonOasisRecertification:
//                    repository.Add<NonOasisRecertificationAssessment>((NonOasisRecertificationAssessment)assessment);
//                    break;
//                case AssessmentType.NonOasisDischarge:
//                    repository.Add<NonOasisDischargeAssessment>((NonOasisDischargeAssessment)assessment);
//                    break;
//                default:
//                    break;
//            }
//        }

//        public static void AddOASISBackup(this SimpleRepository repository, OASISBackup data)
//        {
//            //repository.Add<OASISBackup>(data);
//            try
//            {
//                string sql = @"INSERT INTO oasisbackups(`Id`,`AgencyId`,`AssessmentId`,`Data`,`Created`) VALUES (@id,@agencyid,@assessmentid,@data,@created)";
//                using (var cmd = new FluentCommand<int>(sql))
//                {
//                    cmd.SetConnection("OasisCBackupConnectionString");
//                    cmd.AddGuid("id", data.Id);
//                    cmd.AddGuid("agencyid", data.AgencyId);
//                    cmd.AddGuid("assessmentid", data.AssessmentId);
//                    cmd.AddString("data", data.Data);
//                    cmd.AddDateTime("created", data.Created);
//                    cmd.AsNonQuery();
//                }
//            }
//            catch (Exception ex)
//            {
//            }
//        }

//        public static void UpdateAny(this SimpleRepository repository, IAssessment assessment)
//        {
//            switch (assessment.Type)
//            {
//                case AssessmentType.StartOfCare:
//                    repository.Update<StartOfCareAssessment>((StartOfCareAssessment)assessment);
//                    break;
//                case AssessmentType.DischargeFromAgency:
//                    repository.Update<DischargeFromAgencyAssessment>((DischargeFromAgencyAssessment)assessment);
//                    break;
//                case AssessmentType.DischargeFromAgencyDeath:
//                    repository.Update<DeathAtHomeAssessment>((DeathAtHomeAssessment)assessment);
//                    break;
//                case AssessmentType.FollowUp:
//                    repository.Update<FollowUpAssessment>((FollowUpAssessment)assessment);
//                    break;
//                case AssessmentType.Recertification:
//                    repository.Update<RecertificationAssessment>((RecertificationAssessment)assessment);
//                    break;
//                case AssessmentType.ResumptionOfCare:
//                    repository.Update<ResumptionofCareAssessment>((ResumptionofCareAssessment)assessment);
//                    break;
//                case AssessmentType.TransferInPatientDischarged:
//                    repository.Update<TransferDischargeAssessment>((TransferDischargeAssessment)assessment);
//                    break;
//                case AssessmentType.TransferInPatientNotDischarged:
//                    repository.Update<TransferNotDischargedAssessment>((TransferNotDischargedAssessment)assessment);
//                    break;
//                case AssessmentType.NonOasisStartOfCare:
//                    repository.Update<NonOasisStartOfCareAssessment>((NonOasisStartOfCareAssessment)assessment);
//                    break;
//                case AssessmentType.NonOasisRecertification:
//                    repository.Update<NonOasisRecertificationAssessment>((NonOasisRecertificationAssessment)assessment);
//                    break;
//                case AssessmentType.NonOasisDischarge:
//                    repository.Update<NonOasisDischargeAssessment>((NonOasisDischargeAssessment)assessment);
//                    break;
//                default:
//                    break;
//            }
//        }

//        public static Assessment FindAny(this SimpleRepository repository, string assessmentType, Guid assessmentId, Guid agencyId)
//        {
//            Assessment assessment = null;
//            var type = new AssessmentType();
//            switch (assessmentType)
//            {
//                case "StartOfCare":
//                case "OASISCStartofCare":
//                case "OASISCStartofCarePT":
//                case "OASISCStartofCareOT":
//                    assessment = repository.Single<StartOfCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.StartOfCare;
//                    break;
//                case "ResumptionOfCare":
//                case "OASISCResumptionofCare":
//                case "OASISCResumptionofCareOT":
//                case "OASISCResumptionofCarePT":
//                    assessment = repository.Single<ResumptionofCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.ResumptionOfCare;
//                    break;
//                case "FollowUp":
//                case "OASISCFollowUp":
//                case "OASISCFollowUpOT":
//                case "OASISCFollowUpPT":
//                    assessment = repository.Single<FollowUpAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.FollowUp;
//                    break;
//                case "Recertification":
//                case "OASISCRecertification":
//                case "OASISCRecertificationOT":
//                case "OASISCRecertificationPT":
//                case "OASISCRecertificationST":
//                    assessment = repository.Single<RecertificationAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.Recertification;
//                    break;
//                case "OASISCTransfer":
//                case "OASISCTransferPT":
//                case "OASISCTransferOT":
//                case "TransferInPatientNotDischarged":
//                    assessment = repository.Single<TransferNotDischargedAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.TransferInPatientNotDischarged;
//                    break;
//                case "OASISCTransferDischarge":
//                case "TransferInPatientDischarged":
//                case "OASISCTransferDischargePT":
//                    assessment = repository.Single<TransferDischargeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.TransferInPatientDischarged;
//                    break;
//                case "DischargeFromAgencyDeath":
//                case "OASISCDeath":
//                case "OASISCDeathOT":
//                case "OASISCDeathPT":
//                    assessment = repository.Single<DeathAtHomeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.DischargeFromAgencyDeath;
//                    break;
//                case "DischargeFromAgency":
//                case "OASISCDischarge":
//                case "OASISCDischargeOT":
//                case "OASISCDischargePT":
//                case "OASISCDischargeST":
//                    assessment = repository.Single<DischargeFromAgencyAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.DischargeFromAgency;
//                    break;
//                case "NonOasisDischarge":
//                case "NonOASISDischarge":
//                    assessment = repository.Single<NonOasisDischargeAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.NonOasisDischarge;
//                    break;
//                case "SNAssessment":
//                case "NonOasisStartOfCare":
//                case "NonOASISStartofCare":
//                    assessment = repository.Single<NonOasisStartOfCareAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.NonOasisStartOfCare;
//                    break;
//                case "SNAssessmentRecert":
//                case "NonOasisRecertification":
//                case "NonOASISRecertification":
//                    assessment = repository.Single<NonOasisRecertificationAssessment>(e => e.Id == assessmentId && e.AgencyId == agencyId );
//                    type = AssessmentType.NonOasisRecertification;
//                    break;
//                default:
//                    break;
//            }
//            if (assessment != null)
//            {
//                assessment.Type = type;
//            }
//            return assessment;
//        }

//        public static List<Assessment> GetAssessmentSupplies(this SimpleRepository repository, Guid agencyId, Guid patientId, string assessmentType, List<Guid> assessmentIds)
//        {
//            List<Assessment> list = null;
//            var table = TableName(assessmentType);
//            if (table.IsNotNullOrEmpty())
//            {
//                var script = string.Format(@"SELECT
//                        ast.Id,
//                        ast.Supply 
//                            FROM 
//                                {0} ast
//                                        WHERE 
//                                            ast.AgencyId = @agencyid AND
//                                            ast.PatientId = @patientid AND
//                                            ast.Id IN (@assessmentids) ", table);


//                using (var cmd = new FluentCommand<Assessment>(script))
//                {
//                    list = cmd.SetConnection("OasisCConnectionString")
//                    .AddGuid("agencyid", agencyId)
//                     .AddGuid("patientid", patientId)
//                     .AddString("assessmentIds", assessmentIds.ToCommaSeperatedNoQuote())
//                    .SetMap(reader => new Assessment
//                    {
//                        Id = reader.GetGuid("Id"),
//                        Supply = reader.GetStringNullable("Supply")
//                    })
//                    .AsList();

//                }
//            }
//            return list;
//        }

//        public static Assessment FindAny(this SimpleRepository repository, Guid agencyId, Guid assessmentId, Guid patientId, Guid episodeId, string assessmentType)
//        {
//            Assessment assessment = null;
//            var enumAssessmentType = new AssessmentType();
//            switch (assessmentType)
//            {
//                case "StartOfCare":
//                case "OASISCStartofCare":
//                case "OASISCStartofCarePT":
//                case "OASISCStartofCareOT":
//                    assessment = repository.Single<StartOfCareAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.StartOfCare;
//                    break;
//                case "ResumptionOfCare":
//                case "OASISCResumptionofCare":
//                case "OASISCResumptionofCareOT":
//                case "OASISCResumptionofCarePT":
//                    assessment = repository.Single<ResumptionofCareAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.ResumptionOfCare;
//                    break;
//                case "FollowUp":
//                case "OASISCFollowUp":
//                case "OASISCFollowupOT":
//                case "OASISCFollowupPT":
//                    assessment = repository.Single<FollowUpAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.FollowUp;
//                    break;
//                case "Recertification":
//                case "OASISCRecertification":
//                case "OASISCRecertificationOT":
//                case "OASISCRecertificationPT":
//                case "OASISCRecertificationST":
//                    assessment = repository.Single<RecertificationAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.Recertification;
//                    break;
//                case "OASISCTransfer":
//                case "OASISCTransferPT":
//                case "OASISCTransferOT":
//                case "TransferInPatientNotDischarged":
//                    assessment = repository.Single<TransferNotDischargedAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.TransferInPatientNotDischarged;
//                    break;
//                case "OASISCTransferDischarge":
//                case "OASISCTransferDischargePT":
//                case "TransferInPatientDischarged":
//                    assessment = repository.Single<TransferDischargeAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.TransferInPatientDischarged;
//                    break;
//                case "DischargeFromAgencyDeath":
//                case "OASISCDeath":
//                case "OASISCDeathOT":
//                case "OASISCDeathPT":
//                    assessment = repository.Single<DeathAtHomeAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.DischargeFromAgencyDeath;
//                    break;
//                case "DischargeFromAgency":
//                case "OASISCDischarge":
//                case "OASISCDischargeOT":
//                case "OASISCDischargePT":
//                case "OASISCDischargeST":
//                    assessment = repository.Single<DischargeFromAgencyAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.DischargeFromAgency;
//                    break;
//                case "NonOasisDischarge":
//                case "NonOASISDischarge":
//                    assessment = repository.Single<NonOasisDischargeAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.NonOasisDischarge;
//                    break;
//                case "SNAssessment":
//                case "NonOasisStartOfCare":
//                case "NonOASISStartofCare":
//                    assessment = repository.Single<NonOasisStartOfCareAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.NonOasisStartOfCare;
//                    break;
//                case "SNAssessmentRecert":
//                case "NonOasisRecertification":
//                case "NonOASISRecertification":
//                    assessment = repository.Single<NonOasisRecertificationAssessment>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId );
//                    enumAssessmentType = AssessmentType.NonOasisRecertification;
//                    break;
//                default:
//                    break;
//            }
//            if (assessment != null)
//            {
//                assessment.Type = enumAssessmentType;
//            }
//            return assessment;
//        }

//        public static IList<Assessment> FindAnyByStatus(this SimpleRepository repository, Guid agencyId, string assessmentType, int status)
//        {
//            IList<Assessment> assessments = new List<Assessment>();
//            switch (assessmentType)
//            {
//                case "StartOfCare":
//                case "OASISCStartofCare":
//                case "OASISCStartofCarePT":
//                case "OASISCStartofCareOT":
//                    assessments = repository.Find<StartOfCareAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "ResumptionOfCare":
//                case "OASISCResumptionofCare":
//                case "OASISCResumptionofCareOT":
//                case "OASISCResumptionofCarePT":
//                    assessments = repository.Find<ResumptionofCareAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "FollowUp":
//                case "OASISCFollowUp":
//                case "OASISCFollowUpOT":
//                case "OASISCFollowUpPT":
//                    assessments = repository.Find<FollowUpAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "Recertification":
//                case "OASISCRecertification":
//                case "OASISCRecertificationOT":
//                case "OASISCRecertificationPT":
//                case "OASISCRecertificationST":
//                    assessments = repository.Find<RecertificationAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "OASISCTransfer":
//                case "OASISCTransferPT":
//                case "OASISCTransferOT":
//                case "TransferInPatientNotDischarged":
//                    assessments = repository.Find<TransferNotDischargedAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "OASISCTransferDischarge":
//                case "OASISCTransferDischargePT":
//                case "TransferInPatientDischarged":
//                    assessments = repository.Find<TransferDischargeAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "DischargeFromAgencyDeath":
//                case "OASISCDeath":
//                case "OASISCDeathOT":
//                case "OASISCDeathPT":
//                    assessments = repository.Find<DeathAtHomeAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "DischargeFromAgency":
//                case "OASISCDischarge":
//                case "OASISCDischargeOT":
//                case "OASISCDischargePT":
//                case "OASISCDischargeST":
//                    assessments = repository.Find<DischargeFromAgencyAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "SNAssessment":
//                case "NonOasisStartOfCare":
//                case "NonOASISStartofCare":
//                    assessments = repository.Find<NonOasisStartOfCareAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "SNAssessmentRecert":
//                case "NonOasisRecertification":
//                case "NonOASISRecertification":
//                    assessments = repository.Find<NonOasisRecertificationAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "NonOasisDischarge":
//                case "NonOASISDischarge":
//                    assessments = repository.Find<NonOasisDischargeAssessment>(a => a.AgencyId == agencyId && a.Status == status && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                default:
//                    break;
//            }
//            return assessments;
//        }

//        public static IList<AssessmentExport> FindAnyByStatusLean(this SimpleRepository repository, Guid agencyId, Guid branchId, string assessmentType, int status, int patientStatus, DateTime startDate, DateTime endDate)
//        {
//            var assessments = new List<AssessmentExport>();
//            var table = string.Empty;
//            var type = string.Empty;
//            var name = string.Empty;
//            switch (assessmentType)
//            {
//                case "StartOfCare":
//                case "OASISCStartofCare":
//                case "OASISCStartofCarePT":
//                case "OASISCStartofCareOT":
//                    table = "startofcareassessments";
//                    type = AssessmentType.StartOfCare.ToString();
//                    name = AssessmentType.StartOfCare.GetDescription();
//                    break;
//                case "ResumptionOfCare":
//                case "OASISCResumptionofCare":
//                case "OASISCResumptionofCareOT":
//                case "OASISCResumptionofCarePT":
//                    table = "resumptionofcareassessments";
//                    type = AssessmentType.ResumptionOfCare.ToString();
//                    name = AssessmentType.ResumptionOfCare.GetDescription();
//                    break;
//                case "FollowUp":
//                case "OASISCFollowUp":
//                case "OASISCFollowUpOT":
//                case "OASISCFollowUpPT":
//                    table = "followupassessments";
//                    type = AssessmentType.FollowUp.ToString();
//                    name = AssessmentType.FollowUp.GetDescription();
//                    break;
//                case "Recertification":
//                case "OASISCRecertification":
//                case "OASISCRecertificationOT":
//                case "OASISCRecertificationPT":
//                case "OASISCRecertificationST":
//                    table = "recertificationassessments";
//                    type = AssessmentType.Recertification.ToString();
//                    name = AssessmentType.Recertification.GetDescription();
//                    break;
//                case "OASISCTransfer":
//                case "OASISCTransferPT":
//                case "OASISCTransferOT":
//                case "TransferInPatientNotDischarged":
//                    table = "transfernotdischargedassessments";
//                    type = AssessmentType.TransferInPatientNotDischarged.ToString();
//                    name = AssessmentType.TransferInPatientNotDischarged.GetDescription();
//                    break;
//                case "OASISCTransferDischarge":
//                case "OASISCTransferDischargePT":
//                case "TransferInPatientDischarged":
//                    table = "transferdischargeassessments";
//                    type = AssessmentType.TransferInPatientDischarged.ToString();
//                    name = AssessmentType.TransferInPatientDischarged.GetDescription();
//                    break;
//                case "DischargeFromAgencyDeath":
//                case "OASISCDeath":
//                case "OASISCDeathOT":
//                case "OASISCDeathPT":
//                    table = "deathathomeassessments";
//                    type = AssessmentType.DischargeFromAgencyDeath.ToString();
//                    name = AssessmentType.DischargeFromAgencyDeath.GetDescription();
//                    break;
//                case "DischargeFromAgency":
//                case "OASISCDischarge":
//                case "OASISCDischargeOT":
//                case "OASISCDischargePT":
//                case "OASISCDischargeST":
//                    table = "dischargefromagencyassessments";
//                    type = AssessmentType.DischargeFromAgency.ToString();
//                    name = AssessmentType.DischargeFromAgency.GetDescription();
//                    break;
//                case "SNAssessment":
//                case "NonOasisStartOfCare":
//                case "NonOASISStartofCare":
//                    table = "nonoasisstartofcareassessments";
//                    type = AssessmentType.NonOasisStartOfCare.ToString();
//                    name = AssessmentType.NonOasisStartOfCare.GetDescription();
//                    break;
//                case "SNAssessmentRecert":
//                case "NonOasisRecertification":
//                case "NonOASISRecertification":
//                    table = "nonoasisrecertificationassessments";
//                    type = AssessmentType.NonOasisRecertification.ToString();
//                    name = AssessmentType.NonOasisRecertification.GetDescription();
//                    break;
//                case "NonOasisDischarge":
//                case "NonOASISDischarge":
//                    table = "nonoasisdischargeassessments";
//                    type = AssessmentType.NonOasisDischarge.ToString();
//                    name = AssessmentType.NonOasisDischarge.GetDescription();
//                    break;
//                default:
//                    break;
//            }

//            if (table.IsNotNullOrEmpty())
//            {
//                var patientStatusQuery = string.Empty; ;
//                if (patientStatus <= 0)
//                {
//                    patientStatusQuery = " AND ( agencymanagement.patients.Status = 1 OR agencymanagement.patients.Status = 2 )";
//                }
//                else
//                {
//                    patientStatusQuery = " AND agencymanagement.patients.Status = @statusid";
//                }

//                var dateRange = string.Empty;
//                if (status == 225)
//                {
//                    dateRange = string.Format("AND oasisc.{0}.ExportedDate between @startdate and @enddate ",table);
//                }
//                else if (status == 220 || status == 240)
//                {
//                    dateRange = string.Format("AND oasisc.{0}.AssessmentDate between @startdate and @enddate ",table);
//                }

//                var script = string.Format(@"SELECT
//                                agencymanagement.patients.Id as PatientId, 
//                                agencymanagement.patients.FirstName as FirstName,
//                                agencymanagement.patients.LastName as LastName, 
//                                agencymanagement.patients.PrimaryInsurance as InsuranceId ,
//                                oasisc.{0}.Id  as Id ,
//                                oasisc.{0}.VersionNumber as VersionNumber, 
//                                oasisc.{0}.AssessmentDate as AssessmentDate ,
//                                oasisc.{0}.Modified as Modified ,
//                                oasisc.{0}.ExportedDate as ExportedDate ,
//                                oasisc.{0}.EpisodeId as EpisodeId, 
//                                agencymanagement.patientepisodes.schedule,
//                                agencymanagement.patientepisodes.EndDate as EndDate,
//                                agencymanagement.patientepisodes.StartDate as StartDate  
//                                    FROM
//                                        oasisc.{0} 
//                                            INNER JOIN agencymanagement.patients ON oasisc.{0}.PatientId = agencymanagement.patients.Id 
//                                            INNER JOIN agencymanagement.patientepisodes ON  oasisc.{0}.EpisodeId = agencymanagement.patientepisodes.Id 
//                                                WHERE
//                                                    oasisc.{0}.AgencyId = @agencyid {2} {3} {4} AND
//                                                    agencymanagement.patients.IsDeprecated = 0 AND 
//                                                    agencymanagement.patientepisodes.IsDischarged = 0 AND 
//                                                    agencymanagement.patientepisodes.IsActive = 1 AND
//                                                    oasisc.{0}.Status={1} ", table, status, patientStatusQuery, !branchId.IsEmpty() ? " AND agencymanagement.patients.AgencyLocationId = @branchId " : string.Empty, dateRange);

//                using (var cmd = new FluentCommand<AssessmentExport>(script))
//                {
//                    assessments = cmd.SetConnection("AgencyManagementConnectionString")
//                       .AddGuid("agencyid", agencyId)
//                       .AddGuid("branchId", branchId)
//                       .AddInt("status", status)
//                       .AddInt("statusid", patientStatus)
//                       .AddDateTime("startDate", startDate)
//                       .AddDateTime("endDate", endDate)
//                       .SetMap(reader => new AssessmentExport
//                       {
//                           AssessmentId = reader.GetGuid("Id"),
//                           PatientId = reader.GetGuid("PatientId"),
//                           EpisodeId = reader.GetGuid("EpisodeId"),
//                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
//                           EpisodeStartDate = reader.GetDateTime("StartDate"),
//                           EpisodeEndDate = reader.GetDateTime("EndDate"),
//                           AssessmentName = name,
//                           AssessmentType = type,
//                           EpisodeData = reader.GetString("Schedule"),
//                           CorrectionNumber = reader.GetInt("VersionNumber"),
//                           PatientName = string.Format("{0}, {1}", reader.GetString("LastName").ToUpperCase(), reader.GetString("FirstName").ToUpperCase()),
//                           InsuranceId = reader.GetIntNullable("InsuranceId") != null ? reader.GetInt("InsuranceId") : -1,
//                           ExportedDate = reader.GetDateTime("ExportedDate").Date > DateTime.MinValue ? reader.GetDateTime("ExportedDate") : reader.GetDateTime("Modified")

//                       }).AsList();
//                }
//            }
//            return assessments;
//        }

//        public static IList<AssessmentExport> FindAnyByStatusLean(this SimpleRepository repository, Guid agencyId, Guid branchId, string assessmentType, int status, List<int> paymentSources)
//        {
//            var results = new List<AssessmentExport>();
//            var assessments = new List<AssessmentExport>();
//            var table = string.Empty;
//            var type = string.Empty;
//            var name = string.Empty;
//            switch (assessmentType)
//            {
//                case "StartOfCare":
//                case "OASISCStartofCare":
//                case "OASISCStartofCarePT":
//                case "OASISCStartofCareOT":
//                    table = "startofcareassessments";
//                    type = AssessmentType.StartOfCare.ToString();
//                    name = AssessmentType.StartOfCare.GetDescription();
//                    break;
//                case "ResumptionOfCare":
//                case "OASISCResumptionofCare":
//                case "OASISCResumptionofCareOT":
//                case "OASISCResumptionofCarePT":
//                    table = "resumptionofcareassessments";
//                    type = AssessmentType.ResumptionOfCare.ToString();
//                    name = AssessmentType.ResumptionOfCare.GetDescription();
//                    break;
//                case "FollowUp":
//                case "OASISCFollowUp":
//                case "OASISCFollowUpOT":
//                case "OASISCFollowUpPT":
//                    table = "followupassessments";
//                    type = AssessmentType.FollowUp.ToString();
//                    name = AssessmentType.FollowUp.GetDescription();
//                    break;
//                case "Recertification":
//                case "OASISCRecertification":
//                case "OASISCRecertificationOT":
//                case "OASISCRecertificationPT":
//                case "OASISCRecertificationST":
//                    table = "recertificationassessments";
//                    type = AssessmentType.Recertification.ToString();
//                    name = AssessmentType.Recertification.GetDescription();
//                    break;
//                case "OASISCTransfer":
//                case "OASISCTransferPT":
//                case "OASISCTransferOT":
//                case "TransferInPatientNotDischarged":
//                    table = "transfernotdischargedassessments";
//                    type = AssessmentType.TransferInPatientNotDischarged.ToString();
//                    name = AssessmentType.TransferInPatientNotDischarged.GetDescription();
//                    break;
//                case "OASISCTransferDischarge":
//                case "OASISCTransferDischargePT":
//                case "TransferInPatientDischarged":
//                    table = "transferdischargeassessments";
//                    type = AssessmentType.TransferInPatientDischarged.ToString();
//                    name = AssessmentType.TransferInPatientDischarged.GetDescription();
//                    break;
//                case "DischargeFromAgencyDeath":
//                case "OASISCDeath":
//                case "OASISCDeathOT":
//                case "OASISCDeathPT":
//                    table = "deathathomeassessments";
//                    type = AssessmentType.DischargeFromAgencyDeath.ToString();
//                    name = AssessmentType.DischargeFromAgencyDeath.GetDescription();
//                    break;
//                case "DischargeFromAgency":
//                case "OASISCDischarge":
//                case "OASISCDischargeOT":
//                case "OASISCDischargePT":
//                case "OASISCDischargeST":
//                    table = "dischargefromagencyassessments";
//                    type = AssessmentType.DischargeFromAgency.ToString();
//                    name = AssessmentType.DischargeFromAgency.GetDescription();
//                    break;
//                case "SNAssessment":
//                case "NonOasisStartOfCare":
//                case "NonOASISStartofCare":
//                    table = "nonoasisstartofcareassessments";
//                    type = AssessmentType.NonOasisStartOfCare.ToString();
//                    name = AssessmentType.NonOasisStartOfCare.GetDescription();
//                    break;
//                case "SNAssessmentRecert":
//                case "NonOasisRecertification":
//                case "NonOASISRecertification":
//                    table = "nonoasisrecertificationassessments";
//                    type = AssessmentType.NonOasisRecertification.ToString();
//                    name = AssessmentType.NonOasisRecertification.GetDescription();
//                    break;
//                case "NonOasisDischarge":
//                case "NonOASISDischarge":
//                    table = "nonoasisdischargeassessments";
//                    type = AssessmentType.NonOasisDischarge.ToString();
//                    name = AssessmentType.NonOasisDischarge.GetDescription();
//                    break;
//                default:
//                    break;
//            }

//            if (table.IsNotNullOrEmpty())
//            {
//                var script = string.Format(@"SELECT 
//                        agencymanagement.patients.Id as PatientId,
//                        agencymanagement.patients.FirstName as FirstName,
//                        agencymanagement.patients.LastName as LastName, 
//                        agencymanagement.patients.PrimaryInsurance as InsuranceId, 
//                        oasisc.{0}.Id  as Id,
//                        oasisc.{0}.VersionNumber as VersionNumber,
//                        oasisc.{0}.AssessmentDate as AssessmentDate,
//                        oasisc.{0}.Modified as Modified,
//                        oasisc.{0}.ExportedDate as ExportedDate, 
//                        oasisc.{0}.EpisodeId as EpisodeId, 
//                        (oasisc.{0}.SubmissionFormat Is NULL || oasisc.{0}.SubmissionFormat = '') as IsSubmissionEmpty, 
//                        oasisc.{0}.IsValidated as IsValidated,
//                        agencymanagement.patientepisodes.schedule,
//                        agencymanagement.patientepisodes.EndDate as EndDate, 
//                        agencymanagement.patientepisodes.StartDate as StartDate,
//                        agencymanagement.patients.PaymentSource as PaymentSources 
//                            FROM 
//                                oasisc.{0} 
//                                INNER JOIN agencymanagement.patients ON oasisc.{0}.PatientId = agencymanagement.patients.Id 
//                                INNER JOIN agencymanagement.patientepisodes ON  oasisc.{0}.EpisodeId = agencymanagement.patientepisodes.Id 
//                                    WHERE
//                                        oasisc.{0}.AgencyId = @agencyid AND
//                                        Extract(YEAR FROM FROM_DAYS(DATEDIFF(CURRENT_DATE(), CAST(agencymanagement.patients.DOB as DATETIME)))) >= 18 AND
//                                        agencymanagement.patients.Status IN (1,2) {1} AND
//                                        agencymanagement.patients.IsDeprecated = 0 AND 
//                                        agencymanagement.patientepisodes.IsDischarged = 0 AND
//                                        agencymanagement.patientepisodes.IsActive = 1 AND 
//                                        oasisc.{0}.Status = @status ", table, !branchId.IsEmpty() ? " AND agencymanagement.patients.AgencyLocationId = @branchId " : string.Empty);

//                using (var cmd = new FluentCommand<AssessmentExport>(script))
//                {
//                    assessments = cmd.SetConnection("AgencyManagementConnectionString")
//                       .AddGuid("agencyid", agencyId)
//                       .AddGuid("branchId", branchId)
//                       .AddInt("status", status)
//                       .SetMap(reader => new AssessmentExport
//                       {
//                           AssessmentId = reader.GetGuid("Id"),
//                           PatientId = reader.GetGuid("PatientId"),
//                           EpisodeId = reader.GetGuid("EpisodeId"),
//                           IsValidated = reader.GetBoolean("IsValidated"),
//                           IsSubmissionEmpty = reader.GetBoolean("IsSubmissionEmpty"),
//                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
//                           EpisodeStartDate = reader.GetDateTime("StartDate"),
//                           EpisodeEndDate = reader.GetDateTime("EndDate"),
//                           AssessmentName = name,
//                           AssessmentType = type,
//                           EpisodeData = reader.GetString("Schedule"),
//                           PaymentSources = reader.GetStringNullable("PaymentSources"),
//                           CorrectionNumber = reader.GetInt("VersionNumber"),
//                           PatientName = string.Format("{0}, {1}", reader.GetString("LastName").ToUpperCase(), reader.GetString("FirstName").ToUpperCase()),
//                           InsuranceId = reader.GetIntNullable("InsuranceId") != null ? reader.GetInt("InsuranceId") : -1,
//                           ExportedDate = reader.GetDateTime("ExportedDate").Date > DateTime.MinValue ? reader.GetDateTime("ExportedDate") : reader.GetDateTime("Modified")

//                       }).AsList();
//                }

//                if (assessments != null && assessments.Count > 0)
//                {
//                    assessments.ForEach(a =>
//                    {
//                        if (a.PaymentSources.IsNotNullOrEmpty())
//                        {
//                            var assessmentPaymentSources = a.PaymentSources.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
//                            paymentSources.ForEach(p =>
//                            {
//                                if (assessmentPaymentSources.Contains(p.ToString()))
//                                {
//                                    if (!results.Exists(r => r.AssessmentId == a.AssessmentId))
//                                    {
//                                        results.Add(a);
//                                    }
//                                    return;
//                                }
//                            });
//                        }
//                    });
//                }
//            }
//            return results;
//        }

//        public static IList<Assessment> FindMany(this SimpleRepository repository, Guid agencyId, Guid patientId, string assessmentType)
//        {
//            IList<Assessment> assessments = new List<Assessment>();
//            switch (assessmentType)
//            {
//                case "StartOfCare":
//                case "OASISCStartofCare":
//                case "OASISCStartofCarePT":
//                case "OASISCStartofCareOT":
//                    assessments = repository.Find<StartOfCareAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "ResumptionOfCare":
//                case "OASISCResumptionofCare":
//                case "OASISCResumptionofCareOT":
//                case "OASISCResumptionofCarePT":
//                    assessments = repository.Find<ResumptionofCareAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "FollowUp":
//                case "OASISCFollowUp":
//                case "OASISCFollowUpOT":
//                case "OASISCFollowUpPT":
//                    assessments = repository.Find<FollowUpAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "Recertification":
//                case "OASISCRecertification":
//                case "OASISCRecertificationOT":
//                case "OASISCRecertificationPT":
//                case "OASISCRecertificationST":
//                    assessments = repository.Find<RecertificationAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "OASISCTransfer":
//                case "OASISCTransferPT":
//                case "OASISCTransferOT":
//                case "TransferInPatientNotDischarged":
//                    assessments = repository.Find<TransferNotDischargedAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "OASISCTransferDischarge":
//                case "OASISCTransferDischargePT":
//                case "TransferInPatientDischarged":
//                    assessments = repository.Find<TransferDischargeAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "DischargeFromAgencyDeath":
//                case "OASISCDeath":
//                case "OASISCDeathOT":
//                case "OASISCDeathPT":
//                    assessments = repository.Find<DeathAtHomeAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "DischargeFromAgency":
//                case "OASISCDischarge":
//                case "OASISCDischargeOT":
//                case "OASISCDischargePT":
//                case "OASISCDischargeST":
//                    assessments = repository.Find<DischargeFromAgencyAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "SNAssessment":
//                case "NonOasisStartOfCare":
//                case "NonOASISStartofCare":
//                    assessments = repository.Find<NonOasisStartOfCareAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "SNAssessmentRecert":
//                case "NonOasisRecertification":
//                case "NonOASISRecertification":
//                    assessments = repository.Find<NonOasisRecertificationAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                case "NonOasisDischarge":
//                case "NonOASISDischarge":
//                    assessments = repository.Find<NonOasisDischargeAssessment>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false).Cast<Assessment>().ToList();
//                    break;
//                default:
//                    break;
//            }
//            return assessments;
//        }

//        public static IList<string> FindAssessmentFields(this SimpleRepository repository, string assessmentType)
//        {
//            var results = new List<string>();
//            var formats = new List<SubmissionBodyFormat>(); 
//            var script = string.Format(
//                @"SELECT ElementName "+
//                "FROM submissionbodyformats " +
//                "WHERE {0} = 1 AND IsIgnorable = 0 AND ElementName != ''", assessmentType);

//            using (var cmd = new FluentCommand<SubmissionBodyFormat>(script))
//            {
//                formats = cmd.SetConnection("OasisCConnectionString")
//                   .SetMap(reader => new SubmissionBodyFormat
//                   {
//                       ElementName = reader.GetStringNullable("ElementName")
//                   }).AsList();
//            }
//            if (formats != null && formats.Count > 0)
//            {
//                results = formats.Select(s => s.ElementName.Remove(0, 5)).ToList<string>();
//                if (assessmentType == "RFA01" || assessmentType == "RFA03")
//                {
//                    for (int count = 1; count < 5; count++)
//                    {
//                        results.Add("InpatientFacilityProcedure" + count);
//                    }
//                    for (int count = 1; count < 7; count++)
//                    {
//                        results.Add("InpatientFacilityDiagnosis" + count);
//                        results.Add("MedicalRegimenDiagnosis" + count);
//                    }
//                }
//                if (assessmentType == "RFA01" || assessmentType == "RFA03" || assessmentType == "RFA05"
//                    || assessmentType == "RFA04")
//                {
//                    results.Add("PrimaryDiagnosis");
//                    results.Add("PrimaryDiagnosisDate");
//                    int character = 65;
//                    for (int diagnosisCount = 1; diagnosisCount < 26; diagnosisCount++)
//                    {
//                        results.Add("PrimaryDiagnosis" + diagnosisCount);
//                        results.Add("PrimaryDiagnosis" + diagnosisCount + "Date");
//                        results.Add("PaymentDiagnoses" + (char)character + "3");
//                        results.Add("PaymentDiagnoses" + (char)character + "4");
//                        character++;
//                    }
//                    character = 71;
//                    for (int diagCount2 = 6; diagCount2 < 26; diagCount2++)
//                    {
//                        results.Add("ICD9M" + (char)character + "3");
//                        results.Add("ICD9M" + (char)character + "4");
//                        results.Add("ICD9M" + diagCount2);
//                        results.Add("ExacerbationOrOnsetPrimaryDiagnosis" + diagCount2);
//                        results.Add("OtherDiagnose" + diagCount2 + "Rating");
//                        character++;
//                    }
//                    results.Add("PaymentDiagnosesZ3");
//                    results.Add("PaymentDiagnosesZ4");
//                }
//                results.Add("PressureUlcerLengthDecimal");
//                results.Add("PressureUlcerWidthDecimal");
//                results.Add("PressureUlcerDepthDecimal");
//            }
//            return results;
//        }


//        private static string TableName(string assessmentType)
//        {
//            var tableName = string.Empty;
//            switch (assessmentType)
//            {
//                case "StartOfCare":
//                case "OASISCStartofCare":
//                case "OASISCStartofCarePT":
//                case "OASISCStartofCareOT":
//                    tableName = "startofcareassessments";
//                    break;
//                case "ResumptionOfCare":
//                case "OASISCResumptionofCare":
//                case "OASISCResumptionofCareOT":
//                case "OASISCResumptionofCarePT":
//                    tableName = "resumptionofcareassessment";
//                    break;
//                case "FollowUp":
//                case "OASISCFollowUp":
//                case "OASISCFollowupOT":
//                case "OASISCFollowupPT":
//                    tableName = "followppassessments";
//                    break;
//                case "Recertification":
//                case "OASISCRecertification":
//                case "OASISCRecertificationOT":
//                case "OASISCRecertificationPT":
//                case "OASISCRecertificationST":
//                    tableName = "recertificationassessments";
//                    break;
//                case "OASISCTransfer":
//                case "OASISCTransferPT":
//                case "OASISCTransferOT":
//                case "TransferInPatientNotDischarged":
//                    tableName = "transfernotdischargedassessments";
//                    break;
//                case "OASISCTransferDischarge":
//                case "OASISCTransferDischargePT":
//                case "TransferInPatientDischarged":
//                    tableName = "transferdischargeassessments";
//                    break;
//                case "DischargeFromAgencyDeath":
//                case "OASISCDeath":
//                case "OASISCDeathOT":
//                case "OASISCDeathPT":
//                    tableName = "deathathomeassessments";
//                    break;
//                case "DischargeFromAgency":
//                case "OASISCDischarge":
//                case "OASISCDischargeOT":
//                case "OASISCDischargePT":
//                case "OASISCDischargeST":
//                    tableName = "dischargefromagencyassessments";
//                    break;
//                case "NonOasisDischarge":
//                case "NonOASISDischarge":
//                    tableName = "nonoasisdischargeassessments";
//                    break;
//                case "SNAssessment":
//                case "NonOasisStartOfCare":
//                case "NonOASISStartofCare":
//                    tableName = "nonoasisstartofcareassessments";
//                    break;
//                case "SNAssessmentRecert":
//                case "NonOasisRecertification":
//                case "NonOASISRecertification":
//                    tableName = "nonoasisrecertificationassessments";
//                    break;
//                default:
//                    break;
//            }

//            return tableName;
//        }

    }
}
