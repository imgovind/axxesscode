﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;

    using SubSonic.Repository;

    public class PlanofCareRepository : IPlanofCareRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private readonly string connectionStringName;

        public PlanofCareRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            this.connectionStringName = "OasisCConnectionString";
        }

        #endregion

        //#region IPlanofCareRepository Member

        //public bool Add(PlanofCare planofCare)
        //{
        //    if (planofCare != null)
        //    {
        //        planofCare.Created = DateTime.Now;
        //        planofCare.Modified = DateTime.Now;
        //        database.Add<PlanofCare>(planofCare);
        //        return true;
        //    }
        //    return false;
        //}

        //public bool AddStandAlone(PlanofCareStandAlone planofCare)
        //{
        //    if (planofCare != null)
        //    {
        //        planofCare.IsDeprecated = false;
        //        planofCare.Created = DateTime.Now;
        //        planofCare.Modified = DateTime.Now;
        //        database.Add<PlanofCareStandAlone>(planofCare);
        //        return true;
        //    }
        //    return false;
        //}

        //public bool AddMultiple<T>(List<T> planofCares) where T : class, PlanOfCareAbstract, new()
        //{
        //    try
        //    {
        //        if (planofCares != null && planofCares.Count > 0)
        //        {
        //            database.AddMany<T>(planofCares);
        //            return true;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //    return false;
        //}

        //public bool Update(PlanofCare planofCare)
        //{
        //    bool result = false;

        //    if (planofCare != null)
        //    {
        //        planofCare.Modified = DateTime.Now;
        //        database.Update<PlanofCare>(planofCare);
        //        result = true;
        //    }
        //    return result;
        //}

        //public bool UpdateStandAlone(PlanofCareStandAlone planofCare)
        //{
        //    bool result = false;

        //    if (planofCare != null)
        //    {
        //        planofCare.Modified = DateTime.Now;
        //        database.Update<PlanofCareStandAlone>(planofCare);
        //        result = true;
        //    }
        //    return result;
        //}

        //public PlanofCare Get(Guid agencyId,  Guid patientId, Guid eventId)
        //{
        //    return database.Single<PlanofCare>(p =>  p.AgencyId == agencyId && p.PatientId == patientId && p.Id == eventId);
        //}

        //public PlanofCare Get(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        //{
        //    return database.Single<PlanofCare>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
        //}

        //public PlanofCareStandAlone GetStandAlone(Guid agencyId, Guid patientId, Guid eventId)
        //{
        //    return database.Single<PlanofCareStandAlone>(p => p.AgencyId == agencyId &&  p.PatientId == patientId && p.Id == eventId );
        //}

        //public PlanofCareStandAlone GetStandAlone(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        //{
        //    return database.Single<PlanofCareStandAlone>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
        //}

      

      

        //public PlanofCare GetPlanofCareByAssessmentId(Guid agencyId, Guid episodeId, Guid assessmentId)
        //{
        //    return database.Single<PlanofCare>(p => p.AgencyId == agencyId && p.EpisodeId == episodeId && p.AssessmentId == assessmentId && p.IsDeprecated == false);
        //}

        //public PlanofCare Get(Guid agencyId, Guid episodeId, Guid assessmentId, string assessmentType)
        //{
        //    return database.Single<PlanofCare>(p => p.AgencyId == agencyId && p.EpisodeId==episodeId && p.AssessmentId == assessmentId && p.AssessmentType == assessmentType && p.IsDeprecated == false);
        //}

        //public bool MarkAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated)
        //{
        //    bool result = false;
        //    var planOfCare = database.Single<PlanofCare>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
        //    if (planOfCare != null)
        //    {
        //        planOfCare.IsDeprecated = isDeprecated;
        //        planOfCare.Modified = DateTime.Now;
        //        database.Update<PlanofCare>(planOfCare);
        //        result = true;
        //    }
        //    return result;
        //}

        //public bool MarkStandAloneAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated)
        //{
        //    bool result = false;
        //    var planOfCare = database.Single<PlanofCareStandAlone>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
        //    if (planOfCare != null)
        //    {
        //        planOfCare.IsDeprecated = isDeprecated;
        //        planOfCare.Modified = DateTime.Now;
        //        database.Update<PlanofCareStandAlone>(planOfCare);
        //        result = true;
        //    }
        //    return result;
        //}

        //public bool ReassignUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId)
        //{
        //    bool result = false;
        //    var planOfCare = database.Single<PlanofCare>(p => p.Id == eventId && p.AgencyId == agencyId && p.EpisodeId == episodeId && p.PatientId == patientId);
        //    if (planOfCare != null)
        //    {
        //        try
        //        {
        //            planOfCare.UserId = employeeId;
        //            planOfCare.Modified = DateTime.Now;
        //            database.Update<PlanofCare>(planOfCare);
        //            result = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        result = true;
        //    }
        //    return result;
        //}

        //public List<PlanofCare> GetPlanofCareByStatus(Guid agencyId, int status)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");

        //    if (status > 0)
        //    {
        //        return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.Status == status && p.IsDeprecated == false).ToList();
        //    }
        //    return new List<PlanofCare>();
        //}

        //public List<PlanofCareStandAlone> GetPlanofCareStandAloneByStatus(Guid agencyId, int status)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");

        //    if (status > 0)
        //    {
        //        return database.Find<PlanofCareStandAlone>(p => p.AgencyId == agencyId && p.Status == status && p.IsDeprecated == false).ToList();
        //    }
        //    return new List<PlanofCareStandAlone>();
        //}

        //public List<PlanofCare> GetByPhysicianId(List<Guid> physicianIdentifiers, int status)
        //{
        //    var ids = new StringBuilder();

        //    physicianIdentifiers.ForEach(i =>
        //    {
        //        ids.AppendFormat("'{0}',", i.ToString());
        //    });

        //    var query = new QueryBuilder("select * from `planofcares`")
        //        .Where(string.Format("`planofcares`.Status = {0}", status))
        //        .And("`planofcares`.IsDeprecated = 0")
        //        .And(string.Format("`planofcares`.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

        //    return new FluentCommand<PlanofCare>(query.Build())
        //        .SetConnection("OasisCConnectionString")
        //        .AsList();

        //}

        //public List<PlanofCare> GetByPhysicianIdAndDate(List<Guid> physicianIdentifiers, int status, DateTime startDate, DateTime endDate)
        //{
        //    var ids = new StringBuilder();

        //    physicianIdentifiers.ForEach(i =>
        //    {
        //        ids.AppendFormat("'{0}',", i.ToString());
        //    });

        //    var query = new QueryBuilder("select * from `planofcares`")
        //        .Where(string.Format("`planofcares`.Status = {0}", status))
        //        .And("`planofcares`.IsDeprecated = 0")
        //        .And("DATE(`planofcares`.Created) BETWEEN DATE(@startDate) AND DATE(@endDate)")
        //        .And(string.Format("`planofcares`.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

        //    return new FluentCommand<PlanofCare>(query.Build())
        //        .SetConnection("OasisCConnectionString")
        //        .AddDateTime("startDate",startDate)
        //        .AddDateTime("endDate",endDate)
        //        .AsList();

        //}

        //public List<PlanofCareStandAlone> GetStandAloneByPhysicianId(List<Guid> physicianIdentifiers, int status)
        //{
        //    var ids = new StringBuilder();

        //    physicianIdentifiers.ForEach(i =>
        //    {
        //        ids.AppendFormat("'{0}',", i.ToString());
        //    });

        //    var query = new QueryBuilder("select * from `planofcarestandalones`")
        //        .Where(string.Format("`planofcarestandalones`.Status = {0}", status))
        //        .And("`planofcarestandalones`.IsDeprecated = 0")
        //        .And(string.Format("`planofcarestandalones`.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

        //    return new FluentCommand<PlanofCareStandAlone>(query.Build())
        //        .SetConnection("OasisCConnectionString")
        //        .AsList();

        //}

        //public List<PlanofCareStandAlone> GetStandAloneByPhysicianIdAndDate(List<Guid> physicianIdentifiers, int status, DateTime startDate, DateTime endDate)
        //{
        //    var ids = new StringBuilder();

        //    physicianIdentifiers.ForEach(i =>
        //    {
        //        ids.AppendFormat("'{0}',", i.ToString());
        //    });

        //    var query = new QueryBuilder("select * from `planofcarestandalones`")
        //        .Where(string.Format("`planofcarestandalones`.Status = {0}", status))
        //        .And("`planofcarestandalones`.IsDeprecated = 0")
        //        .And("DATE(`planofcarestandalones`.Created) BETWEEN DATE(@startDate) AND DATE(@endDate)")
        //        .And(string.Format("`planofcarestandalones`.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

        //    return new FluentCommand<PlanofCareStandAlone>(query.Build())
        //        .SetConnection("OasisCConnectionString")
        //        .AddDateTime("startDate",startDate)
        //        .AddDateTime("endDate",endDate)
        //        .AsList();

        //}

        //public List<PlanofCare> GetPlanOfCareOrders(Guid agencyId)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");
        //    return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.IsDeprecated == false).ToList();
        //}

        //public List<PlanofCare> GetPatientPlanOfCare(Guid agencyId , Guid patientId)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");
        //    return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.PatientId==patientId && p.IsDeprecated == false).ToList();
        //}

        //public List<PlanofCareStandAlone> GetPatientPlanOfCareStandAlone(Guid agencyId, Guid patientId)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");
        //    return database.Find<PlanofCareStandAlone>(p => p.AgencyId == agencyId && p.PatientId == patientId && p.IsDeprecated == false).ToList();
        //}

        //public List<PlanofCare> GetPlanofCares(Guid agencyId, int status, string orderIds)
        //{

        //    var script = string.Format(@"SELECT oasisc.planofcares.Id as Id, oasisc.planofcares.EpisodeId as EpisodeId, oasisc.planofcares.OrderNumber as OrderNumber, agencymanagement.patients.Id as PatientId , agencymanagement.patients.FirstName as FirstName, agencymanagement.patients.LastName as LastName, agencymanagement.patients.MiddleInitial as MiddleInitial, oasisc.planofcares.Status as Status , oasisc.planofcares.Created as Created , oasisc.planofcares.ReceivedDate as ReceivedDate, oasisc.planofcares.SentDate as SentDate, oasisc.planofcares.PhysicianData as PhysicianData , oasisc.planofcares.PhysicianId as PhysicianId , oasisc.planofcares.PhysicianSignatureDate as PhysicianSignatureDate " +
        //       "FROM oasisc.planofcares INNER JOIN agencymanagement.patients ON oasisc.planofcares.PatientId = agencymanagement.patients.Id " +
        //       "WHERE oasisc.planofcares.AgencyId = @agencyid AND agencymanagement.patients.IsDeprecated = 0 AND (agencymanagement.patients.Status = 1 || agencymanagement.patients.Status = 2 ) " +
        //       "AND oasisc.planofcares.IsDeprecated = 0 AND oasisc.planofcares.Id IN ( {0} ) AND  oasisc.planofcares.Status = @status", orderIds);

        //    var list = new List<PlanofCare>();
        //    using (var cmd = new FluentCommand<PlanofCare>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .AddInt("status", status)
        //        .SetMap(reader => new PlanofCare
        //        {
        //            Id = reader.GetGuid("Id"),
        //            OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EpisodeId = reader.GetGuid("EpisodeId"),
        //            Status = reader.GetInt("Status"),
        //            Created = reader.GetDateTime("Created"),
        //            ReceivedDate = reader.GetDateTime("ReceivedDate"),
        //            SentDate = reader.GetDateTime("SentDate"),
        //            PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
        //            PhysicianData = reader.GetStringNullable("PhysicianData"),
        //            PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetStringNullable("PhysicianId").ToGuid() : Guid.Empty,
        //            PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //public List<PlanofCare> GetPlanofCares(Guid agencyId, string orderIds)
        //{

        //    var script = string.Format(@"SELECT oasisc.planofcares.Id as Id, oasisc.planofcares.EpisodeId as EpisodeId, oasisc.planofcares.OrderNumber as OrderNumber, agencymanagement.patients.Id as PatientId , agencymanagement.patients.FirstName as FirstName, agencymanagement.patients.LastName as LastName, agencymanagement.patients.MiddleInitial as MiddleInitial, oasisc.planofcares.Status as Status , oasisc.planofcares.Created as Created , oasisc.planofcares.ReceivedDate as ReceivedDate, oasisc.planofcares.SentDate as SentDate, oasisc.planofcares.PhysicianData as PhysicianData , oasisc.planofcares.PhysicianId as PhysicianId  " +
        //       "FROM oasisc.planofcares INNER JOIN agencymanagement.patients ON oasisc.planofcares.PatientId = agencymanagement.patients.Id " +
        //       "WHERE oasisc.planofcares.AgencyId = @agencyid AND agencymanagement.patients.IsDeprecated = 0 AND (agencymanagement.patients.Status = 1 || agencymanagement.patients.Status = 2 ) " +
        //       "AND oasisc.planofcares.IsDeprecated = 0 AND oasisc.planofcares.Id IN ( {0} ) ", orderIds);

        //    var list = new List<PlanofCare>();
        //    using (var cmd = new FluentCommand<PlanofCare>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .SetMap(reader => new PlanofCare
        //        {
        //            Id = reader.GetGuid("Id"),
        //            OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EpisodeId = reader.GetGuid("EpisodeId"),
        //            Status = reader.GetInt("Status"),
        //            Created = reader.GetDateTime("Created"),
        //            ReceivedDate = reader.GetDateTime("ReceivedDate"),
        //            SentDate = reader.GetDateTime("SentDate"),
        //            PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
        //            PhysicianData = reader.GetStringNullable("PhysicianData"),
        //            PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid()? reader.GetStringNullable("PhysicianId").ToGuid():Guid.Empty
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //public List<PlanofCare> GetPatientPlanofCares(Guid agencyId, Guid patientId , string orderIds)
        //{

        //    var script = string.Format(@"SELECT oasisc.planofcares.Id as Id, oasisc.planofcares.EpisodeId as EpisodeId, oasisc.planofcares.OrderNumber as OrderNumber, agencymanagement.patients.Id as PatientId ,  oasisc.planofcares.Status as Status , oasisc.planofcares.Created as Created , oasisc.planofcares.ReceivedDate as ReceivedDate, oasisc.planofcares.SentDate as SentDate, oasisc.planofcares.PhysicianData as PhysicianData , oasisc.planofcares.PhysicianId as PhysicianId  " +
        //       "FROM oasisc.planofcares INNER JOIN agencymanagement.patients ON oasisc.planofcares.PatientId = agencymanagement.patients.Id  " +
        //       "WHERE oasisc.planofcares.AgencyId = @agencyid AND oasisc.planofcares.PatientId= @patientId " +
        //       "AND oasisc.planofcares.IsDeprecated = 0 AND oasisc.planofcares.Id IN ( {0} ) ", orderIds);

        //    var list = new List<PlanofCare>();
        //    using (var cmd = new FluentCommand<PlanofCare>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .AddGuid("patientId", patientId)
        //        .SetMap(reader => new PlanofCare
        //        {
        //            Id = reader.GetGuid("Id"),
        //            OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            PhysicianId = reader.GetGuid("PhysicianId"),
        //            EpisodeId = reader.GetGuid("EpisodeId"),
        //            Status = reader.GetInt("Status"),
        //            Created = reader.GetDateTime("Created"),
        //            ReceivedDate = reader.GetDateTime("ReceivedDate"),
        //            SentDate = reader.GetDateTime("SentDate"),
        //            PhysicianData = reader.GetStringNullable("PhysicianData")
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //public List<PlanofCare> GetPendingSignaturePlanofCares(Guid agencyId, string orderIds)
        //{
        //    var script = string.Format(@"SELECT oasisc.planofcares.Id as Id, oasisc.planofcares.EpisodeId as EpisodeId, oasisc.planofcares.OrderNumber as OrderNumber, agencymanagement.patients.Id as PatientId , agencymanagement.patients.FirstName as FirstName, agencymanagement.patients.LastName as LastName, oasisc.planofcares.Status as Status , oasisc.planofcares.Created as Created , oasisc.planofcares.ReceivedDate as ReceivedDate, oasisc.planofcares.SentDate as SentDate, oasisc.planofcares.PhysicianData as PhysicianData , oasisc.planofcares.PhysicianId as PhysicianId , oasisc.planofcares.PhysicianSignatureDate as PhysicianSignatureDate " +
        //       "FROM oasisc.planofcares INNER JOIN agencymanagement.patients ON oasisc.planofcares.PatientId = agencymanagement.patients.Id " +
        //       "WHERE oasisc.planofcares.AgencyId = @agencyid AND agencymanagement.patients.IsDeprecated = 0 AND (agencymanagement.patients.Status = 1 || agencymanagement.patients.Status = 2 ) " +
        //       "AND oasisc.planofcares.IsDeprecated = 0 AND oasisc.planofcares.Id IN ( {0} ) AND  ( oasisc.planofcares.Status = 130 || oasisc.planofcares.Status = 145 )", orderIds);

        //    var list = new List<PlanofCare>();
        //    using (var cmd = new FluentCommand<PlanofCare>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .SetMap(reader => new PlanofCare
        //        {
        //            Id = reader.GetGuid("Id"),
        //            OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EpisodeId = reader.GetGuid("EpisodeId"),
        //            PhysicianId = reader.GetGuid("PhysicianId"),
        //            Status = reader.GetInt("Status"),
        //            Created = reader.GetDateTime("Created"),
        //            ReceivedDate = reader.GetDateTime("ReceivedDate"),
        //            SentDate = reader.GetDateTime("SentDate"),
        //            PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
        //            PhysicianData = reader.GetStringNullable("PhysicianData"),
        //            PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")

        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //public List<PlanofCareStandAlone> GetPlanofCaresStandAloneByStatus(Guid agencyId, int status, string orderIds)
        //{
        //    var script = string.Format(@"SELECT oasisc.planofcarestandalones.Id as Id, oasisc.planofcarestandalones.EpisodeId as EpisodeId, oasisc.planofcarestandalones.OrderNumber as OrderNumber, agencymanagement.patients.Id as PatientId , agencymanagement.patients.FirstName as FirstName, agencymanagement.patients.LastName as LastName,  agencymanagement.patients.MiddleInitial as MiddleInitial, oasisc.planofcarestandalones.Status as Status , oasisc.planofcarestandalones.Created as Created , oasisc.planofcarestandalones.ReceivedDate as ReceivedDate, oasisc.planofcarestandalones.SentDate as SentDate, oasisc.planofcarestandalones.PhysicianData as PhysicianData , oasisc.planofcarestandalones.PhysicianId as PhysicianId , oasisc.planofcarestandalones.PhysicianSignatureDate as PhysicianSignatureDate " +
        //       "FROM oasisc.planofcarestandalones INNER JOIN agencymanagement.patients ON oasisc.planofcarestandalones.PatientId = agencymanagement.patients.Id " +
        //       "WHERE oasisc.planofcarestandalones.AgencyId = @agencyid AND agencymanagement.patients.IsDeprecated = 0 AND (agencymanagement.patients.Status = 1 || agencymanagement.patients.Status = 2 ) " +
        //       "AND oasisc.planofcarestandalones.IsDeprecated = 0 AND oasisc.planofcarestandalones.Id IN ( {0} ) AND  ( oasisc.planofcarestandalones.Status = @status )", orderIds);
        //    var list = new List<PlanofCareStandAlone>();
        //    using (var cmd = new FluentCommand<PlanofCareStandAlone>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .AddInt("status", status)
        //        .SetMap(reader => new PlanofCareStandAlone
        //        {
        //            Id = reader.GetGuid("Id"),
        //            OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EpisodeId = reader.GetGuid("EpisodeId"),
        //            Status = reader.GetInt("Status"),
        //            Created = reader.GetDateTime("Created"),
        //            ReceivedDate = reader.GetDateTime("ReceivedDate"),
        //            SentDate = reader.GetDateTime("SentDate"),
        //            PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
        //            PhysicianData = reader.GetStringNullable("PhysicianData"),
        //            PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetStringNullable("PhysicianId").ToGuid() : Guid.Empty,
        //            PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //public List<PlanofCareStandAlone> GetPlanofCaresStandAlones(Guid agencyId, string orderIds)
        //{
        //    var script = string.Format(@"SELECT oasisc.planofcarestandalones.Id as Id, oasisc.planofcarestandalones.EpisodeId as EpisodeId, oasisc.planofcarestandalones.OrderNumber as OrderNumber, agencymanagement.patients.Id as PatientId , agencymanagement.patients.FirstName as FirstName, agencymanagement.patients.LastName as LastName, oasisc.planofcarestandalones.Status as Status , oasisc.planofcarestandalones.Created as Created , oasisc.planofcarestandalones.ReceivedDate as ReceivedDate, oasisc.planofcarestandalones.SentDate as SentDate, oasisc.planofcarestandalones.PhysicianData as PhysicianData , oasisc.planofcarestandalones.PhysicianId as PhysicianId, oasisc.planofcarestandalones.PhysicianSignatureDate as PhysicianSignatureDate " +
        //       "FROM oasisc.planofcarestandalones INNER JOIN agencymanagement.patients ON oasisc.planofcarestandalones.PatientId = agencymanagement.patients.Id " +
        //       "WHERE oasisc.planofcarestandalones.AgencyId = @agencyid AND agencymanagement.patients.IsDeprecated = 0 AND (agencymanagement.patients.Status = 1 || agencymanagement.patients.Status = 2 ) " +
        //       "AND oasisc.planofcarestandalones.IsDeprecated = 0 AND oasisc.planofcarestandalones.Id IN ( {0} )", orderIds);
        //    var list = new List<PlanofCareStandAlone>();
        //    using (var cmd = new FluentCommand<PlanofCareStandAlone>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .SetMap(reader => new PlanofCareStandAlone
        //        {
        //            Id = reader.GetGuid("Id"),
        //            OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EpisodeId = reader.GetGuid("EpisodeId"),
        //            Status = reader.GetInt("Status"),
        //            Created = reader.GetDateTime("Created"),
        //            ReceivedDate = reader.GetDateTime("ReceivedDate"),
        //            SentDate = reader.GetDateTime("SentDate"),
        //            PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
        //            PhysicianData = reader.GetStringNullable("PhysicianData"),
        //            PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid()? reader.GetStringNullable("PhysicianId").ToGuid():Guid.Empty,
        //            PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //public List<PlanofCareStandAlone> GetPatientPlanofCaresStandAlones(Guid agencyId, Guid patientId, string orderIds)
        //{
        //    var script = string.Format(@"SELECT oasisc.planofcarestandalones.Id as Id, oasisc.planofcarestandalones.EpisodeId as EpisodeId, oasisc.planofcarestandalones.OrderNumber as OrderNumber, agencymanagement.patients.Id as PatientId ,  oasisc.planofcarestandalones.Status as Status , oasisc.planofcarestandalones.Created as Created , oasisc.planofcarestandalones.ReceivedDate as ReceivedDate, oasisc.planofcarestandalones.SentDate as SentDate, oasisc.planofcarestandalones.PhysicianData as PhysicianData , oasisc.planofcarestandalones.PhysicianId as PhysicianId " +
        //       "FROM oasisc.planofcarestandalones INNER JOIN agencymanagement.patients ON oasisc.planofcarestandalones.PatientId = agencymanagement.patients.Id " +
        //       "WHERE oasisc.planofcarestandalones.AgencyId = @agencyid AND oasisc.planofcarestandalones.PatientId = @patientId  " +
        //       "AND oasisc.planofcarestandalones.IsDeprecated = 0 AND oasisc.planofcarestandalones.Id IN ( {0} )", orderIds);
        //    var list = new List<PlanofCareStandAlone>();
        //    using (var cmd = new FluentCommand<PlanofCareStandAlone>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .AddGuid("patientId", patientId)
        //        .SetMap(reader => new PlanofCareStandAlone
        //        {
        //            Id = reader.GetGuid("Id"),
        //            OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EpisodeId = reader.GetGuid("EpisodeId"),
        //            Status = reader.GetInt("Status"),
        //            Created = reader.GetDateTime("Created"),
        //            ReceivedDate = reader.GetDateTime("ReceivedDate"),
        //            SentDate = reader.GetDateTime("SentDate"),
        //            PhysicianData = reader.GetStringNullable("PhysicianData"),
        //            PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid()? reader.GetStringNullable("PhysicianId").ToGuid():Guid.Empty
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //public List<PlanofCareStandAlone> GetPendingSignaturePlanofCaresStandAlone(Guid agencyId, string orderIds)
        //{
        //    var script = string.Format(@"SELECT oasisc.planofcarestandalones.Id as Id, oasisc.planofcarestandalones.EpisodeId as EpisodeId, oasisc.planofcarestandalones.OrderNumber as OrderNumber, agencymanagement.patients.Id as PatientId , agencymanagement.patients.FirstName as FirstName, agencymanagement.patients.LastName as LastName, oasisc.planofcarestandalones.Status as Status , oasisc.planofcarestandalones.Created as Created , oasisc.planofcarestandalones.ReceivedDate as ReceivedDate, oasisc.planofcarestandalones.SentDate as SentDate, oasisc.planofcarestandalones.PhysicianData as PhysicianData , oasisc.planofcarestandalones.PhysicianId as PhysicianId , oasisc.planofcarestandalones.PhysicianSignatureDate as PhysicianSignatureDate " +
        //       "FROM oasisc.planofcarestandalones INNER JOIN agencymanagement.patients ON oasisc.planofcarestandalones.PatientId = agencymanagement.patients.Id " +
        //       "WHERE oasisc.planofcarestandalones.AgencyId = @agencyid AND agencymanagement.patients.IsDeprecated = 0 AND (agencymanagement.patients.Status = 1 || agencymanagement.patients.Status = 2 ) " +
        //       "AND oasisc.planofcarestandalones.IsDeprecated = 0 AND oasisc.planofcarestandalones.Id IN ( {0} ) AND  ( oasisc.planofcarestandalones.Status = 130 || oasisc.planofcarestandalones.Status = 145 )", orderIds);

        //    var list = new List<PlanofCareStandAlone>();
        //    using (var cmd = new FluentCommand<PlanofCareStandAlone>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("agencyid", agencyId)
        //        .SetMap(reader => new PlanofCareStandAlone
        //        {
        //            Id = reader.GetGuid("Id"),
        //            OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EpisodeId = reader.GetGuid("EpisodeId"),
        //            PhysicianId =reader.GetGuid("PhysicianId"),
        //            Status = reader.GetInt("Status"),
        //            Created = reader.GetDateTime("Created"),
        //            ReceivedDate = reader.GetDateTime("ReceivedDate"),
        //            SentDate = reader.GetDateTime("SentDate"),
        //            PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
        //            PhysicianData = reader.GetStringNullable("PhysicianData"),
        //            PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //public List<PlanofCare> GetAllPlanOfCareOrders()
        //{
        //    return database.All<PlanofCare>().ToList();
        //}

        //public List<PlanofCareStandAlone> GetAllPlanofCareStandAlones()
        //{
        //    return database.All<PlanofCareStandAlone>().ToList();
        //}

        //#endregion


        public bool Add(PlanofCare planofCare)
        {
            if (planofCare != null)
            {
                planofCare.Created = DateTime.Now;
                planofCare.Modified = DateTime.Now;
                database.Add(planofCare);
                return true;
            }
            return false;
        }

        public bool AddMultiple(List<PlanofCare> planofCares)
        {
            try
            {
                if (planofCares != null && planofCares.Count > 0)
                {
                    database.AddMany(planofCares);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool Update(PlanofCare planofCare)
        {
            bool result = false;
            try
            {
                if (planofCare != null)
                {
                    planofCare.Modified = DateTime.Now;
                    result = database.Update(planofCare) > 0;
                }
            }
            catch (Exception)
            {

            }
            return result;
        }

        public PlanofCare Get(Guid agencyId, Guid patientId, Guid eventId)
        {
            return database.Single<PlanofCare>(p => p.Id == eventId && p.AgencyId == agencyId && p.PatientId == patientId);
        }

        public List<PlanofCare> GetPatientPlanOfCare(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<PlanofCare>(p => p.AgencyId == agencyId && p.PatientId == patientId && p.IsDeprecated == false).ToList();
        }

        public bool MarkAsDeleted(Guid agencyId, Guid patientId, Guid eventId, bool isDeprecated)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"UPDATE planofcares poc set poc.IsDeprecated = {0}, poc.Modified = @modified WHERE poc.AgencyId = @agencyid AND poc.PatientId = @patientid AND poc.Id = @eventid", isDeprecated ? 1 : 0);
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("eventid", eventId)
                    .AddDateTime("modified", DateTime.Now)
                    .AsNonQuery();
                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }

            return result;
        }

        public bool ReassignUser(Guid agencyId, Guid patientId, Guid eventId, Guid employeeId)
        {
            bool result = false;
            try
            {
                var script = "UPDATE planofcares poc set poc.UserId = @employeeId, poc.Modified = @modified WHERE poc.AgencyId = @agencyid AND poc.Id = @eventid";
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("employeeId", employeeId)
                    .AddGuid("eventid", eventId)
                    .AddDateTime("modified", DateTime.Now)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public List<PlanofCare> GetPlanofCaresByStatus(Guid agencyId, int status, string orderIds)  //GetPlanofCaresStandAloneByStatus
        {
            var script = string.Format(@"SELECT 
                                    poc.Id as Id, 
                                    poc.EpisodeId as EpisodeId,
                                    poc.OrderNumber as OrderNumber, 
                                    pr.Id as PatientId ,
                                    pr.FirstName as FirstName,
                                    pr.LastName as LastName, 
                                    pr.MiddleInitial as MiddleInitial, 
                                    poc.Status as Status ,
                                    poc.Created as Created ,
                                    poc.ReceivedDate as ReceivedDate, 
                                    poc.SentDate as SentDate, 
                                    poc.PhysicianData as PhysicianData ,
                                    poc.PhysicianId as PhysicianId ,
                                    poc.PhysicianSignatureDate as PhysicianSignatureDate 
                                        FROM 
                                            planofcares poc INNER JOIN patientprofiles pr ON poc.PatientId = pr.Id 
                                                WHERE
                                                    poc.AgencyId = @agencyid AND
                                                    pr.Status IN (1,2) AND 
                                                    poc.IsDeprecated = 0 AND 
                                                    poc.Id IN ( {0} ) AND
                                                    poc.Status = @status", orderIds);
            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    Created = reader.GetDateTime("Created"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetStringNullable("PhysicianId").ToGuid() : Guid.Empty,
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public PlanofCare GetPlanOfCareByAssessment(Guid agencyId, Guid assessmentId)
        {
            return database.Single<PlanofCare>(p => p.AgencyId == agencyId && p.AssessmentId == assessmentId  && p.IsDeprecated == false);
        }

        public PlanofCare GetPlanOfCareByAssessment(Guid agencyId, Guid episodeId, Guid assessmentId)
        {
            return database.Single<PlanofCare>(p => p.AgencyId == agencyId && p.EpisodeId == episodeId && p.AssessmentId == assessmentId && p.IsDeprecated == false);
        }

        public bool RemoveFully(Guid agencyId, Guid patientId, Guid Id)
        {
            try
            {
                var script = "DELETE FROM planofcares WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("id", Id).AsNonQuery() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<PlanofCare> GetPlanOfCareOrderFromScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int[] disciplineTasks, int[] status)
        {
           
            string branchScript = string.Empty;
            if (!branchId.IsEmpty())
            {
                branchScript = " AND pr.AgencyLocationId = @branchId ";
            }

            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} )", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (status != null && status.Length > 0)
            {
                additionalScript += string.Format(" AND st.Status IN ( {0} )", status.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.EventDate as EventDate ,
                        st.Status as Status ,
                        poc.PhysicianData as PhysicianData ,
                        poc.PhysicianId as PhysicianId,
                        poc.ReceivedDate as ReceivedDate,
                        poc.SentDate as SentDate, 
                        poc.PhysicianSignatureDate as PhysicianSignatureDate ,
                        poc.IsStandAlone as IsStandAlone,
                        poc.IsNonOasis as IsNonOasis
                        pr.FirstName as FirstName,
                        pr.LastName as LastName, 
                        pr.MiddleInitial as MiddleInitial
                            FROM 
                                oasisc.planofcares poc
                                   INNER JOIN agencymanagement.scheduletasks st
                                    INNER JOIN agencymanagement.patients  pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            pr.AgencyId = @agencyid  AND 
                                            pr.Status IN (1,2) {0} AND
                                            st.IsDeprecated = 0  AND 
                                            st.IsActive = 1  AND
                                            st.IsDischarged = 0  AND
                                            DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND 
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  {1} ",
                                                                                                                            branchScript,
                                                                                                                             additionalScript);

            var scheduleList = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    OrderNumber = (long)reader.GetDouble("OrderNumber"),
                    SentDate = reader.GetDateTime("SentDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    Status = reader.GetInt("Status", 0),
                    Created = reader.GetDateTime("EventDate"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial(),
                    PhysicianData = reader.GetStringNullable("PhysicianData"),
                    PhysicianId = reader.GetStringNullable("PhysicianId").IsNotNullOrEmpty() && reader.GetStringNullable("PhysicianId").IsGuid() ? reader.GetStringNullable("PhysicianId").ToGuid() : Guid.Empty,
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate"),
                    IsStandAlone = reader.GetBoolean("IsStandAlone"),
                    IsNonOasis = reader.GetBoolean("IsNonOasis")
                })
                .AsList();
            }
            return scheduleList;

             //Id = poc.Id,
             //                       Type = OrderType.HCFA485,
             //                       Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
             //                       PatientName = poc.PatientName,
             //                       PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
             //                       CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,


               
             //   poc.Status as Status ,
             //   poc.Created as Created , 
                
        }



        public List<PlanofCare> GetPlanofCareOrders(Guid agencyId, List<Guid> branchIds, List<int> disciplineTasks, List<int> status, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                                            st.Id as Id ,
                                            st.PatientId as PatientId ,
                                            st.EpisodeId as EpisodeId ,
                                            st.UserId as UserId ,
                                            st.Status as Status ,
                                            st.EventDate as EventDate,
                                            po.PhysicianId as PhysicianId, 
                                            po.OrderNumber as OrderNumber, 
                                            po.ReceivedDate as ReceivedDate,
                                            po.SentDate as SentDate, 
                                            po.PhysicianSignatureDate as PhysicianSignatureDate ,
                                            p.FirstName as FirstName, 
                                            p.LastName as LastName, 
                                            p.MiddleInitial as MiddleInitial
                                                FROM
                                                    agencymanagement.patients p st 
                                                        INNER JOIN  oasisc.planofcares  po ON po.PatientId = p.Id 
                                                                INNER JOIN  agencymanagement.scheduletasks ON st.Id = po.Id
                                                                    WHERE 
                                                                         p.AgencyId = @agencyid AND 
                                                                         p.AgencyLocationId IN({0}) AND 
                                                                         p.Status IN (1,2) AND 
                                                                         st.IsDeprecated = 0  AND
                                                                         st.IsActive = 1  AND
                                                                         st.IsDischarged = 0  AND 
                                                                         st.Status IN ( {1} ) AND     
                                                                         st.DisciplineTask IN ( {2} ) AND
                                                                         DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                                                         DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                                                         po.OrderDate >= @startDate AND 
                                                                         po.OrderDate <= @endDate ", branchIds.ToCommaSeperatedList(),status.ToCommaSeperatedList(),disciplineTasks.ToCommaSeperatedList());
            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    Status = reader.GetInt("Status"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    Created = reader.GetDateTime("EventDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate"),
                    IsStandAlone = reader.GetBoolean("IsStandAlone"),
                    IsNonOasis = reader.GetBoolean("IsNonOasis"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase() + " " + reader.GetStringNullable("MiddleInitial").ToInitial()
                })
                .AsList();
            }
            return list;
        }


        public List<PlanofCare> GetPatientPlanofCareOrders(Guid agencyId, Guid patientId, List<int> disciplineTasks, List<int> optionalStatus, DateTime startDate, DateTime endDate)
        {

            var statusScript = string.Empty;
            if (optionalStatus.IsNotNullOrEmpty())
            {
                statusScript = " AND st.Status IN ( "+optionalStatus.ToCommaSeperatedList()+" ) ";
            }
            var script = string.Format(@"SELECT 
                                            st.Id as Id ,
                                            st.EpisodeId as EpisodeId ,
                                            st.UserId as UserId ,
                                            st.Status as Status ,
                                            st.EventDate as EventDate,
                                            po.PhysicianId as PhysicianId, 
                                            po.OrderNumber as OrderNumber, 
                                            po.ReceivedDate as ReceivedDate,
                                            po.SentDate as SentDate, 
                                            po.PhysicianSignatureDate as PhysicianSignatureDate
                                                FROM
                                                    agencymanagement.scheduletasks st 
                                                        INNER JOIN  oasisc.planofcares  po ON st.Id = po.Id
                                                                    WHERE 
                                                                         st.AgencyId = @agencyid AND 
                                                                         st.PatientId = @patientId AND 
                                                                         st.IsDeprecated = 0  AND
                                                                         st.IsActive = 1  AND
                                                                         st.IsDischarged = 0  {0} AND     
                                                                         st.DisciplineTask IN ( {1} ) AND
                                                                         DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                                                         DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                                                         po.Created >= @startDate AND 
                                                                         po.Created <= @endDate ", statusScript, disciplineTasks.ToCommaSeperatedList());
            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                  .AddGuid("patientid", patientId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    Status = reader.GetInt("Status"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    Created = reader.GetDateTime("EventDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate"),
                    IsStandAlone = reader.GetBoolean("IsStandAlone"),
                    IsNonOasis = reader.GetBoolean("IsNonOasis")
                })
                .AsList();
            }
            return list;
        }


        public List<PlanofCare> GetCurrentAndPreviousPlanofCareOrders(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.Status as Status ,
                        st.EventDate as EventDate,
                        po.PhysicianId as PhysicianId, 
                        po.OrderNumber as OrderNumber, 
                        po.ReceivedDate as ReceivedDate,
                        po.SentDate as SentDate, 
                        po.PhysicianSignatureDate as PhysicianSignatureDate 
                            FROM
                                 agencymanagement.scheduletasks st 
                                    INNER JOIN  oasisc.planofcares  po ON st.Id = po.Id
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND
                                            st.IsMissedVisit = 0 AND
                                            st.IsDeprecated = 0 AND 
                                            st.IsActive = 1 AND
                                            st.IsDischarged = 0 AND 
                                            (( st.EpisodeId = @episodeid AND st.IsOrderForNextEpisode = 0 ) OR (st.EpisodeId != @episodeid AND  DATE(st.EndDate) = DATE(@previousenddate) AND st.IsOrderForNextEpisode = 1 )) AND 
                                            DATE(st.EventDate) between  DATE(st.StartDate) and DATE(st.EndDate) AND
                                            st.DisciplineTask IN ( {0} )",disciplineTasks.ToCommaSeperatedList());

            var list = new List<PlanofCare>();
            using (var cmd = new FluentCommand<PlanofCare>(script))
            {
                list = cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("previousenddate", startDate.AddDays(-1))
                .SetMap(reader => new PlanofCare
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    Status = reader.GetInt("Status"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate"),
                    Created = reader.GetDateTime("EventDate"),
                    IsStandAlone = reader.GetBoolean("IsStandAlone"),
                    IsNonOasis = reader.GetBoolean("IsNonOasis")
                })
                .AsList();
            }
            return list;

        }
    }
}
