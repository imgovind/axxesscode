﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;
    using Extensions;

    using SubSonic.Repository;
    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Common;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;

    public class AssessmentRepository : IAssessmentRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private readonly string connectionStringName;

        public AssessmentRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            this.connectionStringName = "OasisCConnectionString";
        }

        #endregion

        #region IAssessmentRepository Members

        //public bool Add(Assessment assessment)
        //{
        //    if (assessment != null)
        //    {
        //        if (assessment.Questions != null)
        //        {
        //            assessment.OasisData = assessment.Questions.ToXml();
        //        }
        //        assessment.Created = DateTime.Now;
        //        assessment.Modified = DateTime.Now;
        //        database.InsertAny(assessment);
        //        return true;
        //    }
        //    return false;
        //}

        //public bool Update(Assessment assessment)
        //{
        //    bool result = false;
        //    if (assessment != null)
        //    {
        //        if (assessment.Questions != null)
        //        {
        //            assessment.OasisData = assessment.Questions.ToXml();
        //        }
        //        assessment.Modified = DateTime.Now;
        //        database.UpdateAny(assessment);
        //        var data = new OASISBackup();
        //        data.Id = Guid.NewGuid();
        //        data.AgencyId = assessment.AgencyId;
        //        data.AssessmentId = assessment.Id;
        //        data.Data = assessment.OasisData;
        //        data.Created = DateTime.Now;
        //        database.AddOASISBackup(data);
        //        result = true;
        //    }
        //    return result;
        //}

        //public bool UpdateModal(Assessment assessment)
        //{
        //    bool result = false;
        //    if (assessment != null)
        //    {
        //        assessment.Modified = DateTime.Now;
        //        database.UpdateAny(assessment);
        //        result = true;
        //    }
        //    return result;
        //}

        //public Assessment Get(Guid assessmentId, string assessmentType, Guid agencyId)
        //{
        //    Assessment assessment = null;
        //    if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
        //    {
        //        assessment = database.FindAny(assessmentType, assessmentId, agencyId);
        //        if (assessment != null)
        //        {
        //            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
        //        }
        //    }
        //    return assessment;
        //}

        //public bool MarkAsDeleted(Guid agencyId, Guid assessmentId, Guid episodeId, Guid patientId, string assessmentType, bool isDeprecated)
        //{
        //    bool result = false;
        //    if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
        //    {
        //        var assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
        //        if (assessment != null)
        //        {
        //            assessment.IsDeprecated = isDeprecated;
        //            assessment.Modified = DateTime.Now;
        //            database.UpdateAny(assessment);
        //            result = true;
        //        }
        //    }
        //    return result;
        //}

        //public bool ReassignUser(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, Guid employeeId, string assessmentType)
        //{
        //    bool result = false;
        //    if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !employeeId.IsEmpty())
        //    {
        //        var assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
        //        if (assessment != null)
        //        {
        //            try
        //            {
        //                assessment.UserId = employeeId;
        //                assessment.Modified = DateTime.Now;
        //                database.UpdateAny(assessment);
        //                result = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                return false;
        //            }
        //        }
        //        else
        //        {
        //            result = true;
        //        }
                
        //    }
        //    return result;
        //}

        //public Assessment Get(Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType, Guid agencyId)
        //{
        //    Assessment assessment = null;
        //    if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
        //    {
        //        assessment = database.FindAny(agencyId, assessmentId, PatientId, EpisodeId, assessmentType);
        //        if (assessment != null)
        //        {
        //            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
        //        }
        //    }
        //    return assessment;
        //}

        //public Assessment GetAssessmentOnly(Guid agencyId,Guid assessmentId, string assessmentType )
        //{
        //    Assessment assessment = null;
        //    if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
        //    {
        //        assessment = database.FindAny(assessmentType, assessmentId, agencyId);
        //    }
        //    return assessment;
        //}

        //public Assessment GetAssessmentOnly(Guid agencyId, Guid episodeId, Guid patientId,Guid assessmentId, string assessmentType)
        //{
        //    Assessment assessment = null;
        //    if (!agencyId.IsEmpty() && !assessmentId.IsEmpty()&& !patientId.IsEmpty() && !episodeId.IsEmpty() && assessmentType.IsNotNullOrEmpty())
        //    {
        //        assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
        //    }
        //    return assessment;
        //}

        //public List<Assessment> GetAssessmentSupplies(Guid agencyId, Guid patientId, string assessmentType, List<Guid> assessmentIds)
        //{
        //    return database.GetAssessmentSupplies(agencyId, patientId, assessmentType, assessmentIds);
        //}

        //public List<Assessment> GetAllByStatus(Guid agencyId, int status)
        //{
        //    var assessments = new List<Assessment>();
        //    if (!agencyId.IsEmpty() && status > 0)
        //    {
        //        foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
        //        {
        //            var data = database.FindAnyByStatus(agencyId, assessmentType.ToString(), status);
        //            if (data != null && data.Count > 0)
        //            {
        //                assessments.AddRange(data);
        //            }
        //        }
        //    }
        //    return assessments;
        //}

        //public List<Assessment> GetOnlyCMSOasisByStatus(Guid agencyId, int status)
        //{
        //    var assessments = new List<Assessment>();
        //    if (!agencyId.IsEmpty() && status > 0)
        //    {
        //        foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
        //        {
        //            if (assessmentType == AssessmentType.PlanOfCare485 || assessmentType == AssessmentType.NonOasisDischarge || assessmentType == AssessmentType.NonOasisRecertification || assessmentType == AssessmentType.NonOasisStartOfCare)
        //            {
        //                continue;
        //            }
        //            var data = database.FindAnyByStatus(agencyId, assessmentType.ToString(), status);
        //            if (data != null && data.Count > 0)
        //            {
        //                assessments.AddRange(data);
        //            }
        //        }
        //    }
        //    return assessments;
        //}

        //public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, int patientStatus, DateTime startDate, DateTime endDate)
        //{
        //    var assessments = new List<AssessmentExport>();
        //    if (!agencyId.IsEmpty() && status > 0)
        //    {
        //        foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
        //        {
        //            if (assessmentType == AssessmentType.PlanOfCare485 || assessmentType == AssessmentType.NonOasisDischarge || assessmentType == AssessmentType.NonOasisRecertification || assessmentType == AssessmentType.NonOasisStartOfCare)
        //            {
        //                continue;
        //            }
        //            var data = database.FindAnyByStatusLean(agencyId,branchId, assessmentType.ToString(), status,patientStatus, startDate, endDate);

        //            if (data != null && data.Count > 0)
        //            {
        //                assessments.AddRange(data);
        //            }
        //        }
        //    }
        //    return assessments;
        //}

        //public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, List<int> paymentSources)
        //{
        //    var assessments = new List<AssessmentExport>();
        //    if (!agencyId.IsEmpty() && status > 0)
        //    {
        //        foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
        //        {
        //            if (assessmentType == AssessmentType.PlanOfCare485 || assessmentType == AssessmentType.NonOasisDischarge || assessmentType == AssessmentType.NonOasisRecertification || assessmentType == AssessmentType.NonOasisStartOfCare)
        //            {
        //                continue;
        //            }
        //            var data = database.FindAnyByStatusLean(agencyId, branchId, assessmentType.ToString(), status, paymentSources);
        //            if (data != null && data.Count > 0)
        //            {
        //                assessments.AddRange(data);
        //            }
        //        }
        //    }
        //    return assessments;
        //}

        //public List<Assessment> GetPreviousAssessments(Guid agencyId, Guid patientId, AssessmentType assessmentType)
        //{
        //    var previousAssessments = new List<Assessment>();
        //    if (!agencyId.IsEmpty() && !patientId.IsEmpty())
        //    {
        //        previousAssessments = database.FindMany(agencyId, patientId, assessmentType.ToString()).OrderByDescending(a => a.AssessmentDate).ToList();
        //    }
        //    return previousAssessments;
        //}

       

       
        //private string IdentifyAssessmentNumber(string assessmentType)
        //{
        //    string number = "00";
        //    switch (assessmentType)
        //    {
        //        case "StartOfCare":
        //        case "OASISCStartofCare":
        //        case "OASISCStartofCarePT":
        //        case "OASISCStartofCareOT":
        //            number = "01";
        //            break;
        //        case "ResumptionOfCare":
        //        case "OASISCResumptionofCare":
        //        case "OASISCResumptionofCareOT":
        //        case "OASISCResumptionofCarePT":
        //            number = "03";
        //            break;
        //        case "FollowUp":
        //        case "OASISCFollowUp":
        //        case "OASISCFollowUpOT":
        //        case "OASISCFollowUpPT":
        //            number = "05";
        //            break;
        //        case "Recertification":
        //        case "OASISCRecertification":
        //        case "OASISCRecertificationOT":
        //        case "OASISCRecertificationPT":
        //        case "OASISCRecertificationST":
        //            number = "04";
        //            break;
        //        case "OASISCTransfer":
        //        case "OASISCTransferPT":
        //        case "OASISCTransferOT":
        //        case "TransferInPatientNotDischarged":
        //            number = "06";
        //            break;
        //        case "OASISCTransferDischarge":
        //        case "TransferInPatientDischarged":
        //            number = "07";
        //            break;
        //        case "DischargeFromAgencyDeath":
        //        case "OASISCDeath":
        //        case "OASISCDeathOT":
        //        case "OASISCDeathPT":
        //            number = "08";
        //            break;
        //        case "DischargeFromAgency":
        //        case "OASISCDischarge":
        //        case "OASISCDischargeOT":
        //        case "OASISCDischargePT":
        //        case "OASISCDischargeST":
        //            number = "09";
        //            break;
        //        case "NonOasisStartOfCare":
        //        case "NonOASISStartofCare":
        //            number = "10";
        //            break;
        //        case "NonOasisRecertification":
        //        case "NonOASISRecertification":
        //            number = "10";
        //            break;
        //        case "NonOasisDischarge":
        //        case "NonOASISDischarge":
        //            number = "10";
        //            break;
        //        default:
        //            break;
        //    }
        //    return number;
        //}

        #endregion



        #region IAssessmentRepository Members

        public bool InsertAny<T>(T data) where T : class, IAssessment, new()
        {
            var result = false;
            try
            {
                if (data != null)
                {
                    data.Created = DateTime.Now;
                    data.Modified = DateTime.Now;
                    database.Add<T>(data);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool InsertAnyMultiple<T>(List<T> datas) where T : class, IAssessment, new()
        {
            var result = false;
            try
            {
                if (datas != null && datas.Count > 0)
                {
                    database.AddMany<T>(datas);
                    result = true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateAny<T>(T data) where T : class, IAssessment, new()
        {
            var result = false;
            if (data != null)
            {

                data.Modified = DateTime.Now;
                result = database.Update<T>(data) >= 1;
                //throw new Exception();

            }
            return result;
        }

        public bool RemoveModel<T>(Guid Id) where T : class, new()
        {
            var result = false;
            try
            {
                result = database.Delete<T>(Id) > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public T FindAny<T>(Guid agencyId, Guid assessmentId) where T : class, IAssessment, new()
        {
            var assessment = database.Single<T>(e => e.AgencyId == agencyId && e.Id == assessmentId);
            if (assessment != null)
            {
                assessment.Type = AssessmentTypeFactory.ToAssessmentTypeFromTaskName(assessment.Type.ToString());
            }
            return assessment;
        }

        public T FindAny<T>(Guid agencyId, Guid patientId, Guid assessmentId) where T : class, IAssessment, new()
        {
            var assessment = database.Single<T>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == assessmentId);
            if (assessment != null)
            {
                assessment.Type = AssessmentTypeFactory.ToAssessmentTypeFromTaskName(assessment.Type.ToString());
            }
            return assessment;
        }

        public T FindAny<T>(Guid agencyId, Guid assessmentId, Guid patientId, Guid episodeId) where T : class, IAssessment, new()
        {
            var assessment = database.Single<T>(a => a.AgencyId == agencyId && a.Id == assessmentId && a.PatientId == patientId && a.EpisodeId == episodeId);
            if (assessment != null)
            {
                assessment.Type = AssessmentTypeFactory.ToAssessmentTypeFromTaskName(assessment.Type.ToString());
            }
            return assessment;
        }

        public List<AssessmentExport> FindAnyByStatusLean(Guid agencyId, Guid branchId, string[] assessmentTypes, int status, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var assessments = new List<AssessmentExport>();

            var patientStatusQuery = string.Empty;
            if (patientStatus <= 0)
            {
                patientStatusQuery = " AND pr.Status IN (1,2) ";
            }
            else
            {
                patientStatusQuery = " AND pr.Status = @patientstatusid ";
            }

            var dateRange = string.Empty;
            if (status == (int)ScheduleStatus.OasisExported || status == (int)ScheduleStatus.OasisCompletedNotExported)
            {
                dateRange = " AND ast.ExportedDate between @startdate and @enddate ";
            }
            else if (status == (int)ScheduleStatus.OasisCompletedExportReady)
            {
                dateRange = " AND ast.AssessmentDate between @startdate and @enddate ";
            }
            var branchScript = !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty;

            var script = string.Format(@"SELECT 
                                pr.Id as PatientId, 
                                pr.FirstName as FirstName, 
                                pr.LastName as LastName, 
                                pr.PrimaryInsurance as InsuranceId , 
                                ast.Id  as Id , 
                                ast.VersionNumber as VersionNumber, 
                                ast.AssessmentDate as AssessmentDate , 
                                ast.Type as Type ,
                                ast.Modified as Modified , 
                                ast.ExportedDate as ExportedDate , 
                                ast.EpisodeId as EpisodeId, 
                                st.EndDate as EndDate, 
                                st.StartDate as StartDate ,
                                st.EventDate as EventDate ,
                                st.VisitDate as VisitDate 
                                        FROM oasisc.assessments ast
                                                INNER JOIN agencymanagement.patients pr ON ast.PatientId = pr.Id
                                                INNER JOIN agencymanagement.scheduletasks st ON  ast.Id = st.Id
                                                        WHERE 
                                                              pr.AgencyId = @agencyid {0} {1} AND
                                                              pr.IsDeprecated = 0 AND
                                                              st.IsDischarged = 0 AND
                                                              st.IsActive = 1 AND 
                                                              st.IsMissedVisit = 0 AND
                                                              st.IsDeprecated = 0 AND
                                                              DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                                              ast.Type IN ( {3} ) {2} AND 
                                                              ast.Status = @status ", branchScript, patientStatusQuery, dateRange, assessmentTypes.Select(d => "\'" + d + "\'").ToArray().Join(","));

            using (var cmd = new FluentCommand<AssessmentExport>(script))
            {
                assessments = cmd.SetConnection(connectionStringName)
                   .AddGuid("agencyid", agencyId)
                   .AddGuid("branchId", branchId)
                   .AddInt("status", status)
                   .AddInt("patientstatusid", patientStatus)
                   .AddDateTime("startDate", startDate)
                   .AddDateTime("endDate", endDate)
                   .SetMap(reader => new AssessmentExport
                   {
                       AssessmentId = reader.GetGuid("Id"),
                       PatientId = reader.GetGuid("PatientId"),
                       EpisodeId = reader.GetGuid("EpisodeId"),
                       AssessmentDate = reader.GetDateTime("AssessmentDate"),
                       EpisodeStartDate = reader.GetDateTime("StartDate"),
                       EpisodeEndDate = reader.GetDateTime("EndDate"),
                       EventDate = reader.GetDateTime("EventDate"),
                       VisitDate = reader.GetDateTime("VisitDate"),
                       AssessmentType = reader.GetStringNullable("Type"),
                       CorrectionNumber = reader.GetInt("VersionNumber"),
                       PatientName = string.Format("{0}, {1}", reader.GetString("LastName").ToUpperCase(), reader.GetString("FirstName").ToUpperCase()),
                       InsuranceId = reader.GetIntNullable("InsuranceId") != null ? reader.GetInt("InsuranceId") : -1,
                       ExportedDate = reader.GetDateTime("ExportedDate").Date > DateTime.MinValue ? reader.GetDateTime("ExportedDate") : reader.GetDateTime("Modified")

                   }).AsList();
            }
            return assessments;
        }

        public List<AssessmentExport> FindAnyByStatusLean(Guid agencyId, Guid branchId, string[] assessmentTypes, int status, List<int> paymentSources)
        {
            var results = new List<AssessmentExport>();
            if (paymentSources != null && paymentSources.Count > 0)
            {
                var branchScript = !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId " : string.Empty;
                var script = string.Format(
                    @"SELECT 
                        pr.Id as PatientId,
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName, 
                        pr.PrimaryInsurance as InsuranceId, 
                        pr.PaymentSource as PaymentSources,
                        ast.Id  as Id,
                        ast.VersionNumber as VersionNumber,
                        ast.AssessmentDate as AssessmentDate, 
                        ast.Modified as Modified, 
                        ast.ExportedDate as ExportedDate, 
                        ast.EpisodeId as EpisodeId, 
                        ast.Type as Type,
                        (ast.SubmissionFormat Is NULL || ast.SubmissionFormat = '') as IsSubmissionEmpty, 
                        ast.IsValidated as IsValidated, 
                        st.EndDate as EndDate, 
                        st.StartDate as StartDate, 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate 
                                FROM oasisc.assessments ast
                                    INNER JOIN agencymanagement.patients pr ON ast.PatientId = pr.Id
                                    INNER JOIN agencymanagement.scheduletasks st ON  ast.Id = st.Id
                                            WHERE
                                                  pr.AgencyId = @agencyid  {0} AND 
                                                  pr.Status IN (1,2) AND 
                                                  pr.PaymentSource REGEXP '{1}' AND 
                                                  pr.IsDeprecated = 0 AND 
                                                  st.IsDischarged = 0 AND 
                                                  st.IsActive = 1 AND 
                                                  st.IsMissedVisit = 0 AND
                                                  st.IsDeprecated = 0 AND
                                                  DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                                  ast.Type IN ( {2} ) AND 
                                                  ast.Status = @status ", branchScript, paymentSources.Select(d => "[[:<:]]" + d + "[[:>:]]").ToArray().Join("|"), assessmentTypes.Select(d => "\'" + d + "\'").ToArray().Join(","));

                using (var cmd = new FluentCommand<AssessmentExport>(script))
                {
                    results = cmd.SetConnection(connectionStringName)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .SetMap(reader => new AssessmentExport
                       {
                           AssessmentId = reader.GetGuid("Id"),
                           PatientId = reader.GetGuid("PatientId"),
                           EpisodeId = reader.GetGuid("EpisodeId"),
                           AssessmentDate = reader.GetDateTime("AssessmentDate"),
                           EpisodeStartDate = reader.GetDateTime("StartDate"),
                           EpisodeEndDate = reader.GetDateTime("EndDate"),
                           EventDate = reader.GetDateTime("EventDate"),
                           VisitDate = reader.GetDateTime("VisitDate"),
                           AssessmentType = reader.GetString("Type"),
                           IsValidated = reader.GetBoolean("IsValidated"),
                           IsSubmissionEmpty = reader.GetBoolean("IsSubmissionEmpty"),
                           PaymentSources = reader.GetStringNullable("PaymentSources"),
                           CorrectionNumber = reader.GetInt("VersionNumber"),
                           PatientName = string.Format("{0}, {1}", reader.GetString("LastName").ToUpperCase(), reader.GetString("FirstName").ToUpperCase()),
                           InsuranceId = reader.GetIntNullable("InsuranceId") != null ? reader.GetInt("InsuranceId") : -1,
                           ExportedDate = reader.GetDateTime("ExportedDate").Date > DateTime.MinValue ? reader.GetDateTime("ExportedDate") : reader.GetDateTime("Modified")

                       }).AsList();
                }
            }
            return results;



        }


        public Assessment GetEpisodeAssessment(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, bool isNoneOASISIncluded)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.IsBillable as IsBillable ,
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EndDate as EndDate,
                        st.StartDate as StartDate,
                        ast.OasisData as OasisData,
                        ast.ClaimKey as ClaimKey,
                        ast.HippsCode as HippsCode,
                        ast.HippsVersion as HippsVersion
                            FROM
                               agencymanagement.scheduletasks st
                                  INNER JOIN oasisc.assessments ast ON ast.Id = st.Id
                                    WHERE
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid AND
                                        ((st.EpisodeId = @episodeid AND DATE(st.EventDate) between DATE(st.StartDate) and DATE(@maxstartofcaredate) AND st.DisciplineTask IN({0})) OR (DATE(st.EndDate) = DATE(@previousepisodeenddate) AND DATE(st.EventDate) between DATE(@minpreviousepisodeeventdate) and DATE(@previousepisodeenddate) AND st.DisciplineTask IN( {1}))) AND
                                        st.IsDeprecated = 0 AND
                                        st.IsMissedVisit = 0  AND
                                        st.IsActive = 1  AND 
                                        st.IsDischarged = 0  AND
                                        DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate)  
                                            ORDER BY DATE(st.EventDate) DESC LIMIT 1 ",DisciplineTaskFactory.SOCDisciplineTasks(isNoneOASISIncluded).ToCommaSeperatedList(), DisciplineTaskFactory.LastFiveDayAssessments(isNoneOASISIncluded).ToCommaSeperatedList());


            var scheduleEvent = new Assessment();
            using (var cmd = new FluentCommand<Assessment>(script))
            {
                scheduleEvent = cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("maxstartofcaredate", startDate)
                .AddDateTime("minpreviousepisodeeventdate", startDate.AddDays(-6))
                .AddDateTime("previousepisodeenddate", startDate.AddDays(-1))
                .SetMap(reader => new Assessment
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    ScheduleDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    OasisData = reader.GetStringNullable("OasisData"),
                    ClaimKey = reader.GetStringNullable("ClaimKey"),
                    HippsCode = reader.GetStringNullable("HippsCode"),
                    HippsVersion = reader.GetStringNullable("HippsVersion"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public List<Assessment> GetAssessmentSupplies(Guid agencyId, Guid patientId, List<Guid> assessmentIds)
        {
            var script = string.Format(@"SELECT
                        ast.Id,
                        ast.Supply 
                            FROM 
                                assessments ast
                                        WHERE 
                                            ast.AgencyId = @agencyid AND
                                            ast.PatientId = @patientid AND
                                            ast.Id IN ({0}) ", assessmentIds.ToCommaSeperatedList());

            var list = new List<Assessment>();
            using (var cmd = new FluentCommand<Assessment>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                 .AddGuid("patientid", patientId)
                .SetMap(reader => new Assessment
                {
                    Id = reader.GetGuid("Id"),
                    Supply = reader.GetStringNullable("Supply")
                })
                .AsList();

            }
            return list;
        }

        public List<Assessment> GetAssessment1446(Guid agencyId, List<Guid> assessmentIds)
        {
            var script = string.Format(@"SELECT
                        ast.Id,
                        ast.PatientId as PatientId, 
                        ast.EpisodeId as EpisodeId, 
                        ast.SubmissionFormat 
                            FROM 
                                assessments ast
                                        WHERE 
                                            ast.AgencyId = @agencyid AND
                                            ast.Id IN ({0}) ",assessmentIds.ToCommaSeperatedList());

            var list = new List<Assessment>();
            using (var cmd = new FluentCommand<Assessment>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new Assessment
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    SubmissionFormat = reader.GetStringNullable("SubmissionFormat")
                })
                .AsList();

            }
            return list;
        }


        public List<Assessment> GetOnlyNoteQuestionsString(Guid agencyId, Guid patientId, List<Guid> assessmentIds)
        {
            var script =string.Format( @"SELECT
                        ast.Id,
                        ast.PatientId as PatientId, 
                        ast.EpisodeId as EpisodeId, 
                        ast.OasisData 
                            FROM 
                                assessments ast
                                        WHERE 
                                            ast.AgencyId = @agencyid AND
                                            ast.PatientId = @patientid AND
                                            ast.Id IN ({0}) ", assessmentIds.ToCommaSeperatedList());

            var list = new List<Assessment>();
            using (var cmd = new FluentCommand<Assessment>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                 .AddGuid("patientid", patientId)
                .SetMap(reader => new Assessment
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    OasisData = reader.GetStringNullable("OasisData")
                })
                .AsList();

            }
            return list;
        }

        public IList<string> FindAssessmentFields(string assessmentType)
        {
            var results = new List<string>();
            var formats = new List<object>();
            var script = string.Format(
                @"SELECT ElementName " +
                "FROM submissionbodyformats " +
                "WHERE {0} = 1 AND IsIgnorable = 0 AND ElementName != ''", assessmentType);

            using (var cmd = new FluentCommand<object>(script))
            {
                formats = cmd.SetConnection(connectionStringName)
                   .SetMap(reader =>
                       reader.GetStringNullable("ElementName")
                   ).AsList();
            }
            if (formats != null && formats.Count > 0)
            {
                results = formats.Where(s => s.ToString().IsNotNullOrEmpty()).Select(s => s.ToString().Remove(0, 5)).ToList<string>();
                if (assessmentType == "RFA01" || assessmentType == "RFA03")
                {
                    for (int count = 1; count < 5; count++)
                    {
                        results.Add("InpatientFacilityProcedure" + count);
                    }
                    for (int count = 1; count < 7; count++)
                    {
                        results.Add("InpatientFacilityDiagnosis" + count);
                        results.Add("MedicalRegimenDiagnosis" + count);
                    }
                }
                if (assessmentType == "RFA01" || assessmentType == "RFA03" || assessmentType == "RFA05"
                    || assessmentType == "RFA04")
                {
                    results.Add("PrimaryDiagnosis");
                    results.Add("PrimaryDiagnosisDate");
                    int character = 65;
                    for (int diagnosisCount = 1; diagnosisCount < 26; diagnosisCount++)
                    {
                        results.Add("PrimaryDiagnosis" + diagnosisCount);
                        results.Add("PrimaryDiagnosis" + diagnosisCount + "Date");
                        results.Add("PaymentDiagnoses" + (char)character + "3");
                        results.Add("PaymentDiagnoses" + (char)character + "4");
                        character++;
                    }
                    character = 71;
                    for (int diagCount2 = 6; diagCount2 < 26; diagCount2++)
                    {
                        results.Add("ICD9M" + (char)character + "3");
                        results.Add("ICD9M" + (char)character + "4");
                        results.Add("ICD9M" + diagCount2);
                        results.Add("ExacerbationOrOnsetPrimaryDiagnosis" + diagCount2);
                        results.Add("OtherDiagnose" + diagCount2 + "Rating");
                        character++;
                    }
                    results.Add("PaymentDiagnosesZ3");
                    results.Add("PaymentDiagnosesZ4");
                }
                results.Add("PressureUlcerLengthDecimal");
                results.Add("PressureUlcerWidthDecimal");
                results.Add("PressureUlcerDepthDecimal");
            }
            return results;
        }


        
        public bool Add(Assessment assessment)
        {

            if (assessment != null)
            {
                if (assessment.Questions != null)
                {
                    assessment.OasisData = assessment.Questions.ToXml();
                }
                assessment.Created = DateTime.Now;
                assessment.Modified = DateTime.Now;
                return this.InsertAny(assessment);
            }
            return false;
        }

        public bool AddMultiple(List<Assessment> assessments)
        {
            if (assessments != null && assessments.Count > 0)
            {
                assessments.ForEach(a =>
                {
                    if (a.Questions != null)
                    {
                        a.OasisData = a.Questions.ToXml();
                    }
                    a.Created = DateTime.Now;
                    a.Modified = DateTime.Now;
                });
                return this.InsertAnyMultiple(assessments);
            }
            return false;
        }

        /// <summary>
        /// WARNING: THIS METHOD WILL DELETE THE ASSESSMENTS COMPLETLY FROM THE DATABASE
        /// </summary>
        /// <param name="assessments"></param>
        /// <returns></returns>
        public bool RemoveMultiple(List<Assessment> assessments)
        {
            if (assessments != null && assessments.Count > 0)
            {
                return database.DeleteMany(assessments) > 0;
            }
            return false;
        }

        public bool Update(Assessment assessment)
        {
            bool result = false;
            if (assessment != null)
            {
                if (assessment.Questions != null)
                {
                    assessment.OasisData = assessment.Questions.ToXml();
                }
                assessment.Modified = DateTime.Now;
                if (this.UpdateAny(assessment))
                {
                    var data = new OASISBackup();
                    data.Id = Guid.NewGuid();
                    data.AgencyId = assessment.AgencyId;
                    data.AssessmentId = assessment.Id;
                    data.Data = assessment.OasisData;
                    data.Created = DateTime.Now;
                    this.AddOASISBackup(data);
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateModal(Assessment assessment)
        {
            bool result = false;
            if (assessment != null)
            {
                assessment.Modified = DateTime.Now;
                return this.UpdateAny(assessment);
            }
            return result;
        }

        public Assessment Get(Guid agencyId, Guid assessmentId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && !agencyId.IsEmpty())
            {
                assessment = this.FindAny<Assessment>(agencyId, assessmentId);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
            }
            return assessment;
        }

        public Assessment Get(Guid agencyId, Guid patientId, Guid assessmentId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && !agencyId.IsEmpty())
            {
                assessment = this.FindAny<Assessment>(agencyId, patientId, assessmentId);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
            }
            return assessment;
        }

        public Assessment GetAssessmentOnly(Guid agencyId, Guid assessmentId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && !agencyId.IsEmpty())
            {
                assessment = this.FindAny<Assessment>(agencyId, assessmentId);
            }
            return assessment;
        }

        public Assessment GetAssessmentOnly(Guid agencyId, Guid patientId, Guid assessmentId)
        {
            Assessment assessment = null;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !patientId.IsEmpty())
            {
                assessment = this.FindAny<Assessment>(agencyId, patientId, assessmentId);
            }
            return assessment;
        }

        public bool MarkAsDeleted(Guid agencyId, Guid patientId, Guid assessmentId, bool isDeprecated)
        {
            bool result = false;
            try
            {
                var script = @"UPDATE assessments ast set ast.IsDeprecated = @isdeprecated, ast.Modified = @modified WHERE ast.AgencyId = @agencyid AND ast.PatientId = @patientid  AND ast.Id = @assessmentId ";
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("assessmentId", assessmentId)
                    .AddDateTime("modified", DateTime.Now)
                    .AddInt("isdeprecated", isDeprecated ? 1 : 0)
                    .AsNonQuery();
                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool ReassignUser(Guid agencyId, Guid patientId, Guid assessmentId, Guid employeeId)
        {
            bool result = false;
            try
            {
                var script = @"UPDATE assessments ast set ast.UserId = @userid, ast.Modified = @modified WHERE ast.AgencyId = @agencyid AND ast.PatientId = @patientid  AND ast.Id = @assessmentId ";
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("assessmentId", assessmentId)
                     .AddGuid("userid", employeeId)
                    .AddDateTime("modified", DateTime.Now)
                    .AsNonQuery();
                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var assessments = new List<AssessmentExport>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                var assessmentTypes = Enum.GetValues(typeof(AssessmentType)).Cast<AssessmentType>().Where(d => !AssessmentTypeFactory.NonCMSAssessments().Contains((int)d)).Select(d => d.ToString()).ToArray();
                if (assessmentTypes.IsNotNullOrEmpty())
                {
                    assessments = this.FindAnyByStatusLean(agencyId, branchId, assessmentTypes, status, patientStatus, startDate, endDate);
                }
            }
            return assessments;
        }

        public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, List<int> paymentSources)
        {
            var assessments = new List<AssessmentExport>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                var assessmentTypes = Enum.GetValues(typeof(AssessmentType)).Cast<AssessmentType>().Where(d => !AssessmentTypeFactory.NonCMSAssessments().Contains((int)d)).Select(d => d.ToString()).ToArray();
                if (assessmentTypes.IsNotNullOrEmpty())
                {
                    assessments = this.FindAnyByStatusLean(agencyId, branchId, assessmentTypes, status, paymentSources);
                }
            }
            return assessments;
        }


        public bool UsePreviousAssessment(Guid agencyId,  Guid patientId, Guid assessmentId, Guid previousAssessmentId)
        {
            bool result = false;

            var currentAssessment = Get(agencyId, patientId, assessmentId);
            if (currentAssessment != null)
            {
                var previousAssessment = Get(agencyId,patientId, previousAssessmentId);
                if (previousAssessment != null && previousAssessment.OasisData.IsNotNullOrEmpty())
                {
                    var previousAssessmentData = previousAssessment.OasisData.ToObject<List<Question>>();
                    var tempAssessmentData = new List<Question>();
                    if (previousAssessmentData != null && previousAssessmentData.Count > 0)
                    {
                        string type = AssessmentTypeFactory.AssessmentTypeNumber(currentAssessment.Type.ToString());
                        if (!type.Equals("10") && !type.Equals("00"))
                        {
                            var fields = this.FindAssessmentFields("RFA" + type);
                            if (fields != null && fields.Count > 0)
                            {
                                foreach (string field in fields)
                                {
                                    var answer = previousAssessmentData.FirstOrDefault(f => f.Name == field);

                                    if (answer != null)
                                    {
                                        tempAssessmentData.Add(answer);
                                    }
                                }
                                if (type == "01" || type == "03" || type == "04" || type == "09")
                                {
                                    previousAssessmentData.Where(f => f.Type == QuestionType.Generic || f.Type == QuestionType.PlanofCare)
                                        .ForEach(f => tempAssessmentData.Add(f));
                                }
                                else if (type == "05")
                                {
                                    var listOfGenericPlanOfCareFieldsType05 = AssessmentType05Generics();
                                    previousAssessmentData.Where(f => listOfGenericPlanOfCareFieldsType05.Contains(f.Name))
                                                                        .ForEach(f => tempAssessmentData.Add(f));
                                }
                                var currentAssessmentData = currentAssessment.OasisData.ToObject<List<Question>>();
                                currentAssessmentData.Where(field => !tempAssessmentData.Exists(t => t.Name == field.Name)).ForEach(field => tempAssessmentData.Add(field));
                            }
                        }
                        else if (type == "10")
                        {
                            List<string> fields = new List<string>();
                            fields.Add("PrimaryDiagnosis");
                            fields.Add("PrimaryDiagnosisDate");
                            fields.Add("ICD9M");
                            fields.Add("SymptomControlRating");
                            int character = 65;
                            for (int diagnosisCount = 1; diagnosisCount < 26; diagnosisCount++)
                            {
                                fields.Add("PrimaryDiagnosis" + diagnosisCount);
                                fields.Add("PrimaryDiagnosis" + diagnosisCount + "Date");
                                fields.Add("PaymentDiagnoses" + (char)character + "3");
                                fields.Add("PaymentDiagnoses" + (char)character + "4");
                                character++;
                            }
                            character = 65;
                            for (int diagCount2 = 1; diagCount2 < 26; diagCount2++)
                            {
                                fields.Add("ICD9M" + (char)character + "3");
                                fields.Add("ICD9M" + (char)character + "4");
                                fields.Add("ICD9M" + diagCount2);
                                fields.Add("ExacerbationOrOnsetPrimaryDiagnosis" + diagCount2);
                                fields.Add("OtherDiagnose" + diagCount2 + "Rating");
                                character++;
                            }
                            fields.Add("PaymentDiagnosesZ3");
                            fields.Add("PaymentDiagnosesZ4");

                            previousAssessmentData.ForEach(f =>
                            {
                                if (f.Type == QuestionType.Generic || f.Type == QuestionType.PlanofCare)
                                {
                                    tempAssessmentData.Add(f);
                                }
                                else if (f.Type == QuestionType.Moo && fields.Contains(f.Name))
                                {
                                    tempAssessmentData.Add(f);
                                }
                            });
                            var currentAssessmentData = currentAssessment.OasisData.ToObject<List<Question>>();
                            currentAssessmentData.Where(field => !tempAssessmentData.Exists(t => t.Name == field.Name)).ForEach(field => tempAssessmentData.Add(field));
                        }
                        else
                        {
                            return false;
                        }
                        currentAssessment.Questions = tempAssessmentData;
                        return Update(currentAssessment);
                    }
                }
            }
            return result;
        }

        private List<string> AssessmentType05Generics()
        {
            return new List<string>() { "AllergiesDescription", "PulseApical", "PulseApicalRegular",
                "PulseApicalIrregular", "LastVisitDate", "GenericHeight", "PulseRadial", "PulseRadialReqular",
                "PulseRadialIrregular", "Weight", "WeightActualStated", "WeightActualStated", "Temp", "Resp",
                "BPLeftLying", "BPRightLying", "BPSittingLeft", "BPSittingRight", "BPStandingLeft", "BPStandingRight",
                "TempGreaterThan", "TempLessThan", "PulseGreaterThan", "PulseLessThan", "RespirationGreaterThan",
                "RespirationLessThan", "SystolicBPGreaterThan", "SystolicBPLessThan", "DiastolicBPGreaterThan",
                "DiastolicBPLessThan", "02SatLessThan", "FastingBloodSugarGreaterThan", "FastingBloodSugarLessThan",
                "RandomBloddSugarGreaterThan", "RandomBloodSugarLessThan", "WeightGreaterThan", "Pnemonia", "PnemoniaDate",
                "Flu", "FluDate", "TB", "TBDate", "TBExposure", "TBExposureDate", "AdditionalImmunization1Name", "AdditionalImmunization1",
                "AdditionalImmunization1Date", "AdditionalImmunization2Name", "AdditionalImmunization2", "AdditionalImmunization2Date",
                "ImmunizationComments", "Allergies", "SurgicalProcedureDescription1", "SurgicalProcedureCode1", "SurgicalProcedureCode1Date",
                "SurgicalProcedureDescription2", "SurgicalProcedureCode2", "SurgicalProcedureCode2Date"
            };
        }


        private void AddOASISBackup(OASISBackup data)
        {
            //repository.Add<OASISBackup>(data);
            try
            {
                string sql = @"INSERT INTO oasisbackups(`Id`,`AgencyId`,`AssessmentId`,`Data`,`Created`) VALUES (@id,@agencyid,@assessmentid,@data,@created)";
                using (var cmd = new FluentCommand<int>(sql))
                {
                    cmd.SetConnection("OasisCBackupConnectionString");
                    cmd.AddGuid("id", data.Id);
                    cmd.AddGuid("agencyid", data.AgencyId);
                    cmd.AddGuid("assessmentid", data.AssessmentId);
                    cmd.AddString("data", data.Data);
                    cmd.AddDateTime("created", data.Created);
                    cmd.AsNonQuery();
                }
            }
            catch (Exception ex)
            {
            }
        }


        #endregion

    }
}
