﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Domain;

    public interface IPlanofCareRepository
    {
        //bool Add(PlanofCare planofCare);
        //bool Update(PlanofCare planofCare);
        //PlanofCare GetPlanofCareByAssessmentId(Guid agencyId, Guid episodeId, Guid assessmentId);
        //PlanofCare Get(Guid agencyId, Guid patientId, Guid eventId);
        //PlanofCare Get(Guid agencyId, Guid episodeId, Guid assessmentId, string assessmentType);
        //PlanofCare Get(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        //bool MarkAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated);
        //bool ReassignUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        //List<PlanofCare> GetPlanofCareByStatus(Guid agencyId, int status);
        //List<PlanofCare> GetPlanOfCareOrders(Guid agencyId);
        //List<PlanofCare> GetPatientPlanOfCare(Guid agencyId, Guid patientId);

        //PlanofCareStandAlone GetStandAlone(Guid agencyId, Guid patientId, Guid eventId);
        //PlanofCareStandAlone GetStandAlone(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        //bool UpdateStandAlone(PlanofCareStandAlone planOfCare);
        //bool AddStandAlone(PlanofCareStandAlone planofCare);
        //List<PlanofCareStandAlone> GetPatientPlanOfCareStandAlone(Guid agencyId, Guid patientId);
        //List<PlanofCareStandAlone> GetPlanofCareStandAloneByStatus(Guid agencyId, int status);
        //List<PlanofCareStandAlone> GetPendingSignaturePlanofCaresStandAlone(Guid agencyId, string orderIds);
        //List<PlanofCareStandAlone> GetPlanofCaresStandAloneByStatus(Guid agencyId, int status, string orderIds);
        //List<PlanofCareStandAlone> GetPlanofCaresStandAlones(Guid agencyId, string orderIds);
        //List<PlanofCareStandAlone> GetPatientPlanofCaresStandAlones(Guid agencyId, Guid patientId, string orderIds);
        //bool MarkStandAloneAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated);

        //List<PlanofCare> GetByPhysicianId(List<Guid> physicianIdentifiers, int status);
        //List<PlanofCare> GetByPhysicianIdAndDate(List<Guid> physicianIdentifiers, int status, DateTime startDate, DateTime endDate);
        //List<PlanofCareStandAlone> GetStandAloneByPhysicianId(List<Guid> physicianIdentifiers, int status);
        //List<PlanofCareStandAlone> GetStandAloneByPhysicianIdAndDate(List<Guid> physicianIdentifiers, int status, DateTime startDate, DateTime endDate);
        //List<PlanofCare> GetPlanofCares(Guid agencyId, int status, string orderIds);
        //List<PlanofCare> GetPlanofCares(Guid agencyId, string orderIds);
        //List<PlanofCare> GetPatientPlanofCares(Guid agencyId, Guid patientId, string orderIds);
        //List<PlanofCare> GetPendingSignaturePlanofCares(Guid agencyId, string orderIds);

        //List<PlanofCare> GetAllPlanOfCareOrders();
        //List<PlanofCareStandAlone> GetAllPlanofCareStandAlones();

        //bool AddMultiple<T>(List<T> planofCares) where T : class, PlanOfCareAbstract, new();

        bool Add(PlanofCare planofCare);
        bool AddMultiple(List<PlanofCare> planofCares);
        PlanofCare Get(Guid agencyId, Guid patientId, Guid eventId);
        List<PlanofCare> GetPatientPlanOfCare(Guid agencyId, Guid patientId);
        PlanofCare GetPlanOfCareByAssessment(Guid agencyId, Guid assessmentId);
        PlanofCare GetPlanOfCareByAssessment(Guid agencyId, Guid episodeId, Guid assessmentId);
        List<PlanofCare> GetPlanofCaresByStatus(Guid agencyId, int status, string orderIds);
        bool MarkAsDeleted(Guid agencyId, Guid patientId, Guid eventId, bool isDeprecated);
        bool ReassignUser(Guid agencyId, Guid patientId, Guid eventId, Guid employeeId);
        bool RemoveFully(Guid agencyId, Guid patientId, Guid Id);
        bool Update(PlanofCare planofCare);

        List<PlanofCare> GetPlanOfCareOrderFromScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int[] disciplineTasks, int[] status);
        List<PlanofCare> GetPlanofCareOrders(Guid agencyId, List<Guid> branchIds, List<int> disciplineTasks, List<int> status, DateTime startDate, DateTime endDate);
        List<PlanofCare> GetPatientPlanofCareOrders(Guid agencyId, Guid patientId, List<int> disciplineTasks, List<int> optionalStatus, DateTime startDate, DateTime endDate);
        List<PlanofCare> GetCurrentAndPreviousPlanofCareOrders(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, List<int> disciplineTasks);
    }
}
