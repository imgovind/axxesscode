﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Enums;
    
    using SubSonic.SqlGeneration.Schema;

    [Serializable]
    public class Assessment : IAssessment
    {
        #region Constructor

        public Assessment()
        {
            this.ValidationError = string.Empty;
            this.Questions = new List<Question>();
        }

        #endregion

        #region IAssessment Members
        
        public Guid AgencyId { get; set; }
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid UserId { get; set; }
        public int Status { get; set; }
        public string OasisData { get; set; }
        public string ClaimKey { get; set; }
        public string HippsCode { get; set; }
        public string HippsVersion { get; set; }
        public string SubmissionFormat { get; set; }
        public string CancellationFormat { get; set; }
        public int VersionNumber { get; set; }
        public string Supply { get; set; }
        public string SignatureText { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public bool IsDeprecated { get; set; }
        public bool IsValidated { get; set; }
      
        public DateTime AssessmentDate { get; set; }
        public DateTime SignatureDate { get; set; }
        public DateTime ExportedDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public AssessmentType Type { get; set; }

        [SubSonicIgnore]
        public int Version { get; set; }
        [SubSonicIgnore]
        public bool ShowOasisVendorButton { get; set; }
        [SubSonicIgnore]
        public DateTime VisitDate { get; set; }
        [SubSonicIgnore]
        public string MedicationProfile { get; set; }
        [SubSonicIgnore]
        public string AllergyProfile { get; set; }
        [SubSonicIgnore]
        public string AgencyData { get; set; }
        [SubSonicIgnore]
        public string PatientData { get; set; }
        [SubSonicIgnore]
        public string PatientName { get; set; }
        [SubSonicIgnore]
        public string Insurance { get; set; }
        [SubSonicIgnore]
        public bool IsNew { get; set; }
        [SubSonicIgnore]
        public bool IsLastTab { get; set; }
        [SubSonicIgnore]
        public string StatusComment { get; set; }
        [SubSonicIgnore]
        public string TypeName { get { return (this.Type).ToString(); } }
        [SubSonicIgnore]
        public string TypeDescription { get { return (this.Type).GetDescription(); } }
        [SubSonicIgnore]
        public string ValidationError { get; set; }
        [SubSonicIgnore]
        public List<Question> Questions { get; set; }
        [SubSonicIgnore]
        public string Identifier { get { return string.Format("{0}|{1}", this.Id, this.Type); } }
        [SubSonicIgnore]
        public string AssessmentTypeNum {
            get {
                switch (this.Type) {
                    case AssessmentType.StartOfCare: return "01";
                    case AssessmentType.ResumptionOfCare: return "03";
                    case AssessmentType.Recertification: return "04";
                    case AssessmentType.FollowUp: return "05";
                    case AssessmentType.TransferInPatientNotDischarged: return "06";
                    case AssessmentType.TransferInPatientDischarged: return "07";
                    case AssessmentType.DischargeFromAgencyDeath: return "08";
                    case AssessmentType.DischargeFromAgency: return "09";
                    case AssessmentType.NonOasisStartOfCare: return "11";
                    case AssessmentType.NonOasisRecertification: return "14";
                    case AssessmentType.NonOasisDischarge: return "19";
                    default: return "00";
                }
            }
        }

        [SubSonicIgnore]
        public int DisciplineTask { get; set; }
       
        [SubSonicIgnore]
        public string Discipline  { get; set; }
        [SubSonicIgnore]
        public DateTime ScheduleDate { get; set; }
        [SubSonicIgnore]
        public DateTime StartDate { get; set; }
        [SubSonicIgnore]
        public DateTime EndDate { get; set; }
        #endregion

        #region Overrides

        public override string ToString()
        {
            return string.Format("{0}: AgencyId: {1}  EpisodeId: {2}  PatientId: {3} AssessmentId: {4}",
                this.TypeName, this.AgencyId, this.EpisodeId, this.PatientId, this.Id);
        }

        #endregion
    }
}
