﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;

    public class PlanofCare : PlanOfCareAbstract
    {
        public PlanofCare()
        {
            this.Questions = new List<Question>();
        }
        public bool IsNonOasis { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentType { get; set; }

        public override string ToString()
        {
            return string.Format("PlanofCare: AgencyId: {0}  EpisodeId: {1}  PatientId: {2}  Assessment Type: {3}",
                this.AgencyId, this.EpisodeId, this.PatientId, this.AssessmentType);
        }

    }
}
