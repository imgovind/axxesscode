﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    public abstract class PlanOfCareAbstract
    {

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public Guid PhysicianId { get; set; }
        public int Status { get; set; }
        public string Data { get; set; }
        public string PhysicianData { get; set; }
        public long OrderNumber { get; set; }
       
       
        public string PhysicianSignatureText { get; set; }
        public DateTime PhysicianSignatureDate { get; set; }

        public DateTime ReceivedDate { get; set; }
        public DateTime SentDate { get; set; }

        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }
       
        public bool IsNonOasis { get; set; }
        public bool IsStandAlone { get; set; }

        public bool IsDeprecated { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
       

        #region Domain

        [SubSonicIgnore]
        public string AgencyData { get; set; }

        [SubSonicIgnore]
        public string PatientData { get; set; }

        [SubSonicIgnore]
        public string MedicationProfileData { get; set; }

        [SubSonicIgnore]
        public List<Question> Questions { get; set; }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        [SubSonicIgnore]
        public string Allergies { get; set; }

        [SubSonicIgnore]
        public string PhysicianName { get; set; }

        [SubSonicIgnore]
        public string EpisodeStart { get; set; }

        [SubSonicIgnore]
        public string EpisodeEnd { get; set; }

        [SubSonicIgnore]
        public string OrderDateFormatted { get { return this.Created.ToString("MM/dd/yyyy"); } }
        [SubSonicIgnore]
        public string ReceivedDateFormatted { get { return this.ReceivedDate.IsValid() && this.ReceivedDate > DateTime.MinValue ? this.ReceivedDate.ToString("MM/dd/yyyy") : string.Empty; } }
        [SubSonicIgnore]
        public string SentDateFormatted { get { return this.SentDate.IsValid() && this.SentDate > DateTime.MinValue ? this.SentDate.ToString("MM/dd/yyyy") : string.Empty; } }


        [SubSonicIgnore]
        public string StatusComment { get; set; }

        
        #endregion
    }
}
