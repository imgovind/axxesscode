﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;


    public class PlanofCareStandAlone : PlanOfCareAbstract
    {
        public PlanofCareStandAlone()
        {
            this.Questions = new List<Question>();
        }
        public override string ToString()
        {
            return string.Format("PlanofCare: AgencyId: {0}  EpisodeId: {1}  PatientId: {2} ",
                this.AgencyId, this.EpisodeId, this.PatientId);
        }

    }
}
