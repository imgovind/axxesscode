﻿namespace Axxess.OasisC.Domain
{
    using System;
    public class OASISBackup
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid AssessmentId { get; set; }
        public string Data { get; set; }
        public DateTime Created { get; set; }
    }
}
