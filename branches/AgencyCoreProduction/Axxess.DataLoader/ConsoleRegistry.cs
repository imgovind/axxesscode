﻿namespace Axxess.DataLoader
{
    using System;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    using Axxess.Core.Infrastructure;

    //using Axxess.AgencyManagement.Domain;
    //using Axxess.AgencyManagement.Repositories;

    using Axxess.Log.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public class ConsoleRegistry : Registry
    {
        public ConsoleRegistry()
        {
            For<ILog>().Use<DatabaseLog>();
            For<IMembershipDataProvider>().Use<MembershipDataProvider>();
            //For<IAgencyManagementDataProvider>().Use<AgencyManagementDataProvider>();
            For<ILookUpDataProvider>().Use<LookUpDataProvider>();
            For<ILogDataProvider>().Use<LogDataProvider>();
        }
    }
}
