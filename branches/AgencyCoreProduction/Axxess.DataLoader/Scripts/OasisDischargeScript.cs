﻿namespace Axxess.DataLoader
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    public static class OasisDischargeScript
    {
        public static void Run(Guid agencyId, DateTime start, DateTime end)
        {
            string output = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("DischargeReasons_{0}.txt", DateTime.Now.Ticks));
            using (System.IO.TextWriter textWriter = new System.IO.StreamWriter(output, false))
            {
                var dischargeList = Database.GetAssessmentsBetween<DischargeFromAgencyAssessment>(agencyId, start, end);
                if (dischargeList != null && dischargeList.Count > 0)
                {
                    Console.WriteLine("Discharge List Count: {0}", dischargeList.Count);
                    dischargeList.ForEach(a =>
                    {
                        if (a.OasisData.IsNotNullOrEmpty())
                        {
                            var data = a.ToDictionary();
                            textWriter.WriteLine("Patient Id: {0}", data.AnswerOrEmptyString("M0020PatientIdNumber"));
                            textWriter.WriteLine("First Name: {0}", data.AnswerOrEmptyString("M0040FirstName"));
                            textWriter.WriteLine("LastName: {0}", data.AnswerOrEmptyString("M0040LastName"));
                            textWriter.WriteLine("Episode Start Date: {0}", data.AnswerOrEmptyString("GenericEpisodeStartDate"));
                            textWriter.WriteLine("Discharge Date: {0}", data.AnswerOrEmptyString("M0906DischargeDate"));
                            textWriter.WriteLine("Type Of Inpatient Facility: {0}", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility"));
                            textWriter.WriteLine();
                        }
                    });
                }

                var transferList = Database.GetAssessmentsBetween<TransferDischargeAssessment>(agencyId, start, end);
                if (transferList != null && transferList.Count > 0)
                {
                    Console.WriteLine("Transfer Discharge List Count: {0}", transferList.Count);
                    transferList.ForEach(a =>
                    {
                        if (a.OasisData.IsNotNullOrEmpty())
                        {
                            var data = a.ToDictionary();
                            textWriter.WriteLine("Patient Id: {0}", data.AnswerOrEmptyString("M0020PatientIdNumber"));
                            textWriter.WriteLine("Episode Start Date: {0}", data.AnswerOrEmptyString("GenericEpisodeStartDate"));
                            textWriter.WriteLine("First Name: {0}", data.AnswerOrEmptyString("M0040FirstName"));
                            textWriter.WriteLine("LastName: {0}", data.AnswerOrEmptyString("M0040LastName"));
                            textWriter.WriteLine("Discharge Date: {0}", data.AnswerOrEmptyString("M0906DischargeDate"));
                            textWriter.WriteLine("Type Of Inpatient Facility: {0}", data.AnswerOrEmptyString("M2410TypeOfInpatientFacility"));
                        }
                    });
                }
            }
        }
    }
}
