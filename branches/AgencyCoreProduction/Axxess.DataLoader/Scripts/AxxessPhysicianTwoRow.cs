﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;
    using System.Text;
    using System.Linq;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class AxxessPhysicianTwoRow
    {
        private static string input = Path.Combine(App.Root, "Files\\Suwanne-Physician-Working.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Suwanne-Physician-Working_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    int count = 0;
                                    AgencyPhysician physicianData = null;
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        count++;
                                        AgencyPhysician physicianPresent = null;
                                        if (count % 2 == 1)
                                        {
                                            physicianData = new AgencyPhysician();
                                            physicianData.Id = Guid.NewGuid();
                                            physicianData.AgencyId = agencyId;
                                            physicianData.IsDeprecated = false;

                                            var npiStaging = dataRow.GetValue(6);
                                            if (npiStaging.IsNotNullOrEmpty()) 
                                            {
                                                physicianData.NPI = npiStaging;
                                            }
                                            physicianPresent = Database.GetPhysician(physicianData.NPI, agencyId);
                                            if (physicianPresent == null) 
                                            {
                                                physicianPresent = Database.GetPhysician(physicianData.NPI);
                                            }
                                            var nameStaging = dataRow.GetValue(0);
                                            if (nameStaging.IsNotNullOrEmpty())
                                            {
                                                var nameArray = nameStaging.Split(',');
                                                if (nameArray != null) 
                                                {
                                                    if (nameArray.Length > 0) 
                                                    {
                                                        var lastNameStaging = nameArray[0];
                                                        lastNameStaging = lastNameStaging.Replace("M.D.", "");
                                                        lastNameStaging = lastNameStaging.Replace("MD", "");
                                                        lastNameStaging = lastNameStaging.Replace("D.O.", "");
                                                        lastNameStaging = lastNameStaging.Replace("DO", "");

                                                        if (!lastNameStaging.ToUpper().Contains("RODRIGUEZ")) { lastNameStaging = lastNameStaging.Replace("DR", ""); }
                                                        lastNameStaging = lastNameStaging.Replace("D.R.", "");
                                                        lastNameStaging = lastNameStaging.Replace("DPM", "");
                                                        lastNameStaging = lastNameStaging.Replace("D.P.M.", "");
                                                        lastNameStaging = lastNameStaging.Replace("ARNP", "");
                                                        lastNameStaging = lastNameStaging.Replace("DDS", "");
                                                        lastNameStaging = lastNameStaging.Replace("PA", "");
                                                        lastNameStaging = lastNameStaging.Replace("D.O", "");
                                                        lastNameStaging = lastNameStaging.Replace("DO.", "");
                                                        physicianData.LastName = lastNameStaging.Trim();
                                                        if (nameArray.Length > 1) 
                                                        {
                                                            var fNMNArray = nameArray[1].TrimStart().Split(' ');
                                                            if (fNMNArray != null) 
                                                            {
                                                                if (fNMNArray.Length > 0)
                                                                {
                                                                    physicianData.FirstName = fNMNArray[0];
                                                                    if (fNMNArray.Length > 1)
                                                                    {
                                                                        physicianData.MiddleName = fNMNArray[1];
                                                                    }
                                                                }
                                                                else 
                                                                {
                                                                    physicianData.FirstName = fNMNArray[0];
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            

                                            var phoneStaging = dataRow.GetValue(8);
                                            if (phoneStaging.IsNotNullOrEmpty()) 
                                            {
                                                var phoneWorkStaging = phoneStaging.ToPhoneDB();
                                                if (phoneWorkStaging.Length > 10) 
                                                {
                                                    phoneWorkStaging = phoneWorkStaging.Substring(0, 10);
                                                }
                                                physicianData.PhoneWork = phoneWorkStaging;
                                            }
                                            var faxStaging = dataRow.GetValue(11);
                                            if (faxStaging.IsNotNullOrEmpty())
                                            {
                                                var faxNumberStaging = faxStaging.ToPhoneDB();
                                                if (faxNumberStaging.Length > 10)
                                                {
                                                    faxNumberStaging = faxNumberStaging.Substring(0, 10);
                                                }
                                                physicianData.FaxNumber = faxNumberStaging;
                                            }

                                            if (physicianPresent != null)
                                            {
                                                if (physicianData.FirstName.IsNullOrEmpty()) { physicianData.FirstName = physicianPresent.FirstName; }
                                                if (physicianData.LastName.IsNullOrEmpty()) { physicianData.LastName = physicianPresent.LastName; }
                                                if (physicianData.PhoneWork.IsNullOrEmpty()) { physicianData.PhoneWork = physicianPresent.PhoneWork; }
                                                if (physicianData.FaxNumber.IsNullOrEmpty()) { physicianData.FaxNumber = physicianPresent.FaxNumber; }
                                                if (physicianData.FirstName.IsNullOrEmpty()) { physicianData.FirstName = ""; }
                                                if (physicianData.LastName.IsNullOrEmpty()) { physicianData.LastName = ""; }
                                                if (physicianData.PhoneWork.IsNullOrEmpty()) { physicianData.PhoneWork = ""; }
                                                if (physicianData.FaxNumber.IsNullOrEmpty()) { physicianData.FaxNumber = ""; }
                                            }
                                        }
                                        else if (count % 2 == 0)
                                        {
                                            if (physicianPresent == null)
                                            {
                                                var addresstaging = dataRow.GetValue(0);
                                                if (addresstaging.IsNotNullOrEmpty())
                                                {
                                                    if (addresstaging.Contains('|'))
                                                    {
                                                        var addressArray = addresstaging.Split('|');
                                                        if (addressArray != null && addressArray.Length > 0)
                                                        {
                                                            int iterator = 0;
                                                            foreach (var item in addressArray)
                                                            {
                                                                if (item.IsNotNullOrEmpty())
                                                                {
                                                                    if (item.Equals("FL") || item.Equals("GA") || item.Equals("NC") || item.Equals("IN"))
                                                                    {
                                                                        physicianData.AddressStateCode = item;

                                                                        if (iterator != 0)
                                                                        {
                                                                            int newIterator = iterator - 1;
                                                                            if (addressArray[newIterator].Split(' ').Length > 3)
                                                                            {
                                                                                var address1Staging = addressArray[newIterator];
                                                                                if(address1Staging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physicianData.AddressLine1 = address1Staging;                                                                                            
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                var addressCityStaging = addressArray[newIterator];
                                                                                if(addressCityStaging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physicianData.AddressCity = addressCityStaging;
                                                                                }
                                                                            }

                                                                            if (newIterator != 0)
                                                                            { 
                                                                                int newIterator3 = newIterator - 1;
                                                                                var address1Staging = addressArray[newIterator3];
                                                                                if (address1Staging.IsNotNullOrEmpty())
                                                                                {
                                                                                    physicianData.AddressLine1 = address1Staging;
                                                                                }
                                                                            }

                                                                            int newIterator2 = iterator + 1;
                                                                            if (newIterator2 <= (addressArray.Length - 1))
                                                                            {
                                                                                var addressZipStaging = addressArray[newIterator2];
                                                                                if (addressZipStaging.IsNotNullOrEmpty())
                                                                                {
                                                                                    if (addressZipStaging.Length >= 5)
                                                                                    {
                                                                                        var tempAddressZipStaging = addressZipStaging.Substring(0, 5);

                                                                                        if (tempAddressZipStaging.IsNotNullOrEmpty())
                                                                                        {
                                                                                            physicianData.AddressZipCode = tempAddressZipStaging;
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        physicianData.AddressZipCode = addressZipStaging;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                iterator++;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        physicianData.AddressLine1 = "";
                                                        physicianData.AddressLine2 = "";
                                                        physicianData.AddressCity = "";
                                                        physicianData.AddressStateCode = "FL";
                                                        physicianData.AddressZipCode = "";
                                                    }
                                                }
                                                else
                                                {
                                                    physicianData.AddressLine1 = "";
                                                    physicianData.AddressLine2 = "";
                                                    physicianData.AddressCity = "";
                                                    physicianData.AddressStateCode = "FL";
                                                    physicianData.AddressZipCode = "";
                                                }
                                            }
                                            else
                                            {
                                                physicianData.AddressLine1 = physicianPresent.AddressLine1;
                                                physicianData.AddressLine2 = physicianPresent.AddressLine2;
                                                physicianData.AddressCity = physicianPresent.AddressCity;
                                                physicianData.AddressStateCode = physicianPresent.AddressStateCode;
                                                physicianData.AddressZipCode = physicianPresent.AddressZipCode;
                                            }
                                            physicianData.Created = DateTime.Now;
                                            physicianData.Modified = DateTime.Now;

                                            var physician = Database.GetPhysician(physicianData.NPI, agencyId);
                                            if (physician == null)
                                            {
                                                if (Database.Add(physicianData))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, physicianData.DisplayName);
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("{0}) {1} ALEADY EXISTS", i, physician.DisplayName);
                                            }
                                            i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
