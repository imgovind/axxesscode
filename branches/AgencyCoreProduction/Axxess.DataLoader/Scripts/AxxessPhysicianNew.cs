﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class AxxessPhysicianNew
    {
        private static string input = Path.Combine(App.Root, "Files\\AXXESSDOCTOR.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\AXXESSDOCTOR_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string log = Path.Combine(App.Root, string.Format("Files\\LOG_AXXESSDOCTOR_{0}.txt", DateTime.Now.Ticks.ToString()));
        public static void Run(Guid agencyId)
        {
            #region VARS
            #region declaration
            int md_FirstName_col = default(int);
            int md_LastName_col = default(int);
            int md_MiddleName_col = default(int);
            int md_Addr1_col = default(int);
            int md_Addr2_col = default(int);
            int md_City_col = default(int);
            int md_State_col = default(int);
            int md_Zip_col = default(int);
            int md_Phone_col = default(int);
            int md_Fax_col = default(int);
            int md_Npi_col = default(int);
            
            //
            string firstColumn = default(string);
            
            //
            bool isDBWritePermissionGranted = default(bool);
            #endregion

            md_FirstName_col = 0;
            md_LastName_col = 1;
            md_MiddleName_col = 2;
            md_Addr1_col = 3;
            md_Addr2_col = 4;
            md_City_col = 5;
            md_State_col = 6;
            md_Zip_col = 7;
            md_Phone_col = 9;
            md_Fax_col = 10;
            md_Npi_col = 11;
            firstColumn = "md_FirstName_col";
            isDBWritePermissionGranted = true;
            #endregion
            
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    StringBuilder sb = new StringBuilder();
                    TextWriter txtWrtr = new StreamWriter(log, true);
                    int addedCounter = default(int), cannotAddCounter = default(int), skippedCounter = default(int), noNPICounter = default(int); ;
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;
                                    
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var npi = dataRow.GetValue(md_Npi_col);
                                            var physician = Database.GetPhysician(npi, agencyId);
                                            if (physician == null)
                                            {
                                                var info = Database.GetNpiData(npi);
                                                if (info != null)
                                                {
                                                    physician = new AgencyPhysician
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        NPI = npi,
                                                        LoginId = Guid.Empty
                                                    };

                                                    #region PROCESS
                                                    if (md_FirstName_col != default(int) || (firstColumn.Equals("md_FirstName_col") && md_FirstName_col != null))
                                                    {
                                                        var firstNameStaging = dataRow.GetValue(md_FirstName_col);
                                                        if (firstNameStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.FirstName = firstNameStaging;
                                                            sb.Append("FirstName:"); sb.Append(physician.FirstName); sb.Append("\t\t");
                                                        }
                                                    }
                                                    if (md_LastName_col != default(int) || (firstColumn.Equals("md_LastName_col") && md_LastName_col != null))
                                                    {
                                                        var lastNameStaging = dataRow.GetValue(md_LastName_col);
                                                        if (lastNameStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.LastName = lastNameStaging;
                                                            sb.Append("LastName:"); sb.Append(physician.LastName); sb.Append("\t\t");
                                                        }
                                                    }
                                                    if (md_MiddleName_col != default(int) || (firstColumn.Equals("md_MiddleName_col") && md_MiddleName_col != null))
                                                    {
                                                        var middleNameStaging = dataRow.GetValue(md_MiddleName_col);
                                                        if (middleNameStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.MiddleName = middleNameStaging;
                                                            sb.Append("MiddleName:"); sb.Append(physician.MiddleName); sb.Append("\t\t");
                                                        }
                                                    }
                                                    if (md_Addr1_col != default(int) || (firstColumn.Equals("md_Addr1_col") && md_Addr1_col != null))
                                                    {
                                                        var addressLine1Staging = dataRow.GetValue(md_Addr1_col);
                                                        if (addressLine1Staging.IsNotNullOrEmpty())
                                                        {
                                                            physician.AddressLine1 = addressLine1Staging;
                                                            sb.Append("AddressLine1:"); sb.Append(physician.AddressLine1); sb.Append("\t\t");
                                                        }
                                                    }
                                                    if (md_Addr2_col != default(int) || (firstColumn.Equals("md_Addr2_col") && md_Addr2_col != null))
                                                    {
                                                        var addressLine2Staging = dataRow.GetValue(md_Addr2_col);
                                                        if (addressLine2Staging.IsNotNullOrEmpty())
                                                        {
                                                            physician.AddressLine2 = addressLine2Staging;
                                                            sb.Append("AddressLine2:"); sb.Append(physician.AddressLine2); sb.Append("\t\t");
                                                        }
                                                    }
                                                    if (md_City_col != default(int) || (firstColumn.Equals("md_City_col") && md_City_col != null))
                                                    {
                                                        var addressCityStaging = dataRow.GetValue(md_City_col);
                                                        if (addressCityStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.AddressCity = addressCityStaging;
                                                            sb.Append("AddressCity:"); sb.Append(physician.AddressCity); sb.Append("\t\t");
                                                        }
                                                    }
                                                    if (md_State_col != default(int) || (firstColumn.Equals("md_State_col") && md_State_col != null))
                                                    {
                                                        var addressStateCodeStaging = dataRow.GetValue(md_State_col);
                                                        if (addressStateCodeStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.AddressStateCode = addressStateCodeStaging;
                                                            sb.Append("AddressStateCode:"); sb.Append(physician.AddressStateCode); sb.Append("\t\t");
                                                        }
                                                    }
                                                    if (md_Zip_col != default(int) || (firstColumn.Equals("md_Zip_col") && md_Zip_col != null))
                                                    {
                                                        var addressZipCodeStaging = dataRow.GetValue(md_Zip_col);
                                                        if (addressZipCodeStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.AddressZipCode = addressZipCodeStaging;
                                                            sb.Append("AddressZipCode:"); sb.Append(physician.AddressZipCode); sb.Append("\t\t");
                                                        }
                                                    }
                                                    if (md_Phone_col != default(int) || (firstColumn.Equals("md_Phone_col") && md_Phone_col != null))
                                                    {
                                                        var phoneWorkStaging = dataRow.GetValue(md_Phone_col);
                                                        if (phoneWorkStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.PhoneWork = phoneWorkStaging;
                                                            sb.Append("PhoneWork:"); sb.Append(physician.PhoneWork); sb.Append("\t\t");
                                                        }
                                                    }
                                                    if (md_Fax_col != default(int) || (firstColumn.Equals("md_Fax_col") && md_Fax_col != null))
                                                    {
                                                        var faxNumberStaging = dataRow.GetValue(md_Fax_col);
                                                        if (faxNumberStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.FaxNumber = faxNumberStaging;
                                                            sb.Append("FaxNumber:"); sb.Append(physician.FaxNumber); sb.Append("\t\t");
                                                        }
                                                    }

                                                    #endregion

                                                    #region COMMENTS
                                                    //StringBuilder sb2 = new StringBuilder();
                                                    //var mobileStaging = dataRow.GetValue(11);
                                                    //if (mobileStaging.IsNotNullOrEmpty())
                                                    //{
                                                    //    sb2.Append("Phone Mobile: "); sb2.Append(mobileStaging); sb2.Append(" ;"); sb2.Append("\t\t");
                                                    //}

                                                    //var commentsStaging = dataRow.GetValue(12);
                                                    //if (commentsStaging.IsNotNullOrEmpty())
                                                    //{
                                                    //    sb2.Append("Comments: "); sb2.Append(commentsStaging); sb2.Append(" ;"); sb2.Append("\t\t");
                                                    //}
                                                    //physician.Comments = sb2.ToString();

                                                    sb.Append("Comments:"); sb.Append(physician.Comments); sb.Append("\t\t");
                                                    txtWrtr.WriteLine(sb.ToString());
                                                    sb.Length = 0;
                                                    #endregion

                                                }
                                                else
                                                {
                                                    Console.WriteLine("Physician Not Added - No NPI Info");
                                                    noNPICounter++;
                                                }
                                                if (isDBWritePermissionGranted == true)
                                                {
                                                    if (Database.Add(physician))
                                                    {
                                                        Console.WriteLine("(" + i.ToString() + ")" + "Physician Added: " + physician.LastName + ", " + physician.FirstName);
                                                        addedCounter++;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("(" + i.ToString() + ")" + "Physician NOT Added: ");
                                                        cannotAddCounter++;
                                                    }
                                                }
                                            }
                                            else 
                                            {
                                                Console.WriteLine("(" + i.ToString() + ")" + "Physician PRESENT: " + physician.LastName + ", " + physician.FirstName);
                                                skippedCounter++;
                                            }
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                    txtWrtr.Close();
                    Console.WriteLine("Finished Writing to Log File");
                    Console.WriteLine(string.Format("{0} : {1}","ADDED:",addedCounter));
                    Console.WriteLine(string.Format("{0} : {1}:","NOT ADDED:",cannotAddCounter));
                    Console.WriteLine(string.Format("{0} : {1}:","NO NPI:",noNPICounter));
                    Console.WriteLine(string.Format("{0} : {1}:", "SKIPPED:", skippedCounter));
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
