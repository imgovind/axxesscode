﻿namespace Axxess.DataLoader.Domain
{
        using System;
    using System.IO;
    using System.Net;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Excel;
    using HtmlAgilityPack;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using Axxess.LookUp.Domain;

    public static class HippsAndHhrgScript
    {
        private static string hhrginput = Path.Combine(App.Root, "Files\\CY_2014_Final_HH_PPS_Case-Mix_Weights.xlsx");

        public static void Run()
        {
            //LoadHhrg2013();
            LoadHhrg2014();
        }

        private static void LoadHhrg2014()
        {
            using (FileStream hhrgStream = new FileStream(hhrginput, FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(hhrgStream))
                {
                    if (excelReader != null && excelReader.IsValid)
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        DataTable dataTable = excelReader.AsDataSet().Tables[0];
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            int hhrgCounter = 0;
                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!dataRow.IsEmpty())
                                {
                                    if (dataRow.GetValue(2).IsNotNullOrEmpty() && dataRow.GetValue(3).IsNotNullOrEmpty() && dataRow.GetValue(3).IsDouble())
                                    {
                                        var existingHippsAndHhrgs = Database.GetHippsAndHhrg(dataRow.GetValue(2), dataRow.GetValue(3).ToDouble());
                                        if (existingHippsAndHhrgs != null && existingHippsAndHhrgs.Count > 0)
                                        {
                                            existingHippsAndHhrgs.ForEach(hippsAndHhrg =>
                                            {
                                                var newHippsAndHhrg = new HippsAndHhrg()
                                                {
                                                    HHRG = hippsAndHhrg.HHRG,
                                                    HIPPS = hippsAndHhrg.HIPPS,
                                                    HHRGWeight = Math.Round(dataRow.GetValue(4).ToDouble(), 4),
                                                    Time = DateTime.Parse("1/1/2014")
                                                };
                                                if (Database.AddForLookup<HippsAndHhrg>(newHippsAndHhrg))
                                                {
                                                    Console.WriteLine("{0}) {1} - {2} - {3:#0.0000} {4}", hhrgCounter, newHippsAndHhrg.HHRG, newHippsAndHhrg.HIPPS, newHippsAndHhrg.HHRGWeight, newHippsAndHhrg.Time.ToShortDateString());
                                                    hhrgCounter++;
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void LoadHhrg2013()
        {
            using (FileStream hhrgStream = new FileStream(hhrginput, FileMode.Open, FileAccess.Read))
            {
                using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(hhrgStream))
                {
                    if (excelReader != null && excelReader.IsValid)
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        DataTable dataTable = excelReader.AsDataSet().Tables[0];
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            var hhrgCounter = 1;
                            var previousHhrg = string.Empty;
                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                if (!dataRow.IsEmpty())
                                {
                                    if (!dataRow.GetValue(0).ToLower().StartsWith("effective"))
                                    {
                                        var hhrg = dataRow.GetValue(0);
                                        if (previousHhrg.IsNullOrEmpty())
                                        {
                                            previousHhrg = hhrg;
                                        }
                                        if (hhrg.IsNullOrEmpty())
                                        {
                                            hhrg = previousHhrg;
                                        }
                                        var hippsAndHhrg = new HippsAndHhrg()
                                        {
                                            HHRG = hhrg,
                                            HIPPS = dataRow.GetValue(1),
                                            HHRGWeight = dataRow.GetValue(2).ToDouble(),
                                            Time = DateTime.Parse("1/1/2013")
                                        };
                                        if (Database.AddForLookup<HippsAndHhrg>(hippsAndHhrg))
                                        {
                                            Console.WriteLine("{0}) {1} - {2} - {3:#0.0000} {4}", hhrgCounter, hippsAndHhrg.HHRG, hippsAndHhrg.HIPPS, hippsAndHhrg.HHRGWeight, hippsAndHhrg.Time.ToShortDateString());
                                            hhrgCounter++;
                                        }
                                        if (!hhrg.IsEqual(previousHhrg))
                                        {
                                            previousHhrg = hhrg;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
