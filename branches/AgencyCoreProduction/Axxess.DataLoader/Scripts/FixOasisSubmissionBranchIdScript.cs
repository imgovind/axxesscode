﻿namespace Axxess.DataLoader
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Api;
    using Axxess.Api.Contracts;
    using Axxess.Core.Extension;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.AgencyManagement.Domain;


    public static class FixOasisSubmissionBranchIdScript
    {
        private static string output;
        private static TextWriter textWriter;
        private static GrouperAgent grouperAgent = new GrouperAgent();
        public static void Run(Guid agencyId)
        {
            output = Path.Combine(App.Root, string.Format("{0}.txt", agencyId.ToString()));
            using (textWriter = new StreamWriter(output, true))
            {
                var socList = Database.GetAssessmentsWhereStatusZero<StartOfCareAssessment>(agencyId);
                if (socList != null && socList.Count > 0)
                {
                    LoadDataFromScheduledEvent<StartOfCareAssessment>(socList);
                }

                var rocList = Database.GetAssessmentsWhereStatusZero<ResumptionofCareAssessment>(agencyId);
                if (rocList != null && rocList.Count > 0)
                {
                    LoadDataFromScheduledEvent<ResumptionofCareAssessment>(rocList);
                }

                var recertList = Database.GetAssessmentsWhereStatusZero<RecertificationAssessment>(agencyId);
                if (recertList != null && recertList.Count > 0)
                {
                    LoadDataFromScheduledEvent<RecertificationAssessment>(recertList);
                }

                var followupList = Database.GetAssessmentsWhereStatusZero<FollowUpAssessment>(agencyId);
                if (followupList != null && followupList.Count > 0)
                {
                    LoadDataFromScheduledEvent<FollowUpAssessment>(followupList);
                }

                var deathList = Database.GetAssessmentsWhereStatusZero<DeathAtHomeAssessment>(agencyId);
                if (deathList != null && deathList.Count > 0)
                {
                    LoadDataFromScheduledEvent<DeathAtHomeAssessment>(deathList);
                }

                var dischargeList = Database.GetAssessmentsWhereStatusZero<DischargeFromAgencyAssessment>(agencyId);
                if (dischargeList != null && dischargeList.Count > 0)
                {
                    LoadDataFromScheduledEvent<DischargeFromAgencyAssessment>(dischargeList);
                }

                var transferList = Database.GetAssessmentsWhereStatusZero<TransferDischargeAssessment>(agencyId);
                if (transferList != null && transferList.Count > 0)
                {
                    LoadDataFromScheduledEvent<TransferDischargeAssessment>(transferList);
                }

                var transferNotList = Database.GetAssessmentsWhereStatusZero<TransferNotDischargedAssessment>(agencyId);
                if (transferNotList != null && transferNotList.Count > 0)
                {
                    LoadDataFromScheduledEvent<TransferNotDischargedAssessment>(transferNotList);
                }
            }
        }

        private static void LoadDataFromScheduledEvent<T>(List<T> list) where T : Assessment, new()
        {
            Console.WriteLine("Item Count: {0}", list.Count);
            list.ForEach(a =>
            {
                if (a != null && a.SubmissionFormat.IsNotNullOrEmpty())
                {
                    var data = a.ToDictionary();
                    var hipps = grouperAgent.GetHippsCode(a.SubmissionFormat);
                    var scheduledEvent = Database.GetScheduleEventOnly(a.AgencyId, a.EpisodeId, a.PatientId, a.Id);
                    if (scheduledEvent != null && hipps != null)
                    {
                        a.UserId = scheduledEvent.UserId;
                        a.HippsCode = hipps.Code;
                        a.HippsVersion = hipps.Version;
                        a.ClaimKey = hipps.ClaimMatchingKey;
                        if (scheduledEvent.Status == "0")
                        {
                            a.Status = 220;
                        }
                        else
                        {
                            a.Status = scheduledEvent.Status.ToInteger();
                        }
                        a.AssessmentDate = data.AnswerOrDefault("AssessmentCompleted", scheduledEvent.VisitDate).ToDateTime();
                        a.Created = scheduledEvent.EventDate.ToDateTime();
                        a.Modified = DateTime.Now;
                        a.IsValidated = true;
                        a.SignatureDate = a.AssessmentDate;
                        var user = Database.GetUser(a.UserId, a.AgencyId);
                        if (user != null && user.DisplayName.IsNotNullOrEmpty())
                        {
                            a.SignatureText = string.Format("Electronically Signed by: {0}", user.DisplayName);
                        }
                        a.TimeIn = scheduledEvent.TimeIn;
                        a.TimeOut = scheduledEvent.TimeOut;

                        if (Database.UpdateForOasisC<T>(a))
                        {
                            Console.WriteLine("{0} updated", a.ToString());
                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        textWriter.WriteLine(a.ToString());
                    }
                }
            });
        }
    }
}
