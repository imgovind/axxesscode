﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class KinnserCsvScript
    {
        private static string input = Path.Combine(App.Root, "Files\\camp.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\camp_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (var csvReader = new CsvReader(fileStream))
                        {
                            if (csvReader != null)
                            {
                                var i = 1;
                                csvReader.ReadHeaderRecord();
                                foreach (var dataRow in csvReader.DataRecords)
                                {
                                    var patientData = new Patient();
                                    patientData.Id = Guid.NewGuid();
                                    patientData.AgencyId = agencyId;
                                    patientData.AgencyLocationId = locationId;
                                    patientData.Status = 1;
                                    patientData.Ethnicities = string.Empty;
                                    patientData.MaritalStatus = string.Empty;
                                    patientData.IsDeprecated = false;
                                    patientData.IsHospitalized = false;
                                    patientData.PatientIdNumber = dataRow.GetValue(0);
                                    patientData.LastName = dataRow.GetValue(1).ToTitleCase();
                                    patientData.FirstName = dataRow.GetValue(2).ToTitleCase();
                                    patientData.Gender = dataRow.GetValue(3);
                                    patientData.MedicareNumber = dataRow.GetValue(4);
                                    patientData.MedicaidNumber = dataRow.GetValue(5);
                                    patientData.DOB = dataRow.GetValue(6).IsValidDate() ? dataRow.GetValue(6).ToDateTime() : DateTime.MinValue;
                                    patientData.PhoneHome = dataRow.GetValue(7).ToPhoneDB();
                                    patientData.AddressLine1 = dataRow.GetValue(8).ToTitleCase();
                                    patientData.AddressCity = dataRow.GetValue(9).ToTitleCase();
                                    patientData.AddressStateCode = dataRow.GetValue(10);
                                    patientData.AddressZipCode = dataRow.GetValue(11).Substring(0, 5);
                                    patientData.StartofCareDate = dataRow.GetValue(12).IsValidDate() ? dataRow.GetValue(12).ToDateTime() : DateTime.MinValue;

                                    var episodeEndDate = dataRow.GetValue(14).ToDateTime();
                                    if (episodeEndDate.Year < 2011)
                                    {
                                        patientData.Status = 2;
                                    }

                                    if (dataRow.GetValue(26).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Insurance: {0}. ", dataRow.GetValue(26));
                                    }
                                    if (dataRow.GetValue(19).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Triage Level: {0}. ", dataRow.GetValue(19));
                                    }
                                    if (dataRow.GetValue(13).IsNotNullOrEmpty() && dataRow.GetValue(14).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Episode Range: {0} - {1} .", dataRow.GetValue(13), dataRow.GetValue(14));
                                    }
                                    if (dataRow.GetValue(15).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(15));
                                    }
                                    if (dataRow.GetValue(16).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Secondary Diagnosis: {0}. ", dataRow.GetValue(16));
                                    }
                                    if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Disciplines: {0}. ", dataRow.GetValue(17));
                                    }
                                    if (dataRow.GetValue(18).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Frequencies: {0}. ", dataRow.GetValue(18));
                                    }
                                    if (dataRow.GetValue(20).IsNotNullOrEmpty() && dataRow.GetValue(21).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Physician & NPI: {0} {1}. ", dataRow.GetValue(20), dataRow.GetValue(21));
                                    }
                                    if (dataRow.GetValue(24).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Clinician: {0}. ", dataRow.GetValue(24));
                                    }
                                    if (dataRow.GetValue(32).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Case Manager: {0}. ", dataRow.GetValue(32));
                                    }
                                    if (dataRow.GetValue(30).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Therapist: {0}. ", dataRow.GetValue(30));
                                    }
                                    if (dataRow.GetValue(31).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("External Referral: {0}. ", dataRow.GetValue(31));
                                    }

                                    patientData.Comments = patientData.Comments.Replace("'", "");

                                    patientData.Created = DateTime.Now;
                                    patientData.Modified = DateTime.Now;

                                    var medicationProfile = new MedicationProfile()
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agencyId,
                                        PatientId = patientData.Id,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        Medication = "<ArrayOfMedication />"
                                    };

                                    var allergyProfile = new AllergyProfile()
                                    {
                                        Id = Guid.NewGuid(),
                                        AgencyId = agencyId,
                                        PatientId = patientData.Id,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        Allergies = "<ArrayOfAllergy />"
                                    };

                                    if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                    {
                                        var admissionPeriod = new PatientAdmissionDate()
                                        {
                                            Id = Guid.NewGuid(),
                                            AgencyId = agencyId,
                                            Created = DateTime.Now,
                                            DischargedDate = DateTime.MinValue,
                                            IsActive = true,
                                            IsDeprecated = false,
                                            Modified = DateTime.Now,
                                            PatientData = patientData.ToXml().Replace("'", ""),
                                            PatientId = patientData.Id,
                                            Reason = string.Empty,
                                            StartOfCareDate = patientData.StartofCareDate,
                                            Status = patientData.Status
                                        };

                                        if (Database.Add(admissionPeriod))
                                        {
                                            var patient = Database.GetPatient(patientData.Id, agencyId);
                                            if (patient != null)
                                            {
                                                patient.AdmissionId = admissionPeriod.Id;
                                                if (Database.Update(patient))
                                                {
                                                    Console.WriteLine("{0}) {1}", i, patientData.DisplayName);

                                                    var exists = true;
                                                    var npi = dataRow.GetValue(21);
                                                    var physician = Database.GetPhysician(npi, agencyId);
                                                    if (physician == null)
                                                    {
                                                        exists = false;
                                                        var info = Database.GetNpiData(npi);
                                                        if (info != null)
                                                        {
                                                            physician = new AgencyPhysician
                                                            {
                                                                Id = Guid.NewGuid(),
                                                                AgencyId = agencyId,
                                                                NPI = npi,
                                                                LoginId = Guid.Empty,
                                                                PhoneWork = dataRow.GetValue(22).ToPhoneDB(),
                                                                FaxNumber = dataRow.GetValue(23).ToPhoneDB(),
                                                                AddressCity = info.ProviderBusinessPracticeLocationAddressCityName,
                                                                AddressLine1 = info.ProviderFirstLineBusinessPracticeLocationAddress,
                                                                AddressLine2 = info.ProviderSecondLineBusinessPracticeLocationAddress,
                                                                AddressStateCode = info.ProviderBusinessPracticeLocationAddressStateName,
                                                                AddressZipCode = info.ProviderBusinessPracticeLocationAddressPostalCode,
                                                                Credentials = info.ProviderCredentialText,
                                                                Created = DateTime.Now,
                                                                Modified = DateTime.Now,
                                                                IsDeprecated = false,
                                                                PhysicianAccess = false,
                                                                Primary = true
                                                            };
                                                            var physicianNameArray = dataRow.GetValue(20).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (physicianNameArray != null && physicianNameArray.Length > 1)
                                                            {
                                                                physician.LastName = physicianNameArray[1].Trim();
                                                                physician.FirstName = physicianNameArray[0].Trim();
                                                            }
                                                        }

                                                        Database.Add(physician);
                                                    }

                                                    if (physician != null)
                                                    {
                                                        var patientPhysician = new PatientPhysician
                                                        {
                                                            IsPrimary = true,
                                                            PatientId = patientData.Id,
                                                            PhysicianId = physician.Id
                                                        };

                                                        if (Database.Add(patientPhysician))
                                                        {
                                                            Console.WriteLine("{0}) {1} {2}", i, physician.DisplayName, exists ? "ALREADY EXISTS" : "");
                                                        }
                                                    }
                                                    Console.WriteLine();
                                                }
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
