﻿namespace Axxess.DataLoader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public static class AgencyAddAdjustmentsScript
    {
        public static void Run()
        {
            var agencies = Database.GetAgencies();
            if (agencies != null && agencies.Count > 0)
            {
                Console.WriteLine("Agency List Count: {0}", agencies.Count);
                int count = 0;
                List<AgencyAdjustmentCode> adjustmentCodes = new List<AgencyAdjustmentCode>();
                agencies.ForEach(agency =>
                {
                    adjustmentCodes.Add(new AgencyAdjustmentCode()
                    {
                        Id = Guid.NewGuid(),
                        Code = "CON",
                        Description = "Contractual Obligation",
                        IsDeprecated = false,
                        AgencyId = agency.Id,
                        Modified = DateTime.Now,
                        Created = DateTime.Now
                    });

                    adjustmentCodes.Add(new AgencyAdjustmentCode()
                    {
                        Id = Guid.NewGuid(),
                        Code = "BD",
                        Description = "Bad Debt",
                        IsDeprecated = false,
                        AgencyId = agency.Id,
                        Modified = DateTime.Now,
                        Created = DateTime.Now
                    });
                    count++;
                    Console.WriteLine(count + "/" + agencies.Count);
                });
                Console.WriteLine("Adding all adjustments");
                Database.AddMany<AgencyAdjustmentCode>(adjustmentCodes);
            }
        }
    }
}
