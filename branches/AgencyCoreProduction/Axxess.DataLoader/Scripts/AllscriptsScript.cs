﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Data;
    using System.Text;

    using Excel;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class AllscriptsScript
    {
        private static string input = Path.Combine(App.Root, "Files\\ActivePatientReportAxxessImplementation.xlsx");
        private static string output = Path.Combine(App.Root, string.Format("Files\\ActivePatientReportAxxessImplementation_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(string agencyId, string locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                var i = 1;
                                int numberOfRows = 4;
                                int rowCounter = 1;
                                Patient patientData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        if (dataRow.GetValue(0).ToLower().Contains("patient code"))
                                            continue;
                                        else if (dataRow.GetValue(0).ToLower().Contains("address"))
                                            continue;
                                        if (rowCounter % numberOfRows == 1)
                                        {
                                            patientData = new Patient();
                                            patientData.Id = Guid.NewGuid();
                                            patientData.AgencyId = agencyId.ToGuid();
                                            patientData.AgencyLocationId = locationId.ToGuid();
                                            patientData.Ethnicities = string.Empty;
                                            patientData.MaritalStatus = string.Empty;
                                            patientData.IsDeprecated = false;
                                            patientData.IsHospitalized = false;
                                            patientData.Status = 1;
                                            patientData.AddressLine1 = string.Empty;
                                            patientData.AddressLine2 = string.Empty;
                                            patientData.AddressCity = string.Empty;
                                            patientData.AddressStateCode = string.Empty;
                                            patientData.AddressZipCode = string.Empty;

                                            patientData.PatientIdNumber = dataRow.GetValue(0);
                                            var nameArray = dataRow.GetValue(1).Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (nameArray != null && nameArray.Length > 1)
                                            {
                                                var firstNameArray = nameArray[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (firstNameArray != null && firstNameArray.Length > 0)
                                                {
                                                    if (firstNameArray.Length == 2)
                                                    {
                                                        patientData.FirstName = firstNameArray[0].Trim();
                                                        patientData.MiddleInitial = firstNameArray[1].Trim();
                                                    }
                                                    else
                                                    {
                                                        patientData.FirstName = firstNameArray[0].Trim();
                                                    }
                                                }
                                                patientData.LastName = nameArray[0];
                                            }
                                            patientData.Gender = dataRow.GetValue(4).IsEqual("F") ? "Female" : "Male";
                                            patientData.PhoneHome = dataRow.GetValue(5).ToPhoneDB();
                                            
                                            if (dataRow.GetValue(8).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Admitted Date: {0}. ", dataRow.GetValue(8).ToDateTime().ToShortDateString());
                                            }
                                            if (dataRow.GetValue(9).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Primary Physician: {0}. ", dataRow.GetValue(9));
                                            }
                                            
                                        }
                                        else if (rowCounter % numberOfRows == 2)
                                        {
                                            patientData.DOB = dataRow.GetValue(4).IsValidDate() ? dataRow.GetValue(4).ToDateTime() : DateTime.MinValue;
                                            if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                            {
                                                if (dataRow.GetValue(6).Trim().ToLower().Contains("medicare"))
                                                {
                                                    patientData.PrimaryInsurance = "1";
                                                }
                                                patientData.Comments += string.Format("Insurance: {0}. ", dataRow.GetValue(6));
                                            }
                                            if (dataRow.GetValue(9).IsNotNullOrEmpty())
                                            {
                                                patientData.Comments += string.Format("Physician Phone: {0}.", dataRow.GetValue(9));
                                            }
                                            
                                        }
                                        else if (rowCounter % numberOfRows == 3)
                                        {
                                            patientData.AddressLine1 = dataRow.GetValue(0);
                                        }
                                        else if (rowCounter % numberOfRows == 0)
                                        {
                                            if (dataRow.GetValue(0).Trim().IsNotNullOrEmpty())
                                            {
                                                var cityStateZip = dataRow.GetValue(1).Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (cityStateZip != null && cityStateZip.Length > 0)
                                                {
                                                    patientData.AddressCity = cityStateZip[0];
                                                    var stateZip = cityStateZip[1].Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                                                    if (stateZip != null && stateZip.Length > 0)
                                                    {
                                                        patientData.AddressStateCode = stateZip[0];
                                                        patientData.AddressZipCode = stateZip[1]; 
                                                    }
                                                }
                                            }
                                            if (patientData.Comments.IsNotNullOrEmpty())
                                            {
                                                patientData.Comments = patientData.Comments.Replace("'", "");
                                            }

                                            patientData.Created = DateTime.Now;
                                            patientData.Modified = DateTime.Now;

                                            var medicationProfile = new MedicationProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Medication = "<ArrayOfMedication />"
                                            };

                                            var allergyProfile = new AllergyProfile
                                            {
                                                Id = Guid.NewGuid(),
                                                AgencyId = agencyId.ToGuid(),
                                                PatientId = patientData.Id,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                Allergies = "<ArrayOfAllergy />"
                                            };
                                            var a = 5;
                                            if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                            {
                                                Console.WriteLine("{0}) {1}", i, patientData.DisplayName);
                                            }
                                            textWriter.Write(textWriter.NewLine);
                                            i++;
                                        }
                                        rowCounter++;
                                    }
                                }
                                Console.WriteLine(rowCounter);
                            }
                        }
                    }
                }
            }
        }
    }
}
