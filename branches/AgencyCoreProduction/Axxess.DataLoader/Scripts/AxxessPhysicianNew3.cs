﻿namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Text;

    public static class AxxessPhysicianNew3
    {
        private static string input = Path.Combine(App.Root, "Files\\CountryHomeCare-Physician.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\CountryHomeCare-Physician-EventLog_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string log = Path.Combine(App.Root, string.Format("Files\\CountryHomeCare-Physician-DataLog_{0}.txt", DateTime.Now.Ticks.ToString()));
        public static void Run(Guid agencyId)
        {
            #region VARS
            #region declaration
            int md_onename_col = default(int);
            int md_FirstName_col = default(int);
            int md_LastName_col = default(int);
            int md_MiddleName_col = default(int);
            int md_oneAddr_col = default(int);
            int md_Addr1_col = default(int);
            int md_Addr2_col = default(int);
            int md_City_col = default(int);
            int md_State_col = default(int);
            int md_Zip_col = default(int);
            int md_Phone_col = default(int);
            int md_Fax_col = default(int);
            int md_Npi_col = default(int);

            //
            string firstColumn = default(string);

            //
            bool isDBWritePermissionGranted = default(bool);
            #endregion

            firstColumn = "md_onename_col";

            md_onename_col = 0;
            //md_FirstName_col = 0;
            //md_LastName_col = 1;
            //md_MiddleName_col = 2;
            md_oneAddr_col = 1;
            //md_Addr1_col = 1;
            //md_Addr2_col = 2;
            //md_City_col = 3;
            //md_State_col = 4;
            //md_Zip_col = 5;
            md_Phone_col = 2;
            md_Fax_col = 3;
            md_Npi_col = 4;

            isDBWritePermissionGranted = true;
            #endregion

            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    StringBuilder sb = new StringBuilder();
                    TextWriter txtWrtr = new StreamWriter(log, true);
                    int addedCounter = default(int), cannotAddCounter = default(int), skippedCounter = default(int), noNPICounter = default(int); ;
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                        {
                            if (excelReader != null && excelReader.IsValid)
                            {
                                excelReader.IsFirstRowAsColumnNames = true;
                                DataTable dataTable = excelReader.AsDataSet().Tables[0];
                                if (dataTable != null && dataTable.Rows.Count > 0)
                                {
                                    var i = 1;

                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!dataRow.IsEmpty())
                                        {
                                            var npi = dataRow.GetValue(md_Npi_col);
                                            var physician = Database.GetPhysician(npi, agencyId);
                                            if (physician == null)
                                            {
                                                var info = Database.GetNpiData(npi);
                                                if (info != null)
                                                {
                                                    physician = new AgencyPhysician
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        NPI = npi,
                                                        LoginId = Guid.Empty
                                                    };

                                                    #region PROCESS

                                                    #region Name
                                                    bool isPhysicianNameOneCell = default(bool);
                                                    //One Name
                                                    if (md_onename_col != default(int) || (firstColumn.Equals("md_onename_col") && md_onename_col != null))
                                                    {
                                                        var oneNameStaging = dataRow.GetValue(md_onename_col);
                                                        var nameArray = oneNameStaging.Split(',');
                                                        if (nameArray != null && nameArray.Length > 0)
                                                        {
                                                            if (nameArray.Length > 1)
                                                            {
                                                                var naamArray1 = nameArray[1].TrimStart(' ').Split(' ');
                                                                if (naamArray1.Length > 1)
                                                                {
                                                                    physician.MiddleName = naamArray1[1];
                                                                    physician.FirstName = naamArray1[0];
                                                                    sb.Append("MiddleInitial"); sb.Append(": "); sb.Append(physician.MiddleName); sb.Append("\t\t");
                                                                }
                                                                else
                                                                {
                                                                    physician.FirstName = nameArray[1];
                                                                }
                                                                physician.LastName = nameArray[0];
                                                            }
                                                        }
                                                        sb.Append("FirstName"); sb.Append(": "); sb.Append(physician.FirstName); sb.Append("\t\t");
                                                        sb.Append("LastName"); sb.Append(": "); sb.Append(physician.LastName); sb.Append("\t\t");
                                                        isPhysicianNameOneCell = true;
                                                    }
                                                    if (isPhysicianNameOneCell == false)
                                                    {
                                                        if (md_FirstName_col != default(int) || (firstColumn.Equals("md_FirstName_col") && md_FirstName_col != null))
                                                        {
                                                            var firstNameStaging = dataRow.GetValue(md_FirstName_col);
                                                            if (firstNameStaging.IsNotNullOrEmpty())
                                                            {
                                                                physician.FirstName = firstNameStaging;
                                                                sb.Append("FirstName:"); sb.Append(physician.FirstName); sb.Append("\t\t");
                                                            }
                                                        }
                                                        if (md_LastName_col != default(int) || (firstColumn.Equals("md_LastName_col") && md_LastName_col != null))
                                                        {
                                                            var lastNameStaging = dataRow.GetValue(md_LastName_col);
                                                            if (lastNameStaging.IsNotNullOrEmpty())
                                                            {
                                                                physician.LastName = lastNameStaging;
                                                                sb.Append("LastName:"); sb.Append(physician.LastName); sb.Append("\t\t");
                                                            }
                                                        }
                                                        if (md_MiddleName_col != default(int) || (firstColumn.Equals("md_MiddleName_col") && md_MiddleName_col != null))
                                                        {
                                                            var middleNameStaging = dataRow.GetValue(md_MiddleName_col);
                                                            if (middleNameStaging.IsNotNullOrEmpty())
                                                            {
                                                                physician.MiddleName = middleNameStaging;
                                                                sb.Append("MiddleName:"); sb.Append(physician.MiddleName); sb.Append("\t\t");
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                    #region Address
                                                    bool isPhysicianAddressOneCell = default(bool);

                                                    if (md_oneAddr_col != default(int) || (firstColumn.Equals("md_Phone_col") && md_Phone_col != null)) 
                                                    {
                                                        var mdOneAddrStaging = dataRow.GetValue(md_oneAddr_col);
                                                        if (mdOneAddrStaging.IsNotNullOrEmpty())
                                                        {
                                                            var addrArray = mdOneAddrStaging.Split(',');
                                                            if (addrArray != null && addrArray.Length > 0) 
                                                            {
                                                                if (addrArray.Length > 1)
                                                                {
                                                                    #region Addr1, City
                                                                    var addrOneArray = addrArray[0].Split('|');
                                                                    if (addrOneArray != null && addrOneArray.Length > 0) 
                                                                    {
                                                                        if (addrOneArray.Length > 1)
                                                                        {

                                                                            physician.AddressLine1 = addrOneArray[0].IsNotNullOrEmpty() ? addrOneArray[0].Trim() : "";
                                                                            physician.AddressCity = addrOneArray[1].IsNotNullOrEmpty() ? addrOneArray[1].Trim() : "";
                                                                        }
                                                                        else 
                                                                        {
                                                                            physician.AddressLine1 = addrOneArray[0].IsNotNullOrEmpty() ? addrOneArray[0].Trim() : "";
                                                                            physician.AddressCity = "";
                                                                        }
                                                                        physician.AddressLine2 = "";
                                                                    }
                                                                    #endregion

                                                                    #region StateCode + Zip
                                                                    var addrTwoArray = addrArray[1].Split('|');
                                                                    if (addrTwoArray != null && addrTwoArray.Length > 0) 
                                                                    {
                                                                        if (addrTwoArray.Length > 1)
                                                                        {
                                                                            physician.AddressStateCode = addrTwoArray[0].IsNotNullOrEmpty() ? addrTwoArray[0].Trim() : "";
                                                                            physician.AddressZipCode = addrTwoArray[1].IsNotNullOrEmpty() ? addrTwoArray[1].Trim() : "";
                                                                        }
                                                                        else
                                                                        {
                                                                            if (addrTwoArray[0].Trim().ToString().IsInteger())
                                                                            {
                                                                                physician.AddressZipCode = addrTwoArray[0].IsNotNullOrEmpty() ? addrTwoArray[0].Trim() : "";
                                                                            }
                                                                            else 
                                                                            {
                                                                                physician.AddressStateCode = addrTwoArray[0].IsNotNullOrEmpty() ? addrTwoArray[0].Trim() : "";
                                                                            }
                                                                        }
                                                                    }
                                                                    #endregion
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            physician.AddressLine1 = "";
                                                            physician.AddressLine2 = "";
                                                            physician.AddressCity = "";
                                                            physician.AddressStateCode = "";
                                                            physician.AddressZipCode = "";
                                                        }
                                                        sb.Append("AddressLine1:"); sb.Append(physician.AddressLine1); sb.Append("\t\t");
                                                        sb.Append("AddressLine2:"); sb.Append(physician.AddressLine2); sb.Append("\t\t");
                                                        sb.Append("AddressCity:"); sb.Append(physician.AddressCity); sb.Append("\t\t");
                                                        sb.Append("AddressStateCode:"); sb.Append(physician.AddressStateCode); sb.Append("\t\t");
                                                        sb.Append("AddressZipCode:"); sb.Append(physician.AddressZipCode); sb.Append("\t\t");
                                                        //Finish
                                                        isPhysicianAddressOneCell = true;
                                                    }

                                                    if (isPhysicianAddressOneCell == false)
                                                    {
                                                        #region Address1 + Address2
                                                        if (md_Addr1_col != default(int) || (firstColumn.Equals("md_Addr1_col") && md_Addr1_col != null))
                                                        {
                                                            var addressLine1Staging = dataRow.GetValue(md_Addr1_col);
                                                            if (addressLine1Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressLine1 = addressLine1Staging;
                                                                sb.Append("AddressLine1:"); sb.Append(physician.AddressLine1); sb.Append("\t\t");
                                                            }
                                                        }
                                                        if (md_Addr2_col != default(int) || (firstColumn.Equals("md_Addr2_col") && md_Addr2_col != null))
                                                        {
                                                            var addressLine2Staging = dataRow.GetValue(md_Addr2_col);
                                                            if (addressLine2Staging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressLine2 = addressLine2Staging;
                                                                sb.Append("AddressLine2:"); sb.Append(physician.AddressLine2); sb.Append("\t\t");
                                                            }
                                                        }
                                                        #endregion

                                                        #region City + State + Zip
                                                        if (md_City_col != default(int) || (firstColumn.Equals("md_City_col") && md_City_col != null))
                                                        {
                                                            var addressCityStaging = dataRow.GetValue(md_City_col);
                                                            if (addressCityStaging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressCity = addressCityStaging;
                                                                sb.Append("AddressCity:"); sb.Append(physician.AddressCity); sb.Append("\t\t");
                                                            }
                                                        }
                                                        if (md_State_col != default(int) || (firstColumn.Equals("md_State_col") && md_State_col != null))
                                                        {
                                                            var addressStateCodeStaging = dataRow.GetValue(md_State_col);
                                                            if (addressStateCodeStaging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressStateCode = addressStateCodeStaging;
                                                                sb.Append("AddressStateCode:"); sb.Append(physician.AddressStateCode); sb.Append("\t\t");
                                                            }
                                                        }
                                                        if (md_Zip_col != default(int) || (firstColumn.Equals("md_Zip_col") && md_Zip_col != null))
                                                        {
                                                            var addressZipCodeStaging = dataRow.GetValue(md_Zip_col);
                                                            if (addressZipCodeStaging.IsNotNullOrEmpty())
                                                            {
                                                                physician.AddressZipCode = addressZipCodeStaging;
                                                                sb.Append("AddressZipCode:"); sb.Append(physician.AddressZipCode); sb.Append("\t\t");
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                    #endregion

                                                    #region Phone + Fax
                                                    if (md_Phone_col != default(int) || (firstColumn.Equals("md_Phone_col") && md_Phone_col != null))
                                                    {
                                                        var phoneWorkStaging = dataRow.GetValue(md_Phone_col);
                                                        if (phoneWorkStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.PhoneWork = phoneWorkStaging.ToPhoneDB();
                                                            sb.Append("PhoneWork:"); sb.Append(physician.PhoneWork); sb.Append("\t\t");
                                                        }
                                                    }
                                                    if (md_Fax_col != default(int) || (firstColumn.Equals("md_Fax_col") && md_Fax_col != null))
                                                    {
                                                        var faxNumberStaging = dataRow.GetValue(md_Fax_col);
                                                        if (faxNumberStaging.IsNotNullOrEmpty())
                                                        {
                                                            physician.FaxNumber = faxNumberStaging.ToPhoneDB();
                                                            sb.Append("FaxNumber:"); sb.Append(physician.FaxNumber); sb.Append("\t\t");
                                                        }
                                                    }
                                                    #endregion

                                                    #endregion

                                                    #region COMMENTS
                                                    //StringBuilder sb2 = new StringBuilder();
                                                    //var mobileStaging = dataRow.GetValue(11);
                                                    //if (mobileStaging.IsNotNullOrEmpty())
                                                    //{
                                                    //    sb2.Append("Phone Mobile: "); sb2.Append(mobileStaging); sb2.Append(" ;"); sb2.Append("\t\t");
                                                    //}

                                                    //var commentsStaging = dataRow.GetValue(12);
                                                    //if (commentsStaging.IsNotNullOrEmpty())
                                                    //{
                                                    //    sb2.Append("Comments: "); sb2.Append(commentsStaging); sb2.Append(" ;"); sb2.Append("\t\t");
                                                    //}
                                                    //physician.Comments = sb2.ToString();

                                                    sb.Append("Comments:"); sb.Append(physician.Comments); sb.Append("\t\t");
                                                    txtWrtr.WriteLine(sb.ToString());
                                                    sb.Length = 0;
                                                    #endregion

                                                }
                                                else
                                                {
                                                    Console.WriteLine("Physician Not Added - No NPI Info");
                                                    noNPICounter++;
                                                }
                                                if (isDBWritePermissionGranted == true)
                                                {
                                                    if (Database.Add(physician))
                                                    {
                                                        Console.WriteLine("(" + i.ToString() + ")" + "Physician Added: " + physician.LastName + ", " + physician.FirstName);
                                                        addedCounter++;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("(" + i.ToString() + ")" + "Physician NOT Added: ");
                                                        cannotAddCounter++;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("(" + i.ToString() + ")" + "Physician PRESENT: " + physician.LastName + ", " + physician.FirstName);
                                                skippedCounter++;
                                            }
                                        }
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                    txtWrtr.Close();
                    Console.WriteLine("Finished Writing to Log File");
                    Console.WriteLine(string.Format("{0} : {1}", "ADDED:", addedCounter));
                    Console.WriteLine(string.Format("{0} : {1}:", "NOT ADDED:", cannotAddCounter));
                    Console.WriteLine(string.Format("{0} : {1}:", "NO NPI:", noNPICounter));
                    Console.WriteLine(string.Format("{0} : {1}:", "SKIPPED:", skippedCounter));
                }
                catch (Exception ex)
                {
                    textWriter.Write(ex.ToString());
                }
            }
        }
    }
}
