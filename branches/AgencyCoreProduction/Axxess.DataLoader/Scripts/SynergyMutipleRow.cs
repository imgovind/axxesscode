﻿

namespace Axxess.DataLoader.Domain
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Data;
    using System.Text;

    using Excel;
    using Kent.Boogaart.KBCsv;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    public static class SynergyMutipleRow
    {
        private static string input = Path.Combine(App.Root, "Files\\FamilyHome.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\FamilyHome_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run(Guid agencyId, Guid locationId)
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                int rowCounter = 0;
                                int dataCounter = 0;
                                int count = 0;
                                Patient patientData = null;
                                var insuranceNumber = "";
                                var patientStatus = string.Empty;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        switch (dataCounter)
                                        {
                                            case 0:
                                                if (dataRow.GetValue(0).Trim().Equals("Discharged"))
                                                {
                                                    dataCounter = 0;
                                                }
                                                else
                                                {
                                                    dataCounter++;
                                                    patientData = new Patient();
                                                    patientData.Id = Guid.NewGuid();
                                                    patientData.AgencyId = agencyId;
                                                    patientData.AgencyLocationId = locationId;
                                                    patientData.Status = 1;
                                                    patientData.ServiceLocation = "Q5001";
                                                    patientData.Ethnicities = string.Empty;
                                                    patientData.MaritalStatus = string.Empty;
                                                    patientData.IsDeprecated = false;
                                                    patientData.IsHospitalized = false;
                                                    patientData.Gender = "";

                                                    var nameArray = dataRow.GetValue(0);
                                                    patientData.LastName = nameArray.Split(',')[0].Trim();
                                                    var firstMiddle = nameArray.Split(',')[1].Trim();
                                                    if (firstMiddle.Split(' ').Length > 1)
                                                    {

                                                        if (firstMiddle.Split(' ')[1].Length > 1)
                                                        {
                                                            patientData.FirstName = firstMiddle;
                                                        }
                                                        else
                                                        {
                                                            patientData.FirstName = firstMiddle.Split(' ')[0];
                                                            patientData.MiddleInitial = firstMiddle.Split(' ')[1];
                                                        }
                                                    }
                                                    else
                                                    {
                                                        patientData.FirstName = firstMiddle;
                                                    }

                                                    if (dataRow.GetValue(4).IsNotNullOrEmpty())
                                                    {
                                                        patientData.PatientIdNumber = dataRow.GetValue(4);
                                                    }

                                                    if (dataRow.GetValue(5).IsNotNullOrEmpty())
                                                    {
                                                        //patientData.PrimaryInsurance = "1";
                                                        patientData.MedicareNumber = dataRow.GetValue(5);
                                                        insuranceNumber = dataRow.GetValue(5);
                                                    }


                                                    if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                                    {
                                                        patientData.DOB = DateTime.FromOADate(double.Parse(dataRow.GetValue(7)));
                                                    }

                                                    if (dataRow.GetValue(10).IsNotNullOrEmpty())
                                                    {
                                                        patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(10)));
                                                    }
                                                    if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                                    {
                                                        patientStatus = dataRow.GetValue(14);
                                                        //patientData.Comments += string.Format("Status:{0}.", patientStatus);
                                                    }
                                                    if (dataRow.GetValue(20).IsNotNullOrEmpty())
                                                    {
                                                        patientData.Comments += string.Format("DX Code:{0}.", dataRow.GetValue(20));
                                                    }
                                                    if (dataRow.GetValue(25).IsNotNullOrEmpty())
                                                    {
                                                        patientData.Comments += string.Format("Physician:{0}.", dataRow.GetValue(25));
                                                    }
                                                    if (dataRow.GetValue(35).IsNotNullOrEmpty())
                                                    {
                                                        patientData.Comments += string.Format("Physician Phone:{0}.", dataRow.GetValue(35));
                                                    }
                                                    if (dataRow.GetValue(40).IsNotNullOrEmpty())
                                                    {
                                                        patientData.Comments += string.Format("Physician UPIN:{0}.", dataRow.GetValue(40));
                                                    }

                                                    //patientData.Comments += string.Format("Adm:{0}.", dataRow.GetValue(17));
                                                }
                                              
                                                break;
                                            case 1:
                                                dataCounter=0;

                                                var addressRow = dataRow.GetValue(1);
                                                var addressArray = addressRow.Split(new string[] { "," }, StringSplitOptions.None);
                                                if (addressArray.Length == 2)
                                                {
                                                    var statecode = addressArray[1].Trim();
                                                    patientData.AddressStateCode = (statecode.Split(' '))[0].Trim();
                                                    patientData.AddressZipCode = (statecode.Split(' '))[1].Trim();
                                                    //int end = addressArray[0].LastIndexOf(' ') + 1;
                                                    //patientData.AddressCity = addressArray[0].Substring(end);
                                                    //patientData.AddressLine1 = addressArray[0].Substring(0, end - 1).Trim();

                                                    //var cityNAddress = addressArray[0].Trim();
                                                    //var addressLength=cityNAddress.Length;
                                                    //var city = (cityNAddress.Split(' '))[addressLength - 2].Trim() + " " + (cityNAddress.Split(' '))[addressLength - 1].Trim();
                                                    //patientData.AddressCity = city.Trim();

                                                    int end = addressArray[0].LastIndexOf("  ") + 1;
                                                    patientData.AddressCity = addressArray[0].Substring(end);
                                                    patientData.AddressLine1 = addressArray[0].Substring(0, end - 1).Trim();

                                                }
                                                if (addressArray.Length == 3)
                                                {
                                                    var statecode = addressArray[2].Trim();
                                                    patientData.AddressStateCode = (statecode.Split(' '))[0].Trim();
                                                    patientData.AddressZipCode = (statecode.Split(' '))[1].Trim();

                                                    int end = addressArray[1].LastIndexOf("  ") + 1;
                                                    patientData.AddressCity = addressArray[1].Substring(end);
                                                    patientData.AddressLine1 = addressArray[0];
                                                    patientData.AddressLine2 = addressArray[1].Substring(0, end - 1).Trim();

                                                }

                                                if (dataRow.GetValue(14).IsNotNullOrEmpty())
                                                {
                                                    patientData.PhoneHome=dataRow.GetValue(14).ToPhoneDB();
                                                }

                                                if (dataRow.GetValue(32).IsNotNullOrEmpty() && dataRow.GetValue(32).ToLower().Contains("medicare"))
                                                {
                                                    patientData.PrimaryInsurance = "1";
                                                }
                                                else
                                                {
                                                    patientData.MedicareNumber = "";
                                                    patientData.Comments += string.Format("Insurance:{0}. ", dataRow.GetValue(32));
                                                    patientData.Comments += string.Format("Insurance Number:{0}. ", insuranceNumber);
                                                }



                                            //    break;
                                            //case 2:
                                            //    if (dataRow.GetValue(1).Trim().Equals("Notes :"))
                                            //    {
                                            //        patientData.Comments += string.Format("Notes: {0}. ", dataRow.GetValue(2).Replace("\r", " ").Replace("\n", " "));
                                            //    }

                                            //    else
                                            //    {
                                            //        dataCounter = 0;
                                            //        if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                            //        {
                                            //            patientData.Comments += string.Format("Primary Diagnosis:{0}.", dataRow.GetValue(2));
                                            //        }
                                            //        if (dataRow.GetValue(7).IsNotNullOrEmpty())
                                            //        {
                                            //            //var startDate = dataRow.GetValue(7).Replace("-","");
                                            //            patientData.Comments += string.Format("Episode :{0}. ", dataRow.GetValue(7));

                                            //        }

                                                    patientData.Created = DateTime.Now;
                                                    patientData.Modified = DateTime.Now;

                                                    var medicationProfile = new MedicationProfile
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        PatientId = patientData.Id,
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        Medication = "<ArrayOfMedication />"
                                                    };

                                                    var allergyProfile = new AllergyProfile
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        AgencyId = agencyId,
                                                        PatientId = patientData.Id,
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        Allergies = "<ArrayOfAllergy />"
                                                    };

                                                    var exist = Database.GetPatientByMR(patientData.PatientIdNumber, patientData.AgencyId);
                                                    //if (exist == null && !patientStatus.Trim().Equals("Admitted") && !patientStatus.Trim().Equals("Eval"))
                                                    if (exist == null )
                                                    {
                                                        if (Database.Add(patientData) && Database.Add(medicationProfile) && Database.Add(allergyProfile))
                                                        {
                                                            var admissionPeriod = new PatientAdmissionDate
                                                            {
                                                                Id = Guid.NewGuid(),
                                                                AgencyId = agencyId,
                                                                Created = DateTime.Now,
                                                                DischargedDate = DateTime.MinValue,
                                                                IsActive = true,
                                                                IsDeprecated = false,
                                                                Modified = DateTime.Now,
                                                                PatientData = patientData.ToXml().Replace("'", ""),
                                                                PatientId = patientData.Id,
                                                                Reason = string.Empty,
                                                                StartOfCareDate = patientData.StartofCareDate,
                                                                Status = patientData.Status
                                                            };
                                                            if (Database.Add(admissionPeriod))
                                                            {
                                                                var patient = Database.GetPatient(patientData.Id, agencyId);
                                                                if (patient != null)
                                                                {
                                                                    patient.AdmissionId = admissionPeriod.Id;
                                                                    if (Database.Update(patient))
                                                                    {
                                                                        count++;
                                                                        Console.WriteLine("{0}) {1}", count, patientData.DisplayName);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                //}
                                                break;
                                        }
                                        
                                        //dataCounter++;

                                    }
                                   
                                  
                                    rowCounter++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
