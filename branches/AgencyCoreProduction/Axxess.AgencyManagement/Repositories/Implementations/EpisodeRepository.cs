﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.Repository;
using Axxess.Core;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Domain;
using Axxess.Core.Infrastructure;

namespace Axxess.AgencyManagement.Repositories
{
    public class EpisodeRepository : IEpisodeRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private readonly string connectionStringName;

        public EpisodeRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            this.connectionStringName = "AgencyManagementConnectionString";
        }

        #endregion

        public bool AddEpisodeModel(PatientEpisode patientEpisode)
        {
            var result = false;
            if (patientEpisode != null)
            {
                try
                {

                    patientEpisode.Created = DateTime.Now;
                    patientEpisode.Modified = DateTime.Now;
                    database.Add<PatientEpisode>(patientEpisode);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateEpisodeModel(PatientEpisode patientEpisode)
        {
            var result = false;
            try
            {
                if (patientEpisode != null)
                {
                    patientEpisode.Modified = DateTime.Now;
                    result = database.Update<PatientEpisode>(patientEpisode) > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public PatientEpisode GetEpisodeOnly(Guid agencyId, Guid episodeId, Guid patientId)
        {
            return database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
        }



        //new

        public PatientEpisode GetCurrentEpisodeLean(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT
                        patientepisodes.Id as Id,
                        patientepisodes.EndDate, 
                        patientepisodes.StartDate
                                    FROM 
                                        patientepisodes
                                            WHERE 
                                                patientepisodes.AgencyId = @agencyid AND 
                                                patientepisodes.IsActive = 1 AND
                                                patientepisodes.IsDischarged = 0 AND 
                                                patientepisodes.PatientId = @patientid AND
                                                DATE(@date) BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate limit 0,1";

            var currentEpisode = new PatientEpisode();

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                currentEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", DateTime.Now)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsSingle();
            }

            return currentEpisode;
        }


        public PatientEpisode GetCurrentEpisodeLeanWithDetail(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT
                        patientepisodes.Id as Id,
                        patientepisodes.EndDate, 
                        patientepisodes.StartDate,
                        patientepisodes.Details 
                                    FROM 
                                        patientepisodes
                                            WHERE 
                                                patientepisodes.AgencyId = @agencyid AND 
                                                patientepisodes.IsActive = 1 AND
                                                patientepisodes.IsDischarged = 0 AND 
                                                patientepisodes.PatientId = @patientid AND
                                                DATE(@date) BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate limit 0,1";

            var currentEpisode = new PatientEpisode();

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                currentEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", DateTime.Now)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    Details = reader.GetStringNullable("Details"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsSingle();
            }

            return currentEpisode;
        }

        //new
        public List<PatientEpisode> GetMutipleEpisodeLeanWithDetail(Guid agencyId, Guid patientId, List<Guid> episodeIds)
        {
            var script =string.Format(@"SELECT
                        patientepisodes.Id as Id,
                        patientepisodes.EndDate, 
                        patientepisodes.StartDate
                        patientepisodes.Details 
                                    FROM 
                                        patientepisodes
                                            WHERE 
                                                patientepisodes.AgencyId = @agencyid AND 
                                                patientepisodes.PatientId = @patientid AND
                                                patientepisodes.Id IN ({0})", episodeIds.ToCommaSeperatedList());

            var patientEpisodes = new List<PatientEpisode>();

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisodes = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    Details = reader.GetStringNullable("Details"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }

            return patientEpisodes;
        }



        public PatientEpisode GetEpisodeLeanWithDetail(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var script = @"SELECT
                        patientepisodes.EndDate, 
                        patientepisodes.StartDate,
                        patientepisodes.Details 
                                    FROM 
                                        patientepisodes
                                            WHERE 
                                                patientepisodes.AgencyId = @agencyid AND 
                                                patientepisodes.PatientId = @patientid AND
                                                patientepisodes.Id =  @episodeid";

            PatientEpisode patientEpisode = null;

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = episodeId,
                    PatientId = patientId,
                    Details = reader.GetStringNullable("Details"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsSingle();
            }

            return patientEpisode;
        }

        public PatientEpisode GetEpisodeLean(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var script = @"SELECT
                        patientepisodes.EndDate, 
                        patientepisodes.StartDate,
                        patientepisodes.StartOfCareDate as StartOfCareDate
                                    FROM 
                                        patientepisodes
                                            WHERE 
                                                patientepisodes.AgencyId = @agencyid AND 
                                                patientepisodes.PatientId = @patientid AND
                                                patientepisodes.Id =  @episodeid";

            PatientEpisode patientEpisode = null;

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = episodeId,
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate")
                })
                .AsSingle();
            }

            return patientEpisode;
        }



        public DateRange GetEpisodeDateRange(Guid agencyId, Guid patientId, Guid id)
        {
            var script = @"SELECT
                            pe.StartDate,
                            pe.EndDate
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.Id = @id  limit 0,1";
            var dateRange = new DateRange();
            using (var cmd = new FluentCommand<DateRange>(script))
            {
                dateRange = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("id", id)
                .SetMap(reader => new DateRange
                {
                    Id = id.ToString(),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                })
                .AsSingle();
            }
            return dateRange;
        }

        public List<PatientEpisode> GetEpisodeDatasBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var list = new List<PatientEpisode>();
            var script = @"SELECT 
                            pe.Id as EpisodeId, 
                            pe.EndDate as EndDate,
                            pe.StartDate as StartDate
                                FROM
                                    patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND
                                                pe.PatientId = @patientid AND 
                                                pe.IsActive = 1 AND
                                                pe.IsDischarged = 0 AND
                                                DATE(pe.StartDate) <= DATE(@enddate) AND
                                                DATE(pe.EndDate) >= DATE(@startdate)";

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("enddate", endDate)
                .AddDateTime("startdate", startDate)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("EpisodeId"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list.OrderBy(l => l.StartDate).ToList();
        }


        public PatientEpisode GetCurrentEpisodeOrLastEpisode(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT
                        patientepisodes.Id as Id,
                        patientepisodes.EndDate, 
                        patientepisodes.StartDate
                                    FROM 
                                        patientepisodes 
                                            WHERE 
                                                patientepisodes.AgencyId = @agencyid AND 
                                                patientepisodes.IsActive = 1 AND
                                                patientepisodes.IsDischarged = 0 AND 
                                                patientepisodes.PatientId = @patientid AND
                                                ( (patientepisodes.StartDate <= DATE(@date)) OR (DATE(@date) between  patientepisodes.StartDate and patientepisodes.EndDate))
                                                 ORDER BY  patientepisodes.StartDate DESC limit 0,1";

            PatientEpisode currentEpisode = null;

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                currentEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                })
                .AsSingle();
            }
            return currentEpisode;
        }

        public PatientEpisode GetCurrentEpisodeOrLastEpisodeWithDetail(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT
                        patientepisodes.Id as Id,
                        patientepisodes.EndDate, 
                        patientepisodes.StartDate,
                        patientepisodes.Details 
                                    FROM 
                                        patientepisodes 
                                            WHERE 
                                                patientepisodes.AgencyId = @agencyid AND 
                                                patientepisodes.IsActive = 1 AND
                                                patientepisodes.IsDischarged = 0 AND 
                                                patientepisodes.Id = @patientid AND
                                                ( (patientepisodes.StartDate <= DATE(@date)) OR (DATE(@date) between  patientepisodes.StartDate and patientepisodes.EndDate))
                                                 ORDER BY  patientepisodes.StartDate DESC limit 0,1";

            PatientEpisode currentEpisode = null;

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                currentEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Details = reader.GetStringNullable("Details")
                })
                .AsSingle();
            }
            return currentEpisode;
        }

        public PatientEpisode GetNextEpisodeDataLean(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(pe.StartDate) > DATE(@date)  ORDER BY pe.StartDate ASC limit 0,1";
            var dateRange = new PatientEpisode();

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                dateRange = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsSingle();
            }
            return dateRange;
        }

        public PatientEpisode GetNextEpisodeDataLeanWithDetail(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate,
                            pe.Details 
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(pe.StartDate) > DATE(@date)  ORDER BY pe.StartDate ASC limit 0,1";
            var dateRange = new PatientEpisode();

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                dateRange = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Details = reader.GetStringNullable("Details")
                })
                .AsSingle();
            }
            return dateRange;
        }

        public PatientEpisode GetPreviousEpisodeDataLean(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(pe.EndDate) < DATE(@date)  ORDER BY pe.EndDate DESC limit 0,1";
            var patientEpisode = new PatientEpisode();

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public PatientEpisode GetPreviousEpisodeDataLeanWithDetail(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate,
                             pe.Details 
                                FROM 
                                    patientepisodes pe
                                        WHERE
                                            pe.AgencyId = @agencyid AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(pe.EndDate) < DATE(@date)  ORDER BY pe.StartDate DESC limit 0,1";
            var patientEpisode = new PatientEpisode();

            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Details = reader.GetStringNullable("Details")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public PatientEpisode GetEpisodeLeanByStartDate(Guid agencyId, Guid patientId, DateTime date)
        {
            var script = @"SELECT 
                            patientepisodes.Id as Id ,
                            patientepisodes.PatientId as PatientId ,
                            patientepisodes.StartDate as StartDate ,
                            patientepisodes.EndDate as EndDate,
                            patientepisodes.StartOfCareDate as StartOfCareDate
                                FROM
                                    patientepisodes 
                                        WHERE 
                                            patientepisodes.AgencyId = @agencyid AND 
                                            patientepisodes.PatientId = @patientid AND
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND
                                            DATE(patientepisodes.StartDate) = DATE(@date)  
                                            ORDER BY patientepisodes.StartDate ASC LIMIT 0,1 ";

            var patientEpisode = new PatientEpisode();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("date", date)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate")
                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public PatientEpisode GetPreviousEpisodeByEndDate(Guid agencyId, Guid patientId, DateTime endDate)
        {
            var script = @"SELECT 
                            pe.Id, 
                            pe.EndDate, 
                            pe.StartDate,
                            pe.StartOfCareDate as StartOfCareDate
                                FROM 
                                    patientepisodes pe
                                        WHERE 
                                            pe.AgencyId = @agencyid  AND
                                            pe.PatientId = @patientid AND
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(pe.EndDate) = DATE(@enddate) 
                                                ORDER BY DATE(pe.EndDate) DESC LIMIT 1";

            var episode = new PatientEpisode();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                episode = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate")
                })
                .AsSingle();
            }
            return episode;
        }


        public PatientEpisode GetLastEpisode(Guid agencyId, Guid patientId)
        {
            PatientEpisode episode = null;
            if (!patientId.IsEmpty())
            {
                var script = @"SELECT 
                            patientepisodes.Id as Id ,
                            patientepisodes.EndDate as EndDate , 
                            patientepisodes.IsLinkedToDischarge as IsLinkedToDischarge, 
                            patientepisodes.StartDate as StartDate ,
                            patientepisodes.StartOfCareDate as StartOfCareDate ,
                            patientepisodes.AdmissionId as AdmissionId  
                                FROM 
                                    patientepisodes 
                                            WHERE 
                                                patientepisodes.AgencyId = @agencyid  AND
                                                patientepisodes.PatientId = @patientid AND 
                                                patientepisodes.IsActive = 1  AND
                                                patientepisodes.IsDischarged = 0 
                                                    ORDER BY patientepisodes.StartDate DESC LIMIT 1 ";
                using (var cmd = new FluentCommand<PatientEpisode>(script))
                {
                    episode = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new PatientEpisode
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = patientId,
                        IsLinkedToDischarge = reader.GetBoolean("IsLinkedToDischarge"),
                        EndDate = reader.GetDateTime("EndDate"),
                        StartDate = reader.GetDateTime("StartDate"),
                        StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                        AdmissionId = reader.GetGuid("AdmissionId")
                    })
                    .AsSingle();
                }
            }
            return episode;
        }



        public List<PatientEpisode> GetPatientActiveEpisodesLeanWithDetail(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate,
                            pe.Details 
                                FROM 
                                    patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND 
                                                pe.IsActive = 1 AND
                                                pe.IsDischarged = 0 AND 
                                                pe.PatientId = @patientid";
            var list = new List<PatientEpisode>();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Details = reader.GetStringNullable("Details")
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisode> GetPatientActiveEpisodesLeanWithDetailAndAdmissionId(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate,
                            pe.AdmissionId,
                            pe.Details 
                                FROM 
                                    patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND 
                                                pe.IsActive = 1 AND
                                                pe.IsDischarged = 0 AND 
                                                pe.PatientId = @patientid";
            var list = new List<PatientEpisode>();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Details = reader.GetStringNullable("Details")
                })
                .AsList();
            }
            return list;
        }

        public List<PatientEpisode> GetPatientActiveEpisodesLeanWithOutDetail(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate
                                FROM 
                                    patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND 
                                                pe.IsActive = 1 AND
                                                pe.IsDischarged = 0 AND 
                                                pe.PatientId = @patientid";
            var list = new List<PatientEpisode>();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list;
        }


        public List<PatientEpisode> GetPatientActiveEpisodesLeanByDateRange(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var script = @"SELECT 
                            pe.Id,
                            pe.EndDate,
                            pe.StartDate, 
                            pe.Details 
                                FROM 
                                    patientepisodes pe
                                        WHERE 
                                            pe.AgencyId = @agencyid  AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0 AND 
                                            pe.PatientId = @patientid AND 
                                            DATE(pe.StartDate) <= DATE(@enddate) AND
                                            DATE(pe.EndDate) >= DATE(@startdate)";

            var list = new List<PatientEpisode>();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    Details = reader.GetStringNullable("Details"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
            }
            return list.OrderBy(e => e.StartDate).ToList();
        }

        public PatientEpisode GetEpisodeByIdWithSOC(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = @"SELECT 
                        patientepisodes.StartDate as StartDate ,
                        patientepisodes.EndDate as EndDate , 
                        patientepisodes.AdmissionId as AdmissionId , 
                        patientadmissiondates.StartOfCareDate as StartOfCareDate 
                            FROM
                                patientepisodes INNER JOIN patientadmissiondates ON patientepisodes.AdmissionId = patientadmissiondates.Id 
                                    WHERE 
                                        patientepisodes.AgencyId = @agencyid AND
                                        patientadmissiondates.AgencyId = @agencyid AND
                                        patientepisodes.Id = @episodeid AND 
                                        patientepisodes.PatientId = @patientid LIMIT 1 ";

            var patientEpisode = new PatientEpisode();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = episodeId,
                    PatientId = patientId,
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    AdmissionId = reader.GetGuid("AdmissionId"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate")
                })
                .AsSingle();
            }
            return patientEpisode;
        }


        public bool RemoveEpisode(Guid agencyId, Guid patientId, Guid episodeId)
        {
            return EntityHelper.RemovePatientEntity<PatientEpisode, Guid>(agencyId, patientId, episodeId, "AgencyManagementConnectionString");
        }


        public List<PatientsAndEpisode> GetPatientAndEpisodesByBranchStatusInsurance(Guid agencyId, Guid branchId, int statusId, string insuranceIds, DateTime startDate, DateTime endDate)
        {
            List<PatientsAndEpisode> outPatientEpisodes = null;
            if (startDate != null && endDate != null) { } else { startDate = DateTime.Now.AddDays(-59); endDate = DateTime.Now; }
            string branchString = default(string);
            if (!branchId.IsNull() && branchId.IsNotEmpty()) { branchString = " AND patients.AgencyLocationId = @branchId "; }
            string statusString = default(string);
            if (!statusId.IsNull() && statusId != 0) { statusString = " AND patients.`Status` = @statusid "; }
            string insuranceString = default(string);
            if (insuranceIds == string.Empty)
            {
                insuranceString = string.Empty;
            }
            else if (insuranceIds.IsNotNullOrEmpty())
            {
                insuranceString = " AND (patients.PrimaryInsurance IN ( @insuranceids ) || patients.SecondaryInsurance IN (  @insuranceids ) || patients.TertiaryInsurance IN (  @insuranceids )) ";
            }
            var script = string.Format(@"SELECT
                                patients.Id as patientId,
                                patients.PrimaryInsurance as patientPrimaryInsurance,
                                patients.SecondaryInsurance as patientSecondaryInsurance,
                                patients.TertiaryInsurance as patientTertiaryInsurance,
                                patientepisodes.`Schedule` as patientEpisodeSchedule 
                                    FROM 
                                        agencymanagement.patients 
                                            INNER JOIN agencymanagement.patientepisodes ON patients.Id = patientepisodes.PatientId 
                                                WHERE 
                                                    patients.AgencyId = @agencyid  {0} {1} {2} AND
                                                    patientepisodes.StartDate <= DATE(@enddate) AND 
                                                    patientepisodes.EndDate >= DATE(@startdate)"
                                                                            , branchString, statusString, insuranceString);
            using (var cmd = new FluentCommand<PatientsAndEpisode>(script))
            {
                outPatientEpisodes = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("branchid", branchId)
                 .AddDateTime("startdate", startDate)
                 .AddDateTime("enddate", endDate)
                 .AddInt("statusid", statusId)
                 .AddString("insuranceids", insuranceIds)
                 .SetMap(reader => new PatientsAndEpisode
                 {
                     Id = reader.GetGuidIncludeEmpty("patientId"),
                     PrimaryInsurance = reader.GetStringNullable("patientPrimaryInsurance"),
                     SecondaryInsurance = reader.GetStringNullable("patientSecondaryInsurance"),
                     TertiaryInsurance = reader.GetStringNullable("patientTertiaryInsurance"),
                     ///Schedule = reader.GetStringNullable("patientEpisodeSchedule"),
                     ScheduleEvents = reader.GetStringNullable("patientEpisodeSchedule").ToObject<List<ScheduleEvent>>()
                 }).AsList();
            }
            return outPatientEpisodes;
        }

       

        public List<PatientEpisode> EpisodesToDischarge(Guid agencyId, Guid patientId, DateTime dischargeDate, Guid admissionId)
        {
            return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.AdmissionId == admissionId).Where(e => e.StartDate.Date >= dischargeDate.Date).ToList();
        }


        public PatientEpisode GetEpisodeBetweenDischarge(Guid agencyId, Guid patientId, Guid admissionId, DateTime date)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");

            PatientEpisode episode = null;
            if (!patientId.IsEmpty())
            {
                var script = @"SELECT 
                            pe.Id as Id ,
                            pe.EndDate as EndDate , 
                            pe.IsLinkedToDischarge as IsLinkedToDischarge, 
                            pe.StartDate as StartDate ,
                            pe.StartOfCareDate as StartOfCareDate ,
                            pe.AdmissionId as AdmissionId  
                                FROM 
                                    patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid  AND
                                                pe.PatientId = @patientid AND 
                                                pe.AdmissionId= @ AND
                                                pe.IsActive = 1  AND
                                                pe.IsDischarged = 0 AND
                                                DATE(pe.StartDate) < DATE(@date) AND DATE(pe.EndDate) >= @date
                                                    ORDER BY pe.StartDate DESC LIMIT 1 ";
                using (var cmd = new FluentCommand<PatientEpisode>(script))
                {
                    episode = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("admissionid", admissionId)
                    .AddDateTime("date", date)
                    .SetMap(reader => new PatientEpisode
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = patientId,
                        IsLinkedToDischarge = reader.GetBoolean("IsLinkedToDischarge"),
                        EndDate = reader.GetDateTime("EndDate"),
                        StartDate = reader.GetDateTime("StartDate"),
                        StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                        AdmissionId = reader.GetGuid("AdmissionId")
                    })
                    .AsSingle();
                }
            }
            return episode;
            //PatientEpisode episode = null;
            //try
            //{
            //    episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => se.StartDate.Date <= date.Date && se.EndDate.Date >= date.Date).OrderByDescending(se => se.StartDate.Date).ThenByDescending(se => se.EndDate.Date).FirstOrDefault();
            //}
            //catch (Exception e)
            //{
            //    return null;
            //}
            //return episode;
        }

        public List<PatientEpisode> GetEpisodeLeanForDischarge(Guid agencyId, Guid patientId, Guid admissionId, DateTime dischargeDate)
        {
            var script = @"SELECT
                            pe.Id,
                            pe.EndDate, 
                            pe.StartDate,
                            pe.IsDischarged,
                            pe.IsLinkedToDischarge
                                FROM 
                                    patientepisodes pe
                                            WHERE 
                                                pe.AgencyId = @agencyid AND 
                                                pe.PatientId = @patientid AND
                                                pe.AdmissionId = @admissionid AND
                                                DATE(pe.EndDate) > DATE(@dischargedate)";
            var list = new List<PatientEpisode>();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("admissionid", admissionId)
                .AddDateTime("dischargedate", dischargeDate)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    IsLinkedToDischarge = reader.GetBoolean("IsLinkedToDischarge")
                })
                .AsList();
            }
            return list;
        }

        public bool UpdateEpisodeForDischarge(List<PatientEpisode> patientEpisodes)
        {
            var result = false;
            try
            {
                if (patientEpisodes.Count > 0)
                {
                    database.UpdateMany<PatientEpisode>(patientEpisodes);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }


        public bool IsEpisodeExist(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, Guid excludedEpisodeIdOptional)
        {
            var script = string.Format(@"SELECT 
                              1
                                FROM 
                                    patientepisodes pe
                                        WHERE 
                                            pe.AgencyId = @agencyid  AND
                                            pe.PatientId = @patientid {0} AND 
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0 AND 
                                            DATE(pe.StartDate) <= DATE(@enddate) AND
                                            DATE(pe.EndDate) >= DATE(@startdate) limit 1", !excludedEpisodeIdOptional.IsEmpty() ? " AND  pe.Id != @id " : string.Empty);

            //(DATE(pe.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate))";

            var count = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("id", excludedEpisodeIdOptional)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AsScalar();
            }
            return count > 0;
        }

        public bool IsEpisodeExist(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var script = @"SELECT 
                              1
                                FROM 
                                    patientepisodes pe
                                        WHERE 
                                            pe.AgencyId = @agencyid  AND
                                            pe.PatientId = @patientid AND 
                                            pe.Id = @id   limit 1";
            var count = 0;
            using (var cmd = new FluentCommand<int>(script))
            {
                count = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("id", episodeId)
                .AddGuid("patientid", patientId)
                .AsScalar();
            }
            return count > 0;
            //var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == episodeId);
            //if (episode != null)
            //{
            //    return true;
            //}
            //return false;
        }


        public List<PatientEpisode> GetPatientAllEpisodesWithNoException(Guid agencyId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId).ToList();
        }

        public List<EpisodeLean> GetPatientDeactivatedAndDischargedEpisodes(Guid agencyId, Guid patientId)
        {
            var script = @"SELECT 
                        patientepisodes.Id,
                        patientepisodes.PatientId,
                        patientepisodes.IsActive,
                        patientepisodes.IsDischarged, 
                        patientepisodes.EndDate, 
                        patientepisodes.StartDate
                            FROM 
                                patientepisodes 
                                    WHERE 
                                        patientepisodes.AgencyId = @agencyid AND
                                        patientepisodes.PatientId = @patientId AND
                                        (patientepisodes.IsDischarged = 1 || patientepisodes.IsActive = 0)";

            return new FluentCommand<EpisodeLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new EpisodeLean
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    IsActive = reader.GetBoolean("IsActive"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate")
                })
                .AsList();
        }

        public PatientEpisode GetPatientEpisodeFluent(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = @"SELECT
                        patientepisodes.Id as Id ,
                        patientepisodes.EndDate,
                        patientepisodes.StartDate , 
                        patients.Id as PatientId,
                        patients.FirstName, 
                        patients.PatientIdNumber ,
                        patients.LastName, 
                        patients.MiddleInitial
                            FROM
                                patientepisodes 
                                    INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
                                        WHERE 
                                            patientepisodes.AgencyId = @agencyid AND 
                                            patientepisodes.PatientId = @patientId AND 
                                            patientepisodes.Id = @episodeId AND
                                            (patientepisodes.IsDischarged = 0 && patientepisodes.IsActive = 1)";
            var patientEpisode = new PatientEpisode();
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeId", episodeId)
                .SetMap(reader => new PatientEpisode
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    DisplayName = string.Format("{0}, {1} {2}", reader.GetStringNullable("LastName"), reader.GetStringNullable("FirstName"), reader.GetStringNullable("MiddleInitial").ToInitial())

                })
                .AsSingle();
            }
            return patientEpisode;
        }

        public bool SetRecertFlag(Guid agencyId, Guid episodeId, Guid patientId, bool isRecertComplete)
        {
            try
            {
                var script = @"UPDATE patientepisodes set IsRecertCompleted = @isrecertcomplete ,Modified =@modified WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id;";
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                         .AddGuid("agencyid", agencyId)
                         .AddGuid("patientid", patientId)
                         .AddGuid("id", episodeId)
                         .AddInt("isrecertcomplete", isRecertComplete ? 1 : 0)
                         .AddDateTime("modified", DateTime.Now)
                         .AsNonQuery();
                }
                return count > 0;
                //var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                //if (episode != null)
                //{
                //    episode.IsRecertCompleted = isRecertComplete;
                //    episode.Modified = DateTime.Now;
                //    database.Update<PatientEpisode>(episode);
                //    result = true;
                //}
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DischargeEpisodeWithSchedules(Guid agencyId, Guid patientId, Guid admissionId, DateTime startDate, bool IsDischarge)
        {
            try
            {
                var script = string.Format(@"UPDATE 
                                              patientepisodes pe 
                                                LEFT JOIN scheduletasks st ON  st.EpisodeId = pe.Id 
                                                     SET 
                                                        pe.IsDischarged = @isdischarged,
                                                        pe.Modified= @modified, 
                                                        st.IsDischarged = @isdischarged
                                                            WHERE 
                                                                pe.AgencyId = @agencyid  AND
                                                                pe.Patientid = @patientid AND
                                                                pe.AdmissionId = @admissionId AND
                                                                pe.StartDate >= @startdate ");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                         .AddGuid("agencyid", agencyId)
                         .AddGuid("patientid", patientId)
                         .AddGuid("admissionId", admissionId)
                         .AddInt("isdischarged", IsDischarge ? 1 : 0)
                         .AddDateTime("startdate", startDate)
                         .AddDateTime("modified", DateTime.Now)
                         .AsNonQuery();
                }
                return count > 0;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool LinkEpisodeDischargeWithSchedules(Guid agencyId, Guid patientId, Guid admissionId, DateTime dischargeDate, bool IsLinkedToDischarge)
        {
            try
            {
                var script = string.Format(@"UPDATE 
                                              patientepisodes pe 
                                                LEFT JOIN scheduletasks st ON  st.EpisodeId = pe.Id 
                                                     SET 
                                                        pe.IsLinkedToDischarge = @islinkedtodischarge,
                                                        pe.Endate = @enddate,
                                                        pe.Modified= @modified, 
                                                        st.Endate = @enddate
                                                            WHERE 
                                                                pe.AgencyId = @agencyid  AND
                                                                pe.Patientid = @patientid AND
                                                                pe.AdmissionId = @admissionId AND
                                                                pe.StartDate > @enddate AND  pe.EndDate >=@enddate  ");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                         .AddGuid("agencyid", agencyId)
                         .AddGuid("patientid", patientId)
                         .AddGuid("admissionId", admissionId)
                         .AddInt("islinkedtodischarge", IsLinkedToDischarge ? 1 : 0)
                         .AddDateTime("enddate", dischargeDate)
                         .AddDateTime("modified", DateTime.Now)
                         .AsNonQuery();
                }
                return count > 0;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool LinkEpisodeDischargeWithSchedulesByEpisodeId(Guid agencyId, Guid patientId, Guid episodeId, DateTime dischargeDate, bool IsLinkedToDischarge)
        {
            try
            {
                var script = string.Format(@"UPDATE 
                                              patientepisodes pe 
                                                LEFT JOIN scheduletasks st ON  st.EpisodeId = pe.Id 
                                                     SET 
                                                        pe.IsLinkedToDischarge = @islinkedtodischarge,
                                                        pe.Endate = @enddate,
                                                        pe.Modified= @modified, 
                                                        st.Endate = @enddate
                                                            WHERE 
                                                                pe.AgencyId = @agencyid  AND
                                                                pe.Patientid = @patientid AND
                                                                pe.Id = @id A");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(connectionStringName)
                         .AddGuid("agencyid", agencyId)
                         .AddGuid("patientid", patientId)
                         .AddGuid("id", episodeId)
                         .AddDateTime("enddate", dischargeDate)
                         .AddInt("islinkedtodischarge", IsLinkedToDischarge ? 1 : 0)
                         .AddDateTime("modified", DateTime.Now)
                         .AsNonQuery();
                }
                return count > 0;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
