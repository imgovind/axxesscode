﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;

    using AutoMapper;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;

    public class AssetRepository : IAssetRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssetRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssetRepository Methods

        public Asset Get(Guid id, Guid agencyId)
        {
            return database.Single<Asset>(a => a.Id == id && a.AgencyId == agencyId && a.IsDeprecated == false);
        }

        public bool Delete(Guid id,Guid agencyId)
        {
            //var asset = database.Single<Asset>(a => a.AgencyId==agencyId && a.Id == id);
            //if (asset != null)
            //{
            //    asset.IsDeprecated = true;
            //    asset.Modified = DateTime.Now;
            //    database.Update<Asset>(asset);
            //    return true;
            //}
            //return false;
            return EntityHelper.ToggleEntityDeprecation<Asset, Guid>(id, agencyId, true, "AgencyManagementConnectionString");
        }

     

        public bool Remove(Guid agencyId, Guid id)
        {
            var result = false;
            try
            {
                var count = 0;
                var script = string.Format("DELETE FROM assets  WHERE AgencyId = @agencyid  AND Id = @id ");
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("id", id).AsNonQuery();
                }
                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public IList<Asset> GetAssetLean(Guid agencyId, List<Guid> assetIds)
        {
            var list = new List<Asset>();
            if (assetIds.IsNotNullOrEmpty())
            {
                var script = string.Format(@"SELECT 
                                Id as Id ,
                                FileName as FileName 
                                    FROM 
                                        assets  
                                            WHERE 
                                                AgencyId = @agencyid AND 
                                                Id IN({0}) ", assetIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<Asset>(script))
                {
                    list =
                        cmd.SetConnection("AgencyManagementConnectionString")
                           .AddGuid("agencyid", agencyId)
                           .SetMap(
                               reader =>
                               new Asset
                               {
                                   Id = reader.GetGuid("Id"),
                                   FileName = reader.GetStringNullable("FileName")

                               })
                           .AsList();

                }
            }
            return list;
        }

        public bool Add(Asset asset)
        {
            bool result = false;
            if (CoreSettings.UseDBAssets)
            {
                if (AddHelper(asset))
                {
                    result = true;
                }
            }
            else
            {
                if (HttpUpload(CoreSettings.AssetUploadURL, asset))
                {
                    result = true;
                }
            }
            return result;
        }


        private bool AddHelper(Asset asset)
        {

            bool result = false;

            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;

                database.Add<Asset>(asset);
                result = true;
            }

            return result;
        }



        #region PatientDocuments
        public List<PatientDocument> GetPatientDocuments(Guid patientId,Guid agencyId)
        {
            List<PatientDocument> list = new List<PatientDocument>();
            list = database.Find<PatientDocument>(p => p.AgencyId==agencyId && p.PatientId == patientId && p.IsDeprecated == false).ToList();
            return list;
        }

        public bool DeletePatientDocument(Guid id, Guid patientId,Guid agencyId)
        {
            return EntityHelper.TogglePatientEntityDeprecation<Asset>(id, patientId, agencyId, true, "AgencyManagementConnectionString") && Delete(id, agencyId);
        }

        public bool AddPatientDocument(Asset asset, PatientDocument patientDocument)
        {
            bool result = false;
            if (asset != null)
            {
                patientDocument.AssetId = asset.Id;
                patientDocument.Id = Guid.NewGuid();
                patientDocument.Created = DateTime.Now;
                patientDocument.Modified = DateTime.Now;
                database.Add<PatientDocument>(patientDocument);
                result = true;
            }
            return result;
        }

        public bool UpdatePatientDocument(PatientDocument patientDocument)
        {
            var result = false;
            try
            {
                if (patientDocument != null)
                {
                    database.Update<PatientDocument>(patientDocument);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public PatientDocument GetPatientDocument(Guid id, Guid patientId, Guid agencyId)
        {
            return database.Single<PatientDocument>(a => a.Id == id && a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false);
        }
        #endregion

        #endregion

        #region Private Methods

        private bool HttpUpload(string url, Asset asset)
        {

            var result = false;
            byte[] temp = asset.Bytes;

            if (!CoreSettings.StoreAssetBytesInDB)
            {
                asset.Bytes = new Byte[1];
            }

            if (AddHelper(asset))
            {
                return AssetHelper.HttpUploadOrDelete(asset.AgencyId, asset.Id, url, temp, asset.ContentType, asset.FileName);
            }
            return result;
        }

        #endregion
    }
}
