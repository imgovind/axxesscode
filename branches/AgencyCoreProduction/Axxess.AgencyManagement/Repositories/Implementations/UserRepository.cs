﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Domain;
    using Extensions;

    using SubSonic.Repository;
    using Axxess.Core.Enums;

    public class UserRepository : IUserRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public UserRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IUserRepository Members

        public bool Delete(Guid agencyId, Guid userId)
        {
            //var user = database.Single<User>(u => u.AgencyId == agencyId && u.Id == userId);

            //if (user != null)
            //{
            //    user.IsDeprecated = true;
            //    user.Modified = DateTime.Now;
            //    database.Update<User>(user);
            //    UserEngine.Refresh(agencyId);
            //    return true;
            //}
            //return false;
            var result = EntityHelper.ToggleEntityDeprecation<User, Guid>(userId, agencyId, true, "AgencyManagementConnectionString");
            if (result)
            {
                UserEngine.Refresh(agencyId);
            }
            return result;
        }

        public bool Restore(Guid agencyId, Guid userId)
        {
            //var user = database.Single<User>(u => u.AgencyId == agencyId && u.Id == userId);

            //if (user != null)
            //{
            //    user.IsDeprecated = false;
            //    user.Modified = DateTime.Now;
            //    database.Update<User>(user);
            //    UserEngine.Refresh(agencyId);
            //    return true;
            //}

            //return false;
            var result = EntityHelper.ToggleEntityDeprecation<User, Guid>(userId, agencyId, false, "AgencyManagementConnectionString");
            if (result)
            {
                UserEngine.Refresh(agencyId);
            }
            return result;
        }

        public bool SetUserStatus(Guid agencyId, Guid userId, int status)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            bool result = false;

            var user = database.Single<User>(u => u.AgencyId == agencyId && u.Id == userId);

            if (user != null)
            {
                user.Status = status;
                user.Modified = DateTime.Now;
                database.Update<User>(user);
                result = true;
            }

            return result;
        }

        public User GetUserOnly(Guid id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Single<User>(u => u.Id == id && u.AgencyId == agencyId);
        }

        public User GetForLoginOnly(Guid id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");

            if (!id.IsEmpty())
            {
                var user = database.Single<User>(u => u.AgencyId == agencyId && u.Id == id);
                if (user != null)
                {
                    if (user.ProfileData.IsNotNullOrEmpty())
                    {
                        user.Profile = user.ProfileData.ToObject<UserProfile>();
                    }

                    if (user.Permissions.IsNotNullOrEmpty())
                    {
                        user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                    }
                    else
                    {
                        user.PermissionsArray = new List<string>();
                    }


                }

                return user;
            }
            else
            {
                return null;
            }
        }

        #region UserNonVisitRate
        public UserNonVisitRate GetUserNonVisitRate(Guid AgencyId, Guid UserId)
        {
            var result = database.Single<UserNonVisitRate>(u => u.AgencyId == AgencyId && u.UserId == UserId && u.IsDeprecated == false);
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }
        public bool UpdateUserNonVisitRate(UserNonVisitRate userNonVisitRate)
        {
            var result = false;
            if (userNonVisitRate != null)
            {
                userNonVisitRate.Modified = DateTime.Now;
                database.Update<UserNonVisitRate>(userNonVisitRate);
                result = true;
            }
            return result;
        }
        public bool AddUserNonVisitRate(UserNonVisitRate userNonVisitRate)
        {
            if (userNonVisitRate != null && userNonVisitRate.AgencyId.IsNotEmpty() && userNonVisitRate.UserId.IsNotEmpty() && userNonVisitRate.Id.IsNotEmpty())
            {
                userNonVisitRate.Created = DateTime.Now;
                userNonVisitRate.Modified = DateTime.Now;
                var result = database.Add<UserNonVisitRate>(userNonVisitRate);
                return true;
            }
            return false;
        }
        #endregion

        //public User GetByLoginId(Guid loginId)
        //{
        //    Check.Argument.IsNotEmpty(loginId, "loginId");

        //    var user = database.Single<User>(u => u.LoginId == loginId && u.IsDeprecated == false && u.Status == (int) UserStatus.Active);

        //    if (user != null && user.Permissions.IsNotNullOrEmpty())
        //    {
        //        user.PermissionsArray = user.Permissions.ToObject<List<string>>();
        //    }

        //    if (user != null && user.ProfileData.IsNotNullOrEmpty())
        //    {
        //        user.Profile = user.ProfileData.ToObject<UserProfile>();
        //    }

        //    return user;
        //}

        //public IList<User> GetUsersOnly(Guid agencyId)
        //{
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");
        //    return database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false);
        //}

        public IList<User> GetUsersOnly(Guid agencyId, int status)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<User>(u => u.AgencyId == agencyId && u.Status == status && u.IsDeprecated == false);
        }

        public IDictionary<string, object> GetUsersCredential(List<Guid> userIds)
        {
            var userIdList = userIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
            var script = string.Format
                        (
                            "SELECT agencymanagement.users.Id as userId, agencymanagement.users.Credentials as userCredential " +
                            "FROM agencymanagement.users " +
                            "WHERE agencymanagement.users.Id IN ({0});"
                            , userIdList
                        );
            var result = new Dictionary<string, object>();
            using (var cmd = new FluentCommand<object>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetDictonaryId("userId")
                    .SetMap(reader => reader.GetStringNullable("userCredential").ToString()).AsDictionary();
            }
            return result;
        }

        public IDictionary<string, object> GetUsersUserLicensesOnly(Guid agencyId, List<Guid> userIds)
        {
            var userIdList = userIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
            var script = string.Format
                        (
                            @"SELECT 
                                agencymanagement.users.Id as UserId, 
                                agencymanagement.users.Licenses as Licenses
                                    FROM 
                                        agencymanagement.users 
                                            WHERE 
                                            agencymanagement.users.Id IN ({0});"
                                                                    , userIdList
                                                                );
            var result = new Dictionary<string, object>();
            using (var cmd = new FluentCommand<object>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                    .SetDictonaryId("UserId")
                    .SetMap(reader => reader.GetStringNullable("Licenses").ToString()).AsDictionary();
            }
            return result;
        }

        //public IList<User> GetUsersOnlyByBranch(Guid branchId, Guid agencyId)
        //{
        //    Check.Argument.IsNotEmpty(branchId, "branchId");
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");
        //    var users = new List<User>();
        //    var script = @"SELECT users.Id, users.LoginId, users.AgencyId, users.CustomId, users.TitleType, users.TitleTypeOther, " +
        //    "users.FirstName, users.LastName, users.MiddleName, users.Suffix, users.Credentials, users.CredentialsOther, users.Roles, " +
        //    "users.EmploymentType, users.Permissions, users.Licenses, users.Rates, users.ProfileData, users.Messages, " +
        //    "users.Status, users.Created, users.AllowWeekendAccess, users.EarliestLoginTime, users.AutomaticLogoutTime, " +
        //    "users.Comments, users.SSN, users.DOB, users.IsPrimary, users.AccountExpireDate, userlocations.AgencyLocationId " +
        //    "FROM users inner join userlocations " +
        //    "WHERE users.Id=userlocations.UserId AND users.AgencyId = @agencyId AND userlocations.AgencyLocationId=@branchId AND users.IsDeprecated=0";
        //    using (var cmd = new FluentCommand<User>(script))
        //    {
        //        users = cmd.SetConnection("AgencyManagementConnectionString")
        //            .AddGuid("agencyId", agencyId)
        //            .AddGuid("branchId", branchId)
        //            .SetMap(reader => new User
        //            {
        //                Id = reader.GetGuid("Id"),
        //                LoginId = reader.GetGuid("LoginId"),
        //                AgencyId = reader.GetGuid("AgencyId"),
        //                CustomId = reader.GetStringNullable("CustomId"),
        //                TitleType = reader.GetStringNullable("TitleType"),
        //                TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
        //                FirstName = reader.GetStringNullable("FirstName"),
        //                LastName = reader.GetStringNullable("LastName"),
        //                MiddleName = reader.GetStringNullable("MiddleName"),
        //                Suffix = reader.GetStringNullable("Suffix"),
        //                Credentials = reader.GetStringNullable("Credentials"),
        //                CredentialsOther = reader.GetStringNullable("CredentialsOther"),
        //                Roles = reader.GetStringNullable("Roles"),
        //                EmploymentType = reader.GetStringNullable("EmploymentType"),
        //                AgencyLocationId = reader.GetGuid("AgencyLocationId"),
        //                Permissions = reader.GetStringNullable("Permissions"),
        //                Licenses = reader.GetStringNullable("Licenses"),
        //                Rates = reader.GetStringNullable("Rates"),
        //                ProfileData = reader.GetStringNullable("ProfileData"),
        //                Messages = reader.GetStringNullable("Messages"),
        //                Status = reader.GetInt("Status"),
        //                Created = reader.GetDateTime("Created"),
        //                AllowWeekendAccess = reader.GetBoolean("AllowWeekendAccess"),
        //                EarliestLoginTime = reader.GetStringNullable("EarliestLoginTime"),
        //                AutomaticLogoutTime = reader.GetStringNullable("AutomaticLogoutTime"),
        //                Comments = reader.GetStringNullable("Comments"),
        //                SSN = reader.GetStringNullable("SSN"),
        //                DOB = reader.GetDateTime("DOB"),
        //                IsPrimary = reader.GetBoolean("IsPrimary"),
        //                AccountExpireDate = reader.GetDateTime("AccountExpireDate")
        //            }).AsList();
        //    }
        //    return users;
        //}

        public IList<User> GetRatedUserByBranch(Guid branchId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(branchId, "branchId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            var users = new List<User>();
            var script = @"SELECT users.Id, users.LoginId, users.AgencyId, users.CustomId, users.TitleType, users.TitleTypeOther, " +
            "users.FirstName, users.LastName, users.MiddleName, users.Suffix, users.Credentials, users.CredentialsOther, users.Roles, " +
            "users.EmploymentType, users.Permissions, users.Licenses, users.Rates, users.ProfileData, users.Messages, " +
            "users.Status, users.Created, users.AllowWeekendAccess, users.EarliestLoginTime, users.AutomaticLogoutTime, " +
            "users.Comments, users.SSN, users.DOB, users.IsPrimary, users.AccountExpireDate, userlocations.AgencyLocationId " +
            "FROM users inner join userlocations " +
            "WHERE users.Id=userlocations.UserId AND users.AgencyId = @agencyId AND userlocations.AgencyLocationId=@branchId AND users.Rates IS NOT null AND users.IsDeprecated=0";
            using (var cmd = new FluentCommand<User>(script))
            {
                users = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .SetMap(reader => new User
                    {
                        Id = reader.GetGuid("Id"),
                        LoginId = reader.GetGuid("LoginId"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        CustomId = reader.GetStringNullable("CustomId"),
                        TitleType = reader.GetStringNullable("TitleType"),
                        TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        MiddleName = reader.GetStringNullable("MiddleName"),
                        Suffix = reader.GetStringNullable("Suffix"),
                        Credentials = reader.GetStringNullable("Credentials"),
                        CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                        Roles = reader.GetStringNullable("Roles"),
                        EmploymentType = reader.GetStringNullable("EmploymentType"),
                        AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                        Permissions = reader.GetStringNullable("Permissions"),
                        Licenses = reader.GetStringNullable("Licenses"),
                        Rates = reader.GetStringNullable("Rates"),
                        ProfileData = reader.GetStringNullable("ProfileData"),
                        Messages = reader.GetStringNullable("Messages"),
                        Status = reader.GetInt("Status"),
                        Created = reader.GetDateTime("Created"),
                        AllowWeekendAccess = reader.GetBoolean("AllowWeekendAccess"),
                        EarliestLoginTime = reader.GetStringNullable("EarliestLoginTime"),
                        AutomaticLogoutTime = reader.GetStringNullable("AutomaticLogoutTime"),
                        Comments = reader.GetStringNullable("Comments"),
                        SSN = reader.GetStringNullable("SSN"),
                        DOB = reader.GetDateTime("DOB"),
                        IsPrimary = reader.GetBoolean("IsPrimary"),
                        AccountExpireDate = reader.GetDateTime("AccountExpireDate")
                    }).AsList();
            }
            return users;
        }

        //public IList<User> GetUsersOnlyByBranch(Guid branchId, Guid agencyId, int status)
        //{
        //    Check.Argument.IsNotEmpty(branchId, "branchId");
        //    Check.Argument.IsNotEmpty(agencyId, "agencyId");
        //    var users = new List<User>();
        //    var script = @"SELECT users.Id, users.LoginId, users.AgencyId, users.CustomId, users.TitleType, users.TitleTypeOther, " +
        //    "users.FirstName, users.LastName, users.MiddleName, users.Suffix, users.Credentials, users.CredentialsOther, users.Roles, " +
        //    "users.EmploymentType, users.Permissions, users.Licenses, users.Rates, users.ProfileData, users.Messages, " +
        //    "users.Status, users.Created, users.AllowWeekendAccess, users.EarliestLoginTime, users.AutomaticLogoutTime, userlocations.AgencyLocationId, " +
        //    "users.Comments, users.SSN, users.DOB, users.IsPrimary, users.AccountExpireDate " +
        //    "FROM users inner join userlocations " +
        //    "WHERE users.Id=userlocations.UserId AND users.AgencyId = @agencyId AND userlocations.AgencyLocationId=@branchId AND users.Status=@status AND users.IsDeprecated=0";
        //    using (var cmd = new FluentCommand<User>(script))
        //    {
        //        users = cmd.SetConnection("AgencyManagementConnectionString")
        //            .AddGuid("agencyId", agencyId)
        //            .AddGuid("branchId", branchId)
        //            .AddInt("status", status)
        //            .SetMap(reader => new User
        //            {
        //                Id = reader.GetGuid("Id"),
        //                LoginId = reader.GetGuid("LoginId"),
        //                AgencyId = reader.GetGuid("AgencyId"),
        //                CustomId = reader.GetStringNullable("CustomId"),
        //                TitleType = reader.GetStringNullable("TitleType"),
        //                TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
        //                FirstName = reader.GetStringNullable("FirstName"),
        //                LastName = reader.GetStringNullable("LastName"),
        //                MiddleName = reader.GetStringNullable("MiddleName"),
        //                Suffix = reader.GetStringNullable("Suffix"),
        //                Credentials = reader.GetStringNullable("Credentials"),
        //                CredentialsOther = reader.GetStringNullable("CredentialsOther"),
        //                Roles = reader.GetStringNullable("Roles"),
        //                EmploymentType = reader.GetStringNullable("EmploymentType"),
        //                AgencyLocationId = reader.GetGuid("AgencyLocationId"),
        //                Permissions = reader.GetStringNullable("Permissions"),
        //                Licenses = reader.GetStringNullable("Licenses"),
        //                Rates = reader.GetStringNullable("Rates"),
        //                ProfileData = reader.GetStringNullable("ProfileData"),
        //                Messages = reader.GetStringNullable("Messages"),
        //                Status = reader.GetInt("Status"),
        //                Created = reader.GetDateTime("Created"),
        //                AllowWeekendAccess = reader.GetBoolean("AllowWeekendAccess"),
        //                EarliestLoginTime = reader.GetStringNullable("EarliestLoginTime"),
        //                AutomaticLogoutTime = reader.GetStringNullable("AutomaticLogoutTime"),
        //                Comments = reader.GetStringNullable("Comments"),
        //                SSN = reader.GetStringNullable("SSN"),
        //                DOB = reader.GetDateTime("DOB"),
        //                IsPrimary = reader.GetBoolean("IsPrimary"),
        //                AccountExpireDate = reader.GetDateTime("AccountExpireDate")
        //            }).AsList();
        //    }
        //    return users;
        //}

        public IList<User> GetEmployeeRoster(Guid agencyId, Guid branchId, int status)
        {
            var users = new List<User>();
            var script = string.Format(@"SELECT
                                agencymanagement.users.FirstName AS userFirstName,
                                agencymanagement.users.LastName AS userLastName,
                                agencymanagement.users.ProfileData AS userProfileData 
                                    FROM 
                                        agencymanagement.users 
                                            LEFT JOIN agencymanagement.userlocations on agencymanagement.userlocations.UserId = agencymanagement.users.Id 
                                                WHERE
                                                    agencymanagement.users.AgencyId = @agencyId {0} {1} AND 
                                                    agencymanagement.users.IsDeprecated = 0", status == 0 ? string.Empty : "AND agencymanagement.users.`Status` = " + status, !branchId.IsEmpty() ? "AND agencymanagement.userlocations.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<User>(script))
            {
                users = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .SetMap(reader => new User
                    {
                        FirstName = reader.GetStringNullable("userLastName").ToUpperCase(),
                        LastName = reader.GetStringNullable("userFirstName").ToUpperCase(),
                        Profile = reader.GetStringNullable("userProfileData").IsNotNullOrEmpty() ? reader.GetStringNullable("userProfileData").ToObject<UserProfile>() : new UserProfile()
                    })
                    .AsList();
            }
            return users;
        }

        public IList<User> GetAll()
        {
            IList<User> users = new List<User>();
            var agencies = database.Find<Agency>(a => a.IsDeprecated == false && a.IsSuspended == false).ToList();
            agencies.ForEach(a =>
            {
                var agencyUsers = GetAgencyUsers(a.Id);
                agencyUsers.ForEach(u =>
                {
                    users.Add(u);
                });
            });
            return users;
        }

        public IEnumerable<User> All()
        {
            return database.All<User>();
        }

        public int GetActiveUserCount(Guid agencyId)
        {
            return database.Find<User>(u => u.AgencyId == agencyId && u.Status == (int)UserStatus.Active && u.IsDeprecated == false).ToList().Count;
        }

        public IList<User> GetAgencyUsers(Guid agencyId)
        {
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false && u.Status == (int)UserStatus.Active);
            users.ForEach(user =>
            {
                if (user.ProfileData.IsNotNullOrEmpty())
                {
                    user.Profile = user.ProfileData.ToObject<UserProfile>();
                    user.EmailAddress = user.Profile.EmailWork;
                }
                if (user.Permissions.IsNotNullOrEmpty())
                {
                    user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                }
            });
            return users.OrderBy(u => u.FirstName).ToList();
        }

        public IEnumerable<User> GetUsersByStatus(Guid agencyId, int status)
        {
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.Status == status && u.IsDeprecated == false);
            users.ForEach(user =>
            {
                user.LastName = user.LastName.ToTitleCase();
                user.FirstName = user.FirstName.ToTitleCase();
                if (user.ProfileData.IsNotNullOrEmpty())
                {
                    user.Profile = user.ProfileData.ToObject<UserProfile>();
                    user.EmailAddress = user.Profile.EmailWork;
                }
                if (user.Permissions.IsNotNullOrEmpty())
                {
                    user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                }
            });
            return users.OrderBy(u => u.FirstName).ToList();
        }

        public List<User> GetUsersLeanByStatus(Guid agencyId, Guid branchId, int statusId)
        {
            var list = new List<User>();
            var status = " AND ( users.Status = 1 OR users.Status = 2 ) ";
            if (statusId == 1 || statusId == 2)
            {
                status = string.Format(" AND users.Status = {0} ", statusId);
            }
            var script = string.Format(@"SELECT users.Id as Id , users.FirstName as FirstName , users.LastName as LastName , users.Suffix as Suffix, " +
                " users.Created as Created, users.EmploymentType as EmploymentType, users.Comments as Comments, users.ProfileData as ProfileData, " +
                " users.TitleType as TitleType , users.TitleTypeOther as TitleTypeOther , users.Credentials as Credentials , users.CredentialsOther as CredentialsOther " +
                " FROM users " +
                " LEFT JOIN userlocations ul on users.Id = ul.UserId " +                
                " WHERE users.AgencyId = @agencyid AND ul.AgencyLocationId = @branchId AND users.IsDeprecated = 0 {0}", status);
            using (var cmd = new FluentCommand<User>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusId", statusId)
                .SetMap(reader => new User
                {
                    Id = reader.GetGuid("Id"),
                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                    Suffix =  reader.GetStringNullable("Suffix"),
                    Comments = reader.GetStringNullable("Comments"),
                    EmploymentType = reader.GetStringNullable("EmploymentType"),
                    ProfileData = reader.GetStringNullable("ProfileData"),
                    Status = statusId,
                    TitleType = reader.GetStringNullable("TitleType"),
                    TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                    Created = reader.GetDateTime("Created")
                }).AsList();
            }
            return list.OrderBy(u => u.LastName).ThenBy(u => u.FirstName).ToList();
        }

        public IEnumerable<User> GetDeletedUsers(Guid agencyId, bool Isdeprecated)
        {
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == true);
            users.ForEach(user =>
            {
                user.LastName = user.LastName.ToTitleCase();
                user.FirstName = user.FirstName.ToTitleCase();
                if (user.ProfileData.IsNotNullOrEmpty())
                {
                    user.Profile = user.ProfileData.ToObject<UserProfile>();
                    user.EmailAddress = user.Profile.EmailWork;
                }
                if (user.Permissions.IsNotNullOrEmpty())
                {
                    user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                }
            });
            return users.OrderBy(u => u.FirstName).ToList();
        }


        public int GetUserPatientCount(Guid agencyId, Guid userId, byte statusId)
        {
            var script = @"SELECT COUNT(*) FROM patients WHERE patients.AgencyId = @agencyid AND patients.UserId == @userId AND patients.Status = @statusid && patients.IsDeprecated = 0";

            return new FluentCommand<int>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId).AddGuid("userid", userId).AddInt("statusid", statusId).AsScalar();
        }

        public IList<User> GetClinicalUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(user =>
            {
                if (user.Roles.IsClinician() || user.Roles.IsDirectorOfNursing() || user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin())
                {
                    userList.Add(user);
                }
            });
            return userList;
        }

        public IList<User> GetHHAUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(user =>
            {
                if (user.Roles.IsHHA())
                {
                    userList.Add(user);
                }
            });
            return userList;
        }

        public IList<User> GetPTUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(user =>
            {
                if (user.Roles.IsPT())
                {
                    userList.Add(user);
                }
            });
            return userList;
        }

        public IList<User> GetOTUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(user =>
            {
                if (user.Roles.IsOT())
                {
                    userList.Add(user);
                }
            });
            return userList;
        }

        public IList<User> GetLVNUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(user =>
            {
                if (user.Roles.IsNurse())
                {
                    userList.Add(user);
                }
            });
            return userList;
        }

        public IList<User> GetCaseManagerUsers(Guid agencyId)
        {
            var userList = new List<User>();
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(user =>
            {
                if (user.Roles.IsCaseManager() || user.Roles.IsAgencyAdmin() || user.Roles.IsDirectorOfNursing())
                {
                    userList.Add(user);
                }
            });
            return userList;
        }

        public IList<User> GetAuditors(Guid agencyId)
        {
            var userList = new List<User>();
            var users = database.Find<User>(u => u.AgencyId == agencyId && u.Status == 1 && u.IsDeprecated == false);
            users.ForEach(user =>
            {
                if (user.Roles.IsAuditor())
                {
                    userList.Add(user);
                }
            });
            return userList;
        }

        public IList<User> GetUsersByLoginId(Guid loginId)
        {
            return database.Find<User>(u => u.LoginId == loginId && u.IsDeprecated == false && u.Status == (int)UserStatus.Active);
        }

        public IList<User> GetUsersByLoginId(Guid loginId, Guid agencyId)
        {
            return database.Find<User>(u => u.LoginId == loginId && u.AgencyId == agencyId && u.IsDeprecated == false);
        }

        public IList<AgencyLite> GetAgencies(Guid loginId)
        {
            var userAgencies = new List<AgencyLite>();
            var script = new StringBuilder("SELECT agencies.Id AS AgencyId, users.Id AS UserId, agencies.Name, ")
                .Append("IFNULL(users.TitleTypeOther, users.TitleType) as Title, users.Created ")
                .Append("From users, agencies where users.AgencyId = agencies.Id ")
                .AppendFormat("AND users.LoginId = '{0}' AND users.IsDeprecated = 0 ", loginId)
                .Append("AND users.`Status` = 1 AND agencies.IsDeprecated = 0 ORDER BY agencies.Name ASC;").ToString();
            using (var cmd = new FluentCommand<AgencyLite>(script))
            {
                userAgencies = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new AgencyLite
                {
                    Id = reader.GetGuid("AgencyId"),
                    UserId = reader.GetGuid("UserId"),
                    Name = reader.GetStringNullable("Name"),
                    Title = reader.GetStringNullable("Title"),
                    Created = reader.GetDateTime("Created")
                }).AsList();
            }
            return userAgencies;
        }

        public IList<User> GetAgencyUsers(string query, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(query, "query");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            return database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false && u.FirstName.Contains(query)).Take(15).ToList();
        }

        //public LicenseItem GetUserLicenseItem(Guid licenseId, Guid userId, Guid agencyId)
        //{
        //    LicenseItem licenseItem = null;

        //    var user = Get(userId, agencyId, false);
        //    if (user != null)
        //    {
        //        var license = user.LicensesArray.Find(l => l.Id == licenseId && l.IsDeprecated == false);
        //        if (license != null)
        //        {
        //            licenseItem = new LicenseItem
        //            {
        //                Id = license.Id,
        //                UserId = user.Id,
        //                AssetId = license.AssetId,
        //                DisplayName = user.DisplayName,
        //                LicenseType = license.LicenseType,
        //                IssueDate = license.InitiationDate,
        //                ExpireDate = license.ExpirationDate
        //            };
        //        }
        //    }
        //    return licenseItem;
        //}

        //public IList<License> GetUserLicenses(Guid agencyId, Guid userId, bool isAssetNeeded)
        //{
        //    var user = Get(userId, agencyId, false);

        //    if (user != null)
        //    {
        //        if (isAssetNeeded)
        //        {
        //            user.LicensesArray.ForEach(license =>
        //            {
        //                if (!license.AssetId.IsEmpty())
        //                {
        //                    var asset = database.Single<Asset>(a => a.AgencyId == agencyId && a.Id == license.AssetId);
        //                    if (asset != null)
        //                    {
        //                        license.AssetUrl = string.Format("<a href=\"/Asset/{0}\">{1}</a>&#160;", asset.Id.ToString(), asset.FileName);
        //                    }
        //                }
        //            });
        //        }
        //        return user.LicensesArray;
        //    }

        //    return new List<License>();
        //}

        //public List<UserRate> GetUserRates(Guid agencyId, Guid userId)
        //{
        //    var user = Get(userId, agencyId, false);
        //    if (user != null) return user.RatesArray;
        //    return new List<UserRate>();
        //}

        //public License GetUserLicense(Guid id, Guid userId, Guid agencyId)
        //{
        //    var user = Get(userId, agencyId, false);
        //    if (user != null) return user.LicensesArray.First(l => l.Id == id);
        //    return new License();
        //}

        public LicenseItem GetNonUserLicense(Guid licenseId, Guid agencyId)
        {
            return database.Single<LicenseItem>(l => l.Id == licenseId && l.AgencyId == agencyId && l.IsDeprecated == false);
        }

        public bool AddNonUserLicense(LicenseItem licenseItem)
        {
            var result = false;
            if (licenseItem != null)
            {
                database.Add<LicenseItem>(licenseItem);
                result = true;
            }
            return result;
        }

        public bool DeleteNonUserLicense(Guid licenseId, Guid agencyId)
        {
            //bool result = false;
            //var licenseItem = GetNonUserLicense(licenseId, agencyId);
            //if (licenseItem != null)
            //{
            //    licenseItem.IsDeprecated = true;
            //    licenseItem.Modified = DateTime.Now;
            //    database.Update<LicenseItem>(licenseItem);
            //    result = true;
            //}
            //return result;
            return EntityHelper.ToggleEntityDeprecation<LicenseItem, Guid>(licenseId, agencyId, true, "AgencyManagementConnectionString");
        }

        public bool UpdateNonUserLicense(LicenseItem licenseItem)
        {
            bool result = false;
            if (licenseItem != null)
            {
                var existing = database.Single<LicenseItem>(l => l.Id == licenseItem.Id && l.AgencyId == licenseItem.AgencyId);
                if (existing != null)
                {
                    existing.FirstName = licenseItem.FirstName;
                    existing.LastName = licenseItem.LastName;
                    existing.IssueDate = licenseItem.IssueDate;
                    existing.ExpireDate = licenseItem.ExpireDate;
                    existing.Modified = DateTime.Now;
                    if (database.Update<LicenseItem>(existing) > 0)
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public IList<LicenseItem> GetNonUserLicenses(Guid agencyId)
        {
            return database.Find<LicenseItem>(l => l.AgencyId == agencyId && l.IsDeprecated == false).ToList();
        }

        public IList<LicenseItem> GetSoftwareUserLicenses(Guid agencyId)
        {
            var users = GetAgencyUsers(agencyId);
            var userLicenses = new List<LicenseItem>();
            if (users != null && users.Count > 0)
            {
                users.ForEach(user =>
                {
                    if (user.Licenses.IsNotNullOrEmpty())
                    {
                        user.LicensesArray = user.Licenses.ToObject<List<License>>();
                        user.LicensesArray.ForEach(license =>
                        {
                            userLicenses.Add(new LicenseItem
                            {
                                Id = license.Id,
                                UserId = user.Id,
                                AgencyId = user.AgencyId,
                                AssetId = license.AssetId,
                                FirstName = user.FirstName,
                                LastName = user.LastName,
                                LicenseType = license.LicenseType,
                                IssueDate = license.InitiationDate,
                                ExpireDate = license.ExpirationDate
                            });
                        });
                    }
                });
                return userLicenses;
            }
            return new List<LicenseItem>();
        }

        public bool Add(User user)
        {
            var result = false;
            if (user != null)
            {
                user.Id = Guid.NewGuid();
                if (user.AgencyRoleList.Count > 0)
                {
                    user.Roles = user.AgencyRoleList.ToArray().AddColons();
                }
                user.Status = (int)UserStatus.Active;
                user.ProfileData = user.Profile.ToXml();
                user.Messages = new List<MessageState>().ToXml();
                if (user.PermissionsArray.Count > 0)
                {
                    user.Permissions = user.PermissionsArray.ToXml();
                }
                user.Created = DateTime.Now;
                user.Modified = DateTime.Now;
                database.Add<User>(user);
                UserEngine.AddOrUpdate(user.AgencyId,user);
                result = true;

                if (user.LocationList != null && user.LocationList.Count > 0)
                {
                    var userLocations = database.Find<UserLocation>(l => l.AgencyId == user.AgencyId && l.UserId == user.Id);
                    var userLocationIds = userLocations != null ? userLocations.Select(l => l.AgencyLocationId).ToList() : new List<Guid>();
                    user.LocationList.ForEach(locationId =>
                    {
                        if (!userLocationIds.Contains(locationId))
                        {
                            database.Add<UserLocation>(new UserLocation
                            {
                                UserId = user.Id,
                                Id = Guid.NewGuid(),
                                AgencyId = user.AgencyId,
                                AgencyLocationId = locationId
                            });
                        }

                    });

                    userLocationIds.ForEach(locationId =>
                    {
                        if (!user.LocationList.Contains(locationId))
                        {
                            var userLocation = userLocations.Where(l => l.AgencyLocationId == locationId).FirstOrDefault();
                            if (userLocation != null)
                            {
                                database.Delete<UserLocation>(userLocation.Id);
                            }
                        }
                    });
                }
            }
            return result;
        }

        public bool Refresh(User user)
        {
            bool result = false;
            if (user != null)
            {
                user.Modified = DateTime.Now;
                if (database.Update<User>(user) > 0)
                {
                    UserEngine.AddOrUpdate(user.AgencyId, user);
                    result = true;
                }
            }
            return result;
        }

        public bool Update(User user)
        {
            bool result = false;
            if (user != null)
            {
                var editUser = database.Single<User>(u => u.Id == user.Id && u.AgencyId == user.AgencyId && u.IsDeprecated == false);
                if (editUser != null)
                {
                    editUser.CustomId = user.CustomId;
                    //editUser.AgencyLocationId = user.AgencyLocationId;
                    editUser.EmploymentType = user.EmploymentType;
                    if (user.AgencyRoleList != null && user.AgencyRoleList.Count > 0) editUser.Roles = user.AgencyRoleList.ToArray().AddColons();
                    editUser.FirstName = user.FirstName;
                    editUser.LastName = user.LastName;
                    editUser.MiddleName = user.MiddleName;
                    editUser.Suffix = user.Suffix;
                    editUser.TitleType = user.TitleType;
                    editUser.TitleTypeOther = user.TitleTypeOther;
                    editUser.Credentials = user.Credentials;
                    editUser.CredentialsOther = user.CredentialsOther;
                    editUser.Profile = editUser.ProfileData.ToObject<UserProfile>();
                    if (user.Profile != null)
                    {
                        editUser.Profile.AddressLine1 = user.Profile.AddressLine1;
                        editUser.Profile.AddressLine2 = user.Profile.AddressLine2;
                        editUser.Profile.AddressCity = user.Profile.AddressCity;
                        editUser.Profile.AddressZipCode = user.Profile.AddressZipCode;
                        editUser.Profile.AddressStateCode = user.Profile.AddressStateCode;
                        editUser.Profile.Gender = user.Profile.Gender;
                    }
                    if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0) editUser.Profile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
                    if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0) editUser.Profile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
                    if (user.FaxPhoneArray != null && user.FaxPhoneArray.Count > 0) editUser.Profile.PhoneFax = user.FaxPhoneArray.ToArray().PhoneEncode();
                    editUser.AllowWeekendAccess = user.AllowWeekendAccess;
                    editUser.EarliestLoginTime = user.EarliestLoginTime;
                    editUser.AutomaticLogoutTime = user.AutomaticLogoutTime;
                    editUser.ProfileData = editUser.Profile.ToXml();
                    editUser.Comments = user.Comments;
                    editUser.Modified = DateTime.Now;
                    editUser.DOB = user.DOB;
                    editUser.SSN = user.SSN;
                    editUser.AccountExpireDate = user.AccountExpireDate;
                    editUser.HireDate = user.HireDate;
                    editUser.TerminationDate = user.TerminationDate;
                    if (database.Update<User>(editUser) > 0)
                    {
                        result = true;
                        UserEngine.AddOrUpdate(editUser.AgencyId, editUser);
                    }

                    if (user.LocationList != null && user.LocationList.Count > 0)
                    {
                        var userLocations = database.Find<UserLocation>(l => l.AgencyId == user.AgencyId && l.UserId == user.Id);
                        var userLocationIds = userLocations != null ? userLocations.Select(l => l.AgencyLocationId).ToList() : new List<Guid>();
                        user.LocationList.ForEach(locationId =>
                        {
                            if (!userLocationIds.Contains(locationId))
                            {
                                database.Add<UserLocation>(new UserLocation
                                {
                                    UserId = user.Id,
                                    Id = Guid.NewGuid(),
                                    AgencyId = user.AgencyId,
                                    AgencyLocationId = locationId
                                });
                            }

                        });

                        userLocationIds.ForEach(locationId =>
                        {
                            if (!user.LocationList.Contains(locationId))
                            {
                                var userLocation = userLocations.Where(l => l.AgencyLocationId == locationId).FirstOrDefault();
                                if (userLocation != null)
                                {
                                    database.Delete<UserLocation>(userLocation.Id);
                                }
                            }
                        });
                    }
                }
            }
            return result;
        }

        public bool UpdateModel(User user)
        {
            bool result = false;
            if (user != null)
            {
                user.Modified = DateTime.Now;
                if (database.Update<User>(user) > 0)
                {
                    result = true;
                    UserEngine.AddOrUpdate(user.AgencyId, user);
                }
            }
            return result;
        }

        //public bool UpdateModel(User user, bool IsCacheRefresh)
        //{
        //    bool result = false;
        //    if (user != null)
        //    {
        //        user.Modified = DateTime.Now;
        //        if (database.Update<User>(user) > 0)
        //        {
        //            result = true;
        //            if (IsCacheRefresh)
        //            {
        //                UserEngine.AddOrUpdate(user.AgencyId, user);
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public bool UpdateProfile(User user)
        //{
        //    bool result = false;

        //    if (user != null)
        //    {
        //        Check.Argument.IsNotEmpty(user.AgencyId, "AgencyId");
        //        var userToEdit = database.Single<User>(u => u.AgencyId == user.AgencyId && u.Id == user.Id && u.IsDeprecated == false);

        //        if (userToEdit != null && userToEdit.ProfileData.IsNotNullOrEmpty())
        //        {
        //            userToEdit.Profile = userToEdit.ProfileData.ToObject<UserProfile>();

        //            userToEdit.Profile.AddressLine1 = user.Profile.AddressLine1;
        //            userToEdit.Profile.AddressLine2 = user.Profile.AddressLine2;
        //            userToEdit.Profile.AddressCity = user.Profile.AddressCity;
        //            userToEdit.Profile.AddressZipCode = user.Profile.AddressZipCode;
        //            userToEdit.Profile.AddressStateCode = user.Profile.AddressStateCode;

        //            if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0)
        //            {
        //                userToEdit.Profile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
        //            }
        //            if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0)
        //            {
        //                userToEdit.Profile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
        //            }

        //            userToEdit.ProfileData = userToEdit.Profile.ToXml();
        //            userToEdit.Modified = DateTime.Now;

        //            if (database.Update<User>(userToEdit) > 0)
        //            {
        //                UserEngine.AddOrUpdate(user.AgencyId,userToEdit);
        //                result = true;
        //            }
        //        }
        //    }
        //    return result;
        //}

        //public void AddUserEvent(Guid agencyId, Guid patientId, Guid userId, UserEvent userEvent)
        //{

        //    if (!agencyId.IsEmpty() && !patientId.IsEmpty() && !userId.IsEmpty() && userEvent != null)
        //    {
        //        var userEpisode = database.Single<UserSchedule>(us => us.AgencyId == agencyId && us.PatientId == patientId && us.UserId == userId);
        //        try
        //        {
        //            if (userEpisode != null)
        //            {
        //                var events = userEpisode.Visits.ToString().ToObject<List<UserEvent>>();
        //                events.Add(userEvent);
        //                userEpisode.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
        //                userEpisode.Modified = DateTime.Now;
        //                database.Update<UserSchedule>(userEpisode);
        //            }
        //            else if (userEpisode == null)
        //            {
        //                var userSchedule = new UserSchedule();

        //                try
        //                {
        //                    userSchedule.Id = Guid.NewGuid();
        //                    userSchedule.PatientId = patientId;
        //                    userSchedule.UserId = userId;
        //                    userSchedule.AgencyId = agencyId;
        //                    List<UserEvent> events = new List<UserEvent>();
        //                    events.Add(userEvent);
        //                    userSchedule.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
        //                    userSchedule.Created = DateTime.Now;
        //                    userSchedule.Modified = DateTime.Now;
        //                    database.Add<UserSchedule>(userSchedule);

        //                }
        //                catch (Exception e)
        //                {
        //                    //TODO: Log Exception
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            //TODO Log Exception
        //        }
        //    }
        //}

        //public bool Reassign(Guid agencyId, ScheduleEvent scheduleEvent, Guid userId)
        //{
        //    bool result = false;
        //    if (!agencyId.IsEmpty() && !userId.IsEmpty() && scheduleEvent != null)
        //    {
        //        var employeeEpisode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == scheduleEvent.PatientId && e.UserId == scheduleEvent.UserId);
        //        UserEvent newEvent = null;
        //        try
        //        {
        //            if (employeeEpisode != null)
        //            {
        //                var events = employeeEpisode.Visits.ToString().ToObject<List<UserEvent>>();

        //                newEvent = events.FirstOrDefault(e => e.EventId == scheduleEvent.EventId);
        //                if (newEvent != null)
        //                {
        //                    events.RemoveAll(e => e.EventId == newEvent.EventId);
        //                }
        //                employeeEpisode.Visits = Convert.ToString(XElement.Parse(events.ToXml()));
        //                if (newEvent != null)
        //                {
        //                    newEvent.UserId = userId;
        //                }
        //                else
        //                {
        //                    newEvent = new UserEvent
        //                    {
        //                        EventId = scheduleEvent.EventId,
        //                        PatientId = scheduleEvent.PatientId,
        //                        EpisodeId = scheduleEvent.EpisodeId,
        //                        EventDate = scheduleEvent.EventDate,
        //                        Discipline = scheduleEvent.Discipline,
        //                        DisciplineTask = scheduleEvent.DisciplineTask,
        //                        Status = scheduleEvent.Status,
        //                        IsMissedVisit = scheduleEvent.IsMissedVisit,
        //                        UserId = userId,
        //                        TimeIn = scheduleEvent.TimeIn,
        //                        TimeOut = scheduleEvent.TimeOut,
        //                        IsDeprecated = scheduleEvent.IsDeprecated,

        //                    };
        //                }
        //                AddUserEvent(agencyId, scheduleEvent.PatientId, userId, newEvent);
        //                database.Update<UserSchedule>(employeeEpisode);
        //                result = true;
        //            }
        //            else
        //            {
        //                newEvent = new UserEvent
        //                {
        //                    EventId = scheduleEvent.EventId,
        //                    PatientId = scheduleEvent.PatientId,
        //                    EpisodeId = scheduleEvent.EpisodeId,
        //                    EventDate = scheduleEvent.EventDate,
        //                    Discipline = scheduleEvent.Discipline,
        //                    DisciplineTask = scheduleEvent.DisciplineTask,
        //                    Status = scheduleEvent.Status,
        //                    IsMissedVisit = scheduleEvent.IsMissedVisit,
        //                    UserId = userId,
        //                    TimeIn = scheduleEvent.TimeIn,
        //                    TimeOut = scheduleEvent.TimeOut,
        //                    IsDeprecated = scheduleEvent.IsDeprecated,

        //                };
        //                AddUserEvent(agencyId, scheduleEvent.PatientId, userId, newEvent);
        //                result = true;
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            return result;
        //        }
        //    }
        //    return result;
        //}

        //public bool DeleteScheduleEvent(Guid patientId, Guid eventId, Guid userId, Guid agencyId)
        //{
        //    bool result = false;
        //    if (!eventId.IsEmpty() && !patientId.IsEmpty() && !userId.IsEmpty())
        //    {
        //        var userEpisode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == userId);
        //        if (userEpisode != null)
        //        {
        //            List<UserEvent> events = userEpisode.Visits.ToString().ToObject<List<UserEvent>>();
        //            events.RemoveAll(evnt => evnt.EventId == eventId);
        //            userEpisode.Visits = events.ToXml();
        //            database.Update<UserSchedule>(userEpisode);
        //            result = true;
        //        }
        //    }
        //    return result;
        //}

        //public bool RemoveScheduleEvent(Guid agencyId, Guid patientId, Guid eventId, Guid userId)
        //{
        //    bool result = false;
        //    if (!agencyId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty() && !userId.IsEmpty())
        //    {
        //        var userEpisode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == userId);
        //        if (userEpisode != null)
        //        {
        //            var events = userEpisode.Visits.ToString().ToObject<List<UserEvent>>();
        //            events.RemoveAll(ev => ev.EventId == eventId);
        //            userEpisode.Visits = events.ToXml();
        //            database.Update<UserSchedule>(userEpisode);
        //            result = true;
        //        }
        //    }

        //    return result;
        //}

        //public UserEvent GetEvent(Guid agencyId, Guid userId, Guid patientId, Guid eventId)
        //{
        //    UserEvent evnt = null;
        //    if (!agencyId.IsEmpty() && !userId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
        //    {
        //        var episode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == userId);
        //        if (episode != null && episode.Visits.IsNotNullOrEmpty())
        //        {
        //            evnt = episode.Visits.ToObject<List<UserEvent>>().Where(e => e.EventId == eventId).FirstOrDefault();
        //        }
        //    }
        //    return evnt;
        //}

        //public IList<UserEvent> GetSchedules(Guid agencyId)
        //{
        //    var userEvents = new List<UserEvent>();
        //    var userSchedules = database.Find<UserSchedule>(us => us.AgencyId == agencyId);
        //    userSchedules.ForEach(userSchedule =>
        //    {
        //        var userVisits = userSchedule.Visits.IsNotNullOrEmpty() ? userSchedule.Visits.ToObject<List<UserEvent>>() : new List<UserEvent>();
        //        if (userVisits.Count > 0)
        //        {
        //            userVisits.ForEach(userVisit =>
        //            {
        //                var eventDate = userVisit.EventDate.IsNotNullOrEmpty() ? DateTime.Parse(userVisit.EventDate) : DateTime.MaxValue;
        //                if (eventDate != DateTime.MaxValue && eventDate < DateTime.Now.AddDays(7))
        //                {
        //                    userEvents.Add(userVisit);
        //                }
        //            });
        //        }
        //    });
        //    return userEvents;
        //}

        ////public IList<UserEvent> GetSchedule(Guid agencyId, Guid userId)
        //{
        //    var userEvents = new List<UserEvent>();
        //    var userSchedules = database.Find<UserSchedule>(us => us.AgencyId == agencyId && us.UserId == userId);
        //    if (userSchedules != null)
        //    {
        //        userSchedules.ForEach(userSchedule =>
        //        {
        //            var userVisits = userSchedule.Visits.IsNotNullOrEmpty() ? userSchedule.Visits.ToObject<List<UserEvent>>() : new List<UserEvent>();
        //            if (userVisits.Count > 0)
        //            {
        //                userVisits.ForEach(userVisit =>
        //                {
        //                    if (userVisit.Discipline != Disciplines.Claim.ToString())
        //                    {
        //                        userEvents.Add(userVisit);
        //                    }
        //                });
        //            }
        //        });
        //    }
        //    return userEvents;
        //}

        //public IList<UserSchedule> GetScheduleWidget(Guid agencyId, Guid userId)
        //{
        //    var script = @"SELECT userschedules.PatientId, userschedules.Visits, patients.FirstName, patients.LastName FROM userschedules INNER JOIN patients ON userschedules.PatientId = patients.Id WHERE userschedules.UserId = @userid AND userschedules.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patients.Status = 1 LIMIT 0, 100";
        //    return new FluentCommand<UserSchedule>(script)
        //        .SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("userid", userId)
        //        .AddGuid("agencyid", agencyId)
        //        .SetMap(reader => new UserSchedule
        //        {
        //            Visits = reader.GetString("Visits"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
        //        })
        //        .AsList();
        //}

        //public IList<UserSchedule> GetScheduleLean(Guid agencyId, Guid userId)
        //{
        //    var list = new List<UserSchedule>();
        //    var script = @"SELECT userschedules.PatientId, userschedules.Visits, patients.FirstName, patients.LastName, patientepisodes.StartDate, patientepisodes.EndDate, patientepisodes.Details, patientepisodes.Schedule FROM userschedules INNER JOIN patients ON userschedules.PatientId = patients.Id INNER JOIN patientepisodes ON userschedules.PatientId = patientepisodes.PatientId WHERE userschedules.UserId = @userid AND userschedules.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND ( patients.Status = 1 OR patients.Status = 2)";

        //    using (var cmd = new FluentCommand<UserSchedule>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("userid", userId)
        //        .AddGuid("agencyid", agencyId)
        //        .SetMap(reader => new UserSchedule
        //        {
        //            PatientId = reader.GetGuid("PatientId"),
        //            Visits = reader.GetStringNullable("Visits"),
        //            EpisodeDetails = reader.GetStringNullable("Details"),
        //            EpisodeSchedule = reader.GetStringNullable("Schedule"),
        //            EpisodeEndDate = reader.GetDateTime("EndDate"),
        //            EpisodeStartDate = reader.GetDateTime("StartDate"),
        //            PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase()
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //public IList<UserSchedule> GetScheduleLean(Guid agencyId, Guid userId, DateTime start, DateTime end)
        //{
        //    var list = new List<UserSchedule>();
        //    var script = @"SELECT userschedules.PatientId, userschedules.Visits, patients.FirstName, patients.LastName, patientepisodes.StartDate, patientepisodes.EndDate, patientepisodes.Details, patientepisodes.Schedule FROM userschedules INNER JOIN patients ON userschedules.PatientId = patients.Id INNER JOIN patientepisodes ON userschedules.PatientId = patientepisodes.PatientId WHERE userschedules.UserId = @userid AND userschedules.AgencyId = @agencyid AND patients.IsDeprecated = 0 AND patientepisodes.IsActive = 1 AND patientepisodes.IsDischarged = 0 AND DATE(patientepisodes.StartDate) > DATE(@startdate) AND DATE(patientepisodes.EndDate) < DATE(@enddate) AND (patients.Status = 1 OR patients.Status = 2)";

        //    using (var cmd = new FluentCommand<UserSchedule>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .AddGuid("userid", userId)
        //        .AddGuid("agencyid", agencyId)
        //        .AddDateTime("startdate", start)
        //        .AddDateTime("enddate", end)
        //        .SetMap(reader => new UserSchedule
        //        {
        //            Visits = reader.GetString("Visits"),
        //            PatientId = reader.GetGuid("PatientId"),
        //            EpisodeDetails = reader.GetString("Details"),
        //            EpisodeSchedule = reader.GetString("Schedule"),
        //            EpisodeEndDate = reader.GetDateTime("EndDate"),
        //            EpisodeStartDate = reader.GetDateTime("StartDate"),
        //            PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
        //        })
        //        .AsList();
        //    }
        //    return list;
        //}

        //public IList<UserEvent> GetSchedule(Guid agencyId, Guid userId, DateTime start, DateTime end)
        //{
        //    var userEvents = new List<UserEvent>();
        //    var userSchedules = database.Find<UserSchedule>(us => us.UserId == userId && us.AgencyId == agencyId);
        //    if (userSchedules != null)
        //    {
        //        userSchedules.ForEach(userSchedule =>
        //        {
        //            var userVisits = userSchedule.Visits.IsNotNullOrEmpty() ? userSchedule.Visits.ToObject<List<UserEvent>>() : new List<UserEvent>();
        //            if (userVisits.Count > 0)
        //            {
        //                userVisits.ForEach(userVisit =>
        //                {
        //                    if (userVisit.Discipline != Disciplines.Claim.ToString() && userVisit.EventDate.IsValidDate() && userVisit.EventDate.ToDateTime().Date >= start && userVisit.EventDate.ToDateTime().Date <= end)
        //                    {
        //                        userVisit.EventDate = userVisit.EventDate.ToZeroFilled();
        //                        userEvents.Add(userVisit);
        //                    }
        //                });
        //            }
        //        });
        //    }
        //    return userEvents.OrderByDescending(s => s.EventDate).ToList();
        //}

        //public IList<UserSchedule> GetUserSchedules(Guid agencyId, List<Guid> patientIds)
        //{
        //    return database.Find<UserSchedule>(s => s.AgencyId == agencyId).Where(ss => patientIds.Contains(ss.PatientId)).ToList();
        //}

        //public IList<UserSchedule> GetUserSchedules(Guid agencyId, Guid userId, List<Guid> patientIds)
        //{
        //    return database.Find<UserSchedule>(s => s.AgencyId == agencyId && s.UserId == userId).Where(ss => patientIds.Contains(ss.PatientId)).ToList();
        //}

        //public bool UpdateEvent(Guid agencyId, UserEvent userEvent)
        //{
        //    bool result = false;

        //    if (userEvent != null)
        //    {
        //        var userEpisode = database.Single<UserSchedule>(e => e.AgencyId == agencyId && e.UserId == userEvent.UserId && e.PatientId == userEvent.PatientId);
        //        if (userEpisode != null && !string.IsNullOrEmpty(userEpisode.Visits))
        //        {
        //            var events = userEpisode.Visits.ToObject<List<UserEvent>>();
        //            events.ForEach(e =>
        //            {
        //                if (e.EventId == userEvent.EventId)
        //                {
        //                    e.UserId = userEvent.UserId;
        //                    e.Discipline = userEvent.Discipline;
        //                    e.PatientId = userEvent.PatientId;
        //                    e.EventDate = userEvent.EventDate;
        //                    e.VisitDate = userEvent.VisitDate;
        //                    e.Status = userEvent.Status;
        //                    e.EpisodeId = userEvent.EpisodeId;
        //                    e.DisciplineTask = userEvent.DisciplineTask;
        //                    e.Discipline = userEvent.Discipline;
        //                    e.IsMissedVisit = userEvent.IsMissedVisit;
        //                    e.IsDeprecated = userEvent.IsDeprecated;

        //                    return;
        //                }

        //            });
        //            userEpisode.Modified = DateTime.Now;
        //            userEpisode.Visits = events.ToXml();
        //            database.Update<UserSchedule>(userEpisode);
        //            result = true;
        //        }
        //    }
        //    return result;
        //}

//        public List<User> GetAllUsers(Guid agencyId, Guid branchId, int statusId)
//        {
//            var list = new List<User>();
//            var status = " AND ( users.Status = 1 OR users.Status = 2 ) ";
//            if (statusId == 1 || statusId == 2)
//            {
//                status = string.Format(" AND users.Status = {0} ", statusId);
//            }
//            var script = string.Format(@"SELECT
//                            users.Id as Id ,
//                            users.FirstName as FirstName ,
//                            users.LastName as LastName , 
//                            users.Permissions as Permissions ,
//                            users.Licenses as Licenses , 
//                            users.Status as  Status, 
//                            users.Created as Created ,
//                            users.CustomId, 
//                            users.TitleType as TitleType ,
//                            users.TitleTypeOther as  TitleTypeOther ,
//                            users.Credentials as Credentials , 
//                            users.CredentialsOther as CredentialsOther 
//                                FROM 
//                                    users 
//                                        WHERE 
//                                            users.AgencyId = @agencyid AND
//                                            users.IsDeprecated = 0 {0} {1} 
//                                                ORDER BY users.LastName ASC , users.FirstName ASC ", status, !branchId.IsEmpty() ? " AND users.AgencyLocationId = @branchId " : string.Empty);
//            using (var cmd = new FluentCommand<User>(script))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("branchId", branchId)
//                .AddInt("statusId", statusId)
//                .SetMap(reader => new User
//                {
//                    Id = reader.GetGuid("Id"),
//                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
//                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
//                    CustomId = reader.GetStringNullable("CustomId"),
//                    Permissions = reader.GetStringNullable("Permissions"),
//                    Licenses = reader.GetStringNullable("Licenses"),
//                    Status = reader.GetInt("Status"),
//                    TitleType = reader.GetStringNullable("TitleType"),
//                    TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
//                    Credentials = reader.GetStringNullable("Credentials"),
//                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
//                    Created = reader.GetDateTime("Created")
//                }).AsList();
//            }
//            return list;
//        }

//        public Dictionary<string, User> GetAllUsers(Guid agencyId)
//        {
//            var list = new Dictionary<string, User>();

//            var script = string.Format(@"SELECT 
//                                        users.Id as Id , 
//                                        users.FirstName as FirstName ,
//                                        users.LastName as LastName ,
//                                        users.Suffix as Suffix ,
//                                        users.Permissions as Permissions , 
//                                        users.Licenses as Licenses , 
//                                        users.Status as  Status, 
//                                        users.Created as Created ,
//                                        users.TitleType as TitleType ,
//                                        users.TitleTypeOther as  TitleTypeOther ,
//                                        users.Credentials as Credentials ,
//                                        users.CredentialsOther as CredentialsOther ,
//                                        users.IsDeprecated as  IsDeprecated 
//                                            FROM 
//                                                users 
//                                                    WHERE 
//                                                        users.AgencyId = @agencyid 
//                                                             ORDER BY users.LastName ASC , users.FirstName ASC ");
//            using (var cmd = new FluentCommand<User>(script))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                .AddGuid("agencyid", agencyId)
//                .SetDictonaryId("Id")
//                .SetMap(reader => new User
//                {
//                    Id = reader.GetGuid("Id"),
//                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
//                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
//                    Permissions = reader.GetStringNullable("Permissions"),
//                    Licenses = reader.GetStringNullable("Licenses"),
//                    Status = reader.GetInt("Status"),
//                    TitleType = reader.GetStringNullable("TitleType"),
//                    TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
//                    Credentials = reader.GetStringNullable("Credentials"),
//                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
//                    Created = reader.GetDateTime("Created"),
//                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
//                    Suffix = reader.GetStringNullable("Suffix")
//                }).AsDictionary();
//            }
//            return list;
//        }

        public List<User> GetUsersByIds(Guid agencyId, List<Guid> userIds)
        {
            var list = new List<User>();
            if (userIds != null && userIds.Count > 0)
            {
                var script = string.Format(@"SELECT 
                                Id,
                                FirstName,
                                LastName, 
                                MiddleName 
                                    FROM 
                                        users
                                            WHERE 
                                                users.AgencyId = @agencyid AND 
                                                users.IsDeprecated = 0 AND
                                                users.Id IN ({0})", userIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<User>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new User
                     {
                         Id = reader.GetGuid("Id"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         MiddleName = reader.GetStringNullable("MiddleName"),
                     }).AsList();
                }
            }
            return list;
        }

        public List<User> GetUsersWithCredentialsByIds(Guid agencyId, List<Guid> userIds)
        {
            var list = new List<User>();
            if (userIds.IsNotNullOrEmpty())
            {
                var script = string.Format(@"SELECT 
                                            Id,
                                            LoginId,
                                            FirstName,
                                            LastName,
                                            MiddleName ,
                                            Suffix,
                                            Credentials,
                                            CredentialsOther,
                                            IsDeprecated
                                                FROM 
                                                    users 
                                                        WHERE
                                                            users.AgencyId = @agencyid AND
                                                            users.Id IN ({0})", userIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<User>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new User
                     {
                         Id = reader.GetGuid("Id"),
                         LoginId = reader.GetGuid("LoginId"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         MiddleName = reader.GetStringNullable("MiddleName"),
                         Suffix = reader.GetStringNullable("Suffix"),
                         Credentials = reader.GetStringNullable("Credentials"),
                         CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                         IsDeprecated = reader.GetBoolean("IsDeprecated")
                     }).AsList();
                }
            }
            return list;
        }

		public UserCache GetUserCacheById(Guid agencyId, Guid userId)
        {
            UserCache user = null;
            try
            {
                var script = @"SELECT 
                                Id,
                                FirstName,
                                LastName,
                                MiddleName ,
                                Suffix,
                                Credentials,
                                CredentialsOther,
                                IsDeprecated,
                                ProfileData
                                    FROM 
                                        users 
                                            WHERE
                                                users.AgencyId = @agencyid AND
                                                users.Id = @userid limit 1";
                using (var cmd = new FluentCommand<UserCache>(script))
                {
                    user = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("userid", userId)
                     .SetMap(reader => new UserCache
                     {
                         Id = reader.GetGuid("Id"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         MiddleName = reader.GetStringNullable("MiddleName"),
                         Suffix = reader.GetStringNullable("Suffix"),
                         Credentials = reader.GetStringNullable("Credentials"),
                         CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                         IsDeprecated = reader.GetBoolean("IsDeprecated"),
                         ProfileData = reader.GetStringNullable("ProfileData")
                     }).AsSingle();
                }
            }
            catch (Exception)
            {
                return null;
            }
            return user;
        }

        public List<UserCache> GetUserCachesWithCredentialsByIds(Guid agencyId, List<Guid> userIds)
        {
            var list = new List<UserCache>();
            if (userIds.IsNotNullOrEmpty())
            {
                var script = string.Format(@"SELECT 
                                            Id,
                                            FirstName,
                                            LastName,
                                            MiddleName ,
                                            Suffix,
                                            Credentials,
                                            CredentialsOther,
                                            IsDeprecated,
                                            ProfileData
                                                FROM 
                                                    users 
                                                        WHERE
                                                            users.AgencyId = @agencyid AND
                                                            users.Id IN ({0})", userIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<UserCache>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new UserCache
                     {
                         Id = reader.GetGuid("Id"),
                         FirstName = reader.GetStringNullable("FirstName"),
                         LastName = reader.GetStringNullable("LastName"),
                         MiddleName = reader.GetStringNullable("MiddleName"),
                         Suffix = reader.GetStringNullable("Suffix"),
                         Credentials = reader.GetStringNullable("Credentials"),
                         CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                         IsDeprecated = reader.GetBoolean("IsDeprecated"),
                         ProfileData = reader.GetStringNullable("ProfileData")
                     }).AsList();
                }
            }
            return list;
        }
		
        public List<User> GetUsersNameOnly(Guid agencyId, int status)
        {
            var list = new List<User>();
            var script = string.Format(@"SELECT 
                                    Id, 
                                    FirstName,
                                    LastName,
                                    MiddleName 
                                        FROM 
                                            users 
                                                WHERE 
                                                    users.AgencyId = @agencyid AND
                                                    users.Status = @status AND 
                                                    users.IsDeprecated = 0;");
            using (var cmd = new FluentCommand<User>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddInt("status", status)
                 .SetMap(reader => new User
                 {
                     Id = reader.GetGuid("Id"),
                     FirstName = reader.GetStringNullable("FirstName"),
                     LastName = reader.GetStringNullable("LastName"),
                     MiddleName = reader.GetStringNullable("MiddleName"),
                 }).AsList();
            }
            return list;
        }

        //public List<User> GetUsersNameWithCredentials(Guid agencyId)
        //{
        //    var list = new List<User>();
        //    var script = string.Format(@"SELECT Id, FirstName, LastName, MiddleName, Suffix, Credentials, IsDeprecated, CredentialsOther " +
        //                                "FROM users " +
        //                                "WHERE users.AgencyId = @agencyid AND users.IsDeprecated = 0;");
        //    using (var cmd = new FluentCommand<User>(script))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //         .AddGuid("agencyid", agencyId)
        //         .SetMap(reader => new User
        //         {
        //             Id = reader.GetGuid("Id"),
        //             FirstName = reader.GetStringNullable("FirstName"),
        //             LastName = reader.GetStringNullable("LastName"),
        //             MiddleName = reader.GetStringNullable("MiddleName"),
        //             Suffix = reader.GetStringNullable("Suffix"),
        //             Credentials = reader.GetStringNullable("Credentials"),
        //             CredentialsOther = reader.GetStringNullable("CredentialsOther"),
        //             IsDeprecated = reader.GetBoolean("IsDeprecated")
        //         }).AsList();
        //    }
        //    return list;
        //}

        public List<User> GetUsersCustomId(Guid agencyId, List<Guid> userIds)
        {
            var list = new List<User>();
            string ids = userIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
            var script = string.Format(@"SELECT Id, CustomId FROM users" +
                                    " WHERE AgencyId=@agencyid AND Id in ({0});", ids);
            using (var cmd = new FluentCommand<User>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .SetMap(reader => new User
                 {
                     Id = reader.GetGuid("Id"),
                     CustomId=reader.GetStringNullable("CustomId")
                 }).AsList();
            }
            return list;
        }

        public bool BulkDelete(List<Guid> userIds, bool flag)
        {
            var result = false;
            if (userIds.IsNotNullOrEmpty())
            {
                var script = string.Format(@"UPDATE users SET IsDeprecated = {0}, Modified = curdate() WHERE Id in ({1});", flag ? 1 : 0, userIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<User>(script))
                {
                    if (cmd.SetConnection("AgencyManagementConnectionString").AsNonQuery() > 0)
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool BulkDeactivate(List<Guid> userIds, bool flag)
        {
            var result = false;
            if (userIds.IsNotNullOrEmpty())
            {
                var script = string.Format(@"UPDATE users SET `Status` = {0}, Modified = curdate() WHERE Id in ({1});", flag ? 2 : 1, userIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<User>(script))
                {
                    if (cmd.SetConnection("AgencyManagementConnectionString").AsNonQuery() > 0)
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public List<Guid> GetUserBranches(Guid agencyId, Guid userId)
        {
            return database.Find<UserLocation>(l => l.AgencyId == agencyId && l.UserId == userId).Select(l => l.AgencyLocationId).ToList();
        }

        public IDictionary<Guid, Location> GetUserLocationIds(Guid agencyId, Guid userId)
        {
            IDictionary<Guid, Location> list = new Dictionary<Guid, Location>();
            var script = @"SELECT 
                              al.Id,
                              al.IsMainOffice,
                              al.IsLocationStandAlone,
                              al.Payor
                                  FROM 
                                     agencylocations al 
                                        INNER JOIN userlocations ul ON al.Id = ul.AgencyLocationId
                                            WHERE
                                                al.AgencyId = @agencyid AND
                                                ul.UserId = @userid AND
                                                al.IsDeprecated = 0";

            var tempList = new Dictionary<string, Location>();
            using (var cmd = new FluentCommand<Location>(script))
            {
                tempList = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("userid", userId)
                  .SetDictonaryId("Id")
                 .SetMap(reader =>
                new Location
                {
                    IsMainOffice = reader.GetBoolean("IsMainOffice"),
                    IsStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                    Payor = reader.GetStringNullable("Payor")
                }

             ).AsDictionary();
            }
            if (tempList.IsNotNullOrEmpty())
            {
                list = tempList.Where(g => g.Key.IsGuid()).ToDictionary(k => k.Key.ToGuid(), v => v.Value);
            }
            return list;
        }

        public List<AgencyLocation> GetUserAgencyLocationsName(Guid agencyId, Guid userId)
        {
            var list = new List<AgencyLocation>();
            var script = string.Format(@"SELECT 
                                        agencylocations.Id, 
                                        agencylocations.Name 
                                            FROM 
                                                agencylocations 
                                                    INNER JOIN userlocations ON userlocations.AgencyLocationId = agencylocations.Id 
                                                        WHERE 
                                                            userlocations.AgencyId = @agencyid AND 
                                                            userlocations.UserId = @userid;");
            using (var cmd = new FluentCommand<AgencyLocation>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("userid", userId)
                 .SetMap(reader => new AgencyLocation
                 {
                     Id = reader.GetGuid("Id"),
                     Name = reader.GetStringNullable("Name"),
                 }).AsList();
            }
            return list;
        }

        public int GetUserCountPerLocation(Guid agencyId, Guid locationId)
        {
            int count = 0;
            var script = @"SELECT count(id) FROM patients WHERE AgencyId = @agencyid AND AgencyLocationId = @locationId;";
            using (var cmd = new FluentCommand<PatientEpisode>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("locationId", locationId)
                .AsScalar();
            }
            return count;
        }

        public IList<User> GetUserLeanForHtml(Guid agencyId, List<Guid> branchIds, List<int> statusIds)
        {
            var list = new List<User>();
            try
            {
                if (branchIds.IsNotNullOrEmpty() && statusIds.IsNotNullOrEmpty())
                {
                    var script = string.Format(@"SELECT 
                                users.Id as UserId,
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials,
                                users.CredentialsOther as CredentialsOther,
                                users.IsDeprecated as IsUserDeprecated
                                    FROM 
                                        users 
                                                WHERE 
                                                    users.AgencyId = @agencyid AND
                                                    users.AgencyLocationId IN ({0}) AND 
                                                    users.IsDeprecated = 0 AND 
                                                    users.Status IN ({1}) ", branchIds.ToCommaSeperatedList(), statusIds.ToCommaSeperatedList());
                    using (var cmd = new FluentCommand<User>(script))
                    {
                        list = cmd.SetConnection("AgencyManagementConnectionString")
                            .AddGuid("agencyid", agencyId)
                            .SetMap(
                            reader =>
                                 new User
                                {
                                    Id = reader.GetGuid("UserId"),
                                    FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                                    LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                                    Suffix = reader.GetStringNullable("Suffix"),
                                    Credentials = reader.GetStringNullable("Credentials"),
                                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                                    IsDeprecated = reader.GetBoolean("IsUserDeprecated")

                                }).AsList();
                    }
                    if (list.IsNotNullOrEmpty())
                    {
                        list=list.OrderBy(u=>u.FirstName).ThenBy(u=>u.LastName).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return list;
        }

        public IList<User> GetUsersByStatusAndDOB(Guid agencyId, Guid branchId, int status, int month)
        {
            var users = new List<User>();
            var statusScript = string.Format(" AND ( users.Status = {0} OR users.Status = {1} ) ", (int)UserStatus.Active, (int)UserStatus.Inactive);
            if (status == (int)UserStatus.Active || status == (int)UserStatus.Inactive)
            {
                statusScript = string.Format(" AND users.Status = {0} ", status);
            }
            var script = string.Format(@"SELECT 
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther ,
                                users.DOB as DOB ,
                                users.ProfileData as ProfileData
                                    FROM 
                                        users  
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.IsDeprecated = 0 {0} {1} AND 
                                                MONTH(users.DOB) = @month 
                                                    ", statusScript, !branchId.IsEmpty() ? " AND users.AgencyLocationId = @branchId " : string.Empty);
            using (var cmd = new FluentCommand<User>(script))
            {
                users =
                    cmd.SetConnection("AgencyManagementConnectionString")
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("branchId", branchId)
                       .AddInt("status", status)
                       .AddInt("month", month)
                       .SetMap(
                           reader =>
                           new User
                           {
                               FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                               LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                               Suffix = reader.GetStringNullable("Suffix").ToUpperCase(),
                               Credentials = reader.GetStringNullable("Credentials"),
                               CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                               DOB = reader.GetDateTime("DOB"),
                               ProfileData= reader.GetStringNullable("ProfileData")
                              
                           })
                       .AsList();
                if (users.IsNotNullOrEmpty())
                {
                    users = users.OrderBy(u => u.FirstName).ThenBy(u => u.LastName).ToList();
                }
            }
            return users;
        }

        public string GetUserLicensesOnly(Guid agencyId, Guid userId)
        {
            return GetUserXmlDataHelper(agencyId, userId, "Licenses");
        }

        public string GetUserRatesOnly(Guid agencyId, Guid userId)
        {
            return GetUserXmlDataHelper(agencyId, userId, "Rates");
        }

        public User GetUserLicensesWithDisplayName(Guid agencyId, Guid userId)
        {
            var user = new User();
            if (!userId.IsEmpty())
            {
                var script = string.Format(@"SELECT 
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther ,
                                users.CustomId as CustomId,
                                users.Licenses as Licenses
                                    FROM 
                                        users  
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.Id  = @userid limit 0,1");
                using (var cmd = new FluentCommand<User>(script))
                {
                    user =
                        cmd.SetConnection("AgencyManagementConnectionString")
                           .AddGuid("agencyid", agencyId)
                          .AddGuid("userid", userId)
                           .SetMap(
                               reader =>
                               new User
                               {
                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                                   Suffix = reader.GetStringNullable("Suffix").ToUpperCase(),
                                   Credentials = reader.GetStringNullable("Credentials"),
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                                   CustomId = reader.GetStringNullable("CustomId"),
                                   Licenses = reader.GetStringNullable("Licenses")

                               })
                           .AsSingle();

                }
            }
            return user;
        }


        public User GetUserProfileDisplayName(Guid agencyId, Guid userId)
        {
            var user = new User();
            if (!userId.IsEmpty())
            {
                var script = string.Format(@"SELECT 
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther ,
                                users.CustomId as CustomId,
                                users.ProfileData as ProfileData
                                    FROM 
                                        users  
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.Id  = @userid limit 0,1");
                using (var cmd = new FluentCommand<User>(script))
                {
                    user =
                        cmd.SetConnection("AgencyManagementConnectionString")
                           .AddGuid("agencyid", agencyId)
                          .AddGuid("userid", userId)
                           .SetMap(
                               reader =>
                               new User
                               {
                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                                   Suffix = reader.GetStringNullable("Suffix").ToUpperCase(),
                                   Credentials = reader.GetStringNullable("Credentials"),
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                                   CustomId = reader.GetStringNullable("CustomId"),
                                   ProfileData = reader.GetStringNullable("ProfileData")

                               })
                           .AsSingle();

                }
            }
            return user;
        }


        private string GetUserXmlDataHelper(Guid agencyId, Guid userId, string columnName)
        {
            var dataString = string.Empty;
            if (!userId.IsEmpty())
            {
                var script = string.Format(@"SELECT 
                                {0} as Data
                                    FROM 
                                        users  
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.Id = @userid limit 0,1", columnName);
                object tempData = null;
                using (var cmd = new FluentCommand<object>(script))
                {
                    tempData =
                        cmd.SetConnection("AgencyManagementConnectionString")
                           .AddGuid("agencyid", agencyId)
                           .AddGuid("userid", userId)
                           .SetMap(reader => reader.GetStringNullable("Data"))
                           .AsSingle();
                    if (tempData != null)
                    {
                        dataString = tempData.ToString();
                    }
                }
            }
            return dataString;
        }


        public IList<User> GetUsersByStatusForLicenses(Guid agencyId, List<Guid> branchIds, List<int> statusIds)
        {
            var users = new List<User>();
            if (branchIds.IsNotNullOrEmpty() && statusIds.IsNotNullOrEmpty())
            {
                var script = string.Format(@"SELECT 
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther ,
                                users.CustomId as CustomId,
                                users.Licenses as Licenses
                                    FROM 
                                        users  
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.AgencyLocationId IN({0}) AND 
                                                users.Status IN({1}) AND
                                                users.IsDeprecated = 0", branchIds.ToCommaSeperatedList(), statusIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<User>(script))
                {
                    users =
                        cmd.SetConnection("AgencyManagementConnectionString")
                           .AddGuid("agencyid", agencyId)
                           .SetMap(
                               reader =>
                               new User
                               {
                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                                   Suffix = reader.GetStringNullable("Suffix").ToUpperCase(),
                                   Credentials = reader.GetStringNullable("Credentials"),
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                                   CustomId = reader.GetStringNullable("CustomId"),
                                   Licenses = reader.GetStringNullable("Licenses")

                               })
                           .AsList();
                    if (users.IsNotNullOrEmpty())
                    {
                        users = users.OrderBy(u => u.FirstName).ThenBy(u => u.LastName).ToList();
                    }
                }
            }
            return users;
        }

        public IList<User> GetUsersByStatusForPermissions(Guid agencyId, List<Guid> branchIds, List<int> statusIds)
        {
            var users = new List<User>();
            if (branchIds.IsNotNullOrEmpty() && statusIds.IsNotNullOrEmpty())
            {
                var script = string.Format(@"SELECT 
                                users.FirstName as FirstName ,
                                users.LastName as LastName ,
                                users.Suffix as Suffix,
                                users.Credentials as Credentials ,
                                users.CredentialsOther as CredentialsOther ,
                                users.CustomId as CustomId,
                                users.Permissions as Permissions
                                    FROM 
                                        users  
                                            WHERE 
                                                users.AgencyId = @agencyid AND 
                                                users.AgencyLocationId IN({0}) AND 
                                                users.Status IN({1}) AND
                                                users.IsDeprecated = 0", branchIds.ToCommaSeperatedList(), statusIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<User>(script))
                {
                    users =
                        cmd.SetConnection("AgencyManagementConnectionString")
                           .AddGuid("agencyid", agencyId)
                           .SetMap(
                               reader =>
                               new User
                               {
                                   FirstName = reader.GetStringNullable("FirstName").ToUpperCase(),
                                   LastName = reader.GetStringNullable("LastName").ToUpperCase(),
                                   Suffix = reader.GetStringNullable("Suffix").ToUpperCase(),
                                   Credentials = reader.GetStringNullable("Credentials"),
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                                   CustomId = reader.GetStringNullable("CustomId"),
                                   Permissions = reader.GetStringNullable("Permissions")

                               })
                           .AsList();
                    if (users.IsNotNullOrEmpty())
                    {
                        users = users.OrderBy(u => u.FirstName).ThenBy(u => u.LastName).ToList();
                    }
                }
            }
            return users;
        }



        #endregion
    }
}
