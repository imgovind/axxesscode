﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Enums;


    public class ScheduleRepository : IScheduleRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private readonly string connectionStringName;

        public ScheduleRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            this.connectionStringName = "AgencyManagementConnectionString";
        }

        #endregion

        public bool AddScheduleTask(ScheduleEvent scheduleEvent)
        {
            bool result = false;
            if (scheduleEvent != null)
            {
                try
                {
                    database.Add<ScheduleEvent>(scheduleEvent);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool AddMultipleScheduleTask(List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                try
                {
                    database.AddMany<ScheduleEvent>(scheduleEvents);
                    result = true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public ScheduleEvent GetScheduleTask(Guid agencyId, Guid patientId, Guid eventId)
        {
            return database.Single<ScheduleEvent>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Id == eventId);
        }

        public bool UpdateScheduleTaskModal(ScheduleEvent scheduleEvent)
        {
            bool result = false;
            if (scheduleEvent != null)
            {
                result = database.Update<ScheduleEvent>(scheduleEvent) > 0;
            }
            return result;
        }

        public bool UpdateScheduleTasksStatus(Guid agencyId, Guid patientId, Guid eventId, int status)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"UPDATE scheduletasks st set st.Status = @status WHERE st.AgencyId = @agencyid AND st.PatientId = @patientid AND st.Id = @eventid");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("eventid", eventId)
                    .AddInt("status", status)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateScheduleTasksPrintQueue(Guid agencyId, Guid patientId, Guid eventId, bool inPrintQueue)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"UPDATE scheduletasks st set st.InPrintQueue = @isprintqueue WHERE st.AgencyId = @agencyid AND st.PatientId = @patientid AND st.Id = @eventid");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("eventid", eventId)
                    .AddInt("isprintqueue", inPrintQueue ? 1 : 0)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool UpdateScheduleTasksPrintQueueAndStatus(Guid agencyId, Guid patientId, Guid eventId, int status, bool inPrintQueue)
        {
            bool result = false;
            try
            {
                var script = string.Format(@"UPDATE scheduletasks st set st.InPrintQueue = @isprintqueue, st.Status=@status WHERE st.AgencyId = @agencyid AND st.PatientId = @patientid AND st.Id = @eventid");
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientid", patientId)
                    .AddGuid("eventid", eventId)
                    .AddInt("isprintqueue", inPrintQueue ? 1 : 0)
                    .AddInt("status", status)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }

        public bool RemoveScheduleTaskFully(Guid agencyId, Guid patientId, Guid eventId)
        {
            var script = "DELETE FROM scheduletasks WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id = @id";
            using (var cmd = new FluentCommand<int>(script))
            {
                return cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("id", eventId).AsNonQuery() > 0;
            }
        }

        public bool RemoveMultipleScheduleTaskFully(Guid agencyId, Guid patientId, List<Guid> eventIds)
        {
            var script = "DELETE FROM scheduletasks WHERE AgencyId = @agencyid AND PatientId = @patientid AND Id IN ( @ids ) ";
            using (var cmd = new FluentCommand<int>(script))
            {
                return cmd.SetConnection(this.connectionStringName)
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientid", patientId)
               .AddString("ids", eventIds.ToCommaSeperatedNoQuote())
               .AsNonQuery() > 0;
            }
        }

        public List<ScheduleEvent> GetPatientScheduledEventsOnly(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.EventDate,
                        st.VisitDate
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue,
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate ,
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.PatientId = @patientid AND 
                                        st.EpisodeId = @episodeid AND
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0 ");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap((reader) =>
                {
                    var task = new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = patientId,
                        EpisodeId = episodeId,
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        Discipline = reader.GetInt("Discipline"),
                        IsBillable = reader.GetBoolean("IsBillable"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                        InPrintQueue = reader.GetBoolean("InPrintQueue"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                        StartDate = reader.GetDateTime("StartDate"),
                        EndDate = reader.GetDateTime("EndDate")

                    };
                    return task;
                }
                )
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsOnly(Guid agencyId,Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.UserId as UserId ,
                        st.EpisodeId as EpisodeId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.EventDate,
                        st.VisitDate
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue,
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate ,
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.PatientId = @patientid AND 
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap((reader) =>
                {
                    var task = new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = patientId,
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        Discipline = reader.GetInt("Discipline"),
                        IsBillable = reader.GetBoolean("IsBillable"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                        InPrintQueue = reader.GetBoolean("InPrintQueue"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                        StartDate = reader.GetDateTime("StartDate"),
                        EndDate = reader.GetDateTime("EndDate")

                    };
                    return task;
                }
                )
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsOnly(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.UserId as UserId ,
                        st.EpisodeId as EpisodeId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.EventDate,
                        st.VisitDate,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue, st.StartDate as StartDate ,
                        st.EndDate as EndDate ,
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.PatientId = @patientid AND 
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0 AND
                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap((reader) =>
                {
                    var task = new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = patientId,
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        Discipline = reader.GetInt("Discipline"),
                        IsBillable = reader.GetBoolean("IsBillable"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                        InPrintQueue = reader.GetBoolean("InPrintQueue"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                        StartDate = reader.GetDateTime("StartDate"),
                        EndDate = reader.GetDateTime("EndDate")

                    };
                    return task;
                }
                )
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate)
        {
            var schedules = database.Find<ScheduleEvent>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.EpisodeId == episodeId && e.IsDeprecated == false && e.DisciplineTask > 0).ToList();
            if (schedules.IsNotNullOrEmpty())
            {
                schedules = schedules.Where(e => e.VisitDate.Date >= startDate.Date && e.VisitDate.Date <= endDate.Date).OrderBy(s => s.VisitDate.Date).ToList();
            }
            return schedules;
        }

        public bool UpdateScheduleEventsForIsBillable(Guid agencyId, Guid patientId, Guid episodeId, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduletasks st set  st.IsBillable = 1 WHERE  st.AgencyId = @agencyid AND  st.PatientId = @patientid AND  st.EpisodeId = @episodeid AND  st.Id IN ( @ids)");
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddGuid("episodeid", episodeId)
                        .AddString("ids", scheduleEvents.Select(s => s.Id).ToCommaSeperatedNoQuote())
                        .AsNonQuery();
                    }
                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateScheduleEventsForIsBillable(Guid agencyId, Guid patientId, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                try
                {
                    var script = string.Empty;
                    var billableTasks = scheduleEvents.Where(t => t.IsBillable);
                    var nonbillableTasks = scheduleEvents.Where(t => !t.IsBillable);
                    if (billableTasks.IsNotNullOrEmpty())
                    {
                        script = string.Format(@"UPDATE scheduletasks st set st.IsBillable = 1 WHERE st.AgencyId = @agencyid AND st.PatientId = @patientid AND st.Id IN ( {0}); ", billableTasks.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                    }
                    if (nonbillableTasks.IsNotNullOrEmpty())
                    {
                        script += string.Format(@"UPDATE scheduletasks st set st.IsBillable = 0 WHERE st.AgencyId = @agencyid AND st.PatientId = @patientid AND st.Id IN ( {0}); ", nonbillableTasks.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", "));
                    }
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(this.connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AsNonQuery();
                    }
                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public List<ScheduleEvent> GetCurrentAndPreviousOrders(Guid agencyId, PatientEpisode episode, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate  
                            FROM
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.PatientId = pe.PatientId
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND
                                            st.IsMissedVisit = 0 AND
                                            st.IsDeprecated = 0 AND 
                                           (( pe.Id = @episodeid AND st.IsOrderForNextEpisode = 0 ) OR (pe.Id != @episodeid AND pe.IsActive = 1 AND pe.IsDischarged = 0 AND   DATE(pe.EndDate) = DATE(@previousenddate) AND st.IsOrderForNextEpisode = 1 )) AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            (st.SendAsOrder = 1 OR st.DisciplineTask IN ( @disciplinetasks ))" );

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", episode.PatientId)
                .AddGuid("episodeid", episode.Id)
                .AddDateTime("previousenddate", episode.StartDate.AddDays(-1))
                .AddString("disciplinetasks",disciplineTasks.ToCommaSeperatedNoQuote())
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;

        }

//        public List<ScheduleEvent> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks)
//        {
//            var script = string.Format(@"SELECT 
//                        st.Id as Id ,
//                        st.PatientId as PatientId ,
//                        st.EpisodeId as EpisodeId ,
//                        st.UserId as UserId ,
//                        st.DisciplineTask as DisciplineTask , 
//                        st.EventDate as EventDate ,
//                        st.VisitDate as VisitDate ,
//                        st.Status as Status ,
//                        st.Discipline as Discipline ,
//                        st.IsOrderForNextEpisode as IsOrderForNextEpisode 
//                            FROM 
//                                scheduletasks st
//                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
//                                        WHERE
//                                            st.AgencyId = @agencyid AND
//                                            st.PatientId = @patientid  AND
//                                            st.IsDeprecated = 0  AND 
//                                            pe.IsActive = 1  AND
//                                            pe.IsDischarged = 0  AND 
//                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
//                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
//                                            (st.SendAsOrder = 1 OR st.DisciplineTask IN ( @disciplinetask ))");

//            var scheduleList = new List<ScheduleEvent>();
//            using (var cmd = new FluentCommand<ScheduleEvent>(script))
//            {
//                scheduleList = cmd.SetConnection(connectionStringName)
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("patientId", patientId)
//                .AddDateTime("startdate", startDate)
//                .AddDateTime("enddate", endDate)
//                .AddString("disciplinetask",disciplineTasks.ToCommaSeperatedNoQuote())
//                .SetMap(reader => new ScheduleEvent
//                {
//                    EventId = reader.GetGuid("Id"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    EpisodeId = reader.GetGuid("EpisodeId"),
//                    UserId = reader.GetGuid("UserId"),
//                    DisciplineTask = reader.GetInt("DisciplineTask"),
//                    EventDate = reader.GetStringNullable("EventDate"),
//                    VisitDate = reader.GetStringNullable("VisitDate"),
//                    Status = reader.GetStringNullable("Status"),
//                    Discipline = reader.GetStringNullable("Discipline"),
//                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
//                })
//                .AsList();
//            }
//            return scheduleList;
//        }

        public List<ScheduleEvent> GetScheduleEventsForVitalSign(Guid agencyId, Guid patientId,Guid optionalEpisodeId, List<int> disciplineTasks,bool IsDateRangeApplicable, DateRange dateRange, bool isLimitApplicable, int limit, DateTime maxEventDateIfLimit)
        {
            var additionalFilter = string.Empty;
            var orderByAndLimit = string.Empty;
            if (IsDateRangeApplicable && dateRange!=null)
            {
                additionalFilter = " AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            else if (isLimitApplicable)
            {
                additionalFilter = " AND DATE(st.EventDate)<= @maxeventdate ";
                orderByAndLimit = string.Format(" ORDER BY DESC limit {0}", limit);
            }
            
            var episodeScript = string.Empty;
            if (optionalEpisodeId.IsEmpty())
            {
                episodeScript = " st.EpisodeId = @episodeid  AND ";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status 
                            FROM 
                                scheduletasks st
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid {2}   AND
                                            st.IsDeprecated = 0  AND 
                                            st.IsActive = 1  AND
                                            st.IsDischarged = 0  AND 
                                            st.IsMissedVisit =0 AND
                                            DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) {0} AND
                                            st.DisciplineTask IN ( @disciplinetask ) {1}", additionalFilter, orderByAndLimit, episodeScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddGuid("episodeid", optionalEpisodeId)
                .AddDateTime("startdate", dateRange.StartDate)
                .AddDateTime("enddate", dateRange.EndDate)
                .AddDateTime("maxeventdate", maxEventDateIfLimit)
                .AddString("disciplinetask", disciplineTasks.ToCommaSeperatedNoQuote())
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status")
                })
                .AsList();
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetScheduleByBranchDateRangeAndStatus(Guid agencyId, List<Guid> branchIds, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var scheduleList = new List<ScheduleEvent>();
            if (branchIds.IsNotNullOrEmpty())
            {
                var status = string.Empty;
                if (patientStatus <= 0)
                {
                    status = " AND  pr.Status IN (1,2)";
                }
                else
                {
                    status = " AND pr.Status = @statusid";
                }
                var branch = string.Empty;

                var missedVisit = string.Empty;
                if (!IsMissedVisitIncluded)
                {
                    missedVisit = " AND st.IsMissedVisit = 0 ";
                }
                var schedulleStatusScript = string.Empty;
                if (scheduleStatus != null && scheduleStatus.Length > 0)
                {
                    schedulleStatusScript = string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
                }

                var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.ReturnReason as ReturnReason ,
                        st.Comments as Comments ,
                        pe.Details as EpisodeNotes ,
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName 
                            FROM 
                              patients pr 
                                    INNER JOIN patientepisodes pe ON  pr.Id = pe.PatientId 
                                    INNER JOIN  scheduletasks st ON st.EpisodeId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid  AND
                                            pr.AgencyLocationId IN (@branchIds) AND
                                            st.IsDeprecated = 0  {3} AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0  AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) {0}  {1}",
                                               schedulleStatusScript,
                                               branch,
                                               status,
                                               missedVisit);


                using (var cmd = new FluentCommand<ScheduleEvent>(script))
                {
                    scheduleList = cmd.SetConnection(connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddString("branchIds", branchIds.ToCommaSeperatedNoQuote())
                    .AddInt("statusid", patientStatus)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                        Status = reader.GetInt("Status"),
                        Discipline = reader.GetInt("Discipline"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        ReturnReason = reader.GetStringNullable("ReturnReason"),
                        Comments = reader.GetStringNullable("Comments"),
                        EpisodeNotes = reader.GetStringNullable("EpisodeNotes"),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                    })
                    .AsList();
                }
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetPrintQueueTasks(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var additonalScript = string.Empty;

            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.TimeIn as TimeIn ,
                        st.TimeOut as TimeOut ,
                        st.EventDate as EventDate , 
                        st.VisitDate as VisitDate , 
                        st.Status as Status ,
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName
                                FROM 
                                    scheduletasks st
                                        INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id
                                        INNER JOIN patients pr ON st.PatientId = pr.Id  
                                                WHERE
                                                        st.AgencyId = @agencyid {0} AND 
                                                        st.IsMissedVisit = 0 AND
                                                        st.IsDeprecated = 0 AND
                                                        st.InPrintQueue = 1 AND
                                                        pr.IsDeprecated = 0  AND 
                                                        pr.Status IN (1,2) AND 
                                                        st.IsDeprecated = 0  AND 
                                                        pe.IsActive = 1 AND 
                                                        pe.IsDischarged = 0  AND 
                                                        DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) 
                                                        ORDER BY st.EventDate DESC ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>

                  new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate")
                    }
                )
                .AsList();
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsOnlyLeanNew(Guid agencyId, Guid episodeId, Guid patientId, int[] scheduleStatus, string discipline, int[] disciplineTasks, bool isReportAndNotesIncluded)
        {
            var additionalScript = string.Empty;

            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (discipline.IsEqual("all"))
            {
                additionalScript += string.Empty;
            }
            else if (discipline.IsEqual("Therapy"))
            {
                additionalScript += " AND (STRCMP(st.Discipline ,'PT') = 0 OR STRCMP(st.Discipline, 'OT') = 0 OR STRCMP(st.Discipline , 'ST') = 0) ";
            }
            else if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
            {
                additionalScript += " AND (STRCMP(st.Discipline ,'Nursing') = 0 OR STRCMP(st.Discipline, 'ReportsAndNotes') = 0) ";
            }
            else
            {
                additionalScript += " AND st.Discipline = @discipline ";
            }
            //if (discipline.IsNotNullOrEmpty() && discipline.IsEqual("all"))
            //{
            //    additionalScript += string.Format(" AND Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            //}
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate  
                            FROM 
                                scheduletasks  st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.PatientId = @patientid AND 
                                        st.EpisodeId = @episodeid {0} AND
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0 
                                            ORDER BY DATE(st.EventDate) DESC ", additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddString("discipline", discipline)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = episodeId,
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    Discipline = reader.GetInt("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsOnlyNew(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.PatientId = @patientid AND 
                                        st.EpisodeId = @episodeid AND
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0 
                                            ORDER BY DATE(st.EventDate) DESC ");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap((reader) =>
                 new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = patientId,
                        EpisodeId = episodeId,
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                        Discipline = reader.GetInt("Discipline"),
                        IsBillable = reader.GetBoolean("IsBillable"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                        InPrintQueue = reader.GetBoolean("InPrintQueue")
                    }
                )
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid patientId, Guid[] episodeIds, DateTime startDate, DateTime endDate, List<int> disciplines, bool isReportAndNotesIncluded, bool IsDateRange)
        {
            var episodeScript = string.Empty;
            if (episodeIds != null && episodeIds.Length > 0)
            {
                if (episodeIds.Length == 1)
                {
                    episodeScript += string.Format(" AND st.EpisodeId = '{0}' ", episodeIds.FirstOrDefault());
                }
                else
                {
                    episodeScript += string.Format(" AND st.EpisodeId IN ( {0} )", episodeIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));
                }
            }
            var dateRange = IsDateRange ? " AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
            var disciplineScript = string.Empty;
            if (disciplines.IsNotNullOrEmpty())
            {
                disciplineScript = " AND st.Discipline IN (@discipline) ";
            }
            //if (discipline.IsEqual("all"))
            //{
            //    disciplineScript = string.Empty;
            //}
            //else if (discipline.IsEqual("Therapy"))
            //{
            //    disciplineScript = " AND ( STRCMP(st.Discipline ,'PT') = 0 OR STRCMP(st.Discipline, 'OT') = 0 OR STRCMP(st.Discipline , 'ST') = 0 )";
            //}
            //else if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
            //{
            //    disciplineScript = " AND ( STRCMP(st.Discipline ,'Nursing') = 0 OR STRCMP(st.Discipline, 'ReportsAndNotes') = 0 ) ";
            //}
            //else if (discipline.IsEqual("Dietation"))
            //{
            //    disciplineScript = " AND ( STRCMP(st.Discipline ,'Dietician') = 0 ";
            //}
            //else
            //{
            //    disciplineScript = " AND st.Discipline = @discipline ";
            //}
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.ReturnReason as ReturnReason ,
                        st.Comments as Comments ,
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate ,
                        st.Asset as Asset ,
                        st.Version as Version 
                            FROM 
                                scheduletasks st
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid {2} AND 
                                            st.IsDeprecated = 0 AND
                                            st.DisciplineTask > 0 {0} {1} 
                                            ORDER BY DATE(st.EventDate) DESC ", disciplineScript, dateRange, episodeScript);
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddString("discipline", disciplines.ToCommaSeperatedNoQuote())
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTimeWithMin("EventDate"),
                    VisitDate = reader.GetDateTimeWithMin("VisitDate"),
                    Status = reader.GetInt("Status"),
                    Discipline = reader.GetInt("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTimeWithMin("StartDate"),
                    EndDate = reader.GetDateTimeWithMin("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetRAPClaimSchedules(Guid agencyId, List<Guid> ids)
        {
            var completedStatus = ScheduleStatusFactory.OASISAndNurseNotesAfterQA().ToArray();// new int[] { (int)ScheduleStatus.NoteCompleted, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.EvalToBeSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically, (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalReturnedWPhysicianSignature };
            var socDisciplineTasks = DisciplineTaskFactory.SOCDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCStartofCare, (int)DisciplineTasks.OASISCStartofCarePT, (int)DisciplineTasks.OASISCStartofCareOT };
            var script1 = string.Format(@"SELECT 
                                        st.Id as Id ,
                                        st.PatientId as PatientId ,
                                        st.EpisodeId as EpisodeId ,
                                        st.EventDate as EventDate ,
                                        st.VisitDate as VisitDate ,
                                        st.Status as Status ,
                                        st.DisciplineTask as DisciplineTask , 
                                        st.Discipline as Discipline ,
                                        st.IsBillable as IsBillable ,
                                        st.IsMissedVisit as IsMissedVisit , 
                                        st.TimeIn as TimeIn , 
                                        st.TimeOut as TimeOut ,
                                        pe.StartDate as StartDate ,
                                        pe.EndDate as EndDate ,
                                        pe.Id as NextEpisodeId ,
                                       'FirstBillableVisitOrSOC' as VisitType 
                                            FROM 
                                                 scheduletasks st
                                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id  
                                                        WHERE 
                                                            pe.AgencyId = @agencyid AND 
                                                            pe.Id IN ( {0} ) AND 
                                                            DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND 
                                                            st.IsMissedVisit = 0  AND
                                                            st.IsDeprecated = 0 AND
                                                            st.DisciplineTask > 0  AND
                                                            (( st.IsBillable = 1 AND  st.Status IN ( {1} ) )  OR st.DisciplineTask IN ( {2} )) ",
                                                                                                                                                                                    ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "),
                                                                                                                                                                                    completedStatus.Select(s => s.ToString()).ToArray().Join(", "),
                                                                                                                                                                                    socDisciplineTasks.Select(s => s.ToString()).ToArray().Join(", "));

            var rocOrRecertDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true);// new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISCResumptionofCarePT }; 
            var script2 = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut , 
                        secondaryepisode.StartDate as StartDate ,
                        secondaryepisode.EndDate as EndDate ,
                        primaryepisode.Id as NextEpisodeId ,
                        'RecertOrROCOASIS' as VisitType 
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes as primaryepisode ON st.PatientId = primaryepisode.PatientId  
                                    INNER JOIN patientepisodes as secondaryepisode ON st.PatientId = secondaryepisode.PatientId 
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            primaryepisode.Id IN ( {0} )  AND
                                            DATE(secondaryepisode.EndDate) = DATE(DATE_SUB(primaryepisode.StartDate, INTERVAL 1 DAY)) AND
                                            secondaryepisode.IsActive = 1  AND 
                                            secondaryepisode.IsDischarged = 0  AND 
                                            DATE(st.EventDate) between DATE(DATE_SUB(secondaryepisode.EndDate, INTERVAL 5 DAY)) and DATE(secondaryepisode.EndDate) AND
                                            st.IsMissedVisit = 0  AND
                                            st.IsDeprecated = 0 AND
                                            st.DisciplineTask > 0  AND 
                                            st.DisciplineTask IN ( {1} ) ",
                                                                                      ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "),
                                                                                      rocOrRecertDisciplineTasks.Select(s => s.ToString()).ToArray().Join(", "));

            var script = string.Format(" ({0}) UNION ({1}) ", script1, script2);

            var list = new List<ScheduleEvent>();

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    Discipline = reader.GetInt("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    VisitType = reader.GetStringNullable("VisitType"),
                    NextEpisodeId = reader.GetStringNullable("VisitType").IsEqual("RecertOrROCOASIS") ? reader.GetGuid("NextEpisodeId") : Guid.Empty
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetFinalClaimSchedules(Guid agencyId, List<Guid> ids)
        {
            var visitDisciplines = DisciplineFactory.NonBillableDisciplines().Select(d => d.ToString()).ToArray();
            var visitStatus = ScheduleStatusFactory.OASISAndNurseNotesAfterQA().ToArray();
            var orderDisciplineTasks = DisciplineTaskFactory.PhysicianOrdersWithOutFaceToFace().ToArray();
            var orderStatus = new string[] { ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() };

            var script1 = string.Format(@"SELECT 
                                        st.Id as Id ,
                                        st.PatientId as PatientId ,
                                        st.EpisodeId as EpisodeId ,
                                        st.EventDate as EventDate ,
                                        st.VisitDate as VisitDate ,
                                        st.Status as Status ,
                                        st.DisciplineTask as DisciplineTask , 
                                        st.Discipline as Discipline ,
                                        st.IsBillable as IsBillable ,
                                        st.IsMissedVisit as IsMissedVisit , 
                                        st.IsOrderForNextEpisode as IsOrderForNextEpisode ,
                                        pe.StartDate as StartDate ,
                                        pe.EndDate as EndDate ,
                                        pe.Id as NextEpisodeId ,
                                       'VisitsOrOrders' as VisitType 
                                        FROM 
                                            scheduletasks st
                                                INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                                    WHERE
                                                        pe.AgencyId = @agencyid AND 
                                                        pe.Id IN ( {0} ) AND 
                                                        DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                        st.IsMissedVisit = 0  AND 
                                                        st.IsDeprecated = 0 AND 
                                                        st.DisciplineTask > 0  AND 
                                                        (( st.IsBillable = 1 AND  st.Status NOT IN ( {1} )  AND st.Discipline NOT IN ( {2} ))  OR ( st.DisciplineTask IN ( {3} ) AND st.IsOrderForNextEpisode = 0 AND  st.Status NOT IN ( {4} ) )) ", ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "), visitStatus.Select(s => s.ToString()).ToArray().Join(", "), visitDisciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","), orderDisciplineTasks.Select(s => s.ToString()).ToArray().Join(", "), orderStatus.Join(", "));

            var script2 = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit ,
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        secondaryepisode.StartDate as StartDate ,
                        secondaryepisode.EndDate as EndDate ,
                        primaryepisode.Id as NextEpisodeId ,
                       'PreviousOrders' as VisitType 
                            FROM
                                 scheduletasks st
                                    INNER JOIN patientepisodes as primaryepisode ON st.PatientId = primaryepisode.PatientId 
                                    INNER JOIN patientepisodes as secondaryepisode ON st.PatientId = secondaryepisode.PatientId  
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            primaryepisode.Id IN ( {0} )  AND 
                                            DATE(secondaryepisode.EndDate) = DATE(DATE_SUB(primaryepisode.StartDate, INTERVAL 1 DAY)) AND 
                                            secondaryepisode.IsActive = 1  AND
                                            secondaryepisode.IsDischarged = 0  AND 
                                            DATE(st.EventDate) between DATE(secondaryepisode.StartDate) and DATE(secondaryepisode.EndDate) AND
                                            st.IsMissedVisit = 0  AND
                                            st.IsDeprecated = 0  AND
                                            st.IsOrderForNextEpisode = 1 AND
                                            st.DisciplineTask > 0  AND
                                            st.DisciplineTask IN ( {1} ) AND
                                            st.Status NOT IN ( {2} ) ", ids.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "), orderDisciplineTasks.Select(s => s.ToString()).ToArray().Join(", "), orderStatus.Join(", "));

            var script = string.Format(" ({0}) UNION ({1}) ", script1, script2);

            var list = new List<ScheduleEvent>();

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .SetMap(reader => new ScheduleEvent
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     DisciplineTask = reader.GetInt("DisciplineTask"),
                     EventDate = reader.GetDateTime("EventDate"),
                     VisitDate = reader.GetDateTime("VisitDate"),
                     Status = reader.GetInt("Status"),
                     Discipline = reader.GetInt("Discipline"),
                     IsBillable = reader.GetBoolean("IsBillable"),
                     IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                     IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                     EndDate = reader.GetDateTime("EndDate"),
                     StartDate = reader.GetDateTime("StartDate"),
                     VisitType = reader.GetStringNullable("VisitType"),
                     NextEpisodeId = reader.GetStringNullable("VisitType").IsEqual("PreviousOrders") ? reader.GetGuid("NextEpisodeId") : Guid.Empty
                 })
                 .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var additonalScript = string.Empty;

            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Discipline as Discipline ,
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        m.Status as Status ,
                        CONCAT('Reason: ', m.Reason, '\r\n', 'Comments: ', m.Comments) as MissedVisitComments,
                        st.Comments as Comments,
                        pr.PatientIdNumber as PatientIdNumber,
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName,
                        pe.Details as Details,
                        pe.StartDate as StartDate,
                        pe.EndDate as EndDate
                                FROM 
                                    scheduletasks st
                                        INNER JOIN missedvisits m ON st.Id = m.Id
                                        INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id
                                        INNER JOIN patients pr ON st.PatientId = pr.Id  
                                                WHERE
                                                        st.AgencyId = @agencyid {0} AND 
                                                        st.IsMissedVisit = 1 AND
                                                        st.IsDeprecated = 0 AND
                                                        pr.IsDeprecated = 0  AND 
                                                        pr.Status IN (1,2) AND 
                                                        st.IsDeprecated = 0  AND 
                                                        pe.IsActive = 1 AND 
                                                        pe.IsDischarged = 0  AND 
                                                        DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) 
                                                        ORDER BY st.EventDate DESC ", additonalScript);

            List<ScheduleEvent> scheduleList;
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetInt("Discipline"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    MissedVisitComments = reader.GetStringNullable("MissedVisitComments"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsMissedVisit = true,
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                    EpisodeNotes=reader.GetStringNullable("Details"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsList();
            }
            return scheduleList;
        }

        public List<ScheduleEvent> GetScheduleByUserId(Guid agencyId, Guid userId, DateTime start, DateTime end, bool isMissedVist, string additionalFilter)
        {
            var script = string.Format(@"SELECT 
                                    st.Id as Id ,
                                    st.PatientId as PatientId ,
                                    st.EpisodeId as EpisodeId ,
                                    st.UserId as UserId ,
                                    st.DisciplineTask as DisciplineTask , 
                                    st.EventDate as EventDate ,
                                    st.VisitDate as VisitDate ,
                                    st.Status as Status ,
                                    st.IsMissedVisit as IsMissedVisit , 
                                    st.ReturnReason as ReturnReason ,
                                    st.Comments as Comments ,
                                    pe.StartDate as StartDate ,
                                    pe.EndDate as EndDate ,
                                    pr.FirstName as FirstName , 
                                    pr.LastName as LastName ,
                                    pe.Details as EpisodeNotes 
                                        FROM 
                                            scheduletasks st
                                                INNER JOIN patientepisodes pe ON st.EpisodeId = pe.Id 
                                                INNER JOIN patients pr ON st.PatientId = pr.Id 
                                                    WHERE 
                                                        st.AgencyId = @agencyid AND 
                                                        st.UserId = @userid AND 
                                                        st.IsDeprecated = 0 AND 
                                                        st.IsMissedVisit = {0} AND
                                                        st.DisciplineTask > 0 AND 
                                                        pe.IsActive = 1  AND 
                                                        pe.IsDischarged = 0 AND 
                                                        pr.IsDeprecated = 0 AND
                                                        pr.Status IN (1,2) AND
                                                        DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  {1} 
                                                            ORDER BY DATE(st.EventDate) DESC ", isMissedVist, additionalFilter);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", start)
                .AddDateTime("enddate", end)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    EpisodeNotes = reader.GetStringNullable("EpisodeNotes"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return list;
        }

        public IList<UserVisitWidget> GetScheduleWidget(Guid agencyId, Guid userId, DateTime from, DateTime to, int limit, bool isMissedVisit, string additionalFilter)
        {
            var script = string.Format(@"SELECT 
                        pr.Id as PatientId , 
                        pe.Id as EpisodeId,
                        pe.IsDischarged as IsDischarged,
                        st.Id as Id,
                        st.DisciplineTask as DisciplineTask , 
                        st.IsMissedVisit as IsMissedVisit,
                        st.Status as Status , 
                        st.EventDate as EventDate ,
                        pr.FirstName as FirstName , 
                        pr.LastName as LastName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                    INNER JOIN patients pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.UserId = @userid AND
                                            st.IsDeprecated = 0 AND 
                                            st.IsMissedVisit = {0} AND 
                                            st.DisciplineTask > 0 AND
                                            pe.IsActive = 1  AND
                                            pe.IsDischarged = 0 AND
                                            pr.IsDeprecated = 0 AND
                                            pr.Status IN (1,2) AND 
                                            DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  {2} 
                                                ORDER BY DATE(st.EventDate) DESC LIMIT {1}", isMissedVisit, limit, additionalFilter);

            var list = new List<UserVisitWidget>();
            using (var cmd = new FluentCommand<UserVisitWidget>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", from)
                .AddDateTime("enddate", to)
                .SetMap(reader => new UserVisitWidget
                {
                    PatientId = reader.GetGuid("PatientId"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    EventId = reader.GetGuid("Id"),
                    IsDischarged = reader.GetBoolean("IsDischarged"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status"),
                    EventDate = reader.GetDateTime("EventDate").ToString("MM/dd/yyyy")
                })
                .AsList();
            }
            return list;
        }

        public ScheduleEvent GetFirstScheduledEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.IsVisitPaid as IsVisitPaid  
                            FROM 
                                scheduletasks st  
                                    WHERE 
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid AND
                                        st.EpisodeId = @episodeid AND
                                        st.IsDeprecated = 0 AND 
                                        st.DisciplineTask > 0 AND 
                                        st.IsMissedVisit = 0  AND 
                                        DATE(st.EventDate) between  DATE(@episodestartdate) and DATE(@episodeenddate) AND
                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0} 
                                            ORDER BY DATE(st.EventDate) ASC LIMIT 1 ", additionalScript);


            var scheduleEvent = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("episodestartdate", episodeStartDate)
                .AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    Discipline = reader.GetInt("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    StartDate = episodeStartDate,
                    EndDate = episodeEndDate
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public ScheduleEvent GetLastScheduledEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue  
                            FROM
                                scheduletasks st
                                    WHERE
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid AND
                                        st.EpisodeId = @episodeid AND
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0 AND
                                        st.IsMissedVisit = 0  AND
                                        DATE(st.EventDate) between DATE(@episodestartdate) and DATE(@episodeenddate) AND 
                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0}  
                                            ORDER BY DATE(st.EventDate) DESC LIMIT 1 ", additionalScript);


            var scheduleEvent = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("episodestartdate", episodeStartDate)
                .AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    Discipline = reader.GetInt("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = startDate,
                    EndDate = endDate
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public  List<ScheduleEvent> GetPreviousNotes(Guid agencyId, ScheduleEvent scheduledEvent, string[] disciplines, int[] disciplineTasks, int[] status, bool isVersionSpecific)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id,
                        st.PatientId as PatientId,
                        st.EpisodeId as EpisodeId,
                        st.UserId as UserId,
                        st.DisciplineTask as DisciplineTask, 
                        st.EventDate as EventDate,
                        st.VisitDate as VisitDate  
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patientepisodes pe ON st.EpisodeId = pe.Id  
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND
                                            st.Id != @eventid AND
                                            st.IsMissedVisit = 0 AND 
                                            st.IsDeprecated = 0 AND 
                                            pe.IsActive = 1 AND 
                                            pe.IsDischarged = 0 AND
                                            DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            st.DisciplineTask IN ({2}) AND
                                            st.Status IN ({3}) {0} {1} 
                                                ORDER BY DATE(st.EventDate) DESC LIMIT 5",
                    disciplines != null && disciplines.Length > 0 ? " AND st.Discipline IN (" + disciplines.ToCommaSeperatedNoQuote() + ") " : string.Empty, isVersionSpecific ? " AND st.Version = @version " : string.Empty, disciplineTasks.ToCommaSeperatedNoQuote(), status.ToCommaSeperatedNoQuote());
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("eventid", scheduledEvent.Id)
                .AddGuid("patientid", scheduledEvent.PatientId)
                .AddDateTime("startdate", scheduledEvent.EventDate.AddMonths(-4))
                .AddDateTime("enddate", scheduledEvent.EventDate)
                .AddInt("version", scheduledEvent.Version)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduledEventsOnlyWithId(Guid agencyId, Guid patientId, string eventIds, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty;

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.ToCommaSeperatedList());
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.ToCommaSeperatedList());
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask ,
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.TimeIn as TimeIn ,
                        st.TimeOut as TimeOut ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.Surcharge as Surcharge ,
                        st.AssociatedMileage as AssociatedMileage ,
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue ,
                        st.Version  as Version 
                            FROM 
                                scheduletasks st
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND 
                                            st.Id IN ( {0} ) AND
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND 
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {1}
                                                ", eventIds, additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap((reader) =>
               new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        Discipline = reader.GetInt("Discipline"),
                        IsBillable = reader.GetBoolean("IsBillable"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        Surcharge = reader.GetStringNullable("Surcharge"),
                        AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                        InPrintQueue = reader.GetBoolean("InPrintQueue"),
                        Version = reader.GetInt("Version")

                    })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty;
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format(" AND st.Status IN ( {0} ) ", scheduleStatus.ToCommaSeperatedList());
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.ToCommaSeperatedList());
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsBillable as IsBillable ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.TimeIn as TimeIn ,
                        st.TimeOut as TimeOut , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Surcharge as Surcharge ,
                        st.AssociatedMileage as AssociatedMileage ,
                        st.IsDeprecated as IsDeprecated , 
                        st.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        st.IsVisitPaid as IsVisitPaid ,
                        st.InPrintQueue as InPrintQueue ,
                        pe.StartDate as StartDate ,
                        pe.EndDate as EndDate ,
                        st.Version as Version 
                            FROM 
                                 scheduletasks st
                                    LEFT JOIN patientepisodes pe ON  st.EpisodeId = pe.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND 
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND 
                                            pe.IsActive = 1  AND 
                                            pe.IsDischarged = 0  AND
                                            DATE(st.EventDate) between  DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0}
                                                ", additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        Discipline = reader.GetInt("Discipline"),
                        Surcharge = reader.GetStringNullable("Surcharge"),
                        AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                        IsBillable = reader.GetBoolean("IsBillable"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                        IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                        InPrintQueue = reader.GetBoolean("InPrintQueue"),
                        Version = reader.GetInt("Version"),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                    }
                )
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduleByPatientEpisodeUserIdLean(Guid agencyId, Guid patientId, Guid episodeId, Guid optionalUserId)
        {
            var userScript = optionalUserId.IsEmpty() ? string.Empty : " AND st.UserId = @userid ";
            var script = string.Format(@"SELECT 
                                    st.Id as Id ,
                                    st.UserId as UserId ,
                                    st.DisciplineTask as DisciplineTask , 
                                    st.EventDate as EventDate ,
                                    st.VisitDate as VisitDate ,
                                    st.Status as Status ,
                                    st.IsMissedVisit as IsMissedVisit,
                                    st.IsBillable as IsBillable
                                        FROM 
                                            scheduletasks st
                                                    WHERE 
                                                        st.AgencyId = @agencyid AND 
                                                        st.PatientId = @patientid AND 
                                                        st.EpisodeId = @episodeid {0} AND 
                                                        st.IsDeprecated = 0 AND 
                                                        st.DisciplineTask > 0  
                                                             ", userScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", optionalUserId)
                 .AddGuid("patientid", patientId)
                  .AddGuid("episodeid", episodeId)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = episodeId,
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    IsBillable = reader.GetBoolean("IsBillable")
                })
                .AsList();
            }
            return list;
        }

        public bool ToggleScheduledTasksDelete(Guid agencyId, Guid patientId, List<Guid> eventIds, bool isDeprecated)
        {
            bool result = false;
            if (eventIds != null && eventIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduletasks st set st.IsDeprecated = @isdeprecated WHERE st.AgencyId = @agencyid AND st.PatientId = @patientid  AND st.Id IN ( @ids)");
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("patientid", patientId)
                        .AddString("ids",eventIds.ToCommaSeperatedNoQuote())
                        .AddInt("isdeprecated",isDeprecated ? 1 : 0)
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateScheduleTasksForReassign(Guid agencyId, Guid userId, List<Guid> eventIds)
        {
            bool result = false;
            if (eventIds != null && eventIds.Count > 0)
            {
                try
                {
                    var script = @"UPDATE scheduletasks st set st.UserId = @userid WHERE st.AgencyId = @agencyid AND st.Id IN ( @ids )";
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(this.connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddGuid("userid", userId)
                        .AddString("ids", eventIds.ToCommaSeperatedNoQuote())
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public bool UpdateScheduleTasksForEpisodeEdit(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, bool isActive, bool isDischarge)
        {
            bool result = false;
            try
            {
                var script = @"UPDATE scheduletasks st set st.StartDate = @startdate,st.EndDate = @enddate,st.IsActive = @isactive,st.IsDischarged = @isdischarged WHERE st.AgencyId = @agencyid AND st.PatientId = @patientid AND st.EpisodeId = @episodeid";
                var count = 0;
                using (var cmd = new FluentCommand<int>(script))
                {
                    count = cmd.SetConnection(this.connectionStringName)
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddGuid("episodeId", episodeId)
                    .AddDateTime("startdate",startDate)
                    .AddDateTime("enddate",endDate)
                    .AddBoolean("IsActive", isActive)
                    .AddBoolean("IsDischarged", isDischarge)
                    .AsNonQuery();
                }

                result = count > 0;
            }
            catch (Exception)
            {
                return false;
            }
            return result;
        }


        public List<ScheduleEvent> GetUsersScheduleTasksBetweenDatesByStatusForReassign(Guid agencyId, Guid patientId, Guid userId, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, params int[] scheduleStatus)
        {
            var additionalScript="";
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                                    st.Id as Id ,
                                    st.EpisodeId as EpisodeId ,
                                    st.UserId as UserId ,
                                    st.DisciplineTask as DisciplineTask , 
                                    st.Status as Status 
                                        FROM 
                                            scheduletasks st
                                                    WHERE 
                                                        st.AgencyId = @agencyid AND 
                                                        st.PatientId = @patientid AND
                                                        st.UserId = @userid AND 
                                                        st.IsDeprecated = 0 AND 
                                                        st.DisciplineTask > 0 AND 
                                                        st.IsActive = 1  AND 
                                                        st.IsDischarged = 0  AND
                                                        DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0}"
                , additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                 .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = userId,
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status"),
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetUsersScheduleTasksBetweenDatesByStatusForReassign(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, params int[] scheduleStatus)
        {
            var additionalScript = "";
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                                    st.Id as Id ,
                                    st.PatientId as PatientId,
                                    st.EpisodeId as EpisodeId ,
                                    st.UserId as UserId ,
                                    st.DisciplineTask as DisciplineTask , 
                                    st.Status as Status 
                                        FROM 
                                            scheduletasks st
                                                    WHERE 
                                                        st.AgencyId = @agencyid AND 
                                                        st.UserId = @userid AND 
                                                        st.IsDeprecated = 0 AND 
                                                        st.DisciplineTask > 0 AND 
                                                        st.IsActive = 1  AND 
                                                        st.IsDischarged = 0  AND
                                                        DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0}"
                , additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = userId,
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status"),
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduledEventsLeanWithId(Guid agencyId, Guid patientId, Guid episodeId, List<Guid> eventIds)
        {
            var script = string.Format(@"SELECT 
                                            st.Id as Id ,
                                            st.EpisodeId as EpisodeId,
                                            st.DisciplineTask as DisciplineTask , 
                                            st.Status as Status ,
                                            st.Discipline as Discipline 
                                                FROM 
                                                    scheduletasks st
                                                            WHERE 
                                                                st.AgencyId = @agencyid AND
                                                                st.PatientId = @patientid AND
                                                                st.EpisodeId = @episodeid AND 
                                                                st.Id IN ( {0} ) AND
                                                                st.IsDeprecated = 0 AND 
                                                                st.DisciplineTask > 0 ", eventIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", "));

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    EpisodeId =episodeId,
                    PatientId = patientId,
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status"),
                    Discipline = reader.GetInt("Discipline"),
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsForPrint(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.EventDate,
                        st.VisitDate
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate 
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.PatientId = @patientid AND 
                                        st.EpisodeId = @episodeid AND
                                        st.IsDeprecated = 0 AND
                                        st.DisciplineTask > 0 ");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap((reader) =>

                   new ScheduleEvent
                   {
                       Id = reader.GetGuid("Id"),
                       PatientId = patientId,
                       EpisodeId = episodeId,
                       UserId = reader.GetGuid("UserId"),
                       DisciplineTask = reader.GetInt("DisciplineTask"),
                       Status = reader.GetInt("Status"),
                       IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                       Discipline = reader.GetInt("Discipline"),
                       EventDate = reader.GetDateTime("EventDate"),
                       VisitDate = reader.GetDateTime("VisitDate"),
                       StartDate = reader.GetDateTime("StartDate"),
                       EndDate = reader.GetDateTime("EndDate")

                   }
                )
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetScheduleEventsVeryLeanNonOptionalPar(Guid agencyId, Guid patientId, Guid episodeId, int[] scheduleStatus, int[] disciplineTasks, DateTime startDate, DateTime endDate, bool IsDateRange, bool IsMissedVisitIncluded)
        {
            var scheduleAdditionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                scheduleAdditionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                scheduleAdditionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (!IsMissedVisitIncluded)
            {
                scheduleAdditionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (IsDateRange)
            {
                scheduleAdditionalScript += " AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.IsBillable as IsBillable,
                        st.Discipline as Discipline  
                            FROM 
                                scheduletasks st
                                        WHERE
                                            st.AgencyId = @agencyId  AND 
                                            st.PatientId = @patientId  AND
                                            st.EpisodeId = @episodeid  AND 
                                            st.IsDeprecated = 0  {0} ", scheduleAdditionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    EpisodeId = episodeId,
                    PatientId = patientId,
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetInt("Status"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    Discipline = reader.GetInt("Discipline")
                })
                .AsList();
            }
            return scheduleList;
        }

        
        //TODO: If possible move to other repo. 
        /// <summary>
        /// 
        /// </summary>

        public bool UpdateEntityForReassigningUser(Guid agencyId, Guid employeeId, string script)
        {
            return EntityHelper.UpdateEntityForReassigningUser(agencyId, employeeId, script, this.connectionStringName);
        }

        public bool UpdateEntityForReassigningUser(Guid agencyId, Guid patientId, Guid userId, string script)
        {
            return EntityHelper.UpdateEntityForReassigningUser(agencyId, patientId, userId, script, this.connectionStringName);
        }


        public bool UpdateEntityForDelete(Guid agencyId, Guid patientId,Guid episodeId, string script)
        {
            return EntityHelper.UpdateDocumentEntityForDelete(agencyId, patientId, episodeId, script, this.connectionStringName);
        }



        public List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, Guid episodeId)
        {
            var list = new List<ReturnComment>();
            var script = string.Format(@"SELECT 
                                            rc.Id,
                                            rc.EpisodeId,
                                            rc.EventId,
                                            rc.UserId,
                                            rc.Comments,
                                            rc.Modified
                                                FROM 
                                                    returncomments rc 
                                                        WHERE
                                                            rc.AgencyId = @agencyid AND
                                                            rc.EpisodeId = @episodeId AND
                                                            rc.IsDeprecated = 0");
            using (var cmd = new FluentCommand<ReturnComment>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("episodeId", episodeId)
                 .SetMap(reader => new ReturnComment
                 {
                     Id = reader.GetInt("Id"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     EventId = reader.GetGuid("EventId"),
                     UserId = reader.GetGuid("UserId"),
                     Comments = reader.GetStringNullable("Comments"),
                     Modified = reader.GetDateTime("Modified")
                 }).AsList();
            }
            return list;
        }

        public List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, List<Guid> episodeIds)
        {
            var list = new List<ReturnComment>();
            if (episodeIds != null && episodeIds.Count > 0)
            {
                var ids = episodeIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT 
                                            rc.Id,
                                            rc.EpisodeId,
                                            rc.EventId,
                                            rc.UserId,
                                            rc.Comments,
                                            rc.Modified
                                                FROM 
                                                    returncomments rc
                                                        WHERE
                                                            rc.AgencyId = @agencyid AND
                                                            rc.EpisodeId IN ( {0} )AND
                                                            rc.IsDeprecated = 0", ids);
                using (var cmd = new FluentCommand<ReturnComment>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new ReturnComment
                     {
                         Id = reader.GetInt("Id"),
                         EpisodeId = reader.GetGuid("EpisodeId"),
                         EventId = reader.GetGuid("EventId"),
                         UserId = reader.GetGuid("UserId"),
                         Comments = reader.GetStringNullable("Comments"),
                         Modified = reader.GetDateTime("Modified")
                     }).AsList();
                }
            }
            return list;
        }

  

        #region  Missed Visit

//        public bool AddMissedVisit(MissedVisit missedVisit)
//        {
//            try
//            {
//                if (missedVisit != null)
//                {
//                    missedVisit.Date = DateTime.Now;
//                    database.Add<MissedVisit>(missedVisit);
//                    return true;
//                }
//            }
//            catch (Exception)
//            {
//                return false;
//            }
//            return false;
//        }

//        public bool UpdateMissedVisit(MissedVisit missedVisit)
//        {
//            try
//            {
//                if (missedVisit != null)
//                {
//                    missedVisit.Date = DateTime.Now;
//                    database.Update<MissedVisit>(missedVisit);
//                    return true;
//                }
//            }
//            catch (Exception)
//            {
//                return false;
//            }
//            return false;
//        }

//        public MissedVisit GetMissedVisit(Guid agencyId, Guid id)
//        {
//            return database.Single<MissedVisit>(m => m.AgencyId == agencyId && m.Id == id);
//        }

//        public MissedVisit GetMissedVisitForPrint(Guid agencyId, Guid patientId, Guid id)
//        {
//            MissedVisit visit = null;
//            var script = string.Format(@"SELECT 
//                            m.PatientId, 
//                            m.Date, 
//                            m.Status, 
//                            Reason, 
//                            m.Comments, 
//                            IsOrderGenerated, 
//                            IsPhysicianOfficeNotified, 
//                            SignatureText, 
//                            SignatureDate,
//                            EventDate as EventDate,
//                            st.DisciplineTask,
//                            pp.AgencyLocationId,
//                            pp.FirstName,
//                            pp.LastName,
//                            pp.MiddleInitial,
//                            pp.PatientIdNumber
//                                FROM missedvisits m 
//                                INNER JOIN scheduletasks st on m.Id = st.Id 
//                                INNER JOIN patientprofiles pp on m.PatientId = pp.Id
//                                    WHERE m.Id = @id AND m.PatientId = @patientid AND m.AgencyId = @agencyid");
//            using (var cmd = new FluentCommand<MissedVisit>(script))
//            {
//                visit = cmd.SetConnection(connectionStringName)
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("patientid", patientId)
//                .AddGuid("id", id)
//                .SetMap(reader => new MissedVisit
//                {
//                    Id = id,
//                    PatientId = reader.GetGuid("PatientId"),
//                    AgencyId = agencyId,
//                    Date = reader.GetDateTime("Date"),
//                    Status = reader.GetInt("Status"),
//                    Reason = reader.GetStringNullable("Reason"),
//                    Comments = reader.GetStringNullable("Comments"),
//                    IsOrderGenerated = reader.GetBoolean("IsOrderGenerated"),
//                    IsPhysicianOfficeNotified = reader.GetBoolean("IsPhysicianOfficeNotified"),
//                    SignatureText = reader.GetStringNullable("SignatureText"),
//                    SignatureDate = reader.GetDateTime("SignatureDate"),
//                    EventDate = reader.GetDateTime("EventDate").ToZeroFilled(),
//                    DisciplineTaskName = DisciplineTaskFactory.ConvertDisciplineTaskToString(reader.GetInt("DisciplineTask")),
//                    PatientProfile = new PatientProfileLean()
//                    {
//                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
//                        AgencyLocationId = reader.GetGuid("AgencyLocationId"),
//                        FirstName = reader.GetStringNullable("FirstName"),
//                        LastName = reader.GetStringNullable("LastName"),
//                        MiddleInitial = reader.GetStringNullable("MiddleInitial")
//                    }
//                }).AsSingle();
//            }
//            return visit;
//        }

       

//        public List<MissedVisit> GetMissedVisitsByStatus(Guid agencyId, DateTime startDate, DateTime endDate, int status)
//        {
//            var list = new List<MissedVisit>();
//            var script = @"SELECT * 
//                             FROM missedvisits m 
//                                 WHERE 			                                 
//                                 m.Date between @startDate AND @endDate AND
//                                 m.Status = @status";

//            using (var cmd = new FluentCommand<MissedVisit>(script))
//            {
//                list = cmd.SetConnection(connectionStringName)
//                .AddGuid("agencyid", agencyId)
//                .AddDateTime("startdate", startDate)
//                .AddDateTime("enddate", endDate)
//                .AddInt("status", status)
//                .SetMap(reader => new MissedVisit
//                {
//                    Id = reader.GetGuid("Id"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    AgencyId = agencyId,
//                    EpisodeId = reader.GetGuid("EpisodeId"),
//                    Date = reader.GetDateTime("Date"),
//                    Status = reader.GetInt("Status"),
//                    Reason = reader.GetStringNullable("Reason"),
//                    Comments = reader.GetStringNullable("Comments"),
//                    IsOrderGenerated = reader.GetBoolean("IsOrderGenerated"),
//                    IsPhysicianOfficeNotified = reader.GetBoolean("IsPhysicianOfficeNotified"),
//                    SignatureText = reader.GetStringNullable("SignatureText"),
//                    SignatureDate = reader.GetDateTime("SignatureDate"),
//                }).AsList();
//            }
//            return list;
//        }


        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            try
            {
                if (missedVisit != null)
                {
                    missedVisit.Date = DateTime.Now;
                    database.Add<MissedVisit>(missedVisit);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }


        public bool UpdateMissedVisit(MissedVisit missedVisit)
        {
            try
            {
                if (missedVisit != null)
                {
                    missedVisit.Date = DateTime.Now;
                    database.Update<MissedVisit>(missedVisit);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public MissedVisit GetMissedVisitOnly(Guid agencyId, Guid Id)
        {
            return database.Single<MissedVisit>(m => m.AgencyId == agencyId && m.Id == Id);
        }


        public List<MissedVisit> GetMissedVisitsByIds(Guid agencyId, List<Guid> missedVisitIds)
        {
            var list = new List<MissedVisit>();
            if (missedVisitIds != null && missedVisitIds.Count > 0)
            {
                var ids = missedVisitIds.ToCommaSeperatedList();
                var script = string.Format(@"SELECT 
                                            m.Id,
                                            m.Status,
                                            m.Reason,
                                            m.Comments
                                                FROM 
                                                    missedvisits m
                                                        WHERE
                                                            m.AgencyId = @agencyid AND
                                                            m.Id IN ( {0} )", ids);
                using (var cmd = new FluentCommand<MissedVisit>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new MissedVisit
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status"),
                         Reason = reader.GetStringNullable("Reason"),
                         Comments = reader.GetStringNullable("Comments")
                     }).AsList();
                }
            }
            return list;
        }


        public MissedVisit GetMissedVisitsWithScheduleInfo(Guid agencyId, Guid missedVisitId)
        {
            var list = new MissedVisit();
            var script = string.Format(@"SELECT 
                                            m.Id as Id,
                                            m.PatientId as PatientId,
                                            m.EpisodeId as EpisodeId,
                                            st.UserId as UserId,
                                            m.Status as Status,
                                            m.Reason as Reason,
                                            m.Comments as Comments,
                                            m.IsOrderGenerated as IsOrderGenerated,
                                            m.IsPhysicianOfficeNotified as IsPhysicianOfficeNotified,
                                            m.UserSignatureAssetId as UserSignatureAssetId,
                                            m.SignatureDate as SignatureDate,
                                            m.Date as Date,
                                            st.DisciplineTask as DisciplineTask
                                                FROM 
                                                    missedvisits m
                                                       INNER JOIN scheduletasks st ON m.Id = st.Id
                                                        WHERE
                                                            m.AgencyId = @agencyid AND
                                                            m.Id = @missedVisitId limit 0,1");
            using (var cmd = new FluentCommand<MissedVisit>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("missedVisitId", missedVisitId)
                 .SetMap(reader => new MissedVisit
                 {
                     Id = reader.GetGuid("Id"),
                     PatientId = reader.GetGuid("PatientId"),
                     EpisodeId = reader.GetGuid("EpisodeId"),
                     UserId = reader.GetGuid("UserId"),
                     DisciplineTask = reader.GetInt("DisciplineTask"),
                     Status = reader.GetInt("Status"),
                     Reason = reader.GetStringNullable("Reason"),
                     Comments = reader.GetStringNullable("Comments"),
                     IsOrderGenerated = reader.GetBoolean("IsOrderGenerated"),
                     IsPhysicianOfficeNotified=reader.GetBoolean("IsPhysicianOfficeNotified"),
                     SignatureDate = reader.GetDateTime("SignatureDate"),
                     UserSignatureAssetId = reader.GetGuid("UserSignatureAssetId"),
                     Date = reader.GetDateTime("Date")
                 }).AsSingle();
            }

            return list;
        }


        public List<MissedVisit> GetMissedVisitsByIdOnlyStatus(Guid agencyId, List<Guid> missedVisitIds)
        {
            var list = new List<MissedVisit>();
            if (missedVisitIds != null && missedVisitIds.Count > 0)
            {
                var ids = missedVisitIds.ToCommaSeperatedList();
                var script = string.Format(@"SELECT 
                                            m.Id,
                                            m.Status
                                                FROM 
                                                    missedvisits m
                                                        WHERE
                                                            m.AgencyId = @agencyid AND
                                                            m.Id IN ( {0} )", ids);
                using (var cmd = new FluentCommand<MissedVisit>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .SetMap(reader => new MissedVisit
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status"),
                     }).AsList();
                }
            }
            return list;
        }

        public List<MissedVisit> GetMissedVisitsByEpisodeId(Guid agencyId, Guid patientId, Guid episodeId)
        {
            var list = new List<MissedVisit>();
            var script = string.Format(@"SELECT 
                                            m.Id,
                                            m.Status,
                                            m.Reason,
                                            m.Comments
                                                FROM 
                                                    missedvisits m
                                                        WHERE
                                                            m.AgencyId = @agencyid AND
                                                            m.PatientId = @patientid AND
                                                            m.EpisodeId = @episodeid");
            using (var cmd = new FluentCommand<MissedVisit>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                 .AddGuid("agencyid", agencyId)
                 .AddGuid("patientid", patientId)
                 .AddGuid("episodeid", episodeId)
                 .SetMap(reader => new MissedVisit
                 {
                     Id = reader.GetGuid("Id"),
                     Status = reader.GetInt("Status"),
                     Reason = reader.GetStringNullable("Reason"),
                     Comments = reader.GetStringNullable("Comments")
                 }).AsList();
            }
            return list;
        }

        public List<MissedVisit> GetMissedVisitsByEpisodeIds(Guid agencyId, List<Guid> episodeIds)
        {
            var list = new List<MissedVisit>();
            if (episodeIds != null && episodeIds.Count > 0)
            {
                var script = string.Format(@"SELECT 
                                            m.Id,
                                            m.Status,
                                            m.Reason,
                                            m.Comments
                                                FROM 
                                                    missedvisits m
                                                        WHERE
                                                            m.AgencyId = @agencyid AND
                                                            m.EpisodeId IN ( @episodeids )");
                using (var cmd = new FluentCommand<MissedVisit>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddString("episodeids", episodeIds.ToCommaSeperatedNoQuote())
                     .SetMap(reader => new MissedVisit
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status"),
                         Reason = reader.GetStringNullable("Reason"),
                         Comments = reader.GetStringNullable("Comments")
                     }).AsList();
                }
            }
            return list;
        }



        public List<MissedVisit> GetMissedVisitsByEpisodeIds(Guid agencyId, Guid patientId, List<Guid> episodeIds)
        {
            var list = new List<MissedVisit>();
            if (episodeIds != null && episodeIds.Count > 0)
            {
                var script = string.Format(@"SELECT 
                                            m.Id,
                                            m.Status,
                                            m.Reason,
                                            m.Comments
                                                FROM 
                                                    missedvisits m
                                                        WHERE
                                                            m.AgencyId = @agencyid AND
                                                            m.PatientId = @patientid AND
                                                            m.EpisodeId IN ( @episodeids )");
                using (var cmd = new FluentCommand<MissedVisit>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("patientid", patientId)
                     .AddString("episodeids", episodeIds.ToCommaSeperatedNoQuote())
                     .SetMap(reader => new MissedVisit
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status"),
                         Reason = reader.GetStringNullable("Reason"),
                         Comments = reader.GetStringNullable("Comments")
                     }).AsList();
                }
            }
            return list;
        }


        public List<MissedVisit> GetMissedVisitsByEpisodeIdsOnlyStatus(Guid agencyId, Guid patientId, List<Guid> episodeIds)
        {
            var list = new List<MissedVisit>();
            if (episodeIds != null && episodeIds.Count > 0)
            {
                var script = string.Format(@"SELECT 
                                            m.Id,
                                            m.Status
                                                FROM 
                                                    missedvisits m
                                                        WHERE
                                                            m.AgencyId = @agencyid AND
                                                            m.PatientId = @patientid AND
                                                            m.EpisodeId IN ( @episodeids )");
                using (var cmd = new FluentCommand<MissedVisit>(script))
                {
                    list = cmd.SetConnection(connectionStringName)
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("patientid", patientId)
                     .AddString("episodeids", episodeIds.ToCommaSeperatedNoQuote())
                     .SetMap(reader => new MissedVisit
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status")
                     }).AsList();
                }
            }
            return list;
        }

//        public List<MissedVisit> GetMissedVisitsByIdsAndStatus(Guid agencyId, int status, List<Guid> missedVisitIds)
//        {
//            var list = new List<MissedVisit>();
//            if (missedVisitIds != null && missedVisitIds.Count > 0)
//            {
//                var ids = missedVisitIds.ToCommaSeperatedList();
//                var script = string.Format(@"SELECT 
//                                            m.Id,
//                                            m.Status
//                                                FROM 
//                                                    missedvisits m
//                                                        WHERE
//                                                            m.AgencyId = @agencyid AND
//                                                            m.Status = @status AND
//                                                            m.Id IN ( {0} )", ids);
//                using (var cmd = new FluentCommand<MissedVisit>(script))
//                {
//                    list = cmd.SetConnection(connectionStringName)
//                     .AddGuid("agencyid", agencyId)
//                     .AddInt("status", status)
//                     .SetMap(reader => new MissedVisit
//                     {
//                         Id = reader.GetGuid("Id"),
//                         Status = reader.GetInt("Status")
//                     }).AsList();
//                }
//            }
//            return list;
//        }

        public bool ToggleScheduledEventsPaid(Guid agencyId, List<Guid> eventIds, bool isVisitPaid)
        {
            bool result = false;
            if (eventIds != null && eventIds.Count > 0)
            {
                try
                {
                    var script = string.Format(@"UPDATE scheduletasks st set st.IsVisitPaid = @isvisitpaid WHERE st.AgencyId = @agencyid AND  st.Id IN ( @eventids )");
                    var count = 0;
                    using (var cmd = new FluentCommand<int>(script))
                    {
                        count = cmd.SetConnection(this.connectionStringName)
                        .AddGuid("agencyid", agencyId)
                        .AddInt("isvisitpaid", isVisitPaid ? 1 : 0)
                        .AddString("eventids", eventIds.ToCommaSeperatedNoQuote())
                        .AsNonQuery();
                    }

                    result = count > 0;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return result;
        }

        public List<MissedVisit> GetMissedVisitsByIdsAndStatus(Guid agencyId, List<int> statuses, List<Guid> missedVisitIds)
        {
            var list = new List<MissedVisit>();
            if (missedVisitIds != null && missedVisitIds.Count > 0)
            {
                var script = string.Format(@"SELECT 
                                            Id,
                                            Status
                                                FROM 
                                                    missedvisits 
                                                        WHERE
                                                            AgencyId = @agencyid AND
                                                            Id IN ( @ids ) AND
                                                            Status IN ( @statuses) ");
                using (var cmd = new FluentCommand<MissedVisit>(script))
                {
                    list = cmd.SetConnection("AgencyManagementConnectionString")
                     .AddGuid("agencyid", agencyId)
                     .AddString("statuses", statuses.ToCommaSeperatedNoQuote())
                      .AddString("ids", missedVisitIds.ToCommaSeperatedNoQuote())
                     .SetMap(reader => new MissedVisit
                     {
                         Id = reader.GetGuid("Id"),
                         Status = reader.GetInt("Status")
                     }).AsList();
                }
            }
            return list;
        }


        public List<ScheduleEvent> GetPatientScheduledEventsLeanWithUserIdAndCheckMissedVisitStatus(Guid agencyId, Guid patientIdOptional, Guid userIdOptional, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit)
        {
            //var eventDateWhere = GetWhereAppSpecificSql(SqlSelectType.EventDate);
            var additionalScript = string.Empty;
            if (!patientIdOptional.IsEmpty())
            {
                additionalScript = " AND pr.Id = @patientid ";
            }
            else
            {
                additionalScript = " AND pr.Status IN (1,2) ";
            }
            if (!userIdOptional.IsEmpty())
            {
                additionalScript += " AND  st.UserId = @userid ";
            }
            if (IsDateRange)
            {
                additionalScript += " AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.ToCommaSeperatedList());
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                if (IsMissedVisitIncluded)
                {
                    additionalScript += string.Format("  AND ( st.Status IN ( {0} ) OR ( AND st.IsMissedVisit = 1 )) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
                }
                else
                {
                    additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
                }
            }
            var limitScript = string.Empty;

            if (limit > 0)
            {
                limitScript += string.Format(" LIMIT {0} ", limit);
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EventDate as EventDate , 
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.IsMissedVisit as IsMissedVisit ,
                        st.Discipline as Discipline,
                        pr.PatientIdNumber as PatientIdNumber, 
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patients pr ON  pr.Id = st.PatientId
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.IsDeprecated = 0 AND
                                            st.DisciplineTask > 0  AND
                                            DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) {2} {3} ", additionalScript, limitScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientIdOptional)
                .AddGuid("userid", userIdOptional)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                 new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        Discipline = reader.GetInt("Discipline"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit")

                    }
                )
                .AsList();
            }
            if (IsASC)
            {
                return list.OrderBy(l => l.EventDate).ToList();
            }
            else
            {
                return list.OrderByDescending(l => l.EventDate).ToList();
            }
        }

        public List<ScheduleEvent> GetPatientScheduledEventsLeanWithUserId(Guid agencyId, Guid patientIdOptional, Guid userIdOptional, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit)
        {
            var additionalScript = string.Empty;
            if (!patientIdOptional.IsEmpty())
            {
                additionalScript = " AND pr.Id = @patientid ";
            }
            else
            {
                additionalScript = " AND pr.Status IN (1,2) ";
            }
            if (!userIdOptional.IsEmpty())
            {
                additionalScript += " AND  st.UserId = @userid ";
            }
            if (IsDateRange)
            {
                additionalScript += string.Format(" AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) ");
            }
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var limitScript = string.Empty;

            if (limit > 0)
            {
                limitScript += string.Format(" LIMIT {0} ", limit);
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EventDate as EventDate , 
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline,
                        st.IsMissedVisit as IsMissedVisit,
                        pr.PatientIdNumber as PatientIdNumber, 
                        pr.FirstName as FirstName, 
                        pr.LastName as LastName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patients pr ON  pr.Id = st.PatientId
                                        WHERE
                                            st.AgencyId = @agencyid AND
                                            st.IsDeprecated = 0 AND
                                            st.DisciplineTask > 0  AND
                                            DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) {2} {3} ", additionalScript, limitScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientIdOptional)
                 .AddGuid("userid", userIdOptional)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                    new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        Discipline = reader.GetInt("Discipline"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit")

                    })
                .AsList();
            }
            if (IsASC)
            {
                return list.OrderBy(l => l.EventDate).ToList();
            }
            else
            {
                return list.OrderByDescending(l => l.EventDate).ToList();
            }
        }


        public ScheduleEvent GetCarePlan(Guid agencyId, Guid episodeId, Guid patientId,DateTime startDate, DateTime endDate, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.EpisodeId as EpisodeId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Discipline as Discipline
                            FROM
                                scheduletasks st
                                        WHERE
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND
                                            st.EpisodeId = @episodeid AND
                                            st.DisciplineTask IN ({0}) AND
                                            st.IsDeprecated = 0 AND
                                            st.IsMissedVisit = 0 AND
                                            DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)
                                                ORDER BY DATE(st.EventDate) DESC LIMIT 1", disciplineTasks.ToCommaSeperatedList());


            ScheduleEvent scheduleEvent;
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                //.AddDateTime("episodestartdate", episodeStartDate)
                //.AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Discipline = reader.GetInt("Discipline")
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public PatientVisitNote GetCarePlanForEpisode(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.EpisodeId as EpisodeId ,
                        n.Note as Note
                            FROM
                                scheduletasks st
                                     INNER JOIN patientvisitnotes n ON n.Id = st.Id
                                        WHERE
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND
                                            st.EpisodeId = @episodeid AND
                                            st.DisciplineTask IN ({0}) AND
                                            st.IsDeprecated = 0 AND
                                            st.IsMissedVisit = 0 AND
                                            DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) 
                                                ORDER BY DATE(st.EventDate) DESC LIMIT 1", disciplineTasks.ToCommaSeperatedList());


            PatientVisitNote scheduleEvent;
            using (var cmd = new FluentCommand<PatientVisitNote>(script))
            {
                scheduleEvent = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                
                    //.AddDateTime("episodestartdate", episodeStartDate)
                    //.AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Note = reader.GetStringNullable("Note")
                })
                .AsSingle();
            }
            return scheduleEvent;
        }



        public PatientVisitNote GetCarePlanForHHAideCarePlanNote(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.EpisodeId as EpisodeId ,
                        n.Note as Note
                            FROM
                                scheduletasks st
                                     INNER JOIN patientvisitnotes n ON n.Id = st.Id
                                        WHERE
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND
                                            st.DisciplineTask IN ({0}) AND
                                            st.IsDeprecated = 0 AND
                                            st.IsMissedVisit = 0 AND
                                           ((st.EpisodeId = @episodeid AND DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)) OR (DATE(st.EndDate) = DATE(@previousenddate) AND DATE(st.EventDate) > DATE(@previousschedulestartdate))) AND
                                            DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate)  
                                                ORDER BY DATE(st.EventDate) DESC LIMIT 1", disciplineTasks.ToCommaSeperatedList());

            PatientVisitNote scheduleEvent;
            using (var cmd = new FluentCommand<PatientVisitNote>(script))
            {
                scheduleEvent = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("previousenddate", startDate.AddDays(-1))
                .AddDateTime("previousschedulestartdate", startDate.AddDays(-6))
                //.AddDateTime("episodestartdate", episodeStartDate)
                //.AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Note = reader.GetStringNullable("Note")
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public ScheduleEvent GetCarePlanForHHAideCarePlan(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, DateTime eventDate, out IDictionary<string, NotesQuestion> pocQuestions)
        {
            var planOfCareNote = GetCarePlanForHHAideCarePlanNote(agencyId, episodeId, patientId, startDate,endDate, new List<int> { (int)DisciplineTasks.HHAideCarePlan }); //patientService.GetCarePlanBySelectedEpisode(patientId, episodeId, DisciplineTasks.HHAideCarePlan, scheduledEvent.EventDate.ToDateTime());
            if (planOfCareNote != null)
            {
                var pocScheduleEvent = new ScheduleEvent { AgencyId = agencyId, Id = planOfCareNote.Id, PatientId = patientId, EpisodeId = planOfCareNote.EpisodeId };
                pocQuestions = planOfCareNote.ToDictionary();
                if (pocQuestions.IsNotNullOrEmpty())
                {
                    if (pocQuestions.ContainsKey("SelectedEpisodeId") && pocQuestions["SelectedEpisodeId"] != null)
                    {
                        if (pocQuestions["SelectedEpisodeId"].Answer == episodeId.ToString())
                        {
                            return pocScheduleEvent;
                        }
                        else
                        {
                            pocQuestions = null;
                            return null;
                        }
                    }
                    else
                    {
                        if (planOfCareNote.EpisodeId != episodeId)
                        {
                            pocQuestions = null;
                            return null;
                        }
                        else
                        {
                            return pocScheduleEvent;
                        }
                    }
                }
                else
                {
                    if (planOfCareNote.EpisodeId != episodeId)
                    {
                        pocQuestions = null;
                        return null;
                    }
                    else
                    {
                        return pocScheduleEvent;
                    }
                }
            }
            pocQuestions = null;
            return null;
        }


        public List<ScheduleEvent> GetScheduleByBranchDateRangeAndStatusLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var status = string.Empty;
            if (patientStatus <= 0)
            {
                status = " AND  pr.Status IN (1,2)";
            }
            else
            {
                status = " AND pr.Status = @statusid";
            }
            var branch = string.Empty;

            if (!branchId.IsEmpty())
            {
                branch = " AND pr.AgencyLocationId = @branchId";
            }
            var missedVisit = string.Empty;
            if (!IsMissedVisitIncluded)
            {
                missedVisit = " AND st.IsMissedVisit = 0 ";
            }
            var schedulleStatusScript = string.Empty;
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                schedulleStatusScript = string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.ToCommaSeperatedList());
            }

            var script = string.Format(@"SELECT 
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EventDate as EventDate , 
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName ,
                        pr.PatientIdNumber as PatientIdNumber
                            FROM 
                                scheduletasks st
                                    INNER JOIN patients pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid  {0} {1} AND
                                            st.IsDeprecated = 0  {2}  {3} AND
                                            st.IsActive = 1  AND
                                            st.IsDischarged = 0  AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(st.EventDate) between  DATE(st.StartDate) and DATE(st.EndDate)", branch, status, schedulleStatusScript, missedVisit);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader =>
                 new ScheduleEvent
                    {
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        Discipline = reader.GetInt("Discipline"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate")
                    })
                .AsList();
            }
            return scheduleList;

        }

        public List<ScheduleEvent> GetScheduleByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND pr.Status = @statusid ";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId ";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.ToCommaSeperatedList());
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.ToCommaSeperatedList());
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EventDate as EventDate , 
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline ,
                        st.IsMissedVisit as IsMissedVisit , 
                        pr.PatientIdNumber as PatientIdNumber ,
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patients pr ON st.PatientId = pr.Id
                                        WHERE
                                            st.AgencyId = @agencyid  AND 
                                            st.IsDeprecated = 0 AND
                                            st.IsActive = 1 AND
                                            st.IsDischarged = 0 AND
                                            DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        PatientId = reader.GetGuid("PatientId"),
                        EpisodeId = reader.GetGuid("EpisodeId"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        Discipline = reader.GetInt("Discipline"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate")

                    })
                .AsList();
            }
            return scheduleList;

        }

        public List<ScheduleEvent> GetScheduleDeviations(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND pr.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        pr.PatientIdNumber as PatientIdNumber ,
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName  
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patients pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid  AND 
                                            pr.IsDeprecated = 0  AND 
                                            st.IsDeprecated = 0  AND 
                                            st.IsActive = 1 AND 
                                            st.IsDischarged = 0  AND 
                                            DATE(st.EventDate) <> DATE(st.VisitDate) AND
                                            DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND 
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return scheduleList;

        }

        public List<ScheduleEvent> GetScheduledEventsOnlyLean(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty;

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.ToCommaSeperatedList());
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.ToCommaSeperatedList());
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EventDate as EventDate , 
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.Version,
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate 
                            FROM 
                                scheduletasks st
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.PatientId = @patientid AND 
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND 
                                            st.IsActive = 1  AND 
                                            st.IsDischarged = 0  AND
                                            DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0}", additionalScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                 new ScheduleEvent
                    {
                        Id = reader.GetGuid("Id"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        Version = reader.GetInt("Version"),
                        StartDate = reader.GetDateTime("StartDate"),
                        EndDate = reader.GetDateTime("EndDate"),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate")
                    })
                .AsList();
            }
            return list;
        }

        public List<UserVisit> GetUserVisitLean(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additionalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND pr.Status = @statusid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id,
                        st.DisciplineTask as DisciplineTask , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EventDate as EventDate , 
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.IsMissedVisit as IsMissedVisit , 
                        pr.FirstName as FirstName , 
                        pr.LastName as LastName  
                            FROM
                                scheduletasks st
                                    INNER JOIN patients pr ON st.PatientId = pr.Id
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.UserId = @userid AND 
                                            st.IsActive = 1  AND 
                                            st.IsDischarged = 0 AND
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND 
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(st.EventDate) between  DATE(st.StartDate) and DATE(st.EndDate) {0}", additionalScript);

            var list = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader =>
                 new UserVisit
                    {
                        Id = reader.GetGuid("Id"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        Status = reader.GetStringNullable("Status"),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        ScheduleDate = reader.GetStringNullable("EventDate"),
                        VisitDate = reader.GetStringNullable("VisitDate")
                    })
                .AsList();
            }
            return list;
        }

        public List<UserVisit> GetUserVisitLeanByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND pr.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EventDate as EventDate , 
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.IsMissedVisit as IsMissedVisit , 
                        pr.FirstName as FirstName , 
                        pr.LastName as LastName  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patients pr ON st.PatientId = pr.Id
                                        WHERE
                                            st.AgencyId = @agencyid  AND 
                                            pr.IsDeprecated = 0 AND 
                                            st.IsDeprecated = 0 AND
                                            st.IsActive = 1 AND
                                            st.IsDischarged = 0 AND
                                            DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0} ", additonalScript);

            var scheduleList = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                 new UserVisit
                    {
                        Id = reader.GetGuid("Id"),
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                        Status = reader.GetStringNullable("Status"),
                        PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        ScheduleDate = reader.GetStringNullable("EventDate"),
                        VisitDate = reader.GetStringNullable("VisitDate")
                    })
                .AsList();
            }
            return scheduleList;

        }

        public List<VisitSummary> GetPayrollSummmaryLean(Guid agencyId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid)
        {
            var additionalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additionalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND pr.Status = @statusid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }

            if (disciplineTasksNotIncluded != null && disciplineTasksNotIncluded.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask NOT IN ( {0} ) ", disciplineTasksNotIncluded.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (IsVisitPaid.IsEqual("true"))
            {
                additionalScript += " AND st.IsVisitPaid = 1 ";
            }
            else if (IsVisitPaid.IsEqual("false"))
            {
                additionalScript += " AND st.IsVisitPaid = 0 ";
            }

            var script = string.Format(@"SELECT 
                         st.UserId as UserId ,
                         COUNT(*) as VisitCount  
                            FROM 
                                scheduletasks st
                                    INNER JOIN patients pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.UserId != @notuserid AND
                                            st.IsActive = 1  AND
                                            st.IsDischarged = 0 AND 
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND
                                            pr.IsDeprecated = 0 AND 
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            DATE(st.EventDate) between  DATE(st.StartDate) and DATE(st.EndDate) {0} 
                                                GROUP BY st.UserId", additionalScript);

            var list = new List<VisitSummary>();
            using (var cmd = new FluentCommand<VisitSummary>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .AddGuid("notuserid", Guid.Empty)
                .SetMap(reader => new VisitSummary
                {
                    UserId = reader.GetGuid("UserId"),
                    VisitCount = reader.GetInt("VisitCount")
                })
                .AsList();
            }
            return list;
        }

        public List<UserVisit> GetPayrollSummmaryVisits(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid)
        {
            var additionalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additionalScript += " AND pr.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND pr.Status = @statusid ";
            }

            if (!userId.IsEmpty())
            {
                additionalScript += " AND st.UserId = @userid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND st.IsMissedVisit = 0 ";
            }

            if (disciplineTasksNotIncluded != null && disciplineTasksNotIncluded.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask NOT IN ( {0} ) ", disciplineTasksNotIncluded.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (IsVisitPaid.IsEqual("true"))
            {
                additionalScript += " AND st.IsVisitPaid = 1 ";
            }
            else if (IsVisitPaid.IsEqual("false"))
            {
                additionalScript += " AND st.IsVisitPaid = 0 ";
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.IsMissedVisit as IsMissedVisit , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.Surcharge as Surcharge ,
                        st.AssociatedMileage as AssociatedMileage ,
                        st.IsVisitPaid as IsVisitPaid ,
                        st.PrimaryInsurance as PrimaryInsurance,
                        pr.FirstName as FirstName , 
                        pr.LastName as LastName ,
                        pr.PatientIdNumber as PatientIdNumber 
                            FROM 
                                scheduletasks st
                                    LEFT JOIN patients pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.IsActive = 1  AND 
                                            st.IsDischarged = 0 AND
                                            st.IsDeprecated = 0 AND 
                                            st.DisciplineTask > 0 AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(st.EventDate) between  DATE(st.StartDate) and DATE(st.EndDate) {0} 
                                                ORDER BY DATE(st.EventDate) DESC ", additionalScript);

            var list = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .AddGuid("userid", userId)
                .SetMap(reader => new UserVisit
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    ScheduleDate = reader.GetStringNullable("EventDate"),
                    VisitDate = reader.GetStringNullable("VisitDate"),
                    Status = reader.GetStringNullable("Status"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Surcharge = reader.GetStringNullable("Surcharge"),
                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    PrimaryInsurance = reader.GetInt("PrimaryInsurance"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    VisitRate = "$0.00"
                })
                .AsList();
            }
            return list;
        }


        public List<ScheduleEvent> GetMissedVisitSchedulesLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND pr.Status IN (1,2)";
            }
            else
            {
                additonalScript += " AND pr.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND pr.AgencyLocationId = @branchId";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND st.Discipline IN ( {0} ) ", disciplineTasks.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EventDate as EventDate , 
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        pr.PatientIdNumber as PatientIdNumber ,
                        pr.LastName as LastName ,
                        pr.FirstName as FirstName 
                            FROM 
                                scheduletasks st
                                    INNER JOIN patients pr ON st.PatientId = pr.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND
                                            st.IsDeprecated = 0 AND
                                            st.IsMissedVisit = 1 AND 
                                            st.IsActive = 1 AND
                                            st.IsDischarged = 0 AND
                                            DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                            DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) {0} ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader =>
                 new ScheduleEvent
                    {
                        UserId = reader.GetGuid("UserId"),
                        DisciplineTask = reader.GetInt("DisciplineTask"),
                        Status = reader.GetInt("Status"),
                        PatientName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        TimeIn = reader.GetStringNullable("TimeIn"),
                        TimeOut = reader.GetStringNullable("TimeOut"),
                        EventDate = reader.GetDateTime("EventDate"),
                        VisitDate = reader.GetDateTime("VisitDate")
                    })
                .AsList();
            }
            return scheduleList;

        }
        
        //TODO:check for first billable status
        public ScheduleEvent FirstBillableEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var status = ScheduleStatusFactory.AllStatusButOrdersAndReports().Select(s => s.ToString()).ToArray();// new string[] { ((int)ScheduleStatus.NoteCompleted).ToString(), ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), ((int)ScheduleStatus.OasisExported).ToString(), ((int)ScheduleStatus.OasisCompletedNotExported).ToString() };
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.Discipline as Discipline  
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid  AND
                                        st.EpisodeId = @episodeid AND 
                                        st.IsDeprecated = 0 AND 
                                        st.IsBillable = 1 AND  
                                        st.IsMissedVisit = 0  AND
                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                        st.Status IN ( {0} ) 
                                            ORDER BY 
                                                 DATE(st.VisitDate) ASC LIMIT 1", status.Join(","));

            var schedule = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                schedule = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    Discipline = reader.GetInt("Discipline")
                })
                .AsSingle();
            }
            return schedule;
        }

        public bool IsFirstScheduledVisit(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id 
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid  AND
                                        st.EpisodeId = @episodeid AND 
                                        st.IsDeprecated = 0 AND 
                                        st.IsBillable = 1 AND  
                                        st.IsMissedVisit = 0  AND
                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  
                                            ORDER BY 
                                                 DATE(st.EventDate) ASC LIMIT 1");

            var schedule = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                schedule = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id")
                })
                .AsSingle();
            }

            if (schedule != null)
            {
               return schedule.Id == eventId;
            }
            else
            {
               return false;
            }
           
        }

        #endregion


        public List<PatientVisitNote> GetVisitNotesByDisciplineTaskWithStatus(Guid agencyId, Guid patientId, int[] scheduleStatus, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;

            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND st.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND st.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        pv.PhysicianId as PhysicianId , 
                        pv.NoteType as NoteType ,
                        pv.Note as NOte,
                        st.Status as Status ,
                        st.StartDate as StartDate,
                        st.StartDate as EndDate,
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate 
                            FROM 
                                 scheduletasks st
                                    INNER JOIN patientvisitnotes  pv  ON pv.Id = st.Id 
                                        WHERE 
                                            st.AgencyId = @agencyid AND 
                                            st.PatientId = @patientid AND 
                                            st.IsMissedVisit = 0  AND 
                                            st.IsDeprecated = 0 AND
                                            st.IsActive = 1 AND
                                            st.IsDischarged = 0 AND
                                            DATE(st.EventDate) between  DATE(st.StartDate) and DATE(st.EndDate) {0} ", additionalScript);

            var list = new List<PatientVisitNote>();
            using (var cmd = new FluentCommand<PatientVisitNote>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    NoteType = reader.GetStringNullable("NoteType"),
                    Status = reader.GetInt("Status"),
                    Note = reader.GetStringNullable("Note"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetTherapyExceptionScheduleEvents(Guid agencyId, Guid branchId, Guid patientId, DateTime fromDate, DateTime toDate, int statusOptional)
        {
            var disciplines = DisciplineFactory.TherapyDisciplines().Select(d => d.ToString()).ToArray();// new string[] { Disciplines.PT.ToString(), Disciplines.OT.ToString(), Disciplines.ST.ToString() };
            var patientOrStatusScript = string.Empty;
            if (patientId.IsEmpty())
            {
                if (statusOptional > 0)
                {
                    patientOrStatusScript = string.Format(" AND pr.Status = {0} AND pr.IsDeprecated = 0 ", statusOptional);
                }
                else
                {
                    patientOrStatusScript = " AND pr.Status IN (1,2) AND pr.IsDeprecated = 0 ";
                }
            }
            else
            {
                patientOrStatusScript = " AND pr.Id = @patientId ";

            }
            var script = string.Format(@"SELECT 
                                    st.Id as Id ,
                                    st.PatientId as PatientId ,
                                    st.EpisodeId as EpisodeId ,
                                    st.UserId as UserId ,
                                    st.DisciplineTask as DisciplineTask , 
                                    st.EventDate as EventDate ,
                                    st.VisitDate as VisitDate ,
                                    st.Status as Status ,
                                    st.Discipline as Discipline ,
                                    st.EndDate as EndDate,
                                    st.StartDate as StartDate,
                                    pr.FirstName as FirstName, 
                                    pr.LastName as LastName, 
                                    pr.PatientIdNumber as PatientIdNumber 
                                         FROM scheduletasks st  
                                                    INNER JOIN patients pr ON st.PatientId = pr.Id    
                                                          WHERE 
                                                                st.AgencyId = @agencyid {0} {1} AND
                                                                st.IsActive = 1 AND 
                                                                st.IsDischarged = 0 AND 
                                                                DATE(st.StartDate) between DATE(@startdate) and DATE(@enddate) AND 
                                                                DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                                                st.IsDeprecated = 0  AND
                                                                st.IsBillable = 1 AND
                                                                st.IsMissedVisit = 0 AND 
                                                                st.Discipline IN ( {2} )",

                                                                                         !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchId" : string.Empty,
                                                                                         patientOrStatusScript,
                                                                                         disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startdate", fromDate)
                .AddDateTime("enddate", toDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    Discipline = reader.GetInt("Discipline"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;
        }

        public ScheduleEvent GetEpisodeAssessmentScheduleEvent(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, bool isNoneOASISIncluded)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.UserId as UserId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.IsBillable as IsBillable ,
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EndDate as EndDate,
                        st.StartDate as StartDate 
                            FROM
                                scheduletasks st
                                    WHERE
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid AND
                                        ((st.EpisodeId = @episodeid AND DATE(st.EventDate) between DATE(st.StartDate) and DATE(@maxstartofcaredate) AND st.DisciplineTask IN(@startofcares)) OR (DATE(st.EndDate) = DATE(@previousepisodeenddate) AND DATE(st.EventDate) between DATE(@minpreviousepisodeeventdate) and DATE(@previousepisodeenddate) AND st.DisciplineTask IN( @rocorrecert))) AND
                                        st.IsDeprecated = 0 AND
                                        st.IsMissedVisit = 0  AND
                                        st.IsActive = 1  AND 
                                        st.IsDischarged = 0  AND
                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  
                                            ORDER BY DATE(st.EventDate) DESC LIMIT 1 ");


            var scheduleEvent = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("maxstartofcaredate",startDate.AddDays(5))
                .AddDateTime("minpreviousepisodeeventdate", startDate.AddDays(-6))
                .AddDateTime("previousepisodeenddate", startDate.AddDays(-1))
                .AddString("startofcares", DisciplineTaskFactory.SOCDisciplineTasks(isNoneOASISIncluded).ToCommaSeperatedNoQuote())
                .AddString("rocorrecert", DisciplineTaskFactory.LastFiveDayAssessments(isNoneOASISIncluded).ToCommaSeperatedNoQuote())
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public ScheduleEvent LastRecertOrROCScheduleEvent(Guid agencyId, Guid patientId, Guid episodeId, DateTime endDate, bool isNoneOASISIncluded)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.DisciplineTask as DisciplineTask , 
                        st.EventDate as EventDate ,
                        st.VisitDate as VisitDate ,
                        st.Status as Status ,
                        st.IsBillable as IsBillable ,
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut ,
                        st.EndDate as EndDate,
                        st.StartDate as StartDate 
                            FROM
                                scheduletasks st
                                    WHERE
                                        st.AgencyId = @agencyid AND 
                                        st.PatientId = @patientid AND
                                        st.EpisodeId = @episodeid  AND 
                                        DATE(st.EventDate) between DATE(@minpreviousepisodeeventdate) and DATE(@previousepisodeenddate) AND 
                                        st.DisciplineTask IN( @rocorrecert)) AND
                                        st.IsDeprecated = 0 AND
                                        st.IsMissedVisit = 0  AND
                                        st.IsActive = 1  AND 
                                        st.IsDischarged = 0  AND
                                        DATE(st.EventDate) between DATE(@startdate) and DATE(@enddate)  
                                            ORDER BY DATE(st.EventDate) DESC LIMIT 1 ");


            var scheduleEvent = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection(this.connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("minpreviousepisodeeventdate", endDate.AddDays(-5))
                .AddDateTime("previousepisodeenddate", endDate)
                .AddString("rocorrecert", DisciplineTaskFactory.LastFiveDayAssessments(isNoneOASISIncluded).ToCommaSeperatedNoQuote())
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = patientId,
                    EpisodeId = episodeId,
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsSingle();
            }
            return scheduleEvent;
        }


        public List<ScheduleEvent> GetPastDueRecertsLeanByDateRange(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, int payor)
        {
            var insurance = string.Empty;
            var list = new List<ScheduleEvent>();
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    if (payor > 0)
                    {
                        insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance = {0} || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance = {0} ||  pr.TertiaryInsurance >= 1000 )", payor);
                    }
                    else
                    {
                        insurance = "AND ( pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance >= 1000 )";
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.SecondaryInsurance = {0} || pr.TertiaryInsurance = {0} )", insuranceId);
            }

            var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();
            // new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
            var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();
            // new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };

            var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray();
            // new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
            var recertAndROCStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();
            //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

            var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); 
            //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
            var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); 
            //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };


            var script1 = string.Format(@"SELECT 
                    '{5}' as Id ,
                    pe.PatientId as PatientId ,
                    pe.Id as EpisodeId ,
                    '{5}' as UserId ,
                    {7} as DisciplineTask , 
                    '{6}' as VisitDate ,
                    '{6}' as EventDate ,
                    {8} as Status ,
                    pe.StartDate as StartDate, 
                    pe.EndDate as EndDate , 
                    pr.FirstName as FirstName ,
                    pr.LastName as LastName ,
                    pr.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes pe
                                INNER JOIN
                                    patientprofiles pr ON pe.PatientId = pr.Id  
                                        WHERE 
                                               pr.AgencyId = @agencyid  AND
                                               pr.Status = 1 {0} {1} AND 
                                               pr.IsDeprecated = 0 AND 
                                               pe.IsActive = 1 AND 
                                               pe.IsDischarged = 0 AND 
                                               DATEDIFF( pe.EndDate , pe.StartDate ) = 59 AND
                                               DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                               Not Exists ( SELECT * from scheduletasks st WHERE st.PatientId = pr.Id AND st.EpisodeId = pe.Id AND DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND st.DisciplineTask > 0 AND st.IsDeprecated = 0 AND  st.IsMissedVisit = 0 AND ( {2} OR  {3} OR {4}) )",
                                                                                   insurance,
                                                                                   !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty,
                                                                                   string.Format(" ( st.DisciplineTask IN ( {0} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   Guid.Empty.ToString(),
                                                                                   DateTime.MinValue.ToString("yyyy-MM-dd"),
                                                                                   (int)DisciplineTasks.OASISCRecertification,
                                                                                   0);
            var script2 = string.Format(@"SELECT 
                    st.Id as Id ,
                    pr.Id as PatientId ,
                    pr.Id as EpisodeId ,
                    st.UserId as UserId ,
                    st.DisciplineTask as DisciplineTask , 
                    st.EventDate as EventDate ,
                    st.VisitDate as VisitDate ,
                    st.Status as Status ,
                    pe.StartDate as StartDate, 
                    pe.EndDate as EndDate , 
                    pr.FirstName as FirstName ,
                    pr.LastName as LastName ,
                    pr.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes pe
                                INNER JOIN patients pr ON pe.PatientId = pr.Id  
                                LEFT JOIN scheduletasks st ON st.EpisodeId = pe.Id 
                                    WHERE 
                                           pr.AgencyId = @agencyid  AND
                                           pr.Status = 1 {0} {1} AND 
                                           pr.IsDeprecated = 0 AND 
                                           pe.IsActive = 1 AND 
                                           pe.IsDischarged = 0 AND 
                                           DATE(pe.EndDate) < DATE(curdate()) AND 
                                           DATEDIFF( pe.EndDate , pe.StartDate ) = 59 AND
                                           DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND 
                                           st.DisciplineTask > 0 AND
                                           st.IsDeprecated = 0 AND 
                                           st.IsMissedVisit = 0 AND
                                           ( {2} OR  {3} OR {4} )",
                                                           insurance,
                                                           !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty,
                                                           string.Format(" ( st.DisciplineTask IN ( {0} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                           string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                           string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")));

            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1} ORDER BY EndDate DESC", script1, script2)))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                 .AddGuid("branchid", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }
        //TODO: insurance and payor
        public List<ScheduleEvent> GetUpcomingRecertsLean(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, int payor)
        {
            var insurance = string.Empty;
            var list = new List<ScheduleEvent>();
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    if (payor > 0)
                    {
                        insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance = {0} || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance = {0} ||  pr.TertiaryInsurance >= 1000 )", payor);
                    }
                    else
                    {
                        insurance = "AND ( pr.PrimaryInsurance >= 1000 || pr.SecondaryInsurance >= 1000 || pr.TertiaryInsurance >= 1000 )";
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( pr.PrimaryInsurance = {0} || pr.SecondaryInsurance = {0} || pr.TertiaryInsurance = {0} )", insuranceId);
            }

            //var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks(true).ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
            //var dischargeStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };

            var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments(true).ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
            //var recertAndROCStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

           // var transferDisciplineTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionOfCare, (int)DisciplineTasks.OASISCResumptionOfCarePT, (int)DisciplineTasks.OASISCResumptionOfCareOT, (int)DisciplineTasks.OASISBResumptionOfCare };
            //var transferStatus = ScheduleStatusFactory.OASISCompleted(true).ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };




//            var script1 = string.Format(@"SELECT 
//                    '{5}' as Id ,
//                    pe.PatientId as PatientId ,
//                    pe.Id as EpisodeId ,
//                    '{5}' as UserId ,
//                    {7} as DisciplineTask , 
//                    '{6}' as EventDate ,
//                    '{6}' as VisitDate ,
//                    {8} as Status ,
//                    pe.StartDate as StartDate, 
//                    pe.EndDate as EndDate , 
//                    pr.FirstName as FirstName ,
//                    pr.LastName as LastName ,
//                    pr.PatientIdNumber as PatientIdNumber
//                        FROM patientepisodes pe
//                                INNER JOIN patientprofiles pr ON pe.PatientId = pr.Id  
//                                    WHERE 
//                                           pr.AgencyId = @agencyid  AND
//                                           pr.Status = 1 {0} {1} AND 
//                                           pr.IsDeprecated = 0 AND 
//                                           pe.IsActive = 1 AND 
//                                           pe.IsDischarged = 0 AND 
//                                           DATEDIFF( pe.EndDate , pe.StartDate ) = 59 AND
//                                           DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) AND
//                                           Not Exists ( SELECT * from scheduletasks st WHERE st.PatientId = pr.Id AND st.EpisodeId = pe.Id AND DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND st.DisciplineTask > 0 AND st.IsDeprecated = 0 AND  st.IsMissedVisit = 0 AND ( {2} OR  {3} OR {4} ) )",
//                                                                                     insurance,
//                                                                                     !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty,
//                                                                                     string.Format(" ( st.DisciplineTask IN ( {0} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
//                                                                                     string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
//                                                                                     string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")),
//                                                                                     Guid.Empty.ToString(),
//                                                                                     DateTime.MinValue.ToString("yyyy-MM-dd"),
//                                                                                     (int)DisciplineTasks.OASISCRecertification,
//                                                                                     0);
//            var script2 = string.Format(@"SELECT 
//                    st.Id as Id ,
//                    st.PatientId as PatientId ,
//                    st.EpisodeId as EpisodeId ,
//                    st.UserId as UserId ,
//                    st.DisciplineTask as DisciplineTask , 
//                    st.EventDate as EventDate ,
//                    st.VisitDate as VisitDate ,
//                    st.Status as Status ,
//                    pe.StartDate as StartDate, 
//                    pe.EndDate as EndDate , 
//                    pr.FirstName as FirstName ,
//                    pr.LastName as LastName ,
//                    pr.PatientIdNumber as PatientIdNumber
//                        FROM patientepisodes pe
//                                INNER JOIN patientprofiles pr ON pe.PatientId = pr.Id  
//                                LEFT JOIN scheduletasks st ON st.EpisodeId = pe.Id 
//                                    WHERE 
//                                           pr.AgencyId = @agencyid  AND
//                                           pr.Status = 1 {0} {1} AND 
//                                           pr.IsDeprecated = 0 AND 
//                                           pe.IsActive = 1 AND 
//                                           pe.IsDischarged = 0 AND 
//                                           DATEDIFF( pe.EndDate , pe.StartDate ) = 59 AND
//                                           DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) AND
//                                           DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND 
//                                           st.DisciplineTask > 0 AND
//                                           st.IsDeprecated = 0 AND 
//                                           st.IsMissedVisit = 0 AND
//                                           ( {2} OR  {3} OR {4} ) ",
//                                                            insurance,
//                                                            !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty,
//                                                            string.Format(" ( st.DisciplineTask IN ( {0} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
//                                                            string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
//                                                            string.Format(" ( st.DisciplineTask IN ( {0} ) AND st.Status  IN ( {1} ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ", transferDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), transferStatus.Select(d => d.ToString()).ToArray().Join(",")));
             //COALESCE(IFNULL(pe.,0),IFNULL(,0),IFNULL(,0),0 ) as PrimaryInsurance,
            var script3 = string.Format(@"SELECT 
                    st.Id as Id ,
                    st.PatientId as PatientId ,
                    st.EpisodeId as EpisodeId ,
                    st.UserId as UserId ,
                    st.DisciplineTask as DisciplineTask , 
                    st.EventDate as EventDate ,
                    st.VisitDate as VisitDate ,
                    st.Status as Status ,
                    pe.StartDate as StartDate, 
                    pe.EndDate as EndDate , 
                   
                    pr.FirstName as FirstName ,
                    pr.LastName as LastName ,
                    pr.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes pe
                                INNER JOIN patients pr ON pe.PatientId = pr.Id  
                                LEFT JOIN scheduletasks st ON st.EpisodeId = pe.Id 
                                    WHERE 
                                           pr.AgencyId = @agencyid  AND
                                           pr.Status = 1 {0} {1} AND 
                                           pr.IsDeprecated = 0 AND 
                                           pe.IsActive = 1 AND 
                                           pe.IsDischarged = 0 AND 
                                           DATEDIFF( pe.EndDate , pe.StartDate ) = 59 AND
                                           DATE(pe.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           DATE(st.EventDate) between DATE(pe.StartDate) and DATE(pe.EndDate) AND 
                                           st.DisciplineTask > 0 AND
                                           st.IsDeprecated = 0 AND 
                                           st.IsMissedVisit = 0 AND
                                           ( st.DisciplineTask IN ( @disciplinetasks ) AND  DATE(st.EventDate) between DATE(DATE_SUB(pe.EndDate, INTERVAL 5 DAY)) and DATE(pe.EndDate) ) ",
                                                            insurance,
                                                            !branchId.IsEmpty() ? " AND pr.AgencyLocationId = @branchid " : string.Empty);
            
            
            using (var cmd = new FluentCommand<ScheduleEvent>(script3))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchid", branchId)
                .AddString("disciplinetasks",recertAndROCDisciplineTasks.ToCommaSeperatedNoQuote())
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetInt("Status"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                    //,
                    //PrimaryInsurance = reader.GetInt("PrimaryInsurance")
                })
                .AsList();
            }
            return list;
        }


        public List<ScheduleEvent> GetUserScheduleForSpecificDate(Guid agencyId, Guid userid, DateTime eventDate)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.EventDate,
                        st.VisitDate
                        st.Discipline as Discipline ,
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate,
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut 
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.UserId = @userid AND
                                        st.EventDate = @eventdate
                                        st.IsDeprecated = 0 AND
                                        st.IsMissedVisit = 0  AND
                                        st.IsActive = 1  AND 
                                        st.IsDischarged = 0  AND
                                        DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                        st.DisciplineTask > 0 ");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userid)
                .AddDateTime("eventdate", eventDate)
                .SetMap((reader) =>

                   new ScheduleEvent
                   {
                       Id = reader.GetGuid("Id"),
                       PatientId = reader.GetGuid("PatientId"),
                       EpisodeId = reader.GetGuid("EpisodeId"),
                       DisciplineTask = reader.GetInt("DisciplineTask"),
                       Status = reader.GetInt("Status"),
                       EventDate = reader.GetDateTime("EventDate"),
                       VisitDate = reader.GetDateTime("VisitDate"),
                       StartDate = reader.GetDateTime("StartDate"),
                       EndDate = reader.GetDateTime("EndDate"),
                       TimeIn = reader.GetStringNullable("TimeIn"),
                       TimeOut = reader.GetStringNullable("TimeOut"),

                   }
                )
                .AsList();
            }
            return list;
        }


        public List<ScheduleEvent> GetEpisodeScheduleForSpecificDate(Guid agencyId, Guid patientId, Guid episodeId, DateTime eventDate)
        {
            var script = string.Format(@"SELECT 
                        st.Id as Id ,
                        st.PatientId as PatientId ,
                        st.EpisodeId as EpisodeId ,
                        st.DisciplineTask as DisciplineTask , 
                        st.Status as Status ,
                        st.EventDate,
                        st.VisitDate,
                        st.Discipline as Discipline ,
                        st.StartDate as StartDate ,
                        st.EndDate as EndDate,
                        st.TimeIn as TimeIn , 
                        st.TimeOut as TimeOut 
                            FROM 
                                scheduletasks st
                                    WHERE 
                                        st.AgencyId = @agencyid AND
                                        st.PatientId = @patientid AND
                                        st.EpisodeId = @episodeid AND
                                        st.EventDate = @eventdate AND
                                        st.IsDeprecated = 0 AND
                                        st.IsMissedVisit = 0  AND
                                        st.IsActive = 1  AND 
                                        st.IsDischarged = 0  AND
                                        DATE(st.EventDate) between DATE(st.StartDate) and DATE(st.EndDate) AND
                                        st.DisciplineTask > 0 ");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection(connectionStringName)
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("eventdate", eventDate)
                .SetMap((reader) =>

                   new ScheduleEvent
                   {
                       Id = reader.GetGuid("Id"),
                       PatientId = reader.GetGuid("PatientId"),
                       EpisodeId = reader.GetGuid("EpisodeId"),
                       DisciplineTask = reader.GetInt("DisciplineTask"),
                       Status = reader.GetInt("Status"),
                       EventDate = reader.GetDateTime("EventDate"),
                       VisitDate = reader.GetDateTime("VisitDate"),
                       StartDate = reader.GetDateTime("StartDate"),
                       EndDate = reader.GetDateTime("EndDate"),
                       TimeIn = reader.GetStringNullable("TimeIn"),
                       TimeOut = reader.GetStringNullable("TimeOut"),

                   }
                )
                .AsList();
            }
            return list;
        }


        public bool CheckTimeOverlap(ScheduleEvent schedule, out string message)
        {
            message = string.Empty;
            bool result = false;
            Guid eventId = schedule.Id;
            Guid episodeId = schedule.EpisodeId;
            Guid patientId = schedule.PatientId;
            var timeIn = schedule.TimeIn;
            var timeOut = schedule.TimeOut;
            if (timeIn.IsNotNullOrEmpty() && timeOut.IsNotNullOrEmpty())
            {
               // var currentEpisode = patientRepository.GetEpisodeOnly(schedule.AgencyId, episodeId, patientId);
                //var scheduleEventlist =  currentEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                //var scheduleEvent =  scheduleEventlist.First(e => e.EventId == eventId && e.IsDeprecated == false);
                var sameDateScheduleEventList = GetEpisodeScheduleForSpecificDate(schedule.AgencyId, patientId, episodeId, schedule.EventDate);//scheduleEventlist.Where(e => e.EventDate == scheduleEvent.EventDate && e.EventId != eventId && e.IsDeprecated == false);
                var overLapList = new List<ScheduleEvent>();
                foreach (ScheduleEvent s in sameDateScheduleEventList)
                {
                    if (s.TimeIn.IsNotNullOrEmpty() && s.TimeOut.IsNotNullOrEmpty())
                    {
                        if ((s.TimeIn.ToDateTime() < timeIn.ToDateTime() && s.TimeOut.ToDateTime() > timeIn.ToDateTime())
                            || (s.TimeIn.ToDateTime() < timeOut.ToDateTime() && s.TimeOut.ToDateTime() > timeOut.ToDateTime())
                            || (s.TimeIn.ToDateTime() >= timeIn.ToDateTime() && s.TimeOut.ToDateTime() <= timeOut.ToDateTime()))
                        {
                            message += "</br><strong>Warning:</strong>The Time IN/OUT entered conflicts with " + s.DisciplineTaskName + " on " + s.EventDate;
                            result = true;
                            return result;
                        }
                    }
                }
            }
            return result;
        }


       

    }
}
