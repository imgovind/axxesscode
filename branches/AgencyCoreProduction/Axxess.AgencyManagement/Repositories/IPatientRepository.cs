﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    using Enums;
    using Domain;
    using Axxess.Core.Enums;

    public interface IPatientRepository
    {
        bool Add(Patient patient, out Patient patientOut);
        bool Edit(Patient patient, out Patient patientOut);
        bool PatientAdmissionEdit(Patient patient);
        bool PatientAdmissionAdd(Patient patient);
        bool Update(Patient patient, out Patient patientOut);
        bool Update(Patient patient);
        bool RemovePatient(Guid agencyId, Guid id);
        bool TogglePatient(Guid agencyId, Guid id, bool isDeprecated);
        bool SetStatus(Guid agencyId, Guid patient, PatientStatus status);
        bool AddEmergencyContact(PatientEmergencyContact emergencyContact);
        bool AddCPO(CarePlanOversight cpo);
        //bool DeleteCPO(Guid id, Guid agencyId);
        //bool UpdateCPO(CarePlanOversight cpo);
        Patient Get(Guid Id, Guid agencyId);
        Patient GetPatientOnly(Guid id, Guid agencyId);
        //T GetPatient <T>(Guid patientId, Guid agencyId);
        PatientProfileLean GetPatientPrintProfile(Guid agencyId, Guid patientId);
        PatientProfileLean GetPatientForNoteEditOnly(Guid agencyId, Guid patientId);
        PatientProfileLean GetPatientPrintProfileWithAddress(Guid agencyId, Guid patientId);
        PatientProfileLean GetPatientPrintProfileWithPharmacies(Guid agencyId, Guid patientId);
        IList<Birthday> GetPatientBirthdays(Guid agencyId, Guid branchId, int month);
        List<AddressBookEntry> GetPatientAddressListing(Guid agencyId, Guid branchId, int status);
        PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid admissionId);
        PatientAdmissionDate GetPatientLatestAdmissionDate(Guid agencyId, Guid patientId, DateTime date);
        string GetPatientNameById(Guid patientId, Guid agencyId);
        string GetBranchName(Guid agencyId, Guid locationId);
        List<BirthdayWidget> GetCurrentPatientBirthdays(Guid agencyId);
        //IList<Patient> GetAllByAgencyId(Guid agencyId);
        IList<PatientData> All(Guid agencyId, List<Guid> branchIds, int status);
        IList<PatientData> AllDeleted(Guid agencyId, List<Guid> branchIds);
        //IList<Patient> All();
        List<PendingPatient> GetPendingByAgencyId(Guid agencyId,List<Guid> branchIds, PatientStatus status);
        List<NonAdmit> GetPatientNonAdmit(Guid agencyId, List<Guid> branchIds, PatientStatus status);
        List<PatientHospitalizationData> GetHospitalizedPatients(Guid agencyId);
        //IList<Patient> FindPatientOnly(int statusId, Guid agencyId);
        IList<Patient> Find(int statusId, Guid branchCode, Guid agencyId);
        List<PatientRoster> GetPatientByResponsiableEmployee(Guid agencyId, List<Guid> branchIds, List<int> statusIds, Guid userId);
        //IList<Patient> FindByCaseManager(Guid agencyId, Guid branchCode, int statusId, Guid caseManagerId);
        //List<Patient> GetPatientByPhysician(Guid physicianId);
        List<ReferralInfo> GetReferralInfos(Guid agencyId, int status, DateTime startDate, DateTime endDate);
       // int GetPatientStatusCount(Guid agencyId, int statusId);

        List<PatientSelection> GetPatientSelection(Guid agencyId, List<Guid> branchIds, List<int> statusIds, int paymentSourceId, string name);
        List<PatientSelection> GetPatientSelection(Guid agencyId, List<Guid> branchIds, int statusId, int paymentSourceId, string name, Guid userId);
        List<PatientSelection> GetAuditorPatientSelection(Guid agencyId, List<Guid> branchIds, int statusId, int paymentSourceId, string name, Guid userId);
        List<PatientSelection> GetPatientSelectionMedicare(Guid agencyId, List<Guid> branchIds, int statusId, int paymentSourceId, string name);
        List<PatientSelection> GetPatientSelectionAllInsurance(Guid agencyId, List<Guid> branchIds, int statusId, string name, int insurnaceId, List<int> payorIdsIfInsuranceIdZero);
        List<PatientSelection> GetPatientEpisodeDataForList(Guid agencyId, List<Guid> branchIds, Guid userId, List<int> statusIds, string typeOfPayor, int payorValue);
       // List<PatientSelection> GetPatientsWithUserAccess(Guid userId, Guid agencyId, Guid branchId, int statusId, int paymentSourceId, string name);
       // List<PatientEpisode> GetPatientScheduledEvents(Guid agencyId, Guid patientId);
        //List<PatientEpisode> GetPatientScheduledEventsByRange(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetEpisodeData(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetPatientEpisodeDataByBranch(Guid agencyId, Guid locationId, DateTime startDate, DateTime endDate);
        //PatientEpisodeData GetPatientScheduledEvents(Guid agencyId, Guid episodeId, Guid patientId);
        List<Patient> GetPatientByAgencyPhysician(Guid agencyId, Guid loginId);
        PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid emergencyContactID,Guid agencyId);
        IList<PatientEmergencyContact> GetEmergencyContacts(Guid agencyId, Guid patientId);
        PatientEmergencyContact GetFirstEmergencyContactByPatient(Guid agencyId, Guid patientId);
        List<EmergencyContactInfo> GetEmergencyContactInfos(Guid agencyId, Guid branchId, int status);
        List<PatientEvacuation> GetEmergencyPreparednessInfos(Guid agencyId, Guid branchId, int status);
        bool EditEmergencyContact(Guid agencyId, PatientEmergencyContact emergencyContact);
        //List<ReturnComment> GetReturnCommentsByEventIds(Guid agencyId, List<Guid> eventIds);
        bool DeleteEmergencyContacts(Guid agencyId, Guid patientId);
        bool DeleteEmergencyContact(Guid agencyId,Guid Id, Guid patientId);

        bool DeletePhysicianContact(Guid Id, Guid patientId);
        bool SetPrimaryEmergencyContact(Guid agencyId, Guid patientId, Guid emergencyContactId);
        bool AddOrder(PhysicianOrder order);
        bool AddCommunicationNote(CommunicationNote communicationNote);
        CommunicationNote GetCommunicationNote(Guid Id, Guid patientId , Guid agencyId);
        bool EditCommunicationNote(CommunicationNote communicationNote);
        bool UpdateCommunicationNoteModal(CommunicationNote communicationNote);
        bool ToggleCommunicationNoteDeleteStatus(Guid agencyId, Guid Id, Guid patientId, bool isDeprecated);
        List<CommunicationNote> GetCommunicationNotes(Guid agencyId, Guid patientId);
        List<CommunicationNote> GetCommunicationNotes(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate);
        List<CommunicationNote> GetAllCommunicationNotes();
        //bool ReassignCommunicationNoteUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        //PatientNote GetNote(Guid patientId);
        //Guid Note(Guid patientId, string patientNote);
        //string GetReturnReason(Guid eventId, Guid episodeId, Guid patientId, Guid agencyId);
        
        //bool AddReturnReason(Guid eventId, Guid episodeId, Guid patientId, Guid agencyId, string reason, string user);
        //PatientEpisode GetCurrentEpisode(Guid agencyId, Guid patientId);
        //PatientEpisode GetCurrentEpisodeOrLastEpisodeWithScheduleLean(Guid agencyId, Guid patientId, DateTime date);
        //PatientEpisodeData GetCurrentEpisodeLean(Guid agencyId, Guid patientId);
        //PatientEpisode GetCurrentEpisodeOnly(Guid agencyId, Guid patientId);
        //NewEpisodeData GetLastEpisode(Guid agencyId, Guid patientId);
        long GetNextOrderNumber();
        AgencyPhysician GetPatientReferralPhysician(Guid agencyId, Guid patientId);
        //DateRange GetCurrentEpisodeDate(Guid agencyId, Guid patientId);
        //DateRange GetNextEpisode(Guid agencyId, Guid patientId);
        //DateRange GetPreviousEpisode(Guid agencyId, Guid patientId);
        
        //List<ScheduleEvent> GetScheduledEventsByEmployeeAssigned(Guid agencyId, Guid patientId, Guid employeeId, DateTime startDate, DateTime endDate);
        //PatientEpisode GetEpisodeById(Guid agencyId, Guid episodeId, Guid patientId);
        //PatientEpisode GetEpisodeByIdWithSOC(Guid agencyId, Guid episodeId, Guid patientId);
        //List<ScheduleEvent> GetEpisodeSchedulesByEmployee(Guid agencyId, Guid patientId, Guid episodeId, Guid employeeId);
        //PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date, string discipline);
        //PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId, string discipline);
       // PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId);
        //PatientEpisode GetEpisodeLeanByEndDate(Guid agencyId, Guid patientId, DateTime date);
        //PatientEpisode GetEpisodeLeanWithOutSchedule(Guid agencyId, Guid patientId, Guid episodeId);
        //PatientEpisode GetEpisodeLeanWithSchedule(Guid agencyId, Guid patientId, Guid episodeId);
        //PatientEpisode GetEpisodeLeanByStartDate(Guid agencyId, Guid patientId, DateTime date);
        //PatientEpisode GetEpisodeOnlyWithPreviousAndAfter(Guid agencyId, Guid episodeId, Guid patientId);
      
        PatientEpisode GetEpisodeOnly(Guid agencyId, Guid episodeId, Guid patientId);
        //List<PatientEpisode> GetPatientAllEpisodes(Guid agencyId, Guid patientId);
        //List<PatientEpisode> GetPatientActiveEpisodes(Guid agencyId, Guid patientId);
       
       
        //List<RecertEvent> GetPastDueRecertsLean(Guid agencyId);
        //List<RecertEvent> GetPastDueRecertsLeanByDateRange(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate);
       // List<RecertEvent> GetPastDueRecertsWidgetLean(Guid agencyId);
        //List<RecertEvent> GetUpcomingRecertsLean(Guid agencyId);
        //List<RecertEvent> GetUpcomingRecertsLean(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate);
        //List<RecertEvent> GetUpcomingRecertsWidgetLean(Guid agencyId);
        //List<PatientEpisodeData> GetPatientEpisodeDataSchduleWidget(Guid agencyId, DateTime startDate, DateTime endDate);
    
        //List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status);
        //List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status, string orderIds, DateTime startDate, DateTime endDate);
        //List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, Guid branchId, string orderIds, DateTime startDate, DateTime endDate);
        //List<PhysicianOrder> GetPatientPhysicianOrders(Guid agencyId, Guid patientId, string orderIds);
        List<PhysicianOrder> GetAllPhysicianOrders();

        //List<ScheduleEvent> GetPlanOfCareOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int status);
        //List<PhysicianOrder> GetPendingPhysicianSignatureOrders(Guid agencyId, string orderIds, DateTime startDate, DateTime endDate);
        PhysicianOrder GetOrder(Guid id, Guid agencyId);
        PhysicianOrder GetOrder(Guid Id, Guid patientId, Guid agencyId);
        PhysicianOrder GetOrderOnly(Guid Id, Guid agencyId);
        PhysicianOrder GetOrderOnly(Guid Id, Guid patientId, Guid agencyId);
        bool ToggleOrder(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
       // bool ReassignOrdersUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        bool UpdateOrder(PhysicianOrder order);
        bool UpdateOrderModel(PhysicianOrder order);
        //bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived);
        bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived, DateTime dateSend);

        //bool AddEpisode(PatientEpisode patientEpisode);

        //void AddNewUserEvent(Guid agencyId, Guid patientId, UserEvent newUserEvent);
        //bool AddNewScheduleEvent(Guid agencyId, Guid patientId, Guid episodeId, ScheduleEvent newScheduledEvent);

       // bool UpdateEpisode(Guid agencyId, ScheduleEvent editEvent);
       // bool UpdateEpisode(Guid agencyId, Guid episodeId, Guid patientId, List<ScheduleEvent> newEvents);
        //bool UpdateScheduleEventsForIsBillable(Guid agencyId, List<ScheduleEvent> scheduleEvents);
        //bool Reassign(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
       // ScheduleEvent GetSchedule(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        //ScheduleEvent GetScheduleOnly(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        //bool DeleteScheduleEvent(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, int task);

        //bool UpdateEpisode(Guid agencyId, PatientEpisode episode); 
        //bool UpdateEpisode(PatientEpisode episode);
        //List<PatientEpisode> EpisodesToDischarge(Guid agencyId, Guid patientId, DateTime dischargeDate);
        
       
        //bool Activate(Guid agencyId, Guid patientId);
        //bool ActivateWithNewSOC(Guid agencyId, Guid patientId, DateTime startOfCareDate);
        MedicationProfileHistory GetMedicationProfileHistory(Guid Id, Guid AgencyId);
        
        bool AddNewMedicationHistory(MedicationProfileHistory medicationHistory);
        bool AddNewMedicationProfile(MedicationProfile medication);

        MedicationProfile InsertMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType);
        bool UpdateMedication(MedicationProfile medicationProfile);
        bool UpdateMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType);
      
        bool UpdateMedicationForDischarge(Guid MedId, Guid agencyId, Guid Id, DateTime DischargeDate);
        MedicationProfile DeleteMedication(Guid MedId, Guid agencyId, Medication medication);
        bool UpdateMedicationProfileHistory(MedicationProfileHistory medicationProfile);
        bool ISSignedMedicationAssocatiedToAssessmentExist(Guid agencyId, Guid patientId, Guid episodeId, Guid assessmentId);
        MedicationProfileHistory GetSignedMedicationProfileForPatientByEpisode(Guid patientId, Guid agencyId, Guid episodeId);
        IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId, Guid agencyId);
        List<MedicationProfileHistory> GetMedicationHistoryForPatientLean(Guid patientId, Guid agencyId);
        IList<MedicationProfileHistory> GetAllMedicationProfileHistory();

        bool DeleteMedicationProfile(Guid agencyId, Guid patientId, Guid Id);

        //bool DeleteEpisode(Guid agencyId, Patient patient, Guid episodeId);
        //bool DeleteEpisode(Guid agencyId, Guid patientId, Guid episodeId);
        MedicationProfile GetMedicationProfileByPatient(Guid PatientId, Guid AgencyId);
        MedicationProfile GetMedicationProfile(Guid Id, Guid AgencyId);
        bool SaveMedicationProfile(MedicationProfile medicationProfile);

        bool MarkVisitNoteAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated);
        bool MarkPOCAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated);
        //bool ReassignNotesUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        bool UpdateVisitNote(PatientVisitNote patientVisitNote);
        bool UpdateVisitNoteWound(PatientVisitNote patientVisitNote);
       
       // List<MissedVisit> GetMissedVisitsOnly(Guid agencyId, DateTime startDate, DateTime endDate);
        //List<MissedVisit> GetMissedVisitsByStatus(Guid agencyId, DateTime startDate, DateTime endDate, int status);

        bool AddVisitNote(PatientVisitNote patientVisitNote);
        bool AddNoteRelation(NoteRelation relation);
        PatientVisitNote GetVisitNote(Guid agencyId, Guid patientId, Guid noteId);
        NoteRelation GetPlanOfCareRelation(Guid parentId, Guid agencyId);
        PatientVisitNote GetVisitNote(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        PatientVisitNote GetVisitNoteByPlanOfCare(Guid pocId, Guid agencyId);
       // PatientVisitNote GetHHAPlanOfCareVisitNote(Guid episodeId, Guid patientId);
        //PatientVisitNote GetVisitNoteByType(Guid episodeId, Guid patientId, DisciplineTasks disciplineTask);
        //List<PatientVisitNote> GetEvalOrders(Guid agencyId, string evalIds);
        //List<PatientVisitNote> GetEvalOrders(Guid agencyId, List<int> status, string evalIds, DateTime startDate, DateTime endDate);

        bool AddPhoto(Guid patientId, Guid agencyId, Guid assetId, out Patient patientOut);
        bool AdmitPatient(PendingPatient patient, out Patient patientOut);
        bool UpdatePatientOnlyStatus(Guid agencyId, Guid patientId, int status);
        bool AddPatientNonAdmissionReason(PatientNonAdmitReason nonAdmitData);
        bool RemovePatientNonAdmitReason(Guid agencyId, Guid Id);
        bool AddPatientAdmissionDate(PatientAdmissionDate managedDate);
        bool UpdatePatientAdmissionDate(PatientAdmissionDate managedDate);
        bool UpdatePatientAdmissionDateModal(PatientAdmissionDate managedDate);
        PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id);
        IList<PatientAdmissionDate> GetPatientAdmissionDates(Guid agencyId, Guid patientId);
        bool DeletePatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id);
        bool DeprecatedPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id);
        bool DeletePatientAdmissionDates(Guid agencyId, Guid patientId);
        bool DeprecatedPatientAdmissionDates(Guid agencyId, Guid patientId);

        //ScheduleEvent FirstBillableEvent(Guid agencyId, Guid episodeId, Guid patientId);
       // bool IsFirstBillableVisit(Guid agencyId, Guid episodeId, Guid patientId);

        bool AddAuthorization(Authorization authorization);
        bool EditAuthorization(Authorization authorization);
        IList<Authorization> GetAuthorizations( Guid agencyId , Guid patientId);
        IList<Authorization> GetAuthorizationsByStatusAndDate(Guid agencyId, Guid patientId, string status, DateTime startDate, DateTime endDate);
        IList<Authorization> GetActiveAuthorizations(Guid agencyId, Guid patientId, string insuranceId, string status, DateTime startDate, DateTime endDate);
        Authorization GetAuthorization(Guid agencyId, Guid patientId, Guid Id);
        Authorization GetAuthorization(Guid agencyId, Guid Id);
        bool DeleteAuthorization(Guid agencyId, Guid patientId, Guid Id);

        bool IsPatientIdExist(Guid agencyId, string patientIdNumber);
        bool IsMedicareExist(Guid agencyId, string medicareNumber);
        bool IsMedicaidExist(Guid agencyId, string medicaidNumber);
        bool IsSSNExist(Guid agencyId, string ssn);
        bool IsPatientIdExistForEdit(Guid agencyId, Guid patientId, string patientIdNumber);
        bool IsMedicareExistForEdit(Guid agencyId, Guid patientId, string medicareNumber);
        bool IsMedicaidExistForEdit(Guid agencyId, Guid patientId, string medicaidNumber);
        bool IsSSNExistForEdit(Guid agencyId, Guid patientId, string ssn);
        bool IsPatientExist(Guid agencyId, Guid patientId);

        //List<PatientEpisodeData> GetEpisodeByBranch(Guid branchCode, Guid agencyId);
        List<PatientVisitNote> GetPreviousNotes(Guid patientId, Guid agencyId);
       // List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId);
        //List<PatientEpisodeData> GetAllPatientEpisodeData(Guid agencyId);
        List<PatientVisitNote> GetVisitNotesByDisciplineTask(Guid patientId, Guid agencyId, DisciplineTasks task);
        //List<PatientVisitNote> GetVisitNotesByDisciplineTaskWithStatus(Guid agencyId, Guid patientId, DisciplineTasks task, int status);
        bool AddFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter);
        FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid patientId, Guid agencyId);
        FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid agencyId);
        List<FaceToFaceEncounter> GetAllFaceToFaceEncounters();
        bool UpdateFaceToFaceEncounterForRequest(Guid agencyId, Guid orderId, int status, DateTime dateRequested);
        bool UpdateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter);
        bool RemoveFaceToFaceEncounter(Guid agencyId, Guid patientId, Guid Id);
        bool ToggleFaceToFaceEncounterDeleteStatus(Guid agencyId, Guid patientId, Guid Id, bool IsDeprecated);
        //bool ReassignFaceToFaceEncounterUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
       // List<FaceToFaceEncounter> GetFaceToFaceEncounterOrders(Guid agencyId, int status, string orderIds);
        //List<FaceToFaceEncounter> GetPatientFaceToFaceEncounterOrders(Guid agencyId, Guid patientId, string orderIds);
        //List<FaceToFaceEncounter> GetPendingSignatureFaceToFaceEncounterOrders(Guid agencyId, string orderIds);
       
        //List<ScheduleEvent> GetPhysicianOrderScheduleEvents(Guid agencyId, DateTime startDate, DateTime endDate, int status);
        //List<ScheduleEvent> GetOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> status);
        //List<ScheduleEvent> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<ScheduleEvent> GetPendingSignatureOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);

        //PatientEpisode GetPreviousEpisode(Guid agencyId, Guid patientId, DateTime startDate);
        //PatientEpisode GetPreviousEpisodeFluent(Guid agencyId, Guid patientId, DateTime startDate);

        
        //List<PatientEpisodeTherapyException> GetAllEpisodeAfterApril(Guid agencyId, Guid branchId, Guid patientId, DateTime startDate, DateTime endDate);
        List<PatientSocCertPeriod> PatientSocCertPeriods(Guid agencyId, string patientIds, DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientByPhysician(Guid agencyId, Guid agencyPhysicianId);
        List<PatientRoster> GetPatientByPhysician(Guid agencyId, Guid agencyPhysicianId, int status);
        //List<PatientRoster> GetPatientByResponsiableEmployee(Guid agencyId, Guid userId, Guid branchId, int status);
        List<PatientRoster> GetPatientByResponsiableByCaseManager(Guid agencyId, List<Guid> branchIds, List<int> statusIds, Guid caseManagerId);
        List<PatientRoster> GetPatientByInsurance(Guid agencyId, List<Guid> branchIds, int insuranceId, List<int> statusIds);
        List<PatientRoster> GetPatientByAdmissionMonthYear(Guid agencyId, List<Guid> branchIds, List<int> statusIds, int month, int year);
        List<PatientRoster> GetPatientByAdmissionMonthYearUnduplicated(Guid agencyId, Guid branchId, int status, int year);
        List<PatientRoster> GetPatientByAdmissionUnduplicatedByDateRange(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientByAdmissionUnduplicatedServiceByDateRange(Guid agencyId, Guid branchId, int status, DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientByAdmissionYear(Guid agencyId, Guid branchId, int status, int year);
        List<DischargePatient> GetDischargePatients(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientRoster(Guid agencyId, Guid branchId, int statusId, int insuranceId);
        List<PatientRoster> GetPatientRosterByDateRange(Guid agencyId, Guid branchId, int statusId, int insuranceId, DateTime startDate, DateTime endDate);
        List<PatientEpisode> GetSurveyCensesPatientEpisodes(Guid agencyId, string patientIds);
        List<PatientWithPhysicanInfo> GetPatientPhysicianInfos(Guid agencyId, Guid branchId, int statusId);
        List<SurveyCensus> GetSurveyCensesByStatus(Guid agencyId, Guid branchId, int statusId, int insuranceId);
        List<SurveyCensus> GetSurveyCensesByStatusByDateRange(Guid agencyId, Guid branchId, int statusId, int insuranceId, DateTime startDate, DateTime endDate);
        List<AdmissionEpisode> PatientAdmissonPeriods(Guid agencyId, Guid patientId);

        bool AddDeletedItem(DeletedItem deletedItem);
        bool UpdateDeletedItem(DeletedItem deletedItem);
        List<DeletedItem> GetDeletedItems(Guid agencyId, Guid patientId);
        DeletedItem GetDeletedItem(Guid agencyId, Guid episodeId, Guid patientId);

        List<MedicareEligibility> GetMedicareEligibilities(Guid agencyId, Guid patientId);
        MedicareEligibility GetMedicareEligibility(Guid agencyId, Guid patientId, Guid id);
        MedicareEligibility GetMedicareEligibility(Guid agencyId, Guid EpisodeId, Guid patientId, Guid id);
        bool UpdateMedicareEligibility(MedicareEligibility medicareEligibility);

        AllergyProfile GetAllergyProfileByPatient(Guid patientId, Guid agencyId);
        AllergyProfile GetAllergyProfile(Guid profileId, Guid agencyId);
        bool UpdateAllergyProfile(AllergyProfile allergyProfile);
        bool AddAllergyProfile(AllergyProfile allergyProfile);

        string LastPatientId(Guid agencyId);

        //List<PatientEpisodeData> GetEpisodeDatasBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, byte statusId);
       // List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, Guid branchId, byte statusId);
        //List<PatientEpisodeData> GetPatientEpisodeDataForSchedule(Guid agencyId, Guid patientId);
        //List<PatientEpisode> GetPatientEpisodeDataForSchedule(Guid agencyId, Guid branchId, int statusId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetPatientEpisodeDataByBranchAndPatient(Guid agencyId, Guid branchId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<PatientEpisodeData> GetPatientEpisodeData(Guid agencyId, Guid branchId, byte statusId, int insuranceId);

        //List<PatientSelection> GetUserPatients(Guid agencyId, Guid userId, byte statusId);
        //List<PatientSelection> GetUserPatients(Guid agencyId, Guid branchId, Guid userId, byte statusId);
        //List<PatientSelection> GetUserPatients(Guid agencyId, Guid branchId, Guid userId, byte statusId, int insuranceId);

      
        List<BirthdayWidget> GetUserBirthDayReport(Guid agencyId, List<Guid> branchIds, Guid userId, byte statusId, int month);

        bool AddHospitalizationLog(HospitalizationLog transferLog);
        bool UpdateHospitalizationLog(HospitalizationLog transferLog);
        HospitalizationLog GetHospitalizationLog(Guid agencyId, Guid patientId, Guid transferLogId);
        List<HospitalizationLog> GetHospitalizationLogs(Guid patientId, Guid agencyId);


        //List<PhysicianOrder> GetPhysicianOrdersByPhysician(List<Guid> physicianIdentifiers, int status);
        //List<PhysicianOrder> GetPhysicianOrdersByPhysicianAndDate(List<Guid> physicianIdentifiers, int status, DateTime startDate, DateTime endDate);
        //List<FaceToFaceEncounter> GetFaceToFaceEncountersByPhysician(List<Guid> physicianIdentifiers, int status);
        //List<FaceToFaceEncounter> GetFaceToFaceEncountersByPhysicianAndDate(List<Guid> physicianIdentifiers, int status, DateTime startDate, DateTime endDate);
       // List<PatientVisitNote> GetEvalOrdersByPhysician(List<Guid> physicianIdentifiers, int[] status);

        List<PatientAdmission> GetPatientAdmissionsByDateRange(Guid agencyId, Guid branchId,int StatusId, DateTime startDate, DateTime endDate);
        Dictionary<string, int> GetPatientAdmits(Guid agencyId, Guid branchId);

       // List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //List<PatientEpisode> GetEpisodesBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        //bool DeleteScheduleEvents(Guid agencyId, Guid episodeId, Guid patientId, List<Guid> eventsToBeDeleted, out List<ScheduleEvent> deletedEvents);

        bool DeleteReturnComments(Guid agencyId, int id);
        bool AddReturnComment(ReturnComment returnComment);
        bool UpdateReturnComment(ReturnComment returnComment);
        List<ReturnComment> GetReturnComments(Guid agencyId, Guid episodeId, Guid eventId);
        ReturnComment GetReturnComment(Guid agencyId, int id);
        //List<MissedVisit> GetMissedVisitsByIds(Guid agencyId, List<Guid> missedVisitIds);
        //List<MissedVisit> GetMissedVisitsByEpisodeId(Guid agencyId, Guid patientId, Guid episodeId);
        //List<MissedVisit> GetMissedVisitsByEpisodeIds(Guid agencyId, List<Guid> episodeIds);
        //List<MissedVisit> GetMissedVisitsByEpisodeIdsOnlyStatus(Guid agencyId, List<Guid> episodeIds);
        List<ReturnComment> GetALLEpisodeReturnCommentsByEpisodeId(Guid agencyId, Guid episodeId);
        List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, List<Guid> episodeIds);
       
        List<PatientUser> GetPatientUsersByPatient(Guid patientId);
        List<SelectedUser> GetUserAccessibleList(Guid agencyId, Guid patientId);
        List<SelectedUser> GetUserAccessList(Guid agencyId, Guid patientId);
        PatientUser GetPatientUser(Guid patientUserId);
        bool AddPatientUser(PatientUser patientUser);
        bool RemovePatientUser(PatientUser patientUser);
        PatientUser GetPatientAccess(Guid patientId, Guid userId);
        
        //MultiGrid GetPatientAccessData(Guid agencyId, Guid userId);
        //bool IsFirstScheduledVisit(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);

        List<Patient> GetPatientsAdmissionSourceByIds(Guid agencyId, List<Guid> patientIds);

        List<PatientVisitLog> GetVisitVerificationLogs(Guid agencyId, DateTime startDate, DateTime endDate);
        PatientVisitLog GetVisitVerificationLog(Guid agencyId, Guid patientId, Guid episodeId, Guid eventId);

        List<PatientVisitNote> GetNoteSupplies(Guid agencyId, Guid patientId, List<Guid> noteIds);
        List<PatientVisitNote> GetNoteQuestionsString(Guid agencyId, Guid patientId, List<Guid> noteIds);
        bool AddMultipleVisitNote(List<PatientVisitNote> patientVisitNotes);
        bool AddMultipleOrder(List<PhysicianOrder> orders);
        bool AddMultipleCommunicationNote(List<CommunicationNote> communicationNotes);
        bool AddMultipleFaceToFaceEncounter(List<FaceToFaceEncounter> faceToFaceEncounters);
        bool RemovePatientVisitNoteFully(Guid agencyId, Guid patientId, Guid eventId);
        bool RemoveNoteRelationFully(Guid agencyId, Guid patientId, Guid eventId);

        List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, List<Guid> branchIds, List<int> status, DateTime startDate, DateTime endDate);
         List<PhysicianOrder> GetPatientPhysicianOrders(Guid agencyId, Guid patientId, List<int> optionalStatus, DateTime startDate, DateTime endDate);
         List<PhysicianOrder> GetCurrentAndPreviousPhysicianOrders(Guid agencyId, PatientEpisode episode);

         List<FaceToFaceEncounter> GetFaceToFaceEncounters(Guid agencyId, List<Guid> branchIds, List<int> status, DateTime startDate, DateTime endDate);
         List<FaceToFaceEncounter> GetPatientFaceToFaceEncounters(Guid agencyId, Guid patientId, List<int> optionalStatus, DateTime startDate, DateTime endDate);
         List<FaceToFaceEncounter> GetFaceToFaceEncounters(Guid agencyId, Guid patientId, Guid episodeId);

         List<PatientVisitNote> GetEvalOrders(Guid agencyId, List<Guid> branchIds, List<int> disciplineTasks, List<int> status, DateTime startDate, DateTime endDate);
         List<PatientVisitNote> GetPatientEvalOrders(Guid agencyId, Guid patientId, List<int> disciplineTasks, List<int> optionalStatus, DateTime startDate, DateTime endDate);
         List<PatientVisitNote> GetCurrentAndPreviousEvalOrders(Guid agencyId, PatientEpisode episode, List<int> disciplineTasks);

        //PatientEpisode GetMostRecentEpisodeLean(Guid agencyId, Guid patientId);
        //List<PatientEpisode> GetEpisodeLeans(Guid agencyId, Guid patientId);

        PatientNonAdmitReason GetCurrentNonAdmissionReason(Guid agencyId, Guid patientId);
        bool UpdateNonAdmissionReason(PatientNonAdmitReason nonAdmissionData);
        bool UpdateNonAdmissionReasonStatus(Guid agencyId, Guid patientId);
        List<NonAdmit> GetPatientsWithNonAdmissionComments(Guid agencyId, int status);
    }
}
