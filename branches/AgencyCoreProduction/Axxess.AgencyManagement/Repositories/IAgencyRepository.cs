﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IAgencyRepository
    {
        List<AgencyLocationLean> GetLocationsLean(Guid agencyId, List<Guid> locationIds);
        AgencyLocationLean GetLocationLean(Guid agencyId, Guid locationId);
        AgencyLocationLean GetMainLocationLean(Guid agencyId);

        bool ToggleDelete(Guid id);
        Agency Get(Guid id);
        Agency GetAgencyOnly(Guid id);
        Agency GetWithBranches(Guid agencyId);
        Agency GetById(Guid id);
        IEnumerable<Agency> All();
        //IList<Agency> AllAgencies();
        List<AgencyCache> AgencyCaches(List<Guid> agencyIds);
        AgencyCache AgencyCache(Guid agencyId);
        bool Add(Agency agency);
        bool Update(Agency agency);
        List<AgencyLocation> AgencyLocations(Guid agencyId, List<Guid> locationIds);
        int GetBranchesCount(Guid agencyId);
        List<AgencyUser> GetUserNames();
        List<AgencyUser> GetUserNames(Guid agencyId);
        List<AgencyLite> GetAllAgencies();
        bool AddLocation(AgencyLocation agencyLocation);
        AgencyLocation GetMainLocation(Guid agencyId);
        IList<AgencyLocation> GetBranches(Guid agencyId);
        AgencyLocation FindLocation(Guid agencyId, Guid Id);
        AgencyLocation FindLocationOrMain(Guid agencyId, Guid Id);
        bool EditLocation(AgencyLocation location);
        bool EditLocationModal(AgencyLocation location);
        bool UpdateLocation(AgencyLocation location);
        bool EditBranchCost(AgencyLocation location);
        bool AddContact(AgencyContact contact);
        IList<AgencyContact> GetContacts(Guid agencyId);
        AgencyContact FindContact(Guid agencyId, Guid Id);
        bool EditContact(AgencyContact contact);
        bool DeleteContact(Guid agencyId, Guid id);

        bool AddHospital(AgencyHospital hospital);
        IList<AgencyHospital> GetHospitals(Guid agencyId);
        AgencyHospital FindHospital(Guid agencyId, Guid Id);
        bool EditHospital(AgencyHospital hospital);
        bool DeleteHospital(Guid agencyId, Guid Id);


        bool AddPharmacy(AgencyPharmacy pharmacy);
        IList<AgencyPharmacy> GetPharmacies(Guid agencyId);
        AgencyPharmacy FindPharmacy(Guid agencyId, Guid Id);
        List<AgencyPharmacy> GetPharmacies(Guid agencyId, List<Guid> Ids);
        bool EditPharmacy(AgencyPharmacy pharmacy);
        bool DeletePharmacy(Guid agencyId, Guid Id);
        
        bool AddInsurance(AgencyInsurance insurance);
        IList<AgencyInsurance> GetInsurances(Guid agencyId);
        IList<InsuranceLean> GetLeanInsurances(Guid agencyId);
        AgencyInsurance GetInsurance(int insuranceId, Guid agencyId);
        AgencyInsurance FindInsurance(Guid agencyId, int Id);
        bool EditInsurance(AgencyInsurance insurance);
        bool EditInsuranceModal(AgencyInsurance insurance);
        bool DeleteInsurance(Guid agencyId, int Id);
        bool IsMedicareHMO(Guid agencyId, int Id);

        List<InsuranceCache> GetInsurancesForBilling(Guid agencyId, int[] insuranceIds);
        List<InsuranceLean> GetLeanInsurances(Guid agencyId, int[] insuranceIds);

        //Non-Visit Task
        bool AddNonVisitTask(AgencyNonVisit nonVisitTask);
        IList<AgencyNonVisit> GetNonVisitTasks(Guid agencyId);
        AgencyNonVisit GetNonVisitTask(Guid agencyId, Guid id);
        bool UpdateNonVisitTask(AgencyNonVisit nonVisitTask);
        bool DeleteNonVisitTask(Guid agencyId, Guid id);
        //

        //Non-Visit Task Manager
        List<UserNonVisitTask> GetUserNonVisitTasksByUserAndDateRange(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate);
        List<UserNonVisitTask> GetUserNonVisitTasksByUserStatusAndDateRange(Guid agencyId, Guid UserId, DateTime startDate, DateTime endDate, bool Status);
        List<UserNonVisitTask> GetUserNonVisitTasksByDateRange(Guid agencyId, DateTime startDate, DateTime endDate);
        List<UserNonVisitTask> GetUserNonVisitTasksByStatusAndDateRange(Guid agencyId, DateTime startDate, DateTime endDate, bool Status);
        IList<UserNonVisitTask> GetUserNonVisitTasksByUser(Guid agencyId, Guid UserId);
        IList<UserNonVisitTask> GetUserNonVisitTasks(Guid agencyId);
        bool AddUserNonVisitTask(UserNonVisitTask userNonVisitTask);
        UserNonVisitTask GetUserNonVisitTask(Guid agencyId, Guid id);
        bool UpdateUserNonVisitTask(UserNonVisitTask userNonVisitTask);
        bool DeleteUserNonVisitTask(Guid agencyId, Guid id);
        //

        bool AddTemplate(AgencyTemplate template);
        IList<AgencyTemplate> GetTemplates(Guid agencyId);
        AgencyTemplate GetTemplate(Guid agencyId, Guid id);
        bool UpdateTemplate(AgencyTemplate template);
        bool DeleteTemplate(Guid agencyId, Guid id);
        void InsertTemplates(Guid agencyId);
        

        bool AddInfection(Infection infection);
        bool UpdateInfection(Infection infection);
        bool UpdateInfectionModal(Infection infection);
        IList<Infection> GetInfections(Guid agencyId);
        bool ToggleInfectionsDeletedStatus(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
        //bool ReassignInfectionsUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        Infection GetInfectionReport(Guid agencyId, Guid id);

        bool AddIncident(Incident incident);
        bool UpdateIncident(Incident incident);
        bool UpdateIncidentModal(Incident incident);
        IList<Incident> GetIncidents(Guid agencyId);
        bool ToggleIncidentDeletedStatus(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
        bool ReassignIncidentUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        Incident GetIncidentReport(Guid agencyId, Guid id);

        bool AddSupply(AgencySupply supply);
        IList<AgencySupply> GetSupplies(Guid agencyId);
        IList<AgencySupply> GetSupplies(Guid agencyId, string searchTerm, int searchLimit);
        IList<AgencySupply> GetSuppliesByIds(Guid agencyId, List<Guid> supplyIds);
        AgencySupply GetSupply(Guid agencyId, Guid id);
        bool UpdateSupply(AgencySupply supply);
        bool DeleteSupply(Guid agencyId, Guid id);
        void InsertSupplies(Guid agencyId);
        AxxessSubmitterInfo SubmitterInfo(int payerId);

        bool AddReport(Report report);
        bool UpdateReport(Report report);
        Report GetReport(Guid agencyId, Guid reportId);
        bool RemoveReport(Guid agencyId, Guid reportId);
        IList<ReportLite> GetReports(Guid agencyId);
        IList<ReportLite> GetReportsByUserId(Guid agencyId, Guid userId);

        bool AddCustomerNote(CustomerNote note);
        bool UpdateCustomerNote(CustomerNote note);
        CustomerNote GetCustomerNote(Guid agencyId, Guid noteId);
        IList<CustomerNote> GetCustomerNotes(Guid agencyId);

        List<MedicareEligibilitySummary> GetMedicareEligibilitySummariesBetweenDates(Guid agencyId, DateTime startDate, DateTime endDate);
        MedicareEligibilitySummary GetMedicareEligibilitySummary(Guid agencyId, Guid reportId);
        AgencyMedicareInsurance FindLocationMedicareInsurance(Guid agencyId, int payor);
        bool AddAgencyLocationMedicare(AgencyMedicareInsurance medicare);
        bool UpdateAgencyLocationMedicare(AgencyMedicareInsurance medicare);

        bool AddAdjustmentCode(AgencyAdjustmentCode code);
        IList<AgencyAdjustmentCode> GetAdjustmentCodes(Guid agencyId);
        AgencyAdjustmentCode FindAdjustmentCode(Guid agencyId, Guid Id);
        bool UpdateAdjustmentCode(AgencyAdjustmentCode code);
        bool DeleteAdjustmentCode(Guid agencyId, Guid id);

        bool AddUploadType(UploadType uploadType);
        IList<UploadType> GetUploadType(Guid agencyId);
        UploadType FindUploadType(Guid agencyId, Guid Id);
        bool UpdateUploadType(UploadType uploadType);
        bool DeleteUploadType(Guid agencyId, Guid id);

        IList<AgencyTeam> GetTeams(Guid agencyId);
        IList<AgencyTeam> GetTeamsWithUserToolTip(Guid agencyId);
        AgencyTeam GetTeam(Guid agencyId, Guid id);
        bool AddTeam(AgencyTeam team);
        bool UpdateTeam(AgencyTeam team);
        bool DeleteTeam(Guid agencyid, Guid id);
        MultiGrid GetTeamAccessUserData(Guid agencyId, Guid teamId, List<User> agencyUsers);
        IList<SelectedUser> GetTeamUsers(Guid agencyId, Guid teamId, List<User> agencyUsers);
        IList<SelectedUser> GetUserNotInTeam(Guid agencyId, Guid agencyTeamId, List<User> agencyUsers);
        bool AddUsersToTeam(Guid agencyId, Guid agencyTeamId, List<Guid> selectedUsers);
        bool RemoveUsersFromTeam(Guid agencyId, Guid agencyTeamId, List<Guid> selectedUsers);
        MultiGrid GetTeamAccessPatientData(Guid agencyId, Guid teamId);
        List<SelectedPatient> GetPatientsInTeam(Guid agencyId, Guid teamId);
        List<SelectedPatient> GetPatientsNotInTeam(Guid agencyId, Guid teamId);
        bool AddPatientsToTeam(Guid agencyTeamId, List<Guid> patientIds);
        bool RemovePatientsFromTeam(Guid agencyTeamId, List<Guid> patientIds);
        List<SelectedPatient> GetTeamAccess(Guid agencyId, Guid currentUserId);

        bool AddUpgrade(AgencyUpgrade agencyUpgrade);
        List<ReportDescription> GetDescriptions(bool IsProduction);
        bool AddChange(AgencyChange agencyChange);
        LocationPrintProfile AgencyNameWithLocationAddress(Guid agencyId, Guid locationId);
        LocationPrintProfile AgencyNameWithMainLocationAddress(Guid agencyId);

        bool AddSubscriptionPlan(AgencySubscriptionPlan plan);
        bool UpdateSubscriptionPlan(AgencySubscriptionPlan plan);
        List<AgencySubscriptionPlan> GetSubscriptionPlans(Guid agencyId);
        AgencySubscriptionPlan GetSubscriptionPlan(Guid agencyId, Guid locationId);
        int GetPatientCountPerLocation(Guid agencyId, Guid locationId);
        int GetUserCountPerLocation(Guid agencyId, Guid locationId);

        bool AddMultipleInfection(List<Infection> infections);
        bool AddMultipleIncident(List<Incident> incidents);
    }
}
