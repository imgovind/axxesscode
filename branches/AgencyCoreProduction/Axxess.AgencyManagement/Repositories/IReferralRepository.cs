﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Enums;

    public interface IReferralRepository
    {
        IEnumerable<Referral> GetAll(Guid agencyId, ReferralStatus status);
        IEnumerable<Referral> GetAllByCreatedUser(Guid agencyId, Guid userId, ReferralStatus status);

        bool Add(Referral referral);
        Referral Get(Guid agencyId,Guid id);
        bool Update(Referral referral);
        bool Delete(Guid agencyId,Guid id);
        bool UpdateModal(Referral referral);
        bool SetStatus(Guid agencyId, Guid id, ReferralStatus status);
        bool NonAdmitReferral(PendingPatient pending);

        List<ReferralData> All(Guid agencyId, ReferralStatus referralStatus);
        List<ReferralData> AllByUser(Guid agencyId, Guid userId, ReferralStatus status);

        bool SetPrimaryEmergencyContact(Guid agencyId, Guid referralId, Guid emergencyContactId);
        bool AddEmergencyContact(ReferralEmergencyContact emergencyContactInfo);
        //ReferralEmergencyContact GetEmergencyContact(Guid referralId, Guid emergencyContactID,Guid agencyId);
        //IList<ReferralEmergencyContact> GetEmergencyContacts(Guid agencyId, Guid referralId);
        bool EditEmergencyContact(Guid agencyId, ReferralEmergencyContact emergencyContact);
        bool DeleteEmergencyContact(Guid Id, Guid referralId,Guid agencyId);
        ReferralEmergencyContact GetFirstEmergencyContactByReferral(Guid agencyId, Guid referralId);
        //List<NonAdmit> GetReferralsWithNonAdmissionComments(Guid agencyId, int status);
    }
}
