﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using Domain;

    public interface IAssetRepository
    {
        bool Delete(Guid id,Guid agencyId);
        bool Remove(Guid agencyId,Guid id);
        bool Add(Asset asset);
        Asset Get(Guid id, Guid agencyId);
        IList<Asset> GetAssetLean(Guid agencyId, List<Guid> assetIds);
        bool DeletePatientDocument(Guid id, Guid patientId,Guid agencyId);
        bool AddPatientDocument(Asset asset, PatientDocument patientDocument);
        bool UpdatePatientDocument(PatientDocument patientDocument);
        PatientDocument GetPatientDocument(Guid id, Guid patientId, Guid agencyId);
        List<PatientDocument> GetPatientDocuments(Guid patientId,Guid agencyId);
    }
}
