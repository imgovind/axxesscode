﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;

    using Repositories;

    public interface IAgencyManagementDataProvider
    {
        IReferralRepository ReferralRepository
        {
            get;
        }
        IPatientRepository PatientRepository
        {
            get;
        }
        IPhysicianRepository PhysicianRepository
        {
            get;
        }
        IUserRepository UserRepository
        {
            get;
        }
        IAgencyRepository AgencyRepository
        {
            get;
        }
        IAssetRepository AssetRepository
        {
            get;
        }
        IBillingRepository BillingRepository
        {
            get;
        }

        IMessageRepository MessageRepository
        {
            get;
        }

        IScheduleRepository ScheduleRepository
        {
            get;
        }

        IEpisodeRepository EpisodeRepository
        {
            get;
        }
    }
}
