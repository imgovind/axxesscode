﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core;


    public interface IEpisodeRepository
    {

        bool AddEpisodeModel(PatientEpisode patientEpisode);
        bool UpdateEpisodeModel(PatientEpisode patientEpisode);
        PatientEpisode GetEpisodeOnly(Guid agencyId, Guid episodeId, Guid patientId);
        PatientEpisode GetCurrentEpisodeLean(Guid agencyId, Guid patientId);
        PatientEpisode GetCurrentEpisodeLeanWithDetail(Guid agencyId, Guid patientId);
        
        DateRange GetEpisodeDateRange(Guid agencyId, Guid patientId, Guid id);
        PatientEpisode GetEpisodeLeanWithDetail(Guid agencyId, Guid patientId, Guid episodeId);
        PatientEpisode GetEpisodeLean(Guid agencyId, Guid patientId, Guid episodeId);
        PatientEpisode GetNextEpisodeDataLean(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetNextEpisodeDataLeanWithDetail(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetPreviousEpisodeDataLean(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetPreviousEpisodeDataLeanWithDetail(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetEpisodeLeanByStartDate(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetPreviousEpisodeByEndDate(Guid agencyId, Guid patientId, DateTime endDate);
        List<PatientEpisode> GetMutipleEpisodeLeanWithDetail(Guid agencyId, Guid patientId, List<Guid> episodeIds);
        List<PatientEpisode> GetEpisodeDatasBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        PatientEpisode GetCurrentEpisodeOrLastEpisode(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetCurrentEpisodeOrLastEpisodeWithDetail(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetLastEpisode(Guid agencyId, Guid patientId);
        List<PatientEpisode> GetPatientActiveEpisodesLeanWithDetail(Guid agencyId, Guid patientId);
        List<PatientEpisode> GetPatientActiveEpisodesLeanWithDetailAndAdmissionId(Guid agencyId, Guid patientId);
        List<PatientEpisode> GetPatientActiveEpisodesLeanWithOutDetail(Guid agencyId, Guid patientId);
        List<PatientEpisode> GetPatientActiveEpisodesLeanByDateRange(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        PatientEpisode GetEpisodeByIdWithSOC(Guid agencyId, Guid episodeId, Guid patientId);
        bool RemoveEpisode(Guid agencyId, Guid patientId, Guid episodeId);
        List<PatientsAndEpisode> GetPatientAndEpisodesByBranchStatusInsurance(Guid agencyId, Guid branchId, int statusId, string insuranceIds, DateTime startDate, DateTime endDate);

        PatientEpisode GetEpisodeBetweenDischarge(Guid agencyId, Guid patientId, Guid admissionId, DateTime date);
        //List<PatientEpisode> GetEpisodeLeanForDischarge(Guid agencyId, Guid patientId, Guid admissionId);
        List<PatientEpisode> GetEpisodeLeanForDischarge(Guid agencyId, Guid patientId, Guid admissionId, DateTime dischargeDate);
        bool UpdateEpisodeForDischarge(List<PatientEpisode> episodes);
        bool IsEpisodeExist(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, Guid excludedEpisodeIdOptional);
        bool IsEpisodeExist(Guid agencyId, Guid patientId, Guid episodeId);
        List<PatientEpisode> GetPatientAllEpisodesWithNoException(Guid agencyId, Guid patientId);
        List<EpisodeLean> GetPatientDeactivatedAndDischargedEpisodes(Guid agencyId, Guid patientId);
        PatientEpisode GetPatientEpisodeFluent(Guid agencyId, Guid episodeId, Guid patientId);
        bool SetRecertFlag(Guid agencyId, Guid episodeId, Guid patientId, bool isRecertComplete);
        bool DischargeEpisodeWithSchedules(Guid agencyId, Guid patientId, Guid admissionId, DateTime startDate, bool IsDischarge);
        bool LinkEpisodeDischargeWithSchedulesByEpisodeId(Guid agencyId, Guid patientId, Guid episodeId, DateTime dischargeDate, bool IsLinkedToDischarge);

        
    }
}
