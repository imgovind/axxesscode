﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Extension;
    using Axxess.Core;

    public interface IScheduleRepository
    {
        bool AddScheduleTask(ScheduleEvent scheduleEvent);
        bool AddMultipleScheduleTask(List<ScheduleEvent> scheduleEvents);
        ScheduleEvent GetScheduleTask(Guid agencyId, Guid patientId, Guid eventId);
        bool UpdateScheduleTaskModal(ScheduleEvent scheduleEvent);
        bool UpdateScheduleTasksStatus(Guid agencyId, Guid patientId, Guid eventId, int status);
        bool UpdateScheduleTasksPrintQueue(Guid agencyId, Guid patientId, Guid eventId, bool inPrintQueue);
        bool UpdateScheduleTasksPrintQueueAndStatus(Guid agencyId, Guid patientId, Guid eventId, int status, bool inPrintQueue);
        bool RemoveScheduleTaskFully(Guid agencyId, Guid patientId, Guid eventId);
        bool RemoveMultipleScheduleTaskFully(Guid agencyId, Guid patientId, List<Guid> eventIds);
        List<ScheduleEvent> GetPatientScheduledEventsOnly(Guid agencyId, Guid episodeId, Guid patientId);
        List<ScheduleEvent> GetPatientScheduledEventsOnly(Guid agencyId, Guid patientId);
        List<ScheduleEvent> GetPatientScheduledEventsOnly(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate);
        bool UpdateScheduleEventsForIsBillable(Guid agencyId, Guid patientId, Guid episodeId, List<ScheduleEvent> scheduleEvents);
        bool UpdateScheduleEventsForIsBillable(Guid agencyId, Guid patientId, List<ScheduleEvent> scheduleEvents);
        List<ScheduleEvent> GetCurrentAndPreviousOrders(Guid agencyId, PatientEpisode episode, List<int> disciplineTasks);
        List<ScheduleEvent> GetScheduleEventsForVitalSign(Guid agencyId, Guid patientId, Guid optionalEpisodeId, List<int> disciplineTasks, bool IsDateRangeApplicable, DateRange dateRange, bool isLimitApplicable, int limit, DateTime maxEventDateIfLimit);
        //List<ScheduleEvent> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks);
        List<ScheduleEvent> GetScheduleByBranchDateRangeAndStatus(Guid agencyId, List<Guid> branchIds, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded);
        List<ScheduleEvent> GetPrintQueueTasks(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);
        List<ScheduleEvent> GetPatientScheduledEventsOnlyLeanNew(Guid agencyId, Guid episodeId, Guid patientId, int[] scheduleStatus, string discipline, int[] disciplineTasks, bool isReportAndNotesIncluded);
        List<ScheduleEvent> GetPatientScheduledEventsOnlyNew(Guid agencyId, Guid episodeId, Guid patientId);
        List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid patientId, Guid[] episodeIds, DateTime startDate, DateTime endDate, List<int> disciplines, bool isReportAndNotesIncluded, bool IsDateRange);
        List<ScheduleEvent> GetRAPClaimSchedules(Guid agencyId, List<Guid> ids);
        List<ScheduleEvent> GetFinalClaimSchedules(Guid agencyId, List<Guid> ids);
        List<ScheduleEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);
        List<ScheduleEvent> GetScheduleByUserId(Guid agencyId, Guid userId, DateTime start, DateTime end, bool isMissedVist, string additionalFilter);
        IList<UserVisitWidget> GetScheduleWidget(Guid agencyId, Guid userId, DateTime from, DateTime to, int limit, bool isMissedVisit, string additionalFilter);
        ScheduleEvent GetFirstScheduledEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks);
        ScheduleEvent GetLastScheduledEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks);
               List<ScheduleEvent> GetPreviousNotes(Guid agencyId, ScheduleEvent scheduledEvent, string[] disciplines, int[] disciplineTasks, int[] status, bool isVersionSpecific);
        List<ScheduleEvent> GetScheduledEventsOnlyWithId(Guid agencyId, Guid patientId,  string eventIds, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded);
        List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded);
        List<ScheduleEvent> GetScheduleByPatientEpisodeUserIdLean(Guid agencyId, Guid patientId, Guid episodeId, Guid optionalUserId);
        bool ToggleScheduledTasksDelete(Guid agencyId, Guid patientId, List<Guid> eventIds, bool isDeprecated);
        bool UpdateScheduleTasksForReassign(Guid agencyId, Guid userId, List<Guid> eventIds);
        bool UpdateScheduleTasksForEpisodeEdit(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, bool isActive, bool isDischarge);
        List<ScheduleEvent> GetUsersScheduleTasksBetweenDatesByStatusForReassign(Guid agencyId, Guid patientId, Guid userId, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, params int[] scheduleStatus);
        List<ScheduleEvent> GetUsersScheduleTasksBetweenDatesByStatusForReassign(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, params int[] scheduleStatus);

        List<ScheduleEvent> GetPatientScheduledEventsForPrint(Guid agencyId, Guid episodeId, Guid patientId);
        List<ScheduleEvent> GetScheduledEventsLeanWithId(Guid agencyId, Guid patientId, Guid episodeId, List<Guid> eventIds);
        List<ScheduleEvent> GetScheduleEventsVeryLeanNonOptionalPar(Guid agencyId, Guid patientId, Guid episodeId, int[] scheduleStatus, int[] disciplineTasks, DateTime startDate, DateTime endDate, bool IsDateRange, bool IsMissedVisitIncluded);

        bool UpdateEntityForReassigningUser(Guid agencyId, Guid employeeId, string script);
        bool UpdateEntityForReassigningUser(Guid agencyId, Guid patientId, Guid userId, string script);
        bool UpdateEntityForDelete(Guid agencyId, Guid patientId, Guid episodeId, string script);

        List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, Guid episodeId);
        List<ReturnComment> GetALLEpisodeReturnCommentsByIds(Guid agencyId, List<Guid> episodeIds);

        bool AddMissedVisit(MissedVisit missedVisit);
        bool UpdateMissedVisit(MissedVisit missedVisit);
        MissedVisit GetMissedVisitOnly(Guid agencyId, Guid Id);
        MissedVisit GetMissedVisitsWithScheduleInfo(Guid agencyId, Guid missedVisitId);
        List<MissedVisit> GetMissedVisitsByIds(Guid agencyId, List<Guid> missedVisitIds);
        List<MissedVisit> GetMissedVisitsByIdOnlyStatus(Guid agencyId, List<Guid> missedVisitIds);
        List<MissedVisit> GetMissedVisitsByEpisodeId(Guid agencyId, Guid patientId, Guid episodeId);
        List<MissedVisit> GetMissedVisitsByEpisodeIds(Guid agencyId, List<Guid> episodeIds);
        List<MissedVisit> GetMissedVisitsByEpisodeIds(Guid agencyId, Guid patientId, List<Guid> episodeIds);
        List<MissedVisit> GetMissedVisitsByEpisodeIdsOnlyStatus(Guid agencyId, Guid patientId, List<Guid> episodeIds);
        List<MissedVisit> GetMissedVisitsByIdsAndStatus(Guid agencyId, List<int> statuses, List<Guid> missedVisitIds);


        bool ToggleScheduledEventsPaid(Guid agencyId, List<Guid> eventIds, bool isVisitPaid);
        List<ScheduleEvent> GetPatientScheduledEventsLeanWithUserIdAndCheckMissedVisitStatus(Guid agencyId, Guid patientIdOptional, Guid userIdOptional, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit);
        List<ScheduleEvent> GetPatientScheduledEventsLeanWithUserId(Guid agencyId, Guid patientIdOptional, Guid userIdOptional, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit);

        ScheduleEvent GetCarePlan(Guid agencyId, Guid episodeId, Guid patientId,  DateTime startDate, DateTime endDate, List<int> disciplineTasks);
        PatientVisitNote GetCarePlanForEpisode(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks);
        PatientVisitNote GetCarePlanForHHAideCarePlanNote(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate, List<int> disciplineTasks);
        ScheduleEvent GetCarePlanForHHAideCarePlan(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, DateTime eventDate, out IDictionary<string, NotesQuestion> pocQuestions);
        List<ScheduleEvent> GetScheduleByBranchDateRangeAndStatusLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded);
        List<ScheduleEvent> GetScheduleByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded);
        List<ScheduleEvent> GetScheduleDeviations(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded);
        List<ScheduleEvent> GetScheduledEventsOnlyLean(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded);
        List<UserVisit> GetUserVisitLean(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded);
        List<UserVisit> GetUserVisitLeanByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded);
        List<VisitSummary> GetPayrollSummmaryLean(Guid agencyId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid);
        List<UserVisit> GetPayrollSummmaryVisits(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid);
        List<ScheduleEvent> GetMissedVisitSchedulesLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus);

        ScheduleEvent FirstBillableEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate);
        bool IsFirstScheduledVisit(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, DateTime startDate, DateTime endDate);
        List<PatientVisitNote> GetVisitNotesByDisciplineTaskWithStatus(Guid agencyId, Guid patientId, int[] scheduleStatus, int[] disciplineTasks);

        List<ScheduleEvent> GetTherapyExceptionScheduleEvents(Guid agencyId, Guid branchId, Guid patientId, DateTime fromDate, DateTime toDate, int statusOptional);

        ScheduleEvent GetEpisodeAssessmentScheduleEvent(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, bool isNoneOASISIncluded);

        ScheduleEvent LastRecertOrROCScheduleEvent(Guid agencyId, Guid patientId, Guid episodeId, DateTime endDate, bool isNoneOASISIncluded);

        List<ScheduleEvent> GetUpcomingRecertsLean(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, int payor);
        bool CheckTimeOverlap(ScheduleEvent scheduleEvent, out string message);
    }
}
