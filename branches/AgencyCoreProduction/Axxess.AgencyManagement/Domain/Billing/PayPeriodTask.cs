﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.SqlGeneration.Schema;
    public class PayPeriodTask
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PayPeriodId { get; set; }
        public Guid TaskId { get; set; }
        public bool IsDeprecated { get; set; }
    }
}
