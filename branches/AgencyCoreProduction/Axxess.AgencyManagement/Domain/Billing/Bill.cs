﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using Enums;

    public class Bill
    {
        public Bill()
        {
            this.Claims = new List<Claim>();
        }
        public IList<Claim> Claims { get; set; }
        public bool IsElectronicSubmssion { get; set; }
        public Guid BranchId { get; set; }
        public int Insurance { get; set; }
        public bool IsMedicareHMO { get; set; }
        public ClaimTypeSubCategory ClaimType { get; set; }

        public string BranchName { get; set; }
        public string InsuranceName { get; set; }

        public Agency Agency { get; set; }

        public int InvoiceType { get; set; }
    }
}
