﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;

    public class ClaimHistoryLean
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime PaymentDate { get; set; }
        public double PaymentAmount { get; set; }
        public string Type { get; set; }
        public int Status { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public DateTime ClaimDate { get; set; }
        public double ClaimAmount { get; set; }
        public int InvoiceType { get; set; }

        public string DocumentLink
        {
            get
            {
                if (this.InvoiceType == (int)Enums.InvoiceType.UB)
                {
                    return string.Format("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/UB04Pdf', {{ 'patientId': '{0}', 'Id': '{1}', 'type': '{2}' }});\">UB-04</a>", this.PatientId, this.Id, this.Type);
                }
                else if (this.InvoiceType == (int)Enums.InvoiceType.HCFA)
                {
                    return string.Format("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/HCFA1500Pdf', {{ 'patientId': '{0}', 'Id': '{1}', 'type': '{2}'  }});\">HCFA-1500</a> ", this.PatientId, this.Id, this.Type);
                }
                else
                {
                    return string.Empty;
                }

                //if (this.InvoiceType == (int)Axxess.AgencyManagement.Enums.InvoiceType.HCFA)
                //{
                //    return "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/UB04Pdf', { 'patientId': '<#=PatientId#>', 'Id': '<#=Id#>', 'type': '<#=Type#>' });\">UB-04</a>";
                //}
                //else
                //{
                //    return "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/UB04Pdf', { 'patientId': '<#=PatientId#>', 'Id': '<#=Id#>', 'type': '<#=Type#>' });\">UB-04</a>";
                //}
            }
        }

        public string EpisodeRange
        {
            get
            {
                return string.Format("{0}-{1}", this.EpisodeStartDate != null ? this.EpisodeStartDate.ToString("MM/dd/yyyy") : "", this.EpisodeEndDate != null ? this.EpisodeEndDate.ToString("MM/dd/yyyy") : "");
            }
        }
        public string StatusName
        {
            get
            {
                return Enum.IsDefined(typeof(BillingStatus),this.Status)? ((BillingStatus)this.Status).GetDescription():string.Empty;

            }
        }
        public string PaymentDateFormatted
        {
            get
            {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToShortDateString().ToZeroFilled() : string.Empty;
            }
        }
    }
}
