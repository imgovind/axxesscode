﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class SelectableItem
    {
        public Guid Id { get; set; }
        public string DisplayName { get; set; }
    }
}
