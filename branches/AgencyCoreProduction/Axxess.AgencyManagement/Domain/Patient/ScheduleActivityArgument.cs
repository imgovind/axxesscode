﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public class ScheduleActivityArgument
    {
       public Guid EpisodeId { get; set; }
       public Guid PatientId { get; set; }
       public string Discpline { get; set; }
    }
}
