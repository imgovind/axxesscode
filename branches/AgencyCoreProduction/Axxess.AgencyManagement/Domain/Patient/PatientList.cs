﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Enums;
using Axxess.Core.Extension;

namespace Axxess.AgencyManagement.Domain
{
    public class PatientList : PatientRoster
    {
        public string patientMaritalStatus { get; set; }
        public string patientClinician { get; set; }
        public string patientMRN { get; set; }
        public string patientMedicaidNumber { get; set; }
        public string patientSSN { get; set; }
        public string patientDNR { get; set; }
        public DateTime patientEpisodeStartDate { get; set; }
        public DateTime patientEpisodeEndDate { get; set; }
        public string patientEthnicity { get; set; }
        public string patientPaymentSource { get; set; }
        public string patientEmail { get; set; }
        public string patientPrimaryContact { get; set; }
        public int patientServices { get; set; }
        public string patientServicesRequired { get; set; }
        public string patientDME { get; set; }
        public string patientComments { get; set; }
        public string patientPrimaryHealthPlanId { get; set; }
        public string patientMobile { get; set; }
        public string patientReferralName { get; set; }
        public string patientPayer { get; set; }
        public Guid patientCaseManagerId { get; set; }
    }
}
