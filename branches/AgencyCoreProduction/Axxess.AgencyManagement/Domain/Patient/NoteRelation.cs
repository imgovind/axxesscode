﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    public class NoteRelation
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid ParentId { get; set; }
        public string Type { get; set; }
        public bool IsDeprecated { get; set; }
    }
}
