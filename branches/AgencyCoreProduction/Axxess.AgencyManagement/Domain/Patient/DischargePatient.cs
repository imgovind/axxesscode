﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;

    public class DischargePatient
    {
        public string PatientData { get; set; }
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public DateTime StartofCareDate { get; set; }
        public DateTime DischargeDate { get; set; }
        public string DischargeReason { get; set; }
        public string CurrentDischargeReason { get; set; }
        public int CurrentDischargeReasonId { get; set; }
        public int DischargeReasonId { get; set; }
        public int AdmissionDischargeReasonId { get; set; }
        public bool IsTheSameAdmission { get; set; }

        public string DischargeReasonName
        {
            get
            {
                return this.DischargeReasonId.ToEnum<DischargeReasons>(DischargeReasons.None).GetDescription();
            }
        }
    }
}
