﻿

namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;
    public class ReferralInfo
    {
        public DateTime ReferralDate { get; set; }
        public string FirstName{get;set;}
        public string LastName{get;set;}
        public string MiddleName{get;set;}
        public string ReferralFirstName{get;set;}
        public string ReferralLastName{get;set;}
        public string AddressLine1{get;set;}
        public string AddressLine2{get;set;}
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public string OtherReferralSource { get; set; }
        public int Status { get; set; }
        public string ServicesRequired { get; set; }
        public string PhysicianName
        {
            get
            {
                return (this.FirstName + " "+this.MiddleName).Trim()+" "+ this.LastName;
            }
        }
        public string ReferralName
        {
            get
            {
                return this.ReferralFirstName + " " + this.ReferralLastName;
            }
        }
        public string Address
        {
            get
            {
                return (this.AddressLine1 + " " + this.AddressLine2).Trim() + " " + this.AddressCity + " " + this.AddressStateCode + " " + this.AddressZipCode;
            }
        }
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((ReferralStatus)Enum.ToObject(typeof(ReferralStatus), this.Status));
            }
        }
        public string ServiceRequiredName
        {
            get
            {
                if (this.ServicesRequired.IsNotNullOrEmpty())
                {
                    var services = this.ServicesRequired.Trim().Split(';');
                    if (services.Length > 0)
                    {
                        string serviceRequiredName = "";
                        for (int i = 0; i < services.Length; i++)
                        {
                            switch (services[i])
                            {
                                case "0":
                                    serviceRequiredName += ",SN";
                                    break;
                                case "1":
                                    serviceRequiredName += ",HHA";
                                    break;
                                case "2":
                                    serviceRequiredName += ",PT";
                                    break;
                                case "3":
                                    serviceRequiredName += ",OT";
                                    break;
                                case "4":
                                    serviceRequiredName += ",SP";
                                    break;
                                case "5":
                                    serviceRequiredName += ",MSW";
                                    break;
                            }
                        }
                        if (serviceRequiredName.Length > 0)
                        {
                            serviceRequiredName = serviceRequiredName.Substring(1);
                        }
                        return serviceRequiredName;
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }
}
