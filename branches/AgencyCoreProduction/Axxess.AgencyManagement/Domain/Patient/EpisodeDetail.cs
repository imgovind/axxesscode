﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class EpisodeDetail
    {
        public EpisodeDetail()
        {
            this.Assets = new List<Guid>();
        }

        public string Comments { get; set; }
        public List<Guid> Assets { get; set; }
        public string Therapist { get; set; }
        public string CaseManager { get; set; }
        public string PrimaryDiagnosis { get; set; }
        public string SecondaryDiagnosis { get; set; }
        public string TriageCode { get; set; }
        public string FrequencyList { get; set; }
        public string PrimaryInsurance { get; set; }
        public string SecondaryInsurance { get; set; }
        public string PrimaryPhysician { get; set; }
        public string SurchargePayroll { get; set; }
    }
}
