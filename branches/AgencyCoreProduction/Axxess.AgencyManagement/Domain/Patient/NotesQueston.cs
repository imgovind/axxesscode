﻿namespace Axxess.AgencyManagement.Domain
{

    using System;
    using System.Xml;
    using System.Xml.Serialization;

    [XmlRoot()]   
    public class NotesQuestion : IQuestion
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Answer { get; set; }
        [XmlAttribute]
        public string Type { get; set; }


        public static NotesQuestion Create(string name, string answer)
        {
            return new NotesQuestion() { Name = name, Answer = answer };
        }
    }
}
