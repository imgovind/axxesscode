﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;

    public class PendingPatient : EntityBase
    {
        #region Members
        
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid UserId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime StartofCareDate { get; set; }
        public string PrimaryInsurance { get; set; }
        public string SecondaryInsurance { get; set; }
        public string TertiaryInsurance { get; set; }
        public string Payer { get; set; }
        public string Comments { get; set; }
        public string DischargeComments { get; set; }
        public string NonAdmissionComments { get; set; }
        public int Status { get; set; }
        public int ReasonId { get; set; }
        public string Reason { get; set; }
        public bool IsAdmit { get; set; }
        public DateTime NonAdmitDate { get; set; }
        public DateTime ReferralDate { get; set; }
        public Guid CaseManagerId { get; set; }
        public DateTime DateOfDischarge { get; set; }
        public Guid AdmissionId { get; set; }
        public DateTime Created { get; set; }


        #endregion

        #region Domain
        [SubSonicIgnore]
        public bool IsFaceToFaceEncounterCreated { get; set; }

        [SubSonicIgnore]
        public bool ShouldCreateEpisode { get; set; }

        [SubSonicIgnore]
        public string StartOfCareDateFormatted { get { return this.StartofCareDate.ToShortDateString(); } }
        [SubSonicIgnore]
        public DateTime EpisodeStartDate { get; set; }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        public NonAdmitTypes Type { get; set; }
        [SubSonicIgnore]
        public string Branch { get; set; }
        [SubSonicIgnore]
        public string PrimaryInsuranceName { get; set; }
        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToShortDateString().ToZeroFilled(); } }
        [SubSonicIgnore]
        public Guid PrimaryPhysician { get; set; }
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            if (this.IsAdmit)
            {
                AddValidationRule(new Validation(() => this.StartofCareDate == DateTime.MinValue, "Start of care date is required."));
                AddValidationRule(new Validation(() => this.EpisodeStartDate == DateTime.MinValue, "Episode start date is required."));
                AddValidationRule(new Validation(() => this.EpisodeStartDate < this.StartofCareDate, "Episode start date is must be greater or equal to the start of care date."));
                AddValidationRule(new Validation(() => this.PrimaryInsurance.IsEqual("0"), "Insurance is required."));
            }
            else
            {
                AddValidationRule(new Validation(() => this.NonAdmitDate == DateTime.MinValue, "Non-Admission date is required."));
                AddValidationRule(new Validation(() => this.Reason.IsNullOrEmpty(), "Reason for Non-Admission required."));
            }
        }

        #endregion

    }
}
