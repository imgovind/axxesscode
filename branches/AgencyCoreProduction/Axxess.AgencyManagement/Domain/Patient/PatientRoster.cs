﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;

    public class PatientRoster
    {
        public Guid Id { get; set; }
        public string PatientId { get; set; }
        public string PatientLastName { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientMiddleInitial { get; set; }
        public string PatientGender { get; set; }
        public string PatientMedicareNumber { get; set; }
        public DateTime PatientDOB { get; set; }
        public string PatientPhone { get; set; }
        public string PatientAddressLine1 { get; set; }
        public string PatientAddressLine2 { get; set; }
        public string PatientAddressCity { get; set; }
        public string PatientAddressStateCode { get; set; }
        public string PatientAddressZipCode { get; set; }
        public string PatientInsuranceName { get; set; }
        public string PatientInsuranceNumber { get; set; }
        public string PatientInsuranceId { get; set; }
        public string PatientSoC { get; set; }
        public string PatientDischargeDate { get; set; }
        public int PatientStatus { get; set; }
        public string PatientPrimaryDiagnosis { get; set; }
        public string PatientSecondaryDiagnosis { get; set; }
        public Guid PhysicianId { get; set; }
        public string PhysicianNpi { get; set; }
        public string PhysicianName { get; set; }
        public string PhysicianPhone { get; set; }
        public string PhysicianFacsimile { get; set; }
        public string PatientInsurance { get; set; }
        public string PatientCaseManager { get; set; }
        public string PhysicianPhoneHome { get; set; }
        public string PhysicianPhoneMobile { get; set; }
        public string PhysicianEmailAddress { get; set; }
        public string ResponsibleEmployee { get; set; }

        public string AdmissionSource { get; set; }
        public string OtherReferralSource { get; set; }
        public string InternalReferral { get; set; }
        public string ReferrerPhysician { get; set; }
        public string ReferralDate { get; set; }
        public string PatientPolicyNumber
        { 
            get {
                string policyNumber = "";
                if (this.PatientMedicareNumber.IsNotNullOrEmpty())
                {
                    policyNumber = this.PatientMedicareNumber;
                }
                else
                {
                    policyNumber = this.PatientInsuranceNumber;
                }
                return policyNumber;
            } 
        }

        public int Triage { get; set; }

        public string PatientDisplayName
        {
            get
            {
                return string.Format("{0}, {1} {2}", this.PatientLastName.ToUpperCase(), this.PatientFirstName.ToUpperCase(), this.PatientMiddleInitial.ToInitial());
            }
        }

        public string AdmissionSourceName
        {
            get
            {
                int admissionSource;
                if (int.TryParse(this.AdmissionSource, out admissionSource))
                {
                    return Enum.IsDefined(typeof(ReferralSource), admissionSource) ? ((ReferralSource)Enum.ToObject(typeof(ReferralSource), admissionSource)).GetDescription() : string.Empty;
                }
                return string.Empty;
            }
        }
        public string AddressFull
        {
            get
            {

                return string.Format("{0} {1}, {2} {3} {4}", this.PatientAddressLine1,this.PatientAddressLine2 ,this.PatientAddressCity, this.PatientAddressStateCode, this.PatientAddressZipCode);
            }
        }
        public string PatientStatusName
        {
            get
            {
                return Enum.IsDefined(typeof(PatientStatus), this.PatientStatus) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), this.PatientStatus)).GetDescription() : string.Empty;
            }
        }

        public Guid InternalReferralId { get; set; }
        public Guid ReferrerPhysicianId { get; set; }


    }
}
