﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using System.ComponentModel.DataAnnotations;

    public class PatientNonAdmitReason
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        [DataType(DataType.Date)]
        public DateTime NonAdmissionDate { get; set; }
        public string NonAdmissionReason { get; set; }
        public string NonAdmissionComments { get; set; }
        public bool IsCurrent { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
