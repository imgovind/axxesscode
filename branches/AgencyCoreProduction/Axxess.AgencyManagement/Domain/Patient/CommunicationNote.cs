﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;
    using System.Runtime.Serialization;
    using Axxess.Core.Enums;
    [Serializable]
    [DataContract]
    public class CommunicationNote : EntityBase
    {
        #region Members
        
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid UserId { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        public string Text { get; set; }
        public string Recipients { get; set; }
        public int Status { get; set; }
        public DateTime SignatureDate { get; set; }
        public string SignatureText { get; set; }
        public string PhysicianData { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public List<Guid> RecipientArray { get; set; }

        [SubSonicIgnore]
        public bool SendAsMessage { get; set; }

        [SubSonicIgnore]
        public string DisplayName { get; set; }

        [SubSonicIgnore]
        public string PhysicianName { get; set; }

        [SubSonicIgnore]
        public Patient Patient { get; set; }

        [SubSonicIgnore]
        public Agency Agency { get; set; }

        [SubSonicIgnore]
        public string UserDisplayName { get; set; }

        [SubSonicIgnore]
        public string EpisodeStartDate { get; set; }

        [SubSonicIgnore]
        public string EpisodeEndDate { get; set; }

        [SubSonicIgnore]
        public AgencyPhysician Physician { get; set; }

        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                ScheduleStatus status =  Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), this.Status.ToString()) : ScheduleStatus.NoStatus;
                if (status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue && this.Created < DateTime.Today)
                {
                    return ScheduleStatus.CommonNotStarted.GetDescription();
                }
                else
                {
                    return status.GetDescription();
                }
            }
        }

        [SubSonicIgnore]
        public string PrintUrl { get; set; }

        [SubSonicIgnore]
        public string StatusComment { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.PatientId.IsEmpty(), "Patient is required."));
            AddValidationRule(new Validation(() => this.EpisodeId.IsEmpty(), "Episode is required."));
            AddValidationRule(new Validation(() => !this.Created.ToString().IsValidDate(), "Created Date is invalid."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Text), "Text is required."));
        }

        #endregion
    }
}
