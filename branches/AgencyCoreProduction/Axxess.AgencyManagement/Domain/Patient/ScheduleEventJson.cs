﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain
{
    using System.ComponentModel;

    using Axxess.Core.Extension;
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    public class ScheduleEventJson
    {
        public Guid EventId { get; set; }
        
        //public Guid PatientId { get; set; }
        
        public Guid EpisodeId { get; set; }
        
        public string User { get; set; }
        
        public DateTime Date { get; set; }
        
        public string Status { get; set; }
        
        public string Task { get; set; }
        
        public int Discipline { get; set; }
        [DefaultValue(0)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int IsVisitVerified { get; set; }
        [DefaultValue(0)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int HasAttachments { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string OasisProfileUrl { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string StatusComment { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string EpisodeNotes { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string PrintUrl { get; set; }
        [DefaultValue("")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string OnClick { get; set; }
        //Action Permissions
        [DefaultValue(0)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public long AP { get; set; }
        [DefaultValue(0)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int IsComplete { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Comments { get; set; }
        [DefaultValue(0)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int IsMissed { get; set; }
        [DefaultValue(0)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int IsOrphaned { get; set; }

        public ScheduleEventJson(ScheduleEvent s)
        {
            EventId = s.Id;
            //PatientId = s.PatientId;
            EpisodeId = s.EpisodeId;
            Date = s.EventDate;
            User = s.UserName;
            Task = s.DisciplineTaskName;
            Status = s.StatusName;
            IsVisitVerified = s.IsVisitVerified ? 1 : 0;
            OasisProfileUrl = s.OasisProfileUrl.IsNotNullOrEmpty() ? s.OasisProfileUrl : null;
            StatusComment = s.StatusComment.IsNotNullOrEmpty() ? s.StatusComment : null;
            Comments = s.Comments.IsNotNullOrEmpty() ? s.CommentsCleaned : null;
            EpisodeNotes = s.EpisodeNotes.IsNotNullOrEmpty() ? s.EpisodeNotesCleaned : null;
            PrintUrl = s.PrintUrl.IsNotNullOrEmpty() ? s.PrintUrl : null;
            OnClick = s.OnClick;
            IsOrphaned = s.IsOrphaned ? 1 : 0;
            HasAttachments = (s.Assets.Count > 0) ? 1 : 0;
            AP = s.ActionPermission;
            Discipline = s.Discipline;
            IsMissed = s.IsMissedVisit ? 1 : 0;
            IsComplete = s.IsComplete ? 1 : 0;
        }
    }
}
