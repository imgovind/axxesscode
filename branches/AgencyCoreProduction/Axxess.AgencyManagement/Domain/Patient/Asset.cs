namespace Axxess.AgencyManagement.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;

    public class Asset
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string FileName { get; set; }
        public string FileSize { get; set; }
        public string ContentType { get; set; }
        public byte[] Bytes { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public string ModifiedDateFormatted { get { return this.Modified.ToShortDateString().ToZeroFilled(); } }
    
    }
}