﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Enums;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Enums;

    public class FaceToFaceEncounter : EntityBase
    {
        #region Members

        public Guid Id {get; set;}
        public long OrderNumber { get; set;}
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid UserId { get; set; }
        public Guid EpisodeId { get; set; }
        public string Certification { get; set; }
        public int Status { get; set; }
        public DateTime EncounterDate { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime SentDate { get; set; }
        public string ClinicalFinding { get; set; }
        public string MedicalReason { get; set; }
        public string Services { get; set; }
        public string ServicesOther { get; set; }
        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        public string PhysicianData { get; set; } 

        #endregion

        #region Domain

        [SubSonicIgnore]
        public List<string> ServicesArray { get; set; }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        public string PhysicianName { get; set; }
        [SubSonicIgnore]
        public AgencyPhysician Physician { get; set; }
        [SubSonicIgnore]
        public string EpisodeStartDate { get; set; }
        [SubSonicIgnore]
        public string EpisodeEndDate { get; set; }
        [SubSonicIgnore]
        public List<String> PdfPages { get; set; }
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                if (this.Status > 0)
                {
                    return this.Status.ToEnum<ScheduleStatus>(ScheduleStatus.OrderNotYetStarted).GetDescription();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string RequestDateFormatted
        {
            get
            {
                return this.RequestDate.ToShortDateString().ToZeroFilled();
            }
        }
        [SubSonicIgnore]
        public string PrintViewJson { get; set; }
        [SubSonicIgnore]
        public string PrintUrl { get; set; }
        
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.PatientId.IsEmpty(), "Patient is required."));
            AddValidationRule(new Validation(() => this.EpisodeId.IsEmpty(), "Episode is required."));
            AddValidationRule(new Validation(() => this.PhysicianId.IsEmpty(), "Physician is required."));
            AddValidationRule(new Validation(() => !this.RequestDate.ToString().IsValidDate(), "Request Date is invalid."));
        }

        #endregion
    }
}
