﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class AgencyCache
    {
        #region Members

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Payor { get; set; }
        public bool IsDeprecated { get; set; }

        #endregion

        //#region Domain
      
       
        //public string AddressLine1 { get; set; }
        //public string AddressLine2 { get; set; }
        //public string AddressCity { get; set; }
        //public string AddressStateCode { get; set; }
        //public string AddressZipCode { get; set; }
        //public string AddressZipCodeFour { get; set; }
        //public string Phone { get; set; }
        

        //#endregion

       
    }
}
