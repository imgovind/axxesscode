﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;

    public class ShpDataBatch
    {
        public Guid Id { get; set; }
        public int Status { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
        public string SubmissionFormat { get; set; }

        public override string ToString()
        {
            return new StringBuilder()
                .AppendFormat("Id: {0} ", Id)
                .AppendFormat("AgencyId: {0} ", AgencyId)
                .AppendFormat("PatientId: {0} ", PatientId)
                .AppendFormat("Status: {0} ", Status)
                .AppendFormat("Created: {0} ", Created.ToZeroFilled())
                .AppendFormat("Modified: {0}", LastModified.ToZeroFilled())
                .ToString();
        }
    }
}
