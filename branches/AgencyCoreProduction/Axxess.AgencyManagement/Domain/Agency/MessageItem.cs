﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Enums;

    public class MessageItem
    {
        public Guid Id { get; set; }
        public Guid UserMessageId { get; set; }
        public string Subject { get; set; }
        public string FromName { get; set; }
        public bool MarkAsRead { get; set; }
        public DateTime Created { get; set; }
        public MessageType Type { get; set; }
        public string Date { get { return string.Format("{0: MMM d, yyyy hh:mm tt}", this.Created); } }
        public Guid FolderId { get; set; }
    }
}
