﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using System.Xml.Serialization;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class Infection : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid UserId { get; set; }
        public Guid PhysicianId { get; set; }
        public int Status { get; set; }
        public Guid EpisodeId { get; set; }
        public string InfectionType { get; set; }
        public string InfectionTypeOther { get; set; }
        public string Treatment { get; set; }
        public string Orders { get; set; }
        public string FollowUp { get; set; }
        public string MDNotified { get; set; }
        public string TreatmentPrescribed { get; set; }
        public string NewOrdersCreated { get; set; }
        public DateTime InfectionDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }
        public bool IsDeprecated { get; set; }
        #endregion

        #region Domain
        [SubSonicIgnore]
        public List<string> InfectionTypeArray { get; set; }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        [SubSonicIgnore]
        public string PhysicianName { get; set; }

        [SubSonicIgnore]
        public string InfectionDateFormatted { get { return InfectionDate.ToShortDateString(); } }

        [SubSonicIgnore]
        public string EpisodeStartDate { get; set; }

        [SubSonicIgnore]
        public string EpisodeEndDate { get; set; }

        //[SubSonicIgnore]
        //public LocationPrintProfile LocationProfile { get; set; }

        //[SubSonicIgnore]
        //public Patient Patient { get; set; }

        [SubSonicIgnore]
        public string PrintUrl { get; set; }

        [SubSonicIgnore]
        public Dictionary<string, string> Diagnosis { get; set; }

        [SubSonicIgnore]
        public string StatusComment { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.PatientId.IsEmpty(), "Patient is required. "));
            AddValidationRule(new Validation(() => this.EpisodeId.IsEmpty(), "Episode is required. "));
            AddValidationRule(new Validation(() => !InfectionDate.IsValid(), "Infection Date is not valid."));
        }

        #endregion
    }
}
