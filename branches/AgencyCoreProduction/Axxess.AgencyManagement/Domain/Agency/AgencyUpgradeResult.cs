﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class AgencyUpgradeResult
    {
        public bool IsSuccessful { get; set; }
        public string ErrorMessage { get; set; }
    }
}
