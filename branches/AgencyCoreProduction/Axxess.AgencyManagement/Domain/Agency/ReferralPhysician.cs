﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core.Extension;

    public class ReferalPhysician
    {
        public Guid Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Credentials { get; set; }
        public string WorkPhone { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public bool IsPrimary { get; set; }
    }
}
