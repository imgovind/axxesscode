﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class AgencyLocationLean : IdBase
    {
        #region Members
        public Guid AgencyId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneWork { get; set; }
        public string FaxNumber { get; set; }
        public string Payor { get; set; }
        public bool IsLocationStandAlone { get; set; }
        #endregion

        #region Domain
        public string AgencyName { get; set; }
        public string PhoneWorkFormatted { get { return PhoneWork.ToPhone(); } }
        public string FaxNumberFormatted { get { return FaxNumber.ToPhone(); } }
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());

            }
        }
        public string AddressFull
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1} {2}, {3} {4}", this.AddressLine1.Trim(), this.AddressLine2.Trim(), this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return string.Format("{0} {1}, {2} {3}", this.AddressLine1.Trim(), this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
                }
                return string.Empty;
            }
        }
        #endregion

        public override string ToString()
        {
            return this.Name;
        }

        protected override void AddValidationRules()
        {
            throw new NotImplementedException();
        }
    }
}
