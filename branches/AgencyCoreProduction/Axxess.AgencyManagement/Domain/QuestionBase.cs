﻿
namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;
    [XmlRoot()]
    public class QuestionBase
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Answer { get; set; }
    }
}
