﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain
{
    using Axxess.Core.Extension;

    public class UserJson
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public int EmploymentType { get; set; }
        public DateTime Created { get; set; }
        public string Comments { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public int Status { get; set; }

        public UserJson(User user)
        {
            this.Id = user.Id;
            this.Name = user.DisplayName;
            this.Title = user.DisplayTitle;
            this.EmploymentType = user.EmploymentType == "Employee" ? 1 : 2;
            this.Created = user.Created;
            this.Comments = user.Comments;
            this.Status = user.Status;
            var userProfile = user.ProfileData.IsNotNullOrEmpty() ? user.ProfileData.ToObject<UserProfile>() : null;
            if (userProfile != null)
            {
                this.Gender = userProfile.Gender;
                this.Email = userProfile.EmailWork;
                this.Phone = userProfile.PhoneHome;
                this.Mobile = userProfile.PhoneMobile;
            }
        }
    }
}
