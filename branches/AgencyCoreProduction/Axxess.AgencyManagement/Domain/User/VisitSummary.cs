﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    public class VisitSummary
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int VisitCount { get; set; }
        public int TotalVisitTime { get; set; }
        public int TotalTravelTime { get; set; }
        public string Credential { get; set; }
    }
}
