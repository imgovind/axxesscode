﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class UserSelection
    {
        public Guid Id { get; set; }
        public Guid LoginId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Credential { get; set; }
        public string LoginCreated { get; set; }
        public string DisplayName { get; set; }
        public bool IsLoginActive { get; set; }
    }
}
