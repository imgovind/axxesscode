﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core.Extension;
    using System.Xml.Serialization;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Enums;

    public class UserVisit
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public Guid UserId { get; set; }
        public string EpisodeNotes { get; set; }
        public string StatusComment { get; set; }
        public string VisitNotes { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public int DisciplineTask { get; set; }
        public string TaskName { get; set; }
        public bool IsComplete { get; set; }
        public string ScheduleDate { get; set; }
        public string VisitDate { get; set; }
        public string VisitRate { get; set; }
        public string MileageRate { get; set; }
        public string Surcharge { get; set; }
        public string StatusName { get; set; }
        public string Status{ get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string UserDisplayName { get; set; }
        public bool IsMissedVisit { get; set; }
        public bool IsVisitPaid { get; set; }
        public bool IsVisitVerified { get; set; }
        public string PaidDate { get; set; }
        public string AssociatedMileage { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string TravelTimeIn { get; set; }
        public string TravelTimeOut { get; set; }
        public string VisitPayment { get; set; }
        public string TotalPayment { get; set; }
        public double Total { get; set; }
        public double MileagePayment { get; set; }
        public int PrimaryInsurance { get; set; }
        public string Insurance { get; set; }
        public string PayType { get; set; }
        [XmlIgnore]
        public int MinSpent
        {
            get
            {
                var timeIn = DateTime.Now;
                var timeOut = DateTime.Now;
                if (this.TimeOut.IsNotNullOrEmpty() && this.TimeOut.HourToDateTime(ref timeOut) && this.TimeIn.IsNotNullOrEmpty() && this.TimeIn.HourToDateTime(ref timeIn))
                {
                    if (timeOut >= timeIn)
                    {
                        return (timeOut.Hour * 60 + timeOut.Minute) - (timeIn.Hour * 60 + timeIn.Minute);
                    }
                    else
                    {
                        return 24 * 60 - (timeIn.Hour * 60 + timeIn.Minute) + (timeOut.Hour * 60 + timeOut.Minute);
                    }
                    //if (!timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")))
                    //{
                    //    var outi = default(int);
                    //    if (timeIn.Hour > 12) { outi = 24 * 60 - timeIn.Hour * 60 - (timeIn.Minute); } else { outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute); }
                    //    var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                    //    return outi + outO;
                    //}
                    //else if (timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")) && (timeOut.Hour * 60 + timeOut.Minute) < (timeIn.Hour * 60 + timeIn.Minute))
                    //{
                    //    var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                    //    var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                    //    return outi + outO + 12 * 60;
                    //}
                    //else
                    //{
                    //    if (timeOut >= timeIn)
                    //    {
                    //        return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);

                    //    }

                    //}
                }
                return 0;
            }
        }

        [XmlIgnore]
        public bool IsMissedVisitReady { get { return this.ScheduleDate.IsNotNullOrEmpty() && this.ScheduleDate.IsValidDate() && this.ScheduleDate.ToDateTime().Date <= DateTime.Now.Date ? true : false; } }

        [XmlIgnore]
        public int TravelMinSpent
        {
            get
            {
                var timeIn = DateTime.Now;
                var timeOut = DateTime.Now;
                if (this.TravelTimeOut.IsNotNullOrEmpty() && this.TravelTimeOut.HourToDateTime(ref timeOut) && this.TravelTimeIn.IsNotNullOrEmpty() && this.TravelTimeIn.HourToDateTime(ref timeIn))
                {
                    if (!timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")))
                    {
                        var outi = default(int);
                        if (timeIn.Hour > 12) { outi = 24 * 60 - timeIn.Hour * 60 - (timeIn.Minute); } else { outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute); }
                        var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                        return outi + outO;
                    }
                    else if (timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")) && (timeOut.Hour * 60 + timeOut.Minute) < (timeIn.Hour * 60 + timeIn.Minute))
                    {
                        var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                        var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                        return outi + outO + 12 * 60;
                    }
                    else
                    {
                        if (timeOut >= timeIn)
                        {
                            return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);

                        }
                    }
                }
                return 0;
            }
        }

        [XmlIgnore]
        public string DisciplineTaskName
        {
            get
            {
                if (Enum.IsDefined(typeof(DisciplineTasks), this.DisciplineTask)) { return ((DisciplineTasks)this.DisciplineTask).GetDescription(); } else { return string.Empty; };
            }
        }
        
    }
}
