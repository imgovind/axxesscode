﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;
    public class User : IdBase
    {
        #region Members

        public int Status { get; set; }
        public string Roles { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        public string CustomId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Credentials { get; set; }
        public string TitleType { get; set; }
        public string Permissions { get; set; }
        public string Licenses { get; set; }
        public string Rates { get; set; }
        public string ProfileData { get; set; }
        public string Messages { get; set; }
        public string EmploymentType { get; set; }
        public Guid AgencyLocationId { get; set; }      
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }
        public bool AllowWeekendAccess { get; set; }
        public string EarliestLoginTime { get; set; }
        public string AutomaticLogoutTime { get; set; }
        public string TitleTypeOther { get; set; }
        public string CredentialsOther { get; set; }
        public string Comments { get; set; }
        public string SSN { get; set; }
        public DateTime DOB { get; set; }
        public bool IsPrimary { get; set; }
        public DateTime AccountExpireDate { get; set; }
        public DateTime HireDate { get; set; }
        public DateTime TerminationDate { get; set; }


        #endregion

        #region Domain
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                if (this.Status > 0)
                {
                    return ((UserStatus)Enum.ToObject(typeof(UserStatus), this.Status)).GetDescription();
                }
                return "";
            }
        }

        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                var nameBuilder = new StringBuilder();

                if (Credentials.IsEqual("None"))
                {
                    if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                    if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                    if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.Suffix);
                    if (IsDeprecated) nameBuilder.Append(" [deleted]");

                    return nameBuilder.ToString().TrimEnd();
                }

                if (CredentialsOther.IsNotNullOrEmpty()&&Credentials=="Other")
                {
                    if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                    if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                    if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.Suffix);
                    if (this.CredentialsOther.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.CredentialsOther);
                    if (IsDeprecated) nameBuilder.Append(" [deleted]");
                    return nameBuilder.ToString().TrimEnd();
                }

                if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.Suffix);
                if (this.Credentials.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.Credentials);
                if (IsDeprecated) nameBuilder.Append(" [deleted]");
                return nameBuilder.ToString().TrimEnd();
            }
        }

        
       

        [SubSonicIgnore]
        public string DisplayTitle
        {
            get
            {
                if (TitleTypeOther.IsNotNullOrEmpty() && TitleType == "Other")
                {
                    return this.TitleTypeOther;
                }

                
                    return this.TitleType;  
            }
            

        }

        [SubSonicIgnore]
        public string AgencyName { get; set; }

        [SubSonicIgnore]
        public UserProfile Profile { get; set; }

        [SubSonicIgnore]
        public List<string> AgencyRoleList { get; set; }

        [SubSonicIgnore]
        public List<Guid> LocationList { get; set; }

        [SubSonicIgnore]
        public List<MessageState> MessageList { get; set; }

        [SubSonicIgnore]
        public UserStatus UserStatus { get; set; }

        [SubSonicIgnore]
        public string EmailAddress { get; set; }

        [SubSonicIgnore]
        public string HomePhone
        {
            get
            {
                if (this.Profile != null && this.Profile.PhoneHome.IsNotNullOrEmpty())
                {
                    return this.Profile.PhoneHome.ToPhone();
                }
                return string.Empty;
            }
        }

        [SubSonicIgnore]
        public string MobilePhone
        {
            get
            {
                if (this.Profile != null && this.Profile.PhoneMobile.IsNotNullOrEmpty())
                {
                    return this.Profile.PhoneMobile.ToPhone();
                }
                return string.Empty;
            }
        }

        [SubSonicIgnore]
        public List<string> HomePhoneArray { get; set; }
        [SubSonicIgnore]
        public List<string> MobilePhoneArray { get; set; }
        [SubSonicIgnore]
        public List<string> FaxPhoneArray { get; set; }
        [SubSonicIgnore]
        public List<string> PermissionsArray { get; set; }
        [SubSonicIgnore]
        public List<License> LicensesArray { get; set; }
        [SubSonicIgnore]
        public List<UserRate> RatesArray { get; set; }

        [SubSonicIgnore]
        public List<UserNonVisitTaskRate> NonVisitRatesArray { get; set; }
        [SubSonicIgnore]
        public List<MessageState> SystemMessages { get; set; }

        [SubSonicIgnore]
        public PasswordChange PasswordChanger { get; set; }
        [SubSonicIgnore]
        public SignatureChange SignatureChanger { get; set; }

        [SubSonicIgnore]
        public string UserName { get; set; }

        [SubSonicIgnore]
        public string LoginCreated { get; set; }

        #endregion

        public override string ToString()
        {
            return this.DisplayName;
        }

        #region Validation Rules

        protected override void AddValidationRules()
        {
            if (this.Id.IsEmpty())
            {
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.EmailAddress), "User E-mail is required.  <br />"));
                AddValidationRule(new Validation(() => !this.EmailAddress.IsEmail(), "E-mail is not in a valid  format.  <br />"));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "User First Name is required. <br />"));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "User Last Name is required.  <br />"));
            }
            else
            {
                if (this.PasswordChanger != null && this.PasswordChanger.CurrentPassword.IsNotNullOrEmpty() && this.PasswordChanger.NewPassword.IsNotNullOrEmpty() && this.PasswordChanger.NewPasswordConfirm.IsNotNullOrEmpty())
                {
                    AddValidationRule(new Validation(() => this.PasswordChanger.NewPassword.Length < 8, "The minimum password length is 8 characters. <br/>"));
                    AddValidationRule(new Validation(() => !this.PasswordChanger.NewPassword.IsEqual(this.PasswordChanger.NewPasswordConfirm), "The passwords you have entered do not match."));
                }

                if (this.SignatureChanger != null && this.SignatureChanger.CurrentSignature.IsNotNullOrEmpty() && this.SignatureChanger.NewSignature.IsNotNullOrEmpty() && this.SignatureChanger.NewSignatureConfirm.IsNotNullOrEmpty())
                {
                    AddValidationRule(new Validation(() => this.SignatureChanger.NewSignature.Length < 8, "The minimum signature length is 8 characters.  <br/>"));
                    AddValidationRule(new Validation(() => !this.SignatureChanger.NewSignature.IsEqual(this.SignatureChanger.NewSignatureConfirm), "The signatures you have entered do not match."));
                }
            }
        }

        #endregion
    }
}
