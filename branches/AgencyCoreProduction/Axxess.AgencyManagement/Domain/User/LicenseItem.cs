﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;

    public class LicenseItem
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid AssetId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string LicenseType { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string OtherLicenseType { get; set; }
        [SubSonicIgnore]
        public string AssetUrl
        {
            get
            {
                if (!this.AssetId.IsEmpty())
                {
                    return string.Format("<a href=\"/Asset/{0}\">{1}</a>&#160;", this.AssetId.ToString(), this.LicenseType);
                }
                return this.LicenseType;
            }
        }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        public Guid UserId { get; set; }
        [SubSonicIgnore]
        public string IsUser
        {
            get
            {
                return this.UserId.IsEmpty() ? "No" : "Yes";
            }
        }
        [SubSonicIgnore]
        public string IssueDateFormatted { get { return this.IssueDate.ToString("MM/dd/yyyy"); } }
        [SubSonicIgnore]
        public string ExpireDateFormatted { get { return this.ExpireDate.ToString("MM/dd/yyyy"); } }

        #endregion
    }
}
