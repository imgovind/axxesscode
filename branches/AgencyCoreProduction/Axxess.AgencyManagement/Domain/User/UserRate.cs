﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel.DataAnnotations;

    using Axxess.AgencyManagement.Enums;
   
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;

    [XmlRoot()]
    public class UserRate : EntityBase
    {
        #region Members

        [XmlElement]
        [UIHint("DisciplineTask")]
        public int Id { get; set; }
        [XmlElement]
        public string Insurance { get; set; }
        [XmlElement]
        public int RateType { get; set; }
        [XmlElement]
        public double Rate { get; set; }
        [XmlElement]
        public double MileageRate { get; set; }

        #endregion

        #region Domain

        [XmlIgnore]
        public Guid UserId { get; set; }
        [XmlIgnore]
        public string DisciplineTaskName
        {
            get
            {
                return Id.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetDescription();
            }
        }
        [XmlIgnore]
        public string RateTypeName
        {
            get
            {
                return RateType > 0 ? RateType.ToEnum<UserRateTypes>(UserRateTypes.None).GetDescription() : string.Empty;
            }
        }
        [XmlIgnore]
        public string InsuranceName { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.Id <= 0, "Task is required."));
            AddValidationRule(new Validation(() => this.Rate < 0, "Rate is required."));
            AddValidationRule(new Validation(() => this.RateType < 0, "Rate Type is required."));
            AddValidationRule(new Validation(() => this.Insurance.IsNullOrEmpty(), "Insurance is required."));
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            if (this.Rate > 0)
            {
                if (this.RateType == (int)UserRateTypes.Hourly)
                {
                    return string.Format("${0:#0.00} / hr", this.Rate);
                }
                else if (this.RateType == (int)UserRateTypes.PerVisit)
                {
                    return string.Format("${0:#0.00} / visit", this.Rate);
                }
            }
            return string.Empty;
        }

        #endregion
    }
}
