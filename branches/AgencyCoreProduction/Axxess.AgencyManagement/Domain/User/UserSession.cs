﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;
    using System.Xml.Serialization;

    [Serializable]
    public sealed class UserSession
    {
        public Guid UserId { get; set; }
        public Guid LoginId { get; set; }
        public int LoginDay { get; set; }
        public Guid AgencyId { get; set; }
        public string Address { get; set; }
        public bool IsPrimary { get; set; }
        public string FullName { get; set; }
        public string SessionId { get; set; }
        public string AgencyName { get; set; }
        public string DisplayName { get; set; }
        public string AgencyRoles { get; set; }
        public string EmailAddress { get; set; }
        public bool IsAgencyFrozen { get; set; }
        public bool OasisVendorExist { get; set; }
        public string ImpersonatorName { get; set; }
        public bool HasMultipleAccounts { get; set; }
        public string EarliestLoginTime { get; set; }
        public string AutomaticLogoutTime { get; set; }
        public DateTime AccountExpirationDate { get; set; }

        public string Payor { get; set; }
       
        public IDictionary<Guid, Location> Locations { get; set; }
    }
    public class Location
    {
        public bool IsMainOffice { get; set; }
        public bool IsStandAlone { get; set; }
        public string Payor { get; set; }
    }
}
