﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain
{
   public class GridTask
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string UserName { get; set; }
        public string PatientName { get; set; }
        public string Discipline { get; set; }
        public int DisciplineTask { get; set; }
        public bool IsMissedVisit { get; set; }
        public bool IsAllDay { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime DateOut { get; set; }
        public string Range { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public string StatusName { get; set; }
        public string TaskName { get; set; }

        public string EpisodeNotes { get; set; }
        public string ReturnReason { get; set; }
        public string Comments { get; set; }
        public string StatusComment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsComplete { get; set; }
        public bool IsOrphaned { get; set; }
        public string Type { get; set; }
        public string Group { get; set; }
        public bool IsInQA { get; set; }
    }
}
