﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Runtime.Serialization;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    [DataContract(Namespace = "http://api.axxessweb.com/Report/2011/11/")]
    public class VisitsByPayor
    {
        public VisitsByPayor() { }
        public VisitsByPayor(int id) 
        {
            InsuranceId = id;
        }

        public VisitsByPayor(int id, string name) 
        {
            InsuranceId = id;
            InsuranceName = name;
        }

        public string InsuranceName { get; set; }
        public int InsuranceId { get; set; }
        public int RN { get; set; }
        public int LPNLVN { get; set; }
        public int PT { get; set; }
        public int PTA { get; set; }
        public int OT { get; set; }
        public int COTA { get; set; }
        public int ST { get; set; }
        public int MSW { get; set; }
        public int Dietician { get; set; }
        public int HHA { get; set; }
        public int PCW { get; set; }
        public int HMK { get; set; }
        public int Total { get; set; }
    }
}
