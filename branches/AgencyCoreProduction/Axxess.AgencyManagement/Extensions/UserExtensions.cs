﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;

    public static class UserExtensions
    {
        public static List<UserSelection> ForSelection(this IList<User> users)
        {
            var result = new List<UserSelection>();

            if (users != null && users.Count > 0)
            {
                users.ForEach(p => result.Add(new UserSelection { Id = p.Id, FirstName = p.FirstName, LastName = p.LastName, DisplayName = p.DisplayName, }));
            }

            return result.OrderBy(u => u.DisplayName).ToList();
        }

        public static bool HasTrialAccountExpired(this User user)
        {
            var result = false;

            if (user != null && user.AccountExpireDate != DateTime.MinValue)
            {
                if (user.AccountExpireDate < DateTime.Today)
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool EarliestAccessible(this User user)
        {
            var result = false;
            if (user.EarliestLoginTime.IsNotNullOrEmpty())
            {
                var earliestTime = new Time(user.EarliestLoginTime);
                if (earliestTime.TimeOfDay <= DateTime.Now.TimeOfDay)
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public static bool AllowWeekendAccess(this User user)
        {
            return user != null && (!DateTime.Today.IsWeekend() || user.AllowWeekendAccess);
        }

        public static bool IsInRoles(this string roles, params AgencyRoles[] rolesToCheckFor)
        {
            if (roles.IsNotNullOrEmpty())
            {
                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    return rolesToCheckFor.Any(a => agencyRoles.Contains(((int)a).ToString(CultureInfo.InvariantCulture)));
                }
            }
            return false;
        }

        public static bool IsAgencyAdmin(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.Administrator);
        }

        public static bool IsDirectorOfNursing(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.DoN);
        }

        public static bool IsCaseManager(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.CaseManager);
        }

        public static bool IsAuditor(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.Auditor);
        }

        public static bool IsNurse(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.Nurse);
        }

        public static bool IsClinician(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.Nurse, AgencyRoles.PhysicalTherapist, AgencyRoles.OccupationalTherapist, AgencyRoles.SpeechTherapist);
        }

        public static bool IsClinicianOrHHA(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.Nurse, AgencyRoles.PhysicalTherapist, AgencyRoles.OccupationalTherapist, AgencyRoles.SpeechTherapist, AgencyRoles.HHA, AgencyRoles.MedicalSocialWorker);
        }

        public static bool IsHHA(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.HHA);
        }

        public static bool IsPT(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.PhysicalTherapist);
        }

        public static bool IsOT(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.OccupationalTherapist);
        }

        public static bool IsBiller(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.Biller);
        }

        public static bool IsScheduler(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.Scheduler);
        }

        public static bool IsClerk(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.Clerk);
        }

        public static bool IsQA(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.QA);
        }

        public static bool IsOfficeManager(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.OfficeManager);
        }

        public static bool IsCommunityLiason(this string roles)
        {
            return IsInRoles(roles, AgencyRoles.CommunityLiasonOfficer);
        }

        public static bool IsRole(this string roles, AgencyRoles role)
        {
            return IsInRoles(roles, role);
        }
    }
}
