﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;
    using Axxess.Core.Extension;

    public static class PatientExtensions
    {
        public static List<PatientSelection> ForSelection(this IList<Patient> patients)
        {
            var result = new List<PatientSelection>();

            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    if (p != null && p.FirstName.IsNotNullOrEmpty() && p.LastName.IsNotNullOrEmpty())
                    {
                        result.Add(new PatientSelection { Id = p.Id, FirstName = p.FirstName.ToUpper(), LastName = p.LastName.ToUpper(), MI = p.MiddleInitial.IsNotNullOrEmpty() ? p.MiddleInitial.ToUpper() : "", IsDischarged = p.IsDischarged, PatientIdNumber = p.PatientIdNumber });
                    }
                });
            }

            return result;
        }

        public static string ToDisplayName(this PatientProfileLean patient)
        {
            if (patient != null)
            {
                return string.Concat(patient.LastName.ToUpperCase(), ", ", patient.FirstName.ToUpperCase());
            }
            return string.Empty;
        }


        
    }
}
