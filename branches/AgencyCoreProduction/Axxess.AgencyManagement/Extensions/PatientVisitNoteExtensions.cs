﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;

    using Enums;
    using Domain;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;

    public static class PatientVistNoteExtensions
    {
        public static bool IsSkilledNurseNote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (IsSkilledNurseNote(note.NoteType))
                {
                    result = true;
                }
            }

            return result;
        }

        private static bool IsSkilledNurseNote(string noteType)
        {
            var result = false;

            switch (noteType)
            {
                case "SkilledNurseVisit":
                case "SNInsulinAM":
                case "SNInsulinPM":
                case "SNInsulinNoon":
                case "SNInsulinHS":
                case "FoleyCathChange":
                case "SNB12INJ":
                case "SNBMP":
                case "SNCBC":
                case "SNHaldolInj":
                case "PICCMidlinePlacement":
                case "PRNFoleyChange":
                case "PRNSNV":
                case "PRNVPforCMP":
                case "PTWithINR":
                case "PTWithINRPRNSNV":
                case "SkilledNurseHomeInfusionSD":
                case "SkilledNurseHomeInfusionSDAdditional":
                case "SNAssessment":
                case "SNDC":
                case "SNEvaluation":
                case "SNFoleyLabs":
                case "SNFoleyChange":
                case "LVNVisit":
                case "SNInjection":
                case "SNInjectionLabs":
                case "SNLabsSN":
                case "SNVPsychNurse":
                case "SNVwithAideSupervision":
                case "SNVDCPlanning":
                case "SNVTeachingTraining":
                case "SNVManagementAndEvaluation":
                case "SNVObservationAndAssessment":
                case "SNPsychAssessment":
                case "SNWoundCare":
                    result = true;
                    break;
                default:
                    break;
            }
            return result;
        }

        public static bool IsHHANote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (note.NoteType.IsEqual("HHAideVisit"))
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsPASNote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (note.NoteType.IsEqual(DisciplineTasks.PASVisit.ToString()))
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsPTNote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (note.NoteType.IsEqual("PTVisit") || note.NoteType.IsEqual("PTAVisit"))
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsSTNote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (note.NoteType.IsEqual("STVisit"))
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsOTNote(this PatientVisitNote note)
        {
            var result = false;

            if (note != null)
            {
                if (note.NoteType.IsEqual("OTVisit") || note.NoteType.IsEqual("COTAVisit"))
                {
                    result = true;
                }
            }

            return result;
        }

        public static IDictionary<string, NotesQuestion> ToDictionary(this PatientVisitNote patientVisitNote)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null && patientVisitNote.Note != null)
            {
                var noteQuestions = patientVisitNote.Note.ToObject<List<NotesQuestion>>();
                if (noteQuestions.IsNotNullOrEmpty())
                {
                    noteQuestions.ForEach(
                        n =>
                        {
                            if (!questions.ContainsKey(n.Name)) questions.Add(n.Name, n);
                        });
                }
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToDictionary(this List<NotesQuestion> noteQuestions)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (noteQuestions != null && noteQuestions.Count > 0)
            {
                noteQuestions.ForEach(n =>
                {
                    questions.Add(n.Name, n);
                });
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToWoundCareDictionary(this PatientVisitNote patientVisitNote)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            try
            {
                if (patientVisitNote != null && patientVisitNote.WoundNote.IsNotNullOrEmpty())
                {
                    var noteQuestions = patientVisitNote.WoundNote.ToObject<List<NotesQuestion>>();
                    noteQuestions.ForEach(n =>
                    {
                        questions.Add(n.Name, n);
                    });
                }
            }
            catch (Exception ex)
            {
                return questions;
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToHHADefaults(this PatientVisitNote patientVisitNote)
        {
            var vitalSigns = new Dictionary<string, NotesQuestion>();
            if (patientVisitNote != null)
            {
                var questions = patientVisitNote.ToDictionary();

                var names = new string[] { "HHAFrequency", "PrimaryDiagnosis", "PrimaryDiagnosis1"
                    , "DNR", "IsDiet", "Diet", "Allergies", "AllergiesDescription", "IsVitalSignParameter"
                    , "SystolicBPGreaterThan", "DiastolicBPGreaterThan", "PulseGreaterThan", "RespirationGreaterThan"
                    , "TempGreaterThan", "WeightGreaterThan", "SystolicBPLessThan", "DiastolicBPLessThan"
                    , "PulseLessThan", "RespirationLessThan", "TempLessThan", "WeightLessThan" };

                names.ForEach(name =>
                {
                    if (questions.ContainsKey(name) && questions[name] != null)
                    {
                        vitalSigns.Add(name, questions[name]);
                    }
                });
            }
            return vitalSigns;
        }

        public static string AnswerOrEmptyString(this IDictionary<string, NotesQuestion> questions, string key)
        {
            string answer = string.Empty;
            if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty())
            {
                answer = questions[key].Answer.Unclean();
                if (key.IsEqual("ICD9M") || key.IsEqual("ICD9M1"))
                {
                    answer = answer.TrimEndPeriod();
                }
            }
            return answer;
        }

        public static string AnswerForDropDown(this IDictionary<string, NotesQuestion> questions, string key, Dictionary<string, string> possibleAnswers)
        {
            string answer = string.Empty;
            if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty())
            {
                answer = questions[key].Answer.Unclean();
                foreach (KeyValuePair<string, string> pa in possibleAnswers)
                {
                    if (answer.Equals(pa.Key))
                    {
                        answer = pa.Value;
                        break;
                    }
                }
            }
            return answer;
        }
    }
}
