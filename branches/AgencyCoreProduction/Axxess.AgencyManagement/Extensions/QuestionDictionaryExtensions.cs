﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    public static class QuestionDictionaryExtensions
    {

        public static void AddIfNotEmpty<T>(this IDictionary<string, T> to, IDictionary<string, T> from, string key) where T : IQuestion, new()
        {
            var value = from.Get(key);
            if (value != null && value.Answer.IsNotNullOrEmpty())
            {
                to.Add(key, value);
            }
        }

        public static void AddIfNotEmpty<T>(this IList<string> to, IDictionary<string, T> from, string key, string prefix) where T : IQuestion, new()
        {
            var value = from.AnswerOrEmptyString(key);
            if (value.IsNotNullOrEmpty())
            {
                to.Add(prefix + value);
            }
        }

        public static string AnswerOrEmptyString<T>(this IDictionary<string, T> questions, string key)
            where T : IQuestion, new()
        {
            string answer = string.Empty;
            if (questions != null)
            {
                var question = questions.Get(key);
                if (question != null && question.Answer.IsNotNullOrEmpty())
                {
                    answer = question.Answer;
                }
            }
            return answer;
        }

        //public static string AnswerOrEmptyString(this IDictionary<string, NotesQuestion> questions, string key)
        //{
        //    string answer = string.Empty;
        //    if (questions != null)
        //    {
        //        var question = questions.Get(key);
        //        if (question != null && question.Answer.IsNotNullOrEmpty())
        //        {
        //            answer = question.Answer.Unclean();
        //            if (key.IsEqual("ICD9M") || key.IsEqual("ICD9M1"))
        //            {
        //                answer = answer.TrimEndPeriod();
        //            }
        //        }
        //    }
        //    return answer;
        //}

        public static Guid AnswerOrEmptyGuid<T>(this IDictionary<string, T> questions, string key)
            where T : IQuestion, new()
        {
            if (questions != null && questions.Count > 0)
            {
                var question = questions.Get(key);
                if (question != null && question.Answer.IsNotNullOrEmpty())
                {
                    return question.Answer.ToGuid();
                }
            }
            return Guid.Empty;
        }


        public static string AnswerValidDateOrEmptyString<T>(this IDictionary<string, T> questions, string key)
            where T : IQuestion, new()
        {
            string answer = questions.AnswerOrEmptyString(key);
            return answer.IsNotNullOrEmpty() && answer.IsValidDateAndNotMin() ? answer : string.Empty;
        }

        public static DateTime AnswerOrMinDate<T>(this IDictionary<string, T> questions, string key)
             where T : IQuestion, new()
        {
            string answer = questions.AnswerOrEmptyString(key);
            return answer.IsNotNullOrEmpty() ? answer.ToDateTimeOrMin() : DateTime.MinValue;
        }

        public static Double AnswerDoubleOrZero<T>(this IDictionary<string, T> questions, string key)
             where T : IQuestion, new()
        {
            if (questions != null)
            {
                var question = questions.Get(key);
                if (question != null && question.Answer.IsNotNullOrEmpty())
                {
                    return question.Answer.ToDoubleAfterNonDigitStrip();
                }
            }
            return 0.0;
        }

        public static string AnswerForDropDown<T>(this IDictionary<string, T> questions, string key, Dictionary<string, string> possibleAnswers)
             where T : IQuestion, new()
        {
            string answer = string.Empty;
            if (questions != null)
            {
                var question = questions.Get(key);
                if (question != null && question.Answer.IsNotNullOrEmpty())
                {
                    answer = question.Answer.Unclean();
                    foreach (KeyValuePair<string, string> pa in possibleAnswers)
                    {
                        if (answer.Equals(pa.Key))
                        {
                            answer = pa.Value;
                            break;
                        }
                    }
                }
            }
            return answer;
        }

        public static string AnswerOrDefault<T>(this IDictionary<string, T> questions, string key, string defaultValue)
             where T : IQuestion, new()
        {
            string answer = questions.AnswerOrEmptyString(key);
            return answer.IsNotNullOrEmpty() ? answer : defaultValue;
        }

        public static string[] AnswerArray<T>(this IDictionary<string, T> questions, string key, char separator)
             where T : IQuestion, new()
        {
            string answer = questions.AnswerOrEmptyString(key);
            return answer.IsNotNullOrEmpty() ? answer.Split(separator) : new string[] { };
        }

        public static string[] AnswerArray<T>(this IDictionary<string, T> questions, string key)
             where T : IQuestion, new()
        {
            string answer = questions.AnswerOrEmptyString(key);
            return answer.IsNotNullOrEmpty() ? answer.Split(',') : new string[] { };
        }

        public static string ToFrequencyString<T>(this IDictionary<string, T> assessmentQuestions) where T : IQuestion, new()
        {
            var frequencyData = string.Empty;
            if (assessmentQuestions != null && assessmentQuestions.Count > 0)
            {
                var frequencyList = new List<string>();
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485SNFrequency", "SN Frequency:");
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485PTFrequency", "PT Frequency:");
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485OTFrequency", "OT Frequency:");
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485STFrequency", "ST Frequency:");
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485MSWFrequency", "MSW Frequency:");
                frequencyList.AddIfNotEmpty(assessmentQuestions, "485HHAFrequency", "HHA Frequency:");
                if (frequencyList.Count > 0)
                {
                    frequencyData = frequencyList.ToArray().Join(", ") + ".";
                }
            }
            return frequencyData;
        }

        //public static string ToClaimDiagonasisCodesXml(this IDictionary<string, Question> assessmentQuestions)
        //{
        //    string diagnosis = string.Empty;
        //    if (assessmentQuestions != null && assessmentQuestions.Count > 0)
        //    {
        //        var diagnosisCodes = new DiagnosisCodes();
        //        diagnosisCodes.Primary = assessmentQuestions.AnswerOrDefault("M1020ICD9M", null);
        //        diagnosisCodes.Second = assessmentQuestions.AnswerOrDefault("M1020ICD9M1", null);
        //        diagnosisCodes.Third = assessmentQuestions.AnswerOrDefault("M1020ICD9M2", null);
        //        diagnosisCodes.Fourth = assessmentQuestions.AnswerOrDefault("M1020ICD9M3", null);
        //        diagnosisCodes.Fifth = assessmentQuestions.AnswerOrDefault("M1020ICD9M4", null);
        //        diagnosisCodes.Sixth = assessmentQuestions.AnswerOrDefault("M1020ICD9M5", null);
        //        diagnosis = diagnosisCodes.ToXml();
        //    }
        //    return diagnosis;
        //}

        public static IDictionary<string, T> ToDiagnosis<T>(this IDictionary<string, T> assessmentQuestions) where T : IQuestion, new()
        {
            var diagnosis = new Dictionary<string, T>();
            if (assessmentQuestions != null && assessmentQuestions.Count > 0)
            {
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1020PrimaryDiagnosis");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1020ICD9M");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022PrimaryDiagnosis1");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022ICD9M1");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022PrimaryDiagnosis2");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022ICD9M2");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022PrimaryDiagnosis3");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022ICD9M3");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022PrimaryDiagnosis4");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022ICD9M4");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022PrimaryDiagnosis5");
                diagnosis.AddIfNotEmpty(assessmentQuestions, "M1022ICD9M5");
            }
            return diagnosis;
        }

        public static IDictionary<string, T> ToAllergies<T>(this IDictionary<string, T> assessmentQuestions) where T : IQuestion, new()
        {
            var allergies = new Dictionary<string, T>();
            if (assessmentQuestions != null && assessmentQuestions.Count > 0)
            {
                allergies.AddIfNotEmpty(assessmentQuestions, "485Allergies");
                allergies.AddIfNotEmpty(assessmentQuestions, "485AllergiesDescription");
            }
            return allergies;
        }

        //public static string AnswerOrEmptyString(this IDictionary<string, NotesQuestion> questions, string key)
        //{
        //    string answer = string.Empty;
        //    if (questions != null && questions.ContainsKey(key) && questions[key] != null && questions[key].Answer.IsNotNullOrEmpty())
        //    {
        //        answer = questions[key].Answer.Unclean();
        //        if (key.IsEqual("ICD9M") || key.IsEqual("ICD9M1"))
        //        {
        //            answer = answer.TrimEndPeriod();
        //        }
        //    }
        //    return answer;
        //}
    }
}
