﻿namespace Axxess.AgencyManagement.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;

   public static class EntityFactory
    {

       public static Rap CreateRap(Patient patient, PatientEpisode episode, int insuranceId, AgencyPhysician agencyPhysician)
       {
           var rap = new Rap
           {
               Id = episode.Id,
               AgencyId = patient.AgencyId,
               PatientId = patient.Id,
               EpisodeId = episode.Id,
               EpisodeStartDate = episode.StartDate,
               EpisodeEndDate = episode.EndDate,
               IsFirstBillableVisit = false,
               IsOasisComplete = false,
               PatientIdNumber = patient.PatientIdNumber,
               IsGenerated = false,
               MedicareNumber = patient.MedicareNumber,
               FirstName = patient.FirstName,
               LastName = patient.LastName,
               DOB = patient.DOB,
               Gender = patient.Gender,
               AddressLine1 = patient.AddressLine1,
               AddressLine2 = patient.AddressLine2,
               AddressCity = patient.AddressCity,
               AddressStateCode = patient.AddressStateCode,
               AddressZipCode = patient.AddressZipCode,
               StartofCareDate = episode.StartOfCareDate,
               AreOrdersComplete = false,
               AdmissionSource = patient.AdmissionSource,
               PatientStatus = patient.Status,
               UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
               Status = (int)BillingStatus.ClaimCreated,
               HealthPlanId = patient.PrimaryHealthPlanId,
               Relationship = patient.PrimaryRelationship,
               DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
               ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
               Created = DateTime.Now
           };
           if (insuranceId > 0)
           {
               rap.PrimaryInsuranceId = insuranceId;
           }
           else if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
           {
               rap.PrimaryInsuranceId = patient.PrimaryInsurance.ToInteger();
           }
           if (agencyPhysician != null)
           {
               rap.PhysicianNPI = agencyPhysician.NPI;
               rap.PhysicianFirstName = agencyPhysician.FirstName;
               rap.PhysicianLastName = agencyPhysician.LastName;
           }
           //if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
           //{
           //    if (!patient.AgencyPhysicians[0].IsEmpty())
           //    {
           //        var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
           //        if (physician != null)
           //        {
           //            rap.PhysicianNPI = physician.NPI;
           //            rap.PhysicianFirstName = physician.FirstName;
           //            rap.PhysicianLastName = physician.LastName;
           //        }
           //    }
           //}
           return rap;
       }

       public static Final CreateFinal(Patient patient, PatientEpisode episode, int insuranceId, AgencyPhysician agencyPhysician)
       {
           var final = new Final
           {
               Id = episode.Id,
               AgencyId = patient.AgencyId,
               PatientId = patient.Id,
               EpisodeId = episode.Id,
               EpisodeStartDate = episode.StartDate,
               EpisodeEndDate = episode.EndDate,
               IsFirstBillableVisit = false,
               IsOasisComplete = false,
               PatientIdNumber = patient.PatientIdNumber,
               IsGenerated = false,
               MedicareNumber = patient.MedicareNumber,
               FirstName = patient.FirstName,
               LastName = patient.LastName,
               DOB = patient.DOB,
               Gender = patient.Gender,
               AddressLine1 = patient.AddressLine1,
               AddressLine2 = patient.AddressLine2,
               AddressCity = patient.AddressCity,
               AddressStateCode = patient.AddressStateCode,
               AddressZipCode = patient.AddressZipCode,
               StartofCareDate = episode.StartOfCareDate,
               AdmissionSource = patient.AdmissionSource,
               PatientStatus = patient.Status,
               UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
               AreOrdersComplete = false,
               AreVisitsComplete = false,
               Status = (int)BillingStatus.ClaimCreated,
               IsSupplyVerified = false,
               IsFinalInfoVerified = false,
               IsVisitVerified = false,
               IsRapGenerated = false,
               HealthPlanId = patient.PrimaryHealthPlanId,
               Relationship = patient.PrimaryRelationship,
               Created = DateTime.Now,
               IsSupplyNotBillable = false,
               DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
               ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
               Modified = DateTime.Now
           };
           if (insuranceId > 0)
           {
               final.PrimaryInsuranceId = insuranceId;
           }
           else if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger())
           {
               final.PrimaryInsuranceId = patient.PrimaryInsurance.ToInteger();
           }
           if (agencyPhysician != null)
           {
               final.PhysicianNPI = agencyPhysician.NPI;
               final.PhysicianFirstName = agencyPhysician.FirstName;
               final.PhysicianLastName = agencyPhysician.LastName;
           }
           //if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
           //{
           //    if (!patient.AgencyPhysicians[0].IsEmpty())
           //    {
           //        var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
           //        if (physician != null)
           //        {
           //            final.PhysicianNPI = physician.NPI;
           //            final.PhysicianFirstName = physician.FirstName;
           //            final.PhysicianLastName = physician.LastName;
           //        }
           //    }
           //}
           return final;
       }
    }
}
