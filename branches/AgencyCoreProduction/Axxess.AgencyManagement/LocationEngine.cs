﻿namespace Axxess.AgencyManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    public static class LocationEngine
    {
        #region Private Members
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
        private static Guid mainLocationId = Guid.Empty;
        #endregion

        #region Public Methods

        public static string GetName(Guid agencyId, Guid locationId)
        {
            var name = string.Empty;
            if (!locationId.IsEmpty() && !agencyId.IsEmpty())
            {
                var key = Key(agencyId, locationId);
                var locationXml = string.Empty;
                if (!Cacher.TryGet<string>(key, out locationXml))
                {
                    var location = dataProvider.AgencyRepository.GetLocationLean(agencyId, locationId);
                    if (location != null)
                    {
                        Cacher.Set(key, location.ToXml());
                        name = location.Name;
                    }
                }
                else
                {
                    var location = locationXml.ToObject<AgencyLocationLean>();
                    if (location != null)
                    {
                        name = location.Name;
                    }
                }
            }
            return name;
        }

        public static AgencyLocationLean GetMainLocation(Guid agencyId)
        {
            AgencyLocationLean result = null;
            if (agencyId.IsNotEmpty())
            {
                if (mainLocationId.IsEmpty())
                {
                    result = dataProvider.AgencyRepository.GetMainLocationLean(agencyId);
                    mainLocationId = result.Id;
                    var key = Key(agencyId, mainLocationId);
                    var locationXml = result.ToXml();
                    Cacher.Set(key, locationXml);
                }
                else
                {
                    var key = Key(agencyId, mainLocationId);
                    var locationXML = default(string);
                    if (!Cacher.TryGet(key, out locationXML))
                    {
                        result = dataProvider.AgencyRepository.GetMainLocationLean(agencyId);
                        mainLocationId = result.Id;
                        locationXML = result.ToXml();
                        Cacher.Set(key, locationXML);
                    }
                    else
                    {
                        if (locationXML.IsNotNullOrEmpty())
                        {
                            result = locationXML.ToObject<AgencyLocationLean>();
                        }
                    }
                }
            }
            return result;
        }

        public static AgencyLocationLean GetLocation(Guid agencyId, Guid locationId)
        {
            AgencyLocationLean location = null;
            if (!locationId.IsEmpty() && !agencyId.IsEmpty())
            {
                var key = Key(agencyId, locationId);
                var locationXml = Cacher.Get<string>(key);
                if (locationXml.IsNotNullOrEmpty())
                {
                    location = locationXml.ToObject<AgencyLocationLean>();
                    if (location != null)
                    {
                        return location;
                    }
                    else
                    {
                        return AddLocationLean(agencyId, locationId, key);
                    }
                }
                else
                {
                    return AddLocationLean(agencyId, locationId, key);
                }
            }
            return location;
        }

        public static List<AgencyLocationLean> GetLocations(Guid agencyId, List<Guid> locationIds)
        {
            var locations = new List<AgencyLocationLean>();
            var locationIdsNotInCache = new List<Guid>();
            if (!agencyId.IsEmpty() && locationIds != null && locationIds.Count > 0)
            {
                var keys = locationIds.Select(id => Key(agencyId, id));
                var results = Cacher.Get<string>(keys);
                if (results != null && results.Count > 0)
                {
                    results.ForEach((key, value) =>
                    {
                        if (key.IsNotNullOrEmpty() && value != null)
                        {
                            string locationXml = value.ToString();
                            if (locationXml.IsNotNullOrEmpty())
                            {
                                locations.Add(locationXml.ToObject<AgencyLocationLean>());
                            }
                        }

                    });
                }
                locationIdsNotInCache = locationIds.Where(id => !locations.Exists(u => u.Id == id)).ToList();
                if (locationIdsNotInCache != null && locationIdsNotInCache.Count > 0)
                {
                    var locationsNotInCache = dataProvider.AgencyRepository.GetLocationsLean(agencyId, locationIdsNotInCache);
                    if (locationIdsNotInCache != null && locationIdsNotInCache.Count > 0)
                    {
                        locationsNotInCache.ForEach(u =>
                        {
                            var key = Key(agencyId, u.Id);
                            Cacher.Set<string>(key, u.ToXml());
                            locations.Add(u);

                        });
                    }
                }
            }
            return locations;
        }

        public static void AddOrUpdate(Guid agencyId, AgencyLocationLean location)
        {
            if (!agencyId.IsEmpty() && location != null && !location.Id.IsEmpty())
            {
                var locationXml = location.ToXml();
                if (locationXml.IsNotNullOrEmpty())
                {
                    var key = Key(agencyId, location.Id);
                    Cacher.Set<string>(key, locationXml);
                }
            }
        }

        #endregion

        #region Private Methods

        private static AgencyLocationLean AddLocationLean(Guid agencyId, Guid locationId, string key)
        {
            var location = dataProvider.AgencyRepository.GetLocationLean(agencyId, locationId);
            if (location != null)
            {
                Cacher.Set<string>(key, location.ToXml());
            }
            return location;
        }

        private static AgencyLocation AddLocation(Guid agencyId, Guid locationId, string key)
        {
            var location = dataProvider.AgencyRepository.FindLocation(agencyId, locationId);
            if (location != null)
            {
                Cacher.Set<string>(key, location.ToXml());
            }
            return location;
        }

        private static string Key(Guid agencyId, Guid locationId)
        {
            return string.Format("{0}_{1}_{2}", agencyId, (int)CacheType.Branch, locationId);
        }

        #endregion
    }
}
