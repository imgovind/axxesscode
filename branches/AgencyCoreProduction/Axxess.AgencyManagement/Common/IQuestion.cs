﻿namespace Axxess.AgencyManagement.Domain
{
    using Axxess.AgencyManagement.Enums;

    public interface IQuestion
    {
        string Name { get; set; }
        string Answer { get; set; }
    }
}
