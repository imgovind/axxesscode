﻿namespace Axxess.AgencyManagement
{
    using System;
    using System.Threading;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Api;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Enums;

    public static class AgencyEngine
    {
        #region Private Members
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
        #endregion

        #region Public Methods

        public static string GetName(Guid agencyId)
        {
            var name = string.Empty;
            if (!agencyId.IsEmpty())
            {
                var key = Key(agencyId);
                var agencyXml = string.Empty;
                if (!Cacher.TryGet<string>(key, out agencyXml))
                {
                    var agency = dataProvider.AgencyRepository.AgencyCache(agencyId);
                    if (agency != null)
                    {
                        Cacher.Set(key, agency.ToXml());
                        name = agency.Name;
                    }
                }
                else
                {
                    var agency = agencyXml.ToObject<AgencyCache>();
                    if (agency != null)
                    {
                        name = agency.Name;
                    }
                }
            }
            return name;
        }


        public static AgencyCache GetLocation(Guid agencyId)
        {
            AgencyCache location = null;
            if (!agencyId.IsEmpty())
            {
                var key = Key(agencyId);
                var agencyXml = Cacher.Get<string>(key);
                if (agencyXml.IsNotNullOrEmpty())
                {
                    location = agencyXml.ToObject<AgencyCache>();
                    if (location != null)
                    {
                        return location;
                    }
                    else
                    {
                        return AddAgencyLean(agencyId,  key);
                    }
                }
                else
                {
                    return AddAgencyLean(agencyId, key);
                }
            }
            return location;
        }

        public static List<AgencyCache> GetAgencies(List<Guid> agencyIds)
        {
            var agencies = new List<AgencyCache>();
            var agencyIdsNotInCache = new List<Guid>();
            if (agencyIds.IsNotNullOrEmpty())
            {
                var keys = agencyIds.Select(id => Key(id));
                var results = Cacher.Get<string>(keys);
                if (results.IsNotNullOrEmpty())
                {
                    results.ForEach((key, value) =>
                    {
                        if (key.IsNotNullOrEmpty() && value != null)
                        {
                            string agencyXml = value.ToString();
                            if (agencyXml.IsNotNullOrEmpty())
                            {
                                agencies.Add(agencyXml.ToObject<AgencyCache>());
                            }
                        }

                    });
                }
                agencyIdsNotInCache = agencyIds.Where(id => !agencies.Exists(u => u.Id == id)).ToList();
                if (agencyIdsNotInCache.IsNotNullOrEmpty())
                {
                    var agenciesNotInCache = dataProvider.AgencyRepository.AgencyCaches(agencyIdsNotInCache);
                    if (agencyIdsNotInCache.IsNotNullOrEmpty())
                    {
                        agenciesNotInCache.ForEach(u =>
                        {
                            var key = Key(u.Id);
                            Cacher.Set<string>(key, u.ToXml());
                            agencies.Add(u);

                        });
                    }
                }
            }
            return agencies;
        }

        public static void AddOrUpdate(Guid agencyId, AgencyCache agency)
        {
            if (!agencyId.IsEmpty() && agency != null && !agency.Id.IsEmpty())
            {
                var agencyXml = agency.ToXml();
                if (agencyXml.IsNotNullOrEmpty())
                {
                    var key = Key(agencyId);
                    Cacher.Set<string>(key, agencyXml);
                }
            }
        }

        #endregion

        #region Private Methods

        private static AgencyCache AddAgencyLean(Guid agencyId, string key)
        {
            var agency = dataProvider.AgencyRepository.AgencyCache(agencyId);
            if (agency != null)
            {
                Cacher.Set<string>(key, agency.ToXml());
            }
            return agency;
        }

       

        private static string Key(Guid agencyId)
        {
            return string.Format("{0}_{1}", agencyId, (int)CacheType.Agency);
        }

        #endregion

    }
}
