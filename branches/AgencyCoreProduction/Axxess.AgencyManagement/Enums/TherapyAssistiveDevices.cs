﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum TherapyAssistiveDevices 
    {
        [Description("Rolling walker")]
        RollingWalker = 1,
        [Description("Standard walker")]
        StandardWalker = 2,
        [Description("Platform walker")]
        PlatformWalker = 3,
        [Description("4 wheeled walker")]
        FourWheeledWalker = 4,
        [Description("Wide based quad cane")]
        WideBasedQuadCane = 5,
        [Description("Short based quad cane")]
        ShortBasedQuadCane = 6,
        [Description("Standard cane")]
        StandardCan = 7,
        [Description("Crutches")]
        Crutches = 8,
        [Description("None")]
        None = 9,
        [Description("Other")]
        Other = 10
    }

    public enum OralTherapyAssistiveDevices
    {
        [Description("Sock aid")]
        SockAid = 1,
        [Description("Long handled shoe horn")]
        LongHandledShoeHorn = 2,
        [Description("Long handled sponge")]
        LongHandledSponge = 3,
        [Description("Reacher")]
        Reacher = 4,
        [Description("Slide board")]
        SlideBoard = 5,
        [Description("Tub transfer bench")]
        TubTransferBench = 6,
        [Description("3 in one commode")]
        ThreeInOneCommode = 7,
        [Description("Dressing aid")]
        DressingAid = 8,
        [Description("Shower chair")]
        ShowerChair = 9,
        [Description("Modified eatting utensils")]
        ModifiedEattingUtensils = 10
    }
}
