﻿namespace Axxess.AgencyManagement.Enums
{
    public enum AgencyRoles : byte
    {
        Administrator = 1,
        DoN = 2,
        CaseManager = 3,
        Nurse = 4,
        Clerk = 5,
        PhysicalTherapist = 6,
        OccupationalTherapist = 7,
        SpeechTherapist = 8,
        MedicalSocialWorker = 9,
        HHA = 10,
        Scheduler = 11,
        Biller = 12,
        QA = 13,
        Physician = 14,
        OfficeManager = 15,
        CommunityLiasonOfficer = 16,
        ExternalReferralSource = 17,
        DriverAndTransportation = 18,
        OfficeStaff = 19,
        Auditor = 20
    }
}
