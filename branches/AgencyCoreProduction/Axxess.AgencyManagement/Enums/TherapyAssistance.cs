﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum TherapyAssistance 
    {
        [Description("I = Independent")]
        Independent = 1,
        [Description("MI = Modified Independent")]
        ModifiedIndependent = 10,
        [Description("S = Supervision")]
        Supervision = 2,
        [Description("VC = Verbal Cue")]
        VerbalCue = 3,
        [Description("CGA = Contact Guard Assist")]
        ContactGuardAssist = 4,
        [Description("Min A = 25% Assist")]
        MinA = 5,
        [Description("Mod A = 50% Assist")]
        ModA = 6,
        [Description("Max A = 75% Assist")]
        MaxA = 7,
        [Description("Total = 100% Assist")]
        Total = 8,
        [Description("Not Tested")]
        NotTested = 9
    }
}
