﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Infrastructure;

namespace Axxess.AgencyManagement.Enums
{
    public enum MedicareIntermediary
    {
        [CustomDescription("Medicare(Palmetto GBA)", "Medicare Insurance")]
        MedicarePalmettoGBA = 1,
        [CustomDescription("Medicare(National Government Services)", "National Government Services")]
        MedicareNGS = 2,
        [CustomDescription("Medicare(Cigna)", "Cigna Government Services")]
        MedicareCigna = 3,
        [CustomDescription("Medicare(Anthem Health Plans)", "Anthem Health Plans of Maine")]
        MedicareAnthemHealthPlans = 4
    }
}
