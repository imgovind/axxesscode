﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Infrastructure;

namespace Axxess.AgencyManagement.Enums
{
    public enum BillDiscipline
    {
        [CustomDescription("Skilled Nursing", "SNV")]
        Nursing = 1,
        [CustomDescription("Physical Therapy", "PT")]
        PT = 2,
        [CustomDescription("Occupational Therapy", "OT")]
        OT = 3,
        [CustomDescription("Speech Therapy", "ST")]
        ST = 4,
        [CustomDescription("HHA", "HHA")]
        HHA = 5,
        [CustomDescription("Social Worker", "MSW")]
        MSW = 6,
        [CustomDescription("Dietician", "DV")]
        Dietician = 7
    }
}


 