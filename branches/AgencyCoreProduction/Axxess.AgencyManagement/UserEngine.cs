﻿namespace Axxess.AgencyManagement
{
    using System;
    using System.Threading;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Api;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Repositories;
    using Axxess.Api.Contracts;
    using Axxess.AgencyManagement.Domain;

    public static class UserEngine
    {
        #region Private Members

        private static readonly CacheAgent cacheAgent = new CacheAgent();
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();

        #endregion

        #region Public Methods

        //public static string GetName(Guid userId, Guid agencyId)
        //{
        //    var name = string.Empty;//cacheAgent.GetUserDisplayName(userId, agencyId);

        //    //if (name.IsNullOrEmpty() && !userId.IsEmpty())
        //    //{
        //    if (!userId.IsEmpty() && !agencyId.IsEmpty())
        //    {
        //        var user = dataProvider.UserRepository.Get(userId, agencyId, false);
        //        if (user != null)
        //        {
        //            name = user.DisplayName;
        //        }
        //    }
        //    //}
        //    return name;
        //}

        public static void Refresh(Guid agencyId)
        {
            //cacheAgent.RefreshUsers(agencyId);
        }

        //public static List<UserData> Users
        //{
        //    get
        //    {
        //        return cacheAgent.GetUsers();
        //    }
        //}

        public static string GetName(Guid userId, Guid agencyId)
        {
            var name = string.Empty;
            if (!userId.IsEmpty() && !agencyId.IsEmpty())
            {
                var key = Key(agencyId, userId);
                var userXml = string.Empty;
                if ( !Cacher.TryGet<string>(key, out userXml))
                {
                    var user = dataProvider.UserRepository.GetUserCacheById(agencyId, userId);
                    if (user != null)
                    {
                        Cacher.Set(key, user.ToXml());
                        name = user.DisplayName;
                    }
                }
                else
                {
                    var user = userXml.ToObject<UserCache>();
                    if (user != null)
                    {
                        name = user.DisplayName;
                    }
                }
            }
            return name;
        }

        public static UserCache GetUser(Guid userId, Guid agencyId)
        {
            UserCache user = null;
            if (!userId.IsEmpty() && !agencyId.IsEmpty())
            {
                var key = Key(agencyId, userId);
                var userXml = Cacher.Get<string>(key);
                if (userXml.IsNotNullOrEmpty())
                {
                    user = userXml.ToObject<UserCache>();
                    if (user != null)
                    {
                        return user;
                    }
                    else
                    {
                        return AddUser(agencyId, userId, key);
                    }
                }
                else
                {
                    return AddUser(agencyId, userId, key);
                }
            }
            return user;
        }

        public static List<UserCache> GetUsers(Guid agencyId, List<Guid> userIds)
        {
            var users = new List<UserCache>();
            var userIdsNotInCache = new List<Guid>();
            if (!agencyId.IsEmpty() && userIds != null && userIds.Count > 0)
            {
                var keys = userIds.Select(id => Key(agencyId, id));
                var results = Cacher.Get<string>(keys);
                if (results != null && results.Count > 0)
                {
                    results.ForEach((key, value) =>
                    {
                        if (key.IsNotNullOrEmpty() && value != null)
                        {
                            string userXml = value.ToString();
                            if (userXml.IsNotNullOrEmpty())
                            {
                                users.Add(userXml.ToObject<UserCache>());
                            }
                        }

                    });
                }
                userIdsNotInCache = userIds.Where(id => !users.Exists(u => u.Id == id)).ToList();
                if (userIdsNotInCache != null && userIdsNotInCache.Count > 0)
                {
                    var usersNotInCache = dataProvider.UserRepository.GetUserCachesWithCredentialsByIds(agencyId, userIdsNotInCache);
                    if (userIdsNotInCache != null && userIdsNotInCache.Count > 0)
                    {
                        usersNotInCache.ForEach(u =>
                        {
                            var key = Key(agencyId, u.Id);
                            Cacher.Set<string>(key, u.ToXml());
                            users.Add(u);

                        });
                    }
                }
            }
            return users;
        }

        public static void AddOrUpdate(Guid agencyId, User user)
        {
            if (!agencyId.IsEmpty() && user != null && !user.Id.IsEmpty())
            {
                var userXml = user.ToXml();
                if (userXml.IsNotNullOrEmpty())
                {
                    var key = Key(agencyId, user.Id);
                    Cacher.Set<string>(key, userXml);
                }
            }
        }

        public static void AddOrUpdate(Guid agencyId, UserProfile userProfile)
        {
            if (!agencyId.IsEmpty() && userProfile != null && !userProfile.Id.IsEmpty())
            {
                var key = Key(agencyId, userProfile.Id);
                var userXml = Cacher.Get<string>(key);
                if (userXml.IsNotNullOrEmpty())
                {
                    var user = userXml.ToObject<UserCache>();
                    if (user != null)
                    {
                        user.Profile = userProfile;
                        Cacher.Set<string>(key, user.ToXml());
                    }
                    else
                    {
                        AddUser(agencyId, userProfile.Id, key);
                    }
                }
                else
                {
                    AddUser(agencyId, userProfile.Id, key);
                }
            }
        }

        #endregion

        #region Private Methods

        private static UserCache AddUser(Guid agencyId, Guid userId, string key)
        {
            var user = dataProvider.UserRepository.GetUserCacheById(agencyId, userId);
            if (user != null)
            {
                Cacher.Set<string>(key, user.ToXml());
            }
            return user;
        }

        private static string Key(Guid agencyId, Guid userId)
        {
            return string.Format("{0}_{1}_{2}", agencyId, (int)CacheType.User, userId);
        }

        #endregion
    }
}
