﻿namespace Axxess.AgencyManagement
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Threading;
    using System.Collections;
    using System.Collections.Generic;

    using Axxess.Api;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Repositories;

    public static class PhysicianEngine
    {
        #region Private Members

        private static readonly CacheAgent cacheAgent = new CacheAgent();
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();

        #endregion

        #region Public Methods

        public static AgencyPhysician Get(Guid physicianId, Guid agencyId)
        {
            AgencyPhysician physician = null;
            //var physicianInfo = cacheAgent.GetPhysicianXml(physicianId, agencyId);
            //if (physicianInfo.IsNotNullOrEmpty())
            //{
            //    physician = physicianInfo.ToObject<AgencyPhysician>();
            //}
            //else
            //{
                physician = dataProvider.PhysicianRepository.Get(physicianId, agencyId);
            //}
            return physician;
        }

        public static string GetName(Guid physicianId, Guid agencyId)
        {
            var name = string.Empty;
            var physician = Get(physicianId, agencyId);
            if (physician != null)
            {
                name = physician.DisplayName;
            }
            return name;
        }

        public static void Refresh(Guid agencyId)
        {
            //cacheAgent.RefreshPhysicians(agencyId);
        }

        public static IList<AgencyPhysician> AsList(Guid agencyId)
        {
            IList<AgencyPhysician> physicianList = new List<AgencyPhysician>();

            //var physicianInfoList = cacheAgent.GetPhysicians(agencyId);
            //if (physicianInfoList == null || physicianInfoList.Count == 0)
            //{
                physicianList = dataProvider.PhysicianRepository.GetAgencyPhysicians(agencyId);
            //}
            //else
            //{
            //    physicianInfoList.ForEach(p =>
            //    {
            //        if (p.IsNotNullOrEmpty())
            //        {
            //            physicianList.Add(p.ToObject<AgencyPhysician>());
            //        }
            //    });
            //}

            return physicianList;
        }

        public static List<AgencyPhysician> GetAgencyPhysicians(Guid agencyId, List<Guid> physicianIds)
        {
            var physicians = new List<AgencyPhysician>();
            //var physicianIdsNotInCache = new List<Guid>();
            //if (!agencyId.IsEmpty() && physicianIds != null && physicianIds.Count > 0)
            //{
            //    var keys = physicianIds.Select(id => Key(agencyId, id));
            //    var results = Cacher.Get<string>(keys);
            //    if (results != null && results.Count > 0)
            //    {
            //        results.ForEach((key, value) =>
            //        {
            //            if (key.IsNotNullOrEmpty() && value != null)
            //            {
            //                string userXml = value.ToString();
            //                if (userXml.IsNotNullOrEmpty())
            //                {
            //                    physicians.Add(userXml.ToObject<AgencyPhysician>());
            //                }
            //            }

            //        });
            //    }
            //    physicianIdsNotInCache = physicianIds.Where(id => !physicians.Exists(u => u.Id == id)).ToList();
            //    if (physicianIdsNotInCache != null && physicianIdsNotInCache.Count > 0)
            //    {
            //        var physiciansNotInCache = dataProvider.PhysicianRepository.GetPhysiciansByIdsLean(agencyId, physicianIdsNotInCache);
            //        if (physicianIdsNotInCache != null && physicianIdsNotInCache.Count > 0)
            //        {
            //            physiciansNotInCache.ForEach(u =>
            //            {
            //                var key = Key(agencyId, u.Id);
            //                Cacher.Set<string>(key, u.ToXml());
            //                physicians.Add(u);

            //            });
            //        }
            //    }
            //}
            return physicians;
        }


        #endregion
    }
}
