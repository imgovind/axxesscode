﻿namespace Axxess.Api.AbilityNetwork
{
    using System;
    using System.Diagnostics;
    using System.ServiceProcess;

    using Axxess.Api.Contracts;

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
		        { 
			        new AbilityNetworkWindowsService()
		        };
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
