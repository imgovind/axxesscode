﻿namespace Axxess.Api.AbilityNetwork
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Diagnostics;
    using System.Configuration;
    using System.Collections.Generic;
    using System.Security.Cryptography.X509Certificates;

    using Axxess.Core.Extension;

    using Axxess.Api.Contracts;
    using Axxess.Core.Infrastructure;

    public static class AbilityService
    {
        #region Public Methods

        public static hiqhRequest CreateHiqhRequest(bool isMockRequest)
        {
            var credentials = GetCredentials();
            return new hiqhRequest
            {
                mockResponse = isMockRequest ? "true" : "false",
                details = new hiqhRequestDetails[1]
                {
                    new hiqhRequestDetails 
                    { 
                        detail = "ALL"
                    }
                },
                medicareMainframe = new hiqhRequestMedicareMainframe[1]
                {
                   new hiqhRequestMedicareMainframe
                   {
                       application = new hiqhRequestMedicareMainframeApplication[1]
                       {
                           new hiqhRequestMedicareMainframeApplication
                           {
                               name = "DDE",
                               pptnRegion = "",
                               dataCenter = "CDS",
                               //appId = "ACPFA391",
                               //facilityState = "TX",
                               lineOfBusiness = "PartA"
                           }
                       },
                       credential = new hiqhRequestMedicareMainframeCredential[1]
                       {
                           new hiqhRequestMedicareMainframeCredential
                           {
                               userId = credentials != null ? credentials.UserId : ConfigurationManager.AppSettings["UserId"],
                               password = credentials != null ? credentials.Password : ConfigurationManager.AppSettings["Password"]
                           }
                       }
                   }
                }
            };
        }

        public static passwordChangeRequest CreatepasswordChangeRequest(bool isMockRequest, string userId, string oldPassword, string newPassword)
        {
            return new passwordChangeRequest
            {
                newPassword = newPassword,
                mockResponse = isMockRequest ? "true" : "false",
                medicareMainframe = new passwordChangeRequestMedicareMainframe[1]
                {
                   new passwordChangeRequestMedicareMainframe
                   {
                       application = new passwordChangeRequestMedicareMainframeApplication[1]
                       {
                           new passwordChangeRequestMedicareMainframeApplication
                           {
                               name = "DDE",
                               pptnRegion = "",
                               dataCenter = "CDS",
                               appId = "ACPFA391"
                           }
                       },
                       credential = new passwordChangeRequestMedicareMainframeCredential[1]
                       {
                           new passwordChangeRequestMedicareMainframeCredential
                           {
                               userId = userId,
                               password = oldPassword
                           }
                       }
                   }
                }
            };
        }

        public static hiqhRequestSearchCriteria[] CreateSearchCriteria(string hic, string lastName, string firstInitial, string dateOfBirth, string sex, string providerId)
        {
            return new hiqhRequestSearchCriteria[1]
            {
                new hiqhRequestSearchCriteria
                {
                    hic = hic,
                    sex = sex,
                    requestorId = "1",
                    lastName = lastName,
                    dateOfBirth = dateOfBirth,
                    firstInitial = firstInitial,
                    intermediaryNumber = "11004",
                    providerId = providerId
                }
            };
        }

        public static hiqhResponse GetHiqhResponse(Guid patientId, hiqhRequest request, string stateCode)
        {
            int stateHostId = 1;
            hiqhResponse response = null;
            var hostId = GetPatientHost(patientId);
            if (hostId.IsNotNullOrEmpty())
            {
                Windows.EventLog.WriteEntry(string.Format("Using Stored Host: {0} and Patient Id: {1}", hostId, patientId), EventLogEntryType.Information);
                response = AttemptRequest(request, hostId);
                if (response == null)
                {
                    Windows.EventLog.WriteEntry(string.Format("Stored Host Failed: {0}and Patient Id: {1}", hostId, patientId), EventLogEntryType.Information);
                    if (stateCode.IsNotNullOrEmpty())
                    {
                        stateHostId = StateHostList[stateCode];
                    }

                    string[] hostList = HostList[stateHostId];
                    hostList = hostList.Where(h => !h.IsEqual(hostId)).ToArray();
                    foreach (var host in hostList)
                    {
                        response = AttemptRequest(request, host);
                        if (response != null)
                        {
                            if (UpdatePatientHost(patientId, host))
                            {
                                Windows.EventLog.WriteEntry(string.Format("Found with New Host: {0} after failed stored Host: {1} and Patient Id: {2}", host, hostId, patientId), EventLogEntryType.Information);
                            }
                            break;
                        }
                    }
                }
            }
            else
            {
                if (stateCode.IsNotNullOrEmpty())
                {
                    stateHostId = StateHostList[stateCode];
                }
                string[] hostList = HostList[stateHostId];
                foreach (var host in hostList)
                {
                    response = AttemptRequest(request, host);
                    if (response != null)
                    {
                        if (AddPatientHost(patientId, host))
                        {
                            Windows.EventLog.WriteEntry(string.Format("Found with Host: {0} and Patient Id: {1}", hostId, patientId), EventLogEntryType.Information);
                        }
                        break;
                    }
                }
            }

            return response;
        }

        public static passwordChangeResponse GetPasswordChangeResponse(passwordChangeRequest request)
        {
            passwordChangeResponse response = null;

            try
            {
                response = SendPasswordChangeRequest(request.ToXml());
            }
            catch (WebException webEx)
            {
                Windows.EventLog.WriteEntry(webEx.ToString(), EventLogEntryType.Error);
                using (var responseStream = new StreamReader(webEx.Response.GetResponseStream()))
                {
                    ProcessError(responseStream.ReadToEnd());
                }
            }
            return response;
        }

        #endregion

        #region Private Members and Methods

        private static Dictionary<string, int> StateHostList = new Dictionary<string, int>
        {
            { "IL", 1 }, { "MI", 1 }, { "MN", 1 }, { "WI", 1 },
            { "ID", 2 }, { "IA", 2 }, { "KS", 2 }, { "MO", 2 }, { "MT", 2 }, { "NE", 2 }, { "ND", 2 }, { "OR", 2 },{ "SD", 2 }, { "UT", 2 }, { "WA", 2 }, { "WY", 2 },
            { "DE", 3 }, { "NJ", 3 }, { "NY", 3 }, { "PA", 3 },
            { "IN", 4 }, { "MD", 4 }, { "OH", 4 }, { "VA", 4 }, { "WV", 4 },
            { "AK", 5 }, { "AZ", 5 }, { "CA", 5 }, { "HI", 5 }, { "NV", 5 },
            { "CT", 6 }, { "ME", 6 }, { "MA", 6 }, { "NH", 6 }, { "RI", 6 }, { "VT", 6 },
            { "AL", 7 }, { "MS", 7 }, { "NC", 7 }, { "SC", 7 }, { "TN", 7 },
            { "FL", 8 }, { "GA", 8 },
            { "AR", 9 }, { "CO", 9 }, { "LA", 9 }, { "NM", 9 }, { "OK", 9 }, { "TX", 9 }
        };

        private static Dictionary<int, string[]> HostList = new Dictionary<int, string[]>
        {
             { 1, new string[] { "GL", "SE", "SW", "GW", "KS", "MA", "PA", "NE", "SO" } },
             { 2, new string[] { "GW", "SE", "GL", "SW", "KS", "MA", "PA", "NE", "SO" } },
             { 3, new string[] { "KS", "SE", "GL", "GW", "SW", "MA", "PA", "NE", "SO" } },
             { 4, new string[] { "MA", "SE", "GL", "GW", "KS", "SW", "PA", "NE", "SO" } },
             { 5, new string[] { "PA", "SE", "GL", "GW", "KS", "MA", "SW", "NE", "SO" } },
             { 6, new string[] { "NE", "SE", "GL", "GW", "KS", "MA", "PA", "SW", "SO" } },
             { 7, new string[] { "SE", "SW", "GL", "GW", "KS", "MA", "PA", "NE", "SO" } },
             { 8, new string[] { "SO", "SE", "GL", "GW", "KS", "MA", "PA", "NE", "SW" } },
             { 9, new string[] { "SW", "SE", "GL", "GW", "KS", "MA", "PA", "NE", "SO" } },
        };

        private static string[] UnwantedNodes = 
        {
            "preventativeServices",
            "smokingCessation",
            "rehabilitationSessions",
            "telehealthServices",
            "behavioralServices",
            "highIntensityBehavioralCounseling"
        };

        private static string[] ErrorCodes = 
        {
            "ClientCertNotAuthorized",
            "InvalidRequestBody",
            "RequestValidationErrors",
            "ApplicationNotFound",
            "UserIdNotAuthorized",
            "UserIdRevoked",
            "InvalidPassword",
            "PasswordExpired",
            "ApplicationNotAccessible",
            "InvalidProviderId",
            "HicNotFound",
            "ReasonCodeNotFound",
            "ApplicationUnavailable",
            "AlreadyLoggedIn",
            "ServiceUnavailable",
            "RequestTimedOut",
            "ClientCertNotPresented",
            "BadHttpRequest",
            "UnknownApplicationId",
            "SessionStartError",
            "InvalidNewPassword",
            "InvalidIcn",
            "InvalidHcpcs",
            "InvalidFissEligibilityBeneficiary",
            "FissClaimInquiryFailed",
            "CwfInquiryFailed",
            "InvalidCwfSearchCriteria",
            "FissOnlineReportNotAvailable",
            "FissOnlineReportHasNoData",
            "NotAuthorizedForTransaction"
        };

        private static hiqhResponse SendHiqhRequest(string hiqhRequestXml)
        {
            hiqhResponse response = null;

            var encoding = new ASCIIEncoding();
            byte[] requestData = encoding.GetBytes(hiqhRequestXml);
            string eligibilityUrl = @"https://access.abilitynetwork.com:443/access/cwf/hiqh/";
            Uri eligibilityUri = new Uri(eligibilityUrl, UriKind.Absolute);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(eligibilityUri);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "text/xml";
            httpWebRequest.Headers.Add("X-Access-Version", "1");
            httpWebRequest.ContentLength = requestData.Length;

            X509Store certStore = new X509Store(StoreLocation.LocalMachine);
            certStore.Open(OpenFlags.ReadOnly);
            X509Certificate certificate = certStore.Certificates.Get("Adeniyi.Olajide@012");
            httpWebRequest.ClientCertificates.Add(certificate);

            ServicePointManager.ServerCertificateValidationCallback = ((sender, cert, chain, sslPolicyErrors) => true);

            using (Stream requestStream = httpWebRequest.GetRequestStream())
            {
                requestStream.Write(requestData, 0, requestData.Length);
            }

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var responseStream = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    XDocument xDocument = XDocument.Load(responseStream);
                    var unwantedNodes = from node in xDocument.Descendants()
                                        where UnwantedNodes.Contains(node.Name.LocalName)
                                        select node;

                    unwantedNodes.ToList().ForEach(x => x.Remove());

                    response = xDocument.ToString().ToObject<hiqhResponse>();
                }
            }

            return response;
        }

        private static passwordChangeResponse SendPasswordChangeRequest(string passwordChangeRequestXml)
        {
            passwordChangeResponse response = null;

            var encoding = new ASCIIEncoding();
            byte[] requestData = encoding.GetBytes(passwordChangeRequestXml);
            string passwordChangeUrl = @"https://access.abilitynetwork.com:443/access/password/change/";
            Uri passwordChangeUri = new Uri(passwordChangeUrl, UriKind.Absolute);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(passwordChangeUri);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "text/xml";
            httpWebRequest.Headers.Add("X-Access-Version", "1");
            httpWebRequest.ContentLength = requestData.Length;

            X509Store certStore = new X509Store(StoreLocation.LocalMachine);
            certStore.Open(OpenFlags.ReadOnly);
            X509Certificate certificate = certStore.Certificates.Get("Adeniyi.Olajide@012");
            httpWebRequest.ClientCertificates.Add(certificate);

            ServicePointManager.ServerCertificateValidationCallback = ((sender, cert, chain, sslPolicyErrors) => true);

            using (Stream requestStream = httpWebRequest.GetRequestStream())
            {
                requestStream.Write(requestData, 0, requestData.Length);
            }

            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var responseStream = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    response = responseStream.ReadToEnd().ToObject<passwordChangeResponse>();
                }
            }

            return response;
        }

        private static hiqhResponse AttemptRequest(hiqhRequest request, string host)
        {
            hiqhResponse response = null;

            try
            {
                response = SendHiqhRequest(request.UseHost(host).ToXml());
            }
            catch (WebException webEx)
            {
                Windows.EventLog.WriteEntry(string.Format("Web Exception: {0}", webEx.ToString()), EventLogEntryType.Error);
                using (var responseStream = new StreamReader(webEx.Response.GetResponseStream()))
                {
                    ProcessError(responseStream.ReadToEnd());
                }
            }
            return response;
        }

        private static void ProcessError(string responseText)
        {
            if (responseText.IsNotNullOrEmpty())
            {
                Console.WriteLine(responseText);
                Windows.EventLog.WriteEntry(string.Format("Response Text: {0}", responseText), EventLogEntryType.Error);

                var error = responseText.ToObject<ACCESSErrorsError>();
                if (error != null)
                {
                    ErrorCodes.ForEach(errorCode =>
                    {
                        if (errorCode.IsEqual(error.code))
                        {

                        }
                    });
                }
            }
        }

        private static Credentials GetCredentials()
        {
            var credential = new Credentials();
            string sql = "SELECT UserId, Password FROM `ddecredentials` LIMIT 0, 1;";
            using (var fluentCommand = new FluentCommand<Credentials>(sql))
            {
                credential = fluentCommand
                    .SetConnection("AxxessMembershipConnectionString")
                    .SetMap(reader => new Credentials
                    {
                        UserId = reader.GetString("UserId"),
                        Password = reader.GetString("Password")
                    }).AsSingle();
            }
            return credential;
        }

        private static string GetPatientHost(Guid patientId)
        {
            var hostId = string.Empty;
            string sql = "SELECT HostId FROM `medicarepatienthosts` WHERE PatientId = @patientid LIMIT 0, 1;";
            using (var fluentCommand = new FluentCommand<StringVariable>(sql))
            {
                var stringVariable = fluentCommand
                    .SetConnection("AgencyManagementConnectionString")
                    .AddGuid("patientid", patientId)
                    .SetMap(reader => new StringVariable
                    {
                        Value = reader.GetStringNullable("HostId")
                    }).AsSingle();
                if (stringVariable != null && stringVariable.Value.IsNotNullOrEmpty())
                {
                    hostId = stringVariable.Value;
                }
            }
            return hostId;
        }

        private static bool AddPatientHost(Guid patientId, string hostId)
        {
            var result = false;
            string insertPatientHostSql = string.Format("INSERT INTO `medicarepatienthosts`(`PatientId`,`HostId`) VALUES ('{0}', '{1}');", patientId, hostId);
            using (var cmd = new FluentCommand<int>(insertPatientHostSql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        private static bool UpdatePatientHost(Guid patientId, string hostId)
        {
            var result = false;
            string updatePatientHostSql = string.Format("UPDATE `medicarepatienthosts` SET `HostId` = '{0}' WHERE PatientId = '{1}' LIMIT 0, 1;", hostId, patientId);
            using (var cmd = new FluentCommand<int>(updatePatientHostSql))
            {
                cmd.SetConnection("AgencyManagementConnectionString");
                if (cmd.AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        #endregion
        
    }
}
