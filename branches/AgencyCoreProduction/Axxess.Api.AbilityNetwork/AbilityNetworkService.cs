﻿namespace Axxess.Api.AbilityNetwork
{
    using System;
    using System.Diagnostics;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;

    public class AbilityNetworkService : BaseService, IAbilityNetworkService
    {
        #region IAbilityNetworkService Members

        public string GetHiqhRequest(Guid patientId, string hic, string lastName, string firstName, DateTime dateOfBirth, string sex, string providerId, string stateCode)
        {
            var hiqhRequest = AbilityService.CreateHiqhRequest(false);
            hiqhRequest.searchCriteria = AbilityService.CreateSearchCriteria(hic, lastName.Length >= 6 ? lastName.Substring(0, 6) : lastName, firstName.Substring(0, 1), dateOfBirth.ToString("yyyy-MM-dd"), sex.Substring(0, 1), providerId);

            var response = AbilityService.GetHiqhResponse(patientId, hiqhRequest, stateCode);
            if (response != null)
            {
                Windows.EventLog.WriteEntry(string.Format("HIQH Request Success: {0}, {1}, {2}, {3}, {4}, {5}, {6}", hic, lastName, firstName, dateOfBirth.ToString("yyyy-MM-dd"), sex, providerId, patientId), EventLogEntryType.Information);
                return response.ToXml();
            }
            else
            {
                Windows.EventLog.WriteEntry(string.Format("HIQH Request Failure: {0}, {1}, {2}, {3}, {4}, {5}, {6}", hic, lastName, firstName, dateOfBirth.ToString("yyyy-MM-dd"), sex, providerId, patientId), EventLogEntryType.Error);
            }
            return string.Empty;
        }

        public bool GetPasswordChangeRequest(string userId, string oldPassword, string newPassword)
        {
            Windows.EventLog.WriteEntry(string.Format("PasswordChangeInput: {0}, {1}, {2}", userId, oldPassword, newPassword), EventLogEntryType.Information);

            var result = false;
            var changePasswordRequest = AbilityService.CreatepasswordChangeRequest(false, userId, oldPassword, newPassword);

            var response = AbilityService.GetPasswordChangeResponse(changePasswordRequest);
            if (response != null)
            {
                Windows.EventLog.WriteEntry("Password Change Request Success", EventLogEntryType.Information);
                result = true;
            }
            else
            {
                Windows.EventLog.WriteEntry(string.Format("Password Request Failure: {0}, {1}, {2}", userId, oldPassword, newPassword), EventLogEntryType.Error);
            }
            return result;
        }

        #endregion

        #region IService Members

        bool IService.Ping()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
