﻿namespace Axxess.AgencyManagement.App.Security
{
    public interface IFormsAuthenticationService
    {
        string LoginUrl { get; }
        void RedirectToLogin();
        void SignIn(string userName, bool rememberMe);
        void SignOut();
    }
}
