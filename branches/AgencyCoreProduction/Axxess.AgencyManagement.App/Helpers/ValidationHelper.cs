﻿

namespace Axxess.AgencyManagement.App.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;
    using System.Web.Mvc;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Enums;
    using Axxess.Core;

   public static class ValidationHelper
    {

       public static List<Validation> AddNotesValidationRules(string type, string button, string[] keys, DateTime startDate, DateTime endDate, FormCollection formCollection)
       {
           List<Validation> rules = new List<Validation>();
           if (keys.Contains(type + "_VisitDate"))
           {
               rules.Add(new Validation(() => !formCollection[type + "_VisitDate"].IsNotNullOrEmpty(), "Visit date can't be empty."));
               rules.Add(new Validation(() => !formCollection[type + "_VisitDate"].IsValidDate(), "Visit date is not valid."));
               rules.Add(new Validation(() => formCollection[type + "_VisitDate"].IsNotNullOrEmpty() && formCollection[type + "_VisitDate"].IsValidDate() ? !(formCollection[type + "_VisitDate"].ToDateTime().Date >= startDate.Date && formCollection[type + "_VisitDate"].ToDateTime().Date <= endDate.Date) : true, "Visit date is not in the episode range."));
           }

           rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsNotNullOrEmpty(), "Task can't be empty."));
           if (formCollection["DisciplineTask"].IsNotNullOrEmpty())
           {
               rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsInteger() && !Enum.IsDefined(typeof(DisciplineTasks), formCollection["DisciplineTask"].ToInteger()), "Select the right task."));
           }

           //if (keys.Contains(type + "_TimeIn") && keys.Contains(type + "_TimeOut"))
           //{
           //    if (type != DisciplineTasks.DriverOrTransportationNote.ToString())
           //    {
           //        string timeIn = formCollection[type + "_TimeIn"];
           //        string timeOut = formCollection[type + "_TimeOut"];
           //        if (timeIn.IsNotNullOrEmpty() && timeOut.IsNotNullOrEmpty())
           //        {
           //            bool valid = ((timeIn.ToDateTime().Ticks - timeOut.ToDateTime().Ticks) / 60 / 60 / 1000) >= 3;
           //            rules.Add(new Validation(() => valid, "Time-Out is 3 hours greater than Time-In. "));
           //        }
           //    }
           //}


           if (button == "Complete" || button == "Approve")
           {
               if (keys.Contains(type + "_Clinician"))
               {
                   rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_Clinician"]), "Clinician Signature is required."));
               }
               if (keys.Contains(type + "_TimeIn"))
               {
                   if (type != DisciplineTasks.DriverOrTransportationNote.ToString())
                   {
                       rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_TimeIn"]), "Time-In is required. "));
                   }
               }
               if (keys.Contains(type + "_TimeOut"))
               {
                   if (type != DisciplineTasks.DriverOrTransportationNote.ToString())
                   {
                       rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_TimeOut"]), "Time-Out is required. "));
                   }
               }
               if (keys.Contains(type + "_SendAsOrder"))
               {
                   rules.Add(new Validation(() => formCollection[type + "_PhysicianId"].IsNullOrEmpty(), "Physician is required to send as an order."));
               }
               if (keys.Contains(type + "_SignatureDate"))
               {
                   rules.Add(new Validation(() => formCollection[type + "_SignatureDate"].IsNullOrEmpty(), "Signature date cannot be empty."));
                   rules.Add(new Validation(() => !formCollection[type + "_SignatureDate"].IsValidDate(), "Signature date is not valid."));
                   rules.Add(new Validation(() => formCollection[type + "_SignatureDate"].IsNotNullOrEmpty() && formCollection[type + "_SignatureDate"].IsValidDate() && formCollection[type + "_VisitDate"].IsNotNullOrEmpty() && formCollection[type + "_VisitDate"].IsValidDate() ? !(formCollection[type + "_SignatureDate"].ToDateTime() >= formCollection[type + "_VisitDate"].ToDateTime() && formCollection[type + "_SignatureDate"].ToDateTime() <= endDate) : true, "Signature date is not in the valid range."));
               }
               if (type == DisciplineTasks.PTEvaluation.ToString() ||
                   type == DisciplineTasks.PTReEvaluation.ToString() ||
                   type == DisciplineTasks.OTEvaluation.ToString() || 
                   type == DisciplineTasks.OTReEvaluation.ToString() ||
                   type == DisciplineTasks.STEvaluation.ToString() || 
                   type == DisciplineTasks.STReEvaluation.ToString()|| 
                   type == DisciplineTasks.MSWEvaluationAssessment.ToString())
               {
                   rules.Add(new Validation(() => formCollection[type + "_PhysicianId"].IsNullOrEmpty(), "Physician is required. "));
               }
               if ((type == DisciplineTasks.PTDischarge.ToString() || type == DisciplineTasks.SixtyDaySummary.ToString() || type == DisciplineTasks.SNPsychAssessment.ToString()) && keys.Contains(type + "_SendAsOrder"))
               {
                   rules.Add(new Validation(() => formCollection[type + "_PhysicianId"].IsNullOrEmpty(), "Physician is required. "));
               }
           }
           return rules;
       }


       public static JsonViewData ValidateAssessmentTask(List<ScheduleEvent> oldEvents, List<ScheduleEvent> newEvents, DateTime endDate)
       {
           var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be saved. Please try again." };
           try
           {
               if ((oldEvents != null && oldEvents.Count > 0))
               {
                   var recertTasks = DisciplineTaskFactory.RecertDisciplineTasks(false);
                   var transferTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks();
                   var rocTasks = DisciplineTaskFactory.ROCDisciplineTasks();
                   var socTasks = DisciplineTaskFactory.SOCDisciplineTasks(false);
                   var dischargeTasks = DisciplineTaskFactory.DischargeOASISDisciplineTasks(false);

                   foreach (var evnt in newEvents)
                   {
                       if (recertTasks.Contains(evnt.DisciplineTask))
                       {
                           var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date < evnt.EventDate.Date));
                           ScheduleEvent roc = null;
                           if (transfer != null)
                           {
                               roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date > transfer.EventDate.Date));
                           }
                           if (transfer != null && roc == null)
                           {
                               viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                               return (viewData);
                           }
                           else if (transfer != null && roc != null && roc.EventDate.Date <= transfer.EventDate.Date)
                           {
                               viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                               return (viewData);
                           }
                           else if (oldEvents.Exists(oe => recertTasks.Contains(oe.DisciplineTask)))
                           {
                               viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
                               return (viewData);
                           }
                           else if (evnt.EventDate.Date < endDate.AddDays(-5).Date || evnt.EventDate.Date > endDate.Date)
                           {
                               viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
                               return (viewData);
                           }
                       }
                       else if (socTasks.Contains(evnt.DisciplineTask))
                       {
                           if (oldEvents.Exists(oe => socTasks.Contains(evnt.DisciplineTask)))
                           {
                               viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
                               return (viewData);
                           }
                           else if (oldEvents.Exists(oe => dischargeTasks.Contains(oe.DisciplineTask)))
                           {
                           }
                       }
                       else if (rocTasks.Contains(evnt.DisciplineTask))
                       {
                           var roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask));
                           var transfers = oldEvents.Where(oe => transferTasks.Contains(oe.DisciplineTask)).ToList();
                           if (roc == null)
                           {
                               if (transfers == null || (transfers != null && transfers.Count == 0))
                               {
                                   viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                   return (viewData);
                               }
                               else if (transfers.IsNotNullOrEmpty())
                               {
                                   if (transfers.Exists(t => t.EventDate.Date > evnt.EventDate.Date))
                                   {
                                       viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
                                       return (viewData);
                                   }
                               }
                           }
                           else if (roc != null)
                           {
                               if (transfers != null)
                               {
                                   var transfersAfterEvent = transfers.Where(t => t.EventDate.Date > evnt.EventDate.Date).ToList();
                                   var transfersBeforeEvent = transfers.Where(t => t.EventDate.Date < evnt.EventDate.Date);
                                   if ((transfersAfterEvent.IsNotNullOrEmpty() && transfersAfterEvent.Exists(t => roc.EventDate.Date > t.EventDate.Date)) && transfersBeforeEvent == null)
                                   {
                                       viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                       return (viewData);
                                   }
                               }
                           }
                       }
                       else if (transferTasks.Contains(evnt.DisciplineTask))
                       {
                           var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask));
                           ScheduleEvent roc = null;
                           if (transfer != null)
                           {
                               roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date > transfer.EventDate.Date));
                           }
                           if (transfer != null && roc == null)
                           {
                               viewData.errorMessage = "Please create a Resumption of Care before creating another Transfer.";
                               return (viewData);
                           }
                       }
                   }
                   viewData.isSuccessful = true;
               }
               else
               {
                   viewData.isSuccessful = true;
               }
           }
           catch (Exception)
           {
               return viewData;
           }
           return viewData;
       }




    }
}
