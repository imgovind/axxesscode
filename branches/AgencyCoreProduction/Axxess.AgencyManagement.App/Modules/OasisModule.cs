﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class OasisModule : Module
    {
        public override string Name
        {
            get { return "Oasis"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "PlanOfCarePrintPreview",
                "485/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PlanOfCarePrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "485Medication",
                "485/Medication/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PlanOfCareMedication", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "StartOfCarePrintPreview",
                "StartofCare/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "NonOasisStartOfCarePrintPreview",
                "NonOasisStartofCare/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "NonOasisRecertificationPrintPreview",
                "NonOasisRecertification/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "NonOasisDischargePrintPreview",
                "NonOasisDischarge/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "ResumptionOfCarePrintPreview",
                "ResumptionOfCare/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "RecertificationPrintPreview",
                "Recertification/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "TransferInPatientNotDischargedPrintPreview",
                "TransferInPatientNotDischarged/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "TransferInPatientDischargedPrintPreview",
                "TransferInPatientDischarged/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "DischargeFromAgencyPrintPreview",
                "DischargeFromAgency/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "DischargeFromAgencyDeathPrintPreview",
                "DischargeFromAgencyDeath/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "FollowUpPrintPreview",
                "FollowUp/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "Validate",
                "Validate/{Id}/{patientId}/{episodeId}/{assessmentType}",
                new { controller = this.Name, action = "Validate", Id = new IsGuid(), patientId = new IsGuid(), episodeId = new IsGuid(), assessmentType = UrlParameter.Optional });

            routes.MapRoute(
                "Profile",
                "Oasis/Profile/{Id}/{type}",
                new { controller = this.Name, action = "OasisProfilePrint", Id = new IsGuid(), type = UrlParameter.Optional });
        }
    }
}
