﻿namespace Axxess.AgencyManagement.App.Enums
{
    public enum LoginAttemptType
    {
        Failed,
        Success,
        Locked,
        Deactivated,
        TrialPeriodOver,
        AccountSuspended,
        TooManyAttempts
    }
}
