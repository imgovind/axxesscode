﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.Enums
{
    public enum OrderStatusGroup
    {
        StillProcessing,
        Pending,
        Complete
    }
}
