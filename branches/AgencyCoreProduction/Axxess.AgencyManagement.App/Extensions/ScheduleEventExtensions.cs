﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.OasisC.Enums;
    using Axxess.AgencyManagement.Domain;

    public static class ScheduleEventExtensions
    {

        public static AssessmentType GetAssessmentType(this ScheduleEvent scheduleEvent)
        {
            AssessmentType type = AssessmentType.None;
            if (scheduleEvent.IsOASISSOCExcludingNON())
                type = AssessmentType.StartOfCare;
            else if (scheduleEvent.IsOASISROC())
                type = AssessmentType.ResumptionOfCare;
            else if (scheduleEvent.IsOASISRecert())
                type = AssessmentType.Recertification;
            else if (scheduleEvent.IsOASISFollowUp())
                type = AssessmentType.FollowUp;
            else if (scheduleEvent.IsOASISTransfer())
                type = AssessmentType.TransferInPatientNotDischarged;
            else if (scheduleEvent.IsOASISTransferDischarge())
                type = AssessmentType.TransferInPatientDischarged;
            else if (scheduleEvent.IsOASISDOD())
                type = AssessmentType.DischargeFromAgencyDeath;
            else if (scheduleEvent.IsOASISDischarge())
                type = AssessmentType.DischargeFromAgency;
            else if (scheduleEvent.IsNONOASISSOC())
                type = AssessmentType.NonOasisStartOfCare;
            else if (scheduleEvent.IsNONOASISRecert())
                type = AssessmentType.NonOasisRecertification;
            else if (scheduleEvent.IsNONOASISDischarge())
                type = AssessmentType.NonOasisDischarge;

            return type;
        }
    }
}
