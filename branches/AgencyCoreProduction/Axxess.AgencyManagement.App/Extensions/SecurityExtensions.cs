﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using Security;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public static class SecurityExtensions
    {
        public static bool IsSingleSession(this AxxessPrincipal principal)
        {
            var result = false;
            if (principal != null)
            {
                AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                if (identity != null)
                {
                    if (identity.Session.SessionId.IsEqual(SessionStore.SessionId))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public static void UpdateSessionId(this AxxessPrincipal principal, Guid userId, Guid agencyId)
        {
            if (principal != null)
            {
                AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                if (identity != null)
                {
                    identity.Session.SessionId = SessionStore.SessionId;
                    Cacher.Set<AxxessPrincipal>(string.Format("{0}_{1}", userId, agencyId), principal);
                }
            }
        }

        public static bool IsImpersonated(this AxxessPrincipal principal)
        {
            var result = false;
            if (principal != null)
            {
                AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                if (identity != null && identity.IsImpersonated == true)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}

