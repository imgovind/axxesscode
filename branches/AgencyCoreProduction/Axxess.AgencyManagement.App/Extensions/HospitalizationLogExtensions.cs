﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.Core.Extension;
    using Axxess.Core.Enums;

    public static class HospitalizationLogExtensions
    {
        public static IDictionary<string, Question> ToDictionary(this HospitalizationLog hospitalizationLog)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            var data = hospitalizationLog != null && hospitalizationLog.Data.IsNotNullOrEmpty() ? hospitalizationLog.Data.ToObject<List<Question>>() : null;
            if (data != null)
            {
                var key = string.Empty;
                data.ForEach(question =>
                {
                    if (question.Type == QuestionType.Moo)
                    {
                        key = string.Format("{0}{1}", question.Code, question.Name);
                        if (!questions.ContainsKey(key))
                        {
                            questions.Add(key, question);
                        }
                    }
                    else if (question.Type == QuestionType.PlanofCare)
                    {
                        key = string.Format("485{0}", question.Name);
                        if (!questions.ContainsKey(key))
                        {
                            questions.Add(key, question);
                        }
                    }
                    else if (question.Type == QuestionType.Generic)
                    {
                        key = string.Format("Generic{0}", question.Name);
                        if (!questions.ContainsKey(key))
                        {
                            questions.Add(key, question);
                        }
                    }
                    else
                    {
                        key = string.Format("{0}", question.Name);
                        if (!questions.ContainsKey(key))
                        {
                            questions.Add(key, question);
                        }
                    }
                });
            }
            return questions;
        }
    }
}
