﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Axxess.AgencyManagement.App;
using NPOI.HSSF.Util;


namespace Axxess.Core.Extension
{
    public static class WorkBookExtensions
    {
        /*
         * Loops through all of the cells within a HSSFWorkbook and adjusts the font size.
         * Does not automatically adjust the width of the columns and does not set borders.
         */
        public static void FinishWritingToExcelSpreadsheet(this HSSFWorkbook workBook)
        {
            IFont headerFont = workBook.CreateFont();
            headerFont.FontName = "Calibri";
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            ICellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            IFont normalFont = workBook.CreateFont();
            normalFont.FontName = "Calibri";
            normalFont.FontHeightInPoints = 12;

            ICellStyle normalStyle = workBook.CreateCellStyle();
            normalStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            normalStyle.VerticalAlignment = VerticalAlignment.TOP;
            normalStyle.SetFont(normalFont);

            ISheet sheet = workBook.GetSheetAt(0);
            if (sheet != null)
            {
                int numberOfRows = sheet.PhysicalNumberOfRows;
                for (int rowCount = 0; rowCount < numberOfRows; rowCount++)
                {
                    IRow row = sheet.GetRow(rowCount);
                    if (row != null)
                    {
                        var cells = row.Cells;
                        int columnCount = 0;
                        foreach (var cell in cells)
                        {
                            if (rowCount == 0)
                            {
                                cell.CellStyle = headerStyle;
                            }
                            else if (rowCount < 2)
                            {
                                cell.CellStyle = headerStyle;
                            }
                            else if (cell.CellStyle.DataFormat == 0)
                            {

                                cell.CellStyle = normalStyle;
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                            else if (cell.CellStyle.DataFormat > 0)
                            {
                                cell.CellStyle.SetFont(normalFont);
                                cell.CellStyle.VerticalAlignment = VerticalAlignment.TOP;
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                            columnCount++;
                        }
                    }
                }
                IRow totalRow = sheet.GetRow(numberOfRows + 1);
                numberOfRows += totalRow != null ? 0 : 1;
                totalRow = (totalRow != null ? totalRow : sheet.GetRow(numberOfRows + 1));
                if (totalRow != null)
                {
                    numberOfRows++;
                    var totalCell = totalRow.GetCell(0);
                    totalCell.CellStyle = normalStyle;
                }
            }
        }


        public static void FinishWritingToExcelSpreadsheet(this HSSFWorkbook workBook, int columns)
        {
            FinishWritingToExcelSpreadsheet(workBook, columns, 0);
        }

        public static void FinishWritingToExcelSpreadsheet(this HSSFWorkbook workBook, int columns, int specialHeaderRowCount)
        {
            bool hasSpecialHeader = specialHeaderRowCount != 0;
            IFont headerFont = workBook.CreateFont();
            headerFont.FontName = "Calibri";
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;
            
            ICellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            IFont normalFont = workBook.CreateFont();
            normalFont.FontName = "Calibri";
            normalFont.FontHeightInPoints = 12;

            ICellStyle normalStyle = workBook.CreateCellStyle();
            normalStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            normalStyle.VerticalAlignment = VerticalAlignment.TOP;
            normalStyle.SetFont(normalFont);

            ICellStyle normalStyleNotWrapped = workBook.CreateCellStyle();
            normalStyleNotWrapped.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            normalStyleNotWrapped.VerticalAlignment = VerticalAlignment.TOP;
            normalStyleNotWrapped.SetFont(normalFont);

            ICellStyle normalStyleWBorders = workBook.CreateCellStyle();
            normalStyleWBorders.VerticalAlignment = VerticalAlignment.TOP;
            normalStyleWBorders.SetFont(normalFont);
            normalStyleWBorders.BorderBottom = BorderStyle.THIN;
            normalStyleWBorders.BorderLeft = BorderStyle.THIN;
            normalStyleWBorders.BorderRight = BorderStyle.THIN;
            normalStyleWBorders.BorderTop = BorderStyle.THIN;
            normalStyleWBorders.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;

            ISheet sheet = workBook.GetSheetAt(0);
            if (sheet != null)
            {
                int numberOfRows = sheet.PhysicalNumberOfRows;
                if (!hasSpecialHeader)
                {
                    if (columns <= 26)
                    {
                        char letter = (char)(64 + columns);
                        workBook.SetPrintArea(0, "A2:" + letter + (numberOfRows + 2));
                    }
                    else if(columns <= 52)
                    {
                        char letter = (char)(64 + columns % 26);
                        workBook.SetPrintArea(0, "A2:A" + letter + (numberOfRows + 2));
                    }
                }

              
                for (int rowCount = 0; rowCount < numberOfRows; rowCount++)
                {
                    IRow row = sheet.GetRow(rowCount);
                    if (row != null)
                    {
                        var cells = row.Cells;
                        int columnCount = 0;
                        foreach (var cell in cells)
                        {
                            if (hasSpecialHeader && rowCount < (specialHeaderRowCount + 1))
                            {

                            }
                            else if (rowCount == 0 && !hasSpecialHeader)
                            {
                                if (columnCount == 0)
                                {
                                    sheet.Header.Left = "&B&18&\"Calibri\"" + cell.StringCellValue + "&B&14";
                                }
                                else if (columnCount == 1)
                                {
                                    sheet.Header.Right = "&B&28&\"Calibri\"" + cell.StringCellValue;
                                }
                                else
                                {
                                    sheet.Header.Left += "\n" + cell.StringCellValue;
                                }
                                if (columnCount > 2)
                                {
                                    double margin = sheet.GetMargin(MarginType.TopMargin);
                                    margin += .25;
                                    sheet.SetMargin(MarginType.TopMargin, margin);
                                }
                                cell.CellStyle = headerStyle;
                            }
                            else if (rowCount < 2)
                            {
                                cell.CellStyle = headerStyle;
                            }
                            else if (cell.CellStyle.DataFormat == 0)
                            {

                                cell.CellStyle = normalStyleWBorders;
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                            else if (cell.CellStyle.DataFormat > 0)
                            {
                                cell.CellStyle.SetFont(normalFont);
                                cell.CellStyle.WrapText = true;
                                cell.CellStyle.VerticalAlignment = VerticalAlignment.TOP;
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                                cell.CellStyle.BorderBottom = BorderStyle.THIN;
                                cell.CellStyle.BorderLeft = BorderStyle.THIN;
                                cell.CellStyle.BorderRight = BorderStyle.THIN;
                                cell.CellStyle.BorderTop = BorderStyle.THIN;
                                cell.CellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
                            }
                            columnCount++;
                        }
                    }
                }

                IRow totalRow = sheet.GetRow(numberOfRows + 1);
                numberOfRows += totalRow != null ? 0 : 1;
                totalRow = (totalRow != null ? totalRow : sheet.GetRow(numberOfRows + 1));
                if (totalRow != null)
                {
                    numberOfRows++;
                    var totalCell = totalRow.GetCell(0);
                    if (totalCell != null)
                    {
                        totalCell.CellStyle = normalStyleNotWrapped;
                    }
                }
                var heightRow = sheet.CreateRow(numberOfRows + 1);
                heightRow.HeightInPoints = 15.75F;
                sheet.AutoResizeColumns(columns);
                if (!hasSpecialHeader)
                {
                    sheet.CreateFreezePane(0, 2, 0, 2);
                }
            }
        }

        public static void FinishWritingToExcelSpreadsheet(this HSSFWorkbook workBook, int columns, int specialHeaderRowCount, int sheetIndex)
        {
            bool hasSpecialHeader = specialHeaderRowCount != 0;
            IFont headerFont = workBook.CreateFont();
            headerFont.FontName = "Calibri";
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            ICellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            IFont normalFont = workBook.CreateFont();
            normalFont.FontName = "Calibri";
            normalFont.FontHeightInPoints = 12;

            ICellStyle normalStyle = workBook.CreateCellStyle();
            normalStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            normalStyle.VerticalAlignment = VerticalAlignment.TOP;
            normalStyle.SetFont(normalFont);

            ICellStyle normalStyleNotWrapped = workBook.CreateCellStyle();
            normalStyleNotWrapped.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            normalStyleNotWrapped.VerticalAlignment = VerticalAlignment.TOP;
            normalStyleNotWrapped.SetFont(normalFont);

            ICellStyle normalStyleWBorders = workBook.CreateCellStyle();
            normalStyleWBorders.VerticalAlignment = VerticalAlignment.TOP;
            normalStyleWBorders.SetFont(normalFont);
            normalStyleWBorders.BorderBottom = BorderStyle.THIN;
            normalStyleWBorders.BorderLeft = BorderStyle.THIN;
            normalStyleWBorders.BorderRight = BorderStyle.THIN;
            normalStyleWBorders.BorderTop = BorderStyle.THIN;
            normalStyleWBorders.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;

            ISheet sheet = workBook.GetSheetAt(sheetIndex);
            if (sheet != null)
            {
                int numberOfRows = sheet.PhysicalNumberOfRows;
                if (!hasSpecialHeader)
                {
                    if (columns <= 26)
                    {
                        char letter = (char)(64 + columns);
                        workBook.SetPrintArea(0, "A2:" + letter + (numberOfRows + 2));
                    }
                    else if (columns <= 52)
                    {
                        char letter = (char)(64 + columns % 26);
                        workBook.SetPrintArea(0, "A2:A" + letter + (numberOfRows + 2));
                    }
                }


                for (int rowCount = 0; rowCount < numberOfRows; rowCount++)
                {
                    IRow row = sheet.GetRow(rowCount);
                    if (row != null)
                    {
                        var cells = row.Cells;
                        int columnCount = 0;
                        foreach (var cell in cells)
                        {
                            if (hasSpecialHeader && rowCount < (specialHeaderRowCount + 1))
                            {

                            }
                            else if (rowCount == 0 && !hasSpecialHeader)
                            {
                                if (columnCount == 0)
                                {
                                    sheet.Header.Left = "&B&18&\"Calibri\"" + cell.StringCellValue + "&B&14";
                                }
                                else if (columnCount == 1)
                                {
                                    sheet.Header.Right = "&B&28&\"Calibri\"" + cell.StringCellValue;
                                }
                                else
                                {
                                    sheet.Header.Left += "\n" + cell.StringCellValue;
                                }
                                if (columnCount > 2)
                                {
                                    double margin = sheet.GetMargin(MarginType.TopMargin);
                                    margin += .25;
                                    sheet.SetMargin(MarginType.TopMargin, margin);
                                }
                                cell.CellStyle = headerStyle;
                            }
                            else if (rowCount < 2)
                            {
                                cell.CellStyle = headerStyle;
                            }
                            else if (cell.CellStyle.DataFormat == 0)
                            {

                                cell.CellStyle = normalStyleWBorders;
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                            else if (cell.CellStyle.DataFormat > 0)
                            {
                                cell.CellStyle.SetFont(normalFont);
                                cell.CellStyle.WrapText = true;
                                cell.CellStyle.VerticalAlignment = VerticalAlignment.TOP;
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                                cell.CellStyle.BorderBottom = BorderStyle.THIN;
                                cell.CellStyle.BorderLeft = BorderStyle.THIN;
                                cell.CellStyle.BorderRight = BorderStyle.THIN;
                                cell.CellStyle.BorderTop = BorderStyle.THIN;
                                cell.CellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
                            }
                            columnCount++;
                        }
                    }
                }

                IRow totalRow = sheet.GetRow(numberOfRows + 1);
                numberOfRows += totalRow != null ? 0 : 1;
                totalRow = (totalRow != null ? totalRow : sheet.GetRow(numberOfRows + 1));
                if (totalRow != null)
                {
                    numberOfRows++;
                    var totalCell = totalRow.GetCell(0);
                    if (totalCell != null)
                    {
                        totalCell.CellStyle = normalStyleNotWrapped;
                    }
                }
                var heightRow = sheet.CreateRow(numberOfRows + 1);
                heightRow.HeightInPoints = 15.75F;
                sheet.AutoResizeColumns(columns);
                if (!hasSpecialHeader)
                {
                    sheet.CreateFreezePane(0, 2, 0, 2);
                }
            }
        }

        public static void FinishWritingToExcelSpreadsheetWithMultipleHeaders(this HSSFWorkbook workBook, int columns, List<string> possibleHeaderStarters, int headerRowCount)
        {
            
            IFont headerFont = workBook.CreateFont();
            headerFont.FontName = "Calibri";
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            ICellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.Alignment = HorizontalAlignment.LEFT;
            headerStyle.SetFont(headerFont);

            ICellStyle headerStyleCentered = workBook.CreateCellStyle();
            headerStyleCentered.Alignment = HorizontalAlignment.CENTER;
            headerStyleCentered.SetFont(headerFont);

            IFont normalFont = workBook.CreateFont();
            normalFont.FontName = "Calibri";
            normalFont.FontHeightInPoints = 12;

            ICellStyle normalStyle = workBook.CreateCellStyle();
            normalStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            normalStyle.VerticalAlignment = VerticalAlignment.TOP;
            normalStyle.SetFont(normalFont);

            ICellStyle normalStyleNotWrapped = workBook.CreateCellStyle();
            normalStyleNotWrapped.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            normalStyleNotWrapped.VerticalAlignment = VerticalAlignment.TOP;
            normalStyleNotWrapped.SetFont(normalFont);

            ICellStyle normalStyleWBorders = workBook.CreateCellStyle();
            normalStyleWBorders.VerticalAlignment = VerticalAlignment.TOP;
            normalStyleWBorders.SetFont(normalFont);
            normalStyleWBorders.BorderBottom = BorderStyle.THIN;
            normalStyleWBorders.BorderLeft = BorderStyle.THIN;
            normalStyleWBorders.BorderRight = BorderStyle.THIN;
            normalStyleWBorders.BorderTop = BorderStyle.THIN;
            normalStyleWBorders.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;

            ISheet sheet = workBook.GetSheetAt(0);
            if (sheet != null)
            {
                int numberOfRows = sheet.PhysicalNumberOfRows;
                bool isHeader = false;
                int headerCount = 0;
                for (int rowCount = 0; rowCount < numberOfRows; rowCount++)
                {
                    IRow row = sheet.GetRow(rowCount);
                    if (row != null)
                    {
                        if (isHeader)
                        {
                            headerCount++;
                            if (headerCount == headerRowCount)
                            {
                                headerCount = 0;
                                isHeader = false;
                            }
                            //if (headerCount == 1)
                            //{
                            //    //CellRangeAddress cra = new CellRangeAddress(rowCount, rowCount, 0, 2);
                            //    //sheet.AddMergedRegion(cra);
                            //}
                            //else if (headerCount == 3)
                            //{
                            //    headerCount = 0;
                            //    isHeader = false;
                            //}
                        }

                        var cells = row.Cells;
                        int columnCount = 0;
                        foreach (var cell in cells)
                        {
                            //var cell = ((Cell)cells.Current);
                            if (rowCount == 0)
                            {
                                if (columnCount == 0)
                                {
                                    sheet.Header.Left = "&B&18&\"Calibri\"" + cell.StringCellValue + "&B&14";
                                }
                                else if (columnCount == 1)
                                {
                                    sheet.Header.Right = "&B&28&\"Calibri\"" + cell.StringCellValue;
                                }
                                else
                                {
                                    sheet.Header.Left += "\n" + cell.StringCellValue;
                                }
                                cell.CellStyle = headerStyle;
                            }
                            else if (possibleHeaderStarters.Contains(cell.GetCellValue()))
                            {
                                isHeader = true;
                                cell.CellStyle = headerStyle;
                            }
                            else if (headerCount > 0 && headerCount < 3)
                            {
                                cell.CellStyle = headerStyle;
                            }
                            else if (cell.CellStyle.DataFormat == 0)
                            {
                                cell.CellStyle = normalStyleWBorders;
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                            else if (cell.CellStyle.DataFormat > 0)
                            {
                                cell.CellStyle.SetFont(normalFont);
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                                cell.CellStyle.BorderBottom = BorderStyle.THIN;
                                cell.CellStyle.BorderLeft = BorderStyle.THIN;
                                cell.CellStyle.BorderRight = BorderStyle.THIN;
                                cell.CellStyle.BorderTop = BorderStyle.THIN;
                                cell.CellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
                            }
                            columnCount++;
                        }
                    }
                }

                IRow totalRow = sheet.GetRow(numberOfRows + 1);
                if (totalRow != null)
                {
                    var cells = totalRow.Cells;
                    foreach (var cell in cells)
                    {
                        if (cell.CellStyle.DataFormat == 0)
                        {
                            cell.CellStyle = normalStyleNotWrapped;
                        }
                    }
                }
                sheet.AutoResizeColumns(columns);
                sheet.CreateFreezePane(0, 1, 0, 1);
                char letter = (char)(64 + columns);
                workBook.SetPrintArea(0, "A2:" + letter + numberOfRows);
            }
        }

        public static void FinishWritingToGroupedExcelSpreadsheet(this HSSFWorkbook workBook, int columns, List<int> headerRowIndexs, List<int> groupTotalRowIndexs)
        {
            IFont headerFont = workBook.CreateFont();
            headerFont.FontName = "Calibri";
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            ICellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.Alignment = HorizontalAlignment.LEFT;
            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.SetFont(headerFont);

            ICellStyle groupTotalStyle = workBook.CreateCellStyle();
            groupTotalStyle.Alignment = HorizontalAlignment.LEFT;
            groupTotalStyle.BorderTop = BorderStyle.THIN;
            groupTotalStyle.SetFont(headerFont);

            ICellStyle totalStyle = workBook.CreateCellStyle();
            totalStyle.Alignment = HorizontalAlignment.LEFT;
            totalStyle.SetFont(headerFont);

            IFont normalFont = workBook.CreateFont();
            normalFont.FontName = "Calibri";
            normalFont.FontHeightInPoints = 12;

            ICellStyle normalStyle = workBook.CreateCellStyle();
            normalStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
            normalStyle.VerticalAlignment = VerticalAlignment.TOP;
            normalStyle.SetFont(normalFont);

            ICellStyle normalStyleWBorders = workBook.CreateCellStyle();
            normalStyleWBorders.VerticalAlignment = VerticalAlignment.TOP;
            normalStyleWBorders.SetFont(normalFont);
            normalStyleWBorders.BorderBottom = BorderStyle.THIN;
            normalStyleWBorders.BorderLeft = BorderStyle.THIN;
            normalStyleWBorders.BorderRight = BorderStyle.THIN;
            normalStyleWBorders.BorderTop = BorderStyle.THIN;
            normalStyleWBorders.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;

            ISheet sheet = workBook.GetSheetAt(0);
            if (sheet != null)
            {
                int numberOfRows = sheet.PhysicalNumberOfRows;
                for (int rowCount = 0; rowCount < numberOfRows; rowCount++)
                {
                    IRow row = sheet.GetRow(rowCount);
                    if (row != null)
                    {
                        var cells = row.Cells;
                        int columnCount = 0;
                        foreach (var cell in cells)
                        {
                            if (headerRowIndexs.Contains(rowCount))
                            {
                                cell.CellStyle = headerStyle;
                            }
                            else if (groupTotalRowIndexs != null && groupTotalRowIndexs.Contains(rowCount))
                            {
                                cell.CellStyle = groupTotalStyle;
                            }
                            else
                            {
                                if (rowCount == 0)
                                {
                                    if (columnCount == 0)
                                    {
                                        sheet.Header.Left = "&B&18&\"Calibri\"" + cell.StringCellValue + "&B&14";
                                    }
                                    else if (columnCount == 1)
                                    {
                                        sheet.Header.Right = "&B&28&\"Calibri\"" + cell.StringCellValue;
                                    }
                                    else
                                    {
                                        sheet.Header.Left += "\n" + cell.StringCellValue;
                                    }
                                    cell.CellStyle = headerStyle;
                                }
                                else if (cell.CellStyle.DataFormat == 0)
                                {
                                    cell.CellStyle = normalStyle;
                                    cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                                }
                                else if (cell.CellStyle.DataFormat > 0)
                                {
                                    cell.CellStyle.SetFont(normalFont);
                                    cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                                    //cell.CellStyle.BorderBottom = BorderStyle.THIN;
                                    //cell.CellStyle.BorderLeft = BorderStyle.THIN;
                                    //cell.CellStyle.BorderRight = BorderStyle.THIN;
                                    //cell.CellStyle.BorderTop = BorderStyle.THIN;
                                    cell.CellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.WHITE.index;
                                }
                                columnCount++;
                            }
                        }
                    }
                }

                IRow totalRow = sheet.GetRow(numberOfRows - 1);
                totalRow = totalRow == null ? sheet.GetRow(numberOfRows) : totalRow;
                totalRow = totalRow == null ? sheet.GetRow(numberOfRows + 1) : totalRow;
                if (totalRow != null)
                {
                    var cells = totalRow.Cells;
                    foreach (var cell in cells)
                    {
                        if (cell.CellStyle.DataFormat == 0)
                        {
                            cell.CellStyle = totalStyle;
                        }
                    }
                }
                sheet.AutoResizeColumns(columns);
                sheet.CreateFreezePane(0, 1, 0, 1);
                char letter = (char)(64 + columns);
                workBook.SetPrintArea(0, "A2:" + letter + numberOfRows);
            }
        }

        public static int WriteHeaderOfExcelSpreadsheet(this HSSFWorkbook workBook, string agencyName,
            string agencyLocation, string reportName, Dictionary<string, string> titleHeaders)
        {
            ISheet sheet = workBook.GetSheetAt(0);
            
            var titleRow = sheet.GetRow(0);
            titleRow.GetCell(0).SetCellValue(agencyName);

            var reportCell = titleRow.GetCell(3);
            reportCell.SetCellValue(reportName);
            sheet.GetRow(1).GetCell(0).SetCellValue(agencyLocation);

            int rowNum = 2;
            if (titleHeaders != null)
            {
                foreach (var headers in titleHeaders)
                {
                    var thRow = sheet.GetRow(rowNum);
                    thRow.GetCell(0).SetCellValue(headers.Key);
                    thRow.GetCell(0).CellStyle.Alignment = HorizontalAlignment.LEFT;
                    thRow.GetCell(1).SetCellValue(headers.Value);
                    thRow.GetCell(1).CellStyle.Alignment = HorizontalAlignment.LEFT;
                    rowNum++;
                }
            }
            rowNum++;

            return rowNum >= 8 ? rowNum : 8;
           
        }


        public static void CreateMergedRegionStyles(this HSSFWorkbook workBook)
        {
            ICellStyle allBorders = workBook.CreateCellStyle();
            allBorders.BorderBottom = BorderStyle.THIN;
            allBorders.BorderLeft = BorderStyle.THIN;
            allBorders.BorderRight = BorderStyle.THIN;
            allBorders.BorderTop = BorderStyle.THIN;
            allBorders.BottomBorderColor = HSSFColor.BLACK.index;
            allBorders.TopBorderColor = HSSFColor.BLACK.index;
            allBorders.LeftBorderColor = HSSFColor.BLACK.index;
            allBorders.RightBorderColor = HSSFColor.BLACK.index;

            ICellStyle borders1 = workBook.CreateCellStyle();
            borders1.BorderBottom = BorderStyle.THIN;
            borders1.BorderLeft = BorderStyle.THIN;
            borders1.BorderTop = BorderStyle.THIN;
            borders1.BottomBorderColor = HSSFColor.BLACK.index;
            borders1.TopBorderColor = HSSFColor.BLACK.index;
            borders1.LeftBorderColor = HSSFColor.BLACK.index;

            ICellStyle borders2 = workBook.CreateCellStyle();
            borders2.BorderBottom = BorderStyle.THIN;
            borders2.BorderRight = BorderStyle.THIN;
            borders2.BorderTop = BorderStyle.THIN;
            borders2.BottomBorderColor = HSSFColor.BLACK.index;
            borders2.TopBorderColor = HSSFColor.BLACK.index;
            borders2.RightBorderColor = HSSFColor.BLACK.index;

            ICellStyle borders3 = workBook.CreateCellStyle();
            borders3.BorderBottom = BorderStyle.THIN;
            borders3.BorderTop = BorderStyle.THIN;
            borders3.BottomBorderColor = HSSFColor.BLACK.index;
            borders3.TopBorderColor = HSSFColor.BLACK.index;

            ICellStyle borders4 = workBook.CreateCellStyle();
            borders4.BorderRight = BorderStyle.THIN;
            borders4.FillPattern = FillPatternType.SOLID_FOREGROUND;
            borders4.FillForegroundColor = 9;
            borders4.RightBorderColor = 0;
            borders4.BottomBorderColor = 0;
            borders4.TopBorderColor = 0;
            borders4.LeftBorderColor = 0;

            ICellStyle borders5 = workBook.CreateCellStyle();
            borders5.BorderRight = BorderStyle.THIN;
            borders5.FillPattern = FillPatternType.SOLID_FOREGROUND;
            borders5.FillForegroundColor = 9;
            borders5.RightBorderColor = HSSFColor.BLACK.index;
            borders5.BottomBorderColor = 0;
            borders5.TopBorderColor = 0;
            borders5.LeftBorderColor = 0;
        }

        public static void SetMergedRegionBorder(this HSSFWorkbook workBook, HSSFSheet sheet, CellRangeAddress region)
        {
            SetMergedRegionBorder(workBook, sheet, region, HSSFColor.BLACK.index, BorderStyle.THIN);
        }

        public static void SetMergedRegionBorder(this HSSFWorkbook workBook, HSSFSheet sheet, CellRangeAddress region, BorderStyle borderStyle)
        {
            SetMergedRegionBorder(workBook, sheet, region, HSSFColor.BLACK.index, borderStyle);
        }

        public static void SetMergedRegionBorder(this HSSFWorkbook workBook, HSSFSheet sheet, CellRangeAddress region, int color, BorderStyle borderStyle)
        {
            HSSFRegionUtil.SetBorderRight(borderStyle, region, sheet, workBook);
            HSSFRegionUtil.SetBorderLeft(borderStyle, region, sheet, workBook);
            HSSFRegionUtil.SetBorderTop(borderStyle, region, sheet, workBook);
            HSSFRegionUtil.SetBorderBottom(borderStyle, region, sheet, workBook);

            //HSSFRegionUtil.SetRightBorderColor(color, region, sheet, workBook);
            //HSSFRegionUtil.SetLeftBorderColor(color, region, sheet, workBook);
            //HSSFRegionUtil.SetTopBorderColor(color, region, sheet, workBook);
            //HSSFRegionUtil.SetBottomBorderColor(color, region, sheet, workBook);
        }

        public static void AutoResizeColumns(this ISheet sheet, int columns)
        {
            int resizeColCounter = 0;
            do
            {
                sheet.AutoSizeColumn(resizeColCounter);
                resizeColCounter++;
            }
            while (resizeColCounter < columns);
            sheet.PrintSetup.Landscape = true;
            sheet.PrintSetup.FitHeight = 0;
            sheet.FitToPage = true;
        }

        private static string GetCellValue(this ICell cell)
        {
            string result = "";
            if (cell is HSSFCell)
            {
                HSSFCell hssfCell = cell as HSSFCell;
                
                switch (hssfCell.CellType)
                {
                    case CellType.STRING:
                        result = hssfCell.StringCellValue;
                        break;
                    case CellType.NUMERIC:
                        result = hssfCell.NumericCellValue.ToString();
                        break;
                }
            }
            return result;
        }
    }
}
