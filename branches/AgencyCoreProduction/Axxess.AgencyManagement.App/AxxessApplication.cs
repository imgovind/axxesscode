﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Threading;
    using System.Web.Routing;
    using System.Diagnostics;
    using System.Web.SessionState;

    using Security;
    using Extensions;
    using Controllers;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;
    using Axxess.Log.Enums;

    public class AxxessApplication : HttpApplication
    {
        public override void Init()
        {
            base.Init();
            MvcHandler.DisableMvcResponseHeader = true;
            this.AcquireRequestState += new EventHandler(AxxessApplication_AcquireRequestState);
        }

        protected void AxxessApplication_AcquireRequestState(object sender, EventArgs e)
        {
            if (Context.Handler is MvcHandler 
               && Context.User != null 
               && Context.User.Identity.IsAuthenticated)
            {
                IMembershipService membershipService = Container.Resolve<IMembershipService>();
                IFormsAuthenticationService formAuthService = Container.Resolve<IFormsAuthenticationService>();

                var userName = Context.User.Identity.Name;
                AxxessPrincipal principal = membershipService.Get(userName);
                if (principal != null)
                {
                    Thread.CurrentPrincipal = principal;
                    HttpContext.Current.User = principal;

                    SessionMonitor.Instance.Set(Current.LoginId, SessionStore.SessionId);

                    if (principal.IsImpersonated())
                    {
                        return;
                    }
                    else
                    {
                        if (Current.CanContinue)
                        {
                            if (AppSettings.IsSingleUserMode)
                            {
                                if (principal.IsSingleSession())
                                {
                                    return;
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                }
                membershipService.LogOff(userName);
                formAuthService.SignOut();
                formAuthService.RedirectToLogin();
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.UserId.ToString(), LogType.User, LogAction.UserLoggedIn, string.Empty);
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.Run();

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new MobileCapableWebFormViewEngine());
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                if (exception.Message == "FileUploadMoreThanMaxSize")
                {
                    Response.Clear();
                    Server.ClearError();
                    Response.TrySkipIisCustomErrors = true;
                    Response.StatusCode = 200;
                    Response.ContentType = "application/json";
                    Response.StatusDescription = "Error in file size";
                    var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "An attachment is larger than the maximum allowed file size. The maximum allowed file size is 10 MBs(Mega Bytes)." };
                    Response.Write(viewData.ToJson());
                    Response.End();
                }
                else if (exception is HttpRequestValidationException)
                {
                    Response.Clear();
                    Server.ClearError();
                    Response.TrySkipIisCustomErrors = true;
                    Response.StatusCode = 400;
                    Response.ContentType = "application/json";
                    Response.StatusDescription = "A potentially dangerous request was detected";
                    Response.AppendHeader("Content-Disposition", "alert");
                    var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "The form contains potentially dangerous values. Ensure that special characters such as #, $, <, &, etc. are not next to each other." };
                    Response.Write(viewData.ToJson());
                    Response.End();
                }
                else
                {
                    var routeData = new RouteData();
                    routeData.Values["controller"] = "Error";
                    routeData.Values["action"] = "ApplicationError";
                    routeData.Values["error"] = exception.Message;

                    var httpException = exception as HttpException;
                    if (httpException != null)
                    {
                        int statusCode = httpException.GetHttpCode();
                        switch (statusCode)
                        {
                            case 401:
                                routeData.Values["action"] = "NotAuthorized";
                                break;
                            case 403:
                                routeData.Values["action"] = "Forbidden";
                                break;
                            case 404:
                                routeData.Values["action"] = "FileNotFound";
                                break;
                            default:
                                Logger.Exception(exception);
                                routeData.Values["action"] = "ApplicationError";
                                break;
                        }
                        Response.Clear();
                        Server.ClearError();
                    }
                    Response.TrySkipIisCustomErrors = true;
                    IController errorController = new ErrorController();
                    HttpContextWrapper wrapper = new HttpContextWrapper(Context);
                    var requestContext = new RequestContext(wrapper, routeData);
                    errorController.Execute(requestContext);
                }
            }
        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            if (context.Request.IsAuthenticated && custom == "Agency")
            {
                return Current.AgencyId.ToString();
            }
            else
            {
                return base.GetVaryByCustomString(context, custom);
            }
        }
    }
}
