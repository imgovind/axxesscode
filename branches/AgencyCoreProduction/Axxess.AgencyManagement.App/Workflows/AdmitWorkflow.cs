﻿namespace Axxess.AgencyManagement.App.Workflows
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.App.Services;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Enums;

    public class AdmitWorkflow
    {
        #region AdmitPatientWorkflow Members

        private Patient patient { get; set; }

        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;

        public AdmitWorkflow(Patient patient)
        {
            Check.Argument.IsNotNull(patient, "patient");

            this.patient = patient;

            this.patientService = Container.Resolve<IPatientService>();
            this.userRepository = Container.Resolve<IAgencyManagementDataProvider>().UserRepository;
            this.patientRepository = Container.Resolve<IAgencyManagementDataProvider>().PatientRepository;
            this.billingRepository = Container.Resolve<IAgencyManagementDataProvider>().BillingRepository;
            this.physicianRepository = Container.Resolve<IAgencyManagementDataProvider>().PhysicianRepository;

            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        public void Process()
        {
            var work = new WorkSequence();
            var medId = Guid.NewGuid();
            work.Complete += (sequence) =>
            {
                this.isCommitted = this.message.IsNullOrEmpty();
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.message = item.Description;
            };

            if (patient.ShouldCreateEpisode && patient.UserId != Guid.Empty)
            {
                work.Add(() =>
                {
                    if (patient.PharmacyPhoneArray != null && patient.PharmacyPhoneArray.Count > 0)
                    {
                        patient.PharmacyPhone = patient.PharmacyPhoneArray.ToArray().PhoneEncode();
                    }
                    return patientService.CreateMedicationProfile(patient, medId);
                }, () => this.patientRepository.DeleteMedicationProfile(Current.AgencyId, this.patient.Id, medId),
                    "System should not create medication profile for this patient."
                );
                Guid episodeId = Guid.Empty;
                Guid face2FaceId = Guid.Empty;
                work.Add(() => this.patientService.CreateEpisodeAndClaims(this.patient, out episodeId, out face2FaceId),
                    () => this.patientService.RemoveEpisodeAndClaims(this.patient, episodeId, face2FaceId),
                    "System could not create the Patient Episode information.");
            }
            else
            {
                work.Add(() => { return patientRepository.SetStatus(Current.AgencyId, this.patient.Id, PatientStatus.Pending); });
            }

            work.Perform();
        }
        #endregion
    }
}
