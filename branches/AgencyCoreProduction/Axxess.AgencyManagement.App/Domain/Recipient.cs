﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;

    public class Recipient
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}
