﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    public class PreviousNote
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
    }
}
