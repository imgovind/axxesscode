﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System;
    using System.Runtime.Serialization;
    using System.ComponentModel.DataAnnotations;

    using Axxess.Core.Extension;

    using Enums;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Enums;

    [KnownType(typeof(Order))]
    public class Order
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string PatientName { get; set; }
        public string PhysicianName { get; set; }
        public string Text { get; set; }
        public OrderType Type { get; set; }
        public long Number { get; set; }
        public string SentDate { get; set; }
        [DataType(DataType.Date)]
        [UIHint("EmptyDate")]
        public DateTime ReceivedDate { get; set; }
        public string CreatedDate { get; set; }
        public string PrintUrl { get; set; }
        public bool PhysicianAccess { get; set; }
        public string HasPhysicianAccess
        {
            get
            {
                return this.PhysicianAccess ? "Yes" : "No";
            }
        }
        public string PhysicianAccessIcon
        {
            get
            {
                return this.PhysicianAccess ? "<img src='/Images/medical.png' />" : "";
            }
        }
        [DataType(DataType.Date)]
        public DateTime SendDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime PhysicianSignatureDate { get; set; }
        public int Status { get; set; }
        public string StatusName
        {
            get
            {
                ScheduleStatus status = this.Status > 0 && Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? (ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), this.Status.ToString()) : ScheduleStatus.NoStatus;
                if ((status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue) && this.CreatedDate.IsValidDate() && this.CreatedDate.ToDateTime() < DateTime.Today)
                {
                    return ScheduleStatus.CommonNotStarted.GetDescription();
                }
                else
                {
                    return status.GetDescription();
                }
            }
        }

        public string ReceivedDateFormatted { get { return this.ReceivedDate > DateTime.MinValue ? this.ReceivedDate.ToString("MM/dd/yyyy") : string.Empty; } }
        public string SendDateFormatted { get { return this.SendDate > DateTime.MinValue ? this.SendDate.ToString("MM/dd/yyyy") : string.Empty; } }
        public string PhysicianSignatureDateFormatted { get { return this.PhysicianSignatureDate > DateTime.MinValue ? this.PhysicianSignatureDate.ToString("MM/dd/yyyy") : string.Empty; } }
        public string OrderDate
        {
            get
            {
                return CreatedDate.IsNotNullOrEmpty() ? "<span class='float-right'>" + CreatedDate.Split('/')[2] + "</span><span class='float-right'>/</span><span class='float-right'>" + CreatedDate.Split('/')[0] + "/" + CreatedDate.Split('/')[1] + "</span>" : "";
            }
        }


        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid BranchId { get; set; }

    }
}
