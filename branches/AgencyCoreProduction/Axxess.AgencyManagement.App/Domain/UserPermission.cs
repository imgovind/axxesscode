﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace Axxess.AgencyManagement.App.Domain
{
    public class UserPermission
    {
        [ScriptIgnore]
        public int Grouper { get; set; }
        public string Employee { get; set; }
        public string Permission { get; set; }
    }
}
