﻿namespace Axxess.AgencyManagement.App.Domain
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class PasswordReset
    {
        [Required]
        [DisplayName("Login E-mail")]
        public string UserName { get; set; }

        [Required]
        [DisplayName("Security Check")]
        public bool captchaValid { get; set; }
    }
}
