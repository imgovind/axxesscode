﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Exports;
    using Services;

    public static class ReportManager
    {
        private static readonly IAssetService assetService = Container.Resolve<AssetService>();
        private static readonly ILoginRepository loginRepository = Container.Resolve<MembershipDataProvider>().LoginRepository;
        private static readonly IUserRepository userRepository = Container.Resolve<AgencyManagementDataProvider>().UserRepository;
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<AgencyManagementDataProvider>().AgencyRepository;
        private static readonly IMessageRepository messageRepository = Container.Resolve<AgencyManagementDataProvider>().MessageRepository;
        private static readonly IPatientRepository patientRepository = Container.Resolve<AgencyManagementDataProvider>().PatientRepository;

        public static void AddPatientChartDownload(Guid agencyId, Guid episodeId, Guid patientId, Guid reportId)
        {
            var export = new PatientChartDownloader(agencyId, episodeId, patientId);
            CompleteReport(agencyId, reportId, export);
        }
        public static void AddClaimFormDownload(Guid agencyId, Guid branchId, int primaryInsurance, List<Guid> managedClaimSelected, Guid reportId)
        {
            var export = new ClaimFormDownloader(agencyId, branchId, primaryInsurance, managedClaimSelected);
            CompleteReport(agencyId, reportId, export);
        }


        public static void AddPatientsAndVisitsByAgeReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new PatientsAndVisitsByAgeExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddUnbilledVisitsReport(Guid branchId, int insuranceId, int status, DateTime startDate, DateTime endDate, Guid reportId, Guid agencyId, string agencyName)
        {
            var export = new UnbilledVisitsExporter(agencyId, branchId, insuranceId, status, startDate, endDate, agencyName);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddTherapyManagementReport(Guid branchId, DateTime startDate, DateTime endDate, Guid reportId, Guid agencyId, string agencyName)
        {
            var export = new TherapyManagementExporter(agencyId, branchId, startDate, endDate, agencyName);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddPatientListReport(Guid branchId, int StatusId, Guid reportId, Guid agencyId, string agencyName)
        {
            var export = new PatientListExporter(agencyId, StatusId, branchId, agencyName);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddHHRGReport(Guid branchId, DateTime startDate, DateTime endDate, Guid reportId, Guid agencyId, string agencyName)
        {
            var export = new HHRGExporter(agencyId, branchId, startDate, endDate, agencyName);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddEmployeeListReport(Guid branchCode, int StatusId, Guid reportId, Guid agencyId, string agencyName)
        {
            var export = new EmployeeListExporter(agencyId, branchCode, StatusId, agencyName);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddDischargesByReasonReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new DischargesByReasonExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddVisitsByPrimaryPaymentSourceReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new VisitsByPrimaryPaymentSourceExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddVisitsByStaffTypeReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new VisitsByStaffTypeExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddAdmissionsByReferralSourceReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new AdmissionsByReferralSourceExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddPatientsVisitsByPrincipalDiagnosisReport(Guid branchId, int year, Guid reportId, Guid agencyId)
        {
            var startDate = new DateTime(year, 1, 1);
            var endDate = new DateTime(year, 12, 31);

            var export = new PatientsVisitsByPrincipalDiagnosisExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        public static void AddCostReport(Guid branchId, DateTime startDate, DateTime endDate ,Guid reportId, Guid agencyId)
        {
            var export = new CostReportExporter(agencyId, branchId, startDate, endDate);
            CompleteReport(agencyId, reportId, export);
        }

        private static void CompleteReport(Guid agencyId, Guid reportId, BaseExporter export)
        {
            var report = agencyRepository.GetReport(agencyId, reportId);
            if (report != null)
            {
                var bytes = export.Process().ToArray();
                var asset = new Asset
                {
                    Bytes = bytes,
                    AgencyId = agencyId,
                    FileName = export.FileName,
                    ContentType = export.MimeType,
                    FileSize = bytes.Length.ToString()
                };

                if (assetService.AddAsset(asset))
                {
                    report.AssetId = asset.Id;
                    report.Status = "Completed";
                    report.Completed = DateTime.Now;

                    if (agencyRepository.UpdateReport(report))
                    {
                        var user = userRepository.GetUserOnly(report.UserId, agencyId);
                        if (user != null)
                        {
                            if (!user.LoginId.IsEmpty())
                            {
                                var messageId = Guid.NewGuid();
                                var message = new SystemMessage
                                {
                                    Id = messageId,
                                    CreatedBy = "Axxess",
                                    Created = DateTime.Now,
                                    Subject = "Your requested report is ready for download",
                                    Body = "<p>Dear User,</p><p>Go to the \"Report Menu\" and click on \"Completed Reports\" to view your requested report.</p><p>The Axxess Team</p>",
                                };
                                if (messageRepository.AddSystemMessage(message))
                                {
                                    var login = loginRepository.Find(user.LoginId);
                                    if (login != null && login.IsActive)
                                    {
                                        var userMessage = new UserMessage
                                        {
                                            IsRead = false,
                                            UserId = user.Id,
                                            Id = Guid.NewGuid(),
                                            AgencyId = agencyId,
                                            IsDeprecated = false,
                                            FolderId = Guid.Empty,
                                            MessageId = messageId,
                                            MessageType = (int)MessageType.System
                                        };
                                        if (messageRepository.AddUserMessage(userMessage))
                                        {
                                            var parameters = new string[4];
                                            parameters[0] = "recipientfirstname";
                                            parameters[1] = user.DisplayName;
                                            parameters[2] = "senderfullname";
                                            parameters[3] = "Axxess";
                                            var bodyText = MessageBuilder.PrepareTextFrom("NewMessageNotification", parameters);
                                            Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, "Axxess Technology Solutions sent you a message.", bodyText);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                else
                {
                    report.Status = "Failed";
                    report.Completed = DateTime.Now;
                    agencyRepository.UpdateReport(report);
                    if (!asset.Id.IsEmpty())
                    {
                        assetService.RemoveAssetOnly(agencyId, asset.Id);
                    }
                }
            }
        }

        public static JsonViewData RemoveReport(Guid agencyId, Guid reportId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Report could not be deleted." };
            var report = agencyRepository.GetReport(agencyId, reportId);
            if (report != null)
            {
                report.IsDeprecated = true;
                if (agencyRepository.RemoveReport(agencyId, report.Id))
                {
                    if (!report.AssetId.IsEmpty())
                    {
                        if (assetService.RemoveAsset(agencyId, report.AssetId))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "This report has been successfully deleted.";
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "This report has been successfully deleted.";
                }
            }
            return viewData;
        }
    }
}
