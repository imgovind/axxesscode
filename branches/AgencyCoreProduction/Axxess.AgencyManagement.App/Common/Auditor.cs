﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;
    using Axxess.Log.Domain;
    using Axxess.Log.Repositories;
    using Axxess.Core.Enums;

    public static class Auditor
    {
        private static readonly ILogRepository logRepository = Container.Resolve<ILogDataProvider>().LogRepository;

        public static bool Log(Guid episodeId, Guid patientId, Guid entityId, Actions action, DisciplineTasks disciplineTask)
        {
            return Log(episodeId, patientId, entityId, action, ScheduleStatus.NoStatus, disciplineTask, string.Empty);
        }

        public static bool Log(Guid episodeId, Guid patientId, Guid entityId, Actions action, DisciplineTasks disciplineTask, string description)
        {
            return Log(episodeId, patientId, entityId, action, ScheduleStatus.NoStatus, disciplineTask, description);
        }

        public static bool Log(Guid episodeId, Guid patientId, Guid entityId, Actions action, ScheduleStatus status, DisciplineTasks disciplineTask, string description)
        {
            var result = true;

            var auditSilence = HttpContext.Current.Items["AuditSilence"] != null ? HttpContext.Current.Items["AuditSilence"].ToString().ToBoolean() : false;
            if (!auditSilence)
            {
                var log = new TaskLog
                {
                    UserId = Current.UserId,
                    Description = description,
                    AgencyId = Current.AgencyId,
                    UserName = Current.UserFullName,
                    Action = action.ToString(),
                    Status = (int)status,
                    Date = DateTime.Now
                };

                result = SaveAudit(episodeId, patientId, entityId, disciplineTask, log);
            }

            return result;
        }

        public static bool MultiLog(Guid agencyId, List<ScheduleEvent> scheduleEvents, Actions action, string description) 
        {
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var ids = scheduleEvents.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                var taskAudits = logRepository.GetTaskAudits(agencyId, ids);
                var scriptString = string.Empty;
                var log = new TaskLog
                {
                    UserId = Current.UserId,
                    Description = description,
                    AgencyId = Current.AgencyId,
                    UserName = Current.UserFullName,
                    Action = action.ToString(),
                    Date = DateTime.Now
                };

                scheduleEvents.ForEach(s =>
                {
                    var status = 0;
                    log.Status = s.Status;
                    var audit = taskAudits.FirstOrDefault(a => a.EpisodeId == s.EpisodeId && s.Id == a.EntityId);
                    if (audit != null)
                    {
                        var logs = audit.Log.ToObject<List<TaskLog>>();

                        logs.Add(log);
                        var existinglogXml = logs.ToXml();
                        scriptString += string.Format(@"Update taskaudits SET Log = '{0}', Modified = @modified WHERE AgencyId = @agencyid AND PatientId = '{1}' AND EntityId = '{2}' ;", existinglogXml, audit.PatientId, audit.EntityId);
                    }
                    else
                    {
                        var logXmlForNew = new List<TaskLog> { log }.ToXml();
                        scriptString += string.Format(@"INSERT INTO taskaudits ( AgencyId , PatientId, EpisodeId, EntityId , Log , DisciplineTaskId, Created , Modified ) values (@agencyid, '{0}' ,'{1}' ,'{2}' ,'{3}' ,'{4}','{5}' ,@modified );", s.PatientId, s.EpisodeId, s.Id, logXmlForNew, s.DisciplineTask, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                    }
                });
                if (scriptString.IsNotNullOrEmpty())
                {
                    return logRepository.UpdateOrAddTaskAudits(agencyId, scriptString);
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public static bool MultiLog(Guid agencyId, List<ScheduleEvent> scheduleTasks, Actions action, ScheduleStatus status, string description)
        {
            if (scheduleTasks != null && scheduleTasks.Count > 0)
            {
                var ids = scheduleTasks.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                var taskAudits = logRepository.GetTaskAudits(agencyId, ids);
                var scriptString = string.Empty;
                var log = new TaskLog
                {
                    UserId = Current.UserId,
                    Description = description,
                    AgencyId = Current.AgencyId,
                    UserName = Current.UserFullName,
                    Action = action.ToString(),
                    Status = (int)status,
                    Date = DateTime.Now
                };
                var logXmlForNew = new List<TaskLog> { log }.ToXml();
                scheduleTasks.ForEach(s =>
                {
                    var audit = taskAudits.FirstOrDefault(a => s.Id == a.EntityId);
                    if (audit != null)
                    {
                        var logs = audit.Log.ToObject<List<TaskLog>>();
                        logs.Add(log);
                        var existinglogXml = logs.ToXml();
                        scriptString += string.Format(@"Update taskaudits SET Log = '{0}', Modified = @modified WHERE AgencyId = @agencyid AND PatientId = '{1}' AND EntityId = '{2}' ;", existinglogXml, audit.PatientId, audit.EntityId);
                    }
                    else
                    {
                        scriptString += string.Format(@"INSERT INTO taskaudits ( AgencyId , PatientId, EpisodeId, EntityId , Log , DisciplineTaskId, Created , Modified ) values (@agencyid, '{0}' ,'{1}' ,'{2}' ,'{3}' ,'{4}','{5}' ,@modified );", s.PatientId, s.EpisodeId, s.Id, logXmlForNew, s.DisciplineTask, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                    }
                });
                if (scriptString.IsNotNullOrEmpty())
                {
                    return logRepository.UpdateOrAddTaskAudits(agencyId, scriptString);
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public static bool MultiLogOnlyUpdate(Guid agencyId, List<Guid> entityIds, Actions action, ScheduleStatus status, string description)
        {
            if (entityIds != null && entityIds.Count > 0)
            {
                var ids = entityIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var taskAudits = logRepository.GetTaskAudits(agencyId, ids);
                var scriptString = string.Empty;
                var log = new TaskLog
                {
                    UserId = Current.UserId,
                    Description = description,
                    AgencyId = Current.AgencyId,
                    UserName = Current.UserFullName,
                    Action = action.ToString(),
                    Status = (int)status,
                    Date = DateTime.Now
                };
                var logXmlForNew = new List<TaskLog> { log }.ToXml();
                entityIds.ForEach(s =>
                {
                    var audit = taskAudits.FirstOrDefault(a => s == a.EntityId);
                    if (audit != null)
                    {
                        var logs = audit.Log.ToObject<List<TaskLog>>();
                        logs.Add(log);
                        var existinglogXml = logs.ToXml();
                        scriptString += string.Format(@"Update taskaudits SET Log = '{0}', Modified = @modified WHERE AgencyId = @agencyid AND PatientId = '{1}' AND EntityId = '{2}' ;", existinglogXml, audit.PatientId, audit.EntityId);
                    }
                });
                if (scriptString.IsNotNullOrEmpty())
                {
                    return logRepository.UpdateOrAddTaskAudits(agencyId, scriptString);
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public static IList<string> Trail(Guid patientId, Guid entityId, Disciplines disciplineTask)
        {
            var trail = new List<string>();
            var audit = logRepository.GetTaskAudit(Current.AgencyId, patientId, entityId, (int)disciplineTask);

            if (audit != null)
            {
                var logs = audit.Log.ToObject<List<TaskLog>>();
                logs.ForEach(l =>
                {
                    trail.Add(l.ToString());
                });
            }

            return trail;
        }

        private static bool SaveAudit(Guid episodeId, Guid patientId, Guid entityId, DisciplineTasks disciplineTask, TaskLog log)
        {
            var result = false;
            var audit = logRepository.GetTaskAudit(Current.AgencyId, patientId, entityId, (int)disciplineTask);

            if (audit == null)
            {
                audit = new TaskAudit
                {
                    EntityId = entityId,
                    EpisodeId = episodeId,
                    PatientId = patientId,
                    AgencyId = Current.AgencyId,
                    Log = new List<TaskLog> { log }.ToXml(),
                    DisciplineTaskId = (int)disciplineTask
                };

                result = logRepository.AddTaskAudit(audit);
            }
            else
            {
                var logs = audit.Log.ToObject<List<TaskLog>>();
                logs.Add(log);
                audit.Log = logs.ToXml();
                audit.EpisodeId = episodeId;
                result = logRepository.UpdateTaskAudit(audit);
            }

            return result;
        }

        public static bool AddGeneralLog(LogDomain domain, Guid domainId, string entityId, LogType logType, LogAction logAction, string description)
        {
            var audit = new AppAudit { AgencyId = Current.AgencyId, LogDomain = domain.ToString(), DomainId = domainId, EntityId = entityId, UserId = Current.UserId, LogType = logType.ToString(), Action = logAction.ToString(), Date = DateTime.Now, Description = description };
            return logRepository.AddGeneralAudit(audit);
        }
    }
}
