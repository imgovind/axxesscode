﻿namespace Axxess.AgencyManagement.App.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using System.Web.Mvc;
    using Axxess.Core.Extension;

    public class LocatorHelper
    {
        public static List<Locator> ConvertStringToLocatorList(FormCollection formCollection, string stringLocator)
        {
            var locatorList = formCollection[stringLocator].ToArray();
            var locators = new List<Locator>();
            if (locatorList != null && locatorList.Length > 0)
            {
                var keys = formCollection.AllKeys;
                locatorList.ForEach(l =>
                {
                    if (keys.Contains(l + "_Code1") || keys.Contains(l + "_Code2") || keys.Contains(l + "_Code3"))
                    {
                        Locator locator = new Locator(l);
                        if (keys.Contains(l + "_Code1"))
                        {
                            locator.Code1 = formCollection[l + "_Code1"];
                        }
                        if (keys.Contains(l + "_Code2"))
                        {
                            locator.Code2 = formCollection[l + "_Code2"];
                        }
                        
                        if (keys.Contains(l + "_Code3"))
                        {
                            locator.Code3 = formCollection[l + "_Code3"];
                        }
                        if (keys.Contains(l + "_Type"))
                        {
                            locator.Type = (int)Enum.Parse(typeof(LocatorType), formCollection[l + "_Type"]);
                        }
                        locators.Add(locator);
                    }
                });
            }
            return locators;
        }


        
    }
}
