﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web;

    using Axxess.AgencyManagement.Domain;
    using System.Collections.Generic;

    public interface IAssetService
    {
        bool AddAsset(Asset asset);
        bool RemoveAsset(Guid agencyId, Guid assetId);
        bool RemoveAssetOnly(Guid agencyId, Guid assetId);
        bool DeleteAsset(Guid assetId);
        bool AddAsset(HttpPostedFileBase file);
        bool AddAsset(string fileName, string contentType, byte[] bytes);
        bool AddPatientDocument(Asset asset, PatientDocument patientDocument);

        bool AddDocument(PatientDocument patientDocument, HttpFileCollectionBase files);
        List<PatientDocument> GetDocuments(Guid patientId);
        bool DeleteDocument(Guid id, Guid patientId);
        bool EditDocument(PatientDocument patientDocument);
        PatientDocument GetDocumentEdit(Guid patientId, Guid documentId);
    }
}
