﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Collections.Generic;

    using Axxess.AgencyManagement;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    public interface IMessageService
    {
        IList<MessageFolder> FolderList();
        bool AddSystemMessage(Message message);
        bool DeleteMany(List<Guid> messageList);
        int GetTotalMessagesCount(string inboxType);
        Message GetMessage(Guid messageId, Guid userMessageId, int messageTypeId);
        bool DeleteMessage(Guid messageId, int messageTypeId);
        IList<Message> GetMessages(string inboxType, int pageNumber, int pageSize);
        bool SendMessage(Message message, HttpFileCollectionBase httpFiles);
    }
}
