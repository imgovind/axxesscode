﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.Extensions;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Repositories;

    using Axxess.Log.Enums;

    using Enums;
    using Domain;
    using Common;
    using ViewData;
    using Axxess.OasisC.Domain;
    using System.Threading;
    using Axxess.Core.Enums;

    public class AgencyService : IAgencyService
    {
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IEpisodeRepository episodeRepository;
        private readonly IAssessmentRepository assessmentRepository;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider,  ILookUpDataProvider lookUpDataProvider, IOasisCDataProvider oasisDataprovider)
        {
            Check.Argument.IsNotNull(oasisDataprovider, "oasisDataprovider");
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.planofCareRepository = oasisDataprovider.PlanofCareRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.scheduleRepository = agencyManagementDataProvider.ScheduleRepository;
            this.assessmentRepository = oasisDataprovider.OasisAssessmentRepository; ;
            this.episodeRepository = agencyManagementDataProvider.EpisodeRepository;
        }

        public bool CreateAgency(Agency agency)
        {
            try
            {
                agency.IsSuspended = false;
                if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count == 3)
                {
                    agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count == 3)
                {
                    agency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count == 3)
                {
                    agency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                }

                if (agencyRepository.Add(agency))
                {
                    var location = new AgencyLocation();
                    location.Name = agency.LocationName;
                    location.AddressLine1 = agency.AddressLine1;
                    location.AddressLine2 = agency.AddressLine2;
                    location.AddressCity = agency.AddressCity;
                    location.AddressStateCode = agency.AddressStateCode;
                    location.AddressZipCode = agency.AddressZipCode;
                    location.AgencyId = agency.Id;
                    location.IsMainOffice = true;
                    location.IsDeprecated = false;

                    if (agency.PhoneArray != null && agency.PhoneArray.Count > 0)
                    {
                        location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (agency.FaxArray != null && agency.FaxArray.Count > 0)
                    {
                        location.FaxNumber = agency.FaxArray.ToArray().PhoneEncode();
                    }

                    var zipCode = lookupRepository.GetZipCode(agency.AddressZipCode);
                    if (zipCode != null)
                    {
                        location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
                        location.MedicareProviderNumber = agency.MedicareProviderNumber;
                    }
                    var defaultDisciplineTasks = lookupRepository.DisciplineTasks();
                    if (defaultDisciplineTasks != null)
                    {
                        var costRates = new List<ChargeRate>();
                        defaultDisciplineTasks.ForEach(r =>
                        {
                            if (r.DisciplineId == (int)Disciplines.Nursing || r.DisciplineId == (int)Disciplines.PT || r.DisciplineId == (int)Disciplines.OT || r.DisciplineId == (int)Disciplines.ST || r.DisciplineId == (int)Disciplines.HHA || r.DisciplineId == (int)Disciplines.MSW)
                            {
                                costRates.Add(new ChargeRate { Id = r.Id, Code = r.GCode, RevenueCode = r.RevenueCode, Unit = r.Unit, ChargeType = ((int)BillUnitType.Per15Min).ToString(), Charge = r.Rate });
                            }
                        });
                        location.BillData = costRates.ToXml();
                    }
                    //location.Cost = "<ArrayOfCostRate><CostRate><RateDiscipline>SkilledNurse</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseTeaching</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseObservation</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseManagement</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>MedicareSocialWorker</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>HomeHealthAide</RateDiscipline><PerUnit>120</PerUnit></CostRate><CostRate><RateDiscipline>Attendant</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>CompanionCare</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>HomemakerServices</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>PrivateDutySitter</RateDiscipline><PerUnit>0</PerUnit></CostRate></ArrayOfCostRate>";
                    location.Id = Guid.NewGuid();
                    if (agencyRepository.AddLocation(location))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                        var user = new User
                        {
                            AgencyId = agency.Id,
                            AllowWeekendAccess = true,
                            AgencyName = agency.Name,
                            AgencyLocationId = location.Id,
                            Status = (int)UserStatus.Active,
                            LastName = agency.AgencyAdminLastName,
                            FirstName = agency.AgencyAdminFirstName,
                            PermissionsArray = GeneratePermissions(),
                            EmailAddress = agency.AgencyAdminUsername,
                            TitleType = TitleTypes.Administrator.GetDescription(),
                            Profile = new UserProfile() { EmailWork = agency.AgencyAdminUsername },
                            AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() }
                        };

                        IUserService userService = Container.Resolve<IUserService>();
                        return userService.CreateUser(user);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            return false;
        }

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();

            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }

            return list;
        }

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            var zipCode = lookupRepository.GetZipCode(location.AddressZipCode);
            location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
            location.AgencyId = Current.AgencyId;
            location.Id = Guid.NewGuid();
            if (agencyRepository.AddLocation(location))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                result = true;
            }

            return result;
        }

        //public Agency GetAgency(Guid Id)
        //{
        //    Agency agencyViewData = agencyRepository.Get(Id);
        //    if (agencyViewData != null)
        //    {
        //        return agencyViewData;
        //    }
        //    return null;
        //}

        public Agency GetLocationWithAgencyInfo(Guid branchId)
        {
            var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
            if (agency != null)
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId) ?? new AgencyLocation();
                if (location != null)
                {
                    agency.MainLocation = location;
                    if (!location.IsMainOffice && location.IsLocationStandAlone)
                    {
                        agency.TaxId = location.TaxId;
                        agency.TaxIdType = location.TaxIdType;
                        agency.ContactPersonEmail = location.ContactPersonEmail; 
                        agency.ContactPersonPhone = location.ContactPersonPhone;
                        agency.CahpsVendor = location.CahpsVendor;
                        agency.NationalProviderNumber = location.NationalProviderNumber;
                        agency.MedicareProviderNumber = location.MedicareProviderNumber;
                        agency.MedicaidProviderNumber = location.MedicaidProviderNumber;
                        agency.HomeHealthAgencyId = location.HomeHealthAgencyId;
                        agency.ContactPersonFirstName = location.ContactPersonFirstName;
                        agency.ContactPersonLastName = location.ContactPersonLastName;
                    }
                }
            }
            return agency;
            
        }

        public bool CreateContact(AgencyContact contact)
        {
            contact.AgencyId = Current.AgencyId;
            contact.Id = Guid.NewGuid();
            try
            {
                if (agencyRepository.AddContact(contact))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, contact.AgencyId, contact.Id.ToString(), LogType.AgencyContact, LogAction.AgencyContactAdded, string.Empty);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        //public List<UserVisit> GetSchedule()
        //{
        //    var schedule = new List<UserVisit>();
        //    var userEvents = userRepository.GetSchedules(Current.AgencyId);
        //    if (userEvents != null && userEvents.Count > 0)
        //    {
        //        userEvents.ForEach(ue =>
        //        {
        //            var user = userRepository.Get(ue.UserId, Current.AgencyId, false);
        //            if (user != null && !ue.PatientId.IsEmpty())
        //            {
        //                var patient = patientRepository.GetPatientOnly(ue.PatientId, Current.AgencyId);
        //                if (patient != null && !ue.EpisodeId.IsEmpty())
        //                {
        //                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, ue.EpisodeId, patient.Id);
        //                    if (episode != null && episode.IsActive == true && episode.IsDischarged == false)
        //                    {
        //                        var visitNote = string.Empty;
        //                        if (episode.Schedule.IsNotNullOrEmpty())
        //                        {
        //                            var scheduleList = episode.Schedule.ToObject<List<ScheduleEvent>>();
        //                            if (scheduleList != null && scheduleList.Count > 0)
        //                            {
        //                                var scheduledEvent = scheduleList.Find(e => e.EventId == ue.EventId);
        //                                if (scheduledEvent != null && scheduledEvent.Comments.IsNotNullOrEmpty())
        //                                {
        //                                    visitNote = scheduledEvent.Comments;
        //                                }
        //                            }
        //                        }
        //                        schedule.Add(new UserVisit
        //                        {
        //                            Id = ue.EventId,
        //                            UserId = ue.UserId,
        //                            EpisodeId = episode.Id,
        //                            PatientId = patient.Id,
        //                            VisitDate = ue.EventDate.ToZeroFilled(),
        //                            EpisodeNotes = episode.Detail.Comments,
        //                            VisitNotes = visitNote,
        //                            PatientName = patient.DisplayName,
        //                            UserDisplayName = user.DisplayName,
        //                            StatusName = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), ue.Status)).GetDescription(),
        //                            TaskName = ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), ue.DisciplineTask.ToString())).GetDescription()
        //                        });
        //                    }
        //                }
        //            }
        //        });
        //    }
        //    return schedule;
        //}

        private string GetReturnComments(string scheduleCommentString, List<ReturnComment> newComments, List<UserCache> users)
        {
            //string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            //List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in newComments)
            {
                if (comment.IsDeprecated) continue;
                if (scheduleCommentString.IsNotNullOrEmpty())
                {
                    scheduleCommentString += "<hr/>";
                }
                if (comment.UserId == Current.UserId)
                {
                    scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                }
                var userName = string.Empty;
                if (!comment.UserId.IsEmpty())
                {
                    var user = users.FirstOrDefault(u => u.Id == comment.UserId);
                    if (user != null)
                    {
                        userName = user.DisplayName;
                    }
                }
                scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userName, comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return scheduleCommentString;
        }

        public List<PatientEpisodeEvent> GetCaseManagerSchedule(Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var events = new List<PatientEpisodeEvent>();
            var scheduleStatus = ScheduleStatusFactory.CaseManagerStatus();
            //var scheduleStatusString = scheduleStatus.Select(s => s.ToString());
            var scheduleEvents = scheduleRepository.GetScheduleByBranchDateRangeAndStatus(Current.AgencyId, branchId.IsEmpty() ? Current.LocationIds : new List<Guid>() { branchId }, startDate, endDate, status, scheduleStatus.ToArray(), true);
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                var episodeWithEventsDictionary = scheduleEvents.GroupBy(s=>s.EpisodeId).ToDictionary(g => g.First(),g => g.ToList());
                if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
                {
                    var episodeIds = episodeWithEventsDictionary.Keys.Select(s=>s.EpisodeId).ToList();
                    var userIds = new List<Guid>();
                    var users = new List<UserCache>();
                    var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
                    if (returnComments != null && returnComments.Count > 0)
                    {
                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (returnUserIds != null && returnUserIds.Count > 0)
                        {
                            userIds.AddRange(returnUserIds);
                        }
                    }
                    var scheduleUserIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    if (scheduleUserIds != null && scheduleUserIds.Count > 0)
                    {
                        userIds.AddRange(scheduleUserIds);

                    }
                    if (userIds != null && userIds.Count > 0)
                    {
                        users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<UserCache>();
                    }
                    var eventIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.Id.IsEmpty() && s.IsMissedVisit).Select(s => s.Id).Distinct().ToList();
                    var missedVisits = scheduleRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();

                    episodeWithEventsDictionary.ForEach((key, value) =>
                    {
                        if (value != null && value.Count > 0)
                        {
                            var episode = key;
                                value.ForEach(s =>
                                {
                                    var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id && r.EpisodeId == key.EpisodeId).ToList() ?? new List<ReturnComment>();
                                    var redNote = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

                                    var userName = string.Empty;
                                    if (!s.UserId.IsEmpty())
                                    {
                                        var user = users.FirstOrDefault(u => u.Id == s.UserId);
                                        if (user != null)
                                        {
                                            userName = user.DisplayName;
                                        }
                                    }
                                    if (s.IsMissedVisit)
                                    {
                                        var mv = missedVisits.FirstOrDefault(m => m.Id == s.Id); //patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
                                        if (mv != null && mv.Status == (int)ScheduleStatus.NoteMissedVisitPending)
                                        {
                                            var details = episode.EpisodeNotes.IsNotNullOrEmpty() ? episode.EpisodeNotes.ToObject<EpisodeDetail>() : new EpisodeDetail();
                                            Common.Url.Set(s, false, false);
                                            events.Add(new PatientEpisodeEvent
                                            {
                                                EventId = s.Id,
                                                EpisodeId = s.EpisodeId,
                                                PatientId = s.PatientId,
                                                PatientIdNumber = episode.PatientIdNumber,
                                                PrintUrl = s.PrintUrl,
                                                Status = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), mv.Status.ToString())).GetDescription(),
                                                PatientName = episode.PatientName,
                                                TaskName = s.DisciplineTaskName,
                                                UserName = userName,
                                                YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
                                                RedNote = redNote,
                                                BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
                                                EventDate =  s.EventDate.IsValid() ? s.EventDate.ToZeroFilled() : "",
                                                CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.Id, s.DisciplineTask)
                                            });
                                        }
                                    }
                                    else
                                    {
                                        if (scheduleStatus.Contains(s.Status))
                                        {
                                            var details = episode.EpisodeNotes.IsNotNullOrEmpty() ? episode.EpisodeNotes.ToObject<EpisodeDetail>() : new EpisodeDetail();
                                            Common.Url.Set(s, false, false);
                                            events.Add(new PatientEpisodeEvent
                                            {
                                                EventId = s.Id,
                                                EpisodeId = s.EpisodeId,
                                                PatientId = s.PatientId,
                                                PatientIdNumber = episode.PatientIdNumber,
                                                PrintUrl = s.PrintUrl,
                                                Status = s.StatusName,
                                                PatientName = episode.PatientName,
                                                TaskName = s.DisciplineTaskName,
                                                UserName = userName,
                                                YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
                                                RedNote = redNote,
                                                BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
                                                EventDate =  s.EventDate.IsValid() ? s.EventDate.ToZeroFilled() : "",
                                                CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.Id, s.DisciplineTask)
                                            });
                                        }
                                    }
                                });
                        }
                    });
                }
            }
            //var agencyPatientEpisodes = patientRepository.GetPatientEpisodeDataForSchedule(Current.AgencyId, branchId, status, startDate, endDate);
            //if (agencyPatientEpisodes != null && agencyPatientEpisodes.Count > 0)
            //{
            //    var episodeWithEventsDictionary = agencyPatientEpisodes.ToDictionary(g => g.Id,
            //       g => g.Schedule.IsNotNullOrEmpty() ? (g.Schedule.ToObject<List<ScheduleEvent>>() ?? new List<ScheduleEvent>()).Where(s =>
            //             !s.IsDeprecated
            //             && s.EventDate.IsValidDate()
            //             && s.EventDate.ToDateTime().IsBetween(g.StartDate, g.EndDate)
            //              && s.EventDate.ToDateTime().IsBetween(startDate, endDate)
            //             && (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
            //                           || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
            //                           || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
            //                           || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString()
            //                           || s.IsMissedVisit)
            //         ).ToList() : new List<ScheduleEvent>()
            //        );
            //    if (episodeWithEventsDictionary != null && episodeWithEventsDictionary.Count > 0)
            //    {
            //        var episodeIds = episodeWithEventsDictionary.Keys.ToList();
            //        var userIds = new List<Guid>();
            //        var users = new List<User>();
            //        var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
            //        if (returnComments != null && returnComments.Count > 0)
            //        {
            //            var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
            //            if (returnUserIds != null && returnUserIds.Count > 0)
            //            {
            //                userIds.AddRange(returnUserIds);
            //            }
            //        }
            //        var scheduleUserIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
            //        if (scheduleUserIds != null && scheduleUserIds.Count > 0)
            //        {
            //            userIds.AddRange(scheduleUserIds);

            //        }
            //        if (userIds != null && userIds.Count > 0)
            //        {
            //            users = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
            //        }
            //        var eventIds = episodeWithEventsDictionary.SelectMany(s => s.Value).Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
            //        var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();

            //        episodeWithEventsDictionary.ForEach((key, value) =>
            //        {
            //            if (value != null && value.Count > 0)
            //            {
            //                var episode = agencyPatientEpisodes.FirstOrDefault(e => e.Id == key);
            //                if (episode != null)
            //                {
            //                    value.ForEach(s =>
            //                    {
            //                        var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId && r.EpisodeId == key).ToList() ?? new List<ReturnComment>();
            //                        var redNote = this.GetReturnComments(s.ReturnReason, eventReturnReasons, users);

            //                        var userName = string.Empty;
            //                        if (!s.UserId.IsEmpty())
            //                        {
            //                            var user = users.FirstOrDefault(u => u.Id == s.UserId);
            //                            if (user != null)
            //                            {
            //                                userName = user.DisplayName;
            //                            }
            //                        }
            //                        if (s.IsMissedVisit)
            //                        {
            //                            var mv = missedVisits.FirstOrDefault(m => m.Id == s.EventId); //patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
            //                            if (mv != null && mv.Status == (int)ScheduleStatus.NoteMissedVisitPending)
            //                            {
            //                                var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
            //                                Common.Url.Set(s, false, false);
            //                                events.Add(new PatientEpisodeEvent
            //                                {
            //                                    EventId = s.EventId,
            //                                    EpisodeId = s.EpisodeId,
            //                                    PatientId = s.PatientId,
            //                                    PatientIdNumber=episode.PatientIdNumber,
            //                                    PrintUrl = s.PrintUrl,
            //                                    Status = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), mv.Status.ToString())).GetDescription(),
            //                                    PatientName = episode.DisplayName,
            //                                    TaskName = s.DisciplineTaskName,
            //                                    UserName = userName,
            //                                    YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
            //                                    RedNote = redNote,
            //                                    BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
            //                                    EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
            //                                    CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
            //                                });
            //                            }
            //                        }
            //                        else
            //                        {
            //                            if (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
            //                                || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
            //                                || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
            //                                || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString())
            //                            {
            //                                var details = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
            //                                Common.Url.Set(s, false, false);
            //                                events.Add(new PatientEpisodeEvent
            //                                {
            //                                    EventId = s.EventId,
            //                                    EpisodeId = s.EpisodeId,
            //                                    PatientId = s.PatientId,
            //                                    PatientIdNumber = episode.PatientIdNumber,
            //                                    PrintUrl = s.PrintUrl,
            //                                    Status = s.StatusName,
            //                                    PatientName = episode.DisplayName,
            //                                    TaskName = s.DisciplineTaskName,
            //                                    UserName = userName,
            //                                    YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
            //                                    RedNote = redNote,
            //                                    BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
            //                                    EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
            //                                    CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
            //                                });
            //                            }
            //                        }
            //                    });
            //                }
            //            }
            //        });
            //    }
            //}
            return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }

        //public List<PatientEpisodeEvent> GetCaseManagerSchedule(Guid branchId, int status, DateTime startDate, DateTime endDate, bool IsToExcel, bool stickyNoteNeeded)
        //{
        //    var events = new List<PatientEpisodeEvent>();
        //    var scheduleEvents = scheduleRepository.GetScheduleByBranchDateRangeAndStatus(Current.AgencyId, branchId, startDate, endDate, status, ScheduleStatusFactory.CaseManagerStatus().ToArray(), true);
        //    if (scheduleEvents.IsNotNullOrEmpty())
        //    {

        //        var userIds = new List<Guid>();
        //        var users = new List<User>();

        //        List<ReturnComment> returnComments = null;
        //        if (!IsToExcel)
        //        {
        //            if (stickyNoteNeeded)
        //            {
        //                var episodeIds = scheduleEvents.Select(e => e.EpisodeId).Distinct().ToList();
        //                if (episodeIds.IsNotNullOrEmpty())
        //                {
        //                    returnComments = baseScheduleRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
        //                    if (returnComments.IsNotNullOrEmpty())
        //                    {
        //                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
        //                        if (returnUserIds.Count > 0)
        //                        {
        //                            userIds.AddRange(returnUserIds);
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        var scheduleUserIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
        //        if (scheduleUserIds.IsNotNullOrEmpty())
        //        {
        //            userIds.AddRange(scheduleUserIds);
        //        }

        //        if (userIds.IsNotNullOrEmpty())
        //        {
        //            users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<User>();
        //        }
        //        var tasksNeedPlanOfCare = DisciplineTaskFactory.SkilledNurseSharedFile();
        //        tasksNeedPlanOfCare.Add((int)DisciplineTasks.HHAideVisit);

        //        scheduleEvents.ForEach(s =>
        //        {
        //            var userName = string.Empty;
        //            if (!s.UserId.IsEmpty())
        //            {
        //                var user = users.FirstOrDefault(u => u.Id == s.UserId);
        //                if (user != null)
        //                {
        //                    userName = user.DisplayName;
        //                }
        //            }

        //            var redNote = string.Empty;
        //            var detailNote = string.Empty;
        //            if (!IsToExcel)
        //            {
        //                if (stickyNoteNeeded)
        //                {
        //                    detailNote = s.EpisodeNotes.IsNotNullOrEmpty() ? s.EpisodeNotes.Clean() : string.Empty;
        //                    if (returnComments.IsNotNullOrEmpty())
        //                    {
        //                        var eventReturnReasons = returnComments.Where(r => r.EventId == s.Id && r.EpisodeId == s.EpisodeId).ToList();
        //                        if (eventReturnReasons.IsNotNullOrEmpty())
        //                        {
        //                            redNote = TaskHelperFactory<T>.FormatReturnComments(s.ReturnReason, eventReturnReasons, users);
        //                        }
        //                    }
        //                }
        //            }

        //            var task = new PatientEpisodeEvent
        //            {

        //                Status = s.StatusName,
        //                EpisodeRange = string.Format("{0} - {1}", s.StartDate, s.EndDate),
        //                EventDate = s.EventDate,
        //                Time = string.Format("{0} - {1}", s.TimeIn, s.TimeOut),
        //                PatientName = s.PatientName,
        //                TaskName = s.DisciplineTaskName,
        //                UserName = userName,
        //            };
        //            if (!IsToExcel)
        //            {
        //                if (Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask))
        //                {
        //                    task.Type = ((DisciplineTasks)s.DisciplineTask).ToString();
        //                }
        //                task.EventId = s.Id;
        //                task.EpisodeId = s.EpisodeId;
        //                task.PatientId = s.PatientId;
        //                var group = DocumentFactory.GetType(s.DisciplineTask, s.IsMissedVisit);
        //                if (group != DocumentType.None)
        //                {
        //                    task.Group = group.ToString().ToLowerCase();
        //                }
        //                task.Task = s.DisciplineTask;
        //                task.IsPOCNeeded = tasksNeedPlanOfCare.Contains(s.DisciplineTask) && !s.IsMissedVisit;

        //                if (stickyNoteNeeded)
        //                {
        //                    task.YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty;
        //                    task.RedNote = redNote;
        //                    task.BlueNote = detailNote;
        //                }
        //            }

        //            events.Add(task);

        //        });
        //    }
        //    return events.OrderByDescending(e => e.EventDate).ToList();
        //}


        //public List<PatientEpisodeEvent> GetPatientCaseManagerSchedule(Guid patientId)
        //{
        //    var events = new List<PatientEpisodeEvent>();
        //    var patientEpisodes = patientRepository.GetPatientEpisodeDataForSchedule(Current.AgencyId, patientId);
        //    if (patientEpisodes != null && patientEpisodes.Count > 0)
        //    {
        //        var users = new List<User>();
        //        patientEpisodes.ForEach(e =>
        //        {
        //            if (e.Schedule.IsNotNullOrEmpty())
        //            {
        //                var scheduledEvents = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
        //                                || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
        //                                || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
        //                                || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString()
        //                                || s.Status == ((int)ScheduleStatus.NoteMissedVisitPending).ToString()));
        //                if (scheduledEvents != null && scheduledEvents.Count() > 0)
        //                {
        //                    var scheduleUserIds = scheduledEvents.Where(s => !s.UserId.IsEmpty() && !users.Exists(us => us.Id == s.UserId)).Select(s => s.UserId).Distinct().ToList();
        //                    if (scheduleUserIds != null && scheduleUserIds.Count > 0)
        //                    {
        //                        var scheduleUsers = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, scheduleUserIds) ?? new List<User>();
        //                        if (scheduleUsers != null && scheduleUsers.Count > 0)
        //                        {
        //                            users.AddRange(scheduleUsers);
        //                        }
        //                    }
        //                    var eventIds = scheduledEvents.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
        //                    var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();

        //                    scheduledEvents.ForEach(s =>
        //                    {
        //                        var userName = string.Empty;
        //                        if (!s.UserId.IsEmpty())
        //                        {
        //                            var user = users.FirstOrDefault(u => u.Id == s.UserId);
        //                            if (user != null)
        //                            {
        //                                userName = user.DisplayName;
        //                            }
        //                        }
        //                        if (s.IsMissedVisit)
        //                        {
        //                            var mv = missedVisits.FirstOrDefault(m => m.Id == s.EventId);//patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
        //                            if (mv != null && mv.Status == (int)ScheduleStatus.NoteMissedVisitPending)
        //                            {
        //                                var details = e.Details.IsNotNullOrEmpty() ? e.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
        //                                Common.Url.Set(s, false, false);
        //                                events.Add(new PatientEpisodeEvent
        //                                {
        //                                    PrintUrl = s.PrintUrl,
        //                                    Status = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), mv.Status.ToString())).GetDescription(),
        //                                    PatientName = e.PatientName,
        //                                    TaskName = s.DisciplineTaskName,
        //                                    UserName = userName,
        //                                    EpisodeRange = string.Format("{0} - {1}", e.StartDate, e.EndDate),
        //                                    YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
        //                                    RedNote = s.StatusComment.IsNotNullOrEmpty() ? s.StatusComment : string.Empty,
        //                                    BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
        //                                    EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
        //                                    CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
        //                                });
        //                            }
        //                        }
        //                        else
        //                        {
        //                            if (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
        //                                || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
        //                                || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
        //                                || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString())
        //                            {
        //                                var details = e.Details.IsNotNullOrEmpty() ? e.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
        //                                Common.Url.Set(s, false, false);
        //                                events.Add(new PatientEpisodeEvent
        //                                {
        //                                    PrintUrl = s.PrintUrl,
        //                                    Status = s.StatusName,
        //                                    PatientName = e.PatientName,
        //                                    TaskName = s.DisciplineTaskName,
        //                                    UserName = userName,
        //                                    EpisodeRange = string.Format("{0} - {1}", e.StartDate, e.EndDate),
        //                                    YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
        //                                    RedNote = s.StatusComment.IsNotNullOrEmpty() ? s.StatusComment : string.Empty,
        //                                    BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
        //                                    EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
        //                                    CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
        //                                });
        //                            }
        //                        }
        //                    });
        //                }
        //            }
        //            e.Schedule = string.Empty;
        //        });
        //    }
        //    return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        //}

        //public List<RecertEvent> GetRecertsPastDue()
        //{
        //    var pastDueRecerts = new List<RecertEvent>();
        //    var recertEpisodes = patientRepository.GetPastDueRecertsLean(Current.AgencyId);
        //    if (recertEpisodes != null && recertEpisodes.Count > 0)
        //    {
        //        recertEpisodes.ForEach(r =>
        //        {
        //            if (r.Schedule.IsNotNullOrEmpty())
        //            {
        //                var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
        //                if (schedule != null && schedule.Count > 0)
        //                {
        //                    var dischargeSchedules = schedule.Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && 
        //                        ((s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge ||
        //                        s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT ||
        //                        s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT ||
        //                        s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeST ||
        //                        s.DisciplineTask == (int)DisciplineTasks.OASISCDeath || 
        //                        s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT ||
        //                        s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT || 
        //                        s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge ||
        //                        s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge || 
        //                        s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome ||
        //                        s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge)
        //                        || (s.EventDate.ToDateTime().IsBetween(r.TargetDate.AddDays(-5), r.TargetDate) && (s.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransfer)))
        //                        && (s.Status == ((int)ScheduleStatus.OasisNotStarted).ToString() || s.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || s.Status == ((int)ScheduleStatus.OasisReopened).ToString() || s.Status == ((int)ScheduleStatus.OasisSaved).ToString())).ToList();
        //                    if (dischargeSchedules != null && dischargeSchedules.Count > 0)
        //                    {
        //                    }
        //                    else
        //                    {
        //                        var recertSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionofCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
        //                        if (recertSchedule != null)
        //                        {
        //                            if ((recertSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || recertSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || recertSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || recertSchedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
        //                            {

        //                            }
        //                            else
        //                            {
        //                                if (!recertSchedule.UserId.IsEmpty())
        //                                {
        //                                    r.AssignedTo = UserEngine.GetName(recertSchedule.UserId, Current.AgencyId);
        //                                }
        //                                r.Status = recertSchedule.StatusName;
        //                                r.Schedule = string.Empty;
        //                                pastDueRecerts.Add(r);

        //                            }
        //                        }
        //                        else
        //                        {
        //                            r.AssignedTo = "Unassigned";
        //                            r.Status = "Not Scheduled";
        //                            r.Schedule = string.Empty;
        //                            pastDueRecerts.Add(r);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    r.AssignedTo = "Unassigned";
        //                    r.Status = "Not Scheduled";
        //                    r.Schedule = string.Empty;
        //                    pastDueRecerts.Add(r);
        //                }

        //            }
        //            else
        //            {
        //                r.AssignedTo = "Unassigned";
        //                r.Status = "Not Scheduled";
        //                r.Schedule = string.Empty;
        //                pastDueRecerts.Add(r);
        //            }
        //        });
        //    }

        //    return pastDueRecerts;
        //}

        public List<RecertEvent> GetRecertsPastDue(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
           // var recertEpisodes = patientRepository.GetPastDueRecertsLeanByDateRange(Current.AgencyId, branchId, insuranceId, startDate, endDate.AddDays(5));
            var pastDueRecerts = new List<RecertEvent>();
            //if (recertEpisodes != null && recertEpisodes.Count > 0)
            //{
            //    recertEpisodes.ForEach(r =>
            //    {
            //        if (r.Schedule.IsNotNullOrEmpty())
            //        {
            //            r.InsuranceIdFromView = insuranceId;
            //            if (r.PrimaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.PrimaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else if (r.SecondaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.SecondaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else if (r.TertiaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.TertiaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else
            //            {
            //                r.Insurance = "Insurance Not Specified";
            //            }
            //            var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
            //            if (schedule != null && schedule.Count > 0)
            //            {
            //                var dischargeSchedules = schedule.Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) &&
            //                    ((s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeST || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT || 
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome || s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge)
            //                    || (s.EventDate.ToDateTime().IsBetween(r.TargetDate.AddDays(-5), r.TargetDate) && (s.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransfer)))
            //                    && (s.Status == ((int)ScheduleStatus.OasisExported).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || 
            //                    s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
            //                if (dischargeSchedules != null && dischargeSchedules.Count > 0)
            //                {
            //                }
            //                else
            //                {
            //                    var episodeRecertSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionofCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
            //                    if (episodeRecertSchedule != null)
            //                    {
            //                        if ((episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
            //                        {
            //                        }
            //                        else
            //                        {
            //                            if (episodeRecertSchedule.EventDate.ToDateTime().Date < endDate.Date)
            //                            {
            //                                if (!episodeRecertSchedule.UserId.IsEmpty())
            //                                {
            //                                    r.AssignedTo = UserEngine.GetName(episodeRecertSchedule.UserId, Current.AgencyId);
            //                                }
            //                                r.Status = episodeRecertSchedule.StatusName;
            //                                r.Schedule = string.Empty;
            //                                r.DateDifference = endDate.Subtract(episodeRecertSchedule.EventDate.ToDateTime().Date).Days;
            //                                pastDueRecerts.Add(r);
            //                            }
            //                        }
            //                    }
            //                    else
            //                    {
            //                        if (r.TargetDate.Date < endDate.Date)
            //                        {
            //                            r.AssignedTo = "Unassigned";
            //                            r.Status = "Not Scheduled";
            //                            r.Schedule = string.Empty;
            //                            r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                            pastDueRecerts.Add(r);
            //                        }
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                if (r.TargetDate.Date < endDate.Date)
            //                {
            //                    r.AssignedTo = "Unassigned";
            //                    r.Status = "Not Scheduled";
            //                    r.Schedule = string.Empty;
            //                    r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                    pastDueRecerts.Add(r);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            r.InsuranceIdFromView = insuranceId;
            //            if (r.PrimaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.PrimaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else if (r.SecondaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.SecondaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else if (r.TertiaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.TertiaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else
            //            {
            //                r.Insurance = "Insurance Not Specified";
            //            }
            //            if (r.TargetDate.Date < endDate.Date)
            //            {
            //                r.AssignedTo = "Unassigned";
            //                r.Status = "Not Scheduled";
            //                r.Schedule = string.Empty;
            //                r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                pastDueRecerts.Add(r);
            //            }
            //        }
            //    });
            //}
            return pastDueRecerts;
        }

        public List<RecertEvent> GetRecertsPastDueWidget()
        {
            //return patientRepository.GetPastDueRecertsWidgetLean(Current.AgencyId);
            var pastDueRecerts = new List<RecertEvent>();
            //var recertEpisodes = patientRepository.GetPastDueRecertsWidgetLean(Current.AgencyId);
            //if (recertEpisodes != null && recertEpisodes.Count > 0)
            //{
            //    recertEpisodes.ForEach(r =>
            //    {
            //        if (r.Schedule.IsNotNullOrEmpty())
            //        {
            //            var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
            //            if (schedule != null && schedule.Count > 0)
            //            {
            //                var dischargeSchedules = schedule.Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && 
            //                    (s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeST || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath || 
            //                     s.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT ||  s.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge ||
            //                    s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome || 
            //                    s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge) && (s.Status == ((int)ScheduleStatus.OasisExported).ToString() || 
            //                    s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || 
            //                    s.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
            //                if (dischargeSchedules != null && dischargeSchedules.Count > 0)
            //                {
            //                }
            //                else
            //                {
            //                    var episodeRecertSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionofCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
            //                    if (episodeRecertSchedule != null)
            //                    {
            //                        if ((episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || episodeRecertSchedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
            //                        {
            //                        }
            //                        else
            //                        {
            //                            if (episodeRecertSchedule.EventDate.ToDateTime().Date < DateTime.Today.Date)
            //                            {
            //                                if (!episodeRecertSchedule.UserId.IsEmpty())
            //                                {
            //                                    r.AssignedTo = UserEngine.GetName(episodeRecertSchedule.UserId, Current.AgencyId);
            //                                }
            //                                r.Status = episodeRecertSchedule.StatusName;
            //                                r.Schedule = string.Empty;
            //                                r.DateDifference = DateTime.Today.Subtract(episodeRecertSchedule.EventDate.ToDateTime().Date).Days;
            //                                pastDueRecerts.Add(r);
            //                            }
            //                        }
            //                    }
            //                    else
            //                    {
            //                        if (r.TargetDate.Date < DateTime.Today.Date)
            //                        {
            //                            r.AssignedTo = "Unassigned";
            //                            r.Status = "Not Scheduled";
            //                            r.Schedule = string.Empty;
            //                            r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                            pastDueRecerts.Add(r);
            //                        }
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                if (r.TargetDate.Date < DateTime.Today.Date)
            //                {
            //                    r.AssignedTo = "Unassigned";
            //                    r.Status = "Not Scheduled";
            //                    r.Schedule = string.Empty;
            //                    r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                    pastDueRecerts.Add(r);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (r.TargetDate.Date < DateTime.Today.Date)
            //            {
            //                r.AssignedTo = "Unassigned";
            //                r.Status = "Not Scheduled";
            //                r.Schedule = string.Empty;
            //                r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
            //                pastDueRecerts.Add(r);
            //            }
            //        }
            //    });
            //}
            return pastDueRecerts.Take(5).ToList();
        }

        //public List<RecertEvent> GetRecertsUpcoming()
        //{
        //    var recetEpisodes = patientRepository.GetUpcomingRecertsLean(Current.AgencyId);
        //    var upcomingRecets = new List<RecertEvent>();
        //    if (recetEpisodes != null && recetEpisodes.Count > 0)
        //    {
        //        recetEpisodes.ForEach(r =>
        //        {
        //            if (r.Schedule.IsNotNullOrEmpty())
        //            {
        //                var schedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-6).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.AddDays(-1).Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISBRecertification)).OrderByDescending(s => s.EventDate.ToDateTime().Date).FirstOrDefault();
        //                if (schedule != null)
        //                {
        //                    if ((schedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
        //                    {
        //                    }
        //                    else
        //                    {
        //                        if (!schedule.UserId.IsEmpty())
        //                        {
        //                            r.AssignedTo = UserEngine.GetName(schedule.UserId, Current.AgencyId);
        //                        }
        //                        r.Status = schedule.StatusName;
        //                        r.Schedule = string.Empty;
        //                        upcomingRecets.Add(r);
        //                    }
        //                }
        //                else
        //                {
        //                    r.AssignedTo = "Unassigned";
        //                    r.Status = "Not Scheduled";
        //                    r.Schedule = string.Empty;
        //                    upcomingRecets.Add(r);
        //                }
        //            }
        //            else
        //            {
        //                r.AssignedTo = "Unassigned";
        //                r.Status = "Not Scheduled";
        //                r.Schedule = string.Empty;
        //                upcomingRecets.Add(r);
        //            }
        //        });
        //    }
        //    return upcomingRecets;
        //}

        public List<RecertEvent> GetRecertsUpcoming(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var upcomingRecerts = new List<RecertEvent>();
            var schedules = scheduleRepository.GetUpcomingRecertsLean(Current.AgencyId, branchId, insuranceId, startDate, endDate, 0);
            if (schedules.IsNotNullOrEmpty())
            {
                var userIds = schedules.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = UserEngine.GetUsers(Current.AgencyId, userIds);
                var insuranceIds = schedules.Select(s => s.PrimaryInsurance).Distinct().ToList();
                var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds);
                var episodeToScheduleDataSet = schedules.GroupBy(s => s.EpisodeId).ToDictionary(g => g.First(), g => g.ToList()).OrderByDescending(g => g.Key.StartDate);
                if (episodeToScheduleDataSet.IsNotNullOrEmpty())
                {
                    episodeToScheduleDataSet.ForEach(epiosde =>
                    {
                        var insuranceName = "Insurance Not Specified";
                        var insurance = insurances.FirstOrDefault(s => epiosde.Key.PrimaryInsurance == s.Id);
                        if (insurance != null)
                        {
                            insuranceName = insurance.Name;
                        }
                        if (epiosde.Value.IsNotNullOrEmpty())
                        {
                            var schedule = epiosde.Value.OrderByDescending(s => s.EventDate).FirstOrDefault();
                            if (!schedule.Id.IsEmpty())
                            {
                                if (ScheduleStatusFactory.OASISCompleted(true).Contains(schedule.Status))// (schedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
                                {
                                }
                                else
                                {
                                    if (schedule.EventDate.Date >= startDate.Date && schedule.EventDate.Date <= endDate.Date)
                                    {
                                        var userName = string.Empty;
                                        var user = users.FirstOrDefault(s => schedule.UserId == s.Id);
                                        userName = user != null ? user.DisplayName : string.Empty;
                                        upcomingRecerts.Add(new RecertEvent
                                        {
                                            TargetDate = epiosde.Key.EndDate,
                                            StartDate = epiosde.Key.StartDate,
                                            ScheduledDate = schedule.EventDate,
                                            PatientIdNumber = epiosde.Key.PatientIdNumber,
                                            PatientName = epiosde.Key.PatientName,
                                            AssignedTo = userName,
                                            Status = schedule.StatusName,
                                            Insurance = insuranceName
                                        });
                                    }
                                }
                            }
                            else
                            {
                                upcomingRecerts.Add(new RecertEvent
                                    {
                                        // PrimaryInsurance = reader.GetStringNullable("PrimaryInsurance"),
                                        // SecondaryInsurance = reader.GetStringNullable("SecondaryInsurance"),
                                        // TertiaryInsurance = reader.GetStringNullable("TertiaryInsurance"),
                                        TargetDate = epiosde.Key.EndDate,
                                        StartDate = epiosde.Key.StartDate,
                                        PatientIdNumber = epiosde.Key.PatientIdNumber,
                                        PatientName = epiosde.Key.PatientName,
                                        AssignedTo = "Unassigned",
                                        Status = "Not Scheduled",
                                        Insurance = insuranceName
                                    });
                            }
                        }
                        else
                        {
                            upcomingRecerts.Add(new RecertEvent
                            {
                                // PrimaryInsurance = reader.GetStringNullable("PrimaryInsurance"),
                                // SecondaryInsurance = reader.GetStringNullable("SecondaryInsurance"),
                                // TertiaryInsurance = reader.GetStringNullable("TertiaryInsurance"),
                                TargetDate = epiosde.Key.EndDate,
                                StartDate = epiosde.Key.StartDate,
                                PatientIdNumber = epiosde.Key.PatientIdNumber,
                                PatientName = epiosde.Key.PatientName,
                                AssignedTo = "Unassigned",
                                Status = "Not Scheduled",
                                Insurance = insuranceName
                            });
                        }
                    });
                }
            }

            //var recertEpisodes = patientRepository.GetUpcomingRecertsLean(Current.AgencyId, branchId, insuranceId, startDate, endDate);

            //if (recertEpisodes != null && recertEpisodes.Count > 0)
            //{
            //    recertEpisodes.ForEach(r =>
            //    {
            //        //var lastFiveDaysSt);art=r.TargetDate.AddDays(-6);
            //        //if ((r.TargetDate.AddDays(-1).Date <= endDate && r.TargetDate.AddDays(-1).Date >= startDate) || (lastFiveDaysStart.Date >= startDate.Date && lastFiveDaysStart.Date <= endDate.Date))
            //        //{
            //        if (r.Schedule.IsNotNullOrEmpty())
            //        {
            //            r.InsuranceIdFromView = insuranceId;
            //            if (r.PrimaryInsurance.IsNotNullOrEmpty()) 
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.PrimaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else if (r.SecondaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.SecondaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else if (r.TertiaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.TertiaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else
            //            {
            //                r.Insurance = "Insurance Not Specified";
            //            }

            //            var schedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.AddDays(-1).Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-6).Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISBRecertification)).OrderByDescending(s => s.EventDate.ToDateTime().Date).FirstOrDefault();
            //            if (schedule != null)
            //            {
            //                if ((schedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString()))
            //                {
            //                }
            //                else
            //                {
            //                    if (schedule.EventDate.ToDateTime().Date >= startDate.Date && schedule.EventDate.ToDateTime().Date <= endDate.Date)
            //                    {
            //                        if (!schedule.UserId.IsEmpty())
            //                        {
            //                            r.AssignedTo = UserEngine.GetName(schedule.UserId, Current.AgencyId);
            //                        }

            //                        r.Status = schedule.StatusName;
            //                        r.Schedule = string.Empty;
            //                        r.ScheduledDate = schedule.EventDate.ToDateTime();
            //                        r.TargetDate = r.TargetDate.AddDays(-1);//added to fix episode enddate on the report
            //                        upcomingRecerts.Add(r);
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                if (r.TargetDate.AddDays(-1).Date <= endDate.Date)
            //                {
            //                    r.AssignedTo = "Unassigned";
            //                    r.Status = "Not Scheduled";
            //                    r.Schedule = string.Empty;
            //                    r.TargetDate = r.TargetDate.AddDays(-1);//added to fix episode enddate on the report
            //                    upcomingRecerts.Add(r);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            r.InsuranceIdFromView = insuranceId;
            //            if (r.PrimaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.PrimaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else if (r.SecondaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.SecondaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else if (r.TertiaryInsurance.IsNotNullOrEmpty())
            //            {
            //                r.Insurance = InsuranceEngine.Get(r.TertiaryInsurance.ToInteger(), Current.AgencyId).Name;
            //            }
            //            else
            //            {
            //                r.Insurance = "Insurance Not Specified";
            //            }
            //            if (r.TargetDate.AddDays(-1).Date <= endDate.Date)
            //            {
            //                r.AssignedTo = "Unassigned";
            //                r.Status = "Not Scheduled";
            //                r.Schedule = string.Empty;
            //                r.TargetDate = r.TargetDate.AddDays(-1);//added to fix episode enddate on the report
            //                upcomingRecerts.Add(r);
            //            }
            //        }
            //        //}
            //    });
            //}
            return upcomingRecerts;
        }

        public List<RecertEvent> GetRecertsUpcomingWidget()
        {
            return null;// patientRepository.GetUpcomingRecertsWidgetLean(Current.AgencyId);
        }

        public List<InsuranceViewData> GetInsurances()
        {
            var insuranceList = new List<InsuranceViewData>();
            lookupRepository.Insurances().ForEach(i =>
            {
                insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            });
            agencyRepository.GetInsurances(Current.AgencyId).ForEach(i =>
            {
                insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            });
            return insuranceList;
        }
        
        public List<AddressViewData> GetAgencyFullAddress()
        {
            var listOfAddress = agencyRepository.GetBranches(Current.AgencyId);
            var address = new List<AddressViewData>();
            if (listOfAddress != null && listOfAddress.Count > 0)
            {
                listOfAddress.ForEach(l =>
                {
                    address.Add(new AddressViewData { Name = l.Name, FullAddress = string.Format("{0} {1}, {2}, {3} {4}", l.AddressLine1, l.AddressLine2, l.AddressCity, l.AddressStateCode, l.AddressZipCode) });
                });
            }
            return address;
        }

        public List<Infection> GetInfections(Guid agencyId)
        {
            var infections = agencyRepository.GetInfections(Current.AgencyId).ToList();
            if (infections != null && infections.Count > 0)
            {
                infections.ForEach(i =>
                {
                    var patient = patientRepository.GetPatientOnly(i.PatientId, i.AgencyId);
                    if (patient != null)
                    {
                        i.PatientName = patient.DisplayName;
                    }
                    var physician = physicianRepository.Get(i.PhysicianId, i.AgencyId);
                    if (physician != null)
                    {
                        i.PhysicianName = physician.DisplayName;
                    }
                    var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId,  i.PatientId, i.Id);
                    if (scheduleEvent != null)
                    {
                        i.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return infections;
        }

        public List<Incident> GetIncidents(Guid agencyId)
        {
            var incidents = agencyRepository.GetIncidents(Current.AgencyId).ToList();
            if (incidents != null && incidents.Count > 0)
            {
                incidents.ForEach(i =>
                {
                    var patient = patientRepository.GetPatientOnly(i.PatientId, i.AgencyId);
                    if (patient != null)
                    {
                        i.PatientName = patient.DisplayName;
                    }
                    var physician = physicianRepository.Get(i.PhysicianId, i.AgencyId);
                    if (physician != null)
                    {
                        i.PhysicianName = physician.DisplayName;
                    }

                    var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId,  i.PatientId, i.Id);
                    if (scheduleEvent != null)
                    {
                        i.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return incidents;
        }

        public bool ProcessInfections(string button, Guid patientId, Guid eventId)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
                if (infection != null)
                {
                    var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        var oldInPrintQueue = scheduleEvent.InPrintQueue;
                        var description = string.Empty;
                        var updateInfection = false;
                        if (button == "Approve")
                        {
                            infection.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                            infection.Modified = DateTime.Now;
                           
                            scheduleEvent.InPrintQueue = true;
                            scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                            
                            shouldUpdateEpisode = true;
                            description = "Approved By:" + Current.UserFullName;
                            updateInfection = true;
                        }
                        else if (button == "Return")
                        {
                            infection.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                            infection.Modified = DateTime.Now;
                            infection.SignatureText = string.Empty;
                            infection.SignatureDate = DateTime.MinValue;
                            
                            scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                           
                            shouldUpdateEpisode = true;
                            description = "Returned By:" + Current.UserFullName;
                            updateInfection = true;
                        }
                        else if (button == "Print")
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                        if (shouldUpdateEpisode)
                        {
                            if (scheduleRepository.UpdateScheduleTaskModal(scheduleEvent))
                            {
                                if (!updateInfection || (updateInfection && agencyRepository.UpdateInfectionModal(infection)))
                                {
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    result = true;
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleEvent.InPrintQueue = oldInPrintQueue;
                                    scheduleRepository.UpdateScheduleTaskModal(scheduleEvent);
                                }
                            }
                            //if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            //{
                            //    if (userEvent != null)
                            //    {
                            //        if (userRepository.UpdateEvent(Current.AgencyId, userEvent)) result = true;
                            //    }
                            //    else
                            //    {
                            //        userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                            //        result = true;
                            //    }
                            //    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                            //}
                        }
                    }
                }
            }
            return result;
        }

        public bool ProcessIncidents(string button, Guid patientId, Guid eventId)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
                if (incident != null)
                {
                    var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// patientRepository.GetSchedule(Current.AgencyId, incident.EpisodeId, patientId, eventId);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        var oldInPrintQueue = scheduleEvent.InPrintQueue;
                       
                        var description = string.Empty;
                        var updateIncident = false;
                        if (button == "Approve")
                        {
                            incident.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                            incident.Modified = DateTime.Now;
                           
                            scheduleEvent.InPrintQueue = true;
                            scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                            
                            shouldUpdateEpisode = true;
                            description = "Approved by " + Current.UserFullName;
                            updateIncident = true;
                        }
                        else if (button == "Return")
                        {
                            incident.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                            incident.Modified = DateTime.Now;
                            incident.SignatureText = string.Empty;
                            incident.SignatureDate = DateTime.MinValue;
                           
                            scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                            shouldUpdateEpisode = true;
                            description = "Returned by " + Current.UserFullName;
                            updateIncident = true;
                        }
                        else if (button == "Print")
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                        if (shouldUpdateEpisode)
                        {
                            if (scheduleRepository.UpdateScheduleTaskModal(scheduleEvent))
                            {
                                if (!updateIncident || (updateIncident && agencyRepository.UpdateIncidentModal(incident)))
                                {
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    result = true;
                                }
                                else
                                {
                                    scheduleEvent.Status = oldStatus;
                                    scheduleEvent.InPrintQueue = oldInPrintQueue;
                                    scheduleRepository.UpdateScheduleTaskModal(scheduleEvent);
                                }
                            }
                            //if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                            //{
                            //    if (userEvent != null)
                            //    {
                            //        if (userRepository.UpdateEvent(Current.AgencyId, userEvent)) result = true;
                            //    }
                            //    else
                            //    {
                            //        userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                            //        result = true;
                            //    }
                            //    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                            //}
                        }
                    }
                }
            }
            return result;
        }

        public List<SelectListItem> Insurances(string value, bool IsAll, bool IsMedicareTradIncluded)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (IsMedicareTradIncluded)
            {
                //var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                //if (agency != null)
                //{
                    var payorId = 0;
                    if (int.TryParse(Current.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem
                            {
                                Text = standardInsurance.Name,
                                Value = standardInsurance.Id.ToString(),
                                Selected = (standardInsurance.Id.ToString().IsEqual(value))
                            });
                        }
                    }
                //}
            }
            if (IsAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "All",
                    Value = "0"
                });
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            return items;
        }

        public List<SelectListItem> Branchs(string value, bool IsAll)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var branches = LocationEngine.GetLocations(Current.AgencyId, Current.LocationIds);
            if (branches.IsNotNullOrEmpty())
            {
                var tempItems = from branch in branches
                            select new SelectListItem
                            {
                                Text = branch.Name,
                                Value = branch.Id.ToString(),
                                Selected = (branch.Id.ToString().IsEqual(value))
                            };

                items = tempItems.ToList();
                
            }
            if (IsAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "-- All Branches --",
                    Value = Guid.Empty.ToString(),
                });
            }
            return items;
        }

        //public Infection GetInfectionReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        //{
        //    return GetInfectionReportPrint(episodeId, patientId, eventId, Current.AgencyId);
        //}

        public PrintViewData<Infection> GetInfectionReportPrint(Guid agencyId, Guid patientId, Guid eventId)
        {
            var data = new PrintViewData<Infection>();
            var infection = agencyRepository.GetInfectionReport(agencyId, eventId);
            if (infection != null)
            {

                var patient = patientRepository.GetPatientPrintProfile(infection.AgencyId, infection.PatientId);
                 if (patient != null)
                 {
                     data.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, patient.AgencyLocationId);
                     data.PatientProfile = patient;
                     infection.PatientName = patient.LastName + ", " + patient.FirstName;
                 }
                
                if (!infection.EpisodeId.IsEmpty())
                {
                    var episode = episodeRepository.GetEpisodeLean(agencyId, infection.PatientId, infection.EpisodeId);
                    if (episode != null)
                    {
                        infection.EpisodeEndDate = episode.EndDateFormatted;
                        infection.EpisodeStartDate = episode.StartDateFormatted;
                        Assessment assessment = assessmentRepository.GetEpisodeAssessment(Current.AgencyId, patientId, episode.Id, episode.StartDate, true);
                        infection.Diagnosis = new Dictionary<string, string>();
                        if (assessment != null)
                        {
                            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                            var oasisQuestions = assessment.ToDiagnosisQuestionDictionary();
                            infection.Diagnosis.Add("PrimaryDiagnosis", oasisQuestions.ContainsKey("PrimaryDiagnosis") ? oasisQuestions["PrimaryDiagnosis"].Answer : "");
                            infection.Diagnosis.Add("ICD9M", oasisQuestions.ContainsKey("ICD9M") ? oasisQuestions["ICD9M"].Answer : "");
                            infection.Diagnosis.Add("SecondaryDiagnosis", oasisQuestions.ContainsKey("PrimaryDiagnosis1") ? oasisQuestions["PrimaryDiagnosis1"].Answer : "");
                            infection.Diagnosis.Add("ICD9M2", oasisQuestions.ContainsKey("ICD9M1") ? oasisQuestions["ICD9M1"].Answer : "");
                        }
                    }
                }
                if (!infection.PhysicianId.IsEmpty())
                {
                    infection.PhysicianName = PhysicianEngine.GetName(infection.PhysicianId, agencyId);
                }
                data.Data = infection;
            }
            return data;
        }
       
        //public Incident GetIncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        //{
        //    //return GetIncidentReportPrint(episodeId, patientId, eventId, Current.AgencyId);
        //}
       
        //public Incident GetIncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId, Guid agencyId)
        //{
        //    var incident = agencyRepository.GetIncidentReport(agencyId, eventId);
        //    if (incident != null)
        //    {
        //        incident.Agency = agencyRepository.GetWithBranches(agencyId);
        //        incident.Patient = patientRepository.GetPatientOnly(incident.PatientId, incident.AgencyId);

        //        incident.PatientName = incident.Patient.LastName + ", " + incident.Patient.FirstName;
        //        if (!incident.EpisodeId.IsEmpty())
        //        {
        //            var episode = episodeRepository.GetEpisodeLean(agencyId, incident.PatientId,incident.EpisodeId);
        //            if (episode != null)
        //            {
        //                incident.EpisodeEndDate = episode.EndDateFormatted;
        //                incident.EpisodeStartDate = episode.StartDateFormatted;
        //                //Assessment assessment = assessmentRepository.GetEpisodeAssessment(Current.AgencyId, patientId, episode.Id, episode.StartDate, true);
        //                //incident.Diagnosis = new Dictionary<string, string>();
        //                //if (assessment != null)
        //                //{
        //                //    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
        //                //    var oasisQuestions = assessment.ToDiagnosisQuestionDictionary();
        //                //    incident.Diagnosis.Add("PrimaryDiagnosis", oasisQuestions.ContainsKey("PrimaryDiagnosis") ? oasisQuestions["PrimaryDiagnosis"].Answer : "");
        //                //    incident.Diagnosis.Add("ICD9M", oasisQuestions.ContainsKey("ICD9M") ? oasisQuestions["ICD9M"].Answer : "");
        //                //    incident.Diagnosis.Add("SecondaryDiagnosis", oasisQuestions.ContainsKey("PrimaryDiagnosis1") ? oasisQuestions["PrimaryDiagnosis1"].Answer : "");
        //                //    incident.Diagnosis.Add("ICD9M2", oasisQuestions.ContainsKey("ICD9M1") ? oasisQuestions["ICD9M1"].Answer : "");
        //                //}
        //            }
        //        }
        //        if (!incident.PhysicianId.IsEmpty())
        //        {
        //            var physician = physicianRepository.Get(incident.PhysicianId, agencyId);
        //            if (physician != null) incident.PhysicianName = physician.DisplayName;
        //        }
        //    }
        //    return incident;
        //}


        public PrintViewData<Incident> GetIncidentReportPrint(Guid agencyId,Guid patientId, Guid eventId)
        {
            var data = new PrintViewData<Incident>();
            var incident = agencyRepository.GetIncidentReport(agencyId, eventId);
            if (incident != null)
            {
                var patient = patientRepository.GetPatientPrintProfile(incident.AgencyId, incident.PatientId);
                if (patient != null)
                {
                    data.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, patient.AgencyLocationId);
                    data.PatientProfile = patient;
                    incident.PatientName = patient.LastName + ", " + patient.FirstName;
                }
                var dateRange = episodeRepository.GetEpisodeLean(agencyId, incident.PatientId, incident.EpisodeId);
                if (dateRange != null)
                {
                    incident.EpisodeStartDate = dateRange.StartDateFormatted;
                    incident.EpisodeEndDate = dateRange.EndDateFormatted;
                }
                if (!incident.PhysicianId.IsEmpty())
                {
                    incident.PhysicianName = PhysicianEngine.GetName(incident.PhysicianId,agencyId);
                }
                data.Data = incident;
            }
            return data;
        }

        //public List<PatientEpisodeEvent> GetPrintQueue()
        //{
        //    var events = new List<PatientEpisodeEvent>();
        //    var agencyPatientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId);
        //    agencyPatientEpisodes.ForEach(e =>
        //    {
        //        if (e.Schedule.IsNotNullOrEmpty())
        //        {
        //            var scheduledEvents = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.IsDeprecated == false && s.IsMissedVisit == false && s.InPrintQueue == true);
        //            if (scheduledEvents != null)
        //            {
        //                scheduledEvents.ForEach(s =>
        //                {
        //                    events.Add(new PatientEpisodeEvent
        //                    {
        //                        Status = s.StatusName,
        //                        PatientName = e.PatientName,
        //                        TaskName = s.DisciplineTaskName,
        //                        PrintUrl = Url.Download(s, false),
        //                        UserName = UserEngine.GetName(s.UserId, Current.AgencyId),
        //                        EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
        //                        CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
        //                    });
        //                });
        //            }
        //        }
        //        e.Schedule = string.Empty;
        //    });
        //    return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        //}

        public List<PatientEpisodeEvent> GetPrintQueue(Guid BranchId, DateTime StartDate, DateTime EndDate, bool isPrintUrlNeeded)
        {
            var events = new List<PatientEpisodeEvent>();
            var scheduleEvents = scheduleRepository.GetPrintQueueTasks(Current.AgencyId, BranchId, StartDate, EndDate);
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                var users = new List<UserCache>();
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (userIds != null && userIds.Count > 0)
                {
                    users = UserEngine.GetUsers(Current.AgencyId, userIds) ?? new List<UserCache>();
                }
                scheduleEvents.ForEach(s =>
                {
                    var user = users.FirstOrDefault(u => u.Id == s.UserId) ?? new UserCache();
                    events.Add(new PatientEpisodeEvent
                    {
                        Status = s.StatusName,
                        PatientName = s.PatientName,
                        TaskName = s.DisciplineTaskName,
                        PrintUrl = isPrintUrlNeeded ? Url.Download(s, true) : string.Empty,
                        UserName = user != null ? user.DisplayName : string.Empty,
                        EventDate = s.EventDate.IsValid() ? s.EventDate.ToZeroFilled() : "",
                        CustomValue = isPrintUrlNeeded ? string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.Id, s.DisciplineTask) : string.Empty
                    });
                });
            }

            //List<PatientEpisodeData> agencyPatientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, BranchId, StartDate, EndDate);
            
            //agencyPatientEpisodes.ForEach(e =>
            //{
            //    if (e.Schedule.IsNotNullOrEmpty())
            //    {
            //        var scheduledEvents = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime()>=StartDate && s.EventDate.ToDateTime()<=EndDate && s.IsDeprecated == false && s.InPrintQueue == true);
            //        if (scheduledEvents != null)
            //        {
            //            scheduledEvents.ForEach(s =>
            //            {
            //                events.Add(new PatientEpisodeEvent
            //                {
            //                    Status = s.StatusName,
            //                    PatientName = e.PatientName,
            //                    TaskName = s.DisciplineTaskName,
            //                    PrintUrl = Url.Download(s, true),
            //                    UserName = UserEngine.GetName(s.UserId, Current.AgencyId),
            //                    EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
            //                    CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
            //                });
            //            });
            //        }
            //    }
            //    e.Schedule = string.Empty;
            //});
            return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }

        #region NonVisit Tasks
        public IList<UserNonVisitTask> GetUserNonVisitTasks(Guid agencyId) 
        {
            var result = agencyRepository.GetUserNonVisitTasks(agencyId).ToList();
            if (result != null) 
            {
                //Get Task Details
                var agencyNonVisitTaskList = agencyRepository.GetNonVisitTasks(agencyId);
                if (agencyNonVisitTaskList != null)
                {
                    if (agencyNonVisitTaskList.Count > 0)
                    {
                        Dictionary<Guid, string> nonVisitTaskTitleList = new Dictionary<Guid, string>();
                        Dictionary<Guid, string> nonVisitTaskTextList = new Dictionary<Guid, string>();

                        foreach (var agencyNonVisitTask in agencyNonVisitTaskList) 
                        {
                            if (agencyNonVisitTask != null) 
                            {
                                nonVisitTaskTitleList.Add(agencyNonVisitTask.Id, agencyNonVisitTask.Title);
                                nonVisitTaskTextList.Add(agencyNonVisitTask.Id, agencyNonVisitTask.Text);
                            }
                        }

                        foreach (var userNonVisitTask in result)
                        {
                            if (userNonVisitTask.TaskId != null && userNonVisitTask.TaskId.IsNotEmpty())
                            {
                                if (nonVisitTaskTitleList != null) 
                                {
                                    if(nonVisitTaskTitleList.ContainsKey(userNonVisitTask.TaskId))
                                    {
                                        userNonVisitTask.TaskTitle = nonVisitTaskTitleList[userNonVisitTask.TaskId];
                                    }
                                }
                                if (nonVisitTaskTextList != null) 
                                {
                                    if (nonVisitTaskTextList.ContainsKey(userNonVisitTask.TaskId)) 
                                    {
                                        userNonVisitTask.TaskText = nonVisitTaskTextList[userNonVisitTask.TaskId];
                                    }
                                }   
                            }
                            if (userNonVisitTask.Comments != null && userNonVisitTask.Comments.IsNotNullOrEmpty() && userNonVisitTask.Comments.Length > 35)
                            {
                                userNonVisitTask.Comments = userNonVisitTask.Comments.Substring(0, 34) + "...";
                            }
                        }
                    }
                }

                //Get User Details
                var agencyUserList = userRepository.GetAgencyUsers(agencyId);
                if (agencyUserList != null) 
                {
                    Dictionary<Guid, string> agencyUserNameList = new Dictionary<Guid, string>();
                    if (agencyUserList.Count > 0) 
                    {
                        foreach (var agencyUser in agencyUserList) 
                        {
                            if (agencyUser != null) 
                            {
                                string userName = default(string);
                                if (agencyUser.MiddleName.IsNotNullOrEmpty())
                                {
                                    userName = string.Format("{0}, {1} {2}.", agencyUser.LastName, agencyUser.FirstName, agencyUser.MiddleName);
                                }
                                else 
                                {
                                    userName = string.Format("{0}, {1}", agencyUser.LastName, agencyUser.FirstName);
                                }
                                agencyUserNameList.Add(agencyUser.Id, userName);
                            }
                        }

                        foreach (var userNonVisitTask in result) 
                        {
                            if (userNonVisitTask.UserId != null && userNonVisitTask.UserId.IsNotEmpty()) 
                            {
                                if(agencyUserNameList.ContainsKey(userNonVisitTask.UserId))
                                {
                                    userNonVisitTask.UserDisplayName = agencyUserNameList[userNonVisitTask.UserId];
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }
        #endregion

        #region AgencyTeams

        public List<PatientSelection> GetTeamAccessPatients(Guid agencyId, Guid currentUserId)
        {
            var patients = agencyRepository.GetTeamAccess(agencyId, currentUserId);
            var patientResults = new List<PatientSelection>();
            foreach (var patient in patients)
            {
                var p = patientRepository.Get(patient.PatientId, agencyId);
                if (p != null)
                {
                    var temp = patientResults.SingleOrDefault(pa => pa.Id == p.Id);
                    if (temp == null)
                    {
                        patientResults.Add(new PatientSelection
                        {
                            Id = p.Id,
                            PatientIdNumber = p.PatientIdNumber,
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            MI = p.MiddleInitial
                        });
                    }
                }
            }

            return patientResults;
        }
        
        #endregion

        #region Agency Location Upgrades

        public AgencyUpgradeResult Upgrade(AgencyUpgrade agencyUpgrade)
        {
            var result = new AgencyUpgradeResult { IsSuccessful = false, ErrorMessage = string.Empty };
            var newPackage = string.Empty;
            var previousPackage = string.Empty;

            if (agencyUpgrade != null)
            {
                agencyUpgrade.Id = Guid.NewGuid();
                agencyUpgrade.Created = DateTime.Now;
                agencyUpgrade.Amount = -1;
                agencyUpgrade.PreviousAmount = -1;
                agencyUpgrade.EffectiveDate = DateTime.Now;
                agencyUpgrade.AgencyId = Current.AgencyId;
                agencyUpgrade.RequestedById = Current.UserId;

                var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                var plan = agencyRepository.GetSubscriptionPlan(Current.AgencyId, agencyUpgrade.AgencyLocationId);
                if (agency != null && plan != null)
                {
                    if (agencyUpgrade.RequestedPackageId == plan.PlanLimit && agencyUpgrade.AnnualPlanId == 0)
                    {
                        result.IsSuccessful = false;
                        result.ErrorMessage = "Please select a monthly or annual subscription plan";
                    }
                    else 
                    {
                        if (agencyUpgrade.RequestedPackageId > 0 && agencyUpgrade.RequestedPackageId != plan.PlanLimit)
                        {
                            previousPackage = plan.IsUserPlan ? plan.PlanLimit.UserSubscriptionPlanName() : plan.PlanLimit.PatientSubscriptionPlanName();
                            agencyUpgrade.PreviousPackageId = plan.PlanLimit;
                            plan.PlanLimit = agencyUpgrade.RequestedPackageId;
                            agencyUpgrade.IsUserPlan = plan.IsUserPlan;
                            newPackage = plan.IsUserPlan ? plan.PlanLimit.UserSubscriptionPlanName() : plan.PlanLimit.PatientSubscriptionPlanName();
                        }

                        if (agencyUpgrade.AnnualPlanId > 0)
                        {
                            agency.AnnualPlanId = agencyUpgrade.AnnualPlanId;
                            agency.Modified = DateTime.Now;
                        }

                        if (agency.AccountId.IsNotNullOrEmpty())
                        {
                          agencyUpgrade.AccountId = agency.AccountId;
                        }

                        if (agencyRepository.Update(agency) && agencyRepository.AddUpgrade(agencyUpgrade) && agencyRepository.UpdateSubscriptionPlan(plan))
                        {
                            var fullname = Current.UserFullName;
                            var emailAddress = Current.User.Name;
                            ThreadPool.QueueUserWorkItem(state => SendAccountUpgradeNotifications(agency, fullname, emailAddress, previousPackage, newPackage, agencyUpgrade));
                            result.IsSuccessful = true;
                            result.ErrorMessage = string.Empty;

                            if (agencyUpgrade.AnnualPlanId > 0)
                            {
                                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, agencyUpgrade.Id.ToString(), LogType.AgencyUpgrade, LogAction.AgencyUpgraded, string.Format("Annual Subscription Plan: {0}", agencyUpgrade.AnnualPlanDescription));
                            }
                            else
                            {
                                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, agencyUpgrade.Id.ToString(), LogType.AgencyUpgrade, LogAction.AgencyUpgraded, string.Format("Previous Subscription Plan: {0}", previousPackage));
                            }
                        }
                    }
                }
            }
            return result;
        }

        private static void SendAccountUpgradeNotifications(Agency agency, string fullName, string emailAddress, string previousPackage, string newPackage, AgencyUpgrade agencyUpgrade)
        {
            string bodyText = string.Empty;
            string subject = string.Format("{0} - Subscription Plan Change", agency.Name);

            string adminBodyText = string.Empty;
            string adminSubject = string.Format("{0} - Subscription Plan Change", agency.Name);

            if (agencyUpgrade.AnnualPlanId > 0)
            {
                bodyText = MessageBuilder.PrepareTextFrom("AccountAnnualUpgradeNotification", "agencyname", agency.Name, "authorizeduser", fullName, "authorizedemail", emailAddress, "previouspackage", previousPackage, "requestedpackage", newPackage, "comments", agencyUpgrade.Comments, "annualplan", agencyUpgrade.AnnualPlanDescription);
                adminBodyText = MessageBuilder.PrepareTextFrom("AgencyAnnualUpgradeNotification", "primarycontact", agency.ContactPersonFirstName, "authorizeduser", fullName, "previouspackage", previousPackage, "requestedpackage", newPackage, "annualplan", agencyUpgrade.AnnualPlanDescription);
            }
            else
            {
                bodyText = MessageBuilder.PrepareTextFrom("AccountUpgradeNotification", "agencyname", agency.Name, "authorizeduser", fullName, "authorizedemail", emailAddress, "previouspackage", previousPackage, "requestedpackage", newPackage, "comments", agencyUpgrade.Comments);
                adminBodyText = MessageBuilder.PrepareTextFrom("AgencyUpgradeNotification", "primarycontact", agency.ContactPersonFirstName, "authorizeduser", fullName, "previouspackage", previousPackage, "requestedpackage", newPackage);

            }
            Notify.User(CoreSettings.NoReplyEmail, AppSettings.AccountEmailAddress, subject, bodyText);
            Notify.User(CoreSettings.NoReplyEmail, agency.ContactPersonEmail, adminSubject, adminBodyText);
        }

        public void Update(Agency existingAgency, string previousAgencyName)
        {
            var fullname = Current.UserFullName;
            var emailAddress = Current.User.Name;
            var primaryContact = existingAgency.ContactPersonFirstName + " " + existingAgency.ContactPersonLastName;
            if (existingAgency.Name != previousAgencyName) 
            {
                var preAgencyName = previousAgencyName;
                var curAgencyName = existingAgency.Name;
                ThreadPool.QueueUserWorkItem(state => SendAgencyInfoUpdateNotifications(existingAgency, fullname, emailAddress, primaryContact, preAgencyName, curAgencyName));
            }
            else
            {
            ThreadPool.QueueUserWorkItem(state => SendAgencyInfoUpdateNotifications(existingAgency, fullname, emailAddress, primaryContact, null, null));
            }  
        }

        private static void SendAgencyInfoUpdateNotifications(Agency existingAgency, string fullName, string emailAddress, string primaryContact, string preAgencyName, string curAgencyName)
        {
            string bodyText = string.Empty;
            string subject = string.Format("{0} - Company Information Change", existingAgency.Name);

            string adminBodyText = string.Empty;
            string adminSubject = string.Format("{0} - Company Information Change CONTACT PERSON", existingAgency.Name);

            if (preAgencyName != null && curAgencyName != null)
            {
                bodyText = MessageBuilder.PrepareTextFrom("AccountNameCompanyInformationNotification", "agencyname", existingAgency.Name, "authorizeduser", fullName, "authorizedemail", emailAddress, "taxid", existingAgency.TaxId, "contactemail", existingAgency.ContactPersonEmail, "contactphone", existingAgency.ContactPersonPhoneFormatted, "primarycontact", primaryContact, "preagencyname", preAgencyName, "curagencyname", curAgencyName);
                adminBodyText = MessageBuilder.PrepareTextFrom("AgencyNameCompanyInformationNotification", "agencyname", existingAgency.Name, "authorizeduser", fullName, "authorizedemail", emailAddress, "taxid", existingAgency.TaxId, "contactemail", existingAgency.ContactPersonEmail, "contactphone", existingAgency.ContactPersonPhoneFormatted, "primarycontact", primaryContact, "preagencyname", preAgencyName, "curagencyname", curAgencyName);
            }
            else
            {
                bodyText = MessageBuilder.PrepareTextFrom("AccountCompanyInformationNotification", "agencyname", existingAgency.Name, "authorizeduser", fullName, "authorizedemail", emailAddress, "taxid", existingAgency.TaxId, "contactemail", existingAgency.ContactPersonEmail, "contactphone", existingAgency.ContactPersonPhoneFormatted, "primarycontact", primaryContact);
                adminBodyText = MessageBuilder.PrepareTextFrom("AgencyCompanyInformationNotification", "agencyname", existingAgency.Name, "authorizeduser", fullName, "authorizedemail", emailAddress, "taxid", existingAgency.TaxId, "contactemail", existingAgency.ContactPersonEmail, "contactphone", existingAgency.ContactPersonPhoneFormatted, "primarycontact", primaryContact);
            }
            
            //Notify.User(CoreSettings.NoReplyEmail, AppSettings.AccountEmailAddress, subject, bodyText);
            Notify.User(CoreSettings.NoReplyEmail, existingAgency.ContactPersonEmail, adminSubject, adminBodyText);
        }

        public LocationPlanViewData GetAgencySubcriptionPlanDetails(Guid branchId)
        {
            var viewData = new LocationPlanViewData();
            var plan = agencyRepository.GetSubscriptionPlan(Current.AgencyId, branchId);
            if (plan != null)
            {
                if (plan.IsUserPlan)
                {
                    viewData = new LocationPlanViewData
                    {
                        Max = plan.PlanLimit,
                        Id = plan.AgencyLocationId,
                        IsUserPlan = plan.IsUserPlan,
                        CurrentPlan = plan.PlanLimit,
                        NextPlan = plan.PlanLimit.NextUserSubscriptionPlan(),
                        CurrentPlanDescription = plan.PlanLimit.UserSubscriptionPlanName(),
                        NextPlanDescription = plan.PlanLimit.NextUserSubscriptionPlan().UserSubscriptionPlanName(),
                        Count = agencyRepository.GetUserCountPerLocation(plan.AgencyId, plan.AgencyLocationId)
                    };
                }
                else
                {
                    viewData = new LocationPlanViewData
                    {
                        Max = plan.PlanLimit,
                        Id = plan.AgencyLocationId,
                        IsUserPlan = plan.IsUserPlan,
                        CurrentPlan = plan.PlanLimit,
                        NextPlan = plan.PlanLimit.NextPatientSubscriptionPlan(),
                        CurrentPlanDescription = plan.PlanLimit.PatientSubscriptionPlanName(),
                        NextPlanDescription = plan.PlanLimit.NextPatientSubscriptionPlan().PatientSubscriptionPlanName(),
                        Count = agencyRepository.GetPatientCountPerLocation(plan.AgencyId, plan.AgencyLocationId)
                    };
                }
            }
            return viewData;
        }

        #endregion
       

    }
}
