﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Text;
    using System.Security.Cryptography;
    using System.Collections.Specialized;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Infrastructure;
    using System.Collections.Generic;

    public class AssetService : IAssetService
    {
        #region Constructor

        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;

        public AssetService(IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assetRepository = agencyManagementDataProvider.AssetRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }

        #endregion

        #region IAssetService Members

        public bool AddAsset(string fileName, string contentType, byte[] bytes)
        {
            bool result = false;
            var asset = new Asset
            {
                Bytes = bytes,
                FileName = fileName,
                IsDeprecated = false,
                ContentType = contentType,
                AgencyId = Current.AgencyId,
                FileSize = bytes.Length.ToString()
            };
            return  assetRepository.Add(asset);
        }

        public bool AddAsset(HttpPostedFileBase file)
        {
            Check.Argument.IsNotNull(file, "file");

            bool result = false;
            Asset asset = null;
            file.InputStream.Position = 0;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                asset = new Asset
                {
                    IsDeprecated = false,
                    FileName = file.FileName,
                    AgencyId = Current.AgencyId,
                    ContentType = file.ContentType,
                    FileSize = file.ContentLength.ToString(),
                    Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                };
            }
            return assetRepository.Add(asset);
        }

        public bool AddAsset(Asset asset)
        {
            return assetRepository.Add(asset);

        }

        public bool DeleteAsset(Guid assetId)
        {
            return assetRepository.Delete(assetId,Current.AgencyId);
        }

        public bool RemoveAsset(Guid agencyId, Guid assetId)
        {
            var result = false;
            if (assetRepository.Remove(agencyId, assetId))
            {
                result = true;
                AssetHelper.HttpUploadOrDelete(agencyId, assetId,CoreSettings.AssetDeleteURL,new Byte[0], string.Empty,string.Empty);
            }
            return result;
        }

        public bool RemoveAssetOnly(Guid agencyId, Guid assetId)
        {
            var result = false;
            if (assetRepository.Remove(agencyId, assetId))
            {
                result = true;
            }
            return result;
        }


        public bool AddPatientDocument(Asset asset, PatientDocument patientDocument)
        {
            bool result = false;
            if (assetRepository.Add(asset))
            {
                result = true;
                assetRepository.AddPatientDocument(asset, patientDocument);
            }
            return result;
        }

        public bool AddDocument(PatientDocument patientDocument, HttpFileCollectionBase httpFiles)
        {
            var result = false;

            if (httpFiles != null && httpFiles.Count > 0)
            {
                foreach (string key in httpFiles.AllKeys)
                {
                    HttpPostedFileBase file = httpFiles.Get(key);
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        var binaryReader = new BinaryReader(file.InputStream);
                        var asset = new Asset
                        {
                            FileName = file.FileName,
                            AgencyId = Current.AgencyId,
                            ContentType = file.ContentType,
                            FileSize = file.ContentLength.ToString(),
                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                        };
                        if (AddPatientDocument(asset, patientDocument))
                        {
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public bool EditDocument(PatientDocument patientDocument)
        {
            var result = false;
            var existingPatientDocument = assetRepository.GetPatientDocument(patientDocument.Id, patientDocument.PatientId, Current.AgencyId);
            if (existingPatientDocument != null)
            {
                existingPatientDocument.Modified = DateTime.Now;
                existingPatientDocument.Filename = patientDocument.Filename;
                existingPatientDocument.UploadTypeId = patientDocument.UploadTypeId;
                result = assetRepository.UpdatePatientDocument(existingPatientDocument);
            }
            return result;
        }

        public List<PatientDocument> GetDocuments(Guid patientId)
        {
            var list = new List<PatientDocument>();
            list = assetRepository.GetPatientDocuments(patientId, Current.AgencyId);
            list.ForEach(a =>
            {
                var type = agencyRepository.FindUploadType(a.AgencyId, a.UploadTypeId);
                if (type != null)
                {
                    a.TypeName = type.Type;
                }
            });
            return list;
        }

        public bool DeleteDocument(Guid Id, Guid patientId)
        {
            var result = false;
            if (assetRepository.DeletePatientDocument(Id, patientId, Current.AgencyId))
            {
                result = true;
            }
            return result;
        }

        public PatientDocument GetDocumentEdit(Guid patientId, Guid documentId)
        {
            var patientDocument = assetRepository.GetPatientDocument(documentId, patientId, Current.AgencyId);
            if (patientDocument != null)
            {
                patientDocument.PatientName = patientRepository.Get(patientId, Current.AgencyId).DisplayName;
            }
            return patientDocument;
        }


        #endregion

      
    }
}
