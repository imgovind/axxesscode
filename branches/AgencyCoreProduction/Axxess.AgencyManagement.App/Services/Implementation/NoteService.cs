﻿

namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core;
    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.OasisC.Repositories;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.Extensions;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.App.iTextExtension;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.Enums;
    using System.Web.Mvc;
    using System.Web;
    using System.IO;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.App.Helpers;
    using Axxess.Membership.Repositories;
    using Axxess.LookUp.Domain;

    public class NoteService : INoteService
    {


        private readonly ILookupService lookupService;
        private readonly IUserRepository userRepository;
        private readonly IAssetService assetService;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IOasisCDataProvider oasisDataProvider;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IAssessmentRepository oasisAssessmentRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IEpisodeRepository episodeRepository;
        private readonly ILoginRepository loginRepository;

        public NoteService(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, IMembershipDataProvider membershipDataProvider, ILookupService lookupService, IAssetService assetService)
        {
            Check.Argument.IsNotNull(assetService, "assetService");
            Check.Argument.IsNotNull(lookupService, "lookupService");
            Check.Argument.IsNotNull(oasisDataProvider, "oasisDataProvider");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupService = lookupService;
            this.oasisDataProvider = oasisDataProvider;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.oasisAssessmentRepository = oasisDataProvider.OasisAssessmentRepository;
            this.assetService = assetService;
            this.scheduleRepository = agencyManagementDataProvider.ScheduleRepository;
            this.episodeRepository = agencyManagementDataProvider.EpisodeRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
        }


        private IDictionary<string, NotesQuestion> CombineNoteQuestionsAndOasisQuestions(IDictionary<string, NotesQuestion> noteQuestions, IDictionary<string, NotesQuestion> oasisQuestions)
        {
            var questions = noteQuestions;
            if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                {
                    noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                }
                else if (!noteQuestions.ContainsKey("PrimaryDiagnosis"))
                {
                    noteQuestions.Add("PrimaryDiagnosis", oasisQuestions["PrimaryDiagnosis"]);
                }
            }

            if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                {
                    noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                }
                else if (!noteQuestions.ContainsKey("ICD9M"))
                {
                    noteQuestions.Add("ICD9M", oasisQuestions["ICD9M"]);
                }
            }

            if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                {
                    noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                }
                else if (!noteQuestions.ContainsKey("PrimaryDiagnosis1"))
                {
                    noteQuestions.Add("PrimaryDiagnosis1", oasisQuestions["PrimaryDiagnosis1"]);
                }
            }

            if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                {
                    noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                }
                else if (!noteQuestions.ContainsKey("ICD9M1"))
                {
                    noteQuestions.Add("ICD9M1", oasisQuestions["ICD9M1"]);
                }
            }
            return questions;
        }

        public MissedVisit GetMissedVisitPrint()
        {
            var note = new MissedVisit();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return note;
        }

        public MissedVisit GetMissedVisitPrint(Guid patientId, Guid eventId)
        {
            var note = scheduleRepository.GetMissedVisitOnly(Current.AgencyId, eventId);
            if (note != null)
            {
                note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                note.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// patientRepository.GetSchedule(Current.AgencyId, note.EpisodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    note.EventDate = scheduleEvent != null && scheduleEvent.EventDate.IsValid() ? scheduleEvent.EventDate.ToString("MM/dd/yyyy") : string.Empty;
                    note.DisciplineTaskName = scheduleEvent != null && scheduleEvent.DisciplineTaskName.IsNotNullOrEmpty() ? scheduleEvent.DisciplineTaskName : string.Empty;
                }
            }
            return note;
        }

        public PrintViewData<VisitNotePrintViewData> GetVisitNotePrint()
        {

            var viewData = new PrintViewData<VisitNotePrintViewData>();
            var note = new VisitNotePrintViewData();
            note.AgencyId = Current.AgencyId;
            viewData.Data = note;
            viewData.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(note.AgencyId);
            viewData.PatientProfile = new PatientProfileLean();

            return viewData;
        }

        public PrintViewData<VisitNotePrintViewData> GetVisitNotePrint(string type)
        {
            var viewData = new PrintViewData<VisitNotePrintViewData>();
            var note = new VisitNotePrintViewData();
            note.AgencyId = Current.AgencyId;
            note.Type = type;
            viewData.Data = note;
            viewData.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(note.AgencyId);
            viewData.PatientProfile = new PatientProfileLean();
           
            return viewData;
        }

        public VisitNoteEditViewData GetTransportationNote(Guid episodeId, Guid patientId, Guid eventId)
        {

            var viewData = GetTransportationNoteHelper <VisitNoteEditViewData>(episodeId, patientId, eventId);
            var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
            if (patientProfile != null)
            {
                patientProfile.ToDisplayName();
            }
            return viewData;
        }

        public PrintViewData<VisitNotePrintViewData> GetTransportationNotePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new PrintViewData<VisitNotePrintViewData>();
            viewData.Data = GetTransportationNoteHelper <VisitNotePrintViewData>(episodeId, patientId, eventId);
            if (viewData.Data != null)
            {
                viewData.PatientProfile = patientRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
                if (viewData.PatientProfile != null && !viewData.PatientProfile.AgencyLocationId.IsEmpty())
                {
                    viewData.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, viewData.PatientProfile.AgencyLocationId);
                }
            }
            return viewData;
        }

        private T GetTransportationNoteHelper<T>(Guid episodeId, Guid patientId, Guid eventId) where T : VisitNoteViewData, new()
        {
            var note = new T();
            // Note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (visitNote != null)
            {
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    note.StatusComment = scheduledEvent.StatusComment;
                    var noteQuestions = visitNote.ToDictionary();
                    note.Questions = noteQuestions;
                    note.EndDate = scheduledEvent.EndDate;
                    note.StartDate = scheduledEvent.StartDate;
                    note.VisitDate = scheduledEvent.VisitDate.ToString("MM/dd/yyyy");
                    note.DisciplineTask = scheduledEvent.DisciplineTask;
                    note.PatientId = visitNote.PatientId;
                    note.EpisodeId = visitNote.EpisodeId;
                    note.EventId = visitNote.Id;
                    note.UserId = scheduledEvent.UserId;
                    note.Type = visitNote.NoteType.IsNotNullOrEmpty() ? visitNote.NoteType.Trim() : string.Empty;
                    note.TypeName = visitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), visitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), visitNote.NoteType)).GetDescription() : string.Empty;
                    note.Version = visitNote.Version;
                }
                else note.Questions = new Dictionary<string, NotesQuestion>();
            }
            else note.Questions = new Dictionary<string, NotesQuestion>();
            return note;
        }


        public PrintViewData<VisitNotePrintViewData> GetVisitNotePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return GetVisitNotePrint(Current.AgencyId, episodeId, patientId, eventId);
        }

        public PrintViewData<VisitNotePrintViewData> GetVisitNotePrint(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new PrintViewData<VisitNotePrintViewData>();
            var note = new VisitNotePrintViewData();
            //note.Agency = agencyRepository.GetWithBranches(agencyId);
            var patientvisitNote = patientRepository.GetVisitNote(agencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                note.AgencyId = patientvisitNote.AgencyId;
                var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, agencyId);
                if (allergyProfile != null) note.Allergies = allergyProfile.ToString();
                note.SignatureText = patientvisitNote.SignatureText;
                note.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                note.PhysicianSignatureText = patientvisitNote.PhysicianSignatureText;
                note.PhysicianSignatureDate = patientvisitNote.PhysicianSignatureDate;
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    note.DisciplineTask = scheduledEvent.DisciplineTask;
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {

                        var assessment = oasisAssessmentRepository.GetEpisodeAssessment(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.EpisodeId, scheduledEvent.StartDate, true);
                        if (assessment != null)
                        {
                            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                            IDictionary<string, NotesQuestion> assementQuestions = null;
                            if (patientvisitNote.IsSkilledNurseNote())
                            {
                                assementQuestions = assessment.ToSpecificQuestionDictionary(SectionQuestionType.Diagnoses | SectionQuestionType.HomeBoundStatus);
                            }
                            else
                            {
                                assementQuestions = assessment.ToDiagnosisQuestionDictionary();
                            }
                            if (patientvisitNote.Note != null)
                            {
                                var noteQuestions = patientvisitNote.ToDictionary();
                                note.Questions = assementQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                            }
                            else
                            {
                                note.Questions = assementQuestions;
                            }
                        }
                        else if (patientvisitNote.Note != null)
                        {
                            note.Questions = patientvisitNote.ToDictionary();
                        }
                        else
                        {
                            note.Questions = new Dictionary<string, NotesQuestion>();
                        }
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if ((noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"].Answer.IsNullOrEmpty())
                            || !noteQuestions.ContainsKey("PrimaryDiagnosis"))
                        {
                            var assessment = oasisAssessmentRepository.GetEpisodeAssessment(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.EpisodeId, scheduledEvent.StartDate, true);// GetEpisodeAssessment(scheduledEvent, true);
                            if (assessment != null)
                            {
                                assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                                var assementQuestions = assessment.ToNotesQuestionDictionary();
                                note.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, assementQuestions);
                            }
                            else
                            {
                                note.Questions = noteQuestions;
                            }
                        }
                        else
                        {
                            note.Questions = noteQuestions;
                        }
                        if (note.DisciplineTask == (int)DisciplineTasks.PASVisit)
                        {
                            if (!noteQuestions.ContainsKey("TravelTimeIn"))
                            {
                                note.Questions.Add("TravelTimeIn", new NotesQuestion { Name = "TravelTimeIn", Answer = scheduledEvent.TravelTimeIn });
                                note.Questions.Add("TravelTimeOut", new NotesQuestion { Name = "TravelTimeOut", Answer = scheduledEvent.TravelTimeOut });
                            }
                        }

                    }
                }
                else if (patientvisitNote.Note != null)
                {
                    note.Questions = patientvisitNote.ToDictionary();
                }
                if (note.DisciplineTask == (int)DisciplineTasks.HHAideVisit)
                {
                    if (scheduledEvent.EventDate.IsValid())
                    {
                        IDictionary<string, NotesQuestion> pocQuestions = null;
                        var pocEvent = scheduleRepository.GetCarePlanForHHAideCarePlan(agencyId, patientId, episodeId, scheduledEvent.StartDate, scheduledEvent.EndDate, scheduledEvent.EventDate.AddDays(-1), out pocQuestions);// this.GetCarePlanBySelectedEpisode(agencyId, patientId, episodeId, DisciplineTasks.HHAideCarePlan, out pocQuestions, scheduledEvent.EventDate);
                        if (pocEvent != null)
                        {
                            if (note.Questions.ContainsKey("HHAFrequency"))
                            {
                                note.Questions.Remove("HHAFrequency");
                                note.Questions.Add("HHAFrequency", pocQuestions["HHAFrequency"]);
                            }
                            if (note.Questions.ContainsKey("PrimaryDiagnosis"))
                            {
                                note.Questions.Remove("PrimaryDiagnosis");
                                note.Questions.Add("PrimaryDiagnosis", pocQuestions["PrimaryDiagnosis"]);
                            }
                            if (note.Questions.ContainsKey("PrimaryDiagnosis1"))
                            {
                                note.Questions.Remove("PrimaryDiagnosis1");
                                note.Questions.Add("PrimaryDiagnosis1", pocQuestions["PrimaryDiagnosis1"]);
                            }
                        }
                    }
                }
                note.Questions.Remove("Allergies");
                note.Questions.Add("Allergies", new NotesQuestion { Name = "Allergies", Answer = allergyProfile.ToString() });
                var selectedEpisodeId = note.Questions != null && note.Questions.ContainsKey("SelectedEpisodeId") && note.Questions["SelectedEpisodeId"].Answer.IsNotNullOrEmpty() ? note.Questions["SelectedEpisodeId"].Answer.ToGuid() : Guid.Empty;
                if (!selectedEpisodeId.IsEmpty())
                {
                    var episode = episodeRepository.GetEpisodeDateRange(agencyId, patientId, selectedEpisodeId);
                    if (episode != null)
                    {
                        note.EndDate = episode.EndDate;
                        note.StartDate = episode.StartDate;
                    }
                }
                else
                {
                    note.EndDate = scheduledEvent.EndDate;
                    note.StartDate = scheduledEvent.StartDate;
                    note.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString("MM/dd/yyyy") : scheduledEvent.EventDate.ToString("MM/dd/yyyy");
                }
                note.IsWoundCareExist = patientvisitNote.IsWoundCare;
                if (note.IsWoundCareExist) note.WoundCare = patientvisitNote.ToWoundCareDictionary();
                note.PatientId = patientvisitNote.PatientId;
                note.EpisodeId = patientvisitNote.EpisodeId;
                note.EventId = patientvisitNote.Id;
                note.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                note.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                note.Version = patientvisitNote.Version;
                //}
                //}
            }
            else
            {
                note.Questions = new Dictionary<string, NotesQuestion>();
            }

            //var patient = patientRepository.GetPatientOnly(patientId, agencyId);
            viewData.PatientProfile = patientRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
            if (viewData.PatientProfile != null && !viewData.PatientProfile.AgencyLocationId.IsEmpty())
            {
                viewData.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, viewData.PatientProfile.AgencyLocationId);
            }
            if (note.Type != null && (note.Type == DisciplineTasks.PTEvaluation.ToString()
                                        || note.Type == DisciplineTasks.PTReEvaluation.ToString()
                                        || note.Type == DisciplineTasks.OTEvaluation.ToString()
                                        || note.Type == DisciplineTasks.OTReEvaluation.ToString()
                                        || note.Type == DisciplineTasks.STEvaluation.ToString()
                                        || note.Type == DisciplineTasks.STReEvaluation.ToString()
                                        || note.Type == DisciplineTasks.MSWEvaluationAssessment.ToString()
                                        || note.Type == DisciplineTasks.PTDischarge.ToString()
                                        || note.Type == DisciplineTasks.SixtyDaySummary.ToString()))
            {
                AgencyPhysician physician = patientvisitNote.PhysicianData.ToObject<AgencyPhysician>();
                if (physician != null)
                {
                    note.PhysicianId = physician.Id;
                    note.PhysicianDisplayName = physician.DisplayName;
                }
                else if (!patientvisitNote.PhysicianId.IsEmpty())
                {
                    physician = physicianRepository.Get(patientvisitNote.PhysicianId, agencyId);
                    if (physician != null)
                    {
                        note.PhysicianDisplayName = physician.DisplayName;
                    }
                }
            }
            else
            {
                if (note.Questions != null && note.Questions.ContainsKey("PhysicianId") && note.Questions["PhysicianId"].Answer.IsNotNullOrEmpty() && note.Questions["PhysicianId"].Answer.IsGuid())
                {
                    var physician = physicianRepository.Get(note.Questions["PhysicianId"].Answer.ToGuid(), agencyId);
                    if (physician != null)
                    {
                        note.PhysicianId = physician.Id;
                        note.PhysicianDisplayName = physician.DisplayName;
                    }
                }
                else
                {
                    var physician = physicianRepository.GetPatientPrimaryPhysician(agencyId, patientId);
                    if (physician != null)
                    {
                        note.PhysicianId = physician.Id;
                        note.PhysicianDisplayName = physician.DisplayName;
                    }
                    //if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                    //{
                    //    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    //    if (physician != null)
                    //    {
                    //        note.PhysicianId = physician.Id;
                    //        note.PhysicianDisplayName = physician.DisplayName;
                    //    }
                    //}
                }
            }
            if (note.Type != null && (note.Type == DisciplineTasks.PTPlanOfCare.ToString() || note.Type == DisciplineTasks.OTPlanOfCare.ToString() || note.Type == DisciplineTasks.STPlanOfCare.ToString()
                || note.Type == DisciplineTasks.SNPsychAssessment.ToString()))
            {
                note.Questions.Add("OrderNumber", new NotesQuestion { Name = "OrderNumber", Answer = patientvisitNote.OrderNumber.ToString() });
            }

            viewData.Data = note;
            return viewData;
        }


        public AxxessPdf GetPrintPdf(ScheduleEvent scheduleEvent)
        {
            AxxessPdf pdf = null;
            if (scheduleEvent != null)
            {
                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;
                var agencyId = Current.AgencyId;
                switch (task)
                {
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.SNInsulinHS:
                    case DisciplineTasks.SNInsulinNoon:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.LVNVisit:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNWoundCare:
                        {
                            pdf = new SNVisitPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                            break;
                        }
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        pdf = new DiabeticDailyPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.Labs:
                        pdf = new LabsPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.SNPediatricVisit:
                        var snPediatricData = GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id);
                        if (snPediatricData != null && snPediatricData.Data!=null)
                        {
                            if (snPediatricData.Data.Version == 2)
                            {
                                pdf = new PediatricPdf(snPediatricData, PdfDocs.Pediatric2);
                            }
                            else
                            {
                                pdf = new PediatricPdf(snPediatricData, PdfDocs.Pediatric);
                            }
                        }
                        break;
                    case DisciplineTasks.SNPediatricAssessment:
                        pdf = new PediatricPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id), PdfDocs.PediatricAssessment);
                        break;
                    case DisciplineTasks.SNVPsychNurse:
                        pdf = new PsychPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id), PdfDocs.Psych, scheduleEvent.Version);
                        break;
                    case DisciplineTasks.SNPsychAssessment:
                        pdf = new PsychAssessmentPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id), PdfDocs.PsychAssessment, scheduleEvent.Version);
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        pdf = new LVNSVisitPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        pdf = new HHASVisitPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.InitialSummaryOfCare:
                        pdf = new InitialSummaryPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.PTSupervisoryVisit:
                    case DisciplineTasks.OTSupervisoryVisit:
                        pdf = new TherapySupervisoryPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.NutritionalAssessment:
                        pdf = new DieticianPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.OTMaintenance:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.COTAVisit:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTReassessment:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STDischarge:
                    case DisciplineTasks.STReassessment:
                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.STMaintenance:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.PTMaintenance:
                    case DisciplineTasks.PTReassessment:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTAVisit:
                    case DisciplineTasks.OTPlanOfCare:
                    case DisciplineTasks.STPlanOfCare:
                    case DisciplineTasks.PTPlanOfCare:
                        pdf = new TherapyPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.MSWVisit:
                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.MSWProgressNote:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWEvaluationAssessment:
                        pdf = new MSWPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.DriverOrTransportationNote:
                        pdf = new TransportationPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.HHAideVisit:
                        pdf = new HHAVisitPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        pdf = new HHACarePlanPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.PASTravel:
                        pdf = new PASTravelPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.PASVisit:
                        pdf = new PASVisitPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.PASCarePlan:
                        pdf = new PASCarePlanPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.HomeMakerNote:
                        pdf = new HomeMakerNotePdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.OTDischargeSummary:
                        pdf = new DischargeSummaryPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id), PdfDocs.OTDischargeSummary);
                        break;
                    case DisciplineTasks.PTDischargeSummary:
                        var ptDischargeSummaryData = GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id);
                        if (ptDischargeSummaryData != null && ptDischargeSummaryData.Data != null)
                        {
                            if (ptDischargeSummaryData.Data.Version == 2)
                            {
                                pdf = new DischargeSummaryPdf(ptDischargeSummaryData, PdfDocs.PTDischargeSummary2);
                            }
                            else
                            {
                                pdf = new DischargeSummaryPdf(ptDischargeSummaryData, PdfDocs.PTDischargeSummary);
                            }
                        }

                        break;
                    case DisciplineTasks.STDischargeSummary:
                        pdf = new DischargeSummaryPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id), PdfDocs.STDischargeSummary);
                        break;
                    case DisciplineTasks.DischargeSummary:
                        pdf = new DischargeSummaryPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id), PdfDocs.DischargeSummary);
                        break;
                    case DisciplineTasks.SixtyDaySummary:
                    case DisciplineTasks.ThirtyDaySummary:
                    case DisciplineTasks.TenDaySummary:
                        pdf = new SixtyDaySummaryPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.TransferSummary:
                    case DisciplineTasks.CoordinationOfCare:
                        pdf = new TransferSummaryPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;

                    case DisciplineTasks.UAPWoundCareVisit:
                        pdf = new WoundCarePdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                        pdf = new UAPInsulinPdf(GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                        break;
                    case DisciplineTasks.MedicareEligibilityReport:
                        var eligibility = patientRepository.GetMedicareEligibility(agencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                        var patientEligibility = eligibility.Result.FromJson<PatientEligibility>();
                        pdf = new MedicareEligibilityPdf(patientEligibility, agencyRepository.GetWithBranches(agencyId), patientRepository.GetPatientOnly(scheduleEvent.PatientId, agencyId));
                        break;
                    case DisciplineTasks.DieticianVisit:
                        var dieticianData = GetVisitNotePrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id);
                        pdf = new DieticianPdf(dieticianData);
                        break;
                }
            }
            return pdf;
        }

        //public JsonViewData Notes(string button, FormCollection formCollection)
        //{
        //    Check.Argument.IsNotNull(formCollection, "formCollection");
        //    var keys = formCollection.AllKeys;
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The note could not be saved." };
        //    string type = formCollection["Type"];
        //    if (type.IsNotNullOrEmpty())
        //    {
        //        var eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
        //        var episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
        //        var patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();

        //        if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
        //        {
        //            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
        //            var rules = ValidationHelper.AddNotesValidationRules(type, button, keys, episode, formCollection);
        //            if (button == "Save")
        //            {
        //                var entityValidator = new EntityValidator(rules.ToArray());
        //                entityValidator.Validate();
        //                if (entityValidator.IsValid)
        //                {
        //                    string message = string.Empty;
        //                    patientService.CheckTimeOverlap(formCollection, out message);
        //                    if (SaveNotes( button, formCollection))
        //                    {
        //                        viewData.isSuccessful = true;
        //                        viewData.errorMessage = "The note was successfully saved.";
        //                        viewData.warningMessage = message;
        //                    }
        //                    else
        //                    {
        //                        viewData.isSuccessful = false;
        //                        viewData.errorMessage = "The note could not be saved.";
        //                    }
        //                }
        //                else
        //                {
        //                    viewData.isSuccessful = false;
        //                    viewData.errorMessage = entityValidator.Message;
        //                }
        //            }
        //            else if (button == "Complete")
        //            {
        //                var entityValidator = new EntityValidator(rules.ToArray());
        //                entityValidator.Validate();
        //                if (entityValidator.IsValid)
        //                {
        //                    string message = string.Empty;
        //                    patientService.CheckTimeOverlap(formCollection, out message);
        //                    if (SaveNotes( button, formCollection))
        //                    {
        //                        viewData.isSuccessful = true;
        //                        viewData.errorMessage = "The note was successfully Submited." + message;
        //                    }
        //                }
        //                else
        //                {
        //                    viewData.isSuccessful = false;
        //                    viewData.errorMessage = entityValidator.Message;
        //                }
        //            }
        //            else if (button == "Approve")
        //            {
        //                var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
        //                if (patientVisitNote != null)
        //                {
        //                    if ((patientVisitNote.SignatureText.IsNullOrEmpty() || patientVisitNote.SignatureDate == DateTime.MinValue) &&
        //                        (!keys.Contains(type + "_Clinician") || !keys.Contains(type + "_SignatureDate")))
        //                    {
        //                        viewData.isSuccessful = false;
        //                        viewData.errorMessage = "The note could not be approved because the Electronic Signature is missing. Please sign this note before continuing.";
        //                    }
        //                    else
        //                    {
        //                        var entityValidator = new EntityValidator(rules.ToArray());
        //                        entityValidator.Validate();
        //                        if (entityValidator.IsValid)
        //                        {
        //                            string message = string.Empty;
        //                            patientService.CheckTimeOverlap(formCollection, out message);
        //                            if (SaveNotes(patientId, episodeId, eventId, button, formCollection))
        //                            {
        //                                viewData.isSuccessful = true;
        //                                viewData.errorMessage = "The note was successfully Approved." + message;
        //                            }
        //                            else
        //                            {
        //                                return viewData;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            viewData.isSuccessful = false;
        //                            viewData.errorMessage = entityValidator.Message;
        //                        }
        //                    }
        //                }
        //            }
        //            else if (button == "Return")
        //            {
        //                string message = string.Empty;
        //                patientService.CheckTimeOverlap(formCollection, out message);
        //                if (SaveNotes(patientId, episodeId, eventId, button, formCollection))
        //                {
        //                    viewData.isSuccessful = true;
        //                    viewData.errorMessage = "The note was successfully returned." + message;
        //                }
        //                else
        //                {
        //                    viewData.isSuccessful = false;
        //                    viewData.errorMessage = "The note could not be returned.";
        //                }
        //            }
        //        }
        //    }
        //    return viewData;
        //}

        public JsonViewData SaveNotes(string button, FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The note could not be saved." };

            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {

                string type = keys.Contains("Type") ? formCollection["Type"] : string.Empty;
                if (type.IsNotNullOrEmpty())
                {
                    var eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
                    var episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
                    var patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();

                    if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                    {
                        var timeIn = keys.Contains(string.Format("{0}_TimeIn", type)) && formCollection[string.Format("{0}_TimeIn", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeIn", type)] : string.Empty;
                        var timeOut = keys.Contains(string.Format("{0}_TimeOut", type)) && formCollection[string.Format("{0}_TimeOut", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeOut", type)] : string.Empty;
                        var travelTimeIn = keys.Contains(string.Format("{0}_TravelTimeIn", type)) && formCollection[string.Format("{0}_TravelTimeIn", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TravelTimeIn", type)] : string.Empty;
                        var travelTimeOut = keys.Contains(string.Format("{0}_TravelTimeOut", type)) && formCollection[string.Format("{0}_TravelTimeOut", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TravelTimeOut", type)] : string.Empty;
                        var sendAsOrder = keys.Contains(string.Format("{0}_SendAsOrder", type)) && formCollection[string.Format("{0}_SendAsOrder", type)].IsNotNullOrEmpty();
                        var surcharge = keys.Contains(string.Format("{0}_Surcharge", type)) && formCollection[string.Format("{0}_Surcharge", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_Surcharge", type)] : string.Empty;
                        var mileage = keys.Contains(string.Format("{0}_AssociatedMileage", type)) && formCollection[string.Format("{0}_AssociatedMileage", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_AssociatedMileage", type)] : string.Empty;
                        string date = keys.Contains(string.Format("{0}_VisitDate", type)) ? formCollection.Get(string.Format("{0}_VisitDate", type)).ToDateTime().ToShortDateString() : string.Empty;
                        int disciplineTask = keys.Contains("DisciplineTask") ? formCollection.Get("DisciplineTask").ToInteger() : 0;

                        var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);

                        if (scheduleEvent != null)
                        {
                            var rules = ValidationHelper.AddNotesValidationRules(type, button, keys, scheduleEvent.StartDate, scheduleEvent.EndDate, formCollection);
                            if (button == "Complete" || button == "Approve")
                            {
                                if (keys.Contains(type + "_Clinician"))
                                {
                                    var signature = formCollection[type + "_Clinician"];
                                    rules.Add(new Validation(() => signature.IsNotNullOrEmpty() && !this.loginRepository.IsSignatureCorrect(Current.LoginId, signature), "User Signature is not correct."));
                                }
                            }
                            var entityValidator = new EntityValidator(rules.ToArray());
                            entityValidator.Validate();
                            if (entityValidator.IsValid)
                            {
                                string message = string.Empty;
                                var oldSchedule = scheduleEvent.DeepClone();
                                var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);

                                if (patientVisitNote != null)
                                {
                                    scheduleEvent.SendAsOrder = sendAsOrder;
                                    patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                                    patientVisitNote.Note = patientVisitNote.Questions.ToXml();
                                    patientVisitNote.PhysicianId = keys.Contains(string.Format("{0}_PhysicianId", type)) ? formCollection.Get(string.Format("{0}_PhysicianId", type)).ToGuid() : Guid.Empty;
                                    var shouldUpdateEpisode = false;
                                    var description = string.Empty;
                                    var oldStatus = scheduleEvent.Status;
                                    if (button == "Save" || button == "Complete" || button == "Approve")
                                    {
                                        if (button == "Save")
                                        {
                                            if (!(Current.HasRight(Permissions.AccessCaseManagement) && oldStatus == ((int)ScheduleStatus.NoteSubmittedWithSignature)))
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.NoteSaved;
                                            }
                                            if (type == DisciplineTasks.PTPlanOfCare.ToString() || type == DisciplineTasks.OTPlanOfCare.ToString() || type == DisciplineTasks.STPlanOfCare.ToString())
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.OrderSaved;
                                            }
                                        }
                                        else
                                        {
                                            if (button == "Complete")
                                            {
                                                patientVisitNote.UserId = Current.UserId;
                                                scheduleEvent.DisciplineTask = disciplineTask;
                                            }

                                            if (button == "Approve" || Current.HasRight(Permissions.BypassCaseManagement))
                                            {
                                                if ((patientVisitNote.SignatureText.IsNullOrEmpty() || patientVisitNote.SignatureDate == DateTime.MinValue) &&
                                                    (!keys.Contains(type + "_Clinician") || !keys.Contains(type + "_SignatureDate")))
                                                {
                                                    viewData.isSuccessful = false;
                                                    viewData.errorMessage = "The note could not be approved because the Electronic Signature is missing. Please sign this note before continuing.";
                                                    return viewData;
                                                }

                                                scheduleEvent.ReturnReason = string.Empty;
                                                patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                                                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation
                                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment || scheduleEvent.DisciplineTask == (int)DisciplineTasks.SNPsychAssessment)
                                                {
                                                    if (patientVisitNote.Version == 0 || patientVisitNote.Version == 1)
                                                    {
                                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                                    }
                                                    else if (sendAsOrder.Equals("1"))
                                                    {
                                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                                    }
                                                    else
                                                    {
                                                        patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                                    }
                                                }

                                                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTDischarge || scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary || scheduleEvent.DisciplineTask == (int)DisciplineTasks.ThirtyDaySummary || scheduleEvent.DisciplineTask == (int)DisciplineTasks.TenDaySummary
                                                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReassessment || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReassessment || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTDischarge || scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWDischarge || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STDischarge)
                                                {
                                                    if (sendAsOrder.Equals("1"))
                                                    {
                                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                                    }
                                                }
                                                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTPlanOfCare || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTPlanOfCare || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STPlanOfCare)
                                                {
                                                    patientVisitNote.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                                                }
                                            }
                                            else
                                            {
                                                patientVisitNote.Status = (int)ScheduleStatus.NoteSubmittedWithSignature;
                                                if (type == DisciplineTasks.PTPlanOfCare.ToString() || type == DisciplineTasks.OTPlanOfCare.ToString() || type == DisciplineTasks.STPlanOfCare.ToString())
                                                {
                                                    patientVisitNote.Status = (int)ScheduleStatus.OrderSubmittedPendingReview;
                                                }
                                            }
                                            //Various changes that occur for both complete and approve action, but not save
                                            scheduleEvent.InPrintQueue = true;
                                            patientVisitNote.SignatureDate = formCollection[type + "_SignatureDate"].ToDateTime();
                                            patientVisitNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                            if ((type == DisciplineTasks.PTEvaluation.ToString() && patientVisitNote.Version >= 5) || (type == DisciplineTasks.OTEvaluation.ToString() && patientVisitNote.Version >= 3) || (type == DisciplineTasks.STEvaluation.ToString() && patientVisitNote.Version >= 3))
                                            {
                                                if (!sendAsOrder.Equals("1"))
                                                {
                                                    AddPlanOfCareForEval(patientVisitNote, scheduleEvent, type);
                                                }
                                            }
                                        }
                                        scheduleRepository.CheckTimeOverlap(scheduleEvent, out message);
                                        SetNotePhysicianDataByType(type, patientVisitNote);
                                        patientVisitNote.Modified = DateTime.Now;
                                        patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                        scheduleEvent.Status = patientVisitNote.Status;
                                        scheduleEvent.TimeIn = timeIn;
                                        scheduleEvent.TimeOut = timeOut;
                                        scheduleEvent.TravelTimeIn = travelTimeIn;
                                        scheduleEvent.TravelTimeOut = travelTimeOut;
                                        scheduleEvent.Surcharge = surcharge;
                                        scheduleEvent.AssociatedMileage = mileage;
                                        scheduleEvent.DisciplineTask = disciplineTask;
                                        if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary))
                                        {
                                            if (date.IsValidDate())
                                            {
                                                scheduleEvent.VisitDate = date.ToDateTime();
                                            }
                                        }

                                        shouldUpdateEpisode = true;
                                    }
                                    else if (button == "Return")
                                    {
                                        scheduleRepository.CheckTimeOverlap(scheduleEvent, out message);
                                        var returnSignature = keys.Contains(string.Format("{0}_ReturnForSignature", type)) && formCollection[string.Format("{0}_ReturnForSignature", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_ReturnForSignature", type)] : string.Empty;
                                        patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                                        if (returnSignature.IsNotNullOrEmpty())
                                        {
                                            patientVisitNote.Status = ((int)ScheduleStatus.NoteReturnedForClinicianSignature);
                                        }
                                        patientVisitNote.Modified = DateTime.Now;
                                        patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();

                                        scheduleEvent.Status = patientVisitNote.Status;
                                        scheduleEvent.ReturnReason = string.Empty;

                                        shouldUpdateEpisode = true;
                                    }
                                    if (shouldUpdateEpisode)
                                    {
                                        if (scheduleRepository.UpdateScheduleTaskModal(scheduleEvent))
                                        {
                                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                                            {
                                                viewData.isSuccessful = true;
                                                viewData.errorMessage = "The note was successfully processed.";
                                                Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                            }
                                            else
                                            {
                                                scheduleRepository.UpdateScheduleTaskModal(oldSchedule);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = entityValidator.Message;
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        private void SetNotePhysicianDataByType(string type, PatientVisitNote patientVisitNote)
        {
            if (type == DisciplineTasks.PTEvaluation.ToString() || type == DisciplineTasks.PTReEvaluation.ToString() || type == DisciplineTasks.OTEvaluation.ToString() || type == DisciplineTasks.OTReEvaluation.ToString() || type == DisciplineTasks.STEvaluation.ToString() || type == DisciplineTasks.STReEvaluation.ToString() || type == DisciplineTasks.MSWEvaluationAssessment.ToString()
                || type == DisciplineTasks.OTReassessment.ToString() || type == DisciplineTasks.PTReassessment.ToString() || type == DisciplineTasks.MSWDischarge.ToString() || type == DisciplineTasks.OTDischarge.ToString() || type == DisciplineTasks.STDischarge.ToString() || type == DisciplineTasks.PTDischarge.ToString())
            {
                var physician = PhysicianEngine.Get(patientVisitNote.PhysicianId, Current.AgencyId);
                if (physician != null)
                {
                    patientVisitNote.PhysicianData = physician.ToXml();
                }
            }
        }

        public bool SaveWoundCare(FormCollection formCollection, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
            string type = formCollection["Type"];
            if (type.IsNotNullOrEmpty())
            {
                Guid eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
                Guid episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
                Guid patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();
                var patientVisitNote = new PatientVisitNote();
                if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                    patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                    if (patientVisitNote != null)
                    {
                        patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                        if (patientVisitNote.Questions != null)
                        {
                            var listOfAssets = new List<Asset>();
                            var isAsseSave = false;
                            if (httpFiles.Count > 0)
                            {
                                isAsseSave = true;
                                //var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                //var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduleEvents.FirstOrDefault(e => e.EventId == eventId && e.EpisodeId == episodeId && e.PatientId == patientId);
                                scheduleEvent.Assets = scheduleEvent.Asset.ToObject<List<Guid>>() ?? new List<Guid>();
                                foreach (string key in httpFiles.AllKeys)
                                {
                                    var keyArray = key.Split('_');
                                    HttpPostedFileBase file = httpFiles.Get(key);
                                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                                    {
                                        using (var binaryReader = new BinaryReader(file.InputStream))
                                        {
                                            var asset = new Asset
                                            {
                                                FileName = file.FileName,
                                                AgencyId = Current.AgencyId,
                                                ContentType = file.ContentType,
                                                FileSize = file.ContentLength.ToString(),
                                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                            };

                                            listOfAssets.Add(asset);

                                            if (assetService.AddAsset(asset))
                                            {
                                                if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                                {
                                                    patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = asset.Id.ToString();
                                                }
                                                else
                                                {
                                                    patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = asset.Id.ToString(), Type = keyArray[0] });
                                                }
                                                if (!scheduleEvent.Assets.Contains(asset.Id))
                                                {
                                                    scheduleEvent.Assets.Add(asset.Id);
                                                    //scheduleEvents.RemoveAll(e => e.EventId == scheduleEvent.EventId && e.EpisodeId == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
                                                    //scheduleEvents.Add(scheduleEvent);
                                                    //episode.Schedule = scheduleEvents.ToXml();
                                                    //patientRepository.UpdateEpisode(episode);
                                                }
                                            }
                                            else
                                            {
                                                if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                                {
                                                    patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                                }
                                                else
                                                {
                                                    patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                                }
                                                assetsSaved = false;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                        {
                                            patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                        }
                                        else
                                        {
                                            patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                        }
                                    }
                                }
                            }
                            patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                            patientVisitNote.IsWoundCare = true;
                            if (assetsSaved && patientRepository.UpdateVisitNoteWound(patientVisitNote))
                            {
                                result = true;
                                Auditor.Log(patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, DisciplineTasks.SkilledNurseVisit, "Wound Care is added/updated.");
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(name, "name");
            var result = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.WoundNote.IsNotNullOrEmpty())
                {
                    patientVisitNote.Questions = patientVisitNote.WoundNote.ToObject<List<NotesQuestion>>();
                    if (patientVisitNote.Questions.Exists(q => q.Name == name))
                    {
                        patientVisitNote.Questions.SingleOrDefault(q => q.Name == name).Answer = Guid.Empty.ToString();
                        patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (assetService.DeleteAsset(assetId))
                            {
                                //var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                //if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                                //{
                                //var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);//scheduleEvents.FirstOrDefault(e => e.EventId == eventId && e.EpisodeId == episodeId && e.PatientId == patientId);
                                if (scheduleEvent != null && scheduleEvent.Asset.IsNotNullOrEmpty())
                                {
                                    scheduleEvent.Assets = scheduleEvent.Asset.ToObject<List<Guid>>();
                                    if (scheduleEvent.Assets != null && scheduleEvent.Assets.Count > 0 && scheduleEvent.Assets.Contains(assetId))
                                    {
                                        scheduleEvent.Assets.Remove(assetId);
                                        scheduleEvent.Asset = scheduleEvent.Assets.ToXml();
                                        if (scheduleRepository.UpdateScheduleTaskModal(scheduleEvent))
                                        {
                                            result = true;
                                        }
                                    }
                                }
                                else
                                {
                                    result = true;
                                }
                                // }
                            }
                            Auditor.Log(patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, DisciplineTasks.SkilledNurseVisit, "Wound Care Asset is deleted.");
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); // patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    if (scheduleEvent.IsMissedVisit)
                    {
                        var inPrintQueue = scheduleEvent.InPrintQueue;
                        var missedVisit = scheduleRepository.GetMissedVisitOnly(Current.AgencyId, eventId);
                        if (missedVisit != null)
                        {
                            if (button == "Approve")
                            {
                                missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
                                scheduleEvent.InPrintQueue = true;
                                //scheduleEvent.Status = missedVisit.Status.ToString();
                                shouldUpdateEpisode = inPrintQueue != true;
                            }
                            else if (button == "Return")
                            {
                                missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitReturn;
                            }
                            if (!shouldUpdateEpisode || (shouldUpdateEpisode && scheduleRepository.UpdateScheduleTasksPrintQueue(Current.AgencyId, patientId, eventId, scheduleEvent.InPrintQueue)))
                            {
                                if (scheduleRepository.UpdateMissedVisit(missedVisit))
                                {
                                    result = true;
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                }
                                else
                                {
                                    if (shouldUpdateEpisode)
                                    {
                                        scheduleRepository.UpdateScheduleTasksPrintQueue(Current.AgencyId, patientId, eventId, inPrintQueue);
                                    }
                                }
                            }

                        }
                    }
                    else
                    {
                        var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                        if (patientVisitNote != null)
                        {
                            var oldSchedule = scheduleEvent.DeepClone();
                            var updateNote = true;
                            if (button == "Approve")
                            {
                                patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STReEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment || scheduleEvent.DisciplineTask == (int)DisciplineTasks.SNPsychAssessment)
                                {
                                    if (patientVisitNote.Version == 0 || patientVisitNote.Version == 1)
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                    }
                                    else if (scheduleEvent.SendAsOrder)
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                    }
                                    else
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                    }
                                }
                                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTDischarge || scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary || scheduleEvent.DisciplineTask == (int)DisciplineTasks.ThirtyDaySummary || scheduleEvent.DisciplineTask == (int)DisciplineTasks.TenDaySummary || scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTReassessment || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTReassessment || scheduleEvent.DisciplineTask == (int)DisciplineTasks.MSWDischarge || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTDischarge || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STDischarge)
                                {
                                    if (scheduleEvent.SendAsOrder)
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                    }
                                }
                                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PTPlanOfCare || scheduleEvent.DisciplineTask == (int)DisciplineTasks.OTPlanOfCare || scheduleEvent.DisciplineTask == (int)DisciplineTasks.STPlanOfCare)
                                {
                                    if (scheduleEvent.SendAsOrder)
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                                    }
                                }
                                patientVisitNote.Modified = DateTime.Now;
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = patientVisitNote.Status;
                                shouldUpdateEpisode = true;
                            }
                            else if (button == "Return")
                            {
                                patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                                patientVisitNote.Modified = DateTime.Now;

                                scheduleEvent.Status = ((int)ScheduleStatus.NoteReturned);
                                shouldUpdateEpisode = true;
                            }
                            else if (button == "Print")
                            {
                                scheduleEvent.InPrintQueue = false;
                                shouldUpdateEpisode = true;
                                updateNote = false;
                            }

                            if (shouldUpdateEpisode)
                            {

                                if (scheduleRepository.UpdateScheduleTaskModal(scheduleEvent))
                                {
                                    if (!updateNote || (updateNote && patientRepository.UpdateVisitNote(patientVisitNote)))
                                    {
                                        result = true;
                                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                    }
                                    else
                                    {
                                        if (updateNote)
                                        {
                                            scheduleRepository.UpdateScheduleTaskModal(oldSchedule);
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
            return result;
        }


        private bool AddPlanOfCareForEval(PatientVisitNote patientVisitNote, ScheduleEvent scheduleEvent, string type)
        {
            bool result = true;
            var poc = patientRepository.GetPlanOfCareRelation(patientVisitNote.Id, Current.AgencyId);
            int disciplineTask = 0;
            int version = 0;
            if (scheduleEvent.Discipline == (int)Disciplines.PT)
            {
                disciplineTask = (int)DisciplineTasks.PTPlanOfCare;
                version = patientVisitNote.Version - 4;
            }
            else if (scheduleEvent.Discipline == (int)Disciplines.OT)
            {
                disciplineTask = (int)DisciplineTasks.OTPlanOfCare;
                version = patientVisitNote.Version - 2;
            }
            else if (scheduleEvent.Discipline == (int)Disciplines.ST)
            {
                disciplineTask = (int)DisciplineTasks.STPlanOfCare;
                version = patientVisitNote.Version - 2;
            }
            if (poc == null)
            {
                var pocScheduleEvent = new ScheduleEvent
                {
                    Id = Guid.NewGuid(),
                    EventDate = scheduleEvent.VisitDate.IsValid() ? scheduleEvent.VisitDate : scheduleEvent.EventDate,
                    VisitDate = scheduleEvent.VisitDate.IsValid() ? scheduleEvent.VisitDate : scheduleEvent.EventDate,
                    UserId = scheduleEvent.UserId,
                    StartDate = scheduleEvent.StartDate,
                    EndDate = scheduleEvent.EndDate,
                    PatientId = scheduleEvent.PatientId,
                    EpisodeId = scheduleEvent.EpisodeId,
                    Discipline = (int)Disciplines.Orders,
                    DisciplineTask = disciplineTask,
                    Version = version,
                    Status = ((int)ScheduleStatus.OrderSaved),
                };

                if (!CreatePlanofCare(pocScheduleEvent, patientVisitNote))
                {
                    result = false;
                }
            }
            else
            {
                var pocScheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, scheduleEvent.PatientId, poc.Id);
                if (pocScheduleEvent == null)
                {
                    pocScheduleEvent = new ScheduleEvent
                    {
                        AgencyId = Current.AgencyId,
                        Id = Guid.NewGuid(),
                        EventDate = scheduleEvent.EventDate,
                        VisitDate = scheduleEvent.EventDate,
                        UserId = scheduleEvent.UserId,
                        StartDate = scheduleEvent.StartDate,
                        EndDate = scheduleEvent.EndDate,
                        PatientId = scheduleEvent.PatientId,
                        EpisodeId = scheduleEvent.EpisodeId,
                        Discipline = (int)Disciplines.Orders,
                        Version = version,
                        DisciplineTask = disciplineTask,
                        Status = ((int)ScheduleStatus.OrderSaved)
                    };
                    if (scheduleRepository.AddScheduleTask(pocScheduleEvent))
                    {
                        Auditor.Log(pocScheduleEvent.EpisodeId, pocScheduleEvent.PatientId, pocScheduleEvent.Id, Actions.Add, (DisciplineTasks)pocScheduleEvent.DisciplineTask);

                    }
                    else
                    {
                        result = false;
                    }

                }
            }
            return result;
        }

        private List<NotesQuestion> ProcessNoteQuestions(FormCollection formCollection)
        {
            string type = formCollection["Type"];
            formCollection.Remove("Type");
            var questions = new Dictionary<string, NotesQuestion>();
            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if (nameArray != null && nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    if (!questions.ContainsKey(name))
                    {
                        questions.Add(name, new NotesQuestion { Name = name, Answer = formCollection.GetValues(key).Join(","), Type = type });
                    }
                }
            }
            return questions.Values.ToList();
        }

        private bool CreatePlanofCare(ScheduleEvent scheduleEvent, PatientVisitNote note)
        {
            bool result = false;
            var poc = new PatientVisitNote();
            poc.Id = scheduleEvent.Id;
            poc.AgencyId = Current.AgencyId;
            poc.PatientId = note.PatientId;
            poc.EpisodeId = note.EpisodeId;
            poc.UserId = note.UserId;
            poc.PhysicianId = note.PhysicianId;
            poc.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduleEvent.DisciplineTask)).ToString();
            poc.Version = scheduleEvent.Version;
            poc.Questions = ProcessPOCQuestions(note);
            poc.Note = poc.Questions.ToXml();
            poc.Status = scheduleEvent.Status;
            poc.Created = DateTime.Now;
            poc.IsDeprecated = false;
            poc.OrderNumber = patientRepository.GetNextOrderNumber();
            poc.PhysicianData = note.PhysicianData;
            var relation = new NoteRelation();
            relation.Id = scheduleEvent.Id;
            relation.AgencyId = Current.AgencyId;
            relation.ParentId = note.Id;
            relation.IsDeprecated = false;
            if (patientRepository.AddVisitNote(poc))
            {
                if (patientRepository.AddNoteRelation(relation))
                {
                    if (scheduleRepository.AddScheduleTask(scheduleEvent))
                    {
                        Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.Add, (DisciplineTasks)scheduleEvent.DisciplineTask);
                        result = true;
                    }
                    else
                    {
                        patientRepository.RemovePatientVisitNoteFully(scheduleEvent.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                        patientRepository.RemoveNoteRelationFully(scheduleEvent.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                    }
                }
                else
                {
                    patientRepository.RemovePatientVisitNoteFully(scheduleEvent.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                }
            }
            return result;
        }


        private List<NotesQuestion> ProcessPOCQuestions(PatientVisitNote note)
        {
            var questions = note.Questions;
            var pocQuestions = new List<NotesQuestion>();
            if (questions != null)
            {
                foreach (NotesQuestion n in questions)
                {
                    if (n.Name.Equals("TimeIn") || n.Name.Equals("TimeOut") || n.Name.Equals("PrimaryDiagnosis") || n.Name.Equals("PrimaryDiagnosis1"))
                    {
                        pocQuestions.Add(n);
                    }
                    if (n.Name.StartsWith("POC"))
                    {
                        pocQuestions.Add(n);
                    }
                    if (n.Name.EndsWith("Apply"))
                    {
                        if (!pocQuestions.Contains(n))
                            pocQuestions.Add(n);
                    }
                }
            }
            return pocQuestions;
        }

        public List<NotesQuestion> GetPlanOfCareQuestions(Guid pocId, Guid agencyId)
        {
            var note = patientRepository.GetVisitNoteByPlanOfCare(pocId, agencyId);
            note.Questions = note.Note.ToObject<List<NotesQuestion>>();
            return ProcessPOCQuestions(note);
        }


        public MissedVisit GetMissedVisitWithPatientAndUserName(Guid Id)
        {
            var missedVisit = scheduleRepository.GetMissedVisitsWithScheduleInfo(Current.AgencyId, Id);
            if (missedVisit != null)
            {
                missedVisit.PatientName = patientRepository.GetPatientNameById(missedVisit.PatientId, Current.AgencyId);
                if (Enum.IsDefined(typeof(DisciplineTasks), missedVisit.DisciplineTask))
                {
                    missedVisit.VisitType = ((DisciplineTasks)missedVisit.DisciplineTask).GetDescription();
                }
                missedVisit.UserName = UserEngine.GetName(missedVisit.UserId, Current.AgencyId);
            }
            return missedVisit;
        }

        public bool ProcessMissedVisitNotes(string button, Guid Id)
        {
            var mv = scheduleRepository.GetMissedVisitOnly(Current.AgencyId, Id);
            if (button == "Approve")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
            }
            else if (button == "Return")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitReturn;
            }
            if (scheduleRepository.UpdateMissedVisit(mv))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public MissedVisit GetMissedVisitForEdit(Guid id)
        {
            var mv = scheduleRepository.GetMissedVisitOnly(Current.AgencyId, id);
            if (mv != null)
            {
                var schedule = scheduleRepository.GetScheduleTask(Current.AgencyId, mv.PatientId, id);
                if (schedule != null)
                {
                    mv.DisciplineTaskName = schedule.DisciplineTaskName;
                    mv.EventDate = schedule.EventDate.ToString("MM/dd/yyyy");
                    mv.EndDate = schedule.EndDate;
                    mv.StartDate = schedule.StartDate;
                }
                mv.PatientName = patientRepository.GetPatientNameById(mv.PatientId, Current.AgencyId);
            }
            return mv;
        }


        public bool ProcessMissedVisitForm(string button, Guid eventId, string reason)
        {
            var result = false;
            var mv = scheduleRepository.GetMissedVisitOnly(Current.AgencyId, eventId);
            if (button == "Approve")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
            }
            else if (button == "Return")
            {
                mv.Status = (int)ScheduleStatus.NoteMissedVisitReturn;
            }
            if (scheduleRepository.UpdateMissedVisit(mv))
            {
                result = true;
            }
            return result;
        }

        public JsonViewData RestoreMissedVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Visit could not be restored" };
            var evnt = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
            if (evnt != null)
            {
                evnt.IsMissedVisit = false;
                if (scheduleRepository.UpdateScheduleTaskModal(evnt))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Visit has been restored.";
                }
            }
            return viewData;
        }

        public List<ScheduleEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            DateTime now = DateTime.Now;
            var patientEvents = new List<ScheduleEvent>();
            var scheduleEvents = scheduleRepository.GetMissedScheduledEvents(agencyId, branchId, startDate, endDate);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => (se.StartDate.Date >= startDate.Date && se.StartDate.Date <= endDate.Date) || (se.EndDate.Date >= startDate.Date && se.EndDate.Date <= endDate.Date) || (startDate.Date >= se.StartDate.Date && endDate.Date <= se.EndDate.Date)).ToList();
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                var groupedEvents = scheduleEvents.GroupBy(s => s.EpisodeId).ToDictionary(g => g.First(), g => g.ToList());
                if (groupedEvents.IsNotNullOrEmpty())
                {
                    groupedEvents.ForEach((key, value) =>
                    {
                        var detail = key.EpisodeNotes.ToObject<EpisodeDetail>();
                        value.ForEach(v =>
                        {
                            if (detail.Comments.IsNotNullOrEmpty())
                            {
                                v.EpisodeNotes = detail.Comments;
                            }
                            else
                            {
                                v.EpisodeNotes = string.Empty;
                            }
                            v.EventDate = v.EventDate;
                            v.VisitDate = v.VisitDate;
                            v.UserName = !v.UserId.IsEmpty() ? UserEngine.GetName(v.UserId, agencyId).ToUpperCase() : string.Empty;
                            Common.Url.Set(v, true, true);
                            patientEvents.Add(v);
                        });
                    });
                }
            }
            DateTime then = DateTime.Now;
            DateTime diff = new DateTime(then.Ticks - now.Ticks);
            return patientEvents.OrderByDescending(s => s.EventDate).ToList();
        }

        public JsonViewData AddMissedVisit(MissedVisit missedVisit)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Missed visit could not be saved." };
            if (!missedVisit.Id.IsEmpty() && !missedVisit.PatientId.IsEmpty() && !missedVisit.EpisodeId.IsEmpty())
            {
                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, missedVisit.PatientId, missedVisit.Id); //patientRepository.GetSchedule(Current.AgencyId, missedVisit.EpisodeId, missedVisit.PatientId, missedVisit.Id);
                if (scheduleEvent != null)
                {
                    if (scheduleEvent.EventDate.IsValid() && scheduleEvent.EventDate.Date <= DateTime.Now.Date)
                    {
                        var validationRules = new List<Validation>();
                        validationRules.Add(new Validation(() => !missedVisit.SignatureDate.IsValid(), "Signature date is not valid date."));
                        if (missedVisit.UserSignatureAssetId.IsEmpty())
                        {
                            validationRules.Add(new Validation(() => string.IsNullOrEmpty(missedVisit.Signature), "User Signature cannot be empty."));
                            validationRules.Add(new Validation(() => missedVisit.Signature.IsNotNullOrEmpty() ? !loginRepository.IsSignatureCorrect(Current.LoginId, missedVisit.Signature) : false, "User Signature is not correct."));
                        }

                        if (missedVisit.SignatureDate.IsValid())
                        {
                            validationRules.Add(new Validation(() => (missedVisit.SignatureDate < scheduleEvent.StartDate), "Missed visit date must be greater or equal to the episode start date."));
                            validationRules.Add(new Validation(() => (missedVisit.SignatureDate > scheduleEvent.EndDate), "Missed visit date must be must be  less than or equalt to the episode end date."));
                        }

                        var entityValidator = new EntityValidator(validationRules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            if (missedVisit.UserSignatureAssetId.IsNotEmpty())
                            {
                                missedVisit.SignatureText = string.Format("Signed by: {0}", Current.UserFullName);
                            }
                            else
                            {
                                missedVisit.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                            }

                            if (AddMissedVisitHelper(missedVisit, scheduleEvent))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The Missed visit was successfully documented.";
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message;
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The visit scheduled date is in the future.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The visit could not be found.";
                }
            }
            return viewData;
        }

        private bool AddMissedVisitHelper(MissedVisit missedVisit, ScheduleEvent scheduledEvent)
        {
            bool result = false;
            //var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, missedVisit.EpisodeId, missedVisit.PatientId);
            //if (episode != null && episode.Schedule.IsNotNullOrEmpty())
            //{
            missedVisit.AgencyId = Current.AgencyId;
            //var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
            //var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, missedVisit.PatientId, missedVisit.Id); //events.Find(e => e.EventId == missedVisit.Id);
            //if (scheduledEvent != null)
            //{
            var oldIsMissedVisit = scheduledEvent.IsMissedVisit;
            var oldReason = scheduledEvent.MissedVisitFormReturnReason;
            scheduledEvent.IsMissedVisit = true;
            scheduledEvent.MissedVisitFormReturnReason = string.Empty;//Clean up return reason if any. 
            //var userEvent = userRepository.GetEvent(Current.AgencyId, scheduledEvent.UserId, scheduledEvent.PatientId, scheduledEvent.EventId);
            //if (userEvent != null)
            //{
            //    userEvent.IsMissedVisit = true;
            //    userRepository.UpdateEvent(Current.AgencyId, userEvent);
            //}
            var existing = scheduleRepository.GetMissedVisitOnly(Current.AgencyId, scheduledEvent.Id);
            if (existing != null)
            {
                existing.Reason = missedVisit.Reason;
                existing.Comments = missedVisit.Comments;
                existing.EventDate = missedVisit.EventDate;
                existing.SignatureDate = missedVisit.SignatureDate;
                existing.SignatureText = missedVisit.SignatureText;
                existing.IsOrderGenerated = missedVisit.IsOrderGenerated;
                existing.IsPhysicianOfficeNotified = missedVisit.IsPhysicianOfficeNotified;

                if (Current.HasRight(Permissions.BypassCaseManagement))
                {
                    existing.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
                }
                else
                {
                    existing.Status = (int)ScheduleStatus.NoteMissedVisitPending;
                }
                if (scheduleRepository.UpdateMissedVisit(existing))
                {
                    if (scheduleRepository.UpdateScheduleTaskModal(scheduledEvent))
                    {
                        result = true;
                    }
                    else
                    {
                        scheduledEvent.IsMissedVisit = oldIsMissedVisit;
                        scheduledEvent.MissedVisitFormReturnReason = oldReason;
                        scheduleRepository.UpdateScheduleTaskModal(scheduledEvent);
                    }
                }
            }
            else
            {
                if (Current.HasRight(Permissions.BypassCaseManagement))
                {
                    missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitComplete;
                }
                else
                {
                    missedVisit.Status = (int)ScheduleStatus.NoteMissedVisitPending;
                }
                if (scheduleRepository.AddMissedVisit(missedVisit))
                {
                    if (scheduleRepository.UpdateScheduleTaskModal(scheduledEvent))
                    {
                        result = true;
                    }
                    else
                    {
                        scheduledEvent.IsMissedVisit = oldIsMissedVisit;
                        scheduledEvent.MissedVisitFormReturnReason = oldReason;
                        scheduleRepository.UpdateScheduleTaskModal(scheduledEvent);
                    }
                }
            }
            if (result)
            {
                if (Enum.IsDefined(typeof(DisciplineTasks), scheduledEvent.DisciplineTask))
                {
                    Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduledEvent.DisciplineTask), "Documented as a missed visit by " + Current.UserFullName);
                }
            }
            //}
            //}
            return result;
        }

        public bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                var supplies = new List<Supply>();
                if (supply.UniqueIdentifier.IsNotEmpty())
                {
                    if (supply.RevenueCode != null && supply.Description != null)
                    {
                        //int i = 0; DO Nothing
                    }
                    else
                    {
                        AgencySupply tempSupply = agencyRepository.GetSupply(Current.AgencyId, supply.UniqueIdentifier);
                        if (tempSupply != null)
                        {
                            if (tempSupply.RevenueCode != null)
                            {
                                supply.RevenueCode = tempSupply.RevenueCode;
                            }
                        }
                    }
                }
                if (supply.UniqueIdentifier.IsEmpty())
                {
                    supply.UniqueIdentifier = Guid.NewGuid();
                }
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies != null && supplies.Count > 0)
                    {
                        var existingSupply = supplies.Find(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (existingSupply != null)
                        {
                            existingSupply.Date = supply.Date;
                            existingSupply.Quantity = supply.Quantity;
                            existingSupply.Description = supply.Description;
                        }
                        else
                        {
                            supplies.Add(supply);
                        }
                    }
                    else
                    {
                        supplies = new List<Supply> { supply };
                    }
                }
                else
                {
                    supplies = new List<Supply> { supply };
                }
                patientVisitNote.Supply = supplies.ToXml();
                patientVisitNote.IsSupplyExist = true;
                if (patientRepository.UpdateVisitNote(patientVisitNote))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        var editSupply = supplies.SingleOrDefault(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (editSupply != null)
                        {
                            editSupply.Quantity = supply.Quantity;
                            editSupply.UnitCost = supply.UnitCost;
                            editSupply.Description = supply.Description;
                            editSupply.DateForEdit = supply.DateForEdit;
                            editSupply.Code = supply.Code;
                            patientVisitNote.Supply = supplies.ToXml();
                            patientVisitNote.IsSupplyExist = true;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        supplies.ForEach(s =>
                        {
                            if (s.UniqueIdentifier == supply.UniqueIdentifier)
                            {
                                supplies.Remove(s);
                            }
                        });
                        patientVisitNote.Supply = supplies.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public List<Supply> GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId)
        {
            var list = new List<Supply>();
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    list = patientVisitNote.Supply.ToObject<List<Supply>>();
                }
            }
            return list;
        }


        public PrintViewData<VisitNotePrintViewData> GetPASVisitPrint(Guid patientId, Guid episodeId, Guid eventId)
        {
            var viewData = new PrintViewData<VisitNotePrintViewData>();
            var noteViewData = new VisitNotePrintViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                noteViewData.AgencyId = patientvisitNote.AgencyId;
                viewData.PatientProfile = patientRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
                if (viewData.PatientProfile != null && !viewData.PatientProfile.AgencyLocationId.IsEmpty())
                {
                    viewData.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, viewData.PatientProfile.AgencyLocationId);
                }
                noteViewData.SignatureText = patientvisitNote.SignatureText;
                noteViewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                noteViewData.Questions = patientvisitNote.ToDictionary();
                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (scheduleEvent != null)
                {
                    noteViewData.EndDate = scheduleEvent.EndDate;
                    noteViewData.StartDate = scheduleEvent.StartDate;
                    if (!noteViewData.Questions.ContainsKey("TravelTimeIn"))
                    {
                        //var scheduleEvent = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventId == eventId).FirstOrDefault();
                        noteViewData.Questions.Add("TravelTimeIn", new NotesQuestion { Name = "TravelTimeIn", Answer = scheduleEvent.TravelTimeIn });
                        noteViewData.Questions.Add("TravelTimeOut", new NotesQuestion { Name = "TravelTimeOut", Answer = scheduleEvent.TravelTimeOut });
                    }

                    noteViewData.PatientId = patientvisitNote.PatientId;
                    noteViewData.EpisodeId = patientvisitNote.EpisodeId;
                    noteViewData.EventId = patientvisitNote.Id;
                    noteViewData.Version = patientvisitNote.Version;
                }
            }
            else noteViewData.Questions = new Dictionary<string, NotesQuestion>();
            viewData.Data = noteViewData;
            return viewData;
        }

        public PrintViewData<VisitNotePrintViewData> GetPASTravelPrint(Guid patientId, Guid episodeId, Guid eventId)
        {
            var viewData = new PrintViewData<VisitNotePrintViewData>();
            var noteViewData = new VisitNotePrintViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                noteViewData.AgencyId = patientvisitNote.AgencyId;
                viewData.PatientProfile = patientRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
                if (viewData.PatientProfile != null && !viewData.PatientProfile.AgencyLocationId.IsEmpty())
                {
                    viewData.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, viewData.PatientProfile.AgencyLocationId);
                }
                noteViewData.SignatureText = patientvisitNote.SignatureText;
                noteViewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                noteViewData.Questions = patientvisitNote.ToDictionary();
                var episode = episodeRepository.GetEpisodeLean(Current.AgencyId, patientId, episodeId);
                if (episode != null)
                {
                    noteViewData.EndDate = episode.EndDate;
                    noteViewData.StartDate = episode.StartDate;
                }
                noteViewData.PatientId = patientvisitNote.PatientId;
                noteViewData.EpisodeId = patientvisitNote.EpisodeId;
                noteViewData.EventId = patientvisitNote.Id;
                noteViewData.Version = patientvisitNote.Version;
            }
            else noteViewData.Questions = new Dictionary<string, NotesQuestion>();
            viewData.Data = noteViewData;
            return viewData;
        }


        public PrintViewData<VisitNotePrintViewData> GetPASCarePlanPrint(Guid patientId, Guid episodeId, Guid eventId)
        {
            var viewData = new PrintViewData<VisitNotePrintViewData>();
            var noteViewData = new VisitNotePrintViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                noteViewData.AgencyId = patientvisitNote.AgencyId;
                viewData.PatientProfile = patientRepository.GetPatientPrintProfile(Current.AgencyId, patientId);
                if (viewData.PatientProfile != null && !viewData.PatientProfile.AgencyLocationId.IsEmpty())
                {
                    viewData.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, viewData.PatientProfile.AgencyLocationId);
                }
                var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
                if (allergyProfile != null) noteViewData.Allergies = allergyProfile.ToString();
                noteViewData.SignatureText = patientvisitNote.SignatureText;
                noteViewData.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                noteViewData.Questions = patientvisitNote.ToDictionary();
                var episode = episodeRepository.GetEpisodeLean(Current.AgencyId, patientId, episodeId);
                if (episode != null)
                {
                    noteViewData.EndDate = episode.EndDate;
                    noteViewData.StartDate = episode.StartDate;
                }
                noteViewData.PatientId = patientvisitNote.PatientId;
                noteViewData.EpisodeId = patientvisitNote.EpisodeId;
                noteViewData.EventId = patientvisitNote.Id;
                noteViewData.Version = patientvisitNote.Version;
            }
            else noteViewData.Questions = new Dictionary<string, NotesQuestion>();
           //TODO: needs to be removed for print
            var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
            if (physician != null)
            {
                noteViewData.PhysicianId = physician.Id;
            }
            //if (patient != null && patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
            //{
            //    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
            //    if (physician != null) viewData.PhysicianId = physician.Id;
            //}
            return viewData;
        }

    }
}
