﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Web.Mvc;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.Log.Enums;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;
    using Axxess.Core.Enums;

    using Axxess.AgencyManagement.App.ViewData;

    public class UserService : IUserService
    {
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IAssetRepository assetRepository;

        public UserService(IAgencyManagementDataProvider agencyManagmentDataProvider, IMembershipDataProvider membershipDataProvider, IPatientService patientService, ILookUpDataProvider lookupDataProvider)
        {
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.patientService = patientService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.agencyRepository = agencyManagmentDataProvider.AgencyRepository;
            this.patientRepository = agencyManagmentDataProvider.PatientRepository;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.scheduleRepository = agencyManagmentDataProvider.ScheduleRepository;
            this.assetRepository = agencyManagmentDataProvider.AssetRepository;
        }

        public bool IsEmailAddressInUse(string emailAddress)
        {
            var result = false;
            var login = loginRepository.Find(emailAddress);
            if (login != null && login.IsActive)
            {
                var users = userRepository.GetUsersByLoginId(login.Id, Current.AgencyId);
                if (users.Count > 0)
                {
                    result = true;
                }
            }
            return  result;
        }

        public bool IsPasswordCorrect(Guid userId, string password)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(password, login.PasswordHash, login.PasswordSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsSignatureCorrect(Guid userId, string signature)
        {
            if (!userId.IsEmpty() && signature.IsNotNullOrEmpty())
            {
                var user = userRepository.GetUserOnly(userId, Current.AgencyId);
                if (user != null)
                {
                    var login = loginRepository.Find(user.LoginId);
                    if (login != null)
                    {
                        var saltedHash = new SaltedHash();
                        if (saltedHash.VerifyHashAndSalt(signature, login.SignatureHash, login.SignatureSalt))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool CreateUser(User user)
        {
            try
            {
                var isNewLogin = false;
                var login = loginRepository.Find(user.EmailAddress);
                if (login == null)
                {
                    login = new Login();
                    login.DisplayName = user.FirstName;
                    login.EmailAddress = user.EmailAddress;
                    login.Role = Roles.ApplicationUser.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login))
                    {
                        isNewLogin = true;
                    }
                }
                user.LoginId = login.Id;
                user.Profile.EmailWork = user.EmailAddress;
                if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0)
                {
                    user.Profile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
                }

                if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0)
                {
                    user.Profile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
                }

                if (user.FaxPhoneArray != null && user.FaxPhoneArray.Count > 0)
                {
                    user.Profile.PhoneFax = user.FaxPhoneArray.ToArray().PhoneEncode();
                }

                if (userRepository.Add(user))
                {
                    user.LocationList.ForEach(location =>
                    {
                        var plan = agencyRepository.GetSubscriptionPlan(Current.AgencyId, location);
                        if (plan != null && plan.IsUserPlan)
                        {
                            var userCount = agencyRepository.GetUserCountPerLocation(plan.AgencyId, plan.AgencyLocationId);
                            if (userCount > plan.PlanLimit)
                            {
                                var agency = agencyRepository.GetAgencyOnly(Current.AgencyId);
                                if (agency != null)
                                {
                                    var planLimit = plan.PlanLimit;
                                    var nextPlanLimit = plan.PlanLimit.NextUserSubscriptionPlan();
                                    plan.PlanLimit = nextPlanLimit;
                                    if (agencyRepository.UpdateSubscriptionPlan(plan))
                                    {
                                        ThreadPool.QueueUserWorkItem(state => SendOverageNotifications(agency.Name, agency.ContactPersonFirstName, agency.ContactPersonEmail, userCount, planLimit.UserSubscriptionPlanName(), nextPlanLimit.UserSubscriptionPlanName()));
                                    }
                                }
                            }
                        }
                    });
                    if (isNewLogin)
                    {
                        ThreadPool.QueueUserWorkItem(state => SendNewUserNotifications(user));
                    }
                    else
                    {
                        ThreadPool.QueueUserWorkItem(state => SendExistingUserNotification(user));
                    }
                    UserEngine.Refresh(Current.AgencyId);
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserAdded, string.Empty);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return false;
        }

        private static void SendNewUserNotifications(User user)
        {
            string invitationBodyText = string.Empty;
            string invitationSubject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);

            string welcomeBodyText = string.Empty;
            string welcomeSubject = string.Format("{0} - Welcome to AgencyCore, rated the Most Recommended Home Health Software Solution", user.AgencyName);

            var parameters = string.Format("id={0}&agencyid={1}", user.Id, user.AgencyId);
            var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
            invitationBodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName, "encryptedQueryString", encryptedParameters);
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, invitationSubject, invitationBodyText);

            Thread.Sleep(TimeSpan.FromMinutes(1));

            welcomeBodyText = MessageBuilder.PrepareTextFrom("NewUserWelcome");
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, welcomeSubject, welcomeBodyText);
        }

        private static void SendExistingUserNotification(User user)
        {
            string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);
            string bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName);
            Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, subject, bodyText);
        }

        private static void SendOverageNotifications(string agencyName, string adminName, string adminEmailAddress, int userCount, string previousPackage, string newPackage)
        {
            string accountBodyText = string.Empty;
            string accountSubject = string.Format("{0} - Automatic Subscription Plan Change", agencyName);

            string adminBodyText = string.Empty;
            string adminSubject = string.Format("{0} - Automatic Subscription Plan Change", agencyName);

            accountBodyText = MessageBuilder.PrepareTextFrom("AccountUserOverageNotification", "agencyname", agencyName, "usercount", userCount.ToString(), "previouspackage", previousPackage, "newpackage", newPackage);
            adminBodyText = MessageBuilder.PrepareTextFrom("AdminUserOverageNotification", "displayname", adminName, "previouspackage", previousPackage, "newpackage", newPackage);

            Notify.User(CoreSettings.NoReplyEmail, AppSettings.AccountEmailAddress, accountSubject, accountBodyText);
            Notify.User(CoreSettings.NoReplyEmail, adminEmailAddress, adminSubject, adminBodyText);
        }

        public bool DeleteUser(Guid userId)
        {
            var result = false;
            
                    if (userRepository.Delete(Current.AgencyId, userId))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserDeleted, string.Empty);
                        result = true;
                    }
            return result;
        }
        public bool RestoreUser(Guid userId)
        {
            var result = false;

            if (userRepository.Restore(Current.AgencyId, userId))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public List<UserVisitWidget> GetScheduleWidget(Guid userId)
        {
            var to = DateTime.Today.AddDays(14);
            var from = DateTime.Now.AddDays(-89);
            var status = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
            var aditionalFilter = string.Format(" AND st.Status NOT IN ( {0} )", status.Select(s => s.ToString()).ToArray().Join(","));
            var userVisits = scheduleRepository.GetScheduleWidget(Current.AgencyId, userId, from, to, 5, false, aditionalFilter);
            if (userVisits.IsNotNullOrEmpty())
            {
                var scheduledEvent = new ScheduleEvent();
                userVisits.ForEach(v =>
                {
                    scheduledEvent.PatientId = v.PatientId;
                    scheduledEvent.EpisodeId = v.EpisodeId;
                    scheduledEvent.Id = v.EventId;
                    scheduledEvent.DisciplineTask = v.DisciplineTask;
                    scheduledEvent.Status = v.Status;
                    scheduledEvent.IsMissedVisit = v.IsMissedVisit;
                    Common.Url.Set(scheduledEvent, false, false);
                    v.TaskName = scheduledEvent.Url;
                });
            }
            return userVisits.OrderBy(v => v.EventDate.ToOrderedDate()).Take(5).ToList();
        }


        //public List<UserVisitWidget> GetScheduleWidget(Guid userId)
        //{
        //    var to = DateTime.Today.AddDays(14);
        //    var from = DateTime.Now.AddDays(-89);
        //    var userVisits = new List<UserVisitWidget>();
        //    var patientEpisodes = patientRepository.GetPatientEpisodeDataSchduleWidget(Current.AgencyId, from, to);
        //    if (patientEpisodes != null && patientEpisodes.Count > 0)
        //    {
        //        patientEpisodes.ForEach(episode =>
        //        {
        //            if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
        //            {
        //                var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
        //                    s.EventId != Guid.Empty && s.UserId == userId && s.IsDeprecated == false && s.IsMissedVisit == false
        //                   && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date
        //                   && s.EventDate.ToDateTime().Date >= from.Date && s.EventDate.ToDateTime().Date <= to.Date
        //                   && !s.IsCompleted() && s.DisciplineTask != (int)DisciplineTasks.Rap && s.DisciplineTask != (int)DisciplineTasks.Final
        //                    ).ToList();

        //                if (scheduledEvents.Count > 0)
        //                {
        //                    scheduledEvents.ForEach(scheduledEvent =>
        //                    {
        //                        if (scheduledEvent != null)
        //                        {
        //                            Common.Url.Set(scheduledEvent, false, false);
        //                            userVisits.Add(new UserVisitWidget
        //                            {
        //                                PatientId = episode.PatientId,
        //                                IsDischarged = episode.IsDischarged,
        //                                PatientName = episode.PatientName,
        //                                TaskName = scheduledEvent.Url,
        //                                EventDate = scheduledEvent.EventDate.ToZeroFilled()
        //                            });
        //                        }
        //                    });
        //                }
        //            }
        //        });
        //    }

        //    return userVisits.OrderBy(v => v.EventDate.ToOrderedDate()).Take(5).ToList();
        //}


        public List<UserVisit> GetScheduleLean(Guid userId, DateTime from, DateTime to)
        {
            var userVisits = new List<UserVisit>();
            var status = ScheduleStatusFactory.OnAndAfterQAStatus(true).ToArray();
            var aditionalFilter = string.Format(" AND st.Status NOT IN ( {0} ) ", status.Select(s => s.ToString()).ToArray().Join(","));
            var scheduleEvents = scheduleRepository.GetScheduleByUserId(Current.AgencyId, userId, from, to, false, aditionalFilter);
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                var groupedEvents = scheduleEvents.GroupBy(s => s.EpisodeId).ToDictionary(g => g.First(), g => g.ToList());
                if (groupedEvents.IsNotNullOrEmpty())
                {
                    var episodeIds = groupedEvents.Select(g => g.Key.EpisodeId).Distinct().ToList();
                    var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
                    var missedVisits = scheduleRepository.GetMissedVisitsByEpisodeIds(Current.AgencyId, episodeIds) ?? new List<MissedVisit>();
                    var users = new List<UserCache>();
                    if (returnComments != null && returnComments.Count > 0)
                    {
                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (returnUserIds != null && returnUserIds.Count > 0)
                        {
                            var scheduleUsers = UserEngine.GetUsers(Current.AgencyId, returnUserIds) ?? new List<UserCache>();
                            if (scheduleUsers != null && scheduleUsers.Count > 0)
                            {
                                users.AddRange(scheduleUsers);
                            }
                        }
                    }
                    groupedEvents.ForEach((key, value) =>
                   {
                       if (value.IsNotNullOrEmpty())
                       {
                           var detail = key.EpisodeNotes.ToObject<EpisodeDetail>();
                           var episodeNotes = string.Empty;
                           if (detail != null)
                           {
                               episodeNotes = detail.Comments.Clean();
                           }
                           value.ForEach(scheduledEvent =>
                           {
                               var visitNote = string.Empty;
                               var statusComments = string.Empty;
                               //scheduledEvent.EndDate = episode.EndDate.ToDateTime();
                               //scheduledEvent.StartDate = episode.StartDate.ToDateTime();

                               if (scheduledEvent.Comments.IsNotNullOrEmpty())
                               {
                                   visitNote = scheduledEvent.Comments.Clean();
                               }
                               var eventReturnReasons = returnComments.Where(r => r.EventId == scheduledEvent.Id).ToList() ?? new List<ReturnComment>();
                               statusComments = this.GetReturnComments(scheduledEvent.ReturnReason, eventReturnReasons, users);
                               //patientService.GetReturnComments(scheduledEvent.EventId, scheduledEvent.EpisodeId, scheduledEvent.PatientId);
                               Common.Url.Set(scheduledEvent, false, false);
                               userVisits.Add(new UserVisit
                               {
                                   Id = scheduledEvent.Id,
                                   VisitNotes = visitNote,
                                   Url = scheduledEvent.Url,
                                   Status = scheduledEvent.Status.ToString(),
                                   StatusName = scheduledEvent.StatusName,
                                   PatientName = scheduledEvent.PatientName,
                                   StatusComment = statusComments,
                                   TaskName = scheduledEvent.DisciplineTaskName,
                                   EpisodeId = scheduledEvent.EpisodeId,
                                   PatientId = scheduledEvent.PatientId,
                                   EpisodeNotes = episodeNotes,
                                   VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
                                   ScheduleDate = scheduledEvent.EventDate.ToZeroFilled(),
                                   IsMissedVisit = scheduledEvent.IsMissedVisit
                               });
                           });
                       }

                   });
                }
            }
            return userVisits.OrderBy(v => v.VisitDate.ToOrderedDate()).ToList();
        }


        public List<UserVisit> GetScheduleLeanAll(Guid userId, DateTime from, DateTime to)
        {
            var userVisits = new List<UserVisit>();

            var scheduleEvents = scheduleRepository.GetScheduleByUserId(Current.AgencyId, userId, from, to, false, string.Empty);
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                var groupedEvents = scheduleEvents.GroupBy(s => s.EpisodeId).ToDictionary(g => g.First(), g => g.ToList());
                if (groupedEvents.IsNotNullOrEmpty())
                {
                    var episodeIds = groupedEvents.Select(g => g.Key.EpisodeId).Distinct().ToList();
                    var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
                    var missedVisits = scheduleRepository.GetMissedVisitsByEpisodeIds(Current.AgencyId, episodeIds) ?? new List<MissedVisit>();
                    var users = new List<UserCache>();
                    if (returnComments != null && returnComments.Count > 0)
                    {
                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
                        if (returnUserIds != null && returnUserIds.Count > 0)
                        {
                            var scheduleUsers = UserEngine.GetUsers(Current.AgencyId, returnUserIds) ?? new List<UserCache>();
                            if (scheduleUsers != null && scheduleUsers.Count > 0)
                            {
                                users.AddRange(scheduleUsers);
                            }
                        }
                    }

                    groupedEvents.ForEach((key, value) =>
                    {
                        if (value.IsNotNullOrEmpty())
                        {
                            var detail = key.EpisodeNotes.ToObject<EpisodeDetail>();
                            var episodeNotes = string.Empty;
                            if (detail != null)
                            {
                                episodeNotes = detail.Comments.Clean();
                            }
                            value.ForEach(scheduledEvent =>
                            {
                                var visitNote = string.Empty;
                                var statusComments = string.Empty;
                                //scheduledEvent.EndDate = episode.EndDate.ToDateTime();
                                //scheduledEvent.StartDate = episode.StartDate.ToDateTime();

                                if (scheduledEvent.Comments.IsNotNullOrEmpty())
                                {
                                    visitNote = scheduledEvent.Comments.Clean();
                                }
                                var eventReturnReasons = returnComments.Where(r => r.EventId == scheduledEvent.Id).ToList() ?? new List<ReturnComment>();
                                statusComments = this.GetReturnComments(scheduledEvent.ReturnReason, eventReturnReasons, users);
                                //patientService.GetReturnComments(scheduledEvent.EventId, scheduledEvent.EpisodeId, scheduledEvent.PatientId);
                                Common.Url.Set(scheduledEvent, false, false);
                                userVisits.Add(new UserVisit
                                {
                                    Id = scheduledEvent.Id,
                                    VisitNotes = visitNote,
                                    Url = scheduledEvent.Url,
                                    Status = scheduledEvent.Status.ToString(),
                                    StatusName = scheduledEvent.StatusName,
                                    PatientName = scheduledEvent.PatientName,
                                    StatusComment = statusComments,
                                    TaskName = scheduledEvent.DisciplineTaskName,
                                    EpisodeId = scheduledEvent.EpisodeId,
                                    PatientId = scheduledEvent.PatientId,
                                    EpisodeNotes = episodeNotes,
                                    VisitDate = scheduledEvent.VisitDate.ToZeroFilled(),
                                    ScheduleDate = scheduledEvent.EventDate.ToZeroFilled(),
                                    IsMissedVisit = scheduledEvent.IsMissedVisit

                                });
                            });
                        }

                    });

                }
                //if(mySchedules!=null && mySchedules.Count>0)
                //{
                //    var eventIds = mySchedules.Select(s => s.EventId).ToList();
                //    var returnComments = patientRepository.GetReturnCommentsByEventIds(Current.AgencyId, eventIds);
                //    mySchedules.ForEach(s =>
                //    {
                //        Common.Url.Set(s, false, false);
                //        var eventReturnReasons = returnComments.Where(r => r.EventId == s.EventId).ToList() ?? new List<ReturnComment>();
                //        var statusComments = this.GetReturnComments(s.ReturnReason, eventReturnReasons);
                //        userVisits.Add(new UserVisit
                //        {
                //            Id=s.EventId,
                //            VisitNotes=s.Comments.IsNotNullOrEmpty()?s.Comments.Clean():string.Empty,
                //            Url=s.Url,
                //            Status=s.Status,
                //            StatusName=s.StatusName,
                //            PatientName=s.PatientName,
                //            StatusComment = statusComments,
                //            TaskName = s.DisciplineTaskName,
                //            EpisodeId = s.EpisodeId,
                //            PatientId = s.PatientId,
                //            EpisodeNotes = s.EpisodeNotes.IsNotNullOrEmpty()?s.EpisodeNotes.ToObject<EpisodeDetail>().Comments.Clean():string.Empty,
                //            VisitDate = s.EventDate.ToZeroFilled(),
                //            ScheduleDate = s.EventDate.ToZeroFilled(),
                //            IsMissedVisit = s.IsMissedVisit,
                //            IsComplete = s.IsComplete
                //        });
                //    });
                //}
                //if (scheduledEvents != null && scheduledEvents.Count > 0)
                //{
                //    var userIds = new List<Guid>();
                //    var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episode.Id) ?? new List<ReturnComment>();
                //    if (returnComments != null && returnComments.Count > 0)
                //    {
                //        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty() && !users.Exists(us => us.Id == r.UserId)).Select(r => r.UserId).Distinct().ToList();
                //        if (returnUserIds != null && returnUserIds.Count > 0)
                //        {
                //            var scheduleUsers = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, returnUserIds) ?? new List<User>();
                //            if (scheduleUsers != null && scheduleUsers.Count > 0)
                //            {
                //                users.AddRange(scheduleUsers);
                //            }
                //        }
                //    }
                //    var eventIds = scheduledEvents.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
                //    var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();
                //    var episodeDetail = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();

                //    scheduledEvents.ForEach(scheduledEvent =>
                //    {
                //        var visitNote = string.Empty;
                //        var statusComments = string.Empty;
                //        if (scheduledEvent != null)
                //        {
                //            scheduledEvent.EndDate = episode.EndDate.ToDateTime();
                //            scheduledEvent.StartDate = episode.StartDate.ToDateTime();

                //            if (scheduledEvent.Comments.IsNotNullOrEmpty())
                //            {
                //                visitNote = scheduledEvent.Comments.Clean();
                //            }
                //            var eventReturnReasons = returnComments.Where(r => r.EventId == scheduledEvent.EventId).ToList() ?? new List<ReturnComment>();
                //            statusComments = this.GetReturnComments(scheduledEvent.ReturnReason, eventReturnReasons, users);
                //            //patientService.GetReturnComments(scheduledEvent.EventId, scheduledEvent.EpisodeId, scheduledEvent.PatientId);
                //            Common.Url.Set(scheduledEvent, false, false);
                //            userVisits.Add(new UserVisit
                //            {
                //                Id = scheduledEvent.EventId,
                //                VisitNotes = visitNote,
                //                Url = scheduledEvent.Url,
                //                Status = scheduledEvent.Status,
                //                StatusName = scheduledEvent.StatusName,
                //                PatientName = episode.PatientName,
                //                StatusComment = statusComments,
                //                TaskName = scheduledEvent.DisciplineTaskName,
                //                EpisodeId = scheduledEvent.EpisodeId,
                //                PatientId = scheduledEvent.PatientId,
                //                EpisodeNotes = episodeDetail.Comments.Clean(),
                //                VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
                //                ScheduleDate = scheduledEvent.EventDate.ToZeroFilled(),
                //                IsMissedVisit = scheduledEvent.IsMissedVisit
                //            });
                //        }
                //    });
                //}
            }
            return userVisits;
        }


        //public List<UserVisit> GetScheduleLean(Guid userId, DateTime from, DateTime to)
        //{
        //    var userVisits = new List<UserVisit>();
        //    var patientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, from, to);
        //    if (patientEpisodes != null && patientEpisodes.Count > 0)
        //    {
        //        var users = new List<User>();
        //        patientEpisodes.ForEach(episode =>
        //        {
        //            if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
        //            {
        //                var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
        //                    s.EventId != Guid.Empty && s.UserId == userId && s.IsDeprecated == false && s.IsMissedVisit == false
        //                   && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date
        //                   && s.EventDate.ToDateTime().Date >= from.Date && s.EventDate.ToDateTime().Date <= to.Date
        //                   && !s.IsCompleted() && s.DisciplineTask != (int)DisciplineTasks.Rap && s.DisciplineTask != (int)DisciplineTasks.Final
        //                    ).ToList();

        //                if (scheduledEvents!=null && scheduledEvents.Count > 0)
        //                {
        //                    var userIds = new List<Guid>();
        //                    var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episode.Id) ?? new List<ReturnComment>();
        //                    if (returnComments != null && returnComments.Count > 0)
        //                    {
        //                        var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()&& !users.Exists(us => us.Id == r.UserId)).Select(r => r.UserId).Distinct().ToList();
        //                        if (returnUserIds != null && returnUserIds.Count > 0)
        //                        {
        //                            var scheduleUsers = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, returnUserIds) ?? new List<User>();
        //                            if (scheduleUsers != null && scheduleUsers.Count > 0)
        //                            {
        //                                users.AddRange(scheduleUsers);
        //                            }
        //                        }
        //                    }
        //                    var eventIds = scheduledEvents.Where(s => !s.EventId.IsEmpty() && s.IsMissedVisit).Select(s => s.EventId).Distinct().ToList();
        //                    var missedVisits = patientRepository.GetMissedVisitsByIds(Current.AgencyId, eventIds) ?? new List<MissedVisit>();
        //                    var episodeDetail = episode.Details.IsNotNullOrEmpty() ? episode.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();

        //                    scheduledEvents.ForEach(scheduledEvent =>
        //                    {
        //                        var visitNote = string.Empty;
        //                        var statusComments = string.Empty;
        //                        if (scheduledEvent != null )
        //                        {
        //                            scheduledEvent.EndDate = episode.EndDate.ToDateTime();
        //                            scheduledEvent.StartDate = episode.StartDate.ToDateTime();
                                  
        //                            if (scheduledEvent.Comments.IsNotNullOrEmpty())
        //                            {
        //                                visitNote = scheduledEvent.Comments.Clean();
        //                            }
        //                            var eventReturnReasons = returnComments.Where(r => r.EventId == scheduledEvent.EventId).ToList() ?? new List<ReturnComment>();
        //                            statusComments = this.GetReturnComments(scheduledEvent.ReturnReason, eventReturnReasons, users);
        //                             //patientService.GetReturnComments(scheduledEvent.EventId, scheduledEvent.EpisodeId, scheduledEvent.PatientId);
        //                            Common.Url.Set(scheduledEvent, false, false);
        //                            userVisits.Add(new UserVisit
        //                            {
        //                                Id = scheduledEvent.EventId,
        //                                VisitNotes = visitNote,
        //                                Url = scheduledEvent.Url,
        //                                Status = scheduledEvent.Status,
        //                                StatusName = scheduledEvent.StatusName,
        //                                PatientName = episode.PatientName,
        //                                StatusComment = statusComments,
        //                                TaskName = scheduledEvent.DisciplineTaskName,
        //                                EpisodeId = scheduledEvent.EpisodeId,
        //                                PatientId = scheduledEvent.PatientId,
        //                                EpisodeNotes = episodeDetail.Comments.Clean(),
        //                                VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
        //                                ScheduleDate = scheduledEvent.EventDate.ToZeroFilled(),
        //                                IsMissedVisit = scheduledEvent.IsMissedVisit
        //                            });
        //                        }
        //                    });
        //                }
        //            }
        //        });
        //    }

        //    return userVisits.OrderBy(v => v.VisitDate.ToOrderedDate()).ToList();
        //}


        public User GetUserForEditPermissionAndRole(Guid userId)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                if (user.Permissions.IsNotNullOrEmpty())
                {
                    user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                }
            }
            else
            {
                user = new User();
            }
            return user;
        }

        public User GetUserForEditInformation(Guid Id)
        {
            var user = userRepository.GetUserOnly(Id, Current.AgencyId);
            if (user != null)
            {
                if (user.ProfileData.IsNotNullOrEmpty())
                {
                    user.Profile = user.ProfileData.ToObject<UserProfile>();
                }
                user.LocationList = userRepository.GetUserBranches(Current.AgencyId, Id);
            }
            return user;
        }


        public User GetUserProfileDisplayName(Guid userId)
        {
            var user = userRepository.GetUserProfileDisplayName(Current.AgencyId, userId);
            if (user != null)
            {
                if (user.ProfileData.IsNotNullOrEmpty())
                {
                    user.Profile = user.ProfileData.ToObject<UserProfile>();
                    user.ProfileData = "";
                }
                else
                {
                    user.Profile = new UserProfile();
                }
            }
            return user;
        }

        public User GetForEdit(Guid id, Guid agencyId, string category)
        {
            var user = userRepository.GetUserOnly(id, agencyId);
            if (user != null)
            {
                if (category.IsEqual("Information"))
                {
                    if (user.ProfileData.IsNotNullOrEmpty())
                    {
                        user.Profile = user.ProfileData.ToObject<UserProfile>();
                    }
                    user.LocationList = userRepository.GetUserBranches(agencyId, id);
                }
                else if (category.IsEqual("Permissions"))
                {
                    if (user.Permissions.IsNotNullOrEmpty())
                    {
                        user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                    }
                }
                else if (category.IsEqual("Licenses"))
                {
                    if (user.Licenses.IsNotNullOrEmpty())
                    {
                        user.LicensesArray = user.Licenses.ToObject<List<License>>();
                        if (user.LicensesArray.IsNotNullOrEmpty())
                        {
                            var assetIds = user.LicensesArray.Where(l => !l.AssetId.IsEmpty()).Select(l => l.AssetId).Distinct().ToList();
                            if (assetIds.IsNotNullOrEmpty())
                            {
                                var assests = assetRepository.GetAssetLean(user.AgencyId, assetIds);
                                if (assests.IsNotNullOrEmpty())
                                {
                                    user.LicensesArray.ForEach(license =>
                                    {
                                        if (!license.AssetId.IsEmpty())
                                        {
                                            var asset = assests.First(a => a.Id == license.AssetId);
                                            if (asset != null)
                                            {
                                                license.AssetUrl = string.Format("<a href=\"/Asset/{0}\">{1}</a>&#160;", asset.Id.ToString(), asset.FileName);
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                    else
                    {
                        user.LicensesArray = new List<License>();
                    }
                }
                else if (category.IsEqual("PayRates"))
                {

                    if (user.Rates.IsNotNullOrEmpty())
                    {
                        foreach (UserRate u in user.RatesArray)
                        {
                            if (u.Insurance.IsNotNullOrEmpty())
                            {
                                if (u.Insurance.ToInteger() < 1000)
                                {
                                    var insurance = lookupRepository.GetInsurance(u.Insurance.ToInteger());
                                    if (insurance != null)
                                    {
                                        u.InsuranceName = insurance.Name;
                                    }
                                }
                                else
                                {
                                    var insurance = agencyRepository.GetInsurance(u.Insurance.ToInteger(), agencyId);
                                    if (insurance != null)
                                    {
                                        u.InsuranceName = insurance.Name;
                                    }
                                }
                            }
                        }
                    }
                    if (user.NonVisitRatesArray != null && user.NonVisitRatesArray.Count > 0)
                    {
                        foreach (UserNonVisitTaskRate item in user.NonVisitRatesArray)
                        {
                            if (item != null)
                            {
                                if (item.Id != null)
                                {
                                    var agencyNonVisit = agencyRepository.GetNonVisitTask(Current.AgencyId, item.Id);
                                    if (agencyNonVisit != null)
                                    {
                                        item.TaskTitle = agencyNonVisit.Title;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        user.NonVisitRatesArray = new List<UserNonVisitTaskRate>();
                    }
                }

            }
            return user;
        }

        public List<UserNonVisitTaskRate> GetUserNonVisitTaskRates(Guid AgencyId, Guid TaskId, Guid UserId) 
        {
            var nonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, UserId);
            List<UserNonVisitTaskRate> result = null;
            if (nonVisitRate != null)
            {
                result = nonVisitRate.Rate.ToObject<List<UserNonVisitTaskRate>>();
            }
            if (result == null)
            {
                result = new List<UserNonVisitTaskRate>();
            }
            else
            {
                foreach (UserNonVisitTaskRate item in result)
                {
                    if (item != null)
                    {
                        if (item.Id != null)
                        {
                            var agencyNonVisit = agencyRepository.GetNonVisitTask(Current.AgencyId, item.Id);
                            if (agencyNonVisit != null)
                            {
                                item.TaskTitle = agencyNonVisit.Title;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public UserNonVisitTaskRate GetUserNonVisitTaskRate(Guid AgencyId, Guid TaskId, Guid UserId)
        {
            var nonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, UserId);
            List<UserNonVisitTaskRate> userNonVisitTaskRateList = null;
            UserNonVisitTaskRate result = null;
            if (nonVisitRate != null)
            {
                userNonVisitTaskRateList = nonVisitRate.Rate.ToObject<List<UserNonVisitTaskRate>>();
            }
            if (userNonVisitTaskRateList == null)
            {
                userNonVisitTaskRateList = new List<UserNonVisitTaskRate>();
            }
            else 
            {
                var tempResult = userNonVisitTaskRateList.Find(c => c.Id == TaskId);
                if (tempResult != null) 
                {
                    var agencyNonVisit = agencyRepository.GetNonVisitTask(Current.AgencyId, tempResult.Id);
                    if (agencyNonVisit != null)
                    {
                        tempResult.TaskTitle = agencyNonVisit.Title;
                    }
                    result = tempResult;
                }
            }
            return result;
        }


        public User GetUserOnly(Guid id, Guid agencyId) 
        {
            var user = userRepository.GetUserOnly(id, agencyId);
            if (user != null)
            {
                return user;
            }
            else 
            {
                user = new User();
                return user;
            }
        }

        private string GetReturnComments(string scheduleCommentString, List<ReturnComment> newComments, List<UserCache> users)
        {
            //string CommentString = patientRepository.GetReturnReason(eventId, episodeId, patientId, Current.AgencyId);
            //List<ReturnComment> NewComments = patientRepository.GetReturnComments(Current.AgencyId, episodeId, eventId);
            foreach (ReturnComment comment in newComments)
            {
                if (comment.IsDeprecated) continue;
                if (scheduleCommentString.IsNotNullOrEmpty())
                {
                    scheduleCommentString += "<hr/>";
                }
                if (comment.UserId == Current.UserId)
                {
                    scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                }
                var userName = string.Empty;
                if (!comment.UserId.IsEmpty())
                {
                    var user = users.FirstOrDefault(u => u.Id == comment.UserId);
                    if (user != null)
                    {
                        userName = user.DisplayName;
                    }
                }
                scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", userName, comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return scheduleCommentString;
        }

        private string GetReturnComments(string scheduleCommentString, List<ReturnComment> newComments)
        {
            foreach (ReturnComment comment in newComments)
            {
                if (scheduleCommentString.IsNotNullOrEmpty())
                {
                    scheduleCommentString += "<hr/>";
                }
                if (comment.UserId == Current.UserId)
                {
                    scheduleCommentString += string.Format("<span class='edit-controls'>{0}</span>", comment.Id);
                }
                
                scheduleCommentString += string.Format("<span class='user'>{0}</span><span class='time'>{1}</span><span class='reason'>{2}</span>", comment.UserName, comment.Modified.ToString("g"), comment.Comments.Clean());
            }
            return scheduleCommentString;
        }

     


        //public List<UserVisit> GetScheduleLeanAll(Guid userId, DateTime from, DateTime to)
        //{
        //    var userEvents = new List<UserEvent>();
        //    var userVisits = new List<UserVisit>();
        //    var userSchedules = userRepository.GetScheduleLean(Current.AgencyId, userId);
        //    userSchedules.ForEach(s =>
        //    {
        //        if (s.Visits.IsNotNullOrEmpty())
        //        {
        //            var visits = s.Visits.ToObject<List<UserEvent>>();
        //            visits.ForEach(v =>
        //            {
        //                if (v.EventDate.IsValidDate())
        //                {
        //                    var date = v.EventDate.ToDateTime();
        //                    if (date >= from && date <= to
        //                       && v.IsDeprecated == false
        //                       && v.IsMissedVisit == false
        //                       && v.DisciplineTask != (int)DisciplineTasks.Rap
        //                       && v.DisciplineTask != (int)DisciplineTasks.Final)
        //                    {
        //                        v.PatientName = s.PatientName;
        //                        v.EpisodeDetails = s.EpisodeDetails;
        //                        v.EpisodeSchedule = s.EpisodeSchedule;
        //                        v.EpisodeEndDate = s.EpisodeEndDate;
        //                        v.EpisodeStartDate = s.EpisodeStartDate;
        //                        userEvents.Add(v);
        //                    }
        //                }
        //            });
        //        }
        //    });

        //    var orderedList = userEvents.OrderBy(e => e.EventDate.ToOrderedDate());
        //    if (orderedList != null && orderedList.Count() > 0)
        //    {
        //        var episodeIds = orderedList.Where(r => !r.EpisodeId.IsEmpty()).Select(r => r.EpisodeId).Distinct().ToList();
        //        var returnComments = patientRepository.GetALLEpisodeReturnCommentsByIds(Current.AgencyId, episodeIds) ?? new List<ReturnComment>();
        //        var userIds = new List<Guid>();
        //        var users = new List<User>();
        //        if (returnComments != null && returnComments.Count > 0)
        //        {
        //            var returnUserIds = returnComments.Where(r => !r.UserId.IsEmpty()).Select(r => r.UserId).Distinct().ToList();
        //            if (returnUserIds != null && returnUserIds.Count > 0)
        //            {
        //                userIds.AddRange(returnUserIds);
        //            }
        //        }

        //        if (userIds != null && userIds.Count > 0)
        //        {
        //            var scheduleUsers = userRepository.GetUsersWithCredentialsByIds(Current.AgencyId, userIds) ?? new List<User>();
        //            if (scheduleUsers != null && scheduleUsers.Count > 0)
        //            {
        //                users.AddRange(scheduleUsers);
        //            }
        //        }
        //        foreach (UserEvent e in orderedList)
        //        {

        //            var visitNote = string.Empty;
        //            var statusComments = string.Empty;
        //            if (!e.EventId.IsEmpty() && e.EpisodeSchedule.IsNotNullOrEmpty())
        //            {
        //                if (!userVisits.Exists(v => v.Id == e.EventId))
        //                {
        //                    var episodeEvents = e.EpisodeSchedule.ToObject<List<ScheduleEvent>>();
        //                    if (episodeEvents != null && episodeEvents.Count > 0)
        //                    {
        //                        var scheduledEvent = episodeEvents.FirstOrDefault(se => se.EventId == e.EventId);
        //                        if (scheduledEvent != null)
        //                        {
        //                            scheduledEvent.EndDate = e.EpisodeEndDate;
        //                            scheduledEvent.StartDate = e.EpisodeStartDate;
        //                            if (!scheduledEvent.IsDeprecated
        //                               && scheduledEvent.EventDate.IsValidDate() && (scheduledEvent.EventDate.ToDateTime() >= scheduledEvent.StartDate) && (scheduledEvent.EventDate.ToDateTime() <= scheduledEvent.EndDate))
        //                            {
        //                                var episodeDetail = e.EpisodeDetails.IsNotNullOrEmpty() ? e.EpisodeDetails.ToObject<EpisodeDetail>() : new EpisodeDetail();
        //                                if (scheduledEvent.Comments.IsNotNullOrEmpty())
        //                                {
        //                                    visitNote = scheduledEvent.Comments.Clean();
        //                                }
        //                                var eventReturnReasons = returnComments.Where(r => r.EpisodeId == scheduledEvent.EpisodeId && r.EventId == scheduledEvent.EventId).ToList() ?? new List<ReturnComment>();
        //                                statusComments = this.GetReturnComments(scheduledEvent.ReturnReason, eventReturnReasons, users);
        //                                //if (scheduledEvent.StatusComment.IsNotNullOrEmpty())
        //                                //{
        //                                //    statusComments = scheduledEvent.StatusComment;
        //                                //}

        //                                Common.Url.Set(scheduledEvent, false, false);
        //                                userVisits.Add(new UserVisit
        //                                {
        //                                    Id = scheduledEvent.EventId,
        //                                    VisitNotes = visitNote,
        //                                    Url = scheduledEvent.Url,
        //                                    Status = scheduledEvent.Status,
        //                                    StatusName = scheduledEvent.StatusName,
        //                                    PatientName = e.PatientName,
        //                                    StatusComment = statusComments,
        //                                    TaskName = scheduledEvent.DisciplineTaskName,
        //                                    EpisodeId = scheduledEvent.EpisodeId,
        //                                    PatientId = scheduledEvent.PatientId,
        //                                    EpisodeNotes = episodeDetail.Comments.Clean(),
        //                                    VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
        //                                    ScheduleDate = scheduledEvent.EventDate.ToZeroFilled(),
        //                                    IsMissedVisit = scheduledEvent.IsMissedVisit

        //                                });
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return userVisits;
        //}


        //public List<UserVisit> GetCompletedVisits(Guid userId, DateTime start, DateTime end)
        //{
        //    var userEvents = new List<UserEvent>();
        //    var userVisits = new List<UserVisit>();
        //    var userSchedules = userRepository.GetScheduleLean(Current.AgencyId, userId, start, end);
        //    userSchedules.ForEach(s =>
        //    {
        //        if (s.Visits.IsNotNullOrEmpty())
        //        {
        //            var visits = s.Visits.ToObject<List<UserEvent>>();
        //            visits.ForEach(v =>
        //            {
        //                var date = v.EventDate.IsNotNullOrEmpty() ? v.EventDate.ToDateTime() : DateTime.MinValue;
        //                if (date != DateTime.MinValue)
        //                {
        //                    if (date > start && date < end
        //                       && v.IsDeprecated == false
        //                       && v.IsMissedVisit == false
        //                       && v.DisciplineTask != (int)DisciplineTasks.Rap
        //                       && v.DisciplineTask != (int)DisciplineTasks.Final)
        //                    {
        //                        v.PatientName = s.PatientName;
        //                        v.EpisodeDetails = s.EpisodeDetails;
        //                        v.EpisodeSchedule = s.EpisodeSchedule;
        //                        v.EpisodeEndDate = s.EpisodeEndDate;
        //                        v.EpisodeStartDate = s.EpisodeStartDate;
        //                        userEvents.Add(v);
        //                    }
        //                }
        //            });
        //        }
        //    });

        ////    var orderedList = userEvents.OrderBy(e => e.EventDate.ToOrderedDate());
        ////    foreach (UserEvent e in orderedList)
        ////    {
        ////        var visitNote = string.Empty;
        ////        var statusComments = string.Empty;
        ////        if (!e.EventId.IsEmpty() && e.EpisodeSchedule.IsNotNullOrEmpty())
        ////        {
        ////            if (!userVisits.Exists(v => v.Id == e.EventId))
        ////            {
        ////                var episodeEvents = e.EpisodeSchedule.ToObject<List<ScheduleEvent>>();
        ////                if (episodeEvents != null && episodeEvents.Count > 0)
        ////                {
        ////                    var scheduledEvent = episodeEvents.FirstOrDefault(se => se.EventId == e.EventId);
        ////                    if (scheduledEvent != null && scheduledEvent.IsCompleted() && !scheduledEvent.IsDeprecated && !scheduledEvent.IsMissedVisit)
        ////                    {
        ////                        scheduledEvent.EndDate = e.EpisodeEndDate;
        ////                        scheduledEvent.StartDate = e.EpisodeStartDate;
        ////                        if (DateTime.Parse(scheduledEvent.EventDate) >= scheduledEvent.StartDate && DateTime.Parse(scheduledEvent.EventDate) <= scheduledEvent.EndDate)
        ////                        {
        ////                            if (scheduledEvent.Comments.IsNotNullOrEmpty())
        ////                            {
        ////                                visitNote = scheduledEvent.Comments.Clean();
        ////                            }
        ////                            if (scheduledEvent.StatusComment.IsNotNullOrEmpty())
        ////                            {
        ////                                statusComments = scheduledEvent.StatusComment;
        ////                            }

        //                            Common.Url.Set(scheduledEvent, false, false);
        //                            userVisits.Add(new UserVisit
        //                            {
        //                                Id = scheduledEvent.EventId,
        //                                UserId = userId,
        //                                VisitRate = "$0.00",
        //                                Url = scheduledEvent.Url,
        //                                UserDisplayName = UserEngine.GetName(userId, Current.AgencyId),
        //                                Status = scheduledEvent.Status,
        //                                Surcharge = scheduledEvent.Surcharge,

        //                                StatusName = scheduledEvent.StatusName,
        //                                PatientName = e.PatientName,
        //                                StatusComment = statusComments,
        //                                DisciplineTask=scheduledEvent.DisciplineTask,
        //                                EpisodeId = scheduledEvent.EpisodeId,
        //                                PatientId = scheduledEvent.PatientId,
        //                                VisitDate = scheduledEvent.EventDate.ToZeroFilled(),
        //                                IsMissedVisit = scheduledEvent.IsMissedVisit

        ////                            });
        ////                        }
        ////                    }
        ////                }
        ////            }
        ////        }
        ////    }
        ////    return userVisits;
        ////}
                                    
        //}

        public bool UpdateProfile(User user)
        {
            var result = false;
            user.AgencyId = Current.AgencyId;

            var userToEdit = userRepository.GetUserOnly(user.Id, Current.AgencyId);
            if (userToEdit != null)
            {
                if (userToEdit.ProfileData.IsNotNullOrEmpty())
                {
                    userToEdit.Profile = userToEdit.ProfileData.ToObject<UserProfile>();

                    userToEdit.Profile.AddressLine1 = user.Profile.AddressLine1;
                    userToEdit.Profile.AddressLine2 = user.Profile.AddressLine2;
                    userToEdit.Profile.AddressCity = user.Profile.AddressCity;
                    userToEdit.Profile.AddressZipCode = user.Profile.AddressZipCode;
                    userToEdit.Profile.AddressStateCode = user.Profile.AddressStateCode;

                    if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0)
                    {
                        userToEdit.Profile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
                    }
                    if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0)
                    {
                        userToEdit.Profile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
                    }

                    userToEdit.ProfileData = userToEdit.Profile.ToXml();
                    userToEdit.Modified = DateTime.Now;

                    if (userRepository.UpdateModel(userToEdit))
                    {
                        result = true;
                        if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty() || user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                        {
                            Login login = loginRepository.Find(userToEdit.LoginId);
                            if (login != null)
                            {
                                if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty())
                                {
                                    string passwordsalt = string.Empty;
                                    string passwordHash = string.Empty;

                                    var saltedHash = new SaltedHash();
                                    saltedHash.GetHashAndSalt(user.PasswordChanger.NewPassword, out passwordHash, out passwordsalt);
                                    login.PasswordSalt = passwordsalt;
                                    login.PasswordHash = passwordHash;
                                }

                                if (user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                                {
                                    string signaturesalt = string.Empty;
                                    string signatureHash = string.Empty;

                                    var saltedHash = new SaltedHash();
                                    saltedHash.GetHashAndSalt(user.SignatureChanger.NewSignature, out signatureHash, out signaturesalt);
                                    login.SignatureSalt = signaturesalt;
                                    login.SignatureHash = signatureHash;
                                }

                                if (loginRepository.Update(login))
                                {
                                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserProfileUpdated, string.Empty);
                                    
                                }
                                else
                                {
                                    result = false;
                                }
                            }
                        }

                    }
                }
            }

            return result;
        }

        public bool AddLicense(License license, System.Web.HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var isAssetSaved = true;
            var user = userRepository.GetUserOnly(license.UserId, Current.AgencyId);
            if (user != null)
            {
                if (httpFiles.Count > 0)
                {
                    foreach (string key in httpFiles.AllKeys)
                    {
                        HttpPostedFileBase file = httpFiles.Get(key);
                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                        {
                            using (var binaryReader = new BinaryReader(file.InputStream))
                            {
                                var asset = new Asset
                                {
                                    FileName = file.FileName,
                                    AgencyId = Current.AgencyId,
                                    ContentType = file.ContentType,
                                    FileSize = file.ContentLength.ToString(),
                                    Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                };
                                if (assetRepository.Add(asset))
                                {
                                    license.AssetId = asset.Id;
                                }
                                else
                                {
                                    isAssetSaved = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (isAssetSaved)
                {
                    license.Id = Guid.NewGuid();
                    license.Created = DateTime.Now;
                    if (license.OtherLicenseType.IsNotNullOrEmpty())
                    {
                        license.LicenseType = license.OtherLicenseType;
                    }
                    user.LicensesArray = user.Licenses.IsNotNullOrEmpty()? user.Licenses.ToObject<List<License>>() : new List<License>();
                    if (user.LicensesArray.IsNotNullOrEmpty())
                    {
                        user.LicensesArray.Add(license);
                        user.Licenses = user.LicensesArray.ToXml();
                    }
                    else
                    {
                        user.LicensesArray = new List<License>();
                        user.LicensesArray.Add(license);
                        user.Licenses = user.LicensesArray.ToXml();
                    }
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateLicense(License license)
        {
            if (license != null)
            {
                var user = userRepository.GetUserOnly(license.UserId, Current.AgencyId);
                if (user != null)
                {
                    if (user.Licenses.IsNotNullOrEmpty())
                    {
                        user.LicensesArray = user.Licenses.ToObject<List<License>>();
                        if (user.LicensesArray.IsNotNullOrEmpty())
                        {
                            var existingLicense = user.LicensesArray.Find(l => l.Id == license.Id);

                            if (license.OtherLicenseType.IsNullOrEmpty())
                            {
                                existingLicense.LicenseType = license.LicenseType;
                            }
                            else
                            {
                                existingLicense.LicenseType = license.OtherLicenseType;
                            }
                            existingLicense.LicenseNumber = license.LicenseNumber;
                            existingLicense.InitiationDate = license.InitiationDate;
                            existingLicense.ExpirationDate = license.ExpirationDate;
                            user.Licenses = user.LicensesArray.ToXml();
                            if (userRepository.UpdateModel(user))
                            {
                                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
      
        public bool UpdateLicense(Guid id, Guid userId, DateTime expirationDate, string LicenseNumber)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                if (user.Licenses.IsNotNullOrEmpty())
                {
                    user.LicensesArray = user.Licenses.ToObject<List<License>>();
                    if (user.LicensesArray.IsNotNullOrEmpty())
                    {
                        var license = user.LicensesArray.Find(l => l.Id == id);
                        if (license != null)
                        {
                            license.ExpirationDate = expirationDate;
                            license.LicenseNumber = LicenseNumber;
                            user.Licenses = user.LicensesArray.ToXml();
                            if (userRepository.UpdateModel(user))
                            {
                                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool UpdateLicense(Guid id, Guid userId, DateTime initiationDate, DateTime expirationDate)
        {
            var user = userRepository.GetUserOnly(userId, Current.AgencyId);
            if (user != null)
            {
                if (user.Licenses.IsNotNullOrEmpty())
                {
                     user.LicensesArray = user.Licenses.ToObject<List<License>>();
                     if (user.LicensesArray.IsNotNullOrEmpty())
                     {
                         var license = user.LicensesArray.Find(l => l.Id == id);
                         if (license != null)
                         {
                             license.InitiationDate = initiationDate;
                             license.ExpirationDate = expirationDate;
                             user.Licenses = user.LicensesArray.ToXml();
                             if (userRepository.UpdateModel(user))
                             {
                                 Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                                 return true;
                             }
                         }
                     }
                }
            }
            return false;
        }

        public bool DeleteLicense(Guid id, Guid userId)
        {
            var agencyId = Current.AgencyId;
            var user = userRepository.GetUserOnly(userId, agencyId);
            if (user != null)
            {
                if (user.Licenses.IsNotNullOrEmpty())
                {
                    user.LicensesArray = user.Licenses.ToObject<List<License>>();
                    if (user.LicensesArray.IsNotNullOrEmpty())
                    {
                        var license = user.LicensesArray.Find(l => l.Id == id);
                        if (license != null)
                        {
                            if (!license.AssetId.IsEmpty())
                            {
                                assetRepository.Delete(license.AssetId, agencyId);
                            }
                            user.LicensesArray.RemoveAll(l => l.Id == id);
                            user.Licenses = user.LicensesArray.ToXml();
                            if (userRepository.UpdateModel(user))
                            {
                                Auditor.AddGeneralLog(LogDomain.Agency, agencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseDeleted, string.Empty);
                                return true;
                            }

                        }
                    }
                }
            }
            else
            {
                return userRepository.DeleteNonUserLicense(id, Current.AgencyId);
            }
            return false;
        }

        public bool UpdatePermissions(FormCollection formCollection)
        {
            var result = false;
            var userId = formCollection["UserId"] != null ? formCollection["UserId"].ToGuid() : Guid.Empty;
            var permissionArray = formCollection["PermissionsArray"] != null ? formCollection["PermissionsArray"].ToArray().ToList() : null;
            if (permissionArray.IsNotNullOrEmpty())
            {
                var user = userRepository.GetUserOnly(userId, Current.AgencyId);
                if (user != null)
                {
                   
                    user.PermissionsArray = permissionArray;
                    if (user.PermissionsArray.IsNotNullOrEmpty())
                    {
                        user.Permissions = user.PermissionsArray.ToXml();
                    }
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public IList<User> GetUserByBranchAndStatus(Guid branchId, int status)
        {
            return userRepository.GetUserLeanForHtml(Current.AgencyId, branchId.IsEmpty() ? Current.LocationIds : new List<Guid> { branchId }, status > 0 ? new List<int> { status } : new List<int> { (int)UserStatus.Active, (int)UserStatus.Inactive });
            //var users = new List<User>();
            //if (status == 0)
            //{
            //    if (branchId.IsEmpty())
            //    {
            //        users = userRepository.GetUsersOnly(Current.AgencyId).ToList();
            //    }
            //    else
            //    {
            //        users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId).ToList();
            //    }
            //}
            //else
            //{
            //    if (branchId.IsEmpty())
            //    {
            //        users = userRepository.GetUsersByStatus(Current.AgencyId,status).ToList();
            //    }
            //    else
            //    {
            //        users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId,status).ToList();
            //    }

            //}
            //return users.OrderBy(u => u.FirstName).ToList();
        }

        public IList<LicenseItem> GetUserLicenses()
        {
            var list = new List<LicenseItem>();

            var nonuserLicenses = userRepository.GetNonUserLicenses(Current.AgencyId);
            if (nonuserLicenses != null && nonuserLicenses.Count > 0)
            {
                nonuserLicenses.ForEach(license =>
                {
                    list.Add(license);
                });
            }

            var userLicenses = userRepository.GetSoftwareUserLicenses(Current.AgencyId);
            if (userLicenses != null && userLicenses.Count > 0)
            {
                userLicenses.ForEach(license =>
                {
                    license.DisplayName = UserEngine.GetName(license.UserId, license.AgencyId);
                    list.Add(license);
                });
            }
            return list.OrderBy(l => l.FirstName).ToList();
        }

        public string GetUserLicensesExpirationNotification(Guid userId, string userName)
        {
            string result = "";
            var userLicense = userRepository.GetUserLicensesOnly(Current.AgencyId, userId);
            if (userLicense.IsNotNullOrEmpty())
            {
                var licensesArray = userLicense.ToObject<List<License>>();
                //var licensesLists = userRepository.GetUserLicenses(Current.AgencyId, userId, false);
                if (licensesArray.IsNotNullOrEmpty())
                {
                    foreach (License l in licensesArray)
                    {
                        if (l.ExpirationDate < DateTime.Now)
                        {
                            result += l.LicenseType + " of " + userName + " has expired.</br>";
                        }
                        else if (l.ExpirationDate >= DateTime.Now && l.ExpirationDate <= DateTime.Now.AddDays(30))
                        {
                            result += l.LicenseType + " of " + userName + " will be expired in 30 days.</br>";
                        }
                    }
                }
            }
            return result;
        }

        public License GetUserLicense(Guid userId, Guid id)
        {
            var userLicense = userRepository.GetUserLicensesOnly(Current.AgencyId, userId);
            if (userLicense.IsNotNullOrEmpty())
            {
                 var licensesArray = userLicense.ToObject<List<License>>();
                 if (licensesArray.IsNotNullOrEmpty())
                 {
                     return licensesArray.First(l => l.Id == id);
                 }

            }
            return new License();
        }

        public IList<License> GetUserLicenses(Guid userId, bool isAssetUrlNeeded)
        {
            var agencyId = Current.AgencyId;
            var userLicense = userRepository.GetUserLicensesOnly(agencyId, userId);
            if (userLicense.IsNotNullOrEmpty())
            {
                var licensesArray = userLicense.ToObject<List<License>>();
                if (licensesArray.IsNotNullOrEmpty())
                {
                    if (isAssetUrlNeeded)
                    {
                        var assetIds = licensesArray.Where(l => !l.AssetId.IsEmpty()).Select(l => l.AssetId).Distinct().ToList();
                        if (assetIds.IsNotNullOrEmpty())
                        {
                            var assets = assetRepository.GetAssetLean(Current.AgencyId, assetIds);
                            if (assets.IsNotNullOrEmpty())
                            {
                                licensesArray.ForEach(license =>
                                {
                                    if (!license.AssetId.IsEmpty())
                                    {
                                        var asset = assets.FirstOrDefault(a => a.Id == license.AssetId);
                                        if (asset != null)
                                        {
                                            license.AssetUrl = string.Format("<a href=\"/Asset/{0}\">{1}</a>&#160;", asset.Id.ToString(), asset.FileName);
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
                return licensesArray ?? new List<License>();
            }
            return new List<License>();
        }

        public IList<License> GetUserLicenses(Guid branchId, int status)
        {
            var list = new List<License>();

            var users = userRepository.GetUsersByStatusForLicenses(Current.AgencyId, branchId.IsEmpty() ? Current.LocationIds : new List<Guid> { branchId }, status > 0 ? new List<int> { status } : new List<int> { (int)UserStatus.Active, (int)UserStatus.Inactive });
            if (users.IsNotNullOrEmpty())
            {
                var i = 0;
                users.ForEach(u =>
                {
                    if (u.Licenses.IsNotNullOrEmpty())
                    {
                        u.LicensesArray = u.Licenses.ToObject<List<License>>();
                        if (u.LicensesArray != null && u.LicensesArray.Count > 0)
                        {
                            u.LicensesArray.ForEach(l =>
                            {
                                l.UserDisplayName = string.Format("{0}, {1}", u.LastName, u.FirstName);

                                l.CustomId = u.CustomId;
                                l.Grouper = i;
                                list.Add(l);
                            }
                                );
                        }
                        i++;
                    }
                });
            }
            return list;
        }

        public bool AddLicenseItem(LicenseItem licenseItem, System.Web.HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var isAssetSaved = true;
            if (httpFiles.Count > 0)
            {
                foreach (string key in httpFiles.AllKeys)
                {
                    HttpPostedFileBase file = httpFiles.Get(key);
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        using (var binaryReader = new BinaryReader(file.InputStream))
                        {
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                licenseItem.AssetId = asset.Id;
                            }
                            else
                            {
                                isAssetSaved = false;
                                break;
                            }
                        }
                    }
                }
            }
            if (isAssetSaved)
            {
                licenseItem.Id = Guid.NewGuid();
                licenseItem.Created = DateTime.Now;
                licenseItem.Modified = DateTime.Now;
                licenseItem.IsDeprecated = false;
                licenseItem.AgencyId = Current.AgencyId;

                if (licenseItem.OtherLicenseType.IsNotNullOrEmpty())
                {
                    licenseItem.LicenseType = licenseItem.OtherLicenseType;
                }
                if (userRepository.AddNonUserLicense(licenseItem))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, licenseItem.Id.ToString(), LogType.NonUserLicense, LogAction.UserLicenseAdded, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public IList<UserRate> GetUserRates(Guid userId)
        {
            var list = new List<UserRate>();
            var userRates = userRepository.GetUserRatesOnly( Current.AgencyId,userId);
            if (userRates.IsNotNullOrEmpty())
            {
                list = userRates.ToObject<List<UserRate>>();
                if (list.IsNotNullOrEmpty())
                {
                    var insuranceIds = list.Where(l => l.Insurance.IsNotNullOrEmpty() && l.Insurance.IsInteger() && l.Insurance.ToInteger() > 0).Select(l => l.Insurance.ToInteger()).Distinct().ToList();
                    if (insuranceIds.IsNotNullOrEmpty())
                    {
                        var insurances = InsuranceEngine.GetInsurances(Current.AgencyId, insuranceIds);
                        if (insurances.IsNotNullOrEmpty())
                        {
                            list.ForEach(r => {
                                var insurance = insurances.FirstOrDefault(i => i.Id.ToString() == r.Insurance);
                                if (insurance != null)
                                {
                                    r.InsuranceName = insurance.Name;
                                }
                            });
                        }
                    }
                }
            }
            return list;
        }


        public bool LoadUserRate(Guid fromId, Guid toId)
        {
            #region UserPayRates
            bool result = false;
            var fromUserLicense = userRepository.GetUserLicensesOnly(Current.AgencyId, fromId);
            var toUser = userRepository.GetUserOnly( toId,Current.AgencyId);
            toUser.Rates = fromUserLicense;
            if (userRepository.UpdateModel(toUser))
            {
                result = true;
            }
            #endregion
            
            #region UserNonVisitPayRates
            bool doesToUserNonVisitRecordExist = false;
            bool doesFromUserNonVisitRecordExist = false;
            var fromUserNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, fromId);
            if (fromUserNonVisitRate.IsNull())
            {
                fromUserNonVisitRate = new UserNonVisitRate();
                fromUserNonVisitRate.Id = Guid.NewGuid();
                fromUserNonVisitRate.AgencyId = Current.AgencyId;
                fromUserNonVisitRate.UserId = fromId;
                var rate = new List<UserNonVisitTaskRate>();
                fromUserNonVisitRate.Rate = rate.ToXml();
                if (userRepository.AddUserNonVisitRate(fromUserNonVisitRate))
                {
                    doesFromUserNonVisitRecordExist = true;
                }
            }
            else 
            {
                doesFromUserNonVisitRecordExist = true;
            }

            if (doesFromUserNonVisitRecordExist == true)
            {
                var toUserNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, toId);
                if (toUserNonVisitRate.IsNull())
                {
                    toUserNonVisitRate = new UserNonVisitRate();
                    toUserNonVisitRate.Id = Guid.NewGuid();
                    toUserNonVisitRate.AgencyId = Current.AgencyId;
                    toUserNonVisitRate.UserId = toId;
                    toUserNonVisitRate.Rate = fromUserNonVisitRate.Rate;
                    if (userRepository.AddUserNonVisitRate(toUserNonVisitRate))
                    {
                        doesToUserNonVisitRecordExist = true;
                    }
                }
                else
                {
                    doesToUserNonVisitRecordExist = true;
                }
                if (doesToUserNonVisitRecordExist == true)
                {
                    toUserNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, toId);
                    fromUserNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, fromId);
                    toUserNonVisitRate.Rate = fromUserNonVisitRate.Rate;
                    if (userRepository.UpdateUserNonVisitRate(toUserNonVisitRate))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else 
                {
                    result = false;
                }
            }
            else 
            {
                result = false;
            }
            #endregion

            return result;
        }

        public SubscriptionPlanViewData GetAgencyUserSubcriptionPlanDetails()
        {
            var viewData = new SubscriptionPlanViewData();
            var plans = agencyRepository.GetSubscriptionPlans(Current.AgencyId);
            if (plans != null && plans.Count > 0)
            {
                plans.ForEach(plan =>
                {
                    if (plan.IsUserPlan)
                    {
                        viewData.Plans.Add(plan.AgencyLocationId, new LocationPlanViewData
                        {
                            Max = plan.PlanLimit,
                            CurrentPlan = plan.PlanLimit,
                            NextPlan = plan.PlanLimit.NextUserSubscriptionPlan(),
                            CurrentPlanDescription = plan.PlanLimit.UserSubscriptionPlanName(),
                            NextPlanDescription = plan.PlanLimit.NextUserSubscriptionPlan().UserSubscriptionPlanName(),
                            Count = agencyRepository.GetUserCountPerLocation(plan.AgencyId, plan.AgencyLocationId)
                        });
                    }
                });
            }
            return viewData;
        }


        public LicenseItem GetUserLicenseItem(Guid userId, Guid licenseId)
        {
            LicenseItem licenseItem = null;

            var user = userRepository.GetUserLicensesWithDisplayName(Current.AgencyId, userId);
            if (user!=null && user.Licenses.IsNotNullOrEmpty())
            {
                var licensesArray = user.Licenses.ToObject<List<License>>();
                if (licensesArray.IsNotNullOrEmpty())
                {
                    var license = licensesArray.Find(l => l.Id == licenseId && l.IsDeprecated == false);
                    if (license != null)
                    {
                        licenseItem = new LicenseItem
                        {
                            Id = license.Id,
                            UserId = userId,
                            AssetId = license.AssetId,
                            DisplayName = user.DisplayName,
                            LicenseType = license.LicenseType,
                            IssueDate = license.InitiationDate,
                            ExpireDate = license.ExpirationDate
                        };
                    }
                }
            }
            return licenseItem;
        }

      

        public  JsonViewData AddUserRate(UserRate userRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User rate could not be added. Please try again." };
            if (userRate.IsValid)
            {
                var user = userRepository.GetUserOnly(userRate.UserId, Current.AgencyId);
                if (user != null)
                {
                    if (user.Rates.IsNotNullOrEmpty())
                    {
                        var rates = user.Rates.ToObject<List<UserRate>>();
                        if (rates.IsNotNullOrEmpty())
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == userRate.Id && r.Insurance == userRate.Insurance);
                            if (rate != null)
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "User rate already exists.";
                            }
                            else
                            {
                                rates.Add(userRate);
                                user.Rates = rates.ToXml();
                                if (userRepository.UpdateModel(user))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User rate added successfully";
                                }
                            }
                        }
                        else
                        {
                            rates = new List<UserRate>();
                            rates.Add(userRate);
                            user.Rates = rates.ToXml();
                            if (userRepository.UpdateModel(user))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User rate added successfully";
                            }
                        }
                    }
                    else
                    {
                        var rates = new List<UserRate>();
                        rates.Add(userRate);
                        user.Rates = rates.ToXml();
                        if (userRepository.UpdateModel(user))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "User rate added successfully";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userRate.ValidationMessage;
            }
            return viewData;
        }

        public UserRate GetUserRateForEdit(Guid UserId, int Id, string Insurance)
        {
            UserRate userRate = null;
            var user = userRepository.GetUserOnly(UserId, Current.AgencyId);
            if (user != null && user.Rates.IsNotNullOrEmpty())
            {
                var rates = user.Rates.ToObject<List<UserRate>>();
                if (rates.IsNotNullOrEmpty())
                {
                    userRate = rates.FirstOrDefault(r => r.Id == Id && r.Insurance == Insurance);
                    if (userRate != null)
                    {
                        userRate.UserId = user.Id;
                        userRate.InsuranceName = patientService.GetInsurance(userRate.Insurance);
                    }
                    else
                    {
                        userRate = rates.FirstOrDefault(r => r.Id == Id && r.Insurance.IsNullOrEmpty() && r.RateType == 0);
                        if (userRate != null)
                        {
                            userRate.UserId = user.Id;
                            userRate.InsuranceName = patientService.GetInsurance(userRate.Insurance);
                        }
                        else
                        {
                            userRate = new UserRate();
                            userRate.Id = Id;
                            userRate.UserId = user.Id;
                            userRate.Insurance = string.Empty;
                        }
                    }
                }
            }
            return userRate;
        }

        public JsonViewData UpdateUserRate(UserRate userRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User rate could not be updated. Please try again." };
            if (userRate.IsValid)
            {
                var user = userRepository.GetUserOnly(userRate.UserId, Current.AgencyId);
                if (user != null)
                {
                    if (user.Rates.IsNotNullOrEmpty())
                    {
                        var rates = user.Rates.ToObject<List<UserRate>>();
                        if (rates.IsNotNullOrEmpty())
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == userRate.Id && r.Insurance == userRate.Insurance);

                            if (rate == null)
                            {
                                rate = rates.FirstOrDefault(r => r.Id == userRate.Id && r.Insurance.IsNullOrEmpty() && r.RateType == 0);
                                if (rate != null)
                                {
                                    rate.Rate = userRate.Rate;
                                    rate.RateType = userRate.RateType;
                                    rate.MileageRate = userRate.MileageRate;
                                    rate.Insurance = userRate.Insurance;
                                    user.Rates = rates.ToXml();
                                    if (userRepository.Refresh(user))
                                    {
                                        viewData.isSuccessful = true;
                                        viewData.errorMessage = "User rate updated successfully";
                                    }
                                }
                            }
                            else
                            {
                                rate.Rate = userRate.Rate;
                                rate.RateType = userRate.RateType;
                                rate.MileageRate = userRate.MileageRate;
                                rate.Insurance = userRate.Insurance;
                                user.Rates = rates.ToXml();
                                if (userRepository.Refresh(user))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User rate updated successfully";
                                }
                            }
                        }
                    }

                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userRate.ValidationMessage;
            }
            return viewData;
        }

        public JsonViewData DeleteUserRate(Guid UserId, int Id, string Insurance)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance visit rate. Please try again." };
            var user = userRepository.GetUserOnly(UserId, Current.AgencyId);
            if (user != null)
            {
                if (user.Rates.IsNotNullOrEmpty())
                {
                    var rates = user.Rates.ToObject<List<UserRate>>();
                    if (rates.IsNotNullOrEmpty())
                    {
                        var removed = rates.RemoveAll(r => r.Id == Id && r.Insurance == Insurance);
                        if (removed > 0)
                        {
                            user.Rates = rates.ToXml();
                            if (userRepository.UpdateModel(user))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User rate deleted successfully";
                            }
                        }
                        else
                        {
                            removed = rates.RemoveAll(r => r.Id == Id && r.Insurance.IsNullOrEmpty() && r.RateType == 0);
                            if (removed > 0)
                            {
                                user.Rates = rates.ToXml();
                                if (userRepository.UpdateModel(user))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User rate deleted successfully";
                                }
                            }
                        }
                    }
                }
            }
            return viewData;
        }

        public JsonViewData AddUserNonVisitRate(UserNonVisitTaskRate userNonVisitTaskRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User rate could not be added. Please try again." };
            if (userNonVisitTaskRate.IsValid)
            {
                var userNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, userNonVisitTaskRate.UserId);
                if (userNonVisitRate != null)
                {
                    if (userNonVisitRate.Rate.IsNotNullOrEmpty())
                    {
                        var userNonVisitTaskRates = userNonVisitRate.Rate.ToObject<List<UserNonVisitTaskRate>>();
                        if (userNonVisitTaskRates != null && userNonVisitTaskRates.Count > 0)
                        {
                            var rate = userNonVisitTaskRates.FirstOrDefault(r => r.Id == userNonVisitTaskRate.Id);
                            if (rate != null)
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "User rate already exists.";
                            }
                            else
                            {
                                userNonVisitTaskRates.Add(userNonVisitTaskRate);
                                userNonVisitRate.Rate = userNonVisitTaskRates.ToXml();
                                if (userRepository.UpdateUserNonVisitRate(userNonVisitRate))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User rate added successfully";
                                }
                            }
                        }
                        else
                        {
                            var rates = new List<UserNonVisitTaskRate>();
                            rates.Add(userNonVisitTaskRate);
                            userNonVisitRate.Rate = rates.ToXml();
                            if (userRepository.UpdateUserNonVisitRate(userNonVisitRate))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User rate added successfully";
                            }
                        }
                    }
                    else
                    {
                        var rates = new List<UserNonVisitTaskRate>();
                        rates.Add(userNonVisitTaskRate);
                        userNonVisitRate.Rate = rates.ToXml();
                        if (userRepository.UpdateUserNonVisitRate(userNonVisitRate))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "User rate added successfully";
                        }
                    }
                }
                else
                {
                    var newUserNonVisitRate = new UserNonVisitRate();
                    newUserNonVisitRate.Id = Guid.NewGuid();
                    newUserNonVisitRate.AgencyId = Current.AgencyId;
                    newUserNonVisitRate.UserId = userNonVisitTaskRate.UserId;
                    var rates = new List<UserNonVisitTaskRate>();
                    rates.Add(userNonVisitTaskRate);
                    newUserNonVisitRate.Rate = rates.ToXml();
                    if (userRepository.AddUserNonVisitRate(newUserNonVisitRate))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "User Rate Added Successfully";
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userNonVisitTaskRate.ValidationMessage;
            }
            return viewData;
        }

        public UserNonVisitTaskRate GetUserNonVisitRateForEdit(Guid UserId, Guid Id)
        {
            UserNonVisitTaskRate userNonVisitTaskRate = null;
            var userNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, UserId);
            if (userNonVisitRate != null && userNonVisitRate.Rate.IsNotNullOrEmpty())
            {
                var rates = userNonVisitRate.Rate.ToObject<List<UserNonVisitTaskRate>>();
                userNonVisitTaskRate = rates.FirstOrDefault(r => r.Id == Id);
                if (userNonVisitTaskRate != null)
                {
                    userNonVisitTaskRate.UserId = userNonVisitRate.UserId;
                    userNonVisitTaskRate.Id = Id;
                    var agencyNonVisit = agencyRepository.GetNonVisitTask(Current.AgencyId, Id);
                    if (agencyNonVisit != null && agencyNonVisit.Title.IsNotNullOrEmpty())
                    {
                        userNonVisitTaskRate.TaskTitle = agencyNonVisit.Title;
                    }
                }
                else
                {
                    userNonVisitTaskRate = new UserNonVisitTaskRate();
                    userNonVisitTaskRate.Id = Id;
                    userNonVisitTaskRate.UserId = UserId;
                }
            }
            else
            {
                userNonVisitTaskRate = new UserNonVisitTaskRate();
            }
            return userNonVisitTaskRate;
        }

        public JsonViewData UpdateUserNonVisitRate(UserNonVisitTaskRate userNonVisitTaskRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User rate could not be updated. Please try again." };
            if (userNonVisitTaskRate.IsValid)
            {
                var userNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, userNonVisitTaskRate.UserId);
                if (userNonVisitRate != null)
                {
                    if (userNonVisitRate.Rate.IsNotNullOrEmpty())
                    {
                        var rates = userNonVisitRate.Rate.ToObject<List<UserNonVisitTaskRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == userNonVisitTaskRate.Id);
                            if (rate != null)
                            {
                                rate.Rate = userNonVisitTaskRate.Rate;
                                rate.RateType = userNonVisitTaskRate.RateType;
                                userNonVisitRate.Rate = rates.ToXml();
                                if (userRepository.UpdateUserNonVisitRate(userNonVisitRate))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User rate updated successfully";
                                }
                            }
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "User Record Could not be found - Please reload page to resolve issues";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userNonVisitTaskRate.ValidationMessage;
            }
            return viewData;
        }

        public List<UserNonVisitTaskRate> GetUserNonVisitRateList( Guid UserId)
        {
            var nonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, UserId);
            List<UserNonVisitTaskRate> result = null;
            if (nonVisitRate != null)
            {
                result = nonVisitRate.Rate.ToObject<List<UserNonVisitTaskRate>>();
            }
            if (result == null)
            {
                result = new List<UserNonVisitTaskRate>();
            }
            else
            {
                foreach (var item in result)
                {
                    if (item != null)
                    {
                        if (item.Id != null)
                        {
                            var agencyNonVisit = agencyRepository.GetNonVisitTask(Current.AgencyId, item.Id);
                            if (agencyNonVisit != null)
                            {
                                if (agencyNonVisit.Title.IsNotNullOrEmpty())
                                {
                                    item.TaskTitle = agencyNonVisit.Title;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public JsonViewData DeleteUserNonVisitRate(Guid UserId, Guid Id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance visit rate. Please try again." };
            var userNonVisitRate = userRepository.GetUserNonVisitRate(Current.AgencyId, UserId);
            if (userNonVisitRate != null)
            {
                if (userNonVisitRate.Rate.IsNotNullOrEmpty())
                {
                    var rates = userNonVisitRate.Rate.ToObject<List<UserNonVisitTaskRate>>();
                    if (rates != null && rates.Count > 0)
                    {
                        var removed = rates.RemoveAll(r => r.Id == Id);
                        if (removed > 0)
                        {
                            userNonVisitRate.Rate = rates.ToXml();
                            if (userRepository.UpdateUserNonVisitRate(userNonVisitRate))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User rate deleted successfully";
                            }
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "No User Rate Found - Please reload to resolve issues";
            }
            return viewData;
        }
    }
}
