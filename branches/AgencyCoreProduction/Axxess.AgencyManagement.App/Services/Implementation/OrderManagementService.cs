﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.Core.Enums;
using Axxess.AgencyManagement.App.Domain;
using Axxess.AgencyManagement.Repositories;
using Axxess.Core;
using Axxess.Membership.Repositories;
using Axxess.LookUp.Repositories;
using Axxess.OasisC.Repositories;
using Axxess.AgencyManagement.App.Enums;
using Axxess.Core.Extension;
using Axxess.AgencyManagement.Domain;
using Axxess.AgencyManagement.App.Common;
using Axxess.OasisC.Domain;
using Axxess.Log.Enums;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.Enums;
using Axxess.OasisC.Extensions;
using System.Web.Mvc;
using Axxess.AgencyManagement.App.ViewData;

namespace Axxess.AgencyManagement.App.Services
{
    public class OrderManagementService : IOrderManagementService
    {

         private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IEpisodeRepository episodeRepository;

        public OrderManagementService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider,  ILookUpDataProvider lookUpDataProvider, IOasisCDataProvider oasisDataprovider)
        {
            Check.Argument.IsNotNull(oasisDataprovider, "oasisDataprovider");
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.planofCareRepository = oasisDataprovider.PlanofCareRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.scheduleRepository = agencyManagementDataProvider.ScheduleRepository;
            this.episodeRepository = agencyManagementDataProvider.EpisodeRepository;
        }

       

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "MSWEvaluationAssessment":
                        orderType = OrderType.MSWEvaluation;
                        break;
                    case "MSWDischarge":
                        orderType = OrderType.MSWDischarge;
                        break;
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "PTReassessment":
                        orderType = OrderType.PtReassessment;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "OTReassessment":
                        orderType = OrderType.OtReassessment;
                        break;
                    case "OTDischarge":
                        orderType = OrderType.OtDischarge;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                    case "PTDischarge":
                        orderType = OrderType.PTDischarge;
                        break;
                    case "STDischarge":
                        orderType = OrderType.StDischarge;
                        break;
                    case "SixtyDaySummary":
                        orderType = OrderType.SixtyDaySummary;
                        break;
                    case "ThirtyDaySummary":
                        orderType = OrderType.ThirtyDaySummary;
                        break;
                    case "TenDaySummary":
                        orderType = OrderType.TenDaySummary;
                        break;
                    case "PTPlanOfCare":
                        orderType = OrderType.PtPlanOfCare;
                        break;
                    case "OTPlanOfCare":
                        orderType = OrderType.OtPlanOfCare;
                        break;
                    case "STPlanOfCare":
                        orderType = OrderType.StPlanOfCare;
                        break;
                    case "SNPsychAssessment":
                        orderType = OrderType.SnPsychAssessment;
                        break;
                }
            }
            return orderType;
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "MSWEvaluationAssessment":
                        task = DisciplineTasks.MSWEvaluationAssessment;
                        break;
                    case "MSWDischarge":
                        task = DisciplineTasks.MSWDischarge;
                        break;
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "PTReassessment":
                        task = DisciplineTasks.PTReassessment;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "OTReassessment":
                        task = DisciplineTasks.OTReassessment;
                        break;
                    case "OTDischarge":
                        task = DisciplineTasks.OTDischarge;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                    case "PTDischarge":
                        task = DisciplineTasks.PTDischarge;
                        break;
                    case "STDischarge":
                        task = DisciplineTasks.STDischarge;
                        break;
                    case "SixtyDaySummary":
                        task = DisciplineTasks.SixtyDaySummary;
                        break;
                    case "ThirtyDaySummary":
                        task = DisciplineTasks.ThirtyDaySummary;
                        break;
                    case "TenDaySummary":
                        task = DisciplineTasks.TenDaySummary;
                        break;
                    case "PTPlanOfCare":
                        task = DisciplineTasks.PTPlanOfCare;
                        break;
                    case "OTPlanOfCare":
                        task = DisciplineTasks.OTPlanOfCare;
                        break;
                    case "STPlanOfCare":
                        task = DisciplineTasks.STPlanOfCare;
                        break;
                    case "SNPsychAssessment":
                        task = DisciplineTasks.SNPsychAssessment;
                        break;
                }
            }
            return task;
        }

        public Order GetOrder(Guid id, Guid patientId, Guid episodeId, string type)
        {
            var order = new Order();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (!id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty() && type.IsInteger() && Enum.IsDefined(typeof(OrderType), type.ToInteger()))
            {
                var typeEnum = ((OrderType)type.ToInteger()).ToString();
                if (typeEnum.IsNotNullOrEmpty())
                {
                    switch (typeEnum)
                    {
                        case "PhysicianOrder":
                            var physicianOrder = patientRepository.GetOrder(id, Current.AgencyId);
                            if (physicianOrder != null)
                            {
                                order = new Order
                                {
                                    Id = physicianOrder.Id,
                                    PatientId = physicianOrder.PatientId,
                                    EpisodeId = physicianOrder.EpisodeId,
                                    Type = OrderType.PhysicianOrder,
                                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                    Number = physicianOrder.OrderNumber,
                                    PatientName = physicianOrder.DisplayName,
                                    PhysicianName = physicianOrder.PhysicianName,
                                    ReceivedDate = physicianOrder.ReceivedDate > DateTime.MinValue ? physicianOrder.ReceivedDate : physicianOrder.SentDate,
                                    SendDate = physicianOrder.SentDate,
                                    PhysicianSignatureDate = physicianOrder.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "FaceToFaceEncounter":
                            var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                            if (faceToFaceEncounter != null)
                            {
                                order = new Order
                                {
                                    Id = faceToFaceEncounter.Id,
                                    PatientId = faceToFaceEncounter.PatientId,
                                    EpisodeId = faceToFaceEncounter.EpisodeId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = faceToFaceEncounter.OrderNumber,
                                    PatientName = faceToFaceEncounter.DisplayName,
                                    ReceivedDate = faceToFaceEncounter.ReceivedDate > DateTime.MinValue ? faceToFaceEncounter.ReceivedDate : faceToFaceEncounter.RequestDate,
                                    SendDate = faceToFaceEncounter.RequestDate,
                                    PhysicianSignatureDate = faceToFaceEncounter.SignatureDate
                                };
                            }
                            break;
                        case "HCFA485":
                        case "NonOasisHCFA485":
                            var planofCare = planofCareRepository.Get(Current.AgencyId, patientId, id);
                            if (planofCare != null)
                            {
                                order = new Order
                                {
                                    Id = planofCare.Id,
                                    PatientId = planofCare.PatientId,
                                    EpisodeId = planofCare.EpisodeId,
                                    Type = OrderType.HCFA485,
                                    Text = "HCFA485" == type ? DisciplineTasks.HCFA485.GetDescription() : ("NonOasisHCFA485" == type ? DisciplineTasks.NonOasisHCFA485.GetDescription() : string.Empty),
                                    Number = planofCare.OrderNumber,
                                    PatientName = planofCare.PatientName,
                                    ReceivedDate = planofCare.ReceivedDate > DateTime.MinValue ? planofCare.ReceivedDate : planofCare.SentDate,
                                    SendDate = planofCare.SentDate,
                                    PhysicianSignatureDate = planofCare.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "HCFA485StandAlone":
                            var planofCareStandAlone = planofCareRepository.Get(Current.AgencyId, patientId, id);
                            if (planofCareStandAlone != null)
                            {
                                order = new Order
                                {
                                    Id = planofCareStandAlone.Id,
                                    PatientId = planofCareStandAlone.PatientId,
                                    EpisodeId = planofCareStandAlone.EpisodeId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = planofCareStandAlone.OrderNumber,
                                    PatientName = planofCareStandAlone.PatientName,
                                    ReceivedDate = planofCareStandAlone.ReceivedDate > DateTime.MinValue ? planofCareStandAlone.ReceivedDate : planofCareStandAlone.SentDate,
                                    SendDate = planofCareStandAlone.SentDate,
                                    PhysicianSignatureDate = planofCareStandAlone.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "PtEvaluation":
                            var ptEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptEval != null)
                            {
                                order = new Order
                                {
                                    Id = ptEval.Id,
                                    PatientId = ptEval.PatientId,
                                    EpisodeId = ptEval.EpisodeId,
                                    Type = OrderType.PtEvaluation,
                                    Text = DisciplineTasks.PTEvaluation.GetDescription(),
                                    Number = ptEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptEval.ReceivedDate > DateTime.MinValue ? ptEval.ReceivedDate : ptEval.SentDate,
                                    SendDate = ptEval.SentDate,
                                    PhysicianSignatureDate = ptEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "PtReEvaluation":
                            var ptReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptReEval != null)
                            {
                                order = new Order
                                {
                                    Id = ptReEval.Id,
                                    PatientId = ptReEval.PatientId,
                                    EpisodeId = ptReEval.EpisodeId,
                                    Type = OrderType.PtReEvaluation,
                                    Text = DisciplineTasks.PTReEvaluation.GetDescription(),
                                    Number = ptReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptReEval.ReceivedDate > DateTime.MinValue ? ptReEval.ReceivedDate : ptReEval.SentDate,
                                    SendDate = ptReEval.SentDate,
                                    PhysicianSignatureDate = ptReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "OtEvaluation":
                            var otEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otEval != null)
                            {
                                order = new Order
                                {
                                    Id = otEval.Id,
                                    PatientId = otEval.PatientId,
                                    EpisodeId = otEval.EpisodeId,
                                    Type = OrderType.OtEvaluation,
                                    Text = DisciplineTasks.OTEvaluation.GetDescription(),
                                    Number = otEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otEval.ReceivedDate > DateTime.MinValue ? otEval.ReceivedDate : otEval.SentDate,
                                    SendDate = otEval.SentDate,
                                    PhysicianSignatureDate = otEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "OtReEvaluation":
                            var otReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otReEval != null)
                            {
                                order = new Order
                                {
                                    Id = otReEval.Id,
                                    PatientId = otReEval.PatientId,
                                    EpisodeId = otReEval.EpisodeId,
                                    Type = OrderType.OtReEvaluation,
                                    Text = DisciplineTasks.OTReEvaluation.GetDescription(),
                                    Number = otReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otReEval.ReceivedDate > DateTime.MinValue ? otReEval.ReceivedDate : otReEval.SentDate,
                                    SendDate = otReEval.SentDate,
                                    PhysicianSignatureDate = otReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "StEvaluation":
                            var stEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stEval != null)
                            {
                                order = new Order
                                {
                                    Id = stEval.Id,
                                    PatientId = stEval.PatientId,
                                    EpisodeId = stEval.EpisodeId,
                                    Type = OrderType.StEvaluation,
                                    Text = DisciplineTasks.STEvaluation.GetDescription(),
                                    Number = stEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stEval.ReceivedDate > DateTime.MinValue ? stEval.ReceivedDate : stEval.SentDate,
                                    SendDate = stEval.SentDate,
                                    PhysicianSignatureDate = stEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "StReEvaluation":
                            var stReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stReEval != null)
                            {
                                order = new Order
                                {
                                    Id = stReEval.Id,
                                    PatientId = stReEval.PatientId,
                                    EpisodeId = stReEval.EpisodeId,
                                    Type = OrderType.StReEvaluation,
                                    Text = DisciplineTasks.STReEvaluation.GetDescription(),
                                    Number = stReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stReEval.ReceivedDate > DateTime.MinValue ? stReEval.ReceivedDate : stReEval.SentDate,
                                    SendDate = stReEval.SentDate,
                                    PhysicianSignatureDate = stReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "PTDischarge":
                            var ptDischarge = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptDischarge != null)
                            {
                                order = new Order
                                {
                                    Id = ptDischarge.Id,
                                    PatientId = ptDischarge.PatientId,
                                    EpisodeId = ptDischarge.EpisodeId,
                                    Type = OrderType.PTDischarge,
                                    Text = DisciplineTasks.PTDischarge.GetDescription(),
                                    Number = ptDischarge.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptDischarge.ReceivedDate > DateTime.MinValue ? ptDischarge.ReceivedDate : ptDischarge.SentDate,
                                    SendDate = ptDischarge.SentDate,
                                    PhysicianSignatureDate = ptDischarge.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "OTDischarge":
                            var otDischarge = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otDischarge != null)
                            {
                                order = new Order
                                {
                                    Id = otDischarge.Id,
                                    PatientId = otDischarge.PatientId,
                                    EpisodeId = otDischarge.EpisodeId,
                                    Type = OrderType.OtDischarge,
                                    Text = DisciplineTasks.OTDischarge.GetDescription(),
                                    Number = otDischarge.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otDischarge.ReceivedDate > DateTime.MinValue ? otDischarge.ReceivedDate : otDischarge.SentDate,
                                    SendDate = otDischarge.SentDate,
                                    PhysicianSignatureDate = otDischarge.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "OtReassessment":
                            var OtReassessment = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (OtReassessment != null)
                            {
                                order = new Order
                                {
                                    Id = OtReassessment.Id,
                                    PatientId = OtReassessment.PatientId,
                                    EpisodeId = OtReassessment.EpisodeId,
                                    Type = OrderType.OtReassessment,
                                    Text = DisciplineTasks.OTReassessment.GetDescription(),
                                    Number = OtReassessment.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = OtReassessment.ReceivedDate > DateTime.MinValue ? OtReassessment.ReceivedDate : OtReassessment.SentDate,
                                    SendDate = OtReassessment.SentDate,
                                    PhysicianSignatureDate = OtReassessment.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "MSWDischarge":
                            var mswDischarge = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (mswDischarge != null)
                            {
                                order = new Order
                                {
                                    Id = mswDischarge.Id,
                                    PatientId = mswDischarge.PatientId,
                                    EpisodeId = mswDischarge.EpisodeId,
                                    Type = OrderType.MSWDischarge,
                                    Text = DisciplineTasks.MSWDischarge.GetDescription(),
                                    Number = mswDischarge.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = mswDischarge.ReceivedDate > DateTime.MinValue ? mswDischarge.ReceivedDate : mswDischarge.SentDate,
                                    SendDate = mswDischarge.SentDate,
                                    PhysicianSignatureDate = mswDischarge.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "STDischarge":
                            var stDischarge = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stDischarge != null)
                            {
                                order = new Order
                                {
                                    Id = stDischarge.Id,
                                    PatientId = stDischarge.PatientId,
                                    EpisodeId = stDischarge.EpisodeId,
                                    Type = OrderType.StDischarge,
                                    Text = DisciplineTasks.STDischarge.GetDescription(),
                                    Number = stDischarge.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stDischarge.ReceivedDate > DateTime.MinValue ? stDischarge.ReceivedDate : stDischarge.SentDate,
                                    SendDate = stDischarge.SentDate,
                                    PhysicianSignatureDate = stDischarge.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "SNPsychAssessment":
                            var snPsychAssessment = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (snPsychAssessment != null)
                            {
                                order = new Order
                                {
                                    Id = snPsychAssessment.Id,
                                    PatientId = snPsychAssessment.PatientId,
                                    EpisodeId = snPsychAssessment.EpisodeId,
                                    Type = OrderType.SnPsychAssessment,
                                    Text = DisciplineTasks.SNPsychAssessment.GetDescription(),
                                    Number = snPsychAssessment.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = snPsychAssessment.ReceivedDate > DateTime.MinValue ? snPsychAssessment.ReceivedDate : snPsychAssessment.SentDate,
                                    SendDate = snPsychAssessment.SentDate,
                                    PhysicianSignatureDate = snPsychAssessment.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "TenDaySummary":
                            var tenDaySummary = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (tenDaySummary != null)
                            {
                                order = new Order
                                {
                                    Id = tenDaySummary.Id,
                                    PatientId = tenDaySummary.PatientId,
                                    EpisodeId = tenDaySummary.EpisodeId,
                                    Type = OrderType.TenDaySummary,
                                    Text = DisciplineTasks.TenDaySummary.GetDescription(),
                                    Number = tenDaySummary.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = tenDaySummary.ReceivedDate > DateTime.MinValue ? tenDaySummary.ReceivedDate : tenDaySummary.SentDate,
                                    SendDate = tenDaySummary.SentDate,
                                    PhysicianSignatureDate = tenDaySummary.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "ThirtyDaySummary":
                            var thirtyDaySummary = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (thirtyDaySummary != null)
                            {
                                order = new Order
                                {
                                    Id = thirtyDaySummary.Id,
                                    PatientId = thirtyDaySummary.PatientId,
                                    EpisodeId = thirtyDaySummary.EpisodeId,
                                    Type = OrderType.ThirtyDaySummary,
                                    Text = DisciplineTasks.ThirtyDaySummary.GetDescription(),
                                    Number = thirtyDaySummary.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = thirtyDaySummary.ReceivedDate > DateTime.MinValue ? thirtyDaySummary.ReceivedDate : thirtyDaySummary.SentDate,
                                    SendDate = thirtyDaySummary.SentDate,
                                    PhysicianSignatureDate = thirtyDaySummary.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "SixtyDaySummary":
                            var sixtyDaySummary = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (sixtyDaySummary != null)
                            {
                                order = new Order
                                {
                                    Id = sixtyDaySummary.Id,
                                    PatientId = sixtyDaySummary.PatientId,
                                    EpisodeId = sixtyDaySummary.EpisodeId,
                                    Type = OrderType.SixtyDaySummary,
                                    Text = DisciplineTasks.SixtyDaySummary.GetDescription(),
                                    Number = sixtyDaySummary.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = sixtyDaySummary.ReceivedDate > DateTime.MinValue ? sixtyDaySummary.ReceivedDate : sixtyDaySummary.SentDate,
                                    SendDate = sixtyDaySummary.SentDate,
                                    PhysicianSignatureDate = sixtyDaySummary.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "PtPlanOfCare":
                            var ptPlanOfCare = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptPlanOfCare != null)
                            {
                                order = new Order
                                {
                                    Id = ptPlanOfCare.Id,
                                    PatientId = ptPlanOfCare.PatientId,
                                    EpisodeId = ptPlanOfCare.EpisodeId,
                                    Type = OrderType.PtPlanOfCare,
                                    Text = DisciplineTasks.PTPlanOfCare.GetDescription(),
                                    Number = ptPlanOfCare.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptPlanOfCare.ReceivedDate > DateTime.MinValue ? ptPlanOfCare.ReceivedDate : ptPlanOfCare.SentDate,
                                    SendDate = ptPlanOfCare.SentDate,
                                    PhysicianSignatureDate = ptPlanOfCare.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "OtPlanOfCare":
                            var otPlanOfCare = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otPlanOfCare != null)
                            {
                                order = new Order
                                {
                                    Id = otPlanOfCare.Id,
                                    PatientId = otPlanOfCare.PatientId,
                                    EpisodeId = otPlanOfCare.EpisodeId,
                                    Type = OrderType.OtPlanOfCare,
                                    Text = DisciplineTasks.OTPlanOfCare.GetDescription(),
                                    Number = otPlanOfCare.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otPlanOfCare.ReceivedDate > DateTime.MinValue ? otPlanOfCare.ReceivedDate : otPlanOfCare.SentDate,
                                    SendDate = otPlanOfCare.SentDate,
                                    PhysicianSignatureDate = otPlanOfCare.PhysicianSignatureDate
                                };
                            }
                            break;
                        case "StPlanOfCare":
                            var stPlanOfCare = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stPlanOfCare != null)
                            {
                                order = new Order
                                {
                                    Id = stPlanOfCare.Id,
                                    PatientId = stPlanOfCare.PatientId,
                                    EpisodeId = stPlanOfCare.EpisodeId,
                                    Type = OrderType.StPlanOfCare,
                                    Text = DisciplineTasks.STPlanOfCare.GetDescription(),
                                    Number = stPlanOfCare.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stPlanOfCare.ReceivedDate > DateTime.MinValue ? stPlanOfCare.ReceivedDate : stPlanOfCare.SentDate,
                                    SendDate = stPlanOfCare.SentDate,
                                    PhysicianSignatureDate = stPlanOfCare.PhysicianSignatureDate
                                };
                            }
                            break;
                    }
                }
            }

            return order;
        }

        public bool MarkOrdersAsSent(FormCollection formCollection)
        {
            var result = true;

            var sendElectronically = formCollection.Get("SendAutomatically").ToBoolean();
            formCollection.Remove("SendAutomatically");

            var status = (int)ScheduleStatus.OrderSentToPhysician;
            var physician = new AgencyPhysician();

            foreach (var key in formCollection.AllKeys)
            {
                string answers = formCollection.GetValues(key).Join(",");
                string[] answersArray = answers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.PhysicianOrder)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var orderId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var order = patientRepository.GetOrderOnly(orderId, Current.AgencyId);
                        if (order.PhysicianData.IsNotNullOrEmpty()) physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        if (order != null)
                        {
                            var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, order.PatientId, order.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status;
                                if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, scheduledEvent.Status))
                                {
                                    if (!patientRepository.UpdateOrderStatus(Current.AgencyId, orderId, status, DateTime.MinValue, DateTime.Now))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status, DisciplineTasks.PhysicianOrder, string.Empty);
                                    }
                                }
                            }

                        }

                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var episodeId = answerArray != null && answerArray.Length >= 3 ? answerArray[2].ToGuid() : Guid.Empty;
                        var planofCare = planofCareRepository.Get(Current.AgencyId, patientId, pocId);
                        if (planofCare.PhysicianData.IsNotNullOrEmpty()) physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (planofCare != null)
                        {

                            var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, planofCare.PatientId, planofCare.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = (status);
                                if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, scheduledEvent.Status))
                                {
                                    planofCare.Status = status;
                                    planofCare.SentDate = DateTime.Now;
                                    if (!planofCareRepository.Update(planofCare))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status, (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                    }
                                }
                            }

                        }
                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485StandAlone)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocOrderId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var episodeId = answerArray != null && answerArray.Length >= 3 ? answerArray[2].ToGuid() : Guid.Empty;
                        var planofCare = planofCareRepository.Get(Current.AgencyId, patientId, pocOrderId);
                        if (planofCare.PhysicianData.IsNotNullOrEmpty()) physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (planofCare != null)
                        {

                            var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId,  planofCare.PatientId, planofCare.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status;
                                if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, scheduledEvent.Status))
                                {
                                    planofCare.Status = status;
                                    planofCare.SentDate = DateTime.Now;
                                    if (!planofCareRepository.Update(planofCare))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status, (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                    }
                                }
                            }

                        }
                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.FaceToFaceEncounter)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }

                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var facetofaceId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(facetofaceId, Current.AgencyId);
                        if (faceToFaceEncounter.PhysicianData.IsNotNullOrEmpty()) physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                        if (faceToFaceEncounter != null)
                        {

                            var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId,  faceToFaceEncounter.PatientId, faceToFaceEncounter.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status;
                                if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, scheduledEvent.Status))
                                {
                                    if (!patientRepository.UpdateFaceToFaceEncounterForRequest(Current.AgencyId, facetofaceId, status, DateTime.Now))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status, (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                    }
                                }
                            }

                        }
                    });
                }
                if (key.IsInteger() && (key.ToInteger() == (int)OrderType.PtPlanOfCare || key.ToInteger() == (int)OrderType.StPlanOfCare || key.ToInteger() == (int)OrderType.OtPlanOfCare))
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var pocOrder = patientRepository.GetVisitNote(Current.AgencyId, patientId, pocId);
                        if (pocOrder.PhysicianData.IsNotNullOrEmpty()) physician = pocOrder.PhysicianData.ToObject<AgencyPhysician>();
                        if (pocOrder != null)
                        {
                            var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, pocOrder.PatientId, pocOrder.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status;
                                if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, scheduledEvent.Status))
                                {
                                    pocOrder.Status = status;
                                    pocOrder.SentDate = DateTime.Now;
                                    if (!patientRepository.UpdateVisitNote(pocOrder))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status, (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                    }
                                }
                            }

                        }
                    });
                }
                if (key.IsInteger() && (key.ToInteger() == (int)OrderType.PtEvaluation || key.ToInteger() == (int)OrderType.PtReEvaluation || key.ToInteger() == (int)OrderType.PtReassessment
                    || key.ToInteger() == (int)OrderType.OtEvaluation || key.ToInteger() == (int)OrderType.OtReEvaluation || key.ToInteger() == (int)OrderType.OtReassessment || key.ToInteger() == (int)OrderType.OtDischarge
                    || key.ToInteger() == (int)OrderType.StEvaluation || key.ToInteger() == (int)OrderType.StReEvaluation
                    || key.ToInteger() == (int)OrderType.MSWEvaluation) || key.ToInteger() == (int)OrderType.PTDischarge || key.ToInteger() == (int)OrderType.MSWDischarge || key.ToInteger() == (int)OrderType.StDischarge
                    || key.ToInteger() == (int)OrderType.SixtyDaySummary || key.ToInteger() == (int)OrderType.ThirtyDaySummary || key.ToInteger() == (int)OrderType.TenDaySummary || key.ToInteger() == (int)OrderType.SnPsychAssessment)
                {
                    status = (int)ScheduleStatus.EvalSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.EvalSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var evalId = answerArray != null && answerArray.Length >= 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length >= 2 ? answerArray[1].ToGuid() : Guid.Empty;
                        var evalOrder = patientRepository.GetVisitNote(Current.AgencyId, patientId, evalId);
                        if (evalOrder.PhysicianData.IsNotNullOrEmpty()) physician = evalOrder.PhysicianData.ToObject<AgencyPhysician>();
                        if (evalOrder != null)
                        {
                            var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId,  evalOrder.PatientId, evalOrder.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status;
                                if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, scheduledEvent.Status))
                                {
                                    evalOrder.Status = status;
                                    evalOrder.SentDate = DateTime.Now;
                                    if (!patientRepository.UpdateVisitNote(evalOrder))
                                    {
                                        result = false;
                                        return;
                                    }
                                    else
                                    {
                                            Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status, (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                    }
                                }
                            }

                        }
                    });
                }
                if (result && physician != null)
                {
                    string subject = string.Format("{0} has sent you an order", Current.AgencyName);
                    string lastName = physician.LastName.Trim().Substring(0, 1).ToUpper() + physician.LastName.Trim().Substring(1).ToLower();
                    string bodyText = MessageBuilder.PrepareTextFrom("PhysicianOrderNotification", "recipientlastname", lastName, "senderfullname", Current.AgencyName);
                    if (physician.EmailAddress.IsNotNullOrEmpty() && physician.EmailAddress.Trim().IsNotNullOrEmpty())
                    {
                        Notify.User(CoreSettings.NoReplyEmail, physician.EmailAddress, subject, bodyText);
                    }
                }
            }

            return result;
        }

        public void MarkOrderAsReturned(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, id);
            if (scheduledEvent != null)
            {
                var oldStatus = scheduledEvent.Status;
                var task = scheduledEvent.DisciplineTask;
                if (!physicianSignatureDate.IsValid())
                {
                    physicianSignatureDate = DateTime.Today;
                }
                if (task == (int)DisciplineTasks.PhysicianOrder)
                {
                    var order = patientRepository.GetOrderOnly(id, Current.AgencyId);
                    if (order != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                        var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!order.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                        order.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        order.ReceivedDate = receivedDate;
                        order.PhysicianSignatureDate = physicianSignatureDate;
                        if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, scheduledEvent.Status))
                        {
                            if (patientRepository.UpdateOrderModel(order))
                            {

                                    Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status, (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                            else
                            {
                                scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, oldStatus);
                            }
                        }
                    }

                }

                else if (DisciplineTaskFactory.POC().Contains(task))
                {
                    var planofCare = planofCareRepository.Get(Current.AgencyId, patientId, id);
                    if (planofCare != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);

                        var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!planofCare.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                        planofCare.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        planofCare.ReceivedDate = receivedDate;
                        planofCare.PhysicianSignatureDate = physicianSignatureDate;
                        if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, scheduledEvent.Status))
                        {
                            if (planofCareRepository.Update(planofCare))
                            {
                                    Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status, (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                            else
                            {
                                scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, oldStatus);
                            }
                        }
                    }
                }

               //else if (type == OrderType.HCFA485StandAlone)
                // {
                //     var pocStandAlone = planofCareRepository.Get(Current.AgencyId, patientId, id);
                //     if (pocStandAlone != null)
                //     {
                //         var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, pocStandAlone.EpisodeId, pocStandAlone.PatientId, pocStandAlone.Id);
                //         if (scheduledEvent != null)
                //         {
                //             scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                //             if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                //             {
                //                 var physician = pocStandAlone.PhysicianData.ToObject<AgencyPhysician>();
                //                 if (physician != null)
                //                 {
                //                     pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                //                 }
                //                 else if (!pocStandAlone.PhysicianId.IsEmpty())
                //                 {
                //                     physician = physicianRepository.Get(pocStandAlone.PhysicianId, Current.AgencyId);
                //                     if (physician != null)
                //                     {
                //                         pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                //                     }
                //                 }
                //                 pocStandAlone.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                //                 pocStandAlone.ReceivedDate = receivedDate;
                //                 pocStandAlone.PhysicianSignatureDate = physicianSignatureDate;
                //                 planofCareRepository.Update(pocStandAlone);
                //                 if (scheduledEvent.Status.IsInteger())
                //                 {
                //                     Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                //                 }
                //             }
                //         }
                //     }
                // }

                else if (task == (int)DisciplineTasks.FaceToFaceEncounter)
                {
                    var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                    if (faceToFaceEncounter != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                        var physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                        faceToFaceEncounter.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        faceToFaceEncounter.ReceivedDate = receivedDate;
                        faceToFaceEncounter.SignatureDate = physicianSignatureDate;
                        if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, scheduledEvent.Status))
                        {
                            if (patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter))
                            {
                                    Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status, (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                            else
                            {
                                scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, oldStatus);
                            }
                        }

                    }
                }
                //else if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation || type == OrderType.PtReassessment
                //     || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation || type == OrderType.OtReassessment || type == OrderType.OtDischarge
                //     || type == OrderType.StEvaluation || type == OrderType.StReEvaluation || type == OrderType.SnPsychAssessment
                //     || type == OrderType.MSWEvaluation || type == OrderType.PTDischarge || type == OrderType.MSWDischarge || type == OrderType.StDischarge
                //     || type == OrderType.SixtyDaySummary || type == OrderType.ThirtyDaySummary || type == OrderType.TenDaySummary || type == OrderType.PtPlanOfCare || type == OrderType.OtPlanOfCare || type == OrderType.StPlanOfCare)
                else if (DisciplineTaskFactory.NoteOrders().Contains(task))
                {
                    var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                    if (eval != null)
                    {
                        if (type == OrderType.PtPlanOfCare || type == OrderType.OtPlanOfCare || type == OrderType.StPlanOfCare)
                        {
                            scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                        }
                        else
                        {
                            scheduledEvent.Status = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature);
                        }
                        var physician = eval.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!eval.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(eval.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                        eval.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                        eval.ReceivedDate = receivedDate;
                        eval.PhysicianSignatureDate = physicianSignatureDate;
                        if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, scheduledEvent.Status))
                        {
                            if (patientRepository.UpdateVisitNote(eval))
                            {
                                    Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.Id, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status, (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                            else
                            {
                                scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, scheduledEvent.PatientId, scheduledEvent.Id, oldStatus);
                            }
                        }
                    }
                }
            }
        }

        public bool UpdateOrderDates(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            if (type == OrderType.PhysicianOrder)
            {
                var order = patientRepository.GetOrderOnly(id, Current.AgencyId);
                if (order != null)
                {
                    order.ReceivedDate = receivedDate;
                    order.SentDate = sendDate;
                    order.PhysicianSignatureDate = physicianSignatureDate;
                    if (order.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!order.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                order.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = patientRepository.UpdateOrderModel(order);
                }
            }
            else if (type == OrderType.HCFA485)
            {
                var planofCare = planofCareRepository.Get(Current.AgencyId, patientId, id);
                if (planofCare != null)
                {
                    planofCare.ReceivedDate = receivedDate;
                    planofCare.SentDate = sendDate;
                    planofCare.PhysicianSignatureDate = physicianSignatureDate;
                    if (planofCare.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!planofCare.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(planofCare.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = planofCareRepository.Update(planofCare);
                }
            }
            else if (type == OrderType.HCFA485StandAlone)
            {
                var pocStandAlone = planofCareRepository.Get(Current.AgencyId, patientId, id);
                if (pocStandAlone != null)
                {
                    pocStandAlone.ReceivedDate = receivedDate;
                    pocStandAlone.SentDate = sendDate;
                    pocStandAlone.PhysicianSignatureDate = physicianSignatureDate;
                    if (pocStandAlone.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = pocStandAlone.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!pocStandAlone.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(pocStandAlone.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = planofCareRepository.Update(pocStandAlone);
                }
            }
            else if (type == OrderType.FaceToFaceEncounter)
            {
                var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                if (faceToFaceEncounter != null)
                {
                    faceToFaceEncounter.ReceivedDate = receivedDate;
                    faceToFaceEncounter.SentDate = sendDate;
                    faceToFaceEncounter.SignatureDate = physicianSignatureDate;
                    if (faceToFaceEncounter.SignatureText.IsNullOrEmpty())
                    {
                        var physician = faceToFaceEncounter.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                faceToFaceEncounter.SignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter);
                }
            }
            else if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation
                || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation
                || type == OrderType.StEvaluation || type == OrderType.StReEvaluation
                || type == OrderType.MSWEvaluation || type == OrderType.PTDischarge || type == OrderType.StDischarge
                || type == OrderType.OtReassessment || type == OrderType.PtReassessment
                || type == OrderType.PtPlanOfCare || type == OrderType.OtPlanOfCare || type == OrderType.StPlanOfCare
                || type == OrderType.SixtyDaySummary || type == OrderType.ThirtyDaySummary || type == OrderType.TenDaySummary || type == OrderType.SnPsychAssessment)
            {
                var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                if (eval != null)
                {
                    eval.ReceivedDate = receivedDate;
                    eval.SentDate = sendDate;
                    eval.PhysicianSignatureDate = physicianSignatureDate;
                    if (eval.PhysicianSignatureText.IsNullOrEmpty())
                    {
                        var physician = eval.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                        }
                        else if (!eval.PhysicianId.IsEmpty())
                        {
                            physician = physicianRepository.Get(eval.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                eval.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            }
                        }
                    }
                    result = patientRepository.UpdateVisitNote(eval);
                }
            }
            return result;
        }


        public bool ProcessPhysicianOrder(Guid episodeId, Guid patientId, Guid eventId, string actionType)
        {
            var result = false;
            var shouldUpdateEpisode = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.Status;
                    var oldInPrintQueue = scheduleEvent.InPrintQueue;

                    var updateOrder = true;

                    var description = string.Empty;
                    var physicianOrder = patientRepository.GetOrder(eventId, patientId, Current.AgencyId);
                    if (physicianOrder != null)
                    {
                        if (actionType == "Approve")
                        {
                            physicianOrder.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);
                            physicianOrder.Modified = DateTime.Now;
                            scheduleEvent.InPrintQueue = true;
                            scheduleEvent.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);

                            shouldUpdateEpisode = true;
                            description = "Approved By:" + Current.UserFullName;
                        }
                        else if (actionType == "Return")
                        {
                            physicianOrder.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                            physicianOrder.Modified = DateTime.Now;
                            scheduleEvent.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);

                            shouldUpdateEpisode = true;
                            description = "Returned By: " + Current.UserFullName;
                        }
                        else if (actionType == "Print")
                        {
                            scheduleEvent.InPrintQueue = false;

                            shouldUpdateEpisode = true;
                            updateOrder = false;
                        }
                        if (shouldUpdateEpisode)
                        {
                            if (scheduleRepository.UpdateScheduleTasksPrintQueueAndStatus(Current.AgencyId, patientId, eventId, scheduleEvent.Status, scheduleEvent.InPrintQueue))
                            {
                                if (!updateOrder || (updateOrder && patientRepository.UpdateOrder(physicianOrder)))
                                {
                                    result = true;
                                    Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, Actions.StatusChange, (ScheduleStatus)physicianOrder.Status, DisciplineTasks.PhysicianOrder, description);
                                }
                                else
                                {
                                    if (updateOrder)
                                    {
                                        scheduleRepository.UpdateScheduleTasksPrintQueueAndStatus(Current.AgencyId, patientId, eventId, oldStatus, oldInPrintQueue);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        //private List<Order> GetOrdersFromScheduleEvents(Guid patientId, DateTime startDate, DateTime endDate, List<ScheduleEvent> schedules)
        //{
        //    var orders = new List<Order>();
        //    if (schedules != null && schedules.Count > 0)
        //    {
        //        var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
        //        if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
        //        {
        //            var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
        //            var physicianOrders = patientRepository.GetPatientPhysicianOrders(Current.AgencyId, patientId, physicianOrdersIds);
        //            physicianOrders.ForEach(po =>
        //            {
        //                var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
        //                var deleteUrl = !Current.IfOnlyRole(AgencyRoles.Auditor) ? string.Format(" | <a href=\"javaScript:void(0);\" onclick=\"Patient.DeleteOrder('{0}','{1}','{2}');\">Delete</a>", po.Id, po.PatientId, po.EpisodeId) : string.Empty;
        //                orders.Add(new Order
        //                {
        //                    Id = po.Id,
        //                    PatientId = po.PatientId,
        //                    EpisodeId = po.EpisodeId,
        //                    Type = OrderType.PhysicianOrder,
        //                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
        //                    Number = po.OrderNumber,
        //                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
        //                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                    PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true) + deleteUrl,
        //                    CreatedDate = po.OrderDateFormatted.ToZeroFilled(),
        //                    ReceivedDate = po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature ? po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate : DateTime.MinValue,
        //                    SendDate = po.Status == (int)ScheduleStatus.OrderSentToPhysician || po.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || po.Status == (int)ScheduleStatus.OrderSavedByPhysician ? po.SentDate : DateTime.MinValue,
        //                    Status = po.Status
        //                });
        //            });
        //        }
        //        var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
        //        if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
        //        {
        //            var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
        //            var planofCareOrders = planofCareRepository.GetPatientPlanofCares(Current.AgencyId, patientId, planofCareOrdersIds);
        //            planofCareOrders.ForEach(poc =>
        //            {
        //                var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
        //                if (evnt != null)
        //                {
        //                    var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
        //                    orders.Add(new Order
        //                    {
        //                        Id = poc.Id,
        //                        PatientId = poc.PatientId,
        //                        EpisodeId = poc.EpisodeId,
        //                        Type = OrderType.HCFA485,
        //                        Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
        //                        Number = poc.OrderNumber,
        //                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
        //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                        PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
        //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
        //                        ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() ? (poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate) : DateTime.MinValue,
        //                        SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString() || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician).ToString() ? poc.SentDate : DateTime.MinValue,
        //                        Status = poc.Status
        //                    });
        //                }
        //            });
        //        }

        //        var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
        //        if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
        //        {
        //            var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
        //            var planofCareStandAloneOrders = planofCareRepository.GetPatientPlanofCaresStandAlones(Current.AgencyId, patientId, planofCareStandAloneOrdersIds);
        //            planofCareStandAloneOrders.ForEach(poc =>
        //            {
        //                var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
        //                if (evnt != null)
        //                {
        //                    var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
        //                    orders.Add(new Order
        //                    {
        //                        Id = poc.Id,
        //                        PatientId = poc.PatientId,
        //                        EpisodeId = poc.EpisodeId,
        //                        Type = OrderType.HCFA485StandAlone,
        //                        Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
        //                        Number = poc.OrderNumber,
        //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
        //                        PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
        //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
        //                        ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() ? poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate : DateTime.MinValue,
        //                        SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString() || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician).ToString() ? poc.SentDate : DateTime.MinValue,
        //                        Status = poc.Status
        //                    });
        //                }
        //            });
        //        }

        //        var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
        //        if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
        //        {
        //            var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
        //            var faceToFaceEncounters = patientRepository.GetPatientFaceToFaceEncounterOrders(Current.AgencyId, patientId, faceToFaceEncounterOrdersIds);
        //            faceToFaceEncounters.ForEach(ffe =>
        //            {
        //                var evnt = faceToFaceEncounterSchedules.SingleOrDefault(s => s.EventId == ffe.Id);
        //                if (evnt != null)
        //                {
        //                    var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
        //                    orders.Add(new Order
        //                    {
        //                        Id = ffe.Id,
        //                        PatientId = ffe.PatientId,
        //                        EpisodeId = ffe.EpisodeId,
        //                        Type = OrderType.FaceToFaceEncounter,
        //                        Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
        //                        Number = ffe.OrderNumber,
        //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
        //                        PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
        //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
        //                        ReceivedDate = evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() ? ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate : DateTime.MinValue,
        //                        SendDate = evnt.Status == ((int)ScheduleStatus.OrderSentToPhysician).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString() || evnt.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician).ToString() ? ffe.RequestDate : DateTime.MinValue,
        //                        Status = ffe.Status
        //                    });
        //                }
        //            });
        //        }

        //        var evalOrdersSchedule = schedules.Where(s => DisciplineTaskFactory.NoteOrders().Contains(s.DisciplineTask)).ToList();
        //        if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
        //        {
        //            var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
        //            var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, evalOrdersIds);
        //            if (evalOrders != null && evalOrders.Count > 0)
        //            {
        //                evalOrders.ForEach(eval =>
        //                {
        //                    var evnt = evalOrdersSchedule.SingleOrDefault(s => s.EventId == eval.Id);
        //                    if (evnt != null)
        //                    {
        //                        var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
        //                        orders.Add(new Order
        //                        {
        //                            Id = eval.Id,
        //                            PatientId = eval.PatientId,
        //                            EpisodeId = eval.EpisodeId,
        //                            Type = GetOrderType(eval.NoteType),
        //                            Text = GetDisciplineType(eval.NoteType).GetDescription(),
        //                            Number = eval.OrderNumber,
        //                            PatientName = eval.DisplayName,
        //                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
        //                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
        //                            PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
        //                            CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
        //                            ReceivedDate = evnt.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString() ? eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate : DateTime.MinValue,
        //                            SendDate = evnt.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString() || evnt.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString() || evnt.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString() || evnt.Status == ((int)ScheduleStatus.OrderSavedByPhysician).ToString() ? eval.SentDate : DateTime.MinValue,
        //                            Status = eval.Status,

        //                        });
        //                    }
        //                });
        //            }
        //        }
        //    }
        //    return orders.OrderByDescending(o => o.CreatedDate).ToList();
        //}

        public List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate, bool needPrintUrl)
        {
            var orders = new List<Order>();
            var physicianIds = new List<Guid>();
            List<int> status= null;
            var physicianOrders = patientRepository.GetPatientPhysicianOrders(Current.AgencyId, patientId, status, startDate, endDate);
            if (physicianOrders.IsNotNullOrEmpty())
            {
                physicianIds.AddRange(physicianOrders.Select(p => p.PhysicianId));
            }
            var planofCareOrders = planofCareRepository.GetPatientPlanofCareOrders(Current.AgencyId, patientId,  DisciplineTaskFactory.POC(),status,startDate,endDate);
            if (planofCareOrders.IsNotNullOrEmpty())
            {
                physicianIds.AddRange(planofCareOrders.Select(p => p.PhysicianId));
            }
            var evalOrders = patientRepository.GetPatientEvalOrders(Current.AgencyId, patientId, DisciplineTaskFactory.NoteOrders(), status,startDate, endDate);
            if (evalOrders.IsNotNullOrEmpty())
            {
                physicianIds.AddRange(evalOrders.Select(p => p.PhysicianId));
            }
            var faceToFaceEncounters = patientRepository.GetPatientFaceToFaceEncounters(Current.AgencyId, patientId, status,startDate,endDate);
            if (faceToFaceEncounters.IsNotNullOrEmpty())
            {
                physicianIds.AddRange(faceToFaceEncounters.Select(p => p.PhysicianId));
            }
            var physicians = new List<AgencyPhysician>();
            physicianIds = physicianIds.Where(Id => !Id.IsEmpty()).Distinct().ToList();
            if (physicianIds.IsNotNullOrEmpty())
            {
                physicians = PhysicianEngine.GetAgencyPhysicians(Current.AgencyId, physicianIds);
            }

            var formattedPhysicianOrders = PhysicianOrderHelper(physicianOrders, physicians, needPrintUrl, OrderStatusGroup.StillProcessing);
            if (formattedPhysicianOrders.IsNotNullOrEmpty())
            {
                orders.AddRange(formattedPhysicianOrders);
            }
            var formattedPlanofCares = PlanofCareOrderHelper(planofCareOrders, physicians, needPrintUrl, OrderStatusGroup.StillProcessing);
            if (formattedPlanofCares.IsNotNullOrEmpty())
            {
                orders.AddRange(formattedPlanofCares);
            }
            var formattedPatientVisitNotes = PatientVisitNoteHelper(evalOrders, physicians, needPrintUrl, OrderStatusGroup.StillProcessing);
            if (formattedPatientVisitNotes.IsNotNullOrEmpty())
            {
                orders.AddRange(formattedPatientVisitNotes);
            }

            var formattedFaceToFaceEncounters = FaceToFaceEncounterHelper(faceToFaceEncounters, physicians, needPrintUrl, OrderStatusGroup.StillProcessing);
            if (formattedFaceToFaceEncounters.IsNotNullOrEmpty())
            {
                orders.AddRange(formattedFaceToFaceEncounters);
            }


            return orders;
            //var schedules = scheduleRepository.GetPatientOrderScheduleEvents(Current.AgencyId, patientId, startDate, endDate, DisciplineTaskFactory.BillingAndNoteOrdres());
            //return GetOrdersFromScheduleEvents(patientId, startDate, endDate, schedules);
        }

        public List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId, bool needPrintUrl)
        {
            var orders = new List<Order>();
            var episodeRange = episodeRepository.GetEpisodeLean(Current.AgencyId, patientId, episodeId);
            if (episodeRange != null)
            {
                var episode = new PatientEpisode { Id = episodeId, PatientId = patientId, StartDate = episodeRange.StartDate, EndDate = episodeRange.EndDate };

                var physicianIds = new List<Guid>();
                var physicianOrders = patientRepository.GetCurrentAndPreviousPhysicianOrders(Current.AgencyId, episode);
                if (physicianOrders.IsNotNullOrEmpty())
                {
                    physicianIds.AddRange(physicianOrders.Select(p => p.PhysicianId));
                }
                var planofCareOrders = planofCareRepository.GetCurrentAndPreviousPlanofCareOrders(Current.AgencyId,patientId,episodeId,episode.StartDate,DisciplineTaskFactory.POC());
                if (planofCareOrders.IsNotNullOrEmpty())
                {
                    physicianIds.AddRange(planofCareOrders.Select(p => p.PhysicianId));
                }
                var evalOrders = patientRepository.GetCurrentAndPreviousEvalOrders(Current.AgencyId,episode, DisciplineTaskFactory.NoteOrders());
                if (evalOrders.IsNotNullOrEmpty())
                {
                    physicianIds.AddRange(evalOrders.Select(p => p.PhysicianId));
                }
                var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounters(Current.AgencyId, patientId,episodeId);
                if (faceToFaceEncounters.IsNotNullOrEmpty())
                {
                    physicianIds.AddRange(faceToFaceEncounters.Select(p => p.PhysicianId));
                }
                var physicians = new List<AgencyPhysician>();
                physicianIds = physicianIds.Where(Id => !Id.IsEmpty()).Distinct().ToList();
                if (physicianIds.IsNotNullOrEmpty())
                {
                    physicians = PhysicianEngine.GetAgencyPhysicians(Current.AgencyId, physicianIds);
                }

                var formattedPhysicianOrders = PhysicianOrderHelper(physicianOrders, physicians, needPrintUrl, OrderStatusGroup.StillProcessing);
                if (formattedPhysicianOrders.IsNotNullOrEmpty())
                {
                    orders.AddRange(formattedPhysicianOrders);
                }
                var formattedPlanofCares = PlanofCareOrderHelper(planofCareOrders, physicians, needPrintUrl, OrderStatusGroup.StillProcessing);
                if (formattedPlanofCares.IsNotNullOrEmpty())
                {
                    orders.AddRange(formattedPlanofCares);
                }
                var formattedPatientVisitNotes = PatientVisitNoteHelper(evalOrders, physicians, needPrintUrl, OrderStatusGroup.StillProcessing);
                if (formattedPatientVisitNotes.IsNotNullOrEmpty())
                {
                    orders.AddRange(formattedPatientVisitNotes);
                }

                var formattedFaceToFaceEncounters = FaceToFaceEncounterHelper(faceToFaceEncounters, physicians, needPrintUrl, OrderStatusGroup.StillProcessing);
                if (formattedFaceToFaceEncounters.IsNotNullOrEmpty())
                {
                    orders.AddRange(formattedFaceToFaceEncounters);
                }

                //var schedules = scheduleRepository.GetCurrentAndPreviousOrders(Current.AgencyId, episode, DisciplineTaskFactory.BillingOrders());
                //if (schedules != null && schedules.Count > 0)
                //{
                //    orders = GetOrdersFromScheduleEvents(episode.PatientId, episode.StartDate, episode.EndDate, schedules);
                //}
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public List<Order> GetOrdersPendingSignature(Guid branchId, DateTime startDate, DateTime endDate, bool needPrintUrl)
        {
            var orders = GetOrdersHelper(branchId, new List<int> { (int)ScheduleStatus.OrderSentToPhysicianElectronically,(int)ScheduleStatus.OrderSentToPhysician }, new List<int> { (int)ScheduleStatus.EvalSentToPhysician,(int)ScheduleStatus.EvalSentToPhysicianElectronically }, startDate, endDate, needPrintUrl, OrderStatusGroup.Pending);


            //(s.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString()
            //                || s.Status == ((int)ScheduleStatus.OrderSentToPhysician).ToString()
            //                || s.Status == ((int)ScheduleStatus.EvalSentToPhysician).ToString()
            //                || s.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString()))
            //var schedules = patientRepository.GetPendingSignatureOrderScheduleEvents(Current.AgencyId, branchId, startDate, endDate);
            //if (schedules != null && schedules.Count > 0)
            //{
            //    var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
            //    if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
            //    {
            //        var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var physicianOrders = patientRepository.GetPendingPhysicianSignatureOrders(Current.AgencyId, physicianOrdersIds, startDate, endDate);
            //        if (physicianOrders != null && physicianOrders.Count > 0)
            //        {
            //            physicianOrders.ForEach(po =>
            //            {
            //                var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
            //                orders.Add(new Order
            //                {
            //                    Id = po.Id,
            //                    PatientId = po.PatientId,
            //                    EpisodeId = po.EpisodeId,
            //                    Type = OrderType.PhysicianOrder,
            //                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
            //                    Number = po.OrderNumber,
            //                    PatientName = po.DisplayName,
            //                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
            //                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                    SentDate = po.SentDate.ToShortDateString().ToZeroFilled(),
            //                    CreatedDate = po.OrderDateFormatted,
            //                    ReceivedDate = DateTime.Today,
            //                    PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
            //                    PhysicianSignatureDate = po.PhysicianSignatureDate
            //                });
            //            });
            //        }
            //    }

            //    var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
            //    if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
            //    {
            //        var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var planofCareOrders = planofCareRepository.GetPendingSignaturePlanofCares(Current.AgencyId, planofCareOrdersIds);

            //        if (planofCareOrders != null && planofCareOrders.Count > 0)
            //        {
            //            planofCareOrders.ForEach(poc =>
            //            {
            //                var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    AgencyPhysician physician = null;
            //                    if (!poc.PhysicianId.IsEmpty())
            //                    {
            //                        physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
            //                    }
            //                    else
            //                    {
            //                        if (poc.PhysicianData.IsNotNullOrEmpty())
            //                        {
            //                            var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
            //                            if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
            //                            {
            //                                physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
            //                            }
            //                        }
            //                    }
            //                    orders.Add(new Order
            //                    {
            //                        Id = poc.Id,
            //                        PatientId = poc.PatientId,
            //                        EpisodeId = poc.EpisodeId,
            //                        Type = OrderType.HCFA485,
            //                        Text = DisciplineTasks.HCFA485.GetDescription(),
            //                        Number = poc.OrderNumber,
            //                        PatientName = poc.PatientName,
            //                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
            //                        ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
            //                        SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
            //                        PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
            //                        PhysicianSignatureDate = poc.PhysicianSignatureDate
            //                    });
            //                }
            //            });
            //        }
            //    }

            //    var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
            //    if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
            //    {
            //        var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var planofCareStandAloneOrders = planofCareRepository.GetPendingSignaturePlanofCaresStandAlone(Current.AgencyId, planofCareStandAloneOrdersIds);
            //        if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
            //        {
            //            planofCareStandAloneOrders.ForEach(poc =>
            //            {
            //                var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    AgencyPhysician physician = null;
            //                    if (!poc.PhysicianId.IsEmpty())
            //                    {
            //                        physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
            //                    }
            //                    else
            //                    {
            //                        if (poc.PhysicianData.IsNotNullOrEmpty())
            //                        {
            //                            var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
            //                            if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
            //                            {
            //                                physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
            //                            }
            //                        }
            //                    }
            //                    orders.Add(new Order
            //                    {
            //                        Id = poc.Id,
            //                        PatientId = poc.PatientId,
            //                        EpisodeId = poc.EpisodeId,
            //                        Type = OrderType.HCFA485StandAlone,
            //                        Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
            //                        Number = poc.OrderNumber,
            //                        PatientName = poc.PatientName,
            //                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
            //                        ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
            //                        SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
            //                        PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
            //                        PhysicianSignatureDate = poc.PhysicianSignatureDate
            //                    });
            //                }
            //            });
            //        }
            //    }

            //    var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
            //    if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
            //    {
            //        var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var faceToFaceEncounters = patientRepository.GetPendingSignatureFaceToFaceEncounterOrders(Current.AgencyId, faceToFaceEncounterOrdersIds);

            //        if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
            //        {
            //            faceToFaceEncounters.ForEach(ffe =>
            //            {
            //                var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
            //                orders.Add(new Order
            //                {
            //                    Id = ffe.Id,
            //                    PatientId = ffe.PatientId,
            //                    EpisodeId = ffe.EpisodeId,
            //                    Type = OrderType.FaceToFaceEncounter,
            //                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
            //                    Number = ffe.OrderNumber,
            //                    PatientName = ffe.DisplayName,
            //                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
            //                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                    SentDate = ffe.SentDate.ToString("MM/dd/yyyy"),
            //                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy"),
            //                    ReceivedDate = DateTime.Today,
            //                    PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
            //                    PhysicianSignatureDate = ffe.SignatureDate

            //                });
            //            });
            //        }
            //    }

            //    var evalSchedules = schedules.Where(s =>
            //            s.DisciplineTask == (int)DisciplineTasks.PTEvaluation ||
            //            s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation ||
            //            s.DisciplineTask == (int)DisciplineTasks.PTReassessment ||
            //            s.DisciplineTask == (int)DisciplineTasks.SNPsychAssessment ||
            //            s.DisciplineTask == (int)DisciplineTasks.OTEvaluation ||
            //            s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation ||
            //            s.DisciplineTask == (int)DisciplineTasks.OTReassessment ||
            //            s.DisciplineTask == (int)DisciplineTasks.OTDischarge ||
            //            s.DisciplineTask == (int)DisciplineTasks.STEvaluation ||
            //            s.DisciplineTask == (int)DisciplineTasks.STReEvaluation ||
            //            s.DisciplineTask == (int)DisciplineTasks.MSWDischarge ||
            //            s.DisciplineTask == (int)DisciplineTasks.STDischarge ||
            //            s.DisciplineTask == (int)DisciplineTasks.MSWEvaluationAssessment ||
            //            s.DisciplineTask == (int)DisciplineTasks.PTDischarge ||
            //            s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary ||
            //            s.DisciplineTask == (int)DisciplineTasks.ThirtyDaySummary ||
            //            s.DisciplineTask == (int)DisciplineTasks.TenDaySummary ||
            //            s.DisciplineTask == (int)DisciplineTasks.PTPlanOfCare ||
            //            s.DisciplineTask == (int)DisciplineTasks.OTPlanOfCare ||
            //            s.DisciplineTask == (int)DisciplineTasks.STPlanOfCare).ToList();


            //    if (evalSchedules != null && evalSchedules.Count > 0)
            //    {
            //        var evalIds = evalSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int>() { (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically, (int)ScheduleStatus.OrderSentToPhysician, (int)ScheduleStatus.OrderSentToPhysicianElectronically }, evalIds, startDate, endDate);
            //        if (evalOrders != null && evalOrders.Count > 0)
            //        {
            //            evalOrders.ForEach(eval =>
            //            {
            //                var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
            //                orders.Add(new Order
            //                {
            //                    Id = eval.Id,
            //                    PatientId = eval.PatientId,
            //                    EpisodeId = eval.EpisodeId,
            //                    Type = GetOrderType(eval.NoteType),
            //                    Text = GetDisciplineType(eval.NoteType).GetDescription(),
            //                    Number = eval.OrderNumber,
            //                    PatientName = eval.DisplayName,
            //                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
            //                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                    SentDate = eval.SentDate.ToShortDateString().ToZeroFilled(),
            //                    CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
            //                    ReceivedDate = DateTime.Today,
            //                    PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
            //                    PhysicianSignatureDate = eval.PhysicianSignatureDate
            //                });
            //            });
            //        }
            //    }
            //}
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public List<Order> GetProcessedOrders(Guid BranchId, DateTime startDate, DateTime endDate, bool needPrintUrl)
        {
            var orders = GetOrdersHelper(BranchId, new List<int> { (int)ScheduleStatus.OrderReturnedWPhysicianSignature }, new List<int> { (int)ScheduleStatus.EvalReturnedWPhysicianSignature }, startDate, endDate, needPrintUrl, OrderStatusGroup.Complete);

            //var orders = new List<Order>();
            //var schedules = patientRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
            //if (schedules != null && schedules.Count > 0 && status.Count > 1)
            //{
            //    var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
            //    if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
            //    {
            //        var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, status[0], physicianOrdersIds, startDate, endDate);
            //        if (physicianOrders != null && physicianOrders.Count > 0)
            //        {
            //            physicianOrders.ForEach(po =>
            //            {
            //                var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
            //                orders.Add(new Order
            //                {
            //                    Id = po.Id,
            //                    PatientId = po.PatientId,
            //                    EpisodeId = po.EpisodeId,
            //                    Type = OrderType.PhysicianOrder,
            //                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
            //                    Number = po.OrderNumber,
            //                    PatientName = po.DisplayName,
            //                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                    PhysicianName = po.PhysicianName.IsNotNullOrEmpty() ? po.PhysicianName : physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
            //                    PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
            //                    CreatedDate = po.OrderDate.ToShortDateString().ToZeroFilled(),
            //                    ReceivedDate = po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate,
            //                    SendDate = po.SentDate,
            //                    PhysicianSignatureDate = po.PhysicianSignatureDate
            //                });
            //            });
            //        }
            //    }
            //    var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
            //    if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
            //    {
            //        var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, status[0], planofCareOrdersIds);
            //        if (planofCareOrders != null && planofCareOrders.Count > 0)
            //        {
            //            planofCareOrders.ForEach(poc =>
            //            {
            //                var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
            //                    orders.Add(new Order
            //                    {
            //                        Id = poc.Id,
            //                        PatientId = poc.PatientId,
            //                        EpisodeId = poc.EpisodeId,
            //                        Type = OrderType.HCFA485,
            //                        Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
            //                        Number = poc.OrderNumber,
            //                        PatientName = poc.PatientName,
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
            //                        PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
            //                        ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
            //                        SendDate = poc.SentDate,
            //                        PhysicianSignatureDate = poc.PhysicianSignatureDate
            //                    });
            //                }
            //            });
            //        }
            //    }

            //    var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
            //    if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
            //    {
            //        var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, status[0], planofCareStandAloneOrdersIds);
            //        if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
            //        {
            //            planofCareStandAloneOrders.ForEach(poc =>
            //            {
            //                var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
            //                    orders.Add(new Order
            //                    {
            //                        Id = poc.Id,
            //                        PatientId = poc.PatientId,
            //                        EpisodeId = poc.EpisodeId,
            //                        Type = OrderType.HCFA485StandAlone,
            //                        Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
            //                        Number = poc.OrderNumber,
            //                        PatientName = poc.PatientName,
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
            //                        ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
            //                        SendDate = poc.SentDate,
            //                        PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
            //                        PhysicianSignatureDate = poc.PhysicianSignatureDate
            //                    });
            //                }
            //            });
            //        }
            //    }

            //    var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
            //    if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
            //    {
            //        var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, status[0], faceToFaceEncounterOrdersIds);

            //        if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
            //        {
            //            faceToFaceEncounters.ForEach(ffe =>
            //            {
            //                var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.EventId == ffe.Id && s.EpisodeId == ffe.EpisodeId);

            //                if (evnt != null)
            //                {
            //                    var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
            //                    orders.Add(new Order
            //                    {
            //                        Id = ffe.Id,
            //                        PatientId = ffe.PatientId,
            //                        EpisodeId = ffe.EpisodeId,
            //                        Type = OrderType.FaceToFaceEncounter,
            //                        Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
            //                        Number = ffe.OrderNumber,
            //                        PatientName = ffe.DisplayName,
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                        PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
            //                        ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate,
            //                        SendDate = ffe.SentDate,
            //                        PhysicianSignatureDate = ffe.SignatureDate
            //                    });
            //                }
            //            });
            //        }
            //    }

            //    var evalOrdersSchedule = schedules.Where(s =>
            //            s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReassessment
            //            || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReassessment
            //            || s.DisciplineTask == (int)DisciplineTasks.STEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation || s.DisciplineTask == (int)DisciplineTasks.SNPsychAssessment
            //            || s.DisciplineTask == (int)DisciplineTasks.STDischarge || s.DisciplineTask == (int)DisciplineTasks.OTDischarge || s.DisciplineTask == (int)DisciplineTasks.MSWDischarge
            //            || s.DisciplineTask == (int)DisciplineTasks.PTDischarge || s.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary || s.DisciplineTask == (int)DisciplineTasks.ThirtyDaySummary || s.DisciplineTask == (int)DisciplineTasks.TenDaySummary
            //            || s.DisciplineTask == (int)DisciplineTasks.PTPlanOfCare || s.DisciplineTask == (int)DisciplineTasks.OTPlanOfCare || s.DisciplineTask == (int)DisciplineTasks.STPlanOfCare).ToList();
            //    if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
            //    {
            //        var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int> { status[1] }, evalOrdersIds, startDate, endDate);
            //        if (evalOrders != null && evalOrders.Count > 0)
            //        {
            //            evalOrders.ForEach(eval =>
            //            {
            //                var evnt = evalOrdersSchedule.FirstOrDefault(s => s.EventId == eval.Id && s.EpisodeId == eval.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
            //                    orders.Add(new Order
            //                    {
            //                        Id = eval.Id,
            //                        PatientId = eval.PatientId,
            //                        EpisodeId = eval.EpisodeId,
            //                        Type = GetOrderType(eval.NoteType),
            //                        Text = GetDisciplineType(eval.NoteType).GetDescription(),
            //                        Number = eval.OrderNumber,
            //                        PatientName = eval.DisplayName,
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                        PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
            //                        ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
            //                        SendDate = eval.SentDate,
            //                        PhysicianSignatureDate = eval.PhysicianSignatureDate
            //                    });
            //                }
            //            });
            //        }
            //    }
            //}
            return orders;
        }


        public List<Order> GetOrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime startDate, DateTime endDate, bool needPrintUrl)
        {
            var orders = GetOrdersHelper(BranchId, new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician }, new List<int> {  (int)ScheduleStatus.EvalToBeSentToPhysician }, startDate, endDate, needPrintUrl, OrderStatusGroup.Pending);
            //new List<Order>();
            //var status = new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician, (int)ScheduleStatus.EvalToBeSentToPhysician };
            //var physicianIds = new List<Guid>();
            //var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, BranchId, new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician }, startDate, endDate);
            //if (physicianOrders.IsNotNullOrEmpty())
            //{
            //    physicianIds.AddRange(physicianOrders.Select(p => p.PhysicianId));
            //}
            //var planofCareOrders = planofCareRepository.GetPlanofCareOrders(Current.AgencyId, BranchId, DisciplineTaskFactory.POC(), new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician }, startDate, endDate);
            //if (planofCareOrders.IsNotNullOrEmpty())
            //{
            //    physicianIds.AddRange(planofCareOrders.Select(p => p.PhysicianId));
            //}
            //var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, BranchId, DisciplineTaskFactory.NoteOrders(), new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician, (int)ScheduleStatus.EvalToBeSentToPhysician }, startDate, endDate);
            //if (evalOrders.IsNotNullOrEmpty())
            //{
            //    physicianIds.AddRange(evalOrders.Select(p => p.PhysicianId));
            //}
            //var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounters(Current.AgencyId, BranchId, new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician }, startDate, endDate);
            //if (faceToFaceEncounters.IsNotNullOrEmpty())
            //{
            //    physicianIds.AddRange(faceToFaceEncounters.Select(p => p.PhysicianId));
            //}
            //var physicians = new List<AgencyPhysician>();
            //physicianIds = physicianIds.Where(Id => !Id.IsEmpty()).Distinct().ToList();
            //if (physicianIds.IsNotNullOrEmpty())
            //{
            //    physicians = PhysicianEngine.GetAgencyPhysicians(Current.AgencyId, physicianIds);
            //}

            //var formattedPhysicianOrders = PhysicianOrderHelper(physicianOrders, physicians, needPrintUrl, false);
            //if (formattedPhysicianOrders.IsNotNullOrEmpty())
            //{
            //    orders.AddRange(formattedPhysicianOrders);
            //}
            //var formattedPlanofCares = PlanofCareOrderHelper(planofCareOrders, physicians, needPrintUrl, false);
            //if (formattedPlanofCares.IsNotNullOrEmpty())
            //{
            //    orders.AddRange(formattedPlanofCares);
            //}
            //var formattedPatientVisitNotes = PatientVisitNoteHelper(evalOrders, physicians, needPrintUrl, false);
            //if (formattedPatientVisitNotes.IsNotNullOrEmpty())
            //{
            //    orders.AddRange(formattedPatientVisitNotes);
            //}

            //var formattedFaceToFaceEncounters = FaceToFaceEncounterHelper(faceToFaceEncounters, physicians, needPrintUrl, false);
            //if (formattedFaceToFaceEncounters.IsNotNullOrEmpty())
            //{
            //    orders.AddRange(formattedFaceToFaceEncounters);
            //}

            //var schedules = patientRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
            //if (schedules != null && schedules.Count > 0)
            //{
            //    var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
            //    if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
            //    {

            //        var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, physicianOrdersIds, startDate, endDate);
            //        if (physicianOrders != null && physicianOrders.Count > 0)
            //        {
            //            physicianOrders.ForEach(po =>
            //            {
            //                var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
            //                orders.Add(new Order
            //                {
            //                    Id = po.Id,
            //                    PatientId = po.PatientId,
            //                    EpisodeId = po.EpisodeId,
            //                    Type = OrderType.PhysicianOrder,
            //                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
            //                    Number = po.OrderNumber,
            //                    PatientName = po.DisplayName,
            //                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                    PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
            //                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                    CreatedDate = po.OrderDateFormatted

            //                });
            //            });
            //        }
            //    }
            //    var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
            //    if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
            //    {
            //        var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareOrdersIds);

            //        if (planofCareOrders != null && planofCareOrders.Count > 0)
            //        {
            //            planofCareOrders.ForEach(poc =>
            //            {
            //                var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    AgencyPhysician physician = null;
            //                    if (!poc.PhysicianId.IsEmpty())
            //                    {
            //                        physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
            //                    }
            //                    else
            //                    {
            //                        if (poc.PhysicianData.IsNotNullOrEmpty())
            //                        {
            //                            var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
            //                            if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
            //                            {
            //                                physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
            //                            }
            //                        }
            //                    }
            //                    orders.Add(new Order
            //                    {
            //                        Id = poc.Id,
            //                        PatientId = poc.PatientId,
            //                        EpisodeId = poc.EpisodeId,
            //                        Type = OrderType.HCFA485,
            //                        Text = DisciplineTasks.HCFA485.GetDescription(),
            //                        Number = poc.OrderNumber,
            //                        PatientName = poc.PatientName,
            //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                        PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty
            //                    });
            //                }
            //            });
            //        }
            //    }

            //    var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
            //    if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
            //    {
            //        var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareStandAloneOrdersIds);

            //        if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
            //        {
            //            planofCareStandAloneOrders.ForEach(poc =>
            //            {
            //                var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    AgencyPhysician physician = null;
            //                    //var patient = patientRepository.GetPatientOnly(poc.PatientId, Current.AgencyId);
            //                    if (!poc.PhysicianId.IsEmpty())
            //                    {
            //                        physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
            //                    }
            //                    else
            //                    {
            //                        if (poc.PhysicianData.IsNotNullOrEmpty())
            //                        {
            //                            var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
            //                            if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
            //                            {
            //                                physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
            //                            }
            //                        }
            //                    }
            //                    orders.Add(new Order
            //                    {
            //                        Id = poc.Id,
            //                        PatientId = poc.PatientId,
            //                        EpisodeId = poc.EpisodeId,
            //                        Type = OrderType.HCFA485StandAlone,
            //                        Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
            //                        Number = poc.OrderNumber,
            //                        PatientName = poc.PatientName,// patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
            //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                        PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty
            //                    });
            //                }
            //            });
            //        }
            //    }

            //    var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
            //    if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
            //    {
            //        var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, faceToFaceEncounterOrdersIds);
            //        if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
            //        {
            //            faceToFaceEncounters.ForEach(ffe =>
            //            {
            //                var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.EventId == ffe.Id && s.EpisodeId == ffe.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    AgencyPhysician physician = null;
            //                    if (!ffe.PhysicianId.IsEmpty())
            //                    {
            //                        physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
            //                    }
            //                    else
            //                    {
            //                        if (ffe.PhysicianData.IsNotNullOrEmpty())
            //                        {
            //                            var oldPhysician = ffe.PhysicianData.ToObject<AgencyPhysician>();
            //                            if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
            //                            {
            //                                physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
            //                            }
            //                        }
            //                    }
            //                    orders.Add(new Order
            //                    {
            //                        Id = ffe.Id,
            //                        PatientId = ffe.PatientId,
            //                        EpisodeId = ffe.EpisodeId,
            //                        Type = OrderType.FaceToFaceEncounter,
            //                        Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
            //                        Number = ffe.OrderNumber,
            //                        PatientName = ffe.DisplayName,
            //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
            //                        CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy")
            //                    });
            //                }
            //            });
            //        }
            //    }
            //    var evalOrdersSchedule = schedules.Where(s => DisciplineTaskFactory.NoteOrders().Contains(s.DisciplineTask) && status.Contains(s.Status.ToInteger())).ToList();

            //    if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
            //    {
            //        var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, evalOrdersIds);
            //        if (evalOrders != null && evalOrders.Count > 0)
            //        {
            //            evalOrders.ForEach(eval =>
            //            {
            //                var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
            //                orders.Add(new Order
            //                {
            //                    Id = eval.Id,
            //                    PatientId = eval.PatientId,
            //                    EpisodeId = eval.EpisodeId,
            //                    Type = GetOrderType(eval.NoteType),
            //                    Text = GetDisciplineType(eval.NoteType).GetDescription(),
            //                    Number = eval.OrderNumber,
            //                    PatientName = eval.DisplayName,
            //                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                    PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
            //                    CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
            //                    ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
            //                    SendDate = eval.SentDate
            //                });
            //            });
            //        }
            //    }
            //}
            return orders.Where(o => o.PhysicianAccess == sendAutomatically).OrderByDescending(o => o.CreatedDate).ToList();
        }

        public List<Order> GetOrdersHelper(Guid BranchId, List<int> noneVisitNoteStatus, List<int> visitNoteStatus, DateTime startDate, DateTime endDate, bool needPrintUrl, OrderStatusGroup orderStatusGroup)
        {
            var orders = new List<Order>();
            var physicianIds = new List<Guid>();
            var branches = !BranchId.IsEmpty() ? new List<Guid> { BranchId } : Current.LocationIds;
            if (branches.IsNotNullOrEmpty())
            {
                var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, branches, noneVisitNoteStatus, startDate, endDate);
                if (physicianOrders.IsNotNullOrEmpty())
                {
                    physicianIds.AddRange(physicianOrders.Select(p => p.PhysicianId));
                }
                var planofCareOrders = planofCareRepository.GetPlanofCareOrders(Current.AgencyId, branches, DisciplineTaskFactory.POC(), noneVisitNoteStatus, startDate, endDate);
                if (planofCareOrders.IsNotNullOrEmpty())
                {
                    physicianIds.AddRange(planofCareOrders.Select(p => p.PhysicianId));
                }
                var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, branches, DisciplineTaskFactory.NoteOrders(), visitNoteStatus, startDate, endDate);
                if (evalOrders.IsNotNullOrEmpty())
                {
                    physicianIds.AddRange(evalOrders.Select(p => p.PhysicianId));
                }
                var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounters(Current.AgencyId, branches, noneVisitNoteStatus, startDate, endDate);
                if (faceToFaceEncounters.IsNotNullOrEmpty())
                {
                    physicianIds.AddRange(faceToFaceEncounters.Select(p => p.PhysicianId));
                }
                var physicians = new List<AgencyPhysician>();
                physicianIds = physicianIds.Where(Id => !Id.IsEmpty()).Distinct().ToList();
                if (physicianIds.IsNotNullOrEmpty())
                {
                    physicians = PhysicianEngine.GetAgencyPhysicians(Current.AgencyId, physicianIds);
                }

                var formattedPhysicianOrders = PhysicianOrderHelper(physicianOrders, physicians, needPrintUrl, orderStatusGroup);
                if (formattedPhysicianOrders.IsNotNullOrEmpty())
                {
                    orders.AddRange(formattedPhysicianOrders);
                }
                var formattedPlanofCares = PlanofCareOrderHelper(planofCareOrders, physicians, needPrintUrl, orderStatusGroup);
                if (formattedPlanofCares.IsNotNullOrEmpty())
                {
                    orders.AddRange(formattedPlanofCares);
                }
                var formattedPatientVisitNotes = PatientVisitNoteHelper(evalOrders, physicians, needPrintUrl, orderStatusGroup);
                if (formattedPatientVisitNotes.IsNotNullOrEmpty())
                {
                    orders.AddRange(formattedPatientVisitNotes);
                }

                var formattedFaceToFaceEncounters = FaceToFaceEncounterHelper(faceToFaceEncounters, physicians, needPrintUrl, orderStatusGroup);
                if (formattedFaceToFaceEncounters.IsNotNullOrEmpty())
                {
                    orders.AddRange(formattedFaceToFaceEncounters);
                }
            }

            //var schedules = patientRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
            //if (schedules != null && schedules.Count > 0)
            //{
            //    var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
            //    if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
            //    {

            //        var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, physicianOrdersIds, startDate, endDate);
            //        if (physicianOrders != null && physicianOrders.Count > 0)
            //        {
            //            physicianOrders.ForEach(po =>
            //            {
            //                var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
            //                orders.Add(new Order
            //                {
            //                    Id = po.Id,
            //                    PatientId = po.PatientId,
            //                    EpisodeId = po.EpisodeId,
            //                    Type = OrderType.PhysicianOrder,
            //                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
            //                    Number = po.OrderNumber,
            //                    PatientName = po.DisplayName,
            //                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                    PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
            //                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                    CreatedDate = po.OrderDateFormatted

            //                });
            //            });
            //        }
            //    }
            //    var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
            //    if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
            //    {
            //        var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareOrdersIds);

            //        if (planofCareOrders != null && planofCareOrders.Count > 0)
            //        {
            //            planofCareOrders.ForEach(poc =>
            //            {
            //                var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    AgencyPhysician physician = null;
            //                    if (!poc.PhysicianId.IsEmpty())
            //                    {
            //                        physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
            //                    }
            //                    else
            //                    {
            //                        if (poc.PhysicianData.IsNotNullOrEmpty())
            //                        {
            //                            var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
            //                            if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
            //                            {
            //                                physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
            //                            }
            //                        }
            //                    }
            //                    orders.Add(new Order
            //                    {
            //                        Id = poc.Id,
            //                        PatientId = poc.PatientId,
            //                        EpisodeId = poc.EpisodeId,
            //                        Type = OrderType.HCFA485,
            //                        Text = DisciplineTasks.HCFA485.GetDescription(),
            //                        Number = poc.OrderNumber,
            //                        PatientName = poc.PatientName,
            //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                        PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty
            //                    });
            //                }
            //            });
            //        }
            //    }

            //    var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
            //    if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
            //    {
            //        var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareStandAloneOrdersIds);

            //        if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
            //        {
            //            planofCareStandAloneOrders.ForEach(poc =>
            //            {
            //                var evnt = planofCareStandAloneOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id && s.EpisodeId == poc.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    AgencyPhysician physician = null;
            //                    //var patient = patientRepository.GetPatientOnly(poc.PatientId, Current.AgencyId);
            //                    if (!poc.PhysicianId.IsEmpty())
            //                    {
            //                        physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
            //                    }
            //                    else
            //                    {
            //                        if (poc.PhysicianData.IsNotNullOrEmpty())
            //                        {
            //                            var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
            //                            if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
            //                            {
            //                                physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
            //                            }
            //                        }
            //                    }
            //                    orders.Add(new Order
            //                    {
            //                        Id = poc.Id,
            //                        PatientId = poc.PatientId,
            //                        EpisodeId = poc.EpisodeId,
            //                        Type = OrderType.HCFA485StandAlone,
            //                        Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
            //                        Number = poc.OrderNumber,
            //                        PatientName = poc.PatientName,// patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
            //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                        PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty
            //                    });
            //                }
            //            });
            //        }
            //    }

            //    var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
            //    if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
            //    {
            //        var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, faceToFaceEncounterOrdersIds);
            //        if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
            //        {
            //            faceToFaceEncounters.ForEach(ffe =>
            //            {
            //                var evnt = faceToFaceEncounterSchedules.FirstOrDefault(s => s.EventId == ffe.Id && s.EpisodeId == ffe.EpisodeId);
            //                if (evnt != null)
            //                {
            //                    AgencyPhysician physician = null;
            //                    if (!ffe.PhysicianId.IsEmpty())
            //                    {
            //                        physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
            //                    }
            //                    else
            //                    {
            //                        if (ffe.PhysicianData.IsNotNullOrEmpty())
            //                        {
            //                            var oldPhysician = ffe.PhysicianData.ToObject<AgencyPhysician>();
            //                            if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
            //                            {
            //                                physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
            //                            }
            //                        }
            //                    }
            //                    orders.Add(new Order
            //                    {
            //                        Id = ffe.Id,
            //                        PatientId = ffe.PatientId,
            //                        EpisodeId = ffe.EpisodeId,
            //                        Type = OrderType.FaceToFaceEncounter,
            //                        Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
            //                        Number = ffe.OrderNumber,
            //                        PatientName = ffe.DisplayName,
            //                        PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                        PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                        PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
            //                        CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy")
            //                    });
            //                }
            //            });
            //        }
            //    }
            //    var evalOrdersSchedule = schedules.Where(s => DisciplineTaskFactory.NoteOrders().Contains(s.DisciplineTask) && status.Contains(s.Status.ToInteger())).ToList();

            //    if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
            //    {
            //        var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, evalOrdersIds);
            //        if (evalOrders != null && evalOrders.Count > 0)
            //        {
            //            evalOrders.ForEach(eval =>
            //            {
            //                var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
            //                orders.Add(new Order
            //                {
            //                    Id = eval.Id,
            //                    PatientId = eval.PatientId,
            //                    EpisodeId = eval.EpisodeId,
            //                    Type = GetOrderType(eval.NoteType),
            //                    Text = GetDisciplineType(eval.NoteType).GetDescription(),
            //                    Number = eval.OrderNumber,
            //                    PatientName = eval.DisplayName,
            //                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
            //                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
            //                    PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
            //                    CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
            //                    ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
            //                    SendDate = eval.SentDate
            //                });
            //            });
            //        }
            //    }
            //}
            return orders;
        }


        private List<Order> PhysicianOrderHelper(IList<PhysicianOrder> physicianOrders, IList<AgencyPhysician> physicians, bool needPrintUrl, OrderStatusGroup orderStatusGroup)
        {
            var orders = new List<Order>();
            if (physicianOrders != null && physicianOrders.Count > 0)
            {
                physicianOrders.ForEach(po =>
                {
                    var physician = physicians.FirstOrDefault(p => p.Id == po.PhysicianId);// PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);

                    var order = new Order
                     {
                         Id = po.Id,
                         PatientId = po.PatientId,
                         EpisodeId = po.EpisodeId,
                         Type = OrderType.PhysicianOrder,
                         Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                         Number = po.OrderNumber,
                         PatientName = po.DisplayName,
                         PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                         PhysicianAccess = physician != null && physician.PhysicianAccess,
                         CreatedDate = po.OrderDateFormatted

                     };
                    if (needPrintUrl)
                    {
                        order.PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true);
                    }
                    if (OrderStatusGroup.Pending == orderStatusGroup)
                    {
                        order.SentDate = po.SentDate.ToShortDateString().ToZeroFilled();
                        order.ReceivedDate = DateTime.Today;
                        order.PhysicianSignatureDate = po.PhysicianSignatureDate;
                    }
                    else if (OrderStatusGroup.Complete == orderStatusGroup)
                    {
                        order.ReceivedDate = po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate;
                        order.SendDate = po.SentDate;
                        order.PhysicianSignatureDate = po.PhysicianSignatureDate;
                    }
                    else if (OrderStatusGroup.StillProcessing == orderStatusGroup)
                    {
                        order.ReceivedDate = po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature ? po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate : DateTime.MinValue;
                        order.SendDate = po.Status == (int)ScheduleStatus.OrderSentToPhysician || po.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || po.Status == (int)ScheduleStatus.OrderSavedByPhysician ? po.SentDate : DateTime.MinValue;
                        order.Status = po.Status;
                        if (needPrintUrl)
                        {
                            order.PrintUrl += !Current.IfOnlyRole(AgencyRoles.Auditor) ? string.Format(" | <a href=\"javaScript:void(0);\" onclick=\"Patient.DeleteOrder('{0}','{1}','{2}');\">Delete</a>", po.Id, po.PatientId, po.EpisodeId) : string.Empty;
                        }
                    }
                    orders.Add(order);
                });
            }
            return orders;
        }

        private List<Order> PlanofCareOrderHelper(IList<PlanofCare> planofCareOrders, IList<AgencyPhysician> physicians, bool needPrintUrl, OrderStatusGroup orderStatusGroup)
        {
            var orders = new List<Order>();
            if (planofCareOrders != null && planofCareOrders.Count > 0)
            {
                planofCareOrders.ForEach(poc =>
                {
                    var physician = physicians.FirstOrDefault(p => p.Id == poc.PhysicianId);// PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                    var order = new Order
                      {
                          Id = poc.Id,
                          PatientId = poc.PatientId,
                          EpisodeId = poc.EpisodeId,
                          Type = poc.IsStandAlone ? OrderType.HCFA485StandAlone : OrderType.HCFA485,
                          Text = poc.IsStandAlone ? DisciplineTasks.HCFA485StandAlone.GetDescription() : DisciplineTasks.HCFA485.GetDescription(),
                          Number = poc.OrderNumber,
                          PatientName = poc.PatientName,
                          PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                          PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                          CreatedDate = poc.Created.IsValid() ? poc.Created.ToString("MM/dd/yyyy") : string.Empty
                      };
                    if (needPrintUrl)
                    {
                        order.PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, poc.IsStandAlone ? DisciplineTasks.HCFA485StandAlone : DisciplineTasks.HCFA485, poc.Status, true);
                    }
                    if (OrderStatusGroup.Pending == orderStatusGroup)
                    {
                        order.ReceivedDate = DateTime.Today;
                        order.SentDate = poc.SentDate.ToShortDateString().ToZeroFilled();
                        order.PhysicianSignatureDate = poc.PhysicianSignatureDate;
                    }
                    else if (OrderStatusGroup.Complete == orderStatusGroup)
                    {
                        order.ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate;
                        order.SendDate = poc.SentDate;
                        order.PhysicianSignatureDate = poc.PhysicianSignatureDate;
                    }
                    else if (OrderStatusGroup.StillProcessing == orderStatusGroup)
                    {
                        order.ReceivedDate = poc.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) ? (poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate) : DateTime.MinValue;
                        order.SendDate = poc.Status == ((int)ScheduleStatus.OrderSentToPhysician) || poc.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically) || poc.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) || poc.Status == ((int)ScheduleStatus.OrderSavedByPhysician) ? poc.SentDate : DateTime.MinValue;
                        order.Status = poc.Status;
                    }
                    orders.Add(order);
                });
            }
            return orders;
        }

        private List<Order> FaceToFaceEncounterHelper(IList<FaceToFaceEncounter> faceToFaceEncounters, IList<AgencyPhysician> physicians, bool needPrintUrl, OrderStatusGroup orderStatusGroup)
        {
            var orders = new List<Order>();
            if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
            {
                faceToFaceEncounters.ForEach(ffe =>
                {

                    var physician = physicians.FirstOrDefault(p => p.Id == ffe.PhysicianId);//  var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                    var order = new Order
                         {
                             Id = ffe.Id,
                             PatientId = ffe.PatientId,
                             EpisodeId = ffe.EpisodeId,
                             Type = OrderType.FaceToFaceEncounter,
                             Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                             Number = ffe.OrderNumber,
                             PatientName = ffe.DisplayName,
                             PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                             PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                             CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy")


                         };
                    if (needPrintUrl)
                    {
                        order.PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true);
                    }
                    if (OrderStatusGroup.Pending == orderStatusGroup)
                    {
                        order.SentDate = ffe.SentDate.ToString("MM/dd/yyyy");
                        order.ReceivedDate = DateTime.Today;
                        order.PhysicianSignatureDate = ffe.SignatureDate;
                    }
                    else if (OrderStatusGroup.Complete == orderStatusGroup)
                    {
                        order.ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate;
                        order.SendDate = ffe.SentDate;
                        order.PhysicianSignatureDate = ffe.SignatureDate;
                    }
                    else if (OrderStatusGroup.StillProcessing == orderStatusGroup)
                    {

                        order.CreatedDate = ffe.Created.IsValid() ? ffe.Created.ToString("MM/dd/yyyy") : string.Empty;
                        order.ReceivedDate = ffe.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) ? ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate : DateTime.MinValue;
                        order.SendDate = ffe.Status == ((int)ScheduleStatus.OrderSentToPhysician) || ffe.Status == ((int)ScheduleStatus.OrderSentToPhysicianElectronically) || ffe.Status == ((int)ScheduleStatus.OrderReturnedWPhysicianSignature) || ffe.Status == ((int)ScheduleStatus.OrderSavedByPhysician) ? ffe.RequestDate : DateTime.MinValue;
                        order.Status = ffe.Status;
                    }
                    orders.Add(order);
                });
            }
            return orders;
        }

        private List<Order> PatientVisitNoteHelper(IList<PatientVisitNote> evalOrders, IList<AgencyPhysician> physicians, bool needPrintUrl, OrderStatusGroup orderStatusGroup)
        {
            var orders = new List<Order>();
            if (evalOrders != null && evalOrders.Count > 0)
            {
                evalOrders.ForEach(eval =>
                {
                    var physician = physicians.FirstOrDefault(p => p.Id == eval.PhysicianId);// PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                    var order = new Order
                      {
                          Id = eval.Id,
                          PatientId = eval.PatientId,
                          EpisodeId = eval.EpisodeId,
                          Type = GetOrderType(eval.NoteType),
                          Text = GetDisciplineType(eval.NoteType).GetDescription(),
                          Number = eval.OrderNumber,
                          PatientName = eval.DisplayName,
                          PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                          PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                          CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled()
                      };
                    if (needPrintUrl)
                    {
                        order.PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true);
                    }
                    if (OrderStatusGroup.Pending == orderStatusGroup)
                    {
                        order.SentDate = eval.SentDate.ToShortDateString().ToZeroFilled();
                        order.PhysicianSignatureDate = eval.PhysicianSignatureDate;
                        order.ReceivedDate = DateTime.Today;
                    }
                    else if (OrderStatusGroup.Complete == orderStatusGroup)
                    {
                        order.SentDate = eval.SentDate.ToShortDateString().ToZeroFilled();
                        order.PhysicianSignatureDate = eval.PhysicianSignatureDate;
                        order.ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate;
                    }
                    else if (OrderStatusGroup.StillProcessing == orderStatusGroup)
                    {
                        order.CreatedDate = eval.Created.IsValid() ? eval.Created.ToString("MM/dd/yyyy") : string.Empty;
                        order.ReceivedDate = eval.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature) ? eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate : DateTime.MinValue;
                        order.SendDate = eval.Status == ((int)ScheduleStatus.EvalSentToPhysician) || eval.Status == ((int)ScheduleStatus.EvalSentToPhysicianElectronically) || eval.Status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature) || eval.Status == ((int)ScheduleStatus.OrderSavedByPhysician) ? eval.SentDate : DateTime.MinValue;
                        order.Status = eval.Status;
                    }
                   
                    orders.Add(order);
                });
            }
            return orders;
        }


        public PrintViewData<PhysicianOrder> GetOrderPrint()
        {
            return new PrintViewData<PhysicianOrder> { Data = new PhysicianOrder(), LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId) };
        }

        public PrintViewData<PhysicianOrder> GetOrderPrint(Guid patientId, Guid orderId)
        {
            return GetOrderPrint(patientId, orderId, Current.AgencyId);
        }

        public PrintViewData<PhysicianOrder> GetOrderPrint(Guid patientId, Guid orderId, Guid agencyId)
        {
            var viewData = new PrintViewData<PhysicianOrder> { Data = patientRepository.GetOrder(orderId, patientId, agencyId) ?? new PhysicianOrder() };
            viewData.PatientProfile = patientRepository.GetPatientPrintProfileWithAddress(Current.AgencyId, patientId);
            viewData.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, viewData.PatientProfile.AgencyLocationId);
            if ((viewData.Data.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || viewData.Data.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || viewData.Data.Status == (int)ScheduleStatus.OrderSentToPhysician || viewData.Data.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || viewData.Data.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !viewData.Data.PhysicianId.IsEmpty() && viewData.Data.PhysicianData.IsNotNullOrEmpty())
            {
                var physician = viewData.Data.PhysicianData.ToObject<AgencyPhysician>();
                viewData.Data.Physician = physician ?? PhysicianEngine.Get(viewData.Data.PhysicianId, agencyId);
            }
            else
            {
                viewData.Data.Physician = PhysicianEngine.Get(viewData.Data.PhysicianId, agencyId);
            }
            var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, agencyId);
            if (allergyProfile != null)
            {
                viewData.Data.Allergies = allergyProfile.ToString();
            }
            var episode = episodeRepository.GetEpisodeByIdWithSOC(agencyId, viewData.Data.EpisodeId, viewData.Data.PatientId);
            if (episode != null)
            {
                if (viewData.Data.IsOrderForNextEpisode)
                {
                    viewData.Data.EpisodeStartDate = episode.EndDate.AddDays(1).ToShortDateString();
                    viewData.Data.EpisodeEndDate = episode.EndDate.AddDays(60).ToShortDateString();
                }
                else
                {
                    viewData.Data.EpisodeEndDate = episode.EndDateFormatted;
                    viewData.Data.EpisodeStartDate = episode.StartDateFormatted;
                }
                if (viewData.PatientProfile != null)
                {
                    viewData.PatientProfile.StartofCareDate = episode.StartOfCareDate;
                }
            }
            return viewData;
        }


        public PrintViewData<FaceToFaceEncounter> GetFaceToFacePrint()
        {
            var viewData = new PrintViewData<FaceToFaceEncounter> { Data = new FaceToFaceEncounter() };
            viewData.LocationProfile = agencyRepository.AgencyNameWithMainLocationAddress(Current.AgencyId);
            return viewData;
        }

        public PrintViewData<FaceToFaceEncounter> GetFaceToFacePrint(Guid patientId, Guid orderId)
        {
            return GetFaceToFacePrint(patientId, orderId, Current.AgencyId);
        }

        public PrintViewData<FaceToFaceEncounter> GetFaceToFacePrint(Guid patientId, Guid orderId, Guid agencyId)
        {
            var viewData = new PrintViewData<FaceToFaceEncounter>();
            viewData.PatientProfile = patientRepository.GetPatientPrintProfileWithAddress(agencyId, patientId);
            if (viewData.PatientProfile != null)
            {
                viewData.Data = patientRepository.GetFaceToFaceEncounter(orderId, patientId, agencyId) ?? new FaceToFaceEncounter();
                if (viewData.Data != null)
                {
                    if ((viewData.Data.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || viewData.Data.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || viewData.Data.Status == (int)ScheduleStatus.OrderSentToPhysician || viewData.Data.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || viewData.Data.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !viewData.Data.PhysicianId.IsEmpty() && viewData.Data.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = viewData.Data.PhysicianData.ToObject<AgencyPhysician>();
                        viewData.Data.Physician = physician ?? PhysicianEngine.Get(viewData.Data.PhysicianId, agencyId);
                    }
                    else
                    {
                        viewData.Data.Physician = PhysicianEngine.Get(viewData.Data.PhysicianId, agencyId);
                    }
                    var episode = episodeRepository.GetEpisodeByIdWithSOC(agencyId, viewData.Data.EpisodeId, viewData.Data.PatientId);
                    if (episode != null)
                    {
                        viewData.Data.EpisodeEndDate = episode.EndDateFormatted;
                        viewData.Data.EpisodeStartDate = episode.StartDateFormatted;
                        if (viewData.PatientProfile != null)
                        {
                            viewData.PatientProfile.StartofCareDate = episode.StartOfCareDate;
                        }
                    }
                }
                viewData.LocationProfile = agencyRepository.AgencyNameWithLocationAddress(Current.AgencyId, viewData.PatientProfile.AgencyLocationId);
            }
            return viewData;
        }

        //public PlanofCare GetPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId)
        //{
        //    return GetPlanOfCarePrint(episodeId, patientId, eventId, Current.AgencyId);
        //}

        //public PlanofCare GetPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId, Guid agencyId)
        //{
        //    var isStandAlone = false;
        //    PlanofCare planofCare = planofCareRepository.Get(agencyId, patientId, eventId);
        //    //var planofCareStandAlone = planofCareRepository.Get(agencyId, patientId, eventId);
        //    //if (planofCareStandAlone != null)
        //    //{
        //    //    isStandAlone = true;
        //    //    planofCare = planofCareStandAlone.ToPlanofCare();
        //    //}
        //    //else
        //    //{
        //    //    planofCare = planofCareRepository.Get(agencyId, patientId, eventId);
        //    //}

        //    if (planofCare != null && planofCare.Data.IsNotNullOrEmpty())
        //    {
        //        isStandAlone = planofCare.IsStandAlone;
        //        planofCare.EpisodeId = episodeId;
        //        var agency = agencyRepository.GetWithBranches(planofCare.AgencyId);
        //        planofCare.AgencyData = agency != null ? agency.ToXml() : string.Empty;
        //        planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
        //        var patient = patientRepository.GetPatientOnly(planofCare.PatientId, planofCare.AgencyId) ?? new Patient();
        //        if (!planofCare.EpisodeId.IsEmpty())
        //        {
        //            var episode = episodeRepository.GetEpisodeOnly(agencyId, planofCare.EpisodeId, planofCare.PatientId);
        //            if (episode != null && !episode.AdmissionId.IsEmpty())
        //            {
        //                var admission = patientRepository.GetPatientAdmissionDate(agencyId, episode.AdmissionId);
        //                if (admission != null && admission.PatientData.IsNotNullOrEmpty() && admission.StartOfCareDate > DateTime.MinValue)
        //                {
        //                    if (planofCare.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || planofCare.Status == (int)ScheduleStatus.OrderSavedByPhysician || planofCare.Status == (int)ScheduleStatus.OrderSentToPhysician || planofCare.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || planofCare.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || planofCare.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
        //                    {
        //                        patient = admission.PatientData.ToObject<Patient>();
        //                    }
        //                    if (patient != null)
        //                    {
        //                        patient.StartofCareDate = admission.StartOfCareDate;
        //                    }
        //                }
        //            }
        //        }
        //        planofCare.PatientData = patient != null ? patient.ToXml() : string.Empty;
        //        if (!isStandAlone)
        //        {
        //            var episodeRange = GetPlanofCareCertPeriod(episodeId, patientId, planofCare.AssessmentId);
        //            if (episodeRange != null & episodeRange.StartDate != DateTime.MinValue & episodeRange.EndDate != DateTime.MinValue)
        //            {
        //                if (episodeRange.DateRangeDays > 59)
        //                {
        //                    planofCare.EpisodeEnd = episodeRange.EndDateFormatted;
        //                }
        //                else
        //                {
        //                    planofCare.EpisodeEnd = episodeRange.StartDate.AddDays(59).ToString("MM/dd/yyyy");
        //                }
        //                planofCare.EpisodeStart = episodeRange.StartDateFormatted;
        //            }
        //        }
        //        else
        //        {
        //            var answers = planofCare.ToDictionary();
        //            if (answers != null)
        //            {
        //                var episodeAssociatedId = answers.AnswerOrEmptyGuid("EpisodeAssociated");
        //                if (!episodeAssociatedId.IsEmpty())
        //                {
        //                    var episode = episodeRepository.GetEpisodeLean(agencyId, patientId, episodeAssociatedId);
        //                    if (episode != null & episode.StartDate != DateTime.MinValue & episode.EndDate != DateTime.MinValue)
        //                    {
        //                        if ((episode.EndDate - episode.StartDate).Days > 59)
        //                        {
        //                            planofCare.EpisodeEnd = episode.EndDateFormatted;
        //                        }
        //                        else
        //                        {
        //                            planofCare.EpisodeEnd = episode.StartDate.AddDays(59).ToString("MM/dd/yyyy");
        //                        }
        //                        planofCare.EpisodeStart = episode.StartDateFormatted;
        //                    }
        //                }
        //            }
        //        }
        //        if ((planofCare.Status == (int)ScheduleStatus.OrderSubmittedPendingReview || planofCare.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || planofCare.Status == (int)ScheduleStatus.OrderSentToPhysician || planofCare.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || planofCare.Status == (int)ScheduleStatus.OrderToBeSentToPhysician) && !planofCare.PhysicianId.IsEmpty() && planofCare.PhysicianData.IsNotNullOrEmpty())
        //        {
        //        }
        //        else
        //        {
        //            var physician = PhysicianEngine.Get(planofCare.PhysicianId, agencyId);
        //            if (physician != null)
        //            {
        //                planofCare.PhysicianData = physician.ToXml();
        //            }
        //        }
        //        var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, agencyId);
        //        if (allergyProfile != null)
        //        {
        //            planofCare.Allergies = allergyProfile.ToString();
        //        }
        //        bool ifAllergies = false;
        //        foreach (Question q in planofCare.Questions)
        //        {
        //            if (q.Name == "AllergiesDescription" && q.Answer.Trim().IsNotNullOrEmpty())
        //            {
        //                ifAllergies = true;
        //                break;
        //            }
        //        }
        //        if (!ifAllergies)
        //        {
        //            planofCare.Questions.RemoveAll(x => x.Name == "AllergiesDescription");
        //            Question allergiesDescription = new Question { Name = "AllergiesDescription", Answer = allergyProfile.ToString(), Type = QuestionType.PlanofCare };
        //            planofCare.Questions.Add(allergiesDescription);
        //        }

        //    }
        //    return planofCare;
        //}


        public IList<Order> GetPlanOfCareHistory(Guid branchCode, int status, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var disciplineTasks = DisciplineTaskFactory.POC().ToArray();
            var pocWithSchedules = planofCareRepository.GetPlanOfCareOrderFromScheduleEvents(Current.AgencyId, branchCode, startDate, endDate, disciplineTasks, status > 0 ? new int[] { status } : new int[] { });
            if (pocWithSchedules.IsNotNullOrEmpty())
            {
                var physicianIds = pocWithSchedules.Select(poc => poc.PhysicianId).Distinct().ToList();
                var physicians = PhysicianEngine.GetAgencyPhysicians(Current.AgencyId, physicianIds);
                pocWithSchedules.ForEach(poc =>
                {
                    AgencyPhysician physician = null;
                    if (poc.PhysicianData.IsNotNullOrEmpty())
                    {
                        physician = poc.PhysicianData.ToObject<AgencyPhysician>();
                    }
                    if (physician != null)
                    {
                        physician = physicians.FirstOrDefault(p => p.Id == poc.PhysicianId);
                    }
                    orders.Add(new Order
                    {
                        Id = poc.Id,
                        Type = poc.IsStandAlone ? OrderType.HCFA485StandAlone : OrderType.HCFA485,
                        Text = poc.IsStandAlone ? DisciplineTasks.HCFA485StandAlone.GetDescription() : (!poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription()),
                        Number = poc.OrderNumber,
                        PatientName = poc.PatientName,
                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                        CreatedDate = poc.Created.IsValid() ? poc.Created.ToString("MM/dd/yyyy") : string.Empty,
                        ReceivedDate = poc.ReceivedDate,
                        SendDate = poc.SentDate
                    });
                });
            }
            //var schedules = patientRepository.GetPlanOfCareOrderScheduleEvents(Current.AgencyId, branchCode, startDate, endDate, status);
            //if (schedules != null && schedules.Count > 0)
            //{
            //    var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
            //    if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
            //    {
            //        var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, planofCareOrdersIds);
            //        if (planofCareOrders != null && planofCareOrders.Count > 0)
            //        {
            //            planofCareOrders.ForEach(poc =>
            //            {
            //                var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
            //                var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id);
            //                if (evnt != null)
            //                {
            //                    orders.Add(new Order
            //                    {
            //                        Id = poc.Id,
            //                        Type = OrderType.HCFA485,
            //                        Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
            //                        Number = poc.OrderNumber,
            //                        PatientName = poc.PatientName,
            //                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
            //                        ReceivedDate =  poc.ReceivedDate,
            //                        SendDate = poc.SentDate
            //                    });
            //                }
            //            });
            //        }
            //    }

            //    var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
            //    if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
            //    {
            //        var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //        var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAlones(Current.AgencyId, planofCareStandAloneOrdersIds);
            //        if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
            //        {
            //            planofCareStandAloneOrders.ForEach(poc =>
            //            {
            //                var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
            //                if (evnt != null)
            //                {
            //                    var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
            //                    orders.Add(new Order
            //                    {
            //                        Id = poc.Id,
            //                        Type = OrderType.HCFA485StandAlone,
            //                        Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
            //                        Number = poc.OrderNumber,
            //                        PatientName = poc.PatientName,
            //                        PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
            //                        CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
            //                        ReceivedDate =  poc.ReceivedDate,
            //                        SendDate = poc.SentDate
            //                    });
            //                }
            //            });
            //        }
            //    }
            //}
            return orders.OrderBy(o => o.PatientName).ToList();
        }

        public IList<PhysicianOrder> GetPhysicianOrderHistory(Guid branchCode, int status, DateTime startDate, DateTime endDate)
        {
            var branches = !branchCode.IsEmpty() ? new List<Guid> { branchCode } : Current.LocationIds;
            var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, branches, status > 0 ? new List<int> { status } : null, startDate, endDate);
            if (physicianOrders.IsNotNullOrEmpty())
            {
                var physicians = new List<AgencyPhysician>();
                var physicianIds = physicianOrders.Where(p => !p.PhysicianId.IsEmpty()).Select(p => p.PhysicianId).Distinct().ToList();
                if (physicianIds.IsNotNullOrEmpty())
                {
                    physicians = PhysicianEngine.GetAgencyPhysicians(Current.AgencyId, physicianIds);
                }
                physicianOrders.ForEach(po =>
                {
                    var physician = physicians.FirstOrDefault(p => p.Id == po.PhysicianId);
                    if (physician != null)
                    {
                        po.PhysicianName = physician.DisplayName;
                    }
                    po.ReceivedDate = po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature ? po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate : DateTime.MinValue;
                    po.SentDate = po.Status == (int)ScheduleStatus.OrderSentToPhysician || po.Status == (int)ScheduleStatus.OrderSentToPhysicianElectronically || po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature || po.Status == (int)ScheduleStatus.OrderSavedByPhysician ? po.SentDate : DateTime.MinValue;
                });
            }
            //var branchPhysicianOrder = new List<PhysicianOrder>();
            //var schedules = patientRepository.GetPhysicianOrderScheduleEvents(Current.AgencyId, startDate, endDate, status);
            //if (schedules != null && schedules.Count > 0)
            //{
            //    var physicianOrdersIds = schedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
            //    branchPhysicianOrder = patientRepository.GetPhysicianOrders(Current.AgencyId, branchCode, physicianOrdersIds, startDate, endDate);
            //}
            return physicianOrders.OrderBy(o => o.DisplayName).ToList();
        }


    }
}
