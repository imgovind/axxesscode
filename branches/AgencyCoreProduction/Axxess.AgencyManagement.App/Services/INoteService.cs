﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.App.ViewData;
using Axxess.AgencyManagement.Domain;
using Axxess.AgencyManagement.App.iTextExtension;
using System.Web.Mvc;
using System.Web;
using Axxess.Core.Infrastructure;
using Axxess.LookUp.Domain;

namespace Axxess.AgencyManagement.App.Services
{
    public interface INoteService
    {
        PrintViewData<VisitNotePrintViewData> GetVisitNotePrint();
        PrintViewData<VisitNotePrintViewData> GetVisitNotePrint(String type);
        PrintViewData<VisitNotePrintViewData> GetVisitNotePrint(Guid episodeId, Guid patientId, Guid eventId);
        PrintViewData<VisitNotePrintViewData> GetVisitNotePrint(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        VisitNoteEditViewData GetTransportationNote(Guid episodeId, Guid patientId, Guid eventId);
        PrintViewData<VisitNotePrintViewData> GetTransportationNotePrint(Guid episodeId, Guid patientId, Guid eventId);
        MissedVisit GetMissedVisitPrint();
        MissedVisit GetMissedVisitPrint(Guid patientId, Guid eventId);
        AxxessPdf GetPrintPdf(ScheduleEvent scheduleEvent);
        JsonViewData SaveNotes(string button, FormCollection formCollection);
        bool SaveWoundCare(FormCollection formCollection, HttpFileCollectionBase httpFiles);
        bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId);
        bool ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId);
        List<NotesQuestion> GetPlanOfCareQuestions(Guid pocId, Guid agencyId);

        JsonViewData AddMissedVisit(MissedVisit missedVisit);
        MissedVisit GetMissedVisitWithPatientAndUserName(Guid Id);
        MissedVisit GetMissedVisitForEdit(Guid id);
        bool ProcessMissedVisitNotes(string button, Guid Id);
        JsonViewData RestoreMissedVisit(Guid episodeId, Guid patientId, Guid eventId);
        List<ScheduleEvent> GetMissedScheduledEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);

        bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        List<Supply> GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId);
        PrintViewData<VisitNotePrintViewData> GetPASVisitPrint(Guid patientId, Guid episodeId, Guid eventId);
        PrintViewData<VisitNotePrintViewData> GetPASTravelPrint(Guid patientId, Guid episodeId, Guid eventId);
        PrintViewData<VisitNotePrintViewData> GetPASCarePlanPrint(Guid patientId, Guid episodeId, Guid eventId);

    }
}
