﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.App.Enums;
    using System.Web.Mvc;

    public interface IOrderManagementService
    {
        List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId, bool needPrintUrl);
        PrintViewData<FaceToFaceEncounter> GetFaceToFacePrint();
        PrintViewData<FaceToFaceEncounter> GetFaceToFacePrint(Guid patientId, Guid orderId);
        PrintViewData<FaceToFaceEncounter> GetFaceToFacePrint(Guid patientId, Guid orderId, Guid agencyId);
        Order GetOrder(Guid id, Guid patientId, Guid episodeId, string type);
        PrintViewData<PhysicianOrder> GetOrderPrint(Guid patientId, Guid orderId);
        PrintViewData<PhysicianOrder> GetOrderPrint(Guid patientId, Guid orderId, Guid agencyId);
        PrintViewData<PhysicianOrder> GetOrderPrint();
        IList<PhysicianOrder> GetPhysicianOrderHistory(Guid branchCode, int status, DateTime startDate, DateTime endDate);
        bool ProcessPhysicianOrder(Guid episodeId, Guid patientId, Guid eventId, string actionType);
        List<Order> GetOrdersPendingSignature(Guid branchId, DateTime startDate, DateTime endDate, bool needPrintUrl);
        List<Order> GetOrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime startDate, DateTime endDate, bool needPrintUrl);
        List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate, bool needPrintUrl);
        //PlanofCare GetPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId, Guid agencyId);
        //PlanofCare GetPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId);
        IList<Order> GetPlanOfCareHistory(Guid branchCode, int Status, DateTime startDate, DateTime endDate);
        List<Order> GetProcessedOrders(Guid BranchId, DateTime startDate, DateTime endDate, bool needPrintUrl);
        void MarkOrderAsReturned(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate);
        bool MarkOrdersAsSent(FormCollection formCollection);
        bool UpdateOrderDates(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate);
    }
}
