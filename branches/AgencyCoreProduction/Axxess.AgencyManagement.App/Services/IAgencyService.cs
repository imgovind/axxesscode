﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    
    using Enums;
    using Domain;
    using ViewData;

    public interface IAgencyService
    {
        //Agency GetAgency(Guid Id);
        bool CreateAgency(Agency agency);
        bool CreateContact(AgencyContact contact);
        //List<UserVisit> GetSchedule();
        //List<PatientEpisodeEvent> GetPatientCaseManagerSchedule(Guid patientId);
        List<PatientEpisodeEvent> GetCaseManagerSchedule(Guid branchId, int status, DateTime startDate, DateTime endDate);
        bool CreateLocation(AgencyLocation location);
        //List<RecertEvent> GetRecertsPastDue();
        List<RecertEvent> GetRecertsPastDue(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate);
        List<RecertEvent> GetRecertsPastDueWidget();
        //List<RecertEvent> GetRecertsUpcoming();
        List<RecertEvent> GetRecertsUpcoming(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate);
        List<RecertEvent> GetRecertsUpcomingWidget();
        List<InsuranceViewData> GetInsurances();
       
        //List<Order> GetOrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime startDate, DateTime endDate);
        //List<Order> GetProcessedOrders(Guid BranchId, DateTime startDate, DateTime endDate, List<int> status);
        //Order GetOrder(Guid id, Guid patientId, Guid episodeId, string type);
        //List<Order> GetOrdersPendingSignature(Guid branchId, DateTime startDate, DateTime endDate);
        //bool MarkOrdersAsSent(FormCollection formCollection);
        //void MarkOrderAsReturned(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime dateReceived, DateTime physicianSignatureDate);
        //bool UpdateOrderDates(Guid id, Guid patientId, Guid episodeId, OrderType type, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate);
        
        
        List<AddressViewData> GetAgencyFullAddress();

        List<Infection> GetInfections(Guid agencyId);
        List<Incident> GetIncidents(Guid agencyId);
        bool ProcessInfections(string button, Guid patientId, Guid eventId);
        bool ProcessIncidents(string button, Guid patientId, Guid eventId);
        List<SelectListItem> Insurances(string value, bool IsAll, bool IsMedicareTradIncluded);
        List<SelectListItem> Branchs(string value, bool IsAll);
        //Infection GetInfectionReportPrint(Guid episodeId, Guid patientId, Guid eventId);
        PrintViewData<Infection> GetInfectionReportPrint(Guid agencyId, Guid patientId, Guid eventId);
        //Incident GetIncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId);
        //Incident GetIncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId, Guid agencyId);
        PrintViewData<Incident> GetIncidentReportPrint(Guid agencyId,Guid patientId, Guid eventId);
        //List<PatientEpisodeEvent> GetPrintQueue();
        List<PatientEpisodeEvent> GetPrintQueue(Guid BranchId, DateTime StartDate, DateTime EndDate, bool isPrintUrlNeeded);
        List<PatientSelection> GetTeamAccessPatients(Guid agencyId, Guid currentUserId);

        AgencyUpgradeResult Upgrade(AgencyUpgrade agencyUpgrade);

        IList<UserNonVisitTask> GetUserNonVisitTasks(Guid agencyId);
        Agency GetLocationWithAgencyInfo(Guid branchId);
        void Update(Agency existingAgency, string previousAgencyName);

        LocationPlanViewData GetAgencySubcriptionPlanDetails(Guid branchId);
    }
}
