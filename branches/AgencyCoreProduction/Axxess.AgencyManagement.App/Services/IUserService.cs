﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Infrastructure;

    public interface IUserService
    {
        bool CreateUser(User user);
        bool DeleteUser(Guid userId);
        List<UserVisit> GetScheduleLean(Guid userId, DateTime from, DateTime to);
        List<UserVisit> GetScheduleLeanAll(Guid userId, DateTime from, DateTime to);
        List<UserVisitWidget> GetScheduleWidget(Guid userId);
        bool IsEmailAddressInUse(string emailAddress);

        bool UpdateProfile(User user);
        bool IsPasswordCorrect(Guid userId, string password);
        bool IsSignatureCorrect(Guid userId, string signature);
        bool LoadUserRate(Guid fromId, Guid toId);
        bool AddLicense(License license, System.Web.HttpFileCollectionBase httpFiles);
        bool UpdatePermissions(FormCollection formCollection);
        bool DeleteLicense(Guid Id, Guid userId);
        bool UpdateLicense(License license);
        bool UpdateLicense(Guid Id, Guid userId, DateTime expirationDate, string LicenseNumber);
        bool UpdateLicense(Guid Id, Guid userId, DateTime initiationDate, DateTime expirationDate);
        IList<User> GetUserByBranchAndStatus(Guid branchId, int status);
        string GetUserLicensesExpirationNotification(Guid userId, string userName);
        IList<LicenseItem> GetUserLicenses();
        License GetUserLicense(Guid userId, Guid id);
        IList<License> GetUserLicenses(Guid userId, bool isAssetUrlNeeded);
        IList<License> GetUserLicenses(Guid branchId, int status);
        bool AddLicenseItem(LicenseItem licenseItem, System.Web.HttpFileCollectionBase httpFiles);
        User GetUserForEditPermissionAndRole(Guid userId);
        User GetUserForEditInformation(Guid Id);
        User GetUserProfileDisplayName(Guid userId);
        User GetForEdit(Guid id, Guid agencyId, string category);
        IList<UserRate> GetUserRates(Guid userId);
        bool RestoreUser(Guid userId);
        User GetUserOnly(Guid id, Guid agencyId);

        List<UserNonVisitTaskRate> GetUserNonVisitTaskRates(Guid AgencyId, Guid TaskId, Guid UserId);
        UserNonVisitTaskRate GetUserNonVisitTaskRate(Guid AgencyId, Guid TaskId, Guid UserId);

        SubscriptionPlanViewData GetAgencyUserSubcriptionPlanDetails();
        LicenseItem GetUserLicenseItem(Guid userId, Guid licenseId);
        JsonViewData AddUserRate(UserRate userRate);
        UserRate GetUserRateForEdit(Guid UserId, int Id, string Insurance);
        JsonViewData UpdateUserRate(UserRate userRate);
        JsonViewData DeleteUserRate(Guid UserId, int Id, string Insurance);

        JsonViewData AddUserNonVisitRate(UserNonVisitTaskRate userNonVisitTaskRate);
        UserNonVisitTaskRate GetUserNonVisitRateForEdit(Guid UserId, Guid Id);
        JsonViewData UpdateUserNonVisitRate(UserNonVisitTaskRate userNonVisitTaskRate);
        List<UserNonVisitTaskRate> GetUserNonVisitRateList(Guid UserId);
        JsonViewData DeleteUserNonVisitRate(Guid UserId, Guid Id);
    }
}
