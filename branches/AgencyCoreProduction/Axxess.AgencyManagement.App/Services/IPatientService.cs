﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Domain;
    using ViewData; 

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.LookUp.Domain;
    using Axxess.Core;
    using Axxess.Log.Enums;
    using Axxess.Log.Domain;
    using iTextExtension;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;

    public interface IPatientService
    {
        bool AddPatient(Patient patient);
        bool EditPatient(Patient patient);
        bool DeletePatient(Guid id);
        bool AdmitPatient(PendingPatient pending);
        bool NonAdmitPatient(PendingPatient pending);
        bool SetPatientPending(Guid patientId);
        bool DischargePatient(Guid patientId, DateTime dischargeDate, int dischargeReason, string dischargeReasonComments);
        bool DischargePatient(Guid patientId, Guid episodeId, DateTime dischargeDate, string dischargeReasonComments);
        //bool DischargePatient(Guid patientId, Guid episodeId, DateTime dischargeDate, int dischargeReason, string dischargeReasonComments);
        bool ActivatePatient(Guid patientId);
        bool ActivatePatient(Guid patientId, DateTime startOfCareDate);
        bool ReadmitPatient(PendingPatient pending);
        bool AddPhoto(Guid patientId, HttpFileCollectionBase httpFiles);
        bool UpdatePatientForPhotoRemove(Patient patient);

        bool AddPrimaryEmergencyContact(Patient patient);
        PatientAdmissionDate GetIfExitOrCreate(Guid patientId);
        bool NewEmergencyContact(PatientEmergencyContact emergencyContact, Guid patientId);
        bool EditEmergencyContact(PatientEmergencyContact emergencyContact);
        bool DeleteEmergencyContact(Guid id, Guid patientId);
        //bool CheckTimeOverlap(FormCollection formCollection, out string message);
       
        PatientProfile GetProfile(Guid id);
        string GetAllergies(Guid patientId);
        string GetAllergies(Guid patientId, Guid agencyId);

        bool CreateMedicationProfile(Patient patient, Guid medId);
        bool AddMedication(Guid medicationProfileId, Medication medication, string medicationType);
        bool UpdateMedication(Guid medicationProfileId, Medication medication, string medicationType);
        bool DeleteMedication(Guid medicationProfileId, Guid medicationId);
        bool UpdateMedicationStatus(Guid medicationProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate);
        bool SignMedicationHistory(MedicationProfileHistory medicationProfileHistory);
        IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId);
        List<Medication> GetCurrentMedicationsByCategory(Guid patientId, string medicationCategory);
        PrintViewData<MedicationProfileSnapshotViewData> GetMedicationProfilePrint(Guid id);
        PrintViewData<MedicationProfileSnapshotViewData> GetMedicationSnapshotPrint(Guid id);
        MedicationProfileViewData GetMedicationProfileViewData(Guid patientId);
        MedicationProfileViewData GetMedicationProfileSnapShot(Guid patientId);
        PrintViewData<AllergyProfileViewData> GetAllergyProfilePrint(Guid id);
        PrintViewData<DrugDrugInteractionsViewData> GetDrugDrugInteractionsPrint(Guid patientId, List<string> drugsSelected);
       
        bool LinkPhysicians(Patient patient);
        bool LinkPhysician(Guid patientId, Guid physicianId, bool isPrimary);
        bool UnlinkPhysician(Guid patientId, Guid physicianId);
        Guid GetPrimaryPhysicianId(Guid patientId, Guid agencyId);
        JsonViewData UpdateOrder(PhysicianOrder order);
       // void AddPhysicianOrderUserAndScheduleEvent(PhysicianOrder physicianOrder, out PhysicianOrder physicianOrderOut);
        //bool ProcessPhysicianOrder(Guid episodeId, Guid patientId, Guid eventId, string actionType);
        //List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate);
        //List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId);
        //JsonViewData DeletePhysicianOrder(Guid orderId, Guid patientId, Guid episodeId);
        //PhysicianOrder GetOrderPrint();
        //PhysicianOrder GetOrderPrint(Guid patientId, Guid orderId);
        //PhysicianOrder GetOrderPrint(Guid patientId, Guid orderId, Guid agencyId);

        //void AddCommunicationNoteUserAndScheduleEvent(CommunicationNote communicationNote, out CommunicationNote communicationNoteOut);
        CommunicationNote GetCommunicationNoteEdit(Guid patientId, Guid Id);
        JsonViewData UpdateCommunicationNote(CommunicationNote communicationNote);
        bool ProcessCommunicationNotes(string button, Guid patientId, Guid eventId);
        List<CommunicationNote> GetCommunicationNotes(Guid branchId, int patientStatus, DateTime startDate, DateTime endDate);
        List<CommunicationNote> GetCommunicationNotes(Guid patientId);
        //bool DeleteCommunicationNote(Guid Id, Guid patientId);
        CommunicationNote GetCommunicationNotePrint(Guid eventId, Guid patientId);
        CommunicationNote GetCommunicationNotePrint(Guid eventId, Guid patientId, Guid agencyId);
        string GetRelationshipDescription(string id);
        bool CreateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter, DateTime episodeStartDate, DateTime episodeEndDate);
        //FaceToFaceEncounter GetFaceToFacePrint();
        //FaceToFaceEncounter GetFaceToFacePrint(Guid patientId, Guid orderId);
        //FaceToFaceEncounter GetFaceToFacePrint(Guid patientId, Guid orderId, Guid agencyId);

        PatientEpisode CreateEpisode(Guid patientId, DateTime startDate);
        PatientEpisode CreateEpisode(Guid patientId, PatientEpisode episode);
        bool AddEpisode(PatientEpisode patientEpisode);
        PatientEpisode GetEditEpisode(Guid episodeId, Guid patientId);
        bool UpdateEpisode(PatientEpisode episode);
        JsonViewData ActivateEpisode(Guid episodeId, Guid patientId);
        List<EpisodeDateViewData> ActiveEpisodeRangeList(Guid patientId);
        List<EpisodeLean> GetPatientDeactivatedAndDischargedEpisodes(Guid patientId);
        bool IsEpisodeExist(Guid patientId, DateTime startDate, DateTime endDate);
        bool IsEpisodeExist(Guid patientId, DateTime startDate, DateTime endDate, Guid excludedEpisodeIdOptional);

        bool CreateEpisodeAndClaims(Patient patient, out Guid episodeId, out Guid face2faceId);
        void RemoveEpisodeAndClaims(Patient patient, Guid episodeId, Guid face2FaceId);
        void SetInsurance(Patient patient);
        PatientCenterViewData GetPatientCenterViewData(Guid patientId);
        //bool AddMultiDaySchedule(Guid episodeId, Guid patientId, Guid userId, int disciplineTaskId, string visitDates);
        JsonViewData AddMultiDaySchedule(Guid episodeId, Guid patientId, Guid userId, int disciplineTaskId, List<DateTime> visitDateArray);
        //JsonViewData UpdateEpisode(Guid episodeId, Guid patientId, List<ScheduleEvent> newTasks);
        JsonViewData AddMultipleSchedule(Guid patientId, Guid episodeId, List<ScheduleEvent> list);
       // bool UpdateEpisode(Guid episodeId, Guid patientId, string disciplineTask, string discipline, Guid userId, bool isBillable, DateTime startDate, DateTime endDate);
        JsonViewData AddMultiDateRangeScheduleTask(Guid episodeId, Guid patientId, int disciplineTaskId, Guid userId, string StartDate, string EndDate);
        JsonViewData UpdateScheduleEventsForVisitLog(Guid patientId, Guid episodeId, List<ScheduleEvent> scheduleEvents);
       // bool UpdateScheduleEvent(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles);
        //bool UpdateScheduleEventDetail(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles);
        ScheduleEvent GetScheduledEvent(Guid episodeId, Guid patientId, Guid eventId);
        JsonViewData UpdateScheduleEventDetails(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles);
        List<ScheduleEvent> GetScheduledEvents(Guid patientId, Guid episodeId, string discipline);
        ScheduleListViewData GetScheduleListViewData(Guid patientId, string discipline, string dateRangeId);
        List<ScheduleEvent> GetDeletedTasks(Guid patientId);
        //ScheduleEvent GetScheduledEventWithPhysicianId(Guid episodeId, Guid patientId, Guid eventId);
        bool Reopen(Guid episodeId, Guid patientId, Guid eventId);
        //bool RestoreTask(Guid episodeId, Guid patientId, Guid eventId);
        //bool DeleteSchedule(Guid episodeId, Guid patientId, Guid eventId, Guid userId, int task);
        JsonViewData DeleteSchedules(Guid patientId, Guid episodeId, List<Guid> eventsToBeDeleted);
        bool DeleteScheduleEventAsset(Guid episodeId, Guid patientId, Guid eventId, Guid assetId);

        //bool ReassignSchedules(Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate);
        //bool Reassign(Guid episodeId, Guid patientId, Guid eventId, Guid oldUserId, Guid userId);
        //bool ReassignSchedules(Guid patientId, Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate);
        EpisodeLean GetEpisodeLeanWithPatientNameViewData(Guid patientId, Guid episodeId);
        ReassignViewData GetReassignViewData(Guid patientId, Guid id);
        ReassignViewData GetReassignViewData(Guid patientId, Guid episodeId, string type);
        JsonViewData ReassignSchedules(Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate);
        JsonViewData Reassign(Guid episodeId, Guid patientId, Guid eventId,  Guid userId);
        JsonViewData ReassignSchedules(Guid patientId, Guid employeeOldId, Guid employeeId, DateTime startDate, DateTime endDate);

        //PatientSchedule GetPatientWithSchedule(Guid patientId, string discipline);
        //PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId);
        CalendarViewData GetMasterCalendarViewData(Guid patientId, Guid episodeId, string discipline, bool isAfterEpisode, bool isBeforeEpisode, bool isScheduleEventsActionIncluded, bool isFrequencyIncluded);
        //PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId, string discipline);
        //PatientEpisode GetPatientEpisodeWithFrequency(Guid episodeId, Guid patientId, DateTime date, string discipline);

        bool IsValidImage(HttpFileCollectionBase httpFiles);
        PatientEligibility VerifyEligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender);
        string VerifyHiqhEligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender, Guid patientId);
        List<MedicareEligibility> GetMedicareEligibilityLists(Guid patientId);

        string GetScheduledEventUrl(ScheduleEvent scheduleEvent, DisciplineTasks task, DateTime eventDate);
        string GetScheduledEventUrlOnly(ScheduleEvent schedule, DisciplineTasks task, string name);
        PlanofCareViewData GetPatientAndAgencyInfo(Guid episodeId, Guid patientId, Guid eventId);
       
        //void AddInfectionUserAndScheduleEvent(Infection infection, out Infection infectionOut);
        //void AddIncidentUserAndScheduleEvent(Incident incident, out Incident incidentOut);

        List<VisitNoteEditViewData> GetSixtyDaySummary(Guid patientId);
        List<VitalSign> GetPatientVitalSigns(Guid patientId, DateTime startDate, DateTime endDate);
        List<VitalSign> GetPatientVitalSigns(Guid patientId, int limit);
        List<VitalSign> GetVitalSignsForSixtyDaySummary(Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate);

        DisciplineTask GetDisciplineTask(int disciplineTaskId);

        List<PatientEpisodeTherapyException> GetTherapyException(Guid branchId, Guid patientId, DateTime startDate, DateTime endDate, int statusOptional);
        List<PatientEpisodeTherapyException> GetTherapyReevaluationException(Guid branchId, Guid patientId, DateTime startDate, DateTime endDate, int count, int statusOptional);
       
        List<TaskLog> GetTaskLogs(Guid patientId, Guid eventId, int task);
        List<AppAudit> GetGeneralLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId);
        List<AppAudit> GetMedicationLogs(LogDomain logDomain, LogType logType, Guid domainId);

        //IDictionary<Guid, string> GetPreviousSkilledNurseNotes(Guid patientId, ScheduleEvent scheduledEvent);
        //IDictionary<Guid, string> GetPreviousSkilledNurseNotes(Guid patientId, ScheduleEvent scheduledEvent, int version, int type);
        //IDictionary<Guid, string> GetPreviousPediatricVisitNotes(Guid patientId, ScheduleEvent scheduledEvent, int version);
        //IDictionary<Guid, string> GetPreviousPTNotes(Guid patientId, ScheduleEvent scheduledEvent, int version);
        //IDictionary<Guid, string> GetPreviousPTEvals(Guid patientId, ScheduleEvent scheduledEvent);
        //IDictionary<Guid, string> GetPreviousPTDischarges(Guid patientId, ScheduleEvent scheduledEvent);
        //IDictionary<Guid, string> GetPreviousNutritionalAssessmentNotes(Guid patientId, ScheduleEvent scheduledEvent);
        //IDictionary<Guid, string> GetPreviousOTNotes(Guid patientId, ScheduleEvent scheduledEvent);
        //IDictionary<Guid, string> GetPreviousOTEvals(Guid patientId, ScheduleEvent scheduledEvent);

        //IDictionary<Guid, string> GetPreviousSTNotes(Guid patientId, ScheduleEvent scheduledEvent);
        //IDictionary<Guid, string> GetPreviousSTEvals(Guid patientId, ScheduleEvent scheduledEvent);
        //IDictionary<Guid, string> GetPreviousSTReassessment(Guid patientId, ScheduleEvent scheduledEvent);
        //IDictionary<Guid, string> GetPreviousHHANotes(Guid patientId, ScheduleEvent scheduledEvent);
        //IDictionary<Guid, string> GetPreviousMSWProgressNotes(Guid patientId, ScheduleEvent scheduledEvent);

        //IDictionary<Guid, string> GetPreviousCarePlans(Guid patientId, ScheduleEvent scheduledEvent);

        //IDictionary<Guid, string> GetPreviousISOC(Guid patientId, ScheduleEvent scheduledEvent);

        List<PreviousNote> GetPreviousNotes(Guid PatientId, Guid EpisodeId, Guid EventId);

        bool AddAllergy(Allergy allergy);
        bool UpdateAllergy(Allergy allergy);
        bool CreateAllergyProfile(Guid patientId);
        bool UpdateAllergy(Guid allergyProfileId, Guid allergyId, bool isDeleted);

        bool AddHospitalizationLog(FormCollection formCollection);
        bool UpdateHospitalizationLog(FormCollection formCollection);
        List<HospitalizationLog> GetHospitalizationLogs(Guid agencyId, Guid patientId);
        HospitalizationLog GetHospitalizationLog(Guid patientId, Guid hospitalizationLogId);

        List<NonAdmit> GetNonAdmits();
        List<PendingPatient> GetPendingPatients();
        PatientInsuranceInfoViewData PatientInsuranceInfo(Guid patientId, string insuranceId, string insuranceType);
        bool IsValidAdmissionPeriod(Guid admissionId, Guid patientId, DateTime startOfCareDate, DateTime dischargeDate);
        bool IsValidAdmissionPeriod(Guid patientId, DateTime startOfCareDate, DateTime dischargeDate);
        bool MarkPatientAdmissionCurrent(Guid patientId, Guid Id);

        string GetInsurance(string insurance);
        List<PatientData> GetPatients(Guid agencyId, Guid branchId, int status);
        List<PatientData> GetDeletedPatients(Guid agencyId, Guid branchId);

        //PatientVisitNote GetCarePlanBySelectedEpisode(Guid patientId, Guid episodeId, DisciplineTasks discipline, DateTime eventDate);
        //ScheduleEvent GetCarePlanForHHAideCarePlan(ScheduleEvent scheduledEvent, DateTime eventDate, out IDictionary<string, NotesQuestion> pocQuestions);

       
        FrequenciesViewData GetPatientEpisodeFrequencyData(Guid episodeId, Guid patientId);

        List<ScheduleEvent> GetScheduledEventsWithUsers(Guid patientId, DateTime startDate, DateTime endDate);
        //List<ScheduleEvent> GetScheduledEventsWithUsers(Guid patientId, int limit);
        List<ScheduleEvent> GetScheduledEventsByStatus(Guid branchId, Guid patientId, Guid clinicianId, DateTime startDate, DateTime endDate, List<int> statuses);
        List<ScheduleEvent> GetScheduledEventsByType(Guid branchId, Guid patientId, Guid clinicianId, DateTime startDate, DateTime endDate, int type);
        List<ScheduleEvent> GetScheduledEventsForDelete(Guid episodeId, Guid patientId, string discipline);
        ScheduleListViewData GetScheduledEventsForEdit(Guid episodeId, Guid patientId);
        //string GetReturnComments(Guid eventId, Guid episodeId, Guid patientId);
        string GetReturnComments(Guid eventId, Guid episodeId, Guid patientId, string returnReson);
        bool AddReturnComments(Guid eventId, Guid episodeId, string comment);
        bool EditReturnComments(int id, string comment);
        bool DeleteReturnComments(int id);

       

        //PatientEpisode GetCurrentEpisode(Guid AgencyId, Guid PatientId);
        Patient GetPatientOnly(Guid patientId, Guid agencyId);
        List<Patient> GetPatientAdmissionSourcesByIds(Guid agencyId, List<Guid> PatientIds);
      
        VisitLogViewData GetVisitVerificationLog(Guid patientId, Guid episodeId, Guid eventId);
        //AxxessPdf GetPrintPdf(ScheduleEvent scheduleEvent);

        JsonViewData AddInfection(Infection infection);
        Infection GetInfectionForEdit(Guid Id);
        JsonViewData UpdateInfection(Infection infection);
        JsonViewData AddIncident(Incident incident);
        JsonViewData UpdateIncident(Incident incident);
        
       
        JsonViewData AddCommunicationNote(CommunicationNote communicationNote);

        PhysicianOrder GetOrderEdit(Guid patientId, Guid id);
        JsonViewData AddOrder(PhysicianOrder order);
        JsonViewData AddFaceToFaceEncounter([Bind]FaceToFaceEncounter faceToFaceEncounter);

        JsonViewData ToggleTaskStatus(Guid patientId, Guid eventId, bool isDeprecated);

        CalendarViewData GetScheduleWithPreviousAfterEpisodeInfoNew(Guid patientId,  DateTime date, string discipline, bool isScheduleEventsActionIncluded);
        CalendarViewData GetScheduleWithPreviousAfterEpisodeInfoNew(Guid patientId, Guid episodeId, string discipline, bool isScheduleEventsActionIncluded, bool isFrequencyIncluded);
        CalendarViewData GetScheduleWithPreviousAfterEpisodeInfo(Guid patientId, DateTime date, string discipline, bool isAfterEpisode, bool isBeforeEpisode, bool isScheduleEventsActionIncluded);
        CalendarViewData GetScheduleWithPreviousAfterEpisodeInfo(Guid patientId, Guid episodeId, string discipline, bool isAfterEpisode, bool isBeforeEpisode, bool isScheduleEventsActionIncluded, bool isFrequencyIncluded);

        //ReassignViewData GetReassignViewData(Guid episodeId, Guid patientId, Guid eventId);

       // List<ScheduleEvent> ProcessScheduleEvents(Guid episodeId, Guid patientId, List<ScheduleEvent> schedule, EpisodeDetail detail, DateTime startDate, DateTime endDate);
    }
}
