﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    using Axxess.Core;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Enums;

    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;
   
    public interface IAssessmentService
    {
        List<Question> Get485FromAssessment(Assessment assessment);
        OasisViewData SaveAssessment(FormCollection formCollection, HttpFileCollectionBase httpFiles, string assessmentType);
        //Assessment AddAssessment(Patient patient, ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode);
        //Assessment AddAssessment(Patient patient, ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode, string MedicationProfile);
        bool MarkAsDeleted(Guid assessmentId, Guid episodeId, Guid patientId, string assessmentType, bool isDeprecated);
        bool ReassignUser(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, string taskName);
        Assessment GetAssessment(Guid assessmentId, string assessmentType);
        Assessment GetAssessment(Guid episodeId, Guid patientId, Guid eventId, string Type);
        Assessment GetAssessmentWithDisciplineTask(Guid episodeId, Guid patientId, Guid eventId, int disciplineTask);
        Assessment GetAssessmentWithScheduleType(Guid assessmentId, string assessmentType);
        OasisAudit Audit(Guid assessmentId, Guid patientId, Guid episodeId, string assessmentType);
        ValidationInfoViewData Validate(Guid assessmentId, Guid patientId, Guid episodeId);
        bool Validate(Assessment assessment);
        ValidationInfoViewData ValidateInactivate(Guid assessmentId, string assessmentType);
        string OasisHeader(AgencyLocation agencyLocation);
        string OasisFooter(int totalNumberOfRecord);
        string GetOasisSubmissionFormatNew(IDictionary<string, Question> assessmentQuestions, int versionNumber, AgencyLocation patientLocation);
        bool UpdatePlanofCare(FormCollection formCollection);
        bool UpdatePlanOfCareForDetail(ScheduleEvent schedule);
        bool UpdatePlanofCareStandAlone(FormCollection formCollection);
        bool UpdatePlanOfCareStandAloneForDetail(ScheduleEvent schedule);
        void GeneratePlanofCare(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment, bool isNonOasis);
        //bool MarkPlanOfCareAsDeleted(Guid eventId, Guid episodeId, Guid patientId, bool isDeprecated);
        //bool MarkPlanOfCareStandAloneAsDeleted(Guid eventId, Guid episodeId, Guid patientId, bool isDeprecated);
        //bool ReassignPlanOfCaresUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        //ScheduleEvent GetEpisodeAssessmentEventWithOutNonOASIS(PatientEpisode episode, List<ScheduleEvent> scheduleEvents);
        //ScheduleEvent GetEpisodeAssessmentEventWithOutNonOASIS(Guid episodeId, Guid patientId);
        //Assessment GetEpisodeAssessmentNew(Guid agencyId, PatientEpisode episode);
        //Assessment GetEpisodeAssessmentNew(Guid agencyId, PatientEpisode episode, List<ScheduleEvent> scheduleEvents);

        //Assessment GetEpisodeAssessment(Guid agencyId, PatientEpisode episode);
        //Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId);
        //Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId, DateTime eventDate);
        //Assessment GetEpisodeAssessment(Guid agencyId, Guid episodeId, Guid patientId, DateTime eventDate);
        string GetPlanofCareUrl(Guid episodeId, Guid patientId, Guid assessmentId);
        ScheduleEvent GetPlanofCareScheduleEvent(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType);
        IDictionary<string, Question> LocatorQuestions(Assessment assessment);
        bool UpdateAssessmentStatus(Guid Id, Guid patientId, Guid episodeId, string assessmentType, int status);
        bool UpdateAssessmentStatus(ScheduleEvent scheduleEvent, string assessmentType, int status);
        bool UpdateAssessmentCorrectionNumber(Guid id, Guid patientId, Guid episodeId, string assessmentType, int CorrectionNumber);
        bool UpdateAssessmentStatusForSubmit(Guid id, Guid patientId, Guid episodeId, string assessmentType, string status, string signature, DateTime date, string timeIn, string timeOut);
        bool UpdateAssessmentForDetail(ScheduleEvent schedule);
        bool MarkAsExportedOrCompleted(List<string> OasisSelected, int status);
        bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, string name, Guid assetId);
        bool AddSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply);
        List<Supply> GetAssessmentSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType);
        bool UpdateSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply);
        bool DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply);
        bool UpdatePlanofCareStatus(Guid episodeId, Guid patientId, Guid eventId, string actionType);
        AssessmentPrint GetAssessmentPrint(AssessmentType Type);
        AssessmentPrint GetAssessmentPrint(Guid episodeId, Guid patientId, Guid eventId);
        AssessmentPrint GetAssessmentPrint(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        PlanofCare GetPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId);
        PlanofCare GetPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId, Guid agencyId);
        DateRange GetPlanofCareCertPeriod(Guid episodeId, Guid patientId, Guid assessmentId);
        DateRange GetPlanofCareCertPeriod(PatientEpisode episode, Guid assessmentId);
        //DateRange GetPlanofCareCertPeriod(Guid episodeId, Guid patientId, Guid assessmentId, Guid agencyId);
       

        //PlanofCare GetPlanofCare(Guid episodeId, Guid patientId, Guid planofCareId);
        //PlanofCareStandAlone GetPlanofCareStandAlone(Guid episodeId, Guid patientId, Guid planofCareId);

        List<AssessmentExport> GetAssessmentByStatus(Guid branchId, ScheduleStatus status, int patientStatus, DateTime StartDate, DateTime EndDate);
        List<AssessmentExport> GetAssessmentByStatus(Guid branchId, ScheduleStatus status, List<int> paymentSources);
        AssessmentPrint OASISProfileData(Guid assessmentId, string assessmentType);

        bool DeleteOnlyWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Guid assetId);

        //JsonViewData LoadPrevious(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType, Guid previousAssessmentId, string previousAssessmentType);

        //Assessment GetEpisodeAssessment(PatientEpisode episode, bool isAssessmentCompleted);
        Assessment GetEpisodeAssessment(ScheduleEvent scheduleEvent, bool isAssessmentCompleted);
        //ScheduleEvent GetEpisodeAssessmentEvent(PatientEpisode episode, bool isAssessmentCompleted);
        EpisodeDateViewData MasterCalendarpisodeDate(Guid episodeId, Guid patientId, string assessmentType);
        //string GetFrequencyForCalendar(PatientEpisode episode, PatientEpisode previousEpisode);
        bool AddScheduleTaskAndAssessment(Patient patient, ScheduleEvent scheduleEvent);
        JsonViewData AddScheduleTaskAndAssessmentHelper(PatientEpisode patientEpisode, List<ScheduleEvent> scheduleEvents);
        JsonViewData ToggleAssessment(ScheduleEvent task, bool isDeprecated);
        object GetDiagnosisData(Guid patientId, Guid episodeId);
    }
}
