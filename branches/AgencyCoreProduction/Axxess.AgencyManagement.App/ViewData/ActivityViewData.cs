﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using Axxess.AgencyManagement.Domain;
    using System.Collections.Generic;

    public class ActivityViewData
    {
        public List<ScheduleEventJson> ScheduleEvents { get; set; }
    }
}
