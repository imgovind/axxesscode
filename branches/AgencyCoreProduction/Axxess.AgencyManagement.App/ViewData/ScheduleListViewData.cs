﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Domain;

namespace Axxess.AgencyManagement.App.ViewData
{
  public class ScheduleListViewData
    {
      public Guid PatientId { get; set; }
      public Guid EpisodeId { get; set; }
      public string DisplayName { get; set; }
      public DateTime EndDate { get; set; }
      public DateTime StartDate { get; set; }
      public IList<ScheduleEvent> List{ get; set; }
    }
}
