﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    public class AgencyLocationViewData
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string AddressFull
        {
            get
            {
                return string.Format("{0} {1} {2} {3}", this.AddressLine1.Trim(), this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
            }
        }
        public string Phone { get; set; }
        public string[] PhoneArray { get; set; }
        public bool IsMainOffice { get; set; }
    }
}
