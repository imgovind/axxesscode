﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.ViewData
{
   public class EpisodeDateViewData
    {
       public Guid Id { get; set; }
       public DateTime StartDate { get; set; }
       public DateTime EndDate { get; set; }
       public string Type { get; set; }
       public string Range { get; set; }
    }
}
