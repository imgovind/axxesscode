﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.OasisC.Domain;
    using Axxess.Core.Extension;
    using System.Web.Script.Serialization;
    public abstract  class VisitNoteViewData
    {
        public VisitNoteViewData()
        {
            //this.PreviousNotes = new Dictionary<Guid, string>();
        }
       // public Agency Agency { get; set; }
        //public Patient Patient { get; set; }
        //public IDictionary<Guid, string> PreviousNotes { get; set; }
        public IDictionary<string, NotesQuestion> Questions { get; set; }
        public IDictionary<string, NotesQuestion> WoundCare { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string VisitDate { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid EventId { get; set; }
        public Guid PhysicianId { get; set; }
        public Guid UserId { get; set; }
        public string PhysicianDisplayName { get; set; }
        public string SignatureText { get; set; }
        public string SignatureDate { get; set; }
        public string StatusComment { get; set; }
        public string Type { get; set; }
        public string TypeName { get; set; }
        public string UserDisplayName { get; set; }
        public string Allergies { get; set; }
        public int DisciplineTask { get; set; }
        public string PhysicianSignatureText { get; set; }
        public DateTime PhysicianSignatureDate { get; set; }
        public string EpisodeRange { get { return (this.StartDate.IsValid() && this.EndDate.IsValid()) ? string.Format("{0} - {1}", this.StartDate.ToString("MM/dd/yyyy"), this.EndDate.ToString("MM/dd/yyyy")) : string.Empty; } }
        public int Version { get; set; }
        public bool IsWoundCareExist { get; set; }
       

        public IDictionary<String, NotesQuestion> MergeDictionaries() {
            IDictionary<String, NotesQuestion> MergedDictionary = new Dictionary<String, NotesQuestion>();
            foreach (KeyValuePair<String, NotesQuestion> entry in this.Questions) if (!MergedDictionary.ContainsKey(entry.Key)) MergedDictionary.Add(entry);
            foreach (KeyValuePair<String, NotesQuestion> entry in this.WoundCare) if (!MergedDictionary.ContainsKey(entry.Key)) MergedDictionary.Add(entry);
            return MergedDictionary;
        }
    }

    public class VisitNotePrintViewData : VisitNoteViewData
    {
        public VisitNotePrintViewData()
            : base()
        {
        }

        [ScriptIgnore]
        public Guid AgencyId { get; set; }
        public string PrintViewJson { get; set; }
    }

    public class VisitNoteEditViewData : VisitNoteViewData
    {
        public VisitNoteEditViewData()
            : base()
        {
        }

        public string DisplayName {get; set;}
        public string MRN { get; set; }
        public string PrintUrl { get; set; }
        public string CarePlanOrEvalUrl { get; set; }
        public bool IsSupplyExist { get; set; }
       
    }

}