﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Axxess.OasisC.Domain;

    public class PlanofCareViewData
    {
        public PlanofCareViewData()
        {
            this.EditData = new Dictionary<string, Question>();
            this.DisplayData = new Dictionary<string, string>();
        }

        public IDictionary<string, Question> EditData { get; set; }
        public IDictionary<string, string> DisplayData { get; set; }
    }
}