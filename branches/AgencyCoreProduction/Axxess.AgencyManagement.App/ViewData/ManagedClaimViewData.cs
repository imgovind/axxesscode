﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using System.ComponentModel.DataAnnotations;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;

    [KnownType(typeof(ManagedClaimViewData))]
    public class ManagedClaimViewData
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public DateTime FirstBillableVisitDate { get; set; }
        public bool IsVerified { get; set; }
        public DateTime Modified { get; set; }
        public string IsuranceIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string AdmissionSource { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string VerifiedVisit { get; set; }
        public bool AreOrdersComplete { get; set; }
        public int PrimaryInsuranceId { get; set; }

        public bool IsVisitVerified { get; set; }
        public bool IsSupplyVerified { get; set; }
        public bool IsInfoVerified { get; set; }

        [UIHint("Status")]
        public int Status { get; set; }
        [DataType(DataType.Date)]
        public DateTime PaymentDate { get; set; }
        public string PaymentDateFormatted
        {
            get
            {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToShortDateString().ToZeroFilled() : string.Empty;
            }
        }
        public string ClaimDate { get; set; }
        public double PaymentAmount { get; set; }
        public double ClaimAmount { get; set; }
        public string Comment { get; set; }
        public double SupplyTotal { get; set; }
       
        public string StatusName
        {
            get
            {
                return Enum.IsDefined(typeof(ManagedClaimStatus),this.Status)? EnumExtensions.GetDescription((ManagedClaimStatus)Enum.ToObject(typeof(ManagedClaimStatus), this.Status)):string.Empty;

            }
        }
        public string ClaimRange
        {
            get
            {
                return string.Format("{0}-{1}", this.EpisodeStartDate != null ? this.EpisodeStartDate.ToString("MM/dd/yyyy") : "", this.EpisodeEndDate != null ? this.EpisodeEndDate.ToString("MM/dd/yyyy") : "");
            }
        }
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }

        public bool IsHMO { get; set; }
        public string HealthPlanId { get; set; }
        public string PayerId { get; set; }
        public string PayorName { get; set; }
        public string PayorAuthorizationCode { get; set; }
        public string OtherProviderId { get; set; }
        public string ProviderId { get; set; }
        public string ProviderSubscriberId { get; set; }

        public string PrintUrl { get { return this.IsVisitVerified && this.IsInfoVerified && this.IsSupplyVerified ? string.Format("<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/ManagedUB04Pdf', {{ 'patientId': '{0}', 'Id': '{1}' }});\">UB-04</a> | <a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Billing/HCFA1500Pdf', {{ 'patientId': '{0}', 'Id': '{1}' }});\">HCFA-1500</a>", this.PatientId, this.Id) : string.Empty; } }
    }
}
