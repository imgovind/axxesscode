﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    using Axxess.Core.Infrastructure;

    public class NoteViewData : JsonViewData
    {
       public Guid Id { get; set; }
       public Guid PatientId { get; set; }
       public string Note { get; set; }
    }
}
