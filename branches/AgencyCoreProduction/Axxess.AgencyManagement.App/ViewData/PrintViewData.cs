﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    public class PrintViewData<T>
    {
        public T Data { get; set; }
        public LocationPrintProfile LocationProfile { get; set; }
        public PatientProfileLean PatientProfile { get; set; }
    }
}
