﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using Axxess.AgencyManagement.Domain;

    public class AllergyProfileViewData
    {
        public Guid EpisodeId { get; set; }
        public AllergyProfile AllergyProfile { get; set; }
        public Guid PhysicianId { get; set; }
        public string PhysicianDisplayName { get; set; }
    }

    public class AllergyProfileEditViewData : AllergyProfileViewData
    {
        public string DisplayName { get; set; }

    }
}