﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;


    public class PatientSelectionViewData
    {
        public Guid Id { get; set; }

        public string PatientIdNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public byte Status { get; set; }

        public string PaymentSource { get; set; }

    }
}
