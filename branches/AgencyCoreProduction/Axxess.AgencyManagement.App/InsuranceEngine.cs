﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.LookUp.Repositories;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Enums;

    public static class InsuranceEngine
    {
        private static readonly IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();

        #region Nested Class for Singleton

        //class Nested
        //{
        //    static Nested()
        //    {
        //    }

        //    internal static readonly InsuranceEngine instance = new InsuranceEngine();
        //}

        //#endregion

        //#region Public Instance

        //public static InsuranceEngine Instance
        //{
        //    get
        //    {
        //        return Nested.instance;
        //    }
        //}

        //#endregion

       // #region Private Members

       // //private static object syncLock = new object();
       // //private static SafeDictionary<Guid, IDictionary<int, InsuranceCache>> cache = new SafeDictionary<Guid, IDictionary<int, InsuranceCache>>();
       // private static readonly IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
       //// private static readonly ILookUpDataProvider lookUpdataProvider = Container.Resolve<ILookUpDataProvider>();

       // #endregion

        //#region Private Constructor / Methods

        //private InsuranceEngine()
        //{
        //}

        //private void Load(Guid agencyId)
        //{

        //    var insurances = new List<InsuranceCache>();
        //    var standardInsurances = lookUpdataProvider.LookUpRepository.Insurances().Select(i => new InsuranceCache { Id = i.Id, Name = i.Name, PayorType = (int)PayerTypes.MedicareTraditional }).ToList();
        //    if (standardInsurances != null && standardInsurances.Count > 0)
        //    {
        //        insurances.AddRange(standardInsurances);
        //    }
        //    var agencyInsurances = dataProvider.AgencyRepository.GetInsurances(agencyId).Select(i => new InsuranceCache { Id = i.Id, Name = i.Name, PayorType = i.PayorType }).ToList();
        //    if (agencyInsurances != null && agencyInsurances.Count > 0)
        //    {
        //        insurances.AddRange(agencyInsurances);
        //    }

        //    if (insurances != null && insurances.Count > 0)
        //    {
        //        var newInsuranceList = new Dictionary<int, InsuranceCache>();
        //        insurances.ForEach(insurance =>
        //        {
        //            newInsuranceList.Add(insurance.Id, insurance);
        //        });

        //        if (cache.ContainsKey(agencyId))
        //        {
        //            cache[agencyId] = newInsuranceList;
        //        }
        //        else
        //        {
        //            if (!cache.ContainsKey(agencyId))
        //            {
        //                cache.Add(agencyId, newInsuranceList);
        //            }
        //        }
        //    }
        //}

        //#endregion

        //#region Public Methods

        //public InsuranceCache Get(int Id, Guid agencyId)
        //{
        //    if (cache.Count > 0 && cache.ContainsKey(agencyId))
        //    {

        //        var insuranceCache = cache[agencyId];

        //        if (insuranceCache != null && insuranceCache.Count > 0)
        //        {
        //            if (insuranceCache.ContainsKey(Id))
        //            {
        //                return insuranceCache[Id];
        //            }
        //        }
        //        else
        //        {
        //            this.Load(agencyId);

        //            if (cache.Count > 0 && cache.ContainsKey(agencyId))
        //            {
        //                var newInsuranceCache = cache[agencyId];

        //                if (newInsuranceCache != null && newInsuranceCache.Count > 0)
        //                {
        //                    if (newInsuranceCache.ContainsKey(Id))
        //                    {
        //                        return newInsuranceCache[Id];
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        this.Load(agencyId);

        //        if (cache.Count > 0 && cache.ContainsKey(agencyId))
        //        {

        //            var newInsuranceCache = cache[agencyId];

        //            if (newInsuranceCache != null && newInsuranceCache.Count > 0)
        //            {
        //                if (newInsuranceCache.ContainsKey(Id))
        //                {
        //                    return newInsuranceCache[Id];
        //                }
        //            }
        //        }
        //    }
        //    return null;
        //}

        public static void Refresh(Guid agencyId)
        {
            //Load(agencyId);
        }

        //public SafeDictionary<Guid, IDictionary<int, InsuranceCache>> Cache
        //{
        //    get { return cache; }
        //}

        //#endregion

        #region New
        public static string GetName(int insuranceId, Guid agencyId)
        {
            var name = string.Empty;
            if (insuranceId > 0 && !agencyId.IsEmpty())
            {
                if (insuranceId > 1000)
                {
                    var key = Key(agencyId, insuranceId);
                    var insuranceCacheXml = string.Empty;
                    if (!Cacher.TryGet<string>(key, out insuranceCacheXml))
                    {
                        var insurance = dataProvider.AgencyRepository.GetInsurance(insuranceId, agencyId);
                        if (insurance != null)
                        {
                            Cacher.Set(key, (new InsuranceCache { Id = insurance.Id, Name = insurance.Name, PayorType = insurance.PayorType }).ToXml());
                            name = insurance.Name;
                        }
                    }
                    else
                    {
                        var insuranceCache = insuranceCacheXml.ToObject<InsuranceCache>();
                        if (insuranceCache != null)
                        {
                            name = insuranceCache.Name;
                        }
                    }
                }
                else
                {
                    if (Enum.IsDefined(typeof(MedicareIntermediary), insuranceId))
                    {
                        var insurance = (MedicareIntermediary)insuranceId;
                        name = insurance.GetDescription();
                    }
                }
            }
            return name;
        }

        public static InsuranceCache Get(int insuranceId, Guid agencyId)
        {
            InsuranceCache insuranceCache = null;
            if (insuranceId > 0 && !agencyId.IsEmpty())
            {
                if (insuranceId >= 1000)
                {
                    var key = Key(agencyId, insuranceId);
                    var insuranceCacheXml = Cacher.Get<string>(key);
                    if (insuranceCacheXml.IsNotNullOrEmpty())
                    {
                        insuranceCache = insuranceCacheXml.ToObject<InsuranceCache>();
                        if (insuranceCache != null)
                        {
                            return insuranceCache;
                        }
                        else
                        {
                            return AddInsurance(agencyId, insuranceId, key);
                        }
                    }
                    else
                    {
                        return AddInsurance(agencyId, insuranceId, key);
                    }
                }
                else
                {
                    if (Enum.IsDefined(typeof(MedicareIntermediary), insuranceId))
                    {
                        var insurance = (MedicareIntermediary)insuranceId;
                        insuranceCache = new InsuranceCache
                        {
                            Name = insurance.GetDescription(),
                            Id = insuranceId,
                            PayorType = (int)PayerTypes.MedicareTraditional
                        };
                    }
                }
            }
            return insuranceCache;
        }

        public static List<InsuranceCache> GetInsurances(Guid agencyId, List<int> insuranceIds)
        {
            var insuranceCaches = new List<InsuranceCache>();
            var insuranceCacheIdsNotInCache = new List<int>();
            if (!agencyId.IsEmpty() && insuranceIds != null && insuranceIds.Count > 0)
            {
                var nonTradtionalMedicare = insuranceIds.Where(i => i >= 1000).Distinct();
                if (nonTradtionalMedicare.IsNotNullOrEmpty())
                {
                    var keys = nonTradtionalMedicare.Select(id => Key(agencyId, id));
                    var results = Cacher.Get<string>(keys);
                    if (results != null && results.Count > 0)
                    {
                        results.ForEach((key, value) =>
                        {
                            if (key.IsNotNullOrEmpty() && value != null)
                            {
                                string insuranceCacheXml = value.ToString();
                                if (insuranceCacheXml.IsNotNullOrEmpty())
                                {
                                    insuranceCaches.Add(insuranceCacheXml.ToObject<InsuranceCache>());
                                }
                            }

                        });
                    }
                    insuranceCacheIdsNotInCache = nonTradtionalMedicare.Where(id => !insuranceCaches.Exists(u => u.Id == id)).ToList();
                    if (insuranceCacheIdsNotInCache != null && insuranceCacheIdsNotInCache.Count > 0)
                    {
                        var locationsNotInCache = dataProvider.AgencyRepository.GetInsurancesForBilling(agencyId, insuranceCacheIdsNotInCache.ToArray());
                        if (insuranceCacheIdsNotInCache != null && insuranceCacheIdsNotInCache.Count > 0)
                        {
                            locationsNotInCache.ForEach(u =>
                            {
                                var key = Key(agencyId, u.Id);
                                Cacher.Set<string>(key, u.ToXml());
                                insuranceCaches.Add(u);

                            });
                        }
                    }
                }

                var tradtionalMedicare = insuranceIds.Where(i => i < 1000 && i > 0).Distinct();
                if (tradtionalMedicare.IsNotNullOrEmpty())
                {
                    tradtionalMedicare.ForEach((id) =>
                    {
                        if (Enum.IsDefined(typeof(MedicareIntermediary), id))
                        {
                            var insurance = (MedicareIntermediary)id;
                            insuranceCaches.Add(new InsuranceCache
                            {
                                Name = insurance.GetDescription(),
                                Id = id,
                                PayorType = (int)PayerTypes.MedicareTraditional
                            });
                        }
                    });
                }
            }
            return insuranceCaches;
        }

        public static void AddOrUpdate(Guid agencyId, AgencyInsurance insurance)
        {
            if (!agencyId.IsEmpty() && insurance != null && insurance.Id >= 1000)
            {
                var insuranceCache = new InsuranceCache { Id = insurance.Id, Name = insurance.Name, PayorType = insurance.PayorType };
                var insuranceCacheXml = insuranceCache.ToXml();
                if (insuranceCacheXml.IsNotNullOrEmpty())
                {
                    var key = Key(agencyId, insurance.Id);
                    Cacher.Set<string>(key, insuranceCacheXml);
                }
            }
        }

        private static InsuranceCache AddInsurance(Guid agencyId, int insuranceId, string key)
        {
            var insurance = dataProvider.AgencyRepository.GetInsurance(insuranceId, agencyId);
            InsuranceCache insuranceCache = null;
            if (insurance != null)
            {
                insuranceCache = new InsuranceCache { Id = insurance.Id, Name = insurance.Name, PayorType = insurance.PayorType };
                Cacher.Set<string>(key, insuranceCache.ToXml());
            }
            return insuranceCache;
        }

        private static string Key(Guid agencyId, int insuranceId)
        {
            return string.Format("{0}_{1}_{2}", agencyId, (int)CacheType.Insurance, insuranceId);
        }
        #endregion

        #endregion
    }
}
