﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Extensions;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using Axxess.AgencyManagement.Domain;


    public class PastDueRecertExporter : BaseExporter
    {
         private IList<RecertEvent> recertEvents;
         private DateTime startDate;
         public PastDueRecertExporter(IList<RecertEvent> recertEvents, DateTime startDate)
            : base()
        {
            this.recertEvents = recertEvents;
            this.startDate = startDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Past Due Recerts.";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("PastDueRecerts");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Past Due Recerts.");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", startDate.ToString("MM/dd/yyyy"), DateTime.Now.ToString("MM/dd/yyyy"))));
            bool booleanInsuranceAll = default(bool);
            if (this.recertEvents != null && this.recertEvents.Count > 0) 
            {
                var item = this.recertEvents.FirstOrDefault();
                if (item != null && item.InsuranceIdFromView == 0) { booleanInsuranceAll = true; } else { booleanInsuranceAll = false; }
            }

            var headerRow = sheet.CreateRow(1);

            if (booleanInsuranceAll) 
            {
                headerRow.CreateCell(0).SetCellValue("MRN");
                headerRow.CreateCell(1).SetCellValue("Patient");
                headerRow.CreateCell(2).SetCellValue("Insurance");
                headerRow.CreateCell(3).SetCellValue("Employee Responsible");
                headerRow.CreateCell(4).SetCellValue("Status");
                headerRow.CreateCell(5).SetCellValue("Due Date");
                headerRow.CreateCell(6).SetCellValue("Past Date");
            }
            else 
            {
                headerRow.CreateCell(0).SetCellValue("MRN");
                headerRow.CreateCell(1).SetCellValue("Patient");
                headerRow.CreateCell(2).SetCellValue("Employee Responsible");
                headerRow.CreateCell(3).SetCellValue("Status");
                headerRow.CreateCell(4).SetCellValue("Due Date");
                headerRow.CreateCell(5).SetCellValue("Past Date");
            }

            if (this.recertEvents.Count > 0)
            {
                int i = 2;
                this.recertEvents.ForEach(a =>
                {
                    var dataRow = sheet.CreateRow(i);
                    if (booleanInsuranceAll) 
                    {
                        dataRow.CreateCell(0).SetCellValue(a.PatientIdNumber);
                        dataRow.CreateCell(1).SetCellValue(a.PatientName);
                        dataRow.CreateCell(2).SetCellValue(a.Insurance);
                        dataRow.CreateCell(3).SetCellValue(a.AssignedTo);
                        dataRow.CreateCell(4).SetCellValue(a.Status);
                        if (a.TargetDate != DateTime.MinValue)
                        {
                            var createdDateCell = dataRow.CreateCell(5);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(a.TargetDate);
                        }
                        else
                        {
                            dataRow.CreateCell(5).SetCellValue(string.Empty);
                        }
                        dataRow.CreateCell(6).SetCellValue(a.DateDifference);
                    }
                    else
                    {
                        dataRow.CreateCell(0).SetCellValue(a.PatientIdNumber);
                        dataRow.CreateCell(1).SetCellValue(a.PatientName);
                        dataRow.CreateCell(2).SetCellValue(a.AssignedTo);
                        dataRow.CreateCell(3).SetCellValue(a.Status);
                        if (a.TargetDate != DateTime.MinValue)
                        {
                            var createdDateCell = dataRow.CreateCell(4);
                            createdDateCell.CellStyle = dateStyle;
                            createdDateCell.SetCellValue(a.TargetDate);
                        }
                        else
                        {
                            dataRow.CreateCell(4).SetCellValue(string.Empty);
                        }
                        dataRow.CreateCell(5).SetCellValue(a.DateDifference);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Recerts. : {0}", recertEvents.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(6);
        }
    }
}
