﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.IO;

    using Axxess.Core.Extension;

    using Enums;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;

    using Kent.Boogaart.KBCsv;

    using Ionic.Zip;

    public abstract class BaseExporter : IDisposable
    {
        #region Constructor

        public BaseExporter()
        {
            this.FormatType = ExportFormatType.XLS;
        }

        #endregion

        #region Public Methods

        public string MimeType
        {
            get
            {
                return FormatType.GetDescription();
            }
        }

        public string FileName { get; set; }

        public ExportFormatType FormatType { get; set; }

        public MemoryStream Process()
        {
            var memoryStream = new MemoryStream();
            if (this.FormatType == ExportFormatType.XLS)
            {
                memoryStream = this.ProcessSpreadsheet();
            }
            else if (this.FormatType == ExportFormatType.CSV)
            {
                memoryStream = this.ProcessCommaSeparatedFile();
            }
            else if (this.FormatType == ExportFormatType.ZIP)
            {
                memoryStream = this.ProcessArchive();
            }

            return memoryStream;
        }

        #endregion

        #region Virtual Spreadsheet Methods
        
        protected HSSFWorkbook workBook;
        protected virtual void InitializeExcel()
        {
            workBook = new HSSFWorkbook();
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            si.Author = "Axxess";
            workBook.SummaryInformation = si;
        }
        protected virtual void WriteToExcelSpreadsheet() { }
        protected MemoryStream ProcessSpreadsheet()
        {
            this.InitializeExcel();
            this.WriteToExcelSpreadsheet();

            var memoryStream = new MemoryStream();
            workBook.Write(memoryStream);
            return memoryStream;
        }

        #endregion

        #region Virtual Comma Separated File Methods

        protected CsvWriter csvWriter;
        protected virtual void WriteCommaSeparatedFile() { }
        protected MemoryStream ProcessCommaSeparatedFile()
        {
            var csvStream = new MemoryStream();
            using (csvWriter = new CsvWriter(csvStream))
            {
                csvWriter.NewLine = "\r";
                this.WriteCommaSeparatedFile();
            }
            return csvStream;
        }

        #endregion

        #region Virtual Archive Methods
        protected ZipFile zipFile;
        protected virtual void WriteToArchive() { }
        protected MemoryStream ProcessArchive()
        {

            var outputStream = new MemoryStream();
            using (this.zipFile = new ZipFile())
            {
                zipFile.UseZip64WhenSaving = Zip64Option.Never;
                zipFile.UseUnicodeAsNecessary = true;
                this.WriteToArchive();
                zipFile.Save(outputStream);
                outputStream.Position = 0;
                return outputStream;
            }
        }

        #endregion

        #region IDisposable

        private bool isDisposed;
        protected bool IsDisposed
        {
            get { return isDisposed; }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.IsDisposed)
                return;

            if (disposing)
            {
                isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
