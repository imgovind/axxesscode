﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class UserExporter : BaseExporter
    {
        private IList<User> users;
        public UserExporter(IList<User> users)
            : base()
        {
            this.users = users;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employees";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Employees");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employees");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Id");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("Suffix");
            headerRow.CreateCell(3).SetCellValue("First Name");
            headerRow.CreateCell(4).SetCellValue("Email");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            headerRow.CreateCell(6).SetCellValue("Mobile Phone");
            headerRow.CreateCell(7).SetCellValue("Address 1");
            headerRow.CreateCell(8).SetCellValue("Address 2");
            headerRow.CreateCell(9).SetCellValue("City");
            headerRow.CreateCell(10).SetCellValue("State");
            headerRow.CreateCell(11).SetCellValue("Zip Code");
            headerRow.CreateCell(12).SetCellValue("Gender");

            if (this.users.Count > 0)
            {
                int i = 2;
                this.users.ForEach(u =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(u.CustomId);
                    dataRow.CreateCell(1).SetCellValue(u.LastName.ToUpper());
                    dataRow.CreateCell(2).SetCellValue(u.Suffix);
                    dataRow.CreateCell(3).SetCellValue(u.FirstName.ToUpper());
                    if (u.Profile == null && u.ProfileData.IsNotNullOrEmpty()) u.Profile = u.ProfileData.ToObject<UserProfile>();
                    dataRow.CreateCell(4).SetCellValue(u.Profile.EmailWork);
                    dataRow.CreateCell(5).SetCellValue(u.Profile.PhoneHome.ToPhone());
                    dataRow.CreateCell(6).SetCellValue(u.Profile.PhoneMobile.ToPhone());
                    dataRow.CreateCell(7).SetCellValue(u.Profile.AddressLine1);
                    dataRow.CreateCell(8).SetCellValue(u.Profile.AddressLine2);
                    dataRow.CreateCell(9).SetCellValue(u.Profile.AddressCity);
                    dataRow.CreateCell(10).SetCellValue(u.Profile.AddressStateCode);
                    dataRow.CreateCell(11).SetCellValue(u.Profile.AddressZipCode);
                    dataRow.CreateCell(12).SetCellValue(u.Profile.Gender);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employees: {0}", users.Count));
            }

            workBook.FinishWritingToExcelSpreadsheet(13);
        }
    }
}
