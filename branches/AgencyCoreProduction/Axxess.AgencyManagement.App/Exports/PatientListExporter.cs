﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HPSF;

using Axxess.Api;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;
using Axxess.AgencyManagement.App.Services;
using Axxess.AgencyManagement.Repositories;
using Axxess.LookUp.Repositories;
using Axxess.Log.Repositories;
using Axxess.OasisC.Repositories;
using Axxess.AgencyManagement.Domain;
using Axxess.Membership.Repositories;

namespace Axxess.AgencyManagement.App.Exports
{
    class PatientListExporter : BaseExporter
    {

        #region Private Members and Constructor

        private Guid AgencyId;
        private string AgencyName;
        private Guid BranchId;
        private int StatusId;
        //private static readonly ILookUpDataProvider lookupDataProvider = new LookUpDataProvider();
        //private static readonly ILookupService lookupService = new LookupService(lookupDataProvider);
        private static readonly ReportAgent reportAgent = new ReportAgent();
        //private static readonly IAgencyManagementDataProvider agencyManagementDataProvider = new AgencyManagementDataProvider();
        //private static readonly ILogDataProvider logDataProvider = new LogDataProvider();
        //private static readonly IOasisCDataProvider oasisDataProvider = new OasisCDataProvider();
        //private static readonly IAssetService assetService = new AssetService(agencyManagementDataProvider);
        //private static readonly IAssessmentService assessmentService = new AssessmentService(oasisDataProvider, agencyManagementDataProvider, lookupDataProvider, lookupService, assetService);
        //private static readonly IDrugService drugService = new DrugService();
        //private static readonly IPatientService patientService = new PatientService(agencyManagementDataProvider, lookupDataProvider, logDataProvider, oasisDataProvider, assessmentService, drugService, assetService);
        //private static readonly IMembershipDataProvider membershipDataProvider = new MembershipDataProvider();
        //private static readonly IPhysicianService physicianService = new PhysicianService(agencyManagementDataProvider, membershipDataProvider,lookupDataProvider);
        //private static readonly IUserService userService = new UserService(agencyManagementDataProvider, membershipDataProvider, patientService, lookupDataProvider, assetService);

        public PatientListExporter(Guid agencyId, int statusId, Guid branchId, String agencyName)
            : base()
        {
            AgencyId = agencyId;
            StatusId = statusId;
            AgencyName = agencyName;
            BranchId = branchId;

            FormatType = Axxess.AgencyManagement.App.Enums.ExportFormatType.XLS;
            this.FileName = "PatientList.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PatientList";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("Patient List");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var currencyStyle = base.workBook.CreateCellStyle();
            currencyStyle.DataFormat = (short)8;

            var titleRow = sheet.CreateRow(0);

            var tCell1 = titleRow.CreateCell(0);
            tCell1.SetCellValue(AgencyName);

            var tCell2 = titleRow.CreateCell(2);
            tCell2.SetCellValue("Patient List Report");

            var tCell3= titleRow.CreateCell(4);
            tCell3.SetCellValue("Effective Date: " + DateTime.Today.ToZeroFilled());

            List<Dictionary<string, string>> data = reportAgent.PatientListReport(AgencyId, BranchId, StatusId);

            if (data != null && data.Count > 0)
            {
                var rowIndex = 1;
                var colIndex = 0;
                var columnSize = 0;
                var dictionary = data.FirstOrDefault();

                #region preprocessResultData
                data.ForEach(gist =>
                {
                    //Preprocess the episode start date
                    var patientIdStaging = gist["PatientId"];
                    if (patientIdStaging.IsNotNullOrEmpty())
                    {
                        Guid PatientId = patientIdStaging.ToGuid();
                        if (PatientId != null && PatientId != Guid.Empty)
                        {
                            var episodeStartDateTemp = GetEpisodeStartDate(AgencyId, PatientId);
                            if (episodeStartDateTemp != null)
                            {
                                gist["Episode Start Date"] = episodeStartDateTemp;
                            }
                            else
                            {
                                gist["Episode Start Date"] = string.Empty;
                            }
                            
                            AgencyPhysician localPhysicianVar = GetPhysicianForAPId(AgencyId, PatientId);
                            if (localPhysicianVar != null)
                            {
                                var physicianNameOMG = localPhysicianVar.LastName + ", " + localPhysicianVar.FirstName + " " + localPhysicianVar.MiddleName;
                                if (physicianNameOMG.Equals(",  "))
                                {
                                    gist["Physician"] = string.Empty;
                                }
                                else
                                {
                                    gist["Physician"] = physicianNameOMG;
                                }
                                gist["Physician NPI"] = localPhysicianVar.NPI;
                                gist["Physician Phone"] = localPhysicianVar.PhoneWork;
                            }
                            else
                            {
                                gist["Physician"] = string.Empty;
                                gist["Physician NPI"] = string.Empty;
                                gist["Physician Phone"] = string.Empty;
                            }
                        }
                        else 
                        {
                            gist["Episode Start Date"] = string.Empty;
                            gist["Physician"] = string.Empty;
                            gist["Physician NPI"] = string.Empty;
                            gist["Physician Phone"] = string.Empty;
                        }
                    }
                    else 
                    {
                        gist["Episode Start Date"] = string.Empty;
                        gist["Physician"] = string.Empty;
                        gist["Physician NPI"] = string.Empty;
                        gist["Physician Phone"] = string.Empty;
                    }
                    //Preprocess the Primary Insurance Information.
                    
                    var primaryInsuranceNumberStaging = gist["PrimaryInsuranceNumber"];
                    if (primaryInsuranceNumberStaging.IsNotNullOrEmpty())
                    {
                        int primaryInsuranceId = 0;
                        if (int.TryParse(primaryInsuranceNumberStaging,out primaryInsuranceId))
                        {
                            if (primaryInsuranceId < 1000)
                            {
                                var primaryInsuranceNumberTemp = GetAxxessLookUpInsurance(primaryInsuranceId);
                                if (primaryInsuranceNumberTemp != null)
                                {
                                    gist["Primary Insurance"] = primaryInsuranceNumberTemp;
                                }
                                else
                                {
                                    gist["Primary Insurance"] = string.Empty;
                                }
                            }
                        }
                        else 
                        {
                            gist["Primary Insurance"] = string.Empty;
                        }
                    }
                    else 
                    {
                        gist["Primary Insurance"] = string.Empty;
                    }

                    //var clinicianIdStaging = gist["Clinician Assigned"];
                    //if (clinicianIdStaging.IsNotNullOrEmpty())
                    //{
                    //    Guid gClinicianIdStaging = clinicianIdStaging.ToGuid();
                    //    if (!gClinicianIdStaging.IsNull() && gClinicianIdStaging.IsNotEmpty())
                    //    {
                    //        var clinicianName = getClinicianName(AgencyId, gClinicianIdStaging);
                    //        if (clinicianName != null)
                    //        {
                    //            gist["Clinician Assigned"] = clinicianName;
                    //        }
                    //        else
                    //        {
                    //            gist["Clinician Assigned"] = string.Empty;
                    //        }
                    //    }
                    //}
                    //else 
                    //{
                    //    gist["Clinician Assigned"] = string.Empty;
                    //}

                    var dnrStaging = gist["DNR"];
                    if (dnrStaging.IsNotNullOrEmpty()) 
                    {
                        var dnrTemp = GetDNRRecord(dnrStaging);
                        if (dnrTemp.IsNotNullOrEmpty())
                        {
                            gist["DNR"] = dnrTemp;
                        }
                        else 
                        {
                            gist["DNR"] = string.Empty;
                        }
                    }

                    var dmeStaging = gist["DME"];
                    if (dmeStaging.IsNotNullOrEmpty()) 
                    {
                        var dmeTemp = GetDMERecord(dmeStaging);
                        if (dmeTemp.IsNotNullOrEmpty())
                        {
                            gist["DME"] = dmeTemp;
                        }
                        else 
                        {
                            gist["DME"] = string.Empty;
                        }
                    }

                    var ethStaging = gist["Ethnicity"];
                    if (ethStaging.IsNotNullOrEmpty())
                    {
                        var ethTemp = GetEthnicityRecord(ethStaging);
                        if (ethTemp.IsNotNullOrEmpty())
                        {
                            gist["Ethnicity"] = ethTemp;
                        }
                        else
                        {
                            gist["Ethnicity"] = string.Empty;
                        }
                    }

                    var paySourceStaging = gist["Payment Source"];
                    if (paySourceStaging.IsNotNullOrEmpty())
                    {
                        var paySourceTemp = GetPaymentSourceRecord(paySourceStaging);
                        if (paySourceTemp.IsNotNullOrEmpty())
                        {
                            gist["Payment Source"] = paySourceTemp;
                        }
                        else
                        {
                            gist["Payment Source"] = string.Empty;
                        }
                    }

                    gist.Remove("PrimaryInsuranceNumber");
                    gist.Remove("PatientId");
                });
                #endregion

                var headerRow = sheet.CreateRow(rowIndex);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    var hCell = headerRow.CreateCell(colIndex);
                    hCell.SetCellValue(kvp.Key);
                    colIndex++;
                }
                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;
                
                data.ForEach(list =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        if (kvp.Value != null)
                        {
                            string value = kvp.Value.Trim();
                            var cell = dataRow.CreateCell(colIndex);
                            if (value.IsNotNullOrEmpty())
                            {
                                cell.SetCellValue(value);
                            }
                            if (value == "")
                            {
                                cell.SetCellValue(string.Empty);
                            }
                        }
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });
                workBook.FinishWritingToExcelSpreadsheet(columnSize);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Episode Information found!");
            }
        }
        #endregion

        # region Helper Methods

    

        private string GetEpisodeStartDate(Guid AgencyId, Guid PatientId) 
        {
            string outResult = default(string);

            PatientEpisode homernomer = new PatientEpisode();
            //homernomer = patientService.GetCurrentEpisode(AgencyId, PatientId);
            if (homernomer != null)
            {
                outResult = homernomer.StartDate.ToShortDateString();
                return outResult;
            }
            else 
            {
                return outResult;
            }
        }

        private string GetEpisodeEndDate(Guid AgencyId, Guid PatientId)
        {
            string outResult = default(string);

            PatientEpisode homernomer = new PatientEpisode();
            //homernomer = patientService.GetCurrentEpisode(AgencyId, PatientId);
            outResult = homernomer.EndDate.ToShortDateString();
            return outResult;
        }

        private AgencyPhysician GetPhysicianForAPId(Guid AgencyId, Guid PatientId) 
        {
            AgencyPhysician outPhysicianResult = new AgencyPhysician();

            Guid PhysicianId = Guid.Empty;

            if (PhysicianId != null && PhysicianId != Guid.Empty)
            {
                bool doesPhysicianExist = default(bool);

                //doesPhysicianExist = physicianService.doesPhysicianExist(PatientId, PhysicianId);

                if (doesPhysicianExist != false)
                {
                    //outPhysicianResult = physicianService.getPhysicianOnly(PhysicianId, AgencyId);
                    return outPhysicianResult;
                }
                else 
                {
                    return null;
                }
            }
            else 
            {
                return null;
            }
        }

        private string GetAxxessLookUpInsurance(int id) 
        {
            string outResult = default(string);

            //outResult = lookupService.GetInsuranceName(id);

            return outResult;
        }

        private string GetDNRRecord(string DNR)
        {
            string outResult = default(string);
            Dictionary<string, string> DNRDictionary = new Dictionary<string, string>() 
            { 
                { "0", "No" },
                { "1", "Yes" }
            };
            if (DNRDictionary.ContainsKey(DNR))
            {
                string value = default(string);
                if (DNRDictionary.TryGetValue(DNR, out value))
                {
                    outResult = value;
                }
            }
            else
            {
                outResult = string.Empty;
            }
            return outResult;
        }

        private string GetDMERecord(string DME) 
        {
            string outResult = default(string);

            string[] inputStrings = DME.Split(';');
            int inputStringLength = default(int);
            var temp = new List<string>();
            foreach (var s in inputStrings)
            {
                if (!string.IsNullOrEmpty(s))
                    temp.Add(s);
            }
            inputStrings = temp.ToArray();
            inputStringLength = inputStrings.Length;
            Dictionary<string, string> DMEDictionary = new Dictionary<string, string>() 
            {
                {"0","Bedside Commode"},
                {"1","Cane"},
                {"2","Elevated Toilet Seat"},
                {"3","Grab bars"},
                {"4","Hospital Bed"},
                {"5","Nebulizer"},
                {"6","Oxygen"},
                {"7","Tub/Shower Bench"},
                {"8","Walker"},
                {"9","WheelChair"},
                {"10","Other"}
            };
            int iterator = 1;
            foreach (string s in inputStrings) 
            {
                if (DMEDictionary.ContainsKey(s)) 
                {
                    outResult += DMEDictionary[s];
                    if (iterator != inputStringLength)
                    {
                        outResult += ", ";
                    }
                }
                iterator++;
            }
            return outResult;
        }

        private string GetEthnicityRecord(string Ethnicity)
        {
            string outResult = default(string);

            string[] inputStrings = Ethnicity.Split(';');
            int inputStringLength = default(int);
            var temp = new List<string>();
            foreach (var s in inputStrings)
            {
                if (!string.IsNullOrEmpty(s))
                    temp.Add(s);
            }
            inputStrings = temp.ToArray();
            inputStringLength = inputStrings.Length;
            Dictionary<string, string> EthnicityDictionary = new Dictionary<string, string>() 
            {
                {"0","American Indian (or) Alaskan Native"},
                {"1","Asian"},
                {"2","Black (or) African American"},
                {"3","Hispanic (or) Latino"},
                {"4","Native Hawaiian (or) Pacific Islander"},
                {"5","White"},
                {"6","Unknown"}
            };
            int iterator = 1;
            foreach (string s in inputStrings)
            {
                if (EthnicityDictionary.ContainsKey(s))
                {
                    outResult += EthnicityDictionary[s];
                    if (iterator != inputStringLength)
                    {
                        outResult += ", ";
                    }
                }
                iterator++;
            }
            return outResult;
        }

        private string GetPaymentSourceRecord(string paymentSource)
        {
            string outResult = default(string);

            string[] inputStrings = paymentSource.Split(';');
            int inputStringLength = default(int);
            var temp = new List<string>();
            foreach (var s in inputStrings)
            {
                if (!string.IsNullOrEmpty(s))
                    temp.Add(s);
            }
            inputStrings = temp.ToArray();
            inputStringLength = inputStrings.Length;
            Dictionary<string, string> PaymentSourceDictionary = new Dictionary<string, string>() 
            {
                {"0","None, No charge for current services"},
                {"1","Medicare (traditional fee-for-service)"},
                {"2","Medicare (HMO/Managed Care)"},
                {"3","Medicaid (traditional fee-for-service)"},
                {"4","Medicaid (HMO/Managed Care)"},
                {"5","Worker's Compensation"},
                {"6","Title programs (e.g., Title III, V or XX)"},
                {"7","Other government (e.g., CHAMPUS, VA, etc)"},
                {"8","Private Insurance"},
                {"9","Private (HMO/Managed Care)"},
                {"10","Self-Pay"},
                {"11","Unknown"},
                {"12","Other (Specify)"}
            };
            int iterator = 1;
            foreach (string s in inputStrings)
            {
                if (PaymentSourceDictionary.ContainsKey(s))
                {
                    outResult += PaymentSourceDictionary[s];
                    if (iterator != inputStringLength)
                    {
                        outResult += ", ";
                    }
                }
                iterator++;
            }
            return outResult;
        }

        #endregion
    }
}
