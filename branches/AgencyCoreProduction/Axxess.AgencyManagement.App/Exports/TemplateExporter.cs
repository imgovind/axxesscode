﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
 
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;


    public class TemplateExporter : BaseExporter
    {
         private IList<AgencyTemplate> templates;
         public TemplateExporter(IList<AgencyTemplate> templates)
            : base()
        {
            this.templates = templates;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Agency Templates";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("AgencyTemplates");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Agency Templates");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Created Date");
            headerRow.CreateCell(2).SetCellValue("Modified Date");

            if (this.templates.Count > 0)
            {
                int i = 2;
                this.templates.ForEach(template =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(template.Title);
                    if (template.Created != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(1);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(template.Created);
                    }
                    else
                    {
                        dataRow.CreateCell(1).SetCellValue(string.Empty);
                    }
                    if (template.Modified != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(template.Modified);
                    }
                    else
                    {
                        dataRow.CreateCell(2).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Agency Templates: {0}", templates.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(3);
           
        }
    }
}
