﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    using Kent.Boogaart.KBCsv;

    public class PpsEpisodeExporter : BaseExporter
    {
        #region Private Members and Constructor

        private Guid branchId;
        private DateTime endDate;
        private DateTime startDate;
        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public PpsEpisodeExporter(Guid branchId, DateTime startDate, DateTime endDate)
            : base()
        {
            this.branchId = branchId;
            this.endDate = endDate;
            this.startDate = startDate;

            this.FormatType = ExportFormatType.XLS;
            this.FileName = "PPSLogEpisodeInformation.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PPS Log Episode Information";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("EpisodeInformation");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");
            DateTime startTimer = DateTime.Now;
            List<Dictionary<string, string>> data = reportAgent.PPSEpisodeInformation(Current.AgencyId, branchId, startDate, endDate);
            TimeSpan endTimer = DateTime.Now - startTimer;
            if (data != null && data.Count > 0)
            {
                var rowIndex = 1;
                var colIndex = 0;
                var columnSize = 0;
                var dictionary = data.FirstOrDefault();

                var titleRow = sheet.CreateRow(0);
                titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
                titleRow.CreateCell(1).SetCellValue("PPS Episode Information Report");
                titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
                titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1}", this.startDate.ToString("MM/dd/yyyy"), this.endDate.ToString("MM/dd/yyyy")));

                var headerRow = sheet.CreateRow(rowIndex);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    headerRow.CreateCell(colIndex).SetCellValue(kvp.Key);
                    colIndex++;
                }
                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;

                data.ForEach(list =>
                {
                    var dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        if (kvp.Value != null)
                        {
                            string value = kvp.Value.Trim();
                            bool createdCell = false;
                            if (colIndex == 4 || colIndex == 8 || colIndex == 9 || colIndex == 10 || colIndex == 12)
                            {
                                DateTime parsedDate = DateTime.MinValue;
                                bool successfullyParsedDate = DateTime.TryParse(value, out parsedDate);
                                if (successfullyParsedDate)
                                {
                                    if (parsedDate != DateTime.MinValue)
                                    {
                                        var createdDateCell = dataRow.CreateCell(colIndex);
                                        createdDateCell.CellStyle = dateStyle;
                                        createdDateCell.SetCellValue(parsedDate);
                                        createdCell = true;
                                    }
                                }
                            }
                            else if (colIndex == 7 || colIndex == 14 || colIndex == 15 || colIndex == 18 || colIndex == 20 || colIndex == 21)
                            {
                                double parsedValue = 0;
                                bool successfullyParsedValue = double.TryParse(value, out parsedValue);
                                if (successfullyParsedValue)
                                {
                                    dataRow.CreateCell(colIndex).SetCellValue(parsedValue);
                                    createdCell = true;
                                }
                            }

                            if (!createdCell)
                            {
                                dataRow.CreateCell(colIndex).SetCellValue(value.IsNotNullOrEmpty() ? value : string.Empty);
                            }
                        }
                        else
                        {
                            dataRow.CreateCell(colIndex).SetCellValue(string.Empty);
                        }
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });
                workBook.FinishWritingToExcelSpreadsheet(columnSize);
            }
            else
            {
                var errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Episode Information found!");
                workBook.FinishWritingToExcelSpreadsheet(1);
            }
        }

        #endregion

    }
}
