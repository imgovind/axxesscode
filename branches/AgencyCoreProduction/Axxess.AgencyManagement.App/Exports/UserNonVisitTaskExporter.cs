﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

    public class UserNonVisitTaskExporter : BaseExporter
    {
        private IList<UserNonVisitTask> agencyUserNonVisitTasks;

        public UserNonVisitTaskExporter(IList<UserNonVisitTask> userNonVisitTasks)
            : base()    
        {
            this.agencyUserNonVisitTasks = userNonVisitTasks;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Agency User Non-Visit Tasks";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("AgencyUserNonVisitTasks");

            var dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Agency User Non-Visit Tasks");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("User Name");
            headerRow.CreateCell(1).SetCellValue("Task Name");
            headerRow.CreateCell(2).SetCellValue("Task Date");
            headerRow.CreateCell(3).SetCellValue("Time In");
            headerRow.CreateCell(4).SetCellValue("Time Out");
            headerRow.CreateCell(5).SetCellValue("Paid Date");
            headerRow.CreateCell(6).SetCellValue("Paid Status");
            headerRow.CreateCell(7).SetCellValue("Comments");
            if (this.agencyUserNonVisitTasks.Count > 0)
            {
                int i = 2;
                this.agencyUserNonVisitTasks.ForEach(agencyUserNonVisitTask =>
                {
                    var dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(agencyUserNonVisitTask.UserDisplayName);
                    dataRow.CreateCell(1).SetCellValue(agencyUserNonVisitTask.TaskTitle); 
                    dataRow.CreateCell(2).SetCellValue(agencyUserNonVisitTask.TaskDateDateString);
                    dataRow.CreateCell(3).SetCellValue(agencyUserNonVisitTask.TimeInDateString);
                    dataRow.CreateCell(4).SetCellValue(agencyUserNonVisitTask.TimeOutDateString);
                    dataRow.CreateCell(5).SetCellValue(agencyUserNonVisitTask.PaidDateDateString);
                    dataRow.CreateCell(6).SetCellValue(agencyUserNonVisitTask.PaidStatus == true ? "Paid" : "Not Paid");
                    dataRow.CreateCell(7).SetCellValue(agencyUserNonVisitTask.Comments);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Agency User Non-Visit Tasks: {0}", agencyUserNonVisitTasks.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(5); 
        }
    }
}
