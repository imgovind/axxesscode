﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.Extensions;
    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using Axxess.AgencyManagement.Domain;

   public class PayrollSummaryExport : BaseExporter
    {
        private IList<VisitSummary> visitSummaries;
        private DateTime StartDate;
        private DateTime EndDate;
        public PayrollSummaryExport(IList<VisitSummary> visitSummaries, DateTime StartDate, DateTime EndDate)
            : base()
        {
            this.visitSummaries = visitSummaries;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Payroll Visit Summary";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            var sheet = base.workBook.CreateSheet("PayrollVisitSummary");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Payroll Visit Summary");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("User");
            headerRow.CreateCell(1).SetCellValue("Credential");
            headerRow.CreateCell(2).SetCellValue("Count");
            headerRow.CreateCell(3).SetCellValue("Total Visit Time");
            headerRow.CreateCell(4).SetCellValue("Total Travel Time");

            if (this.visitSummaries.Count > 0)
            {
                int i = 2;
                this.visitSummaries.ForEach(order =>
                {
                    if (order != null)
                    {
                        var username = order.LastName.IsNotNullOrEmpty() ? (order.FirstName.IsNotNullOrEmpty() ? order.LastName + ", " + order.FirstName : order.UserName) : order.UserName;
                        var row = sheet.CreateRow(i);
                        row.CreateCell(0).SetCellValue(username);
                        row.CreateCell(1).SetCellValue(order.Credential.IsNotNullOrEmpty() ? order.Credential : "User credential not specified");
                        row.CreateCell(2).SetCellValue(order.VisitCount);
                        row.CreateCell(3).SetCellValue(order.TotalVisitTime);
                        row.CreateCell(4).SetCellValue(order.TotalTravelTime);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Payroll Visit Summary: {0}", visitSummaries.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(2);
        }

    }
}
