﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    class ClaimResponseXml : BaseXml
    {
        private Dictionary<String, NotesQuestion> Data = new Dictionary<String, NotesQuestion>();
        public ClaimResponseXml(ClaimData data)
            : base(PdfDocs.ClaimResponse)
        {
            this.Data.Add("Response", new NotesQuestion());
            this.Data["Response"].Answer = data.Response.ToString();
            this.Init();
            if (this.Layout.Count == 0) this.Layout.Add(new XmlPrintSection(this, null));
        }
        public override string GetData(string Index)
        {
            return this.Data != null && this.Data.ContainsKey(Index) && this.Data[Index].Answer.IsNotNullOrEmpty() ? this.Data[Index].Answer : string.Empty;
        }
    }
}
