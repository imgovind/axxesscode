﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using XmlParsing;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.OasisC.Domain;
    using Axxess.Core.Extension;
    using Axxess.OasisC.Extensions;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.LookUp.Repositories;
    using Axxess.LookUp.Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.App.Domain;
    public class OasisProfilePdf : AxxessPdf {
        private OasisProfileXml xml;
        public OasisProfilePdf(AssessmentPrint data) {
            this.IsOasis = true;
            this.xml = new OasisProfileXml(data);
            this.SetType(PdfDocs.OasisProfile);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            this.SetContent(content);
            float[] margins = new float[] { 82, 28.3F, 28.3F, 28.3F };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Location;
            fieldmap[0].Add("agency", 
                    (Current.AgencyName + "\n" ) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean() : "") +
                        (location.AddressLine2.IsNotNullOrEmpty() ? ", " + location.AddressLine2.Clean() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean() + ", " : "") +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : "") +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : "") +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "")
                    : "")
                );
            this.SetFields(fieldmap);
        }
    }
}
