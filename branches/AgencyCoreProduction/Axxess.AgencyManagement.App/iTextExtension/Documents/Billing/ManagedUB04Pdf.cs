﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.App.Services;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.App.Enums;
using Axxess.AgencyManagement.Repositories;
    class ManagedUB04Pdf : UB04Pdf
    {
        public ManagedUB04Pdf(UBOFourViewData data, IBillingService billingService, IAgencyRepository agencyRepository)
            : base(data, billingService, agencyRepository)
        { 
        }
        protected override IElement[] BuildContent(UBOFourViewData data)
        {
            var content = new AxxessTable[] { new AxxessTable(new float[] { 24, 123, 73, 35, 39, 49, 49, 10 }, false) };
            var font = AxxessPdf.sans;
            font.Size = 8;
            int count = 0;
            float[] padding = new float[] { 0, 1, .05F, 1 }, borders = new float[] { 0, 0, 0, 0 }, moneypad = new float[] { 0, 9, .05F, 1 };
            if (data != null && data.Claim != null && data.Claim.IsHomeHealthServiceIncluded)
            {
                content[0].AddCell("0023", font, padding, borders);
                content[0].AddCell("HOME HEALTH SERVICES", font, padding, borders);
                content[0].AddCell(data.Claim != null && data.Claim.HippsCode.IsNotNullOrEmpty() ? data.Claim.HippsCode : string.Empty, font, padding, borders);
                content[0].AddCell(data.Claim != null && data.Claim.FirstBillableVisitDate.IsValid() ? data.Claim.FirstBillableVisitDate.ToString("MMddyyyy") : string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell("0.00", font, "Right", moneypad, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                count++;
            }

            var supplies = data.Claim != null && data.Claim.Supply.IsNotNullOrEmpty() ? data.Claim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && s.Date.IsValidDate() && !s.IsDeprecated).OrderBy(s => s.Date.ToDateTime().Date).ToList() : new List<Supply>();
            if (supplies != null && supplies.Count > 0)
            {
                var supplyTotal = 0.0;
                foreach (var supply in supplies)
                {
                    content[0].AddCell(supply.RevenueCode, font, padding, borders);
                    content[0].AddCell(supply.Description, font, padding, borders);
                    content[0].AddCell(supply.Code, font, padding, borders);
                    content[0].AddCell(supply.DateForEdit.ToString("MMddyyyy"), font, padding, borders);
                    content[0].AddCell(supply.Quantity > 0 ? supply.Quantity.ToString() : string.Empty, font, padding, borders);
                    content[0].AddCell(string.Format("{0:0.00}", supply.TotalCost), font, "Right", moneypad, borders);
                    content[0].AddCell(string.Empty, font, padding, borders);
                    content[0].AddCell(string.Empty, font, padding, borders);
                    supplyTotal += supply.TotalCost;
                    count++;
                }
                this.SupplyValue = supplyTotal;
            }
            var schedules = data.Claim != null && data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ToList() : new List<ScheduleEvent>();
            if (schedules != null && schedules.Count > 0)
            {
                var visits = billingService.BillableVisitSummary(data.Claim.AgencyLocationId, schedules, ClaimType.MAN, data.Claim.ChargeRates, true);
                if (visits != null && visits.Count > 0)
                {
                    var total = 0.0;
                    foreach (var visit in visits)
                    {
                        content[0].AddCell(visit.RevenueCode, font, padding, borders);
                        content[0].AddCell(visit.PreferredName, font, padding, borders);
                        content[0].AddCell(string.Format("{0} {1} {2} {3} {4}", visit.HCPCSCode, visit.Modifier, visit.Modifier2, visit.Modifier3, visit.Modifier4), font, padding, borders);
                        content[0].AddCell(visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MMddyyyy") : string.Empty, font, padding, borders);
                        content[0].AddCell(visit.Unit > 0 ? visit.Unit.ToString() : string.Empty, font, padding, borders);
                        content[0].AddCell(visit.Charge > 0 ? string.Format("{0:#0.00}", visit.Charge) : string.Empty, font, "Right", moneypad, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        total += visit.Charge;
                        count++;
                    }
                    this.VisitValue = total;
                }
            }
            if (count == 0)
            {
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, "Right", moneypad, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
            }
            return content;
        }

        protected override double CalculateTotal(UBOFourViewData data)
        {
            return this.SupplyValue + this.VisitValue;
        }
    }
}