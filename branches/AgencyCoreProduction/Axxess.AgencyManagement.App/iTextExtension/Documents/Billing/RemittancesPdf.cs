﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using iTextSharp.text;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.App.Helpers;
    class RemittancesPdf : AxxessPdf {
        public RemittancesPdf(PrintViewData<IList<RemittanceLean>> data) {
            this.SetType(PdfDocs.Remittances);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            if (data!=null || !data.Data.IsNotNullOrEmpty()) {
                Paragraph[] content = new Paragraph[] { new Paragraph(" ") };
                this.SetContent(content);
            } else {
                AxxessTable[] content = new AxxessTable[1];
                float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { 0, 0, .2F, 0 };
                content[0] = new AxxessTable(new float[] { 10, 61, 44, 40, 43, 32 }, false);
                int count = 0;
                foreach (var remittance in data.Data) {
                    content[0].AddCell((++count).ToString(), fonts[0], padding, borders);
                    content[0].AddCell(remittance.RemitId, fonts[0], padding, borders);
                    content[0].AddCell(remittance.RemittanceDate > DateTime.MinValue ? remittance.RemittanceDate.ToString("MM/dd/yyyy") : string.Empty, fonts[0], padding, borders);
                    content[0].AddCell(remittance.PaymentDate > DateTime.MinValue ? remittance.PaymentDate.ToString("MM/dd/yyyy") : string.Empty, fonts[0], padding, borders);
                    content[0].AddCell(string.Format("${0:#,0.00}", remittance.PaymentAmount), fonts[0], padding, borders);
                    content[0].AddCell(remittance.TotalClaims.ToString(), fonts[0], padding, borders);
                }
                this.SetContent(content);
            }
            this.SetMargins(new float[] { 100, 28, 28, 28 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            this.SetFields(fieldmap);
        }
    }
}