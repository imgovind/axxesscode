﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class ComNotePdf : AxxessPdf {
        public ComNotePdf(CommunicationNote data) {
            this.SetType(PdfDocs.ComNote);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(data != null && data.Text.IsNotNullOrEmpty() ? data.Text : " ", fonts[0]) };
            this.SetContent(content);
            float[] margins = new float[] { 185, 35, 75, 35 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });

            if (data.Agency != null)
            {
                var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
                
                    fieldmap[0].Add("agency", (
                        data != null && data.Agency != null ?
                            (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : String.Empty) +
                            (location != null ?
                                (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean() : String.Empty) +
                                (location.AddressLine2.IsNotNullOrEmpty() ? ", " + location.AddressLine2.Clean() + "\n" : "\n") +
                                (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean() + ", " : String.Empty) +
                                (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                                (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                                (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                                (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                            : String.Empty)
                        : String.Empty));
            }
            fieldmap[0].Add("patient", (data != null && data.Patient != null ? (data.Patient.AddressLine1.IsNotNullOrEmpty() ? data.Patient.AddressLine1.Clean() + "\n" : "") + (data.Patient.AddressLine2.IsNotNullOrEmpty() ? data.Patient.AddressLine2.Clean() + "\n" : "") + (data.Patient.AddressCity.IsNotNullOrEmpty() ? data.Patient.AddressCity.Clean() + ", " : "") + (data.Patient.AddressStateCode.IsNotNullOrEmpty() ? data.Patient.AddressStateCode.ToTitleCase() + "  " : "") + (data.Patient.AddressZipCode.IsNotNullOrEmpty() ? data.Patient.AddressZipCode + "\n" : "") + (data.Patient.PhoneHome.IsNotNullOrEmpty() ? data.Patient.PhoneHome.ToPhone() + "\n" : "") + (data.Patient.MedicareNumber.IsNotNullOrEmpty() ? "HIC: " + data.Patient.MedicareNumber : "") : ""));
            fieldmap[0].Add("physician", (data != null && data.Physician != null ? (data.Physician.AddressLine1.IsNotNullOrEmpty() ? data.Physician.AddressLine1.Clean() : "") + (data.Physician.AddressLine2.IsNotNullOrEmpty() ? ", " + data.Physician.AddressLine2.Clean() + "\n" : "\n") + (data.Physician.AddressCity.IsNotNullOrEmpty() ? data.Physician.AddressCity.Clean() + ", " : "") + (data.Physician.AddressStateCode.IsNotNullOrEmpty() ? data.Physician.AddressStateCode.ToTitleCase() + "  " : "") + (data.Physician.AddressZipCode.IsNotNullOrEmpty() ? data.Physician.AddressZipCode + "\n" : "") + (data.Physician.PhoneWork.IsNotNullOrEmpty() ? "Phone: " + data.Physician.PhoneWork.ToPhone() : "") + (data.Physician.FaxNumber.IsNotNullOrEmpty() ? " | Fax: " + data.Physician.FaxNumber.ToPhone() + "\n" : "\n") + (data.Physician.NPI.IsNotNullOrEmpty() ? "NPI: " + data.Physician.NPI : "") : ""));
            fieldmap[0].Add("dob", data != null && data.Patient != null && data.Patient.DOB.IsValid() ? data.Patient.DOB.ToShortDateString() : string.Empty);
            fieldmap[0].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("date", data != null && data.Created.IsValid() ? data.Created.ToShortDateString() : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate.IsValid() ? data.SignatureDate.ToShortDateString() : "");
            fieldmap[1].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToUpper() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToUpper() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            fieldmap[1].Add("physicianname", data != null && data.Physician != null && data.Physician.DisplayName.IsNotNullOrEmpty() ? data.Physician.DisplayName : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}