﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.Core.Infrastructure;
    using Axxess.LookUp.Repositories;
    class MedicareEligibilityPdf : AxxessPdf {
        public MedicareEligibilityPdf(PatientEligibility data, Agency agency, Patient patient) {
            this.SetType(PdfDocs.MedicareEligibility);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            int TableCount = 1;
            AxxessTable[] content = null;
            if (data == null) {
                content = new AxxessTable[TableCount];
                content[0] = new AxxessTable(1);
                content[0].AddCell("Server Error: No Medicare Eligibility Report found for this patient!");
            } else {
                if (data.Request_Result != null && data.Request_Result.success.IsNotNullOrEmpty() && data.Request_Result.success.IsEqual("no")) {
                    content = new AxxessTable[TableCount];
                    content[0] = new AxxessTable(1);
                    content[0].AddCell(string.Format("Server Error: {0}", 
                        data.Request_Result.response.IsNotNullOrEmpty() ? data.Request_Result.response : ""));
                } else if (data.Request_Validation != null && data.Request_Validation.success.IsNotNullOrEmpty() && data.Request_Validation.success.IsEqual("yes")) {
                    content = new AxxessTable[TableCount];
                    content[0] = new AxxessTable(1);
                    content[0].AddCell("Server Error:\n" +
                        (data.Request_Validation.reject_reason_code.IsNotNullOrEmpty() ? data.Request_Validation.reject_reason_code : string.Empty) + ", " +
                        (data.Request_Validation.follow_up_action_code.IsNotNullOrEmpty() ? data.Request_Validation.follow_up_action_code : string.Empty));
                } else if (data.Functional_Acknowledgment != null && data.Functional_Acknowledgment.success.IsNotNullOrEmpty() && data.Functional_Acknowledgment.success.IsEqual("yes")) {
                    content = new AxxessTable[TableCount];
                    content[0] = new AxxessTable(1);
                    content[0].AddCell("Server Error:\n" +
                        (data.Functional_Acknowledgment.segment_syntax_error_code.IsNotNullOrEmpty() ? data.Functional_Acknowledgment.segment_syntax_error_code : string.Empty) + ", " +
                        (data.Functional_Acknowledgment.copy_of_bad_data_element.IsNotNullOrEmpty() ? data.Functional_Acknowledgment.copy_of_bad_data_element : string.Empty));
                } else {
                    if (data.Episode != null && data.Episode.name.IsNotNullOrEmpty()) TableCount++;
                    if (data.Health_Benefit_Plan_Coverage != null && data.Health_Benefit_Plan_Coverage.success.IsNotNullOrEmpty() && data.Health_Benefit_Plan_Coverage.success.IsEqual("yes")) TableCount++;
                    float[] padding = new float[] { 0, 0, 5, 0 }, borders = new float[] { .5F, 0, 0, 0 };
                    content = new AxxessTable[TableCount];
                    content[0] = new AxxessTable(1);
                    if (data != null && data.Subscriber != null && data.Subscriber.success.IsNotNullOrEmpty() && data.Subscriber.success.IsEqual("yes"))
                    {
                        AxxessCell headercell = new AxxessCell(padding, borders);
                        AxxessCell contentcell = new AxxessCell(padding, borders);
                        headercell.AddElement(new AxxessTitle("Medicare", fonts[1]));
                        content[0].AddCell(headercell);
                        content[0].HeaderRows = 1;
                        if (data.Medicare_Part_A != null && data.Medicare_Part_A.success.IsNotNullOrEmpty() && data.Medicare_Part_A.success.IsEqual("yes")) contentcell.AddElement(new AxxessContentSection("Part A:", fonts[1], data.Medicare_Part_A.eligibility_or_benefit_information.IsNotNullOrEmpty() && data.Medicare_Part_A.eligibility_or_benefit_information == "Inactive" ? "Inactive" : data.Medicare_Part_A.date.IsNotNullOrEmpty() ? data.Medicare_Part_A.date.ToDateTimeString() + " - Current" : string.Empty, fonts[0], 12));
                        if (data.Medicare_Part_B != null && data.Medicare_Part_B.success.IsNotNullOrEmpty() && data.Medicare_Part_B.success.IsEqual("yes")) contentcell.AddElement(new AxxessContentSection("Part B:", fonts[1], data.Medicare_Part_B.eligibility_or_benefit_information.IsNotNullOrEmpty() && data.Medicare_Part_B.eligibility_or_benefit_information == "Inactive" ? "Inactive" : data.Medicare_Part_B.date.IsNotNullOrEmpty() ? data.Medicare_Part_B.date.ToDateTimeString() + " - Current" : string.Empty, fonts[0], 12));
                        if (data.Health_Benefit_Plan_Coverage != null && !data.Health_Benefit_Plan_Coverage.success.IsEqual("yes")) contentcell.AddElement(new AxxessContentSection("Primary Payer:", fonts[1], "Medicare", fonts[0], 12));
                        content[0].AddCell(contentcell);
                        if (data.Episode != null && data.Episode.name.IsNotNullOrEmpty())
                        {
                            content[1] = new AxxessTable(1);
                            headercell = new AxxessCell(padding, borders);
                            contentcell = new AxxessCell(padding, borders);
                            headercell.AddElement(new AxxessTitle("Last Home Care Episode", fonts[1]));
                            content[1].AddCell(headercell);
                            content[1].HeaderRows = 1;
                            contentcell.AddElement(new AxxessContentSection("Payer:", fonts[1], data.Episode.name.IsNotNullOrEmpty() ? data.Episode.name : string.Empty, fonts[0], 12));
                            string npi = data.Episode.reference_id.IsNotNullOrEmpty() ? data.Episode.reference_id : string.Empty;
                            contentcell.AddElement(new AxxessContentSection("NPI#:", fonts[1], npi, fonts[0], 12));
                            var agencyData = Container.Resolve<ILookUpDataProvider>().LookUpRepository.GetNpiData(npi);
                            if (agencyData != null)
                            {
                                contentcell.AddElement(new AxxessContentSection("Name:", fonts[1], agencyData.ProviderOrganizationName.IsNotNullOrEmpty() ? agencyData.ProviderOrganizationName : string.Empty, fonts[0], 12));
                                contentcell.AddElement(new AxxessContentSection("Address:", fonts[1], agencyData.ProviderFirstLineBusinessPracticeLocationAddress + " " + agencyData.ProviderSecondLineBusinessPracticeLocationAddress + "\n" + agencyData.ProviderBusinessPracticeLocationAddressCityName + ", " + agencyData.ProviderBusinessMailingAddressStateName + "  " + agencyData.ProviderBusinessMailingAddressPostalCode, fonts[0], 12));
                                contentcell.AddElement(new AxxessContentSection("Phone:", fonts[1], agencyData.ProviderBusinessPracticeLocationAddressTelephoneNumber.IsNotNullOrEmpty() ? agencyData.ProviderBusinessPracticeLocationAddressTelephoneNumber.ToPhone() : string.Empty, fonts[0], 12));
                                contentcell.AddElement(new AxxessContentSection("Fax:", fonts[1], agencyData.ProviderBusinessPracticeLocationAddressFaxNumber.IsNotNullOrEmpty() ? agencyData.ProviderBusinessPracticeLocationAddressFaxNumber.ToPhone() : string.Empty, fonts[0], 12));
                            }
                            contentcell.AddElement(new AxxessContentSection("Episode Date Range:", fonts[1], (data.Episode.period_start.IsNotNullOrEmpty() ? data.Episode.period_start.ToDateTimeString() : string.Empty) + (data.Episode.period_end.IsNotNullOrEmpty() ? " - " + data.Episode.period_end.ToDateTimeString() : string.Empty), fonts[0], 12));
                            content[1].AddCell(contentcell);
                        }
                        if (data.Health_Benefit_Plan_Coverage != null && data.Health_Benefit_Plan_Coverage.success.IsNotNullOrEmpty() && data.Health_Benefit_Plan_Coverage.success.IsEqual("yes"))
                        {
                            content[content.Count() - 1] = new AxxessTable(1);
                            headercell = new AxxessCell(padding, borders);
                            contentcell = new AxxessCell(padding, borders);
                            headercell.AddElement(new AxxessTitle("Health Benefit Plan Coverage", fonts[1]));
                            content[content.Count() - 1].AddCell(headercell);
                            content[content.Count() - 1].HeaderRows = 1;
                            contentcell.AddElement(new AxxessContentSection("Health Benefit Plan Coverage:", fonts[1], data.Health_Benefit_Plan_Coverage.name.IsNotNullOrEmpty() ? data.Health_Benefit_Plan_Coverage.name : string.Empty, fonts[0], 12));
                            contentcell.AddElement(new AxxessContentSection("Insurance Type:", fonts[1], data.Health_Benefit_Plan_Coverage.insurance_type.IsNotNullOrEmpty() ? data.Health_Benefit_Plan_Coverage.insurance_type : string.Empty, fonts[0], 12));
                            contentcell.AddElement(new AxxessContentSection("Reference Id Qualifier:", fonts[1], data.Health_Benefit_Plan_Coverage.reference_id_qualifier.IsNotNullOrEmpty() ? data.Health_Benefit_Plan_Coverage.reference_id_qualifier : string.Empty, fonts[0], 12));
                            contentcell.AddElement(new AxxessContentSection("Address:", fonts[1], data.Health_Benefit_Plan_Coverage.address1.IsNotNullOrEmpty() ? data.Health_Benefit_Plan_Coverage.address1 : string.Empty, fonts[0], 12));
                            contentcell.AddElement(new AxxessContentSection("Phone Number:", fonts[1], data.Health_Benefit_Plan_Coverage.phone.IsNotNullOrEmpty() ? data.Health_Benefit_Plan_Coverage.phone : string.Empty, fonts[0], 12));
                            contentcell.AddElement(new AxxessContentSection("Health Benefit Plan Payer:", fonts[1], data.Health_Benefit_Plan_Coverage.payer.IsNotNullOrEmpty() ? data.Health_Benefit_Plan_Coverage.payer : string.Empty, fonts[0], 12));
                            contentcell.AddElement(new AxxessContentSection("Reference Id:", fonts[1], data.Health_Benefit_Plan_Coverage.reference_id.IsNotNullOrEmpty() ? data.Health_Benefit_Plan_Coverage.reference_id : string.Empty, fonts[0], 12));
                            contentcell.AddElement(new AxxessContentSection("Date:", fonts[1], data.Health_Benefit_Plan_Coverage.date.IsNotNullOrEmpty() ? data.Health_Benefit_Plan_Coverage.date : string.Empty, fonts[0], 12));
                            contentcell.AddElement(new AxxessContentSection("City:", fonts[1], data.Health_Benefit_Plan_Coverage.city.IsNotNullOrEmpty() ? data.Health_Benefit_Plan_Coverage.city : string.Empty, fonts[0], 12));
                            contentcell.AddElement(new AxxessContentSection("State:", fonts[1], data.Health_Benefit_Plan_Coverage.state.IsNotNullOrEmpty() ? data.Health_Benefit_Plan_Coverage.state : string.Empty, fonts[0], 12));
                            content[content.Count() - 1].AddCell(contentcell);
                        }
                    }
                    else
                    {
                        AxxessCell headercell = new AxxessCell(padding, borders);
                        headercell.AddElement(new AxxessTitle("Patient not found", fonts[1]));
                        content[0].AddCell(headercell);
                    }
                }
            }
            this.SetContent(content);
            this.SetMargins(new float[] { 150, 28.3F, 90.5F, 28.3F });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = agency.GetBranch(patient != null ? patient.AgencyLocationId : Guid.Empty);
            if (location == null) location = agency.GetMainOffice();
            fieldmap[0].Add("agency", (
                data != null && agency != null ?
                    (agency.Name.IsNotNullOrEmpty() ? agency.Name + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? ", " + location.AddressLine2.Clean() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            if (data != null && data.Subscriber != null && data.Subscriber.success == "yes")
            {
                fieldmap[0].Add("mcare", data != null && data.Subscriber != null && data.Subscriber.identification_code.IsNotNullOrEmpty() ? data.Subscriber.identification_code : "");
                fieldmap[0].Add("dob", data != null && data.Subscriber != null && data.Subscriber.date.IsNotNullOrEmpty() ? data.Subscriber.date.ToDateTimeString() : "");
                fieldmap[0].Add("gender", data != null && data.Subscriber != null && data.Subscriber.gender.IsNotNullOrEmpty() ? data.Subscriber.gender : "");
                fieldmap[1].Add("patientname", data != null && data.Subscriber != null ? (data.Subscriber.last_name.IsNotNullOrEmpty() ? data.Subscriber.last_name.ToUpper() + ", " : "") + (data.Subscriber.first_name.IsNotNullOrEmpty() ? data.Subscriber.first_name.ToUpper() + " " : "") + (data.Subscriber.middle_name.IsNotNullOrEmpty() ? data.Subscriber.middle_name.Substring(0, 1).ToUpper() + "\n" : "\n") : "");
            }
            else
            {
                fieldmap[0].Add("mcare", patient != null && patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber : "");
                fieldmap[0].Add("dob", patient != null && patient.DOB.IsValid() ? patient.DOB.ToZeroFilled() : "");
                fieldmap[0].Add("gender", patient != null && patient.Gender.IsNotNullOrEmpty() ? patient.Gender : "");
                fieldmap[1].Add("patientname", patient != null && patient.DisplayNameWithMi.IsNotNullOrEmpty() ? patient.DisplayNameWithMi : "");
            }
            this.SetFields(fieldmap);
        }
    }
}