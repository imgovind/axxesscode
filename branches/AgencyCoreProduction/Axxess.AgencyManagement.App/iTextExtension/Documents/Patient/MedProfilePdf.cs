﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.App.Helpers;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class MedProfilePdf : AxxessPdf
    {
        public MedProfilePdf(PrintViewData<MedicationProfileViewData> data)
        {
            this.SetType(PdfDocs.MedProfile);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            var medicationData = data.Data != null && data.Data.MedicationProfile != null && data.Data.MedicationProfile.Medication.IsNotNullOrEmpty() ? data.Data.MedicationProfile.Medication.ToObject<List<Medication>>().OrderByDescending(m => m.StartDateSortable).ToList() : new List<Medication>();
            this.SetContent(this.BuildContent(medicationData, fonts));
            this.SetMargins(new float[] { 250, 28, 90.5F, 28 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("episode", (data.Data != null && data.Data.StartDate.IsValid() ? data.Data.StartDate.ToShortDateString() : "") + " - " + (data.Data != null && data.Data.EndDate.IsValid() ? data.Data.EndDate.ToShortDateString() : ""));
            fieldmap[0].Add("pridiagnosis", data.Data != null && data.Data.Questions != null && data.Data.Questions.ContainsKey("M1020PrimaryDiagnosis") ? data.Data.Questions["M1020PrimaryDiagnosis"].Answer : "");
            fieldmap[0].Add("secdiagnosis", data.Data != null && data.Data.Questions != null && data.Data.Questions.ContainsKey("M1022PrimaryDiagnosis1") ? data.Data.Questions["M1022PrimaryDiagnosis1"].Answer : "");
            fieldmap[0].Add("physician", data.Data != null && data.Data.PhysicianDisplayName.IsNotNullOrEmpty() ? data.Data.PhysicianDisplayName : "");
            fieldmap[0].Add("pripharmacy", data.Data != null &&data.Data.PrimaryPharmacy!=null && data.Data.PrimaryPharmacy.Name.IsNotNullOrEmpty() ? data.Data.PrimaryPharmacy.Name : "");
            fieldmap[0].Add("pripharmphone", data.Data != null && data.Data.PrimaryPharmacy !=null&& data.Data.PrimaryPharmacy.Phone.IsNotNullOrEmpty() ? data.Data.PrimaryPharmacy.Phone.ToPhone() : "");
            fieldmap[0].Add("pripharmfax", data.Data != null && data.Data.PrimaryPharmacy != null && data.Data.PrimaryPharmacy.FaxNumber.IsNotNullOrEmpty() ? data.Data.PrimaryPharmacy.FaxNumber.ToPhone() : "");
            fieldmap[0].Add("secpharmacy", data.Data != null && data.Data.SecondaryPharmacy != null && data.Data.SecondaryPharmacy.Name.IsNotNullOrEmpty() ? data.Data.SecondaryPharmacy.Name : "");
            fieldmap[0].Add("secpharmphone", data.Data != null && data.Data.SecondaryPharmacy != null && data.Data.SecondaryPharmacy.Phone.IsNotNullOrEmpty() ? data.Data.SecondaryPharmacy.Phone.ToPhone() : "");
            fieldmap[0].Add("secpharmfax", data.Data != null && data.Data.SecondaryPharmacy != null && data.Data.SecondaryPharmacy.FaxNumber.IsNotNullOrEmpty() ? data.Data.SecondaryPharmacy.FaxNumber.ToPhone() : "");
            fieldmap[0].Add("allergies", data.Data != null && data.Data.Allergies.IsNotNullOrEmpty() ? data.Data.Allergies : "");
            fieldmap[0].Add("sign", data.Data != null && data.Data.SignatureName.IsNotNullOrEmpty() ? data.Data.SignatureName : "");
            fieldmap[0].Add("signdate", data.Data != null && data.Data.SignatureDate.IsValid() ? data.Data.SignatureDate.ToShortDateString() : "");
            fieldmap[1].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            this.SetFields(fieldmap);
        }

        public MedProfilePdf(PrintViewData<MedicationProfileSnapshotViewData> data)
        {
            this.SetType(PdfDocs.MedProfile);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            var medicationData = data.Data != null && data.Data.MedicationProfile != null && data.Data.MedicationProfile.Medication.IsNotNullOrEmpty() ? data.Data.MedicationProfile.Medication.ToObject<List<Medication>>().OrderByDescending(m => m.StartDateSortable).ToList() : new List<Medication>();
            this.SetContent(this.BuildContent(medicationData, fonts));
            this.SetMargins(new float[] { 175, 28, 120, 28 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[0].Add("mr", data.Data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            fieldmap[0].Add("physician", data.Data != null && data.Data.PhysicianName.IsNotNullOrEmpty() ? data.Data.PhysicianName : "");
            fieldmap[0].Add("episode", (data.Data != null && data.Data.StartDate.IsValid() ? data.Data.StartDate.ToShortDateString() : "") + " - " + (data.Data != null && data.Data.EndDate.IsValid() ? data.Data.EndDate.ToShortDateString() : ""));

            fieldmap[0].Add("pripharmacy", data.Data != null && data.Data.PharmacyName.IsNotNullOrEmpty() ? data.Data.PharmacyName : "");
            fieldmap[0].Add("pripharmphone", data.Data != null && data.Data.PharmacyPhone.IsNotNullOrEmpty() ? data.Data.PharmacyPhone.ToPhone() : "");
            fieldmap[0].Add("pripharmfax", data.Data != null && data.Data.PharmacyFax.IsNotNullOrEmpty() ? data.Data.PharmacyFax.ToPhone() : "");
           
            fieldmap[0].Add("secpharmacy", data.Data != null && data.Data.SecondaryPharmacyName.IsNotNullOrEmpty() ? data.Data.SecondaryPharmacyName : "");
            fieldmap[0].Add("secpharmphone", data.Data != null  && data.Data.SecondaryPharmacyPhone.IsNotNullOrEmpty() ? data.Data.SecondaryPharmacyPhone.ToPhone() : "");
            fieldmap[0].Add("secpharmfax", data.Data != null && data.Data.SecondaryPharmacyFax.IsNotNullOrEmpty() ? data.Data.SecondaryPharmacyFax.ToPhone() : "");
            
            fieldmap[0].Add("pridiagnosis", data.Data != null && data.Data.PrimaryDiagnosis.IsNotNullOrEmpty() ? data.Data.PrimaryDiagnosis : "");
            fieldmap[0].Add("secdiagnosis", data.Data != null && data.Data.SecondaryDiagnosis.IsNotNullOrEmpty() ? data.Data.SecondaryDiagnosis : "");
            fieldmap[0].Add("allergies", data.Data != null && data.Data.Allergies.IsNotNullOrEmpty() ? data.Data.Allergies : "");
            fieldmap[0].Add("sign", data.Data != null && data.Data.SignatureText.IsNotNullOrEmpty() ? data.Data.SignatureText : "");
            fieldmap[0].Add("signdate", data.Data != null && data.Data.SignatureDate.IsValid() ? data.Data.SignatureDate.ToShortDateString() : "");
            this.SetFields(fieldmap);
        }
        private AxxessTable[] BuildContent(List<Medication> medications, List<Font> fonts)
        {
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(1) };
            if (medications.Count > 0)
            {
                AxxessTable active = new AxxessTable(new float[] { 2.85F, 11.67F, 31.91F, 12.97F, 16.86F, 2.59F, 21.15F }, true);
                AxxessTable discont = new AxxessTable(new float[] { 2.85F, 11.67F, 27.91F, 12.97F, 16.86F, 2.59F, 20.15F, 11.67F }, true);
                AxxessCell lsTitle = new AxxessCell(new float[] { 0, 1, 8, 1 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell dateTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell medTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell freqTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell routeTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell typeTitle = new AxxessCell(new float[] { 0, 4, 8, 4 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell clasTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                AxxessCell dcTitle = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                lsTitle.AddElement(new Chunk("LS", fonts[1]));
                dateTitle.AddElement(new Chunk("Start Date", fonts[1]));
                medTitle.AddElement(new Chunk("Medication & Dosage", fonts[1]));
                freqTitle.AddElement(new Chunk("Frequency", fonts[1]));
                routeTitle.AddElement(new Chunk("Route", fonts[1]));
                
                typeTitle.AddElement(new Chunk("", fonts[1]));
                clasTitle.AddElement(new Chunk("Classification", fonts[1]));
                dcTitle.AddElement(new Chunk("D/C Date", fonts[1]));
                active.AddCell(lsTitle);
                active.AddCell(dateTitle);
                active.AddCell(medTitle);
                active.AddCell(freqTitle);
                active.AddCell(routeTitle);
                active.AddCell(typeTitle);
                active.AddCell(clasTitle);
                active.HeaderRows = 1;
                discont.AddCell(lsTitle);
                discont.AddCell(dateTitle);
                discont.AddCell(medTitle);
                discont.AddCell(freqTitle);
                discont.AddCell(routeTitle);
                discont.AddCell(typeTitle);
                discont.AddCell(clasTitle);
                discont.AddCell(dcTitle);
                discont.HeaderRows = 1;
                foreach (var medication in medications)
                {
                    if (medication.MedicationCategory == "Active")
                    {
                        AxxessCell ls = new AxxessCell(new float[] { 0, 3, 8, 3 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell date = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell med = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell route = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell freq = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell type = new AxxessCell(new float[] { 0, 4, 8, 4 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell clas = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, 0, .5F, 0 });
                        ls.AddElement(new AxxessCheckbox("", medication.IsLongStanding));
                        date.AddElement(new Chunk(medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty, fonts[0]));
                        med.AddElement(new Chunk(medication.MedicationDosage, fonts[0]));
                        freq.AddElement(new Chunk(medication.Frequency, fonts[0]));
                        route.AddElement(new Chunk(medication.Route, fonts[0]));
                        type.AddElement(new Chunk(medication.MedicationType.Value, fonts[0]));
                        clas.AddElement(new Chunk(medication.Classification, fonts[0]));
                        active.AddCell(ls);
                        active.AddCell(date);
                        active.AddCell(med);
                        active.AddCell(freq);
                        active.AddCell(route);
                        active.AddCell(type);
                        active.AddCell(clas);
                    }
                    if (medication.MedicationCategory == "DC")
                    {
                        AxxessCell ls = new AxxessCell(new float[] { 0, 3, 8, 3 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell date = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell med = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell freq = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell route = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell type = new AxxessCell(new float[] { 0, 4, 8, 4 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell clas = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        AxxessCell dcdate = new AxxessCell(new float[] { 0, 2, 8, 2 }, new float[] { 0, .5F, .5F, 0 });
                        ls.AddElement(new AxxessCheckbox("", medication.IsLongStanding));
                        date.AddElement(new Chunk(medication.StartDate.IsValid() ? medication.StartDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty, fonts[0]));
                        med.AddElement(new Chunk(medication.MedicationDosage, fonts[0]));
                        route.AddElement(new Chunk(medication.Route, fonts[0]));
                        freq.AddElement(new Chunk(medication.Frequency, fonts[0]));
                        type.AddElement(new Chunk(medication.MedicationType.Value, fonts[0]));
                        clas.AddElement(new Chunk(medication.Classification, fonts[0]));
                        dcdate.AddElement(new Chunk(medication.DCDate.IsValid() ? medication.DCDate.ToShortDateString().ToZeroFilled().Clean() : string.Empty, fonts[0]));
                        discont.AddCell(ls);
                        discont.AddCell(date);
                        discont.AddCell(med);
                        discont.AddCell(freq);
                        discont.AddCell(route);
                        discont.AddCell(type);
                        discont.AddCell(clas);
                        discont.AddCell(dcdate);
                    }
                }
                if (active.Rows.Count > 1)
                {
                    AxxessCell title = new AxxessCell(new float[] { 0, 0, 8, 0 }, new float[] { 0, 0, .5F, 0 }), chart = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 });
                    title.AddElement(new AxxessTitle("Active Medications", fonts[1]));
                    chart.AddElement(active);
                    content[0].AddCell(title);
                    content[0].AddCell(chart);
                }
                if (discont.Rows.Count > 1)
                {
                    AxxessCell title = new AxxessCell(new float[] { 0, 0, 8, 0 }, new float[] { 0, 0, .5F, 0 }), chart = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 });
                    title.AddElement(new AxxessTitle("Discontinued Medications", fonts[1]));
                    chart.AddElement(discont);
                    content[0].AddCell(title);
                    content[0].AddCell(chart);
                }
            }
            else
            {
                AxxessCell title = new AxxessCell();
                title.AddElement(new AxxessTitle("No Medications Found", fonts[1]));
                content[0].AddCell(title);
            }
            return content;
        }
    }
}