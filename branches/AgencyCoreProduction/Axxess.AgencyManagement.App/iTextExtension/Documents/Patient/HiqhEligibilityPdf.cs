﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Linq;
    using System.Xml.XPath;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;

    using iTextSharp.text;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.LookUp.Repositories;

    class HiqhEligibilityPdf : AxxessPdf
    {
        public HiqhEligibilityPdf(string input, Agency agency, Patient patient)
        {
            this.SetType(PdfDocs.MedicareEligibility);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            int TableCount = 7;
            AxxessTable[] content = null;
            if (input.IsNullOrEmpty())
            {
                content = new AxxessTable[TableCount];
                content[0] = new AxxessTable(1);
                content[0].AddCell("No Medicare Eligibility Report found for this patient!");
            }
            else
            {
                int counter = 0;
                content = new AxxessTable[TableCount];
                float[] padding = new float[] { 0, 0, 5, 0 }, borders = new float[] { .5F, 0, 0, 0 };

                using (var stringReader = new StringReader(input))
                {
                    XPathDocument xPathDocument = new XPathDocument(stringReader);
                    XPathNavigator xPathNavigator = xPathDocument.CreateNavigator();

                    content[counter] = new AxxessTable(1);
                    AxxessCell correctedHeadercell = new AxxessCell(padding, borders);
                    correctedHeadercell.AddElement(new AxxessTitle("Corrected Beneficiary Information", fonts[1]));
                    content[counter].AddCell(correctedHeadercell);
                    content[counter].HeaderRows = 1;

                    var correctedBeneficiaryNodes = xPathNavigator.Select("hiqhResponse/correctedBeneficiary");
                    foreach (XPathNavigator node in correctedBeneficiaryNodes)
                    {
                        AxxessCell correctedContentcell = new AxxessCell(padding, borders);
                        correctedContentcell.AddElement(new AxxessContentSection("HIC Number:", fonts[1], node.SelectSingleNode("hic") != null ? node.SelectSingleNode("hic").Value : string.Empty, fonts[0], 12));
                        correctedContentcell.AddElement(new AxxessContentSection("Message:", fonts[1], node.SelectSingleNode("lastName") != null ? node.SelectSingleNode("lastName").Value : string.Empty, fonts[0], 12));
                        correctedContentcell.AddElement(new AxxessContentSection("First Initial:", fonts[1], node.SelectSingleNode("firstInitial") != null ? node.SelectSingleNode("firstInitial").Value : string.Empty, fonts[0], 12));
                        correctedContentcell.AddElement(new AxxessContentSection("Date of Birth:", fonts[1], node.SelectSingleNode("dateOfBirth") != null ? node.SelectSingleNode("dateOfBirth").Value : string.Empty, fonts[0], 12));
                        correctedContentcell.AddElement(new AxxessContentSection("Sex:", fonts[1], node.SelectSingleNode("sex") != null ? node.SelectSingleNode("sex").Value : string.Empty, fonts[0], 12));
                        content[counter].AddCell(correctedContentcell);
                    }
                    counter++;

                    content[counter] = new AxxessTable(1);
                    AxxessCell beneficiaryHeadercell = new AxxessCell(padding, borders);
                    beneficiaryHeadercell.AddElement(new AxxessTitle("Beneficiary Information, Entitlement Dates, and Part B Information", fonts[1]));
                    content[counter].AddCell(beneficiaryHeadercell);
                    content[counter].HeaderRows = 1;

                    AxxessCell beneficiaryContentcell = new AxxessCell(padding, borders);
                    var beneficiaryInformationNodes = xPathNavigator.Select("hiqhResponse/beneficiaryInformation");
                    foreach (XPathNavigator node in beneficiaryInformationNodes)
                    {
                        beneficiaryContentcell.AddElement(new AxxessContentSection("Provider Id:", fonts[1], node.SelectSingleNode("providerId") != null ? node.SelectSingleNode("providerId").Value : string.Empty, fonts[0], 12));
                        beneficiaryContentcell.AddElement(new AxxessContentSection("Message:", fonts[1], node.SelectSingleNode("message") != null ? node.SelectSingleNode("message").Value : string.Empty, fonts[0], 12));
                        beneficiaryContentcell.AddElement(new AxxessContentSection("Full Name:", fonts[1], node.SelectSingleNode("fullName") != null ? node.SelectSingleNode("fullName").Value : string.Empty, fonts[0], 12));
                    }
                    var currentEntitlementNodes = xPathNavigator.Select("hiqhResponse/beneficiaryInformation/currentEntitlement");
                    foreach (XPathNavigator node in currentEntitlementNodes)
                    {
                        beneficiaryContentcell.AddElement(new AxxessContentSection("Part A Entitlement - Effective date:", fonts[1], node.SelectSingleNode("partAEffectiveDate") != null ? node.SelectSingleNode("partAEffectiveDate").Value + " - Current" : string.Empty, fonts[0], 12));
                        beneficiaryContentcell.AddElement(new AxxessContentSection("Part B Entitlement - Effective date:", fonts[1], node.SelectSingleNode("partBEffectiveDate") != null ? node.SelectSingleNode("partBEffectiveDate").Value + " - Current" : string.Empty, fonts[0], 12));
                    }
                    
                    content[counter].AddCell(beneficiaryContentcell);
                    counter++;

                    content[counter] = new AxxessTable(1);
                    AxxessCell hmoHeadercell = new AxxessCell(padding, borders);
                    hmoHeadercell.AddElement(new AxxessTitle("HMO Plan Information", fonts[1]));
                    content[counter].AddCell(hmoHeadercell);
                    content[counter].HeaderRows = 1;

                    var planInformationNodes = xPathNavigator.Select("hiqhResponse/planInformation/hmoPlans/hmoPlan");
                    foreach (XPathNavigator node in planInformationNodes)
                    {
                        AxxessCell hmoContentcell = new AxxessCell(padding, borders);
                        hmoContentcell.AddElement(new AxxessContentSection("Type:", fonts[1], node.SelectSingleNode("type") != null ? node.SelectSingleNode("type").Value : string.Empty, fonts[0], 12));
                        hmoContentcell.AddElement(new AxxessContentSection("Plan Identification Code:", fonts[1], node.SelectSingleNode("idCode") != null ? node.SelectSingleNode("idCode").Value : string.Empty, fonts[0], 12));
                        hmoContentcell.AddElement(new AxxessContentSection("Option Code:", fonts[1], node.SelectSingleNode("optionCode") != null ? node.SelectSingleNode("optionCode").Value : string.Empty, fonts[0], 12));
                        hmoContentcell.AddElement(new AxxessContentSection("Effective Date:", fonts[1], node.SelectSingleNode("effectiveDate") != null ? node.SelectSingleNode("effectiveDate").Value : string.Empty, fonts[0], 12));
                        hmoContentcell.AddElement(new AxxessContentSection("Termination Date:", fonts[1], node.SelectSingleNode("terminationDate") != null ? node.SelectSingleNode("terminationDate").Value : string.Empty, fonts[0], 12));
                        content[counter].AddCell(hmoContentcell);
                    }
                    
                    counter++;

                    content[counter] = new AxxessTable(1);
                    AxxessCell hospiceHeadercell = new AxxessCell(padding, borders);
                    hospiceHeadercell.AddElement(new AxxessTitle("Hospice Benefit Periods", fonts[1]));
                    content[counter].AddCell(hospiceHeadercell);
                    content[counter].HeaderRows = 1;

                    var hospiceInformationNodes = xPathNavigator.Select("hiqhResponse/hospiceInformation/hospiceBenefitPeriods/hospiceBenefitPeriod");
                    foreach (XPathNavigator node in hospiceInformationNodes)
                    {
                        AxxessCell hospiceContentcell = new AxxessCell(padding, borders);
                        hospiceContentcell.AddElement(new AxxessContentSection("Period:", fonts[1], node.SelectSingleNode("period") != null ? node.SelectSingleNode("period").Value : string.Empty, fonts[0], 12));
                        hospiceContentcell.AddElement(new AxxessContentSection("First Provider Start Date:", fonts[1], node.SelectSingleNode("firstProviderStartDate") != null ? node.SelectSingleNode("firstProviderStartDate").Value : string.Empty, fonts[0], 12));
                        hospiceContentcell.AddElement(new AxxessContentSection("First Provider Id:", fonts[1], node.SelectSingleNode("firstProviderId") != null ? node.SelectSingleNode("firstProviderId").Value : string.Empty, fonts[0], 12));
                        hospiceContentcell.AddElement(new AxxessContentSection("First Intermediary Id:", fonts[1], node.SelectSingleNode("firstIntermediaryId") != null ? node.SelectSingleNode("firstIntermediaryId").Value : string.Empty, fonts[0], 12));
                        hospiceContentcell.AddElement(new AxxessContentSection("First Owner Change Start Date:", fonts[1], node.SelectSingleNode("firstOwnerChangeStartDate") != null ? node.SelectSingleNode("firstOwnerChangeStartDate").Value : string.Empty, fonts[0], 12));
                        content[counter].AddCell(hospiceContentcell);
                    }
                    
                    counter++;

                    content[counter] = new AxxessTable(1);
                    AxxessCell hhBenefitHeadercell = new AxxessCell(padding, borders);
                    hhBenefitHeadercell.AddElement(new AxxessTitle("Home Health PPS Benefit Periods", fonts[1]));
                    content[counter].AddCell(hhBenefitHeadercell);
                    content[counter].HeaderRows = 1;

                    var homeHealthBenefitNodes = xPathNavigator.Select("hiqhResponse/homeHealthBenefitPeriods/homeHealthBenefitPeriods/homeHealthBenefitPeriod");
                    foreach (XPathNavigator node in homeHealthBenefitNodes)
                    {
                        AxxessCell hhBenefitContentcell = new AxxessCell(padding, borders);
                        hhBenefitContentcell.AddElement(new AxxessContentSection("Spell Number:", fonts[1], node.SelectSingleNode("spellNumber") != null ? node.SelectSingleNode("spellNumber").Value : string.Empty, fonts[0], 12));
                        hhBenefitContentcell.AddElement(new AxxessContentSection("Qualifying Indicator:", fonts[1], node.SelectSingleNode("qualifyingIndicator") != null ? node.SelectSingleNode("qualifyingIndicator").Value : string.Empty, fonts[0], 12));
                        hhBenefitContentcell.AddElement(new AxxessContentSection("PartA Visits Remaining:", fonts[1], node.SelectSingleNode("partAVisitsRemaining") != null ? node.SelectSingleNode("partAVisitsRemaining").Value : string.Empty, fonts[0], 12));
                        hhBenefitContentcell.AddElement(new AxxessContentSection("Earliest Billing Date:", fonts[1], node.SelectSingleNode("earliestBillingDate") != null ? node.SelectSingleNode("earliestBillingDate").Value : string.Empty, fonts[0], 12));
                        hhBenefitContentcell.AddElement(new AxxessContentSection("Latest Billing Date:", fonts[1], node.SelectSingleNode("latestBillingDate") != null ? node.SelectSingleNode("latestBillingDate").Value : string.Empty, fonts[0], 12));
                        hhBenefitContentcell.AddElement(new AxxessContentSection("PartB Visits Applied:", fonts[1], node.SelectSingleNode("partBVisitsApplied") != null ? node.SelectSingleNode("partBVisitsApplied").Value : string.Empty, fonts[0], 12));
                        content[counter].AddCell(hhBenefitContentcell);
                    }
                    
                    counter++;

                    content[counter] = new AxxessTable(1);
                    AxxessCell hhEpisodeHeadercell = new AxxessCell(padding, borders);
                    hhEpisodeHeadercell.AddElement(new AxxessTitle("Home Health PPS Episodes", fonts[1]));
                    content[counter].AddCell(hhEpisodeHeadercell);
                    content[counter].HeaderRows = 1;

                    var homeHealthEpisodeNodes = xPathNavigator.Select("hiqhResponse/homeHealthEpisodes/homeHealthEpisodes/homeHealthEpisode");
                    foreach (XPathNavigator node in homeHealthEpisodeNodes)
                    {
                        AxxessCell hhEpisodeContentcell = new AxxessCell(padding, borders);
                        hhEpisodeContentcell.AddElement(new AxxessContentSection("Start Date:", fonts[1], node.SelectSingleNode("startDate") != null ? node.SelectSingleNode("startDate").Value : string.Empty, fonts[0], 12));
                        hhEpisodeContentcell.AddElement(new AxxessContentSection("End Date:", fonts[1], node.SelectSingleNode("endDate") != null ? node.SelectSingleNode("endDate").Value : string.Empty, fonts[0], 12));
                        hhEpisodeContentcell.AddElement(new AxxessContentSection("Intermediary Number:", fonts[1], node.SelectSingleNode("intermediaryNumber") != null ? node.SelectSingleNode("intermediaryNumber").Value : string.Empty, fonts[0], 12));

                        if (node.SelectSingleNode("providerNumber") != null)
                        {
                            var medicareProviderNumber = node.SelectSingleNode("providerNumber").Value;
                            if (medicareProviderNumber.IsNotNullOrEmpty())
                            {
                                hhEpisodeContentcell.AddElement(new AxxessContentSection("Provider Number:", fonts[1], medicareProviderNumber, fonts[0], 12));
                                var medicareProvider = Container.Resolve<ILookUpDataProvider>().LookUpRepository.GetMedicareProvider(medicareProviderNumber);
                                if (medicareProvider != null)
                                {
                                    hhEpisodeContentcell.AddElement(new AxxessContentSection("Provider Name:", fonts[1], medicareProvider.Name, fonts[0], 12));
                                    hhEpisodeContentcell.AddElement(new AxxessContentSection("Provider Address:", fonts[1], medicareProvider.Address, fonts[0], 12));
                                    hhEpisodeContentcell.AddElement(new AxxessContentSection("Provider City/State/Zip:", fonts[1], string.Format("{0}, {1} {2}", medicareProvider.City, medicareProvider.StateCode, medicareProvider.ZipCode), fonts[0], 12));
                                    hhEpisodeContentcell.AddElement(new AxxessContentSection("Provider Phone:", fonts[1], medicareProvider.Phone.ToPhone(), fonts[0], 12));
                                }
                            }
                        }

                        hhEpisodeContentcell.AddElement(new AxxessContentSection("Earliest Billing Date:", fonts[1], node.SelectSingleNode("earliestBillingDate") != null ? node.SelectSingleNode("earliestBillingDate").Value : string.Empty, fonts[0], 12));
                        hhEpisodeContentcell.AddElement(new AxxessContentSection("Latest Billing Date:", fonts[1], node.SelectSingleNode("latestBillingDate") != null ? node.SelectSingleNode("latestBillingDate").Value : string.Empty, fonts[0], 12));
                        hhEpisodeContentcell.AddElement(new AxxessContentSection("Patient Status Code:", fonts[1], node.SelectSingleNode("patientStatusCode") != null ? node.SelectSingleNode("patientStatusCode").Value : string.Empty, fonts[0], 12));
                        hhEpisodeContentcell.AddElement(new AxxessContentSection("Valid Cancel Indicator:", fonts[1], node.SelectSingleNode("validCancelIndicator") != null ? node.SelectSingleNode("validCancelIndicator").Value : string.Empty, fonts[0], 12));
                        content[counter].AddCell(hhEpisodeContentcell);
                    }
                    
                    counter++;

                    content[counter] = new AxxessTable(1);
                    AxxessCell medicarePayerHeadercell = new AxxessCell(padding, borders);
                    medicarePayerHeadercell.AddElement(new AxxessTitle("Medicare Secondary Payer (MSP) Information", fonts[1]));
                    content[counter].AddCell(medicarePayerHeadercell);
                    content[counter].HeaderRows = 1;

                    var medicareSecondaryPayerNodes = xPathNavigator.Select("hiqhResponse/medicareSecondaryPayers/medicareSecondaryPayers/medicareSecondaryPayer");
                    foreach (XPathNavigator node in medicareSecondaryPayerNodes)
                    {
                        AxxessCell medicarePayerContentcell = new AxxessCell(padding, borders);
                        medicarePayerContentcell.AddElement(new AxxessContentSection("Record:", fonts[1], node.SelectSingleNode("record") != null ? node.SelectSingleNode("record").Value : string.Empty, fonts[0], 12));
                        medicarePayerContentcell.AddElement(new AxxessContentSection("MSP Code:", fonts[1], node.SelectSingleNode("mspCode") != null ? node.SelectSingleNode("mspCode").Value : string.Empty, fonts[0], 12));
                        medicarePayerContentcell.AddElement(new AxxessContentSection("MSP Code Description:", fonts[1], node.SelectSingleNode("mspCodeDescription") != null ? node.SelectSingleNode("mspCodeDescription").Value : string.Empty, fonts[0], 12));
                        medicarePayerContentcell.AddElement(new AxxessContentSection("Effective Date:", fonts[1], node.SelectSingleNode("effectiveDate") != null ? node.SelectSingleNode("effectiveDate").Value : string.Empty, fonts[0], 12));
                        medicarePayerContentcell.AddElement(new AxxessContentSection("Termination Date:", fonts[1], node.SelectSingleNode("terminationDate") != null ? node.SelectSingleNode("terminationDate").Value : string.Empty, fonts[0], 12));
                        medicarePayerContentcell.AddElement(new AxxessContentSection("Intermediary Number:", fonts[1], node.SelectSingleNode("intermediaryNumber") != null ? node.SelectSingleNode("intermediaryNumber").Value : string.Empty, fonts[0], 12));
                        medicarePayerContentcell.AddElement(new AxxessContentSection("Accretion Date:", fonts[1], node.SelectSingleNode("accretionDate") != null ? node.SelectSingleNode("accretionDate").Value : string.Empty, fonts[0], 12));
                        content[counter].AddCell(medicarePayerContentcell);
                    }
                    
                    counter++;
                }
            }
            this.SetContent(content);
            this.SetMargins(new float[] { 150, 28.3F, 90.5F, 28.3F });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = agency.GetBranch(patient != null ? patient.AgencyLocationId : Guid.Empty);
            if (location == null) location = agency.GetMainOffice();
            fieldmap[0].Add("agency", (
                input.IsNotNullOrEmpty() && agency != null ?
                    (agency.Name.IsNotNullOrEmpty() ? agency.Name + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? ", " + location.AddressLine2.Clean() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));

            fieldmap[0].Add("mcare", patient != null && patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber : "");
            fieldmap[0].Add("dob", patient != null && patient.DOB.IsValid() ? patient.DOB.ToZeroFilled() : "");
            fieldmap[0].Add("gender", patient != null && patient.Gender.IsNotNullOrEmpty() ? patient.Gender : "");
            fieldmap[1].Add("patientname", patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "");

            this.SetFields(fieldmap);
        }
    }
}