﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.App.Helpers;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;

    class DrugDrugInteractionsPdf : AxxessPdf
    {
        public DrugDrugInteractionsPdf(PrintViewData<DrugDrugInteractionsViewData> data)
        {
            this.SetType(PdfDocs.DrugDrugInteraction);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 11F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(1) };
            if (data == null || data.Data.DrugDrugInteractions == null || data.Data.DrugDrugInteractions.Count == 0)
            {
                AxxessCell contentcell = new AxxessCell(padding, borders);
                contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], "No interactions found during this screening.", fonts[0], 12));
                content[0].AddCell(contentcell);
            }
            else
            {
                data.Data.DrugDrugInteractions.ForEach(d =>
                {
                    AxxessCell contentcell = new AxxessCell(padding, borders);
                    contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], d.InteractionDescription, fonts[0], 12));
                    contentcell.AddElement(new AxxessContentSection("SeverityDescription", fonts[1], d.SeverityDescription, fonts[0], 12));
                    if (d.ConsumerText.Length > 0)
                    {
                        d.ConsumerText.ForEach(ct => contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], ct, fonts[0], 10)));
                    }
                    content[0].AddCell(contentcell);
                });
            }

            this.SetContent(content);
            float[] margins = new float[] { 118, 28.3F, 28.3F, 28.3F };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("auditvendor", "Drug Screening provided by LexiComp Lexi-Data");
            fieldmap[0].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[0].Add("mrn", data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}