﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.App.Helpers;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.App.ViewData;
    class PhysFaceToFacePdf : AxxessPdf
    {
        private PhysFaceToFaceXml xml;
        public PhysFaceToFacePdf(PrintViewData<FaceToFaceEncounter> data)
        {
            this.xml = new PhysFaceToFaceXml(data);
            this.SetType(PdfDocs.PhysFaceToFace);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            this.SetContent(new IElement[1] { new AxxessContentSection(this.xml.GetLayout()[0], fonts, true, 10, this.IsOasis) });
            float[] margins = new float[] { 205, 35, 70, 35 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
            fieldmap[0].Add("patient", PrintHelper.PatientAddress(data.PatientProfile));
            fieldmap[0].Add("physician", PrintHelper.PhysicianAddressWithNPI(data.Data.Physician));
            fieldmap[0].Add("mr", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
            fieldmap[0].Add("order", data != null && data.Data.OrderNumber > 0 ? data.Data.OrderNumber.ToString() : string.Empty);
            fieldmap[0].Add("dob", data != null && data.PatientProfile != null && data.PatientProfile.DOB.IsValid() ? data.PatientProfile.DOB.ToZeroFilled() : string.Empty);
            fieldmap[0].Add("socdate", data != null && data.PatientProfile != null && data.PatientProfile.StartofCareDate.IsValid() ? data.PatientProfile.StartofCareDate.ToZeroFilled() : string.Empty);
            fieldmap[0].Add("episode", data != null ? (data.Data.EpisodeStartDate.IsNotNullOrEmpty() && data.Data.EpisodeStartDate.IsValidDate() ? (data.Data.EpisodeEndDate.IsNotNullOrEmpty() && data.Data.EpisodeEndDate.IsValidDate() ? string.Format("{0}-{1}", data.Data.EpisodeStartDate, data.Data.EpisodeEndDate) : string.Empty) : string.Empty) : string.Empty);
            fieldmap[0].Add("physsign", data != null && data.Data.SignatureText.IsNotNullOrEmpty() ? data.Data.SignatureText : string.Empty);
            fieldmap[0].Add("physsigndate", data != null && data.Data.SignatureDate.IsValid() ? data.Data.SignatureDate.ToShortDateString() : string.Empty);
            fieldmap[1].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[1].Add("physicianname", data != null && data.Data.Physician != null && data.Data.Physician.DisplayName.IsNotNullOrEmpty() ? data.Data.Physician.DisplayName : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}