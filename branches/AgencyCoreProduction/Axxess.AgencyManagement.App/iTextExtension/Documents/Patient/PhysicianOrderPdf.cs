﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.App.Helpers;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class PhysicianOrderPdf : AxxessPdf {
        public PhysicianOrderPdf(PrintViewData<PhysicianOrder> data) {
            this.SetType(PdfDocs.PhysicianOrder);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            float[] margins = new float[] { 225, 35, 125, 35 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            if (data != null)
            {
                Paragraph[] content = new Paragraph[] { new Paragraph(data.Data.Text.IsNotNullOrEmpty() ? data.Data.Text : " ", fonts[0]) };
                this.SetContent(content);
                fieldmap[0].Add("agency", PrintHelper.AgencyAddress(data.LocationProfile));
                fieldmap[0].Add("patient", PrintHelper.PatientAddress(data.PatientProfile));
                fieldmap[0].Add("physician", PrintHelper.PhysicianAddressWithNPI(data.Data.Physician));
                fieldmap[0].Add("orderdate", data.Data.OrderDate.IsValid() ? data.Data.OrderDate.ToShortDateString() : "");
                fieldmap[0].Add("ordernum", data.Data.OrderNumber.ToString().IsNotNullOrEmpty() && data.Data.OrderNumber.ToString().ToInteger() != 0 ? data.Data.OrderNumber.ToString() : "");
                fieldmap[0].Add("episode", (data.Data.EpisodeStartDate.IsValidDate() ? data.Data.EpisodeStartDate + "—" : string.Empty) + (data.Data.EpisodeEndDate.IsValidDate() ? data.Data.EpisodeEndDate : string.Empty));
                fieldmap[0].Add("dob", data.PatientProfile != null && data.PatientProfile.DOB.IsValid() ? data.PatientProfile.DOB.ToShortDateString() : string.Empty);
                fieldmap[0].Add("mr", data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);
                fieldmap[0].Add("allergies", data.Data.Allergies.IsNotNullOrEmpty() ? data.Data.Allergies : "");
                fieldmap[0].Add("summary", data.Data.Summary.IsNotNullOrEmpty() ? data.Data.Summary : "");
                fieldmap[0].Add("IsOrderReadAndVerified", data.Data.IsOrderReadAndVerified ? "X" : "");
                fieldmap[0].Add("clinsign", data.Data.SignatureText.IsNotNullOrEmpty() ? data.Data.SignatureText : "");
                fieldmap[0].Add("clinsigndate", data.Data.SignatureDate.IsValid() ? data.Data.SignatureDate.ToShortDateString() : "");
                fieldmap[0].Add("physsign", data.Data.PhysicianSignatureText.IsNotNullOrEmpty() ? data.Data.PhysicianSignatureText : "");
                fieldmap[0].Add("physsigndate", data.Data.PhysicianSignatureDate.IsValid() ? data.Data.PhysicianSignatureDate.ToShortDateString() : "");
                fieldmap[1].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
                fieldmap[1].Add("physicianname", data.Data.Physician != null && data.Data.Physician.DisplayName.IsNotNullOrEmpty() ? data.Data.Physician.DisplayName : string.Empty);
                this.SetFields(fieldmap);
            }
        }
    }
}