﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.App.Helpers;
    class IncidentReportPdf : AxxessPdf {
        private IncidentReportXml xml;
        public IncidentReportPdf(PrintViewData<Incident> data)
        {
            this.xml = new IncidentReportXml(data.Data ?? new Incident());
            this.SetType(PdfDocs.IncidentReport);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans); 
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            this.SetContent(content);
            float[] margins = new float[] { 195, 28.8F, 70, 28.8F };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            //var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            var location = data.LocationProfile ?? new LocationPrintProfile();
            //fieldmap[0].Add("agency", (
            //    data != null && data.Agency != null ?
            //        (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name + "\n" : String.Empty) +
            //        (location != null ?
            //            (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean() : String.Empty) +
            //            (location.AddressLine2.IsNotNullOrEmpty() ? ", " + location.AddressLine2.Clean() + "\n" : "\n") +
            //            (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean() + ", " : String.Empty) +
            //            (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
            //            (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
            //            (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
            //            (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
            //        : String.Empty)
            //    : String.Empty));
            var entityData = data.Data ?? new Incident();
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(location));
            fieldmap[0].Add("incidenttype", entityData.IncidentType);
            fieldmap[0].Add("incidentdate", entityData.IncidentDate.IsValid() ? entityData.IncidentDate.ToShortDateString() : String.Empty);
            fieldmap[0].Add("episode", (entityData.EpisodeStartDate.IsValidDate() ? entityData.EpisodeStartDate + "—" : String.Empty) + (data != null && entityData.EpisodeEndDate.IsValidDate() ? entityData.EpisodeEndDate : String.Empty));
            fieldmap[0].Add("physician", entityData.PhysicianName);
            fieldmap[0].Add("mdnotice", entityData.MDNotified);
            fieldmap[0].Add("famcgnotice", entityData.FamilyNotified);
            fieldmap[0].Add("neworders", entityData.NewOrdersCreated);
            fieldmap[0].Add("sign", entityData.SignatureText);
            fieldmap[0].Add("signdate", entityData.SignatureDate.IsValid() ? entityData.SignatureDate.ToShortDateString() : String.Empty);
            fieldmap[1].Add("patientname", PrintHelper.PatientName(data.PatientProfile));
            fieldmap[1].Add("mrn", data != null && data.PatientProfile != null && data.PatientProfile.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientProfile.PatientIdNumber : string.Empty);

            //fieldmap[1].Add("patientname", data != null && data.Patient != null && data.Patient.DisplayName.IsNotNullOrEmpty() ? data.Patient.DisplayName : string.Empty);
            //fieldmap[1].Add("mrn", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            
            this.SetFields(fieldmap);
        }
    }
}