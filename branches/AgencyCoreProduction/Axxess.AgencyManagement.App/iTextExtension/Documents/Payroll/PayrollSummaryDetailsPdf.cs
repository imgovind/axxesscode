﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class PayrollSummaryDetailsPdf : AxxessPdf {
        private List<PayrollDetail> data;
        private string agencyName;
        private DateTime startDate;
        private DateTime endDate;
        public PayrollSummaryDetailsPdf(PayrollDetail data, string agencyName, DateTime startDate, DateTime endDate)
        {
            this.SetPageSize(AxxessDoc.Landscape);
            this.data = new List<PayrollDetail>();
            this.data.Add(data);
            this.agencyName = agencyName;
            this.startDate = startDate;
            this.endDate = endDate;
            this.BuildLayout();
        }
        public PayrollSummaryDetailsPdf(List<PayrollDetail> data, string agencyName, DateTime startDate, DateTime endDate)
        {
            this.SetPageSize(AxxessDoc.Landscape);
            this.data = data;
            this.agencyName = agencyName;
            this.startDate = startDate;
            this.endDate = endDate;
            this.BuildLayout();
        }
        private void BuildLayout() {
            this.SetType(PdfDocs.PayrollDetail);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 8F;
            fonts[2].Size = 10F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[this.data.Count+1];
            int counter = 0;
            double grandTotalVisitPayment = 0;
            double grandTotalMileagePayment = 0;
            double grandTotalSurcharge = 0;
            double grandTotalPayment = 0;
            var grandCategoties = new Dictionary<string, TaskSub>();
            var grandSingleCategory = new TaskSub();
            var summaryList = new List<TaskSub>();
            foreach (var table in data)
            {
                AxxessTable usertable = new AxxessTable(new float[] { 2, 2, 2, 1, 1, 1, 1,1,1,1, 1, 1, 1, 1, 1 }, false);
                AxxessCell user = new AxxessCell(padding, borders);
                user.Colspan = 15;
                user.AddElement(new Paragraph("Employee: " + table.Name, fonts[2]));
                usertable.AddCell(user);
                usertable.AddCell("Patient Name", fonts[1], padding, borders);
                usertable.AddCell("Task", fonts[1], padding, borders);
                usertable.AddCell("Visit Date", fonts[1], padding, borders);
                usertable.AddCell("Time In", fonts[1], padding, borders);
                usertable.AddCell("Time Out", fonts[1], padding, borders);
                usertable.AddCell("Visit Hours", fonts[1], padding, borders);
                usertable.AddCell("Travel Time In", fonts[1], padding, borders);
                usertable.AddCell("Travel Time Out", fonts[1], padding, borders);
                usertable.AddCell("Travel Hours", fonts[1], padding, borders);
                usertable.AddCell("Pay Rate", fonts[1], padding, borders);
                usertable.AddCell("Miles", fonts[1], padding, borders);
                usertable.AddCell("Mileage Rate", fonts[1], padding, borders);
                usertable.AddCell("Visit Pay", fonts[1], padding, borders);
                usertable.AddCell("Surcharge", fonts[1], padding, borders);
                usertable.AddCell("Total Pay", fonts[1], padding, borders);
                usertable.HeaderRows = 2;
                var totalTime = 0;
                var totalTravelTime = 0;
                var totalPay = 0.0;
                var totalMilage = 0.0;
                var totalMileagePay = 0.0;
                var totalSurcharge = 0.0;
                var totalVisitPay = 0.0;
                var subSummaryList = new Dictionary<string, TaskSub>();
                table.Visits = table.Visits.OrderBy(e => e.PatientName).ToList();
                foreach (UserVisit row in table.Visits)
                {
                    usertable.AddCell(row.PatientName, fonts[0], padding, borders);
                    usertable.AddCell(row.TaskName, fonts[0], padding, borders);
                    usertable.AddCell(row.VisitDate, fonts[0], padding, borders);
                    usertable.AddCell(row.TimeIn, fonts[0], padding, borders);
                    usertable.AddCell(row.TimeOut, fonts[0], padding, borders);
                    usertable.AddCell(string.Format("{0:#0.00}", (double)row.MinSpent / 60), fonts[0], padding, borders);
                    usertable.AddCell(row.TravelTimeIn, fonts[0], padding, borders);
                    usertable.AddCell(row.TravelTimeOut, fonts[0], padding, borders);
                    usertable.AddCell(string.Format("{0:#0.00}", (double)row.TravelMinSpent / 60), fonts[0], padding, borders);
                    usertable.AddCell(row.VisitRate, fonts[0], padding, borders);
                    usertable.AddCell(row.AssociatedMileage, fonts[0], padding, borders);
                    usertable.AddCell(row.MileageRate, fonts[0], padding, borders);
                    usertable.AddCell(row.VisitPayment, fonts[0], padding, borders);
                    usertable.AddCell(row.Surcharge, fonts[0], padding, borders);
                    usertable.AddCell(row.TotalPayment, fonts[0], padding, borders);
                    if (subSummaryList.ContainsKey(row.TaskName))
                    {
                        TaskSub subSummaryValue = subSummaryList[row.TaskName];
                        if (subSummaryValue != null)
                        {
                            subSummaryValue.TaskVisit += 1;
                            subSummaryValue.TaskPayment += row.VisitPayment.Substring(1).Trim().ToDouble();
                        }
                    }
                    else
                    {
                        subSummaryList.Add(row.TaskName, new TaskSub { TaskName = row.TaskName, TaskPayment = row.VisitPayment.Substring(1).Trim().ToDouble(), TaskVisit = 1 });
                    }
                    summaryList.Add(new TaskSub { TaskName = row.TaskName, TaskVisit = 1, TaskPayment = row.VisitPayment.Substring(1).Trim().ToDouble() });
                    totalTime += row.MinSpent;
                    totalTravelTime += row.TravelMinSpent;
                    totalVisitPay += row.VisitPayment.Substring(1).Trim().ToDouble();
                    totalMilage+=row.AssociatedMileage.IsNotNullOrEmpty()&&row.AssociatedMileage.IsDouble()?row.AssociatedMileage.ToDouble():0;
                    totalMileagePay += row.MileagePayment;
                    totalSurcharge +=row.Surcharge.Trim().StartsWith("$")? row.Surcharge.Substring(1).ToDouble():row.Surcharge.ToDouble();
                    totalPay = (totalVisitPay + totalMileagePay + totalSurcharge);
                }
                grandTotalMileagePayment += totalMileagePay;
                grandTotalVisitPayment += totalVisitPay;
                grandTotalSurcharge += totalSurcharge;
                content[counter++] = usertable;
                foreach (TaskSub c in subSummaryList.Values)
                {
                    AxxessCell item = new AxxessCell(padding, borders);
                    item.Colspan = 15;
                    item.AddElement(new AxxessTitle(string.Format("{0}: {1} visits, pay ${2}.", c.TaskName, c.TaskVisit, c.TaskPayment), fonts[1]));
                    usertable.AddCell(item);
                }
                AxxessCell userFooter = new AxxessCell(padding, borders);
                userFooter.Colspan = 15;
                userFooter.AddElement(new AxxessTitle(string.Format("Total Visits: {0}, Total Visit Time: {1}, Total Visit Payment: {2}, Total Mileage:{3}, Total Mileage Payment:{4}, Total Surcharge:{5}, Grand Total:{6}", table.Visits.Count, string.Format(" {0} min = {1:#0.00} hour(s) ", totalTime, (double)totalTime / 60), string.Format("${0:#0.00}", totalVisitPay), totalMilage, string.Format("${0:#0.00}", totalMileagePay), string.Format("${0:#0.00}", totalSurcharge), string.Format("${0:#0.00}", totalPay)), fonts[1]));
                usertable.AddCell(userFooter);
                AxxessCell userFooterPS = new AxxessCell(padding, borders);
                userFooterPS.Colspan = 15;
                userFooterPS.AddElement(new Paragraph(" \n "));
                usertable.AddCell(userFooterPS);
            }
            summaryList.OrderBy(t => t.TaskName).ForEach(t =>
            {
                if (grandSingleCategory.TaskName == t.TaskName)
                {
                    if (grandCategoties.ContainsKey(t.TaskName))
                    {
                        grandCategoties.Remove(t.TaskName);
                    }
                    grandSingleCategory.TaskVisit++;
                    grandSingleCategory.TaskPayment += t.TaskPayment;
                }
                else
                {
                    grandSingleCategory = new TaskSub();
                    grandSingleCategory.TaskName = t.TaskName;
                    grandSingleCategory.TaskVisit = 1;
                    grandSingleCategory.TaskPayment += t.TaskPayment;
                }
                grandCategoties.Add(t.TaskName, grandSingleCategory);
            });
            AxxessTable summaryRow = new AxxessTable(new float[] {  1 }, false);
            summaryRow.AddCell(" \n ");
            summaryRow.AddCell("Summary");
            foreach (TaskSub c in grandCategoties.Values)
            {
                summaryRow.AddCell(string.Format("{0}: {1} visits, pay ${2}.", c.TaskName, c.TaskVisit, c.TaskPayment));
            }
            grandTotalPayment = grandTotalMileagePayment + grandTotalVisitPayment + grandTotalSurcharge;
            summaryRow.AddCell(string.Format("Total Number of Employee:{0}", data.Count));
            summaryRow.AddCell(string.Format("Total Visit Payment:${0}", grandTotalVisitPayment));
            summaryRow.AddCell(string.Format("Total Mileage Payment:${0}", grandTotalMileagePayment));
            summaryRow.AddCell(string.Format("Total Surcharge:${0}", grandTotalSurcharge));
            summaryRow.AddCell(string.Format("Total Payment:${0}", grandTotalPayment));
            content[counter++] = summaryRow;
            this.SetContent(content);
            float[] margins = new float[] { 57, 28, 28, 28 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", this.agencyName);
            fieldmap[0].Add("date", this.startDate.ToShortDateString() + " - " + this.endDate.ToShortDateString());
            this.SetFields(fieldmap);
        }
    }
}

