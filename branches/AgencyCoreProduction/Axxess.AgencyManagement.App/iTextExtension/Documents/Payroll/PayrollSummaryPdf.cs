﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.App.Helpers;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class PayrollSummaryPdf : AxxessPdf {
        public PayrollSummaryPdf(List<VisitSummary> data, LocationPrintProfile agency, DateTime startDate, DateTime endDate) {
            this.SetType(PdfDocs.PayrollSummary);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(3, false) };
            content[0].AddCell("User", fonts[1], padding, borders);
            content[0].AddCell("Credential", fonts[1], padding, borders);
            content[0].AddCell("Count", fonts[1], padding, borders);
            content[0].HeaderRows = 1;
            foreach (VisitSummary row in data) {
                if (row != null)
                {
                    var username = row.LastName.IsNotNullOrEmpty() ? (row.FirstName.IsNotNullOrEmpty() ? row.LastName + ", " + row.FirstName : row.UserName) : row.UserName;
                    var credentials = row.Credential.IsNotNullOrEmpty() ? row.Credential : "User credential not specified";
                    content[0].AddCell(username, fonts[0], padding, borders);
                    content[0].AddCell(credentials, fonts[0], padding, borders);
                    content[0].AddCell(row.VisitCount.ToString(), fonts[0], padding, borders);
                }
            }
            this.SetContent(content);
            float[] margins = new float[] { 100, 28, 28, 28 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(agency));
            fieldmap[0].Add("startdate", startDate.ToShortDateString());
            fieldmap[0].Add("enddate", endDate.ToShortDateString());
            this.SetFields(fieldmap);
        }
    }
}