﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.App.Enums;
using Axxess.AgencyManagement.Domain;
    abstract class VisitNotePdf : AxxessPdf {
        private VisitNoteXml xml;
       
        protected String DocType;
        protected LocationPrintProfile Location;
        protected PatientProfileLean Patient;
        protected VisitNotePrintViewData NoteData;

        public VisitNotePdf(PrintViewData<VisitNotePrintViewData> printViewData, int rev)
            : base()
        {
            this.Location = printViewData.LocationProfile;
            this.Patient = printViewData.PatientProfile;
            this.NoteData = printViewData.Data ?? new VisitNotePrintViewData();
            var data = printViewData.Data ?? new VisitNotePrintViewData();
            PdfDoc type;
            switch (data.Type)
            {
                case "HHAideCarePlan":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.HHACarePlan2;
                        rev = 0;
                    }
                    else
                    {
                        type = PdfDocs.HHACarePlan;
                    }
                    this.Init(type, rev);
                    break;
                case "MSWAssessment":
                case "MSWDischarge":
                case "MSWEvaluationAssessment":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.MSWEval2;
                        rev = 0;
                    }
                    else
                    {
                        type = PdfDocs.MSWEval;
                    }
                    this.Init( type, rev);
                    break;
                case "MSWProgressNote":
                    type = PdfDocs.MSWProgress;
                    this.Init( type, rev);
                    break;
                case "MSWVisit":
                    type = PdfDocs.MSWVisit;
                    this.Init( type, rev);
                    break;
                case "PTDischarge":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.PTDischarge2;
                        rev = 0;
                    }
                    else
                    {
                        type = PdfDocs.PTDischarge;
                    }
                    this.Init( type, rev);
                    break;
                case "PTDischargeSummary":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.PTDischargeSummary2;
                        rev = 0;
                    }
                    else
                    {
                        type = PdfDocs.PTDischargeSummary;
                    }
                    this.Init( type, rev);
                    break;
                case "PTReEvaluation":
                case "PTMaintenance":
                case "PTEvaluation":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.PTEval2;
                        rev = 0;
                    }
                    else if (data.Version == 3)
                    {
                        type = PdfDocs.PTEval3;
                        rev = 0;
                    }
                    else if (data.Version == 4)
                    {
                        type = PdfDocs.PTEval4;
                        rev = 0;
                    }
                    else if (data.Version == 5)
                    {
                        type = PdfDocs.PTEval5;
                        rev = 0;
                    }
                    else
                        type = PdfDocs.PTEval;
                    this.Init( type, rev);
                    break;
                case "PTReassessment":
                    type = PdfDocs.PTReassessment;
                    this.Init(type, rev);
                    break;
                case "PTAVisit":
                case "PTVisit":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.PTVisit2;
                        rev = 0;
                    }
                    else
                    {
                        type = PdfDocs.PTVisit;
                    }
                    this.Init( type, rev);
                    break;
                case "OTDischarge":
                    if (data.Version == 3)
                    {
                        type = PdfDocs.OTDischarge3;
                        rev = 0;
                    }
                    else if (data.Version == 2)
                    {
                        type = PdfDocs.OTDischarge2;
                        rev = 0;
                    }
                    else
                        type = PdfDocs.OTDischarge;
                    this.Init( type, rev);
                    break;
                case "OTReEvaluation":
                case "OTEvaluation":
                    if (data.Version == 4)
                    {
                        type = PdfDocs.OTEval4;
                        rev = 0;
                    }
                    else if (data.Version == 3)
                    {
                        type = PdfDocs.OTEval3;
                        rev = 0;
                    }
                    else if (data.Version == 2)
                    {
                        type = PdfDocs.OTEval2;
                        rev = 0;
                    }
                    else
                    {
                        type = PdfDocs.OTEval;
                    }
                    this.Init( type, rev);
                    break;
                case "OTMaintenance":
                    type = PdfDocs.OTEval;
                    this.Init( type, rev);
                    break;
                case "OTReassessment":
                    type = PdfDocs.OTReassessment;
                    this.Init( type, rev);
                    break;
                case "OTVisit":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.OTVisit2;
                        rev = 0;
                    }
                    else
                        type = PdfDocs.OTVisit;
                    this.Init( type, rev);
                    break;
                case "COTAVisit":
                    type = PdfDocs.OTVisit;
                    this.Init( type, rev);
                    break;
                case "STEvaluation":
                case "STReEvaluation":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.STEval2;
                        rev = 0;
                    }
                    else if (data.Version == 3)
                    {
                        type = PdfDocs.STEval3;
                        rev = 0;
                    }
                    else
                        type = PdfDocs.STEval;
                    this.Init( type, rev);
                    break;
                case "STMaintenance":
                    type = PdfDocs.STEval;
                    this.Init( type, rev);
                    break;
                case "STDischarge":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.STDischarge;
                    }
                    else
                    {
                        type = PdfDocs.STEval;
                    }
                    this.Init( type, rev);
                    break;
                case "STReassessment":
                    type = PdfDocs.STReassessment;
                    this.Init( type, rev);
                    break;
                case "STVisit":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.STVisit2;
                        rev = 0;
                    }
                    else
                    {
                        type = PdfDocs.STVisit;
                    }

                    this.Init( type, rev);
                    break;
                case "DriverOrTransportationNote":
                    type = PdfDocs.TransportationLog;
                    this.Init( type, rev);
                    break;
                case "NutritionalAssessment":
                    type = PdfDocs.NutritionalAssessment;
                    this.Init( type, rev);
                    break;
                case "PTPlanOfCare":
                    type = PdfDocs.PTPOC;
                    this.Init( type, rev);
                    break;
                case "OTPlanOfCare":
                    if (data.Version == 2)
                    {
                        type = PdfDocs.OTPOC2;
                        rev = 0;
                    }
                    else
                    {
                        type = PdfDocs.OTPOC;
                    }
                    this.Init( type, rev);
                    break;
                case "STPlanOfCare":
                    type = PdfDocs.STPlanOfCare;
                    this.Init(type, rev);
                    break;

            }
        }

        public VisitNotePdf(PrintViewData<VisitNotePrintViewData> printViewData, PdfDoc type, int rev)
            : base()
        {
            this.Location = printViewData.LocationProfile;
            this.Patient = printViewData.PatientProfile;
            this.NoteData = printViewData.Data ?? new VisitNotePrintViewData();
            this.Init(type, rev);
        }

        private void Init(PdfDoc type, int rev)
        {
            var data = this.NoteData;
            if (rev > 0) this.xml = new VisitNoteXml(data, type, rev);
            else this.xml = new VisitNoteXml(data, type);
            this.SetType(type);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            this.SetContent(this.Content(this.xml));
            this.SetMargins(this.Margins());
            var fieldMap = this.FieldMap();
            fieldMap[0].Add("surcharge", data != null && data.Questions != null && data.Questions.ContainsKey("Surcharge") && data.Questions["Surcharge"].Answer.IsNotNullOrEmpty() ? data.Questions["Surcharge"].Answer : "");
            fieldMap[0].Add("mileage", data != null && data.Questions != null && data.Questions.ContainsKey("AssociatedMileage") && data.Questions["AssociatedMileage"].Answer.IsNotNullOrEmpty() ? data.Questions["AssociatedMileage"].Answer : "");
            this.SetFields(fieldMap);
        }
    
        protected virtual IElement[] Content(VisitNoteXml xml) {
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            return content;
        }
        
        protected virtual List<Dictionary<String, String>> FieldMap() {
            return new List<Dictionary<string, string>>();
        }
        protected virtual float[] Margins() {
            return new float[] { 0, 0, 0, 0 };
        }
    }
}