﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.Helpers;
    class PASVisitPdf : VisitNotePdf {
        public PASVisitPdf(PrintViewData<VisitNotePrintViewData> data) : base(data, PdfDocs.PASVisit, 0) { }
        protected override float[] Margins() {
            return new float[] { 160, 28.3F, 60, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(this.Location));
            fieldmap[0].Add("mr", this.Patient.PatientIdNumber);
            var data = this.NoteData;
            fieldmap[0].Add("visitdate", data != null && data.VisitDate != null && data.VisitDate.IsValidDate() ? data.VisitDate : "");
            fieldmap[0].Add("timein", data != null && data.Questions != null && data.Questions.ContainsKey("TimeIn") && data.Questions["TimeIn"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeIn"].Answer : "");
            fieldmap[0].Add("timeout", data != null && data.Questions != null && data.Questions.ContainsKey("TimeOut") && data.Questions["TimeOut"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeOut"].Answer : "");
            fieldmap[0].Add("traveltimein", data != null && data.Questions != null && data.Questions.ContainsKey("TravelTimeIn") && data.Questions["TravelTimeIn"].Answer.IsNotNullOrEmpty() ? data.Questions["TravelTimeIn"].Answer : "");
            fieldmap[0].Add("traveltimeout", data != null && data.Questions != null && data.Questions.ContainsKey("TravelTimeOut") && data.Questions["TravelTimeOut"].Answer.IsNotNullOrEmpty() ? data.Questions["TravelTimeOut"].Answer : "");
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? data.StartDate.ToShortDateString() + "-" + data.EndDate.ToShortDateString() : "");
            fieldmap[0].Add("hhafreq", data != null && data.Questions != null && data.Questions.ContainsKey("HHAFrequency") && data.Questions["HHAFrequency"].Answer.IsNotNullOrEmpty() ? data.Questions["HHAFrequency"].Answer : "");
            fieldmap[0].Add("dnr", data != null && data.Questions != null && data.Questions.ContainsKey("DNR") && data.Questions["DNR"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["DNR"].Answer.Equals("1") ? "Yes" : "") +
                (data.Questions["DNR"].Answer.Equals("0") ? "No" : "") : "");
            fieldmap[0].Add("diagnosis1", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis") && data.Questions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis"].Answer : "");
            fieldmap[0].Add("diagnosis2", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis1") && data.Questions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis1"].Answer : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", PrintHelper.PatientName(this.Patient));
            return fieldmap;
        }
    }
}