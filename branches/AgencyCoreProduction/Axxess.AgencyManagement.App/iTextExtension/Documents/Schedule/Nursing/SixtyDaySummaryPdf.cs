﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.Helpers;
    class SixtyDaySummaryPdf : VisitNotePdf {
        public SixtyDaySummaryPdf(PrintViewData<VisitNotePrintViewData> data) : base(data, PdfDocs.SixtyDaySummary, 0) { }
        protected override float[] Margins() {
 	        return new float[] { 120, 28.3F, 125, 28.3F };
        }
        protected override List<Dictionary<string, string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(this.Location));
            var data = this.NoteData;
            fieldmap[0].Add("subject", data != null && data.TypeName != null ? data.TypeName : "");
            fieldmap[0].Add("mr", this.Patient.PatientIdNumber);
            fieldmap[0].Add("dob", this.Patient.DOB.IsValid() ? this.Patient.DOB.ToString("MM/dd/yyyy") : "");
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName != null ? data.PhysicianDisplayName.ToTitleCase() : "");
            //string notificationDate="";
            //if (data != null &&data.Questions!=null&& data.Questions.ContainsKey("DNR") && data.Questions["DNR"].Answer == "1")
            //{
            //    if (data.Questions.ContainsKey("NotificationDate") && data.Questions["NotificationDate"].Answer.IsNotNullOrEmpty())
            //    {
            //        if (data.Questions["NotificationDate"].Answer == "1")
            //            notificationDate = "5 days";
            //        else if (data.Questions["NotificationDate"].Answer == "2")
            //            notificationDate = "2 days";
            //        else if (data.Questions["NotificationDate"].Answer == "3")
            //        {
            //            if (data.Questions.ContainsKey("NotificationDateOther") && data.Questions["NotificationDateOther"].Answer.IsNotNullOrEmpty())
            //                notificationDate = data.Questions["NotificationDateOther"].Answer;
            //        }
            //    }
            //}
            fieldmap[0].Add("dnr", data != null && data.Questions != null ? (data.Questions.ContainsKey("DNR") && data.Questions["DNR"].Answer == "1" ? "Yes" : "No")  :  "");
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? string.Format(" {0} – {1}", data.StartDate.ToShortDateString(), data.EndDate.ToShortDateString()) : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", PrintHelper.PatientName(this.Patient));
            return fieldmap;
        }
    }
}