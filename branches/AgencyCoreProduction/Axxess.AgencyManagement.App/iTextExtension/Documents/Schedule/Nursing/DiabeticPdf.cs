﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.App.Helpers;
    class DiabeticDailyPdf : VisitNotePdf {
        public DiabeticDailyPdf(PrintViewData<VisitNotePrintViewData> data) : base(data, PdfDocs.DiabeticDaily, 0) { }
        protected override IElement[] Content(VisitNoteXml xml) {
            AxxessTable[] content = new AxxessTable[xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in xml.GetLayout()) {
                content[count] = new AxxessTable(section.Cols > 0 ? section.Cols : 1);
                foreach (XmlPrintSection subsection in section.Subsection) {
                    AxxessCell sectionCell = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessContentSection contentSection = new AxxessContentSection(subsection, this.GetFonts(), true, 7.5F, this.IsOasis);
                    sectionCell.AddElement(contentSection);
                    content[count].AddCell(sectionCell);
                }
                count++;
            }
            return content;
        }
        protected override float[] Margins() {
            return new float[] { 120, 28.3F, 60, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(this.Location));
            fieldmap[0].Add("patientname", PrintHelper.PatientName(this.Patient));
            fieldmap[0].Add("mr", this.Patient.PatientIdNumber);
            var data = this.NoteData;
            fieldmap[0].Add("visitdate", data != null && data.Questions != null && data.Questions.ContainsKey("VisitDate") && data.Questions["VisitDate"].Answer.IsNotNullOrEmpty() ? data.Questions["VisitDate"].Answer : string.Empty);
            fieldmap[0].Add("prn", data != null && data.Questions != null && data.Questions.ContainsKey("ScheduledPrn") && data.Questions["ScheduledPrn"].Answer.IsNotNullOrEmpty() ? data.Questions["ScheduledPrn"].Answer : string.Empty);
            fieldmap[0].Add("timein", data != null && data.Questions != null && data.Questions.ContainsKey("TimeIn") && data.Questions["TimeIn"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeIn"].Answer : string.Empty);
            fieldmap[0].Add("timeout", data != null && data.Questions != null && data.Questions.ContainsKey("TimeOut") && data.Questions["TimeOut"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeOut"].Answer : string.Empty);
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : string.Empty);
            fieldmap[0].Add("signdate", data != null && data.SignatureDate.IsNotNullOrEmpty() && data.SignatureDate.ToDateTime() != DateTime.MinValue ? data.SignatureDate : string.Empty);
            return fieldmap;
        }
    }
}