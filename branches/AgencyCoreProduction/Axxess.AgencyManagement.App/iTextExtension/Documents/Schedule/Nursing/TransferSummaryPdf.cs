﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Enums;
    using Axxess.AgencyManagement.App.Helpers;
    class TransferSummaryPdf : VisitNotePdf {
        public TransferSummaryPdf(PrintViewData<VisitNotePrintViewData> data) : base(data, data.Data != null ? ((data.Data.Type == DisciplineTasks.TransferSummary.ToString() && data.Data.Version > 1) ? (data.Data.Version == 2 ? PdfDocs.TransferSummary2 : PdfDocs.TransferSummary3) : (data.Data.Version == 2 ? PdfDocs.TransferSummary4 : PdfDocs.TransferSummary)) : PdfDocs.TransferSummary, 0) { }
        protected override float[] Margins() {
            var data = this.NoteData;
            if (data.Type == DisciplineTasks.TransferSummary.ToString() && (data.Version == 2 || data.Version == 3))
            {
                return new float[] { 170, 28.3F, 60, 28.3F };
            }
            else
            {
                return new float[] { 120, 28.3F, 60, 28.3F };
            }
        }
        protected override List<Dictionary<string,string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(this.Location));
            var data = this.NoteData;
            fieldmap[0].Add("visitdate", data != null && data.VisitDate != null && data.VisitDate.IsValidDate() ? data.VisitDate : "");
            fieldmap[0].Add("mr", this.Patient.PatientIdNumber);
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? data.StartDate.ToShortDateString() + "-" + data.EndDate.ToShortDateString() : "");
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName != null ? data.PhysicianDisplayName.ToTitleCase() : "");
            fieldmap[0].Add("transferdate", data != null && data.Questions != null && data.Questions.ContainsKey("TransferDate") && data.Questions["TransferDate"].Answer.IsNotNullOrEmpty() ? data.Questions["TransferDate"].Answer : "");
            fieldmap[0].Add("physicianphone", data != null && data.Questions != null && data.Questions.ContainsKey("PhysicianPhone") && data.Questions["PhysicianPhone"].Answer.IsNotNullOrEmpty() ? data.Questions["PhysicianPhone"].Answer : "");
            fieldmap[0].Add("emergencycontact", data != null && data.Questions != null && data.Questions.ContainsKey("EmergencyContact") && data.Questions["EmergencyContact"].Answer.IsNotNullOrEmpty() ? data.Questions["EmergencyContact"].Answer : "");
            fieldmap[0].Add("reportrecipient", data != null && data.Questions != null && data.Questions.ContainsKey("ReceivingName") && data.Questions["ReceivingName"].Answer.IsNotNullOrEmpty() ? data.Questions["ReceivingName"].Answer : "");
            fieldmap[0].Add("receivingdate", data != null && data.Questions != null && data.Questions.ContainsKey("ReceivingDate") && data.Questions["ReceivingDate"].Answer.IsNotNullOrEmpty() ? data.Questions["ReceivingDate"].Answer : "");
            fieldmap[0].Add("diagnosis1", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis") && data.Questions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis"].Answer : "");
            fieldmap[0].Add("diagnosis2", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis1") && data.Questions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis1"].Answer : "");
            fieldmap[0].Add("diagnosis3", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis2") && data.Questions["PrimaryDiagnosis2"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis2"].Answer : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", PrintHelper.PatientName(this.Patient));
            fieldmap[1].Add("doctype", data != null && data.Type.IsNotNullOrEmpty() && data.Type == DisciplineTasks.TransferSummary.ToString() ? "TRANSFER SUMMARY" : "COORDINATION OF CARE");
            return fieldmap;
        }
    }
}