﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.Helpers;
    class LVNSVisitPdf : VisitNotePdf {
        public LVNSVisitPdf(PrintViewData<VisitNotePrintViewData> data) : base(data, PdfDocs.LVNSVisit, 0) { }
        protected override float[] Margins() {
            return new float[] { 120, 28.3F, 60, 28.3F };
        }
        protected override List<Dictionary<string, string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(this.Location));
            var data = this.NoteData;
            fieldmap[0].Add("visitdate", data != null && data.VisitDate != null && data.VisitDate.IsValidDate() ? data.VisitDate : "");
            fieldmap[0].Add("mr", this.Patient.PatientIdNumber);
            fieldmap[0].Add("lvn", data != null && data.Questions != null && data.Questions.ContainsKey("LVN") && data.Questions["LVN"].Answer.IsNotNullOrEmpty() ? UserEngine.GetName(data.Questions["LVN"].Answer.ToGuid(),data.AgencyId) : "");
            fieldmap[0].Add("lvnpresent", data != null && data.Questions != null && data.Questions.ContainsKey("LVNPresent") && data.Questions["LVNPresent"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["LVNPresent"].Answer == "1" ? "Yes" : "") +
                (data.Questions["LVNPresent"].Answer == "0" ? "No" : "")
            : "");
            fieldmap[0].Add("milage", data != null && data.Questions != null && data.Questions.ContainsKey("AssociatedMileage") && data.Questions["AssociatedMileage"].Answer.IsNotNullOrEmpty() ? data.Questions["AssociatedMileage"].Answer : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", PrintHelper.PatientName(this.Patient));
            return fieldmap;
        }
    }
}