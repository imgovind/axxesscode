﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.App.Helpers;
    class WoundCarePdf : VisitNotePdf {
        public WoundCarePdf(PrintViewData<VisitNotePrintViewData> data) : base(data, PdfDocs.WoundCare, 0) { }
        protected override float[] Margins() {
            return new float[] { 83, 28, 25, 28 };
        }
        protected override List<Dictionary<string,string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(this.Location));
            return fieldmap;
        }
    }
}