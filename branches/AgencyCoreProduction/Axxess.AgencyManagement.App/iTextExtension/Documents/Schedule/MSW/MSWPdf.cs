﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.Helpers;
    class MSWPdf : VisitNotePdf {
        public MSWPdf(PrintViewData<VisitNotePrintViewData> data) : base(data, 0) { }
        protected override float[] Margins() {
            var data = this.NoteData;
            return new float[] { 140, 28.3F, data.Type.Contains("Assessment") || data.Type.Contains("MSWDischarge") ? 93 : 60, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(this.Location));
            fieldmap[0].Add("mr", this.Patient.PatientIdNumber);
            var data = this.NoteData;
            fieldmap[0].Add("visitdate", data != null && data.VisitDate != null && data.VisitDate.IsValidDate() ? data.VisitDate : "");
            fieldmap[0].Add("episode", data != null && data.EpisodeRange.IsNotNullOrEmpty() ? data.EpisodeRange : "");
            fieldmap[0].Add("timein", data != null && data.Questions != null && data.Questions.ContainsKey("TimeIn") && data.Questions["TimeIn"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeIn"].Answer : "");
            fieldmap[0].Add("timeout", data != null && data.Questions != null && data.Questions.ContainsKey("TimeOut") && data.Questions["TimeOut"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeOut"].Answer : "");
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName.IsNotNullOrEmpty() ? data.PhysicianDisplayName : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            switch (data.Type) {
                case "MSWAssessment": fieldmap[1].Add("doctype", "MSW ASSESSMENT"); break;
                case "MSWDischarge": fieldmap[1].Add("doctype", "MSW DISCHARGE"); break;
                case "MSWEvaluationAssessment": fieldmap[1].Add("doctype", "MSW EVALUATION"); break;
                case "MSWProgressNote": fieldmap[1].Add("doctype", "MSW PROGRESS NOTE"); break;
                case "MSWVisit": fieldmap[1].Add("doctype", "MSW VISIT"); break;
            }
            fieldmap[0].Add("patientname",PrintHelper.PatientName(this.Patient));
            return fieldmap;
        }
    }
}