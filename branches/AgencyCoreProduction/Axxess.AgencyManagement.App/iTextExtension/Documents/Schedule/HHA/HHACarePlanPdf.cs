﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.Helpers;
    class HHACarePlanPdf : VisitNotePdf {
        public HHACarePlanPdf(PrintViewData<VisitNotePrintViewData> data) : base(data, PdfDocs.HHACarePlan, 0) { }
        public HHACarePlanPdf(PrintViewData<VisitNotePrintViewData> data, int rev) : base(data, rev) { }
        protected override float[] Margins() {
            return new float[] { 160, 28.3F, 60, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(this.Location));
            var data = this.NoteData;
            fieldmap[0].Add("visitdate", data != null && data.Questions != null && data.Questions.ContainsKey("VisitDate")&& data.Questions["VisitDate"].Answer.IsNotNullOrEmpty() ?  data.Questions["VisitDate"].Answer: "");
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? data.StartDate.ToShortDateString() + "-" + data.EndDate.ToShortDateString() : "");
            fieldmap[0].Add("diagnosis1", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis") && data.Questions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis"].Answer : "");
            fieldmap[0].Add("diagnosis2", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis1") && data.Questions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis1"].Answer : "");
            fieldmap[0].Add("allergies", data != null && data.Questions != null && data.Questions.ContainsKey("AllergiesDescription") && data.Questions["AllergiesDescription"].Answer.IsNotNullOrEmpty() ? data.Questions["AllergiesDescription"].Answer : "");
            fieldmap[0].Add("mr", this.Patient.PatientIdNumber);
            fieldmap[0].Add("hhafreq", data != null && data.Questions != null && data.Questions.ContainsKey("HHAFrequency") && data.Questions["HHAFrequency"].Answer.IsNotNullOrEmpty() ? data.Questions["HHAFrequency"].Answer : "");
            fieldmap[0].Add("dnr", data != null && data.Questions != null && data.Questions.ContainsKey("DNR") && data.Questions["DNR"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["DNR"].Answer.Equals("1") ? "Yes" : "") +
                (data.Questions["DNR"].Answer.Equals("0") ? "No" : "") : "");
            fieldmap[0].Add("diet", data != null && data.Questions != null && data.Questions.ContainsKey("Diet") && data.Questions["Diet"].Answer.IsNotNullOrEmpty() ? data.Questions["Diet"].Answer : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", PrintHelper.PatientName(this.Patient));
            return fieldmap;
        }
    }
}