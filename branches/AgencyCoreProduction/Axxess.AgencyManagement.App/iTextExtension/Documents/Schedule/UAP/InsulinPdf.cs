﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.App.Helpers;
    class UAPInsulinPdf : VisitNotePdf {
        public UAPInsulinPdf(PrintViewData<VisitNotePrintViewData> data) : base(data, PdfDocs.UAPInsulin, 0) { }
        protected override float[] Margins() {
            return new float[] { 140, 28.3F, 60, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(this.Location));
            fieldmap[0].Add("patientname", PrintHelper.PatientName(this.Patient));
            fieldmap[0].Add("mr", this.Patient.PatientIdNumber);
            var data = this.NoteData;
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? string.Format(" {0} – {1}", data.StartDate.ToShortDateString(), data.EndDate.ToShortDateString()) : "");
            fieldmap[0].Add("visitdate", data != null && data.Questions != null && data.Questions.ContainsKey("VisitDate") && data.Questions["VisitDate"].Answer.IsNotNullOrEmpty() ? data.Questions["VisitDate"].Answer : string.Empty);
            fieldmap[0].Add("timein", data != null && data.Questions != null && data.Questions.ContainsKey("TimeIn") && data.Questions["TimeIn"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeIn"].Answer : string.Empty);
            fieldmap[0].Add("timeout", data != null && data.Questions != null && data.Questions.ContainsKey("TimeOut") && data.Questions["TimeOut"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeOut"].Answer : string.Empty);
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : string.Empty);
            fieldmap[0].Add("signdate", data != null && data.SignatureDate.IsNotNullOrEmpty() && data.SignatureDate.ToDateTime() != DateTime.MinValue ? data.SignatureDate : string.Empty);
            return fieldmap;
        }
    }
}