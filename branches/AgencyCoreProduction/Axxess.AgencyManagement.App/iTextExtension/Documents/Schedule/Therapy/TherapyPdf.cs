﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.App.Helpers;
    class TherapyPdf : VisitNotePdf {
        public TherapyPdf(PrintViewData<VisitNotePrintViewData> data ) : base(data, 1) { }
        public TherapyPdf(PrintViewData<VisitNotePrintViewData> data , int rev) : base(data, rev) { }
        protected override IElement[] Content(VisitNoteXml xml)
        {
            AxxessTable[] content = new AxxessTable[xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in xml.GetLayout())
            {
                content[count] = new AxxessTable(section.Cols > 0 ? section.Cols : 1);
                foreach (XmlPrintSection subsection in section.Subsection)
                {
                    AxxessCell sectionCell = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessContentSection contentSection = new AxxessContentSection(subsection, this.GetFonts(), true, 9.5F, this.IsOasis);
                    sectionCell.AddElement(contentSection);
                    content[count].AddCell(sectionCell);
                }
                count++;
            }
            return content;
        }
        protected override float[] Margins() {
            var data = this.NoteData;
            return new float[] { data.Type.Contains("PTDischarge") ? 160 : 140, 28.3F, data.Type.Contains("Evaluation") || data.Type.Contains("Discharge") ? 93 : 60, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency",PrintHelper.AgencyAddress(this.Location));
            var data = this.NoteData;
            fieldmap[0].Add("DOB", data != null && data.Questions != null && data.Questions.ContainsKey("PTDischargeDOB") && data.Questions["PTDischargeDOB"].Answer.IsNotNullOrEmpty() ? data.Questions["PTDischargeDOB"].Answer : "");
            fieldmap[0].Add("visitdate", data != null && data.VisitDate != null && data.VisitDate.IsValidDate() ? data.VisitDate : "");
            fieldmap[0].Add("mr",this.Patient.PatientIdNumber);
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName.IsNotNullOrEmpty() ? data.PhysicianDisplayName : "");
            fieldmap[0].Add("episode", data != null && data.EpisodeRange.IsNotNullOrEmpty() ? data.EpisodeRange : "");
            fieldmap[0].Add("timein", data != null && data.Questions != null && data.Questions.ContainsKey("TimeIn") && data.Questions["TimeIn"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeIn"].Answer : "");
            fieldmap[0].Add("timeout", data != null && data.Questions != null && data.Questions.ContainsKey("TimeOut") && data.Questions["TimeOut"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeOut"].Answer : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("type", data != null && data.Questions != null && data.Questions.ContainsKey("ReassessmentType") && data.Questions["ReassessmentType"].Answer.IsNotNullOrEmpty() ? data.Questions["ReassessmentType"].Answer : "");
            fieldmap[0].Add("ordernumber", data != null && data.Questions != null && data.Questions.ContainsKey("OrderNumber") && data.Questions["OrderNumber"].Answer.IsNotNullOrEmpty() ? data.Questions["OrderNumber"].Answer : "");
            switch (data.Type) {
                case "PTDischarge": fieldmap[1].Add("doctype", "PT DISCHARGE"); break;
                case "PTEvaluation": fieldmap[1].Add("doctype", "PT EVALUATION"); break;
                case "PTReEvaluation": fieldmap[1].Add("doctype", "PT RE-EVALUATION"); break;
                case "PTMaintenance": fieldmap[1].Add("doctype", "PT MAINTENANCE VISIT"); break;
                case "PTVisit": fieldmap[1].Add("doctype", "PT VISIT"); break;
                case "PTAVisit": fieldmap[1].Add("doctype", "PTA VISIT"); break;
                case "OTEvaluation": fieldmap[1].Add("doctype", "OT EVALUATION"); break;
                case "OTReEvaluation": fieldmap[1].Add("doctype", "OT RE-EVALUATION"); break;
                case "OTDischarge": fieldmap[1].Add("doctype", "OT DISCHARGE"); break;
                case "OTMaintenance": fieldmap[1].Add("doctype", "OT MAINTENANCE VISIT"); break;
                case "OTVisit": fieldmap[1].Add("doctype", "OT VISIT"); break;
                case "COTAVisit": fieldmap[1].Add("doctype", "COTA VISIT"); break;
                case "STEvaluation": fieldmap[1].Add("doctype", "ST EVALUATION"); break;
                case "STReEvaluation": fieldmap[1].Add("doctype", "ST RE-EVALUATION"); break;
                case "STReassessment": fieldmap[1].Add("doctype", "ST RE-ASSESSMENT"); break;
                case "STMaintenance": fieldmap[1].Add("doctype", "ST MAINTENANCE VISIT"); break;
                case "STDischarge": fieldmap[1].Add("doctype", "ST DISCHARGE"); break;
                case "STVisit": fieldmap[1].Add("doctype", "ST VISIT"); break;
                case "PTPlanOfCare": fieldmap[1].Add("doctype", "PT Plan Of Care"); break;
                case "OTPlanOfCare": fieldmap[1].Add("doctype", "OT Plan Of Care"); break;
                case "STPlanOfCare": fieldmap[1].Add("doctype", "ST Plan Of Care"); break;
            }
            fieldmap[0].Add("patientname", PrintHelper.PatientName(this.Patient));
            fieldmap[0].Add("patientnamePTDischarge",   PrintHelper.PatientName(this.Patient) + (this.Patient.PatientIdNumber.IsNotNullOrEmpty() ? "(" + this.Patient.PatientIdNumber + ")" : ""));
            return fieldmap;
        }
    }
}