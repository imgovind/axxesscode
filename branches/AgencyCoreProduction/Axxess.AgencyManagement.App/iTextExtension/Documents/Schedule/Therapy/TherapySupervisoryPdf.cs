﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.App.Helpers;

    class TherapySupervisoryPdf : VisitNotePdf {
        public TherapySupervisoryPdf(PrintViewData<VisitNotePrintViewData> data) : base(data, PdfDocs.SupervisoryVisit, 0) { }
        protected override float[] Margins() {
            return new float[] { 120, 28.3F, 60, 28.3F };
        }

        protected override List<Dictionary<string, string>> FieldMap() {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", PrintHelper.AgencyAddress(this.Location));
            var data = this.NoteData;
            fieldmap[0].Add("visitdate", data != null && data.VisitDate != null && data.VisitDate.IsValidDate() ? data.VisitDate : "");
            fieldmap[0].Add("mr", this.Patient.PatientIdNumber);
            fieldmap[0].Add("aide", data != null && data.Questions != null && data.Questions.ContainsKey("Therapist") && data.Questions["Therapist"].Answer.IsNotNullOrEmpty() ? UserEngine.GetName(data.Questions["Therapist"].Answer.ToGuid(), data.AgencyId) : "");
            fieldmap[0].Add("aidepresent", data != null && data.Questions != null && data.Questions.ContainsKey("TherapistPresent") && data.Questions["TherapistPresent"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["TherapistPresent"].Answer == "1" ? "Yes" : "") +
                (data.Questions["TherapistPresent"].Answer == "0" ? "No" : "")
            : "");
            fieldmap[0].Add("milage", data != null && data.Questions != null && data.Questions.ContainsKey("AssociatedMileage") && data.Questions["AssociatedMileage"].Answer.IsNotNullOrEmpty() ? data.Questions["AssociatedMileage"].Answer : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[0].Add("patientname", PrintHelper.PatientName(this.Patient));
            switch (data.Type)
            {
                case "PTSupervisoryVisit": fieldmap[1].Add("doctype", "PTA SUPERVISORY VISIT"); break;
                case "OTSupervisoryVisit": fieldmap[1].Add("doctype", "COTA SUPERVISORY VISIT"); break;
            }
            return fieldmap;
        }
    }
}