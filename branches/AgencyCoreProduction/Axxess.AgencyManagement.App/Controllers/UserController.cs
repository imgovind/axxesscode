﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Enums;
    using Domain;
    using ViewData;
    using Security;
    using Services;
    using iTextExtension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Repositories;

    using Axxess.Log.Enums;

    using Telerik.Web.Mvc;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class UserController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IMembershipService membershipService;
        private readonly IPatientService patientService;

        public UserController(IAgencyManagementDataProvider dataProvider, IUserService userService, IMembershipService membershipService, IPatientService patientService, IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(membershipService, "membershipService");

            this.userService = userService;
            this.membershipService = membershipService;
            this.userRepository = dataProvider.UserRepository;
            this.patientService = patientService;
        }

        #endregion

        #region UserController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView(userService.GetAgencyUserSubcriptionPlanDetails());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewContent(Guid userId)
        {
            return PartialView("NewContent", userService.GetUserForEditPermissionAndRole(userId));
        }

      

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");
            var viewData = new JsonViewData();
            
            if (!userService.IsEmailAddressInUse(user.EmailAddress))
            {
                if (user.IsValid)
                {
                    if (user.LocationList != null && user.LocationList.Count > 0)
                    {
                        user.AgencyId = Current.AgencyId;
                        user.AgencyName = Current.AgencyName;
                        if (userService.CreateUser(user))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "User was saved successfully";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "Error in saving the new User.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please select one branch/location for this user.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = user.ValidationMessage;
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "This E-mail Address is already in use for your agency";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid Id)
        {
            return PartialView(userService.GetUserForEditInformation(Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSection(Guid id, String category)
        {
            var cat = category.IsNotNullOrEmpty() ? category.Split('_').Last() : string.Empty;
            return PartialView("Edit/" + cat, userService.GetForEdit(id, Current.AgencyId, cat));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData();

            if (user.IsValid)
            {
                if (user.LocationList != null && user.LocationList.Count > 0)
                {
                    user.AgencyId = Current.AgencyId;
                    if (userRepository.Update(user))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserEdited, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "User was saved successfully";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in saving the new User.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Please select at least one branch/location for this user.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLicense([Bind] License license)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be saved." };
            if (userService.AddLicense(license, Request.Files))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "License saved successfully";
            }
            return PartialView("JsonResult", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLicense(Guid Id, Guid userId, DateTime ExpirationDate, string LicenseNumber)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");

            if (userService.UpdateLicense(Id, userId, ExpirationDate, LicenseNumber))
            {
                return View(new GridModel(userService.GetUserLicenses(userId, true)));
            }
            return View(new GridModel(new List<License>()));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteLicenseFromGrid(Guid Id, Guid userId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");

            if (userService.DeleteLicense(Id, userId))
            {
                return View(new GridModel(userService.GetUserLicenses(userId, true)));
            }
            return View(new GridModel(new List<License>()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdatePermissions(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Permissions could not be updated." };
            if (userService.UpdatePermissions(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Permissions updated successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Deactivate(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deactivated. Try Again." };
            if (userRepository.SetUserStatus(Current.AgencyId, userId, (int)UserStatus.Inactive))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserDeactivated, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deactivated successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MultiDeactivate(List<Guid> UserId)
        {

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User(s) cannot be deactivated. Please try again." };
            if (UserId != null && UserId.Count > 0)
            {
                if (userRepository.BulkDeactivate(UserId, true))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User(s) have been deactivated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activate(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be activated. Try Again." };

            if (userRepository.SetUserStatus(Current.AgencyId, userId, (int)UserStatus.Active))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, userId.ToString(), LogType.User, LogAction.UserActivated, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been activated successfully.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MultiActivate(List<Guid> UserId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User(s) cannot be activated. Please try again." };
            var userCount = userRepository.GetActiveUserCount(Current.AgencyId);

            if (UserId != null && UserId.Count > 0)
            {
                if (userRepository.BulkDeactivate(UserId, false))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User(s) have been activated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deleted. Please try again." };
            if (userService.DeleteUser(userId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Restore(Guid userId)
        {
            Check.Argument.IsNotEmpty(userId, "userId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be restored. Please try again." };
            if (userService.RestoreUser(userId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been restored successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MultiDelete(List<Guid> UserId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User(s) cannot be deleted. Please try again." };
            if (UserId != null && UserId.Count > 0)
            {
                if (userRepository.BulkDelete(UserId, true))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User(s) have been deleted successfully.";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MultiRestore(List<Guid> UserId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User(s) cannot be restored. Please try again." };
            if (UserId != null && UserId.Count > 0)
            {
                if (userRepository.BulkDelete(UserId, false))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User(s) have been restored successfully.";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Schedule()
        {
            ViewData["UserScheduleGroupName"] = "VisitDate";
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView(userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleGrouped(string groupName, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            ViewData["UserScheduleGroupName"] = groupName;
            return PartialView("ScheduleGrid", userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleList()
        {
            var pageNumber = this.HttpContext.Request.Params["page"];
            return View(new GridModel(userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14))));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ScheduleWidget()
        {
            return Json(userService.GetScheduleWidget(Current.UserId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Profile()
        {
            return PartialView("Profile/Edit", userService.GetUserProfileDisplayName(Current.UserId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Profile([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your profile could not be saved." };

            if (user.IsValid)
            {
                if (user.PasswordChanger.NewPassword.IsNotNullOrEmpty() && !userService.IsPasswordCorrect(user.Id, user.PasswordChanger.CurrentPassword))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The password provided does not match the one on file.";
                }
                else
                {
                    if (user.SignatureChanger.NewSignature.IsNotNullOrEmpty() && !userService.IsSignatureCorrect(user.Id, user.SignatureChanger.CurrentSignature))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature provided does not match the one on file.";
                    }
                    else
                    {
                        if (userService.UpdateProfile(user))
                        {

                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your profile has been updated successfully.";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult All()
        {
            return Json(userRepository.GetAgencyUsers(Current.AgencyId).ForSelection());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            var branches = LocationEngine.GetLocations(Current.AgencyId, Current.LocationIds);
            return View(branches);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Grid(Guid branchId, int status)
        {
            var users = userRepository.GetUsersLeanByStatus(Current.AgencyId, branchId, status).Select(user => new UserJson(user)).ToList();
            //var users = new List<User>();
            //var elementExit = false;
            //var activeUsers = userRepository.GetUsersByStatus(Current.AgencyId, (int)UserStatus.Active);
            //if (activeUsers != null && activeUsers.Count() > 0)
            //{
            //    elementExit = true;
            //    users.AddRange(activeUsers);
            //}
            //var inactiveUsers = userRepository.GetUsersByStatus(Current.AgencyId, (int)UserStatus.Inactive);
            //if (inactiveUsers != null && inactiveUsers.Count() > 0)
            //{
            //    elementExit = true;
            //    users.AddRange(inactiveUsers);
            //}
            //if (elementExit)
            //{
            //    ViewData["SortColumn"] = "PatientName";
            //    ViewData["SortDirection"] = "ASC";
            //}
            return CustomJson(new { data = users, total = users.Count });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DeletedUserList()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";


            return PartialView("DeletedUser", userRepository.GetDeletedUsers(Current.AgencyId, true).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedUser(string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            else
            {
                ViewData["SortColumn"] = "DisplayName";
                ViewData["SortDirection"] = "ASC";
            }

            return PartialView("DeletedUser", userRepository.GetDeletedUsers(Current.AgencyId, true).ToList());
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActiveContent(string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView(userRepository.GetUsersByStatus(Current.AgencyId, (int)UserStatus.Active).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InActiveContent(string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView(userRepository.GetUsersByStatus(Current.AgencyId, (int)UserStatus.Inactive).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult BranchList(Guid branchId, int status)
        {
            return Json(userService.GetUserByBranchAndStatus(branchId, status).Select(u => new { Id = u.Id, Name = u.DisplayName}).ToList());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ForgotSignature()
        {
            return PartialView("Signature/Forgot", Current.User.Session.EmailAddress);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GoToMeeting()
        {
            return PartialView("Help/GoToMeeting", string.Empty);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult JoinMe()
        {
            return PartialView("Help/JoinMeMeeting", string.Empty);
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Webinars()
        //{
        //    return PartialView("Help/Webinars", userRepository.Get(Current.UserId, Current.AgencyId, false));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EmailSignature()
        {
            var viewData = new JsonViewData();

            if (membershipService.ResetSignature(Current.LoginId))
            {
                viewData.isSuccessful = true;
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Signature could not be reset. Please try again later.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UserCalendar()
        {
            var fromDate = DateUtilities.GetStartOfMonth(DateTime.Today.Month, DateTime.Now.Year);
            var toDate = DateUtilities.GetEndOfMonth(DateTime.Today.Month, DateTime.Now.Year);
            return PartialView("Calendar", new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult UserCalendarPdf(DateTime month)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month.Month, month.Year);
            var toDate = DateUtilities.GetEndOfMonth(month.Month, month.Year);
            MonthCalendarPdf doc = new MonthCalendarPdf(new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
            var PdfStream = doc.GetStream();
            PdfStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MonthlyCalendar_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(PdfStream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserCalendarNavigate(int month, int year)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month, year);
            var toDate = DateUtilities.GetEndOfMonth(month, year);
            return PartialView("Calendar", new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserVisits(int month, int year)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month, year);
            var toDate = DateUtilities.GetEndOfMonth(month, year);
            return PartialView("CalendarGrid", new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserLogs(Guid userId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.User, Current.AgencyId, userId.ToString()));
        }
        #endregion

        #region License
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseList(Guid UserId)
        {
            return PartialView("License/List", userService.GetUserLicenses(UserId, true));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseNew(Guid UserId)
        {
            return PartialView("License/New", UserId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LicenseAdd([Bind] License license)
        {
            Check.Argument.IsNotNull(license, "license");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be added." };
            var entityValidator = new EntityValidator(
               new Validation(() => license.InitiationDate.Date > license.ExpirationDate.Date, "The Expiration Date cannot be earlier than the Issue Date.")
            );
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                if (userService.AddLicense(license, Request.Files))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "License added successfully";
                }
            }
            else
            {
                viewData.errorMessage = entityValidator.Message;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseEdit(Guid Id, Guid UserId)
        {
            var license = userService.GetUserLicense( UserId,Id);
            if (license != null)
            {
                Array licenseTypeValues = Enum.GetValues(typeof(LicenseTypes));
                bool isExisted = true;
                foreach (LicenseTypes licenseType in licenseTypeValues)
                {
                    if (licenseType.ToString().Equals(license.LicenseType))
                    {
                        isExisted = true;
                    }
                    else
                    {
                        isExisted = false;
                    }
                }
                if (!isExisted)
                {
                    license.OtherLicenseType = license.LicenseType;
                    license.LicenseType = "Other";
                }
            }
            
            return PartialView("License/Edit", license);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LicenseUpdate([Bind] License license)
        {
            Check.Argument.IsNotNull(license, "license");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The License could not be updated. Please try again." };
            if (license != null && userService.UpdateLicense(license))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, license.UserId.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "The License was updated successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseDelete(Guid Id, Guid UserId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(UserId, "UserId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User license cannot be deleted. Try Again." };
            if (userService.DeleteLicense(Id, UserId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User license has been successfully deleted.";
                return Json(viewData);
            }
            return Json(viewData);
        }
        #endregion

        #region Rate
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RateList(Guid UserId)
        {
            return PartialView("Rate/List", userService.GetUserRates(UserId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RateNew(Guid UserId)
        {
            return PartialView("Rate/New", UserId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RateAdd([Bind]UserRate userRate)
        {
            return Json( userService.AddUserRate(userRate));
        }

       
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RateEdit(int Id, Guid UserId, string Insurance)
        {
            return PartialView("Rate/Edit", userService.GetUserRateForEdit(UserId, Id, Insurance));
        }

    

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RateUpdate([Bind] UserRate userRate)
        {
            return Json(userService.UpdateUserRate(userRate));
        }

        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LoadUserRate(Guid fromId, Guid toId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to load rate. Please try again." };
            if (userService.LoadUserRate(fromId, toId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User rate loaded successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RateDelete(int Id, Guid UserId, string Insurance)
        {
            return Json(userService.DeleteUserRate(UserId, Id, Insurance));
        }

     
        #endregion

        #region NonVisitRate
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonVisitRateList(Guid UserId)
        {
            return PartialView("NonVisitRate/List", userService.GetUserNonVisitRateList(UserId));
        }

       
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonVisitRateNew(Guid UserId)
        {
            return PartialView("NonVisitRate/New", UserId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NonVisitRateAdd(UserNonVisitTaskRate userNonVisitTaskRate)
        {
            return Json(userService.AddUserNonVisitRate(userNonVisitTaskRate));
        }

     

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonVisitRateEdit(Guid Id, Guid UserId)
        {
            return PartialView("NonVisitRate/Edit", userService.GetUserNonVisitRateForEdit(  UserId, Id));
        }

       
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NonVisitRateUpdate(UserNonVisitTaskRate userNonVisitTaskRate)
        {
            return Json(userService.UpdateUserNonVisitRate(userNonVisitTaskRate));
        }

    
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonVisitRateDelete(Guid Id, Guid UserId)
        {
            return Json(userService.DeleteUserNonVisitRate( UserId, Id));
        }

      
        #endregion

        #region PatientAccess
        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult PatientAccessData(Guid id)
        //{
        //    var patients = patientRepository.GetPatientAccessData(Current.AgencyId, id);
        //    return Json(patients);
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult PatientAccessMove(bool left, Guid id, List<Guid> movedItems)
        //{
        //    var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to update patient access." };
        //    if (movedItems != null && movedItems.Count > 0)
        //    {
        //        if (left)
        //        {
        //            foreach (var patientId in movedItems) patientRepository.AddPatientUser(new PatientUser { UserId = id, PatientId = patientId });
        //            viewData.isSuccessful = true;
        //            viewData.errorMessage = "Patient access was successfully updated.";
        //        }
        //        else
        //        {
        //            foreach (var patientId in movedItems) patientRepository.RemovePatientUser(new PatientUser { UserId = id, PatientId = patientId });
        //            viewData.isSuccessful = true;
        //            viewData.errorMessage = "Patient access was successfully updated.";

        //        }
        //    }
        //    return Json(viewData);
        //}
        #endregion
    }
}
