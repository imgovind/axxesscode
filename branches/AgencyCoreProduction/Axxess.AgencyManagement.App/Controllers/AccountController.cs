﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using MvcReCaptcha;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Domain;
    using Security;
    using ViewData;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;
    using Axxess.Log.Enums;

    [Compress]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AccountController : BaseController
    {
        #region Constructor

        public AccountController()
            : this(null, null)
        {
        }

        public AccountController(IFormsAuthenticationService formsAuthentication, IMembershipService membershipService)
        {
            this.MembershipService = membershipService;
            this.AuthenticationService = formsAuthentication;
        }

        #endregion

        #region Properties

        public IMembershipService MembershipService
        {
            get;
            private set;
        }

        public IFormsAuthenticationService AuthenticationService
        {
            get;
            private set;
        }

        #endregion

        #region AccountController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOn()
        {
            var logon = new Logon();
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                return RedirectToAction("Index", "Home");
            }

            if ((HttpContext.Request.Cookies.Count > 0) && (HttpContext.Request.Cookies[AppSettings.RememberMeCookie] != null))
            {
                var cookie = HttpContext.Request.Cookies[AppSettings.RememberMeCookie];
                logon.UserName = Crypto.Decrypt(cookie.Value);
            }

            return View(logon);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings",
            Justification = "Needs to take same parameter type as Controller.Redirect()")]
        public JsonResult LogOn([Bind] Logon logon)
        {
            var viewData = Validate<AccountViewData>(
                             new Validation(() => string.IsNullOrEmpty(logon.UserName), "You must specify a username. <br/>"),
                             new Validation(() => string.IsNullOrEmpty(logon.Password), "You must specify a password.  <br/>"),
                             new Validation(() => !logon.UserName.IsEmail(), "E-mail Address is not valid.<br/>")
                        );
            if (viewData.isSuccessful)
            {
                if (LoginMonitor.Instance.IsUserLocked(logon.UserName, Current.IpAddress))
                {
                    viewData.isLocked = true;
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "You exceeded the maximum number of login attempts. Please note that your AgencyCore online access will be disabled for 5 minutes. If you cannot remember your password, please click <a href=\"/Forgot\">here</a> to reset your password.";
                }
                else
                {
                    var account = new Account { UserName = logon.UserName.Trim(), Password = logon.Password.Trim() };
                    if (MembershipService.Login(account))
                    {
                        viewData.errorMessage = "Login successful.";
                        if (account.HasMultiple)
                        {
                            viewData.hasMultiple = true;
                            viewData.isSuccessful = true;
                            viewData.email = account.EmailAddress;
                            viewData.id = account.LoginId.ToString();
                        }
                        else
                        {
                            var loginAttempt = this.MembershipService.Validate(account);
                            if (loginAttempt == LoginAttemptType.Success)
                            {
                                viewData.redirectUrl = "/";
                                viewData.isSuccessful = true;
                                this.AuthenticationService.SignIn(string.Format("{0}_{1}", account.UserId, account.AgencyId), logon.RememberMe);
                                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.UserId.ToString(), LogType.User, LogAction.UserLoggedIn, string.Empty);
                            }
                            else
                            {
                                if (loginAttempt == LoginAttemptType.AccountInUse)
                                {
                                    viewData.isAccountInUse = true;
                                    viewData.userId = account.UserId.ToString();
                                    viewData.agencyId = account.AgencyId.ToString();
                                }

                                viewData.isSuccessful = false;
                                viewData.errorMessage = ProcessLoginAttempt(loginAttempt);
                            }
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Username / Password combination failed.";

                        LoginMonitor.Instance.Track(logon.UserName.Trim(), Current.IpAddress);
                        if (!LoginMonitor.Instance.HasAttempts(logon.UserName.Trim(), Current.IpAddress))
                        {
                            viewData.isLocked = true;
                            viewData.errorMessage = "You exceeded the maximum number of login attempts. Please note that your AgencyCore online access will be disabled for 5 minutes. If you cannot remember your password, please click <a href=\"/Forgot\">here</a> to reset your password.";
                        }
                    }
                }
            }

            return Json(viewData);
        }

        private string ProcessLoginAttempt(LoginAttemptType loginAttempt)
        {
            var message = string.Empty;
            switch (loginAttempt)
            {
                case LoginAttemptType.Locked:
                    message = "This user has been locked. Please contact your Agency/Company's Administrator.";
                    break;
                case LoginAttemptType.Deactivated:
                    message = "This user is deactivated. Please contact your Agency/Company's Administrator.";
                    break;
                case LoginAttemptType.TrialPeriodOver:
                    message = "Your Company's trial period is over. Please contact your Agency/Company's Administrator.";
                    break;
                case LoginAttemptType.WeekendAccessRestricted:
                    message = "Your weekend access has been restricted. Please contact your Agency/Company's Administrator.";
                    break;
                case LoginAttemptType.AccountSuspended:
                    message = "You do not have access to your account at this time. Please contact your Agency/Company's Administrator.";
                    break;
                case LoginAttemptType.TooManyAttempts:
                    message = "You exceeded the maximum number of login attempts. Please note that your AgencyCore online access will be disabled for 5 minutes. If you cannot remember your password, please click <a href=\"/Forgot\">here</a> to reset your password.";
                    break;
                case LoginAttemptType.AccountInUse:
                    message = "This user is already logged in on another computer. If you choose to proceed, the user will be automatically logged off the other computer and their work will not be saved.";
                    break;
                case LoginAttemptType.AgencyFrozen:
                    message = "Your Company has been frozen. Please contact your Agency/Company's Administrator.";
                    break;
                case LoginAttemptType.TrialAccountExpired:
                    message = "Your trial account has expired. Please contact Axxess for a full account.";
                    break;
                case LoginAttemptType.AgencyReadOnlyPeriodExpired:
                    message = "Your 2-year read-only access has expired. Please contact Axxess for a full account.";
                    break;
                case LoginAttemptType.TooEarly:
                    message = "Your account is restricted to logging in after a certain time. Please contact your Agency/Company's Administrator.";
                    break;
                default:
                    message = "Login failed.";
                    break;
            }
            return message;
        }

        private string ProcessResetAttempt(ResetAttemptType resetAttempt)
        {
            var message = string.Empty;
            switch (resetAttempt)
            {
                case ResetAttemptType.Locked:
                    message = "This user has been locked. Please contact your Agency/Company's Administrator.";
                    break;
                case ResetAttemptType.Deactivated:
                    message = "This user is deactivated. Please contact your Agency/Company's Administrator.";
                    break;
                default:
                    message = "Reset failed. Please try again.";
                    break;
            }
            return message;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AppLogOn()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(System.Web.HttpContext.Current.User.Identity.Name);
                this.AuthenticationService.SignOut();
            }

            if (HttpContext.Request.QueryString["linkid"] != null)
            {
                var linkId = HttpContext.Request.QueryString["linkid"].ToGuid();
                if (MembershipService.Impersonate(linkId))
                {
                    this.AuthenticationService.SignIn(Current.User.Name, false);
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("LogOn", "Account");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FacebookLogOn(string accessToken)
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(System.Web.HttpContext.Current.User.Identity.Name);
                this.AuthenticationService.SignOut();
            }

            if (accessToken.IsNotNullOrEmpty())
            {
                if (MembershipService.FacebookLogin(accessToken))
                {
                    this.AuthenticationService.SignIn(Current.User.Name, false);
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("LogOn", "Account");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LogOff()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.UserId.ToString(), LogType.User, LogAction.UserLoggedOut, string.Empty);
                this.MembershipService.LogOff(System.Web.HttpContext.Current.User.Identity.Name);
                this.AuthenticationService.SignOut();
            }

            return RedirectToAction("LogOn", "Account");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ForgotPassword()
        {
            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(System.Web.HttpContext.Current.User.Identity.Name);
                this.AuthenticationService.SignOut();
            }
            return View();
        }

        [CaptchaValidator]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ForgotPassword(string EmailAddress, bool captchaValid)
        {
            var viewData = Validate<JsonViewData>(
                            new Validation(() => string.IsNullOrEmpty(EmailAddress.Trim()), "You must specify your E-mail Address. <br/>"),
                            new Validation(() => !EmailAddress.IsEmail(), "E-mail Address is not valid.<br/>"),
                            new Validation(() => !(captchaValid), "The Captcha text you entered didn't match the security check. Please try again. <br/>")
                       );

            if (viewData.isSuccessful)
            {
                var resetAttempt = this.MembershipService.Validate(EmailAddress.Trim());

                if (resetAttempt == ResetAttemptType.Success)
                {
                    string baseUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, UrlHelper.GenerateContentUrl("~", HttpContext));
                    if (this.MembershipService.ResetPassword(EmailAddress.Trim(), baseUrl))
                    {
                        viewData.errorMessage = "A link to reset your password has been sent to your e-mail address.<br />Please check your e-mail to complete the login process.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = ProcessResetAttempt(resetAttempt);
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Reset()
        {
            var account = new Account();
            if (HttpContext.Request.QueryString["loginid"] != null && HttpContext.Request.QueryString["type"] != null)
            {
                var loginRepository = Container.Resolve<IMembershipDataProvider>().LoginRepository;
                var login = loginRepository.Find(HttpContext.Request.QueryString["loginid"].ToGuid());
                if (login != null)
                {
                    account.LoginId = login.Id;
                    account.Name = login.DisplayName;
                    account.EmailAddress = login.EmailAddress;
                    account.ResetType = HttpContext.Request.QueryString["type"].IsEqual("password") ? Change.Password : Change.Signature;
                }
            }
            else
            {
                throw new HttpException(404, string.Empty);
            }
            return View(account);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "Exceptions result in password not being changed.")]
        public JsonResult Reset([Bind] Account account)
        {
            Check.Argument.IsNotNull(account, "account");

            var viewData = new AccountViewData();

            if (account.ResetType == Change.Password)
            {
                viewData = Validate<AccountViewData>(
                           new Validation(() => string.IsNullOrEmpty(account.Password), "You must specify a password. <br/>"),
                           new Validation(() => account.Password.Length < 8, "The minimum password length is 8 characters.")
                        );

                if (viewData.isSuccessful)
                {
                    if (this.MembershipService.UpdatePassword(account))
                    {
                        LoginMonitor.Instance.Remove(account.EmailAddress);
                        viewData.errorMessage = "Your password has been successfully changed. <a href=\"/login\">Login</a>";
                        viewData.redirectUrl = "/";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Change password attempt failed.";
                    }
                }
            }
            else
            {
                viewData = Validate<AccountViewData>(
                           new Validation(() => string.IsNullOrEmpty(account.Signature), "You must specify a signature. <br/>"),
                           new Validation(() => account.Signature.Length < 8, "The minimum signature length is 8 characters.")
                        );

                if (viewData.isSuccessful)
                {
                    if (this.MembershipService.UpdateSignature(account))
                    {
                        viewData.errorMessage = "Your signature has been successfully changed. <a href=\"/login\">Login</a>";
                        viewData.redirectUrl = "/";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Change signature attempt failed.";
                    }
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Activate()
        {
            var account = new Account();
            if (HttpContext.Request.QueryString["id"] != null && HttpContext.Request.QueryString["agencyid"] != null)
            {
                var loginRepository = Container.Resolve<IMembershipDataProvider>().LoginRepository;
                var userRepository = Container.Resolve<IAgencyManagementDataProvider>().UserRepository;
                var agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;
                var user = userRepository.GetUserOnly(HttpContext.Request.QueryString["id"].ToGuid(), HttpContext.Request.QueryString["agencyid"].ToGuid());
                if (user != null)
                {
                    account.UserId = user.Id;
                    account.Name = user.DisplayName;
                    var login = loginRepository.Find(user.LoginId);
                    if (login != null)
                    {
                        account.EmailAddress = login.EmailAddress;
                    }
                    account.AgencyName = AgencyEngine.GetName(user.AgencyId);
                    account.AgencyId = user.AgencyId;
                }
            }
            else
            {
                throw new HttpException(404, string.Empty);
            }
            return View(account);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Activate([Bind] Account account)
        {
            var viewData = Validate<AccountViewData>(
                            new Validation(() => string.IsNullOrEmpty(account.Password), "You must specify a password. <br/>"),
                            new Validation(() => account.Password.IsNotNullOrEmpty() &&  account.Password.Length < 8, "The minimum password length is 8 characters.")
                            );

            if (viewData.isSuccessful)
            {
                if (this.MembershipService.Activate(account))
                {
                    this.AuthenticationService.SignIn(string.Format("{0}_{1}", account.UserId, account.AgencyId), false);
                    viewData.redirectUrl = "/";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Account Activation failed. Please try again later.";
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Link()
        {
            var account = new Account();
            if (HttpContext.Request.QueryString["id"] != null && HttpContext.Request.QueryString["agencyid"] != null)
            {
                var loginRepository = Container.Resolve<IMembershipDataProvider>().LoginRepository;
                var userRepository = Container.Resolve<IAgencyManagementDataProvider>().UserRepository;
                var agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;
                var user = userRepository.GetUserOnly(HttpContext.Request.QueryString["id"].ToGuid(), HttpContext.Request.QueryString["agencyid"].ToGuid());
                if (user != null)
                {
                    account.UserId = user.Id;
                    account.Name = user.DisplayName;
                    var login = loginRepository.Find(user.LoginId);
                    if (login != null)
                    {
                        account.LoginId = login.Id;
                        account.EmailAddress = login.EmailAddress;
                    }
                    account.AgencyName = AgencyEngine.GetName(user.AgencyId);
                    account.AgencyId = user.AgencyId;
                }
            }
            return View(account);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Agencies(Guid loginId)
        {
            var userRepository = Container.Resolve<IAgencyManagementDataProvider>().UserRepository;
            return PartialView(userRepository.GetAgencies(loginId).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Agency(Guid agencyId, Guid userId)
        {
            var account = new Account { AgencyId = agencyId, UserId = userId };
            var viewData = new AccountViewData() { isSuccessful = false, errorMessage = "Could not log into the selected agency profile." };

            if (System.Web.HttpContext.Current.IsUserAuthenticated())
            {
                this.MembershipService.LogOff(System.Web.HttpContext.Current.User.Identity.Name);
                this.AuthenticationService.SignOut();
            }

            if (this.MembershipService.InitializeWith(account))
            {
                var loginAttempt = this.MembershipService.Validate(account);
                if (loginAttempt == LoginAttemptType.Success)
                {
                    viewData.redirectUrl = "/";
                    viewData.isSuccessful = true;
                    this.AuthenticationService.SignIn(string.Format("{0}_{1}", account.UserId, account.AgencyId), false);
                    Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.UserId.ToString(), LogType.User, LogAction.UserLoggedIn, string.Empty);
                }
                else
                {
                    if (loginAttempt == LoginAttemptType.AccountInUse)
                    {
                        viewData.userId  = account.UserId.ToString();
                        viewData.agencyId  = account.AgencyId.ToString();
                        viewData.email = account.EmailAddress;
                        viewData.isAccountInUse = true;
                    }

                    viewData.isSuccessful = false;
                    viewData.errorMessage = ProcessLoginAttempt(loginAttempt);
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Kick(Guid userId, Guid agencyId)
        {
            var viewData = new AccountViewData() { isSuccessful = false, errorMessage = "Could not proceed with login. Please try again." };

            if (this.MembershipService.Switch(userId, agencyId))
            {
                viewData.redirectUrl = "/";
                viewData.isSuccessful = true;
                viewData.errorMessage = string.Empty;
                this.AuthenticationService.SignIn(string.Format("{0}_{1}", userId, agencyId), false);
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, Current.UserId.ToString(), LogType.User, LogAction.UserLoggedIn, string.Empty);
            }

            return Json(viewData);
        }

        #endregion
    }

}
