﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.UI.WebControls;
    using System.Collections.Generic;

    using Common;
    using ViewData;
    using Services;
    using Security;
    using Workflows;
    using Extensions;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    using Axxess.LookUp.Domain;

    using Axxess.Log.Enums;

    using Telerik.Web.Mvc;
    using Axxess.Core.Enums;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ScheduleController : BaseController
    {
        #region Constructor

        private readonly IPatientService patientService;
        private readonly INoteService noteService;
        private readonly IAssessmentService assessmentService;
        private readonly IOrderManagementService orderManagementService;
        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IReportService reportService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IEpisodeRepository episodeRepository;

        public ScheduleController(IAgencyManagementDataProvider dataProvider, IPatientService patientService, INoteService noteService, IUserService userService, IAgencyService agencyService, IReportService reportService, IAssessmentService assessmentService, IOrderManagementService orderManagementService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(patientService, "patientService");

            this.patientService = patientService;
            this.userRepository = dataProvider.UserRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.scheduleRepository = dataProvider.ScheduleRepository;
            this.episodeRepository = dataProvider.EpisodeRepository;
            this.noteService = noteService;
            this.userService = userService;
            this.agencyService = agencyService;
            this.reportService = reportService;
            this.assessmentService = assessmentService;
            this.orderManagementService = orderManagementService;
        }

        #endregion

        #region ScheduleController Actions

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult All(int statusId, Guid branchId, byte paymentSourceId)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                patientList = patientRepository.GetPatientSelection(Current.AgencyId, branchId.IsEmpty() ? Current.LocationIds : new List<Guid>() { branchId }, statusId > 0 ? new List<int> { statusId } : PatientStatusFactory.CenterStatus(), paymentSourceId, "");
            }
            else if (Current.IsClinicianOrHHA)
            {
                patientList = patientRepository.GetPatientEpisodeDataForList(Current.AgencyId, branchId.IsEmpty() ? Current.LocationIds : new List<Guid> { branchId }, Current.UserId, statusId > 0 ? new List<int> { statusId } : PatientStatusFactory.CenterStatus(), "PaymentSource", paymentSourceId); //patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId);

                //var patientsAccess = patientRepository.GetPatientsWithUserAccess(Current.UserId, Current.AgencyId, Guid.Empty, (int)statusId, 0, "");
                //if (patientsAccess != null)
                //{
                //    foreach (var p in patientsAccess)
                //    {
                //        var search = patientList.Find(pa => pa.DisplayName == p.DisplayName);
                //        if (search == null)
                //        {
                //            patientList.Add(p);
                //        }
                //        search = null;
                //    }
                //}
                //var teamPatients = agencyService.GetTeamAccessPatients(Current.AgencyId, Current.UserId);
                //patientList.AddRange(teamPatients);
            }
            else if (Current.IfOnlyRole(AgencyRoles.Auditor))
            {
                patientList = patientRepository.GetAuditorPatientSelection(Current.AgencyId, branchId.IsEmpty() ? Current.LocationIds : new List<Guid>() { branchId }, statusId, paymentSourceId, "", Current.UserId);
            }
            else { }

            patientList = patientList ?? new List<PatientSelection>();
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Center(int? status)
        {
            var viewData = new ScheduleViewData();
            int patientStatus = status.HasValue ? status.Value : (int)PatientStatus.Active;
            viewData.PatientStatus = patientStatus;
            //if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            //{
            //    viewData.Count = patientRepository.GetPatientStatusCount(Current.AgencyId, patientStatus);
            //}
            //else if (Current.IsClinicianOrHHA)
            //{
            //    viewData.Count = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)patientStatus).Count;
            //}
            //else { viewData.Count = 0; }
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Data(Guid patientId)
        {

            var viewData = new CalendarViewData();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                viewData = patientService.GetScheduleWithPreviousAfterEpisodeInfo(patientId, DateTime.Now, "All", true, true, true) ?? new CalendarViewData();
                viewData.PatientId = patientId;
                viewData.IsDischarged = patient.IsDischarged;
                viewData.IsNotAdmitted = patient.IsNotAdmitted;
                viewData.IsPending = patient.IsPending;
                viewData.DisplayName = patient.DisplayName;
                viewData.PatientStatus = patient.Status;
            }
            //var viewData = new ScheduleViewData();
            //viewData.PatientId = patientId;
            //var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            //if (patient != null)
            //{
            //    viewData.Episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, "all");
            //    viewData.IsDischarged = patient.IsDischarged;
            //    viewData.IsNotAdmitted = patient.IsNotAdmitted;
            //    viewData.IsPending = patient.IsPending;
            //    viewData.PatientStatus = patient.Status;
            //}

            //List<ScheduleEventJson> tasks = null;
            //string patientName = patientRepository.GetPatientNameById(patientId, Current.AgencyId);
            //var previousEpisodeId = Guid.Empty;
            //var nextEpisodeId = Guid.Empty;
            //PatientEpisode episode;
            //if (episodeId.HasValue)
            //{
            //    episode = patientRepository.GetEpisodeLean(Current.AgencyId, patientId, episodeId.Value);
            //}
            //else
            //{
            //    episode = patientRepository.GetMostRecentEpisodeLean(Current.AgencyId, patientId);
            //}
            //if (episode != null)
            //{
            //    var schedule = episode.Schedule.ToObject<List<ScheduleEvent>>();
            //    var episodeDetail = episode.Details.ToObject<EpisodeDetail>();
            //    tasks = patientService.ProcessScheduleEvents(episode.Id, patientId, schedule, episodeDetail, episode.StartDate, episode.EndDate).Select(s => new ScheduleEventJson(s)).OrderByDescending(o => o.Date).ToList();
            //    var allEpisodes = patientRepository.GetEpisodeLeans(Current.AgencyId, patientId).OrderByDescending(o => o.StartDate).ToList();
            //    if (allEpisodes.IsNotNullOrEmpty())
            //    {
            //        var indexOfEpisode = allEpisodes.IndexOf(episode);
            //        if (indexOfEpisode > 0)
            //        {
            //            nextEpisodeId = allEpisodes[indexOfEpisode - 1].Id;
            //        }
            //        if (indexOfEpisode < allEpisodes.Count - 1)
            //        {
            //            previousEpisodeId = allEpisodes[indexOfEpisode + 1].Id;
            //        }
            //    }
            //    return CustomJson(new { scheduleEvents = tasks, id = episode.Id, patientName, startDate = episode.StartDateFormatted, endDate = episode.EndDateFormatted, previousEpisodeId, nextEpisodeId, episodes = allEpisodes.Select(e => new { id = e.Id, dateRange = e.StartDateFormatted + " - " + e.EndDateFormatted }).ToList() });
            //}
            //return CustomJson(new { id = Guid.Empty });
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RefreshData(Guid patientId, Guid episodeId)
        {

            var viewData = new CalendarViewData();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                viewData = patientService.GetScheduleWithPreviousAfterEpisodeInfo(patientId, DateTime.Now, "All", true, true, true) ?? new CalendarViewData();
                viewData.PatientId = patientId;
                viewData.IsDischarged = patient.IsDischarged;
                viewData.IsNotAdmitted = patient.IsNotAdmitted;
                viewData.IsPending = patient.IsPending;
                viewData.DisplayName = patient.DisplayName;
                viewData.PatientStatus = patient.Status;
            }
            //var viewData = new ScheduleViewData();
            //viewData.PatientId = patientId;
            //var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            //if (patient != null)
            //{
            //    viewData.Episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, "all");
            //    viewData.IsDischarged = patient.IsDischarged;
            //}
            return PartialView("Data", viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Activities(Guid patientId, string discipline)
        //{
        //    return PartialView(patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Calendar(Guid patientId, string discipline)
        {

            CalendarViewData calendarViewData = null;
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    calendarViewData = patientService.GetScheduleWithPreviousAfterEpisodeInfo(patientId, DateTime.Now, discipline, true, true, false) ?? new CalendarViewData();
                    calendarViewData.LocationId = patient.AgencyLocationId;
                    calendarViewData.DisplayName = patient.DisplayName;
                    calendarViewData.IsDischarged = patient.IsDischarged;
                    calendarViewData.PatientId = patientId;
                    calendarViewData.IsDischarged = patient.IsDischarged;
                }
            }
            //CalendarViewData calendarViewData = null;
            //if (!patientId.IsEmpty())
            //{
            //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            //    if (patient != null)
            //    {
            //        calendarViewData = new CalendarViewData();
            //        var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline);
            //        if (episode != null)
            //        {
            //            calendarViewData.Episode = episode;
            //            calendarViewData.PatientId = patientId;
            //        }
            //        else
            //        {
            //            calendarViewData.PatientId = patientId;
            //        }
            //        calendarViewData.PatientId = patientId;
            //        calendarViewData.IsDischarged = patient.IsDischarged;
            //    }
            //}
            return PartialView(calendarViewData ?? new CalendarViewData());
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activity(Guid patientId, Guid? episodeId)
        {
            Check.Argument.IsNotNull(patientId, "patientId");
            var viewData = new CalendarViewData();
            if (episodeId.HasValue)
            {
                viewData = patientService.GetScheduleWithPreviousAfterEpisodeInfoNew(patientId, episodeId.Value, "All", true, false) ?? new CalendarViewData();
            }
            else
            {
                viewData = patientService.GetScheduleWithPreviousAfterEpisodeInfoNew(patientId, DateTime.Now, "All", true) ?? new CalendarViewData();
            }
            var name = "";
            var tasks = new List<ScheduleEventJson>();
            if (viewData.ScheduleEvents.IsNotNullOrEmpty())
            {
                viewData.ScheduleEvents.ForEach(s => tasks.Add(new ScheduleEventJson(s)));
            }

            return CustomJson(new { scheduleEvents = tasks, id = viewData.EpisodeId, name, startDate = viewData.StartDate.ToString("MM/dd/yyyy"), endDate = viewData.EndDate.ToString("MM/dd/yyyy"), viewData.PreviousEpisode, viewData.NextEpisode, episodes = viewData.Episodes.Select(e => new { id = e.Id, dateRange = e.StartDateFormatted + " - " + e.EndDateFormatted }).ToList() });
        }

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Activity(Guid episodeId, Guid patientId, string discipline, string orderBy)
        //{
        //    Check.Argument.IsNotNull(episodeId, "episodeId");
        //    Check.Argument.IsNotNull(patientId, "patientId");
        //    Check.Argument.IsNotNull(discipline, "discipline");

        //    var patientActivities = patientService.GetScheduledEvents(patientId, episodeId, discipline);
        //    if (orderBy.IsNotNullOrEmpty())
        //    {
        //        string sortPram = orderBy.Split('-')[0].Trim();
        //        string sortOrder = orderBy.Split('-')[1].Trim();
        //        patientActivities = patientActivities.AsQueryable().OrderByDynamic(sortPram, sortOrder == "asc" ? SortDirection.Ascending : SortDirection.Descending).ToList();
        //    }
        //    else
        //    {
        //        patientActivities = patientActivities.OrderByDescending(e => e.EventDateSortable).ToList();
        //    }
        //    return new LargeJsonResult
        //    {
        //        MaxJsonLength = int.MaxValue,
        //        JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet,
        //        Data = new GridModel<ScheduleEvent>
        //        {
        //            Data = patientActivities ?? new List<ScheduleEvent>()
        //        }
        //    };
        //}


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MultiDay(Guid episodeId, Guid patientId)
        {
            return PartialView("MultiDayScheduler", episodeRepository.GetEpisodeLean(Current.AgencyId, patientId, episodeId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditDetails(Guid episodeId, Guid patientId, Guid eventId)
        {
            if (patientId.IsEmpty() || episodeId.IsEmpty() || eventId.IsEmpty())
            {
                return PartialView("Detail/Edit", new ScheduleEvent());
            }
            return PartialView("Detail/Edit", patientService.GetScheduledEvent(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Attachments(Guid episodeId, Guid patientId, Guid eventId)
        {
            if (patientId.IsEmpty() || episodeId.IsEmpty() || eventId.IsEmpty())
            {
                return PartialView("Attachments", new ScheduleEvent());
            }
            return PartialView("Attachments", patientService.GetScheduledEvent(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateDetails(ScheduleEvent scheduleEvent)
        {
            return Json(patientService.UpdateScheduleEventDetails(scheduleEvent, Request.Files));
            //var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task details could not be saved." };
            //var validationRules = new List<Validation>();
            //if (scheduleEvent.IsEpisodeReassiged && scheduleEvent.EpisodeId != scheduleEvent.NewEpisodeId)
            //{
            //    var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.NewEpisodeId, scheduleEvent.PatientId);
            //    if (patientEpisode != null)
            //    {
            //        var oldEvents = (patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).Where(s => s.EventDate.IsValidDate() && !s.IsDeprecated).OrderByDescending(o => o.EventDate.ToDateTime().Date).ToList();
            //        var evnt = scheduleEvent;
            //        if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST))
            //        {
            //            var transfer = oldEvents.FirstOrDefault(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT) && (oe.EventDate.ToDateTime().Date < evnt.EventDate.ToDateTime().Date));
            //            ScheduleEvent roc = null;
            //            if (transfer != null) roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date));
            //            if (transfer != null && roc == null)
            //            {
            //                viewData.errorMessage = "Creation of a Recertification Assessment is not allowed if the patient was transferred. Please create a Resumption of Care Assessment instead.";
            //                return Json(viewData);
            //            }
            //            else if (transfer != null && roc != null && roc.EventDate.ToDateTime().Date <= transfer.EventDate.ToDateTime().Date)
            //            {
            //                viewData.errorMessage = "Creation of a Recertification Assessment is not allowed if the patient was transfered. Please create a Resumption of Care Assessment instead.";
            //                return Json(viewData);
            //            }
            //            else if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST))
            //            {
            //                viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete the exisiting one before creating a new one.";
            //                return Json(viewData);
            //            }
            //            else if (evnt.EventDate.ToDateTime().Date < patientEpisode.EndDate.AddDays(-5).Date || evnt.EventDate.ToDateTime().Date > patientEpisode.EndDate.Date)
            //            {
            //                viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
            //                return Json(viewData);
            //            }
            //        }
            //        else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || evnt.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCStartofCareOT))
            //        {
            //            if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT || oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCareOT))
            //            {
            //                viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete the exisiting one before creating a new one.";
            //                return Json(viewData);
            //            }
            //        }
            //        else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT))
            //        {
            //            var roc = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
            //            var transfer = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
            //            if (roc == null)
            //            {
            //                if (transfer == null)
            //                {
            //                    viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
            //                    return Json(viewData);
            //                }
            //                else if (transfer != null && (transfer.EventDate.ToDateTime() > evnt.EventDate.ToDateTime()))
            //                {
            //                    viewData.errorMessage = "The Resumption of Care date should be later that the Transfer date.";
            //                    return Json(viewData);
            //                }
            //            }
            //            else if (roc != null)
            //            {
            //                if (transfer != null && (roc.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date))
            //                {
            //                    viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
            //                    return Json(viewData);
            //                }
            //            }
            //        }
            //        else if ((evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || evnt.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT))
            //        {
            //            var transfer = oldEvents.FirstOrDefault(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
            //            ScheduleEvent roc = null;
            //            if (transfer != null) roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date));
            //            if (transfer != null && roc == null)
            //            {
            //                viewData.errorMessage = "Please Create a Resumption of Care Assessment before creating another Transfer.";
            //                return Json(viewData);
            //            }
            //        }
            //        if (evnt.DisciplineTask != (int)DisciplineTasks.FaceToFaceEncounter && evnt.Discipline!="Orders")
            //        {
            //            validationRules.Add(new Validation(() => scheduleEvent.EventDate.IsNullOrEmpty(), "Schedule date is required."));
            //            validationRules.Add(new Validation(() => !scheduleEvent.EventDate.IsValidDate(), "Schedule date is not valid."));
            //            validationRules.Add(new Validation(() => scheduleEvent.EventDate.IsNotNullOrEmpty() && scheduleEvent.EventDate.IsValidDate() ? !(scheduleEvent.EventDate.ToDateTime() >= patientEpisode.StartDate.Date && scheduleEvent.EventDate.ToDateTime().Date <= patientEpisode.EndDate.Date) : true, "Schedule date is not in the episode range."));
            //            validationRules.Add(new Validation(() => scheduleEvent.VisitDate.IsNullOrEmpty(), "Visit date is required."));
            //            validationRules.Add(new Validation(() => !scheduleEvent.VisitDate.IsValidDate(), "Visit date is not valid."));
            //            validationRules.Add(new Validation(() => scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? !(scheduleEvent.VisitDate.ToDateTime() >= patientEpisode.StartDate.Date && scheduleEvent.VisitDate.ToDateTime().Date <= patientEpisode.EndDate.Date) : true, "Visit date is not in the episode range."));
            //        }
            //        var entityValidator = new EntityValidator(validationRules.ToArray());
            //        entityValidator.Validate();
            //        if (entityValidator.IsValid)
            //        {
            //            if (patientService.UpdateScheduleEventDetail(scheduleEvent, Request.Files))
            //            {
            //                string message = string.Empty;
            //                scheduleEvent.EpisodeId = scheduleEvent.NewEpisodeId;
            //                patientService.CheckTimeOverlap(scheduleEvent, out message);
            //                viewData.errorMessage = "Task details updated successfully."+message;
            //                viewData.isSuccessful = true;
            //            }
            //        }
            //        else viewData.errorMessage = entityValidator.Message;
            //    }
            //}
            //else
            //{
            //    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
            //    if (episode != null)
            //    {
            //        if (scheduleEvent.DisciplineTask != (int)DisciplineTasks.FaceToFaceEncounter)
            //        {
            //            validationRules.Add(new Validation(() => scheduleEvent.EventDate.IsNullOrEmpty(), "Schedule date is required."));
            //            validationRules.Add(new Validation(() => !scheduleEvent.EventDate.IsValidDate(), "Schedule date is not valid."));
            //            validationRules.Add(new Validation(() => scheduleEvent.EventDate.IsNotNullOrEmpty() && scheduleEvent.EventDate.IsValidDate() ? !(scheduleEvent.EventDate.ToDateTime() >= episode.StartDate && scheduleEvent.EventDate.ToDateTime() <= episode.EndDate) : true, "Schedule date is not in the episode range."));
            //            validationRules.Add(new Validation(() => scheduleEvent.VisitDate.IsNullOrEmpty(), "Visit date is required."));
            //            validationRules.Add(new Validation(() => !scheduleEvent.VisitDate.IsValidDate(), "Visit date is not valid."));
            //            validationRules.Add(new Validation(() => scheduleEvent.VisitDate.IsNotNullOrEmpty() && scheduleEvent.VisitDate.IsValidDate() ? !(scheduleEvent.VisitDate.ToDateTime() >= episode.StartDate && scheduleEvent.VisitDate.ToDateTime() <= episode.EndDate) : true, "Visit date is not in the episode range."));
            //        }
            //        var entityValidator = new EntityValidator(validationRules.ToArray());
            //        entityValidator.Validate();
            //        if (entityValidator.IsValid)
            //        {
            //            if (patientService.UpdateScheduleEvent(scheduleEvent, Request.Files))
            //            {
            //                string message = string.Empty;
            //                patientService.CheckTimeOverlap(scheduleEvent, out message);
            //                viewData.errorMessage = "Task details updated successfully."+message;
            //                viewData.isSuccessful = true;
            //            }
            //        }
            //        else viewData.errorMessage = entityValidator.Message;
            //    }
            //}
            //return Json(viewData, "text/html; charset=utf-8");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Missed/New", scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddMissedVisit([Bind] MissedVisit missedVisit)
        {
            Check.Argument.IsNotNull(missedVisit, "missedVisit");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Missed visit could not be saved." };
            if (!missedVisit.Id.IsEmpty() && !missedVisit.PatientId.IsEmpty() && !missedVisit.EpisodeId.IsEmpty())
            {
                viewData = noteService.AddMissedVisit(missedVisit);
                //var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, missedVisit.PatientId, missedVisit.Id); //patientRepository.GetSchedule(Current.AgencyId, missedVisit.EpisodeId, missedVisit.PatientId, missedVisit.Id);
                //if (scheduleEvent != null)
                //{
                //    if (scheduleEvent.EventDate.IsNotNullOrEmpty() && scheduleEvent.EventDate.IsValidDate() && scheduleEvent.EventDate.ToDateTime().Date <= DateTime.Now.Date)
                //    {
                //        var validationRules = new List<Validation>();
                //        validationRules.Add(new Validation(() => !missedVisit.SignatureDate.IsValid(), "Signature date is not valid date."));
                //        if (missedVisit.UserSignatureAssetId.IsEmpty())
                //        {
                //            validationRules.Add(new Validation(() => string.IsNullOrEmpty(missedVisit.Signature), "User Signature cannot be empty."));
                //            validationRules.Add(new Validation(() => missedVisit.Signature.IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, missedVisit.Signature) : false, "User Signature is not correct."));
                //        }

                //        if (missedVisit.SignatureDate.IsValid())
                //        {
                //            validationRules.Add(new Validation(() => (missedVisit.SignatureDate < scheduleEvent.StartDate), "Missed visit date must be greater or equal to the episode start date."));
                //            validationRules.Add(new Validation(() => (missedVisit.SignatureDate > scheduleEvent.EndDate), "Missed visit date must be must be  less than or equalt to the episode end date."));
                //        }

                //        var entityValidator = new EntityValidator(validationRules.ToArray());
                //        entityValidator.Validate();
                //        if (entityValidator.IsValid)
                //        {
                //            if (missedVisit.UserSignatureAssetId.IsNotEmpty())
                //            {
                //                missedVisit.SignatureText = string.Format("Signed by: {0}", Current.UserFullName);
                //            }
                //            else
                //            {
                //                missedVisit.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                //            }

                //            if (patientService.AddMissedVisit(missedVisit))
                //            {
                //                viewData.isSuccessful = true;
                //                viewData.errorMessage = "The Missed visit was successfully documented.";
                //            }
                //            else
                //            {
                //                viewData.isSuccessful = false;
                //            }
                //        }
                //        else
                //        {
                //            viewData.isSuccessful = false;
                //            viewData.errorMessage = entityValidator.Message;
                //        }
                //    }
                //    else
                //    {
                //        viewData.isSuccessful = false;
                //        viewData.errorMessage = "The visit scheduled date is in the future.";
                //    }
                //}
                //else
                //{
                //    viewData.isSuccessful = false;
                //    viewData.errorMessage = "The visit could not be found.";
                //}
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MissedVisitRestore(Guid episodeId, Guid patientId, Guid eventId)
        {
            return Json(noteService.RestoreMissedVisit(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MissedVisitPrint(Guid patientId, Guid eventId)
        {
            return View("Missed/Print", noteService.GetMissedVisitPrint(patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MissedVisitPdf(Guid patientId, Guid eventId)
        {
            var doc = new MissedVisitPdf(noteService.GetMissedVisitPrint(patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MissedVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MissedVisitBlank()
        {
            return View("Missed/Print", noteService.GetMissedVisitPrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MissedVisitPdfBlank()
        {
            var doc = new MissedVisitPdf(noteService.GetMissedVisitPrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MissedVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisitInfo(Guid id)
        {
            return PartialView("Missed/Popup", noteService.GetMissedVisitWithPatientAndUserName(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditMissedVisitInfo(Guid id)
        {
            return PartialView("Missed/Edit", noteService.GetMissedVisitForEdit(id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MissedVisitsList()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            ViewData["MissedVisitsGroupName"] = "EventDate";
            return PartialView("MissedVisits/List", noteService.GetMissedScheduledEvents(Current.AgencyId, Guid.Empty, DateTime.Now.AddDays(-89), DateTime.Today));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisitsContent(Guid branchId, DateTime startDate, DateTime endDate, string groupName, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            ViewData["MissedVisitsGroupName"] = groupName;
            return PartialView("MissedVisits/Content", noteService.GetMissedScheduledEvents(Current.AgencyId, branchId, startDate, endDate));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LVNSVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                viewData.Questions = patientvisitNote.ToDictionary();
                //var scheduledEvents =  episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); // scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString("MM/dd/yyyy") : scheduledEvent.EventDate.ToString("MM/dd/yyyy");
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;

                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId,episode,scheduledEvents);
                    if (assessment != null) viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    //}
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    // }
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
            }
            else viewData.Questions = new Dictionary<string, NotesQuestion>();
            return PartialView("Nursing/LVNSVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult LVNSVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new LVNSVisitPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=LVNSupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LVNSVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Nursing/LVNSVisitPrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult LVNSVisitPdfBlank()
        {
            var doc = new LVNSVisitPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=LVNSupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LVNSVisitBlank()
        {
            return PartialView("Nursing/LVNSVisitPrint", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HHASVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToDictionary();
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(scheduledEvent, DisciplineTasks.HHAideCarePlan, scheduledEvent.EventDate);
                    viewData.UserId = scheduledEvent.UserId;

                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("HHA/SupervisorVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHASVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("HHA/SupervisorVisitPrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult HHASVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new HHASVisitPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHASupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHASVisitBlank()
        {
            return PartialView("HHA/SupervisorVisitPrint", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult HHASVisitPdfBlank()
        {
            var doc = new HHASVisitPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHASupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        private IDictionary<string, NotesQuestion> CombineNoteQuestionsAndOasisQuestions(IDictionary<string, NotesQuestion> noteQuestions, IDictionary<string, NotesQuestion> oasisQuestions)
        {
            var questions = noteQuestions;
            if (oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)
                {
                    noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                }
                else if (!noteQuestions.ContainsKey("PrimaryDiagnosis"))
                {
                    noteQuestions.Add("PrimaryDiagnosis", oasisQuestions["PrimaryDiagnosis"]);
                }
            }

            if (oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)
                {
                    noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                }
                else if (!noteQuestions.ContainsKey("ICD9M"))
                {
                    noteQuestions.Add("ICD9M", oasisQuestions["ICD9M"]);
                }
            }

            if (oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)
                {
                    noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                }
                else if (!noteQuestions.ContainsKey("PrimaryDiagnosis1"))
                {
                    noteQuestions.Add("PrimaryDiagnosis1", oasisQuestions["PrimaryDiagnosis1"]);
                }
            }

            if (oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty())
            {
                if (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)
                {
                    noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                }
                else if (!noteQuestions.ContainsKey("ICD9M1"))
                {
                    noteQuestions.Add("ICD9M1", oasisQuestions["ICD9M1"]);
                }
            }
            if (oasisQuestions.ContainsKey("PatientDNR") && oasisQuestions["PatientDNR"] != null && oasisQuestions["PatientDNR"].Answer.IsNotNullOrEmpty())
            {
                oasisQuestions["PatientDNR"].Answer = oasisQuestions["PatientDNR"].Answer == "Yes" ? "1" : "0";
                if (noteQuestions.ContainsKey("DNR") && noteQuestions["DNR"] != null)
                {
                    noteQuestions["DNR"].Answer = oasisQuestions["PatientDNR"].Answer;
                }
                else if (!noteQuestions.ContainsKey("DNR"))
                {
                    noteQuestions.Add("DNR", oasisQuestions["PatientDNR"]);
                }
            }
            return questions;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNPsychVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                viewData.UserId = patientvisitNote.UserId;
                viewData.TypeName = patientvisitNote.NoteType;
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents =  episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToSpecificQuestionDictionary(SectionQuestionType.Diagnoses | SectionQuestionType.HomeBoundStatus);
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {

                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                    viewData.IsSupplyExist = patientvisitNote.IsSupplyExist;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    viewData.Version = patientvisitNote.Version;
                    //viewData.PreviousNotes = null;// patientService.GetPreviousSkilledNurseNotes(patientId, scheduledEvent, viewData.Version, scheduledEvent.DisciplineTask);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                        viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                    }
                    //if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                    //{
                    //    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    //    if (physician != null)
                    //    {
                    //        viewData.PhysicianId = physician.Id;
                    //        viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                    //    }
                    //}
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            
            return PartialView("Nursing/SNPsychVisit/FormRev" + (viewData.Version != 0 ? viewData.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNPsychVisitContent(Guid patientId, Guid noteId, Guid previousNoteId)
        {
            var viewData = new VisitNoteEditViewData();
            var previousNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, previousNoteId);
            if (previousNote != null)
            {
                viewData.TypeName = previousNote.NoteType;
                viewData.IsWoundCareExist = previousNote.IsWoundCare;
                viewData.IsSupplyExist = previousNote.IsSupplyExist;
                viewData.PatientId = previousNote.PatientId;

                var noteItems = previousNote.ToDictionary();

                var nameArray = new string[] { "PatientId", "EpisodeId", "EventId", "DisciplineTask", "VisitDate", "TimeIn", "TimeOut", "PreviousNotes", "Clinician", "SignatureDate", "button", "PrimaryDiagnosis", "ICD9M", "PrimaryDiagnosis1", "ICD9M1" };
                nameArray.ForEach(name =>
                {
                    noteItems.Remove(name);
                });
                viewData.Questions = noteItems;

                var currentNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
                if (currentNote != null)
                {
                    currentNote.Questions = noteItems.Values.ToList();
                    currentNote.Note = currentNote.Questions.ToXml();
                    currentNote.IsWoundCare = previousNote.IsWoundCare;
                    currentNote.WoundNote = previousNote.WoundNote;
                    currentNote.Modified = DateTime.Now;
                    if (patientRepository.UpdateVisitNote(currentNote))
                    {
                        if (scheduleRepository.UpdateScheduleTasksStatus(currentNote.AgencyId, currentNote.PatientId, currentNote.Id, ((int)ScheduleStatus.NoteSaved)))
                        {
                            Auditor.Log(currentNote.EpisodeId, currentNote.PatientId, currentNote.Id, Actions.StatusChange, ScheduleStatus.NoteSaved, DisciplineTasks.SNVPsychNurse, "Loaded Previous Note");
                        }
                        //var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, currentNote.EpisodeId, currentNote.PatientId, currentNote.Id);
                        //if (scheduleEvent != null)
                        //{
                        //    scheduleEvent.Status = ((int)ScheduleStatus.NoteSaved).ToString();
                        //    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, currentNote.Id);
                        //    if (userEvent != null)
                        //    {
                        //        userEvent.Status = scheduleEvent.Status;
                        //    }
                        //    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        //    {
                        //        if (userEvent != null)
                        //        {
                        //            userRepository.UpdateEvent(Current.AgencyId, userEvent);
                        //        }
                        //        else
                        //        {
                        //            userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit });
                        //        }
                        //        if (scheduleEvent.Status.IsInteger())
                        //        {
                        //            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, "Loaded Previous Note");
                        //        }
                        //    }
                        //}
                    }
                    viewData.Version = currentNote.Version;
                    viewData.EpisodeId = currentNote.EpisodeId;  //previousNote.EpisodeId;
                    viewData.EventId = currentNote.Id; //previousNote.Id;
                    viewData.Type = currentNote.NoteType.IsNotNullOrEmpty() ? currentNote.NoteType.Trim() : string.Empty;
                }
            }
            return PartialView("Nursing/SNPsychVisit/ContentRev" + (viewData.Version != 0 ? viewData.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNPsychVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var print = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return View(string.Format("Nursing/SNPsychVisit/PrintRev{0}", print != null && print.Data != null && print.Data.Version != 0 ? print.Data.Version : 1), print);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNPsychVisitBlank()
        {
            return View("Nursing/SNPsychVisit/PrintRev3", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult SNPsychVisitPdfBlank()
        {
            var doc = new PsychPdf(noteService.GetVisitNotePrint(), PdfDocs.Psych, 3);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SNPsychVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SNPsychVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            PsychPdf doc = null;
            if (note != null && note.Data != null && note.Data.Version > 0)
            {
                doc = new PsychPdf(note, PdfDocs.Psych, note.Data.Version);
            }
            else
            {
                doc = new PsychPdf(note, PdfDocs.Psych, 1);
            }
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SNPsychVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNPsychAssessment(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                viewData.UserId = patientvisitNote.UserId;
                viewData.TypeName = patientvisitNote.NoteType;
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); // scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToSpecificQuestionDictionary(SectionQuestionType.Diagnoses | SectionQuestionType.HomeBoundStatus);
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {

                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                    viewData.IsSupplyExist = patientvisitNote.IsSupplyExist;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    viewData.Version = patientvisitNote.Version;
                    //viewData.PreviousNotes = null;//patientService.GetPreviousSkilledNurseNotes(patientId, scheduledEvent, viewData.Version, scheduledEvent.DisciplineTask);
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //else
                //{
                //    viewData.Questions = new Dictionary<string, NotesQuestion>();
                //}
                //}
                var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                if (patientProfile != null)
                {
                    viewData.SetPateintInfoForEdit(patientProfile);
                }
                var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                if (physician != null)
                {
                    viewData.PhysicianId = physician.Id;
                    viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                }
            }
            

            return PartialView("Nursing/SNPsychAssessment/FormRev" + (viewData.Version != 0 ? viewData.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNPsychAssessmentContent(Guid patientId, Guid noteId, Guid previousNoteId)
        {
            var viewData = new VisitNoteEditViewData();
            var previousNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, previousNoteId);
            if (previousNote != null)
            {
                viewData.TypeName = previousNote.NoteType;
                viewData.IsWoundCareExist = previousNote.IsWoundCare;
                viewData.IsSupplyExist = previousNote.IsSupplyExist;
                viewData.PatientId = previousNote.PatientId;

                var noteItems = previousNote.ToDictionary();

                var nameArray = new string[] { "PatientId", "EpisodeId", "EventId", "DisciplineTask", "VisitDate", "TimeIn", "TimeOut", "PreviousNotes", "Clinician", "SignatureDate", "button", "PrimaryDiagnosis", "ICD9M", "PrimaryDiagnosis1", "ICD9M1" };
                nameArray.ForEach(name =>
                {
                    noteItems.Remove(name);
                });
                viewData.Questions = noteItems;

                var currentNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
                if (currentNote != null)
                {
                    currentNote.Questions = noteItems.Values.ToList();
                    currentNote.Note = currentNote.Questions.ToXml();
                    currentNote.IsWoundCare = previousNote.IsWoundCare;
                    currentNote.WoundNote = previousNote.WoundNote;
                    currentNote.Modified = DateTime.Now;
                    if (patientRepository.UpdateVisitNote(currentNote))
                    {
                        if (scheduleRepository.UpdateScheduleTasksStatus(Current.AgencyId, currentNote.PatientId, currentNote.Id, ((int)ScheduleStatus.NoteSaved)))
                        {
                            Auditor.Log(currentNote.EpisodeId, currentNote.PatientId, currentNote.Id, Actions.StatusChange, ScheduleStatus.NoteSaved, DisciplineTasks.SNPsychAssessment, "Loaded Previous Note");
                        }
                        //var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId,currentNote.PatientId, currentNote.Id);
                        //if (scheduleEvent != null)
                        //{
                        //    scheduleEvent.Status = ((int)ScheduleStatus.NoteSaved).ToString();


                        //    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, currentNote.Id);
                        //    if (userEvent != null)
                        //    {
                        //        userEvent.Status = scheduleEvent.Status;
                        //    }
                        //    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        //    {
                        //        if (userEvent != null)
                        //        {
                        //            userRepository.UpdateEvent(Current.AgencyId, userEvent);
                        //        }
                        //        else
                        //        {
                        //            userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit });
                        //        }
                        //        if (scheduleEvent.Status.IsInteger())
                        //        {
                        //            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, "Loaded Previous Note");
                        //        }
                        //    }
                        //}
                    }
                    viewData.Version = currentNote.Version;
                    viewData.EpisodeId = currentNote.EpisodeId;  //previousNote.EpisodeId;
                    viewData.EventId = currentNote.Id; //previousNote.Id;
                    viewData.Type = currentNote.NoteType.IsNotNullOrEmpty() ? currentNote.NoteType.Trim() : string.Empty;
                }
            }
            return PartialView("Nursing/SNPsychAssessment/ContentRev" + (viewData.Version != 0 ? viewData.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNPsychAssessmentPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return View(string.Format("Nursing/SNPsychAssessment/PrintRev{0}",viewData!=null && viewData.Data!=null && viewData.Data.Version != 0 ? viewData.Data.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SNPsychAssessmentPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            PsychAssessmentPdf doc = new PsychAssessmentPdf(viewData, PdfDocs.PsychAssessment, viewData != null && viewData.Data != null && viewData.Data.Version != 0 ? viewData.Data.Version : 1);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SNPsychAssessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNPsychAssessmentBlank()
        {
            return View("Nursing/SNPsychAssessment/PrintRev2", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult SNPsychAssessmentPdfBlank()
        {
            var doc = new PsychAssessmentPdf(noteService.GetVisitNotePrint(), PdfDocs.PsychAssessment, 2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SNPsychAssessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientvisitNote != null)
                {
                    //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                    //if (episode != null)
                    //{
                    viewData.UserId = patientvisitNote.UserId;
                    viewData.TypeName = patientvisitNote.NoteType;
                    IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                    var noteQuestions = patientvisitNote.ToDictionary();
                    //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    //if (scheduledEvents.IsNotNullOrEmpty())
                    //{
                    var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                    if (scheduledEvent != null)
                    {
                        viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                        viewData.UserId = scheduledEvent.UserId;
                        var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                        if (assessment != null)
                        {
                            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                            oasisQuestions = assessment.ToSpecificQuestionDictionary(SectionQuestionType.Diagnoses | SectionQuestionType.HomeBoundStatus);
                            viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                        }
                        if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                        {
                            if (patientvisitNote.Note != null)
                            {
                                viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                            }
                            else
                            {
                                viewData.Questions = oasisQuestions;
                            }
                        }
                        else
                        {
                            viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                        }
                        viewData.EndDate = scheduledEvent.EndDate;
                        viewData.StartDate = scheduledEvent.StartDate;
                        viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                        viewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                        viewData.IsSupplyExist = patientvisitNote.IsSupplyExist;
                        viewData.PatientId = patientvisitNote.PatientId;
                        viewData.EpisodeId = patientvisitNote.EpisodeId;
                        viewData.EventId = patientvisitNote.Id;
                        viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                        viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                        viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                        //viewData.PreviousNotes = null;// patientService.GetPreviousSkilledNurseNotes(patientId, scheduledEvent);
                        viewData.Version = patientvisitNote.Version;
                        var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                        if (patientProfile != null)
                        {
                            viewData.SetPateintInfoForEdit(patientProfile);
                        }
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                        if (physician != null)
                        {
                            viewData.PhysicianId = physician.Id;
                            viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                        }
                    }
                    else
                    {
                        viewData.Questions = noteQuestions;
                    }
                    //}
                    //else
                    //{
                    //    viewData.Questions = new Dictionary<string, NotesQuestion>();
                    //}
                    //}
                }
               
            }
            return PartialView("Nursing/SNVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNVisitContent(Guid patientId, Guid noteId, Guid previousNoteId)
        {
            var viewData = new VisitNoteEditViewData();
            var previousNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, previousNoteId);
            if (previousNote != null)
            {
                viewData.TypeName = previousNote.NoteType;
                viewData.IsWoundCareExist = previousNote.IsWoundCare;
                viewData.IsSupplyExist = previousNote.IsSupplyExist;
                viewData.PatientId = previousNote.PatientId;

                var noteItems = previousNote.ToDictionary();

                var nameArray = new string[] { "PatientId", "EpisodeId", "EventId", "DisciplineTask", "VisitDate", "TimeIn", "TimeOut", "PreviousNotes", "Clinician", "SignatureDate", "button", "PrimaryDiagnosis", "ICD9M", "PrimaryDiagnosis1", "ICD9M1" };
                nameArray.ForEach(name =>
                {
                    noteItems.Remove(name);
                });
                viewData.Questions = noteItems;

                var currentNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
                if (currentNote != null)
                {
                    currentNote.Questions = noteItems.Values.ToList();
                    currentNote.Note = currentNote.Questions.ToXml();
                    currentNote.IsWoundCare = previousNote.IsWoundCare;
                    currentNote.WoundNote = previousNote.WoundNote;
                    currentNote.Modified = DateTime.Now;
                    if (patientRepository.UpdateVisitNote(currentNote))
                    {
                        if (scheduleRepository.UpdateScheduleTasksStatus(currentNote.AgencyId, currentNote.PatientId, currentNote.Id, ((int)ScheduleStatus.NoteSaved)))
                        {
                            Auditor.Log(currentNote.EpisodeId, currentNote.PatientId, currentNote.Id, Actions.StatusChange, ScheduleStatus.NoteSaved, (Enum.IsDefined(typeof(DisciplineTasks), currentNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), currentNote.NoteType)) : DisciplineTasks.SkilledNurseVisit), "Loaded Previous Note");
                        }
                        //var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, currentNote.EpisodeId, currentNote.PatientId, currentNote.Id);
                        //if (scheduleEvent != null)
                        //{
                        //    scheduleEvent.Status = ((int)ScheduleStatus.NoteSaved).ToString();
                        //    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, currentNote.Id);
                        //    if (userEvent != null)
                        //    {
                        //        userEvent.Status = scheduleEvent.Status;
                        //    }
                        //    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        //    {
                        //        if (userEvent != null)
                        //        {
                        //            userRepository.UpdateEvent(Current.AgencyId, userEvent);
                        //        }
                        //        else
                        //        {
                        //            userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit });
                        //        }
                        //        if (scheduleEvent.Status.IsInteger())
                        //        {
                        //            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, "Loaded Previous Note");
                        //        }
                        //    }
                        //}
                    }
                    viewData.EpisodeId = currentNote.EpisodeId;  //previousNote.EpisodeId;
                    viewData.EventId = currentNote.Id; //previousNote.Id;
                    viewData.Type = currentNote.NoteType.IsNotNullOrEmpty() ? currentNote.NoteType.Trim() : string.Empty;
                }
            }
            return PartialView("Nursing/SNVisitContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Nursing/SNVisitPrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SNVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new SNVisitPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SNVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LabsPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Nursing/LabsPrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult LabsPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new LabsPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Labs_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNVisitBlank()
        {
            return View("Nursing/SNVisitPrint", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult SNVisitPdfBlank()
        {
            var doc = new SNVisitPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SNVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult WoundCarePdfBlank()
        {
            var doc = new WoundCarePdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=WoundCare_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {

                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    //var noteQuestions = patientvisitNote.ToDictionary();
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)) noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)) noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)) noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)) noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                    //viewData.Questions = noteQuestions;

                    //var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    //if (episode != null)
                    //{
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    //var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    //if (evnt != null)
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    if (scheduledEvent.EventDate.IsValid())
                    {
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(scheduledEvent, DisciplineTasks.PTEvaluation, scheduledEvent.EventDate);
                    }
                    //}
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousPTNotes(patientId, scheduledEvent, patientvisitNote.Version);
                    viewData.Version = patientvisitNote.Version;
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //else
                //{
                //    viewData.Questions = new Dictionary<string, NotesQuestion>();
                //}
                //}
            }
            return PartialView(string.Format("Therapy/PTVisit/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTVisitContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView(string.Format("Therapy/PTVisit/ContentRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTVisitBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("PTVisit"), 2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTAVisitBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("PTAVisit"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTAVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/PTVisit/PrintRev{0}",viewData!=null && viewData.Data!=null &&  viewData.Data.Version > 0 ? viewData.Data.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTSupervisoryVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToDictionary();
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    //var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    //if (episode != null && scheduledEvent.EventDate.IsValidDate())
                    //{
                    viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(scheduledEvent, DisciplineTasks.PTEvaluation, scheduledEvent.EventDate);
                    //}
                    viewData.UserId = scheduledEvent.UserId;

                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView(string.Format("Therapy/PTSupervisor/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTSupervisoryVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/PTSupervisor/PrintRev{0}", viewData != null && viewData.Data != null && viewData.Data.Version > 0 ? viewData.Data.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTSupervisoryVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapySupervisoryPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTSupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTSupervisoryVisitBlank()
        {
            var doc = new TherapySupervisoryPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTSupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTSupervisoryVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToDictionary();
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); // patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(scheduledEvent, DisciplineTasks.OTEvaluation, scheduledEvent.EventDate);
                    viewData.UserId = scheduledEvent.UserId;

                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView(string.Format("Therapy/OTSupervisor/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTSupervisoryVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/OTSupervisor/PrintRev{0}", viewData != null && viewData.Data != null && viewData.Data.Version > 0 ? viewData.Data.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTSupervisoryVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapySupervisoryPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTSupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTSupervisoryVisitBlank()
        {
            var doc = new TherapySupervisoryPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTSupVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTAVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            return this.PTVisitPdf(episodeId, patientId, eventId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTDischarge(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();

                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents =  episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                        viewData.Questions.Add("SendAsOrder", new NotesQuestion { Name = "SendAsOrder", Answer = "1" });

                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    //var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    //if (episode != null)
                    //{
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    //var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    //if (evnt != null)
                    //{
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    //}
                    if (scheduledEvent.EventDate.IsValid())
                    {
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(scheduledEvent, DisciplineTasks.PTEvaluation, scheduledEvent.EventDate);
                    }
                    //}
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousPTDischarges(patientId, scheduledEvent);
                    viewData.Version = patientvisitNote.Version;
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                // }
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);

            if (viewData.Questions.ContainsKey("PTDischargeDOB"))
            {
                viewData.Questions["PTDischargeDOB"].Answer = patient.DOB.ToShortDateString();
            }
            else
            {
                viewData.Questions.Add("PTDischargeDOB", new NotesQuestion { Name = "PTDischargeDOB", Answer = patient.DOB.ToShortDateString() });
            }

            return PartialView(string.Format("Therapy/PTDischarge/FormRev{0}", viewData.Version > 1 ? viewData.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTDischargeContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView(string.Format("Therapy/PTDischarge/ContentRev{0}", viewData.Version > 1 ? viewData.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTDischargeBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("PTDischarge"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTDischargePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var view = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/PTDischarge/PrintRev{0}",view!=null && view.Data!=null && view.Data.Version > 0 ? view.Data.Version : 1), view);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTDischargePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTEvaluation(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduleEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduleEvent.ReturnReason);
                    viewData.UserId = scheduleEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduleEvent, false);//assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    //var noteQuestions = patientvisitNote.ToDictionary();
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)) noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)) noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)) noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)) noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                    //viewData.Questions = noteQuestions;

                    viewData.EndDate = scheduleEvent.EndDate;
                    viewData.StartDate = scheduleEvent.StartDate;
                    viewData.VisitDate = scheduleEvent.VisitDate.IsValid() ? scheduleEvent.VisitDate.ToString() : scheduleEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduleEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.Version = patientvisitNote.Version;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousPTEvals(patientId, scheduleEvent);
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            if (patientvisitNote != null && patientvisitNote.NoteType.Equals("PTReassessment"))
            {
                return PartialView("Therapy/PTReassessment/FormRev1", viewData);
            }
            else
                return PartialView(string.Format("Therapy/PTEvaluation/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTPlanOfCare(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); // patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                viewData.Questions = patientvisitNote.ToDictionary();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.VisitDate = scheduledEvent.EventDate.ToString();

                    //var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    //if (episode != null)
                    //{
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    //var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    //if (evnt != null)
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    //}
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.Version = patientvisitNote.Version;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";

                    viewData.PhysicianId = patientvisitNote.PhysicianId;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }

                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            if (patientvisitNote.OrderNumber != 0)
            {
                viewData.Questions.Add("OrderNumber", new NotesQuestion { Name = "OrderNumber", Answer = patientvisitNote.OrderNumber.ToString() });
            }
            return PartialView(string.Format("Therapy/PTPOC/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTEvaluationContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Version = patientvisitNote.Version;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            if (type.Equals("PTReassessment"))
                return PartialView("Therapy/PTReassessment/ContentRev1", viewData);
            else
                return PartialView(string.Format("Therapy/PTEvaluation/ContentRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTEvaluationBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("PTEvaluation"), 4);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTReEvaluationBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("PTReEvaluation"), 3);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTReEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTMaintenanceBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("PTMaintenance"), 3);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTMaintenance_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTEvaluationPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/PTEvaluation/PrintRev{0}", viewData != null && viewData.Data != null && viewData.Data.Version > 0 ? viewData.Data.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/PTPOC/PrintRev{0}", viewData != null && viewData.Data != null && viewData.Data.Version > 0 ? viewData.Data.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTReassessmentPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var PTReassessmentNote = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView("Therapy/PTReassessment/PrintRev1", PTReassessmentNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTEvaluationPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        public FileStreamResult PTPlanOfCarePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTPlanOfCare_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTReassessmentPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTReassessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTReassessmentBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("PTReassessment"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTReassessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTReEvaluationPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTReEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTMaintenancePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PTMaintenance_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InitialSummaryOfCare(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                var noteQuestions = patientvisitNote.ToDictionary();
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                //var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (scheduledEvent != null)
                {
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    //var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    //if (evnt != null) 
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousISOC(patientId, scheduledEvent);
                    viewData.Questions = patientvisitNote.ToDictionary();
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
            }
            else viewData.Questions = new Dictionary<string, NotesQuestion>();
            return PartialView("Nursing/InitialSummaryOfCare", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult InitialSummaryOfCarePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var ISOCNote = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView("Nursing/InitialSummaryOfCarePrint", ISOCNote);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult InitialSummaryOfCarePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new InitialSummaryPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=ISOC_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTEvaluation(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);//assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    //var noteQuestions = patientvisitNote.ToDictionary();
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)) noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)) noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)) noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)) noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                    //viewData.Questions = noteQuestions;

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousOTEvals(patientId, scheduledEvent);
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            if (patientvisitNote.NoteType.Equals("OTReassessment"))
            {
                return PartialView("Therapy/OTReassessment/FormRev1", viewData);
            }
            else
            {
                //return PartialView("Therapy/OTEvaluation/FormRev3", viewData);
                return PartialView(string.Format("Therapy/OTEvaluation/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTDischarge(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
           // viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                        if (!patientvisitNote.NoteType.Equals("OTDischarge"))
                            viewData.Questions.Add("SendAsOrder", new NotesQuestion { Name = "SendAsOrder", Answer = "1" });
                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    //var noteQuestions = patientvisitNote.ToDictionary();
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)) noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)) noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)) noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)) noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                    //viewData.Questions = noteQuestions;

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousOTEvals(patientId, scheduledEvent);
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }

            return PartialView(string.Format("Therapy/OTDischarge/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NutritionalAssessmentContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Version = patientvisitNote.Version;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("Diet/NutritionalAssessment/ContentRev1", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTEvaluationContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Version = patientvisitNote.Version;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            if (type.Equals("OTReassessment"))
            {
                return PartialView("Therapy/OTReassessment/ContentRev1", viewData);
            }
            else if (type.Equals("OTDischarge"))
            {
                return PartialView(string.Format("Therapy/OTDischarge/ContentRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
            }
            else
            {
                return PartialView(string.Format("Therapy/OTEvaluation/ContentRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTEvaluationBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("OTEvaluation"), 2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTReEvaluationBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("OTReEvaluation"), 2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTReEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTDischargeBlank()
        {
            var blanknote = noteService.GetVisitNotePrint("OTDischarge");
            if (blanknote != null && blanknote.Data != null)
            {
                blanknote.Data.Version = 3;
            }
            var doc = new TherapyPdf(blanknote);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTMaintenanceBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("OTMaintenance"), 2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTMaintenance_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult COTAVisitBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("COTAVisit"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=COTAVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTEvaluationPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var OTEvaluationNote = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/OTEvaluation/PrintRev{0}", OTEvaluationNote != null && OTEvaluationNote.Data!=null && OTEvaluationNote.Data.Version > 0 ? OTEvaluationNote.Data.Version : 1), OTEvaluationNote);
            //return PartialView("Therapy/OTEvaluation/PrintRev3", OTEvaluationNote);
        }

        public ActionResult OTDischargePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var OTEvaluationNote = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/OTDischarge/PrintRev{0}", OTEvaluationNote != null && OTEvaluationNote.Data != null && OTEvaluationNote.Data.Version > 0 ? OTEvaluationNote.Data.Version : 1), OTEvaluationNote);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTReassessmentPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var OTReassessmentNote = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView("Therapy/OTReassessment/PrintRev1", OTReassessmentNote);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NutritionalAssessmentPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var NutritionalAssessmentNote = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView("Diet/NutritionalAssessment/PrintRev1", NutritionalAssessmentNote);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTReassessmentBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("OTReassessment"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTReassessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTEvaluationPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult NutritionalAssessmentPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new DieticianPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=NutritionalAssessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTReEvaluationPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTReEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTReassessmentPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTReassessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTDischargePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTMaintenancePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTMaintenance_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    //if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted)) viewData.Questions = oasisQuestions;
                    //else
                    //{
                    //    var noteQuestions = patientvisitNote.ToDictionary();
                    //    if ((oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)) noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                    //    if ((oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)) noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                    //    if ((oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)) noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                    //    if ((oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)) noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                    //    viewData.Questions = noteQuestions;
                    //}
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    if (scheduledEvent.EventDate.IsValid())
                    {
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(scheduledEvent, DisciplineTasks.OTEvaluation, scheduledEvent.EventDate);
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;// patientService.GetPreviousOTNotes(patientId, scheduledEvent);
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }

                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView(string.Format("Therapy/OTVisit/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTVisitContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Version = patientvisitNote.Version;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView(string.Format("Therapy/OTVisit/ContentRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTVisitBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("OTVisit"), 2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/OTVisit/PrintRev{0}", viewData != null && viewData.Data != null && viewData.Data.Version > 0 ? viewData.Data.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            var doc = new TherapyPdf(viewData, viewData != null && viewData.Data != null && viewData.Data.Version > 0 ? viewData.Data.Version : 1);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTPlanOfCare(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                viewData.Questions = patientvisitNote.ToDictionary();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.VisitDate = scheduledEvent.EventDate.ToString();

                    //var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    //if (episode != null)
                    //{
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    //var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    //if (evnt != null) 
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    //}
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.Version = patientvisitNote.Version;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";

                    viewData.PhysicianId = patientvisitNote.PhysicianId;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }

                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            if (patientvisitNote.OrderNumber != null)
            {
                viewData.Questions.Add("OrderNumber", new NotesQuestion { Name = "OrderNumber", Answer = patientvisitNote.OrderNumber.ToString() });
            }
            return PartialView(string.Format("Therapy/OTPOC/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        public FileStreamResult OTPlanOfCarePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OTPlanOfCare_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/OTPOC/PrintRev{0}", viewData != null && viewData.Data != null && viewData.Data.Version > 0 ? viewData.Data.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult COTAVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            return this.OTVisitPdf(episodeId, patientId, eventId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STReassessment(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);  //scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(episodeId, patientId, assessment.Id);
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;// patientService.GetPreviousSTReassessment(patientId, scheduledEvent);
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("Therapy/STReassessment/FormRev1", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STReassessmentPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView("Therapy/STReassessment/PrintRev1", note);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STReassessmentContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Version = patientvisitNote.Version;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView("Therapy/STReassessment/ContentRev1", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STReassessmentPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STReassessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STEvaluation(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();

                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousSTEvals(patientId, scheduledEvent);
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            if (patientvisitNote.NoteType.Equals("STDischarge"))
            {
                return PartialView(string.Format("Therapy/STDischarge/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
            }
            else
            {
                return PartialView(string.Format("Therapy/STEvaluation/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PlanofCareContent(string type, Guid eventId, int version)
        {
            var viewData = new VisitNoteEditViewData();
            var note = new PatientVisitNote();
            note.Questions = noteService.GetPlanOfCareQuestions(eventId, Current.AgencyId);
            viewData.Questions = note.Questions.ToDictionary();
            string url = string.Empty;
            switch (type)
            {
                case "PTPlanOfCare":
                    url = string.Format("Therapy/PTPOC/ContentRev{0}", version > 0 ? version : 1);
                    break;
                case "OTPlanOfCare":
                    url = string.Format("Therapy/OTPOC/ContentRev{0}", version > 0 ? version : 1);
                    break;
                case "STPlanOfCare":
                    url = string.Format("Therapy/STPlanOfCare/ContentRev{0}", version > 0 ? version : 1);
                    break;
            }
            return PartialView(url, viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STPlanOfCare(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                viewData.Questions = patientvisitNote.ToDictionary();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.VisitDate = scheduledEvent.EventDate.ToString();

                    //var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    //if (episode != null)
                    //{
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    //var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    //if (evnt != null) 
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    //}
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.Version = patientvisitNote.Version;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";

                    viewData.PhysicianId = patientvisitNote.PhysicianId;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }

                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            if (patientvisitNote.OrderNumber != null)
            {
                viewData.Questions.Add("OrderNumber", new NotesQuestion { Name = "OrderNumber", Answer = patientvisitNote.OrderNumber.ToString() });
            }
            return PartialView(string.Format("Therapy/STPlanOfCare/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STEvaluationContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Version = patientvisitNote.Version;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            if (viewData.Type.Equals("STDischarge"))
            {
                return PartialView(string.Format("Therapy/STDischarge/ContentRev{0}", viewData.Version > 0 ? viewData.Version : 1), viewData);
            }
            else
            {
                return PartialView(string.Format("Therapy/STEvaluation/ContentRev{0}", viewData.Version > 0 ? viewData.Version : 1), viewData);
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STEvaluationPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            if (viewData != null && viewData.Data != null && viewData.Data.Type.Equals("STDischarge"))
            {
                return PartialView(string.Format("Therapy/STDischarge/PrintRev{0}", viewData.Data.Version > 0 ? viewData.Data.Version : 1), viewData);
            }
            else
            {
                return PartialView(string.Format("Therapy/STEvaluation/PrintRev{0}",  viewData.Data.Version > 0 ? viewData.Data.Version : 1), viewData);
                //return PartialView("Therapy/STEvaluation/PrintRev2", note);
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = noteService.GetVisitNotePrint(episodeId, patientId, eventId);

            return PartialView(string.Format("Therapy/STPlanOfCare/PrintRev{0}", note != null && note.Data!=null && note.Data.Version > 0 ? note.Data.Version : 1), note);
            //return PartialView("Therapy/STEvaluation/PrintRev2", note);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STEvaluationPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            var doc = new TherapyPdf(viewData);
            var stream = doc.GetStream();
            stream.Position = 0;
            if (viewData!=null && viewData.Data!=null && viewData.Data.Type.Equals("STDischarge"))
            {
                HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            }
            else
            {
                HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            }
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STReEvaluationPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STReEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STMaintenancePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STMaintenance_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STDischargePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STPlanOfCarePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            var doc = new TherapyPdf(viewData);
            var stream = doc.GetStream();
            stream.Position = 0;

            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STPlanOfCare_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STEvaluationBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("STEvaluation"), 2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STReassessmentBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("STReassessment"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STReassessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STMaintenanceBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("STMaintenance"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STMaintenance_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STReEvaluationBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("STReEvaluation"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STReEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STDischargeBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("STDischarge"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        //public FileStreamResult STPlanOfCareBlank()
        //{
        //    var doc = new TherapyPdf(assessmentService.GetVisitNotePrint("STPlanOfCare"), 2);
        //    var stream = doc.GetStream();
        //    stream.Position = 0;
        //    HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STPlanOfCare{0}.pdf", DateTime.Now.Ticks.ToString()));
        //    return new FileStreamResult(stream, "application/pdf");
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
           // viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    //var noteQuestions = patientvisitNote.ToDictionary();
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)) noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)) noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)) noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)) noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                    //viewData.Questions = noteQuestions;

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    if (scheduledEvent.EventDate.IsValid())
                    {
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(scheduledEvent, DisciplineTasks.STEvaluation, scheduledEvent.EventDate);
                    }
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousSTNotes(patientId, scheduledEvent);
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView(string.Format("Therapy/STVisit/FormRev{0}", patientvisitNote != null && patientvisitNote.Version > 1 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STVisitContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Version = patientvisitNote.Version;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
            }
            return PartialView(string.Format("Therapy/STVisit/ContentRev{0}", patientvisitNote != null && patientvisitNote.Version > 1 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Therapy/STVisit/PrintRev{0}", viewData != null && viewData.Data != null && viewData.Data.Version > 1 ? viewData.Data.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STVisitBlank()
        {
            var doc = new TherapyPdf(noteService.GetVisitNotePrint("STVisit"), 2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=STVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MSWEvaluationAssessment(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();

                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    //var noteQuestions = patientvisitNote.ToDictionary();
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)) noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)) noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                    //if ((oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)) noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                    //if ((oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)) noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                    //viewData.Questions = noteQuestions;

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                if (patientvisitNote.Version > 1 && !viewData.Questions.ContainsKey("GenericEmergencyContact"))
                {
                    var emergencyContact = patientRepository.GetFirstEmergencyContactByPatient(Current.AgencyId, patientId);
                    if (emergencyContact != null)
                    {
                        viewData.Questions.Add("GenericEmergencyContact", new NotesQuestion { Name = "GenericEmergencyContact", Answer = emergencyContact.DisplayName });
                    }
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView(string.Format("MSW/MSWEvaluationAssessment{0}", viewData.Version > 1 ? viewData.Version.ToString() : string.Empty), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWEvaluationAssessmentPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var data = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return View(string.Format("MSW/MSWEvaluationAssessmentPrint{0}",data!=null && data.Data!=null && data.Data.Version > 1 ? data.Data.Version.ToString() : string.Empty), data);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MSWEvaluationAssessmentPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new MSWPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MSWDischargePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            return this.MSWEvaluationAssessmentPdf(episodeId, patientId, eventId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MSWAssessmentPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            return this.MSWEvaluationAssessmentPdf(episodeId, patientId, eventId);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MSWEvaluationAssessmentBlank()
        {
            var doc = new MSWPdf(noteService.GetVisitNotePrint("MSWEvaluationAssessment"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWEval_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MSWProgressNote(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);//assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousMSWProgressNotes(patientId, scheduledEvent);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("MSW/MSWProgressNote", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MSWProgressNoteContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.Version = patientvisitNote.Version;
            }
            return PartialView("MSW/MSWProgressNoteContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWProgressNotePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("MSW/MSWProgressNotePrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MSWProgressNotePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new MSWPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWProgNote_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWProgressNoteBlank()
        {
            return View("MSW/MSWProgressNotePrint", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MSWProgressNotePdfBlank()
        {
            var doc = new MSWPdf(noteService.GetVisitNotePrint("MSWProgressNote"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWProgNote_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MSWVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            var noteQuestions = patientvisitNote.ToDictionary();
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
                // }
                // }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }

            return PartialView("MSW/MSWVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("MSW/MSWVisitPrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MSWVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new MSWPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MSWVisitBlank()
        {
            return View("MSW/MSWVisitPrint", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MSWVisitPdfBlank()
        {
            var doc = new MSWPdf(noteService.GetVisitNotePrint("MSWVisit"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MSWAssessmentPdfBlank()
        {
            var doc = new MSWPdf(noteService.GetVisitNotePrint("MSWAssessment"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWAssessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult MSWDischargePdfBlank()
        {
            var doc = new MSWPdf(noteService.GetVisitNotePrint("MSWDischarge"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MSWDischarge_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TransportationNote(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("MSW/TransportationNote", noteService.GetTransportationNote(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TransportationNotePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = noteService.GetTransportationNotePrint(episodeId, patientId, eventId);
            if (note != null && note.Data != null)
            {
                var xml = new VisitNoteXml(note.Data, PdfDocs.TransportationLog);
                note.Data.PrintViewJson = xml.GetJson();
            }
            return View("MSW/TransportationNotePrint", note);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult TransportationNotePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TransportationPdf(noteService.GetTransportationNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=TransportationLog_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TransportationNoteBlank()
        {
            return View("MSW/TransportationNotePrint", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult TransportationNotePdfBlank()
        {
            var doc = new TransportationPdf(noteService.GetVisitNotePrint("DriverOrTransportationNote"));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=TransportationLog_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundCare(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();

            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToWoundCareDictionary();
                var episode = episodeRepository.GetEpisodeLean(Current.AgencyId, patientId, episodeId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Version = patientvisitNote.Version;
            }
            else
            {
                viewData.PatientId = patientId;
                viewData.EpisodeId = episodeId;
                viewData.EventId = eventId;
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("Nursing/WoundCare", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundCareSave(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Wound care could not be saved." };
            if (noteService.SaveWoundCare(formCollection, Request.Files))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your wound care flowsheet has been saved successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SixtyDaySummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
            viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";

            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null && scheduledEvent.EventDate.IsValid())
                {
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.VisitDate = scheduledEvent.EventDate.IsValid() ? scheduledEvent.EventDate.ToString() : "";//This date will be set as order date if "Send as an order" is checked
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                        if (assessment != null)
                        {
                            assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                            oasisQuestions = assessment.ToNotesQuestionDictionary();
                            viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                        }

                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(new Dictionary<string, NotesQuestion>(), oasisQuestions);
                        var vitalSigns = patientService.GetVitalSignsForSixtyDaySummary(patientId, episodeId, scheduledEvent.StartDate, scheduledEvent.EventDate);
                        if (vitalSigns != null && vitalSigns.Count > 0)
                        {
                            if (viewData.Questions != null)
                            {
                                #region BP

                                var bp = new List<string>();
                                var bpSysLow = int.MinValue;
                                var bpSysHigh = int.MaxValue;
                                var bpDiaLow = int.MinValue;
                                var bpDiaHigh = int.MaxValue;

                                var bpSitLeft = vitalSigns.Where(v => v.BPSittingLeft.IsNotNullOrEmpty() && v.BPSittingLeft.Contains("/")).Select(v => v.BPSittingLeft).ToList();
                                var bpSitRight = vitalSigns.Where(v => v.BPSittingRight.IsNotNullOrEmpty() && v.BPSittingRight.Contains("/")).Select(v => v.BPSittingRight).ToList();
                                var bpStandLeft = vitalSigns.Where(v => v.BPStandingLeft.IsNotNullOrEmpty() && v.BPStandingLeft.Contains("/")).Select(v => v.BPStandingLeft).ToList();
                                var bpStandRight = vitalSigns.Where(v => v.BPStandingRight.IsNotNullOrEmpty() && v.BPStandingRight.Contains("/")).Select(v => v.BPStandingRight).ToList();
                                var bpLyLeft = vitalSigns.Where(v => v.BPLyingLeft.IsNotNullOrEmpty() && v.BPLyingLeft.Contains("/")).Select(v => v.BPLyingLeft).ToList();
                                var bpLyRight = vitalSigns.Where(v => v.BPLyingRight.IsNotNullOrEmpty() && v.BPLyingRight.Contains("/")).Select(v => v.BPLyingRight).ToList();


                                var bpSit = vitalSigns.Where(v => v.BPSitting.IsNotNullOrEmpty()).Select(v => v.BPSitting).ToList();
                                var bpStand = vitalSigns.Where(v => v.BPStanding.IsNotNullOrEmpty()).Select(v => v.BPStanding).ToList();
                                var bpLy = vitalSigns.Where(v => v.BPLying.IsNotNullOrEmpty()).Select(v => v.BPLying).ToList();


                                if (bpSitLeft != null && bpSitLeft.Count > 0) bp.AddRange(bpSitLeft.AsEnumerable());
                                if (bpSitRight != null && bpSitRight.Count > 0) bp.AddRange(bpSitRight.AsEnumerable());
                                if (bpStandLeft != null && bpStandLeft.Count > 0) bp.AddRange(bpStandLeft.AsEnumerable());
                                if (bpStandRight != null && bpStandRight.Count > 0) bp.AddRange(bpStandRight.AsEnumerable());
                                if (bpLyLeft != null && bpLyLeft.Count > 0) bp.AddRange(bpLyLeft.AsEnumerable());
                                if (bpLyRight != null && bpLyRight.Count > 0) bp.AddRange(bpLyRight.AsEnumerable());

                                if (bpSit != null && bpSit.Count > 0) bp.AddRange(bpSit.AsEnumerable());
                                if (bpStand != null && bpStand.Count > 0) bp.AddRange(bpStand.AsEnumerable());
                                if (bpLy != null && bpLy.Count > 0) bp.AddRange(bpLy.AsEnumerable());

                                if (bp != null && bp.Count > 0)
                                {
                                    bpSysLow = bp.Select(v =>
                                    {
                                        var d = v.ToDigitsOnly();
                                        var min = d.Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (min != null && min.Length > 1)
                                        {
                                            if (min[0].IsInteger())
                                            {
                                                return min[0].ToInteger();
                                            }
                                        }
                                        return int.MinValue;
                                    }).Min();

                                    bpSysHigh = bp.Select(v =>
                                    {
                                        var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (max != null && max.Length > 1)
                                        {
                                            if (max[0].IsInteger())
                                            {
                                                return max[0].ToInteger();
                                            }
                                        }
                                        return int.MaxValue;
                                    }).Max();

                                    bpDiaLow = bp.Select(v =>
                                    {
                                        var min = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (min != null && min.Length > 1)
                                        {
                                            if (min[1].IsInteger())
                                            {
                                                return min[1].ToInteger();
                                            }
                                        }
                                        return int.MinValue;
                                    }).Min();

                                    bpDiaHigh = bp.Select(v =>
                                    {
                                        var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (max != null && max.Length > 1)
                                        {
                                            if (max[1].IsInteger())
                                            {
                                                return max[1].ToInteger();
                                            }
                                        }
                                        return int.MaxValue;
                                    }).Max();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBPMax"))
                                {
                                    viewData.Questions["VitalSignBPMax"].Answer = bpSysHigh != int.MaxValue ? bpSysHigh.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBPMax", new NotesQuestion { Name = "VitalSignBPMax", Answer = bpSysHigh != int.MaxValue ? bpSysHigh.ToString() : string.Empty });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBPMin"))
                                {
                                    viewData.Questions["VitalSignBPMin"].Answer = bpSysLow != int.MinValue ? bpSysLow.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBPMin", new NotesQuestion { Name = "VitalSignBPMin", Answer = bpSysLow != int.MinValue ? bpSysLow.ToString() : string.Empty });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBPDiaMax"))
                                {
                                    viewData.Questions["VitalSignBPDiaMax"].Answer = bpDiaHigh != int.MaxValue ? bpDiaHigh.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBPDiaMax", new NotesQuestion { Name = "VitalSignBPDiaMax", Answer = bpDiaHigh != int.MaxValue ? bpDiaHigh.ToString() : string.Empty });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBPDiaMin"))
                                {
                                    viewData.Questions["VitalSignBPDiaMin"].Answer = bpDiaLow != int.MinValue ? bpDiaLow.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBPDiaMin", new NotesQuestion { Name = "VitalSignBPDiaMin", Answer = bpDiaLow != int.MinValue ? bpDiaLow.ToString() : string.Empty });
                                }

                                #endregion

                                #region HR

                                var apicalPulseMax = int.MinValue;
                                var apicalPulseMin = int.MaxValue;
                                var apicalPulse = vitalSigns.Where(v => v.ApicalPulse.IsNotNullOrEmpty() && v.ApicalPulse.IsInteger()).Select(v => v.ApicalPulse.ToInteger()).ToList();
                                if (apicalPulse != null && apicalPulse.Count > 0)
                                {
                                    apicalPulseMax = apicalPulse.Max();
                                    apicalPulseMin = apicalPulse.Min();
                                }

                                var radialPulseMax = int.MinValue;
                                var radialPulseMin = int.MaxValue;
                                var radialPulse = vitalSigns.Where(v => v.RadialPulse.IsNotNullOrEmpty() && v.RadialPulse.IsInteger()).Select(v => v.RadialPulse.ToInteger()).ToList();
                                if (radialPulse != null && radialPulse.Count > 0)
                                {
                                    radialPulseMax = radialPulse.Max();
                                    radialPulseMin = radialPulse.Min();
                                }

                                var maxHR = Math.Max(apicalPulseMax, radialPulseMax);
                                if (viewData.Questions.ContainsKey("VitalSignHRMax"))
                                {
                                    viewData.Questions["VitalSignHRMax"].Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignHRMax", new NotesQuestion { Name = "VitalSignHRMax", Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty });
                                }

                                var minHR = Math.Min(apicalPulseMin, radialPulseMin);

                                if (viewData.Questions.ContainsKey("VitalSignHRMin"))
                                {
                                    viewData.Questions["VitalSignHRMin"].Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty;
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignHRMin", new NotesQuestion { Name = "VitalSignHRMin", Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty });
                                }
                                #endregion

                                #region Resp

                                var respMax = int.MaxValue;
                                var respMin = int.MinValue;
                                var resp = vitalSigns.Where(v => v.Resp.IsNotNullOrEmpty() && v.Resp.IsInteger()).Select(v => v.Resp.ToInteger()).ToList();
                                if (resp != null && resp.Count > 0)
                                {
                                    respMin = resp.Min();
                                    respMax = resp.Max();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignRespMax"))
                                {
                                    viewData.Questions["VitalSignRespMax"].Answer = respMax != int.MaxValue ? respMax.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignRespMax", new NotesQuestion { Name = "VitalSignRespMax", Answer = respMax != int.MaxValue ? respMax.ToString() : "" });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignRespMin"))
                                {
                                    viewData.Questions["VitalSignRespMin"].Answer = respMin != int.MinValue ? respMin.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignRespMin", new NotesQuestion { Name = "VitalSignRespMin", Answer = respMin != int.MinValue ? respMin.ToString() : "" });
                                }

                                #endregion

                                #region Temp

                                var tempMax = double.MaxValue;
                                var tempMin = double.MinValue;
                                var temp = vitalSigns.Where(v => v.Temp.IsNotNullOrEmpty() && v.Temp.ToDecimalOnly().IsDouble()).Select(v => v.Temp.ToDecimalOnly().ToDouble()).ToList();
                                bool tempHasDegrees = vitalSigns.Exists(v => v.Temp.IsNotNullOrEmpty() && v.Temp.Contains("F") || v.Temp.Contains("f"));
                                if (temp != null && temp.Count > 0)
                                {
                                    tempMax = temp.Max();
                                    tempMin = temp.Min();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignTempMax"))
                                {
                                    viewData.Questions["VitalSignTempMax"].Answer = tempMax != double.MaxValue ? tempMax.ToString() + (tempHasDegrees ? "F" : "") : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignTempMax", new NotesQuestion { Name = "VitalSignTempMax", Answer = tempMax != double.MaxValue ? tempMax.ToString() + (tempHasDegrees ? "F" : "") : "" });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignTempMin"))
                                {
                                    viewData.Questions["VitalSignTempMin"].Answer = tempMin != double.MinValue ? tempMin.ToString() + (tempHasDegrees ? "F" : "") : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignTempMin", new NotesQuestion { Name = "VitalSignTempMin", Answer = tempMin != double.MinValue ? tempMin.ToString() + (tempHasDegrees ? "F" : "") : "" });
                                }

                                #endregion

                                #region BS

                                var bsMax = int.MaxValue;
                                var bsMin = int.MinValue;
                                var bsAllMax = vitalSigns.Where(v => v.BSMax.IsNotNullOrEmpty() && v.BSMax.IsInteger() && v.BSMax.ToInteger() > 0).Select(v => v.BSMax.Replace("/", "").ToDigitsOnly().ToInteger()).ToList();
                                var bsAllMin = vitalSigns.Where(v => v.BSMin.IsNotNullOrEmpty() && v.BSMin.IsInteger() && v.BSMin.ToInteger() > 0).Select(v => v.BSMin.Replace("/", "").ToDigitsOnly().ToInteger()).ToList();
                                if (bsAllMax != null && bsAllMax.Count > 0)
                                {
                                    bsMax = bsAllMax.Max();
                                }
                                if (bsAllMin != null && bsAllMin.Count > 0)
                                {
                                    bsMin = bsAllMin.Min();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBGMax"))
                                {
                                    viewData.Questions["VitalSignBGMax"].Answer = bsMax != int.MaxValue ? bsMax.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBGMax", new NotesQuestion { Name = "VitalSignBGMax", Answer = bsMax != int.MaxValue ? bsMax.ToString() : "" });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignBGMin"))
                                {
                                    viewData.Questions["VitalSignBGMin"].Answer = bsMin != int.MinValue ? bsMin.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignBGMin", new NotesQuestion { Name = "VitalSignBGMin", Answer = bsMin != int.MinValue ? bsMin.ToString() : "" });
                                }
                                #endregion

                                #region Weight

                                var weightMax = double.MaxValue;
                                var weightMin = double.MinValue;
                                var weight = vitalSigns.Where(v => v.Weight.IsNotNullOrEmpty() && v.Weight.IsDouble()).Select(v => v.Weight.ToDouble()).ToList();
                                if (weight != null && weight.Count > 0)
                                {
                                    weightMin = weight.Min();
                                    weightMax = weight.Max();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignWeightMax"))
                                {
                                    viewData.Questions["VitalSignWeightMax"].Answer = weightMax != double.MaxValue ? weightMax.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignWeightMax", new NotesQuestion { Name = "VitalSignWeightMax", Answer = weightMax != double.MaxValue ? weightMax.ToString() : "" });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignWeightMin"))
                                {
                                    viewData.Questions["VitalSignWeightMin"].Answer = weightMin != double.MinValue ? weightMin.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignWeightMin", new NotesQuestion { Name = "VitalSignWeightMin", Answer = weightMin != double.MinValue ? weightMin.ToString() : "" });
                                }

                                #endregion

                                #region Pain

                                var painMax = int.MaxValue;
                                var painMin = int.MinValue;
                                var pain = vitalSigns.Where(v => v.PainLevel.IsNotNullOrEmpty() && v.PainLevel.IsInteger()).Select(v => v.PainLevel.ToInteger()).ToList();
                                if (pain != null && pain.Count > 0)
                                {
                                    painMin = pain.Min();
                                    painMax = pain.Max();
                                }

                                if (viewData.Questions.ContainsKey("VitalSignPainMax"))
                                {
                                    viewData.Questions["VitalSignPainMax"].Answer = painMax != int.MaxValue ? painMax.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignPainMax", new NotesQuestion { Name = "VitalSignPainMax", Answer = painMax != int.MaxValue ? painMax.ToString() : "" });
                                }

                                if (viewData.Questions.ContainsKey("VitalSignPainMin"))
                                {
                                    viewData.Questions["VitalSignPainMin"].Answer = painMin != int.MinValue ? painMin.ToString() : "";
                                }
                                else
                                {
                                    viewData.Questions.Add("VitalSignPainMin", new NotesQuestion { Name = "VitalSignPainMin", Answer = painMin != int.MinValue ? painMin.ToString() : "" });
                                }

                                #endregion
                            }

                        }
                        if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                        {
                            viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                        }

                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        viewData.Questions = noteQuestions;
                    }
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                }
                else
                {
                    viewData.Questions = new Dictionary<string, NotesQuestion>();
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }

            return PartialView("Nursing/60DaySummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SixtyDaySummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            if (viewData != null && viewData.Data != null)
            {
                var xml = new VisitNoteXml(viewData.Data, PdfDocs.SixtyDaySummary);
                viewData.Data.PrintViewJson = xml.GetJson();
            }
            return View("Nursing/60DaySummaryPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SixtyDaySummaryBlank()
        {
            return View("Nursing/60DaySummaryPrint", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SixtyDaySummaryPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            var doc = new SixtyDaySummaryPdf(viewData);
            var stream = doc.GetStream();
            stream.Position = 0;
            if (viewData != null && viewData.Data != null)
            {
                if (viewData.Data.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary)
                {
                    HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SixtyDaySummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
                }
                else if (viewData.Data.DisciplineTask == (int)DisciplineTasks.ThirtyDaySummary)
                {
                    HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=ThirtyDaySummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
                }
                else if (viewData.Data.DisciplineTask == (int)DisciplineTasks.TenDaySummary)
                {
                    HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=TenDaySummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
                }
            }
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult SixtyDaySummaryPdfBlank()
        {
            var doc = new SixtyDaySummaryPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SixtyDaySummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CoordinationOfCare(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                        GetVitalSigns(episodeId, patientId, viewData, scheduledEvent);
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                        viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                        if (viewData.Questions != null && !viewData.Questions.ContainsKey("PhysicianPhone"))
                        {
                            viewData.Questions.Add("PhysicianPhone", new NotesQuestion { Name = "PhysicianPhone", Answer = physician.PhoneWorkFormatted });
                        }
                    }
                    if (viewData.Questions != null && !viewData.Questions.ContainsKey("EmergencyContact"))
                    {
                        var contact = patientRepository.GetEmergencyContacts(Current.AgencyId, patientId).FirstOrDefault();
                        string contactName = "";
                        if (contact != null) contactName = contact.DisplayName;
                        viewData.Questions.Add("EmergencyContact", new NotesQuestion { Name = "EmergencyContact", Answer = contactName });
                    }
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            
                

            return PartialView(string.Format("Nursing/CoordinationOfCare/CoordinationOfCare{0}", viewData.Version > 1 ? viewData.Version.ToString() : string.Empty), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TransferSummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                        GetVitalSigns(episodeId, patientId, viewData, scheduledEvent);
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                    if (viewData.Questions != null && !viewData.Questions.ContainsKey("EmergencyContact"))
                    {
                        var contact = patientRepository.GetEmergencyContacts(Current.AgencyId, patientId).FirstOrDefault();
                        string contactName = "";
                        if (contact != null) contactName = contact.DisplayName;
                        viewData.Questions.Add("EmergencyContact", new NotesQuestion { Name = "EmergencyContact", Answer = contactName });
                    }
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                        viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                        if (viewData.Questions != null && !viewData.Questions.ContainsKey("PhysicianPhone"))
                        {
                            viewData.Questions.Add("PhysicianPhone", new NotesQuestion { Name = "PhysicianPhone", Answer = physician.PhoneWorkFormatted });
                        }
                    }
                }
                // }

                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }

            return PartialView(string.Format("Nursing/TransferSummary/TransferSummary{0}", viewData.Version > 1 ? viewData.Version.ToString() : string.Empty), viewData);
        }

        private void GetVitalSigns(Guid episodeId, Guid patientId, VisitNoteEditViewData viewData, ScheduleEvent scheduledEvent)
        {
            var vitalSigns = patientService.GetVitalSignsForSixtyDaySummary(patientId, episodeId, scheduledEvent.StartDate, scheduledEvent.EventDate);
            if (vitalSigns != null && vitalSigns.Count > 0)
            {
                if (viewData.Questions != null)
                {
                    #region BP

                    var bp = new List<string>();
                    var bpSysLow = int.MinValue;
                    var bpSysHigh = int.MaxValue;
                    var bpDiaLow = int.MinValue;
                    var bpDiaHigh = int.MaxValue;

                    var bpSitLeft = vitalSigns.Where(v => v.BPSittingLeft.IsNotNullOrEmpty() && v.BPSittingLeft.Contains("/")).Select(v => v.BPSittingLeft).ToList();
                    var bpSitRight = vitalSigns.Where(v => v.BPSittingRight.IsNotNullOrEmpty() && v.BPSittingRight.Contains("/")).Select(v => v.BPSittingRight).ToList();
                    var bpStandLeft = vitalSigns.Where(v => v.BPStandingLeft.IsNotNullOrEmpty() && v.BPStandingLeft.Contains("/")).Select(v => v.BPStandingLeft).ToList();
                    var bpStandRight = vitalSigns.Where(v => v.BPStandingRight.IsNotNullOrEmpty() && v.BPStandingRight.Contains("/")).Select(v => v.BPStandingRight).ToList();
                    var bpLyLeft = vitalSigns.Where(v => v.BPLyingLeft.IsNotNullOrEmpty() && v.BPLyingLeft.Contains("/")).Select(v => v.BPLyingLeft).ToList();
                    var bpLyRight = vitalSigns.Where(v => v.BPLyingRight.IsNotNullOrEmpty() && v.BPLyingRight.Contains("/")).Select(v => v.BPLyingRight).ToList();


                    var bpSit = vitalSigns.Where(v => v.BPSitting.IsNotNullOrEmpty()).Select(v => v.BPSitting).ToList();
                    var bpStand = vitalSigns.Where(v => v.BPStanding.IsNotNullOrEmpty()).Select(v => v.BPStanding).ToList();
                    var bpLy = vitalSigns.Where(v => v.BPLying.IsNotNullOrEmpty()).Select(v => v.BPLying).ToList();


                    if (bpSitLeft != null && bpSitLeft.Count > 0) bp.AddRange(bpSitLeft.AsEnumerable());
                    if (bpSitRight != null && bpSitRight.Count > 0) bp.AddRange(bpSitRight.AsEnumerable());
                    if (bpStandLeft != null && bpStandLeft.Count > 0) bp.AddRange(bpStandLeft.AsEnumerable());
                    if (bpStandRight != null && bpStandRight.Count > 0) bp.AddRange(bpStandRight.AsEnumerable());
                    if (bpLyLeft != null && bpLyLeft.Count > 0) bp.AddRange(bpLyLeft.AsEnumerable());
                    if (bpLyRight != null && bpLyRight.Count > 0) bp.AddRange(bpLyRight.AsEnumerable());

                    if (bpSit != null && bpSit.Count > 0) bp.AddRange(bpSit.AsEnumerable());
                    if (bpStand != null && bpStand.Count > 0) bp.AddRange(bpStand.AsEnumerable());
                    if (bpLy != null && bpLy.Count > 0) bp.AddRange(bpLy.AsEnumerable());

                    if (bp != null && bp.Count > 0)
                    {
                        bpSysLow = bp.Select(v =>
                        {
                            var d = v.ToDigitsOnly();
                            var min = d.Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                            if (min != null && min.Length > 1)
                            {
                                if (min[0].IsInteger())
                                {
                                    return min[0].ToInteger();
                                }
                            }
                            return int.MinValue;
                        }).Min();

                        bpSysHigh = bp.Select(v =>
                        {
                            var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                            if (max != null && max.Length > 1)
                            {
                                if (max[0].IsInteger())
                                {
                                    return max[0].ToInteger();
                                }
                            }
                            return int.MaxValue;
                        }).Max();

                        bpDiaLow = bp.Select(v =>
                        {
                            var min = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                            if (min != null && min.Length > 1)
                            {
                                if (min[1].IsInteger())
                                {
                                    return min[1].ToInteger();
                                }
                            }
                            return int.MinValue;
                        }).Min();

                        bpDiaHigh = bp.Select(v =>
                        {
                            var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                            if (max != null && max.Length > 1)
                            {
                                if (max[1].IsInteger())
                                {
                                    return max[1].ToInteger();
                                }
                            }
                            return int.MaxValue;
                        }).Max();
                    }

                    if (viewData.Questions.ContainsKey("VitalSignBPMax"))
                    {
                        viewData.Questions["VitalSignBPMax"].Answer = bpSysHigh != int.MaxValue ? bpSysHigh.ToString() : string.Empty;
                    }
                    else
                    {
                        viewData.Questions.Add("VitalSignBPMax", new NotesQuestion { Name = "VitalSignBPMax", Answer = bpSysHigh != int.MaxValue ? bpSysHigh.ToString() : string.Empty });
                    }

                    if (viewData.Questions.ContainsKey("VitalSignBPMin"))
                    {
                        viewData.Questions["VitalSignBPMin"].Answer = bpSysLow != int.MinValue ? bpSysLow.ToString() : string.Empty;
                    }
                    else
                    {
                        viewData.Questions.Add("VitalSignBPMin", new NotesQuestion { Name = "VitalSignBPMin", Answer = bpSysLow != int.MinValue ? bpSysLow.ToString() : string.Empty });
                    }

                    if (viewData.Questions.ContainsKey("VitalSignBPDiaMax"))
                    {
                        viewData.Questions["VitalSignBPDiaMax"].Answer = bpDiaHigh != int.MaxValue ? bpDiaHigh.ToString() : string.Empty;
                    }
                    else
                    {
                        viewData.Questions.Add("VitalSignBPDiaMax", new NotesQuestion { Name = "VitalSignBPDiaMax", Answer = bpDiaHigh != int.MaxValue ? bpDiaHigh.ToString() : string.Empty });
                    }

                    if (viewData.Questions.ContainsKey("VitalSignBPDiaMin"))
                    {
                        viewData.Questions["VitalSignBPDiaMin"].Answer = bpDiaLow != int.MinValue ? bpDiaLow.ToString() : string.Empty;
                    }
                    else
                    {
                        viewData.Questions.Add("VitalSignBPDiaMin", new NotesQuestion { Name = "VitalSignBPDiaMin", Answer = bpDiaLow != int.MinValue ? bpDiaLow.ToString() : string.Empty });
                    }

                    #endregion

                    #region HR
                    var apicalPulseMax = int.MinValue;
                    var apicalPulseMin = int.MaxValue;
                    var apicalPulse = vitalSigns.Where(v => v.ApicalPulse.IsNotNullOrEmpty() && v.ApicalPulse.IsInteger()).Select(v => v.ApicalPulse.ToInteger()).ToList();
                    if (apicalPulse != null && apicalPulse.Count > 0)
                    {
                        apicalPulseMax = apicalPulse.Max();
                        apicalPulseMin = apicalPulse.Min();
                    }
                    var radialPulseMax = int.MinValue;
                    var radialPulseMin = int.MaxValue;
                    var radialPulse = vitalSigns.Where(v => v.RadialPulse.IsNotNullOrEmpty() && v.RadialPulse.IsInteger()).Select(v => v.RadialPulse.ToInteger()).ToList();
                    if (radialPulse != null && radialPulse.Count > 0)
                    {
                        radialPulseMax = radialPulse.Max();
                        radialPulseMin = radialPulse.Min();
                    }
                    var maxHR = Math.Max(apicalPulseMax, radialPulseMax);
                    if (viewData.Questions.ContainsKey("VitalSignHRMax")) viewData.Questions["VitalSignHRMax"].Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty;
                    else viewData.Questions.Add("VitalSignHRMax", new NotesQuestion { Name = "VitalSignHRMax", Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty });
                    var minHR = Math.Min(apicalPulseMin, radialPulseMin);
                    if (viewData.Questions.ContainsKey("VitalSignHRMin")) viewData.Questions["VitalSignHRMin"].Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty;
                    else viewData.Questions.Add("VitalSignHRMin", new NotesQuestion { Name = "VitalSignHRMin", Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty });
                    #endregion

                    #region Resp
                    var respMax = int.MaxValue;
                    var respMin = int.MinValue;
                    var resp = vitalSigns.Where(v => v.Resp.IsNotNullOrEmpty() && v.Resp.IsInteger()).Select(v => v.Resp.ToInteger()).ToList();
                    if (resp != null && resp.Count > 0)
                    {
                        respMin = resp.Min();
                        respMax = resp.Max();
                    }
                    if (viewData.Questions.ContainsKey("VitalSignRespMax")) viewData.Questions["VitalSignRespMax"].Answer = respMax != int.MaxValue ? respMax.ToString() : "";
                    else viewData.Questions.Add("VitalSignRespMax", new NotesQuestion { Name = "VitalSignRespMax", Answer = respMax != int.MaxValue ? respMax.ToString() : "" });
                    if (viewData.Questions.ContainsKey("VitalSignRespMin")) viewData.Questions["VitalSignRespMin"].Answer = respMin != int.MinValue ? respMin.ToString() : "";
                    else viewData.Questions.Add("VitalSignRespMin", new NotesQuestion { Name = "VitalSignRespMin", Answer = respMin != int.MinValue ? respMin.ToString() : "" });
                    #endregion

                    #region Temp
                    var tempMax = double.MaxValue;
                    var tempMin = double.MinValue;
                    bool tempHasDegrees = vitalSigns.Exists(v => v.Temp.IsNotNullOrEmpty() && v.Temp.Contains("F") || v.Temp.Contains("f"));
                    var temp = vitalSigns.Where(v => v.Temp.IsNotNullOrEmpty() && v.Temp.ToDecimalOnly().IsDouble()).Select(v => v.Temp.ToDecimalOnly().ToDouble()).ToList();
                    if (temp != null && temp.Count > 0)
                    {
                        tempMax = temp.Max();
                        tempMin = temp.Min();
                    }

                    if (viewData.Questions.ContainsKey("VitalSignTempMax")) viewData.Questions["VitalSignTempMax"].Answer = tempMax != double.MaxValue ? tempMax.ToString() + (tempHasDegrees ? "F" : "") : "";
                    else viewData.Questions.Add("VitalSignTempMax", new NotesQuestion { Name = "VitalSignTempMax", Answer = tempMax != double.MaxValue ? tempMax.ToString() + (tempHasDegrees ? "F" : "") : "" });
                    if (viewData.Questions.ContainsKey("VitalSignTempMin")) viewData.Questions["VitalSignTempMin"].Answer = tempMin != double.MinValue ? tempMin.ToString() + (tempHasDegrees ? "F" : "") : "";
                    else viewData.Questions.Add("VitalSignTempMin", new NotesQuestion { Name = "VitalSignTempMin", Answer = tempMin != double.MinValue ? tempMin.ToString() + (tempHasDegrees ? "F" : "") : "" });
                    #endregion

                    #region BS
                    var bsMax = int.MaxValue;
                    var bsMin = int.MinValue;
                    var bsAllMax = vitalSigns.Where(v => v.BSMax.IsNotNullOrEmpty() && v.BSMax.IsInteger() && v.BSMax.ToInteger() > 0).Select(v => v.BSMax.Replace("/", "").ToDigitsOnly().ToInteger()).ToList();
                    var bsAllMin = vitalSigns.Where(v => v.BSMin.IsNotNullOrEmpty() && v.BSMin.IsInteger() && v.BSMin.ToInteger() > 0).Select(v => v.BSMin.Replace("/", "").ToDigitsOnly().ToInteger()).ToList();
                    if (bsAllMax != null && bsAllMax.Count > 0) bsMax = bsAllMax.Max();
                    if (bsAllMin != null && bsAllMin.Count > 0) bsMin = bsAllMin.Min();
                    if (viewData.Questions.ContainsKey("VitalSignBGMax")) viewData.Questions["VitalSignBGMax"].Answer = bsMax != int.MaxValue ? bsMax.ToString() : "";
                    else viewData.Questions.Add("VitalSignBGMax", new NotesQuestion { Name = "VitalSignBGMax", Answer = bsMax != int.MaxValue ? bsMax.ToString() : "" });
                    if (viewData.Questions.ContainsKey("VitalSignBGMin")) viewData.Questions["VitalSignBGMin"].Answer = bsMin != int.MinValue ? bsMin.ToString() : "";
                    else viewData.Questions.Add("VitalSignBGMin", new NotesQuestion { Name = "VitalSignBGMin", Answer = bsMin != int.MinValue ? bsMin.ToString() : "" });
                    #endregion

                    #region Weight
                    var weightMax = double.MaxValue;
                    var weightMin = double.MinValue;
                    var weight = vitalSigns.Where(v => v.Weight.IsNotNullOrEmpty() && v.Weight.IsDouble()).Select(v => v.Weight.ToDouble()).ToList();
                    if (weight != null && weight.Count > 0)
                    {
                        weightMin = weight.Min();
                        weightMax = weight.Max();
                    }
                    if (viewData.Questions.ContainsKey("VitalSignWeightMax")) viewData.Questions["VitalSignWeightMax"].Answer = weightMax != double.MaxValue ? weightMax.ToString() : "";
                    else viewData.Questions.Add("VitalSignWeightMax", new NotesQuestion { Name = "VitalSignWeightMax", Answer = weightMax != double.MaxValue ? weightMax.ToString() : "" });
                    if (viewData.Questions.ContainsKey("VitalSignWeightMin")) viewData.Questions["VitalSignWeightMin"].Answer = weightMin != double.MinValue ? weightMin.ToString() : "";
                    else viewData.Questions.Add("VitalSignWeightMin", new NotesQuestion { Name = "VitalSignWeightMin", Answer = weightMin != double.MinValue ? weightMin.ToString() : "" });
                    #endregion

                    #region Pain
                    var painMax = int.MaxValue;
                    var painMin = int.MinValue;
                    var pain = vitalSigns.Where(v => v.PainLevel.IsNotNullOrEmpty() && v.PainLevel.IsInteger()).Select(v => v.PainLevel.ToInteger()).ToList();
                    if (pain != null && pain.Count > 0)
                    {
                        painMin = pain.Min();
                        painMax = pain.Max();
                    }
                    if (viewData.Questions.ContainsKey("VitalSignPainMax")) viewData.Questions["VitalSignPainMax"].Answer = painMax != int.MaxValue ? painMax.ToString() : "";
                    else viewData.Questions.Add("VitalSignPainMax", new NotesQuestion { Name = "VitalSignPainMax", Answer = painMax != int.MaxValue ? painMax.ToString() : "" });
                    if (viewData.Questions.ContainsKey("VitalSignPainMin")) viewData.Questions["VitalSignPainMin"].Answer = painMin != int.MinValue ? painMin.ToString() : "";
                    else viewData.Questions.Add("VitalSignPainMin", new NotesQuestion { Name = "VitalSignPainMin", Answer = painMin != int.MinValue ? painMin.ToString() : "" });
                    #endregion
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult CoordinationOfCarePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TransferSummaryPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=CoordinationOfCare_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult TransferSummaryPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new TransferSummaryPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=TransferSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CoordinationOfCarePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return View(string.Format("Nursing/CoordinationOfCare/CoordinationOfCarePrint{0}", viewData != null && viewData.Data != null && viewData.Data.Version > 1 ? viewData.Data.Version.ToString() : string.Empty), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TransferSummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return View(string.Format("Nursing/TransferSummary/TransferSummaryPrint{0}", viewData != null && viewData.Data != null && viewData.Data.Version > 1 ? viewData.Data.Version.ToString() : string.Empty), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult CoordinationOfCarePdfBlank()
        {
            var visit = noteService.GetVisitNotePrint();
            if (visit != null && visit.Data != null)
            {
                visit.Data.Type = DisciplineTasks.CoordinationOfCare.ToString();
            }
            var doc = new TransferSummaryPdf(visit);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=CoordinationOfCare_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult TransferSummaryPdfBlank()
        {
            var viewData = noteService.GetVisitNotePrint();
            if (viewData != null && viewData.Data != null)
            {
                viewData.Data.Type = DisciplineTasks.TransferSummary.ToString();
                viewData.Data.Version = 2;
            }
            var doc = new TransferSummaryPdf(viewData);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=TransferSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CoordinationOfCareBlank()
        {
            return PartialView("Nursing/CoordinationOfCare/CoordinationOfCarePrint", noteService.GetVisitNotePrint());
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TransferSummaryBlank()
        {
            return PartialView("Nursing/TransferSummaryPrint", noteService.GetVisitNotePrint());
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HHAideVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    //viewData.PreviousNotes = null;// patientService.GetPreviousHHANotes(patientId, scheduledEvent);
                    viewData.UserId = scheduledEvent.UserId;

                    IDictionary<string, NotesQuestion> pocQuestions = null;
                    if (scheduledEvent.EventDate.IsValid())
                    {
                        var pocEvent = scheduleRepository.GetCarePlanForHHAideCarePlan(Current.AgencyId, patientId, episodeId, scheduledEvent.StartDate, scheduledEvent.EndDate, scheduledEvent.EventDate.AddDays(-1), out pocQuestions);
                        if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                        {
                            if (pocEvent != null)
                            {
                                if (pocQuestions.ContainsKey("HHAFrequency"))
                                {
                                    noteQuestions.Remove("HHAFrequency");
                                    noteQuestions.Add("HHAFrequency", pocQuestions["HHAFrequency"]);
                                }
                                if (pocQuestions.ContainsKey("PrimaryDiagnosis"))
                                {
                                    noteQuestions.Remove("PrimaryDiagnosis");
                                    noteQuestions.Add("PrimaryDiagnosis", pocQuestions["PrimaryDiagnosis"]);
                                }
                                if (pocQuestions.ContainsKey("ICD9M"))
                                {
                                    noteQuestions.Remove("ICD9M");
                                    noteQuestions.Add("ICD9M", pocQuestions["ICD9M"]);
                                }
                                if (pocQuestions.ContainsKey("PrimaryDiagnosis1"))
                                {
                                    noteQuestions.Remove("PrimaryDiagnosis1");
                                    noteQuestions.Add("PrimaryDiagnosis1", pocQuestions["PrimaryDiagnosis1"]);
                                }
                                if (pocQuestions.ContainsKey("ICD9M1"))
                                {
                                    noteQuestions.Remove("ICD9M1");
                                    noteQuestions.Add("ICD9M1", pocQuestions["ICD9M1"]);
                                }
                                if (pocQuestions.ContainsKey("DNR"))
                                {
                                    noteQuestions.Remove("DNR");
                                    noteQuestions.Add("DNR", pocQuestions["DNR"]);
                                }
                                if (pocQuestions.ContainsKey("DiastolicBPGreaterThan"))
                                {
                                    noteQuestions.Remove("DiastolicBPGreaterThan");
                                    noteQuestions.Add("DiastolicBPGreaterThan", pocQuestions["DiastolicBPGreaterThan"]);
                                }
                                if (pocQuestions.ContainsKey("SystolicBPGreaterThan"))
                                {
                                    noteQuestions.Remove("SystolicBPGreaterThan");
                                    noteQuestions.Add("SystolicBPGreaterThan", pocQuestions["SystolicBPGreaterThan"]);
                                }
                                if (pocQuestions.ContainsKey("PulseGreaterThan"))
                                {
                                    noteQuestions.Remove("PulseGreaterThan");
                                    noteQuestions.Add("PulseGreaterThan", pocQuestions["PulseGreaterThan"]);
                                }
                                if (pocQuestions.ContainsKey("RespirationGreaterThan"))
                                {
                                    noteQuestions.Remove("RespirationGreaterThan");
                                    noteQuestions.Add("RespirationGreaterThan", pocQuestions["RespirationGreaterThan"]);
                                }
                                if (pocQuestions.ContainsKey("TempGreaterThan"))
                                {
                                    noteQuestions.Remove("TempGreaterThan");
                                    noteQuestions.Add("TempGreaterThan", pocQuestions["TempGreaterThan"]);
                                }
                                if (pocQuestions.ContainsKey("WeightGreaterThan"))
                                {
                                    noteQuestions.Remove("WeightGreaterThan");
                                    noteQuestions.Add("WeightGreaterThan", pocQuestions["WeightGreaterThan"]);
                                }
                                if (pocQuestions.ContainsKey("DiastolicBPLessThan"))
                                {
                                    noteQuestions.Remove("DiastolicBPLessThan");
                                    noteQuestions.Add("DiastolicBPLessThan", pocQuestions["DiastolicBPLessThan"]);
                                }
                                if (pocQuestions.ContainsKey("SystolicBPLessThan"))
                                {
                                    noteQuestions.Remove("SystolicBPLessThan");
                                    noteQuestions.Add("SystolicBPLessThan", pocQuestions["SystolicBPLessThan"]);
                                }
                                if (pocQuestions.ContainsKey("PulseLessThan"))
                                {
                                    noteQuestions.Remove("PulseLessThan");
                                    noteQuestions.Add("PulseLessThan", pocQuestions["PulseLessThan"]);
                                }
                                if (pocQuestions.ContainsKey("RespirationLessThan"))
                                {
                                    noteQuestions.Remove("RespirationLessThan");
                                    noteQuestions.Add("RespirationLessThan", pocQuestions["RespirationLessThan"]);
                                }
                                if (pocQuestions.ContainsKey("TempLessThan"))
                                {
                                    noteQuestions.Remove("TempLessThan");
                                    noteQuestions.Add("TempLessThan", pocQuestions["TempLessThan"]);
                                }
                                if (pocQuestions.ContainsKey("WeightLessThan"))
                                {
                                    noteQuestions.Remove("WeightLessThan");
                                    noteQuestions.Add("WeightLessThan", pocQuestions["WeightLessThan"]);
                                }
                                viewData.Questions = noteQuestions;
                            }
                            else
                            {

                                var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                                if (assessment != null)
                                {
                                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                                    oasisQuestions = assessment.ToNotesQuestionDictionary();
                                }
                                else
                                {
                                    viewData.Questions = noteQuestions;
                                }
                                viewData.UserId = scheduledEvent.UserId;
                            }
                        }
                        else
                        {
                            if (pocEvent != null)
                            {
                                if (noteQuestions.ContainsKey("HHAFrequency"))
                                {
                                    noteQuestions.Remove("HHAFrequency");
                                    noteQuestions.Add("HHAFrequency", pocQuestions["HHAFrequency"]);
                                }
                                if (noteQuestions.ContainsKey("PrimaryDiagnosis"))
                                {
                                    noteQuestions.Remove("PrimaryDiagnosis");
                                    noteQuestions.Add("PrimaryDiagnosis", pocQuestions["PrimaryDiagnosis"]);
                                }
                                if (noteQuestions.ContainsKey("PrimaryDiagnosis1"))
                                {
                                    noteQuestions.Remove("PrimaryDiagnosis1");
                                    noteQuestions.Add("PrimaryDiagnosis1", pocQuestions["PrimaryDiagnosis1"]);
                                }
                            }
                            viewData.Questions = noteQuestions;
                        }
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    if (scheduledEvent.EventDate.IsValid())
                    {
                        viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(scheduledEvent, DisciplineTasks.HHAideCarePlan, scheduledEvent.EventDate);
                    }

                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                //}
                //else
                //{
                //    viewData.Questions = new Dictionary<string, NotesQuestion>();
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("HHA/VisitNote", viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HHAideVisitContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.Version = patientvisitNote.Version;
            }
            return PartialView("HHA/VisitNoteContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHAideVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("HHA/VisitNotePrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult HHAideVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new HHAVisitPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHAVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult HHAideVisitBlank()
        {
            var doc = new HHAVisitPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHAVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HomeMakerNote(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); // patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    //viewData.PreviousNotes = null;//patientService.GetPreviousHHANotes(patientId, scheduledEvent);
                    viewData.UserId = scheduledEvent.UserId;

                    viewData.Questions = patientvisitNote.ToDictionary();
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    //viewData.CarePlanOrEvalUrl = patientService.GetScheduledEventUrl(episode, DisciplineTasks.HomeMakerNote);
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("HHA/HomeMakerNote", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HomeMakerNoteContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.Version = patientvisitNote.Version;
            }
            return PartialView("HHA/HomeMakerNoteContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HomeMakerNotePrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            if (note != null && note.Data != null)
            {
                var xml = new VisitNoteXml(note.Data, PdfDocs.HomeMakerNote);
                note.Data.PrintViewJson = xml.GetJson();
            }
            return View("HHA/HomeMakerNotePrint", note);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult HomeMakerNotePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new HomeMakerNotePdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HomeMakerNote_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult HomeMakerNotePdfBlank()
        {
            var doc = new HomeMakerNotePdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=HomeMakerNote_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HHACarePlan(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                viewData.Allergies = patientService.GetAllergies(patientId);
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);//assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }
                    viewData.UserId = scheduledEvent.UserId;


                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (oasisQuestions.ContainsKey("PatientDNR") && oasisQuestions["PatientDNR"] != null)
                        {
                            oasisQuestions["PatientDNR"].Answer = oasisQuestions["PatientDNR"].Answer == "Yes" ? "1" : "0";
                            oasisQuestions.Add("DNR", oasisQuestions["PatientDNR"]);
                            oasisQuestions.Remove("PatientDNR");
                        }
                        viewData.Questions = oasisQuestions;
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousCarePlans(patientId, scheduledEvent);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            //return PartialView("HHA/CarePlan", viewData);
            return PartialView(string.Format("HHA/CarePlan{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HHACarePlanContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.Version = patientvisitNote.Version;
            }
            //return PartialView("HHA/CarePlanContent", viewData);
            return PartialView(string.Format("HHA/CarePlanContent{0}", patientvisitNote != null && patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHACarePlanPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            //return View("HHA/CarePlanPrint", assessmentService.GetVisitNotePrint(episodeId, patientId, eventId));
            return PartialView(string.Format("HHA/CarePlanPrint{0}", viewData != null && viewData.Data !=null && viewData.Data.Version > 0 ? viewData.Data.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult HHACarePlanPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            var doc = new HHACarePlanPdf(note, note.Data.Version);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHACarePlan_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult HHACarePlanPdfFromHHAVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new OasisPlanOfCareJson { isSuccessful = false, errorMessage = "No Care Plan found for this episode." };
            var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
            if (scheduledEvent != null)
            {
                IDictionary<string, NotesQuestion> pocQuestions = null;
                var poc = scheduleRepository.GetCarePlanForHHAideCarePlan(Current.AgencyId, patientId, episodeId, scheduledEvent.StartDate, scheduledEvent.EndDate, scheduledEvent.EventDate.AddDays(-1), out pocQuestions); //patientService.GetCarePlanBySelectedEpisode(patientId, episodeId, DisciplineTasks.HHAideCarePlan, scheduledEvent.EventDate.ToDateTime());
                if (poc != null)
                {
                    viewData.isSuccessful = true;
                    viewData.url = "/Schedule/HHACarePlanPdf";
                    viewData.episodeId = poc.EpisodeId;
                    viewData.patientId = poc.PatientId;
                    viewData.eventId = poc.Id;
                }
            }
            return Json(viewData);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HHACarePlanBlank()
        {
            return PartialView("HHA/CarePlanPrint", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult HHACarePlanPdfBlank()
        {
            var doc = new HHACarePlanPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HHACarePlan_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DischargeSummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.PhysicianId = patientvisitNote.PhysicianId;

                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                    viewData.StatusComment = scheduledEvent.StatusComment;
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false); //assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                        if (physician != null)
                        {
                            viewData.PhysicianId = physician.Id;
                        }
                        //if (patient != null && patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        //{
                        //    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        //    if (physician != null) viewData.PhysicianId = physician.Id;
                        //}
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("Nursing/DischargeSummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PTDischargeSummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.PhysicianId = patientvisitNote.PhysicianId;
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{

                //var scheduledEvents =  episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                    viewData.Type = DisciplineTasks.PTDischargeSummary.ToString();
                    viewData.TypeName = DisciplineTasks.PTDischargeSummary.GetDescription();
                    viewData.StatusComment = scheduledEvent.StatusComment;
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false); //assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                        if (physician != null)
                        {
                            viewData.PhysicianId = physician.Id;
                            if (viewData.Questions != null && !viewData.Questions.ContainsKey("PhysicianPhone"))
                            {
                                viewData.Questions.Add("PhysicianPhone", new NotesQuestion { Name = "PhysicianPhone", Answer = physician.PhoneWorkFormatted });
                            }
                        }
                        //if (patient != null && patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        //{
                        //    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        //    if (physician != null) viewData.PhysicianId = physician.Id;
                        //}
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if ((oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)) noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                        if ((oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)) noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                        if ((oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)) noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                        if ((oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)) noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                        viewData.Questions = noteQuestions;
                    }
                }
                //}
                //}
            }
            else viewData.Questions = new Dictionary<string, NotesQuestion>();


            return PartialView(string.Format("Therapy/PTDischargeSummary/DischargeSummary{0}", viewData.Version > 1 ? viewData.Version.ToString() : string.Empty), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNLabs(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.UserId = patientvisitNote.UserId;
                viewData.TypeName = patientvisitNote.NoteType;
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                //var scheduledEvents =  episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                   
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false); //assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            var noteQuestions = patientvisitNote.ToDictionary();
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                    viewData.IsSupplyExist = patientvisitNote.IsSupplyExist;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;// patientService.GetPreviousSkilledNurseNotes(patientId, scheduledEvent);
                    viewData.Version = patientvisitNote.Version;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        viewData.PhysicianId = physician.Id;
                        viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                    }
                    //if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                    //{
                    //    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                    //    if (physician != null)
                    //    {
                    //        viewData.PhysicianId = physician.Id;
                    //        viewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                    //    }
                    //}
                }
                //}
                //}
                //else
                //{
                //    viewData.Questions = new Dictionary<string, NotesQuestion>();
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
                
            return PartialView("Nursing/Labs", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OTDischargeSummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.PhysicianId = patientvisitNote.PhysicianId;
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{

                //var scheduledEvents =  episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.Type = DisciplineTasks.OTDischargeSummary.ToString();
                    viewData.TypeName = DisciplineTasks.OTDischargeSummary.GetDescription();
                    viewData.StatusComment = scheduledEvent.StatusComment;
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false); // assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                        if (physician != null)
                        {
                            viewData.PhysicianId = physician.Id;
                        }
                        //if (patient != null && patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        //{
                        //    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        //    if (physician != null) viewData.PhysicianId = physician.Id;
                        //}
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if ((oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)) noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                        if ((oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)) noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                        if ((oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)) noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                        if ((oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)) noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                        viewData.Questions = noteQuestions;
                    }
                }
                //}
                //}
            }
            else viewData.Questions = new Dictionary<string, NotesQuestion>();


            return PartialView("Therapy/OTDischargeSummary/DischargeSummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NutritionalAssessment(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false); //assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                    }
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {

                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.StatusComment = scheduledEvent.StatusComment;
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;// patientService.GetPreviousNutritionalAssessmentNotes(patientId, scheduledEvent);
                    viewData.Version = patientvisitNote.Version;
                    viewData.PhysicianId = patientService.GetPrimaryPhysicianId(patientId, Current.AgencyId);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                //}
                // }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("Diet/NutritionalAssessment/FormRev1", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult STDischargeSummary(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.PhysicianId = patientvisitNote.PhysicianId;
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{

                //var scheduledEvents =  episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.Type = DisciplineTasks.STDischargeSummary.ToString();
                    viewData.TypeName = DisciplineTasks.STDischargeSummary.GetDescription();
                    viewData.StatusComment = scheduledEvent.StatusComment;
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.Questions = oasisQuestions;
                        var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                        if (physician != null)
                        {
                            viewData.PhysicianId = physician.Id;
                        }
                        //if (patient != null && patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        //{
                        //    var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        //    if (physician != null) viewData.PhysicianId = physician.Id;
                        //}
                    }
                    else
                    {
                        var noteQuestions = patientvisitNote.ToDictionary();
                        if ((oasisQuestions.ContainsKey("PrimaryDiagnosis") && oasisQuestions["PrimaryDiagnosis"] != null && oasisQuestions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"] != null)) noteQuestions["PrimaryDiagnosis"].Answer = oasisQuestions["PrimaryDiagnosis"].Answer;
                        if ((oasisQuestions.ContainsKey("ICD9M") && oasisQuestions["ICD9M"] != null && oasisQuestions["ICD9M"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M") && noteQuestions["ICD9M"] != null)) noteQuestions["ICD9M"].Answer = oasisQuestions["ICD9M"].Answer;
                        if ((oasisQuestions.ContainsKey("PrimaryDiagnosis1") && oasisQuestions["PrimaryDiagnosis1"] != null && oasisQuestions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("PrimaryDiagnosis1") && noteQuestions["PrimaryDiagnosis1"] != null)) noteQuestions["PrimaryDiagnosis1"].Answer = oasisQuestions["PrimaryDiagnosis1"].Answer;
                        if ((oasisQuestions.ContainsKey("ICD9M1") && oasisQuestions["ICD9M1"] != null && oasisQuestions["ICD9M1"].Answer.IsNotNullOrEmpty()) && (noteQuestions.ContainsKey("ICD9M1") && noteQuestions["ICD9M1"] != null)) noteQuestions["ICD9M1"].Answer = oasisQuestions["ICD9M1"].Answer;
                        viewData.Questions = noteQuestions;
                    }
                }
                //}
                //}
            }
            else viewData.Questions = new Dictionary<string, NotesQuestion>();


            return PartialView("Therapy/STDischargeSummary/DischargeSummary", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult DischargeSummaryPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new DischargeSummaryPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId), PdfDocs.DischargeSummary);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SN_DischargeSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PTDischargeSummaryPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);

            var doc = new DischargeSummaryPdf(viewData,viewData!=null && viewData.Data!=null && viewData.Data.Version > 1 ? PdfDocs.PTDischargeSummary2 : PdfDocs.PTDischargeSummary);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=PT_DischargeSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OTDischargeSummaryPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new DischargeSummaryPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId), PdfDocs.OTDischargeSummary);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=OT_DischargeSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult STDischargeSummaryPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new DischargeSummaryPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId), PdfDocs.STDischargeSummary);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=ST_DischargeSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargeSummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Nursing/DischargeSummaryPrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PTDischargeSummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return View(string.Format("Therapy/PTDischargeSummary/DischargeSummaryPrint{0}",viewData!=null && viewData.Data!=null && viewData.Data.Version > 1 ? viewData.Data.Version.ToString() : string.Empty), viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OTDischargeSummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Therapy/OTDischargeSummary/DischargeSummaryPrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult STDischargeSummaryPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Therapy/STDischargeSummary/DischargeSummaryPrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult DischargeSummaryPdfBlank()
        {
            var doc = new DischargeSummaryPdf(noteService.GetVisitNotePrint(), PdfDocs.DischargeSummary);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=SN_DischargeSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PTDischargeSummaryPdfBlank()
        {
            var doc = new DischargeSummaryPdf(noteService.GetVisitNotePrint("PTDischargeSummary"), PdfDocs.PTDischargeSummary2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=PT_DischargeSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult OTDischargeSummaryPdfBlank()
        {
            var doc = new DischargeSummaryPdf(noteService.GetVisitNotePrint("OTDischargeSummary"), PdfDocs.OTDischargeSummary);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=OT_DischargeSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult STDischargeSummaryPdfBlank()
        {
            var doc = new DischargeSummaryPdf(noteService.GetVisitNotePrint("STDischargeSummary"), PdfDocs.STDischargeSummary);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=ST_DischargeSummary_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult NutritionalAssessmentPdfBlank()
        {
            var doc = new DischargeSummaryPdf(noteService.GetVisitNotePrint(), PdfDocs.NutritionalAssessment);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=NutritionalAssessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargeSummaryBlank()
        {
            return View("Nursing/DischargeSummaryPrint", noteService.GetVisitNotePrint());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditEpisode(Guid episodeId, Guid patientId)
        {
            PatientEpisode patientEpisode;
            if (patientId.IsEmpty() || episodeId.IsEmpty())
            {
                var episodeViewData = new PatientEpisode();
                var selection = new List<SelectListItem>();
                episodeViewData.AdmissionDates = selection;
                return PartialView("Episode/Edit", episodeViewData);
            }
            else
            {
                patientEpisode = patientService.GetEditEpisode(episodeId, patientId);
            }
            return PartialView("Episode/Edit", patientEpisode);
        }



        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateEpisode([Bind] PatientEpisode patientEpisode)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Episode could not be saved." };
            var patient = patientRepository.GetPatientOnly(patientEpisode.PatientId, Current.AgencyId);
            var validationRules = new List<Validation>();
            validationRules.Add(new Validation(() => !(patient != null), "Patient data is not available."));
            if (patient != null)
            {
                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientEpisode.AdmissionId);
                validationRules.Add(new Validation(() => !(admissionData != null), "Admission data is not available."));
            }
            validationRules.Add(new Validation(() => !patientEpisode.StartDate.IsValid(), "Episode start date is not valid date."));
            validationRules.Add(new Validation(() => !patientEpisode.EndDate.IsValid(), "Episode end date is not valid date."));
            validationRules.Add(new Validation(() => patientEpisode.Detail.PrimaryInsurance.IsEqual("0"), "Insurance is required."));
            if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
            {
                validationRules.Add(new Validation(() => !(patientEpisode.StartDate < patientEpisode.EndDate), "Episode start date must be less than episode end date."));
                validationRules.Add(new Validation(() => !(patientEpisode.EndDate.Subtract(patientEpisode.StartDate).Days <= 60), "Episode period can't be more than 60 days."));
            }
            if (!patientEpisode.IsActive)
            {
                validationRules.Add(new Validation(() => patientService.IsEpisodeExist(patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate, patientEpisode.Id), "Episode date is not in the valid date range."));
            }
            if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
            {
                validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate <= patientEpisode.StartDate), "Episode start date must be greater than start of care date."));
                validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate < patientEpisode.EndDate), "Episode end date must be   greater than start of care date."));
                //if (patientEpisode.Detail != null && patientEpisode.Detail.PrimaryInsurance.IsNotNullOrEmpty())
                //{
                //    if (patientEpisode.Detail.PrimaryInsurance.Equals("1") 
                //        || patientEpisode.Detail.PrimaryInsurance.Equals("2")
                //        || patientEpisode.Detail.PrimaryInsurance.Equals("3")
                //        || patientEpisode.Detail.PrimaryInsurance.Equals("4"))
                //    {
                //        var lastEpisode = patientRepository.GetPreviousEpisode(Current.AgencyId, patient.Id, patientEpisode.StartDate);
                //        Boolean doesLastEpisodeContainDischargeAssessment = false;
                //        if (lastEpisode != null)
                //        {
                //            if (lastEpisode.Schedule.IsNotNullOrEmpty())
                //            {
                //                var lastEpisodeEvents = lastEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                //                doesLastEpisodeContainDischargeAssessment = lastEpisodeEvents.ContainsAnyCompletedDischargeAssessment();
                //            }
                //            if (!lastEpisode.IsLinkedToDischarge && !doesLastEpisodeContainDischargeAssessment)
                //            {
                //                validationRules.Add(new Validation(() => (lastEpisode.EndDate.AddDays(1) != patientEpisode.StartDate), "There is a gap between the last episode's end date and the new episode's start date."));
                //            }
                //        }
                //        Boolean doesCurrentEpisodeContainDischargeAssessment = false;
                //        if(patientEpisode.Schedule.IsNotNullOrEmpty())
                //        {
                //            var currentEpisodeEvents = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                //            doesCurrentEpisodeContainDischargeAssessment = currentEpisodeEvents.ContainsAnyCompletedDischargeAssessment();
                //        }
                //        if (!patientEpisode.IsLinkedToDischarge && !doesCurrentEpisodeContainDischargeAssessment)
                //        {
                //            validationRules.Add(new Validation(() => (patientEpisode.StartDate.AddDays(59) != patientEpisode.EndDate), "Episode period must be 60 days."));
                //        }
                //    }
                //}
            }
            var entityValidator = new EntityValidator(validationRules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                patientEpisode.IsActive = !patientEpisode.IsActive;
                if (patientService.UpdateEpisode(patientEpisode))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Episode has been successfully updated.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = entityValidator.Message;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewPatientEpisode()
        {
            var episodeViewData = new NewEpisodeData();
            var selection = new List<SelectListItem>();
            episodeViewData.AdmissionDates = selection;
            return PartialView("NewEpisode", episodeViewData);
        }

        public ActionResult NewPatientEpisodeContent(Guid patientId)
        {
            var episodeViewData = new NewEpisodeData();
            if (!patientId.IsEmpty())
            {

                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var episode = episodeRepository.GetLastEpisode(Current.AgencyId, patientId);
                    if (episode != null)
                    {
                        episodeViewData.EndDate = episode.EndDate;
                        episodeViewData.StartDate = episode.StartDate;

                    }
                    else
                        episodeViewData.PatientId = patientId;
                    episodeViewData.DisplayName = patient.DisplayName;
                    episodeViewData.StartOfCareDate = patient.StartofCareDate;
                    episodeViewData.CaseManager = patient.CaseManagerId.ToString();
                    episodeViewData.PrimaryInsurance = patient.PrimaryInsurance;
                    episodeViewData.SecondaryInsurance = patient.SecondaryInsurance;
                    episodeViewData.AdmissionId = patient.AdmissionId;
                    var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        episodeViewData.PrimaryPhysician = physician.Id.ToString();
                    }
                    //if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                    //{
                    //    var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                    //    if (primaryPhysician == null)
                    //    {
                    //        primaryPhysician = patient.PhysicianContacts.First();
                    //    }
                    //    episodeViewData.PrimaryPhysician = primaryPhysician.Id.ToString();
                    //}

                    var selection = new List<SelectListItem>();
                    var admissiondates = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
                    if (admissiondates != null && admissiondates.Count > 0)
                    {
                        selection = admissiondates.Select(a => new SelectListItem { Text = a.StartOfCareDate.ToString("MM/dd/yyy"), Value = a.Id.ToString(), Selected = a.Id == episodeViewData.AdmissionId }).ToList();
                    }
                    episodeViewData.AdmissionDates = selection;
                }
            }
            return PartialView("NewEpisodeContent", episodeViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewEpisode(Guid patientId)
        {
            var episodeViewData = new NewEpisodeData();
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    episodeViewData.PatientId = patientId;
                    episodeViewData.DisplayName = patient.DisplayName;
                    episodeViewData.StartOfCareDate = patient.StartofCareDate;
                    episodeViewData.CaseManager = patient.CaseManagerId.ToString();
                    episodeViewData.PrimaryInsurance = patient.PrimaryInsurance;
                    episodeViewData.SecondaryInsurance = patient.SecondaryInsurance;
                    episodeViewData.AdmissionId = patient.AdmissionId;
                    episodeViewData.AgencyLocationId = patient.AgencyLocationId;
                    var physician = physicianRepository.GetPatientPrimaryOrFirstPhysician(Current.AgencyId, patientId);
                    if (physician != null)
                    {
                        episodeViewData.PrimaryPhysician = physician.Id.ToString();
                    }
                    //if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                    //{
                    //    var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                    //    if (primaryPhysician == null)
                    //    {
                    //        primaryPhysician = patient.PhysicianContacts.First();
                    //    }
                    //    episodeViewData.PrimaryPhysician = primaryPhysician.Id.ToString();
                    //}

                    var episode = episodeRepository.GetLastEpisode(Current.AgencyId, patientId);
                    if (episode != null)
                    {
                        episodeViewData.EndDate = episode.EndDate;
                        episodeViewData.StartDate = episode.StartDate;
                    }
                    else
                    {
                        episodeViewData.EndDate = DateTime.MinValue.Date;
                        episodeViewData.StartDate = DateTime.MinValue.Date;
                    }
                    var selection = new List<SelectListItem>();
                    var admissiondates = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
                    if (admissiondates != null && admissiondates.Count > 0)
                    {
                        selection = admissiondates.Select(a => new SelectListItem { Text = a.StartOfCareDate.ToString("MM/dd/yyy"), Value = a.Id.ToString(), Selected = a.Id == episodeViewData.AdmissionId }).ToList();
                    }
                    else
                    {
                        var admission = patientService.GetIfExitOrCreate(patientId);
                        if (admission != null)
                        {
                            selection.Add(new SelectListItem { Text = admission.StartOfCareDate.ToString("MM/dd/yyy"), Value = admission.Id.ToString(), Selected = true });
                        }
                    }
                    episodeViewData.AdmissionDates = selection;
                }
            }
            return PartialView("Episode/New", episodeViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddEpisode(PatientEpisode patientEpisode, EpisodeDetail detail)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Episode could not be saved" };
            if (patientEpisode != null && detail != null)
            {
                patientEpisode.Detail = detail;
                var patient = patientRepository.GetPatientOnly(patientEpisode.PatientId, Current.AgencyId);
                var validationRules = new List<Validation>();
                validationRules.Add(new Validation(() => !(patient != null), "Patient data is not available."));
                if (patient != null)
                {
                    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientEpisode.AdmissionId);
                    validationRules.Add(new Validation(() => !(admissionData != null), "Admission data is not available."));
                }
                validationRules.Add(new Validation(() => detail.PrimaryInsurance.IsEqual("0"), "Insurance is required."));
                validationRules.Add(new Validation(() => !patientEpisode.StartDate.IsValid(), "Episode start date is not valid date."));
                validationRules.Add(new Validation(() => !patientEpisode.EndDate.IsValid(), "Episode end date is not valid date."));
                if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
                {
                    validationRules.Add(new Validation(() => !(patientEpisode.StartDate < patientEpisode.EndDate), "Episode start date must be less than episode end date."));
                    validationRules.Add(new Validation(() => !(patientEpisode.EndDate.Subtract(patientEpisode.StartDate).Days <= 60), "Episode period can't be more than 60 days."));
                }
                validationRules.Add(new Validation(() => patientService.IsEpisodeExist(patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate), "Episode date is not in the valid date range."));
                if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
                {
                    validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate <= patientEpisode.StartDate), "Episode start date must be greater than start of care date."));
                    validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate < patientEpisode.EndDate), "Episode end date must be greater than start of care date."));
                    if (detail.PrimaryInsurance.IsNotNullOrEmpty())
                    {
                        if (detail.PrimaryInsurance.Equals("1") || detail.PrimaryInsurance.Equals("2") || detail.PrimaryInsurance.Equals("3") || detail.PrimaryInsurance.Equals("4"))
                        {
                            //validationRules.Add(new Validation(() => (patientEpisode.StartDate.AddDays(59) != patientEpisode.EndDate), "Episode period must be 60 days."));

                            //var previousEpisode = patientRepository.GetPreviousEpisode(Current.AgencyId, patient.Id, patientEpisode.StartDate);
                            //Boolean doesPreviousEpisodeContainDischargeAssessment = false;
                            //if (previousEpisode != null)
                            //{
                            //    if (previousEpisode.Schedule.IsNotNullOrEmpty())
                            //    {
                            //        var previousEpisodeEvents = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                            //        doesPreviousEpisodeContainDischargeAssessment = previousEpisodeEvents.ContainsAnyCompletedDischargeAssessment();
                            //    }
                            //    if (!previousEpisode.IsLinkedToDischarge && !doesPreviousEpisodeContainDischargeAssessment)
                            //    {
                            //        validationRules.Add(new Validation(() => (previousEpisode.EndDate.AddDays(1) != patientEpisode.StartDate), "There is a gap between the last episode's end date and the new episode's start date."));
                            //    }
                            //}
                        }
                    }
                }
                var entityValidator = new EntityValidator(validationRules.ToArray());
                entityValidator.Validate();

                patientEpisode.Modified = DateTime.MinValue;

                if (entityValidator.IsValid && patient != null)
                {
                    var workflow = new CreateEpisodeWorkflow(patient, patientEpisode);
                    if (workflow.IsCommitted)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Episode was created successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = workflow.Message;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SupplyWorksheet(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (viewData.IsNull())
            {
                viewData = new PatientVisitNote();
                viewData.AgencyId = Current.AgencyId;
                viewData.EpisodeId = episodeId;
                viewData.PatientId = patientId;
                viewData.Id = eventId;
            }
            //return PartialView("Nursing/NotesSupplyWorkSheet", patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId));
            return PartialView("Nursing/NotesSupplyWorkSheet", viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View(new GridModel(noteService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            noteService.AddNoteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(noteService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            noteService.UpdateNoteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(noteService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            noteService.DeleteNoteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(noteService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(Guid episodeId, Guid patientId, List<ScheduleEvent> tasks)
        {
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotEmpty(tasks, "tasks");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be saved. Please try again." };
            if (!episodeId.IsEmpty())
            {
                if (!patientId.IsEmpty())
                {
                    if (tasks.IsNotNullOrEmpty())
                    {
                        return Json(patientService.AddMultipleSchedule(patientId, episodeId, tasks));
                    }
                    else
                    {
                        viewData.errorMessage = "You have pick at least on task.";
                    }
                }
                else
                {
                    viewData.errorMessage = "Patient information not identified. Try again.";
                }

            }
            else
            {
                viewData.errorMessage = "Episode information not identified. Try again.";
            }
            return Json(viewData);
            //var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, epissodeId, patientId);
            //if (patientEpisode != null)
            //{
            //    var oldTasks = patientEpisode.Schedule.IsNotNullOrEmpty() ? (patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).Where(s => !s.IsDeprecated).OrderByDescending(o => o.EventDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();

            //    string licenseNotification = string.Empty;
            //    string AssessmentNotification = string.Empty;
            //    foreach (var newTask in newTasks)
            //    {
            //        if (newTask.DisciplineTask == (int)DisciplineTasks.Rap)
            //        {
            //            var rap = billingRepository.GetRap(Current.AgencyId, formCollection["patientId"].ToGuid(), formCollection["episodeId"].ToGuid());
            //            if (rap != null)
            //            {
            //                viewData.errorMessage = "RAP claim already exists. To create a new one, delete the existing first.";
            //                return Json(viewData);
            //            }
            //        }
            //        if (newTask.DisciplineTask == (int)DisciplineTasks.Final)
            //        {
            //            var final = billingRepository.GetFinal(Current.AgencyId, formCollection["PatientId"].ToGuid(), formCollection["episodeId"].ToGuid());
            //            if (final != null)
            //            {
            //                viewData.errorMessage = "Final already created. To create a new one, delete the existing first.";
            //                return Json(viewData);
            //            }
            //        }
            //        if ((newTask.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || newTask.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || newTask.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || newTask.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST))
            //        {
            //            var transfer = oldTasks.FirstOrDefault(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT) && (oe.EventDate.ToDateTime().Date < newTask.EventDate.ToDateTime().Date));
            //            ScheduleEvent roc = null;
            //            if (transfer != null)
            //            {
            //                roc = oldTasks.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date));
            //            }
            //            if (transfer != null && roc == null)
            //            {
            //                viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transferred. Please create a Resumption of Care Assessment instead.";
            //                return Json(viewData);
            //            }
            //            else if (transfer != null && roc != null && roc.EventDate.ToDateTime().Date <= transfer.EventDate.ToDateTime().Date)
            //            {
            //                viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transferred. Please create a Resumption of Care Assessment instead.";
            //                return Json(viewData);
            //            }
            //            else if (oldTasks.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST))
            //            {
            //                viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
            //                return Json(viewData);
            //            }
            //            else if (newTask.EventDate.ToDateTime().Date < patientEpisode.EndDate.AddDays(-5).Date || newTask.EventDate.ToDateTime().Date > patientEpisode.EndDate.Date)
            //            {
            //                viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
            //                return Json(viewData);
            //            }
            //        }
            //        else if ((newTask.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || newTask.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT || newTask.DisciplineTask == (int)DisciplineTasks.OASISCStartofCareOT))
            //        {
            //            if (oldTasks.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT || oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCareOT))
            //            {
            //                viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
            //                return Json(viewData);
            //            }
            //            else if (oldTasks.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargeST))
            //            {
            //            }
            //        }
            //        else if ((newTask.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || newTask.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || newTask.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT))
            //        {
            //            var roc = oldTasks.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
            //            var transfer = oldTasks.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
            //            if (roc == null)
            //            {
            //                if (transfer == null)
            //                {
            //                    viewData.errorMessage = "A Resumption of Care Assessment cannot be created before a Transfer Assessment.";
            //                    return Json(viewData);
            //                }
            //                else if (transfer != null && (transfer.EventDate.ToDateTime() > newTask.EventDate.ToDateTime()))
            //                {
            //                    viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
            //                    return Json(viewData);
            //                }
            //            }
            //            else if (roc != null)
            //            {
            //                if (transfer != null && (roc.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date))
            //                {
            //                    viewData.errorMessage = "A Resumption of Care Assessment cannot be created before a Transfer.";
            //                    return Json(viewData);
            //                }
            //            }
            //        }
            //        else if ((newTask.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || newTask.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || newTask.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT))
            //        {
            //            var transfer = oldTasks.FirstOrDefault(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
            //            ScheduleEvent roc = null;
            //            if (transfer != null)
            //            {
            //                roc = oldTasks.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime().Date > transfer.EventDate.ToDateTime().Date));
            //            }
            //            if (transfer != null && roc == null)
            //            {
            //                viewData.errorMessage = "Please create a Resumption of Care before creating another Transfer.";
            //                return Json(viewData);
            //            }
            //        }

            //        if (newTask.Discipline == "PT")
            //        {
            //            var ptEvents = oldTasks.Where(o => !o.IsMissedVisit && o.Discipline == "PT" && o.EventDate.ToDateTime() < newTask.EventDate.ToDateTime()).ToList();
            //            if (ptEvents != null)
            //            {

            //                if (ptEvents.Count() == 12)
            //                {
            //                    if (newTask.DisciplineTask != (int)DisciplineTasks.PTReassessment || newTask.DisciplineTask != (int)DisciplineTasks.PTReEvaluation)
            //                    {
            //                        AssessmentNotification = "</br><strong>Warning:</strong>" + "13th Visit PT Re-Evaluation /Re-Assess.";
            //                    }
            //                }
            //                if (ptEvents.Count() == 18)
            //                {
            //                    if (newTask.DisciplineTask != (int)DisciplineTasks.PTReassessment || newTask.DisciplineTask != (int)DisciplineTasks.PTReEvaluation)
            //                    {
            //                        AssessmentNotification = "</br><strong>Warning:</strong>" + "19th Visit PT Re-Evaluation /Re-Assess.";
            //                    }
            //                }
            //            }
            //        }
            //        else if (newTask.Discipline == "OT")
            //        {
            //            var otEvents = oldTasks.Where(o => !o.IsMissedVisit && o.Discipline == "OT" && o.EventDate.ToDateTime() < newTask.EventDate.ToDateTime()).ToList();
            //            if (otEvents != null)
            //            {

            //                if (otEvents.Count() == 12)
            //                {
            //                    if (newTask.DisciplineTask != (int)DisciplineTasks.OTReassessment || newTask.DisciplineTask != (int)DisciplineTasks.OTReEvaluation)
            //                    {
            //                        AssessmentNotification = "</br><strong>Warning:</strong>" + "13th Visit OT Re-Evaluation /Re-Assess.";
            //                    }
            //                }
            //                if (otEvents.Count() == 18)
            //                {
            //                    if (newTask.DisciplineTask != (int)DisciplineTasks.PTReassessment || newTask.DisciplineTask != (int)DisciplineTasks.PTReEvaluation)
            //                    {
            //                        AssessmentNotification = "</br><strong>Warning:</strong>" + "19th Visit OT Re-Evaluation /Re-Assess.";
            //                    }
            //                }
            //            }
            //        }
            //        else if (newTask.Discipline == "ST")
            //        {
            //            var stEvents = oldTasks.Where(o => !o.IsMissedVisit && o.Discipline == "ST" && o.EventDate.ToDateTime() < newTask.EventDate.ToDateTime()).ToList();
            //            if (stEvents != null)
            //            {

            //                if (stEvents.Count() == 12)
            //                {
            //                    if (newTask.DisciplineTask != (int)DisciplineTasks.STReassessment || newTask.DisciplineTask != (int)DisciplineTasks.STReEvaluation)
            //                    {
            //                        AssessmentNotification = "</br><strong>Warning:</strong>" + "13th Visit ST Re-Evaluation /Re-Assess.";
            //                    }
            //                }
            //                if (stEvents.Count() == 18)
            //                {
            //                    if (newTask.DisciplineTask != (int)DisciplineTasks.PTReassessment || newTask.DisciplineTask != (int)DisciplineTasks.PTReEvaluation)
            //                    {
            //                        AssessmentNotification = "</br><strong>Warning:</strong>" + "19th Visit ST Re-Evaluation /Re-Assess.";
            //                    }
            //                }
            //            }
            //        }
            //        var licenseExpirationNotification = userService.GetUserLicensesExpirationNotification(newTask.UserId, newTask.UserName);
            //        if (licenseExpirationNotification.IsNotNullOrEmpty())
            //        {
            //            licenseNotification += "</br><strong>Warning:</strong>" + licenseExpirationNotification;
            //        }
            //    }
            //    if (patientService.UpdateEpisode(formCollection["episodeId"].ToGuid(), formCollection["patientId"].ToGuid(), formCollection["Patient_Schedule"]))
            //    {
            //        viewData.isSuccessful = true;
            //        viewData.errorMessage = "Your task has been successfully scheduled." + AssessmentNotification + licenseNotification;

            //    }
            //    else
            //    {
            //        viewData.isSuccessful = false;
            //        viewData.errorMessage = "New Task(s) could not be saved.";
            //    }
            //}

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMultiple(Guid episodeId, Guid patientId, int DisciplineTask, string Discipline, Guid userId, bool IsBillable, string StartDate, string EndDate)
        {
            Check.Argument.IsNotNull(userId, "userId");
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotNegativeOrZero(DisciplineTask, "DisciplineTask");

            //var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Event is not Saved" };
            //viewData = Validate<JsonViewData>(
            //              new Validation(() => string.IsNullOrEmpty(StartDate.ToString()), ". Start Date is required."),
            //              new Validation(() => !StartDate.IsValidDate(), ". Start Date is not in the valid range."),
            //              new Validation(() => string.IsNullOrEmpty(EndDate.ToString()), ". End Date is required."),
            //              new Validation(() => !EndDate.IsValidDate(), ". End Date is not in the valid range.")
            //              );
            //if (viewData.isSuccessful)
            //{
            //    var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            //    if (StartDate.ToDateTime().Date >= EndDate.ToDateTime().Date)
            //    {
            //        viewData.errorMessage = "The start date must be greater than end date.";
            //        viewData.isSuccessful = false;
            //        return Json(viewData);
            //    }
            //    else if (StartDate.ToDateTime().Date < patientEpisode.StartDate.Date || StartDate.ToDateTime().Date > patientEpisode.EndDate.Date || EndDate.ToDateTime().Date < patientEpisode.StartDate.Date || EndDate.ToDateTime().Date > patientEpisode.EndDate.Date)
            //    {
            //        viewData.errorMessage = "The start date and end date has to be in the current episode date range.";
            //        viewData.isSuccessful = false;
            //        return Json(viewData);
            //    }
            //    else
            //    {
            //        if (patientService.UpdateEpisode(episodeId, patientId, DisciplineTask, Discipline, userId, IsBillable, StartDate.ToDateTime(), EndDate.ToDateTime()))
            //        {
            //            viewData.isSuccessful = true;
            //            viewData.errorMessage = "Events are successfully Saved.";
            //            return Json(viewData);
            //        }
            //        return Json(viewData);
            //    }
            //}
            //else
            //{
            //    return Json(viewData);
            //}
            return Json(patientService.AddMultiDateRangeScheduleTask(episodeId, patientId, DisciplineTask, userId, StartDate, EndDate));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMultiDaySchedule(Guid episodeId, Guid patientId, Guid userId, int disciplineTaskId, string visitDates)
        {
            Check.Argument.IsNotNull(userId, "userId");
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Pick the proper discipline task." };
            if (visitDates.IsNotNullOrEmpty())
            {
                var visitDateArray = visitDates.Split(',').Where(s => s.IsNotNullOrEmpty() && s.IsDate()).Select(s => s.ToDateTime()).ToList();
                if (visitDateArray != null && visitDateArray.Count > 0)
                {
                    viewData = patientService.AddMultiDaySchedule(episodeId, patientId, userId, disciplineTaskId, visitDateArray);
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Select at least one valid date from the calendar .";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Select at least one valid date from the calendar .";
            }


            //var discipline = patientService.GetDisciplineTask(disciplineTaskId);
            //if (discipline != null)
            //{
            //    var visitDateArray = visitDates.Split(',').Where(s => s.IsNotNullOrEmpty() && s.IsDate()).ToArray();
            //    if (!(discipline.IsMultiple == false && visitDateArray.Length > 1))
            //    {
            //        viewData = Validate<JsonViewData>(new Validation(() => string.IsNullOrEmpty(visitDates.ToString()), "Select at least one date from the calendar."));

            //        if (viewData.isSuccessful)
            //        {
            //            var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            //            var oldEvents = (patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).Where(s => !s.IsDeprecated).OrderByDescending(o => o.EventDate.ToDateTime()).ToList();
            //            var events = oldEvents.OrderByDescending(o => o.EventDate.ToDateTime());

            //            foreach (var date in visitDateArray)
            //            {
            //                if (date.IsDate())
            //                {
            //                    if (disciplineTaskId == (int)DisciplineTasks.Rap)
            //                    {
            //                        var rap = billingRepository.GetRap(Current.AgencyId, patientId, episodeId);
            //                        if (rap != null)
            //                        {
            //                            viewData.isSuccessful = false;
            //                            viewData.errorMessage = "Rap already created. To create a new one, delete the existing first.";
            //                            return Json(viewData);
            //                        }
            //                    }
            //                    if (disciplineTaskId == (int)DisciplineTasks.Final)
            //                    {
            //                        var final = billingRepository.GetFinal(Current.AgencyId, patientId, episodeId);
            //                        if (final != null)
            //                        {
            //                            viewData.isSuccessful = false;
            //                            viewData.errorMessage = "Final already created. To create a new one, delete the existing first.";
            //                            return Json(viewData);
            //                        }
            //                    }
            //                    if ((disciplineTaskId == (int)DisciplineTasks.OASISCRecertification || disciplineTaskId == (int)DisciplineTasks.OASISCRecertificationOT || disciplineTaskId == (int)DisciplineTasks.OASISCRecertificationPT || disciplineTaskId == (int)DisciplineTasks.OASISCRecertificationST))
            //                    {
            //                        var transfer = oldEvents.FirstOrDefault(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT) && (oe.EventDate.ToDateTime() < date.ToDateTime()));
            //                        ScheduleEvent roc = null;
            //                        if (transfer != null)
            //                        {
            //                            roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime() > transfer.EventDate.ToDateTime()));
            //                        }
            //                        if (transfer != null && roc == null)
            //                        {
            //                            viewData.isSuccessful = false;
            //                            viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
            //                            return Json(viewData);
            //                        }
            //                        else if (transfer != null && roc != null && roc.EventDate.ToDateTime() <= transfer.EventDate.ToDateTime())
            //                        {
            //                            viewData.isSuccessful = false;
            //                            viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
            //                            return Json(viewData);
            //                        }
            //                        else if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || oe.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationST))
            //                        {
            //                            viewData.isSuccessful = false;
            //                            viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
            //                            return Json(viewData);
            //                        }
            //                        else if (date.ToDateTime() < patientEpisode.EndDate.AddDays(-5) || date.ToDateTime() > patientEpisode.EndDate)
            //                        {
            //                            viewData.isSuccessful = false;
            //                            viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
            //                            return Json(viewData);
            //                        }
            //                    }
            //                    else if ((disciplineTaskId == (int)DisciplineTasks.OASISCStartofCare || disciplineTaskId == (int)DisciplineTasks.OASISCStartofCarePT || disciplineTaskId == (int)DisciplineTasks.OASISCStartofCareOT))
            //                    {
            //                        if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT || oe.DisciplineTask == (int)DisciplineTasks.OASISCStartofCareOT))
            //                        {
            //                            viewData.isSuccessful = false;
            //                            viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
            //                            return Json(viewData);
            //                        }
            //                        else if (oldEvents.Exists(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || oe.DisciplineTask == (int)DisciplineTasks.OASISCDischargeST))
            //                        {
            //                        }
            //                    }
            //                    else if ((disciplineTaskId == (int)DisciplineTasks.OASISCResumptionofCare || disciplineTaskId == (int)DisciplineTasks.OASISCResumptionofCareOT || disciplineTaskId == (int)DisciplineTasks.OASISCResumptionofCarePT))
            //                    {
            //                        var roc = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
            //                        var transfer = oldEvents.Find(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
            //                        if (roc == null)
            //                        {
            //                            if (transfer == null)
            //                            {
            //                                viewData.isSuccessful = false;
            //                                viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
            //                                return Json(viewData);
            //                            }
            //                            else if (transfer != null && (transfer.EventDate.ToDateTime() > date.ToDateTime()))
            //                            {
            //                                viewData.isSuccessful = false;
            //                                viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
            //                                return Json(viewData);
            //                            }
            //                        }
            //                        else if (roc != null)
            //                        {
            //                            if (transfer != null && (roc.EventDate.ToDateTime() > transfer.EventDate.ToDateTime()))
            //                            {
            //                                viewData.isSuccessful = false;
            //                                viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
            //                                return Json(viewData);
            //                            }
            //                        }
            //                    }
            //                    else if ((disciplineTaskId == (int)DisciplineTasks.OASISCTransfer || disciplineTaskId == (int)DisciplineTasks.OASISCTransferOT || disciplineTaskId == (int)DisciplineTasks.OASISCTransferPT))
            //                    {
            //                        var transfer = oldEvents.FirstOrDefault(oe => oe.DisciplineTask == (int)DisciplineTasks.OASISCTransfer || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCTransferPT);
            //                        ScheduleEvent roc = null;
            //                        if (transfer != null)
            //                        {
            //                            roc = oldEvents.Find(oe => (oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || oe.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT) && (oe.EventDate.ToDateTime() > transfer.EventDate.ToDateTime()));
            //                        }
            //                        if (transfer != null && roc == null)
            //                        {
            //                            viewData.isSuccessful = false;
            //                            viewData.errorMessage = "Please create a Resumption of Care before creating another Transfer.";
            //                            return Json(viewData);
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    viewData.isSuccessful = false;
            //                    viewData.errorMessage = "Date is not in the right format.";
            //                    return Json(viewData);
            //                }
            //            }

            //            if (!patientService.AddMultiDaySchedule(episodeId, patientId, userId, disciplineTaskId, visitDates))
            //            {
            //                viewData.isSuccessful = false;
            //                viewData.errorMessage = "Unable to save this page. Please try again.";
            //            }
            //            else
            //            {
            //                viewData.errorMessage = "Task(s) scheduled for user succesfully.";
            //            }
            //        }
            //    }
            //    else
            //    {
            //        viewData.isSuccessful = false;
            //        viewData.errorMessage = "Mutiple task is not allowed for this discipline.";
            //    }
            //}
            return Json(viewData);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult Get(Guid id, string discipline)
        //{
        //    Check.Argument.IsNotEmpty(id, "id");
        //    Check.Argument.IsNotNull(discipline, "discipline");
        //    return Json(patientService.GetPatientWithSchedule(id, discipline));
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult GetEpisode(Guid patientId, Guid episodeId, string discipline)
        //{
        //    Check.Argument.IsNotEmpty(patientId, "patientId");
        //    Check.Argument.IsNotEmpty(episodeId, "episodeId");
        //    Check.Argument.IsNotNull(discipline, "discipline");
        //    return Json(patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, discipline));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Notes(string button, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            //var keys = formCollection.AllKeys;
            //var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The note could not be saved." };
            //string type = formCollection["Type"];
            //if (type.IsNotNullOrEmpty())
            //{
            //    var eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
            //    var episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
            //    var patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();

            //    if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            //    {
            //        var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            //        var rules = AddNotesValidationRules(type, button, keys, episode, formCollection);
            //        if (button == "Save")
            //        {
            //            var entityValidator = new EntityValidator(rules.ToArray());
            //            entityValidator.Validate();
            //            if (entityValidator.IsValid)
            //            {
            //                string message = string.Empty;
            //                patientService.CheckTimeOverlap(formCollection, out message);
            //                if (noteService.SaveNotes(button, formCollection))
            //                {                                
            //                    viewData.isSuccessful = true;
            //                    viewData.errorMessage = "The note was successfully saved.";
            //                    viewData.warningMessage = message;
            //                }
            //                else
            //                {
            //                    viewData.isSuccessful = false;
            //                    viewData.errorMessage = "The note could not be saved.";
            //                }
            //            }
            //            else
            //            {
            //                viewData.isSuccessful = false;
            //                viewData.errorMessage = entityValidator.Message;
            //            }
            //        }
            //        else if (button == "Complete")
            //        {
            //            var entityValidator = new EntityValidator(rules.ToArray());
            //            entityValidator.Validate();
            //            if (entityValidator.IsValid)
            //            {
            //                string message = string.Empty;
            //                patientService.CheckTimeOverlap(formCollection, out message);
            //                if (noteService.SaveNotes(button, formCollection))
            //                {
            //                    viewData.isSuccessful = true;
            //                    viewData.errorMessage = "The note was successfully Submited."+message;
            //                }
            //            }
            //            else
            //            {
            //                viewData.isSuccessful = false;
            //                viewData.errorMessage = entityValidator.Message;
            //            }
            //        }
            //        else if (button == "Approve")
            //        {
            //            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            //            if (patientVisitNote != null)
            //            {
            //                if ((patientVisitNote.SignatureText.IsNullOrEmpty() || patientVisitNote.SignatureDate == DateTime.MinValue) &&
            //                    (!keys.Contains(type + "_Clinician") || !keys.Contains(type + "_SignatureDate")))
            //                {
            //                    viewData.isSuccessful = false;
            //                    viewData.errorMessage = "The note could not be approved because the Electronic Signature is missing. Please sign this note before continuing.";
            //                }
            //                else
            //                {
            //                    var entityValidator = new EntityValidator(rules.ToArray());
            //                    entityValidator.Validate();
            //                    if (entityValidator.IsValid)
            //                    {
            //                        string message = string.Empty;
            //                        patientService.CheckTimeOverlap(formCollection, out message);
            //                        if (noteService.SaveNotes(button, formCollection))
            //                        {
            //                            viewData.isSuccessful = true;
            //                            viewData.errorMessage = "The note was successfully Approved."+message;
            //                        }
            //                        else
            //                        {
            //                            return Json(viewData);
            //                        }
            //                    }
            //                    else
            //                    {
            //                        viewData.isSuccessful = false;
            //                        viewData.errorMessage = entityValidator.Message;
            //                    }
            //                }
            //            }
            //        }
            //        else if (button == "Return")
            //        {
            //            string message = string.Empty;
            //            patientService.CheckTimeOverlap(formCollection, out message);
            //            if (noteService.SaveNotes(button, formCollection))
            //            {
            //                viewData.isSuccessful = true;
            //                viewData.errorMessage = "The note was successfully returned."+message;
            //            }
            //            else
            //            {
            //                viewData.isSuccessful = false;
            //                viewData.errorMessage = "The note could not be returned.";
            //            }
            //        }
            //    }
            //}
            return Json(noteService.SaveNotes(button, formCollection));
        }

        //private List<Validation> AddNotesValidationRules(string type, string button, string[] keys, PatientEpisode episode, FormCollection formCollection)
        //{
        //    List<Validation> rules = new List<Validation>();
        //    if (keys.Contains(type + "_VisitDate"))
        //    {
        //        rules.Add(new Validation(() => !formCollection[type + "_VisitDate"].IsNotNullOrEmpty(), "Visit date can't be empty."));
        //        rules.Add(new Validation(() => !formCollection[type + "_VisitDate"].IsValidDate(), "Visit date is not valid."));
        //        rules.Add(new Validation(() => formCollection[type + "_VisitDate"].IsNotNullOrEmpty() && formCollection[type + "_VisitDate"].IsValidDate() ? !(formCollection[type + "_VisitDate"].ToDateTime().Date >= episode.StartDate.Date && formCollection[type + "_VisitDate"].ToDateTime().Date <= episode.EndDate.Date) : true, "Visit date is not in the episode range."));
        //    }

        //    rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsNotNullOrEmpty(), "Task can't be empty."));
        //    if (formCollection["DisciplineTask"].IsNotNullOrEmpty())
        //    {
        //        rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsInteger() && !Enum.IsDefined(typeof(DisciplineTasks), formCollection["DisciplineTask"].ToInteger()), "Select the right task."));
        //    }

        //    //if (keys.Contains(type + "_TimeIn") && keys.Contains(type + "_TimeOut"))
        //    //{
        //    //    if (type != DisciplineTasks.DriverOrTransportationNote.ToString())
        //    //    {
        //    //        string timeIn = formCollection[type + "_TimeIn"];
        //    //        string timeOut = formCollection[type + "_TimeOut"];
        //    //        if (timeIn.IsNotNullOrEmpty() && timeOut.IsNotNullOrEmpty())
        //    //        {
        //    //            bool valid = ((timeIn.ToDateTime().Ticks - timeOut.ToDateTime().Ticks) / 60 / 60 / 1000) >= 3;
        //    //            rules.Add(new Validation(() => valid, "Time-Out is 3 hours greater than Time-In. "));
        //    //        }
        //    //    }
        //    //}


        //    if (button == "Complete" || button == "Approve")
        //    {
        //        if (keys.Contains(type + "_Clinician"))
        //        {
        //            rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_Clinician"]), "Clinician Signature is required."));
        //            rules.Add(new Validation(() => formCollection[type + "_Clinician"].IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, formCollection[type + "_Clinician"]) : false, "User Signature is not correct."));
        //        }
        //        if (keys.Contains(type + "_TimeIn"))
        //        {
        //            if (type != DisciplineTasks.DriverOrTransportationNote.ToString())
        //            {
        //                rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_TimeIn"]), "Time-In is required. "));
        //            }
        //        }
        //        if (keys.Contains(type + "_TimeOut"))
        //        {
        //            if (type != DisciplineTasks.DriverOrTransportationNote.ToString())
        //            {
        //                rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_TimeOut"]), "Time-Out is required. "));
        //            }
        //        }
        //        if (keys.Contains(type + "_SendAsOrder"))
        //        {
        //            rules.Add(new Validation(() => formCollection[type + "_PhysicianId"].IsNullOrEmpty(), "Physician is required to send as an order."));
        //        }
        //        if (keys.Contains(type + "_SignatureDate"))
        //        {
        //            rules.Add(new Validation(() => formCollection[type + "_SignatureDate"].IsNullOrEmpty(), "Signature date cannot be empty."));
        //            rules.Add(new Validation(() => !formCollection[type + "_SignatureDate"].IsValidDate(), "Signature date is not valid."));
        //            rules.Add(new Validation(() => formCollection[type + "_SignatureDate"].IsNotNullOrEmpty() && formCollection[type + "_SignatureDate"].IsValidDate() && formCollection[type + "_VisitDate"].IsNotNullOrEmpty() && formCollection[type + "_VisitDate"].IsValidDate() ? !(formCollection[type + "_SignatureDate"].ToDateTime() >= formCollection[type + "_VisitDate"].ToDateTime() && formCollection[type + "_SignatureDate"].ToDateTime() <= episode.EndDate) : true, "Signature date is not in the valid range."));
        //        }
        //        if (type == DisciplineTasks.PTEvaluation.ToString() || type == DisciplineTasks.PTReEvaluation.ToString()
        //                        || type == DisciplineTasks.OTEvaluation.ToString() || type == DisciplineTasks.OTReEvaluation.ToString()
        //                        || type == DisciplineTasks.STEvaluation.ToString() || type == DisciplineTasks.STReEvaluation.ToString()
        //                        || type == DisciplineTasks.MSWEvaluationAssessment.ToString())
        //        {
        //            rules.Add(new Validation(() => formCollection[type + "_PhysicianId"].IsNullOrEmpty(), "Physician is required. "));
        //        }
        //        if ((type == DisciplineTasks.PTDischarge.ToString() || type == DisciplineTasks.SixtyDaySummary.ToString() || type == DisciplineTasks.SNPsychAssessment.ToString()) && keys.Contains(type + "_SendAsOrder"))
        //        {
        //            rules.Add(new Validation(() => formCollection[type + "_PhysicianId"].IsNullOrEmpty(), "Physician is required. "));
        //        }
        //    }
        //    return rules;
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your note could not be saved." };
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                if (button == "Approve")
                {
                    if (noteService.ProcessNotes(button, episodeId, patientId, eventId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your note has been successfully approved.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your note could not be approved.";
                    }
                }
                else if (button == "Return")
                {
                    if (noteService.ProcessNotes(button, episodeId, patientId, eventId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your note has been successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your note could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }
        //TODO: need optimization 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BulkUpdate(List<string> CustomValue, string CommandType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The item(s) you selected could not be updated." };
            if (CustomValue != null && CustomValue.Count > 0 && CommandType.IsNotNullOrEmpty())
            {
                int total = CustomValue.Count;
                int count = 0;
                CustomValue.ForEach(v =>
                {
                    var infos = v.Split('|');
                    if (infos.Length == 4 && infos[0].IsGuid() && infos[1].IsGuid() && infos[2].IsGuid() && infos[3].IsInteger())
                    {//infos[0].ToGuid(),
                        var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, infos[1].ToGuid(), infos[2].ToGuid());
                        if (scheduleEvent != null && scheduleEvent.DisciplineTask == infos[3].ToInteger())
                        {
                            if (scheduleEvent.IsMissedVisit == true)
                            {
                                if (CommandType == "Print")
                                {
                                    scheduleEvent.InPrintQueue = false;
                                    if (scheduleRepository.UpdateScheduleTaskModal(scheduleEvent)) count++;

                                }
                                else if (noteService.ProcessMissedVisitNotes(CommandType, scheduleEvent.Id))
                                {
                                    count++;
                                }
                            }
                            else
                            {
                                var eventType = scheduleEvent.TypeOfEvent();
                                if (eventType.IsNotNullOrEmpty())
                                {
                                    switch (eventType)
                                    {
                                        case "Notes":
                                            if (noteService.ProcessNotes(CommandType, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id)) count++;
                                            break;
                                        case "OASIS":
                                            if (Enum.IsDefined(typeof(DisciplineTasks), infos[3].ToInteger()))
                                            {
                                                var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), infos[3].ToInteger())).ToString();
                                                if (CommandType == "Approve")
                                                {
                                                    if (assessmentService.UpdateAssessmentStatus(scheduleEvent.Id, scheduleEvent.PatientId, scheduleEvent.EpisodeId, assessmentType, ((int)ScheduleStatus.OasisCompletedExportReady)))
                                                    {
                                                        if ((assessmentType == DisciplineTasks.OASISCDischargeOT.ToString()
                                                            || assessmentType == DisciplineTasks.OASISCDischarge.ToString()
                                                            || assessmentType == DisciplineTasks.OASISCDischargePT.ToString()
                                                            || assessmentType == DisciplineTasks.OASISCDischargeST.ToString()
                                                            || assessmentType == "DischargeFromAgency")
                                                            || (assessmentType == DisciplineTasks.OASISCTransferDischargePT.ToString()
                                                            || assessmentType == "OASISCTransferDischargePT")
                                                            || (assessmentType == DisciplineTasks.OASISCTransferDischarge.ToString()
                                                            || assessmentType == "OASISCTransferDischarge")
                                                            || (assessmentType == DisciplineTasks.OASISCDeath.ToString()
                                                            || assessmentType == DisciplineTasks.OASISCDeathOT.ToString()
                                                            || assessmentType == DisciplineTasks.OASISCDeathPT.ToString()
                                                            || assessmentType == "OASISCDeath"))
                                                        {
                                                            var assessment = assessmentService.GetAssessment(scheduleEvent.Id, assessmentType);
                                                            if (assessment != null)
                                                            {
                                                                var assessmentData = assessment.ToDictionary();
                                                                var schedule = scheduleRepository.GetScheduleTask(Current.AgencyId,scheduleEvent.PatientId, scheduleEvent.Id);
                                                                if (schedule != null)
                                                                {
                                                                    var date = DateTime.MinValue;
                                                                    var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                                                                    if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate()) date = dateAssessment.ToDateTime();
                                                                    var eventDateSchedule = schedule.EventDate;
                                                                    if (eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                                                    var visitDateSchedule = schedule.VisitDate;
                                                                    if (visitDateSchedule.IsValid()) date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                                                    if (date > DateTime.MinValue) patientService.DischargePatient(scheduleEvent.PatientId, scheduleEvent.EpisodeId, date, "Patient discharged due to discharge oasis.");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                var schedule = scheduleRepository.GetScheduleTask(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.Id);
                                                                if (schedule != null)
                                                                {
                                                                    var date = DateTime.MinValue;
                                                                    var eventDateSchedule = schedule.EventDate;
                                                                    if (eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                                                    var visitDateSchedule = schedule.VisitDate;
                                                                    if (visitDateSchedule.IsValid()) date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                                                    if (date > DateTime.MinValue) patientService.DischargePatient(scheduleEvent.PatientId, schedule.EpisodeId, date, "Patient discharged due to discharge oasis.");
                                                                }
                                                            }
                                                        }
                                                        count++;
                                                    }
                                                }
                                                else if (CommandType == "Return")
                                                {
                                                    if (assessmentService.UpdateAssessmentStatus(scheduleEvent.Id, scheduleEvent.PatientId, scheduleEvent.EpisodeId, assessmentType, ((int)ScheduleStatus.OasisReturnedForClinicianReview))) count++;
                                                }
                                                else if (CommandType == "Print")
                                                {
                                                    scheduleEvent.InPrintQueue = false;
                                                    if (scheduleRepository.UpdateScheduleTaskModal(scheduleEvent)) count++;
                                                }
                                            }
                                            break;
                                        case "PhysicianOrder":
                                            if (orderManagementService.ProcessPhysicianOrder(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, CommandType)) count++;
                                            break;
                                        case "PlanOfCare":
                                            if (assessmentService.UpdatePlanofCareStatus(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, CommandType)) count++;
                                            break;
                                        case "IncidentAccident":
                                            if (agencyService.ProcessIncidents(CommandType, scheduleEvent.PatientId, scheduleEvent.Id)) count++;
                                            break;
                                        case "Infection":
                                            if (agencyService.ProcessInfections(CommandType, scheduleEvent.PatientId, scheduleEvent.Id)) count++;
                                            break;
                                        case "CommunicationNote":
                                            if (patientService.ProcessCommunicationNotes(CommandType, scheduleEvent.PatientId, scheduleEvent.Id)) count++;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                });
                viewData.isSuccessful = true;
                if (count == total)
                {
                    viewData.errorMessage = string.Format("All ({0}) items were updated successfully.", count);
                }
                else if (count < total)
                {
                    viewData.errorMessage = string.Format("{0} out of {1} items were updated successfully. Try them again.", count, total);
                }

            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Wound care asset could not be deleted." };
            if (noteService.DeleteWoundCareAsset(episodeId, patientId, eventId, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Wound care asset successfully deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteScheduleEventAsset(Guid patientId, Guid episodeId, Guid eventId, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Asset Not Deleted." };
            if (patientService.DeleteScheduleEventAsset(episodeId, patientId, eventId, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Asset Successfully Deleted.";
            }
            return Json(viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Frequencies(Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            FrequenciesViewData viewData = patientService.GetPatientEpisodeFrequencyData(episodeId, patientId);
            return PartialView("Frequencies", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignModal(Guid patientId, Guid eventId)
        {
            var viewData = patientService.GetReassignViewData(patientId,eventId );
            return PartialView("Reassign", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReAssign(Guid episodeId, Guid patientId, Guid eventId, Guid oldUserId, Guid userId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(userId, "userId");
            var viewData = patientService.Reassign(episodeId, patientId, eventId, userId);// new JsonViewData { isSuccessful = false, errorMessage = "This task could not be reassigned to another user" };
            //if (patientService.Reassign(episodeId, patientId, eventId, oldUserId, userId))
            //{
            //    viewData.isSuccessful = true;
            //    viewData.errorMessage = "This task has been reassigned sucessfully.";
            //}
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReAssignSchedules()
        {
            return PartialView("MultiReassign", new ReassignViewData { EpisodeId = Guid.Empty, PatientId = Guid.Empty, Type = "All" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReAssignSchedulesContent(Guid episodeId, Guid patientId, string type)
        {
            return PartialView("MultiReassign", patientService.GetReassignViewData(patientId, episodeId, type));
        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReAssignPatientSchedules(Guid PatientId, Guid EmployeeOldId, Guid EmployeeId, DateTime StartDate, DateTime EndDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Events could not be reassigned." };
            if (!PatientId.IsEmpty() && !EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    viewData = patientService.ReassignSchedules(PatientId, EmployeeOldId, EmployeeId, StartDate, EndDate);
                    //{
                    //    viewData.isSuccessful = true;
                    //    viewData.errorMessage = "Events are reassigned sucessfully.";
                    //}
                    //else
                    //{
                    //    viewData.isSuccessful = false;
                    //    viewData.errorMessage = "Events are not reassigned. Try again.";
                    //}
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee has to be different from the previous one.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignEpisodeSchedules(Guid PatientId, Guid EmployeeOldId, Guid EmployeeId, DateTime StartDate, DateTime EndDate)
        {
            JsonViewData viewData = new JsonViewData { isSuccessful = false, errorMessage = "Events is not reassigned." };
            if (!PatientId.IsEmpty() && !EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    viewData = patientService.ReassignSchedules(PatientId, EmployeeOldId, EmployeeId, StartDate, EndDate);
                    //{
                    //    viewData.isSuccessful = true;
                    //    viewData.errorMessage = "Events are reassigned sucessfully.";
                    //}
                    //else
                    //{
                    //    viewData.isSuccessful = false;
                    //    viewData.errorMessage = "Events are not reassigned. Try again.";
                    //}
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee has to be different from the previous one.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignAllSchedules(Guid EmployeeOldId, Guid EmployeeId, DateTime StartDate, DateTime EndDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Events is not reassigned." };
            if (!EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    viewData = patientService.ReassignSchedules(EmployeeOldId, EmployeeId, StartDate, EndDate);
                    //if (patientService.ReassignSchedules(EmployeeOldId, EmployeeId, StartDate, EndDate))
                    //{
                    //    viewData.isSuccessful = true;
                    //    viewData.errorMessage = "Events are reassigned sucessfully.";
                    //}
                    //else
                    //{
                    //    viewData.isSuccessful = false;
                    //    viewData.errorMessage = "Events are not reassigned. Try again.";
                    //}
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee  has to be different from the previous one.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Reopen(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task cannot be reopened." };
            if (patientService.Reopen(episodeId, patientId, eventId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Task has been reopened sucessfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error trying to delete this task. Please try again." };
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                viewData = patientService.ToggleTaskStatus(patientId, eventId, true);
                //if (patientService.DeleteSchedule(episodeId, patientId, eventId, employeeId, task))
                //{
                //    viewData.isSuccessful = true;
                //    viewData.errorMessage = "Task has been successfully deleted.";
                //}
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteMultiple(Guid PatientId, Guid EpisodeId, List<Guid> EventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The selected task(s) could not be deleted." };
            if (!PatientId.IsEmpty())
            {
                viewData = patientService.DeleteSchedules(PatientId, EpisodeId, EventId);
                //if (patientService.DeleteSchedules(PatientId, EpisodeId, EventId))
                //{
                //    viewData.isSuccessful = true;
                //    viewData.errorMessage = "The selected task(s) sucessfully deleted.";
                //}
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteList(Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");

            var patientActivities = patientService.GetScheduledEventsForDelete(episodeId, patientId, "all");
            patientActivities = patientActivities.Where(w => w.ActionUrl.Contains("Delete")).ToList();
            return View(new GridModel(patientActivities));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteSchedules(Guid episodeId, Guid patientId)
        {
            return PartialView("Delete", patientService.GetEpisodeLeanWithPatientNameViewData(patientId, episodeId));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VisitLog(Guid episodeId, Guid patientId)
        {
            return PartialView("VisitLog", patientService.GetScheduledEventsForEdit(episodeId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditVisitLog(Guid patientId, Guid episodeId, List<ScheduleEvent> scheduleEvents)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "You must select at least one task to edit." };
            if (scheduleEvents.IsNotNullOrEmpty())
            {
                viewData = patientService.UpdateScheduleEventsForVisitLog(patientId, episodeId, scheduleEvents);
                //if (viewData.isSuccessful)
                //{
                //    viewData.errorMessage = successCount + " task(s) has been successfully edited."+message;
                //}
            }
            return Json(viewData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Restore(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error trying to restore this task. Please try again." };
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                viewData = patientService.ToggleTaskStatus(patientId, eventId, false);
                //if (patientService.RestoreTask(episodeId, patientId, eventId))
                //{
                //    viewData.isSuccessful = true;
                //    viewData.errorMessage = "Task has been successfully restored.";
                //}
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MasterCalendarMain(Guid patientId, Guid episodeId)
        {
            var viewData = patientService.GetMasterCalendarViewData(patientId, episodeId, "All", true, true, false, true) ?? new CalendarViewData();
            return PartialView("MasterCalendar", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MasterCalendar(Guid patientId, Guid episodeId)
        {
            var viewData = patientService.GetMasterCalendarViewData(patientId, episodeId, "All", true, true, false, true) ?? new CalendarViewData();
            return PartialView("MasterCalendar", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MasterCalendarPdf(Guid patientId, Guid episodeId, bool showMissedVisits)
        {
            var viewData = patientService.GetMasterCalendarViewData(patientId, episodeId, "All", true, true, false, true) ?? new CalendarViewData();
            var doc = new MasterCalendarPdf(viewData, showMissedVisits);
            var PdfStream = doc.GetStream();
            PdfStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MasterCalendar_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(PdfStream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CalendarNav(Guid patientId, Guid episodeId, string discipline)
        {
            var viewData = new CalendarViewData();

            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                viewData = patientService.GetScheduleWithPreviousAfterEpisodeInfo(patientId, episodeId, discipline, true, true, true, false) ?? new CalendarViewData();
                viewData.LocationId = patient.AgencyLocationId;
                viewData.PatientId = patient.Id;
                viewData.IsDischarged = patient.IsDischarged;
                viewData.DisplayName = patient.DisplayName;
                viewData.PatientId = patientId;
            }
            viewData.PatientId = patientId;
            //CalendarViewData calendarViewData = null;
            //if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            //{
            //    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            //    if (patient != null)
            //    {
            //        calendarViewData = new CalendarViewData();
            //        var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, discipline);
            //        if (episode != null)
            //        {
            //            calendarViewData.Episode = episode;
            //            calendarViewData.PatientId = patientId;
            //        }
            //        else
            //        {
            //            calendarViewData.PatientId = patientId;
            //        }
            //        calendarViewData.IsDischarged = patient.IsDischarged;
            //    }
            //}
            return PartialView("Calendar", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActivityFirstTime(Guid patientId, string discipline)
        {
            var episode = episodeRepository.GetCurrentEpisodeOrLastEpisode(Current.AgencyId, patientId, DateTime.Now);
            if (patientId.IsEmpty() || episode == null)
            {
                return PartialView("Activities", new ScheduleActivityArgument { EpisodeId = Guid.Empty, PatientId = Guid.Empty, Discpline = "" });
            }
            return PartialView("Activities", new ScheduleActivityArgument { EpisodeId = episode.Id, PatientId = patientId, Discpline = discipline });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PASTravel(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                var noteQuestions = patientvisitNote.ToDictionary();
                viewData.Questions = noteQuestions;

                var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);  //patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (scheduleEvent != null)
                {
                    viewData.EndDate = scheduleEvent.EndDate;
                    viewData.StartDate = scheduleEvent.StartDate;
                    //var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    //if (evnt != null)
                    //{
                    viewData.UserId = scheduleEvent.UserId;
                    viewData.VisitDate = scheduleEvent.VisitDate.IsValid() ? scheduleEvent.VisitDate.ToString() : scheduleEvent.EventDate.ToString();
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduleEvent.ReturnReason);
                    // }

                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("PAS/PASTravel", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PASTravelPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("PAS/PASTravelPrint", noteService.GetPASTravelPrint( patientId,  episodeId,  eventId));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PASTravelPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new PASTravelPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PASTravel_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PASTravelBlank()
        {
            var doc = new PASTravelPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PASTravel_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PASVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                var noteQuestions = patientvisitNote.ToDictionary();
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {

                        //var pasCarePlanId = Guid.Empty;
                        //foreach (ScheduleEvent ev in scheduledEvents)
                        //{
                        //    if (ev.DisciplineTask == (int)DisciplineTasks.PASCarePlan)
                        //    {
                        //        pasCarePlanId = ev.EventId;
                        //        break;
                        //    }
                        //}
                        //if (pasCarePlanId != Guid.Empty)
                        //{
                        var pocEvent = scheduleRepository.GetCarePlanForEpisode(Current.AgencyId, scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.StartDate, scheduledEvent.EventDate, new List<int> { (int)DisciplineTasks.PASCarePlan });// patientRepository.GetVisitNote(Current.AgencyId, patientId, pasCarePlanId);

                        if (pocEvent != null)
                        {
                            var pocQuestions = pocEvent.ToDictionary();
                            if (pocQuestions.ContainsKey("DiastolicBPGreaterThan"))
                            {
                                noteQuestions.Add("DiastolicBPGreaterThan", pocQuestions["DiastolicBPGreaterThan"]);
                            }

                            if (pocQuestions.ContainsKey("SystolicBPGreaterThan"))
                            {
                                noteQuestions.Add("SystolicBPGreaterThan", pocQuestions["SystolicBPGreaterThan"]);
                            }

                            if (pocQuestions.ContainsKey("PulseGreaterThan"))
                            {
                                noteQuestions.Add("PulseGreaterThan", pocQuestions["PulseGreaterThan"]);
                            }

                            if (pocQuestions.ContainsKey("RespirationGreaterThan"))
                            {
                                noteQuestions.Add("RespirationGreaterThan", pocQuestions["RespirationGreaterThan"]);
                            }

                            if (pocQuestions.ContainsKey("TempGreaterThan"))
                            {
                                noteQuestions.Add("TempGreaterThan", pocQuestions["TempGreaterThan"]);
                            }

                            if (pocQuestions.ContainsKey("WeightGreaterThan"))
                            {
                                noteQuestions.Add("WeightGreaterThan", pocQuestions["WeightGreaterThan"]);
                            }

                            if (pocQuestions.ContainsKey("DiastolicBPLessThan"))
                            {
                                noteQuestions.Add("DiastolicBPLessThan", pocQuestions["DiastolicBPLessThan"]);
                            }

                            if (pocQuestions.ContainsKey("SystolicBPLessThan"))
                            {
                                noteQuestions.Add("SystolicBPLessThan", pocQuestions["SystolicBPLessThan"]);
                            }

                            if (pocQuestions.ContainsKey("PulseLessThan"))
                            {
                                noteQuestions.Add("PulseLessThan", pocQuestions["PulseLessThan"]);
                            }

                            if (pocQuestions.ContainsKey("RespirationLessThan"))
                            {
                                noteQuestions.Add("RespirationLessThan", pocQuestions["RespirationLessThan"]);
                            }

                            if (pocQuestions.ContainsKey("TempLessThan"))
                            {
                                noteQuestions.Add("TempLessThan", pocQuestions["TempLessThan"]);
                            }

                            if (pocQuestions.ContainsKey("WeightLessThan"))
                            {
                                noteQuestions.Add("WeightLessThan", pocQuestions["WeightLessThan"]);
                            }

                            if (pocQuestions.ContainsKey("PrimaryDiagnosis"))
                            {
                                noteQuestions.Add("PrimaryDiagnosis", pocQuestions["PrimaryDiagnosis"]);
                            }
                            if (pocQuestions.ContainsKey("ICD9M"))
                            {
                                noteQuestions.Add("ICD9M", pocQuestions["ICD9M"]);
                            }

                            if (pocQuestions.ContainsKey("PrimaryDiagnosis1"))
                            {
                                noteQuestions.Add("PrimaryDiagnosis1", pocQuestions["PrimaryDiagnosis1"]);
                            }
                            if (pocQuestions.ContainsKey("ICD9M1"))
                            {
                                noteQuestions.Add("ICD9M1", pocQuestions["ICD9M1"]);
                            }
                            if (pocQuestions.ContainsKey("DNR"))
                            {
                                noteQuestions.Add("DNR", pocQuestions["DNR"]);
                            }
                            if (pocQuestions.ContainsKey("IsVitalSignParameter"))
                            {
                                noteQuestions.Add("IsVitalSignParameter", pocQuestions["IsVitalSignParameter"]);
                            }
                            viewData.Questions = noteQuestions;
                        }
                        else
                        {
                            viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                            if ((noteQuestions.ContainsKey("PrimaryDiagnosis") && noteQuestions["PrimaryDiagnosis"].Answer.IsNullOrEmpty()) ||
                                !noteQuestions.ContainsKey("PrimaryDiagnosis"))
                            {
                                var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);// assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                                if (assessment != null)
                                {
                                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                                    var oasisQuestions = assessment.ToNotesQuestionDictionary();
                                    viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                                }
                            }
                        }
                        //}
                    }
                    {
                        viewData.Questions = noteQuestions;
                    }

                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    //viewData.PreviousNotes = null;//patientService.GetPreviousSkilledNurseNotes(patientId, scheduledEvent);
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                //}

                //}

            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("PAS/PASVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PASVisitContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.Version = patientvisitNote.Version;
            }
            return PartialView("PAS/PASVisitContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PASVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("PAS/PASVisitPrint", noteService.GetPASVisitPrint( patientId,  episodeId,  eventId));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PASVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new PASVisitPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PASVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PASVisitBlank()
        {
            var doc = new PASVisitPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PASVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PASCarePlan(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                viewData.Allergies = patientService.GetAllergies(patientId);
                var noteQuestions = patientvisitNote.ToDictionary();
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); // scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        viewData.UserId = scheduledEvent.UserId;
                        var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);//assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                        if (assessment != null)
                        {
                            var oasisQuestions = assessment.ToNotesQuestionDictionary();
                            if (oasisQuestions.ContainsKey("PatientDNR") && oasisQuestions["PatientDNR"] != null)
                            {
                                oasisQuestions["PatientDNR"].Answer = oasisQuestions["PatientDNR"].Answer == "Yes" ? "1" : "0";
                                oasisQuestions.Add("DNR", oasisQuestions["PatientDNR"]);
                                oasisQuestions.Remove("PatientDNR");
                            }
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        viewData.Questions = noteQuestions;
                    }

                    //var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                    //if (episode != null)
                    //{
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    //var evnt = episode.Schedule.ToObject<List<ScheduleEvent>>().SingleOrDefault(e => e.EventId == eventId);
                    //if (evnt != null)
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    //}
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    //viewData.PreviousNotes = null;//patientService.GetPreviousCarePlans(patientId, scheduledEvent);
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                else
                {
                    viewData.Questions = noteQuestions;
                }
                // }
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("PAS/PASCarePlan", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PASCarePlanContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.Version = patientvisitNote.Version;
            }
            return PartialView("PAS/PASCarePlanContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PASCarePlanPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("PAS/PASCarePlanPrint", noteService.GetPASCarePlanPrint( patientId,  episodeId,  eventId));
        }

       

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PASCarePlanPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new PASCarePlanPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PASCarePlan_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PASCarePlanBlank()
        {
            var doc = new PASCarePlanPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PASCarePlan_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Deviation()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Deviation", reportService.GetScheduleDeviation(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeviationContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("DeviationContent", reportService.GetScheduleDeviation(BranchCode, StartDate, EndDate));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeRangeList(Guid patientId)
        {
            return Json(patientService.ActiveEpisodeRangeList(patientId));
        }



        public ActionResult ScheduleLogs(Guid eventId, Guid patientId, int task)
        {
            return PartialView("Logs", patientService.GetTaskLogs(patientId, eventId, task));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Inactive(Guid patientId)
        {
            return PartialView("Episode/Inactive", patientService.GetPatientDeactivatedAndDischargedEpisodes(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InactiveGrid(Guid patientId)
        {
            return PartialView("Episode/InactiveGrid", patientService.GetPatientDeactivatedAndDischargedEpisodes(patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActivateEpisode(Guid episodeId, Guid patientId)
        {
            var viewData = patientService.ActivateEpisode(episodeId, patientId);
            //new JsonViewData { isSuccessful = false, errorMessage = "Episode could not be activated. Try again." };
            //var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            //if (patientEpisode != null)
            //{
            //    var validationRules = new List<Validation>();
            //    validationRules.Add(new Validation(() => !patientService.IsValidEpisode(patientEpisode.Id, patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate), "Episode date is not in the valid date range."));
            //    var entityValidator = new EntityValidator(validationRules.ToArray());
            //    entityValidator.Validate();
            //    if (entityValidator.IsValid)
            //    {

            //        patientEpisode.IsActive = true;
            //        patientEpisode.IsDischarged = false;
            //        patientEpisode.Modified = DateTime.Now;
            //        if (patientRepository.UpdateEpisode(patientEpisode))
            //        {
            //            Auditor.AddGeneralLog(LogDomain.Patient, patientEpisode.PatientId, patientEpisode.Id.ToString(), LogType.Episode, LogAction.EpisodeActivated, string.Empty);
            //            viewData.isSuccessful = true;
            //            viewData.errorMessage = "Episode has been successfully activated.";
            //        }
            //    }
            //    else
            //    {
            //        viewData.isSuccessful = false;
            //        viewData.errorMessage = entityValidator.Message;
            //    }
            //}
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeLogs(Guid episodeId, Guid patientId)
        {
            return PartialView("Episode/Logs", patientService.GetGeneralLogs(LogDomain.Patient, LogType.Episode, patientId, episodeId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNDiabeticDailyVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                viewData.Questions = patientvisitNote.ToDictionary();
                //var scheduledEvents =  episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);//assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }

                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Version = patientvisitNote.Version;
                    //viewData.PreviousNotes = null;//patientService.GetPreviousSkilledNurseNotes(patientId, scheduledEvent);
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
                //}
                //}
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }

            return PartialView("Nursing/SNDiabeticDailyVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNDiabeticDailyVisitContent(Guid patientId, Guid noteId, Guid previousNoteId)
        {
            var viewData = new VisitNoteEditViewData();
            var previousNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, previousNoteId);
            if (previousNote != null)
            {
                viewData.TypeName = previousNote.NoteType;
                viewData.IsWoundCareExist = previousNote.IsWoundCare;
                viewData.IsSupplyExist = previousNote.IsSupplyExist;
                viewData.PatientId = previousNote.PatientId;


                var noteItems = previousNote.ToDictionary();

                var nameArray = new string[] { "PatientId", "EpisodeId", "EventId", "DisciplineTask", "VisitDate", "TimeIn", "TimeOut", "PreviousNotes", "Clinician", "SignatureDate", "button", "PrimaryDiagnosis", "ICD9M", "PrimaryDiagnosis1", "ICD9M1" };
                nameArray.ForEach(name =>
                {
                    noteItems.Remove(name);
                });
                viewData.Questions = noteItems;

                var currentNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
                if (currentNote != null)
                {
                    currentNote.Questions = noteItems.Values.ToList();
                    currentNote.Note = currentNote.Questions.ToXml();
                    currentNote.IsWoundCare = previousNote.IsWoundCare;
                    currentNote.WoundNote = previousNote.WoundNote;
                    currentNote.Modified = DateTime.Now;
                    if (patientRepository.UpdateVisitNote(currentNote))
                    {
                        if (scheduleRepository.UpdateScheduleTasksStatus(currentNote.AgencyId, currentNote.PatientId, currentNote.Id, ((int)ScheduleStatus.NoteSaved)))
                        {
                            Auditor.Log(currentNote.EpisodeId, currentNote.PatientId, currentNote.Id, Actions.StatusChange, ScheduleStatus.NoteSaved, DisciplineTasks.SNDiabeticDailyVisit, "Loaded Previous Note");
                        }
                        //var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, currentNote.EpisodeId, currentNote.PatientId, currentNote.Id);
                        //if (scheduleEvent != null)
                        //{
                        //    scheduleEvent.Status = ((int)ScheduleStatus.NoteSaved).ToString();
                        //    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, currentNote.Id);
                        //    if (userEvent != null)
                        //    {
                        //        userEvent.Status = scheduleEvent.Status;
                        //    }
                        //    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        //    {
                        //        if (userEvent != null)
                        //        {
                        //            userRepository.UpdateEvent(Current.AgencyId, userEvent);
                        //        }
                        //        else
                        //        {
                        //            userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit });
                        //        }
                        //        if (scheduleEvent.Status.IsInteger())
                        //        {
                        //            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, "Loaded Previous Note");
                        //        }
                        //    }
                        //}
                    }
                    viewData.EpisodeId = currentNote.EpisodeId;  //previousNote.EpisodeId;
                    viewData.EventId = currentNote.Id; //previousNote.Id;
                    viewData.Type = currentNote.NoteType.IsNotNullOrEmpty() ? currentNote.NoteType.Trim() : string.Empty;
                }
            }
            return PartialView("Nursing/SNDiabeticDailyVisitContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SNDiabeticDailyVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new DiabeticDailyPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=SNDiabeticDailyVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNDiabeticDailyVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Nursing/SNDiabeticDailyVisitPrint", noteService.GetVisitNotePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult SNDiabeticDailyVisitPdfBlank()
        {
            var doc = new LVNSVisitPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=SNDiabeticDailyVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNPediatricAssessment(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId); //scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);//assessmentService.GetEpisodeAssessmentNew(Current.AgencyId, episode, scheduledEvents);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {

                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }
                    viewData.PatientId = patientvisitNote.PatientId;
                    viewData.EpisodeId = patientvisitNote.EpisodeId;
                    viewData.EventId = patientvisitNote.Id;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                    viewData.Allergies = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId).ToXml();
                }
                //}
                //}
            }
            return PartialView("Nursing/SNPediatricAssessment", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNPediatricAssessmentPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var note = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView("Nursing/SNPediatricAssessmentPrint", note);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SNPediatricAssessmentPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            var doc = new PediatricPdf(viewData, PdfDocs.PediatricAssessment);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=SNPediatricAssessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult SNPediatricAssessmentPdfBlank()
        {
            var viewData = noteService.GetVisitNotePrint();
            if (viewData != null && viewData.Data != null)
            {
                viewData.Data.Type = "SNPediatricAssessment";
            }
            var doc = new PediatricPdf(viewData, PdfDocs.PediatricAssessment);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=SNPediatricAssessment_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNPediatricVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var agencyId = Current.AgencyId;
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.Get(agencyId);
            var patientvisitNote = patientRepository.GetVisitNote(agencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                //var episode = patientRepository.GetEpisodeLeanWithSchedule(Current.AgencyId, patientId, episodeId);
                //if (episode != null)
                //{
                IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                var noteQuestions = patientvisitNote.ToDictionary();
                //var scheduledEvents =  episode.Schedule.ToObject<List<ScheduleEvent>>();
                //if (scheduledEvents.IsNotNullOrEmpty())
                //{
                var scheduledEvent = scheduleRepository.GetScheduleTask(agencyId, patientId, eventId);// scheduledEvents.Where(e => e.EventId == eventId && e.IsDeprecated == false).FirstOrDefault();
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    var assessment = assessmentService.GetEpisodeAssessment(scheduledEvent, false);
                    if (assessment != null)
                    {
                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                        viewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(assessment.EpisodeId, patientId, assessment.Id);
                    }
                    if (patientvisitNote.Status == ((int)ScheduleStatus.NoteNotYetDue) || patientvisitNote.Status == ((int)ScheduleStatus.NoteNotStarted))
                    {
                        if (patientvisitNote.Note != null)
                        {
                            viewData.Questions = oasisQuestions.CombineOasisQuestionsAndNoteQuestions(noteQuestions);
                        }
                        else
                        {
                            viewData.Questions = oasisQuestions;
                        }
                    }
                    else
                    {
                        viewData.Questions = CombineNoteQuestionsAndOasisQuestions(noteQuestions, oasisQuestions);
                    }
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Version = patientvisitNote.Version;
                //if (patientvisitNote.Version == 2)
                //{
                //    viewData.PreviousNotes = null;// patientService.GetPreviousPediatricVisitNotes(patientId, scheduledEvent, 2);
                //}
                //else
                //{
                //    viewData.PreviousNotes = null;//patientService.GetPreviousSkilledNurseNotes(patientId, scheduledEvent);
                //}
                viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                //}
                //}
                //else
                //{
                //    viewData.Questions = new Dictionary<string, NotesQuestion>();
                //}
                var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                if (patientProfile != null)
                {
                    viewData.SetPateintInfoForEdit(patientProfile);
                }
            }
            else
            {
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }

            return PartialView(string.Format("Nursing/SNPediatricVisit{0}", patientvisitNote.Version > 0 ? patientvisitNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SNPediatricVisitContent(Guid patientId, Guid noteId, Guid previousNoteId)
        {
            var viewData = new VisitNoteEditViewData();
            var previousNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, previousNoteId);
            if (previousNote != null)
            {
                viewData.TypeName = previousNote.NoteType;
                viewData.IsWoundCareExist = previousNote.IsWoundCare;
                viewData.IsSupplyExist = previousNote.IsSupplyExist;
                viewData.PatientId = previousNote.PatientId;


                var noteItems = previousNote.ToDictionary();

                var nameArray = new string[] { "PatientId", "EpisodeId", "EventId", "DisciplineTask", "VisitDate", "TimeIn", "TimeOut", "PreviousNotes", "Clinician", "SignatureDate", "button", "PrimaryDiagnosis", "ICD9M", "PrimaryDiagnosis1", "ICD9M1" };
                nameArray.ForEach(name =>
                {
                    noteItems.Remove(name);
                });
                viewData.Questions = noteItems;

                var currentNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
                if (currentNote != null)
                {
                    currentNote.Questions = noteItems.Values.ToList();
                    currentNote.Note = currentNote.Questions.ToXml();
                    currentNote.IsWoundCare = previousNote.IsWoundCare;
                    currentNote.WoundNote = previousNote.WoundNote;
                    currentNote.Modified = DateTime.Now;
                    if (patientRepository.UpdateVisitNote(currentNote))
                    {
                        if (scheduleRepository.UpdateScheduleTasksStatus(currentNote.AgencyId, currentNote.PatientId, currentNote.Id, ((int)ScheduleStatus.NoteSaved)))
                        {
                            Auditor.Log(currentNote.EpisodeId, currentNote.PatientId, currentNote.Id, Actions.StatusChange, ScheduleStatus.NoteSaved, DisciplineTasks.SNPediatricVisit, "Loaded Previous Note");
                        }
                        //var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, currentNote.EpisodeId, currentNote.PatientId, currentNote.Id);
                        //if (scheduleEvent != null)
                        //{
                        //    scheduleEvent.Status = ((int)ScheduleStatus.NoteSaved).ToString();
                        //    var userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, currentNote.Id);
                        //    if (userEvent != null)
                        //    {
                        //        userEvent.Status = scheduleEvent.Status;
                        //    }
                        //    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        //    {
                        //        if (userEvent != null)
                        //        {
                        //            userRepository.UpdateEvent(Current.AgencyId, userEvent);
                        //        }
                        //        else
                        //        {
                        //            userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, VisitDate = scheduleEvent.VisitDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit });
                        //        }
                        //        if (scheduleEvent.Status.IsInteger())
                        //        {
                        //            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status.ToInteger(), (DisciplineTasks)scheduleEvent.DisciplineTask, "Loaded Previous Note");
                        //        }
                        //    }
                        //}
                    }
                    viewData.EpisodeId = currentNote.EpisodeId;  //previousNote.EpisodeId;
                    viewData.EventId = currentNote.Id; //previousNote.Id;
                    viewData.Type = currentNote.NoteType.IsNotNullOrEmpty() ? currentNote.NoteType.Trim() : string.Empty;
                }
            }
            return PartialView(string.Format("Nursing/SNPediatricVisitContent{0}", previousNote.Version > 0 ? previousNote.Version : 1), viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult SNPediatricVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            if (viewData!=null && viewData.Data!=null && viewData.Data.Version == 2)
            {
                var doc = new PediatricPdf(viewData, PdfDocs.Pediatric2);
                var stream = doc.GetStream();
                stream.Position = 0;
                HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=SNPediatricVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
                return new FileStreamResult(stream, "application/pdf");
            }
            else
            {
                var doc = new PediatricPdf(viewData, PdfDocs.Pediatric);
                var stream = doc.GetStream();
                stream.Position = 0;
                HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=SNPediatricVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
                return new FileStreamResult(stream, "application/pdf");
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SNPediatricVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var SNPediatricVisit = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            return PartialView(string.Format("Nursing/SNPediatricVisitPrint{0}",SNPediatricVisit!=null && SNPediatricVisit.Data!=null && SNPediatricVisit.Data.Version > 0 ? SNPediatricVisit.Data.Version : 1), SNPediatricVisit);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult SNPediatricVisitPdfBlank()
        {
            var viewdata = noteService.GetVisitNotePrint();
            if (viewdata != null && viewdata.Data != null)
            {
                viewdata.Data.Type = "SNPediatricVisit";
            }
            var doc = new PediatricPdf(viewdata, PdfDocs.Pediatric2);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=SNPediatricVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UAPWoundCareVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToDictionary();
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);// patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.EventId = scheduledEvent.Id;
                    viewData.PatientId = scheduledEvent.PatientId;
                    viewData.EpisodeId = scheduledEvent.EpisodeId;
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                   // viewData.PreviousNotes = null;//patientService.GetPreviousHHANotes(patientId, scheduledEvent);
                    viewData.Version = patientvisitNote.Version;
                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
            }
            else viewData.Questions = new Dictionary<string, NotesQuestion>();
            return PartialView("UAP/UAPWoundCareVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UAPWoundCareVisitContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.Version = patientvisitNote.Version;
            }
            return PartialView("UAP/UAPWoundCareVisitContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult UAPWoundCareVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new UAPWoundCarePdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=UAPWoundCarePrint_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UAPWoundCareVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            if (viewData != null && viewData.Data != null)
            {
                var xml = new VisitNoteXml(viewData.Data, PdfDocs.UAPWoundCare);
                viewData.Data.PrintViewJson = xml.GetJson();
            }
            return PartialView("UAP/UAPWoundCareVisitPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult UAPWoundCareVisitBlank()
        {
            var doc = new UAPWoundCarePdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=UAPWoundCareVisit_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UAPInsulinPrepAdminVisit(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteEditViewData();
            //viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToDictionary();
                var scheduledEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);//patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
                if (scheduledEvent != null)
                {
                    viewData.StatusComment = patientService.GetReturnComments(eventId, episodeId, patientId, scheduledEvent.ReturnReason);
                    viewData.UserId = scheduledEvent.UserId;
                    viewData.EventId = scheduledEvent.Id;
                    viewData.PatientId = scheduledEvent.PatientId;
                    viewData.EpisodeId = scheduledEvent.EpisodeId;
                    viewData.EndDate = scheduledEvent.EndDate;
                    viewData.StartDate = scheduledEvent.StartDate;
                    viewData.DisciplineTask = scheduledEvent.DisciplineTask;
                    viewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString() : scheduledEvent.EventDate.ToString();
                    //viewData.PreviousNotes = null;//patientService.GetPreviousHHANotes(patientId, scheduledEvent);
                    viewData.Version = patientvisitNote.Version;


                    viewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                    viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                    var patientProfile = patientRepository.GetPatientForNoteEditOnly(Current.AgencyId, patientId);
                    if (patientProfile != null)
                    {
                        viewData.SetPateintInfoForEdit(patientProfile);
                    }
                }
            }
            else viewData.Questions = new Dictionary<string, NotesQuestion>();
            return PartialView("UAP/UAPInsulinPrepAdminVisit", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UAPInsulinPrepAdminVisitContent(Guid patientId, Guid noteId, string type)
        {
            var viewData = new VisitNoteEditViewData();
            viewData.Type = type;
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, noteId);
            if (patientvisitNote != null)
            {
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                viewData.Questions = patientvisitNote.ToDictionary();
                viewData.Version = patientvisitNote.Version;
            }
            return PartialView("UAP/UAPInsulinPrepAdminVisitContent", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult UAPInsulinPrepAdminVisitPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new UAPInsulinPdf(noteService.GetVisitNotePrint(episodeId, patientId, eventId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=UAPInsulin_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UAPInsulinPrepAdminVisitPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = noteService.GetVisitNotePrint(episodeId, patientId, eventId);
            if (viewData != null)
            {
                var xml = new VisitNoteXml(viewData.Data, PdfDocs.UAPInsulin);
                viewData.Data.PrintViewJson = xml.GetJson();
            }
            return PartialView("UAP/UAPInsulinPrepAdminVisitPrint", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult UAPInsulinPrepAdminVisitBlank()
        {
            var doc = new UAPInsulinPdf(noteService.GetVisitNotePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=UAPInsulin_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetDiagnosisData(Guid patientId, Guid episodeId)
        {
            return Json(assessmentService.GetDiagnosisData(patientId, episodeId));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReturnReason(Guid eventId, Guid episodeId, Guid patientId)
        {
            var viewData = new JsonViewData();
            var scheduleEvent = scheduleRepository.GetScheduleTask(Current.AgencyId, patientId, eventId);
            if (scheduleEvent != null)
            {
                var comments = patientService.GetReturnComments(eventId, episodeId, patientId, scheduleEvent.ReturnReason);
                if (comments.IsNotNullOrEmpty())
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = comments;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddReturnReason(Guid eventId, Guid episodeId, Guid patientId, string comment)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The return comments failed to save." };
            if (patientService.AddReturnComments(eventId, episodeId, comment))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Return comment saved successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EditReturnReason(int id, string comment)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The return comments failed to save." };
            if (patientService.EditReturnComments(id, comment))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Return comment saved successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteReturnReason(int id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The return comments failed to delete." };
            if (patientService.DeleteReturnComments(id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Return comment deleted successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VerificationInformation(Guid episodeId, Guid patientId, Guid eventId)
        {
            if (episodeId.IsEmpty() || patientId.IsEmpty() || eventId.IsEmpty())
            {
                return PartialView("Verification", new ScheduleEvent());
            }
            return PartialView("Verification", patientService.GetVisitVerificationLog(patientId, episodeId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetScheduledEvent(Guid episodeId, Guid patientId, Guid eventId)
        {
            ScheduleEvent task = null;
            if (episodeId.IsNotEmpty() && patientId.IsNotEmpty() && eventId.IsNotEmpty())
            {
                task = patientService.GetScheduledEvent(episodeId, patientId, eventId);
            }
            return Json(task);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PrintPdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var scheduleEvent = patientService.GetScheduledEvent(episodeId, patientId, eventId);
            if (scheduleEvent != null)
            {
                var agencyId = Current.AgencyId;
                AxxessPdf pdf = null;
                var type = scheduleEvent.DisciplineTask;
                if (DisciplineTaskFactory.AllAssessments(true).Contains(scheduleEvent.DisciplineTask))
                {
                    pdf = new OasisPdf(assessmentService.GetAssessmentPrint(agencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id));
                }
                else if (DisciplineTaskFactory.AllPatientVisitNotes().Contains(scheduleEvent.DisciplineTask))
                {
                    pdf = noteService.GetPrintPdf(scheduleEvent);
                }
                else if (DisciplineTaskFactory.POC().Contains(scheduleEvent.DisciplineTask))
                {
                    pdf = new PlanOfCarePdf(assessmentService.GetPlanOfCarePrint(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.Id, agencyId));
                }
                else
                {
                    switch (type)
                    {
                        case (int)DisciplineTasks.PhysicianOrder:
                            {
                                pdf = new PhysicianOrderPdf(orderManagementService.GetOrderPrint(scheduleEvent.PatientId, scheduleEvent.Id, agencyId));
                            }
                            break;
                        case (int)DisciplineTasks.IncidentAccidentReport:
                            {
                                pdf = new IncidentReportPdf(agencyService.GetIncidentReportPrint(agencyId, scheduleEvent.PatientId, scheduleEvent.Id));
                            }
                            break;
                        case (int)DisciplineTasks.InfectionReport:
                            {
                                pdf = new InfectionReportPdf(agencyService.GetInfectionReportPrint(agencyId, scheduleEvent.PatientId, scheduleEvent.Id));
                            }
                            break;
                        case (int)DisciplineTasks.CommunicationNote:
                            {
                                pdf = new ComNotePdf(patientService.GetCommunicationNotePrint(scheduleEvent.Id, scheduleEvent.PatientId, agencyId));
                            }
                            break;
                        case (int)DisciplineTasks.FaceToFaceEncounter:
                            {
                                pdf = new PhysFaceToFacePdf(orderManagementService.GetFaceToFacePrint(scheduleEvent.PatientId, scheduleEvent.Id, agencyId));
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (pdf != null)
                {
                    var stream = pdf.GetStream();
                    stream.Position = 0;
                    HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Document_{0}.pdf", DateTime.Now.Ticks.ToString()));

                    return new FileStreamResult(stream, "application/pdf");
                }
            }
            return null;
        }
        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Previous(Guid PatientId, Guid EpisodeId, Guid EventId)
        {
            var previousNotes = patientService.GetPreviousNotes(PatientId, EpisodeId, EventId);
            var items = new List<SelectListItem>();
            items.Insert(0, new SelectListItem { Text = "-- Select Previous Notes --", Value = Guid.Empty.ToString() });
            if (previousNotes.IsNotNullOrEmpty())
            {
                previousNotes.ForEach(v => items.Add(new SelectListItem { Text = string.Format("{0} {1}", v.Name, v.Date), Value = v.Id.ToString() }));
            }
            return Json(items);
        }
    }
}
