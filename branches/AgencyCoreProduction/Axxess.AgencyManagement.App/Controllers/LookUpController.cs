﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Xml.XPath;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    using Services;
    using Security;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Telerik.Web.Mvc;

    [Compress]
    [HandleError]
    [SslRedirect]
    [AxxessAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class LookUpController : BaseController
    {
        #region Constructor
        private readonly IDrugService drugService;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;

        public LookUpController(ILookUpDataProvider lookupDataProvider, IDrugService drugService, IAgencyManagementDataProvider agencyManagementDataProvider)
        {
            Check.Argument.IsNotNull(drugService, "drugService");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");

            this.drugService = drugService;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Weather(string format, string callback, string diagnostics, string env, string q)
        {
            var parameters = new StringBuilder();
            parameters.AppendFormat("?format={0}", format);
            parameters.AppendFormat("&diagnostics={0}", diagnostics);
            parameters.AppendFormat("&callback={0}", callback);
            parameters.AppendFormat("&diagnostics={0}", diagnostics);
            parameters.AppendFormat("&env={0}", env);
            parameters.AppendFormat("&q={0}", q);
            return Content(GetWeather(parameters.ToString()), "application/json");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ProxyResult WeatherImage(string parameter)
        {
            return new ProxyResult(string.Format(AppSettings.WeatherImageUriFormat, parameter));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ProxyResult WeatherForecastImage(string parameter)
        {
            return new ProxyResult(string.Format(AppSettings.WeatherForecastImageUriFormat, parameter));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ProxyResult WeatherImageThumbnail(string parameter)
        {
            return new ProxyResult(string.Format(AppSettings.WeatherImageThumbnailUriFormat, parameter));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult States()
        {
            return Json(lookupRepository.States());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult Races()
        {
            return Json(lookupRepository.Races());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult PaymentSources()
        {
            return Json(lookupRepository.PaymentSources());
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //[OutputCache(Duration = 86400, VaryByParam = "None")]
        //public JsonResult DrugClassifications()
        //{
        //    return Json(lookupRepository.DrugClassifications());
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult ReferralSources()
        {
            return Json(lookupRepository.ReferralSources());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult SupplyCategories()
        {
            return Json(lookupRepository.SupplyCategories());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult AdmissionSources()
        {
            return Json(lookupRepository.AdmissionSources());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DisciplineTasks(string Discipline)
        {
            if (Discipline.IsNullOrEmpty()) return Json(lookupRepository.DisciplineTasks());
            return Json(lookupRepository.DisciplineTasks(Discipline));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult MultipleDisciplineTasks()
        {
            return Json(lookupRepository.DisciplineTasks().Where(d => d.IsMultiple).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DiagnosisCodes()
        {
            var gridModel = new GridModel();
            var pageNumber = this.HttpContext.Request.Params["page"];
            var diagnosisCodes = lookupRepository.DiagnosisCodes();
            if (diagnosisCodes != null && pageNumber.HasValue())
            {
                int page = int.Parse(pageNumber);
                gridModel.Data = diagnosisCodes.Skip((page - 1) * 13).Take(13);
                gridModel.Total = diagnosisCodes.Count;
            }
            return Json(gridModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DiagnosisCode(string term, string type)
        {
            var diagnosisCodes = new List<DiagnosisCode>();
            if (type.ToUpper() == "ICD") diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.Code.StartsWith(term.ToUpper().Trim())).ToList();
            else if (type.ToUpper() == "DIAGNOSIS") diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.ShortDescription.ToLower().Contains(term.ToLower())).ToList();
            return Json(diagnosisCodes);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DiagnosisCodeNoVE(string term, string type)
        {
            var diagnosisCodes = new List<DiagnosisCode>();
            if (type.ToUpper() == "ICD") diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.Code.StartsWith(term.ToUpper().Trim())).Where(s => !s.Code.StartsWith("V") && !s.Code.StartsWith("E")).ToList();
            else if (type.ToUpper() == "DIAGNOSIS") diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.ShortDescription.ToLower().Contains(term.ToLower())).Where(s => !s.Code.StartsWith("V") && !s.Code.StartsWith("E")).ToList();
            return Json(diagnosisCodes);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Supplies(string term, int limit, string type)
        {
            if (type == "Code")
            {
                var supplies = lookupRepository.Supplies().Where(p => p.Code.Contains
                    (term.ToUpperCase())).Select(p => new { p.Description, p.Code ,p.Id}).Take(limit).ToList();
                return Json(supplies);
            }
            else if (type == "Description")
            {
                var supplies = lookupRepository.Supplies().Where(p => p.Description.ToLower().Contains(term.ToLower())).Select(p => new { p.Description, p.Code ,p.Id}).Take(limit).ToList();
                return Json(supplies);
            }

            return Json(null);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SuppliesGrid(string q, int limit, string type)
        {
            if (q != string.Empty)
            {
                if (type == "Code")
                {
                    var supplies = lookupRepository.Supplies().Where(p => p.Code.StartsWith
                        (q.ToUpper())).Take(limit).ToList();
                    return View(new GridModel(supplies));
                }
                else if (type == "Description")
                {
                    var supplies = lookupRepository.Supplies().Where(p => p.Description.ToLower().Contains(q.ToLower())).Take(limit).ToList();
                    return View(new GridModel(supplies));
                }
            }
            var list = new List<Supply>();
            return View(new GridModel(list));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult ProcedureCodes()
        {
            return Json(lookupRepository.ProcedureCodes());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ProcedureCode(string term, string type)
        {
            var procedureCodes = new List<ProcedureCode>();
            if (type.ToUpper() == "PROCEDUREICD") procedureCodes = lookupRepository.ProcedureCodes().Where(p => p.Code.StartsWith(term)).ToList();
            else if (type.ToUpper() == "PROCEDURE") procedureCodes = lookupRepository.ProcedureCodes().Where(p => p.ShortDescription.ToLower().Contains(term.ToLower())).ToList();
            return Json(procedureCodes);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Npi(string npiNumber)
        {
            return Json(lookupRepository.GetNpiData(npiNumber));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Npis(string term)
        {
            return Json(lookupRepository.GetNpis(term, 20));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NewShortGuid()
        {
            return Json(new { text = ShortGuid.NewId().ToString() });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationRoute(string term)
        {
            return Json(lookupRepository.MedicationRoute(term, 20));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationClassification(string term)
        {
            return Json(lookupRepository.MedicationClassification(term, 20));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationDosage(string q, int limit)
        {
            return Json(lookupRepository.MedicationDosage(q, limit));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Drugs(string term)
        {
            return Json(drugService.DrugsStartsWith(term, 20));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DrugClassifications(string genericDrugId)
        {
            return Json(drugService.GetDrugClassifications(genericDrugId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Physicians(string term)
        {
            IEnumerable<SelectListItem> physicianItems = new List<SelectListItem>();
            var physicians = PhysicianEngine.AsList(Current.AgencyId);
            if (physicians != null && physicians.Count > 0)
            {
                if (term.IsNotNullOrEmpty())
                {
                    physicians = physicians.Where(p => p.DisplayName.ToLowerInvariant().Contains(term.ToLowerInvariant())).Take(100).ToList();
                }
                physicianItems = from physician in physicians
                                 select new SelectListItem
                                 {
                                     Text = physician.DisplayName,
                                     Value = physician.Id.ToString()
                                 };
            }
            return Json(physicianItems.OrderBy(p => p.Text).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PhysicianName(Guid id)
        {
            var physician = PhysicianEngine.Get(id, Current.AgencyId);
            if (physician != null)
            {
                string DisplayName = physician.DisplayName;
                return Json(DisplayName);
            }
            else return Json(string.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Patients(string term)
        {
            IEnumerable<SelectListItem> patientItems = new List<SelectListItem>();
            var patients = patientRepository.Find(1, Guid.Empty, Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                if (term.IsNotNullOrEmpty())
                {
                    patients = patients.Where(p => p.DisplayNameWithMi.ToLowerInvariant().Contains(term.ToLowerInvariant())).Take(100).ToList();
                }
                patientItems = from patient in patients select new SelectListItem { Text = patient.DisplayNameWithMi, Value = patient.Id.ToString() };
            }
            return Json(patientItems.OrderBy(p => p.Text).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientName(Guid id)
        {
            return Json(patientRepository.GetPatientNameById(id, Current.AgencyId));
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CityStateByZipCode(string zipCode)
        {
            var viewData = new CityStateViewData();
            var xml = "<CityStateLookupRequest USERID='781AXXES1980'><ZipCode ID='0'><Zip5>" + zipCode + "</Zip5></ZipCode></CityStateLookupRequest>";
            var url = "http://production.shippingapis.com/ShippingAPI.dll";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            Encoding e = Encoding.GetEncoding("iso-8859-1");
            string data = "API=CityStateLookup&XML=" + HttpUtility.UrlEncode(xml, e);
            XPathDocument XMLResponse = null;
            using (StreamWriter postBody = new StreamWriter(request.GetRequestStream()))
            {
                postBody.Write(data);//, 0, byteData.Length);
            }
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (HttpStatusCode.OK != response.StatusCode)
            {
                Console.WriteLine();
            }
            else
            {
                StreamReader responseStream = new StreamReader(response.GetResponseStream());
                using (responseStream)
                {
                    XPathDocument doc = new XPathDocument(responseStream);
                    XMLResponse = doc;

                    if (XMLResponse != null)
                    {
                        XPathNavigator nav = XMLResponse.CreateNavigator();
                        var iter = nav.Select("//ZipCode");
                        while (iter.MoveNext())
                        {
                            iter.Current.MoveToChild("City", "");
                            if (!iter.Current.InnerXml.Contains("<Error>"))
                            {
                                viewData.City = iter.Current.InnerXml;
                                iter.Current.MoveToFollowing("State", "");
                                viewData.State = iter.Current.InnerXml;
                                viewData.isSuccessful = true;
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                break;
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Private Methods

        private string GetWeather(string parameters)
        {
            if (AppSettings.GetRemoteContent.ToBoolean())
            {
                string jsonResult = string.Empty;
                string weatherUrl = string.Concat(AppSettings.YahooWeatherUri, parameters);
                Uri weatherUri = new Uri(weatherUrl, UriKind.Absolute);

                HttpWebRequest webRequest = (HttpWebRequest)System.Net.WebRequest.Create(weatherUri);
                HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        jsonResult = sr.ReadToEnd();
                    }
                }
                return string.Format("{0}", jsonResult);
            }
            else return string.Empty;
        }

        #endregion
    }
}
