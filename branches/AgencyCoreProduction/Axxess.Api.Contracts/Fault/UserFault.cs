﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/UserCacheFault/2011/10/", Name = "UserCacheFault")]
    public class UserCacheFault : DefaultFault
    {
        public UserCacheFault() : base() {  }

        public UserCacheFault(string message) : base((int)FaultReasons.UserCacheFault, message) { }
    }
}
