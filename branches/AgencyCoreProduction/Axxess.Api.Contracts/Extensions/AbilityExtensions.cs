﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Diagnostics;
    using System.Security.Cryptography.X509Certificates;

    using Axxess.Api.Contracts;

    public static class AbilityNetworkExtensions
    {
        public static X509Certificate2 Get(this X509Certificate2Collection certificates, string searchString)
        {
            X509Certificate2 x509 = null;
            foreach (X509Certificate2 certificate in certificates)
            {
                if (certificate.FriendlyName.Contains(searchString))
                {
                    x509 = certificate;
                    break;
                }
            }
            return x509;
        }

        public static hiqhRequest UseHost(this hiqhRequest request, string host)
        {
            if (request != null && request.searchCriteria.Length > 0)
            {
                request.searchCriteria[0].hostId = host;
            }
            return request;
        }

        public static hiqhRequest UseCredentials(this hiqhRequest request, string userId, string password)
        {
            if (request != null && request.medicareMainframe.Length > 0)
            {
                if (request.medicareMainframe[0].credential != null && request.medicareMainframe[0].credential.Length > 0)
                {
                    request.medicareMainframe[0].credential[0].userId = userId;
                    request.medicareMainframe[0].credential[0].password = password;
                }
                else
                {
                    request.medicareMainframe[0].credential = new hiqhRequestMedicareMainframeCredential[1]
                    {
                        new hiqhRequestMedicareMainframeCredential
                        {
                           userId = userId,
                           password = password
                        }
                    };
                }
            }
            return request;
        }

        public static passwordChangeRequest UseCredentials(this passwordChangeRequest request, string userId, string password)
        {
            if (request != null && request.medicareMainframe.Length > 0)
            {
                if (request.medicareMainframe[0].credential != null && request.medicareMainframe[0].credential.Length > 0)
                {
                    request.medicareMainframe[0].credential[0].userId = userId;
                    request.medicareMainframe[0].credential[0].password = password;
                }
                else
                {
                    request.medicareMainframe[0].credential = new passwordChangeRequestMedicareMainframeCredential[1]
                    {
                        new passwordChangeRequestMedicareMainframeCredential
                        {
                           userId = userId,
                           password = password
                        }
                    };
                }
            }
            return request;
        }
    }
}
