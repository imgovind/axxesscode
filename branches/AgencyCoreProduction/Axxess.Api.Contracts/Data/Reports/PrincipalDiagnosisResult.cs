﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/PrincipalDiagnosisResult/2012/03/")]
    public class PrincipalDiagnosisResult : BaseCaliforniaReportResult
    {
        [DataMember]
        public int Visits { get; set; }
        [DataMember]
        public int Patients { get; set; }
    }
}
