﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/CostReportResult/2012/03/")]
    public class CostReportResult
    {
        [DataMember]
        public int SNMedicareVisits { get; set; }
        [DataMember]
        public int SNNonMedicareVisits { get; set; }
        [DataMember]
        public int SNMedicarePatients { get; set; }
        [DataMember]
        public int SNNonMedicarePatients { get; set; }

        [DataMember]
        public int PTMedicareVisits { get; set; }
        [DataMember]
        public int PTNonMedicareVisits { get; set; }
        [DataMember]
        public int PTMedicarePatients { get; set; }
        [DataMember]
        public int PTNonMedicarePatients { get; set; }

        [DataMember]
        public int OTMedicareVisits { get; set; }
        [DataMember]
        public int OTNonMedicareVisits { get; set; }
        [DataMember]
        public int OTMedicarePatients { get; set; }
        [DataMember]
        public int OTNonMedicarePatients { get; set; }

        [DataMember]
        public int STMedicareVisits { get; set; }
        [DataMember]
        public int STNonMedicareVisits { get; set; }
        [DataMember]
        public int STMedicarePatients { get; set; }
        [DataMember]
        public int STNonMedicarePatients { get; set; }

        [DataMember]
        public int MSWMedicareVisits { get; set; }
        [DataMember]
        public int MSWNonMedicareVisits { get; set; }
        [DataMember]
        public int MSWMedicarePatients { get; set; }
        [DataMember]
        public int MSWNonMedicarePatients { get; set; }

        [DataMember]
        public int HHAMedicareVisits { get; set; }
        [DataMember]
        public int HHANonMedicareVisits { get; set; }
        [DataMember]
        public int HHAMedicarePatients { get; set; }
        [DataMember]
        public int HHANonMedicarePatients { get; set; }

        [DataMember]
        public int SNTotalPatients { get; set; }
        [DataMember]
        public int PTTotalPatients { get; set; }
        [DataMember]
        public int OTTotalPatients { get; set; }
        [DataMember]
        public int STTotalPatients { get; set; }
        [DataMember]
        public int MSWTotalPatients { get; set; }
        [DataMember]
        public int HHATotalPatients { get; set; }

        [DataMember]
        public int MedicareUnduplicated { get; set; }
        [DataMember]
        public int NonMedicareUnduplicated { get; set; }
        [DataMember]
        public int TotalUnduplicated { get; set; }
        [DataMember]
        public int HHAMedicareHours { get; set; }
        [DataMember]
        public int HHANonMedicareHours { get; set; }
    }
}
