﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/PrimaryPaymentSourceResult/2012/03/")]
    public class PrimaryPaymentSourceResult : BaseCaliforniaReportResult
    {
        [DataMember]
        public int Visits { get; set; }
    }
}
