﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlRoot("ACCESS-Errors", Namespace = "", IsNullable = false)]
    public class ACCESSErrors : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string footer { get; set; }

        [XmlElement("error", Form = XmlSchemaForm.Unqualified)]
        public ACCESSErrorsError[] error { get; set; }
    }

    [XmlRoot("error", Namespace = "", IsNullable = false)]
    public class ACCESSErrorsError : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string code { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string message { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string httpStatusCode { get; set; }

        [XmlArray(Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItem("detail", typeof(ACCESSErrorsErrorDetailsDetail), Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public ACCESSErrorsErrorDetailsDetail[] details { get; set; }
    }

    public class ACCESSErrorsErrorDetailsDetail : BaseApiEntity
    {
        [XmlAttribute()]
        public string key { get; set; }

        [XmlAttribute()]
        public string value { get; set; }
    }

    [XmlRoot(Namespace = "", IsNullable = false)]
    public class ErrorDataSet : BaseApiEntity
    {
        [XmlElement("ACCESS-Errors")]
        public ACCESSErrors[] Items { get; set; }
    }
}
