﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlRoot(Namespace = "", IsNullable = false)]
    public class hiqhRequest : BaseApiEntity
    {
        [XmlElement("medicareMainframe", Form = XmlSchemaForm.Unqualified)]
        public hiqhRequestMedicareMainframe[] medicareMainframe { get; set; }

        [XmlElement("details", Form = XmlSchemaForm.Unqualified)]
        public hiqhRequestDetails[] details { get; set; }

        [XmlElement("searchCriteria", Form = XmlSchemaForm.Unqualified)]
        public hiqhRequestSearchCriteria[] searchCriteria { get; set; }

        [XmlAttribute()]
        public string mockResponse { get; set; }
    }

    public class hiqhRequestMedicareMainframe : BaseApiEntity
    {
        [XmlElement("application", Form = XmlSchemaForm.Unqualified)]
        public hiqhRequestMedicareMainframeApplication[] application { get; set; }

        [XmlElement("credential", Form = XmlSchemaForm.Unqualified)]
        public hiqhRequestMedicareMainframeCredential[] credential { get; set; }

        [XmlElement("clerkCredential", Form = XmlSchemaForm.Unqualified)]
        public hiqhRequestMedicareMainframeClerkCredential[] clerkCredential { get; set; }

    }

    public class hiqhRequestMedicareMainframeApplication : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string facilityState { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string lineOfBusiness { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string name { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string dataCenter { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string appId { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string pptnRegion { get; set; }
    }

    public class hiqhRequestMedicareMainframeCredential : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string userId { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string password { get; set; }
    }

    public class hiqhRequestMedicareMainframeClerkCredential : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string userId { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string password { get; set; }
    }

    public class hiqhRequestDetails : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string detail { get; set; }
    }

    public class hiqhRequestSearchCriteria : BaseApiEntity
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string hic { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string lastName { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string firstInitial { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string dateOfBirth { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string sex { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string requestorId { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string intermediaryNumber { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string providerIdType { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string providerId { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string hostId { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string applicableDate { get; set; }

        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public string reasonCode { get; set; }
    }

    [XmlRoot(Namespace = "", IsNullable = false)]
    public class RequestDataSet : BaseApiEntity
    {
        [XmlElement("hiqhRequest")]
        public hiqhRequest[] Items { get; set; }
    }
}
