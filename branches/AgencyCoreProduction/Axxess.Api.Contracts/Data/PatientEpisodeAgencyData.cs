﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class PatientEpisodeAgencyData 
    {
        #region agencylocations Table
        public Guid locationId { get; set; }
        public Guid locationAgencyId { get; set; }
        public string locationName { get; set; }
        public int locationPayor { get; set; }
        public string locationNPI { get; set; }
        public string locationMedicarePN { get; set; }
        public string locationMedicaidPN { get; set; }
        public int locationCahpsVendor { get; set; }
        public string locationCahpsVendorId { get; set; }
        public string locationCahpsSurveyDesignator { get; set; }
        public bool locationIsStandAlone { get; set; }
        public string locationZipCode { get; set; }
        #endregion

        #region patients table
        public Guid patientId { get; set; }
        public string patientMRN { get; set; }
        public DateTime patientDOB { get; set; }
        public string patientFirstName { get; set; }
        public string patientLastName { get; set; }
        public string patientMI { get; set; }
        public int patientStatus { get; set; }
        public DateTime patientDischargeDate { get; set; }
        public string patientMedicare { get; set; }
        public string patientMedicaid { get; set; }
        public string patientAddr1 { get; set; }
        public string patientAddr2 { get; set; }
        public string patientCity { get; set; }
        public string patientState { get; set; }
        public string patientZip { get; set; }
        public string patientPhone { get; set; }
        public DateTime patientSOC { get; set; }
        #endregion

        #region patientepisodes table
        public Guid episodeId { get; set; }
        public Guid episodeAdmissionId { get; set; }
        public DateTime episodeStartDate { get; set; }
        public DateTime episodeEndDate { get; set; }
        public string episodeSchedule { get; set; }
        public string episodeDetail { get; set; }
        #endregion
    }
}
