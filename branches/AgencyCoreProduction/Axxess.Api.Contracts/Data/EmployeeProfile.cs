﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;
   
    using System.Collections.Generic;
   
    using System.Xml.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/EmployeeProfile/2013/10/")]
    [XmlRoot("UserProfile")]
    public class EmployeeProfile
    {

        [DataMember]
        public string SSN { get; set; }
        [DataMember]
        public DateTime DOB { get; set; }
        [DataMember]
        public DateTime EmploymentStartDate { get; set; }
        [DataMember]
        public DateTime TerminationDate { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string MaritalStatus { get; set; }
        [DataMember]
        public string Nationality { get; set; }
        [DataMember]
        public string Ethnicity { get; set; }
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public string AddressCity { get; set; }
        [DataMember]
        public string AddressStateCode { get; set; }
        [DataMember]
        public string AddressZipCode { get; set; }
        [DataMember]
        public string PhoneHome { get; set; }
        [DataMember]
        public string PhoneMobile { get; set; }
        [DataMember]
        public string PhoneWork { get; set; }
        [DataMember]
        public string PhoneFax { get; set; }
        [DataMember]
        public string EmailWork { get; set; }
        [DataMember]
        public string EmailPersonal { get; set; }
        [DataMember]
        public bool HasMilitaryService { get; set; }
        [DataMember]
        public string DriversLicenseNumber { get; set; }
        [DataMember]
        public string DriversLicenseStateCode { get; set; }
        [DataMember]
        public DateTime DriversLicenseExpiration { get; set; }
        [DataMember]
        public Guid PhotoAssetId { get; set; }

        //
        //public string AddressFull
        //{
        //    get
        //    {
        //        return string.Format("{0} {1} {2} {3} {4}", this.AddressLine1!=null? this.AddressLine1.Trim() : string.Empty, this.AddressLine2!=null? this.AddressLine2.Trim() : string.Empty, this.AddressCity!=null ? this.AddressCity.Trim() : string.Empty, this.AddressStateCode!=null ? this.AddressStateCode.Trim() : string.Empty, this.AddressZipCode!=null? this.AddressZipCode.Trim() : string.Empty);
        //    }
        //}

        
       
    }
}