﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    [ServiceContract(Namespace = "http://api.axxessweb.com/2010/05/")]
    public interface IGrouperService : IService
    {
        [OperationContract]
        Hipps GetHippsCode(string oasisDataString);
    }
}
