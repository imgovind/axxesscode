﻿namespace Axxess.Scheduled.PlayMakerCRM
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Configuration;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    public static class PlayMakerService
    {
        #region Public Methods

        public static Data CreateDataRequest(bool sandBox, string dataType, string process)
        {
            return new Data
            {
                Key = ConfigurationManager.AppSettings.Get("Key"),
                Pwd = ConfigurationManager.AppSettings.Get("Password"),
                Request = new DataRequest[1] {
                    new DataRequest {
                        Name = process,
                        Method = "Upsert",
                        Object = dataType,
                        Process = process,
                        Test = sandBox ? "true" : "false",
                        Sandbox = sandBox ? "true" : "false",
                        Records = new DataRequestRecords[1] { 
                             new DataRequestRecords {
                                 Record = new List<DataRequestRecordsRecord>().ToArray()
                             }
                        }
                    }
                }
            };
        }

        public static DataRequestRecordsRecord CreateRecord()
        {
            return new DataRequestRecordsRecord
            {
                Fields = new DataRequestRecordsRecordFields[1] {
                    new DataRequestRecordsRecordFields {
                    }
                }
            };
        }

        public static void SendDataRequest(Data data)
        {
            if (data != null)
            {
                try
                {
                    var encoding = new ASCIIEncoding();
                    byte[] requestData = encoding.GetBytes(data.ToXml());
                    string eligibilityUrl = @"https://webservices.playmakercrm.com/index.php?queue_id=1/";
                    Uri eligibilityUri = new Uri(eligibilityUrl, UriKind.Absolute);

                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(eligibilityUri);
                    httpWebRequest.Method = "POST";
                    httpWebRequest.ContentType = "text/xml";
                    httpWebRequest.ContentLength = requestData.Length;

                    using (Stream requestStream = httpWebRequest.GetRequestStream())
                    {
                        requestStream.Write(requestData, 0, requestData.Length);
                    }

                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        using (var responseStream = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            var responseText = responseStream.ReadToEnd();
                            if (responseText.IsNotNullOrEmpty())
                            {
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.ToString());
                }
            }
        }

        #endregion

        #region Private Members and Methods

        #endregion
    }
}
