﻿namespace Axxess.Scheduled.PlayMakerCRM
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;

    using Axxess.Core.Extension;
    using System.Configuration;

    public static class PlayMakerService
    {
        #region Public Methods

        public static Data CreateDataRequest(bool sandBox)
        {
            return new Data
            {
                Key = ConfigurationManager.AppSettings.Get("Key"),
                Pwd = ConfigurationManager.AppSettings.Get("Password"),
                Request = new DataRequest[1] {
                    new DataRequest {
                        Records = new DataRequestRecords[] { }
                    }
                },
                Response = new DataResponse[] { }
            };
        }


        public static DataRequestRecordsRecordFieldsField CreateField(string key, string name, string value)
        {
            return new DataRequestRecordsRecordFieldsField
            {
                Key = key,
                Name = name,
                Value = value
            };
        }

        public static void SendDataRequest(Data data)
        {
            if (data != null)
            {
                try
                {
                    var encoding = new ASCIIEncoding();
                    byte[] requestData = encoding.GetBytes(data.ToXml());
                    string eligibilityUrl = @"https://webservices.playmakercrm.com/index.php?queue_id=1/";
                    Uri eligibilityUri = new Uri(eligibilityUrl, UriKind.Absolute);

                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(eligibilityUri);
                    httpWebRequest.Method = "POST";
                    httpWebRequest.ContentType = "text/xml";
                    httpWebRequest.ContentLength = requestData.Length;

                    using (Stream requestStream = httpWebRequest.GetRequestStream())
                    {
                        requestStream.Write(requestData, 0, requestData.Length);
                    }

                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        using (var responseStream = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            var responseText = responseStream.ReadToEnd();
                            if (responseText.IsNotNullOrEmpty())
                            {
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.ToString());
                }
            }
        }

        #endregion

        #region Private Members and Methods

        #endregion
    }
}
