﻿
namespace Axxess.Core.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;

    public enum Tables
    {

        [CustomDescription("Agency", "agencies")]
        Agencies = 1,
        [CustomDescription("Agency Location", "agencylocations")]
        Agencylocations = 2,
        [CustomDescription("Agency Contact", "agencycontacts")]
        AgencyContacts = 3,
        [CustomDescription("Agency Hospital", "agencyhospitals")]
        AgencyHospitals = 4,
        [CustomDescription("Agency Insurance", "agencyinsurances")]
        AgencyInsurances = 5,
        [CustomDescription("Agency Medicare Insurance", "agencymedicareinsurances")]
        AgencyMedicareInsurances = 6,
        [CustomDescription("Agency Physician", "agencyphysicians")]
        AgencyPhysicians = 7,
        [CustomDescription("Agency Supply", "agencysupplies")]
        AgencySupplies = 8,
        [CustomDescription("Agency Template", "agencytemplates")]
        AgencyTemplates = 9,
        [CustomDescription("Agency Adjustment Code", "agencyadjustmentcodes")]
        AgencyAdjustmentCodes = 10,
        [CustomDescription("Agency System Message", "agencysystemmessages")]
        AgencySystemMessages = 11,
        [CustomDescription("Agency teams", "agencyteams")]
        AgencyTeams = 12,
        [CustomDescription("License Item", "licenseitems")]
        LicenseItems = 13,
        [CustomDescription("Upload Type", "uploadtypes")]
        UploadTypes = 14,

        //User and Only User dependent 
        [CustomDescription("User", "users")]
        Users = 100,

        //Agency, Referral  dependent 
        [CustomDescription("Referral", "referrals")]
        Referrals = 150,
        [CustomDescription("Referral Emergency Contact", "referralemergencycontacts")]
        ReferralEmergencyContacts = 151,

        //Agency,Patient and Only Patient dependent 
        [CustomDescription("Patient", "patients")]
        Patients = 200,
        [CustomDescription("Patient Physician", "patientphysicians")]
        PatientPhysicians = 201,
        [CustomDescription("Patient Teams", "patientteams")]
        PatientTeams = 202,
        [CustomDescription("Patient User", "patientusers")]
        PatientUsers = 203,
        [CustomDescription("Patient Admission Date", "patientadmissiondates")]
        PatientAdmissionDates = 204,
        [CustomDescription("Allergy Profile", "allergyprofiles")]
        AllergyProfiles = 205,
        [CustomDescription("Medication Profile", "medicationprofiles")]
        MedicationProfile = 206,
        [CustomDescription("Medication Profile History", "medicationprofilehistories")]
        MedicationProfileHistories = 207,
        [CustomDescription("Patient Emergency Contact", "patientemergencycontacts")]
        PatientEmergencyContacts = 208,
        [CustomDescription("Authorization", "authorizations")]
        Authorizations = 209,
        [CustomDescription("Hospitalization Log", "hospitalizationlogs")]
        HospitalizationLogs = 210,
        [CustomDescription("Patient Document", "patientdocuments")]
        PatientDocuments = 211,
        [CustomDescription("Medicare Eligibility", "medicareeligibilities")]
        MedicareEligibilities = 212,
        [CustomDescription("Medicare Eligibility Summary", "medicareeligibilitysummaries")]
        MedicareEligibilitySummaries = 213,

        [CustomDescription("Patient Episode", "patientepisodes")]
        PatientEpisodes = 300,

        [CustomDescription("Schedule Event", "scheduleevents")]
        ScheduleEvents = 350,

        //Agency, Pateint,Episode,User, Physician  dependent 
        //Agency, Pateint, User and Epiosode dependent 
        [CustomDescription("Patient Visit Note", "patientvisitnotes")]
        PatientVisitNotes = 400,
        [CustomDescription("Assessment", "assessments")]
        Assessments = 410,
        [CustomDescription("Physician Order", "physicianorders")]
        PhysicianOrders = 420,
        [CustomDescription("Plan Of Care", "planofcares")]
        PlanOfCares = 425,
        [CustomDescription("Plan Of Care Stand Alone", "planofcarestandalones")]
        PlanOfCareAtandAlones = 430,
        [CustomDescription("Communication Note", "communicationnotes")]
        CommunicationNotes = 435,
        [CustomDescription("Face To Face Encounter", "facetofaceencounters")]
        FaceToFaceEncounters = 440,

        [CustomDescription("Incident", "incidents")]
        Incidents = 445,
        [CustomDescription("Infection", "infections")]
        Infections = 450,

        [CustomDescription("Return Comment", "returncomments")]
        ReturnComments = 500,

        //Agency, Pateint, Episode and Only Episode dependent 

        [CustomDescription("Missed Visit", "missedvisits")]
        MissedVisits = 525,

        [CustomDescription("Asset", "assets")]
        Assets = 530,

        [CustomDescription("Rap", "raps")]
        Raps = 600,
        [CustomDescription("Rap SnapShot", "rapsnapshots")]
        RapSnapShots = 601,

        [CustomDescription("Final", "finals")]
        Finals = 650,
        [CustomDescription("Final SnapShot", "finalsnapshots")]
        FinalSnapShots = 651,

        [CustomDescription("Managed Claim", "managedclaims")]
        ManagedClaims = 700,
        [CustomDescription("Managed Claim Adjustment", "managedclaimadjustments")]
        ManagedClaimAdjustments = 710,
        [CustomDescription("Managed Claim Payment", "managedclaimpayments")]
        ManagedClaimPayments = 720,

        //Agency, Pateint, Physician  dependent 
        //Agency, Pateint,Episode,Claim  dependent 
        [CustomDescription("Secondary Claim", "secondaryclaims")]
        SecondaryClaims = 750,

        [CustomDescription("Claim Data", "claimdatas")]
        ClaimDatas = 800,

        [CustomDescription("Remittance Queues", "remitqueues")]
        RemitQueues = 825,
        [CustomDescription("Remittance", "remittances")]
        Remittances = 826,

        [CustomDescription("Report", "reports")]
        Reports = 850,

        //Agency, Pateint, User  dependent 

        [CustomDescription("Message Folder", "messagefolders")]
        MessageFolders = 900,
        [CustomDescription("Message", "messages")]
        Messages = 901,
        [CustomDescription("Message Details", "messagedetails")]
        MessageDetails = 902,

        [CustomDescription("CarePlan OverSight", "careplanoversights")]
        CarePlanOverSights = 1000,
        [CustomDescription("Shp Data Login", "shpdatalogins")]
        ShpDataLogins = 1005,
        [CustomDescription("Shp Data Batche", "shpdatabatches")]
        ShpDataBatches = 1006
    }
}
