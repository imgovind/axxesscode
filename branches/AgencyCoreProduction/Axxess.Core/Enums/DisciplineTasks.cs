﻿namespace Axxess.Core.Enums
{
    using System.ComponentModel;
    using Axxess.Core.Infrastructure;

    public enum DisciplineTasks
    {
        [CustomDescription("No Discipline", "No Discipline", "None", "None", "None", "None")]
        NoDiscipline = 0,
        [CustomDescription("Skilled Nurse Visit", "SN Visit", "SN", "Notes", "SN", "Skilled Nurse Visit")]
        SkilledNurseVisit = 1,
        [CustomDescription("OASIS-B Follow-up", "Follow-up", "OASIS", "Assessment", "OASISBFollowUp", "OASIS-B Follow-up Visit")]
        OASISBFollowUp = 2,
        [CustomDescription("OASIS-B Recertification", "Recert.", "OASIS", "Assessment", "OASISBRecertification", "OASIS-B Recertification Visit")]
        OASISBRecertification = 3,
        [CustomDescription("OASIS-B Resumption of Care", "ROC", "OASIS", "Assessment", "OASISBResumptionofCare", "OASIS-B Resumption of Care Visit")]
        OASISBResumptionofCare = 4,
        [CustomDescription("OASIS-B Discharge", "Discharge", "OASIS", "Assessment", "OASISBDischarge", "OASIS-B Discharge")]
        OASISBDischarge = 10,
        [CustomDescription("OASIS-B Start of Care", "SOC", "OASIS", "Assessment", "OASISBStartofCare", "OASIS-B Start of Care Visit")]
        OASISBStartofCare = 11,
        [CustomDescription("OASIS-B Transfer", "Transfer", "OASIS", "Assessment", "OASISBTransfer", "")]
        OASISBTransfer = 12,
        [CustomDescription("OASIS B Death at Home", "Death", "OASIS", "Assessment", "OASISBDeathatHome", "")]
        OASISBDeathatHome = 15,
        [CustomDescription("OASIS-C Death", "OASIS Death", "OASIS", "Assessment", "OASISCDeath", "OASIS-C Death")]
        OASISCDeath = 5,
        [CustomDescription("OASIS-C Discharge", "OASIS D/C", "OASIS", "Assessment", "OASISCDischarge", "OASIS-C Discharge Visit")]
        OASISCDischarge = 6,
        [CustomDescription("OASIS-C Follow-up", "OASIS F/U", "OASIS", "Assessment", "OASISCFollowUp", "OASIS-C Follow-up Visit")]
        OASISCFollowUp = 7,
        [CustomDescription("OASIS-C Recertification", "OASIS Recert", "OASIS", "Assessment", "OASISCRecertification", "OASIS-C Recertification Visit")]
        OASISCRecertification = 8,
        [CustomDescription("OASIS-C Resumption of Care", "OASIS ROC", "OASIS", "Assessment", "OASISCResumptionofCare", "OASIS-C Resumption of Care Visit")]
        OASISCResumptionofCare = 9,
        [CustomDescription("OASIS-C Start of Care", "OASIS SOC", "OASIS", "Assessment", "OASISCStartofCare", "OASIS-C Start of Care Visit")]
        OASISCStartofCare = 13,
        [CustomDescription("OASIS-C Transfer", "OASIS TRFR", "OASIS", "Assessment", "OASISCTransfer", "OASIS-C Transfer")]
        OASISCTransfer = 14,
        [CustomDescription("OASIS-C Transfer Discharge", "OASIS TRFR D/C", "OASIS", "Assessment", "OASISCTransferDischarge", "OASIS-C Transfer Discharge")]
        OASISCTransferDischarge = 88,
        [CustomDescription("SN Insulin AM Visit", "SN Insulin AM", "SN", "Notes", "SN", "SN Insulin - AM Visit")]
        SNInsulinAM = 16,
        [CustomDescription("SN Insulin PM Visit", "SN Insulin PM", "SN", "Notes", "SN", "SN Insulin - PM Visit")]
        SNInsulinPM = 17,
        [CustomDescription("SN Discharge Summary", "SN D/C SUM", "SN", "Notes", "DS", "SN Discharge Summary")]
        DischargeSummary = 18,
        [CustomDescription("SN Foley Cath Change", "SN FOLEY", "SN", "Notes", "SN", "Foley Cath Change Visit")]
        FoleyCathChange = 19,
        [CustomDescription("LVN/LPN Supervisory Visit", "LVN Sup Visit", "SN", "Notes", "LVNSupervisoryVisit", "LVN Supervisory Visit")]
        LVNSupervisoryVisit = 20,
        [CustomDescription("SN B12 Injection Visit", "SN B12 INJ", "SN", "Notes", "SN", "SN B12 Injection Visit")]
        SNB12INJ = 21,
        [CustomDescription("SN BMP Visit", "SN BMP", "SN", "Notes", "SN", "SN BMP Visit")]
        SNBMP = 22,
        [CustomDescription("SN CBC Visit", "SN CBC", "SN", "Notes", "SN", "SN CBC Visit")]
        SNCBC = 23,
        [CustomDescription("SN Haldol Injection Visit", "SN HALDOL", "SN", "Notes", "SN", "SN Haldol Injection Visit")]
        SNHaldolInj = 24,
        [CustomDescription("SN PICC/Midline Placement Visit", "SN PICC/MIDLINE", "SN", "Notes", "SN", "PICC/Midline Placement Visit")]
        PICCMidlinePlacement = 25,
        [CustomDescription("SN PRN Foley Change Visit", "SN PRN FOLEY", "SN", "Notes", "SN", "PRN Foley Change Visit")]
        PRNFoleyChange = 26,
        [CustomDescription("SN PRN Visit", "SN PRN", "SN", "Notes", "SN", "SN PRN Visit")]
        PRNSNV = 27,
        [CustomDescription("SN PRN VP for CMP Visit", "SN PRN VP CMP", "SN", "Notes", "SN", "PRN VP for CMP Visit")]
        PRNVPforCMP = 28,
        [CustomDescription("PT w/ INR", "PT W/ INR", "PT", "Notes", "PTWithINR", "PT w/ INR Visit")]
        PTWithINR = 29,
        [CustomDescription("PT w/ INR PRN", "PT W/ INR PRN", "SN", "Notes", "SN", "PT w/ INR PRN SNV Visit")]
        PTWithINRPRNSNV = 30,
        [CustomDescription("SN Home Infusion SD Visit", "SN HOME INFUSION", "SN", "Notes", "SN", "SN Home Infusion SD Visit")]
        SkilledNurseHomeInfusionSD = 31,
        [CustomDescription("SN Home Infusion SD Additional Visit", "SN HOME INFUSION ADDT", "SN", "Notes", "SN", "SN Home Infusion SD Additional Visit")]
        SkilledNurseHomeInfusionSDAdditional = 32,
        [CustomDescription("SN Assessment (Start of Care)", "SN ASSESSMENT", "SN", "Notes", "SNAssessment", "SN Assessment Visit")]
        SNAssessment = 33,
        [CustomDescription("SN DC Visit", "SN D/C", "SN", "Notes", "SN", "SN DC Visit")]
        SNDC = 34,
        [CustomDescription("SN Evaluation Visit", "SN EVAL", "SN", "Notes", "SN", "SN Evaluation Visit")]
        SNEvaluation = 35,
        [CustomDescription("SN Foley Labs Visit", "SN FOLEY LAB", "SN", "Notes", "SN", "SN Foley Labs Visit")]
        SNFoleyLabs = 36,
        [CustomDescription("SN Foley Change Visit", "SN FOLEY CHANGE", "SN", "Notes", "SN", "SN Foley Change Visit")]
        SNFoleyChange = 37,
        [CustomDescription("SN Injection Visit", "SN INJ", "SN", "Notes", "SN", "SN Injection Visit")]
        SNInjection = 38,
        [CustomDescription("SN Injection/Labs Visit", "SN INJ/LAB", "SN", "Notes", "SN", "SN Injection/Labs Visit")]
        SNInjectionLabs = 39,
        [CustomDescription("SN Labs Visit", "SN LAB", "SN", "Notes", "SN", "SN Labs Visit")]
        SNLabsSN = 40,
        [CustomDescription("SN Psychiatric Nurse Visit", "SN PSYCH", "SN", "Notes", "SN", "SN Psychiatric Nurse Visit")]
        SNVPsychNurse = 41,
        [CustomDescription("SN with Aide Supervision Visit", "SN AIDE SUP", "SN", "Notes", "SN", "SN with Aide Supervision Visit")]
        SNVwithAideSupervision = 42,
        [CustomDescription("SN DC Planning Visit", "SN D/C PLAN", "SN", "Notes", "SN", "SN DC Planning Visit")]
        SNVDCPlanning = 43,
        [CustomDescription("PT Evaluation", "PT EVAL", "PT", "Notes", "PTNotes", "PT Evaluation Visit" )]
        PTEvaluation = 44,
        [CustomDescription("PT Visit", "PT VISIT", "PT", "Notes", "PTVisit", "PT Visit")]
        PTVisit = 45,
        [CustomDescription("PT Discharge", "PT D/C", "PT", "Notes", "PTDischarge", "PT Discharge")]
        PTDischarge = 46,
        [CustomDescription("OT Evaluation", "OT EVAL", "OT", "Notes", "OTNotes", "OT Evaluation Visit")]
        OTEvaluation = 47,
        [CustomDescription("OT Re-Evaluation", "OT REEVAL", "OT", "Notes", "OTNotes", "OT Re-Evaluation Visit")]
        OTReEvaluation = 48,
        [CustomDescription("OT Visit", "OT VISIT", "OT", "Notes", "OTVisit", "OT Visit")]
        OTVisit = 49,
        [CustomDescription("ST Visit", "ST VISIT", "ST", "Notes", "STVisit", "ST Visit")]
        STVisit = 50,
        [CustomDescription("ST Evaluation", "ST EVAL", "ST", "Notes", "STNotes", "ST Evaluation Visit")]
        STEvaluation = 51,
        [CustomDescription("ST Discharge", "ST D/C", "ST", "Notes", "STNotes", "ST Discharge")]
        STDischarge = 52,
        [CustomDescription("MSW Evaluation", "MSW EVAL", "MSW", "Notes", "MSWEvaluationAssessment", "MSW Evaluation Visit")]
        MSWEvaluationAssessment = 53,
        [CustomDescription("HHA Visit", "HHA VISIT", "HHA", "Notes", "HHAideVisit", "HHA Visit")]
        HHAideVisit = 54,
        [CustomDescription("HHA Supervisory Visit", "HHA SUP", "SN", "Notes", "HHAideSupervisoryVisit", "HHA Supervisory Visit")]
        HHAideSupervisoryVisit = 55,
        [CustomDescription("MSW Visit", "MSW VISIT", "MSW", "Notes", "MSWVisit", "MSW Visit")]
        MSWVisit = 56,
        [CustomDescription("MSW Discharge", "MSW D/C", "MSW", "Notes", "MSWDischarge", "MSW Discharge")]
        MSWDischarge = 57,
        [CustomDescription("Dietician Visit", "DIET VISIT", "DV", "Notes", "DieticianVisit", "Dietician Visit")]
        DieticianVisit = 58,
        [CustomDescription("PTA Visit", "PTA VISIT", "PT", "Notes", "PTVisit", "PTA Visit")]
        PTAVisit = 59,
        [CustomDescription("PT Re-Evaluation", "PT REEVAL", "PT", "Notes", "PTNotes", "PT Re-Evaluation Visit")]
        PTReEvaluation = 60,
        [CustomDescription("OASIS-C Start of Care (PT)", "OASIS SOC PT", "OASIS", "Assessment", "OASISCStartofCarePT", "OASIS-C Start of Care (PT) Visit")]
        OASISCStartofCarePT = 61,
        [CustomDescription("OASIS-C Resumption of Care (PT)", "OASIS ROC PT", "OASIS", "Assessment", "OASISCResumptionofCarePT", "OASIS-C Resumption of Care (PT) Visit")]
        OASISCResumptionofCarePT = 62,
        [CustomDescription("OASIS-C Death (PT)", "OASIS Death PT", "OASIS", "Assessment", "OASISCDeathPT", "OASIS-C Death (PT)")]
        OASISCDeathPT = 63,
        [CustomDescription("OASIS-C Discharge (PT)", "OASIS D/C PT", "OASIS", "Assessment", "OASISCDischargePT", "OASIS-C Discharge (PT)")]
        OASISCDischargePT = 64,
        [CustomDescription("OASIS-C Follow-up (PT)", "OASIS F/U PT", "OASIS", "Assessment", "OASISCFollowupPT", "OASIS-C Follow-up (PT) Visit")]
        OASISCFollowupPT = 65,
        [CustomDescription("OASIS-C Recertification (PT)", "OASIS RECERT PT", "OASIS", "Assessment", "OASISCRecertificationPT", "OASIS-C Recertification (PT) Visit")]
        OASISCRecertificationPT = 66,
        [CustomDescription("OASIS-C Transfer (PT)", "OASIS TRFR PT", "OASIS", "Assessment", "OASISCTransferPT", "OASIS-C Transfer (PT)")]
        OASISCTransferPT = 67,
        [CustomDescription("COTA Visit", "COTA VISIT", "OT", "Notes", "OTVisit", "COTA Visit")]
        COTAVisit = 68,
        [CustomDescription("OASIS-C Start of Care (OT)", "OASIS SOC OT", "OASIS", "Assessment", "OASISCStartofCareOT", "OASIS-C Start of Care (OT) Visit")]
        OASISCStartofCareOT = 112,
        [CustomDescription("OASIS-C Resumption of Care (OT)", "OASIS ROC OT", "OASIS", "Assessment", "OASISCResumptionofCareOT", "OASIS-C Resumption of Care (OT) Visit")]
        OASISCResumptionofCareOT = 69,
        [CustomDescription("OASIS-C Recertification (OT)", "OASIS RECERT OT", "OASIS", "Assessment", "OASISCRecertificationOT", "OASIS-C Recertification (OT) Visit")]
        OASISCRecertificationOT = 73,
        [CustomDescription("OASIS-C Death (OT)", "OASIS DEATH OT", "OASIS", "Assessment", "OASISCDeathOT", "OASIS-C Death (OT)")]
        OASISCDeathOT = 70,
        [CustomDescription("OASIS-C Discharge (OT)", "OASIS D/C OT", "OASIS", "Assessment", "OASISCDischargeOT", "OASIS-C Discharge (OT)")]
        OASISCDischargeOT = 71,
        [CustomDescription("OASIS-C Follow-up (OT)", "OASIS F/U OT", "OASIS", "Assessment", "OASISCFollowupOT", "OASIS-C Follow-up (OT) Visit")]
        OASISCFollowupOT = 72,
        [CustomDescription("OASIS-C Transfer (OT)", "OASIS TRFR OT", "OASIS", "Assessment", "OASISCTransferOT", "OASIS-C Transfer (OT)")]
        OASISCTransferOT = 74,
        [CustomDescription("HHA Care Plan", "HHA CAREPLAN", "HHA", "Notes", "HHAideCarePlan", "HHAide Care Plan")]
        HHAideCarePlan = 75,
        [CustomDescription("MSW Assessment", "MSW ASSESSMENT", "MSW", "Notes", "MSWAssessment", "MSW Assessment Visit")]
        MSWAssessment = 76,
        [CustomDescription("MSW Progress Note", "MSW VISIT", "MSW", "Notes", "MSWProgressNote", "MSW Progress Visit")]
        MSWProgressNote = 77,
        [CustomDescription("485 Plan of Care (From Assessment)", "485 POC", "485", "Orders", "HCFA485", "485 Plan of Care (From OASIS Assessment)")]
        HCFA485 = 78,
        [CustomDescription("486 Plan Of Care", "486 ADDENDUM", "486", "Orders", "HCFA486", "HCFA 486")]
        HCFA486 = 79,
        [CustomDescription("Physician Order", "PHY ORDER", "Order", "Orders", "PhysicianOrder", "Physician Order")]
        PhysicianOrder = 80,
        [CustomDescription("Physician Order", "POST HOSP ORDER", "Order", "Orders", "PostHospitalizationOrder", "Post Hospitalization Order")]
        PostHospitalizationOrder = 81,
        [CustomDescription("Medicaid POC", "MEDICAID POC", "Order", "Orders", "MedicaidPOC", "Medicaid POC")]
        MedicaidPOC = 82,
        [CustomDescription("RAP", "RAP", "Claim", "Claims", "RAP", "RAP")]
        Rap = 83,
        [CustomDescription("Final", "EOE", "Claim", "Claims", "Final", "Final")]
        Final = 84,
        [CustomDescription("60-Day Summary/Case Conference", "60 DAY SUM", "SN", "Notes", "SixtyDaySummary", "60 Day Summary/Case Conference")]
        SixtyDaySummary = 85,
        [CustomDescription("Transfer Summary", "TRFR SUM", "SN", "Notes", "TransferSummary", "Transfer Summary")]
        TransferSummary = 86,
        [CustomDescription("Communication Note", "COMM NOTE", "Notes", "Notes", "CommunicationNote", "Communication Note")]
        CommunicationNote = 87,
        [CustomDescription("Non-OASIS Start of Care", "NONOASIS SOC", "NonOASIS", "Assessment", "NonOASISStartofCare", "Non-OASIS Start of Care Visit")]
        NonOASISStartofCare = 89,
        [CustomDescription("Non-OASIS Recertification", "NONOASIS RECERT", "NonOASIS", "Assessment", "NonOASISRecertification", "Non-OASIS Recertification Visit")]
        NonOASISRecertification = 90,
        [CustomDescription("Non-OASIS Discharge", "NONOASIS D/c", "NonOASIS", "Assessment", "NonOASISDischarge", "Non-OASIS Discharge")]
        NonOASISDischarge = 91,
        [CustomDescription("Non-OASIS Plan of Care", "NONOASIS 485 POC", "NonOasis485", "Orders", "NonOasisHCFA485", "Non-OASIS HCFA 485")]
        NonOasisHCFA485 = 92,
        [CustomDescription("Incident/Accident Log", "INC/ACC LOG", "ReportsAndNotes", "ReportsAndNotes", "IncidentAccidentReport", "Incident / Accident Log")]
        IncidentAccidentReport = 93,
        [CustomDescription("Infection Log", "INF LOG", "ReportsAndNotes", "ReportsAndNotes", "InfectionReport", "Infection Log")]
        InfectionReport = 94,
        [CustomDescription("SN Teaching/Training Visit", "SN TEACH/TRAIN", "SN", "Notes", "SN", "SN Teaching/Training Visit")]
        SNVTeachingTraining = 95,
        [CustomDescription("SN Observation & Assessment Visit", "SN OBSERVE/ASSESS", "SN", "Notes", "SN Observation & Assessment Visit", "SN Observation & Assessment Visit")]
        SNVObservationAndAssessment = 96,
        [CustomDescription("SN Management & Evaluation Visit", "SN MANAGE/EVAL", "SN", "Notes", "SN Management & Evaluation Visit")]
        SNVManagementAndEvaluation = 97,
        [CustomDescription("Plan of Treatment/Care", "POC/TREATMENT", "485", "Orders", "HCFA485StandAlone", "Plan of Treatment/Care")]
        HCFA485StandAlone = 98,
        [CustomDescription("PAS Note", "PAS", "HHA", "Notes", "PASVisit", "PAS Visit")]
        PASVisit = 99,
        [CustomDescription("PAS Care Plan", "PAS CAREPLAN", "HHA", "Notes", "PASCarePlan", "PAS Care Plan")]
        PASCarePlan = 100,
        [CustomDescription("Skilled Nurse Assessment (Recertification)", "SN RECERT ASSESS", "SN", "Notes", "SNAssessmentRecert", "SN Assessment (Recertification) Visit")]
        SNAssessmentRecert = 101,
        [CustomDescription("Physician Face-to-face Encounter", "PHY F2F ENC", "Order", "Orders", "FaceToFaceEncounter", "FaceToFaceEncounter")]
        FaceToFaceEncounter = 102,
        [CustomDescription("OT Discharge", "OT D/C", "OT", "Notes", "OTNotes", "OT Discharge")]
        OTDischarge = 103,
        [CustomDescription("ST Re-Evaluation", "ST REEVAL", "ST", "Notes", "STNotes", "ST Re-Evaluation Visit")]
        STReEvaluation = 104,
        [CustomDescription("PT Maintenance Visit", "PT MAINT", "PT", "Notes", "PTNotes", "PT Maintenance Visit")]
        PTMaintenance = 105,
        [CustomDescription("OT Maintenance Visit", "OT MAINT", "OT", "Notes", "OTNotes", "OT Maintenance Visit")]
        OTMaintenance = 106,
        [CustomDescription("ST Maintenance Visit", "ST MAINT", "ST", "Notes", "STNotes", "ST Maintenance Visit")]
        STMaintenance = 107,
        [CustomDescription("Medicare Eligibility Report", "MED ELIG REPORT", "MER", "ReportsAndNotes", "MedicareEligibilityReport", "Medicare Eligibility Report")]
        MedicareEligibilityReport = 108,
        [CustomDescription("Coordination of Care", "COC", "SN", "Notes", "CoordinationOfCare", "Coordination Of Care")]
        CoordinationOfCare = 109,
        [CustomDescription("Driver/Transportation Log", "DRV LOG", "MSW", "Notes", "DriverOrTransportationNote", "Driver/Transportation Log")]
        DriverOrTransportationNote = 110,
        [CustomDescription("SN Diabetic Daily Visit", "SN DIABETIC", "SN", "Notes", "SN", "SN Diabetic Daily Visit")]
        SNDiabeticDailyVisit = 111,
        [CustomDescription("UAP Wound Care Note", "UAP WOUND", "HHA", "Notes", "UAP", "UAP Wound Care Visit")]
        UAPWoundCareVisit = 113,
        [CustomDescription("UAP Insulin Prep-Aministration Note", "UAP INSULIN PREP", "HHA", "Notes", "UAP", "UAP Insulin Prep-Aministration Visit")]
        UAPInsulinPrepAdminVisit = 114,
        [CustomDescription("Home Maker Note", "HOME MAKER", "HHA", "Notes", "HomeMakerNote", "Home Maker Visit")]
        HomeMakerNote = 115,
        [CustomDescription("PT Discharge Summary", "PT D/C SUM", "PT", "Notes", "PTDS", "PT Discharge Summary")]
        PTDischargeSummary = 116,
        [CustomDescription("OT Discharge Summary", "OT D/C SUM", "OT", "Notes", "OTDS", "OT Discharge Summary")]
        OTDischargeSummary = 117,
        [CustomDescription("OASIS-C Transfer Discharge (PT)", "OASIS TRFR D/C PT", "OASIS", "Assessment", "OASISCTransferDischargePT", "OASIS-C Transfer Discharge (PT)")]
        OASISCTransferDischargePT = 118,
        [CustomDescription("SN Pediatric Visit", "SN PEDIATRIC", "SN", "Notes", "SN", "Skilled Nursing Pediatric Visit")]
        SNPediatricVisit = 119,
        [CustomDescription("Lab", "LAB", "SN", "Notes", "Lab", "Lab")]
        Labs = 120,
        [CustomDescription("PTA Supervisory Visit", "PTA SUP VISIT", "PT", "Notes", "PTSupervisoryVisit", "PTA Supervisory Visit")]
        PTSupervisoryVisit = 121,
        [CustomDescription("COTA Supervisory Visit", "COTA SUP VISIT", "OT", "Notes", "OTSupervisoryVisit", "COTA Supervisory Visit")]
        OTSupervisoryVisit = 122,
        [CustomDescription("PT Reassessment", "PT REASSESS", "PT", "Notes", "PTReassessment", "PT Reassessment Visit")]
        PTReassessment = 123,
        [CustomDescription("OT Reassessment", "OT REASSESS", "OT", "Notes", "OTReassessment", "OT Reassessment Visit")]
        OTReassessment = 124,
        [CustomDescription("Initial Summary of Care", "INIT SUM OF CARE", "SN", "Notes", "SN", "Initial Summary of Care")]
        InitialSummaryOfCare = 125,
        [CustomDescription("SN Insulin Noon Visit", "SN INSULIN NOON", "SN", "Notes", "SN", "SN Insulin - Noon Visit")]
        SNInsulinNoon = 126,
        [CustomDescription("SN Insulin HS Visit", "SN INSULIN HS", "SN", "Notes", "SN", "SN Insulin - HS Visit")]
        SNInsulinHS = 127,
        [CustomDescription("SN Pediatric Assessment", "SN PEDIAT ASSESS", "SN", "Notes", "SN", "SN Pediatric Assessment Visit")]
        SNPediatricAssessment = 128,
        [CustomDescription("SN Psychiatric Nurse Assessment", "SN PSYCH ASSESS", "SN", "Notes", "SN", "SN Psychiatric Nurse Assessment Visit")]
        SNPsychAssessment = 129,
        [CustomDescription("ST Reassessment", "ST REASSESS", "ST", "Notes", "STNotes", "ST Reassessment Visit")]
        STReassessment = 130,
        [CustomDescription("PAS Travel Note", "PAS TRAVEL", "HHA", "Notes", "PASTravel", "PAS Travel Note")]
        PASTravel = 131,
        [CustomDescription("Nutritional Assessment Form", "NUTRI ASSESS", "DV", "Notes", "NutritionalAssessment", "Nutritional Assessment Visit")]
        NutritionalAssessment = 132,
        [CustomDescription("ST Discharge Summary", "ST D/C SUM", "ST", "Notes", "STDS", "ST Discharge Summary")]
        STDischargeSummary = 133,
        [CustomDescription("OASIS-C Discharge (ST)", "OASIS D/C ST", "OASIS", "Assessment", "OASISCDischargeST", "OASIS-C Discharge (ST)")]
        OASISCDischargeST = 134,
        [CustomDescription("OT Plan Of Care", "OT POC", "Order", "Orders", "OTPlanOfCare", "OT Plan Of Care")]
        OTPlanOfCare = 135,
        [CustomDescription("ST Plan Of Care", "ST POC", "Order", "Orders", "STPlanOfCare", "ST Plan Of Care")]
        STPlanOfCare = 136,
        [CustomDescription("PT Plan Of Care", "PT POC", "Order", "Orders", "PTPlanOfCare", "PT Plan Of Care")]
        PTPlanOfCare = 137,
        [CustomDescription("SN Wound Care Visit", "SN WOUND CARE", "SN", "Notes", "SN", "SN Wound Care Visit")]
        SNWoundCare = 138,
        [CustomDescription("LVN/LPN Visit", "LVN/LPN Visit", "SN", "Notes", "LVN/LPN Visit")]
        LVNVisit = 139,
        [CustomDescription("30-Day Summary/Case Conference", "30 DAY SUM", "SN", "Notes", "ThirtyDaySummary", "30 Day Summary/Case Conference")]
        ThirtyDaySummary = 140,
        [CustomDescription("10-Day Summary/Case Conference", "10 DAY SUM", "SN", "Notes", "TenDaySummary", "10 Day Summary/Case Conference")]
        TenDaySummary = 141,
        [CustomDescription("OASIS-C Recertification (ST)", "OASIS RECERT ST", "OASIS", "Assessment", "OASISCRecertificationST", "OASIS-C Recertification (ST) Visit")]
        OASISCRecertificationST = 142,
    }
}
