﻿namespace Axxess.Core.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;
    using System.ComponentModel;

    public enum BillingStatus
    {
        [Description("Created")]
        ClaimCreated = 300,
        [Description("ReOpenend")]
        ClaimReOpen = 301,
        [Description("Submitted")]
        ClaimSubmitted = 305,
        [Description("Rejected")]
        ClaimRejected = 310,
        [Description("Payment Pending")]
        ClaimPaymentPending = 315,
        [Description("Accepted/Processing")]
        ClaimAccepted = 320,
        [Description("Returned")]
        ClaimWithErrors = 325,
        [Description("Paid")]
        ClaimPaidClaim = 330,
        [Description("Cancelled")]
        ClaimCancelledClaim = 335,
        [Description("Processed as Primary")]
        ProcessedAsPrimary = 1,
        [Description("Processed as Secondary")]
        ProcessedAsSecondary = 2,
        [Description("Processed as Tertiary")]
        ProcessedAsTertiary = 3,
        [Description("Denied")]
        Denied = 4,
        [Description("Pended")]
        Pended = 5,
        [Description("Received, but not in process")]
        ReceivedNotInProcess = 10,
        [Description("Suspended")]
        Suspended = 13,
        [Description("Suspended - investigation with field")]
        SuspendedInvestigationWithField = 15,
        [Description("Suspended - return with material")]
        SuspendedReturnWithMaterial = 16,
        [Description("Suspended - review pending")]
        SuspendedReviewPending = 17,
        [Description("Processed as Primary")]
        ProcessedAsPrimaryFTAP = 19, //FTAP=>Forwarded to Additional Payer(s)
        [Description("Processed as Secondary")]
        ProcessedAsSecondaryFTAP = 20,
        [Description("Processed as Tertiary")]
        ProcessedAsTertiaryFTAP = 21,
        [Description("Reversal of Previous Payment")]
        ReversalOfPreviousPayment = 22,
        [Description("Not Our Claim, Forwarded to Additional Payer(s)")]
        NotOurClaimFTAP = 23,
        [Description("Predetermination Pricing Only - No Payment")]
        PredeterminationPricingOnly = 25,
        [Description("Reviewed")]
        Reviewed = 27
    }
}
