﻿namespace Axxess.Core.Enums
{
    using System.ComponentModel;
    using Axxess.Core.Infrastructure;

    public enum Disciplines
    {
        [CustomDescription("Nursing", "SNV")]
        Nursing = 1,
        [CustomDescription("PT", "PT")]
        PT = 2,
        [CustomDescription("OT", "OT")]
        OT = 3,
        [CustomDescription("ST", "ST")]
        ST = 4,
        [CustomDescription("HHA", "HHA")]
        HHA = 5,
        [CustomDescription("MSW", "MSW")]
        MSW = 6,
        [CustomDescription("Dietician", "Dietician")]
        Dietician = 7,
        [CustomDescription("Report & Notes", "ReportsAndNotes")]
        ReportsAndNotes = 8,
        [CustomDescription("Orders", "Orders")]
        Orders = 9,
        [CustomDescription("Claim", "Claim")]
        Claim = 10

    }
}


 