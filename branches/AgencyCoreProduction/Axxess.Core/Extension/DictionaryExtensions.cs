﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Extension
{
    public static class DictionaryExtensions
    {
        public static T Get<T>(this IDictionary<string, T> instance, string key)
        {
            return instance.ContainsKey(key) ? (T)instance[key] : default(T);
        }

        public static string Get(this IDictionary<string, string> instance, string key)
        {
            return instance.ContainsKey(key) ? instance[key] : string.Empty;
        }

        public static void Set<T>(this IDictionary<string, object> instance, string key, T value)
        {
            instance[key] = value;
        }

        public static bool IsNotNullOrEmpty<K, V>(this Dictionary<K, V> dictionary)
        {
            return dictionary != null ? dictionary.Count > 0 : false;
        }

        public static void Merge<TKey, TValue>(this IDictionary<TKey, TValue> first, IDictionary<TKey, TValue> second)
        {
            if (second == null) return;
            if (first == null) { first = new Dictionary<TKey, TValue>(); }

            foreach (var item in second)
            {
                if (!first.ContainsKey(item.Key))
                {
                    first.Add(item.Key, item.Value);
                }
            }
        }

        public static void RemoveAll<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, Func<TValue, bool> predicate)
        {
            var keys = dictionary.Keys.Where(k => predicate(dictionary[k])).ToList();
            foreach (var key in keys)
	        {
                dictionary.Remove(key);
	        }
        }
    }
}
