﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Linq;
    using System.Diagnostics;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Linq.Expressions;
    

    public static class EnumerableExtensions
    {
        [DebuggerStepThrough]
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (T item in enumerable)
            {
                action(item);
            }
        }

        [DebuggerStepThrough]
        public static void ForEach<K, V>(this IDictionary<K, V> enumerable, Action<K, V> action)
        {
            foreach (KeyValuePair<K, V> item in enumerable)
            {
                action(item.Key, item.Value);
            }
        }

        [DebuggerStepThrough]
        public static List<T> ToList<T>(this SafeList<T> safeList) where T : class
        {
            var list = new List<T>();
            safeList.ForEach(item =>
            {
                list.Add(item);
            });
            return list;
        }

        public static bool IsNotNullOrEmpty<T>(this IEnumerable<T> enumerable) //where T : class
        {
            return enumerable != null && enumerable.Any();
        }

        //[DebuggerStepThrough]
        //public static bool IsNotNullOrEmpty<T>(this IEnumerable<T> enumerable)
        //{
        //    return enumerable != null && enumerable.Any();
        //}

        //[DebuggerStepThrough]
        //public static bool IsNotNullOrEmpty<T>(this IList<T> enumerable)
        //{
        //    return enumerable != null && enumerable.Count > 0;
        //}

        //[DebuggerStepThrough]
        //public static bool IsNotNullOrEmpty<T>(this IList<T> enumerable)
        //{
        //    return enumerable != null && enumerable.Count > 0;
        //}


        [DebuggerStepThrough]
        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> source, int size)
        {
            Check.Argument.IsNotNull(source, "source");
            Check.Argument.IsNotNegativeOrZero(size, "size");

            int index = 1;
            IEnumerable<T> partition = source.Take(size).AsEnumerable();

            while (partition.Any())
            {
                yield return partition;
                partition = source.Skip(index++ * size).Take(size).AsEnumerable();
            }
        }

        [DebuggerStepThrough]
        public static IEnumerable<T> Dequeue<T>(this Queue<T> queue, int limit)
        {
            do
            {
                if (queue.Count == 0)
                {
                    yield break;
                }
                yield return queue.Dequeue();
                limit--;
            } while (limit > 0);
        }

        public static IQueryable<T> OrderByDynamic<T>(this IQueryable<T> query, string attribute, SortDirection direction)
        {
            try
            {
                string orderMethodName = direction == SortDirection.Ascending ? "OrderBy" : "OrderByDescending";
                Type t = typeof(T);

                var param = Expression.Parameter(t, attribute);
                var property = t.GetProperty(attribute);

                return query.Provider.CreateQuery<T>(
                    Expression.Call(
                        typeof(Queryable),
                        orderMethodName,
                        new Type[] { t, property.PropertyType },
                        query.Expression,
                        Expression.Quote(
                            Expression.Lambda(
                                Expression.Property(param, property),
                                param))
                    ));
            }
            catch (Exception) // Probably invalid input, you can catch specifics if you want
            {
                return query; // Return unsorted query
            }
        }

       

        [DebuggerStepThrough]
        public static string ToCommaSeperatedList<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
        }

        [DebuggerStepThrough]
        public static string ToCommaSeperatedNoQuote<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.Select(s => s.ToString()).ToArray().Join(", ");
        }
    }
}
