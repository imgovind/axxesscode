﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Reflection;
    using System.Diagnostics;
    using System.ComponentModel;

    using Infrastructure;

    public static class EnumExtensions
    {
        [DebuggerStepThrough]
        public static T ToEnum<T>(this byte target, T defaultValue) where T : IComparable, IFormattable
        {
            return target.ToString().ToEnum(defaultValue);
        }

        [DebuggerStepThrough]
        public static T ToEnum<T>(this int target, T defaultValue) where T : IComparable, IFormattable
        {
            return target.ToString().ToEnum(defaultValue);
        }

        [DebuggerStepThrough]
        public static string ToName<T>(this int target, T defaultValue) where T : IComparable, IFormattable
        {
            return target.ToEnum<T>(defaultValue).ToString();
        }

        [DebuggerStepThrough]
        public static T ToEnum<T>(this string target, T defaultValue) where T : IComparable, IFormattable
        {
            T convertedValue = defaultValue;

            if (target.IsNotNullOrEmpty())
            {
                try
                {
                    convertedValue = (T)Enum.Parse(typeof(T), target.Trim(), true);
                }
                catch (ArgumentException)
                {
                }
            }

            return convertedValue;
        }

        [DebuggerStepThrough]
        public static string GetDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        [DebuggerStepThrough]
        public static string GetCustomShortDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            CustomDescriptionAttribute [] attributes =
                (CustomDescriptionAttribute[])fi.GetCustomAttributes(
                typeof(CustomDescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].ShortDescription;
            else
                return value.ToString();
        }

        [DebuggerStepThrough]
        public static string GetGroup(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            GroupDescriptionAttribute[] attributes =
                (GroupDescriptionAttribute[])fi.GetCustomAttributes(
                typeof(GroupDescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Group;
            else
                return value.ToString();
        }

       
        [DebuggerStepThrough]
        public static string GetCustomCategory(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            if (fi != null)
            {
                CustomDescriptionAttribute[] attributes =
                    (CustomDescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(CustomDescriptionAttribute),
                    false);

                if (attributes != null &&
                    attributes.Length > 0)
                    return attributes[0].Category;
                else
                    return value.ToString();
            }
            else
            {
                return value.ToString();
            }
        }

        [DebuggerStepThrough]
        public static string GetCustomGroup(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            if (fi != null)
            {
                CustomDescriptionAttribute[] attributes =
                    (CustomDescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(CustomDescriptionAttribute),
                    false);

                if (attributes != null &&
                    attributes.Length > 0)
                    return attributes[0].StatusGroup;
                else
                    return value.ToString();
            }
            else
            {
                return value.ToString();
            }
        }

        [DebuggerStepThrough]
        public static string GetFormGroup(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            CustomDescriptionAttribute[] attributes =
                (CustomDescriptionAttribute[])fi.GetCustomAttributes(
                typeof(CustomDescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].FormGroup;
            else
                return value.ToString();
        }

        [DebuggerStepThrough]
        public static string GetActualDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            CustomDescriptionAttribute[] attributes =
                (CustomDescriptionAttribute[])fi.GetCustomAttributes(
                typeof(CustomDescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        [DebuggerStepThrough]
        public static Permission GetPermission(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            PermissionAttribute[] attributes = (PermissionAttribute[])fi.GetCustomAttributes(typeof(PermissionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return new Permission()
                {
                    Category = attributes[0].Category,
                    Description = attributes[0].Description,
                    LongDescription = attributes[0].LongDescription,
                    Tip = attributes[0].Tip
                };
            }
            else
                return new Permission();
        }

        [DebuggerStepThrough]
        public static Enum GetValueFromDescription(this Enum MyEnum, string Description)
        {
            Enum retEnumValue = null;

            foreach (Enum e in Enum.GetValues(MyEnum.GetType()))
            {
                string sValue = GetDescription(e);
                if (sValue.ToLower() == Description.ToLower())
                {
                    retEnumValue = e;
                    break;
                }
            }
            return retEnumValue;
        }

    }
}
