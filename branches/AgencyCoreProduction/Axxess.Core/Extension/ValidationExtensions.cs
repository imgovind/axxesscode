﻿
namespace Axxess.Core.Extension
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;

    public static class ValidationExtensions
    {
        public static T Validate<T>(this List<Validation> validations) where T : JsonViewData, new()
        {
            var entityValidator = new EntityValidator(validations.ToArray());
            entityValidator.Validate();

            if (!entityValidator.IsValid)
            {
                return new T { isSuccessful = false, errorMessage = entityValidator.Message };
            }

            else
            {
                return new T { isSuccessful = true };
            }
        }
    }
}
