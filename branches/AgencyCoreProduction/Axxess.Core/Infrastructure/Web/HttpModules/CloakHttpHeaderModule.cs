﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Web;
    using System.Collections.Generic;
    using Extension;

    public class CloakHttpHeaderModule : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloakHttpHeaderModule"/> class.
        /// </summary>
        public CloakHttpHeaderModule()
        {
            this.headersToCloak = new List<string>
                                      {
                                              "Server",
                                              "X-AspNet-Version",
                                              "X-AspNetMvc-Version",
                                              "X-Powered-By",
                                      };
        }


        /// <summary>
        /// Handles the current request.
        /// </summary>
        /// <param name="context">
        /// The HttpApplication context.
        /// </param>
        public void Init(HttpApplication context)
        {
            context.PreSendRequestHeaders += this.OnPreSendRequestHeaders;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// List of Headers to remove
        /// </summary>
        private List<string> headersToCloak;

       

        /// <summary>
        /// Remove all headers from the HTTP Response.
        /// </summary>
        /// <param name="sender">
        /// The object raising the event
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private void OnPreSendRequestHeaders(object sender, EventArgs e)
        {
            this.headersToCloak.ForEach(h => HttpContext.Current.Response.Headers.Remove(h));
        }

        #endregion
    }
}
