﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.Core.Infrastructure.Web.Base
{
    using System.Web;
    using System.Web.Mvc;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class CustomJsonResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = string.IsNullOrEmpty(this.ContentType) ? "application/json" : this.ContentType;
            if (this.ContentEncoding != null)
                response.ContentEncoding = this.ContentEncoding;
            if (this.Data == null)
                return;
            response.Write(JsonConvert.SerializeObject(this.Data, new IsoDateTimeConverter()));
        }
    }
}
