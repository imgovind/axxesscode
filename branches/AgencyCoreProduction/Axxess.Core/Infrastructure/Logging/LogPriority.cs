﻿namespace Axxess.Core.Infrastructure
{
    using System;

    public enum LogPriority
    {
        Debug,
        Info,
        Warn,
        Error,
        Fatal,
        Off
    }
}
