﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Timers;

namespace Axxess.Core.Infrastructure
{
    public class LogEngine
    {
        #region Nested Class for Singleton
        class Nested
        {
            static Nested()
            {
                instance.StartTimer();
            }

            internal static readonly LogEngine instance = new LogEngine();
        }

        #endregion

        #region Private Members

        private Timer updateTimer;
        private List<LogEntry> newErrors;
        private List<LogEntry> tempErrors;

        private static object syncLock = new object();
        
        #endregion

        #region Public Instance

        public static LogEngine Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Constructor / Methods

        private LogEngine()
        {
            newErrors = new List<LogEntry>(1000);
        }

        private void StartTimer()
        {
            this.updateTimer = new System.Timers.Timer(TimeSpan.FromMinutes(30).TotalMilliseconds);
            this.updateTimer.Enabled = true;
            this.updateTimer.Elapsed += new ElapsedEventHandler(this.Timer_Elapsed);
            this.updateTimer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            lock (syncLock)
            {
                this.tempErrors = this.newErrors;
                this.newErrors = new List<LogEntry>(1000);
            }
            this.ProcessExceptions();
        }

        private void ProcessExceptions()
        {
            if (this.tempErrors.Count > 0)
            {
                int counter = 0;
                List<LogEntry> uniques = new List<LogEntry>();
                this.tempErrors.ForEach(
                    delegate(LogEntry error)
                    {
                        if (!uniques.Contains(error))
                        {
                            error.Impression++;
                            uniques.Add(error);
                        }
                        else
                        {
                            uniques[counter].Impression++;
                        }
                        counter++;
                    });
            }
            this.tempErrors = new List<LogEntry>();
        }
        #endregion

        #region Public Methods

        public void Add(LogEntry error)
        {
            lock (syncLock)
            {
                this.newErrors.Add(error);
            }
        }

        #endregion

    }
}
