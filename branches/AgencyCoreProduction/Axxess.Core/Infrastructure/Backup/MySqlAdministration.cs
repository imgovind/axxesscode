﻿namespace Axxess.Core.Infrastructure
{
    using System;

    public class MySqlAdministration : IDatabaseAdministration
    {
        #region Private Members and Constants

        private string databaseName;
        private string connectionString;

        public const int MIN_ROW_COUNT_FOR_SPLITTING = 3000;
        public const long MAX_ALLOWED_PACKET = 1 * 1024 * 1024;
        public const string SQL_GET_TABLE_NAMES = "SHOW TABLES;";
        public const string SQL_GET_TABLE_COLUMN_NAMES = "SHOW COLUMNS FROM `{0}`;";
        public const string SQL_GET_TABLE_CREATE_SYNTAX = "SHOW CREATE TABLE `{0}`;";
        public const string SQL_GET_MYSQL_VERSION = "SHOW variables WHERE Variable_name = 'version';";
        public const string SQL_GET_MAX_ALLOWED_PACKET = "SHOW variables WHERE Variable_name = 'max_allowed_packet';";
        public const string SQL_GET_TABLE_ROW_COUNT = "SELECT COUNT(Id) FROM `{0}`;";

        #endregion

        #region Public Properties

        public string DatabaseName
        {
            get
            {
                return databaseName;
            }
        }

        public string FilePath { get; set; }

        #endregion

        #region Constructor

        public MySqlAdministration(string connectionString)
        {
            Check.Argument.IsNotEmpty(connectionString, "connectionString");

            this.connectionString = connectionString;
            this.databaseName = ExtractDatabaseName(connectionString);
        }

        #endregion

        #region IDatabaseAdministration Methods

        public void Backup(bool enableEncryption)
        {
            IDatabaseOperation backup = new MySqlBackup(this.databaseName, this.connectionString, enableEncryption, this.FilePath);
            backup.Execute();
        }

        public void Restore(bool enableEncryption)
        {
            IDatabaseOperation restore = new MySqlRestore(this.connectionString, this.FilePath, enableEncryption);
            restore.Execute();
        }

        #endregion


        #region Private Methods

        private string ExtractDatabaseName(string connectionString)
        {
            string[] sa = connectionString.Split(';');
            foreach (string s in sa)
            {
                if (s.ToLower().StartsWith("database"))
                {
                    string[] sb = s.Split('=');
                    return sb[1];
                }
            }
            throw new Exception("Database Name is not detected in Connection String.");
        }

        #endregion

    }
}
