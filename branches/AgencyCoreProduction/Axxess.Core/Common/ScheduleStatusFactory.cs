﻿namespace Axxess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public static class ScheduleStatusFactory
    {

        public static List<int> NoteCompleted(bool IsQaIncluded)
        {
            var list = new List<int>();
            if (IsQaIncluded)
            {
                list.Add((int)ScheduleStatus.NoteSubmittedWithSignature);
            }
            list.Add((int)ScheduleStatus.NoteCompleted);
            return list;
        }
        public static List<int> OnlyEvalNoteCompleted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.EvalSentToPhysician);
            list.Add((int)ScheduleStatus.EvalToBeSentToPhysician);
            list.Add((int)ScheduleStatus.EvalReturnedWPhysicianSignature);
            list.Add((int)ScheduleStatus.EvalSentToPhysicianElectronically);
            return list;
        }

        public static List<int> EvalNoteCompleted()
        {
            var list = new List<int>();
            list.AddRange(OnlyEvalNoteCompleted());
            list.AddRange(NoteCompleted(true));
            return list;
        }

        public static List<int> CaseManagerStatus()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OrderSubmittedPendingReview);
            list.Add((int)ScheduleStatus.OasisCompletedPendingReview);
            list.Add((int)ScheduleStatus.NoteSubmittedWithSignature);
            list.Add((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature);
            return list;
        }

       
        public static List<int> OASISAndNurseNotesAfterQA()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteCompleted);
            list.AddRange(OnlyEvalNoteCompleted());
            list.AddRange(OASISAfterQA());
            return list;
        }

        public static List<int> OnAndAfterQAStatus(bool IsQaIncluded)
        {
            var list = new List<int>();
            list.AddRange(OrdersCompleted(IsQaIncluded));
            list.AddRange(OnlyEvalNoteCompleted());
            list.AddRange(NoteCompleted(IsQaIncluded));
            list.AddRange(NoteMissedVisitOnAndAfterQA(IsQaIncluded));
            list.AddRange(OASISCompleted(IsQaIncluded));
            list.AddRange(ReportAndNotesCompleted(IsQaIncluded));
            return list;
        }

        public static List<int> OrdersAfterQA()
        {

            var list = new List<int>();
            list.Add((int)ScheduleStatus.OrderSentToPhysician);
            list.Add((int)ScheduleStatus.OrderToBeSentToPhysician);
            list.Add((int)ScheduleStatus.OrderReturnedWPhysicianSignature);
            list.Add((int)ScheduleStatus.OrderSentToPhysicianElectronically);
            list.Add((int)ScheduleStatus.OrderSavedByPhysician);
            return list;
        }

        public static List<int> OrdersCompleted(bool IsQaIncluded)
        {
            var list = new List<int>();
            if (IsQaIncluded)
            {
                list.Add((int)ScheduleStatus.OrderSubmittedPendingReview);
            }
            list.AddRange(OrdersAfterQA());
            return list;
        }

        public static List<int> NoteMissedVisitOnAndAfterQA(bool IsQaIncluded)
        {
            var list = new List<int>();
            if (IsQaIncluded)
            {
                list.Add((int)ScheduleStatus.NoteMissedVisitPending);
            }
            list.Add((int)ScheduleStatus.NoteMissedVisitComplete);
            return list;
        }

        public static List<int> OASISAfterQA()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OasisCompletedExportReady);
            list.Add((int)ScheduleStatus.OasisCompletedNotExported);
            list.Add((int)ScheduleStatus.OasisExported);
            return list;
        }

        public static List<int> OASISCompleted(bool IsQaIncluded)
        {
            var list = new List<int>();
            if (IsQaIncluded)
            {
                list.Add((int)ScheduleStatus.OasisCompletedPendingReview);
            }
            list.AddRange(OASISAfterQA());
            return list;
        }

        public static List<int> ReportAndNotesCompleted(bool IsQaIncluded)
        {
            var list = new List<int>();
            if (IsQaIncluded)
            {
                list.Add((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature);
            }
            list.Add((int)ScheduleStatus.ReportAndNotesCompleted);
            return list;
        }

        public static List<int> AllStatusButOrdersAndReports()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteNotStarted);
            list.Add((int)ScheduleStatus.NoteNotYetDue);
            list.Add((int)ScheduleStatus.NoteSaved);
            list.Add((int)ScheduleStatus.NoteSubmittedWithSignature);
            list.Add((int)ScheduleStatus.NoteCompleted);
            list.Add((int)ScheduleStatus.NoteReopened);
            list.Add((int)ScheduleStatus.NoteReturned);
            list.Add((int)ScheduleStatus.NoteReturnedForClinicianSignature);
            list.Add((int)ScheduleStatus.EvalReturnedByPhysician);
            list.Add((int)ScheduleStatus.OasisCompletedExportReady);
            list.Add((int)ScheduleStatus.OasisExported);
            list.Add((int)ScheduleStatus.OasisCompletedNotExported);
            list.Add((int)ScheduleStatus.OasisNotYetDue);
            list.Add((int)ScheduleStatus.OasisNotStarted);
            list.Add((int)ScheduleStatus.OasisSaved);
            list.Add((int)ScheduleStatus.OasisCompletedPendingReview);
            list.Add((int)ScheduleStatus.OasisReturnedForClinicianReview);
            list.Add((int)ScheduleStatus.OasisReopened);
            list.Add((int)ScheduleStatus.EvalToBeSentToPhysician);
            list.Add((int)ScheduleStatus.EvalSentToPhysician);
            list.Add((int)ScheduleStatus.EvalReturnedWPhysicianSignature);
            list.Add((int)ScheduleStatus.EvalSentToPhysicianElectronically);
            return list;
        }

        public static List<int> AllNoteNotYetStarted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.CommonNotStarted);
            list.Add((int)ScheduleStatus.OrderNotYetStarted);
            list.Add((int)ScheduleStatus.OrderNotYetDue);
            list.Add((int)ScheduleStatus.OasisNotYetDue);
            list.Add((int)ScheduleStatus.OasisNotStarted);
            list.Add((int)ScheduleStatus.NoteNotYetDue);
            list.Add((int)ScheduleStatus.NoteNotStarted);
            list.Add((int)ScheduleStatus.ReportAndNotesCreated);
            return list;
        }

        public static List<int> MissedVisitStatus()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteMissedVisit);
            list.Add((int)ScheduleStatus.NoteMissedVisitPending);
            list.Add((int)ScheduleStatus.NoteMissedVisitComplete);
            list.Add((int)ScheduleStatus.NoteMissedVisitReturn);
            return list;
        }

        public static List<int> OpenOASISStatus()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OasisNotStarted);
            list.Add((int)ScheduleStatus.OasisNotYetDue);
            list.Add((int)ScheduleStatus.OasisReopened);
            list.Add((int)ScheduleStatus.OasisSaved);
            return list;
        }

        public static List<int> NoteNotStarted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.NoteNotStarted);
            list.Add((int)ScheduleStatus.NoteNotYetDue);
            return list;
        }

        public static List<int> OASISNotStarted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OasisNotStarted);
            list.Add((int)ScheduleStatus.OasisNotYetDue);
            return list;
        }

        public static List<int> OrdersNotStarted()
        {
            var list = new List<int>();
            list.Add((int)ScheduleStatus.OrderNotYetDue);
            list.Add((int)ScheduleStatus.OrderNotYetStarted);
            return list;
        }

        public static List<int> AllNotStarted()
        {
            var list = new List<int>();
            list.AddRange(NoteNotStarted());
            list.AddRange(OASISNotStarted());
            list.AddRange(OrdersNotStarted());
            list.Add((int)ScheduleStatus.ReportAndNotesCreated);
            return list;
        }


    }
}
