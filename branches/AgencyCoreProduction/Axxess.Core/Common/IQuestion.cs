﻿namespace Axxess.Core
{

    public interface IQuestion
    {
        string Name { get; set; }
        string Answer { get; set; }
        
    }
}
