﻿namespace Axxess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;

    public static class PatientStatusFactory
    {

       public static List<int> PossibleAdmit()
        {
            var list = new List<int>();
            list.Add((int)PatientStatus.Pending);
            list.Add((int)PatientStatus.NonAdmission);
            return list;
        }

        public static List<int> PossibleNonAdmit()
        {
            var list = new List<int>();
            list.Add((int)PatientStatus.Pending);
            list.Add((int)PatientStatus.Active);
            list.Add((int)PatientStatus.Discharged);
            return list;
        }
        /// <summary>
        /// The status to show on the patient and schedule center
        /// </summary>
        /// <returns></returns>
        public static List<int> CenterStatus()
        {
            var list = new List<int>();
            list.Add((int)PatientStatus.Active);
            list.Add((int)PatientStatus.Discharged);
            list.Add((int)PatientStatus.Pending);
            list.Add((int)PatientStatus.NonAdmission);
            return list;
        }
        public static List<int> OnceAdmittedStatus()
        {
            var list = new List<int>();
            list.Add((int)PatientStatus.Active);
            list.Add((int)PatientStatus.Discharged);
            return list;
        }

        public static List<int> AllStatus(bool isIncludeDeprecated)
        {
            var list = new List<int>();
            list.Add((int)PatientStatus.Active);
            list.Add((int)PatientStatus.Discharged);
            list.Add((int)PatientStatus.Pending);
            list.Add((int)PatientStatus.NonAdmission);
            if (isIncludeDeprecated)
            {
                list.Add((int)PatientStatus.Deprecated);
            }
            return list;
        }
    }
}
