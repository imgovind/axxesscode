﻿namespace Axxess.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;
    using Extension;

    public static class DisciplineFactory
    {

        public static List<Disciplines> MainDisciplines()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.Nursing);
            list.AddRange(TherapyDisciplines());
            list.Add(Disciplines.HHA);
            list.Add(Disciplines.MSW);
            return list;
        }

        public static List<Disciplines> AllDisciplinesExceptDiet()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.Nursing);
            list.AddRange(TherapyDisciplines());
            list.Add(Disciplines.HHA);
            list.Add(Disciplines.MSW);
            list.Add(Disciplines.Orders);
            list.Add(Disciplines.ReportsAndNotes);
            return list;
        }


        public static List<Disciplines> MainDisciplinesPlus()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.Nursing);
            list.AddRange(TherapyDisciplines());
            list.Add(Disciplines.HHA);
            list.Add(Disciplines.MSW);
            list.Add(Disciplines.Dietician);
            return list;
        }


        public static List<Disciplines> SkilledCareDisciplines()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.Nursing);
            list.AddRange(TherapyDisciplines());
            return list;
        }

        public static List<Disciplines> NonBillableDisciplines()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.ReportsAndNotes);
            list.Add(Disciplines.Orders);
            list.Add(Disciplines.Claim);
            return list;
        }

        public static List<Disciplines> TherapyDisciplines()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.PT);
            list.Add(Disciplines.OT);
            list.Add(Disciplines.ST);
            return list;
        }

        public static List<Disciplines> HHA()
        {
            var list = new List<Disciplines>();
            list.Add(Disciplines.HHA);
            return list;
        }

        public static List<Disciplines> DisciplinesGroupByName(string name)
        {
            var list = new List<Disciplines>();
            if (name.IsEqual("Therapy"))
            {
                list.AddRange(TherapyDisciplines());
            }
            else if (name.IsEqual("Nursing"))
            {
                list.Add(Disciplines.Nursing);
                list.Add(Disciplines.ReportsAndNotes);
            }
            else if (name.IsEqual("PT"))
            {
                list.Add(Disciplines.PT);
            }
            else if (name.IsEqual("ST"))
            {
                list.Add(Disciplines.ST);
            }
            else if (name.IsEqual("OT"))
            {
                list.Add(Disciplines.OT);
            }
            else if (name.IsEqual("Orders"))
            {
                list.Add(Disciplines.Orders);
            }
            else if (name.IsEqual("HHA"))
            {
                list.Add(Disciplines.HHA);
            }
            else if (name.IsEqual("MSW"))
            {
                list.Add(Disciplines.MSW);
            }
            else if (name.IsEqual("Dietician"))
            {
                list.Add(Disciplines.Dietician);
            }
            else if (name.IsEqual("ReportsAndNotes"))
            {
                list.Add(Disciplines.ReportsAndNotes);
            }
            return list;
        }
    }
}
