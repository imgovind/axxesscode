﻿using System;
using System.Diagnostics;
using System.ServiceProcess;

using Axxess.Api.Contracts;

namespace Axxess.Api.Services
{
    static class Program
    {
        static void Main()
        {
            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
		        { 
			        new ValidationWindowsService(), new GrouperWindowsService(), new ReportWindowsService(), new CacheWindowsService()
		        };
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
