﻿namespace Axxess.Api.Services
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Contracts;

    public sealed class AgencyEngine
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
            }

            internal static readonly AgencyEngine instance = new AgencyEngine();
        }

        #endregion

        #region Public Instance

        public static AgencyEngine Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Members

        private SafeList<UniqueItem> agencies;

        #endregion

        #region Private Constructor / Methods

        private AgencyEngine()
        {
            this.Load();
        }

        private void Load()
        {
            this.agencies = new SafeList<UniqueItem>();
            var agencyList = DataProvider.GetAllAgencies();
            if (agencyList != null && agencyList.Count > 0)
            {
                agencyList.ForEach(agency =>
                {
                    this.agencies.Add(new UniqueItem { Id = agency.Id, Xml = agency.ToXml() });
                });
            }
        }

        private void Load(Guid agencyId)
        {
            var agency = DataProvider.GetAgencyWithBranches(agencyId);
            var item = this.agencies.Single(a => a.Id.ToString().IsEqual(agencyId.ToString()));
            if (item == null)
            {
                if (agency != null)
                {
                    this.agencies.Add(new UniqueItem { Id = agency.Id, Xml = agency.ToXml() });
                }
            }
            else
            {
                item.Xml = agency.ToXml();
            }
        }

        #endregion

        #region Public Methods

        public string Get(Guid agencyId)
        {
            var agency = this.agencies.Single(a => a.Id.ToString().IsEqual(agencyId.ToString()));
            if (agency != null)
            {
                return agency.Xml;
            }
            return string.Empty;
        }

        public void Refresh(Guid agencyId)
        {
            Load(agencyId);
        }

        public int Count
        {
            get
            {
                return agencies.Count;
            }
        }

        #endregion

    }
}
