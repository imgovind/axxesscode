﻿namespace Axxess.Api.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Infrastructure;

    internal static class DataProvider
    {
        #region Static Helper Methods

        internal static List<AgencyData> GetAllAgencies()
        {
            var agencies = new List<AgencyData>();
            var script = @"SELECT Id, Name, CahpsVendor, MedicareProviderNumber, MedicaidProviderNumber, NationalProviderNumber, " +
                "CahpsVendorId, CahpsSurveyDesignator FROM Agencies;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agencies = cmd.SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new AgencyData
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator")
                })
                .AsList();
            }

            return agencies;
        }

        internal static AgencyData GetAgencyWithBranches(Guid agencyId)
        {
            var agency = new AgencyData();
            var script = @"SELECT Id, Name, CahpsVendor, MedicareProviderNumber, MedicaidProviderNumber, NationalProviderNumber, " +
                "CahpsVendorId, CahpsSurveyDesignator FROM Agencies WHERE Id = @agencyid;";

            using (var cmd = new FluentCommand<AgencyData>(script))
            {
                agency = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new AgencyData
                    {
                        Id = reader.GetGuid("Id"),
                        Name = reader.GetString("Name"),
                        CahpsVendor = reader.GetInt("CahpsVendor"),
                        MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                        MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                        NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                        CahpsVendorClientId = reader.GetStringNullable("CahpsVendorId"),
                        CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator")
                    })
                    .AsSingle();
            }

            return agency;
        }

        internal static List<PhysicianData> GetAllPhysicians()
        {
            return new List<PhysicianData>();
        }

        internal static List<PhysicianData> GetAgencyPhysicians(Guid agencyId)
        {
            return new List<PhysicianData>();
        }

        internal static List<UserData> GetUserNames()
        {
            return new List<UserData>();
        }

        internal static List<UserData> GetUserNames(Guid agencyId)
        {
            return new List<UserData>();
        }

        #endregion
    }
}
