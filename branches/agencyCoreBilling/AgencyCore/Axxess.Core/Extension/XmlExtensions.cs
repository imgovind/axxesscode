﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Diagnostics;

    using Axxess.Core.Infrastructure;
    using System.Collections;

    public static class XmlExtensions
    {
        [DebuggerStepThrough]
        public static string ToXml<T>(this T instance)
        {
            if (instance == null)
                return string.Empty;

            return Serializer.Serialize(instance);
        }

        [DebuggerStepThrough]
        public static T ToObject<T>(this string text)
        {
            if (text.IsNullOrEmpty())
                return default(T);
            return Serializer.Deserialize<T>(text);
        }
    }
}
