﻿namespace Axxess.Core.Extension
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    public static class FormCollectionExtensions
    {
        public static string GetString(this FormCollection collection, string name)
        {
            var keys = collection.AllKeys;
            if (collection != null && name.IsNotNullOrEmpty() && keys.Contains(name) && collection[name].IsNotNullOrEmpty())
            {
                return collection[name];
            }
            return string.Empty;
        }

        public static Guid GetGuid(this FormCollection collection, string name)
        {
            var item = collection.GetString(name);
            if (item.IsNotNullOrEmpty() && item.IsGuid())
            {
                return item.ToGuid();
            }
            return Guid.Empty;
        }

        public static List<string> GetStringList(this FormCollection collection, string name)
        {
            if (collection[name].IsNotNullOrEmpty())
            {
                string[] array = collection[name].Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (array.Length > 0)
                {
                    return new List<string>(array);
                }
            }
            return new List<string>();
        }

        public static string GetArrayString(this FormCollection collection, string name)
        {
            string result = "";
            if (collection[name].IsNotNullOrEmpty())
            {
                string[] array = collection[name].Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (array.Length > 0)
                {
                    result = string.Join(";", array);
                }
            }
            return result;
        }
    }
}
