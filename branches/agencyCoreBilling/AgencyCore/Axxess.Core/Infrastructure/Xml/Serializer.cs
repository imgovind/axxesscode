﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Text;
    using System.Xml.Serialization;

    public static class Serializer
    {
        public static string Serialize<T>(T instance)
        {
            if (instance != null)
            {
                StringBuilder stringBuilder = new StringBuilder();

                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;
                xmlSettings.OmitXmlDeclaration = true;
                xmlSettings.Encoding = Encoding.UTF8;

                XmlSerializerNamespaces nameSpaces = new XmlSerializerNamespaces();
                nameSpaces.Add("", "");

                XmlWriter xmlWriter = XmlWriter.Create(stringBuilder, xmlSettings);

                XmlSerializer xmlSerializer = new XmlSerializer(instance.GetType());
                xmlSerializer.Serialize(xmlWriter, instance, nameSpaces);
                xmlWriter.Flush();

                return stringBuilder.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public static T Deserialize<T>(string xmlString)
        {
            T instance = default(T);
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));

                 instance = (T)xmlSerializer.Deserialize(memoryStream);

                if (instance == null)
                {
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                return instance;
            }
            return instance;
        }

    }
}

