﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Xml.Schema;
    using System.Text;

    public class XmlValidator : IValidator
    {
        private readonly string xmlFragment;
        private readonly string schemaDocument;

        public XmlValidator(string xmlString, string schemaLocation)
        {
            Check.Argument.IsNotEmpty(xmlString, "Xml String");
            Check.Argument.IsNotEmpty(schemaLocation, "Schema Location");

            this.xmlFragment = xmlString;
            this.schemaDocument = schemaLocation;
        }

        private bool isValid { get; set; }
        public bool IsValid { get { return isValid; } }

        private string message { get; set; }
        public string Message { get { return message; } }

        public void Validate()
        {
            XmlReaderSettings settings = new XmlReaderSettings();

            settings.ValidationEventHandler += new ValidationEventHandler(this.ValidationEventHandler);
            settings.ValidationType = ValidationType.Schema;
            settings.IgnoreWhitespace = true;
            settings.IgnoreComments = true;
            settings.Schemas.Add(null, XmlReader.Create(this.schemaDocument));

            StringReader stringReader = new StringReader(this.xmlFragment);
            using (XmlReader xmlValidatingReader = XmlReader.Create(stringReader, settings))
            {
                while (xmlValidatingReader.Read()) { }
            }
        }

        private void ValidationEventHandler(object sender, ValidationEventArgs args)
        {
            this.isValid = false;
            if (args.Severity == XmlSeverityType.Warning)
            {
                this.message = string.Format("Warning: {0}", args.Message);
            }
            else
            {
                this.message = string.Format("Error: {0}", args.Message);
            }
        }
    }
}
