﻿namespace Axxess.Core.Infrastructure
{
    public class JsonViewData
    {
        public string url { get; set; }
        public bool isSuccessful { get; set; }
        public string errorMessage { get; set; }
    }
}
