﻿namespace Axxess.Core.Infrastructure
{
    using System;
    using System.Collections.Generic;

    using StructureMap;

    public class StructureMapDependencyResolver : IDependencyResolver
    {
        #region IDependencyResolver Members

        public StructureMapDependencyResolver()
        {

        }

        public void Register<T>(T instance)
        {
            ObjectFactory.Inject<T>(instance);
        }

        public void Inject<T>(T existing)
        {
            ObjectFactory.Inject<T>(existing);
        }

        public T Resolve<T>(Type type)
        {
            return (T)ObjectFactory.GetInstance(type);
        }

        public T Resolve<T>(Type type, string name)
        {
            return (T)ObjectFactory.GetNamedInstance(type, name);
        }

        public T Resolve<T>()
        {
            return Resolve<T>(typeof(T));
        }

        public T Resolve<T>(string name)
        {
            return (T)ObjectFactory.GetNamedInstance<T>(name);
        }

        public IEnumerable<T> ResolveAll<T>()
        {
            return (IList<T>)ObjectFactory.GetAllInstances<T>();
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
           ObjectFactory.ResetDefaults();
        }

        #endregion
    }
}
