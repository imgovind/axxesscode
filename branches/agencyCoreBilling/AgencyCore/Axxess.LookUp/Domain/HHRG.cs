﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.LookUp.Domain
{
   public class HippsAndHhrg
    {
       public int Id { get; set; }
       public string HHRG { get; set; }
       public string HIPPS { get; set; }
       public double HHRGWeight { get; set; }
       public double SupplyAddOn { get; set; }
    }
}
