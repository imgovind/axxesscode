﻿namespace Axxess.LookUp.Domain
{
    using System;

    public class MedicareRate
    {
        public int Id { get; set; }
        public string Discipline { get; set; }
        public string PerVisitRate { get; set; }
    }
}
