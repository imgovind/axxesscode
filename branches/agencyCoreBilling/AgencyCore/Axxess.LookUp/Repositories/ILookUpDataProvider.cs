﻿namespace Axxess.LookUp.Repositories
{
    public interface ILookUpDataProvider
    {
        ILookupRepository LookUpRepository { get; }
    }
}
