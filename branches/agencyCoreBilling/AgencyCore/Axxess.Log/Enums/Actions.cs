﻿namespace Axxess.Log.Enums
{
    using System.ComponentModel;

    public enum Actions
    {
        [Description("Created")]
        Add,
        [Description("Updated")]
        Edit,
        [Description("Updated Task Detail")]
        EditDetail,
        [Description("Deleted")]
        Deleted,
        [Description("Approved")]
        Approved,
        [Description("Returned")]
        Returned,
        [Description("Status changed")]
        StatusChange,
        [Description("Restored")]
        Restored,
        [Description("Reassigned")]
        Reassigned,
        [Description("Saved")]
        Saved,
        [Description("Saved")]
        EditAssessment,
        [Description("Printed")]
        Printed
    }
}
