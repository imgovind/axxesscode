﻿namespace Axxess.OasisC.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;
    using Extensions;

    using SubSonic.Repository;
    using Axxess.OasisC.Enums;

    public class AssessmentRepository : IAssessmentRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AssessmentRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IAssessmentRepository Members

        public bool Add(Assessment assessment)
        {
            if (assessment != null)
            {
                if (assessment.Questions != null)
                {
                    assessment.OasisData = assessment.Questions.ToXml();
                }
                assessment.Created = DateTime.Now;
                assessment.Modified = DateTime.Now;
                database.InsertAny(assessment);
                return true;
            }
            return false;
        }

        public bool Update(Assessment assessment)
        {
            bool result = false;
            if (assessment != null)
            {
                if (assessment.Questions != null)
                {
                    assessment.OasisData = assessment.Questions.ToXml();
                }
                assessment.Modified = DateTime.Now;
                database.UpdateAny(assessment);
                result = true;
            }
            return result;
        }

        public bool UpdateModal(Assessment assessment)
        {
            bool result = false;
            if (assessment != null)
            {
                assessment.Modified = DateTime.Now;
                database.UpdateAny(assessment);
                result = true;
            }
            return result;
        }

        public Assessment Get(Guid assessmentId, string assessmentType, Guid agencyId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(assessmentType, assessmentId, agencyId);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
            }
            return assessment;
        }

        public bool MarkAsDeleted(Guid agencyId, Guid assessmentId, Guid episodeId, Guid patientId, string assessmentType, bool isDeprecated)
        {
            bool result = false;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                var assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
                if (assessment != null)
                {
                    assessment.IsDeprecated = isDeprecated;
                    assessment.Modified = DateTime.Now;
                    database.UpdateAny(assessment);
                    result = true;
                }
            }
            return result;
        }

        public bool ReassignUser(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, Guid employeeId, string assessmentType)
        {
            bool result = false;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !employeeId.IsEmpty())
            {
                var assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
                if (assessment != null)
                {
                    try
                    {
                        assessment.UserId = employeeId;
                        assessment.Modified = DateTime.Now;
                        database.UpdateAny(assessment);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
                else
                {
                    result = true;
                }
                
            }
            return result;
        }

        public Assessment GetWithNoList(Guid agencyId, Guid assessmentId, Guid episodeId, Guid patientId, string assessmentType)
        {
            Assessment assessment = null;
            if (!agencyId.IsEmpty() && !assessmentId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
            }
            return assessment;
        }

        public Assessment Get(Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType, Guid agencyId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(agencyId, assessmentId, PatientId, EpisodeId, assessmentType);
                if (assessment != null)
                {
                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                }
            }
            return assessment;
        }

        public Assessment GetAssessmentOnly(Guid assessmentId, Guid PatientId, Guid EpisodeId, string assessmentType, Guid agencyId)
        {
            Assessment assessment = null;
            if (!assessmentId.IsEmpty()&& !PatientId.IsEmpty() && !EpisodeId.IsEmpty() && assessmentType.IsNotNullOrEmpty() && !agencyId.IsEmpty())
            {
                assessment = database.FindAny(agencyId, assessmentId, PatientId, EpisodeId, assessmentType);
            }
            return assessment;
        }

        public List<Assessment> GetAllByStatus(Guid agencyId, int status)
        {
            var assessments = new List<Assessment>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
                {
                    var data = database.FindAnyByStatus(agencyId, assessmentType.ToString(), status);
                    assessments.AddRange(data);
                }
            }
            return assessments;
        }

        public List<Assessment> GetOnlyCMSOasisByStatus(Guid agencyId, int status)
        {
            var assessments = new List<Assessment>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
                {
                    if (assessmentType == AssessmentType.PlanOfCare485 || assessmentType == AssessmentType.NonOasisDischarge || assessmentType == AssessmentType.NonOasisRecertification || assessmentType == AssessmentType.NonOasisStartOfCare)
                    {
                        continue;
                    }
                    var data = database.FindAnyByStatus(agencyId, assessmentType.ToString(), status);
                    assessments.AddRange(data);
                }
            }
            return assessments;
        }

        public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var assessments = new List<AssessmentExport>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
                {
                    if (assessmentType == AssessmentType.PlanOfCare485 || assessmentType == AssessmentType.NonOasisDischarge || assessmentType == AssessmentType.NonOasisRecertification || assessmentType == AssessmentType.NonOasisStartOfCare)
                    {
                        continue;
                    }
                    var data = database.FindAnyByStatusLean(agencyId,branchId, assessmentType.ToString(), status,patientStatus, startDate, endDate);
                    assessments.AddRange(data);
                }
            }
            return assessments;
        }

        public List<AssessmentExport> GetOnlyCMSOasisByStatusLean(Guid agencyId, Guid branchId, int status, List<int> paymentSources)
        {
            var assessments = new List<AssessmentExport>();
            if (!agencyId.IsEmpty() && status > 0)
            {
                foreach (AssessmentType assessmentType in Enum.GetValues(typeof(AssessmentType)))
                {
                    if (assessmentType == AssessmentType.PlanOfCare485 || assessmentType == AssessmentType.NonOasisDischarge || assessmentType == AssessmentType.NonOasisRecertification || assessmentType == AssessmentType.NonOasisStartOfCare)
                    {
                        continue;
                    }
                    var data = database.FindAnyByStatusLean(agencyId, branchId, assessmentType.ToString(), status, paymentSources);
                    assessments.AddRange(data);
                }
            }
            return assessments;
        }

        public List<Assessment> GetPreviousAssessments(Guid agencyId, Guid patientId, AssessmentType assessmentType)
        {
            var previousAssessments = new List<Assessment>();
            if (!agencyId.IsEmpty() && !patientId.IsEmpty())
            {
                previousAssessments = database.FindMany(agencyId, patientId, assessmentType.ToString()).OrderByDescending(a => a.AssessmentDate).ToList();
            }
            return previousAssessments;
        }

        public bool UsePreviousAssessment(Guid agencyId, Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType, Guid previousAssessmentId, string previousAssessmentType)
        {
            bool result = false;
            var previousAssessment = database.FindAny(previousAssessmentType, previousAssessmentId, agencyId);
            var currentAssessment = database.FindAny(agencyId, assessmentId, patientId, episodeId, assessmentType);
            if (previousAssessment != null && currentAssessment != null)
            {
                if (previousAssessment.OasisData.IsNotNullOrEmpty())
                {
                    currentAssessment.OasisData = previousAssessment.OasisData;
                    currentAssessment.Questions = currentAssessment.OasisData.ToObject<List<Question>>();
                    return Update(currentAssessment);
                }
            }
            return result;
        }

        #endregion
    }
}
