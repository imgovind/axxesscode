﻿namespace Axxess.OasisC.Domain
{
    using System;

    using Enums;

    [Serializable]
    public class RecertificationAssessment : Assessment
    {
        public RecertificationAssessment()
        {
            this.Type = AssessmentType.Recertification;
        }

        public override string ToString()
        {
            return string.Format("Recert: AgencyId: {0}  EpisodeId: {1}  PatientId: {2}",
                this.AgencyId, this.EpisodeId, this.PatientId);
        }
    }
}
