﻿namespace Axxess.OasisC.Domain
{
    public class DiagnosisOrProcedure
    {
        public string Code { get; set; }
        public string Text { get; set; }
        public string Date { get; set; }
    }
}
