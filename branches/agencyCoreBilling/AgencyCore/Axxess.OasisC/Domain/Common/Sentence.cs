﻿namespace Axxess.OasisC.Domain
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class Sentence
    {
        public Sentence()
        {
            this.Items = new List<Item>();
            this.Fields = new List<Field>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Format { get; set; }
        public IList<Item> Items { get; set; }
        public IList<Field> Fields { get; set; }
    }
}
