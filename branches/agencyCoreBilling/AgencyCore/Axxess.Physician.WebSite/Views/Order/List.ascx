﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type='text/javascript'>acore.renamewindow('{0}`s Signed Orders','listpastorders');</script>", Current.DisplayName)%>
<div class="wrapper">
    <%= Html.Telerik().Grid<Order>().Name("List_CompleteOrders").Columns(columns => {
        columns.Bound(o => o.Number).Title("Order #").Sortable(true).Width(110);
        columns.Bound(o => o.AgencyName).Title("Agency").Sortable(true);
        columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
        columns.Bound(o => o.StatusName).Title("Status").Sortable(true);
        columns.Bound(o => o.OrderDate).Title("Order Date").Sortable(true).Width(110);
        columns.Bound(o => o.PrintUrl).Title(" ").Width(30);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("CompleteGrid", "Order")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_CompleteOrders .t-grid-toolbar").html("");
    $(".t-grid-content").css({ 'height': 'auto' });
</script>
    
