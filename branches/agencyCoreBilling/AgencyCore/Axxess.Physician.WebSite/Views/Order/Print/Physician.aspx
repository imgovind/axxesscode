﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PhysicianOrder>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model != null && Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Physician Order<%= Model != null && Model.Patient != null ? (" | " + (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToTitleCase() + ", " : "") + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToTitleCase() + " " : "") + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() : "")) : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Combined(true).Compress(true)) %>
</head>
<body></body><%
Html.Telerik().ScriptRegistrar().Globalization(true) 
     .DefaultGroup(group => group
     .Add("/Modules/pdfprint.min.js")
     .Compress(true).Combined(true).CacheDurationInDays(5))
     .OnDocumentReady(() => {  %>
        pdfprint.cssclass = "physorder";
        pdfprint.vars = {
            "agency": "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "<br />" : "") + (Model.Agency.MainLocation != null ? (Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.ToTitleCase() : "") + (Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : "") + (Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "  " : "") + (Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode.Clean() : "") + (Model.Agency.MainLocation.PhoneWorkFormatted.IsNotNullOrEmpty() ? "<br />Phone: " + Model.Agency.MainLocation.PhoneWorkFormatted : "") + (Model.Agency.MainLocation.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + Model.Agency.MainLocation.FaxNumberFormatted : "") : "") : "").Clean()%>",
            "patientname": "<%= (Model != null && Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() + "<br />" : "<br />") : "").Clean() %>",
            "patient": "<%= (Model != null && Model.Patient != null ? (Model.Patient.AddressLine1.IsNotNullOrEmpty() ? Model.Patient.AddressLine1.ToTitleCase() + "<br />" : "") + (Model.Patient.AddressLine2.IsNotNullOrEmpty() ? Model.Patient.AddressLine2.ToTitleCase() + "<br />" : "") + (Model.Patient.AddressCity.IsNotNullOrEmpty() ? Model.Patient.AddressCity.ToTitleCase() + ", " : "") + (Model.Patient.AddressStateCode.IsNotNullOrEmpty() ? Model.Patient.AddressStateCode.ToTitleCase() + "  " : "") + (Model.Patient.AddressZipCode.IsNotNullOrEmpty() ? Model.Patient.AddressZipCode + "<br />" : "") + (Model.Patient.PhoneHome.IsNotNullOrEmpty() ? Model.Patient.PhoneHome.ToPhone() + "<br />" : "") + (Model.Patient.MedicareNumber.IsNotNullOrEmpty() ? "HIC: " + Model.Patient.MedicareNumber : "") : "").Clean() %>",
            "physicianname": "<%= (Model != null && Model.Physician != null ? (Model.Physician.LastName.IsNotNullOrEmpty() ? Model.Physician.LastName.ToTitleCase() + ", " : "") + (Model.Physician.FirstName.IsNotNullOrEmpty() ? Model.Physician.FirstName.ToTitleCase() + "<br />" : "<br />") : "").Clean() %>",
            "physician": "<%= (Model != null && Model.Physician != null ? (Model.Physician.AddressLine1.IsNotNullOrEmpty() ? Model.Physician.AddressLine1.ToTitleCase() : "") + (Model.Physician.AddressLine2.IsNotNullOrEmpty() ? ", " + Model.Physician.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (Model.Physician.AddressCity.IsNotNullOrEmpty() ? Model.Physician.AddressCity.ToTitleCase() + ", " : "") + (Model.Physician.AddressStateCode.IsNotNullOrEmpty() ? Model.Physician.AddressStateCode.ToTitleCase() + "  " : "") + (Model.Physician.AddressZipCode.IsNotNullOrEmpty() ? Model.Physician.AddressZipCode + "<br />" : "") + (Model.Physician.PhoneWork.IsNotNullOrEmpty() ? "Phone: " + Model.Physician.PhoneWork.ToPhone() : "") + (Model.Physician.FaxNumber.IsNotNullOrEmpty() ? " | Fax: " + Model.Physician.FaxNumber.ToPhone() + "<br />" : "<br />") + (Model.Physician.NPI.IsNotNullOrEmpty() ? "NPI: " + Model.Physician.NPI : "") : "").Clean() %>",
            "orderdate": "<%= (Model != null && Model.OrderDate.IsValid() ? Model.OrderDate.ToShortDateString() : "").Clean() %>",
            "ordernum": "<%= (Model != null && Model.OrderNumber.ToString().IsNotNullOrEmpty() && Model.OrderNumber.ToString().ToInteger() != 0 ? Model.OrderNumber.ToString() : "").Clean() %>",
            "allergies": "<%= (Model != null && Model.Allergies.IsNotNullOrEmpty() ? Model.Allergies : "").Clean() %>",
            "summary": "<%= (Model != null && Model.Summary.IsNotNullOrEmpty() ? Model.Summary : "").Clean() %>",
            "clinsign": "<%= (Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : "").Clean() %>",
            "clinsigndate": "<%= (Model != null && Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString() : "").Clean() %>",
            "physsign": "<%= (Model != null && Model.PhysicianSignatureText.IsNotNullOrEmpty() ? Model.PhysicianSignatureText : "").Clean() %>",
            "physsigndate": "<%= (Model != null && Model.ReceivedDate.IsValid() ? Model.ReceivedDate.ToShortDateString() : "").Clean() %>"
        };
        pdfprint.build("<%= Model != null && Model.Text.IsNotNullOrEmpty() ? Model.Text.Clean() : "" %>"); <%
     }).Render(); %>
</html>