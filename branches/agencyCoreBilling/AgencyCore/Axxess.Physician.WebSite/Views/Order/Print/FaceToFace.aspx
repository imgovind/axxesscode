﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<FaceToFaceEncounter>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
     <title><%= Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>FACE-TO-FACE ENCOUNTER<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
     <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true)) %>
    <% Html.Telerik().ScriptRegistrar().Globalization(true).DefaultGroup(group => group.Add("/Modules/printview.min.js").Compress(true).Combined(true).CacheDurationInDays(5)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.firstheader = "";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr  %3E%3Ctd  colspan=%222%22%3E" +
            '%3Cspan class=%22big%22% %3E <%= Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3C/span%3E %3Cbr /%3E" : ""%><%=Model.Agency != null && Model.Agency.MainLocation != null && Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.Clean().ToTitleCase() : ""%><%=Model.Agency != null && Model.Agency.MainLocation != null && Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%=Model.Agency != null && Model.Agency.MainLocation != null && Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.Clean().ToTitleCase() + ", " : ""%><%=Model.Agency != null && Model.Agency.MainLocation != null && Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%=Model.Agency != null && Model.Agency.MainLocation != null && Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode.Clean() : ""%>' +
            "%3C/td%3E%3Cth colspan=%222%22% class=%22h1%22%3E FACE-TO-FACE ENCOUNTER%3C/th%3E%3C/tr%3E"+
            "%3Ctr %3E%3Ctd colspan=%224%22%3E%3Cspan class=%22medium%22%3EPatient's Name&#160;:&#160; " +
            '<%=Model.Patient!=null? Model.Patient.LastName : ""%>,<%= Model.Patient!=null?Model.Patient.FirstName:"" %> <%=Model.Patient!=null? Model.Patient.MiddleInitial:"" %>' +
            "%3C/span%3E &#160;&#160;&#160; %3Cspan class=%22medium%22%3E MRN&#160;:&#160;<%= Model.Patient!=null?Model.Patient.PatientIdNumber:string.Empty %> %3C/span%3E %3C/td%3E %3C/tr%3E"+
            "%3Ctr %3E%3Ctd colspan=%224%22%3E%3Cspan class=%22medium%22%3E%3Cspan class=%22medium%22%3E Physician Name&#160;:&#160;<%=Model.Physician!=null? Model.Physician.DisplayName:string.Empty %> %3C/span%3E %3Cbr /%3E  %3Cspan class=%22medium%22%3E Physician Address&#160;:&#160;<%= Model.Physician!=null?Model.Physician.AddressFull:string.Empty %>  %3C/span%3E %3Cbr /%3E %3Cspan class=%22medium%22%3E %3Clabel %3E%3Cstrong%3ESOC Date :%3C/strong%3E %3C/label%3E %3Clabel%3E<%= Model.Patient!=null?Model.Patient.StartOfCareDateFormatted:string.Empty %>%3C/label%3E %3C/span%3E %3C/td%3E   %3C/tr%3E%3C/tbody%3E%3C/table%3E ";
    printview.footer = "";
    printview.addsection(
    printview.col(1,
         "<br/><table class='noborders'><tbody><tr><td>"+
        printview.checkbox("<b>POC Certifying Physician.</b> ",<%= Model!=null && Model.Certification.IsNotNullOrEmpty()&&Model.Certification.Contains("1")  ? "true" : "false"%>) +
        "</td><td>"+
        printview.checkbox("<b>Non POC Certifying Physician.</b> ",<%= Model!=null && Model.Certification.IsNotNullOrEmpty()&&Model.Certification.Contains("2")  ? "true" : "false"%>) +
        "</td></tr><tr><td colspan='2'>"+
        "I certify that the above named patient is under my care and that I, or the nurse practitioner or physician’s assistant working with me, had the required face-to-face encounter meeting the encounter requirements on the date below."+
        "</td></tr><tr><td colspan='2'><label><strong>Face To Face Encounter Date:</strong>&#160;______________________________</label>"+
        "</td></tr><tr><td colspan='2'><span><strong><br/>The medical reason, diagnosis, or condition related to the primary reason for home healthcare for the encounter was:</strong></span></td></tr><tr><td colspan='2'>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "</td></tr></tbody></table><br/>"+
        "<span><strong>Clinical Findings that Support the medical need for  home health services and support home patient's homebound status are as follows: </strong></span>"+
        "<table class='noborders'><tbody><tr><td><span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span>"+
        "<span class='line'></span></td></tr></tbody></table><br/>"+
        "<table class='noborders'><tbody><tr><td colspan='3'>&#160;&#160;I hereby certify that based on my clinical finings , the patient is homebound and the following home health services are medically necessary.<em>(Mark all that apply)</em>"+
        "</td></tr><tr><td>"+
         printview.checkbox("<b>Skilled Nursing</b> ",<%= Model!=null && Model.Services.IsNotNullOrEmpty()&&Model.Services.Contains("1")  ? "true" : "false"%>) +
        "</td><td>"+
         printview.checkbox("<b>Physical Therapy</b> ",<%= Model!=null && Model.Services.IsNotNullOrEmpty()&&Model.Services.Contains("2")  ? "true" : "false"%>) +
         "</td><td>"+
         printview.checkbox("<b>Occupational Therapy</b> ",<%= Model!=null && Model.Services.IsNotNullOrEmpty()&&Model.Services.Contains("3")  ? "true" : "false"%>) +
         "</td></tr><tr><td>"+
          printview.checkbox("<b>SpeechTherapy</b> ",<%= Model!=null && Model.Services.IsNotNullOrEmpty()&&Model.Services.Contains("4")  ? "true" : "false"%>) +
        "</td><td>"+
         printview.checkbox("<b>Home Health Aide</b> ",<%= Model!=null && Model.Services.IsNotNullOrEmpty()&&Model.Services.Contains("5")  ? "true" : "false"%>) +
         "</td><td>"+
         printview.checkbox("<b>MSW</b> ",<%= Model!=null && Model.Services.IsNotNullOrEmpty()&&Model.Services.Contains("6")  ? "true" : "false"%>) +
         "<tr><td colspan='3'>"+
         printview.checkbox("<b>Other Services:-</b> _________________________________________________",<%= Model!=null && Model.Services.IsNotNullOrEmpty()&&Model.Services.Contains("7")  ? "true" : "false"%>) +
         "</td></tr></tbody></table><br/>"+
         "<table class='noborders'><tbody><tr><td><label>Physician Signature: _______________________________</label></td><td>Signature Date: _______________________________</td></tr></tbody></table><br/>"
    ));
</script>
</body>
</html>
