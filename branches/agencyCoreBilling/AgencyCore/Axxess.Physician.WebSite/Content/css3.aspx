﻿<%@ Page Language="C#" %><%
Response.ContentType = "text/css";
String prefix = "";
switch (HttpContext.Current.Request.Browser.Browser){
    case "Firefox":
        prefix = "-moz-";
        break;
    case "AppleMAC-Safari":
        prefix = "-webkit-";
        break;
    case "Konqueror":
        prefix = "-khtml-";
        break;
} %>
/*-- gradients --*/
.note_modal{background-image:<%= prefix %>linear-gradient(300deg,#ffff87,#b8b108)}
.note_modal.blue{background-image:<%= prefix %>linear-gradient(300deg,#b6e0ff,#0d78e0)}
.note_modal.red{background-image:<%= prefix %>linear-gradient(300deg,#ff8787,#b80707)}
.modal-gray{background-image:<%= prefix %>linear-gradient(90deg,transparent,rgba(255,255,255,.4))}
#bar_bottom li.active a{background:<%= prefix %>linear-gradient(300deg,rgba(200,200,200,.9),rgba(50,73,126,.7),rgb(50,73,126))}
.agencySelection{background-image:<%= prefix %>linear-gradient(315deg,#ccc,#eee,#aaa,#eee,#bbb)}
#agencySelectionLinks li{background:transparent <%= prefix %>linear-gradient(90deg,rgba(100,100,150,.5),rgba(200,200,200,.8)) repeat scroll 0 0}
#agencySelectionLinks li a:hover{background:transparent <%= prefix %>radial-gradient(#ffc,rgba(255,255,204,.2)) repeat scroll 0 0}
ul.context_menu li.hover{background-image:<%= prefix %>linear-gradient(90deg,#7fadc7,#afd7ed,#7fadc7)}
.t-state-selected{background:#dae7ed <%= prefix %>linear-gradient(90deg,transparent,rgba(255,255,255,.25),transparent)}
.t-grid-content .t-state-hover{background:#e6e8e8 <%= prefix %>linear-gradient(90deg,transparent,rgba(255,255,255,.25),transparent)}
.t-header, .t-grid-header{background:<%= prefix %>linear-gradient(90deg,#abcbf1,#dbe5f1,#cfdff1)}
/*-- transforms --*/
.note_modal.bottom{<%= prefix %>transform:rotate(4deg)}
.note_modal.middle{<%= prefix %>transform:rotate(2deg)}
/*-- border radius --*/
.trical .abs_left,.trical .abs_left a{<%= prefix %>border-radius:0 0 1em 0}
.trical .abs_right,.trical .abs_right a{<%= prefix %>border-radius:0 0 0 1em}
fieldset,.twothirds,.modal-gray,fieldset .shade,#tooltip.calday,.tooltipbox,.page .wrapper .main,.page .wrapper .encapsulation,.agencySelection,#agencySelectionLinks li,#agencySelectionLinks li a:hover{<%= prefix %>border-radius:.9em}
.encapsulation h4,.show_scheduler,.widget,.widget-more,#printreturnreason{<%= prefix %>border-radius:.9em .9em 0 0}
.verttab li{<%= prefix %>border-radius:.9em 0 0 .9em}
.medication .t-grid,.post_controls{<%= prefix %>border-radius:0 0 .8em .8em}
.widget-head{<%= prefix %>border-radius:.7em .7em 0 0}
div.buttons li a,.ancillary_button a,.page .wrapper .encapsulation h5{<%= prefix %>border-radius:.6em}
div.buttons li,.ancillary_button,.diagnosis_input,.masterCalendar .active div,#login-wrapper .button{<%= prefix %>border-radius:.5em}
.medication .t-grid-footer{<%= prefix %>border-radius:0 0 .5em .5em}
.tooltip_oasis,.tooltipbox .content,.notes th,.notes td,.notessub th,.notessub td,#bar_top ul.menu ul.menu,#bar_bottom li a,.window,.ui-dialog,.jGrowl div.jGrowl-notification,div.jGrowl div.jGrowl-closer,.notes ul,.notes ol,.notes ul,.notes ol,.medprofile ul,.medprofile ol,.billing ul,.billing ol,.payroll ul,.payroll ol,.addressblock,#directions,.braden td,.braden th,.braden_score li.strong,#login-wrapper form input,#login-wrapper form textarea,#login-wrapper form select,.ui-widget-shadow,.t-pager .t-state-active,.t-pager .t-state-hover,li.token-input-token-facebook,.notes li.first.last,.medprofile li.first.last,.billing li.first.last,.payroll li.first.last{<%= prefix %>border-radius:.4em}
#printbox,#validationbox,.window_top,.ui-dialog-titlebar,.notes li.first,.medprofile li.first,.billing li.first,.payroll li.first,.billing .discpiline-title,.box-header,.t-tabstrip .t-item{<%= prefix %>border-radius:.4em .4em 0 0}
#printcontrols,#oasis_validation_controls,#bar_top ul.menu,.window_bottom,.notes li.last,.medprofile li.last,.billing li.last,.payroll li.last{<%= prefix %>border-radius:0 0 .4em .4em}
.events ul,.window_min,.window_resize,.window_close,.ui-dialog-titlebar-close,.notification,.teaser_view .replies,.teaser_view .views,.post_view .creation,.t-drag-clue,.t-grouping-header .t-group-indicator,.t-treeview .t-state-hover,.t-treeview .t-state-selected{<%= prefix %>border-radius:.3em}
.t-button{<%= prefix %>border-radius:.2em}
#bar_top ul.menu li:last-child a,.box{<%= prefix %>border-radius:0 0 .2em .2em}
#bar_top ul.menu ul li:first-child a{<%= prefix %>border-radius:.2em .2em 0 0}
.verttab,.general,.notes table td,.notes table th,.notessub table td,.notessub table th,#bar_top ul.menu li:last-child li a,.maximized,.maximized .window_top,.maximized .window_bottom{<%= prefix %>border-radius:0}
/*-- box shadow --*/
#bar_top,#bar_top ul.menu,ul.context_menu,.messageContentPanel div.messageHeaderContainer,.messageContentPanel div.messageBodyContainer{<%= prefix %>box-shadow:#333 .1em .1em .4em}
.window,.ui-dialog,#tooltip.calday,.tooltipbox{<%= prefix %>box-shadow:#555 .2em .2em .6em}
.window.active,.ui-dialog{<%= prefix %>box-shadow:#333 .4em .4em 1.5em}
.window_close:hover,.ui-dialog-titlebar-close:hover{<%= prefix %>box-shadow:#f00 0 0 1em}
.window_min:hover,.window_resize:hover{<%= prefix %>box-shadow:#09f 0 0 1em}
.jGrowl div.jGrowl-notification,div.jGrowl div.jGrowl-closer,.t-menu .t-group,.t-filter-options,.t-datepicker-calendar,.page .wrapper .main,.page .wrapper .encapsulation{<%= prefix %>box-shadow:#bbb .3em .3em .5em}
.cal .active,.notes li.hover,.medprofile li.hover,.billing li.hover,.payroll li.hover,.ancillary_button a:hover,div.buttons li a:hover,.t-state-selected{<%= prefix %>box-shadow:#333 0 .1em .4em}
.t-grid-content .t-state-hover{<%= prefix %>box-shadow:#888 0 .1em .4em}
#masterCalendarTable .active div{<%= prefix %>box-shadow:#555 .5em .5em 2em}
.widget,#printreturnreason,.addressblock,#directions,.diagnosis_input,#agencySelectionLinks li{<%= prefix %>box-shadow:#333 .2em .2em .5em}
.note_modal.bottom,.modal-gray,.agencySelection{<%= prefix %>box-shadow:#000 1em 1em 2em}
.notes th,.notes td,.notessub th,.notessub td,.notes ul,.notes ol,.medprofile ul,.medprofile ol,.billing ul,.billing ol,.payroll ul,.payroll ol,#FinalTabStrip fieldset,.braden td,.braden th{<%= prefix %>box-shadow:#aaa 0 .2em .3em}
.events ul{<%= prefix %>box-shadow:#333 0 .7em .7em}
.ancillary_button a:active,div.buttons li a:active{<%= prefix %>box-shadow:#000 0 0 .5em}
.notes table td,.notes table th,.notessub table td,.notessub table th,.braden .total{<%= prefix %>box-shadow:none}
#login-wrapper form input.error:focus{<%= prefix %>box-shadow:inset #ea5338 0 .1em .2em}
#login-wrapper form input:focus,#login-wrapper form textarea:focus,#login-wrapper form select:focus{<%= prefix %>box-shadow:#d0e1f7 0 0 .3em}
.remittance tr.payinfo th{<%= prefix %>box-shadow:#aaa 0 .2em .3em }
.remittance tr.payinfo td,.remittance tr.reminfo td{<%= prefix %>box-shadow:#aaa 0 .2em .3em }

.remittance tr.even.bottom td{<%= prefix %>box-shadow:#aaa 0 .2em 0em }
.remittance tr.odd.bottom td{<%= prefix %>box-shadow:#aaa 0 .2em 0em}

.remittance tr.odd.bottom td:first-child{<%= prefix %>border-radius:0em 0em 0em .7em}
.remittance tr.odd.bottom td:last-child{<%= prefix %>border-radius:0em 0em .7em 0em}
.remittance tr.even.bottom td:first-child{<%= prefix %>border-radius:0em 0em 0em .7em}
.remittance tr.even.bottom td:last-child{<%= prefix %>border-radius:0em 0em .7em 0em}

.remittance tr.odd.top td:first-child{<%= prefix %>border-radius:.7em 0em 0em 0em}
.remittance tr.odd.top td:last-child{<%= prefix %>border-radius:0em .7em 0em 0em}
.remittance tr.even.top td:first-child{<%= prefix %>border-radius:.7em 0em 0em 0em}
.remittance tr.even.top td:last-child{<%= prefix %>border-radius:0em .7em 0em 0em}
.remittance tr.payinfo td td{<%= prefix %>box-shadow:none}