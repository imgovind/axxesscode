﻿var ChangePassword = {
    Init: function() {
        $("#changePasswordForm").validate({
            rules: {
                Password: { required: true, minlength: 8 }
            },
            messages: {
                Password: { required: "* Required", minlength: "8 characters minimum" }
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { U.block(); },
                    success: function(result) {
                        if (result.isSuccessful) window.location.replace(result.redirectUrl);
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span>' + result.errorMessage + '</span>');
                            U.unBlock();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var Logon = {
    Init: function() {
        $("#login-window").show();
        $("#browser-window").show();
        $("#loginForm").validate({
            messages: { UserName: "", Password: "" },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { U.block(); },
                    success: function(result) {
                        if (result.isSuccessful) window.location.replace(result.redirectUrl);
                        else {
                            if (result.isLocked) {
                                U.unBlock();
                                $("#Login_UserName").attr("disabled", "disabled");
                                $("#Login_Password").attr("disabled", "disabled");
                                $("#Login_RememberMe").hide();
                                $("#Login_Forgot").hide();
                                $("#Login_Button").hide();
                                $("#messages").empty().removeClass().addClass("notification error").append('<span>' + result.errorMessage + '</span>');
                            }
                            else {
                                $("#messages").empty().removeClass().addClass("notification error").append('<span>' + result.errorMessage + '</span>');
                                U.unBlock();
                            }
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var ResetPassword = {
    Init: function() {
        $("#forgotPasswordForm").validate({
            messages: { UserName: "" },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { U.block(); },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $("#messages").empty().removeClass().addClass("notification success").append('<span>' + result.errorMessage + '</span>');
                            $("#forgotPasswordFormContainer").hide();
                        } else $("#messages").empty().removeClass().addClass("notification error").append('<span>' + result.errorMessage + '</span>');
                        U.unBlock();
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
var Activate = {
    Init: function() {
        $("#activateAccountForm").validate({
            rules: {
                Password: { required: true, minlength: 8 }
            },
            messages: {
                Password: { required: "* required", minlength: "8 characters minimum" }
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { U.block(); },
                    success: function(result) {
                        if (result.isSuccessful) window.location.replace(result.redirectUrl);
                        else {
                            $("#messages").empty().removeClass().addClass("notification error").append('<span>' + result.errorMessage + '</span>');
                            U.unBlock();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}
