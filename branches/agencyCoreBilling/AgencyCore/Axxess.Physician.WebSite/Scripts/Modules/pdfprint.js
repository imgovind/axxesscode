﻿var pdfprint = {
    cssclass: "",
    vars: {},
    build: function(content) {
        content = content.replace(/\<br \/\>/g, " <br/> ").split(" ");
        do {
            this.newpage();
            while (content.length && $('.content:last').attr('scrollHeight') <= $('.content:last').attr('offsetHeight')) {
                if ($('.content:first').html().length) content.shift();
                $('.content:last').append(" " + content[0]);
            }
            $('.content:last').html($('.content:last').html().substring(1, $('.content:last').html().lastIndexOf(" ")));
        } while (content.length);
        $(".content").css({ "overflow-y": "hidden" });
    },
    buildsections: function(sections) {
        var first = true;
        this.newpage();
        do {
            var $dompointer = $(".content");
            var content = null;
            $('.content:last').append("<table><thead><tr><th colspan='" + sections[0].content[0].length + "'>" + sections[0].title + "</th></tr></thead><tbody></tbody></table>");
            if (sections[0].content.length == 1 && sections[0].content[0].length == 1) {
                $('.content:last tbody:last').append("<tr><td></td></tr>");
                content = sections[0].content[0][0].replace(/\<br \/\>/g, " <br/> ").split(" ");
                $dompointer = $(".content:last td:last");
            } else {
                content = [this.generatecontent(sections[0].content)];
                $dompointer = $(".content:last tbody:last");
            }
            do {
                while (content.length && $('.content:last').attr('scrollHeight') <= $('.content:last').attr('offsetHeight')) {
                    if (first) first = false;
                    else content.shift();
                    if (content.length) $dompointer.append(" " + content[0]);
                }
                if (content.length) {
                    $('.content:last').html($('.content:last').html().substring(0, $('.content:last').html().lastIndexOf(" ")));
                    this.newpage();
                    $('.content:last').append("<table><thead><tr><th colspan='" + sections[0].content[0].length + "'>" + sections[0].title + "</th></tr></thead><tbody><tr><td></td></tr></tbody></table>");
                    $dompointer = $(".content:last td:last");
                }
                first = true;
            } while (content.length);
            sections.shift();
        } while (sections.length);
        $(".content").each(function() { $(this).css({ "overflow-y": "hidden" }).find("table:last").css({ "border-bottom": "0 none" }) });
    },
    newpage: function() {
        $('body').append($('<div>').addClass('page ' + this.cssclass).html($('<div>').addClass('content')));
        $.each(this.vars, function(index, value) { $('.page:last').append($('<div>').addClass(index).html(value)); });
    },
    generatecontent: function(content) {
        var section = "";
        for (var i = 0; i < content.length; i++) {
            section += unescape("%3Ctr%3E");
            for (var j = 0; j < content[i].length; j++) section += unescape("%3Ctd%3E" + content[i][j] + "%3C/td%3E");
            section += unescape("%3C/tr%3E");
        }
        return section;
    },
    checkbox: function(label, checked) {
        return (checked ? "&#x2612; " : "&#x2610; ") + label;
    }
};