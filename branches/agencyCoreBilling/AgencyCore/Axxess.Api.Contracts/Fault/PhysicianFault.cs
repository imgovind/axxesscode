﻿namespace Axxess.Api.Contracts
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://api.axxessweb.com/PhysicianCacheFault/2011/10/", Name = "PhysicianCacheFault")]
    public class PhysicianCacheFault : DefaultFault
    {
        public PhysicianCacheFault() : base() {  }

        public PhysicianCacheFault(string message) : base((int)FaultReasons.PhysicianCacheFault, message) { }
    }
}
