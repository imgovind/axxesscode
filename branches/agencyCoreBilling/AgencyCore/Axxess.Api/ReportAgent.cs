﻿namespace Axxess.Api
{
    using Axxess.Api.Contracts;

    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    public class ReportAgent : BaseAgent<IReportService>
    {
        #region Overrides

        public override string ToString()
        {
            return "ReportService";
        }

        #endregion

        #region Base Service Methods

        public bool Ping()
        {
            return Service.Ping();
        }

        #endregion

        #region Report Methods

        public List<Dictionary<string, string>> CahpsExport(Guid agencyId, int sampleMonth, int sampleYear)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.CahpsExport(agencyId, sampleMonth, sampleYear), this.ToString());
            return export;
        }

        public List<Dictionary<string, string>> CahpsExport(Guid agencyId, int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var export = new List<Dictionary<string, string>>();
            BaseAgent<IReportService>.Call(c => export = c.CahpsExportByPaymentSources(agencyId, sampleMonth, sampleYear, paymentSources), this.ToString());
            return export;
        }


        #endregion

    }
}
