﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.ViewData
{
    public class ReassignViewData
    {
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string Type { get; set; }
        public string PatientDisplayName { get; set; }
        public string EpisodeRange { get; set; }
    }
}
