﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    
    public class OrderViewData
    {
       public Guid Id { get; set; }
       public Guid PatientId { get; set; }
       public Guid PhysicianId { get; set; }
       public Guid UserId { get; set; }
       public string OrderDate { get; set; }
       public string OrderSummary { get; set; }
       public string OrderText { get; set; }
       public int OrderProcessType { get; set; }
    }
}
