﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;

    public class ClaimInfoSnapShotViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string PayorName { get; set; }
        public string HHRG { get; set; }
        public string HIPPS { get; set; }
        public string ClaimKey { get; set; }
        public double StandardEpisodeRate { get; set; }
        public double SupplyReimbursement { get; set; }
        public double ProspectivePay { get; set; }
        public string Type { get; set; }
        public bool Visible { get; set; }
    }
}
