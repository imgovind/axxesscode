﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public class CommunicationNoteViewData
    {
        public Guid Id {get; set;}
        public Guid PatientId { get; set; }
        public string CommunicationNoteDate { get; set; }
        public string CommunicationNoteText { get; set; }
    }
}
