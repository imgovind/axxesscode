﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using Axxess.AgencyManagement.Domain;

    public class PatientCenterViewData
    {
        public Patient Patient { get; set; }
        public PatientEpisode Episode { get; set; }
        public List<ScheduleEvent> ScheduleEvents { get; set; }
        public List<PatientSelection> Patients { get; set; }
        public int Count { get; set; }
    }
}
