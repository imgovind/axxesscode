﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.ViewData
{
    public class ManagedClaimSnapShotViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string IsuranceIdNumber { get; set; }
        public string PayorName { get; set; }
        public string HHRG { get; set; }
        public string HIPPS { get; set; }
        public string ClaimKey { get; set; }

        public string HealthPlainId { get; set; }
        public string AuthorizationNumber { get; set; }


        public double StandardEpisodeRate { get; set; }
        public double SupplyReimbursement { get; set; }
        public double ProspectivePay { get; set; }
        public string Type { get; set; }
        public bool Visible { get; set; }
    }
}
