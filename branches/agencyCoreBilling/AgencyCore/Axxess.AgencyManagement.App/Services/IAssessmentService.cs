﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    using Axxess.Core;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Enums;

    using Axxess.Api.Contracts;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.LookUp.Domain;
   
    public interface IAssessmentService
    {
        List<Question> Get485FromAssessment(Assessment assessment);
        Assessment SaveAssessment(FormCollection formCollection, HttpFileCollectionBase httpFiles);
        Assessment AddAssessment(ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode);
        Assessment AddAssessment(ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode, string MedicationProfile);
        bool MarkAsDeleted(Guid assessmentId, Guid episodeId, Guid patientId, string assessmentType, bool isDeprecated);
        bool ReassignUser(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, string taskName);
        Assessment GetAssessment(Guid assessmentId, string assessmentType);
        Assessment GetAssessmentWithScheduleType(Guid assessmentId, string assessmentType);
        OasisAudit Audit(Guid assessmentId, Guid patientId, Guid episodeId, string assessmentType);
        ValidationInfoViewData Validate(Guid assessmentId, Guid patientId ,Guid episodeId, string assessmentType);
        ValidationInfoViewData ValidateInactivate(Guid assessmentId, string assessmentType);
        string OasisHeader(Agency agency);
        string OasisFooter(int totalNumberOfRecord);
        List<SubmissionBodyFormat> GetOasisSubmissionFormatInstructions();
        string GetOasisSubmissionFormat(List<SubmissionBodyFormat> instructions, IDictionary<string, Question> assessment, int versionNumber);
        bool UpdatePlanofCare(FormCollection formCollection);
        bool UpdatePlanOfCareForDetail(ScheduleEvent schedule);
        bool UpdatePlanofCareStandAlone(FormCollection formCollection);
        bool UpdatePlanOfCareStandAloneForDetail(ScheduleEvent schedule);
        bool CreatePlanofCare(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment, bool isNonOasis);
        void GeneratePlanofCare(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment, bool isNonOasis);
        bool MarkPlanOfCareAsDeleted(Guid eventId, Guid episodeId, Guid patientId, bool isDeprecated);
        bool MarkPlanOfCareStandAloneAsDeleted(Guid eventId, Guid episodeId, Guid patientId, bool isDeprecated);
        bool ReassignPlanOfCaresUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        ScheduleEvent GetEpisodeAssessmentEvent(Guid episodeId, Guid patientId);
        Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId);
        Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId, DateTime eventDate);
        string GetPlanofCareUrl(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType);
        ScheduleEvent GetPlanofCareScheduleEvent(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType);
        IAssessment InsertMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication,string MedicationType);
        IAssessment DeleteMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication);
        bool UpdateMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Medication medication, string MedicationType);
        Medication GetMedication(Guid AssessmentId, Guid PatientId, string AssessmentType, Guid Id);
        bool UpdateMedicationForDischarge(Guid AssessmentId, Guid PatientId, string AssessmentType, Guid Id, DateTime DischargeDate);
        IDictionary<string, Question> Allergies(Guid assessmentId, string AssessmentType);
        IDictionary<string, Question> Allergies(Assessment assessment);
        IDictionary<string, Question> Diagnosis(Guid assessmentId, string AssessmentType);
        IDictionary<string, Question> Diagnosis(Assessment assessment);
        IDictionary<string, Question> LocatorQuestions(Assessment assessment);
        bool UpdateAssessmentStatus(Guid Id, Guid patientId, Guid episodeId, string assessmentType, string status, string reason);
        bool UpdateAssessmentCorrectionNumber(Guid id, Guid patientId, Guid episodeId, string assessmentType, int CorrectionNumber);
        bool UpdateAssessmentStatusForSubmit(Guid id, Guid patientId, Guid episodeId, string assessmentType, string status, string signature, DateTime date, string timeIn, string timeOut);
        bool UpdateAssessmentForDetail(ScheduleEvent schedule);
        bool MarkAsExported(List<string> OasisSelected);
        bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, string name, Guid assetId);
        bool AddSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, int supplyId, string quantity , string date);
        List<Supply> GetAssessmentSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType);
        bool UpdateSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply);
        bool DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply);
        bool UpdatePlanofCareStatus(Guid episodeId, Guid patientId, Guid eventId, string actionType, string reason);
        Assessment GetAssessmentPrint(AssessmentType Type);
        Assessment GetAssessmentPrint(Guid episodeId, Guid patientId, Guid eventId, AssessmentType Type);
        PlanofCare GetPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId);
        DateRange GetPlanofCareCertPeriod(Guid episodeId, Guid patientId, Guid assessmentId);
        VisitNoteViewData GetVisitNotePrint();
        VisitNoteViewData GetVisitNotePrint(String type);
        VisitNoteViewData GetVisitNotePrint(Guid episodeId, Guid patientId, Guid eventId);
        VisitNoteViewData GetTransportationNote(Guid episodeId, Guid patientId, Guid eventId);
        MissedVisit GetMissedVisitPrint();
        MissedVisit GetMissedVisitPrint(Guid patientId, Guid eventId);

        PlanofCare GetPlanofCare(Guid episodeId, Guid patientId, Guid planofCareId);
        PlanofCareStandAlone GetPlanofCareStandAlone(Guid episodeId, Guid patientId, Guid planofCareId);

        List<AssessmentExport> GetAssessmentByStatus(Guid branchId, ScheduleStatus status, int patientStatus, DateTime StartDate, DateTime EndDate);
        List<AssessmentExport> GetAssessmentByStatus(Guid branchId, ScheduleStatus status, List<int> paymentSources);
    }
}
