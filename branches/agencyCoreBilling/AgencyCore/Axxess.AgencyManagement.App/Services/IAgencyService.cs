﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    
    using Enums;
    using Domain;
    using ViewData;

    public interface IAgencyService
    {
        Agency GetAgency(Guid Id);
        bool CreateAgency(Agency agency);
        bool CreateContact(AgencyContact contact);
        List<UserVisit> GetSchedule();
        List<PatientEpisodeEvent> GetCaseManagerSchedule(Guid branchId, int status);
        bool CreateLocation(AgencyLocation location);
        List<RecertEvent> GetRecertsPastDue();
        List<RecertEvent> GetRecertsPastDue(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate);
        List<RecertEvent> GetRecertsPastDueWidget();
        List<RecertEvent> GetRecertsUpcoming();
        List<RecertEvent> GetRecertsUpcoming(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate);
        List<RecertEvent> GetRecertsUpcomingWidget();
        List<InsuranceViewData> GetInsurances();
        List<Order> GetOrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime startDate, DateTime endDate);
        List<Order> GetProcessedOrders(Guid BranchId, DateTime startDate, DateTime endDate, List<int> status);
        Order GetOrder(Guid id, Guid patientId, string type);
        List<Order> GetOrdersPendingSignature(Guid branchId, DateTime startDate, DateTime endDate);
        bool MarkOrdersAsSent(FormCollection formCollection);
        void MarkOrderAsReturned(Guid id, Guid patientId, OrderType type, DateTime dateReceived);
        bool UpdateOrderDates(Guid id, Guid patientId, OrderType type, DateTime receivedDate, DateTime sendDate);
        List<AddressViewData> GetAgencyFullAddress();

        List<Infection> GetInfections(Guid agencyId);
        List<Incident> GetIncidents(Guid agencyId);
        bool ProcessInfections(string button, Guid patientId, Guid eventId, string reason);
        bool ProcessIncidents(string button, Guid patientId, Guid eventId, string reason);
        List<SelectListItem> Insurances(string value, bool IsAll, bool IsMedicareTradIncluded);
        List<SelectListItem> Branchs(string value, bool IsAll);
        Infection GetInfectionReportPrint(Guid episodeId, Guid patientId, Guid eventId);
        Incident GetIncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId);

        List<PatientEpisodeEvent> GetPrintQueue();
    }
}
