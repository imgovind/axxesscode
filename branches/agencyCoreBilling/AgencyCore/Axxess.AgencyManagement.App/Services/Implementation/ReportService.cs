﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain;
    using ViewData;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.App.Enums;

    public class ReportService : IReportService
    {
        #region Constructor and Private Members

        private readonly IUserRepository userRepository;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly ILookupRepository lookUpRepository;


        public ReportService(IAgencyManagementDataProvider agencyManagementDataProvider, IOasisCDataProvider oasisDataProvider,ILookUpDataProvider lookUpDataProvider ,IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assessmentService = assessmentService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.assessmentRepository = oasisDataProvider.OasisAssessmentRepository;
            this.lookUpRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Patient Reports

        public List<Birthday> GetPatientBirthdays(Guid addressBranchCode)
        {
            IList<Patient> patients = null;
            var birthdays = new List<Birthday>();
            if (addressBranchCode.IsEmpty())
            {
                patients = patientRepository.FindPatientOnly((int)PatientStatus.Active, Current.AgencyId);
            }
            else
            {
                patients = patientRepository.Find((int)PatientStatus.Active, addressBranchCode, Current.AgencyId);
            }
            patients.ForEach(patient =>
            {
                birthdays.Add(new Birthday
                {
                    Id = patient.Id,
                    Name = patient.DisplayName,
                    Date = patient.DOB,
                    AddressLine1 = patient.AddressLine1,
                    AddressLine2 = patient.AddressLine2,
                    AddressCity = patient.AddressCity,
                    AddressStateCode = patient.AddressStateCode,
                    AddressZipCode = patient.AddressZipCode,
                    PhoneHome = patient.PhoneHome,
                    PhoneMobile = patient.PhoneMobile,
                    EmailAddress = patient.EmailAddress
                });
            });

            return birthdays;
        }

        public List<Birthday> GetPatientBirthdays(Guid branchId, int month)
        {
            IList<Patient> patients = null;
            var birthdays = new List<Birthday>();
            if (branchId.IsEmpty())
            {
                patients = patientRepository.FindPatientOnly((int)PatientStatus.Active, Current.AgencyId);
            }
            else
            {
                patients = patientRepository.Find((int)PatientStatus.Active, branchId, Current.AgencyId);
            }
            if (patients != null)
            {

                patients.ForEach(patient =>
                {
                    if (patient.DOB.IsValid() && patient.DOB.Month == month)
                    {
                        birthdays.Add(new Birthday
                        {
                            Id = patient.Id,
                            Name = patient.DisplayName.ToUpperCase(),
                            IdNumber = patient.PatientIdNumber,
                            Date = patient.DOB,
                            AddressLine1 = patient.AddressLine1,
                            AddressLine2 = patient.AddressLine2,
                            AddressCity = patient.AddressCity,
                            AddressStateCode = patient.AddressStateCode,
                            AddressZipCode = patient.AddressZipCode,
                            PhoneHome = patient.PhoneHome,
                            PhoneMobile = patient.PhoneMobile,
                            EmailAddress = patient.EmailAddress
                        });
                    }
                });
            }

            return birthdays.OrderByDescending(b=>b.Date.Day).ThenBy(b=>b.Name).ToList();
        }

        public List<Birthday> GetCurrentBirthdays()
        {
            var birthdays = new List<Birthday>();
            var patients = new List<PatientSelection>();

            if (Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsOfficeManager || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                birthdays = patientRepository.GetCurrentPatientBirthdays(Current.AgencyId);
            }
            else if (Current.IsClinicianOrHHA)
            {
                patients = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active);

                if (patients != null && patients.Count > 0)
                {
                    patients.ForEach(patient =>
                    {
                        if (patient.DOB.Month == DateTime.Now.Month)
                        {
                            birthdays.Add(new Birthday
                            {
                                Date = patient.DOB,
                                PhoneHome = patient.PhoneNumber,
                                Name = patient.DisplayName.ToUpperCase()
                            });
                        }
                    });
                }
            }

            return birthdays;
        }

        public List<AddressBookEntry> GetPatientAddressListing(Guid branchId, int statusId)
        {
            var contacts = new List<AddressBookEntry>();
            var patients = patientRepository.Find(statusId, branchId, Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(patient =>
                {
                    contacts.Add(new AddressBookEntry
                    {
                        Id = patient.Id,
                        Name = patient.DisplayName.ToUpperCase(),
                        IdNumber = patient.PatientIdNumber,
                        AddressLine1 = patient.AddressLine1.ToTitleCase(),
                        AddressLine2 = patient.AddressLine2.ToTitleCase(),
                        AddressCity = patient.AddressCity.ToTitleCase(),
                        AddressStateCode = patient.AddressStateCode,
                        AddressZipCode = patient.AddressZipCode,
                        PhoneHome = patient.PhoneHome.ToPhone(),
                        PhoneMobile = patient.PhoneMobile.ToPhone(),
                        EmailAddress = patient.EmailAddress
                    });
                });
            }

            return contacts.OrderBy(a=>a.Name).ToList();
        }

        public List<EmergencyContactInfo> GetPatientEmergencyContacts(int statusId, Guid branchCode)
        {
            return patientRepository.GetEmergencyContactInfos(Current.AgencyId, branchCode, statusId);
        }

        public List<PatientSocCertPeriod> GetPatientSocCertPeriod(Guid branchId, int statusId, DateTime startDate, DateTime endDate)
        {
            var socCertPeriod = new List<PatientSocCertPeriod>();
            var patients = patientRepository.GetPatientPhysicianInfos(Current.AgencyId, branchId, statusId);
            if (patients != null && patients.Count > 0)
            {
                var ids= patients.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                if (ids.IsNotNullOrEmpty())
                {
                    socCertPeriod = patientRepository.PatientSocCertPeriods(Current.AgencyId, ids , startDate, endDate);
                    if (socCertPeriod != null && socCertPeriod.Count > 0)
                    {
                        socCertPeriod.ForEach(soc =>
                        {
                            var tempPatient = patients.FirstOrDefault(p => p.Id == soc.Id); 
                            if (soc.PatientData.IsNotNullOrEmpty())
                            {
                                var patient = soc.PatientData.ToObject<Patient>();
                                if (patient != null)
                                {
                                    soc.PatientFirstName = patient.FirstName.ToUpperCase();
                                    soc.PatientLastName = patient.LastName.ToUpperCase();
                                    soc.PatientPatientID = patient.PatientIdNumber;
                                    soc.PatientSoC = patient.StartofCareDate.ToString("MM/dd/yyyy");
                                }
                            }
                            else
                            {
                                soc.PatientFirstName = tempPatient.FirstName.ToUpperCase();
                                soc.PatientLastName = tempPatient.LastName.ToUpperCase();
                                soc.PatientPatientID = tempPatient.PatientIdNumber;
                            }
                            if (tempPatient != null)
                            {
                                soc.respEmp = UserEngine.GetName(tempPatient.UserId, Current.AgencyId);
                                soc.PhysicianName = PhysicianEngine.GetName(tempPatient.PhysicianId, Current.AgencyId);
                            }
                        });
                    }
                }
            }
            return socCertPeriod.OrderBy(r=>r.PatientLastName).ThenBy(r=>r.PatientFirstName).ToList();
        }

        public List<PatientOnCallListing> GetPatientOnCallListing(DateTime startDate, DateTime endDate)
        {
            List<PatientOnCallListing> OnCall = new List<PatientOnCallListing>();
            var patients = patientRepository.GetAllByAgencyId(Current.AgencyId);
            patients.ForEach(patient =>
            {
                var lastEpisode = patientRepository.GetEpisode(Current.AgencyId, patient.Id, DateTime.Now, "Nursing");
                var emergencyContact = patientRepository.GetEmergencyContacts(patient.Id).Where(c => c.IsPrimary).FirstOrDefault();

                var onCall = new PatientOnCallListing();
                if (lastEpisode != null)
                {
                    onCall.SocCertPeriod = lastEpisode.StartDateFormatted + " - "
                        + lastEpisode.EndDateFormatted;
                }
                patient.Physician = physicianRepository.GetPatientPhysicians(patient.Id, Current.AgencyId).Where(p => p.Primary).FirstOrDefault();
                var patientRoster = new PatientRoster();
                onCall.Id = patient.Id;
                onCall.PatientPatientID = patient.PatientIdNumber;
                onCall.PatientLastName = patient.LastName;
                onCall.PatientFirstName = patient.FirstName;
                onCall.PatientSoC = patient.StartOfCareDateFormatted;
                if (patient.Physician != null)
                {
                    onCall.PhysicianName = patient.Physician.LastName + ", " + patient.Physician.FirstName;
                    onCall.PhysicianPhone = patient.Physician.PhoneWork;
                    onCall.PhysicianFacsimile = patient.Physician.FaxNumber;
                    onCall.PhysicianPhoneHome = patient.Physician.FaxNumber;
                    onCall.PhysicianEmailAddress = patient.Physician.EmailAddress;
                }

                var emp = userRepository.Get(patient.UserId, Current.AgencyId);
                onCall.respEmp = "";
                if (emp != null)
                {
                    onCall.respEmp = emp.DisplayName;
                }
                if (emergencyContact != null)
                {
                    onCall.ContactName = emergencyContact.FirstName + ", " + emergencyContact.LastName;
                    onCall.ContactRelation = emergencyContact.Relationship;
                    onCall.ContactPhoneHome = emergencyContact.PrimaryPhone;
                    onCall.ContactEmailAddress = emergencyContact.EmailAddress;
                }
                onCall.PatientInsurance = patient.PaymentSource;
                OnCall.Add(onCall);
            });

            return OnCall;
        }

        public List<PatientRoster> GetPatientRoster(Guid branchCode, int statusId, int insuranceId)
        {
            var rosterList = patientRepository.GetPatientRoster(Current.AgencyId, branchCode, statusId, insuranceId);
            if (rosterList != null && rosterList.Count > 0)
            {
                rosterList.ForEach(roster =>
                {
                    var lastEpisode = patientRepository.GetEpisode(Current.AgencyId, roster.Id, DateTime.Now, "Nursing");
                    IDictionary<string, Question> lastAssessment = null;
                    if (lastEpisode != null && !lastEpisode.AssessmentId.IsEmpty() && lastEpisode.AssessmentType.IsNotNullOrEmpty())
                    {
                        lastAssessment = assessmentService.GetAssessment(lastEpisode.AssessmentId, lastEpisode.AssessmentType).ToDictionary();
                    }
                    if (lastAssessment != null && lastAssessment.ContainsKey("M1020PrimaryDiagnosis"))
                    {
                        roster.PatientPrimaryDiagnosis = lastAssessment["M1020PrimaryDiagnosis"].Answer;
                    }
                    else
                    {
                        roster.PatientPrimaryDiagnosis = "";
                    }
                    if (lastAssessment != null && lastAssessment.ContainsKey("M1022PrimaryDiagnosis1"))
                    {
                        roster.PatientSecondaryDiagnosis = lastAssessment["M1022PrimaryDiagnosis1"].Answer;
                    }
                    else
                    {
                        roster.PatientSecondaryDiagnosis = "";
                    }
                    if (!roster.PhysicianId.IsEmpty())
                    {
                        var physician = PhysicianEngine.Get(roster.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            roster.PhysicianNpi = physician.NPI;
                            roster.PhysicianName = physician.DisplayName;
                            roster.PhysicianPhone = physician.PhoneWork.ToPhone();
                            roster.PhysicianFacsimile = physician.FaxNumber;
                            roster.PhysicianPhoneHome = physician.PhoneAlternate;
                            roster.PhysicianEmailAddress = physician.EmailAddress;
                        }
                    }
                });
            }
            return rosterList.OrderBy(o => o.PatientDisplayName).ToList();
        }

        public List<PatientRoster> GetPatientRosterByInsurance(Guid branchCode, int insurance, int statusId)
        {
            var rosterList = new List<PatientRoster>();
            if (insurance > 0)
            {
                rosterList = patientRepository.GetPatientByInsurance(Current.AgencyId, branchCode, insurance, statusId);
            }
            return rosterList.OrderBy(r => r.PatientDisplayName).ToList();
        }

        public List<Authorization> GetExpiringAuthorizaton(Guid branchId, int status)
        {
            var autorizations = new List<Authorization>();
            var patients = patientRepository.Find(status, branchId, Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(patient =>
                {
                    var allAuthorization = patientRepository.GetAuthorizations(Current.AgencyId, patient.Id);
                    if (allAuthorization != null && allAuthorization.Count > 0)
                    {
                        allAuthorization.ForEach(auto =>
                        {
                            if (auto.EndDate <= DateTime.Now.AddDays(14))
                            {
                                auto.DisplayName = patient.DisplayName;
                                autorizations.Add(auto);
                            }
                        });
                    }
                }
                );
            }
            return autorizations;
        }

        public List<PatientRoster> GetPatientByPhysician(Guid agencyPhysicianId)
        {
            var rosterList = new List<PatientRoster>();
            IList<Patient> patients = new List<Patient>();
            if (!agencyPhysicianId.IsEmpty())
            {
                patients = physicianRepository.GetPhysicanPatients(agencyPhysicianId, Current.AgencyId);
                if (patients != null && patients.Count > 0)
                {
                    patients.ForEach(patient =>
                    {
                        var roster = new PatientRoster();
                        roster.Id = patient.Id;
                        roster.PatientId = patient.PatientIdNumber;
                        roster.PatientLastName = patient.LastName.ToUpperCase();
                        roster.PatientFirstName = patient.FirstName.ToUpperCase();
                        roster.PatientGender = patient.Gender;
                        roster.PatientMedicareNumber = patient.MedicareNumber;
                        roster.PatientDOB = patient.DOB;
                        roster.PatientPhone = patient.PhoneHome;
                        roster.PatientAddressLine1 = patient.AddressLine1;
                        roster.PatientAddressLine2 = patient.AddressLine2;
                        roster.PatientAddressCity = patient.AddressCity;
                        roster.PatientAddressStateCode = patient.AddressStateCode;
                        roster.PatientAddressZipCode = patient.AddressZipCode;
                        roster.PatientSoC = patient.StartOfCareDateFormatted;
                        roster.PatientInsurance = patient.PaymentSource;
                        rosterList.Add(roster);

                    });
                }
            }

            return rosterList.OrderBy(r=>r.PatientDisplayName).ToList();
        }

        public List<PatientRoster> GetPatientByResponsiableEmployee(Guid branchCode, Guid userId, int statusId)
        {
            var roster = new List<PatientRoster>();
            var patients = patientRepository.FindByUser(statusId,branchCode, Current.AgencyId,userId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(patient =>
                {
                    var patientRoster = new PatientRoster();
                    patientRoster.Id = patient.Id;
                    patientRoster.PatientId = patient.PatientIdNumber;
                    patientRoster.PatientLastName = patient.LastName.ToUpperCase();
                    patientRoster.PatientFirstName = patient.FirstName.ToUpperCase();
                    patientRoster.PatientAddressLine1 = patient.AddressLine1;
                    patientRoster.PatientAddressLine2 = patient.AddressLine2;
                    patientRoster.PatientAddressCity = patient.AddressCity;
                    patientRoster.PatientAddressStateCode = patient.AddressStateCode;
                    patientRoster.PatientAddressZipCode = patient.AddressZipCode;
                    patientRoster.PatientSoC = patient.StartofCareDate.IsValid() ? patient.StartofCareDate.ToString("MM/dd/yyyy") : "";
                    roster.Add(patientRoster);
                });
            }
            return roster.OrderBy(r=>r.PatientLastName).ThenBy(r=>r.PatientFirstName).ToList();
        }

        public List<PatientRoster> GetPatientByResponsiableByCaseManager(Guid branchCode, Guid caseManagerId, int statusId)
        {
            var roster = new List<PatientRoster>();
            var patients = patientRepository.FindByCaseManager(Current.AgencyId, branchCode, statusId, caseManagerId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(patient =>
                {
                    var patientRoster = new PatientRoster();
                    patientRoster.Id = patient.Id;
                    patientRoster.PatientId = patient.PatientIdNumber;
                    patientRoster.PatientLastName = patient.LastName.ToUpperCase();
                    patientRoster.PatientFirstName = patient.FirstName.ToUpperCase();
                    patientRoster.PatientAddressLine1 = patient.AddressLine1;
                    patientRoster.PatientAddressLine2 = patient.AddressLine2;
                    patientRoster.PatientAddressCity = patient.AddressCity;
                    patientRoster.PatientAddressStateCode = patient.AddressStateCode;
                    patientRoster.PatientAddressZipCode = patient.AddressZipCode;
                    patientRoster.PatientSoC = patient.StartofCareDate.IsValid() ? patient.StartofCareDate.ToString("MM/dd/yyyy") : "";
                    roster.Add(patientRoster);
                });
            }
            return roster.OrderBy(r => r.PatientLastName).ThenBy(r => r.PatientFirstName).ToList();
        }

        public List<SurveyCensus> GetPatientSurveyCensus(Guid branchId, int statusId)
        {
            var surveyCensuses = patientRepository.GetSurveyCensesByStatus(Current.AgencyId, branchId, statusId);

            if (surveyCensuses != null && surveyCensuses.Count > 0)
            {
                var ids = surveyCensuses.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                 //if (ids.IsNotNullOrEmpty())
                 //{
                var surveyCensuseEpisodes = patientRepository.GetSurveyCensesPatientEpisodes(Current.AgencyId, ids);
                 //    if (surveyCensuses != null && surveyCensuses.Count > 0)
                 //    {

                         surveyCensuses.ForEach(surveyCensus =>
                         {
                             var patientSurveyCensuseEpisodes = surveyCensuseEpisodes.Where(s => s.Id == surveyCensus.Id).OrderByDescending(s=>s.StartDate).ToList();
                             if (patientSurveyCensuseEpisodes != null && patientSurveyCensuseEpisodes.Count > 0)
                             {
                                 var episode = patientSurveyCensuseEpisodes.FirstOrDefault();//patientRepository.GetCurrentEpisodeOnly(Current.AgencyId, surveyCensus.Id);
                                 if (episode != null)
                                 {
                                     surveyCensus.CertPeriod = (episode.StartDate.IsValid() ? episode.StartDate.ToString("MM/dd/yyyy") : "") + " - " + (episode.EndDate.IsValid() ? episode.EndDate.ToString("MM/dd/yyyy") : "");
                                     var assessment = GetEpisodeAssessment(episode, patientSurveyCensuseEpisodes.Count >= 2 ? patientSurveyCensuseEpisodes[1] : null);
                                     if (assessment != null)
                                     {
                                         var diagnosis = assessment.Diagnosis();
                                         if (diagnosis != null && diagnosis.Count > 0)
                                         {
                                             surveyCensus.PrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
                                             surveyCensus.SecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
                                         }
                                     }
                                     if (episode.Schedule.IsNotNullOrEmpty())
                                     {
                                         surveyCensus.Discipline = episode.Schedule.ToObject<List<ScheduleEvent>>().Discipline().Join(",");
                                     }
                                 }
                             }
                             surveyCensus.CaseManagerDisplayName = UserEngine.GetName(surveyCensus.CaseManagerId, Current.AgencyId);
                             if (surveyCensus.InsuranceId.IsNotNullOrEmpty() && surveyCensus.InsuranceId.IsInteger())
                             {
                                 var insurance = InsuranceEngine.Instance.Get(surveyCensus.InsuranceId.ToInteger(), Current.AgencyId);
                                 if (insurance != null)
                                 {
                                     surveyCensus.InsuranceName = insurance.Name;
                                 }
                             }
                             //var physiacian = PhysicianEngine.Get(surveyCensus.PhysicianId, Current.AgencyId);
                             //if (physiacian != null)
                             //{
                             //    surveyCensus.PhysicianDisplayName = physiacian.DisplayName;
                             //    surveyCensus.PhysicianNPI = physiacian.NPI;
                             //    surveyCensus.PhysicianPhone = physiacian.PhoneWork.ToPhone();
                             //    surveyCensus.PhysicianFax = physiacian.FaxNumber;
                             //}
                         });
                 //    }
                 //}
            }
            return surveyCensuses;
        }

        public List<PatientRoster> GetPatientMonthlyAdmission( Guid branchCode ,int statusId, int month, int year)
        {
            var rosterList = patientRepository.GetPatientByAdmissionMonthYear(Current.AgencyId, branchCode, statusId, month, year);
            return rosterList.OrderBy(o => o.PatientFirstName).ThenBy(o=>o.PatientLastName).ToList();
        }

        public List<PatientRoster> GetPatientAnnualAdmission(Guid branchCode, int statusId, int year)
        {
            var rosterList = patientRepository.GetPatientByAdmissionYear(Current.AgencyId, branchCode, statusId, year);
            return rosterList.OrderBy(o => o.PatientFirstName).ToList();
        }

        #endregion

        #region Clinical Reports

        public IList<OpenOasis> GetAllOpenOasis(Guid branchCode, DateTime startDate, DateTime endDate)
        {
            var openOasisList = new List<OpenOasis>();
            var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, branchCode, startDate, endDate);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                patientEpisodes.ForEach(patientEpisode =>
                {
                    if (patientEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && e.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && e.EventDate.ToDateTime().Date >= startDate.Date && e.EventDate.ToDateTime().Date <= endDate.Date && e.IsDeprecated == false && e.IsAssessment() && e.IsOasisOpen() && !e.IsMissedVisit).ToList();
                        if (events != null && events.Count > 0)
                        {
                            events.ForEach(e =>
                            {
                                var openOasis = new OpenOasis();
                                openOasis.PatientIdNumber = patientEpisode.PatientIdNumber;
                                openOasis.PatientName = patientEpisode.PatientName.ToUpperCase();
                                openOasis.AssessmentName = e.DisciplineTaskName;
                                openOasis.Status = e.StatusName;
                                openOasis.Date = e.EventDate.IsNotNullOrEmpty() && e.EventDate.IsValidDate() ? e.EventDate.ToDateTime().ToString("MM/dd/yyyy") : "";
                                if (!e.UserId.IsEmpty())
                                {
                                    openOasis.CurrentlyAssigned = UserEngine.GetName(e.UserId, Current.AgencyId);
                                }
                                openOasisList.Add(openOasis);
                            });
                        }
                    }
                });
            }
            return openOasisList.OrderBy(o => o.PatientName).ToList();
        }

        public IList<ClinicalOrder> GetOrders(int statusId)
        {
            IList<ClinicalOrder> orderList = new List<ClinicalOrder>();

            var patients = patientRepository.FindPatientOnly((int)PatientStatus.Active, Current.AgencyId);
            patients.ForEach(patient =>
            {
                var patientEpisodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patient.Id);
                if (patientEpisodes!=null && patientEpisodes.Count > 0)
                {
                    patientEpisodes.ForEach(episode =>
                    {
                        if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                        {
                           var events = episode.Schedule.ToObject<List<ScheduleEvent>>();
                            if (events != null && events.Count > 0)
                            {
                                events.ForEach(e =>
                                {
                                    if (e.IsOrderAndStatus(statusId))
                                    {
                                        var order = patientRepository.GetOrder(e.EventId, patient.Id, Current.AgencyId);
                                        if (order != null)
                                        {
                                            var clinicalOrder = new ClinicalOrder();
                                            clinicalOrder.Id = e.EventId.ToString();
                                            clinicalOrder.Type = e.DisciplineTaskName;
                                            clinicalOrder.Number = order.OrderNumber.ToString();
                                            clinicalOrder.PatientName = patient.DisplayName.ToTitleCase();
                                            clinicalOrder.Physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId).DisplayName;
                                            clinicalOrder.Status = e.StatusName;
                                            clinicalOrder.CreatedDate = order.Created.ToShortDateString();
                                            orderList.Add(clinicalOrder);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });

            return orderList;
        }

        public List<MissedVisit> GetAllMissedVisit(Guid branchCode, DateTime startDate, DateTime endDate)
        {
            var missedVisitList = new List<MissedVisit>();
            var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, branchCode, startDate, endDate);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                patientEpisodes.ForEach(patientEpisode =>
                {
                    if (patientEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e =>e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && e.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && e.EventDate.ToDateTime().Date <= endDate.Date && e.EventDate.ToDateTime().Date >= startDate.Date && e.IsMissedVisit).ToList();
                        if (events != null && events.Count > 0)
                        {
                            events.ForEach(e =>
                            {
                                var missedVisit = new MissedVisit();
                                missedVisit.PatientIdNumber = patientEpisode.PatientIdNumber;
                                missedVisit.PatientName = patientEpisode.PatientName.ToUpperCase();
                                missedVisit.Date = e.EventDate.IsNotNullOrEmpty() && e.EventDate.IsValidDate() ? e.EventDate.ToDateTime() : DateTime.MinValue;
                                missedVisit.DisciplineTaskName = e.DisciplineTaskName;
                                if (!e.UserId.IsEmpty())
                                {
                                    missedVisit.UserName = UserEngine.GetName(e.UserId, Current.AgencyId);
                                }
                                missedVisitList.Add(missedVisit);

                            });
                        }
                    }
                });
            }
            return missedVisitList;
        }

        public IList<PhysicianOrder> GetPhysicianOrderHistory(Guid branchCode, int status, DateTime startDate, DateTime endDate)
        {
            var branchPhysicianOrder = new List<PhysicianOrder>();
            var schedules = patientRepository.GetPhysicianOrderScheduleEvents(Current.AgencyId, startDate, endDate, status);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersIds = schedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                branchPhysicianOrder = patientRepository.GetPhysicianOrders(Current.AgencyId, physicianOrdersIds, startDate, endDate);
            }
            return branchPhysicianOrder.OrderBy(o => o.DisplayName).ToList();
        }

        public IList<Order> GetPlanOfCareHistory(Guid branchCode, int status, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var schedules = patientRepository.GetPlanOfCareOrderScheduleEvents(Current.AgencyId, startDate, endDate, status);
            if (schedules != null && schedules.Count > 0)
            {
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, planofCareOrdersIds);
                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                            var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485,
                                    Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SendDate = poc.SentDate
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAlones(Current.AgencyId,  planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SendDate = poc.SentDate
                                });
                            }
                        });
                    }
                }
            }
            return orders.OrderBy(o => o.PatientName).ToList();
        }

        #endregion

        #region Schedule Reports

        public List<ScheduleEvent> GetPatientScheduleEventsByDateRange(Guid patientId, DateTime fromDate, DateTime toDate)
        {
            var outputEvents = new List<ScheduleEvent>();
            var patientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, patientId, fromDate, toDate);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                patientEpisodes.ForEach(patientEpisode =>
                {
                    if (patientEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var events = patientEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.EventDate.IsValidDate() && e.EventDate.ToDateTime().Date <= patientEpisode.EndDate.ToDateTime().Date && e.EventDate.ToDateTime().Date >= patientEpisode.StartDate.ToDateTime().Date && e.EventDate.ToDateTime().Date >= fromDate.Date && e.EventDate.ToDateTime().Date <= toDate.Date).ToList();
                        if (events != null && events.Count > 0)
                        {
                            events.ForEach(e =>
                            {
                                e.PatientName = patientEpisode.PatientName.ToUpperCase();
                                e.PatientIdNumber = patientEpisode.PatientIdNumber;
                                e.EventDate = e.EventDate.ToZeroFilled();
                                if (!e.UserId.IsEmpty())
                                {
                                    e.UserName = UserEngine.GetName(e.UserId, Current.AgencyId);
                                }
                                if (e.StatusName.IsEqual("Not Yet Started") || e.StatusName.IsEqual("Not Yet Due"))
                                {
                                    e.VisitDate = string.Empty;
                                }
                                outputEvents.Add(e);
                            });
                        }
                    }
                });
            }

            return outputEvents.OrderBy(s => s.PatientName).ToList();
        }

        public List<UserVisit> GetUserScheduleEventsByDateRange(Guid userId, Guid branchCode, DateTime from, DateTime to)
        {
            var userVisits = new List<UserVisit>();
            var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, branchCode, from, to);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                patientEpisodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
                    {
                        var scheduledEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                            s.EventId != Guid.Empty && s.UserId == userId && s.IsDeprecated == false && s.IsMissedVisit == false
                           && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date
                           && s.EventDate.ToDateTime().Date >= from.Date && s.EventDate.ToDateTime().Date <= to.Date
                           && !s.IsCompleted() && s.DisciplineTask != (int)DisciplineTasks.Rap && s.DisciplineTask != (int)DisciplineTasks.Final
                            ).ToList();


                        if ( scheduledEvents!=null && scheduledEvents.Count > 0)
                        {
                            scheduledEvents.ForEach(scheduledEvent =>
                            {
                                if (scheduledEvent != null)
                                {
                                    scheduledEvent.EndDate = episode.EndDate.ToDateTime();
                                    scheduledEvent.StartDate = episode.StartDate.ToDateTime();

                                    var userVisit = new UserVisit
                                    {
                                        Status = scheduledEvent.Status,
                                        StatusName = scheduledEvent.StatusName,
                                        PatientName = episode.PatientName,
                                        TaskName = scheduledEvent.DisciplineTaskName,
                                        UserDisplayName = UserEngine.GetName(scheduledEvent.UserId, Current.AgencyId),
                                        VisitDate = scheduledEvent.VisitDate.IsNotNullOrEmpty()
                                       && scheduledEvent.VisitDate.IsValidDate()
                                       && scheduledEvent.VisitDate.ToDateTime().Date <= DateTime.Now.Date ? scheduledEvent.VisitDate.ToZeroFilled() : "",
                                        ScheduleDate = scheduledEvent.EventDate.IsNotNullOrEmpty()
                                       && scheduledEvent.EventDate.IsValidDate() ? scheduledEvent.EventDate.ToZeroFilled() : ""
                                    };

                                    if (scheduledEvent.StatusName.IsEqual("Not Yet Started") || scheduledEvent.StatusName.IsEqual("Not Yet Due"))
                                    {
                                        userVisit.VisitDate = string.Empty;
                                    }

                                    userVisits.Add(userVisit);

                                }
                            });
                        }
                    }
                });
            }

            return userVisits.OrderBy(v => v.PatientName).ToList();
        }

        public List<ScheduleEvent> GetPastDueScheduleEvents(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var episodeByBranch = patientRepository.GetEpisodeByBranch(branchId, Current.AgencyId);
            var scheduleEvents = new List<ScheduleEvent>();
            if (episodeByBranch != null && episodeByBranch.Count>0)
            {
                episodeByBranch.ForEach(e =>
                {
                    if (e.StartDate.IsValidDate() && e.EndDate.IsValidDate())
                    {
                        var schedules = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && s.EventDate.ToDateTime().Date <= e.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= e.StartDate.ToDateTime().Date && s.IsDeprecated == false && s.IsPastDue).ToList();

                        if (schedules != null && schedules.Count > 0)
                        {
                            schedules.ForEach(s =>
                            {
                                s.PatientName = e.PatientName.ToUpper();
                                s.PatientIdNumber = e.PatientIdNumber;

                                if (!s.UserId.IsEmpty())
                                {
                                    s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
                                }
                                s.EventDate = s.EventDate.ToZeroFilled();
                               
                                scheduleEvents.Add(s);
                            });
                        }
                    }
                });
            }
            return scheduleEvents.OrderBy(s => s.PatientName).ToList();
        }

        public List<ScheduleEvent> GetPastDueScheduleEventsByDiscipline(Guid branchId, string discipline, DateTime startDate, DateTime endDate)
        {
            var episodeByBranch = patientRepository.GetEpisodeByBranch(branchId, Current.AgencyId);
            var scheduleEvents = new List<ScheduleEvent>();
            if (episodeByBranch != null && episodeByBranch.Count>0)
            {
                episodeByBranch.ForEach(e =>
                {
                    if (e.StartDate.IsValidDate() && e.EndDate.IsValidDate())
                    {
                        var schedules = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && s.EventDate.ToDateTime().Date <= e.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= e.StartDate.ToDateTime().Date && s.IsDeprecated == false && s.Discipline == discipline && s.IsPastDue).ToList();
                        if (schedules != null && schedules.Count>0)
                        {
                            schedules.ForEach(s =>
                            {
                                s.PatientName = e.PatientName.ToUpper();
                                s.PatientIdNumber = e.PatientIdNumber;
                                if (!s.UserId.IsEmpty())
                                {
                                    s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
                                }
                                s.EventDate = s.EventDate.ToZeroFilled();
                               
                                scheduleEvents.Add(s);
                            });
                        }
                    }
                });
            }
            return scheduleEvents.OrderBy(s=>s.PatientName).ToList();
        }

        public List<ScheduleEvent> GetScheduleEventsByDateRange(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var episodeByBranch = patientRepository.GetEpisodeByBranch(branchId, Current.AgencyId);
            var scheduleEvents = new List<ScheduleEvent>();
            if (episodeByBranch != null)
            {
                episodeByBranch.ForEach(e =>
                {
                    if (e.StartDate.IsValidDate() && e.EndDate.IsValidDate())
                    {
                        var schedules = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && s.EventDate.ToDateTime().Date <= e.EndDate.ToDateTime().Date && s.EventDate.ToDateTime().Date >= e.StartDate.ToDateTime().Date && s.IsDeprecated == false).ToList();
                        if (schedules != null && schedules.Count > 0)
                        {
                                schedules.ForEach(s =>
                                {
                                        s.PatientName = e.PatientName.ToUpperCase();
                                        s.PatientIdNumber = e.PatientIdNumber;
                                        if (!s.UserId.IsEmpty())
                                        {
                                            s.UserName = UserEngine.GetName(s.UserId, Current.AgencyId);
                                        }
                                        scheduleEvents.Add(s);
                                });
                        }
                    }
                });
            }
            return scheduleEvents;
        }

        public List<ScheduleEvent> GetCaseManagerScheduleByBranch(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var schedule = new List<ScheduleEvent>();
            var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, branchId, startDate, endDate);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                patientEpisodes.ForEach(episode =>
                {
                    if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
                    {
                        var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>
                          s.EventId != Guid.Empty && s.IsDeprecated == false && s.IsMissedVisit == false
                         && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date
                         && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date
                         && (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString() || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString() || s.Status == ((int)ScheduleStatus.NoteReopened).ToString() || s.Status == ((int)ScheduleStatus.OasisReopened).ToString()) && s.DisciplineTask != (int)DisciplineTasks.Rap && s.DisciplineTask != (int)DisciplineTasks.Final
                          ).ToList();
                        scheduleEvents.ForEach(scheduleEvent =>
                        {
                            scheduleEvent.PatientName = episode.PatientName;
                            scheduleEvent.PatientIdNumber = episode.PatientIdNumber;
                            scheduleEvent.EventDate = scheduleEvent.EventDate.ToZeroFilled();
                            if (!scheduleEvent.UserId.IsEmpty())
                            {
                                scheduleEvent.UserName = UserEngine.GetName(scheduleEvent.UserId, Current.AgencyId);
                            }
                            schedule.Add(scheduleEvent);
                        });
                    }
                });
            }
            return schedule.OrderBy(o => o.PatientName).ToList();
        }

        public List<ScheduleEvent> GetScheduleDeviation(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var schedule = new List<ScheduleEvent>();
           var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, branchId, startDate, endDate);
           if (patientEpisodes != null && patientEpisodes.Count > 0)
           {
               patientEpisodes.ForEach(episode =>
               {
                   if (episode.Schedule.IsNotNullOrEmpty() && episode.EndDate.IsValidDate() && episode.StartDate.IsValidDate())
                   {
                       var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(e => e.VisitDate.IsNotNullOrEmpty() && e.VisitDate.IsValidDate() && e.EventDate.IsNotNullOrEmpty() && e.EventDate.IsValidDate() && !(0 == e.VisitDate.ToDateTime().CompareTo(e.EventDate.ToDateTime())) && e.EventDate.ToDateTime().Date>=startDate.Date && e.EventDate.ToDateTime().Date<=endDate.Date).ToList();
                       if (scheduleEvents != null && scheduleEvents.Count > 0)
                       {
                           scheduleEvents.ForEach(scheduleEvent =>
                           {
                               scheduleEvent.PatientName = episode.PatientName.ToUpperCase();
                               scheduleEvent.PatientIdNumber = episode.PatientIdNumber;
                               scheduleEvent.EventDate = scheduleEvent.EventDate.ToZeroFilled();
                               scheduleEvent.VisitDate = scheduleEvent.VisitDate.ToZeroFilled();
                               if (!scheduleEvent.UserId.IsEmpty())
                               {
                                   scheduleEvent.UserName = UserEngine.GetName(scheduleEvent.UserId, Current.AgencyId);
                               }
                               schedule.Add(scheduleEvent);
                           });
                       }
                   }
               });
           }
            return schedule;
        }

        #endregion

        #region Billing Reports

        public List<TypeOfBill> UnProcessedBillViewData(Guid branchId, string type)
        {
            var listOfbill = new List<TypeOfBill>();
            if (type.IsEqual("RAP"))
            {
                listOfbill = billingRepository.GetRapsByStatus(Current.AgencyId, (int)ScheduleStatus.ClaimCreated);
            }
            else if (type.IsEqual("Final"))
            {
                listOfbill = billingRepository.GetFinalsByStatus(Current.AgencyId, (int)ScheduleStatus.ClaimCreated);
            }
            else
            {
                var raps = billingRepository.GetRapsByStatus(Current.AgencyId, (int)ScheduleStatus.ClaimCreated);
                if (raps != null && raps.Count > 0)
                {
                    listOfbill.AddRange(raps);
                }

                var finals =  billingRepository.GetFinalsByStatus(Current.AgencyId, (int)ScheduleStatus.ClaimCreated);
                if (finals != null && finals.Count > 0)
                {
                    listOfbill.AddRange(finals);
                }
            }
            return listOfbill.OrderBy(b=>b.LastName).ThenBy(b=>b.FirstName).ToList();
        }

        public List<ClaimLean> BillViewDataByStatus(Guid branchId, string type, int status, DateTime startDate, DateTime endDate)
        {
            var listOfBill = new List<ClaimLean>();
            if (type.IsEqual("RAP"))
            {
                var raps = billingRepository.GetRapClaims(Current.AgencyId, branchId, status, startDate, endDate);
                if (raps != null && raps.Count>=0)
                {
                    listOfBill.AddRange(raps);
                         
                }
            }
            else if (type.IsEqual("Final"))
            {
                var finals = billingRepository.GetFinalClaims(Current.AgencyId, branchId,  status, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            else
            {
                var raps = billingRepository.GetRapClaims(Current.AgencyId, branchId,  status, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);

                }
                var finals = billingRepository.GetFinalClaims(Current.AgencyId, branchId,  status, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            return listOfBill.OrderBy(b => b.DisplayName).ToList();
        }

        public List<ClaimLean> SubmittedBillViewDataByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate)
        {
            var listOfBill = new List<ClaimLean>();
            if (type.IsEqual("RAP"))
            {
                var raps = billingRepository.GetRapClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);

                }
            }
            else if (type.IsEqual("Final"))
            {
                var finals = billingRepository.GetFinalClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            else
            {
                var raps = billingRepository.GetRapClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);

                }
                var finals = billingRepository.GetFinalClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            return listOfBill.OrderBy(b => b.DisplayName).ToList();
        }

        public IList<ClaimBill> AllUnProcessedRap(Guid branchId)
        {
            return billingRepository.GetOutstandingRapClaims(Current.AgencyId, branchId, 0);
        }

        public IList<ClaimBill> AllUnProcessedFinal(Guid branchId)
        {
            return billingRepository.GetOutstandingFinalClaims(Current.AgencyId, branchId, 0);
        }

        public IList<ClaimBill> GetPotentialCliamAutoCancels(Guid branchId)
        {
            var finalAutoCancel = new List<ClaimBill>();
            var finals = AllUnProcessedFinal(branchId);
            if (finals != null && finals.Count > 0)
            {
                finals.ForEach(final =>
                {
                    var rap = billingRepository.GetRap(Current.AgencyId, final.Id);
                    if (rap != null)
                    {
                        if ((rap.Status == (int)ScheduleStatus.ClaimAccepted || rap.Status == (int)ScheduleStatus.ClaimPaidClaim || rap.Status == (int)ScheduleStatus.ClaimPaymentPending || rap.Status == (int)ScheduleStatus.ClaimSubmitted) && (final.Status == (int)ScheduleStatus.ClaimCreated || final.Status == (int)ScheduleStatus.ClaimReOpen || final.Status == (int)ScheduleStatus.ClaimRejected || final.Status == (int)ScheduleStatus.ClaimWithErrors || final.Status == (int)ScheduleStatus.ClaimCancelledClaim) && (rap.ClaimDate.AddDays(76) >= DateTime.Now))
                        {
                            finalAutoCancel.Add(final);
                        }
                    }
                });
            }
            return finalAutoCancel.OrderBy(r => r.LastName).ThenBy(r => r.FirstName).ToList(); ;
        }

        #endregion

        #region Employee Reports

        public List<User> GetEmployeeRoster(Guid branchCode, int status)
        {
            var users = userRepository.GetEmployeeRoster(Current.AgencyId, branchCode, status);
            return users.OrderBy(e => e.DisplayName).ToList();
        }

        public List<Birthday> GetEmployeeBirthdays(Guid branchCode, int status, int month)
        {
            var birthdays = new List<Birthday>();
            var users = new List<User>();
            if (status == 0)
            {
                if (branchCode.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchCode, Current.AgencyId).ToList();
                }
            }
            else
            {
                if (branchCode.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId, status).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchCode, Current.AgencyId, status).ToList();
                }
            }
            if (users != null)
            {
                users.ForEach(user =>
                {
                    if (user.ProfileData.IsNotNullOrEmpty())
                    {
                        user.Profile = user.ProfileData.ToObject<UserProfile>();
                        user.EmailAddress = user.Profile.EmailWork;
                    }
                    if (user.Profile != null && user.Profile.DOB.IsValid() && user.Profile.DOB.Month == month)
                    {
                        birthdays.Add(new Birthday { Name = user.DisplayName, Date = user.Profile.DOB, AddressLine1 = user.Profile.AddressLine1, AddressLine2 = user.Profile.AddressLine2, AddressCity = user.Profile.AddressCity, AddressStateCode = user.Profile.AddressZipCode, PhoneHome = user.Profile.PhoneHome });
                    }
                });
            }
            return birthdays;
        }

        public List<License> GetEmployeeExpiringLicenses(Guid branchCode, int status)
        {
            var license = new List<License>();
            var users = new List<User>();
            if (status == 0)
            {
                if (branchCode.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchCode, Current.AgencyId).ToList();
                }
            }
            else
            {
                if (branchCode.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId, status).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchCode, Current.AgencyId, status).ToList();
                }
            }
            if (users != null)
            {
                users.ForEach(user =>
                {
                    if (user.Licenses.IsNotNullOrEmpty())
                    {
                        var userLicenses = user.Licenses.ToObject<List<License>>();

                        if (userLicenses != null)
                        {
                            userLicenses.ForEach(l =>
                            {
                                if (l.ExpirationDate <= DateTime.Now.AddDays(60))
                                {
                                    l.UserDispalyName = user.DisplayName;
                                    license.Add(l);
                                }
                            });
                        }
                    }
                });
            }
            return license;
        }

        public List<UserVisit> GetEmployeeScheduleByDateRange(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var schedules = new List<UserVisit>();
            var patientEpisodes = patientRepository.GetPatientEpisodeDataByBranch(Current.AgencyId, branchId, startDate, endDate);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                patientEpisodes.ForEach(episode =>
                {
                    if (episode.StartDate.IsValidDate() && episode.EndDate.IsValidDate() && episode.Schedule.IsNotNullOrEmpty())
                    {
                        var scheduleList = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.UserId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date && !s.IsDeprecated && !s.IsMissedVisit).ToList();
                        if (scheduleList != null && scheduleList.Count > 0)
                        {
                            scheduleList.ForEach(s =>
                            {
                                schedules.Add(new UserVisit
                                 {
                                     ScheduleDate = s.EventDate.ToZeroFilled(),
                                     VisitDate = s.StatusName.IsEqual("Not Yet Started") || s.StatusName.IsEqual("Not Yet Due") ? string.Empty : s.VisitDate.ToZeroFilled(),
                                     PatientName = episode.PatientName.ToUpperCase(),
                                     UserDisplayName = UserEngine.GetName(s.UserId, Current.AgencyId).ToUpperCase(),
                                     StatusName = s.StatusName,
                                     TaskName = Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), s.DisciplineTask.ToString())).GetDescription() : ""
                                 });
                            });
                        }
                    }
                });
            }

            return schedules.OrderBy(e => e.UserDisplayName).ToList();
        }

        #endregion

        #region Statistical Reports

        public List<UserVisit> GetEmployeeVisistList(Guid userId, DateTime startDate, DateTime endDate)
        {
            var schedules = new List<UserVisit>();
            var patientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId, startDate, endDate);
            if (patientEpisodes != null && patientEpisodes.Count > 0)
            {
                patientEpisodes.ForEach(episode =>
                {
                    if (episode.StartDate.IsValidDate() && episode.EndDate.IsValidDate() && episode.Schedule.IsNotNullOrEmpty())
                    {
                        var scheduleList = episode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.UserId.IsEmpty() && s.UserId == userId && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= startDate.Date && s.EventDate.ToDateTime().Date <= endDate.Date && s.EventDate.ToDateTime().Date >= episode.StartDate.ToDateTime().Date && s.EventDate.ToDateTime().Date <= episode.EndDate.ToDateTime().Date && !s.IsDeprecated && s.Discipline != Disciplines.Claim.ToString()).ToList();
                        if (scheduleList != null && scheduleList.Count > 0)
                        {
                            scheduleList.ForEach(s =>
                            {
                                schedules.Add(new UserVisit
                                {
                                    ScheduleDate = s.EventDate.ToZeroFilled(),
                                    VisitDate = s.StatusName.IsEqual("Not Yet Started") || s.StatusName.IsEqual("Not Yet Due") ? string.Empty : s.VisitDate.ToZeroFilled(),
                                    PatientName = episode.PatientName.ToUpperCase(),
                                    StatusName = s.StatusName,
                                    TaskName = Enum.IsDefined(typeof(DisciplineTasks), s.DisciplineTask) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), s.DisciplineTask.ToString())).GetDescription() : ""
                                });
                            });
                        }
                    }
                });
            }

            return schedules.ToList();
        }

        //Duplicate with the assessment service
        public Assessment GetEpisodeAssessment(PatientEpisode currentEpisode, PatientEpisode previousEpisode)
        {
            Assessment assessment = null;
            if (currentEpisode != null && currentEpisode.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = currentEpisode.Schedule.ToObject<List<ScheduleEvent>>();
                scheduleEvents.ForEach(e =>
                {
                    if (e.IsStartofCareAssessment())
                    {
                        assessment = assessmentRepository.Get(e.EventId, "StartOfCare", Current.AgencyId);
                        return;
                    }
                });

                if (assessment == null && previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                {
                    var prevEpisodeEvents = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= currentEpisode.StartDate.AddDays(-5).Date) && (s.EventDate.ToDateTime().Date < currentEpisode.StartDate.Date) && !s.IsDeprecated && !s.IsMissedVisit).ToList();
                    if (assessment == null)
                    {
                        prevEpisodeEvents.ForEach(e =>
                        {
                            if (e.IsRecertificationAssessment())
                            {
                                assessment = assessmentRepository.Get(e.EventId, "Recertification", Current.AgencyId);
                                return;
                            }
                        });

                        if (assessment == null)
                        {
                            prevEpisodeEvents.ForEach(e =>
                            {
                                if (e.IsResumptionofCareAssessment())
                                {
                                    assessment = assessmentRepository.Get(e.EventId, "ResumptionOfCare", Current.AgencyId);
                                    return;
                                }
                            });

                        }
                    }
                }
            }
            return assessment;
        }

        #endregion

    }
}
