﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    using System.Xml.Linq;
    using System.Net;
    using System.IO;
    using System.Text.RegularExpressions;

    using ViewData;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;

    using Axxess.Membership.Logging;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.Log.Enums;

    using iTextSharp.text;
    using Axxess.AgencyManagement.App.Enums;
    using System.Web;



    public class BillingService : IBillingService
    {
        #region Private Members

        private readonly IUserRepository userRepository;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IReferralRepository referrralRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly ILookupRepository lookUpRepository;

        #endregion

        #region Constructor

        public BillingService(IAgencyManagementDataProvider dataProvider, IOasisCDataProvider oasisCDataProvider, ILookUpDataProvider lookUpDataProvider, IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            this.assessmentService = assessmentService;
            this.patientRepository = dataProvider.PatientRepository;
            this.userRepository = dataProvider.UserRepository;
            this.referrralRepository = dataProvider.ReferralRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.assessmentRepository = oasisCDataProvider.OasisAssessmentRepository;
            this.lookUpRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Public Methods

        public bool AddRap(Rap rap)
        {
            var result = false;
            if (billingRepository.AddRap(rap))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public bool AddFinal(Final final)
        {
            var result = false;
            if (billingRepository.AddFinal(final))
            {
                Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public string GenerateJson(List<Guid> rapToGenerate, List<Guid> finalToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, Agency agency, AxxessSubmitterInfo payerInfo, out List<Rap> raps, out List<Final> finlas, bool isHMO, int insuranceId , AgencyLocation branch)
        {
            string requestArr = string.Empty;
            claimInfo = new List<ClaimInfo>();
            var unitType = -1;
            var visitRateWithUnit = GetInsuranceUnit(insuranceId, out unitType);
            var rapClaim = billingRepository.GetRapsToGenerateByIds(Current.AgencyId, rapToGenerate);
            var finalClaim = billingRepository.GetFinalsToGenerateByIds(Current.AgencyId, finalToGenerate);
            raps = rapClaim;
            finlas = finalClaim;
            var agencyLocation = agencyRepository.GetMainLocation(agency.Id);
            if (agencyLocation != null)
            {
                var patients = new List<object>();
                int rapCount = rapClaim.Count;
                foreach (var rap in rapClaim)
                {
                    var diagnosis = rap.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(rap.DiagnosisCode) : null;
                    var conditionCodes = rap.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(rap.ConditionCodes) : null;
                    var claims = new List<object>();
                    claimInfo.Add(new ClaimInfo { ClaimId = rap.Id, PatientId = rap.PatientId, EpisodeId = rap.EpisodeId, ClaimType = rap.Type == 0 ? "322" : "" });
                    var rapObj = new
                    {
                        claim_id = rap.Id,
                        claim_type = rap.Type == 1 ? "" : "322",
                        claim_physician_upin = rap.PhysicianNPI,
                        claim_physician_last_name = rap.PhysicianLastName,
                        claim_physician_first_name = rap.PhysicianFirstName,
                        claim_first_visit_date = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
                        claim_episode_start_date = rap.EpisodeStartDate.ToString("MM/dd/yyyy"),
                        claim_episode_end_date = rap.EpisodeStartDate.ToString("MM/dd/yyyy"),
                        claim_hipps_code = rap.HippsCode,
                        claim_oasis_key = rap.ClaimKey,
                        claim_hmo_auth_key = rap.AuthorizationNumber,
                        claim_diagnosis_code1 = (diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : ""),
                        claim_diagnosis_code2 = (diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : ""),
                        claim_diagnosis_code3 = (diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : ""),
                        claim_diagnosis_code4 = (diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : ""),
                        claim_diagnosis_code5 = (diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : ""),
                        claim_diagnosis_code6 = (diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : ""),
                        claim_condition_code18 = (conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : ""),
                        claim_condition_code19 = (conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : ""),
                        claim_condition_code20 = (conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : ""),
                        claim_condition_code21 = (conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : ""),
                        claim_condition_code22 = (conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : ""),
                        claim_condition_code23 = (conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : ""),
                        claim_condition_code24 = (conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : ""),
                        claim_condition_code25 = (conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : ""),
                        claim_condition_code26 = (conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : ""),
                        claim_condition_code27 = (conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : ""),
                        claim_condition_code28 = (conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : ""),
                        claim_total_charge_amount = "0",
                        claim_admission_source_code = rap.AdmissionSource.IsNotNullOrEmpty() ? lookUpRepository.GetAdmissionSourceCode(int.Parse(rap.AdmissionSource)) : string.Empty,
                        claim_patient_status_code = rap.UB4PatientStatus,
                    };
                    claims.Add(rapObj);
                    if (finalClaim != null && finalClaim.Count > 0)
                    {
                        var final = finalClaim.Find(f => f.PatientId == rap.PatientId && f.EpisodeId == rap.EpisodeId);
                        var visitTotalAmount = 0.00;
                        if (final != null)
                        {
                            diagnosis = final.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(final.DiagnosisCode) : null;
                            conditionCodes = final.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(final.ConditionCodes) : null;
                            var visits = final.VerifiedVisits.IsNotNullOrEmpty() ? final.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(f => f.VisitDate.IsValidDate()).OrderBy(f => f.VisitDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();
                            var visitList = new List<object>();
                            if (visits.Count > 0)
                            {
                                visits.ForEach(v =>
                                {
                                    var discipline = v.GIdentify();
                                    var amount = visitRateWithUnit.ContainsKey(discipline) && visitRateWithUnit[discipline].Amount.IsNotNullOrEmpty() && visitRateWithUnit[discipline].Amount.IsDouble() ? visitRateWithUnit[discipline].Amount.ToDouble() : 0.00;
                                    visitTotalAmount += amount;
                                    var unit = isHMO ? (unitType == 1 ? 1 : (unitType == 2 ? (int)Math.Ceiling((double)v.MinSpent / 60) : (unitType == 3 ? (int)Math.Ceiling((double)v.MinSpent / 15) : 0))) : v.Unit;
                                    visitList.Add(new { date = v.VisitDate, type = discipline, units = unit, amount = isHMO ? amount * unit : amount });
                                });
                            }
                            claimInfo.Add(new ClaimInfo { ClaimId = final.Id, PatientId = final.PatientId, EpisodeId = final.EpisodeId, ClaimType = final.Type == 0 ? "329" : "" });
                            var finalObj = new
                            {
                                claim_id = final.Id,
                                claim_type = final.Type == 1 ? "" : "329",
                                claim_physician_upin = final.PhysicianNPI,
                                claim_physician_last_name = final.PhysicianLastName,
                                claim_physician_first_name = final.PhysicianFirstName,
                                claim_first_visit_date = final.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
                                claim_episode_start_date = final.EpisodeStartDate.ToString("MM/dd/yyyy"),
                                claim_episode_end_date = final.EpisodeEndDate.ToString("MM/dd/yyyy"),
                                claim_hipps_code = final.HippsCode,
                                claim_oasis_key = final.ClaimKey,
                                claim_hmo_auth_key = final.AuthorizationNumber,
                                claim_diagnosis_code1 = (diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : ""),
                                claim_diagnosis_code2 = (diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : ""),
                                claim_diagnosis_code3 = (diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : ""),
                                claim_diagnosis_code4 = (diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : ""),
                                claim_diagnosis_code5 = (diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : ""),
                                claim_diagnosis_code6 = (diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : ""),
                                claim_condition_code18 = (conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : ""),
                                claim_condition_code19 = (conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : ""),
                                claim_condition_code20 = (conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : ""),
                                claim_condition_code21 = (conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : ""),
                                claim_condition_code22 = (conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : ""),
                                claim_condition_code23 = (conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : ""),
                                claim_condition_code24 = (conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : ""),
                                claim_condition_code25 = (conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : ""),
                                claim_condition_code26 = (conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : ""),
                                claim_condition_code27 = (conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : ""),
                                claim_condition_code28 = (conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : ""),
                                claim_total_charge_amount = Math.Round(visitTotalAmount, 2),
                                claim_admission_source_code = final.AdmissionSource.IsNotNullOrEmpty() ? lookUpRepository.GetAdmissionSourceCode(int.Parse(final.AdmissionSource)) : string.Empty,
                                claim_patient_status_code = final.UB4PatientStatus,
                                claim_supply_value = final.SupplyTotal,
                                claim_visits = visitList
                            };
                            claims.Add(finalObj);
                            finalClaim.RemoveAll(f => f.PatientId == rap.PatientId && f.EpisodeId == rap.EpisodeId);
                        }
                    }
                    var patient = new
                    {
                        patient_gender = rap.Gender.Substring(0, 1),
                        patient_medicare_num = rap.MedicareNumber,
                        patient_record_num = rap.PatientIdNumber,
                        patient_dob = rap.DOB.ToString("MM/dd/yyyy"),
                        patient_doa = rap.StartofCareDate.ToString("MM/dd/yyyy"),
                        patient_dod = rap.IsRapDischage() && rap.DischargeDate.Date > DateTime.MinValue.Date ? rap.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
                        patient_address = rap.AddressLine1,
                        patient_address2 = rap.AddressLine2,
                        patient_city = rap.AddressCity,
                        patient_state = rap.AddressStateCode,
                        patient_zip = rap.AddressZipCode,
                        patient_cbsa = lookUpRepository.CbsaCodeByZip(rap.AddressZipCode),
                        patient_last_name = rap.LastName,
                        patient_first_name = rap.FirstName,
                        hmo_plan_id = rap.HealthPlanId,
                        patient_middle_initial = "",
                        claims_arr = claims
                    };
                    patients.Add(patient);
                }

                if (finalClaim != null && finalClaim.Count > 0)
                {
                    foreach (var final in finalClaim)
                    {
                        var claims = new List<object>();
                        var diagnosis = final.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(final.DiagnosisCode) : null;
                        var conditionCodes = final.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(final.ConditionCodes) : null;
                        var visitTotalAmount = 0.00;
                        var visits = final.VerifiedVisits.IsNotNullOrEmpty() ? final.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(f => f.VisitDate.IsValidDate()).OrderBy(f => f.VisitDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();
                        var visitList = new List<object>();
                        if (visits.Count > 0)
                        {
                            visits.ForEach(v =>
                            {
                                var discipline = v.GIdentify();
                                var amount = visitRateWithUnit.ContainsKey(discipline) && visitRateWithUnit[discipline].Amount.IsNotNullOrEmpty() && visitRateWithUnit[discipline].Amount.IsDouble() ? visitRateWithUnit[discipline].Amount.ToDouble() : 0.00;
                                visitTotalAmount += amount;
                                var unit = isHMO ? (unitType == 1 ? 1 : (unitType == 2 ? (int)Math.Ceiling((double)v.MinSpent / 60) : (unitType == 3 ? (int)Math.Ceiling((double)v.MinSpent / 15) : 0))) : v.Unit;
                                visitList.Add(new { date = v.VisitDate, type = discipline, units = unit, amount = isHMO ? amount * unit : amount });
                            });
                        }
                        claimInfo.Add(new ClaimInfo { ClaimId = final.Id, PatientId = final.PatientId, EpisodeId = final.EpisodeId, ClaimType = final.Type == 0 ? "329" : "" });
                        var finalObj = new
                        {
                            claim_id = final.Id,
                            claim_type = final.Type == 1 ? "" : "329",
                            claim_physician_upin = final.PhysicianNPI,
                            claim_physician_last_name = final.PhysicianLastName,
                            claim_physician_first_name = final.PhysicianFirstName,
                            claim_first_visit_date = final.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
                            claim_episode_start_date = final.EpisodeStartDate.ToString("MM/dd/yyyy"),
                            claim_episode_end_date = final.EpisodeEndDate.ToString("MM/dd/yyyy"),
                            claim_hipps_code = final.HippsCode,
                            claim_oasis_key = final.ClaimKey,
                            claim_hmo_auth_key = final.AuthorizationNumber,
                            claim_diagnosis_code1 = (diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : ""),
                            claim_diagnosis_code2 = (diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : ""),
                            claim_diagnosis_code3 = (diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : ""),
                            claim_diagnosis_code4 = (diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : ""),
                            claim_diagnosis_code5 = (diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : ""),
                            claim_diagnosis_code6 = (diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : ""),
                            claim_condition_code18 = (conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : ""),
                            claim_condition_code19 = (conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : ""),
                            claim_condition_code20 = (conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : ""),
                            claim_condition_code21 = (conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : ""),
                            claim_condition_code22 = (conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : ""),
                            claim_condition_code23 = (conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : ""),
                            claim_condition_code24 = (conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : ""),
                            claim_condition_code25 = (conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : ""),
                            claim_condition_code26 = (conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : ""),
                            claim_condition_code27 = (conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : ""),
                            claim_condition_code28 = (conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : ""),
                            claim_total_charge_amount = Math.Round(visitTotalAmount, 2),
                            claim_admission_source_code = final.AdmissionSource.IsNotNullOrEmpty() ? lookUpRepository.GetAdmissionSourceCode(int.Parse(final.AdmissionSource)) : string.Empty,
                            claim_patient_status_code = final.UB4PatientStatus,
                            claim_supply_value = final.SupplyTotal,
                            claim_visits = visitList
                        };
                        claims.Add(finalObj);
                        var patient = new
                        {
                            patient_gender = final.Gender.Substring(0, 1),
                            patient_medicare_num = final.MedicareNumber,
                            patient_record_num = final.PatientIdNumber,
                            patient_dob = final.DOB.ToString("MM/dd/yyyy"),
                            patient_doa = final.StartofCareDate.ToString("MM/dd/yyyy"),
                            patient_dod = final.IsFinalDischage()&& final.DischargeDate.Date > DateTime.MinValue.Date ? final.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
                            patient_address = final.AddressLine1,
                            patient_address2 = final.AddressLine2,
                            patient_city = final.AddressCity,
                            patient_state = final.AddressStateCode,
                            patient_zip = final.AddressZipCode,
                            patient_cbsa = lookUpRepository.CbsaCodeByZip(final.AddressZipCode),
                            patient_last_name = final.LastName,
                            patient_first_name = final.FirstName,
                            patient_middle_initial = "",
                            hmo_plan_id = final.HealthPlanId,
                            claims_arr = claims
                        };
                        patients.Add(patient);
                    }
                }

                var visitRates = new List<object>();
                visitRateWithUnit.ForEach(vr =>
                {
                    var key = vr.Key;
                    var value = vr.Value;
                    var visitRate = GetVisitRateInstance(key, value.CodeOne, value.CodeTwo, value.Amount, value.Unit);
                    if (visitRate != null)
                    {
                        visitRates.Add(visitRate);
                    }
                });
                if (isHMO)
                {
                    var locators = payerInfo.Ub04Locator81cca.ToObject<List<Locator>>();
                    var additionaCodes = new object();
                    if (locators != null && locators.Count > 0) { additionaCodes = payerInfo.Ub04Locator81cca.ToObject<List<Locator>>().Select(l => new { code1 = l.Code1, code2 = l.Code2, code3 = l.Code3 }); } else { additionaCodes = string.Empty; }
                    var agencyClaim = new
                    {
                        format = "ansi837",
                        submit_type = commandType.ToString(),
                        user_login_name = Current.User.Name,
                        hmo_payer_id = payerInfo.HMOPayerId,
                        hmo_payer_name = payerInfo.HMOPayerName,
                        hmo_submitter_id = payerInfo.HMOSubmitterId,
                        hmo_provider_id = payerInfo.ProviderId,
                        hmo_other_provider_id = payerInfo.OtherProviderId,
                        hmo_provider_subscriber_id = payerInfo.ProviderSubscriberId,
                        hmo_additional_codes = additionaCodes,
                        payer_id = payerInfo.Code,
                        payer_name = payerInfo.Name,
                        submitter_id = payerInfo.SubmitterId,
                        insurance_is_axxess_biller = payerInfo.IsAxxessTheBiller,
                        clearing_house_id = payerInfo.ClearingHouseSubmitterId,
                        provider_claim_type = payerInfo.BillType,
                        interchange_receiver_id = payerInfo.InterchangeReceiverId,
                        clearing_house = payerInfo.ClearingHouse,
                        claim_billtype = ClaimType.HMO.ToString(),
                        submitter_name = payerInfo.SubmitterName,
                        submitter_phone = payerInfo.Phone,
                        submitter_fax = payerInfo.Fax,
                        user_agency_name = agency.Name,
                        user_tax_id = agency.TaxId,
                        user_national_provider_id = agency.NationalProviderNumber,
                        user_address_1 = agencyLocation.AddressLine1,
                        user_address_2 = agencyLocation.AddressLine2,
                        user_city = agencyLocation.AddressCity,
                        user_state = agencyLocation.AddressStateCode,
                        user_zip = agencyLocation.AddressZipCode,
                        user_phone = agencyLocation.PhoneWork,
                        user_fax = agencyLocation.FaxNumber,
                        user_CBSA_code = lookUpRepository.CbsaCodeByZip(agencyLocation.AddressZipCode),
                        ansi_837_id = claimId,
                        visit_rates = visitRates,
                        patients_arr = patients
                    };
                    var jss = new JavaScriptSerializer();
                    requestArr = jss.Serialize(agencyClaim);
                }
                else
                {
                    var agencyClaim = new
                    {
                        format = "ansi837",
                        submit_type = commandType.ToString(),
                        user_login_name = Current.User.Name,
                        payer_id = payerInfo.Code,
                        payer_name = payerInfo.Name,
                        submitter_id = payerInfo.SubmitterId,
                        claim_billtype = ClaimType.CMS.ToString(),
                        submitter_name = payerInfo.SubmitterName,
                        submitter_phone = payerInfo.Phone,
                        submitter_fax = payerInfo.Fax,
                        user_agency_name = agency.Name,
                        user_tax_id = agency.TaxId,
                        user_national_provider_id = agency.NationalProviderNumber,
                        user_address_1 = agencyLocation.AddressLine1,
                        user_address_2 = agencyLocation.AddressLine2,
                        user_city = agencyLocation.AddressCity,
                        user_state = agencyLocation.AddressStateCode,
                        user_zip = agencyLocation.AddressZipCode,
                        user_phone = agencyLocation.PhoneWork,
                        user_fax = agencyLocation.FaxNumber,
                        user_CBSA_code = lookUpRepository.CbsaCodeByZip(agencyLocation.AddressZipCode),
                        ansi_837_id = claimId,
                        visit_rates = visitRates,
                        patients_arr = patients
                    };
                    var jss = new JavaScriptSerializer();
                    requestArr = jss.Serialize(agencyClaim);
                }
            }
            return requestArr;
        }

        public bool GenerateDirect(List<Guid> rapToGenerate, List<Guid> finalToGenerate, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId)
        {
            var claimData = new ClaimData { AgencyId = Current.AgencyId };
            var claimInfo = new List<ClaimInfo>();
            bool result = false;
            claimDataOut = null;
            billExchange = new BillExchange { isSuccessful = false, Message = "There is a problem creating the claim. Try Again." };
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                int payorId;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var payerInfo = billingRepository.SubmitterInfo(payorId);
                    if (payerInfo != null)
                    {
                        if (commandType == ClaimCommandType.download)
                        {
                            payerInfo.SubmitterId = agency.SubmitterId;
                            payerInfo.SubmitterName = agency.SubmitterName;
                            payerInfo.Phone = agency.SubmitterPhone;
                            payerInfo.Fax = agency.SubmitterFax;
                        }
                        var isHMO = false;
                        if (insuranceId >= 1000)
                        {
                            var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
                            if (insurance != null && insurance.PayorType == 2)
                            {
                                isHMO = true;
                                var checkHmoPayorId = false;
                                var message = string.Empty;
                                if (insurance.PayorId.IsNotNullOrEmpty() && !(insurance.PayorId == "0"))
                                {
                                    payerInfo.HMOPayerId = insurance.PayorId;
                                    checkHmoPayorId = true;
                                }
                                else
                                {
                                    checkHmoPayorId = false;
                                    message = "Payor Id information is not correct.";
                                }
                                payerInfo.HMOPayerName = insurance.Name;
                                var checkHmoSubmitterId = false;
                                if (insurance.SubmitterId.IsNotNullOrEmpty())
                                {
                                    payerInfo.HMOSubmitterId = insurance.SubmitterId;
                                    checkHmoSubmitterId = true;
                                }
                                else
                                {
                                    checkHmoSubmitterId = false;
                                    if (message.IsNullOrEmpty()) { message = "The submitter Id is not correct."; } else { message = message + "," + "The submitter Id is not correct."; }
                                }
                                if (!(checkHmoPayorId && checkHmoSubmitterId))
                                {
                                    billExchange.Message = message;
                                    return false;
                                }
                                payerInfo.Ub04Locator81cca = insurance.Ub04Locator81cca;
                                payerInfo.IsAxxessTheBiller = insurance.IsAxxessTheBiller;
                                payerInfo.InterchangeReceiverId = insurance.InterchangeReceiverId;
                                payerInfo.ClearingHouse = insurance.ClearingHouse;
                                payerInfo.ClearingHouseSubmitterId = insurance.ClearingHouseSubmitterId;
                                payerInfo.BillType = insurance.BillType;
                            }
                            else
                            {
                                billExchange.Message = "Medicare HMO information is not right.";
                                return false;
                            }
                        }
                        else if (insuranceId < 1000 && insuranceId > 0)
                        {
                            isHMO = false;
                        }
                        else
                        {
                            billExchange.Message = "Payor information is not right.";
                            return false;
                        }
                        var claimId = GetNextClaimId(claimData);
                        claimData.Id = claimId;
                        List<Rap> rapClaims = null;
                        List<Final> finlaClaims = null;
                        var branch = agencyRepository.FindLocation(Current.AgencyId, branchId);
                        if (branch != null)
                        {
                            var requestArr = GenerateJson(rapToGenerate, finalToGenerate, commandType, claimId, out claimInfo, agency, payerInfo, out rapClaims, out finlaClaims, isHMO, insuranceId, branch);
                            if (requestArr.IsNotNullOrEmpty())
                            {
                                if (billingRepository.AddRapSnapShots(rapClaims, claimData.Id) && billingRepository.AddFinaSnapShots(finlaClaims, claimData.Id))
                                {
                                    requestArr = requestArr.Replace("&", "U+0026");
                                    billExchange = GenerateANSI(requestArr);

                                    if (billExchange != null && billExchange.isSuccessful && billExchange.Status == "OK")
                                    {
                                        if (billExchange.Result.IsNotNullOrEmpty())
                                        {
                                            claimData.Data = billExchange.Result;
                                            claimData.BillIdentifers = claimInfo.ToXml<List<ClaimInfo>>();
                                            claimData.ClaimType = isHMO ? ClaimType.HMO.ToString() : ClaimType.CMS.ToString();
                                            billingRepository.UpdateClaimData(claimData);
                                            if (commandType == ClaimCommandType.direct)
                                            {
                                                if (rapClaims != null && rapClaims.Count > 0)
                                                {
                                                    billingRepository.MarkRapsAsSubmitted(Current.AgencyId, rapClaims);
                                                    rapClaims.ForEach(rap =>
                                                    {
                                                        Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPSubmittedElectronically, string.Empty);
                                                    });
                                                }
                                                if (finlaClaims != null && finlaClaims.Count > 0)
                                                {
                                                    billingRepository.MarkFinalsAsSubmitted(Current.AgencyId, finlaClaims);
                                                    finlaClaims.ForEach(final =>
                                                    {
                                                        Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalSubmittedElectronically, string.Empty);
                                                    });
                                                }
                                            }
                                            claimDataOut = claimData;
                                            result = true;
                                        }
                                        else
                                        {
                                            billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                                            billingRepository.DeleteFinaSnapShots(claimData.Id);
                                            billingRepository.DeleteRapSnapShots(claimData.Id);
                                            claimDataOut = null;
                                            result = false;
                                        }
                                    }
                                    else
                                    {
                                        billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                                        billingRepository.DeleteFinaSnapShots(claimData.Id);
                                        billingRepository.DeleteRapSnapShots(claimData.Id);
                                        result = false;
                                    }
                                }
                                else
                                {
                                    billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                                    billingRepository.DeleteFinaSnapShots(claimData.Id);
                                    billingRepository.DeleteRapSnapShots(claimData.Id);
                                }
                            }
                            else
                            {
                                billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                                result = false;
                            }
                        }
                        else
                        {
                            billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                            billExchange.Message = "Main/Branch information is not available.";
                            result = false;
                        }
                    }
                    else
                    {
                        billExchange.Message = "Payer information is not right.";
                    }
                }
                else
                {
                    billExchange.Message = "Payer information is not right.";
                }
            }
            else
            {
                billExchange.Message = "Claim Information is not correct. Try again.";
            }
            return result;
        }

        public BillExchange GenerateANSI(string jsonData)
        {
            var billExchange = new BillExchange();
            try
            {
                var encoding = new ASCIIEncoding();
                string postData = ("request=" + jsonData);
                byte[] data = encoding.GetBytes(postData);
                var request = (HttpWebRequest)WebRequest.Create(AppSettings.ANSIGeneratorUrl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                var newStream = request.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();
                var response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var receiveStream = response.GetResponseStream();
                    var encode = System.Text.Encoding.GetEncoding("utf-8");
                    var readStream = new StreamReader(receiveStream, encode);
                    var strResult = readStream.ReadToEnd();
                    var jss = new JavaScriptSerializer();
                    billExchange = jss.Deserialize<BillExchange>(strResult);
                    if (billExchange.Status == "OK")
                    {
                        billExchange.isSuccessful = true;
                    }
                    else
                    {
                        billExchange.isSuccessful = false;
                    }
                }
                else
                {
                    billExchange.Message = "The is a problem processing these claim. Try Again.";
                    billExchange.isSuccessful = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                billExchange.Message = "There is system problem. Try Again.";
                billExchange.isSuccessful = false;
                return billExchange;
            }
            return billExchange;
        }

        public bool VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> visit)
        {
            bool result = false;
            if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (scheduleEvents != null)
                    {
                        var visitList = new List<ScheduleEvent>();
                        var claim = billingRepository.GetFinal(Current.AgencyId, Id);
                        var finalVisit = claim != null && claim.VerifiedVisits.IsNotNullOrEmpty() ? claim.VerifiedVisits.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();
                        visit.ForEach(v =>
                        {
                            var ScheduleVisit = scheduleEvents.FirstOrDefault(s => s.EventId == v);
                            if (ScheduleVisit != null)
                            {
                                ScheduleVisit.IsBillable = true;
                                visitList.Add(ScheduleVisit);
                            }
                        });
                        if (finalVisit != null)
                        {
                            finalVisit.ForEach(f =>
                            {
                                if (scheduleEvents.Exists(e => e.EventId == f.EventId) && !visit.Contains(f.EventId))
                                {
                                    scheduleEvents.FirstOrDefault(e => e.EventId == f.EventId).IsBillable = false;
                                }
                            });
                        }
                        claim.IsVisitVerified = true;
                        claim.VerifiedVisits = visitList.ToXml();
                        claim.Modified = DateTime.Now;
                        episode.Schedule = scheduleEvents.ToXml();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, episode))
                        {
                            result = billingRepository.UpdateFinalStatus(claim);
                        }
                    }
                }
            }
            return result;
        }

        public bool VisitSupply(Guid Id, Guid episodeId, Guid patientId, FormCollection formCollection, List<Guid> supplyId)
        {
            bool result = false;
            if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var keys = formCollection.AllKeys;
                var claim = billingRepository.GetFinal(Current.AgencyId, Id);
                if (claim != null)
                {
                    var visits = new List<ScheduleEvent>();
                    var episodeSupplyList = new List<Supply>();
                    if (claim.VerifiedVisits.IsNotNullOrEmpty())
                    {
                        visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>();
                        if (visits != null && visits.Count > 0)
                        {
                            visits.ForEach(v =>
                            {
                                episodeSupplyList.AddRange(this.GetSupply(v));
                            });
                        }
                    }
                    var tempSupplyList = new List<Supply>();
                    var existingSupplyList = claim.Supply.IsNotNullOrEmpty() ? claim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                    if (supplyId != null && supplyId.Count > 0)
                    {
                        supplyId.ForEach(v =>
                        {
                            if (existingSupplyList.Exists(s => s.UniqueIdentifier == v))
                            {
                                var currentSupply = existingSupplyList.FirstOrDefault(s => s.UniqueIdentifier == v);
                                if (currentSupply != null)
                                {
                                    currentSupply.IsBillable = true;
                                    currentSupply.UnitCost = keys.Contains("UnitCost" + v) && formCollection["UnitCost" + v].IsNotNullOrEmpty() ? formCollection["UnitCost" + v].ToDouble() : 0;
                                    currentSupply.Total = keys.Contains("Total" + v) && formCollection["Total" + v].IsNotNullOrEmpty() ? formCollection["Total" + v].ToDouble() : 0;
                                    tempSupplyList.Add(currentSupply);
                                }
                            }
                            else if (episodeSupplyList.Exists(s => s.UniqueIdentifier == v))
                            {
                                var currentSupply = episodeSupplyList.FirstOrDefault(s => s.UniqueIdentifier == v);
                                if (currentSupply != null)
                                {
                                    currentSupply.IsBillable = true;
                                    currentSupply.UnitCost = keys.Contains("UnitCost" + v) && formCollection["UnitCost" + v].IsNotNullOrEmpty() ? formCollection["UnitCost" + v].ToDouble() : 0;
                                    currentSupply.Total = keys.Contains("Total" + v) && formCollection["Total" + v].IsNotNullOrEmpty() ? formCollection["Total" + v].ToDouble() : 0;
                                    tempSupplyList.Add(currentSupply);
                                }
                            }
                        });
                    }
                    claim.SupplyTotal = formCollection["SupplyTotal"].ToDouble();
                    claim.Supply = tempSupplyList.ToXml();
                    claim.Modified = DateTime.Now;
                    claim.IsSupplyVerified = true;
                    result = billingRepository.UpdateFinalStatus(claim);
                }
            }
            return result;
        }

        public Bill AllUnProcessedBill(Guid branchId, int insuranceId)
        {
            var bill = new Bill();
            var raps = AllUnProcessedRaps(branchId, insuranceId);
            bill.RapClaims = raps != null ? raps : new List<ClaimBill>();
            var finals = AllUnProcessedFinals(branchId, insuranceId);
            bill.FinalClaims = finals != null ? finals : new List<ClaimBill>();
            bill.BranchId = branchId;
            bill.Insurance = insuranceId;
            bill.IsMedicareHMO = agencyRepository.IsMedicareHMO(Current.AgencyId, insuranceId);
            return bill;
        }

        public IList<ClaimBill> AllUnProcessedRaps(Guid branchId, int insuranceId)
        {
            var raps = billingRepository.GetOutstandingRapClaims(Current.AgencyId, branchId, insuranceId);
            if (raps != null)
            {
                raps.ForEach(rap =>
                {
                    if (rap.Status == (int)ScheduleStatus.ClaimCreated)
                    {
                        rap.IsOasisComplete = rap.IsOasisComplete || IsOasisComplete(rap);
                        rap.IsFirstBillableVisit = rap.IsFirstBillableVisit || IsFirstBillableVisit(rap);
                    }
                    else
                    {
                        rap.IsFirstBillableVisit = true;
                        rap.IsOasisComplete = true;
                        rap.IsVerified = true;
                    }
                });
            }
            return raps;
        }

        public IList<ClaimBill> AllUnProcessedFinals(Guid branchId, int insuranceId)
        {
            var finals = billingRepository.GetOutstandingFinalClaims(Current.AgencyId, branchId, insuranceId);
            if (finals != null)
            {
                finals.ForEach(final =>
                {
                    if (final.Status == (int)ScheduleStatus.ClaimCreated)
                    {
                        final.AreOrdersComplete = final.AreOrdersComplete || IsOrdersComplete(final);
                        final.AreVisitsComplete = final.IsVisitVerified || IsVisitComplete(final);
                        var rap = billingRepository.GetRap(Current.AgencyId, final.Id);
                        if (rap != null)
                        {
                            final.IsRapGenerated = final.IsRapGenerated || rap.IsGenerated || rap.Status == (int)ScheduleStatus.ClaimSubmitted || rap.Status == (int)ScheduleStatus.ClaimPaidClaim || rap.Status == (int)ScheduleStatus.ClaimPaymentPending || rap.Status == (int)ScheduleStatus.ClaimAccepted;
                        }
                    }
                    else
                    {
                        final.AreOrdersComplete = true;
                        final.IsRapGenerated = true;
                    }
                });
            }
            return finals;
        }

        public BillLean ClaimToGenerate(List<Guid> rapToGenerate, List<Guid> finalToGenerate, Guid branchId, int primaryInsurance)
        {
            var isElectronicSubmssion = false;
            if (primaryInsurance < 1000 && primaryInsurance > 0)
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    isElectronicSubmssion = agency.IsAxxessTheBiller;
                }
            }
            else if (primaryInsurance >= 1000)
            {
                var insurance = agencyRepository.GetInsurance(primaryInsurance, Current.AgencyId);
                if (insurance != null)
                {
                    isElectronicSubmssion = insurance.IsAxxessTheBiller;
                }
            }
            return new BillLean { BranchId = branchId, Insurance = primaryInsurance, Finals = billingRepository.GetFinalsByIds(Current.AgencyId, finalToGenerate), Raps = billingRepository.GetRapsByIds(Current.AgencyId, rapToGenerate), IsElectronicSubmssion = isElectronicSubmssion };
        }

        public long GetNextClaimId(ClaimData claimData)
        {
            return billingRepository.AddClaimData(claimData);
        }

        public IList<ClaimHistoryLean> Activity(Guid patientId, int insuranceId)
        {
            var raps = billingRepository.GetRapsHistory(Current.AgencyId, patientId, insuranceId);
            var finals = billingRepository.GetFinalsHistory(Current.AgencyId, patientId, insuranceId);
            var claims = new List<ClaimHistoryLean>();
            if (raps != null && raps.Count > 0)
            {
                claims.AddRange(raps);
            }
            if (finals != null && finals.Count > 0)
            {
                claims.AddRange(finals);
            }
            return claims;
        }

        public IList<PendingClaimLean> PendingClaimRaps(Guid branchId, string primaryInsurance)
        {
            var claims = new List<PendingClaimLean>();
            if (primaryInsurance.IsInteger())
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var claimsToAddRap = billingRepository.PendingClaimRaps(Current.AgencyId, branchId, primaryInsurance.ToInteger());
                if (claimsToAddRap != null && claimsToAddRap.Count > 0)
                {
                    claimsToAddRap.ForEach(c => { c.ClaimAmount = c.AssessmentType.IsNotNullOrEmpty() && (c.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || c.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString()|| c.AssessmentType==DisciplineTasks.OASISCStartofCareOT.ToString()) ? 0.6 * Math.Round(lookUpRepository.ProspectivePayAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2) : 0.5 * Math.Round(lookUpRepository.ProspectivePayAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                    claims.AddRange(claimsToAddRap);
                }
            }
            return claims;
        }

        public IList<PendingClaimLean> PendingClaimFinals(Guid branchId, string primaryInsurance)
        {
            var claims = new List<PendingClaimLean>();
            if (primaryInsurance.IsInteger())
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var claimsToAddFinal = billingRepository.PendingClaimFinals(Current.AgencyId, branchId, primaryInsurance.ToInteger());
                if (claimsToAddFinal != null && claimsToAddFinal.Count > 0)
                {
                    claimsToAddFinal.ForEach(c => { c.ClaimAmount = Math.Round(lookUpRepository.ProspectivePayAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                    claims.AddRange(claimsToAddFinal);
                }
            }
            return claims;
        }

        public IList<ClaimLean> AccountsReceivables(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var claims = new List<ClaimLean>();
            var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
            var claimsToAddRap = billingRepository.GetAccountsReceivableRaps(Current.AgencyId, branchId, insurance, startDate, endDate);
            if (claimsToAddRap != null && claimsToAddRap.Count > 0)
            {
                claimsToAddRap.ForEach(c => { c.ClaimAmount = c.AssessmentType.IsNotNullOrEmpty() && (c.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || c.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString()||c.AssessmentType==DisciplineTasks.OASISCStartofCareOT.ToString()) ? 0.6 * Math.Round(lookUpRepository.ProspectivePayAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2) : 0.5 * Math.Round(lookUpRepository.ProspectivePayAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                claims.AddRange(claimsToAddRap);
            }
            var claimsToAddFinal = billingRepository.GetAccountsReceivableFinals(Current.AgencyId, branchId, insurance, startDate, endDate);
            if (claimsToAddFinal != null && claimsToAddFinal.Count > 0)
            {
                claimsToAddFinal.ForEach(c => { c.ClaimAmount = Math.Round(lookUpRepository.ProspectivePayAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                claims.AddRange(claimsToAddFinal);
            }
            return claims.OrderBy(c => c.DisplayName).ToList();
        }

        public IList<ClaimLean> AccountsReceivableRaps(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var claims = new List<ClaimLean>();
            var claimsToAdd = billingRepository.GetAccountsReceivableRaps(Current.AgencyId, branchId, insurance, startDate, endDate);
            if (claimsToAdd != null && claimsToAdd.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                claimsToAdd.ForEach(c => { c.ClaimAmount = c.AssessmentType.IsNotNullOrEmpty() && (c.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || c.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || c.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString()) ? 0.6 * Math.Round(lookUpRepository.ProspectivePayAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2) : 0.5 * Math.Round(lookUpRepository.ProspectivePayAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                claims.AddRange(claimsToAdd);
            }
            return claims.OrderBy(c => c.DisplayName).ToList();
        }

        public IList<ClaimLean> AccountsReceivableFinals(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var claims = new List<ClaimLean>();
            var claimsToAdd = billingRepository.GetAccountsReceivableFinals(Current.AgencyId, branchId, insurance, startDate, endDate);
            if (claimsToAdd != null && claimsToAdd.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                claimsToAdd.ForEach(c => { c.ClaimAmount = Math.Round(lookUpRepository.ProspectivePayAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                claims.AddRange(claimsToAdd);
            }
            return claims.OrderBy(c => c.DisplayName).ToList();
        }

        public IList<TypeOfBill> GetAllUnProcessedBill()
        {
            var claims = new List<TypeOfBill>();
            var raps = billingRepository.GetOutstandingRaps(Current.AgencyId);
            if (raps != null && raps.Count>0)
            {
                claims.AddRange(raps);
            }
            var finals = billingRepository.GetOutstandingFinals(Current.AgencyId);
            if (finals != null && finals.Count>0)
            {
                claims.AddRange(finals);
            }
            return claims.OrderBy(c => c.SortData).Take(5).ToList();
        }

        public List<Supply> GetSupply(ScheduleEvent scheduleEvent)
        {
            var supplies = new List<Supply>();
            switch ((DisciplineTasks)scheduleEvent.DisciplineTask)
            {
                case DisciplineTasks.NonOASISRecertification:
                case DisciplineTasks.NonOASISStartofCare:
                case DisciplineTasks.OASISCStartofCare:
                case DisciplineTasks.OASISCStartofCarePT:
                case DisciplineTasks.OASISCStartofCareOT:
                case DisciplineTasks.OASISCResumptionofCare:
                case DisciplineTasks.OASISCResumptionofCarePT:
                case DisciplineTasks.OASISCResumptionofCareOT:
                case DisciplineTasks.OASISCFollowUp:
                case DisciplineTasks.OASISCFollowupPT:
                case DisciplineTasks.OASISCFollowupOT:
                case DisciplineTasks.OASISCRecertification:
                case DisciplineTasks.OASISCRecertificationPT:
                case DisciplineTasks.OASISCRecertificationOT:
                    var assessment = assessmentRepository.GetWithNoList(Current.AgencyId, scheduleEvent.EventId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString());
                    if (assessment != null && assessment.Supply.IsNotNullOrEmpty())
                    {
                        supplies = assessment.Supply.ToObject<List<Supply>>();
                    }
                    break;
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                case DisciplineTasks.SNVManagementAndEvaluation:
                case DisciplineTasks.SNVObservationAndAssessment:
                case DisciplineTasks.SNVTeachingTraining:
                    var note = patientRepository.GetVisitNote(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                    if (note != null && note.Supply.IsNotNullOrEmpty())
                    {
                        supplies = note.Supply.ToObject<List<Supply>>();
                    }
                    break;
            }
            return supplies;
        }

        public Rap GetRap(Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            var rap = billingRepository.GetRap(Current.AgencyId, patientId, episodeId);
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (rap != null && patient != null && episode != null)
            {
                rap.IsMedicareHMO = agencyRepository.IsMedicareHMO(Current.AgencyId, rap.PrimaryInsuranceId);
                if ((rap.Status == (int)ScheduleStatus.ClaimCreated))
                {
                    if (!rap.IsVerified)
                    {
                        if (patient != null)
                        {
                            rap.FirstName = patient.FirstName;
                            rap.LastName = patient.LastName;
                            rap.MedicareNumber = patient.MedicareNumber;
                            rap.PatientIdNumber = patient.PatientIdNumber;
                            rap.Gender = patient.Gender;
                            rap.DOB = patient.DOB;
                            rap.AddressLine1 = patient.AddressLine1;
                            rap.AddressLine2 = patient.AddressLine2;
                            rap.AddressCity = patient.AddressCity;
                            rap.AddressStateCode = patient.AddressStateCode;
                            rap.AddressZipCode = patient.AddressZipCode;
                            rap.AdmissionSource = patient.AdmissionSource;
                            rap.PatientStatus = patient.Status;
                            rap.UB4PatientStatus= patient.Status==1? "30": (patient.Status==2?"01": string.Empty);
                            if (episode != null)
                            {
                                var managedDate = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                                if (managedDate != null)
                                {
                                    if (rap.IsRapDischage())
                                    {
                                        if (managedDate.DischargedDate.Date > DateTime.MinValue.Date)
                                        {
                                            rap.DischargeDate = managedDate.DischargedDate;
                                        }
                                    }
                                    rap.StartofCareDate = managedDate.StartOfCareDate;
                                }
                                rap.EpisodeStartDate = episode.StartDate;
                            }
                            //int primaryInsurance;
                            //if (int.TryParse(patient.PrimaryInsurance, out primaryInsurance))
                            //{
                            //    rap.PrimaryInsuranceId = primaryInsurance;
                            //}
                            if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count >= 0)
                            {
                                var physician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                                if (physician != null)
                                {
                                    rap.PhysicianLastName = physician.LastName;
                                    rap.PhysicianFirstName = physician.FirstName;
                                    rap.PhysicianNPI = physician.NPI;
                                }
                                else
                                {
                                    physician = patient.PhysicianContacts.FirstOrDefault();
                                    if (physician != null)
                                    {
                                        rap.PhysicianLastName = physician.LastName;
                                        rap.PhysicianFirstName = physician.FirstName;
                                        rap.PhysicianNPI = physician.NPI;
                                    }
                                }
                            }
                        }
                        if (patientRepository.IsFirstBillableVisit(Current.AgencyId, episodeId, patientId))
                        {
                            var evnt = patientRepository.FirstBillableEvent(Current.AgencyId, episodeId, patientId);
                            if (evnt != null && evnt.EventDate.IsValidDate())
                            {
                                rap.FirstBillableVisitDateFormat = evnt.VisitDate.ToDateTime().ToString("MM/dd/yyyy");
                            }
                        }
                        else
                        {
                            rap.FirstBillableVisitDateFormat = string.Empty;
                        }
                        var assessmentEvent = assessmentService.GetEpisodeAssessmentEvent(episodeId, patientId);
                        if (assessmentEvent != null)
                        {
                            var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                            var assessment = assessmentRepository.Get(assessmentEvent.EventId, patientId, assessmentEvent.EpisodeId, assessmentType, Current.AgencyId);
                            if (assessment != null)
                            {
                                IDictionary<string, Question> assessmentQuestions = assessment.ToDictionary();
                                string diagnosis = "<DiagonasisCodes>";
                                diagnosis += "<code1>" + (assessmentQuestions.ContainsKey("M1020ICD9M") ? assessmentQuestions["M1020ICD9M"].Answer : string.Empty) + "</code1>";
                                diagnosis += "<code2>" + (assessmentQuestions.ContainsKey("M1022ICD9M1") ? assessmentQuestions["M1022ICD9M1"].Answer : string.Empty) + "</code2>";
                                diagnosis += "<code3>" + (assessmentQuestions.ContainsKey("M1022ICD9M2") ? assessmentQuestions["M1022ICD9M2"].Answer : string.Empty) + "</code3>";
                                diagnosis += "<code4>" + (assessmentQuestions.ContainsKey("M1022ICD9M3") ? assessmentQuestions["M1022ICD9M3"].Answer : string.Empty) + "</code4>";
                                diagnosis += "<code5>" + (assessmentQuestions.ContainsKey("M1022ICD9M4") ? assessmentQuestions["M1022ICD9M4"].Answer : string.Empty) + "</code5>";
                                diagnosis += "<code6>" + (assessmentQuestions.ContainsKey("M1022ICD9M5") ? assessmentQuestions["M1022ICD9M5"].Answer : string.Empty) + "</code6>";
                                diagnosis += "</DiagonasisCodes>";
                                rap.DiagnosisCode = diagnosis;
                                rap.HippsCode = assessment.HippsCode;
                                rap.ClaimKey = assessment.ClaimKey;
                                rap.AssessmentType = assessmentType;
                                if (assessmentType.IsNotNullOrEmpty())
                                {
                                    var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                    if (assessmentType == DisciplineTasks.OASISCStartofCare.ToString() || assessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || assessmentType == DisciplineTasks.OASISCStartofCareOT.ToString())
                                    {
                                        rap.ProspectivePay = Math.Round(0.6 * lookUpRepository.ProspectivePayAmount(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                    }
                                    else
                                    {
                                        rap.ProspectivePay = Math.Round(0.5 * lookUpRepository.ProspectivePayAmount(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        rap.FirstBillableVisitDateFormat = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                    }
                }
                else
                {
                    rap.FirstBillableVisitDateFormat = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                }
               // rap.EpisodeStartDate = episode.StartDate;
               // rap.EpisodeEndDate = episode.EndDate;
                rap.BranchId = patient.AgencyLocationId;
            }
            return rap;
        }

        public Final GetFinalInfo(Guid patientId, Guid episodeId)
        {
            var final = billingRepository.GetFinal(Current.AgencyId, patientId, episodeId);
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            if (final != null && patient != null && episode != null)
            {
                var insuranceId = 0;
                if (int.TryParse(patient.PrimaryInsurance, out insuranceId))
                {
                    final.IsMedicareHMO = agencyRepository.IsMedicareHMO(Current.AgencyId, insuranceId);
                }
                if ((final.Status == (int)ScheduleStatus.ClaimCreated))
                {
                    if (!final.IsFinalInfoVerified)
                    {
                        var rap = billingRepository.GetRap(Current.AgencyId, patientId, episodeId);
                        if (rap != null && (rap.IsVerified))
                        {
                            final.FirstName = rap.FirstName;
                            final.LastName = rap.LastName;
                            final.MedicareNumber = rap.MedicareNumber;
                            final.PatientIdNumber = rap.PatientIdNumber;
                            final.DiagnosisCode = rap.DiagnosisCode;
                            final.ConditionCodes = rap.ConditionCodes;
                            final.Gender = rap.Gender;
                            final.DOB = rap.DOB;
                            final.EpisodeStartDate = rap.EpisodeStartDate;
                            final.StartofCareDate = rap.StartofCareDate;
                            final.AddressLine1 = rap.AddressLine1;
                            final.AddressLine2 = rap.AddressLine2;
                            final.AddressCity = rap.AddressCity;
                            final.AddressStateCode = rap.AddressStateCode;
                            final.AddressZipCode = rap.AddressZipCode;
                            final.HippsCode = rap.HippsCode;
                            final.ClaimKey = rap.ClaimKey;
                            final.FirstBillableVisitDateFormat = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                            final.PhysicianLastName = rap.PhysicianLastName;
                            final.PhysicianFirstName = rap.PhysicianFirstName;
                            final.PhysicianNPI = rap.PhysicianNPI;
                            final.AdmissionSource = rap.AdmissionSource;
                            final.AssessmentType = rap.AssessmentType;
                            final.UB4PatientStatus = rap.UB4PatientStatus;
                            int primaryInsurance;
                            if (int.TryParse(patient.PrimaryInsurance, out primaryInsurance))
                            {
                                final.PrimaryInsuranceId = primaryInsurance;
                            }
                            if (episode != null)
                            {
                                var managedDate = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                                if (managedDate != null)
                                {
                                    if (final.IsFinalDischage())
                                    {
                                        if (managedDate.DischargedDate.Date > DateTime.MinValue.Date)
                                        {
                                            final.DischargeDate = managedDate.DischargedDate;
                                        }
                                    }
                                }
                                final.EpisodeStartDate = episode.StartDate;
                                final.EpisodeEndDate = episode.EndDate;
                            }
                            if (final.AssessmentType.IsNotNullOrEmpty())
                            {
                                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                if (final.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || final.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || final.AssessmentType==DisciplineTasks.OASISCStartofCareOT.ToString())
                                {
                                    final.ProspectivePay = Math.Round(lookUpRepository.ProspectivePayAmount(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                }
                                else
                                {
                                    final.ProspectivePay = Math.Round(lookUpRepository.ProspectivePayAmount(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                }
                            }
                        }
                        else if (rap == null)
                        {
                            if (patient != null)
                            {
                                final.FirstName = patient.FirstName;
                                final.LastName = patient.LastName;
                                final.MedicareNumber = patient.MedicareNumber;
                                final.PatientIdNumber = patient.PatientIdNumber;
                                final.Gender = patient.Gender;
                                final.DOB = patient.DOB;
                                final.AddressLine2 = patient.AddressLine2;
                                final.AddressCity = patient.AddressCity;
                                final.AddressStateCode = patient.AddressStateCode;
                                final.AddressZipCode = patient.AddressZipCode;
                                final.UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty);
                                int primaryInsurance;
                                if (int.TryParse(patient.PrimaryInsurance, out primaryInsurance))
                                {
                                    final.PrimaryInsuranceId = primaryInsurance;
                                }
                                if (episode != null)
                                {
                                    var managedDate = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                                    if (managedDate != null)
                                    {
                                        if (final.IsFinalDischage())
                                        {
                                            if (managedDate.DischargedDate > DateTime.MinValue)
                                            {
                                                final.DischargeDate = managedDate.DischargedDate;
                                            }
                                        }
                                        final.StartofCareDate = managedDate.StartOfCareDate;
                                    }
                                    final.EpisodeStartDate = episode.StartDate;
                                    final.EpisodeEndDate = episode.EndDate;
                                    
                                }
                                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count >= 0)
                                {
                                    var physician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                                    if (physician != null)
                                    {
                                        final.PhysicianLastName = physician.LastName;
                                        final.PhysicianFirstName = physician.FirstName;
                                        final.PhysicianNPI = physician.NPI;
                                    }
                                    else
                                    {
                                        physician = patient.PhysicianContacts.FirstOrDefault();
                                        if (physician != null)
                                        {
                                            final.PhysicianLastName = physician.LastName;
                                            final.PhysicianFirstName = physician.FirstName;
                                            final.PhysicianNPI = physician.NPI;
                                        }
                                    }
                                }
                            }
                            if (patientRepository.IsFirstBillableVisit(Current.AgencyId, episodeId, patientId))
                            {
                                var evnt = patientRepository.FirstBillableEvent(Current.AgencyId, episodeId, patientId);
                                if (evnt != null && evnt.EventDate.IsValidDate())
                                {
                                    final.FirstBillableVisitDateFormat = evnt.VisitDate.ToDateTime().ToString("MM/dd/yyyy");
                                }
                            }
                            else
                            {
                                final.FirstBillableVisitDateFormat = string.Empty;
                            }
                            var assessmentEvent = assessmentService.GetEpisodeAssessmentEvent(episodeId, patientId);
                            if (assessmentEvent != null)
                            {
                                var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                                var assessment = assessmentRepository.Get(assessmentEvent.EventId, patientId, assessmentEvent.EpisodeId, assessmentType, Current.AgencyId);
                                if (assessment != null)
                                {
                                    IDictionary<string, Question> assessmentQuestions = assessment.ToDictionary();
                                    string diagnosis = "<DiagonasisCodes>";
                                    diagnosis += "<code1>" + (assessmentQuestions.ContainsKey("M1020ICD9M") ? assessmentQuestions["M1020ICD9M"].Answer : string.Empty) + "</code1>";
                                    diagnosis += "<code2>" + (assessmentQuestions.ContainsKey("M1022ICD9M1") ? assessmentQuestions["M1022ICD9M1"].Answer : string.Empty) + "</code2>";
                                    diagnosis += "<code3>" + (assessmentQuestions.ContainsKey("M1022ICD9M2") ? assessmentQuestions["M1022ICD9M2"].Answer : string.Empty) + "</code3>";
                                    diagnosis += "<code4>" + (assessmentQuestions.ContainsKey("M1022ICD9M3") ? assessmentQuestions["M1022ICD9M3"].Answer : string.Empty) + "</code4>";
                                    diagnosis += "<code5>" + (assessmentQuestions.ContainsKey("M1022ICD9M4") ? assessmentQuestions["M1022ICD9M4"].Answer : string.Empty) + "</code5>";
                                    diagnosis += "<code6>" + (assessmentQuestions.ContainsKey("M1022ICD9M5") ? assessmentQuestions["M1022ICD9M5"].Answer : string.Empty) + "</code6>";
                                    diagnosis += "</DiagonasisCodes>";
                                    final.DiagnosisCode = diagnosis;
                                    final.HippsCode = assessment.HippsCode;
                                    final.ClaimKey = assessment.ClaimKey;
                                    final.AssessmentType = assessmentType;
                                    if (assessmentType.IsNotNullOrEmpty())
                                    {
                                        var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                        if (assessmentType == DisciplineTasks.OASISCStartofCare.ToString() || assessmentType == DisciplineTasks.OASISCStartofCarePT.ToString()|| assessmentType==DisciplineTasks.OASISCStartofCareOT.ToString())
                                        {
                                            final.ProspectivePay = Math.Round(lookUpRepository.ProspectivePayAmount(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                        }
                                        else
                                        {
                                            final.ProspectivePay = Math.Round(lookUpRepository.ProspectivePayAmount(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                   
                }
                else
                {
                    //if (episode != null)
                    //{
                    //    final.EpisodeStartDate = episode.StartDate;
                    //    final.EpisodeEndDate = episode.EndDate;
                    //}
                    final.FirstBillableVisitDateFormat = final.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                }
                final.BranchId = patient.AgencyLocationId;
            }
            return final;
        }

        public object GetVisitRateInstance(string disciplineName, string codeOne, string codeTwo, string amount, int unit)
        {
            switch (disciplineName)
            {
                case "SN":
                    return new { SN = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "SNM":
                    return new { SNM = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "SNO":
                    return new { SNO = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "SNT":
                    return new { SNT = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "HHA":
                    return new { HHA = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "PT":
                    return new { PT = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "PTA":
                    return new { PTA = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "PTM":
                    return new { PTM = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "OT":
                    return new { OT = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "OTA":
                    return new { OTA = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "OTM":
                    return new { OTM = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "ST":
                    return new { ST = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "STM":
                    return new { STM = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
                case "MSW":
                    return new { MSW = new { id1 = codeOne, id2 = codeTwo, units = unit, amount = amount } };
            }
            return null;
        }

        public Dictionary<string, BillInfo> GetInsuranceUnit(int insurnaceId, out int unitType)
        {
            var unit = new Dictionary<string, BillInfo>();
            unitType = -1;
            if (insurnaceId >= 1000)
            {
                var insurance = agencyRepository.FindInsurance(Current.AgencyId, insurnaceId);
                if (insurance != null)
                {
                    var data = insurance.ToChargeRateDictionary();
                    unitType = insurance.ChargeType;
                    if (data != null && data.Count > 0)
                    {
                        if (data.ContainsKey("SkilledNurse"))
                        {
                            unit.Add("SN", new BillInfo { Discipline = "SN", Amount = data["SkilledNurse"].Charge, Unit = 3, CodeOne = "0551", CodeTwo = data["SkilledNurse"].Code });
                        }
                        if (data.ContainsKey("SkilledNurseManagement"))
                        {
                            unit.Add("SNM", new BillInfo { Discipline = "SNM", Amount = data["SkilledNurseManagement"].Charge, Unit = 3, CodeOne = "0551", CodeTwo = data["SkilledNurseManagement"].Code });
                        }
                        if (data.ContainsKey("SkilledNurseObservation"))
                        {
                            unit.Add("SNO", new BillInfo { Discipline = "SNO", Amount = data["SkilledNurseObservation"].Charge, Unit = 3, CodeOne = "0551", CodeTwo = data["SkilledNurseObservation"].Code });
                        }
                        if (data.ContainsKey("SkilledNurseTeaching"))
                        {
                            unit.Add("SNT", new BillInfo { Discipline = "SNT", Amount = data["SkilledNurseTeaching"].Charge, Unit = 3, CodeOne = "0551", CodeTwo = data["SkilledNurseTeaching"].Code });
                        }
                        if (data.ContainsKey("HomeHealthAide"))
                        {
                            unit.Add("HHA", new BillInfo { Discipline = "HHA", Amount = data["HomeHealthAide"].Charge, Unit = 4, CodeOne = "0571", CodeTwo = data["HomeHealthAide"].Code });
                        }
                        if (data.ContainsKey("PhysicalTherapy"))
                        {
                            unit.Add("PT", new BillInfo { Discipline = "PT", Amount = data["PhysicalTherapy"].Charge, Unit = 3, CodeOne = "0421", CodeTwo = data["PhysicalTherapy"].Code });
                        }
                        if (data.ContainsKey("PhysicalTherapyAssistance"))
                        {
                            unit.Add("PTA", new BillInfo { Discipline = "PTA", Amount = data["PhysicalTherapyAssistance"].Charge, Unit = 3, CodeOne = "0421", CodeTwo = data["PhysicalTherapyAssistance"].Code });
                        }
                        if (data.ContainsKey("PhysicalTherapyMaintenance"))
                        {
                            unit.Add("PTM", new BillInfo { Discipline = "PTM", Amount = data["PhysicalTherapyMaintenance"].Charge, Unit = 3, CodeOne = "0421", CodeTwo = data["PhysicalTherapyMaintenance"].Code });
                        }
                        if (data.ContainsKey("OccupationalTherapy"))
                        {
                            unit.Add("OT", new BillInfo { Discipline = "OT", Amount = data["OccupationalTherapy"].Charge, Unit = 3, CodeOne = "0431", CodeTwo = data["OccupationalTherapy"].Code });
                        }
                        if (data.ContainsKey("OccupationalTherapyAssistance"))
                        {
                            unit.Add("OTA", new BillInfo { Discipline = "OTA", Amount = data["OccupationalTherapyAssistance"].Charge, Unit = 3, CodeOne = "0431", CodeTwo = data["OccupationalTherapyAssistance"].Code });
                        }
                        if (data.ContainsKey("OccupationalTherapyMaintenance"))
                        {
                            unit.Add("OTM", new BillInfo { Discipline = "OTM", Amount = data["OccupationalTherapyMaintenance"].Charge, Unit = 3, CodeOne = "0431", CodeTwo = data["OccupationalTherapyMaintenance"].Code });
                        }
                        if (data.ContainsKey("SpeechTherapy"))
                        {
                            unit.Add("ST", new BillInfo { Discipline = "ST", Amount = data["SpeechTherapy"].Charge, Unit = 4, CodeOne = "0440", CodeTwo = data["SpeechTherapy"].Code });
                        }
                        if (data.ContainsKey("SpeechTherapyMaintenance"))
                        {
                            unit.Add("STM", new BillInfo { Discipline = "STM", Amount = data["SpeechTherapyMaintenance"].Charge, Unit = 4, CodeOne = "0440", CodeTwo = data["SpeechTherapyMaintenance"].Code });
                        }
                        if (data.ContainsKey("MedicareSocialWorker"))
                        {
                            unit.Add("MSW", new BillInfo { Discipline = "MSW", Amount = data["MedicareSocialWorker"].Charge, Unit = 3, CodeOne = "0561", CodeTwo = data["MedicareSocialWorker"].Code });
                        }
                    }
                }
            }
            else if (insurnaceId < 1000)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                if (agencyLocation != null && agencyLocation.Cost.IsNotNullOrEmpty())
                {
                    var data = agencyLocation.ToCostRateDictionary();
                    if (data != null && data.Count > 0)
                    {
                        unit.Add("SN", new BillInfo { Discipline = "SN", Amount = data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].PerUnit : string.Empty, Unit = 3, CodeOne = "0551", CodeTwo = "G0154" });
                        unit.Add("SNM", new BillInfo { Discipline = "SNM", Amount = data.ContainsKey("SkilledNurseManagement") ? data["SkilledNurseManagement"].PerUnit : string.Empty, Unit = 3, CodeOne = "0551", CodeTwo = "G0162" });
                        unit.Add("SNO", new BillInfo { Discipline = "SNO", Amount = data.ContainsKey("SkilledNurseObservation") ? data["SkilledNurseObservation"].PerUnit : string.Empty, Unit = 3, CodeOne = "0551", CodeTwo = "G0163" });
                        unit.Add("SNT", new BillInfo { Discipline = "SNT", Amount = data.ContainsKey("SkilledNurseTeaching") ? data["SkilledNurseTeaching"].PerUnit : string.Empty, Unit = 3, CodeOne = "0551", CodeTwo = "G0164" });
                        unit.Add("HHA", new BillInfo { Discipline = "HHA", Amount = data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].PerUnit : string.Empty, Unit = 4, CodeOne = "0571", CodeTwo = "G0156" });
                        unit.Add("PT", new BillInfo { Discipline = "PT", Amount = data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].PerUnit : string.Empty, Unit = 3, CodeOne = "0421", CodeTwo = "G0151" });
                        unit.Add("PTA", new BillInfo { Discipline = "PTA", Amount = data.ContainsKey("PhysicalTherapyAssistance") ? data["PhysicalTherapyAssistance"].PerUnit : string.Empty, Unit = 3, CodeOne = "0421", CodeTwo = "G0157" });
                        unit.Add("PTM", new BillInfo { Discipline = "PTM", Amount = data.ContainsKey("PhysicalTherapyMaintenance") ? data["PhysicalTherapyMaintenance"].PerUnit : string.Empty, Unit = 3, CodeOne = "0421", CodeTwo = "G0159" });
                        unit.Add("OT", new BillInfo { Discipline = "OT", Amount = data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].PerUnit : string.Empty, Unit = 3, CodeOne = "0431", CodeTwo = "G0152" });
                        unit.Add("OTA", new BillInfo { Discipline = "OTA", Amount = data.ContainsKey("OccupationalTherapyAssistance") ? data["OccupationalTherapyAssistance"].PerUnit : string.Empty, Unit = 3, CodeOne = "0431", CodeTwo = "G0158" });
                        unit.Add("OTM", new BillInfo { Discipline = "OTM", Amount = data.ContainsKey("OccupationalTherapyMaintenance") ? data["OccupationalTherapyMaintenance"].PerUnit : string.Empty, Unit = 3, CodeOne = "0431", CodeTwo = "G0160" });
                        unit.Add("ST", new BillInfo { Discipline = "ST", Amount = data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].PerUnit : string.Empty, Unit = 4, CodeOne = "0440", CodeTwo = "G0153" });
                        unit.Add("STM", new BillInfo { Discipline = "STM", Amount = data.ContainsKey("SpeechTherapyMaintenance") ? data["SpeechTherapyMaintenance"].PerUnit : string.Empty, Unit = 4, CodeOne = "0440", CodeTwo = "G0161" });
                        unit.Add("MSW", new BillInfo { Discipline = "MSW", Amount = data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].PerUnit : string.Empty, Unit = 3, CodeOne = "0561", CodeTwo = "G0155" });
                    }
                    else
                    {
                        unit.Add("SN", new BillInfo { Discipline = "SN", Amount = "200.00", Unit = 3, CodeOne = "0551", CodeTwo = "G0154" });
                        unit.Add("SNM", new BillInfo { Discipline = "SNM", Amount = "200.00", Unit = 3, CodeOne = "0551", CodeTwo = "G0162" });
                        unit.Add("SNO", new BillInfo { Discipline = "SNO", Amount = "200.00", Unit = 3, CodeOne = "0551", CodeTwo = "G0163" });
                        unit.Add("SNT", new BillInfo { Discipline = "SNT", Amount = "200.00", Unit = 3, CodeOne = "0551", CodeTwo = "G0164" });
                        unit.Add("HHA", new BillInfo { Discipline = "HHA", Amount = "120.00", Unit = 4, CodeOne = "0571", CodeTwo = "G0156" });
                        unit.Add("PT", new BillInfo { Discipline = "PT", Amount = "250.00", Unit = 3, CodeOne = "0421", CodeTwo = "G0151" });
                        unit.Add("PTA", new BillInfo { Discipline = "PTA", Amount = "250.00", Unit = 3, CodeOne = "0421", CodeTwo = "G0157" });
                        unit.Add("PTM", new BillInfo { Discipline = "PTM", Amount = "250.00", Unit = 3, CodeOne = "0421", CodeTwo = "G0159" });
                        unit.Add("OT", new BillInfo { Discipline = "OT", Amount = "250.00", Unit = 3, CodeOne = "0431", CodeTwo = "G0152" });
                        unit.Add("OTA", new BillInfo { Discipline = "OTA", Amount = "250.00", Unit = 3, CodeOne = "0431", CodeTwo = "G0158" });
                        unit.Add("OTM", new BillInfo { Discipline = "OTM", Amount = "250.00", Unit = 3, CodeOne = "0431", CodeTwo = "G0160" });
                        unit.Add("ST", new BillInfo { Discipline = "ST", Amount = "250.00", Unit = 4, CodeOne = "0440", CodeTwo = "G0153" });
                        unit.Add("STM", new BillInfo { Discipline = "STM", Amount = "250.00", Unit = 4, CodeOne = "0440", CodeTwo = "G0161" });
                        unit.Add("MSW", new BillInfo { Discipline = "MSW", Amount = "250.00", Unit = 3, CodeOne = "0561", CodeTwo = "G0155" });
                    }
                }
                else
                {
                    unit.Add("SN", new BillInfo { Discipline = "SN", Amount = "200.00", Unit = 3, CodeOne = "0551", CodeTwo = "G0154" });
                    unit.Add("SNM", new BillInfo { Discipline = "SNM", Amount = "200.00", Unit = 3, CodeOne = "0551", CodeTwo = "G0162" });
                    unit.Add("SNO", new BillInfo { Discipline = "SNO", Amount = "200.00", Unit = 3, CodeOne = "0551", CodeTwo = "G0163" });
                    unit.Add("SNT", new BillInfo { Discipline = "SNT", Amount = "200.00", Unit = 3, CodeOne = "0551", CodeTwo = "G0164" });
                    unit.Add("HHA", new BillInfo { Discipline = "HHA", Amount = "120.00", Unit = 4, CodeOne = "0571", CodeTwo = "G0156" });
                    unit.Add("PT", new BillInfo { Discipline = "PT", Amount = "250.00", Unit = 3, CodeOne = "0421", CodeTwo = "G0151" });
                    unit.Add("PTA", new BillInfo { Discipline = "PTA", Amount = "250.00", Unit = 3, CodeOne = "0421", CodeTwo = "G0157" });
                    unit.Add("PTM", new BillInfo { Discipline = "PTM", Amount = "250.00", Unit = 3, CodeOne = "0421", CodeTwo = "G0159" });
                    unit.Add("OT", new BillInfo { Discipline = "OT", Amount = "250.00", Unit = 3, CodeOne = "0431", CodeTwo = "G0152" });
                    unit.Add("OTA", new BillInfo { Discipline = "OTA", Amount = "250.00", Unit = 3, CodeOne = "0431", CodeTwo = "G0158" });
                    unit.Add("OTM", new BillInfo { Discipline = "OTM", Amount = "250.00", Unit = 3, CodeOne = "0431", CodeTwo = "G0160" });
                    unit.Add("ST", new BillInfo { Discipline = "ST", Amount = "250.00", Unit = 4, CodeOne = "0440", CodeTwo = "G0153" });
                    unit.Add("STM", new BillInfo { Discipline = "STM", Amount = "250.00", Unit = 4, CodeOne = "0440", CodeTwo = "G0161" });
                    unit.Add("MSW", new BillInfo { Discipline = "MSW", Amount = "250.00", Unit = 3, CodeOne = "0561", CodeTwo = "G0155" });
                }
            }
            return unit;
        }

        public bool UpdateRapStatus(List<Guid> rapToGenerate, Guid agencyId, string statusType)
        {
            bool result = false;
            if (rapToGenerate != null)
            {
                rapToGenerate.ForEach(r =>
                {
                    var rap = billingRepository.GetRap(agencyId, r);
                    if (rap != null)
                    {
                        var oldStatus = rap.Status;
                        if (statusType == "Submit")
                        {
                            if (rap.Status != (int)ScheduleStatus.ClaimSubmitted)
                            {
                                rap.Status = (int)ScheduleStatus.ClaimSubmitted;
                                rap.ClaimDate = DateTime.Now;
                            }
                            rap.IsGenerated = true;
                            rap.IsVerified = true;
                        }
                        else if (statusType == "Cancelled")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimCancelledClaim;
                        }
                        else if (statusType == "Rejected")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimRejected;
                        }
                        else if (statusType == "Accepted")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimAccepted;
                            rap.IsGenerated = true;
                        }
                        else if (statusType == "PaymentPending")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimPaymentPending;
                            rap.IsGenerated = true;
                        }
                        else if (statusType == "Error")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimWithErrors;
                        }
                        else if (statusType == "Paid")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimPaidClaim;
                            rap.IsGenerated = true;
                        }
                        else if (statusType == "ReOpen")
                        {
                            rap.Status = (int)ScheduleStatus.ClaimReOpen;
                            rap.ClaimDate = DateTime.MinValue;
                            rap.IsVerified = false;
                            rap.IsGenerated = false;
                        }
                        if (billingRepository.UpdateRapStatus(rap))
                        {
                            var final = billingRepository.GetFinal(agencyId, rap.PatientId, rap.EpisodeId);
                            if (final != null)
                            {
                                final.IsRapGenerated = rap.IsGenerated;
                                billingRepository.UpdateFinalStatus(final);
                            }
                            var isStatusChange = oldStatus == rap.Status;
                            if (!isStatusChange)
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPUpdatedWithStatus, ((Enum.IsDefined(typeof(ScheduleStatus), oldStatus) ? ("From " + ((ScheduleStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ScheduleStatus), rap.Status) ? (" To " + ((ScheduleStatus)rap.Status).GetDescription()) : string.Empty)));
                            }
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool UpdateFinalStatus(List<Guid> finalToGenerate, Guid agencyId, string statusType)
        {
            bool result = false;
            if (finalToGenerate != null)
            {
                finalToGenerate.ForEach(f =>
                {
                    var final = billingRepository.GetFinal(agencyId, f);
                    if (final != null)
                    {
                        var oldStatus = final.Status;
                        if (statusType == "Submit")
                        {
                            if (final.Status != (int)ScheduleStatus.ClaimSubmitted)
                            {
                                final.Status = (int)ScheduleStatus.ClaimSubmitted;
                                final.ClaimDate = DateTime.Now;
                            }
                            final.IsGenerated = true;
                            final.IsFinalInfoVerified = true;
                            final.IsVisitVerified = true;
                            final.IsSupplyVerified = true;
                        }
                        else if (statusType == "Cancelled")
                        {
                            final.Status = (int)ScheduleStatus.ClaimCancelledClaim;
                        }
                        else if (statusType == "Rejected")
                        {
                            final.Status = (int)ScheduleStatus.ClaimRejected;
                        }
                        else if (statusType == "Accepted")
                        {
                            final.Status = (int)ScheduleStatus.ClaimAccepted;
                        }
                        else if (statusType == "PaymentPending")
                        {
                            final.Status = (int)ScheduleStatus.ClaimPaymentPending;
                        }
                        else if (statusType == "Error")
                        {
                            final.Status = (int)ScheduleStatus.ClaimWithErrors;
                        }
                        else if (statusType == "Paid")
                        {
                            final.Status = (int)ScheduleStatus.ClaimPaidClaim;
                        }
                        else if (statusType == "ReOpen")
                        {
                            final.Status = (int)ScheduleStatus.ClaimReOpen;
                            final.ClaimDate = DateTime.MinValue;
                            final.IsFinalInfoVerified = false;
                            final.IsSupplyVerified = false;
                            final.IsVisitVerified = false;
                            final.IsGenerated = false;
                        }
                        if (billingRepository.UpdateFinalStatus(final))
                        {
                            var isStatusChange = oldStatus == final.Status;
                            if (!isStatusChange)
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalUpdatedWithStatus, ((Enum.IsDefined(typeof(ScheduleStatus), oldStatus) ? ("From " + ((ScheduleStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ScheduleStatus), final.Status) ? (" To " + ((ScheduleStatus)final.Status).GetDescription()) : string.Empty)));
                            }
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool FinalComplete(Guid Id)
        {
            var final = billingRepository.GetFinal(Current.AgencyId, Id);
            bool result = false;
            if (final != null)
            {
                if (billingRepository.UpdateFinalStatus(final))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalSummaryVerified, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public UBOFourViewData GetUBOFourInfo(Guid patientId, Guid claimId, string type)
        {
            var uboFourViewData = new UBOFourViewData();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                uboFourViewData.Agency = agency;
                uboFourViewData.AgencyLocation = agency.MainLocation;
                if (type.IsEqual("rap"))
                {
                    var rap = billingRepository.GetRap(Current.AgencyId, claimId);
                    if (rap != null)
                    {
                        var patient = patientRepository.GetPatientOnly(rap.PatientId, Current.AgencyId);
                        AgencyLocation branch = null;
                        if (patient != null)
                        {
                            branch = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                        }
                        var rapClaimData = new ClaimViewData
                        {
                            Status = patient != null && patient.Status == (int)PatientStatus.Discharged ? 1 : 30,
                            PatientIdNumber = rap.PatientIdNumber,
                            Type = rap.Type == 1 ? "" : "322",
                            EpisodeStartDate = rap.EpisodeStartDate,
                            EpisodeEndDate = rap.EpisodeEndDate,
                            FirstName = rap.FirstName,
                            LastName = rap.LastName,
                            AddressLine1 = rap.AddressLine1,
                            AddressLine2 = rap.AddressLine2,
                            AddressCity = rap.AddressCity,
                            AddressStateCode = rap.AddressStateCode,
                            AddressZipCode = rap.AddressZipCode,
                            DOB = rap.DOB,
                            Gender = rap.Gender,
                            AdmissionSource = rap.AdmissionSource.IsNotNullOrEmpty() && rap.AdmissionSource != "0" ? lookUpRepository.GetAdmissionSourceCode(int.Parse(rap.AdmissionSource)) : "9",
                            CBSA =lookUpRepository.CbsaCodeByZip(rap.AddressZipCode),
                            HippsCode = rap.HippsCode,
                            PrimaryInsuranceId = patient != null && patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() ? patient.PrimaryInsurance.ToInteger() : rap.PrimaryInsuranceId,
                            ClaimKey = rap.ClaimKey,
                            DiagnosisCode = rap.DiagnosisCode,
                            PhysicianFirstName = rap.PhysicianFirstName,
                            PhysicianLastName = rap.PhysicianLastName,
                            PhysicianNPI = rap.PhysicianNPI,
                            StartofCareDate = rap.StartofCareDate,
                            MedicareNumber = rap.MedicareNumber,
                            FirstBillableVisitDate = rap.FirstBillableVisitDate,
                            HealthPlanId = rap.HealthPlanId,
                            PayorAuthorizationCode = rap.AuthorizationNumber,
                            ConditionCodes = rap.ConditionCodes
                        };
                        uboFourViewData.Claim = rapClaimData;
                    }
                }
                else if (type.IsEqual("final"))
                {
                    var final = billingRepository.GetFinal(Current.AgencyId, claimId);
                    if (final != null)
                    {
                        var patient = patientRepository.GetPatientOnly(final.PatientId, Current.AgencyId);
                        AgencyLocation branch = null;
                        if (patient != null)
                        {
                            branch = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                        }
                        var finalClaimData = new ClaimViewData
                        {
                            Status = patient != null && patient.Status == (int)PatientStatus.Discharged ? 1 : 30,
                            PatientIdNumber = final.PatientIdNumber,
                            Type = final.Type == 1 ? "" : "329",
                            EpisodeStartDate = final.EpisodeStartDate,
                            EpisodeEndDate = final.EpisodeEndDate,
                            FirstName = final.FirstName,
                            LastName = final.LastName,
                            AddressLine1 = final.AddressLine1,
                            AddressLine2 = final.AddressLine2,
                            AddressCity = final.AddressCity,
                            AddressStateCode = final.AddressStateCode,
                            AddressZipCode = final.AddressZipCode,
                            DOB = final.DOB,
                            Gender = final.Gender,
                            AdmissionSource = final.AdmissionSource.IsNotNullOrEmpty() && final.AdmissionSource != "0" ? lookUpRepository.GetAdmissionSourceCode(int.Parse(final.AdmissionSource)) : "9",
                            CBSA = lookUpRepository.CbsaCodeByZip(final.AddressZipCode),
                            HippsCode = final.HippsCode,
                            PrimaryInsuranceId = patient != null && patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() ? patient.PrimaryInsurance.ToInteger() : final.PrimaryInsuranceId,
                            ClaimKey = final.ClaimKey,
                            DiagnosisCode = final.DiagnosisCode,
                            PhysicianFirstName = final.PhysicianFirstName,
                            PhysicianLastName = final.PhysicianLastName,
                            PhysicianNPI = final.PhysicianNPI,
                            VerifiedVisit = final.VerifiedVisits,
                            StartofCareDate = final.StartofCareDate,
                            MedicareNumber = final.MedicareNumber,
                            SupplyTotal = final.SupplyTotal,
                            FirstBillableVisitDate = final.FirstBillableVisitDate,
                            HealthPlanId = final.HealthPlanId,
                            PayorAuthorizationCode = final.AuthorizationNumber,
                            ConditionCodes = final.ConditionCodes
                        };
                        uboFourViewData.Claim = finalClaimData;
                    }
                }
                if (uboFourViewData.Claim != null)
                {
                    if (uboFourViewData.Claim.PrimaryInsuranceId >= 1000)
                    {
                        var insurance = agencyRepository.GetInsurance(uboFourViewData.Claim.PrimaryInsuranceId, Current.AgencyId);
                        if (insurance != null)
                        {
                            uboFourViewData.Claim.IsHMO = true;
                            if (insurance.PayorId.IsNotNullOrEmpty() && !(insurance.PayorId == "0")) uboFourViewData.Claim.PayerId = insurance.PayorId;
                            uboFourViewData.Claim.PayorName = insurance.Name;
                            uboFourViewData.Claim.PayorAddressLine1 = insurance.AddressFirstRow;
                            uboFourViewData.Claim.PayorAddressLine2 = insurance.AddressSecondRow;
                            uboFourViewData.Claim.OtherProviderId = insurance.OtherProviderId;
                            uboFourViewData.Claim.ProviderId = insurance.ProviderId;
                            uboFourViewData.Claim.ProviderSubscriberId = insurance.ProviderSubscriberId;
                        }
                    }
                    else if (uboFourViewData.Claim.PrimaryInsuranceId < 1000 && uboFourViewData.Claim.PrimaryInsuranceId > 0)
                    {
                        uboFourViewData.Claim.IsHMO = false;
                        var insurance = lookUpRepository.GetInsurance(uboFourViewData.Claim.PrimaryInsuranceId);
                        if (insurance != null)
                        {
                            uboFourViewData.Claim.PayorName = insurance.Name;
                          
                        }
                    }
                }
            }
            return uboFourViewData;
        }

        public string GetCbsaFromZip(string zip)
        {
            var result = lookUpRepository.CbsaCodeByZip(zip);
            return result == null ? string.Empty : result;
        }

        public ClaimInfoSnapShotViewData GetClaimSnapShotInfo(Guid patientId, Guid claimId, string type)
        {
            var claimInfo = new ClaimInfoSnapShotViewData();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                if (type.IsEqual("Rap"))
                {
                    var rap = billingRepository.GetRap(Current.AgencyId, claimId);
                    if (rap != null)
                    {
                        if (rap.Status == (int)ScheduleStatus.ClaimPaidClaim || rap.Status == (int)ScheduleStatus.ClaimPaymentPending || rap.Status == (int)ScheduleStatus.ClaimSubmitted || rap.Status == (int)ScheduleStatus.ClaimAccepted)
                        {
                            claimInfo.Id = rap.Id;
                            claimInfo.PatientId = rap.PatientId;
                            claimInfo.PatientName = string.Format("{0} {1}", rap.FirstName, rap.LastName);
                            claimInfo.PatientIdNumber = rap.PatientIdNumber;
                            claimInfo.MedicareNumber = rap.MedicareNumber;
                            claimInfo.Type = "RAP";
                            var hhrg = lookUpRepository.GetHHRGByHIPPSCODE(rap.HippsCode);
                            if (hhrg != null)
                            {
                                claimInfo.HHRG = hhrg.HHRG;
                            }
                            claimInfo.HIPPS = rap.HippsCode;
                            claimInfo.ClaimKey = rap.ClaimKey;

                            if (rap.HippsCode.IsNotNullOrEmpty() && rap.HippsCode.Length == 5)
                            {
                                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                PPSStandard ppsStandard;
                                claimInfo.SupplyReimbursement = GetSupplyReimbursement(rap.HippsCode[4], rap.EpisodeStartDate.Year, out ppsStandard);
                                claimInfo.StandardEpisodeRate = lookUpRepository.ProspectivePayAmount(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                            }
                            claimInfo.ProspectivePay = rap.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || rap.AssessmentType==DisciplineTasks.OASISCStartofCareOT.ToString() ? 0.6 * claimInfo.StandardEpisodeRate : 0.5 * claimInfo.StandardEpisodeRate;
                            if (!(rap.PrimaryInsuranceId <= 0))
                            {
                                if (rap.PrimaryInsuranceId >= 1000)
                                {
                                    var insurance = agencyRepository.GetInsurance(rap.PrimaryInsuranceId, Current.AgencyId);
                                    if (insurance != null)
                                    {
                                        claimInfo.PayorName = insurance.Name;
                                    }
                                }
                                else
                                {
                                    var insurance = lookUpRepository.GetInsurance(rap.PrimaryInsuranceId);
                                    if (insurance != null)
                                    {
                                        claimInfo.PayorName = insurance.Name;
                                    }
                                }
                            }
                            if (rap.IsVerified)
                            {
                                claimInfo.Visible = true;
                            }
                        }
                        else
                        {
                            claimInfo.Id = rap.Id;
                            claimInfo.PatientId = rap.PatientId;
                            claimInfo.PatientName = string.Format("{0} {1}", patient.FirstName, patient.LastName);
                            claimInfo.PatientIdNumber = patient.PatientIdNumber;
                            claimInfo.MedicareNumber = patient.MedicareNumber;
                            claimInfo.Type = "RAP";
                           
                                if (!(rap.PrimaryInsuranceId <= 0))
                                {
                                    if (rap.PrimaryInsuranceId >= 1000)
                                    {
                                        var insurance = agencyRepository.GetInsurance(rap.PrimaryInsuranceId, Current.AgencyId);
                                        if (insurance != null)
                                        {
                                            claimInfo.PayorName = insurance.Name;
                                        }
                                    }
                                    else
                                    {
                                        var insurance = lookUpRepository.GetInsurance(rap.PrimaryInsuranceId);
                                        if (insurance != null)
                                        {
                                            claimInfo.PayorName = insurance.Name;
                                        }
                                    }
                                }
                            claimInfo.Visible = false;
                        }
                    }
                }
                else if (type.IsEqual("Final"))
                {
                    var final = billingRepository.GetFinal(Current.AgencyId, claimId);
                    if (final != null)
                    {
                        if (final.Status == (int)ScheduleStatus.ClaimPaidClaim || final.Status == (int)ScheduleStatus.ClaimPaymentPending || final.Status == (int)ScheduleStatus.ClaimSubmitted || final.Status == (int)ScheduleStatus.ClaimAccepted)
                        {
                            claimInfo.Id = final.Id;
                            claimInfo.PatientId = final.PatientId;
                            claimInfo.PatientName = string.Format("{0} {1}", final.FirstName, final.LastName);
                            claimInfo.PatientIdNumber = final.PatientIdNumber;
                            claimInfo.MedicareNumber = final.MedicareNumber;
                            claimInfo.Type = "Final";
                            var hhrg = lookUpRepository.GetHHRGByHIPPSCODE(final.HippsCode);
                            if (hhrg != null)
                            {
                                claimInfo.HHRG = hhrg.HHRG;
                            }
                            claimInfo.HIPPS = final.HippsCode;
                            claimInfo.ClaimKey = final.ClaimKey;

                            if (final.HippsCode.IsNotNullOrEmpty())
                            {
                                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                PPSStandard ppsStandard;
                                claimInfo.SupplyReimbursement = GetSupplyReimbursement(final.HippsCode[final.HippsCode.Length - 1], final.EpisodeStartDate.Year, out ppsStandard);
                                claimInfo.StandardEpisodeRate = lookUpRepository.ProspectivePayAmount(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                            }
                            claimInfo.ProspectivePay = claimInfo.StandardEpisodeRate;
                            if (!(final.PrimaryInsuranceId <= 0))
                            {
                                if (final.PrimaryInsuranceId >= 1000)
                                {
                                    var insurance = agencyRepository.GetInsurance(final.PrimaryInsuranceId, Current.AgencyId);
                                    if (insurance != null)
                                    {
                                        claimInfo.PayorName = insurance.Name;
                                    }
                                }
                                else
                                {
                                    var insurance = lookUpRepository.GetInsurance(final.PrimaryInsuranceId);
                                    if (insurance != null)
                                    {
                                        claimInfo.PayorName = insurance.Name;
                                    }
                                }
                            }
                            claimInfo.Visible = true;
                        }
                        else
                        {
                            claimInfo.Id = final.Id;
                            claimInfo.PatientId = final.PatientId;
                            claimInfo.PatientName = string.Format("{0} {1}", patient.FirstName, patient.LastName);
                            claimInfo.PatientIdNumber = patient.PatientIdNumber;
                            claimInfo.MedicareNumber = patient.MedicareNumber;
                            claimInfo.Type = "Final";
                            claimInfo.Visible = false;
                            
                            if (!(final.PrimaryInsuranceId <= 0))
                            {
                                if (final.PrimaryInsuranceId >= 1000)
                                {
                                    var insurance = agencyRepository.GetInsurance(final.PrimaryInsuranceId, Current.AgencyId);
                                    if (insurance != null)
                                    {
                                        claimInfo.PayorName = insurance.Name;
                                    }
                                }
                                else
                                {
                                    var insurance = lookUpRepository.GetInsurance(final.PrimaryInsuranceId);
                                    if (insurance != null)
                                    {
                                        claimInfo.PayorName = insurance.Name;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return claimInfo;
        }

        public ClaimViewData GetClaimViewData(Guid patientId, Guid Id, string type)
        {
            var claimData = new ClaimViewData();
            if (type == "Rap" || type == "RAP")
            {
                var rap = billingRepository.GetRap(Current.AgencyId, Id);
                if (rap != null)
                {
                    claimData.Id = rap.Id;
                    claimData.PatientId = rap.PatientId;
                    claimData.EpisodeId = rap.EpisodeId;
                    claimData.FirstName = rap.FirstName;
                    claimData.LastName = rap.LastName;
                    claimData.MedicareNumber = rap.MedicareNumber;
                    claimData.PrimaryInsuranceId = rap.PrimaryInsuranceId;
                    claimData.PaymentDate = rap.PaymentDate;
                    claimData.PaymentAmount = rap.Payment;
                    claimData.Type = "RAP";
                    claimData.Created = rap.Created;
                    claimData.Status = rap.Status;
                    claimData.EpisodeStartDate = rap.EpisodeStartDate;
                    claimData.EpisodeEndDate = rap.EpisodeEndDate;
                    claimData.ClaimDate = rap.ClaimDate.IsValid() ? rap.ClaimDate.ToString("MM/dd/yyyy") : rap.EpisodeStartDate.ToString("MM/dd/yyyy");
                    claimData.ClaimAmount = rap.ProspectivePay;
                    claimData.Comment = rap.Comment;
                }
            }
            else if (type == "Final" || type == "FINAL")
            {
                var final = billingRepository.GetFinal(Current.AgencyId, Id);
                if (final != null)
                {
                    claimData.PatientId = final.PatientId;
                    claimData.Id = final.Id;
                    claimData.EpisodeId = final.EpisodeId;
                    claimData.Type = "Final";
                    claimData.FirstName = final.FirstName;
                    claimData.LastName = final.LastName;
                    claimData.MedicareNumber = final.MedicareNumber;
                    claimData.PrimaryInsuranceId = final.PrimaryInsuranceId;
                    claimData.PaymentDate = final.PaymentDate;
                    claimData.Created = final.Created;
                    claimData.Status = final.Status;
                    claimData.EpisodeStartDate = final.EpisodeStartDate;
                    claimData.EpisodeEndDate = final.EpisodeEndDate;
                    claimData.PaymentAmount = final.Payment;
                    claimData.ClaimDate = final.ClaimDate.IsValid() ? final.ClaimDate.ToString("MM/dd/yyyy") : final.EpisodeEndDate.ToString("MM/dd/yyyy");
                    claimData.ClaimAmount = final.ProspectivePay;
                    claimData.Comment = final.Comment;
                }
            }
            return claimData;
        }

        public double GetSupplyReimbursement(char type, int year, out PPSStandard ppsStandardOut)
        {
            var ppsStandard = lookUpRepository.GetPPSStandardByYear(year);
            ppsStandardOut = ppsStandard;
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        return ppsStandard.S;
                    case 'T':
                        return ppsStandard.T;
                    case 'U':
                        return ppsStandard.U;
                    case 'V':
                        return ppsStandard.V;
                    case 'W':
                        return ppsStandard.W;
                    case 'X':
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        public bool UpdateProccesedClaimStatus(Guid patientId, Guid Id, string type, DateTime claimDate, double paymentAmount, DateTime paymentDate, int status, int primaryInsuranceId, string comment)
        {
            bool result = false;
            if (type.IsEqual("rap"))
            {
                var rap = billingRepository.GetRap(Current.AgencyId, Id);
                if (rap != null)
                {
                    rap.Payment = paymentAmount;
                    rap.PaymentDate = paymentDate;
                    rap.Comment = comment;
                    rap.PrimaryInsuranceId = primaryInsuranceId;
                    var isStatusChange = rap.Status == status;
                    var oldStatus = rap.Status;
                    if (status == (int)ScheduleStatus.ClaimReOpen || status == (int)ScheduleStatus.ClaimCreated)
                    {
                        rap.IsVerified = false;
                        rap.IsGenerated = false;
                    }
                    if (status == (int)ScheduleStatus.ClaimSubmitted && rap.Status != (int)ScheduleStatus.ClaimSubmitted)
                    {
                        rap.ClaimDate = DateTime.Now;
                        if (rap.IsVerified)
                        {
                            rap.IsGenerated = true;
                        }
                    }
                    rap.Status = status;
                    if (rap.Status == (int)ScheduleStatus.ClaimPaidClaim || rap.Status == (int)ScheduleStatus.ClaimPaymentPending || rap.Status == (int)ScheduleStatus.ClaimAccepted || rap.Status == (int)ScheduleStatus.ClaimSubmitted)
                    {
                        rap.IsGenerated = true;
                    }
                    if (billingRepository.UpdateRapStatus(rap))
                    {
                        var final = billingRepository.GetFinal(Current.AgencyId, rap.PatientId, rap.EpisodeId);
                        if (final != null)
                        {
                            final.IsRapGenerated = rap.IsGenerated;
                            billingRepository.UpdateFinalStatus(final);
                        }
                        Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, !isStatusChange ? LogAction.RAPUpdatedWithStatus : LogAction.RAPUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(ScheduleStatus), oldStatus) ? ("From " + ((ScheduleStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ScheduleStatus), status) ? (" To " + ((ScheduleStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                        result = true;
                    }
                }
            }
            else if (type.IsEqual("Final"))
            {
                var final = billingRepository.GetFinal(Current.AgencyId, Id);
                if (final != null)
                {
                    final.Payment = paymentAmount;
                    final.PaymentDate = paymentDate;
                    final.Comment = comment;
                    final.PrimaryInsuranceId = primaryInsuranceId;
                    var isStatusChange = final.Status == status;
                    var oldStatus = final.Status;
                    if (status == (int)ScheduleStatus.ClaimReOpen || status == (int)ScheduleStatus.ClaimCreated)
                    {
                        final.IsFinalInfoVerified = false;
                        final.IsSupplyVerified = false;
                        final.IsVisitVerified = false;
                        final.IsGenerated = false;
                    }
                    if (status == (int)ScheduleStatus.ClaimSubmitted && final.Status != (int)ScheduleStatus.ClaimSubmitted)
                    {
                        final.ClaimDate = DateTime.Now;
                        final.IsGenerated = true;
                    }
                    final.Status = status;
                    if (billingRepository.UpdateFinalStatus(final))
                    {
                        Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, !isStatusChange ? LogAction.FinalUpdatedWithStatus : LogAction.FinalUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(ScheduleStatus), oldStatus) ? ("From " + ((ScheduleStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ScheduleStatus), status) ? (" To " + ((ScheduleStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdatePendingClaimStatus(FormCollection formCollection, string[] rapIds, string[] finalIds)
        {
            bool result = false;
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length>0)
            {
                if (rapIds != null && rapIds.Length > 0)
                {
                    foreach (var id in rapIds)
                    {
                        var guidId = id.ToGuid();
                        var type = keys.Contains(string.Format("RapType_{0}", id)) && formCollection[string.Format("RapType_{0}", id)].IsNotNullOrEmpty() ? formCollection[string.Format("RapType_{0}", id)].ToString() : string.Empty;
                        var date = keys.Contains(string.Format("RapPaymentDate_{0}", id)) && formCollection[string.Format("RapPaymentDate_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("RapPaymentDate_{0}", id)].IsValidDate() ? formCollection[string.Format("RapPaymentDate_{0}", id)] : string.Empty;
                        var paymentAmount = keys.Contains(string.Format("RapPayment_{0}", id)) && formCollection[string.Format("RapPayment_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("RapPayment_{0}", id)].IsDouble() ? formCollection[string.Format("RapPayment_{0}", id)].ToDouble() : 0;
                        var status = keys.Contains(string.Format("RapStatus_{0}", id)) && formCollection[string.Format("RapStatus_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("RapStatus_{0}", id)].IsInteger() ? formCollection[string.Format("RapStatus_{0}", id)].ToInteger() : 0;
                        if (!guidId.IsEmpty() && type.IsNotNullOrEmpty())
                        {
                            if (type.IsEqual("Rap"))
                            {
                                var rap = billingRepository.GetRap(Current.AgencyId, guidId);
                                if (rap != null)
                                {
                                    rap.Payment = paymentAmount;
                                    rap.PaymentDate = date.IsNotNullOrEmpty() ? date.ToDateTime() : rap.PaymentDate;
                                    if (status == (int)ScheduleStatus.ClaimSubmitted && rap.Status != (int)ScheduleStatus.ClaimSubmitted)
                                    {
                                        rap.ClaimDate = DateTime.Now;
                                        if (rap.IsVerified)
                                        {
                                            rap.IsGenerated = true;
                                        }
                                    }
                                    var isStatusChange = rap.Status == status;
                                    var oldStatus = rap.Status;
                                    rap.Status = status;
                                    if (rap.Status == (int)ScheduleStatus.ClaimCreated)
                                    {
                                        rap.IsVerified = false;
                                        rap.IsGenerated = false;
                                    }
                                    if (billingRepository.UpdateRapStatus(rap))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Final, !isStatusChange ? LogAction.RAPUpdatedWithStatus : LogAction.RAPUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(ScheduleStatus), oldStatus) ? ("From " + ((ScheduleStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ScheduleStatus), status) ? (" To " + ((ScheduleStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                                        result = true;
                                    }
                                }
                            }
                        }
                    }
                }
                if (finalIds != null && finalIds.Length > 0)
                {
                    foreach (var id in finalIds)
                    {
                        var guidId = id.ToGuid();
                        var type = keys.Contains(string.Format("FinalType_{0}", id)) && formCollection[string.Format("FinalType_{0}", id)].IsNotNullOrEmpty() ? formCollection[string.Format("FinalType_{0}", id)].ToString() : string.Empty;
                        var date = keys.Contains(string.Format("FinalPaymentDate_{0}", id)) && formCollection[string.Format("FinalPaymentDate_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("FinalPaymentDate_{0}", id)].IsValidDate() ? formCollection[string.Format("FinalPaymentDate_{0}", id)] : string.Empty;
                        var paymentAmount = keys.Contains(string.Format("FinalPayment_{0}", id)) && formCollection[string.Format("FinalPayment_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("FinalPayment_{0}", id)].IsDouble() ? formCollection[string.Format("FinalPayment_{0}", id)].ToDouble() : 0;
                        var status = keys.Contains(string.Format("FinalStatus_{0}", id)) && formCollection[string.Format("FinalStatus_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("FinalStatus_{0}", id)].IsInteger() ? formCollection[string.Format("FinalStatus_{0}", id)].ToInteger() : 0;
                        if (!guidId.IsEmpty() && type.IsNotNullOrEmpty())
                        {
                            if (type.IsEqual("Final"))
                            {
                                var final = billingRepository.GetFinal(Current.AgencyId, guidId);
                                if (final != null)
                                {
                                    final.Payment = paymentAmount;
                                    final.PaymentDate = date.IsNotNullOrEmpty() ? date.ToDateTime() : final.PaymentDate;
                                    var isStatusChange = final.Status == status;
                                    var oldStatus = final.Status;
                                    final.Status = status;
                                    if (final.Status == (int)ScheduleStatus.ClaimCreated)
                                    {
                                        final.IsFinalInfoVerified = false;
                                        final.IsSupplyVerified = false;
                                        final.IsVisitVerified = false;
                                        final.IsGenerated = false;
                                    }
                                    if (billingRepository.UpdateFinalStatus(final))
                                    {
                                        Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, !isStatusChange ? LogAction.FinalUpdatedWithStatus : LogAction.FinalUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(ScheduleStatus), oldStatus) ? ("From " + ((ScheduleStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ScheduleStatus), status) ? (" To " + ((ScheduleStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                                        result = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateRapClaimStatus(Guid Id, DateTime paymentDate, double paymentAmount, int status)
        {
            bool result = false;
            var rap = billingRepository.GetRap(Current.AgencyId, Id);
            if (rap != null)
            {
                rap.Payment = paymentAmount;
                rap.PaymentDate = paymentDate;
                if (status == (int)ScheduleStatus.ClaimSubmitted && rap.Status != (int)ScheduleStatus.ClaimSubmitted)
                {
                    rap.ClaimDate = DateTime.Now;
                    if (rap.IsVerified)
                    {
                        rap.IsGenerated = true;
                    }
                }
                var isStatusChange = rap.Status == status;
                var oldStatus = rap.Status;
                rap.Status = status;
                if (rap.Status == (int)ScheduleStatus.ClaimPaidClaim || rap.Status == (int)ScheduleStatus.ClaimPaymentPending || rap.Status == (int)ScheduleStatus.ClaimAccepted || rap.Status == (int)ScheduleStatus.ClaimSubmitted)
                {
                    rap.IsGenerated = true;
                }
                if (rap.Status == (int)ScheduleStatus.ClaimCreated || rap.Status == (int)ScheduleStatus.ClaimReOpen)
                {
                    rap.IsVerified = false;
                    rap.IsGenerated = false;
                }
                if (billingRepository.UpdateRapStatus(rap))
                {
                    var final = billingRepository.GetFinal(Current.AgencyId, rap.PatientId, rap.EpisodeId);
                    if (final != null)
                    {
                        final.IsRapGenerated = rap.IsGenerated;
                        billingRepository.UpdateFinalStatus(final);
                    }
                    Auditor.AddGeneralLog(LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, !isStatusChange ? LogAction.RAPUpdatedWithStatus : LogAction.RAPUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(ScheduleStatus), oldStatus) ? ("From " + ((ScheduleStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ScheduleStatus), status) ? (" To " + ((ScheduleStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateFinalClaimStatus(Guid Id, DateTime paymentDate, double paymentAmount, int status)
        {
            bool result = false;
            var final = billingRepository.GetFinal(Current.AgencyId, Id);
            if (final != null)
            {
                final.Payment = paymentAmount;
                final.PaymentDate = paymentDate;
                if (status == (int)ScheduleStatus.ClaimSubmitted && final.Status != (int)ScheduleStatus.ClaimSubmitted)
                {
                    final.ClaimDate = DateTime.Now;
                }
                var isStatusChange = final.Status == status;
                var oldStatus = final.Status;
                final.Status = status;
                if (final.Status == (int)ScheduleStatus.ClaimCreated)
                {
                    final.IsFinalInfoVerified = false;
                    final.IsSupplyVerified = false;
                    final.IsVisitVerified = false;
                    final.IsGenerated = false;
                }
                if (billingRepository.UpdateFinalStatus(final))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, !isStatusChange ? LogAction.FinalUpdatedWithStatus : LogAction.FinalUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(ScheduleStatus), oldStatus) ? ("From " + ((ScheduleStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ScheduleStatus), status) ? (" To " + ((ScheduleStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool IsEpisodeHasClaim(Guid episodeId, Guid patientId, string type)
        {
            var claimIds = billingRepository.GetClaimIds(Current.AgencyId, patientId, type);
            if (claimIds != null && claimIds.Count > 0)
            {
                return claimIds.Contains(episodeId);
            }
            return false;
        }

        public IList<ClaimSnapShotViewData> ClaimSnapShots(Guid Id, string type)
        {
            var claims = new List<ClaimSnapShotViewData>();
            if (type.IsEqual("Rap"))
            {
                var rapSnapShots = billingRepository.GetRapSnapShots(Current.AgencyId, Id);
                if (rapSnapShots != null && rapSnapShots.Count > 0)
                {
                    rapSnapShots.ForEach(rapSnapShot =>
               {
                   claims.Add(
                       new ClaimSnapShotViewData
                       {
                           Id = rapSnapShot.Id,
                           BatchId = rapSnapShot.BatchId,
                           PatientId = rapSnapShot.PatientId,
                           EpisodeId = rapSnapShot.EpisodeId,
                           FirstName = rapSnapShot.FirstName,
                           LastName = rapSnapShot.LastName,
                           MedicareNumber = rapSnapShot.MedicareNumber,
                           PaymentDate = rapSnapShot.PaymentDate,
                           PaymentAmount = rapSnapShot.Payment,
                           Type = "Rap",
                           Created = rapSnapShot.Created,
                           Status = rapSnapShot.Status,
                           EpisodeStartDate = rapSnapShot.EpisodeStartDate,
                           EpisodeEndDate = rapSnapShot.EpisodeEndDate,
                           ClaimDate = rapSnapShot.ClaimDate.IsValid() ? rapSnapShot.ClaimDate.ToString("MM/dd/yyyy hh:mm tt") : rapSnapShot.EpisodeStartDate.ToString("MM/dd/yyyy hh:mm tt"),
                           ClaimAmount = rapSnapShot.ProspectivePay
                       });
               });
                }
            }
            if (type.IsEqual("Final"))
            {
                var finalSnapShots = billingRepository.GetFinalSnapShots(Current.AgencyId, Id);
                if (finalSnapShots != null && finalSnapShots.Count > 0)
                {
                    finalSnapShots.ForEach(finalSnapShot =>
               {
                   claims.Add(
                       new ClaimSnapShotViewData
                       {
                           PatientId = finalSnapShot.PatientId,
                           BatchId = finalSnapShot.BatchId,
                           Id = finalSnapShot.Id,
                           EpisodeId = finalSnapShot.EpisodeId,
                           Type = "Final",
                           FirstName = finalSnapShot.FirstName,
                           LastName = finalSnapShot.LastName,
                           MedicareNumber = finalSnapShot.MedicareNumber,
                           PaymentDate = finalSnapShot.PaymentDate,
                           Created = finalSnapShot.Created,
                           Status = finalSnapShot.Status,
                           EpisodeStartDate = finalSnapShot.EpisodeStartDate,
                           EpisodeEndDate = finalSnapShot.EpisodeEndDate,
                           PaymentAmount = finalSnapShot.Payment,
                           ClaimDate = finalSnapShot.ClaimDate.IsValid() ? finalSnapShot.ClaimDate.ToString("MM/dd/yyyy hh:mm tt") : finalSnapShot.EpisodeEndDate.ToString("{MM/dd/yyyy hh:mm tt"),
                           ClaimAmount = finalSnapShot.ProspectivePay
                       });
               });
                }
            }
            return claims;
        }

        public bool UpdateSnapShot(Guid Id, long batchId, double paymentAmount, DateTime paymentDate, int status, string type)
        {
            bool result = false;
            if (type == "Rap" || type == "RAP")
            {
                var rapSnapShot = billingRepository.GetRapSnapShot(Current.AgencyId, Id, batchId);
                if (rapSnapShot != null)
                {
                    rapSnapShot.Payment = paymentAmount;
                    rapSnapShot.PaymentDate = paymentDate;
                    rapSnapShot.Status = status;
                    result = billingRepository.UpdateRapSnapShots(rapSnapShot);
                    if (result && batchId == billingRepository.GetLastRapSnapShotBatchId(Current.AgencyId, Id))
                    {
                        UpdateRapClaimStatus(Id, paymentDate, paymentAmount, status);
                    }
                }
            }
            else if (type == "Final" || type == "FINAL")
            {
                var finalSnapShot = billingRepository.GetFinalSnapShot(Current.AgencyId, Id, batchId);
                if (finalSnapShot != null)
                {
                    finalSnapShot.Payment = paymentAmount;
                    finalSnapShot.PaymentDate = paymentDate;
                    finalSnapShot.Status = status;
                    result = billingRepository.UpdateFinalSnapShots(finalSnapShot);
                    if (result && batchId == billingRepository.GetLastFinalSnapShotBatchId(Current.AgencyId, Id))
                    {
                        UpdateFinalClaimStatus(Id, paymentDate, paymentAmount, status);
                    }
                }
            }
            return result;
        }

        public ManagedClaim GetManagedClaimInfo(Guid patientId, Guid Id)
        {
            var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            if (managedClaim != null && patient != null)
            {
                if (managedClaim.Status == (int)ManagedClaimStatus.ClaimCreated)
                {
                    if (!managedClaim.IsInfoVerified)
                    {
                        var episodes = patientRepository.GetEpisodesBetween(Current.AgencyId, patientId, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
                        if (episodes != null && episodes.Count>0)
                        {
                            if (episodes.Count == 1)
                            {
                                var assessmentEvent = assessmentService.GetEpisodeAssessmentEvent(episodes[0].Id, patientId);
                                if (assessmentEvent != null)
                                {
                                    var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                                    var assessment = assessmentRepository.Get(assessmentEvent.EventId, patientId, assessmentEvent.EpisodeId, assessmentType, Current.AgencyId);
                                    if (assessment != null)
                                    {
                                        IDictionary<string, Question> assessmentQuestions = assessment.ToDictionary();
                                        string diagnosis = "<DiagonasisCodes>";
                                        diagnosis += "<code1>" + (assessmentQuestions.ContainsKey("M1020ICD9M") ? assessmentQuestions["M1020ICD9M"].Answer : string.Empty) + "</code1>";
                                        diagnosis += "<code2>" + (assessmentQuestions.ContainsKey("M1022ICD9M1") ? assessmentQuestions["M1022ICD9M1"].Answer : string.Empty) + "</code2>";
                                        diagnosis += "<code3>" + (assessmentQuestions.ContainsKey("M1022ICD9M2") ? assessmentQuestions["M1022ICD9M2"].Answer : string.Empty) + "</code3>";
                                        diagnosis += "<code4>" + (assessmentQuestions.ContainsKey("M1022ICD9M3") ? assessmentQuestions["M1022ICD9M3"].Answer : string.Empty) + "</code4>";
                                        diagnosis += "<code5>" + (assessmentQuestions.ContainsKey("M1022ICD9M4") ? assessmentQuestions["M1022ICD9M4"].Answer : string.Empty) + "</code5>";
                                        diagnosis += "<code6>" + (assessmentQuestions.ContainsKey("M1022ICD9M5") ? assessmentQuestions["M1022ICD9M5"].Answer : string.Empty) + "</code6>";
                                        diagnosis += "</DiagonasisCodes>";
                                        managedClaim.DiagnosisCode = diagnosis;
                                        managedClaim.HippsCode = assessment.HippsCode;
                                        managedClaim.ClaimKey = assessment.ClaimKey;
                                        managedClaim.AssessmentType = assessmentType;
                                        if (assessmentType.IsNotNullOrEmpty())
                                        {
                                            var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                            if (assessmentType == DisciplineTasks.OASISCStartofCare.ToString() || assessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || assessmentType== DisciplineTasks.OASISCStartofCareOT.ToString())
                                            {
                                                managedClaim.ProspectivePay = Math.Round(lookUpRepository.ProspectivePayAmount(assessment.HippsCode, episodes[0].StartDate.ToDateTime(), managedClaim.AddressZipCode.IsNotNullOrEmpty() ? managedClaim.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                            }
                                            else
                                            {
                                                managedClaim.ProspectivePay = Math.Round(lookUpRepository.ProspectivePayAmount(assessment.HippsCode, episodes[0].StartDate.ToDateTime(), managedClaim.AddressZipCode.IsNotNullOrEmpty() ? managedClaim.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                            }
                                        }
                                    }
                                }
                            }
                            else if (episodes.Count > 1)
                            {
                                managedClaim.HasMultipleEpisodes = true;
                            }
                        }
                        if (patient != null)
                        {
                            managedClaim.FirstName = patient.FirstName;
                            managedClaim.LastName = patient.LastName;
                            managedClaim.IsuranceIdNumber = patient.MedicareNumber;
                            managedClaim.PatientIdNumber = patient.PatientIdNumber;
                            managedClaim.Gender = patient.Gender;
                            managedClaim.DOB = patient.DOB;
                            managedClaim.PatientStatus = patient.Status;
                            managedClaim.UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty);
                            var managedData = patientRepository.GetPatientLatestAdmissionDate(Current.AgencyId, patient.Id, managedClaim.EpisodeStartDate);
                            if (managedData != null)
                            {
                                if (managedClaim.IsManagedClamDischage())
                                {
                                    if (managedData.DischargedDate.Date > DateTime.MinValue.Date)
                                    {
                                        managedClaim.DischargeDate = managedData.DischargedDate;
                                    }
                                    else
                                    {
                                        managedClaim.DischargeDate = patient.DischargeDate;
                                    }
                                }

                                if (managedData.StartOfCareDate > DateTime.MinValue)
                                {
                                    managedClaim.StartofCareDate = managedData.StartOfCareDate;
                                }
                                else
                                {
                                    managedClaim.StartofCareDate = patient.StartofCareDate;
                                }
                            }
                            else
                            {
                                if (managedClaim.IsManagedClamDischage())
                                {
                                    managedClaim.DischargeDate = patient.DischargeDate;
                                }
                                managedClaim.StartofCareDate = patient.StartofCareDate;
                            }
                            managedClaim.AddressLine1 = patient.AddressLine1;
                            managedClaim.AddressLine2 = patient.AddressLine2;
                            managedClaim.AddressCity = patient.AddressCity;
                            managedClaim.AddressStateCode = patient.AddressStateCode;
                            managedClaim.AddressZipCode = patient.AddressZipCode;
                            managedClaim.AdmissionSource = patient.AdmissionSource;
                            if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count >= 0)
                            {
                                var physician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                                if (physician != null)
                                {
                                    managedClaim.PhysicianLastName = physician.LastName;
                                    managedClaim.PhysicianFirstName = physician.FirstName;
                                    managedClaim.PhysicianNPI = physician.NPI;
                                }
                                else
                                {
                                    physician = patient.PhysicianContacts.FirstOrDefault();
                                    if (physician != null)
                                    {
                                        managedClaim.PhysicianLastName = physician.LastName;
                                        managedClaim.PhysicianFirstName = physician.FirstName;
                                        managedClaim.PhysicianNPI = physician.NPI;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return managedClaim;
        }

        public List<SelectListItem> GetManagedClaimEpisodes(Guid patientId, Guid managedClaimId)
        {
            var list = new List<SelectListItem>();
            var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, managedClaimId);
            if (managedClaim != null)
            {
                var episodes = patientRepository.GetEpisodesBetween(Current.AgencyId, patientId, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
                if (episodes != null)
                {
                    if (episodes.Count > 1)
                    {
                        episodes.ForEach(e =>
                        {
                            list.Add(new SelectListItem
                            {
                                Text = string.Format("{0} - {1}", e.StartDate, e.EndDate),
                                Value = string.Format("{0}_{1}", e.Id, e.PatientId)
                            });
                        });
                    }
                }
            }
            return list;
        }

        public ManagedClaimEpisodeData GetEpisodeAssessmentData(Guid episodeId, Guid patientId)
        {
            var managedClaim = new ManagedClaimEpisodeData();
          
            var assessmentEvent = assessmentService.GetEpisodeAssessmentEvent(episodeId, patientId);
            if (assessmentEvent != null)
            {
                var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                var assessment = assessmentRepository.Get(assessmentEvent.EventId, patientId, assessmentEvent.EpisodeId, assessmentType, Current.AgencyId);
                if (assessment != null)
                {
                    var assessmentQuestions = assessment.ToDictionary();
                    managedClaim.DiagnosisCode1 = assessmentQuestions.ContainsKey("M1020ICD9M") ? assessmentQuestions["M1020ICD9M"].Answer : string.Empty;
                    managedClaim.DiagnosisCode2 = assessmentQuestions.ContainsKey("M1022ICD9M1") ? assessmentQuestions["M1022ICD9M1"].Answer : string.Empty;
                    managedClaim.DiagnosisCode3 = assessmentQuestions.ContainsKey("M1022ICD9M2") ? assessmentQuestions["M1022ICD9M2"].Answer : string.Empty;
                    managedClaim.DiagnosisCode4 = assessmentQuestions.ContainsKey("M1022ICD9M3") ? assessmentQuestions["M1022ICD9M3"].Answer : string.Empty;
                    managedClaim.DiagnosisCode5 = assessmentQuestions.ContainsKey("M1022ICD9M4") ? assessmentQuestions["M1022ICD9M4"].Answer : string.Empty;
                    managedClaim.DiagnosisCode6 = assessmentQuestions.ContainsKey("M1022ICD9M5") ? assessmentQuestions["M1022ICD9M5"].Answer : string.Empty;
                    managedClaim.HippsCode = assessment.HippsCode;
                    managedClaim.ClaimKey = assessment.ClaimKey;
                    managedClaim.AssessmentType = assessmentType;
                    if (assessmentType.IsNotNullOrEmpty())
                    {
                        var patient = patientRepository.Get(patientId, Current.AgencyId);
                        var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                        if (assessmentType == DisciplineTasks.OASISCStartofCare.ToString() || assessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || assessmentType==DisciplineTasks.OASISCStartofCareOT.ToString())
                        {
                            managedClaim.ProspectivePay = Math.Round(lookUpRepository.ProspectivePayAmount(assessment.HippsCode, assessmentEvent.StartDate, patient != null && patient.AddressZipCode.IsNotNullOrEmpty() ? patient.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                        }
                        else
                        {
                            managedClaim.ProspectivePay = Math.Round(lookUpRepository.ProspectivePayAmount(assessment.HippsCode, assessmentEvent.StartDate, patient != null && patient.AddressZipCode.IsNotNullOrEmpty() ? patient.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                        }
                    }
                }
            }
            return managedClaim;
        }

        public bool ManagedVisitVerify(Guid Id, Guid patientId, List<Guid> visit)
        {
            bool result = false;
            
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                if (managedClaim != null)
                {
                    var scheduleEvents = patientRepository.GetScheduledEventsOnly(Current.AgencyId, patientId, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
                    if (scheduleEvents != null && scheduleEvents.Count > 0 && visit != null && visit.Count > 0)
                    {
                        var visitList = new List<ScheduleEvent>();
                        var managedClaimVisit = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();
                        visit.ForEach(v =>
                        {
                            var scheduleVisit = scheduleEvents.FirstOrDefault(s => s.EventId == v);
                            if (scheduleVisit != null)
                            {
                                scheduleVisit.IsBillable = true;
                                visitList.Add(scheduleVisit);
                            }
                        });
                        if (managedClaimVisit != null && managedClaimVisit.Count > 0)
                        {
                            managedClaimVisit.ForEach(f =>
                            {
                                if (scheduleEvents.Exists(e => e.EventId == f.EventId) && !visit.Contains(f.EventId))
                                {
                                    scheduleEvents.FirstOrDefault(e => e.EventId == f.EventId).IsBillable = false;
                                }
                            });
                        }
                        managedClaim.IsVisitVerified = true;
                        managedClaim.VerifiedVisits = visitList.ToXml();
                        managedClaim.Modified = DateTime.Now;
                        if (patientRepository.UpdateScheduleEventsForIsBillable(Current.AgencyId, scheduleEvents))
                        {
                            result = billingRepository.UpdateManagedClaimForVisitVerify(managedClaim);
                        }
                    }
                    else
                    {
                        managedClaim.IsVisitVerified = true;
                        managedClaim.VerifiedVisits = new List<ScheduleEvent>().ToXml();
                        managedClaim.Modified = DateTime.Now;
                        result = billingRepository.UpdateManagedClaimForVisitVerify(managedClaim);
                    }
                }
            }
            return result;
        }

        public bool ManagedVisitSupply(Guid Id, Guid patientId, FormCollection formCollection, List<Guid> supplyId)
        {
            bool result = false;
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var keys = formCollection.AllKeys;
                var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                if (claim != null)
                {
                    var visits = new List<ScheduleEvent>();
                    var episodeSupplyList = new List<Supply>();
                    if (claim.VerifiedVisits.IsNotNullOrEmpty())
                    {
                        visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>();
                        if (visits != null && visits.Count > 0)
                        {
                            visits.ForEach(v =>
                            {
                                episodeSupplyList.AddRange(this.GetSupply(v));
                            });
                        }
                    }
                    var tempSupplyList = new List<Supply>();
                    var existingSupplyList = claim.Supply.IsNotNullOrEmpty() ? claim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                    if (supplyId != null && supplyId.Count > 0)
                    {
                        supplyId.ForEach(v =>
                        {
                            if (existingSupplyList.Exists(s => s.UniqueIdentifier == v))
                            {
                                var currentSupply = existingSupplyList.FirstOrDefault(s => s.UniqueIdentifier == v);
                                if (currentSupply != null)
                                {
                                    currentSupply.IsBillable = true;
                                    currentSupply.UnitCost = keys.Contains("UnitCost" + v) && formCollection["UnitCost" + v].IsNotNullOrEmpty() ? formCollection["UnitCost" + v].ToDouble() : 0;
                                    currentSupply.Total = keys.Contains("Total" + v) && formCollection["Total" + v].IsNotNullOrEmpty() ? formCollection["Total" + v].ToDouble() : 0;
                                    tempSupplyList.Add(currentSupply);
                                }
                            }
                            else if (episodeSupplyList.Exists(s => s.UniqueIdentifier == v))
                            {
                                var currentSupply = episodeSupplyList.FirstOrDefault(s => s.UniqueIdentifier == v);
                                if (currentSupply != null)
                                {
                                    currentSupply.IsBillable = true;
                                    currentSupply.UnitCost = keys.Contains("UnitCost" + v) && formCollection["UnitCost" + v].IsNotNullOrEmpty() ? formCollection["UnitCost" + v].ToDouble() : 0;
                                    currentSupply.Total = keys.Contains("Total" + v) && formCollection["Total" + v].IsNotNullOrEmpty() ? formCollection["Total" + v].ToDouble() : 0;
                                    tempSupplyList.Add(currentSupply);
                                }
                            }
                        });
                    }
                    claim.SupplyTotal = keys.Contains("SupplyTotal") && formCollection["SupplyTotal"].IsNotNullOrEmpty() && formCollection["SupplyTotal"].IsDouble() ? formCollection["SupplyTotal"].ToDouble() : claim.SupplyTotal;
                    claim.SupplyCode = keys.Contains("SupplyCode") ? formCollection["SupplyCode"] : claim.SupplyCode;
                    claim.Supply = tempSupplyList.ToXml();
                    claim.Modified = DateTime.Now;
                    claim.IsSupplyVerified = true;
                    result = billingRepository.UpdateManagedClaimForSupplyVerify(claim);
                }
            }
            return result;
        }

        public UBOFourViewData GetManagedUBOFourInfo(Guid patientId, Guid claimId)
        {
            var uboFourViewData = new UBOFourViewData();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                uboFourViewData.Agency = agency;
                uboFourViewData.AgencyLocation = agency.MainLocation;

                var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, claimId);
                if (managedClaim != null)
                {
                    var patient = patientRepository.GetPatientOnly(managedClaim.PatientId, Current.AgencyId);
                    AgencyLocation branch = null;
                    if (patient != null)
                    {
                        branch = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId);
                    }
                    var claimData = new ClaimViewData
                    {
                        Status = patient != null && patient.Status == (int)PatientStatus.Discharged ? 1 : 30,
                        PatientIdNumber = managedClaim.PatientIdNumber,
                        Type = managedClaim.Type,
                        EpisodeStartDate = managedClaim.EpisodeStartDate,
                        EpisodeEndDate = managedClaim.EpisodeEndDate,
                        FirstName = managedClaim.FirstName,
                        LastName = managedClaim.LastName,
                        AddressLine1 = managedClaim.AddressLine1,
                        AddressLine2 = managedClaim.AddressLine2,
                        AddressCity = managedClaim.AddressCity,
                        AddressStateCode = managedClaim.AddressStateCode,
                        AddressZipCode = managedClaim.AddressZipCode,
                        DOB = managedClaim.DOB,
                        Gender = managedClaim.Gender,
                        AdmissionSource = managedClaim.AdmissionSource.IsNotNullOrEmpty() && managedClaim.AdmissionSource != "0" ? lookUpRepository.GetAdmissionSourceCode(int.Parse(managedClaim.AdmissionSource)) : "9",
                        CBSA = lookUpRepository.CbsaCodeByZip( managedClaim.AddressZipCode),
                        HippsCode = managedClaim.HippsCode,
                        PrimaryInsuranceId = managedClaim.PrimaryInsuranceId,
                        ClaimKey = managedClaim.ClaimKey,
                        DiagnosisCode = managedClaim.DiagnosisCode,
                        PhysicianFirstName = managedClaim.PhysicianFirstName,
                        PhysicianLastName = managedClaim.PhysicianLastName,
                        PhysicianNPI = managedClaim.PhysicianNPI,
                        VerifiedVisit = managedClaim.VerifiedVisits,
                        StartofCareDate = managedClaim.StartofCareDate,
                        MedicareNumber = managedClaim.IsuranceIdNumber,
                        SupplyTotal = managedClaim.SupplyTotal,
                        FirstBillableVisitDate = managedClaim.FirstBillableVisitDate,
                        HealthPlanId = managedClaim.HealthPlanId,
                        PayorAuthorizationCode = managedClaim.AuthorizationNumber,
                        ConditionCodes = managedClaim.ConditionCodes,
                        SupplyCode=managedClaim.SupplyCode
                    };
                    uboFourViewData.Claim = claimData;
                }
                if (uboFourViewData.Claim != null)
                {
                    if (uboFourViewData.Claim.PrimaryInsuranceId >= 1000)
                    {
                        var insurance = agencyRepository.GetInsurance(uboFourViewData.Claim.PrimaryInsuranceId, Current.AgencyId);
                        if (insurance != null)
                        {
                            uboFourViewData.Claim.IsHMO = true;
                            if (insurance.PayorId.IsNotNullOrEmpty() && !(insurance.PayorId == "0"))
                            {
                                uboFourViewData.Claim.PayerId = insurance.PayorId;
                            }
                            uboFourViewData.Claim.PayorName = insurance.Name;
                            uboFourViewData.Claim.PayorAddressLine1 = insurance.AddressFirstRow;
                            uboFourViewData.Claim.PayorAddressLine2 = insurance.AddressSecondRow;
                            uboFourViewData.Claim.OtherProviderId = insurance.OtherProviderId;
                            uboFourViewData.Claim.ProviderId = insurance.ProviderId;
                            uboFourViewData.Claim.ProviderSubscriberId = insurance.ProviderSubscriberId;
                        }
                    }
                    else if (uboFourViewData.Claim.PrimaryInsuranceId < 1000 && uboFourViewData.Claim.PrimaryInsuranceId > 0)
                    {
                        uboFourViewData.Claim.IsHMO = false;
                        var insurance = lookUpRepository.GetInsurance(uboFourViewData.Claim.PrimaryInsuranceId);
                        if (insurance != null)
                        {
                            uboFourViewData.Claim.PayorName = insurance.Name;
                        }
                    }
                }
            }
            return uboFourViewData;
        }

        public bool UpdateProccesedManagedClaimStatus(Guid patientId, Guid Id, DateTime claimDate, double paymentAmount, DateTime paymentDate, int status, string comment)
        {
            bool result = false;
            var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
            if (managedClaim != null)
            {
                managedClaim.Payment = paymentAmount;
                managedClaim.PaymentDate = paymentDate;
                managedClaim.Comment = comment;
                if (status == (int)ManagedClaimStatus.ClaimReOpen || status == (int)ManagedClaimStatus.ClaimCreated)
                {
                    managedClaim.IsInfoVerified = false;
                    managedClaim.IsSupplyVerified = false;
                    managedClaim.IsVisitVerified = false;
                    managedClaim.IsGenerated = false;
                }
                if (status == (int)ManagedClaimStatus.ClaimSubmitted && managedClaim.Status != (int)ManagedClaimStatus.ClaimSubmitted)
                {
                    managedClaim.ClaimDate = DateTime.Now;
                }
                var oldStatus = managedClaim.Status;
                var isStatusChange = managedClaim.Status == status;
                managedClaim.Status = status;
                if (billingRepository.UpdateManagedClaim(managedClaim))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.Rap, !isStatusChange ? LogAction.ManagedUpdatedWithStatus : LogAction.ManagedUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(ScheduleStatus), oldStatus) ? ("From " + ((ScheduleStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ScheduleStatus), status) ? (" To " + ((ScheduleStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public ManagedClaimSnapShotViewData GetManagedClaimSnapShotInfo(Guid patientId, Guid claimId)
        {
            var claimInfo = new ManagedClaimSnapShotViewData();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, claimId);
                if (managedClaim != null)
                {
                    claimInfo.Id = managedClaim.Id;
                    claimInfo.PatientId = patient.Id;
                    if (managedClaim.IsInfoVerified)
                    {
                        claimInfo.PatientName = string.Format("{0} {1}", managedClaim.FirstName, managedClaim.LastName);
                        claimInfo.PatientIdNumber = managedClaim.PatientIdNumber;
                        claimInfo.IsuranceIdNumber = managedClaim.IsuranceIdNumber;
                    }
                    else
                    {
                        claimInfo.PatientName = string.Format("{0} {1}", patient.FirstName, patient.LastName);
                        claimInfo.PatientIdNumber = patient.PatientIdNumber;
                        claimInfo.IsuranceIdNumber = patient.MedicareNumber;
                    }
                    claimInfo.AuthorizationNumber = managedClaim.AuthorizationNumber;
                    claimInfo.HealthPlainId = managedClaim.HealthPlanId;
                    var hhrg = lookUpRepository.GetHHRGByHIPPSCODE(managedClaim.HippsCode);
                    if (hhrg != null)
                    {
                        claimInfo.HHRG = hhrg.HHRG;
                    }
                    claimInfo.HIPPS = managedClaim.HippsCode;
                    claimInfo.ClaimKey = managedClaim.ClaimKey;

                    if (managedClaim.HippsCode.IsNotNullOrEmpty())
                    {
                        var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                        PPSStandard ppsStandard;
                        claimInfo.SupplyReimbursement = GetSupplyReimbursement(managedClaim.HippsCode[managedClaim.HippsCode.Length - 1], managedClaim.EpisodeStartDate.Year, out ppsStandard);
                        claimInfo.StandardEpisodeRate = lookUpRepository.ProspectivePayAmount(managedClaim.HippsCode, managedClaim.EpisodeStartDate, managedClaim.AddressZipCode.IsNotNullOrEmpty() ? managedClaim.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                    }
                    claimInfo.ProspectivePay = claimInfo.StandardEpisodeRate;
                    if (!(managedClaim.PrimaryInsuranceId <= 0))
                    {
                        if (managedClaim.PrimaryInsuranceId >= 1000)
                        {
                            var insurance = agencyRepository.GetInsurance(managedClaim.PrimaryInsuranceId, Current.AgencyId);
                            if (insurance != null)
                            {
                                claimInfo.PayorName = insurance.Name;
                            }
                        }
                        else
                        {
                            claimInfo.PayorName = string.Empty;
                        }
                    }
                    claimInfo.Visible = true;
                }
            }
            return claimInfo;
        }

        public bool ManagedComplete(Guid Id, Guid patientId)
        {
            var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
            bool result = false;
            if (managedClaim != null)
            {
                if (billingRepository.UpdateManagedClaim(managedClaim))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedSummaryVerified, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public Bill GetClaimsPrint(Guid branchId, int insuranceId)
        {
            var agency = agencyRepository.GetWithBranches(Current.AgencyId);
            Bill claims = null;
            if (agency != null)
            {
                //var agencyMainBranch = agencyRepository.GetMainLocation(Current.AgencyId);
                //if (agencyMainBranch != null && !agencyMainBranch.Id.IsEmpty())
                //{
                //    int payorType;
                //    if (agency.Payor.IsNotNullOrEmpty() && int.TryParse(agency.Payor, out payorType))
                //    {
                //        claims = AllUnProcessedBill(agencyMainBranch.Id, payorType);
                //    }
                //    else
                //    {
                //        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
                //        if (agencyMedicareInsurance != null)
                //        {
                            claims = AllUnProcessedBill(branchId, insuranceId);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        var agencyMedicareInsurance = agencyRepository.GetInsurances(Current.AgencyId).Where(i => i.PayorType == 3 || i.PayorType == 2).OrderBy(i => i.Id).ToList().FirstOrDefault();
            //        if (agencyMedicareInsurance != null)
            //        {
            //            claims = AllUnProcessedBill(agencyMainBranch.Id, agencyMedicareInsurance.Id);
            //        }
            //    }
                            if (claims != null)
                            {
                                claims.Insurance = insuranceId;
                                claims.BranchId = branchId;
                                claims.Agency = agency;
                            }
            }
           
            return claims;
        }

        public Final GetFinalPrint(Guid episodeId, Guid patientId)
        {
            var final = GetFinalInfo(patientId, episodeId);
            final.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var unitType = -1;
            final.BillInformations = GetInsuranceUnit(final.PrimaryInsuranceId, out unitType);
            return final;
        }

        public Rap GetRapPrint(Guid episodeId, Guid patientId)
        {
            var rap = GetRap(patientId, episodeId);
            rap.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            int source;
            if (int.TryParse(rap.AdmissionSource, out source))
            {
                var admissionSource = lookUpRepository.GetAdmissionSource(source);
                if (admissionSource != null) rap.AdmissionSourceDisplay = admissionSource.Description;
            }
            return rap;
        }

        public string GenerateJsonForManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, Agency agency, AgencyInsurance payerInfo, bool isHMO, int insuranceId, out List<ManagedClaim> managedClaims , AgencyLocation branch)
        {
            string requestArr = string.Empty;
            claimInfo = new List<ClaimInfo>();
            var unitType = -1;
            var visitRateWithUnit = GetInsuranceUnit(insuranceId, out unitType);
            var managedClaimLists = billingRepository.GetManagedClaimsToGenerateByIds(Current.AgencyId, managedClaimToGenerate);
            var agencyLocation = agencyRepository.GetMainLocation(agency.Id);
            managedClaims = managedClaimLists;
            if (agencyLocation != null)
            {
                var patients = new List<object>();
                if (managedClaimLists != null && managedClaimLists.Count > 0)
                {
                    foreach (var managedClaim in managedClaimLists)
                    {
                        var claims = new List<object>();
                        var diagnosis = managedClaim.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(managedClaim.DiagnosisCode) : null;
                        var conditionCodes = managedClaim.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(managedClaim.ConditionCodes) : null;
                        var visitTotalAmount = 0.00;
                        var visits = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(f => f.VisitDate.IsValidDate()).OrderBy(f => f.VisitDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();
                        var visitList = new List<object>();
                        if (visits!=null && visits.Count > 0)
                        {
                            visits.ForEach(v =>
                            {
                                var discipline = v.GIdentify();
                                var amount = visitRateWithUnit.ContainsKey(discipline) && visitRateWithUnit[discipline].Amount.IsNotNullOrEmpty() && visitRateWithUnit[discipline].Amount.IsDouble() ? visitRateWithUnit[discipline].Amount.ToDouble() : 0.00;
                                visitTotalAmount += amount;
                                var unit = (unitType == 1 ? 1 : (unitType == 2 ? (int)Math.Ceiling((double)v.MinSpent / 60) : (unitType == 3 ? (int)Math.Ceiling((double)v.MinSpent / 15) : 0)));
                                visitList.Add(new { date = v.VisitDate, type = discipline, units = unit, amount = amount * unit });
                            });
                        }
                        claimInfo.Add(new ClaimInfo { ClaimId = managedClaim.Id, PatientId = managedClaim.PatientId, EpisodeId = managedClaim.EpisodeId, ClaimType = managedClaim.Type });
                        var finalObj = new
                        {
                            claim_id = managedClaim.Id,
                            claim_type = managedClaim.Type,
                            claim_physician_upin = managedClaim.PhysicianNPI,
                            claim_physician_last_name = managedClaim.PhysicianLastName,
                            claim_physician_first_name = managedClaim.PhysicianFirstName,
                            claim_first_visit_date = managedClaim.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
                            claim_episode_start_date = managedClaim.EpisodeStartDate.ToString("MM/dd/yyyy"),
                            claim_episode_end_date = managedClaim.EpisodeEndDate.ToString("MM/dd/yyyy"),
                            claim_hipps_code = managedClaim.HippsCode,
                            claim_oasis_key = managedClaim.ClaimKey,
                            claim_hmo_auth_key = managedClaim.AuthorizationNumber,
                            claim_diagnosis_code1 = (diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : ""),
                            claim_diagnosis_code2 = (diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : ""),
                            claim_diagnosis_code3 = (diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : ""),
                            claim_diagnosis_code4 = (diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : ""),
                            claim_diagnosis_code5 = (diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : ""),
                            claim_diagnosis_code6 = (diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : ""),
                            claim_condition_code18 = (conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : ""),
                            claim_condition_code19 = (conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : ""),
                            claim_condition_code20 = (conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : ""),
                            claim_condition_code21 = (conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : ""),
                            claim_condition_code22 = (conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : ""),
                            claim_condition_code23 = (conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : ""),
                            claim_condition_code24 = (conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : ""),
                            claim_condition_code25 = (conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : ""),
                            claim_condition_code26 = (conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : ""),
                            claim_condition_code27 = (conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : ""),
                            claim_condition_code28 = (conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : ""),
                            claim_total_charge_amount = Math.Round(visitTotalAmount, 2),
                            claim_admission_source_code = managedClaim.AdmissionSource.IsNotNullOrEmpty() ? lookUpRepository.GetAdmissionSourceCode(int.Parse(managedClaim.AdmissionSource)) : string.Empty,
                            claim_patient_status_code = managedClaim.UB4PatientStatus,
                            claim_supply_value = managedClaim.SupplyTotal,
                            claim_visits = visitList
                        };
                        claims.Add(finalObj);
                        var patient = new
                        {
                            patient_gender = managedClaim.Gender.Substring(0, 1),
                            patient_record_num = managedClaim.PatientIdNumber,
                            patient_dob = managedClaim.DOB.ToString("MM/dd/yyyy"),
                            patient_doa = managedClaim.StartofCareDate.ToString("MM/dd/yyyy"),
                            patient_dod = managedClaim.IsManagedClamDischage() && managedClaim.DischargeDate.Date>DateTime.MinValue.Date?managedClaim.DischargeDate.ToString("MM/dd/yyyy"):string.Empty,
                            patient_address = managedClaim.AddressLine1,
                            patient_address2 = managedClaim.AddressLine2,
                            patient_city = managedClaim.AddressCity,
                            patient_state = managedClaim.AddressStateCode,
                            patient_zip = managedClaim.AddressZipCode,
                            patient_cbsa = lookUpRepository.CbsaCodeByZip(managedClaim.AddressZipCode),
                            patient_last_name = managedClaim.LastName,
                            patient_first_name = managedClaim.FirstName,
                            patient_middle_initial = "",
                            hmo_plan_id = managedClaim.HealthPlanId,
                            claims_arr = claims
                        };
                        patients.Add(patient);
                    }
                }
                var visitRates = new List<object>();
                visitRateWithUnit.ForEach(vr =>
                {
                    var key = vr.Key;
                    var value = vr.Value;
                    var visitRate = GetVisitRateInstance(key, value.CodeOne, value.CodeTwo, value.Amount, value.Unit);
                    if (visitRate != null)
                    {
                        visitRates.Add(visitRate);
                    }
                });
                var additionaCodes = payerInfo.ToLocatorDictionary().Values.Select(l => new { code1 = l.Code1, code2 = l.Code2, code3 = l.Code3 });
                var agencyClaim = new
                {
                    format = "ansi837",
                    submit_type = commandType.ToString(),
                    user_login_name = Current.User.Name,
                    hmo_payer_id = payerInfo.PayorId,
                    hmo_payer_name = payerInfo.Name,
                    hmo_submitter_id = payerInfo.SubmitterId,
                    hmo_provider_id = payerInfo.ProviderId,
                    hmo_other_provider_id = payerInfo.OtherProviderId,
                    hmo_provider_subscriber_id = payerInfo.ProviderSubscriberId,
                    hmo_additional_codes = additionaCodes,
                    // payer_id = payerInfo.Code,
                    payer_name = payerInfo.Name,
                    insurance_is_axxess_biller = payerInfo.IsAxxessTheBiller,
                    clearing_house_id = payerInfo.ClearingHouseSubmitterId,
                    provider_claim_type = payerInfo.BillType,
                    interchange_receiver_id = payerInfo.InterchangeReceiverId,
                    clearing_house = payerInfo.ClearingHouse,
                    claim_billtype = ClaimType.MAN.ToString(),
                    submitter_name = payerInfo.SubmitterName,
                    submitter_phone = payerInfo.SubmitterPhone,
                    // submitter_fax = payerInfo.Fax,
                    user_agency_name = agency.Name,
                    user_tax_id = agency.TaxId,
                    user_national_provider_id = agency.NationalProviderNumber,
                    user_address_1 = agencyLocation.AddressLine1,
                    user_address_2 = agencyLocation.AddressLine2,
                    user_city = agencyLocation.AddressCity,
                    user_state = agencyLocation.AddressStateCode,
                    user_zip = agencyLocation.AddressZipCode,
                    user_phone = agencyLocation.PhoneWork,
                    user_fax = agencyLocation.FaxNumber,
                    user_CBSA_code = lookUpRepository.CbsaCodeByZip(agencyLocation.AddressZipCode),
                    ansi_837_id = claimId,
                    visit_rates = visitRates,
                    patients_arr = patients
                };
                var jss = new JavaScriptSerializer();
                requestArr = jss.Serialize(agencyClaim);
            }
            return requestArr;
        }

        public bool GenerateManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId)
        {
            var claimData = new ClaimData { AgencyId = Current.AgencyId };
            var claimInfo = new List<ClaimInfo>();
            bool result = false;
            claimDataOut = null;
            billExchange = new BillExchange { isSuccessful = false, Message = "There is a problem creating the claim. Try Again." };
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                if (insuranceId >= 1000)
                {
                    var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
                    if (insurance != null)
                    {
                        var isHMO = insurance.PayorType == 2;
                        claimData.ClaimType = ClaimType.MAN.ToString();
                        var claimId = GetNextClaimId(claimData);
                        claimData.Id = claimId;
                        List<ManagedClaim> managedClaims = null;
                        var branch = agencyRepository.FindLocation(Current.AgencyId, branchId);
                        if (branch != null)
                        {
                            var requestArr = GenerateJsonForManaged(managedClaimToGenerate, commandType, claimId, out claimInfo, agency, insurance, isHMO, insuranceId, out managedClaims , branch);
                            if (requestArr.IsNotNullOrEmpty())
                            {
                                requestArr = requestArr.Replace("&", "U+0026");
                                billExchange = GenerateANSI(requestArr);
                                if (billExchange != null && billExchange.isSuccessful && billExchange.Status == "OK")
                                {
                                    if (billExchange.Result.IsNotNullOrEmpty())
                                    {
                                        claimData.Data = billExchange.Result;
                                        claimData.ClaimType = ClaimType.MAN.ToString();
                                        claimData.BillIdentifers = claimInfo.ToXml<List<ClaimInfo>>();
                                        billingRepository.UpdateClaimData(claimData);
                                        if (commandType == ClaimCommandType.direct)
                                        {
                                            if (managedClaims != null && managedClaims.Count > 0)
                                            {
                                                billingRepository.MarkManagedClaimsAsSubmitted(Current.AgencyId, managedClaims);
                                                managedClaims.ForEach(claim =>
                                                {
                                                    Auditor.AddGeneralLog(LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedSubmittedElectronically, string.Empty);
                                                });
                                            }
                                        }
                                        claimDataOut = claimData;
                                        result = true;
                                    }
                                    else
                                    {
                                        billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                                        claimDataOut = null;
                                        result = false;
                                    }
                                }
                                else
                                {
                                    billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                                    result = false;
                                }
                            }
                            else
                            {
                                billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                                billExchange.Message = "System problem. Try again.";
                                result = false;
                            }
                        }
                        else
                        {
                            billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                            billExchange.Message = "Main/Branch information is not available.";
                            result = false;
                        }
                    }
                    else
                    {
                        billExchange.Message = "Insurance/Payer information is not right.";
                        return false;
                    }
                }
                else
                {
                    billExchange.Message = "Insurance/Payer information is not right.";
                }
            }
            else
            {
                billExchange.Message = "Claim Information is not correct. Try again.";
            }
            return result;
        }

        public ManagedBillViewData ManagedBill(Guid branchId, int insuranceId, int status)
        {
            var manageBill = new ManagedBillViewData();
            manageBill.Bills = billingRepository.GetManagedClaims(Current.AgencyId, branchId, insuranceId, status);
            manageBill.BranchId = branchId;
            manageBill.Insurance = insuranceId;
            var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
            if (insurance != null)
            {
                manageBill.IsElectronicSubmssion = insurance.IsAxxessTheBiller;
            }
            return manageBill;
        }

        public ManagedBillViewData ManagedClaimToGenerate(List<Guid> managedClaimToGenerate, Guid branchId, int primaryInsurance)
        {
            var isElectronicSubmssion = false;
            var insurance = agencyRepository.GetInsurance(primaryInsurance, Current.AgencyId);
            if (insurance != null)
            {
                isElectronicSubmssion = insurance.IsAxxessTheBiller;
            }
            return new ManagedBillViewData { BranchId = branchId, Insurance = primaryInsurance, Bills = billingRepository.GetManagedClaimByIds(Current.AgencyId, branchId, primaryInsurance, managedClaimToGenerate), IsElectronicSubmssion = isElectronicSubmssion };
        }

        public bool UpdateManagedClaimStatus(List<Guid> managedClaimToGenerate, Guid agencyId, string statusType)
        {
            bool result = false;
            if (managedClaimToGenerate != null)
            {
                managedClaimToGenerate.ForEach(id =>
                {
                    var managedClaim = billingRepository.GetManagedClaim(agencyId, id);
                    if (managedClaim != null)
                    {
                        var oldStatus = managedClaim.Status;
                        if (statusType == "Submit")
                        {
                            if (managedClaim.Status != (int)ManagedClaimStatus.ClaimSubmitted)
                            {
                                managedClaim.Status = (int)ManagedClaimStatus.ClaimSubmitted;
                                managedClaim.ClaimDate = DateTime.Now;
                            }
                            managedClaim.IsGenerated = true;
                            managedClaim.IsInfoVerified = true;
                            managedClaim.IsVisitVerified = true;
                            managedClaim.IsSupplyVerified = true;
                        }
                        else if (statusType == "Cancelled")
                        {
                            managedClaim.Status = (int)ManagedClaimStatus.ClaimCancelledClaim;
                        }
                        else if (statusType == "Rejected")
                        {
                            managedClaim.Status = (int)ManagedClaimStatus.ClaimRejected;
                        }
                        else if (statusType == "Accepted")
                        {
                            managedClaim.Status = (int)ManagedClaimStatus.ClaimAccepted;
                        }
                        else if (statusType == "PaymentPending")
                        {
                            managedClaim.Status = (int)ManagedClaimStatus.ClaimPaymentPending;
                        }
                        else if (statusType == "Error")
                        {
                            managedClaim.Status = (int)ManagedClaimStatus.ClaimWithErrors;
                        }
                        else if (statusType == "Paid")
                        {
                            managedClaim.Status = (int)ManagedClaimStatus.ClaimPaidClaim;
                        }
                        else if (statusType == "ReOpen")
                        {
                            managedClaim.Status = (int)ManagedClaimStatus.ClaimReOpen;
                            managedClaim.ClaimDate = DateTime.MinValue;
                            managedClaim.IsInfoVerified = false;
                            managedClaim.IsSupplyVerified = false;
                            managedClaim.IsVisitVerified = false;
                            managedClaim.IsGenerated = false;
                        }
                        if (billingRepository.UpdateManagedClaim(managedClaim))
                        {
                            var isStatusChange = oldStatus == managedClaim.Status;
                            if (!isStatusChange)
                            {
                                Auditor.AddGeneralLog(LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedUpdatedWithStatus, ((Enum.IsDefined(typeof(ManagedClaimStatus), oldStatus) ? ("From " + ((ManagedClaimStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ScheduleStatus), managedClaim.Status) ? (" To " + ((ScheduleStatus)managedClaim.Status).GetDescription()) : string.Empty)));
                            }
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool AddRemittanceUpload(HttpPostedFileBase file)
        {
            var result = false;
            var streamReader = new StreamReader(file.InputStream);
            var remit = new RemitQueue
            {
                Id = Guid.NewGuid(),
                AgencyId = Current.AgencyId,
                Data = streamReader.ReadToEnd(),
                IsUpload = true,
                Status = RemitQueueStatus.queued.ToString()
            };
            if (billingRepository.AddRemitQueue(remit))
            {
                try
                {
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(AppSettings.RemittanceSchedulerUrl);
                    request.Method = "GET";
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Stream receiveStream = response.GetResponseStream();
                        Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                        StreamReader readStream = new StreamReader(receiveStream, encode);
                        var strResult = readStream.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {
                    return true;
                }
                result = true;
            }
            return result;
        }

        public List<ClaimInfoDetail> GetSubmittedBatchClaims(int batchId)
        {
            var claimInfos = new List<ClaimInfoDetail>();
            var claimData = billingRepository.GetClaimData(Current.AgencyId, batchId);
            if (claimData != null && claimData.ClaimType.IsNotNullOrEmpty() && claimData.BillIdentifers.IsNotNullOrEmpty())
            {
                var claims = claimData.BillIdentifers.ToObject<List<ClaimInfo>>();
                if (claims != null && claims.Count > 0)
                {
                    if (claimData.ClaimType.ToUpperCase() == ClaimType.MAN.ToString())
                    {
                        var managedClaims = billingRepository.GetManagedClaimInfoDetails(Current.AgencyId, claims.Select(c => c.ClaimId).ToList());
                        if (managedClaims != null && managedClaims.Count > 0)
                        {
                            claimInfos.AddRange(managedClaims);
                        }
                    }
                    else if (claimData.ClaimType.ToUpperCase() == ClaimType.CMS.ToString() || claimData.ClaimType.ToUpperCase() == ClaimType.HMO.ToString())
                    {
                        var raps = claims.Where(c => c.ClaimType == "322" || c.ClaimType.ToUpperCase() == "RAP").ToList();
                        if (raps != null && raps.Count > 0)
                        {
                            var medicareRapClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, raps.Select(c => c.ClaimId).ToList(), "RAP");
                            if (medicareRapClaims != null && medicareRapClaims.Count > 0)
                            {
                                claimInfos.AddRange(medicareRapClaims);
                            }
                        }
                        var finals = claims.Where(c => c.ClaimType == "329" || c.ClaimType.ToUpperCase() == "FINAL").ToList();
                        if (finals != null && finals.Count > 0)
                        {
                            var medicareFinalClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, finals.Select(c => c.ClaimId).ToList(), "Final");
                            if (medicareFinalClaims != null && medicareFinalClaims.Count > 0)
                            {
                                claimInfos.AddRange(medicareFinalClaims);
                            }
                        }
                    }
                }
            }
            return claimInfos;
        }

        public IList<ClaimInfoDetail> BillingBatch(string claimType, DateTime batchDate)
        {
            var claimInfos = new List<ClaimInfoDetail>();
            var batchList = billingRepository.GetClaimData(Current.AgencyId, claimType, batchDate);
            if (batchList != null && batchList.Count > 0)
            {
                batchList.ForEach(batch =>
                {
                    if (batch != null && batch.BillIdentifers.IsNotNullOrEmpty())
                    {
                        var claims = batch.BillIdentifers.ToObject<List<ClaimInfo>>();
                        if (claims != null && claims.Count > 0)
                        {
                            if (batch.ClaimType.ToUpperCase() == ClaimType.MAN.ToString())
                            {
                                var managedClaims = billingRepository.GetManagedClaimInfoDetails(Current.AgencyId, claims.Select(c => c.ClaimId).ToList());
                                if (managedClaims != null && managedClaims.Count > 0)
                                {
                                    claimInfos.AddRange(managedClaims);
                                }
                            }
                            else if (batch.ClaimType.ToUpperCase() == ClaimType.CMS.ToString() || batch.ClaimType.ToUpperCase() == ClaimType.HMO.ToString())
                            {
                                var raps = claims.Where(c => c.ClaimType == "322" || c.ClaimType.ToUpperCase() == "RAP").ToList();
                                if (raps != null && raps.Count > 0)
                                {
                                    var medicareRapClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, raps.Select(c => c.ClaimId).ToList(), "RAP");
                                    if (medicareRapClaims != null && medicareRapClaims.Count > 0)
                                    {
                                        claimInfos.AddRange(medicareRapClaims);
                                    }
                                }
                                var finals = claims.Where(c => c.ClaimType == "329" || c.ClaimType.ToUpperCase() == "FINAL").ToList();
                                if (finals != null && finals.Count > 0)
                                {
                                    var medicareFinalClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, finals.Select(c => c.ClaimId).ToList(), "Final");
                                    if (medicareFinalClaims != null && medicareFinalClaims.Count > 0)
                                    {
                                        claimInfos.AddRange(medicareFinalClaims);
                                    }
                                }
                            }
                        }
                    }
                });

                if (claimInfos.Count > 0)
                {
                    claimInfos.Add(new ClaimInfoDetail { ProspectivePay = claimInfos.Sum(c => c.ProspectivePay) });
                }
            }
            return claimInfos;
        }

        #endregion

        #region Private Methods

        private bool IsFirstBillableVisit(ClaimBill claim)
        {
            var isFirstBillableVisit = false;
            if (claim != null && claim.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = claim.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.ToDateTime().Date >= claim.EpisodeStartDate.Date && s.EventDate.ToDateTime().Date <= claim.EpisodeEndDate.Date && !s.IsDeprecated && !s.IsMissedVisit);
                if (scheduleEvents != null)
                {
                    foreach (var scheduleEvent in scheduleEvents)
                    {
                        var status = scheduleEvent.Status;
                        if (scheduleEvent.IsBillable && (status == ((int)ScheduleStatus.NoteCompleted).ToString() || status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || status == ((int)ScheduleStatus.OasisExported).ToString()))
                        {
                            isFirstBillableVisit = true;
                            break;
                        }
                    }
                }
            }
            return isFirstBillableVisit;
        }

        private bool IsOasisComplete(ClaimBill claim)
        {
            var isOasisComplete = false;
            if (claim != null && claim.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = claim.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.ToDateTime().Date >= claim.EpisodeStartDate.Date && s.EventDate.ToDateTime().Date <= claim.EpisodeEndDate.Date && !s.IsDeprecated && !s.IsMissedVisit);

                var socSchedule = scheduleEvents.FirstOrDefault(s => s.DisciplineTask == (int)DisciplineTasks.OASISCStartofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCStartofCarePT|| s.DisciplineTask == (int)DisciplineTasks.OASISCStartofCareOT);
                if (socSchedule != null)
                {
                    var status = socSchedule.Status;
                    if (status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || status == ((int)ScheduleStatus.OasisExported).ToString())
                    {
                        isOasisComplete = true;
                    }
                }
                else
                {
                    var previousEpisode = patientRepository.GetPreviousEpisode(Current.AgencyId, claim.PatientId, claim.EpisodeStartDate);
                    if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                    {
                        var previousSchedules = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.IsValidDate() && s.EventDate.ToDateTime().Date >= claim.EpisodeStartDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date < claim.EpisodeStartDate.Date && !s.IsDeprecated && !s.IsMissedVisit).OrderByDescending(s => s.EventDate.ToDateTime().Date).ToList();
                        if (previousSchedules != null)
                        {
                            var recertSchedule = previousSchedules.FirstOrDefault(s => s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT);
                            if (recertSchedule != null)
                            {
                                var status = recertSchedule.Status;
                                if (status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || status == ((int)ScheduleStatus.OasisExported).ToString())
                                {
                                    isOasisComplete = true;
                                }
                            }
                            else
                            {
                                var rocSchedule = previousSchedules.FirstOrDefault(s => s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT);
                                if (rocSchedule != null)
                                {
                                    var status = rocSchedule.Status;
                                    if (status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || status == ((int)ScheduleStatus.OasisExported).ToString())
                                    {
                                        isOasisComplete = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return isOasisComplete;
        }

        private bool IsOrdersComplete(ClaimBill claim)
        {
            var result = false;
            if (claim != null && claim.Schedule.IsNotNullOrEmpty())
            {
                var currentScheduleList = new List<ScheduleEvent>();
                var scheduleEvents = claim.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.ToDateTime().Date >= claim.EpisodeStartDate.Date
                   && s.EventDate.ToDateTime().Date <= claim.EpisodeEndDate.Date && !s.IsDeprecated && !s.IsMissedVisit
                   && (s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder || s.DisciplineTask == (int)DisciplineTasks.HCFA485
                    || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485) && !s.IsOrderForNextEpisode && s.Status != ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString()).ToList();

                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    currentScheduleList.AddRange(scheduleEvents);
                }
                var previousScheduleList = new List<ScheduleEvent>();
                var previousEpisode = patientRepository.GetPreviousEpisode(Current.AgencyId, claim.PatientId, claim.EpisodeStartDate);
                if (previousEpisode != null && previousEpisode.Schedule.IsNotNullOrEmpty())
                {
                    previousScheduleList = previousEpisode.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.ToDateTime().Date >= previousEpisode.StartDate.Date && s.EventDate.ToDateTime().Date <= previousEpisode.EndDate.Date && !s.IsDeprecated && ((s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485)) && s.IsOrderForNextEpisode == true && s.Status != ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString()).ToList();
                    if (previousScheduleList != null && previousScheduleList.Count > 0)
                    {
                        currentScheduleList.AddRange(previousScheduleList);
                    }
                }
                if (currentScheduleList != null && currentScheduleList.Count > 0)
                {
                                    
                        result = false;
                    
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        private bool IsVisitComplete(ClaimBill claim)
        {
            var result = false;
            if (claim != null && claim.Schedule.IsNotNullOrEmpty())
            {
                var scheduleEvents = claim.Schedule.ToObject<List<ScheduleEvent>>().Where(s => !s.EventId.IsEmpty() && s.EventDate.ToDateTime().Date >= claim.EpisodeStartDate.Date && s.EventDate.ToDateTime().Date <= claim.EpisodeEndDate.Date && !s.IsDeprecated && !(s.Discipline == Disciplines.ReportsAndNotes.ToString()) && !(s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter) && !(s.Discipline == Disciplines.Orders.ToString()) && !(s.Discipline == Disciplines.Claim.ToString()) && (!(s.Status == ((int)ScheduleStatus.NoteCompleted).ToString()) && !(s.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString()) && !(s.Status == ((int)ScheduleStatus.OasisExported).ToString())) && !s.IsMissedVisit && s.IsBillable).ToList();
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }

            return result;
        }

        #endregion
    }
}
