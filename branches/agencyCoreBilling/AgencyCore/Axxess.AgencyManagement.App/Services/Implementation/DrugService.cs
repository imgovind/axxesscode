﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Linq;
    using System.Data.Common;
    using System.Collections.Generic;

    using LexiData;

    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class DrugService : IDrugService
    {
        #region Private Members and Constructor
       
        private string providerInvariantName = "MySql.Data.MySqlClient";

        #endregion

        #region IDrugService Members

        public IList<Drug> DrugsStartsWith(string query, int limit)
        {
            var drugs = new List<Drug>();
            var drugList = new List<string>();

            DbConnection dbConnection = null;
            DbProviderFactory dbFactory = null;
            dbFactory = DbProviderFactories.GetFactory(this.providerInvariantName);
            if (dbFactory == null)
            {
                throw new Exception("Unable to create a DbFactory");
            }
            dbConnection = dbFactory.CreateConnection();
            dbConnection.ConnectionString = Container.Resolve<IWebConfiguration>().ConnectionStrings("LexiDataConnectionString");

            dbConnection.Open();

            GenericDAL dataLayer = new GenericDAL(dbFactory, dbConnection, providerInvariantName);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.SearchType = SEARCHTYPE.WordBeginsWith;
            searchCriteria.SearchMethod = SEARCHMETHOD.Normalized;

            DrugFilter drugFilter = new DrugFilter();
            drugFilter.IncludeBrandName = true;
            drugFilter.IncludeSynonyms = true;

            List<GenericDrug> genericDrugs = dataLayer.SearchGenericDrug(query, searchCriteria, drugFilter);

            int count = 0;
            genericDrugs.ForEach(genericDrug =>
            {
                var drug = new Drug
                {
                    Name = genericDrug.MatchedName,
                    LexiDrugId = genericDrug.GenDrugID
                };
                if (!drugList.Contains(genericDrug.MatchedName.Trim().ToLower()))
                {
                    drugs.Add(drug);
                    drugList.Add(genericDrug.MatchedName.Trim().ToLower());
                    count++;
                }
            });

            dbConnection.Close();

            return drugs.OrderBy(d => d.Name).Take(limit).ToList();
        }

        public List<DrugDrugInteractionResult> GetDrugDrugInteractions(List<string> drugs)
        {
            var result = new List<DrugDrugInteractionResult>();

            DbConnection dbConnection = null;
            DbProviderFactory dbFactory = null;
            dbFactory = DbProviderFactories.GetFactory(this.providerInvariantName);
            if (dbFactory == null)
            {
                throw new Exception("Unable to create a DbFactory");
            }
            dbConnection = dbFactory.CreateConnection();
            dbConnection.ConnectionString = Container.Resolve<IWebConfiguration>().ConnectionStrings("LexiDataConnectionString");

            dbConnection.Open();

            GenericDAL dataLayer = new GenericDAL(dbFactory, dbConnection, providerInvariantName);

            ScreeningContext screeningContext = new ScreeningContext();

            drugs.ForEach(d =>
            {
                var baseDrug = dataLayer.GetGenericDrug(d);
                screeningContext.Drugs.Add(baseDrug);
            });

            result = dataLayer.GetDrugDrugInteractions(screeningContext, false, 0);

            dbConnection.Close();

            return result;
        }
        #endregion
    }
}
