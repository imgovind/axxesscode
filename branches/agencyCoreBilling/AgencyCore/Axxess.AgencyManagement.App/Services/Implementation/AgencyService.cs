﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Repositories;

    using Axxess.Log.Enums;

    using Enums;
    using Domain;
    using Common;
    using ViewData;

    public class AgencyService : IAgencyService
    {
        private readonly IUserRepository userRepository;
        private readonly IErrorRepository errorRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, ILookUpDataProvider lookUpDataProvider, IOasisCDataProvider oasisDataprovider)
        {
            Check.Argument.IsNotNull(oasisDataprovider, "oasisDataprovider");
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.planofCareRepository = oasisDataprovider.PlanofCareRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        public bool CreateAgency(Agency agency)
        {
            try
            {
                agency.IsSuspended = false;
                if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count == 3)
                {
                    agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count == 3)
                {
                    agency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count == 3)
                {
                    agency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                }

                if (agencyRepository.Add(agency))
                {
                    var location = new AgencyLocation();
                    location.Name = agency.LocationName;
                    location.AddressLine1 = agency.AddressLine1;
                    location.AddressLine2 = agency.AddressLine2;
                    location.AddressCity = agency.AddressCity;
                    location.AddressStateCode = agency.AddressStateCode;
                    location.AddressZipCode = agency.AddressZipCode;
                    location.AgencyId = agency.Id;
                    location.IsMainOffice = true;
                    location.IsDeprecated = false;

                    if (agency.PhoneArray != null && agency.PhoneArray.Count > 0)
                    {
                        location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (agency.FaxArray != null && agency.FaxArray.Count > 0)
                    {
                        location.FaxNumber = agency.FaxArray.ToArray().PhoneEncode();
                    }

                    var zipCode = lookupRepository.GetZipCode(agency.AddressZipCode);
                    if (zipCode != null)
                    {
                        location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
                        location.MedicareProviderNumber = agency.MedicareProviderNumber;
                    }
                    location.Cost = "<ArrayOfCostRate><CostRate><RateDiscipline>SkilledNurse</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseTeaching</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseObservation</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseManagement</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>MedicareSocialWorker</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>HomeHealthAide</RateDiscipline><PerUnit>120</PerUnit></CostRate><CostRate><RateDiscipline>Attendant</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>CompanionCare</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>HomemakerServices</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>PrivateDutySitter</RateDiscipline><PerUnit>0</PerUnit></CostRate></ArrayOfCostRate>";
                    location.Id = Guid.NewGuid();
                    if (agencyRepository.AddLocation(location))
                    {
                        Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                        var user = new User
                        {
                            AgencyId = agency.Id,
                            AllowWeekendAccess = true,
                            AgencyName = agency.Name,
                            AgencyLocationId = location.Id,
                            Status = (int)UserStatus.Active,
                            LastName = agency.AgencyAdminLastName,
                            FirstName = agency.AgencyAdminFirstName,
                            PermissionsArray = GeneratePermissions(),
                            EmailAddress = agency.AgencyAdminUsername,
                            TitleType = TitleTypes.Administrator.GetDescription(),
                            Profile = new UserProfile() { EmailWork = agency.AgencyAdminUsername },
                            AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() }
                        };

                        IUserService userService = Container.Resolve<IUserService>();
                        return userService.CreateUser(user);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            return false;
        }

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();

            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }

            return list;
        }

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            var zipCode = lookupRepository.GetZipCode(location.AddressZipCode);
            location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
            location.AgencyId = Current.AgencyId;
            location.Id = Guid.NewGuid();
            if (agencyRepository.AddLocation(location))
            {
                Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                result = true;
            }

            return result;
        }

        public Agency GetAgency(Guid Id)
        {
            Agency agencyViewData = agencyRepository.Get(Id);
            if (agencyViewData != null)
            {
                return agencyViewData;
            }
            return null;
        }

        public bool CreateContact(AgencyContact contact)
        {
            contact.AgencyId = Current.AgencyId;
            contact.Id = Guid.NewGuid();
            try
            {
                if (agencyRepository.AddContact(contact))
                {
                    Auditor.AddGeneralLog(LogDomain.Agency, contact.AgencyId, contact.Id.ToString(), LogType.AgencyContact, LogAction.AgencyContactAdded, string.Empty);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public List<UserVisit> GetSchedule()
        {
            var schedule = new List<UserVisit>();
            var userEvents = userRepository.GetSchedules(Current.AgencyId);
            userEvents.ForEach(ue =>
            {
                var user = userRepository.Get(ue.UserId, Current.AgencyId);
                if (user != null && !ue.PatientId.IsEmpty())
                {
                    var patient = patientRepository.GetPatient<Patient>(ue.PatientId, Current.AgencyId);
                    if (patient != null && !ue.EpisodeId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisode(Current.AgencyId, ue.EpisodeId, patient.Id);
                        if (episode != null && episode.IsActive == true && episode.IsDischarged == false)
                        {
                            var visitNote = string.Empty;
                            if (episode.Schedule.IsNotNullOrEmpty())
                            {
                                var scheduleList = episode.Schedule.ToObject<List<ScheduleEvent>>();
                                if (scheduleList != null && scheduleList.Count > 0)
                                {
                                    var scheduledEvent = scheduleList.Find(e => e.EventId == ue.EventId);
                                    if (scheduledEvent != null && scheduledEvent.Comments.IsNotNullOrEmpty())
                                    {
                                        visitNote = scheduledEvent.Comments;
                                    }

                                }
                            }
                            schedule.Add(new UserVisit
                            {
                                Id = ue.EventId,
                                UserId = ue.UserId,
                                EpisodeId = episode.Id,
                                PatientId = patient.Id,
                                VisitDate = ue.EventDate.ToZeroFilled(),
                                EpisodeNotes = episode.Detail.Comments,
                                VisitNotes = visitNote,
                                PatientName = patient.DisplayName,
                                UserDisplayName = user.DisplayName,
                                StatusName = ((ScheduleStatus)Enum.Parse(typeof(ScheduleStatus), ue.Status)).GetDescription(),
                                TaskName = ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), ue.DisciplineTask.ToString())).GetDescription()
                            });
                        }
                    }
                }
            });
            return schedule;
        }

        public List<PatientEpisodeEvent> GetCaseManagerSchedule(Guid branchId, int status)
        {
            var events = new List<PatientEpisodeEvent>();
            var agencyPatientEpisodes = patientRepository.GetPatientEpisodeDataForSchedule(Current.AgencyId, branchId, status);
            if (agencyPatientEpisodes != null && agencyPatientEpisodes.Count > 0)
            {
                agencyPatientEpisodes.ForEach(e =>
                {
                    if (e.Schedule.IsNotNullOrEmpty())
                    {
                        var scheduledEvents = e.Schedule.ToObject<List<ScheduleEvent>>()
                            .Where(s => s.IsDeprecated == false && s.IsMissedVisit == false &&
                            (s.Status == ((int)ScheduleStatus.OrderSubmittedPendingReview).ToString()
                                    || s.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString()
                                    || s.Status == ((int)ScheduleStatus.NoteSubmittedWithSignature).ToString()
                                    || s.Status == ((int)ScheduleStatus.ReportAndNotesSubmittedWithSignature).ToString()));
                        if (scheduledEvents != null)
                        {
                            scheduledEvents.ForEach(s =>
                            {
                                var details = e.Details.IsNotNullOrEmpty() ? e.Details.ToObject<EpisodeDetail>() : new EpisodeDetail();
                                Common.Url.Set(s, false, false);
                                events.Add(new PatientEpisodeEvent
                                {
                                    PrintUrl = s.PrintUrl,
                                    Status = s.StatusName,
                                    PatientName = e.PatientName,
                                    TaskName = s.DisciplineTaskName,
                                    UserName = UserEngine.GetName(s.UserId, Current.AgencyId),
                                    YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
                                    RedNote = s.StatusComment.IsNotNullOrEmpty() ? s.StatusComment.Clean() : string.Empty,
                                    BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
                                    EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
                                    CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
                                });
                            });
                        }
                    }
                    e.Schedule = string.Empty;
                });
            }
            return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }

        public List<RecertEvent> GetRecertsPastDue()
        {
            var recetEpisodes = patientRepository.GetPastDueRecertsLean(Current.AgencyId);
            var pastDueRecerts = new List<RecertEvent>();
            if (recetEpisodes != null && recetEpisodes.Count > 0)
            {
                recetEpisodes.ForEach(r =>
                {
                    if (r.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
                        if (schedule != null && schedule.Count > 0)
                        {
                            var discahrgeSchedules = schedule.Where(s => s.IsDeprecated = false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome || s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge) && (s.Status == ((int)ScheduleStatus.OasisNotStarted).ToString() || s.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || s.Status == ((int)ScheduleStatus.OasisReopened).ToString() || s.Status == ((int)ScheduleStatus.OasisSaved).ToString())).ToList();
                            if (discahrgeSchedules != null && discahrgeSchedules.Count > 0)
                            {
                            }
                            else
                            {
                                var recetSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated = false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionofCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
                                if (recetSchedule != null)
                                {
                                    if ((recetSchedule.Status ==((int) ScheduleStatus.OasisExported).ToString() || recetSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || recetSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString()))
                                    {

                                    }
                                    else
                                    {
                                        if (!recetSchedule.UserId.IsEmpty())
                                        {
                                            r.AssignedTo = UserEngine.GetName(recetSchedule.UserId, Current.AgencyId);
                                        }
                                        r.Status = recetSchedule.StatusName;
                                        r.Schedule = string.Empty;
                                        pastDueRecerts.Add(r);

                                    }
                                }
                                else
                                {
                                    r.AssignedTo = "Unassigned";
                                    r.Status = "Not Scheduled";
                                    r.Schedule = string.Empty;
                                    pastDueRecerts.Add(r);
                                }
                            }
                        }
                        else
                        {
                            r.AssignedTo = "Unassigned";
                            r.Status = "Not Scheduled";
                            r.Schedule = string.Empty;
                            pastDueRecerts.Add(r);
                        }

                    }
                    else
                    {
                        r.AssignedTo = "Unassigned";
                        r.Status = "Not Scheduled";
                        r.Schedule = string.Empty;
                        pastDueRecerts.Add(r);
                    }
                });
            }

            return pastDueRecerts;
        }

        public List<RecertEvent> GetRecertsPastDue(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var recetEpisodes = patientRepository.GetPastDueRecertsLeanByDateRange(Current.AgencyId, branchId,insuranceId, startDate, endDate.AddDays(5));
            var pastDueRecerts = new List<RecertEvent>();
            if (recetEpisodes != null && recetEpisodes.Count > 0)
            {
                recetEpisodes.ForEach(r =>
                {
                    //if (r.TargetDate.Date >= startDate.Date && r.TargetDate.Date < endDate.AddDays(5).Date)
                    //{
                        if (r.Schedule.IsNotNullOrEmpty())
                        {
                            var schedule = r.Schedule.ToObject<List<ScheduleEvent>>();
                            if (schedule != null && schedule.Count > 0)
                            {
                                var discahrgeSchedules = schedule.Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargeOT || s.DisciplineTask == (int)DisciplineTasks.OASISCDischargePT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeath || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathOT || s.DisciplineTask == (int)DisciplineTasks.OASISCDeathPT || s.DisciplineTask == (int)DisciplineTasks.OASISCTransferDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDischarge || s.DisciplineTask == (int)DisciplineTasks.OASISBDeathatHome || s.DisciplineTask == (int)DisciplineTasks.NonOASISDischarge) && (s.Status == ((int)ScheduleStatus.OasisNotStarted).ToString() || s.Status == ((int)ScheduleStatus.OasisNotYetDue).ToString() || s.Status == ((int)ScheduleStatus.OasisReopened).ToString() || s.Status == ((int)ScheduleStatus.OasisSaved).ToString())).ToList();
                                if (discahrgeSchedules != null && discahrgeSchedules.Count > 0)
                                {
                                }
                                else
                                {
                                    var episodeRecetSchedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-5).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.Date ) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCare || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCarePT || s.DisciplineTask == (int)DisciplineTasks.OASISCResumptionofCareOT || s.DisciplineTask == (int)DisciplineTasks.OASISBResumptionofCare)).OrderByDescending(s => s.EventDate.ToDateTime()).FirstOrDefault();
                                    if (episodeRecetSchedule != null)
                                    {
                                        if ((episodeRecetSchedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || episodeRecetSchedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || episodeRecetSchedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString()))
                                        {

                                        }
                                        else
                                        {
                                            if (episodeRecetSchedule.EventDate.ToDateTime().Date < endDate.Date)
                                            {
                                                if (!episodeRecetSchedule.UserId.IsEmpty())
                                                {
                                                    r.AssignedTo = UserEngine.GetName(episodeRecetSchedule.UserId, Current.AgencyId);
                                                }
                                                r.Status = episodeRecetSchedule.StatusName;
                                                r.Schedule = string.Empty;
                                                r.DateDifference = endDate.Subtract(episodeRecetSchedule.EventDate.ToDateTime().Date).Days;
                                                pastDueRecerts.Add(r);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (r.TargetDate.Date < endDate.Date)
                                        {
                                            r.AssignedTo = "Unassigned";
                                            r.Status = "Not Scheduled";
                                            r.Schedule = string.Empty;
                                            r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
                                            pastDueRecerts.Add(r);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (r.TargetDate.Date <endDate.Date)
                                {
                                    r.AssignedTo = "Unassigned";
                                    r.Status = "Not Scheduled";
                                    r.Schedule = string.Empty;
                                    r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
                                    pastDueRecerts.Add(r);
                                }
                            }

                        }
                        else
                        {
                            if (r.TargetDate.Date < endDate.Date)
                            {
                                r.AssignedTo = "Unassigned";
                                r.Status = "Not Scheduled";
                                r.Schedule = string.Empty;
                                r.DateDifference = DateTime.Now.Subtract(r.TargetDate).Days;
                                pastDueRecerts.Add(r);
                            }
                        }
                    //}
                       
                });
            }
            return pastDueRecerts;
        }

        public List<RecertEvent> GetRecertsPastDueWidget()
        {
            return patientRepository.GetPastDueRecertsWidgetLean(Current.AgencyId);
        }

        public List<RecertEvent> GetRecertsUpcoming()
        {
            var recetEpisodes = patientRepository.GetUpcomingRecertsLean(Current.AgencyId);
            var upcomingRecets = new List<RecertEvent>();
            if (recetEpisodes != null && recetEpisodes.Count > 0)
            {
                recetEpisodes.ForEach(r =>
                {
                    if (r.Schedule.IsNotNullOrEmpty())
                    {
                        var schedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-6).Date && s.EventDate.ToDateTime().Date <= r.TargetDate.AddDays(-1).Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISBRecertification)).OrderByDescending(s => s.EventDate.ToDateTime().Date).FirstOrDefault();
                        if (schedule!=null)
                        {
                            if ((schedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString()))
                            {
                            }
                            else
                            {
                                if (!schedule.UserId.IsEmpty())
                                    {
                                        r.AssignedTo = UserEngine.GetName(schedule.UserId, Current.AgencyId);
                                    }
                                r.Status = schedule.StatusName;
                                r.Schedule = string.Empty;
                                upcomingRecets.Add(r);
                            }
                        }
                        else
                        {
                            r.AssignedTo = "Unassigned";
                            r.Status = "Not Scheduled";
                            r.Schedule = string.Empty;
                            upcomingRecets.Add(r);
                        }
                    }
                    else
                    {
                        r.AssignedTo = "Unassigned";
                        r.Status = "Not Scheduled";
                        r.Schedule = string.Empty;
                        upcomingRecets.Add(r);
                    }
                });
            }
            return upcomingRecets;
        }

        public List<RecertEvent> GetRecertsUpcoming(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var recetEpisodes = patientRepository.GetUpcomingRecertsLean(Current.AgencyId, branchId, insuranceId,  startDate,  endDate);
            var upcomingRecets = new List<RecertEvent>();
            if (recetEpisodes != null && recetEpisodes.Count > 0)
            {
                recetEpisodes.ForEach(r =>
                {
                    //var lastFiveDaysStart=r.TargetDate.AddDays(-6);
                    //if ((r.TargetDate.AddDays(-1).Date <= endDate && r.TargetDate.AddDays(-1).Date >= startDate) || (lastFiveDaysStart.Date >= startDate.Date && lastFiveDaysStart.Date <= endDate.Date))
                    //{
                        if (r.Schedule.IsNotNullOrEmpty())
                        {
                            var schedule = r.Schedule.ToObject<List<ScheduleEvent>>().Where(s =>  s.IsDeprecated == false && s.EventDate.IsValidDate() && (s.EventDate.ToDateTime().Date >= r.StartDate.Date && s.EventDate.ToDateTime().Date <= r.TargetDate.AddDays(-1).Date) && (s.EventDate.ToDateTime().Date >= r.TargetDate.AddDays(-6).Date) && (s.DisciplineTask == (int)DisciplineTasks.OASISCRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationPT || s.DisciplineTask == (int)DisciplineTasks.OASISCRecertificationOT || s.DisciplineTask == (int)DisciplineTasks.NonOASISRecertification || s.DisciplineTask == (int)DisciplineTasks.OASISBRecertification)  ).OrderByDescending(s => s.EventDate.ToDateTime().Date).FirstOrDefault();
                            if (schedule != null)
                            {
                                if ((schedule.Status == ((int)ScheduleStatus.OasisExported).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || schedule.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() ))
                                {
                                }
                                else
                                {
                                    if (schedule.EventDate.ToDateTime().Date >= startDate.Date && schedule.EventDate.ToDateTime().Date <= endDate.Date)
                                    {
                                        if (!schedule.UserId.IsEmpty())
                                        {
                                            r.AssignedTo = UserEngine.GetName(schedule.UserId, Current.AgencyId);
                                        }
                                        r.Status = schedule.StatusName;
                                        r.Schedule = string.Empty;
                                        upcomingRecets.Add(r);
                                    }
                                }
                            }
                            else
                            {
                                if (r.TargetDate.AddDays(-1).Date < endDate.Date)
                                {
                                    r.AssignedTo = "Unassigned";
                                    r.Status = "Not Scheduled";
                                    r.Schedule = string.Empty;
                                    upcomingRecets.Add(r);
                                }
                            }
                        }
                        else
                        {
                            if (r.TargetDate.AddDays(-1).Date < endDate.Date)
                            {
                                r.AssignedTo = "Unassigned";
                                r.Status = "Not Scheduled";
                                r.Schedule = string.Empty;
                                upcomingRecets.Add(r);
                            }
                        }
                    //}
                });
            }
            return upcomingRecets;
        }

        public List<RecertEvent> GetRecertsUpcomingWidget()
        {
            return patientRepository.GetUpcomingRecertsWidgetLean(Current.AgencyId);
        }

        public List<InsuranceViewData> GetInsurances()
        {
            var insuranceList = new List<InsuranceViewData>();
            lookupRepository.Insurances().ForEach(i =>
            {
                insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            });
            agencyRepository.GetInsurances(Current.AgencyId).ForEach(i =>
            {
                insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            });
            return insuranceList;
        }

        public List<Order> GetOrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var status = new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician, (int)ScheduleStatus.EvalToBeSentToPhysician };
            var schedules = patientRepository.GetOrderScheduleEvents(Current.AgencyId,BranchId, startDate, endDate, status);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianName = po.PhysicianName,
                                PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                CreatedDate = po.OrderDateFormatted

                            });
                        });
                    }
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareOrdersIds);

                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485,
                                    Text = DisciplineTasks.HCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareStandAloneOrdersIds);

                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                var patient = patientRepository.GetPatientOnly(poc.PatientId, Current.AgencyId);
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, faceToFaceEncounterOrdersIds);
                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                        {
                            var evnt = faceToFaceEncounterSchedules.SingleOrDefault(s => s.EventId == ffe.Id);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!ffe.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (ffe.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = ffe.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy")
                                });
                            }
                        });
                    }
                }
                var evalOrdersSchedule = schedules.Where(s =>
                        s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation).ToList();
                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, status[1], evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            orders.Add(new Order
                            {
                                Id = eval.Id,
                                Type = GetOrderType(eval.NoteType),
                                Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                Number = eval.OrderNumber,
                                PatientName = eval.DisplayName,
                                PhysicianName = eval.PhysicianName,
                                PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
                                SendDate = eval.SentDate
                            });
                        });
                    }
                }
            }
            return orders.Where(o => o.PhysicianAccess == sendAutomatically).OrderByDescending(o => o.CreatedDate).ToList();
        }

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                }
            }
            return orderType;
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                }
            }
            return task;
        }

        public List<Order> GetProcessedOrders(Guid BranchId, DateTime startDate, DateTime endDate, List<int> status)
        {
            var orders = new List<Order>();
            var schedules = patientRepository.GetOrderScheduleEvents(Current.AgencyId,BranchId, startDate, endDate, status);
            if (schedules != null && schedules.Count > 0 && status.Count > 1)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, status[0], physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianName = po.PhysicianName,
                                PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
                                CreatedDate = po.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate,
                                SendDate = po.SentDate
                            });
                        });
                    }
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, status[0], planofCareOrdersIds);
                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                var physician = poc.PhysicianData.IsNotNullOrEmpty() ? poc.PhysicianData.ToObject<AgencyPhysician>() : PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485,
                                    Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SendDate = poc.SentDate
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, status[0], planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                var physician = poc.PhysicianData.IsNotNullOrEmpty() ? poc.PhysicianData.ToObject<AgencyPhysician>() : PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled()
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                 {
                     var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                     var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, status[0], faceToFaceEncounterOrdersIds);

                     if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                     {
                         faceToFaceEncounters.ForEach(ffe =>
                         {
                             var evnt = faceToFaceEncounterSchedules.SingleOrDefault(s => s.EventId == ffe.Id);
                             if (evnt != null)
                             {
                                 var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                 orders.Add(new Order
                                 {
                                     Id = ffe.Id,
                                     Type = OrderType.FaceToFaceEncounter,
                                     Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                     Number = ffe.OrderNumber,
                                     PatientName = ffe.DisplayName,
                                     PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                     PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                     CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                     ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate,
                                     SendDate = ffe.RequestDate
                                 });
                             }
                         });
                     }
                 }

                var evalOrdersSchedule = schedules.Where(s => 
                        s.DisciplineTask == (int)DisciplineTasks.PTEvaluation || s.DisciplineTask == (int)DisciplineTasks.PTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.OTEvaluation || s.DisciplineTask == (int)DisciplineTasks.OTReEvaluation
                        || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation || s.DisciplineTask == (int)DisciplineTasks.STReEvaluation).ToList();
                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, status[1], evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var evnt = faceToFaceEncounterSchedules.SingleOrDefault(s => s.EventId == eval.Id);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = eval.Id,
                                    Type = GetOrderType(eval.NoteType),
                                    Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                    Number = eval.OrderNumber,
                                    PatientName = eval.DisplayName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
                                    SendDate = eval.SentDate
                                });
                            }
                        });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public Order GetOrder(Guid id, Guid patientId, string type)
        {
            var order = new Order();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (!id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty() && type.IsInteger() && Enum.IsDefined(typeof(OrderType), type.ToInteger()))
            {
                var typeEnum = ((OrderType)type.ToInteger()).ToString();
                if (typeEnum.IsNotNullOrEmpty())
                {
                    switch (typeEnum)
                    {
                        case "PhysicianOrder":
                            var physicianOrder = patientRepository.GetOrder(id, Current.AgencyId);
                            if (physicianOrder != null)
                            {
                                order = new Order
                                {
                                    Id = physicianOrder.Id,
                                    PatientId = physicianOrder.PatientId,
                                    Type = OrderType.PhysicianOrder,
                                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                    Number = physicianOrder.OrderNumber,
                                    PatientName = physicianOrder.DisplayName,
                                    PhysicianName = physicianOrder.PhysicianName,
                                    ReceivedDate = physicianOrder.ReceivedDate > DateTime.MinValue ? physicianOrder.ReceivedDate : physicianOrder.SentDate,
                                    SendDate = physicianOrder.SentDate
                                };
                            }
                            break;
                        case "FaceToFaceEncounter":
                            var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                            if (faceToFaceEncounter != null)
                            {
                                order = new Order
                                {
                                    Id = faceToFaceEncounter.Id,
                                    PatientId = faceToFaceEncounter.PatientId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = faceToFaceEncounter.OrderNumber,
                                    PatientName = faceToFaceEncounter.DisplayName,
                                    ReceivedDate = faceToFaceEncounter.ReceivedDate > DateTime.MinValue ? faceToFaceEncounter.ReceivedDate : faceToFaceEncounter.RequestDate,
                                    SendDate = faceToFaceEncounter.RequestDate
                                };
                            }
                            break;
                        case "HCFA485":
                        case "NonOasisHCFA485":
                            var planofCare = planofCareRepository.Get(Current.AgencyId, id);
                            if (planofCare != null)
                            {
                                order = new Order
                                {
                                    Id = planofCare.Id,
                                    Type = OrderType.HCFA485,
                                    PatientId = planofCare.PatientId,
                                    Text = "HCFA485" == type ? DisciplineTasks.HCFA485.GetDescription() : ("NonOasisHCFA485" == type ? DisciplineTasks.NonOasisHCFA485.GetDescription() : string.Empty),
                                    Number = planofCare.OrderNumber,
                                    PatientName = planofCare.PatientName,
                                    ReceivedDate = planofCare.ReceivedDate > DateTime.MinValue ? planofCare.ReceivedDate : planofCare.SentDate,
                                    SendDate = planofCare.SentDate
                                };
                            }
                            break;
                        case "PtEvaluation":
                            var ptEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptEval != null)
                            {
                                order = new Order
                                {
                                    Id = ptEval.Id,
                                    PatientId = ptEval.PatientId,
                                    Type = OrderType.PtEvaluation,
                                    Text = DisciplineTasks.PTEvaluation.GetDescription(),
                                    Number = ptEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptEval.ReceivedDate > DateTime.MinValue ? ptEval.ReceivedDate : ptEval.SentDate,
                                    SendDate = ptEval.SentDate
                                };
                            }
                            break;
                        case "PtReEvaluation":
                            var ptReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptReEval != null)
                            {
                                order = new Order
                                {
                                    Id = ptReEval.Id,
                                    PatientId = ptReEval.PatientId,
                                    Type = OrderType.PtReEvaluation,
                                    Text = DisciplineTasks.PTReEvaluation.GetDescription(),
                                    Number = ptReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptReEval.ReceivedDate > DateTime.MinValue ? ptReEval.ReceivedDate : ptReEval.SentDate,
                                    SendDate = ptReEval.SentDate
                                };
                            }
                            break;
                        case "OtEvaluation":
                            var otEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otEval != null)
                            {
                                order = new Order
                                {
                                    Id = otEval.Id,
                                    PatientId = otEval.PatientId,
                                    Type = OrderType.OtEvaluation,
                                    Text = DisciplineTasks.OTEvaluation.GetDescription(),
                                    Number = otEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otEval.ReceivedDate > DateTime.MinValue ? otEval.ReceivedDate : otEval.SentDate,
                                    SendDate = otEval.SentDate
                                };
                            }
                            break;
                        case "OtReEvaluation":
                            var otReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otReEval != null)
                            {
                                order = new Order
                                {
                                    Id = otReEval.Id,
                                    PatientId = otReEval.PatientId,
                                    Type = OrderType.OtReEvaluation,
                                    Text = DisciplineTasks.OTReEvaluation.GetDescription(),
                                    Number = otReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otReEval.ReceivedDate > DateTime.MinValue ? otReEval.ReceivedDate : otReEval.SentDate,
                                    SendDate = otReEval.SentDate
                                };
                            }
                            break;
                        case "StEvaluation":
                            var stEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stEval != null)
                            {
                                order = new Order
                                {
                                    Id = stEval.Id,
                                    PatientId = stEval.PatientId,
                                    Type = OrderType.StEvaluation,
                                    Text = DisciplineTasks.STEvaluation.GetDescription(),
                                    Number = stEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stEval.ReceivedDate > DateTime.MinValue ? stEval.ReceivedDate : stEval.SentDate,
                                    SendDate = stEval.SentDate
                                };
                            }
                            break;
                        case "StReEvaluation":
                            var stReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stReEval != null)
                            {
                                order = new Order
                                {
                                    Id = stReEval.Id,
                                    PatientId = stReEval.PatientId,
                                    Type = OrderType.StReEvaluation,
                                    Text = DisciplineTasks.STReEvaluation.GetDescription(),
                                    Number = stReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stReEval.ReceivedDate > DateTime.MinValue ? stReEval.ReceivedDate : stReEval.SentDate,
                                    SendDate = stReEval.SentDate
                                };
                            }
                            break;
                    }
                }
            }

            return order;
        }

        public List<Order> GetOrdersPendingSignature(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var schedules = patientRepository.GetPendingSignatureOrderScheduleEvents(Current.AgencyId, branchId, startDate, endDate);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPendingPhysicianSignatureOrders(Current.AgencyId, physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianName = po.PhysicianName,
                                SentDate = po.SentDate.ToShortDateString().ToZeroFilled(),
                                CreatedDate = po.OrderDateFormatted,
                                ReceivedDate = DateTime.Today
                            });
                        });
                    }
                }

                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPendingSignaturePlanofCares(Current.AgencyId, planofCareOrdersIds);

                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                var physician = poc.PhysicianData.IsNotNullOrEmpty() ? poc.PhysicianData.ToObject<AgencyPhysician>() : PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485,
                                    Text = DisciplineTasks.HCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled()
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPendingSignaturePlanofCaresStandAlone(Current.AgencyId, planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                var physician = poc.PhysicianData.IsNotNullOrEmpty() ? poc.PhysicianData.ToObject<AgencyPhysician>() : PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate = evnt.EventDate.IsNotNullOrEmpty() && evnt.EventDate.IsValidDate() ? evnt.EventDate.ToDateTime().ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled()
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetPendingSignatureFaceToFaceEncounterOrders(Current.AgencyId, faceToFaceEncounterOrdersIds);

                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                            {
                                var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    SentDate = ffe.RequestDate.ToString("MM/dd/yyyy"),
                                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy"),
                                    ReceivedDate = DateTime.Today
                                });
                            });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public bool MarkOrdersAsSent(FormCollection formCollection)
        {
            var result = true;
            var status = (int)ScheduleStatus.OrderSentToPhysician;
            var sendElectronically = formCollection.Get("SendAutomatically").ToBoolean();
            formCollection.Remove("SendAutomatically");
            if (sendElectronically)
            {
                status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
            }

            foreach (var key in formCollection.AllKeys)
            {
                string answers = formCollection.GetValues(key).Join(",");
                string[] answersArray = answers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.PhysicianOrder)
                {
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var orderId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var order = patientRepository.GetOrderOnly(orderId, Current.AgencyId);
                        if (order != null)
                        {
                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, order.EpisodeId, order.PatientId, order.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status.ToString();
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                                {
                                    if (scheduledEvent.Status.IsInteger())
                                    {
                                        Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), DisciplineTasks.PhysicianOrder, string.Empty);
                                    }
                                }
                            }
                            if (!patientRepository.UpdateOrderStatus(Current.AgencyId, orderId, status, DateTime.MinValue))
                            {
                                result = false;
                                return;
                            }
                        }
                      
                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485)
                {
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var planofCare = planofCareRepository.Get(Current.AgencyId, pocId);
                        if (planofCare != null)
                        {
                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = (status).ToString();
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                                {
                                    if (scheduledEvent.Status.IsInteger())
                                    {
                                        Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                    }
                                }
                            }
                            planofCare.Status = status;
                            planofCare.SentDate = DateTime.Now;
                            if (!planofCareRepository.Update(planofCare))
                            {
                                result = false;
                                return;
                            }
                        }
                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485StandAlone)
                {
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocOrderId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var planofCare = planofCareRepository.GetStandAlone(Current.AgencyId, pocOrderId);
                        if (planofCare != null)
                        {
                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status.ToString();
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                                {
                                    if (scheduledEvent.Status.IsInteger())
                                    {
                                        Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                    }
                                }
                            }
                            planofCare.Status = status;
                            planofCare.SentDate = DateTime.Now;
                            if (!planofCareRepository.UpdateStandAlone(planofCare))
                            {
                                result = false;
                                return;
                            }
                        }
                    });
                }
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.FaceToFaceEncounter)
                {
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var facetofaceId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length > 1 ? answerArray[1].ToGuid() : Guid.Empty;
                        var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(facetofaceId, Current.AgencyId);
                        if (faceToFaceEncounter != null)
                        {
                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.PatientId, faceToFaceEncounter.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status.ToString();
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                                {
                                    if (scheduledEvent.Status.IsInteger())
                                    {
                                        Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                    }
                                }
                            }
                            if (!patientRepository.UpdateFaceToFaceEncounterForRequest(Current.AgencyId, facetofaceId, status, DateTime.Now))
                            {
                                result = false;
                                return;
                            }
                        }
                    });
                }
                if (key.IsInteger() && (key.ToInteger() == (int)OrderType.PtEvaluation || key.ToInteger() == (int)OrderType.PtReEvaluation
                    || key.ToInteger() == (int)OrderType.OtEvaluation || key.ToInteger() == (int)OrderType.OtReEvaluation
                    || key.ToInteger() == (int)OrderType.StEvaluation || key.ToInteger() == (int)OrderType.StReEvaluation))
                {
                    status = (int)ScheduleStatus.EvalSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.EvalSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var evalId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length > 1 ? answerArray[1].ToGuid() : Guid.Empty;
                        var evalOrder = patientRepository.GetVisitNote(Current.AgencyId, patientId, evalId);
                        if (evalOrder != null)
                        {
                            var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, evalOrder.EpisodeId, evalOrder.PatientId, evalOrder.Id);
                            if (scheduledEvent != null)
                            {
                                scheduledEvent.Status = status.ToString();
                                if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                                {
                                    if (scheduledEvent.Status.IsInteger())
                                    {
                                        Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                                    }
                                }
                            }
                            evalOrder.Status = status;
                            evalOrder.SentDate = DateTime.Now;
                            if (!patientRepository.UpdateVisitNote(evalOrder))
                            {
                                result = false;
                                return;
                            }
                        }
                    });
                }
            }

            return result;
        }

        public void MarkOrderAsReturned(Guid id, Guid patientId, OrderType type, DateTime receivedDate)
        {
            if (type == OrderType.PhysicianOrder)
            {
                var order = patientRepository.GetOrderOnly(id, Current.AgencyId);
                if (order != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, order.EpisodeId, order.PatientId, order.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                        {
                            if (scheduledEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                    patientRepository.UpdateOrderStatus(Current.AgencyId, id, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, receivedDate);
                }
            }

            if (type == OrderType.HCFA485)
            {
                var planofCare = planofCareRepository.Get(Current.AgencyId, id);
                if (planofCare != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, planofCare.EpisodeId, planofCare.PatientId, planofCare.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                        {
                            if (scheduledEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                    var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                    }
                    planofCare.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                    planofCare.ReceivedDate = receivedDate;
                    planofCareRepository.Update(planofCare);
                }
            }

            if (type == OrderType.HCFA485StandAlone)
            {
                var pocStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, id);
                if (pocStandAlone != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, pocStandAlone.EpisodeId, pocStandAlone.PatientId, pocStandAlone.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                        {
                            if (scheduledEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                    var physician = pocStandAlone.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                    }
                    pocStandAlone.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                    pocStandAlone.ReceivedDate = receivedDate;
                    planofCareRepository.UpdateStandAlone(pocStandAlone);
                }
            }

            if (type == OrderType.FaceToFaceEncounter)
            {
                var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                if (faceToFaceEncounter != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.PatientId, faceToFaceEncounter.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                        {
                            if (scheduledEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }

                    faceToFaceEncounter.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                    faceToFaceEncounter.ReceivedDate = receivedDate;
                    patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter);
                }
            }
            if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation
                || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation
                || type == OrderType.StEvaluation || type == OrderType.StReEvaluation)
            {
                var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                if (eval != null)
                {
                    var scheduledEvent = patientRepository.GetSchedule(Current.AgencyId, eval.EpisodeId, eval.PatientId, eval.Id);
                    if (scheduledEvent != null)
                    {
                        scheduledEvent.Status = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString();
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduledEvent))
                        {
                            if (scheduledEvent.Status.IsInteger())
                            {
                                Auditor.Log(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduledEvent.Status.ToInteger(), (DisciplineTasks)scheduledEvent.DisciplineTask, string.Empty);
                            }
                        }
                    }
                    eval.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                    eval.ReceivedDate = receivedDate;
                    patientRepository.UpdateVisitNote(eval);
                }
            }
        }

        public bool UpdateOrderDates(Guid id, Guid patientId, OrderType type, DateTime receivedDate, DateTime sendDate)
        {

            bool result = false;
            if (type == OrderType.PhysicianOrder)
            {
                result = patientRepository.UpdateOrderStatus(Current.AgencyId, id, (int)ScheduleStatus.OrderReturnedWPhysicianSignature, receivedDate, sendDate);
            }
            else if (type == OrderType.HCFA485)
            {
                var planofCare = planofCareRepository.Get(Current.AgencyId, id);
                if (planofCare != null)
                {
                    planofCare.ReceivedDate = receivedDate;
                    planofCare.SentDate = sendDate;
                    result = planofCareRepository.Update(planofCare);
                }
            }
            else if (type == OrderType.HCFA485StandAlone)
            {
                var pocStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, id);
                if (pocStandAlone != null)
                {
                    pocStandAlone.ReceivedDate = receivedDate;
                    pocStandAlone.SentDate = sendDate;
                    result = planofCareRepository.UpdateStandAlone(pocStandAlone);
                }
            }
            else if (type == OrderType.FaceToFaceEncounter)
            {
                var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                if (faceToFaceEncounter != null)
                {
                    faceToFaceEncounter.ReceivedDate = receivedDate;
                    faceToFaceEncounter.RequestDate = sendDate;
                    result = patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter);
                }
            }
            else if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation
                || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation
                || type == OrderType.StEvaluation || type == OrderType.StReEvaluation)
            {
                var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                if (eval != null)
                {
                    eval.ReceivedDate = receivedDate;
                    eval.SentDate = sendDate;
                    result = patientRepository.UpdateVisitNote(eval);
                }
            }
            return result;
        }

        public List<AddressViewData> GetAgencyFullAddress()
        {
            var listOfAddress = agencyRepository.GetBranches(Current.AgencyId);
            var address = new List<AddressViewData>();
            if (listOfAddress != null && listOfAddress.Count>0)
            {
                listOfAddress.ForEach(l =>
                {
                    address.Add(new AddressViewData { Name = l.Name, FullAddress = string.Format("{0} {1}, {2}, {3} {4}", l.AddressLine1, l.AddressLine2, l.AddressCity, l.AddressStateCode, l.AddressZipCode) });
                });
            }
            return address;
        }

        public List<Infection> GetInfections(Guid agencyId)
        {
            var infections = agencyRepository.GetInfections(Current.AgencyId).ToList();
            if (infections != null && infections.Count > 0)
            {
                infections.ForEach(i =>
                {
                    var patient = patientRepository.GetPatientOnly(i.PatientId, i.AgencyId);
                    if (patient != null)
                    {
                        i.PatientName = patient.DisplayName;
                    }
                    var physician = physicianRepository.Get(i.PhysicianId, i.AgencyId);
                    if (physician != null)
                    {
                        i.PhysicianName = physician.DisplayName;
                    }
                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, i.EpisodeId, i.PatientId, i.Id);
                    if (scheduleEvent != null)
                    {
                        i.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return infections;
        }

        public List<Incident> GetIncidents(Guid agencyId)
        {
            var incidents = agencyRepository.GetIncidents(Current.AgencyId).ToList();
            if (incidents != null && incidents.Count > 0)
            {
                incidents.ForEach(i =>
                {
                    var patient = patientRepository.GetPatientOnly(i.PatientId, i.AgencyId);
                    if (patient != null)
                    {
                        i.PatientName = patient.DisplayName;
                    }
                    var physician = physicianRepository.Get(i.PhysicianId, i.AgencyId);
                    if (physician != null)
                    {
                        i.PhysicianName = physician.DisplayName;
                    }

                    var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, i.EpisodeId, i.PatientId, i.Id);
                    if (scheduleEvent != null)
                    {
                        i.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return incidents;
        }

        public bool ProcessInfections(string button, Guid patientId, Guid eventId, string reason)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;

            UserEvent userEvent = null;
            ScheduleEvent scheduleEvent = null;

            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);

                if (infection != null)
                {
                    if (!infection.EpisodeId.IsEmpty())
                    {
                        scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, infection.EpisodeId, patientId, eventId);
                        if (scheduleEvent != null )
                        {
                            userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                        }
                    }
                    var description = string.Empty;
                    if (button == "Approve")
                    {
                        infection.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                        infection.Modified = DateTime.Now;
                        if (agencyRepository.UpdateInfectionModal(infection))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                                description = "Approved By:" + Current.UserFullName;
                            }
                        }
                    }
                    else if (button == "Return")
                    {
                        infection.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                        infection.Modified = DateTime.Now;
                        infection.SignatureText = string.Empty;
                        infection.SignatureDate = DateTime.MinValue;
                        if (agencyRepository.UpdateInfectionModal(infection))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReturned).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.ReportAndNotesReturned).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                                description = "Returned By:" + Current.UserFullName;
                            }
                        }
                    }
                    else if (button == "Print")
                    {
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                    }

                    if (scheduleEvent != null && shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(Current.AgencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                        }
                    }
                }
            }
            return result;
        }

        public bool ProcessIncidents(string button, Guid patientId, Guid eventId, string reason)
        {
            var result = false;
            Guid userId = Current.UserId;
            var shouldUpdateEpisode = false;

            UserEvent userEvent = null;
            ScheduleEvent scheduleEvent = null;

            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
                if (incident != null)
                {
                    if (!incident.EpisodeId.IsEmpty())
                    {
                        scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, incident.EpisodeId, patientId, eventId);
                        if (scheduleEvent != null)
                        {
                            userEvent = userRepository.GetEvent(Current.AgencyId, scheduleEvent.UserId, patientId, eventId);
                        }
                    }
                    var description = string.Empty;
                    var action = new Actions();
                    if (button == "Approve")
                    {
                        incident.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                        incident.Modified = DateTime.Now;
                        if (agencyRepository.UpdateIncidentModal(incident))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString();
                                scheduleEvent.ReturnReason = string.Empty;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString();
                                    userEvent.ReturnReason = string.Empty;
                                }
                                shouldUpdateEpisode = true;
                                description = "Approved by " + Current.UserFullName;
                                action = Actions.Approved;
                            }
                        }
                    }
                    else if (button == "Return")
                    {
                        incident.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                        incident.Modified = DateTime.Now;
                        incident.SignatureText = string.Empty;
                        incident.SignatureDate = DateTime.MinValue;
                        if (agencyRepository.UpdateIncidentModal(incident))
                        {
                            if (scheduleEvent != null)
                            {
                                scheduleEvent.Status = ((int)ScheduleStatus.ReportAndNotesReturned).ToString();
                                scheduleEvent.ReturnReason = reason;
                                if (userEvent != null)
                                {
                                    userEvent.Status = ((int)ScheduleStatus.ReportAndNotesReturned).ToString();
                                    userEvent.ReturnReason = reason;
                                }
                                shouldUpdateEpisode = true;
                                description = "Returned by " + Current.UserFullName;
                                action = Actions.Returned;
                            }
                        }
                    }
                    else if (button == "Print")
                    {
                        if (scheduleEvent != null)
                        {
                            scheduleEvent.InPrintQueue = false;
                            shouldUpdateEpisode = true;
                        }
                    }

                    if (scheduleEvent != null && shouldUpdateEpisode)
                    {
                        if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                        {
                            if (userEvent != null)
                            {
                                if (userRepository.UpdateEvent(Current.AgencyId, userEvent))
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                userRepository.AddUserEvent(Current.AgencyId, patientId, scheduleEvent.UserId, new UserEvent { EventId = scheduleEvent.EventId, PatientId = scheduleEvent.PatientId, EventDate = scheduleEvent.EventDate, Discipline = scheduleEvent.Discipline, DisciplineTask = scheduleEvent.DisciplineTask, EpisodeId = scheduleEvent.EpisodeId, Status = scheduleEvent.Status, TimeIn = scheduleEvent.TimeIn, TimeOut = scheduleEvent.TimeOut, UserId = scheduleEvent.UserId, IsMissedVisit = scheduleEvent.IsMissedVisit, ReturnReason = scheduleEvent.ReturnReason });
                                result = true;
                            }
                            Auditor.Log(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                        }
                    }
                }
            }
            return result;
        }

        public List<SelectListItem> Insurances(string value, bool IsAll, bool IsMedicareTradIncluded)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (IsMedicareTradIncluded)
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem
                            {
                                Text = standardInsurance.Name,
                                Value = standardInsurance.Id.ToString(),
                                Selected = (standardInsurance.Id.ToString().IsEqual(value))
                            });
                        }
                    }
                }
            }
            if (IsAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "All",
                    Value = "0"
                });
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            return items;
        }

        public List<SelectListItem> Branchs(string value, bool IsAll)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var branches = agencyRepository.GetBranches(Current.AgencyId);
            tempItems = from branch in branches
                        select new SelectListItem
                        {
                            Text = branch.Name,
                            Value = branch.Id.ToString(),
                            Selected = (branch.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            if (IsAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "-- All Branches --",
                    Value = Guid.Empty.ToString(),
                });
            }
            return items;
        }

        public Infection GetInfectionReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
            if (infection != null)
            {
                infection.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                infection.Patient = patientRepository.GetPatientOnly(infection.PatientId, Current.AgencyId);

                if (!infection.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, infection.EpisodeId, infection.PatientId);
                    if (episode != null)
                    {
                        infection.EpisodeEndDate = episode.EndDateFormatted;
                        infection.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
                if (!infection.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(infection.PhysicianId, Current.AgencyId);
                    if (physician != null) infection.PhysicianName = physician.DisplayName;
                }
            }
            return infection;
        }

        public Incident GetIncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
            if (incident != null)
            {
                incident.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                incident.Patient = patientRepository.GetPatientOnly(incident.PatientId, incident.AgencyId);
                if (!incident.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, incident.EpisodeId, incident.PatientId);
                    if (episode != null)
                    {
                        incident.EpisodeEndDate = episode.EndDateFormatted;
                        incident.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
                if (!incident.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(incident.PhysicianId, Current.AgencyId);
                    if (physician != null) incident.PhysicianName = physician.DisplayName;
                }
            }
            return incident;
        }

        public List<PatientEpisodeEvent> GetPrintQueue()
        {
            var events = new List<PatientEpisodeEvent>();
            var agencyPatientEpisodes = patientRepository.GetPatientEpisodeData(Current.AgencyId);
            agencyPatientEpisodes.ForEach(e =>
            {
                if (e.Schedule.IsNotNullOrEmpty())
                {
                    var scheduledEvents = e.Schedule.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() && s.IsDeprecated == false && s.IsMissedVisit == false && s.InPrintQueue == true);
                    if (scheduledEvents != null)
                    {
                        scheduledEvents.ForEach(s =>
                        {
                            events.Add(new PatientEpisodeEvent
                            {
                                Status = s.StatusName,
                                PatientName = e.PatientName,
                                TaskName = s.DisciplineTaskName,
                                PrintUrl = Url.Download(s, false),
                                UserName = UserEngine.GetName(s.UserId, Current.AgencyId),
                                EventDate = s.EventDate.IsNotNullOrEmpty() && s.EventDate.IsValidDate() ? s.EventDate.ToZeroFilled() : "",
                                CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
                            });
                        });
                    }
                }
                e.Schedule = string.Empty;
            });
            return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }
    }
}
