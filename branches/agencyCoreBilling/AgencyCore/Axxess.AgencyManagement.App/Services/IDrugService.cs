﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;

    using LexiData;

    public interface IDrugService
    {
        IList<Drug> DrugsStartsWith(string query, int limit);
        List<DrugDrugInteractionResult> GetDrugDrugInteractions(List<string> drugs);
    }
}
