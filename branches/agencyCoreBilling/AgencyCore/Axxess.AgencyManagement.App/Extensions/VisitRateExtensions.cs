﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    public static class VisitRateExtensions
    {
        public static IDictionary<string, ChargeRate> ToChargeRateDictionary(this AgencyInsurance agencyInsurance)
        {
            IDictionary<string, ChargeRate> questions = new Dictionary<string, ChargeRate>();
            if (agencyInsurance != null && agencyInsurance.Charge.IsNotNullOrEmpty())
            {
                var chargeRateQuestions = agencyInsurance.Charge.ToObject<List<ChargeRate>>();
                if (chargeRateQuestions != null && chargeRateQuestions.Count > 0)
                {
                    chargeRateQuestions.ForEach(n =>
                    {
                        questions.Add(n.RateDiscipline, n);
                    });
                }
            }
            return questions;
        }

        public static IDictionary<string, Locator> ToLocatorDictionary(this AgencyInsurance agencyInsurance)
        {
            IDictionary<string, Locator> questions = new Dictionary<string, Locator>();
            if (agencyInsurance != null && agencyInsurance.Ub04Locator81cca.IsNotNullOrEmpty())
            {
                var locatorQuestions = agencyInsurance.Ub04Locator81cca.ToObject<List<Locator>>();
                if (locatorQuestions != null && locatorQuestions.Count > 0)
                {
                    locatorQuestions.ForEach(n =>
                    {
                        questions.Add(n.LocatorId, n);
                    });
                }
            }
            return questions;
        }

        public static IDictionary<string, CostRate> ToCostRateDictionary(this AgencyLocation agencyLocation)
        {
            IDictionary<string, CostRate> questions = new Dictionary<string, CostRate>();
            if (agencyLocation != null && agencyLocation.Cost.IsNotNullOrEmpty())
            {
                var costRateQuestions = agencyLocation.Cost.ToObject<List<CostRate>>();
                if (costRateQuestions != null && costRateQuestions.Count > 0)
                {
                    costRateQuestions.ForEach(n =>
                    {
                        questions.Add(n.RateDiscipline, n);
                    });
                }
            }
            return questions;
        }

    }
}
