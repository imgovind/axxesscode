﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using Security;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public static class SecurityExtensions
    {
        public static SingleUser ToSingleUser(this AxxessPrincipal principal)
        {
            SingleUser singleUser = null;
            if (principal != null)
            {
                AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                if (identity != null)
                {
                    singleUser = new SingleUser()
                    {
                        LoginId = identity.Id,
                        EmailAddress = identity.Name,
                        IpAddress = Current.IpAddress,
                        SessionId = SessionStore.SessionId,
                        FullName = identity.Session.FullName,
                        AgencyName = identity.Session.AgencyName,
                        IsAuthenticated = identity.IsAuthenticated,
                        LastSecureActivity = identity.LastSecureActivity
                    };
                }
            }
            return singleUser;
        }

        public static bool IsImpersonated(this AxxessPrincipal principal)
        {
            var result = false;
            if (principal != null)
            {
                AxxessIdentity identity = (AxxessIdentity)principal.Identity;
                if (identity != null && identity.IsImpersonated == true)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}

