﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Telerik.Web.Mvc;
    using Services;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class LookUpController : BaseController
    {
        #region Constructor
        private readonly IDrugService drugService;
        private readonly ILookupRepository lookupRepository;

        public LookUpController(ILookUpDataProvider lookupDataProvider, IDrugService drugService)
        {
            Check.Argument.IsNotNull(drugService, "drugService");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");

            this.drugService = drugService;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
        }

        #endregion

        #region Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Weather(string format, string callback, string diagnostics, string env, string q)
        {
            var parameters = new StringBuilder();
            parameters.AppendFormat("?format={0}", format);
            parameters.AppendFormat("&diagnostics={0}", diagnostics);
            parameters.AppendFormat("&callback={0}", callback);
            parameters.AppendFormat("&diagnostics={0}", diagnostics);
            parameters.AppendFormat("&env={0}", env);
            parameters.AppendFormat("&q={0}", q);
            return Content(GetWeather(parameters.ToString()), "application/json");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ProxyResult WeatherImage(string parameter)
        {
            return new ProxyResult(string.Format(AppSettings.WeatherImageUriFormat, parameter));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ProxyResult WeatherForecastImage(string parameter)
        {
            return new ProxyResult(string.Format(AppSettings.WeatherForecastImageUriFormat, parameter));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ProxyResult WeatherImageThumbnail(string parameter)
        {
            return new ProxyResult(string.Format(AppSettings.WeatherImageThumbnailUriFormat, parameter));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult States()
        {
            return Json(lookupRepository.States());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult Races()
        {
            return Json(lookupRepository.Races());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult PaymentSources()
        {
            return Json(lookupRepository.PaymentSources());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult DrugClassifications()
        {
            return Json(lookupRepository.DrugClassifications());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult ReferralSources()
        {
            return Json(lookupRepository.ReferralSources());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult SupplyCategories()
        {
            return Json(lookupRepository.SupplyCategories());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult AdmissionSources()
        {
            return Json(lookupRepository.AdmissionSources());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DisciplineTasks(string Discipline)
        {
            return Json(lookupRepository.DisciplineTasks(Discipline));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult MultipleDisciplineTasks()
        {
            return Json(lookupRepository.DisciplineTasks().Where(d => d.IsMultiple).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DiagnosisCodes()
        {
            var gridModel = new GridModel();
            var pageNumber = this.HttpContext.Request.Params["page"];
            var diagnosisCodes = lookupRepository.DiagnosisCodes();
            if (diagnosisCodes != null && pageNumber.HasValue())
            {
                int page = int.Parse(pageNumber);
                gridModel.Data = diagnosisCodes.Skip((page - 1) * 13).Take(13);
                gridModel.Total = diagnosisCodes.Count;
            }
            return Json(gridModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DiagnosisCode(String term, String type)
        {
            var diagnosisCodes = new List<DiagnosisCode>();
            if (type.ToUpper() == "ICD") diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.Code.StartsWith(term.ToUpper().Trim())).ToList();
            else if (type.ToUpper() == "DIAGNOSIS") diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.ShortDescription.ToLower().Contains(term.ToLower())).ToList();
            return Json(diagnosisCodes);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DiagnosisCodeNoVE(String term, String type)
        {
            var diagnosisCodes = new List<DiagnosisCode>();
            if (type.ToUpper() == "ICD") diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.Code.StartsWith(term.ToUpper().Trim())).Where(s => !s.Code.StartsWith("V") && !s.Code.StartsWith("E")).ToList();
            else if (type.ToUpper() == "DIAGNOSIS") diagnosisCodes = lookupRepository.DiagnosisCodes().Where(p => p.ShortDescription.ToLower().Contains(term.ToLower())).Where(s => !s.Code.StartsWith("V") && !s.Code.StartsWith("E")).ToList();
            return Json(diagnosisCodes);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Supplies(string q, int limit, string type)
        {
            if (type == "Code")
            {
                var supplies = lookupRepository.Supplies().Where(p => p.Code.Contains
                    (q.ToUpper())).Select(p => new { p.Description, p.Code }).Take(limit).ToList();
                return Json(supplies);
            }
            else if (type == "Description")
            {
                var supplies = lookupRepository.Supplies().Where(p => p.Description.ToLower().Contains(q.ToLower())).Select(p => new { p.Description, p.Code }).Take(limit).ToList();
                return Json(supplies);
            }

            return Json(null);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SuppliesGrid(string q, int limit, string type)
        {
            if (q != string.Empty)
            {
                if (type == "Code")
                {
                    var supplies = lookupRepository.Supplies().Where(p => p.Code.StartsWith
                        (q.ToUpper())).Take(limit).ToList();
                    return View(new GridModel(supplies));
                }
                else if (type == "Description")
                {
                    var supplies = lookupRepository.Supplies().Where(p => p.Description.ToLower().Contains(q.ToLower())).Take(limit).ToList();
                    return View(new GridModel(supplies));
                }
            }
            var list = new List<Supply>();
            return View(new GridModel(list));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "None")]
        public JsonResult ProcedureCodes()
        {
            return Json(lookupRepository.ProcedureCodes());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ProcedureCode(String term, String type)
        {
            var procedureCodes = new List<ProcedureCode>();
            if (type.ToUpper() == "PROCEDUREICD") procedureCodes = lookupRepository.ProcedureCodes().Where(p => p.Code.StartsWith(term)).ToList();
            else if (type.ToUpper() == "PROCEDURE") procedureCodes = lookupRepository.ProcedureCodes().Where(p => p.ShortDescription.ToLower().Contains(term.ToLower())).ToList();
            return Json(procedureCodes);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Npi(string npiNumber)
        {
            return Json(lookupRepository.GetNpiData(npiNumber));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Npis(String term)
        {
            return Json(lookupRepository.GetNpis(term, 20));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NewShortGuid()
        {
            return Json(new { text = ShortGuid.NewId().ToString() });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationRoute(String term)
        {
            return Json(lookupRepository.MedicationRoute(term, 20));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationClassification(String term)
        {
            return Json(lookupRepository.MedicationClassification(term, 20));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MedicationDosage(string q, int limit)
        {
            return Json(lookupRepository.MedicationDosage(q, limit));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Drugs(String term)
        {
            return Json(drugService.DrugsStartsWith(term, 20));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Physicians(String term)
        {
            IEnumerable<SelectListItem> physicianItems = new List<SelectListItem>();
            if (term.IsNotNullOrEmpty())
            {
                var physicians = PhysicianEngine.AsList(Current.AgencyId);
                if (physicians != null)
                {
                    physicians = physicians.Where(p => p.DisplayName.ToLowerInvariant().Contains(term.ToLowerInvariant())).Take(100).ToList();

                    if (physicians.Count > 0)
                    {
                        physicianItems = from physician in physicians
                                         select new SelectListItem
                                         {
                                             Text = physician.DisplayName,
                                             Value = physician.Id.ToString()
                                         };
                    }
                }
            }
            
            return Json(physicianItems.ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PhysicianName(Guid id)
        {
            var physician = PhysicianEngine.Get(id, Current.AgencyId);
            if (physician != null)
            {
                String DisplayName = physician.DisplayName;
                return Json(DisplayName);
            }
            else return Json(string.Empty);
        }

        #endregion

        #region Private Methods

        private string GetWeather(string parameters)
        {
            if (AppSettings.GetRemoteContent.ToBoolean())
            {
                string jsonResult = string.Empty;
                string weatherUrl = string.Concat(AppSettings.YahooWeatherUri, parameters);
                Uri weatherUri = new Uri(weatherUrl, UriKind.Absolute);

                HttpWebRequest webRequest = (HttpWebRequest)System.Net.WebRequest.Create(weatherUri);
                HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        jsonResult = sr.ReadToEnd();
                    }
                }
                return string.Format("{0}", jsonResult);
            }
            else return string.Empty;
        }

        #endregion
    }
}
