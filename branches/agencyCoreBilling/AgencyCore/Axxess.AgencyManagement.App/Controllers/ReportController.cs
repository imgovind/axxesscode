﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Telerik.Web.Mvc;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Services;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Enums;


    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ReportController : BaseController
    {
        #region Constructor / Member

        private readonly IAgencyService agencyService;
        private readonly IReportService reportService;
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IBillingService billingService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IAgencyRepository agencyRepository;
        public ReportController(IAgencyManagementDataProvider dataProvider, IReportService reportService, IPatientService patientService, IBillingService billingService, IAgencyService agencyService)
        {
            Check.Argument.IsNotNull(reportService, "reportService");
            this.agencyService = agencyService;
            this.reportService = reportService;
            this.patientService = patientService;
            this.billingService = billingService;
            this.userRepository = dataProvider.UserRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
        }

        #endregion

        #region ReportController Actions

        #region General Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Dashboard()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Patient()
        {
            return PartialView("Patient/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Clinical()
        {
            return PartialView("Clinical/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Schedule()
        {
            return PartialView("Schedule/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Billing()
        {
            return PartialView("Billing/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Employee()
        {
            return PartialView("Employee/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Statistical()
        {
            return PartialView("Statistical/Home");
        }

        #endregion

        #region Patient Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientRoster()
        {
            return PartialView("Patient/Roster");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientRoster(Guid BranchCode, int StatusId, int InsuranceId)
        {
            return View(new GridModel(reportService.GetPatientRoster(BranchCode, StatusId, InsuranceId)));
        }

        public ActionResult ExportPatientRoster(Guid BranchCode, int StatusId, int InsuranceId)
        {
            var patientRosters = reportService.GetPatientRoster(BranchCode, StatusId, InsuranceId);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Roster";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientRoster");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Roster");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR #");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Medicare #");
            headerRow.CreateCell(3).SetCellValue("Address");
            headerRow.CreateCell(4).SetCellValue("City");
            headerRow.CreateCell(5).SetCellValue("State");
            headerRow.CreateCell(6).SetCellValue("Zip Code");
            headerRow.CreateCell(7).SetCellValue("Home Phone");
            headerRow.CreateCell(8).SetCellValue("Gender");
            headerRow.CreateCell(9).SetCellValue("Triage");
            headerRow.CreateCell(10).SetCellValue("Date of Birth");
            headerRow.CreateCell(11).SetCellValue("Start of Care Date");
            headerRow.CreateCell(12).SetCellValue("Discharge Date");
            headerRow.CreateCell(13).SetCellValue("Physician");
            headerRow.CreateCell(14).SetCellValue("Physician NPI");
            headerRow.CreateCell(15).SetCellValue("Physician Phone");
            sheet.CreateFreezePane(0, 2, 0, 2);

            int rowNumber = 2;

            foreach (var patient in patientRosters)
            {
                var row = sheet.CreateRow(rowNumber++);
                row.CreateCell(0).SetCellValue(patient.PatientId);
                row.CreateCell(1).SetCellValue(patient.PatientDisplayName);
                row.CreateCell(2).SetCellValue(patient.PatientMedicareNumber);
                row.CreateCell(3).SetCellValue(patient.PatientAddressLine1);
                row.CreateCell(4).SetCellValue(patient.PatientAddressCity);
                row.CreateCell(5).SetCellValue(patient.PatientAddressStateCode);
                row.CreateCell(6).SetCellValue(patient.PatientAddressZipCode);
                row.CreateCell(7).SetCellValue(patient.PatientPhone);
                row.CreateCell(8).SetCellValue(patient.PatientGender);
                row.CreateCell(9).SetCellValue(patient.Triage);
                row.CreateCell(10).SetCellValue(patient.PatientDOB.ToString("MM/dd/yyyy"));
                row.CreateCell(11).SetCellValue(patient.PatientSoC.ToZeroFilled());
                row.CreateCell(12).SetCellValue(patient.PatientDischargeDate.ToZeroFilled());
                row.CreateCell(13).SetCellValue(patient.PhysicianName);
                row.CreateCell(14).SetCellValue(patient.PhysicianNpi);
                row.CreateCell(15).SetCellValue(patient.PhysicianPhone);
            }
            var totalRow = sheet.CreateRow(rowNumber++);
            totalRow.CreateCell(0).SetCellValue(string.Format("Total Number of Patients: {0}", patientRosters.Count));
            int columnCounter = 0;
            do
            {
                sheet.AutoSizeColumn(columnCounter);
                columnCounter++;
            }
            while (columnCounter < 16);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientRoster_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Cahps()
        {
            return PartialView("Patient/Cahps");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientEmergencyList()
        {
            return PartialView("Patient/EmergencyList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientEmergencyList(Guid BranchCode, int StatusId)
        {
            return View(new GridModel(reportService.GetPatientEmergencyContacts(StatusId, BranchCode)));
        }

        public ActionResult ExportPatientEmergencyList(Guid BranchCode, int StatusId)
        {
            var emergencyContactInfos = reportService.GetPatientEmergencyContacts(StatusId, BranchCode);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Emergency List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientEmergencyList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Emergency List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Triage");
            headerRow.CreateCell(3).SetCellValue("Contact");
            headerRow.CreateCell(4).SetCellValue("Relationship");
            headerRow.CreateCell(5).SetCellValue("Contact Phone");
            headerRow.CreateCell(6).SetCellValue("Contact E-mail");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (emergencyContactInfos != null && emergencyContactInfos.Count > 0)
            {
                int rowNumber = 2;
                foreach (var emergencyContactInfo in emergencyContactInfos)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(emergencyContactInfo.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(emergencyContactInfo.PatientName);
                    row.CreateCell(2).SetCellValue(emergencyContactInfo.Triage);
                    row.CreateCell(3).SetCellValue(emergencyContactInfo.ContactName);
                    row.CreateCell(4).SetCellValue(emergencyContactInfo.ContactRelation);
                    row.CreateCell(5).SetCellValue(emergencyContactInfo.ContactPhoneHome);
                    row.CreateCell(6).SetCellValue(emergencyContactInfo.ContactEmailAddress);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Emergency List: {0}", emergencyContactInfos.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientEmergencyList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientBirthdayList()
        {
            return PartialView("Patient/BirthdayList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientBirthdayList(Guid BranchId, int month)
        {
            return View(new GridModel(patientRepository.GetPatientBirthdays(Current.AgencyId, BranchId, month)));
        }

        public ActionResult ExportPatientBirthdayList(Guid BranchId, int month)
        {
            var birthDays = patientRepository.GetPatientBirthdays(Current.AgencyId, BranchId, month);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Birthday List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientBirthdayList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Birthday List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (new DateTime(DateTime.Now.Year, month, 1).ToString("MMMM"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Age");
            headerRow.CreateCell(3).SetCellValue("Birth Day");
            headerRow.CreateCell(4).SetCellValue("Address First Row");
            headerRow.CreateCell(5).SetCellValue("Address Second Row");
            headerRow.CreateCell(6).SetCellValue("Home Phone");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (birthDays != null && birthDays.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in birthDays)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.IdNumber);
                    row.CreateCell(1).SetCellValue(birthDay.Name);
                    row.CreateCell(2).SetCellValue(birthDay.Age);
                    row.CreateCell(3).SetCellValue(birthDay.BirthDay);
                    row.CreateCell(4).SetCellValue(birthDay.AddressFirstRow);
                    row.CreateCell(5).SetCellValue(birthDay.AddressSecondRow);
                    row.CreateCell(6).SetCellValue(birthDay.PhoneHome);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Birthday List: {0}", birthDays.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientBirthdayList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientAddressList()
        {
            return PartialView("Patient/AddressList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientAddressList(Guid BranchId, int StatusId)
        {
            return View(new GridModel(patientRepository.GetPatientAddressListing(Current.AgencyId, BranchId, StatusId)));
        }

        public ActionResult ExportPatientAddressList(Guid BranchId, int StatusId)
        {
            var addressBookEntries = patientRepository.GetPatientAddressListing(Current.AgencyId, BranchId, StatusId);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Address List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientAddressList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Address List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Address");
            headerRow.CreateCell(3).SetCellValue("City");
            headerRow.CreateCell(4).SetCellValue("State");
            headerRow.CreateCell(5).SetCellValue("Zip Code");
            headerRow.CreateCell(6).SetCellValue("Home Phone");
            headerRow.CreateCell(7).SetCellValue("Home Phone");
            headerRow.CreateCell(8).SetCellValue("Email Address");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (addressBookEntries != null && addressBookEntries.Count > 0)
            {
                int rowNumber = 2;
                foreach (var addressBook in addressBookEntries)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(addressBook.IdNumber);
                    row.CreateCell(1).SetCellValue(addressBook.Name);
                    row.CreateCell(2).SetCellValue(addressBook.AddressFirstRow);
                    row.CreateCell(3).SetCellValue(addressBook.AddressCity);
                    row.CreateCell(4).SetCellValue(addressBook.AddressStateCode);
                    row.CreateCell(5).SetCellValue(addressBook.AddressZipCode);
                    row.CreateCell(6).SetCellValue(addressBook.PhoneHome);
                    row.CreateCell(7).SetCellValue(addressBook.PhoneMobile);
                    row.CreateCell(8).SetCellValue(addressBook.EmailAddress);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Address List: {0}", addressBookEntries.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientBirthdayList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientByPhysicians()
        {
            return PartialView("Patient/Physician");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientByPhysicians(Guid AgencyPhysicianId)
        {
            if (AgencyPhysicianId.IsEmpty())
            {
                return View(new GridModel(new List<PatientRoster>()));
            }
            return View(new GridModel(patientRepository.GetPatientByPhysician(Current.AgencyId, AgencyPhysicianId)));
        }

        public ActionResult ExportPatientByPhysicians(Guid AgencyPhysicianId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patients By Physician";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientByPhysicians");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patients By Physician");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var physician = PhysicianEngine.Get(AgencyPhysicianId, Current.AgencyId);
            if (physician != null)
            {
                titleRow.CreateCell(3).SetCellValue(string.Format("Physician : {0}", physician.DisplayName));
            }

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Address");
            headerRow.CreateCell(2).SetCellValue("City");
            headerRow.CreateCell(3).SetCellValue("State");
            headerRow.CreateCell(4).SetCellValue("Zip Code");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            headerRow.CreateCell(6).SetCellValue("Patient Gender");
            sheet.CreateFreezePane(0, 1, 0, 1);

            var patientRosters = patientRepository.GetPatientByPhysician(Current.AgencyId, AgencyPhysicianId);
            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientDisplayName);
                    row.CreateCell(1).SetCellValue(patient.PatientAddressLine1);
                    row.CreateCell(2).SetCellValue(patient.PatientAddressCity);
                    row.CreateCell(3).SetCellValue(patient.PatientAddressStateCode);
                    row.CreateCell(4).SetCellValue(patient.PatientAddressZipCode);
                    row.CreateCell(5).SetCellValue(patient.PatientPhone);
                    row.CreateCell(6).SetCellValue(patient.PatientGender);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patients By Physician: {0}", patientRosters.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientByPhysicians_{0}.xls", DateTime.Now.Ticks));
        }

        [GridAction]
        public JsonResult PatientBirthdayWidget()
        {
            var viewData = new List<Birthday>();
            viewData = reportService.GetCurrentBirthdays();
            return Json(new GridModel(viewData));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSocCertPeriodListing()
        {
            return PartialView("Patient/PatientSocCertPeriodListing");
        }

        [GridAction]
        public ActionResult PatientSocCertPeriodListingResult(int StatusId, Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.GetPatientSocCertPeriod(BranchId, StatusId ,StartDate, EndDate)));
        }

        public ActionResult ExportPatientSocCertPeriodListing(int StatusId, Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient SOC Cert. Period Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientSocCertPeriodListing");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient SOC Cert. Period Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("SOC Date");
            headerRow.CreateCell(4).SetCellValue("SOC Cert. Period");
            headerRow.CreateCell(5).SetCellValue("Physician Name");
            headerRow.CreateCell(6).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var patientSocCertPeriods = reportService.GetPatientSocCertPeriod(BranchId, StatusId ,StartDate, EndDate);
            if (patientSocCertPeriods != null && patientSocCertPeriods.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patientSocCertPeriod in patientSocCertPeriods)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patientSocCertPeriod.PatientPatientID);
                    row.CreateCell(1).SetCellValue(patientSocCertPeriod.PatientLastName);
                    row.CreateCell(2).SetCellValue(patientSocCertPeriod.PatientFirstName);
                    row.CreateCell(3).SetCellValue(patientSocCertPeriod.PatientSoC);
                    row.CreateCell(4).SetCellValue(patientSocCertPeriod.SocCertPeriod);
                    row.CreateCell(5).SetCellValue(patientSocCertPeriod.PhysicianName);
                    row.CreateCell(6).SetCellValue(patientSocCertPeriod.respEmp);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient SOC Cert. Period Listing: {0}", patientSocCertPeriods.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientSocCertPeriodListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientOnCallListing()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult PatientOnCallListingResult(Guid Name, string AddressStateCode, Guid AddressBranchCode, string ReportStep)
        {
            if (ReportStep != "2")
            {
                return View(new GridModel(new List<PatientOnCallListing>()));
            }
            var viewData = new List<PatientOnCallListing>();
            viewData = reportService.GetPatientOnCallListing(new DateTime(1936, 1, 1), DateTime.Now);
            return View(new GridModel(viewData));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientByResponsibleEmployeeListing()
        {
            return PartialView("Patient/PatientByResponsibleEmployeeListing");
        }

        [GridAction]
        public ActionResult PatientByResponsibleEmployeeListingResult(Guid UserId, Guid BranchId, int StatusId)
        {
            var viewData = new List<PatientRoster>();
            if (UserId.IsEmpty())
            {
                return View(new GridModel(viewData));
            }
            viewData = patientRepository.GetPatientByResponsiableEmployee(Current.AgencyId, UserId, BranchId, StatusId);
            return View(new GridModel(viewData));
        }

        public ActionResult ExportPatientByResponsibleEmployeeListing(Guid UserId, Guid BranchId, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient By Responsible Employee Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientByResponsibleEmployeeListing");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient By Responsible Employee Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Employee : {0}", UserEngine.GetName(UserId, Current.AgencyId)));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Address");
            headerRow.CreateCell(4).SetCellValue("SOC Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (UserId.IsEmpty())
            {
                MemoryStream outputNoUser = new MemoryStream();
                workbook.Write(outputNoUser);
                return File(outputNoUser.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleEmployeeListing_{0}.xls", DateTime.Now.Ticks));
            }

            var patientRosters = patientRepository.GetPatientByResponsiableEmployee(Current.AgencyId, UserId, BranchId, StatusId);
            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientId);
                    row.CreateCell(1).SetCellValue(patient.PatientLastName);
                    row.CreateCell(2).SetCellValue(patient.PatientFirstName);
                    row.CreateCell(3).SetCellValue(patient.AddressFull);
                    row.CreateCell(4).SetCellValue(patient.PatientSoC);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient By Responsible Employee Listing: {0}", patientRosters.Count));

            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleEmployeeListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientByResponsibleCaseManagerListing()
        {
            return PartialView("Patient/PatientByResponsibleCaseManager");
        }

        [GridAction]
        public ActionResult PatientByResponsibleCaseManager(Guid CaseManagerId, Guid BranchId, int StatusId)
        {
            var viewData = new List<PatientRoster>();
            if (CaseManagerId.IsEmpty())
            {
                return View(new GridModel(viewData));
            }
            viewData = patientRepository.GetPatientByResponsiableByCaseManager(Current.AgencyId, CaseManagerId, BranchId, StatusId);
            return View(new GridModel(viewData));
        }

        public ActionResult ExportPatientByResponsibleCaseManager(Guid CaseManagerId, Guid BranchId, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient By Responsible CaseManager";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientByResponsibleCaseManager");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient By Responsible CaseManager");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Case Manager : {0}", UserEngine.GetName(CaseManagerId, Current.AgencyId)));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Address");
            headerRow.CreateCell(4).SetCellValue("SOC Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (CaseManagerId.IsEmpty())
            {
                MemoryStream outputNoUser = new MemoryStream();
                workbook.Write(outputNoUser);
                return File(outputNoUser.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleCaseManager_{0}.xls", DateTime.Now.Ticks));
            }

            var patientRosters = patientRepository.GetPatientByResponsiableByCaseManager(Current.AgencyId, CaseManagerId, BranchId, StatusId);

            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientId);
                    row.CreateCell(1).SetCellValue(patient.PatientLastName);
                    row.CreateCell(2).SetCellValue(patient.PatientFirstName);
                    row.CreateCell(3).SetCellValue(patient.AddressFull);
                    row.CreateCell(4).SetCellValue(patient.PatientSoC);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient By Responsible CaseManager: {0}", patientRosters.Count));

            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleCaseManager_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientExpiringAuthorizations()
        {
            return PartialView("Patient/ExpiringAuthorizations");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpiringAuthorizations(Guid BranchCode, int Status)
        {
            return View(new GridModel(reportService.GetExpiringAuthorizaton(BranchCode, Status)));
        }

        public ActionResult ExportExpiringAuthorizations(Guid BranchCode, int Status)
        {
            var autorizations = reportService.GetExpiringAuthorizaton(BranchCode, Status);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Expiring Authorizations";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ExpiringAuthorizations");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Expiring Authorizations");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Number");
            headerRow.CreateCell(3).SetCellValue("Start Date");
            headerRow.CreateCell(4).SetCellValue("End Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (autorizations != null && autorizations.Count > 0)
            {
                int rowNumber = 2;
                foreach (var auto in autorizations)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(auto.DisplayName);
                    row.CreateCell(1).SetCellValue(auto.Status);
                    row.CreateCell(2).SetCellValue(auto.Number);
                    row.CreateCell(3).SetCellValue(auto.StartDateFormatted);
                    row.CreateCell(4).SetCellValue(auto.EndDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Expiring Authorizations: {0}", autorizations.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ExpiringAuthorizations_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSurveyCensus()
        {
            return PartialView("Patient/SurveyCensus");
        }

        [GridAction]
        public ActionResult PatientSurveyCensus(Guid BranchId, int Status)
        {
            return View(new GridModel(reportService.GetPatientSurveyCensus(BranchId, Status)));
        }

        public ActionResult ExportPatientSurveyCensus(Guid BranchId, int Status)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Survey Census";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientSurveyCensus");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Survey Census");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR #");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Medicare #");
            headerRow.CreateCell(3).SetCellValue("SSN");
            headerRow.CreateCell(4).SetCellValue("DOB");
            headerRow.CreateCell(5).SetCellValue("SOC Date");
            headerRow.CreateCell(6).SetCellValue("Cert. Period");
            headerRow.CreateCell(7).SetCellValue("Insurance");
            headerRow.CreateCell(8).SetCellValue("Primary Diagnosis");
            headerRow.CreateCell(9).SetCellValue("Secondary Diagnosis");
            headerRow.CreateCell(10).SetCellValue("Triage");
            headerRow.CreateCell(11).SetCellValue("Disciplines");
            headerRow.CreateCell(12).SetCellValue("Phone");
            headerRow.CreateCell(13).SetCellValue("Address Line1");
            headerRow.CreateCell(14).SetCellValue("Address Line2");
            headerRow.CreateCell(15).SetCellValue("Physician");
            headerRow.CreateCell(16).SetCellValue("NPI");
            headerRow.CreateCell(17).SetCellValue("Physician Phone");
            headerRow.CreateCell(18).SetCellValue("Physician Fax");
            headerRow.CreateCell(19).SetCellValue("Case Manager");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var surveyCensuses = reportService.GetPatientSurveyCensus(BranchId, Status);
            if (surveyCensuses != null && surveyCensuses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var surveyCensus in surveyCensuses)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(surveyCensus.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(surveyCensus.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(surveyCensus.MedicareNumber);
                    row.CreateCell(3).SetCellValue(surveyCensus.SSN);
                    row.CreateCell(4).SetCellValue(surveyCensus.DOB.ToString("MM/dd/yyyy"));
                    row.CreateCell(5).SetCellValue(surveyCensus.SOC.ToString("MM/dd/yyyy"));
                    row.CreateCell(6).SetCellValue(surveyCensus.CertPeriod);
                    row.CreateCell(7).SetCellValue(surveyCensus.InsuranceName);
                    row.CreateCell(8).SetCellValue(surveyCensus.PrimaryDiagnosis);
                    row.CreateCell(9).SetCellValue(surveyCensus.SecondaryDiagnosis);
                    row.CreateCell(10).SetCellValue(surveyCensus.Triage);
                    row.CreateCell(11).SetCellValue(surveyCensus.Discipline);
                    row.CreateCell(12).SetCellValue(surveyCensus.Phone);
                    row.CreateCell(13).SetCellValue(surveyCensus.DisplayAddressLine1);
                    row.CreateCell(14).SetCellValue(surveyCensus.DisplayAddressLine2);
                    row.CreateCell(15).SetCellValue(surveyCensus.PhysicianDisplayName);
                    row.CreateCell(16).SetCellValue(surveyCensus.PhysicianNPI);
                    row.CreateCell(17).SetCellValue(surveyCensus.PhysicianPhone );
                    row.CreateCell(18).SetCellValue(surveyCensus.PhysicianFax);
                    row.CreateCell(19).SetCellValue(surveyCensus.CaseManagerDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Survey Census: {0}", surveyCensuses.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
            sheet.AutoSizeColumn(12);
            sheet.AutoSizeColumn(13);
            sheet.AutoSizeColumn(14);
            sheet.AutoSizeColumn(15);
            sheet.AutoSizeColumn(16);
            sheet.AutoSizeColumn(17);
            sheet.AutoSizeColumn(18);
            sheet.AutoSizeColumn(19);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientSurveyCensus_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientVitalSigns()
        {
            return PartialView("Patient/VitalSigns");
        }

        [GridAction]
        public ActionResult PatientVitalSigns(Guid patientId, DateTime StartDate, DateTime EndDate)
        {
            if (patientId.IsEmpty())
            {
                return View(new GridModel(new List<VitalSign>()));
            }
            return View(new GridModel(patientService.GetPatientVitalSigns(patientId, StartDate, EndDate)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSixtyDaySummary()
        {
            return PartialView("Patient/SixtyDaySummary");
        }

        [GridAction]
        public ActionResult PatientSixtyDaySummary(Guid patientId)
        {
            if (patientId.IsEmpty())
            {
                return View(new GridModel(new List<VisitNoteViewData>()));
            }
            return View(new GridModel(patientService.GetSixtyDaySummary(patientId)));
        }

        public ActionResult ExportPatientSixtyDaySummary(Guid patientId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Sixty Day Summary";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("SixtyDaySummary");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Sixty Day Summary");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee Name");
            headerRow.CreateCell(1).SetCellValue("Visit Date");
            headerRow.CreateCell(2).SetCellValue("Signature Date");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("Physician Name");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var sixtyDaySummaries = patientService.GetSixtyDaySummary(patientId);
            if (sixtyDaySummaries != null && sixtyDaySummaries.Count > 0)
            {
                int rowNumber = 2;
                foreach (var sixtyDaySummary in sixtyDaySummaries)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(sixtyDaySummary.UserDisplayName);
                    row.CreateCell(1).SetCellValue(sixtyDaySummary.VisitDate);
                    row.CreateCell(2).SetCellValue(sixtyDaySummary.SignatureDate);
                    row.CreateCell(3).SetCellValue(sixtyDaySummary.EpisodeRange);
                    row.CreateCell(4).SetCellValue(sixtyDaySummary.PhysicianDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Sixty Day Summary: {0}", sixtyDaySummaries.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientSixtyDaySummary_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Clinical Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalOpenOasis()
        {
            return PartialView("Clinical/OpenOasis");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalOpenOasis(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.GetAllOpenOasis(BranchId, StartDate, EndDate)));
        }

        public ActionResult ExportClinicalOpenOasis(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Open OASIS";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("OpenOASIS");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Open OASIS");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("MR#");
            headerRow.CreateCell(2).SetCellValue("Assessment Type");
            headerRow.CreateCell(3).SetCellValue("Date");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var openOasis = reportService.GetAllOpenOasis(BranchId, StartDate, EndDate);
            if (openOasis != null && openOasis.Count > 0)
            {
                int rowNumber = 2;
                foreach (var oasis in openOasis)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(oasis.PatientName);
                    row.CreateCell(1).SetCellValue(oasis.PatientIdNumber);
                    row.CreateCell(2).SetCellValue(oasis.AssessmentName);
                    row.CreateCell(3).SetCellValue(oasis.Date);
                    row.CreateCell(4).SetCellValue(oasis.Status);
                    row.CreateCell(5).SetCellValue(oasis.CurrentlyAssigned);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Open OASIS: {0}", openOasis.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalOpenOASIS_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalOrders()
        {
            return PartialView("Clinical/Orders");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalOrders([Bind] ReportParameters parameters)
        {
            return View(new GridModel(reportService.GetOrders(parameters.StatusId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalMissedVisit()
        {
            return PartialView("Clinical/MissedVisit");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalMissedVisitResult(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.GetAllMissedVisit(BranchId, StartDate, EndDate)));
        }

        public ActionResult ExportClinicalMissedVisit(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Missed Visit";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("MissedVisit");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Missed Visit");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var missedVisits = reportService.GetAllMissedVisit(BranchId, StartDate, EndDate);
            if (missedVisits != null && missedVisits.Count > 0)
            {
                int rowNumber = 2;
                foreach (var missedVisit in missedVisits)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(missedVisit.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(missedVisit.PatientName);
                    row.CreateCell(2).SetCellValue(string.Format("{0:MM/dd/yyyy}", missedVisit.Date));
                    row.CreateCell(3).SetCellValue(missedVisit.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(missedVisit.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Missed Visit: {0}", missedVisits.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalMissedVisit_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalPhysicianOrderHistory()
        {
            return PartialView("Clinical/OrderHistory");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalPhysicianOrderHistory(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.GetPhysicianOrderHistory(BranchId, Status, StartDate, EndDate)));
        }

        public ActionResult ExportClinicalPhysicianOrderHistory(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Physician Order History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PhysicianOrderHistory");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Physician Order History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Order Number");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Physician Name");
            headerRow.CreateCell(3).SetCellValue("Order Date");
            headerRow.CreateCell(4).SetCellValue("Sent Date");
            headerRow.CreateCell(5).SetCellValue("Received Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var physicianOrders = reportService.GetPhysicianOrderHistory(BranchId, Status, StartDate, EndDate).ToList();
            if (physicianOrders != null && physicianOrders.Count > 0)
            {
                int rowNumber = 2;
                foreach (var physicianOrder in physicianOrders)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(physicianOrder.OrderNumber);
                    row.CreateCell(1).SetCellValue(physicianOrder.DisplayName);
                    row.CreateCell(2).SetCellValue(physicianOrder.PhysicianName);
                    row.CreateCell(3).SetCellValue(physicianOrder.OrderDateFormatted);
                    row.CreateCell(4).SetCellValue(physicianOrder.SentDateFormatted);
                    row.CreateCell(5).SetCellValue(physicianOrder.ReceivedDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Physician Order History: {0}", physicianOrders.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalPhysicianOrderHistory_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalPlanOfCareHistory()
        {
            return PartialView("Clinical/PlanOfCareHistory");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalPlanOfCareHistory(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.GetPlanOfCareHistory(BranchId, Status, StartDate, EndDate)));
        }

        public ActionResult ExportClinicalPlanOfCareHistory(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Plan Of Care History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PlanOfCareHistory");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Plan Of Care History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Order Number");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Physician Name");
            headerRow.CreateCell(3).SetCellValue("Order Date");
            headerRow.CreateCell(4).SetCellValue("Sent Date");
            headerRow.CreateCell(5).SetCellValue("Received Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var planOfCareHistories = reportService.GetPlanOfCareHistory(BranchId, Status, StartDate, EndDate).ToList();
            if (planOfCareHistories != null && planOfCareHistories.Count > 0)
            {
                int rowNumber = 2;
                foreach (var planOfCareHistory in planOfCareHistories)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(planOfCareHistory.Number);
                    row.CreateCell(1).SetCellValue(planOfCareHistory.PatientName);
                    row.CreateCell(2).SetCellValue(planOfCareHistory.PhysicianName);
                    row.CreateCell(3).SetCellValue(planOfCareHistory.CreatedDate);
                    row.CreateCell(4).SetCellValue(planOfCareHistory.SendDateFormatted);
                    row.CreateCell(5).SetCellValue(planOfCareHistory.ReceivedDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Plan Of Care History: {0}", planOfCareHistories.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalPlanOfCareHistory_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalThirteenAndNineteenVisitException()
        {
            return PartialView("Clinical/ThirteenAndNineteenVisitException");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalThirteenAndNineteenVisitException(Guid BranchId)
        {
            return View(new GridModel(patientService.GetTherapyException(BranchId)));
        }

        public ActionResult ExportClinicalThirteenAndNineteenVisitException(Guid BranchId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Thirteen And Nineteen Visit Exception";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ThirteenAndNineteen VisitException");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Thirteen And Nineteen Visit Exception");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Scheduled");
            headerRow.CreateCell(4).SetCellValue("Completed");
            headerRow.CreateCell(5).SetCellValue("Day");
            headerRow.CreateCell(6).SetCellValue("13th Visit");
            headerRow.CreateCell(7).SetCellValue("19th Visit");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var visitExceptions = patientService.GetTherapyException(BranchId);
            if (visitExceptions != null && visitExceptions.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visitException in visitExceptions)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(visitException.PatientName);
                    row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                    row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                    row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                    row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                    row.CreateCell(6).SetCellValue(visitException.ThirteenVisit);
                    row.CreateCell(7).SetCellValue(visitException.NineteenVisit);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Thirteen And Nineteen Visit Exception: {0}", visitExceptions.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ThirteenAndNineteenVisitException_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalThirteenTherapyReevaluationException()
        {
            return PartialView("Clinical/ThirteenTherapyReevaluationException");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalThirteenTherapyReevaluationException(Guid BranchId)
        {
            return View(new GridModel(patientService.GetTherapyReevaluationException(BranchId, 13)));
        }

        public ActionResult ExportClinicalThirteenTherapyReevaluationException(Guid BranchId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Thirteen And Nineteen Visit Exception";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ThirteenAndNineteen VisitException");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Thirteen And Nineteen Visit Exception");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Scheduled");
            headerRow.CreateCell(4).SetCellValue("Completed");
            headerRow.CreateCell(5).SetCellValue("Day");
            headerRow.CreateCell(6).SetCellValue("PT Re-eval");
            headerRow.CreateCell(7).SetCellValue("ST Re-eval");
            headerRow.CreateCell(8).SetCellValue("OT Re-eval");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var visitExceptions = patientService.GetTherapyReevaluationException(BranchId, 13);
            if (visitExceptions != null && visitExceptions.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visitException in visitExceptions)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(visitException.PatientName);
                    row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                    row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                    row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                    row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                    row.CreateCell(6).SetCellValue(visitException.PTEval);
                    row.CreateCell(7).SetCellValue(visitException.STEval);
                    row.CreateCell(8).SetCellValue(visitException.OTEval);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Thirteen And Nineteen Visit Exception: {0}", visitExceptions.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ThirteenAndNineteenVisitException_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalNineteenTherapyReevaluationException()
        {
            return PartialView("Clinical/NineteenTherapyReevaluationException");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalNineteenTherapyReevaluationException(Guid BranchId)
        {
            return View(new GridModel(patientService.GetTherapyReevaluationException(BranchId, 19)));
        }

        public ActionResult ExportClinicalNineteenTherapyReevaluationException(Guid BranchId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Thirteen And Nineteen Visit Exception";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ThirteenAndNineteen VisitException");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Thirteen And Nineteen Visit Exception");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Scheduled");
            headerRow.CreateCell(4).SetCellValue("Completed");
            headerRow.CreateCell(5).SetCellValue("Day");
            headerRow.CreateCell(6).SetCellValue("PT Re-eval");
            headerRow.CreateCell(7).SetCellValue("ST Re-eval");
            headerRow.CreateCell(8).SetCellValue("OT Re-eval");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var visitExceptions = patientService.GetTherapyReevaluationException(BranchId, 19);
            if (visitExceptions != null && visitExceptions.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visitException in visitExceptions)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(visitException.PatientName);
                    row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                    row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                    row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                    row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                    row.CreateCell(6).SetCellValue(visitException.PTEval);
                    row.CreateCell(7).SetCellValue(visitException.STEval);
                    row.CreateCell(8).SetCellValue(visitException.OTEval);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Thirteen And Nineteen Visit Exception: {0}", visitExceptions.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ThirteenAndNineteenVisitException_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Schedule Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SchedulePatientWeeklySchedule()
        {
            return PartialView("Schedule/PatientWeeklySchedule");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SchedulePatientWeeklyScheduleResult(Guid patientId)
        {
            if (patientId.IsEmpty())
            {
                return View(new GridModel(new List<ScheduleEvent>()));
            }
            return View(new GridModel(reportService.GetPatientScheduleEventsByDateRange(patientId, DateTime.Today, DateTime.Today.AddDays(7))));
        }

        public ActionResult ExportSchedulePatientWeeklySchedule(Guid patientId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Schedule Patient Weekly";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("SchedulePatientWeekly");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Patient Weekly");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", DateTime.Today.ToString("MM/dd/yyyy"), DateTime.Today.AddDays(7).ToString("MM/dd/yyyy"))));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Employee");
            if (patientId.IsEmpty())
            {
                MemoryStream outputEmpty = new MemoryStream();
                workbook.Write(outputEmpty);
                return File(outputEmpty.ToArray(), "application/vnd.ms-excel", string.Format("SchedulePatientWeekly_{0}.xls", DateTime.Now.Ticks));
            }
            sheet.CreateFreezePane(0, 2, 0, 2);
            var scheduleEvents = reportService.GetPatientScheduleEventsByDateRange(patientId, DateTime.Today, DateTime.Today.AddDays(7));
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(1).SetCellValue(evnt.StatusName);
                    row.CreateCell(2).SetCellValue(evnt.EventDate);
                    row.CreateCell(3).SetCellValue(evnt.VisitDate);
                    row.CreateCell(4).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Patient Weekly: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("SchedulePatientWeekly_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientMonthlyScheduleListing()
        {
            return PartialView("Schedule/PatientMonthlySchedule");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientMonthlySchedule(Guid patientId, int month)
        {
            if (patientId.IsEmpty() || month <= 0)
            {
                return View(new GridModel(new List<ScheduleEvent>()));
            }
            var fromDate = DateUtilities.GetStartOfMonth(month, DateTime.Now.Year);
            var toDate = DateUtilities.GetEndOfMonth(month, DateTime.Now.Year);
            return View(new GridModel(reportService.GetPatientScheduleEventsByDateRange(patientId, fromDate, toDate)));
        }

        public ActionResult ExportPatientMonthlySchedule(Guid patientId, int month)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Monthly Schedule";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientMonthlySchedule");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Monthly Schedule");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (!(month <= 0) && !(month > 13)) ? (new DateTime(DateTime.Now.Year, month, 1)).ToString("MMMM") : string.Empty));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (patientId.IsEmpty() || month <= 0)
            {
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientMonthlySchedule_{0}.xls", DateTime.Now.Ticks));
            }
            else
            {
                var fromDate = DateUtilities.GetStartOfMonth(month, DateTime.Now.Year);
                var toDate = DateUtilities.GetEndOfMonth(month, DateTime.Now.Year);
                var scheduleEvents = reportService.GetPatientScheduleEventsByDateRange(patientId, fromDate, toDate);
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    int rowNumber = 2;
                    foreach (var evnt in scheduleEvents)
                    {
                        var row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                        row.CreateCell(1).SetCellValue(evnt.StatusName);
                        row.CreateCell(2).SetCellValue(evnt.EventDate);
                        row.CreateCell(3).SetCellValue(evnt.VisitDate);
                        row.CreateCell(4).SetCellValue(evnt.UserName);
                        rowNumber++;
                    }
                    var totalRow = sheet.CreateRow(rowNumber + 2);
                    totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Monthly Schedule: {0}", scheduleEvents.Count));
                }
                sheet.AutoSizeColumn(0);
                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientMonthlySchedule_{0}.xls", DateTime.Now.Ticks));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleEmployeeWeekly()
        {
            return PartialView("Schedule/EmployeeWeekly");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleEmployeeWeeklyResult(Guid BranchId, Guid userId, DateTime StartDate, DateTime EndDate)
        {
            if (userId.IsEmpty())
            {
                return View(new GridModel(new List<UserVisit>()));
            }
            return View(new GridModel(reportService.GetUserScheduleEventsByDateRange(userId, BranchId, StartDate, EndDate)));
        }

        public ActionResult ExportScheduleEmployeeWeekly(Guid BranchId, Guid userId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Schedule Employee Weekly";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ScheduleEmployeeWeekly");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Employee Weekly");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Status");
            headerRow.CreateCell(3).SetCellValue("Schedule Date");
            headerRow.CreateCell(4).SetCellValue("Visit Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var userEvents = reportService.GetUserScheduleEventsByDateRange(userId, BranchId, StartDate, EndDate);
            if (userEvents != null && userEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in userEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientName);
                    row.CreateCell(1).SetCellValue(evnt.TaskName);
                    row.CreateCell(2).SetCellValue(evnt.StatusName);
                    row.CreateCell(3).SetCellValue(evnt.ScheduleDate);
                    row.CreateCell(4).SetCellValue(evnt.VisitDate);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Employee Weekly: {0}", userEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleEmployeeWeekly_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleMonthlyWork()
        {
            return PartialView("Schedule/MonthlyWork");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleMonthlyWorkResult(Guid userId, int month, Guid BranchId)
        {
            if (userId.IsEmpty() || month <= 0)
            {
                return View(new GridModel(new List<UserVisit>()));
            }
            var fromDate = DateUtilities.GetStartOfMonth(month, DateTime.Now.Year);
            var toDate = DateUtilities.GetEndOfMonth(month, DateTime.Now.Year);
            return View(new GridModel(reportService.GetUserScheduleEventsByDateRange(userId, BranchId, fromDate, toDate)));
        }

        public ActionResult ExportScheduleMonthlyWork(Guid userId, int month, Guid BranchId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Schedule Monthly Work";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ScheduleMonthlyWork");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Monthly Work");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (!(month <= 0) && !(month > 13)) ? (new DateTime(DateTime.Now.Year, month, 1)).ToString("MMMM") : string.Empty));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Status");
            headerRow.CreateCell(3).SetCellValue("Schedule Date");
            headerRow.CreateCell(4).SetCellValue("Visit Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (userId.IsEmpty() || month <= 0)
            {
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleMonthlyWork_{0}.xls", DateTime.Now.Ticks));
            }
            else
            {
                var fromDate = DateUtilities.GetStartOfMonth(month, DateTime.Now.Year);
                var toDate = DateUtilities.GetEndOfMonth(month, DateTime.Now.Year);
                var userEvents = reportService.GetUserScheduleEventsByDateRange(userId, BranchId, fromDate, toDate);
                if (userEvents != null && userEvents.Count > 0)
                {
                    int rowNumber = 2;
                    foreach (var evnt in userEvents)
                    {
                        var row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(evnt.PatientName);
                        row.CreateCell(1).SetCellValue(evnt.TaskName);
                        row.CreateCell(2).SetCellValue(evnt.StatusName);
                        row.CreateCell(3).SetCellValue(evnt.ScheduleDate);
                        row.CreateCell(4).SetCellValue(evnt.VisitDate);
                        rowNumber++;
                    }
                    var totalRow = sheet.CreateRow(rowNumber + 2);
                    totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Monthly Work: {0}", userEvents.Count));
                }
                sheet.AutoSizeColumn(0);
                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleMonthlyWork_{0}.xls", DateTime.Now.Ticks));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SchedulePastDueVisits()
        {
            return PartialView("Schedule/PastDueVisits");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SchedulePastDueVisitsResult(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.GetPastDueScheduleEvents(BranchId, StartDate, EndDate)));
        }

        public ActionResult ExportSchedulePastDueVisits(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Past Due Visits";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PastDueVisits");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Past Due Visits");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var scheduleEvents = reportService.GetPastDueScheduleEvents(BranchId, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.EventDate);
                    row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Visits: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("SchedulePastDueVisits_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SchedulePastDueVisitsByDiscipline()
        {
            return PartialView("Schedule/PastDueVisitsByDiscipline");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SchedulePastDueVisitsByDisciplineResult(Guid BranchId, string discipline, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.GetPastDueScheduleEventsByDiscipline(BranchId, discipline, StartDate, EndDate)));
        }

        public ActionResult ExportSchedulePastDueVisitsByDiscipline(Guid BranchId, string discipline, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Past Due Visits By Discipline";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PastDueVisitsByDiscipline");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Past Due Visits By Discipline");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
            titleRow.CreateCell(4).SetCellValue(string.Format("Discipline: {0}", Enum.IsDefined(typeof(Disciplines), discipline) ? ((Disciplines)Enum.Parse(typeof(Disciplines), discipline)).GetDescription() : string.Empty));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var scheduleEvents = reportService.GetPastDueScheduleEventsByDiscipline(BranchId, discipline, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.EventDate);
                    row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Visits By Discipline: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PastDueVisitsByDiscipline_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleDailyWork()
        {
            return PartialView("Schedule/DailyWork");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleDailyWorkResult(DateTime Date, Guid BranchId)
        {
            return View(new GridModel(reportService.GetScheduleEventsByDateRange(BranchId, Date, Date)));
        }

        public ActionResult ExportScheduleDailyWork(DateTime Date, Guid BranchId)
        {
            var scheduleEvents = reportService.GetScheduleEventsByDateRange(BranchId, Date, Date);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Daily Work Schedule";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("DailyWorkSchedule");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Daily Work Schedule");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Task");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Schedule Date");
            headerRow.CreateCell(5).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(3).SetCellValue(evnt.StatusName);
                    row.CreateCell(4).SetCellValue(evnt.EventDate);
                    row.CreateCell(5).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Daily Work Schedule: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("DailyWorkSchedule_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleCaseManagerTask()
        {
            return PartialView("Schedule/CaseManagerTask");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleCaseManagerTask(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.GetCaseManagerScheduleByBranch(BranchId, StartDate, EndDate)));
        }

        public ActionResult ExportScheduleCaseManagerTask(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var scheduleEvents = reportService.GetCaseManagerScheduleByBranch(BranchId, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Case Manager Task";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("CaseManagerTask");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Case Manager Task");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.EventDate);
                    row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(evnt.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Case Manager Task: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("CaseManagerTask_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleDeviation()
        {
            return PartialView("Schedule/Deviation");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleDeviation(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.GetScheduleDeviation(BranchId, StartDate, EndDate)));
        }

        public ActionResult ExportScheduleDeviation(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {

            var scheduleEvents = reportService.GetScheduleDeviation(BranchId, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Schedule Deviation";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ScheduleDeviation");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Deviation");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("MR #");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Employee");
            headerRow.CreateCell(5).SetCellValue("Schedule Date");
            headerRow.CreateCell(6).SetCellValue("Visit Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(3).SetCellValue(evnt.StatusName);
                    row.CreateCell(4).SetCellValue(evnt.UserName);
                    row.CreateCell(5).SetCellValue(evnt.EventDate);
                    row.CreateCell(6).SetCellValue(evnt.VisitDate);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Deviation: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleDeviation_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SchedulePastDueRecet()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("Schedule/PastDueRecet", agency != null ? agency.Payor : "0");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleRecertsPastDue(Guid BranchId, int insuranceId, DateTime StartDate)
        {
            return View(new GridModel(agencyService.GetRecertsPastDue(BranchId, insuranceId, StartDate, DateTime.Now)));
        }

        public ActionResult ExportSchedulePastDueRecet(Guid BranchId, int insuranceId, DateTime StartDate)
        {

            var recets = agencyService.GetRecertsPastDue(BranchId, insuranceId, StartDate, DateTime.Now);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Past Due Recet";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PastDueRecet");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Past Due Recet");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), DateTime.Now.ToString("MM/dd/yyyy"))));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("MR #");
            headerRow.CreateCell(2).SetCellValue("Employee Responsible");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Due Date");
            headerRow.CreateCell(5).SetCellValue("Past Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (recets != null && recets.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in recets)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientName);
                    row.CreateCell(1).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(2).SetCellValue(evnt.AssignedTo);
                    row.CreateCell(3).SetCellValue(evnt.Status);
                    row.CreateCell(4).SetCellValue(evnt.TargetDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(5).SetCellValue(evnt.DateDifference);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Recet: {0}", recets.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PastDueRecet_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleUpcomingRecet()
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            return PartialView("Schedule/UpcomingRecet", agency != null ? agency.Payor : "0");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleRecertsUpcoming(Guid BranchId, int InsuranceId)
        {
            return View(new GridModel(agencyService.GetRecertsUpcoming(BranchId, InsuranceId, DateTime.Now, DateTime.Now.AddDays(24))));
        }

        public ActionResult ExportScheduleUpcomingRecet(Guid BranchId, int InsuranceId)
        {
            var recets = agencyService.GetRecertsUpcoming(BranchId, InsuranceId, DateTime.Now, DateTime.Now.AddDays(24));
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Upcoming Recet";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UpcomingRecet");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Upcoming Recet");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("MR #");
            headerRow.CreateCell(2).SetCellValue("Employee Responsible");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Due Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (recets != null && recets.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in recets)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientName);
                    row.CreateCell(1).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(2).SetCellValue(evnt.AssignedTo);
                    row.CreateCell(3).SetCellValue(evnt.Status);
                    row.CreateCell(4).SetCellValue(evnt.TargetDate.ToString("MM/dd/yyyy"));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Upcoming Recet: {0}", recets.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UpcomingRecet_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Billing Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OutstandingClaims()
        {
            return PartialView("Billing/OutstandingClaims");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OutstandingClaims(Guid BranchId, string type)
        {
            return View(new GridModel(reportService.UnProcessedBillViewData(BranchId, type)));
        }

        public ActionResult ExportOutstandingClaims(Guid BranchId, string type)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Outstanding Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("OutstandingClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Outstanding Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Type: {0}", type.ToUpperCase()));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("Type");
            sheet.CreateFreezePane(0, 1, 0, 1);
            var bills = reportService.UnProcessedBillViewData(BranchId, type);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.LastName);
                    row.CreateCell(2).SetCellValue(bill.FirstName);
                    row.CreateCell(3).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(4).SetCellValue(bill.Type);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Outstanding Claims: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("OutstandingClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClaimsByStatus()
        {
            return PartialView("Billing/ClaimByStatus");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimsByStatus(Guid BranchId, string type, int Status, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.BillViewDataByStatus(BranchId, type, Status, StartDate, EndDate)));
        }

        public ActionResult ExportClaimsByStatus(Guid BranchId, string type, int Status, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Claims By Status";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ClaimsByStatus");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Claims By Status");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", type.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", Enum.IsDefined(typeof(ScheduleStatus), Status) ? ((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), Status)).GetDescription() : string.Empty));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Claim Amount");
            headerRow.CreateCell(5).SetCellValue("Claim Date");
            headerRow.CreateCell(6).SetCellValue("Payment Amount");
            headerRow.CreateCell(7).SetCellValue("Payment Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = reportService.BillViewDataByStatus(BranchId, type, Status, StartDate, EndDate);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    row.CreateCell(4).SetCellValue(bill.ClaimAmount);
                    row.CreateCell(5).SetCellValue(bill.ClaimDateFormatted);
                    row.CreateCell(6).SetCellValue(bill.PaymentAmount);
                    row.CreateCell(7).SetCellValue(bill.PaymentDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Claims By Status: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("OutstandingClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SubmittedClaims()
        {
            return PartialView("Billing/SubmittedClaims");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmittedClaims(Guid BranchId, string type, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.SubmittedBillViewDataByDateRange(BranchId, type, StartDate, EndDate)));
        }

        public ActionResult ExportSubmittedClaims(Guid BranchId, string type, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Claims By Status";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ClaimsByStatus");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Claims By Status");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", type.ToUpperCase()));
            // titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", Enum.IsDefined(typeof(ScheduleStatus), Status) ? ((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), Status)).GetDescription() : string.Empty));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Claim Amount");
            headerRow.CreateCell(5).SetCellValue("Claim Date");
            headerRow.CreateCell(6).SetCellValue("Payment Amount");
            headerRow.CreateCell(7).SetCellValue("Payment Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = reportService.SubmittedBillViewDataByDateRange(BranchId, type, StartDate, EndDate);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    row.CreateCell(4).SetCellValue(bill.ClaimAmount);
                    row.CreateCell(5).SetCellValue(bill.ClaimDateFormatted);
                    row.CreateCell(6).SetCellValue(bill.PaymentAmount);
                    row.CreateCell(7).SetCellValue(bill.PaymentDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Claims By Status: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("OutstandingClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AccountsReceivable()
        {
            return PartialView("Billing/AccountsReceivable");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AccountsReceivable(Guid BranchId, string type, int Insurance, DateTime StartDate, DateTime EndDate)
        {
            var viewData = new List<ClaimLean>();
            if (type.ToUpperCase() == "RAP")
            {
                viewData = billingService.AccountsReceivableRaps(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            else if (type == "Final")
            {
                viewData = billingService.AccountsReceivableFinals(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            else if (type == "All")
            {
                viewData = billingService.AccountsReceivables(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            return View(new GridModel(viewData));
        }

        public ActionResult ExportAccountsReceivable(Guid BranchId, string type, int Insurance, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Accounts Receivable";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("AccountsReceivable");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Accounts Receivable");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", type.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(Insurance, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Claim Date");
            headerRow.CreateCell(6).SetCellValue("Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = new List<ClaimLean>();
            if (type.IsEqual("RAP"))
            {
                bills = billingService.AccountsReceivableRaps(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            else if (type.IsEqual("Final"))
            {
                bills = billingService.AccountsReceivableFinals(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            else if (type.IsEqual("All"))
            {
                bills = billingService.AccountsReceivables(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    row.CreateCell(4).SetCellValue(bill.StatusName);
                    row.CreateCell(5).SetCellValue(bill.ClaimDateFormatted);
                    row.CreateCell(6).SetCellValue(Math.Round(bill.ClaimAmount, 2));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Accounts Receivable: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("AccountsReceivable_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AgedAccountsReceivable()
        {
            return PartialView("Billing/AgedAccountsReceivable");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AgedAccountsReceivable(Guid BranchId, string type, int Insurance, DateTime StartDate, DateTime EndDate)
        {
            var viewData = new List<ClaimLean>();
            if (type.ToUpperCase() == "RAP")
            {
                viewData = billingService.AccountsReceivableRaps(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            else if (type == "Final")
            {
                viewData = billingService.AccountsReceivableFinals(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            else if (type == "All")
            {
                viewData = billingService.AccountsReceivables(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            return View(new GridModel(viewData));
        }

        public ActionResult ExportAgedAccountsReceivable(Guid BranchId, string type, int Insurance, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Aged Accounts Receivable";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("AgedAccountsReceivable");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Aged Accounts Receivable");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", type.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(Insurance, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Episode Range");
            headerRow.CreateCell(2).SetCellValue("Type");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Claim Date");
            headerRow.CreateCell(5).SetCellValue("1-30");
            headerRow.CreateCell(6).SetCellValue("31-60");
            headerRow.CreateCell(7).SetCellValue("61-90");
            headerRow.CreateCell(8).SetCellValue("> 90");
            headerRow.CreateCell(9).SetCellValue("Total");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = new List<ClaimLean>();
            if (type.IsEqual("RAP"))
            {
                bills = billingService.AccountsReceivableRaps(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            else if (type.IsEqual("Final"))
            {
                bills = billingService.AccountsReceivableFinals(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            else if (type.IsEqual("All"))
            {
                bills = billingService.AccountsReceivables(BranchId, Insurance, StartDate, EndDate).ToList();
            }
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.DisplayName);
                    row.CreateCell(1).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(2).SetCellValue(bill.Type);
                    row.CreateCell(3).SetCellValue(bill.StatusName);
                    row.CreateCell(4).SetCellValue(bill.ClaimDateFormatted);
                    row.CreateCell(5).SetCellValue(Math.Round(bill.Amount30, 2));
                    row.CreateCell(6).SetCellValue(Math.Round(bill.Amount60, 2));
                    row.CreateCell(7).SetCellValue(Math.Round(bill.Amount90, 2));
                    row.CreateCell(8).SetCellValue(Math.Round(bill.AmountOver90, 2));
                    row.CreateCell(9).SetCellValue(Math.Round(bill.ClaimAmount, 2));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Aged Accounts Receivable: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("AgedAccountsReceivable_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSRAPClaims()
        {
            return PartialView("Billing/PPSRAPClaims");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PPSRAPClaims(Guid BranchId)
        {
            return View(new GridModel(billingRepository.GetPPSRapClaims(Current.AgencyId, BranchId)));
        }

        public ActionResult ExportPPSRAPClaims(Guid BranchId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PPS RAP Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PPSRAPClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("PPS RAP Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("HIPPS");
            headerRow.CreateCell(5).SetCellValue("Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var raps = billingRepository.GetPPSRapClaims(Current.AgencyId, BranchId);
            if (raps != null && raps.Count > 0)
            {
                int rowNumber = 2;
                foreach (var rap in raps)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.LastName);
                    row.CreateCell(2).SetCellValue(rap.FirstName);
                    row.CreateCell(3).SetCellValue(rap.EpisodeRange);
                    row.CreateCell(4).SetCellValue(rap.HippsCode);
                    row.CreateCell(5).SetCellValue(rap.ProspectivePay);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of PPS RAP Claims: {0}", raps.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PPSRAPClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSFinalClaims()
        {
            return PartialView("Billing/PPSFinalClaims");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PPSFinalClaims(Guid BranchId)
        {
            return View(new GridModel(billingRepository.GetPPSFinalClaims(Current.AgencyId, BranchId)));
        }

        public ActionResult ExportPPSFinalClaims(Guid BranchId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PPS Final Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PPSFinalClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("PPS Final Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("HIPPS");
            headerRow.CreateCell(5).SetCellValue("Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var finals = billingRepository.GetPPSFinalClaims(Current.AgencyId, BranchId);
            if (finals != null && finals.Count > 0)
            {
                int rowNumber = 2;
                foreach (var final in finals)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(final.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(final.LastName);
                    row.CreateCell(2).SetCellValue(final.FirstName);
                    row.CreateCell(3).SetCellValue(final.EpisodeRange);
                    row.CreateCell(4).SetCellValue(final.HippsCode);
                    row.CreateCell(5).SetCellValue(final.ProspectivePay);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of PPS Final Claims: {0}", finals.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PPSFinalClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PotentialClaimAutoCancel()
        {
            return PartialView("Billing/PotentialClaimAutoCancel");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PotentialClaimAutoCancel(Guid BranchId)
        {
            return View(new GridModel(billingRepository.GetPotentialCliamAutoCancels(Current.AgencyId, BranchId)));
        }

        public ActionResult ExportPotentialClaimAutoCancel(Guid BranchId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Potential Claim AutoCancel";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PotentialClaimAutoCancel");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Potential Claim AutoCancel");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var finals = billingRepository.GetPotentialCliamAutoCancels(Current.AgencyId, BranchId);
            if (finals != null && finals.Count > 0)
            {
                int rowNumber = 2;
                foreach (var final in finals)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(final.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(final.LastName);
                    row.CreateCell(2).SetCellValue(final.FirstName);
                    row.CreateCell(3).SetCellValue(final.EpisodeRange);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Potential Claim AutoCancel: {0}", finals.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PPSFinalClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargePatients()
        {
            return PartialView("Patient/DischargePatients");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DischargePatients(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(patientRepository.GetDischargePatients(Current.AgencyId, BranchId,StartDate,EndDate)));
        }

        public ActionResult ExportDischargePatients(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Discharge Patients";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("DischargePatients");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Discharge Patients");
            titleRow.CreateCell(2).SetCellValue(string.Format("Date Range : {0} - {1} ", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("SOC Date");
            headerRow.CreateCell(4).SetCellValue("Discharge Date");
            headerRow.CreateCell(5).SetCellValue("Discharge Reason");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var dischargePatients = patientRepository.GetDischargePatients(Current.AgencyId, BranchId, StartDate, EndDate);
            if (dischargePatients != null && dischargePatients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in dischargePatients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(patient.LastName);
                    row.CreateCell(2).SetCellValue(patient.FirstName);
                    row.CreateCell(3).SetCellValue(patient.StartofCareDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(4).SetCellValue(patient.DischargeDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(5).SetCellValue(patient.DischargeReason);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Discharge Patients: {0}", dischargePatients.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("DischargePatients_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BillingBatch()
        {
            return PartialView("Billing/BillingBatch");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BillingBatch(string ClaimType, DateTime BatchDate)
        {
            var viewData = billingService.BillingBatch(ClaimType, BatchDate).ToList();
            return View(new GridModel(viewData));
        }

        public ActionResult ExportBillingBatch(string ClaimType, DateTime BatchDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Billing Batch Report";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("BillingBatch");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Billing Batch");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Claim Type: {0}", ClaimType.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date: {0}", BatchDate.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Bill Type");
            headerRow.CreateCell(4).SetCellValue("Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = billingService.BillingBatch(ClaimType, BatchDate).ToList();
                
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.Range);
                    row.CreateCell(3).SetCellValue(bill.BillType);
                    row.CreateCell(4).SetCellValue(Math.Round(bill.ProspectivePay, 2));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("BillingBatch_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Employee Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeRoster()
        {
            return PartialView("Employee/Roster");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeRoster(Guid BranchId, int Status)
        {
            return View(new GridModel(reportService.GetEmployeeRoster(BranchId, Status)));
        }

        public ActionResult ExportEmployeeRoster(Guid BranchId, int Status)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Roster";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeRoster");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Roster");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", Status == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), Status) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), Status)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Address");
            headerRow.CreateCell(2).SetCellValue("City");
            headerRow.CreateCell(3).SetCellValue("State");
            headerRow.CreateCell(4).SetCellValue("Zip Code");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            headerRow.CreateCell(6).SetCellValue("Gender");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeRosters = reportService.GetEmployeeRoster(BranchId, Status);
            if (employeeRosters != null && employeeRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var employee in employeeRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(employee.DisplayName);
                    row.CreateCell(1).SetCellValue(employee.Profile.Address);
                    row.CreateCell(2).SetCellValue(employee.Profile.AddressCity);
                    row.CreateCell(3).SetCellValue(employee.Profile.AddressStateCode);
                    row.CreateCell(4).SetCellValue(employee.Profile.AddressZipCode);
                    row.CreateCell(5).SetCellValue(employee.Profile.PhoneHome);
                    row.CreateCell(6).SetCellValue(employee.Profile.Gender);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Roster: {0}", employeeRosters.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeRoster_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeBirthdayList()
        {
            return PartialView("Employee/BirthdayList");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeBirthdayList(Guid BranchId, int Status, int month)
        {
            return View(new GridModel(reportService.GetEmployeeBirthdays(BranchId, Status, month)));
        }

        public ActionResult ExportEmployeeBirthdayList(Guid BranchId, int Status, int month)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Birthday List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeBirthdayList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Birthday List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (!(month <= 0) && !(month > 13)) ? (new DateTime(DateTime.Now.Year, month, 1)).ToString("MMMM") : string.Empty));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", Status == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), Status) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), Status)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Age");
            headerRow.CreateCell(2).SetCellValue("Birth Day");
            headerRow.CreateCell(3).SetCellValue("Address First Row");
            headerRow.CreateCell(4).SetCellValue("Address Second Row");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeBirthDays = reportService.GetEmployeeBirthdays(BranchId, Status, month);
            if (employeeBirthDays != null && employeeBirthDays.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in employeeBirthDays)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.Name);
                    row.CreateCell(1).SetCellValue(birthDay.Age);
                    row.CreateCell(2).SetCellValue(birthDay.BirthDay);
                    row.CreateCell(3).SetCellValue(birthDay.AddressFirstRow);
                    row.CreateCell(4).SetCellValue(birthDay.AddressSecondRow);
                    row.CreateCell(5).SetCellValue(birthDay.PhoneHome);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Birthday List: {0}", employeeBirthDays.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeBirthdayList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeLicenseListing()
        {
            return PartialView("Employee/License");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeLicenseListing(Guid userId)
        {
            if (userId.IsEmpty())
            {
                return View(new GridModel(new List<License>()));
            }
            return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId)));
        }

        public ActionResult ExportEmployeeLicenseListing(Guid userId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee License Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeLicenseListing");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee License Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Initiation Date");
            headerRow.CreateCell(2).SetCellValue("Expiration Date");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeLicenses = userRepository.GetUserLicenses(Current.AgencyId, userId);
            if (employeeLicenses != null && employeeLicenses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var employeeLicense in employeeLicenses)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(employeeLicense.LicenseType);
                    row.CreateCell(1).SetCellValue(employeeLicense.InitiationDateFormatted);
                    row.CreateCell(2).SetCellValue(employeeLicense.ExpirationDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee License Listing: {0}", employeeLicenses.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeExpiringLicense()
        {
            return PartialView("Employee/ExpiringLicenses");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeExpiringLicense(Guid BranchId, int Status)
        {

            return View(new GridModel(reportService.GetEmployeeExpiringLicenses(BranchId, Status)));
        }

        public ActionResult ExportEmployeeExpiringLicense(Guid BranchId, int Status)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Expiring License";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeExpiringLicense");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Expiring License");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", Status == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), Status) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), Status)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee Name");
            headerRow.CreateCell(1).SetCellValue("License Name");
            headerRow.CreateCell(2).SetCellValue("Initiation Date");
            headerRow.CreateCell(3).SetCellValue("Expiration Date");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeLicenses = reportService.GetEmployeeExpiringLicenses(BranchId, Status);
            if (employeeLicenses != null && employeeLicenses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var employeeLicense in employeeLicenses)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(employeeLicense.UserDispalyName);
                    row.CreateCell(1).SetCellValue(employeeLicense.LicenseType);
                    row.CreateCell(2).SetCellValue(employeeLicense.InitiationDateFormatted);
                    row.CreateCell(3).SetCellValue(employeeLicense.ExpirationDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Expiring License: {0}", employeeLicenses.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeExpiringLicenses_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeVisitByDateRange()
        {
            return PartialView("Employee/ScheduleByDateRange");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeVisitByDateRange(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(reportService.GetEmployeeScheduleByDateRange(BranchId, StartDate, EndDate)));
        }

        public ActionResult ExportEmployeeVisitByDateRange(Guid BranchId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Visit By Date Range";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeVisitByDateRange");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Visit By Date Range");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Task");
            headerRow.CreateCell(3).SetCellValue("Schedule Date");
            headerRow.CreateCell(4).SetCellValue("Visit Date");
            headerRow.CreateCell(5).SetCellValue("Status");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeVisits = reportService.GetEmployeeScheduleByDateRange(BranchId, StartDate, EndDate);
            if (employeeVisits != null && employeeVisits.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visit in employeeVisits)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visit.UserDisplayName);
                    row.CreateCell(1).SetCellValue(visit.PatientName);
                    row.CreateCell(2).SetCellValue(visit.TaskName);
                    row.CreateCell(3).SetCellValue(visit.ScheduleDate);
                    row.CreateCell(4).SetCellValue(visit.VisitDate);
                    row.CreateCell(5).SetCellValue(visit.StatusName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Visit By Date Range: {0}", employeeVisits.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeVisitByDateRange_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Statistical Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalPatientVisitHistory()
        {
            return PartialView("Statistical/PatientVisitHistory");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalPatientVisitHistory(Guid patientId, DateTime StartDate, DateTime EndDate)
        {
            if (patientId.IsEmpty())
            {
                return View(new GridModel(new List<ScheduleEvent>()));
            }
            return View(new GridModel(patientRepository.GetScheduledEventsOnly(Current.AgencyId, patientId, StartDate, EndDate)));
        }

        public ActionResult ExportStatisticalPatientVisitHistory(Guid patientId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Visit History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientVisitHistory");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Visit History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Employee");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var scheduleEvents = patientRepository.GetScheduledEventsOnly(Current.AgencyId, patientId, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(1).SetCellValue(evnt.StatusName);
                    row.CreateCell(2).SetCellValue(evnt.EventDate);
                    row.CreateCell(3).SetCellValue(evnt.VisitDate);
                    row.CreateCell(4).SetCellValue(evnt.UserName);

                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Visit History: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalEmployeeVisitHistory()
        {
            return PartialView("Statistical/EmployeeVisitHistory");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalEmployeeVisitHistory(Guid userId, DateTime StartDate, DateTime EndDate)
        {
            if (userId.IsEmpty())
            {
                return View(new GridModel(new List<UserEvent>()));

            }
            return View(new GridModel(reportService.GetEmployeeVisistList(userId, StartDate, EndDate)));
        }

        public ActionResult ExportStatisticalEmployeeVisitHistory(Guid userId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Visit History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeVisitHistory");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Visit History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Patient");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var scheduleEvents = reportService.GetEmployeeVisistList(userId, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.TaskName);
                    row.CreateCell(1).SetCellValue(evnt.StatusName);
                    row.CreateCell(2).SetCellValue(evnt.ScheduleDate);
                    row.CreateCell(3).SetCellValue(evnt.VisitDate);
                    row.CreateCell(4).SetCellValue(evnt.PatientName);

                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Visit History: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalMonthlyAdmissionListing()
        {
            return PartialView("Statistical/MonthlyAdmission");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalMonthlyAdmission(Guid BranchId, int Status, int month, int year)
        {
            return View(new GridModel(patientRepository.GetPatientByAdmissionMonthYear(Current.AgencyId, BranchId, Status, month, year)));
        }

        public ActionResult ExportStatisticalMonthlyAdmission(Guid BranchId, int Status, int month, int year)
        {
            var patients = patientRepository.GetPatientByAdmissionMonthYear(Current.AgencyId, BranchId, Status, month, year);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Monthly Admission List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientMonthlyAdmission");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Monthly Admission List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month/Year : {0}/{1}", (!(month <= 0) && !(month > 13)) ? (new DateTime(DateTime.Now.Year, month, 1)).ToString("MMMM") : string.Empty, year));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", Status == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), Status) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), Status)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("First Name");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("Admission Source");
            headerRow.CreateCell(3).SetCellValue("Admission Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (patients != null && patients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in patients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.PatientFirstName);
                    row.CreateCell(1).SetCellValue(birthDay.PatientLastName);
                    row.CreateCell(2).SetCellValue(birthDay.AdmissionSourceName);
                    row.CreateCell(3).SetCellValue(birthDay.PatientSoC);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Monthly Admission List: {0}", patients.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientMonthlyAdmission_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalAnnualAdmissionListing()
        {
            return PartialView("Statistical/AnnualAdmission");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalAnnualAdmission(Guid BranchId, int Status, int year)
        {
            return View(new GridModel(patientRepository.GetPatientByAdmissionYear(Current.AgencyId, BranchId, Status, year)));
        }

        public ActionResult ExportStatisticalAnnualAdmission(Guid BranchId, int Status, int year)
        {
            var patients = patientRepository.GetPatientByAdmissionYear(Current.AgencyId, BranchId, Status, year);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Annual Admission List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientAnnualAdmission");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Annual Admission List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Year : {0}", year));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", Status == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), Status) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), Status)).GetDescription() : string.Empty)));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("First Name");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("Admission Source");
            headerRow.CreateCell(3).SetCellValue("Admission Date");

            sheet.CreateFreezePane(0, 2, 0, 2);

            if (patients != null && patients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in patients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.PatientFirstName);
                    row.CreateCell(1).SetCellValue(birthDay.PatientLastName);
                    row.CreateCell(2).SetCellValue(birthDay.AdmissionSourceName);
                    row.CreateCell(3).SetCellValue(birthDay.PatientSoC);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Annual Admission List: {0}", patients.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientAnnualAdmission_{0}.xls", DateTime.Now.Ticks));
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalUnduplicatedCensusReportListing()
        {
            return PartialView("Statistical/UnduplicatedCensusReport");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalUnduplicatedCensusReport(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(patientRepository.GetPatientByAdmissionUnduplicatedByDateRange(Current.AgencyId, BranchId, Status, StartDate,EndDate)));
        }

        public ActionResult ExportStatisticalUnduplicatedCensusReport(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            var patients = patientRepository.GetPatientByAdmissionUnduplicatedByDateRange(Current.AgencyId, BranchId, Status, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Unduplicated Census List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UnduplicatedCensusReport");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Unduplicated Census List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1} ", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", Status == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), Status) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), Status)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("First Name");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("Admission Source");
            headerRow.CreateCell(3).SetCellValue("Admission Date");
            headerRow.CreateCell(4).SetCellValue("Discharge Date");
            headerRow.CreateCell(5).SetCellValue("Status");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (patients != null && patients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in patients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.PatientFirstName);
                    row.CreateCell(1).SetCellValue(birthDay.PatientLastName);
                    row.CreateCell(2).SetCellValue(birthDay.AdmissionSourceName);
                    row.CreateCell(3).SetCellValue(birthDay.PatientSoC);
                    row.CreateCell(4).SetCellValue(birthDay.PatientDischargeDate);
                    row.CreateCell(5).SetCellValue(birthDay.PatientStatusName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Unduplicated Census List: {0}", patients.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UnduplicatedCensusReport_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalCensusByPrimaryInsurance()
        {
            return PartialView("Statistical/CensusByPrimaryInsurance");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CensusByPrimaryInsurance(Guid BranchCode, int insurance, int Status)
        {
            if (insurance <= 0)
            {
                return View(new GridModel(new List<PatientRoster>()));
            }
            return View(new GridModel(reportService.GetPatientRosterByInsurance(BranchCode, insurance, Status)));
        }

        public ActionResult ExportCensusByPrimaryInsurance(Guid BranchCode, int insurance, int Status)
        {
            var patientRosters = insurance > 0 ? reportService.GetPatientRosterByInsurance(BranchCode, insurance, Status) : new List<PatientRoster>();
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Census By Primary Insurance";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("CensusByPrimaryInsurance");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Census By Primary Insurance");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", Status == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), Status) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), Status)).GetDescription() : string.Empty)));
            var insuranceModel = InsuranceEngine.Instance.Get(insurance, Current.AgencyId);
            if (insuranceModel != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance : {0}", insuranceModel.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Address");
            headerRow.CreateCell(3).SetCellValue("City");
            headerRow.CreateCell(4).SetCellValue("State");
            headerRow.CreateCell(5).SetCellValue("Zip Code");
            headerRow.CreateCell(6).SetCellValue("Home Phone");
            headerRow.CreateCell(7).SetCellValue("Gender");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientId);
                    row.CreateCell(1).SetCellValue(patient.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(patient.PatientAddressLine1);
                    row.CreateCell(3).SetCellValue(patient.PatientAddressCity);
                    row.CreateCell(4).SetCellValue(patient.PatientAddressStateCode);
                    row.CreateCell(5).SetCellValue(patient.PatientAddressZipCode);
                    row.CreateCell(6).SetCellValue(patient.PatientPhone);
                    row.CreateCell(7).SetCellValue(patient.PatientGender);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Census By Primary Insurance: {0}", patientRosters.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("CensusByPrimaryInsurance_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #endregion
    }
}
