﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Enums;
    using Services;
    using ViewData;
    using Workflows;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using Axxess.Membership.Domain;

    using Axxess.OasisC.Domain;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Log.Enums;

    using AutoMapper;

    using Telerik.Web.Mvc;

    using Axxess.AgencyManagement.App.iTextExtension;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using System.Web.Script.Serialization;
    using Axxess.LookUp.Repositories;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PatientController : BaseController
    {
        #region Constructor

        private readonly IDateService dateService;
        private readonly IUserService userService;
        private readonly IMessageService messageService;
        private readonly IPatientService patientService;
        private readonly IAssessmentService assessmentService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IReferralRepository referralRepository;
        private readonly IPhysicianRepository physicianRepository;

        public PatientController(IAgencyManagementDataProvider agencyManagementDataProvider, IPatientService patientService, IAssessmentService assessmentService, IUserService userService, IMessageService messageService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.patientService = patientService;
            this.messageService = messageService;
            this.assessmentService = assessmentService;
            this.dateService = Container.Resolve<IDateService>();
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region PatientController Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New(Guid referralId)
        {
            if (!referralId.IsEmpty())
            {
                return PartialView("New", referralRepository.Get(Current.AgencyId, referralId));
            }
            return PartialView("New", null);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Add([Bind] Patient patient)
        {
            Check.Argument.IsNotNull(patient, "patient");
            var rules = new List<Validation>();
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient could not be saved" };
            if (patient != null)
            {
                if (patient.PatientIdNumber.IsNotNullOrEmpty())
                {
                    bool patientIdCheck = patientRepository.IsPatientIdExist(Current.AgencyId, patient.PatientIdNumber);
                    rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
                }
                if (patient.MedicareNumber.IsNotNullOrEmpty())
                {
                    bool medicareNumberCheck = patientRepository.IsMedicareExist(Current.AgencyId, patient.MedicareNumber);
                    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                }
                if (patient.MedicaidNumber.IsNotNullOrEmpty())
                {
                    bool medicaidNumberCheck = patientRepository.IsMedicaidExist(Current.AgencyId, patient.MedicaidNumber);
                    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                }
                
                if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() && patient.PrimaryInsurance.ToInteger() >= 1000)
                {
                    rules.Add(new Validation(() => patient.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger() && patient.SecondaryInsurance.ToInteger() >= 1000)
                {
                    rules.Add(new Validation(() => patient.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }

                if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger() && patient.TertiaryInsurance.ToInteger() >= 1000)
                {
                    rules.Add(new Validation(() => patient.TertiaryInsurance.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }

                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (patient.IsValid && entityValidator.IsValid)
                {
                    patient.AgencyId = Current.AgencyId;
                    patient.Id = Guid.NewGuid();
                    patient.Encode(); // setting string arrays to one field
                    if (patient.Status == (int)PatientStatus.Active)
                    {
                        var workflow = new CreatePatientWorkflow(patient);
                        if (workflow.IsCommitted)
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Patient was created successfully.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = workflow.Message;
                        }
                    }
                    else
                    {
                        var workflow = new PendingPatientWorkFlow(patient);
                        if (workflow.IsCommitted)
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Patient was created successfully.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                        }
                    }
                    if (viewData.isSuccessful && !patient.ReferralId.IsEmpty())
                    {
                        if (referralRepository.SetStatus(Current.AgencyId, patient.ReferralId, ReferralStatus.Admitted))
                        {
                            Auditor.AddGeneralLog(LogDomain.Agency, Current.AgencyId, patient.ReferralId.ToString(), LogType.Referral, LogAction.ReferralAdmitted, string.Empty);
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = patient.ValidationMessage + "\n" + entityValidator.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Map(Guid patientId)
        {
            return PartialView("~/Views/Patient/Map.ascx", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Verify(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            return PartialView("Eligibility", patientService.VerifyEligibility(medicareNumber, lastName, firstName, dob, gender));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Center()
        {
            var viewData = new PatientCenterViewData();
            if (Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsOfficeManager || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                viewData.Count = patientRepository.GetPatientStatusCount(Current.AgencyId, (int)PatientStatus.Active);
            }
            else if (Current.IsClinicianOrHHA)
            {
                viewData.Count = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active).Count;
            }
            else { viewData.Count = 0; }

            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Data(Guid patientId)
        {
            var viewData = new PatientCenterViewData();
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            return PartialView(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Info(Guid patientId)
        {
            if (patientId.IsEmpty())
            {
                return PartialView(new Patient());
            }
            return PartialView(patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InfoPopup(Guid patientId) {
            return PartialView("Popup", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            return Json(patientRepository.Get(id, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult All(Guid branchId, byte statusId, byte paymentSourceId, string name)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                patientList = patientRepository.GetPatientSelection(Current.AgencyId,  branchId, statusId, paymentSourceId, name);
            }
            else if (Current.IsClinicianOrHHA)
            {
                patientList = patientRepository.GetUserPatients(Current.AgencyId,  branchId, Current.UserId, statusId);
            }
            else { }

            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllMedicare(Guid branchId, byte statusId, string paymentSourceId, string name)
        {
            var patientList = new List<PatientSelection>();
            int insurance;
            if (int.TryParse(paymentSourceId, out insurance))
            {
                if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
                {
                    patientList = patientRepository.GetPatientSelectionMedicare(Current.AgencyId,branchId, statusId, insurance, name);
                }
                else if (Current.IsClinicianOrHHA)
                {
                    patientList = patientRepository.GetUserPatients(Current.AgencyId,branchId, Current.UserId, statusId, paymentSourceId);
                }
                else { }
            }
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllInsurancePatient(Guid  branchId, byte statusId, string name, int insurnace)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                patientList = patientRepository.GetPatientSelectionAllInsurance(Current.AgencyId, branchId, statusId, name, insurnace);
            }
            else if (Current.IsClinicianOrHHA)
            {
                patientList = patientRepository.GetUserPatients(Current.AgencyId,branchId, Current.UserId, statusId, insurnace.ToString());
            }
            else { }
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activity(Guid patientId, string discipline, string dateRangeId, DateTime rangeStartDate, DateTime rangeEndDate)
        {
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            Check.Argument.IsNotNull(dateRangeId, "dateRangeId");

            var dateRange = new DateRange { Id = dateRangeId };
            List<ScheduleEvent> patientActivities = null;

            if (dateRangeId == "DateRange")
            {
                dateRange.EndDate = rangeEndDate;
                dateRange.StartDate = rangeStartDate;
                patientActivities = patientService.GetScheduledEvents(patientId, discipline, dateRange);
            }
            else
            {
                dateRange = dateService.GetDateRange(dateRangeId, patientId);
                patientActivities = patientService.GetScheduledEvents(patientId, discipline, dateRange);
            }

            return View(new GridModel(patientActivities));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewOrder()
        {
            return PartialView("Order/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewOrder(Guid patientId)
        {
            return PartialView("Order/New", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddOrder(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New order could not be saved." };
            if (order.IsValid)
            {
                order.Id = Guid.NewGuid();
                order.UserId = Current.UserId;
                order.AgencyId = Current.AgencyId;
                order.OrderNumber = patientRepository.GetNextOrderNumber();
                order.Created = DateTime.Now;
                if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
                {
                    if (order.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, order.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this order.";
                        return Json(viewData);
                    }
                    else
                    {
                        order.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            order.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                        }
                        if (!order.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                order.PhysicianData = physician.ToXml();
                            }
                        }
                    }
                }
                var newScheduleEvent = new ScheduleEvent
                {
                    EventId = order.Id,
                    UserId = order.UserId,
                    PatientId = order.PatientId,
                    EpisodeId = order.EpisodeId,
                    Status = order.Status.ToString(),
                    Discipline = Disciplines.Orders.ToString(),
                    EventDate = order.OrderDate.ToShortDateString(),
                    VisitDate = order.OrderDate.ToShortDateString(),
                    DisciplineTask = (int)DisciplineTasks.PhysicianOrder,
                    IsOrderForNextEpisode = order.IsOrderForNextEpisode
                };

                if (patientRepository.AddOrder(order))
                {
                    if (patientRepository.UpdateEpisode(Current.AgencyId, order.EpisodeId, order.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
                    {
                        Auditor.Log(order.EpisodeId, order.PatientId, order.Id, Actions.Add, (DisciplineTasks)newScheduleEvent.DisciplineTask);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Order has been saved successfully.";
                        if (order.Status == (int)ScheduleStatus.OrderToBeSentToPhysician)
                        {
                            viewData.errorMessage = "Order has been completed successfully.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Order could not be saved! Please try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Order could not be saved! Please try again.";
                }

            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = order.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianOrderPrint(Guid patientId, Guid orderId)
        {
            return View("Order/Print", patientService.GetOrderPrint(patientId, orderId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PhysicianOrderPdf(Guid patientId, Guid orderId)
        {
            PhysicianOrderPdf doc = new PhysicianOrderPdf(patientService.GetOrderPrint(patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=PhysicianOrder_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianOrderBlank()
        {
            return View("Order/Print", patientService.GetOrderPrint());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult PhysicianOrderPdfBlank()
        {
            PhysicianOrderPdf doc = new PhysicianOrderPdf(patientService.GetOrderPrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=PhysicianOrder_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditOrder(Guid id, Guid patientId)
        {
            var order = patientRepository.GetOrder(id, patientId, Current.AgencyId);
            if (order != null && !order.EpisodeId.IsEmpty())
            {
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, order.EpisodeId, order.PatientId);
                if (episode != null)
                {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;

                }
            }
            return PartialView("Order/Edit", order);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteOrder(Guid id, Guid patientId, Guid episodeId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order could not be deleted." };
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                if (patientService.DeletePhysicianOrder(id, patientId , episodeId))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Order has been deleted successfully.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Order could not be deleted! Please try again.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateOrder(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Order could not be updated." };
            if (order.IsValid)
            {
                order.AgencyId = Current.AgencyId;
                if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
                {
                    if (order.SignatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, order.SignatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature and/or date to complete this order.";
                        return Json(viewData);
                    }
                    else
                    {
                        order.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            order.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                        }
                        if (!order.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(order.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                order.PhysicianData = physician.ToXml();
                            }
                        }
                    }
                }
                else
                {
                    order.SignatureText = string.Empty;
                }

                var episode = patientRepository.GetEpisodeById(Current.AgencyId, order.EpisodeId, order.PatientId);
                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvets = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (scheduleEvets != null && scheduleEvets.Exists(e => e.EventId == order.Id && e.PatientId == order.PatientId))
                    {
                        var evnt = scheduleEvets.Single(e => e.EventId == order.Id && e.PatientId == order.PatientId);
                        if (evnt != null)
                        {
                            evnt.VisitDate = order.OrderDate.ToString("MM/dd/yyyy");
                            evnt.Status = order.Status.ToString();
                            evnt.ReturnReason = string.Empty;
                            evnt.IsOrderForNextEpisode = order.IsOrderForNextEpisode;
                            episode.Schedule = scheduleEvets.ToXml();
                            if (patientRepository.UpdateEpisode(episode))
                            {
                                var userEvent = userRepository.GetEvent(Current.AgencyId, evnt.UserId, evnt.PatientId, evnt.EventId);
                                if (userEvent != null)
                                {
                                    userEvent.EventDate = evnt.EventDate;
                                    userEvent.VisitDate = evnt.VisitDate;
                                    userEvent.Discipline = Disciplines.Orders.ToString();
                                    userEvent.Status = evnt.Status;
                                    userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                }
                                else
                                {
                                    var newUserEvent = new UserEvent
                                    {
                                        EventId = order.Id,
                                        UserId = order.UserId,
                                        PatientId = order.PatientId,
                                        EpisodeId = order.EpisodeId,
                                        Status = order.Status.ToString(),
                                        Discipline = Disciplines.Orders.ToString(),
                                        EventDate = evnt.EventDate,
                                        VisitDate = evnt.VisitDate,
                                        DisciplineTask = (int)DisciplineTasks.PhysicianOrder
                                    };
                                    patientRepository.AddNewUserEvent(Current.AgencyId, order.PatientId, newUserEvent);
                                }
                                if (evnt.Status.IsInteger())
                                {
                                    Auditor.Log(evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Add, (ScheduleStatus)evnt.Status.ToInteger(), DisciplineTasks.PhysicianOrder, string.Empty);
                                }
                            }
                            else
                            {
                                viewData.errorMessage = "Order don't get the right episode .Try again.";
                                viewData.isSuccessful = false;
                                return Json(viewData);
                            }
                        }
                        else
                        {
                            patientService.AddPhysicianOrderUserAndScheduleEvent(order, out order);
                        }
                    }
                    else
                    {
                        patientService.AddPhysicianOrderUserAndScheduleEvent(order, out order);
                    }
                }
                else
                {
                    patientService.AddPhysicianOrderUserAndScheduleEvent(order, out order);
                }

                if (patientRepository.UpdateOrder(order))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Order has been updated successfully.";
                    if (order.Status == (int)ScheduleStatus.OrderSubmittedPendingReview)
                    {
                        viewData.errorMessage = "Order has been completed and pending QA Review.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Order could not be saved! Please try again.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = order.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewFaceToFaceEncounter()
        {
            return PartialView("FaceToFaceEncounter/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddFaceToFaceEncounter([Bind]FaceToFaceEncounter faceToFaceEncounter)
        {
            Check.Argument.IsNotNull(faceToFaceEncounter, "faceToFaceEncounter");

            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Face-to-face Encounter could not be saved" };

            if (faceToFaceEncounter.IsValid)
            {
                faceToFaceEncounter.Id = Guid.NewGuid();
                if (patientService.CreateFaceToFaceEncounter(faceToFaceEncounter))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Face-to-face Encounter successfully saved";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Face-to-face Encounter.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = faceToFaceEncounter.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianFaceToFaceEncounterPrint(Guid patientId, Guid orderId)
        {
            var note = patientService.GetFaceToFacePrint(patientId, orderId);
            var xml = new PhysFaceToFaceXml(note);
            note.PrintViewJson = xml.GetJson();
            return View("FaceToFaceEncounter/Print", note);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PhysicianFaceToFaceEncounterBlank()
        {
            return View("FaceToFaceEncounter/Print", patientService.GetFaceToFacePrint());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PhysicianFaceToFaceEncounterPdf(Guid patientId, Guid orderId)
        {
            PhysFaceToFacePdf doc = new PhysFaceToFacePdf(patientService.GetFaceToFacePrint(patientId, orderId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=FaceToFace_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult ViewFaceToFaceEncounterPdfBlank()
        {
            PhysFaceToFacePdf doc = new PhysFaceToFacePdf(patientService.GetFaceToFacePrint());
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=FaceToFace_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateOrderStatus(Guid eventId, Guid patientId, Guid episodeId, string orderType, string actionType, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Order could not been updated." };
            if (orderType == "PhysicianOrder")
            {
                if (patientService.ProcessPhysicianOrder(episodeId, patientId, eventId, actionType, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The order has been updated successfully.";
                }
            }
            else if (orderType == "PlanofCare" || orderType == "PlanofCareStandAlone")
            {
                if (assessmentService.UpdatePlanofCareStatus(episodeId, patientId, eventId, actionType, reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The order has been updated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetOrder(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return Json(patientRepository.GetOrder(Id, patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientProfilePrint(Guid id)
        {
            return View("Profile", patientService.GetProfile(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PatientProfilePdf(Guid id)
        {
            PatientProfilePdf doc = new PatientProfilePdf(patientService.GetProfile(id));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=PatientProfile_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TriageClassification(Guid id)
        {
            return View("TriageClassification", patientService.GetProfile(id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Grid()
        {
            return View("List");
        }

        [GridAction]
        public ActionResult List()
        {
            var patientList = patientRepository.All(Current.AgencyId);
            return View(new GridModel(patientList));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingGrid()
        {
            return View("Pending");
        }

        [GridAction]
        public ActionResult PendingList()
        {
            var patientList = patientService.GetPendingPatients();
            return View(new GridModel(patientList));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NonAdmitGrid()
        {
            return View("NonAdmitList");
        }

        [GridAction]
        public ActionResult NonAdmitList()
        {
            var nonAdmitList = patientService.GetNonAdmits();
            return View(new GridModel(nonAdmitList));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HospitalizationGrid()
        {
            return View("HospitalizationList");
        }

        [GridAction]
        public ActionResult HospitalizationList()
        {
            var patientList = patientRepository.GetHospitalizedPatients(Current.AgencyId);
            if (patientList != null && patientList.Count > 0)
            {
                patientList.ForEach(d =>
                {
                    d.User = !d.UserId.IsEmpty() ? UserEngine.GetName(d.UserId, Current.AgencyId) : string.Empty;
                });
            }
            return View(new GridModel(patientList));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HospitalizationLogs(Guid patientId)
        {
            var viewData = new HospitalizationViewData();
            viewData.Logs = patientService.GetHospitalizationLogs(Current.AgencyId, patientId);
            viewData.Patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            return View("Hospitalization/List", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewHospitalizationLog(Guid patientId)
        {
            return PartialView("Hospitalization/New", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsertHospitalizationLog(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new hospitalization log could not be added." };
            if (patientService.AddHospitalizationLog(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The new hospitalization log was added successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditHospitalizationLog(Guid patientId, Guid hospitalizationLogId)
        {
            return PartialView("Hospitalization/Edit", patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, hospitalizationLogId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateHospitalizationLog(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Hospitalization Log could not be updated. Please try again." };

            if (patientService.UpdateHospitalizationLog(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The Hospitalization Log was updated successfully.";
            }
            
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult HospitalizationLogPdf(Guid patientId, Guid hospitalizationLogId)
        {
            var doc = new HospitalizationLogPdf(patientService.GetHospitalizationLog(patientId, hospitalizationLogId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=HospitalizationLog_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateHospitalizationLogStatus(Guid allergyProfileId, Guid allergyId, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Allergy status could not be updated." };
            if (patientService.UpdateAllergy(allergyProfileId, allergyId, isDeprecated))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Allergy was updated successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewPhoto(Guid patientId)
        {
            return PartialView("Photo", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddPhoto([Bind] Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient photo could not be saved." };

            if (patientService.IsValidImage(Request.Files))
            {
                if (patientService.AddPhoto(patientId, Request.Files))
                {
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.errorMessage = "File uploaded is not a valid image.";
            }

            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult RemovePhoto(Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient photo could not be saved." };

            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {

                if (patientService.UpdatePatientForPhotoRemove(patient))
                {
                    viewData.isSuccessful = true;
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditPatientContent(Guid patientId)
        {
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            //if (patient != null)
            //{
            //    var managedDate = patientRepository.GetPatientLatestAdmissionDate(Current.AgencyId, patientId, (int)PatientDateType.StartOfCareDate);
            //    if (managedDate != null && managedDate.StartOfCareDate > DateTime.MinValue)
            //    {
            //        patient.StartofCareDate = managedDate.StartOfCareDate;
            //    }
            //}
            return PartialView("~/Views/Patient/Edit.ascx", patient);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit([Bind] Patient patient)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Patient could not be edited" };
            if (patient != null && !patient.Id.IsEmpty())
            {
                var rules = new List<Validation>();

                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.FirstName), "Patient first name is required. <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.LastName), "Patient last name is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.DOB.ToString()), "Patient date of birth is required. <br/>"));
                rules.Add(new Validation(() => !patient.DOB.ToString().IsValidDate(), "Date Of birth  for the patient is not in the valid range.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.Gender), "Patient gender has to be selected.  <br/>"));
                rules.Add(new Validation(() => (patient.EmailAddress == null ? !string.IsNullOrEmpty(patient.EmailAddress) : !patient.EmailAddress.IsEmail()), "Patient e-mail is not in a valid  format.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressLine1), "Patient address line is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressCity), "Patient city is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressStateCode), "Patient state is required.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.AddressZipCode), "Patient zip is required.  <br/>"));
                rules.Add(new Validation(() => !string.IsNullOrEmpty(patient.SSN) ? !patient.SSN.IsSSN() : false, "Patient SSN is not in valid format.  <br/>"));
                rules.Add(new Validation(() => string.IsNullOrEmpty(patient.StartofCareDate.ToString()), "Patient Start of care date is required.  <br/>"));
                rules.Add(new Validation(() => !patient.StartofCareDate.ToString().IsValidDate(), "Patient Start of care date is not in valid format.  <br/>"));

                if (patient.PatientIdNumber.IsNotNullOrEmpty())
                {
                    bool patientIdCheck = patientRepository.IsPatientIdExistForEdit(Current.AgencyId, patient.Id, patient.PatientIdNumber);
                    rules.Add(new Validation(() => patientIdCheck, "Patient Id Number already exists."));
                }
                if (patient.MedicareNumber.IsNotNullOrEmpty())
                {
                    bool medicareNumberCheck = patientRepository.IsMedicareExistForEdit(Current.AgencyId, patient.Id, patient.MedicareNumber);
                    rules.Add(new Validation(() => medicareNumberCheck, "Medicare Number already exists."));
                }
                if (patient.MedicaidNumber.IsNotNullOrEmpty())
                {
                    bool medicaidNumberCheck = patientRepository.IsMedicaidExistForEdit(Current.AgencyId, patient.Id, patient.MedicaidNumber);
                    rules.Add(new Validation(() => medicaidNumberCheck, "Medicaid Number already exists."));
                }
                if (patient.SSN.IsNotNullOrEmpty())
                {
                    bool ssnNumberCheck = patientRepository.IsSSNExistForEdit(Current.AgencyId, patient.Id, patient.SSN);
                    rules.Add(new Validation(() => ssnNumberCheck, "SSN Number already exists."));
                }
                if (patient.PrimaryInsurance.IsNotNullOrEmpty() && patient.PrimaryInsurance.IsInteger() && patient.PrimaryInsurance.ToInteger() >= 1000)
                {
                    rules.Add(new Validation(() => patient.PrimaryHealthPlanId.IsNullOrEmpty(), "Primary Insurance Health Plan Id is required."));
                }
                if (patient.SecondaryInsurance.IsNotNullOrEmpty() && patient.SecondaryInsurance.IsInteger() && patient.SecondaryInsurance.ToInteger() >= 1000)
                {
                    rules.Add(new Validation(() => patient.SecondaryHealthPlanId.IsNullOrEmpty(), "Secondary Insurance Health Plan Id is required."));
                }

                if (patient.TertiaryInsurance.IsNotNullOrEmpty() && patient.TertiaryInsurance.IsInteger() && patient.TertiaryInsurance.ToInteger() >= 1000)
                {
                    rules.Add(new Validation(() => patient.TertiaryInsurance.IsNullOrEmpty(), "Tertiary Insurance Health Plan Id is required."));
                }
                var entityValidator = new EntityValidator(rules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {

                    patient.AgencyId = Current.AgencyId;
                    patient.Encode();// setting string arrays to one field
                    if (patientService.EditPatient(patient))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your Data successfully edited";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the data.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetEmergencyContact(Guid patientId, Guid EmergencyContactId)
        {
            Check.Argument.IsNotEmpty(EmergencyContactId, "EmergencyContactId");
            return Json(patientRepository.GetEmergencyContact(patientId, EmergencyContactId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewEmergencyContact(Guid PatientId, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            var viewData = Validate<JsonViewData>(
                           new Validation(() => string.IsNullOrEmpty(emergencyContact.FirstName), "Emergency Contact first name is required. <br/>"),
                           new Validation(() => string.IsNullOrEmpty(emergencyContact.LastName), "Emergency Contact last name is required.  <br/>"),
                           new Validation(() => (emergencyContact.EmailAddress == null ? !string.IsNullOrEmpty(emergencyContact.EmailAddress) : !emergencyContact.EmailAddress.IsEmail()), "Emergency Contact e-mail is not in a valid  format.  <br/>"),
                           new Validation(() => emergencyContact.PhonePrimaryArray == null || !(emergencyContact.PhonePrimaryArray.Count > 0), "Phone is required.  <br/>")
                      );
            if (viewData.isSuccessful)
            {
                if (patientService.NewEmergencyContact(emergencyContact, PatientId))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Data successfully added";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in editing the data.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditEmergencyContact(Guid Id, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            var viewData = Validate<JsonViewData>(
                           new Validation(() => string.IsNullOrEmpty(emergencyContact.FirstName), "Emergency Contact first name is required. <br/>"),
                           new Validation(() => string.IsNullOrEmpty(emergencyContact.LastName), "Emergency Contact last name is required.  <br/>"),
                           new Validation(() => (emergencyContact.EmailAddress == null ? !string.IsNullOrEmpty(emergencyContact.EmailAddress) : !emergencyContact.EmailAddress.IsEmail()), "Emergency Contact e-mail is not in a valid  format.  <br/>"),
                           new Validation(() => emergencyContact.PhonePrimaryArray == null || !(emergencyContact.PhonePrimaryArray.Count > 0), "Phone is required.  <br/>")
                      );
            if (viewData.isSuccessful)
            {
                if (patientService.EditEmergencyContact(emergencyContact))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Emergency contact successfully updated.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Emergency contact could not be updated. Please try again.";
                }
            }
            return Json(viewData);
        }

        [GridAction]
        public ActionResult GetEmergencyContacts(Guid PatientId)
        {
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            return Json(new GridModel { Data = patientRepository.GetEmergencyContacts(PatientId) });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteEmergencyContact(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Data Is Not deleted. Try Again." };
            if (patientService.DeleteEmergencyContact(id, patientId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Data Is Not deleted. Try Again." };
            if (patientService.DeletePatient(patientId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddPatientPhysicain(Guid id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your data is not added. Try Again." };
            if (!physicianRepository.DoesPhysicianExist(patientId, id))
            {
                if (patientService.LinkPhysician(patientId, id, false))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your data is successfully added.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Physician already exists.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetPatientPhysicianContact(Guid PhysicianContactId, Guid PatientId)
        {
            Check.Argument.IsNotEmpty(PhysicianContactId, "PhysicianContactId");
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            return Json(physicianRepository.GetByPatientId(PhysicianContactId, PatientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletePhysicianContact(Guid id, Guid patientID)
        {
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(patientID, "patientID");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your data is not deleted. Try Again." };
            if (patientService.UnlinkPhysician(patientID, id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in deleting the data.";
            }
            return Json(viewData);
        }

        [GridAction]
        public ActionResult GetPhysicians(Guid PatientId)
        {
            Check.Argument.IsNotEmpty(PatientId, "PatientId");
            return Json(new GridModel(physicianRepository.GetPatientPhysicians(PatientId, Current.AgencyId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewCommunicationNote()
        {
            return PartialView("CommunicationNote/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewCommunicationNote(Guid patientId)
        {
            return PartialView("CommunicationNote/New", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddCommunicationNote([Bind]CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");

            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Communication Note could not be saved" };

            if (communicationNote.IsValid)
            {
                if (communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature)
                {
                    var rules = new List<Validation>();
                    rules.Add(new Validation(() => string.IsNullOrEmpty(communicationNote.SignatureText), "User Signature can't be empty."));
                    rules.Add(new Validation(() => communicationNote.SignatureText.IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, communicationNote.SignatureText) : false, "User Signature is not correct."));
                    rules.Add(new Validation(() => !(communicationNote.SignatureDate > DateTime.MinValue), "Signature date is not valid."));
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        communicationNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            communicationNote.Status = (int)ScheduleStatus.NoteCompleted;
                        }
                        if (!communicationNote.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(communicationNote.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                communicationNote.PhysicianData = physician.ToXml();
                            }
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                        return Json(viewData);
                    }
                }
                else
                {
                    communicationNote.SignatureText = string.Empty;
                }
                communicationNote.Id = Guid.NewGuid();
                communicationNote.UserId = Current.UserId;
                communicationNote.AgencyId = Current.AgencyId;
                if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0)
                {
                    communicationNote.Recipients = communicationNote.RecipientArray.ToXml();
                }

                var newScheduleEvent = new ScheduleEvent
                {
                    EventId = communicationNote.Id,
                    UserId = communicationNote.UserId,
                    EpisodeId = communicationNote.EpisodeId,
                    PatientId = communicationNote.PatientId,
                    Status = communicationNote.Status.ToString(),
                    Discipline = Disciplines.ReportsAndNotes.ToString(),
                    EventDate = communicationNote.Created.ToShortDateString(),
                    VisitDate = communicationNote.Created.ToShortDateString(),
                    DisciplineTask = (int)DisciplineTasks.CommunicationNote
                };

                if (patientRepository.UpdateEpisode(Current.AgencyId, communicationNote.EpisodeId, communicationNote.PatientId, new List<ScheduleEvent> { newScheduleEvent }))
                {
                    Auditor.Log(communicationNote.EpisodeId, communicationNote.PatientId, communicationNote.Id, Actions.Add, DisciplineTasks.CommunicationNote);
                    if (patientRepository.AddCommunicationNote(communicationNote))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Communication note successfully saved";

                        if (communicationNote.SendAsMessage == true &&
                            (communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature
                            || communicationNote.Status == (int)ScheduleStatus.NoteCompleted))
                        {
                            if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0)
                            {
                                var message = new Message
                                {
                                    Type = MessageType.User,
                                    AgencyId = Current.AgencyId,
                                    Body = communicationNote.Text,
                                    Subject = "Communication Note Message",
                                    PatientId = communicationNote.PatientId,
                                    Recipients = communicationNote.RecipientArray
                                };

                                if (messageService.SendMessage(message, null))
                                {
                                    viewData.errorMessage = "Communication note successfully saved and sent to receipients";
                                }
                                else
                                {
                                    viewData.isSuccessful = false;
                                    viewData.errorMessage = "Communication note saved but notification could not be sent to the recipients";
                                }
                            }
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error saving the communication note.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error saving the communication note.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = communicationNote.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetCommunicationNote(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(Id, "Id");
            var communicationNote = patientRepository.GetCommunicationNote(Id, patientId, Current.AgencyId);
            if (communicationNote != null && !communicationNote.PhysicianId.IsEmpty())
            {
                AgencyPhysician physician = null;
                if ((communicationNote.Status == (int)ScheduleStatus.NoteCompleted || communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature) && !communicationNote.PhysicianId.IsEmpty() && communicationNote.PhysicianData.IsNotNullOrEmpty())
                {
                    physician = communicationNote.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        communicationNote.PhysicianName =  physician.DisplayName ;
                    }
                    else
                    {
                        physician = PhysicianEngine.Get(communicationNote.PhysicianId, Current.AgencyId);
                        communicationNote.PhysicianName = physician != null ? physician.DisplayName : string.Empty;
                    }
                }
                else
                {
                     physician = PhysicianEngine.Get(communicationNote.PhysicianId, Current.AgencyId);
                    communicationNote.PhysicianName = physician != null ? physician.DisplayName : string.Empty;
                }
            }
            return Json(communicationNote);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditCommunicationNote(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var communicationNote = patientRepository.GetCommunicationNote(Id, patientId, Current.AgencyId);
            if (communicationNote != null)
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    communicationNote.DisplayName = patient.DisplayName;
                }
                communicationNote.RecipientArray = communicationNote.Recipients.IsNotNullOrEmpty() ? communicationNote.Recipients.ToObject<List<Guid>>() : new List<Guid>();
                communicationNote.SignatureDate = DateTime.Now;
                if (!communicationNote.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, communicationNote.EpisodeId, communicationNote.PatientId);
                    if (episode != null)
                    {
                        communicationNote.EpisodeEndDate = episode.EndDateFormatted;
                        communicationNote.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
            }
            else
            {
                communicationNote = new CommunicationNote();
            }
            return PartialView("CommunicationNote/Edit", communicationNote);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateCommunicationNote(CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");

            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Communication Note could not be saved" };
            if (communicationNote.IsValid)
            {
                if (communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature)
                {
                    var rules = new List<Validation>();
                    rules.Add(new Validation(() => string.IsNullOrEmpty(communicationNote.SignatureText), "User Signature can't be empty."));
                    rules.Add(new Validation(() => communicationNote.SignatureText.IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, communicationNote.SignatureText) : false, "User Signature is not correct."));
                    rules.Add(new Validation(() => !(communicationNote.SignatureDate > DateTime.MinValue), "Signature date is not valid."));
                    var entityValidator = new EntityValidator(rules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        communicationNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                        if (Current.HasRight(Permissions.BypassCaseManagement))
                        {
                            communicationNote.Status = (int)ScheduleStatus.NoteCompleted;
                        }
                        if (!communicationNote.PhysicianId.IsEmpty())
                        {
                            var physician = physicianRepository.Get(communicationNote.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                communicationNote.PhysicianData = physician.ToXml();
                            }
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = entityValidator.Message;
                        return Json(viewData);
                    }
                }
                else
                {
                    communicationNote.SignatureText = string.Empty;
                }
                communicationNote.AgencyId = Current.AgencyId;
                if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0)
                {
                    communicationNote.Recipients = communicationNote.RecipientArray.ToXml();
                }

                var episode = patientRepository.GetEpisodeById(Current.AgencyId, communicationNote.EpisodeId, communicationNote.PatientId);
                if (episode != null && episode.Schedule.IsNotNullOrEmpty())
                {
                    var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                    if (scheduleEvents != null && scheduleEvents.Exists(e => e.EventId == communicationNote.Id && e.PatientId == communicationNote.PatientId))
                    {
                        var evnt = scheduleEvents.Single(e => e.EventId == communicationNote.Id && e.PatientId == communicationNote.PatientId);
                        if (evnt != null)
                        {
                            evnt.VisitDate = communicationNote.Created.ToString("MM/dd/yyyy");
                            evnt.Discipline = Disciplines.ReportsAndNotes.ToString();
                            evnt.Status = communicationNote.Status.ToString();
                            evnt.ReturnReason = string.Empty;
                            episode.Schedule = scheduleEvents.ToXml();
                            if (patientRepository.UpdateEpisode(episode))
                            {
                                var userEvent = userRepository.GetEvent(Current.AgencyId, evnt.UserId, evnt.PatientId, evnt.EventId);
                                if (userEvent != null)
                                {
                                    userEvent.EventDate = evnt.EventDate;
                                    userEvent.VisitDate = evnt.VisitDate;
                                    userEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                                    userEvent.Status = evnt.Status;
                                    userRepository.UpdateEvent(Current.AgencyId, userEvent);
                                }
                                else
                                {
                                    var newUserEvent = new UserEvent
                                    {
                                        EventId = communicationNote.Id,
                                        UserId = communicationNote.UserId,
                                        PatientId = communicationNote.PatientId,
                                        EpisodeId = communicationNote.EpisodeId,
                                        Status = communicationNote.Status.ToString(),
                                        Discipline = Disciplines.ReportsAndNotes.ToString(),
                                        EventDate = evnt.EventDate,
                                        VisitDate = evnt.VisitDate,
                                        DisciplineTask = (int)DisciplineTasks.CommunicationNote
                                    };
                                    patientRepository.AddNewUserEvent(Current.AgencyId, communicationNote.PatientId, newUserEvent);
                                }
                                if (evnt.Status.IsInteger())
                                {
                                    Auditor.Log(evnt.EpisodeId, evnt.PatientId, evnt.EventId, Actions.Add, (ScheduleStatus)evnt.Status.ToInteger(), DisciplineTasks.CommunicationNote, string.Empty);
                                }

                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Error in updating the data. Try again.";
                                return Json(viewData);
                            }
                        }
                        else
                        {
                            patientService.AddCommunicationNoteUserAndScheduleEvent(communicationNote, out communicationNote);
                        }
                    }
                    else
                    {
                        patientService.AddCommunicationNoteUserAndScheduleEvent(communicationNote, out communicationNote);
                    }
                }
                else
                {
                    patientService.AddCommunicationNoteUserAndScheduleEvent(communicationNote, out communicationNote);
                }

                if (patientRepository.EditCommunicationNote(communicationNote))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Communication note successfully saved";

                    if (communicationNote.Status == (int)ScheduleStatus.NoteSubmittedWithSignature 
                        || communicationNote.Status == (int)ScheduleStatus.NoteCompleted)
                    {
                        if (communicationNote.RecipientArray != null && communicationNote.RecipientArray.Count > 0)
                        {
                            var message = new Message
                            {
                                Type = MessageType.User,
                                AgencyId = Current.AgencyId,
                                Body = communicationNote.Text,
                                Subject = "Communication Note Message",
                                PatientId = communicationNote.PatientId,
                                Recipients = communicationNote.RecipientArray
                            };

                            if (messageService.SendMessage(message, null))
                            {
                                viewData.errorMessage = "Communication note successfully saved and sent to recipients";
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Communication note saved but notification could not be sent to the recipients";
                            }
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in Saving the data.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = communicationNote.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CommunicationNotesView()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("CommunicationNote/List");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteCommunicationNote(Guid Id, Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(Id, "Id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Data Is Not deleted. Try Again." };
            if (patientService.DeleteCommunicationNote(Id, patientId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully deleted.";
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Error in Deleting the data.";
            }
            return Json(viewData);

        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CommunicationNotes(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(patientService.GetCommunicationNotes( BranchId,  Status,StartDate, EndDate)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientCommunicationNotesView(Guid patientId)
        {
            return PartialView("CommunicationList", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientCommunicationNotes(Guid patientId)
        {
            return View(new GridModel(patientService.GetCommunicationNotes(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CommunicationNotePrint(Guid patientId, Guid eventId)
        {
            return PartialView("CommunicationNote/Print", patientService.GetCommunicationNotePrint(eventId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult CommunicationNotePdf(Guid patientId, Guid eventId)
        {
            ComNotePdf doc = new ComNotePdf(patientService.GetCommunicationNotePrint(eventId, patientId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=CommunicationNote_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ProcessCommunicationNotes(string button, Guid patientId, Guid eventId, string reason)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your communication note could not be saved." };
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                if (button == "Approve")
                {
                    if (patientService.ProcessCommunicationNotes(button, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your communication note has been successfully approved.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your communication note could not be approved.";
                    }
                }
                else if (button == "Return")
                {
                    if (patientService.ProcessCommunicationNotes(button, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your communication note has been successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your communication note could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetNote(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new NoteViewData { isSuccessful = false };
            var patientNote = patientRepository.GetNote(patientId);
            if (patientNote != null)
            {
                viewData.Id = patientNote.Id;
                viewData.Note = patientNote.Note;
                viewData.PatientId = patientNote.PatientId;
                viewData.isSuccessful = true;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Note(Guid patientId, string patientNote)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Note Is Not Saved. Try Again." };
            var guid = patientRepository.Note(patientId, patientNote);
            if (!guid.IsEmpty())
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Note Successfully Saved.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DateRange(string dateRangeId, Guid patientId)
        {
            return Json(dateService.GetDateRange(dateRangeId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewEmergencyContactContent(Guid patientId)
        {
            return PartialView("~/Views/Patient/EmergencyContact/New.ascx", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditEmergencyContactContent(Guid patientId, Guid Id)
        {
            return PartialView("~/Views/Patient/EmergencyContact/Edit.ascx", patientRepository.GetEmergencyContact(patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewAdmit(Guid id, NonAdmitTypes type)
        {
            Check.Argument.IsNotNull(id, "id");

            PendingPatient pending = null;
            if (type == NonAdmitTypes.Patient)
            {
                var patient = patientRepository.Get(id, Current.AgencyId);
                if (patient != null)
                {
                    pending = new PendingPatient
                    {
                        Id = patient.Id,
                        DisplayName = patient.DisplayName,
                        PatientIdNumber = patient.PatientIdNumber,
                        StartofCareDate = patient.StartofCareDate,
                        ReferralDate = patient.ReferralDate,
                        CaseManagerId = patient.CaseManagerId,
                        PrimaryInsurance = patient.PrimaryInsurance,
                        SecondaryInsurance = patient.SecondaryInsurance,
                        TertiaryInsurance = patient.TertiaryInsurance,
                        Type = NonAdmitTypes.Patient,
                        Payer = patient.Payer,
                        UserId = patient.UserId
                    };
                }
            }
            else
            {
                var referral = referralRepository.Get(Current.AgencyId, id);
                if (referral != null)
                {
                    pending = new PendingPatient
                    {
                        Id = referral.Id,
                        DisplayName = referral.DisplayName,
                        PatientIdNumber = string.Empty,
                        StartofCareDate = DateTime.Today,
                        ReferralDate = referral.ReferralDate,
                        PrimaryInsurance = string.Empty,
                        SecondaryInsurance = string.Empty,
                        TertiaryInsurance = string.Empty,
                        Type = NonAdmitTypes.Referral,
                        UserId = referral.UserId
                    };
                }
            }

            return PartialView("Admit", pending);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddAdmit([Bind] PendingPatient patient)
        {
            Check.Argument.IsNotNull(patient, "PendingPatient");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient could not be admitted." };
            patient.AgencyId = Current.AgencyId;
            
            if (patient.IsValid)
            {
                if (patientService.AdmitPatient(patient))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Patient has been admitted successfully.";
                }
            }
            else
            {
                viewData.errorMessage = patient.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewNonAdmit(Guid patientId)
        {
            return PartialView("NonAdmit", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddNonAdmit([Bind] PendingPatient patient)
        {
            Check.Argument.IsNotNull(patient, "PendingPatient");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Non-Admission of patient could not be saved." };
            patient.AgencyId = Current.AgencyId;
            if (patientService.NonAdmitPatient(patient))
            {
              
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient non-admission successful.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicationDischarge()
        {
            return PartialView("MedicationProfile/Discharge");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LastestMedications(Guid patientId)
        {
            var medications = string.Empty;
            var medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medicationProfile != null)
            {
                medications = medicationProfile.ToString();
            }
            return Json(medications);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Medication(Guid medId, string medicationCategory, string assessmentType)
        {
            var medicationProfile = patientRepository.GetMedicationProfile(medId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var list = medicationProfile.Medication.ToObject<List<Medication>>();
                return View(new GridModel(list.FindAll(m => m.MedicationCategory == medicationCategory).OrderByDescending(l => l.StartDateSortable)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Medications(Guid medicationProfileId)
        {
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            return View("MedicationProfile/Medication/List", medicationProfile);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DrugDrugInteractions(Guid medicationProfileId)
        {
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            return View("MedicationProfile/Medication/DrugDrug", medicationProfile);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult DrugDrugInteractionsPdf(Guid patientId, List<string> drugsSelected)
        {
            var doc = new DrugDrugInteractionsPdf(patientService.GetDrugDrugInteractionsPrint(patientId, drugsSelected));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=DrugDrugInteractions_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationsForOasis(Guid medicationProfileId, string assessmentType)
        {
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            return View("MedicationProfile/ProfileGrid", new OasisMedicationProfileViewData { Id = medicationProfile.Id, AssessmentType = assessmentType, Profile = medicationProfile });
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationSnapshot(Guid medId)
        {
            var medicationProfile = patientRepository.GetMedicationProfile(medId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var list = medicationProfile.Medication.ToObject<List<Medication>>();
                return View(new GridModel(list.FindAll(l => l.MedicationCategory == "Active").OrderByDescending(l => l.StartDateSortable)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InsertMedication(Guid medId, Medication medication, string MedicationType, string medicationCategory, string assessmentType)
        {
            var medProfile = patientRepository.InsertMedication(medId, Current.AgencyId, medication, MedicationType);
            if (medProfile != null && medProfile.Medication.IsNotNullOrEmpty())
            {
                var list = medProfile.Medication.ToObject<List<Medication>>();
                return View(new GridModel(list.FindAll(m => m.MedicationCategory == medicationCategory).OrderByDescending(l => l.StartDateSortable)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsertNewMedication(Guid medicationProfileId, [Bind] Medication medication, string medicationType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new medication could not be added to the medication profile." };
            if (medication != null)
            {
                if (patientService.AddMedication(medicationProfileId, medication, medicationType))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The new medication was added to the medication profile successfully.";
                }
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateMedicationSnapshotHistory(Guid Id, Guid patientId, DateTime signedDate)
        {
            var medicationProfileSnapShot = patientRepository.GetMedicationProfileHistory(Id, Current.AgencyId);
            if (medicationProfileSnapShot != null)
            {
                medicationProfileSnapShot.SignedDate = signedDate;
                if (patientRepository.UpdateMedicationProfileHistory(medicationProfileSnapShot))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, medicationProfileSnapShot.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationHistoryUpdated, string.Empty);
                    return View(new GridModel(patientService.GetMedicationHistoryForPatient(patientId)));
                }
            }
            var list = new List<MedicationProfileHistory>();
            return View(new GridModel(list));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateMedication(Guid medId, Medication medication, string MedicationType, string medicationCategory, string assessmentType)
        {
            patientRepository.UpdateMedication(medId, Current.AgencyId, medication, MedicationType);
            var medProfile = patientRepository.GetMedicationProfile(medId, Current.AgencyId);
            if (medProfile != null && medProfile.Medication != null)
            {
                var list = medProfile.Medication.ToObject<List<Medication>>();
                return View(new GridModel(list.FindAll(m => m.MedicationCategory == medicationCategory).OrderByDescending(l => l.StartDateSortable)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdatePatientMedication([Bind] Medication medication, string medicationType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The medication could not be updated. Please try again." };
            if (medication != null)
            {
                if (patientService.UpdateMedication(medication.ProfileId, medication, medicationType))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The medication was updated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateMedicationStatus(Guid medProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The medication could not be updated. Please try again." };
            if (patientService.UpdateMedicationStatus(medProfileId, medicationId, medicationCategory, dischargeDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The medication was updated successfully.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateMedicationForDischarge(Guid medId, Guid patientId, DateTime dischargeDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your medication could not be discharged." };
            if (patientRepository.UpdateMedicationForDischarge(medId, Current.AgencyId, patientId, dischargeDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your medication has been discharged.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteMedication(Guid medId, Medication medication, string medicationCategory, string assessmentType)
        {
            patientRepository.DeleteMedication(medId, Current.AgencyId, medication);
            var medProfile = patientRepository.GetMedicationProfile(medId, Current.AgencyId);
            if (medProfile != null && medProfile.Medication.IsNotNullOrEmpty())
            {
                var list = medProfile.Medication.ToObject<List<Medication>>();
                return View(new GridModel(list.FindAll(m => m.MedicationCategory == medicationCategory).OrderByDescending(l => l.StartDateSortable)));
            }
            else
            {
                var list = new List<Medication>();
                return View(new GridModel(list));
            }
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteMedicationSnapshotHistory(Guid Id, Guid patientId)
        {
            var medicationProfileSnapShot = patientRepository.GetMedicationProfileHistory(Id, Current.AgencyId);
            if (medicationProfileSnapShot != null)
            {
                medicationProfileSnapShot.IsDeprecated = true;
                if (patientRepository.UpdateMedicationProfileHistory(medicationProfileSnapShot))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, medicationProfileSnapShot.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationHistoryDeleted, string.Empty);
                    return View(new GridModel(patientService.GetMedicationHistoryForPatient(patientId)));
                }
            }
            var list = new List<MedicationProfileHistory>();
            return View(new GridModel(list));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveMedicationProfile(MedicationProfile medicationProfile)
        {
            Check.Argument.IsNotNull(medicationProfile, "medicationProfile");
            var viewData = Validate<JsonViewData>();
            viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Medication Profile Is Not Saved. Try Again." };
            medicationProfile.AgencyId = Current.AgencyId;
            if (patientRepository.SaveMedicationProfile(medicationProfile))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your data is successfully saved.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SignMedicationHistory(MedicationProfileHistory medicationProfileHistory)
        {
            Check.Argument.IsNotNull(medicationProfileHistory, "medicationProfileHistory");

            var viewData = Validate<JsonViewData>();
            viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Medication Profile Snapshot could not be created. Try Again." };
            viewData = Validate<JsonViewData>(
                   new Validation(() => string.IsNullOrEmpty(medicationProfileHistory.Signature), "The signature field is empty."),
                   new Validation(() => !userService.IsSignatureCorrect(Current.UserId, medicationProfileHistory.Signature), "The signature provided is not correct."),
                   new Validation(() => !medicationProfileHistory.SignedDate.IsValid(), "The signature date is not valid.")
                   );
            if (viewData.isSuccessful)
            {
                if (patientService.SignMedicationHistory(medicationProfileHistory))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Medication Profile Snapshot was created successfully.";
                }
                else
                {
                    viewData.isSuccessful = false;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationProfileSnapShotHistory(Guid patientId)
        {
            return PartialView("MedicationProfile/SnapShotHistory", patientRepository.Get(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationSnapshotHistory(Guid patientId)
        {
            return View(new GridModel(patientService.GetMedicationHistoryForPatient(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationProfileSnapShot(Guid patientId)
        {
            var medProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var viewData = new MedicationProfileViewData();
            if (medProfile != null)
            {
                viewData.MedicationProfile = medProfile;
                viewData.Allergies = patientService.GetAllergies(patientId);
                viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
                if (viewData.Patient != null)
                {
                    if (viewData.Patient.PhysicianContacts != null && viewData.Patient.PhysicianContacts.Count > 0)
                    {
                        var physician = viewData.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physician != null)
                        {
                            viewData.PhysicianId = physician.Id;
                        }
                    }
                    viewData.PharmacyName = viewData.Patient.PharmacyName;
                    viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                }
                var currentEpisode = patientRepository.GetCurrentEpisode(Current.AgencyId, patientId);
                if (currentEpisode != null)
                {
                    viewData.EpisodeId = currentEpisode.Id;
                    if (!currentEpisode.Id.IsEmpty() && !patientId.IsEmpty())
                    {
                        var assessment = assessmentService.GetEpisodeAssessment(currentEpisode.Id, patientId);
                        if (assessment != null)
                        {
                            var diagnosis = assessmentService.Diagnosis(assessment);
                            if (diagnosis != null && diagnosis.Count > 0)
                            {
                                viewData.Questions = diagnosis;
                            }
                        }
                    }
                }
            }
            return PartialView("MedicationProfile/Sign", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewMedication(Guid medProfileId)
        {
            return PartialView("MedicationProfile/Medication/New", medProfileId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditMedication(Guid medProfileId, Guid medicationId)
        {
            var medication = new Medication();
            var medicationProfile = patientRepository.GetMedicationProfile(medProfileId, Current.AgencyId);
            if (medicationProfile != null)
            {
                medication = medicationProfile.Medication.ToObject<List<Medication>>().SingleOrDefault(m => m.Id == medicationId);
                medication.ProfileId = medicationProfile.Id;
            }
            return PartialView("MedicationProfile/Medication/Edit", medication);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DischargeMedication(Guid medProfileId, Guid medicationId)
        {
            var medication = new Medication();
            var medicationProfile = patientRepository.GetMedicationProfile(medProfileId, Current.AgencyId);
            if (medicationProfile != null)
            {
                medication = medicationProfile.Medication.ToObject<List<Medication>>().SingleOrDefault(m => m.Id == medicationId);
                medication.ProfileId = medicationProfile.Id;
            }
            return PartialView("MedicationProfile/Medication/Discharge", medication);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeletePatientMedication(Guid medProfileId, Guid medicationId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Medication could not be deleted from the Medication Profile." };
            if (patientService.DeleteMedication(medProfileId, medicationId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Medication was deleted successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationProfile(Guid patientId)
        {
            var viewData = new MedicationProfileViewData();
            var medicationProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medicationProfile != null)
            {
                viewData.MedicationProfile = medicationProfile;
                viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
                viewData.Allergies = patientService.GetAllergies(patientId);

                if (viewData.Patient != null)
                {
                    if (viewData.Patient.PhysicianContacts != null && viewData.Patient.PhysicianContacts.Count > 0)
                    {
                        var physician = viewData.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physician != null)
                        {
                            viewData.PhysicianId = physician.Id;
                        }
                    }
                    if (medicationProfile.PharmacyName.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyName = medicationProfile.PharmacyName;
                    }
                    else
                    {
                        viewData.PharmacyName = viewData.Patient.PharmacyName;
                    }
                    if (medicationProfile.PharmacyPhone.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyPhone = medicationProfile.PharmacyPhone;
                    }
                    else
                    {
                        viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                    }

                    var currentEpisode = patientRepository.GetCurrentEpisode(Current.AgencyId, patientId);
                    if (currentEpisode != null)
                    {
                        viewData.EpisodeId = currentEpisode.Id;
                        viewData.StartDate = currentEpisode.StartDate;
                        viewData.EndDate = currentEpisode.EndDate;

                        if (!currentEpisode.Id.IsEmpty() && !patientId.IsEmpty())
                        {
                            var assessment = assessmentService.GetEpisodeAssessment(currentEpisode.Id, patientId);

                            if (assessment != null)
                            {
                                var diagnosis = assessmentService.Diagnosis(assessment);
                                if (diagnosis != null)
                                {
                                    diagnosis.Merge<string, Question>(assessmentService.Allergies(assessment));
                                    viewData.Questions = diagnosis;
                                }
                                else
                                {
                                    viewData.Questions = assessmentService.Allergies(assessment);
                                }
                            }
                        }
                    }
                }
            }
            return PartialView("MedicationProfile/Profile", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicationProfilePrint(Guid id)
        {
            return View("MedicationProfile/Print", patientService.GetMedicationProfilePrint(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MedicationProfilePdf(Guid id)
        {
            MedProfilePdf doc = new MedProfilePdf(patientService.GetMedicationProfilePrint(id));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=MedProfile_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MedicationSnapshotPdf(Guid id)
        {
            MedProfilePdf doc = new MedProfilePdf(patientService.GetMedicationSnapshotPrint(id));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=MedProfile_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MedicationProfileSnapshotPrint(Guid id)
        {
            return View("MedicationProfile/Print", patientService.GetMedicationSnapshotPrint(id));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NewAuthorization()
        {
            return PartialView("Authorization/New", Guid.Empty);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewAuthorization(Guid patientId)
        {
            return PartialView("Authorization/New", patientId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddAuthorization([Bind]Authorization authorization)
        {
            Check.Argument.IsNotNull(authorization, "authorization");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "New authorization could not be saved." };
            if (authorization.IsValid)
            {
                authorization.UserId = Current.UserId;
                authorization.AgencyId = Current.AgencyId;
                authorization.Id = Guid.NewGuid();
                if (patientRepository.AddAuthorization(authorization))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, authorization.PatientId, authorization.Id.ToString(), LogType.Authorization, LogAction.AuthorizationAdded, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Authorization was saved successfully.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = authorization.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditAuthorization(Guid patientId, Guid Id)
        {

            return PartialView("Authorization/Edit", patientRepository.GetAuthorization(Current.AgencyId, patientId, Id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateAuthorization([Bind]Authorization authorization)
        {
            Check.Argument.IsNotNull(authorization, "authorization");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Edit authorization could not be saved." };

            if (authorization.IsValid)
            {
                authorization.AgencyId = Current.AgencyId;
                if (patientRepository.EditAuthorization(authorization))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, authorization.PatientId, authorization.Id.ToString(), LogType.Authorization, LogAction.AuthorizationEdited, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Authorization was saved successfully.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = authorization.ValidationMessage;
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AuthorizationList(Guid patientId)
        {
            IList<Authorization> authorizations = patientRepository.GetAuthorizations(Current.AgencyId, patientId);
            return View(new GridModel(authorizations));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AuthorizationGrid(Guid patientId)
        {
            return PartialView("Authorization/List", patientRepository.Get(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteAuthorization(Guid Id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Delete authorization is not Successful." };
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                if (patientRepository.DeleteAuthorization(Current.AgencyId, patientId, Id))
                {
                    Auditor.AddGeneralLog(LogDomain.Patient, patientId, Id.ToString(), LogType.Authorization, LogAction.AuthorizationDeleted, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Authorization was deleted successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Status(Guid patientId)
        {
            return PartialView(patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateStatus([Bind] PendingPatient patient)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The patient status could not be changed. Please try again." };
            if (!patient.Id.IsEmpty())
            {
                patient.AgencyId = Current.AgencyId;
                if (patient.Status == (int)PatientStatus.Active)
                {
                    if (patientService.ActivatePatient(patient.Id))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient was successfully activated.";
                    }
                }
                if (patient.Status == (int)PatientStatus.Discharged)
                {
                    if (patientService.DischargePatient(patient.Id, patient.DateOfDischarge, patient.Comments))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient was successfully discharged.";
                    }
                }
                if (patient.Status == (int)PatientStatus.Pending)
                {
                    if (patientService.SetPatientPending(patient.Id))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient status has been set to pending successfully.";
                    }
                }
                if (patient.Status == (int)PatientStatus.NonAdmission)
                {
                    if (patientService.NonAdmitPatient(patient))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The patient status has been set to non-admit successfully.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Readmit(Guid patientId)
        {
            return PartialView(patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PatientReadmit(Guid PatientId, DateTime ReadmissionDate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Patient Re-admission is unsuccessful." };
            if (patientService.ActivatePatient(PatientId, ReadmissionDate))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Patient Re-admission is successful.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult BranchList(int status, Guid branchId)
        {
            return Json(patientRepository.Find(status, branchId, Current.AgencyId).OrderBy(s => s.DisplayName.ToUpperCase()).Select(p => new { Id = p.Id, Name = p.DisplayName }).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersHistory(Guid patientId)
        {
            return PartialView("OrdersHistory", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OrdersHistoryList(Guid patientId, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(patientService.GetPatientOrders(patientId, StartDate, EndDate)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeOrdersView(Guid episodeId, Guid patientId)
        {
            return PartialView("EpisodeOrders", patientRepository.GetPatientEpisodFluent(Current.AgencyId, episodeId, patientId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeOrders(Guid episodeId, Guid patientId)
        {
            return View(new GridModel(patientService.GetEpisodeOrders(episodeId, patientId))); 
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedTaskHistory(Guid patientId)
        {
            return PartialView("DeletedTaskHistory", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletedTaskHistoryList(Guid patientId)
        {
            return View(new GridModel(patientService.GetDeletedTasks(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SixtyDaySummary(Guid patientId)
        {
            return PartialView("SixtyDaySummary", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SixtyDaySummaryList(Guid patientId)
        {
            return View(new GridModel(patientService.GetSixtyDaySummary((patientId))));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VitalSigns(Guid patientId)
        {
            return PartialView("VitalSigns", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityReports(Guid patientId)
        {
            return View(new GridModel(patientService.GetMedicareEligibilityLists(patientId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibilityList(Guid patientId)
        {
            return PartialView("EligibilityList", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicareEligibility(Guid medicareEligibilityId, Guid patientId)
        {
            var result = new PatientEligibility();
            var medicareEligibility = patientRepository.GetMedicareEligibility(Current.AgencyId, patientId, medicareEligibilityId);
            if (medicareEligibility != null && medicareEligibility.Result.IsNotNullOrEmpty())
            {
                var javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Deserialize<PatientEligibility>(medicareEligibility.Result);
                string npi = result != null && result.Episode != null && result.Episode.reference_id.IsNotNullOrEmpty() ? result.Episode.reference_id : null;
                if (npi != null)
                {
                    var OtherAgencyData = Container.Resolve<ILookUpDataProvider>().LookUpRepository.GetNpiData(npi);
                    if (OtherAgencyData != null)
                    {
                        result.Other_Agency_Data.name = OtherAgencyData.ProviderOrganizationName.IsNotNullOrEmpty() ? OtherAgencyData.ProviderOrganizationName : string.Empty;
                        result.Other_Agency_Data.address1 = OtherAgencyData.ProviderFirstLineBusinessPracticeLocationAddress.IsNotNullOrEmpty() ? OtherAgencyData.ProviderFirstLineBusinessPracticeLocationAddress : string.Empty;
                        result.Other_Agency_Data.address2 = OtherAgencyData.ProviderSecondLineBusinessPracticeLocationAddress.IsNotNullOrEmpty() ? OtherAgencyData.ProviderSecondLineBusinessPracticeLocationAddress : string.Empty;
                        result.Other_Agency_Data.city = OtherAgencyData.ProviderBusinessPracticeLocationAddressCityName.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessPracticeLocationAddressCityName : string.Empty;
                        result.Other_Agency_Data.state = OtherAgencyData.ProviderBusinessMailingAddressStateName.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessMailingAddressStateName : string.Empty;
                        result.Other_Agency_Data.zip = OtherAgencyData.ProviderBusinessMailingAddressPostalCode.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessMailingAddressPostalCode : string.Empty;
                        result.Other_Agency_Data.phone = OtherAgencyData.ProviderBusinessPracticeLocationAddressTelephoneNumber.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessPracticeLocationAddressTelephoneNumber : string.Empty;
                        result.Other_Agency_Data.fax = OtherAgencyData.ProviderBusinessPracticeLocationAddressFaxNumber.IsNotNullOrEmpty() ? OtherAgencyData.ProviderBusinessPracticeLocationAddressFaxNumber : string.Empty;
                    }
                }
            }
            return PartialView("Eligibility", result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MedicareEligibilityPdf([Bind]PatientEligibility Eligibility)
        {
            var doc = new MedicareEligibilityPdf(Eligibility, agencyRepository.GetWithBranches(Current.AgencyId), null);
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MedicareEligibility_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult MedicareEligibilityReportPdf(Guid patientId, Guid mcareEligibilityId)
        {
            PatientEligibility pEligibility = null;
            var eligibility = patientRepository.GetMedicareEligibility(Current.AgencyId, patientId, mcareEligibilityId);
            if (eligibility != null && eligibility.Result.IsNotNullOrEmpty()) pEligibility = eligibility.Result.FromJson<PatientEligibility>();
            var doc = new MedicareEligibilityPdf(pEligibility, agencyRepository.GetWithBranches(Current.AgencyId), patientRepository.GetPatientOnly(patientId, Current.AgencyId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=MedicareEligibility_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientLogs(Guid patientId)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Patient, LogType.Patient, patientId, patientId.ToString()));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MedicationLogs(Guid patientId)
        {
            return PartialView("ActivityLogs", patientService.GetMedicationLogs(LogDomain.Patient, LogType.Patient, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllergyProfile(Guid PatientId)
        {
            var viewData = new AllergyProfileViewData();
            var allergyProfile = patientRepository.GetAllergyProfileByPatient(PatientId, Current.AgencyId);
            if (allergyProfile != null)
            {
                viewData.AllergyProfile = allergyProfile;
                if (viewData.Patient != null)
                {
                    if (viewData.Patient.PhysicianContacts != null && viewData.Patient.PhysicianContacts.Count > 0)
                    {
                        var physician = viewData.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physician != null)
                        {
                            viewData.PhysicianId = physician.Id;
                        }
                    }
                }
            }
            viewData.Patient = patientRepository.Get(PatientId, Current.AgencyId);

            return PartialView("Allergies/Profile", viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AllergyProfilePrint(Guid id)
        {
            return View("Allergies/Print", patientService.GetAllergyProfilePrint(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult AllergyProfilePdf(Guid id)
        {
            AllergyProfilePdf doc = new AllergyProfilePdf(patientService.GetAllergyProfilePrint(id));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=AllergyProfile_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Allergies(Guid allergyProfileId)
        {
            var allergyProfile = patientRepository.GetAllergyProfile(allergyProfileId, Current.AgencyId);
            return PartialView("Allergies/List", allergyProfile);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllergyList(Guid patientId)
        {
            return PartialView("AllergyList", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewAllergy(Guid allergyProfileId)
        {
            return PartialView("Allergies/New", allergyProfileId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult InsertAllergy([Bind] Allergy allergy)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The new allergy could not be added to the allergy profile." };
            if (allergy != null)
            {
                if (patientService.AddAllergy(allergy))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The new allergy was added to the allergy profile successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditAllergy(Guid allergyProfileId, Guid allergyId)
        {
            var allergy = new Allergy();
            var allergyProfile = patientRepository.GetAllergyProfile(allergyProfileId, Current.AgencyId);
            if (allergyProfile != null)
            {
                allergy = allergyProfile.Allergies.ToObject<List<Allergy>>().SingleOrDefault(a => a.Id == allergyId);
                allergy.ProfileId = allergyProfile.Id;
            }
            return PartialView("Allergies/Edit", allergy);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NextStep()
        {
            return PartialView("NextStep");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateAllergy([Bind] Allergy allergy)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Allergy could not be updated. Please try again." };
            if (allergy != null)
            {
                if (patientService.UpdateAllergy(allergy))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Allergy was updated successfully.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdateAllergyStatus(Guid allergyProfileId, Guid allergyId, bool isDeprecated)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The Allergy status could not be updated." };
            if (patientService.UpdateAllergy(allergyProfileId, allergyId, isDeprecated))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Allergy was updated successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagedDates(Guid patientId)
        {
            return PartialView("ManagedDates", patientRepository.GetPatientOnly(patientId, Current.AgencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientManagedDates(Guid patientId)
        {
            return View(new GridModel(patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId)));
        }

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult UpdatePatientManagedDates(Guid PatientId, Guid Id, DateTime Date)
        //{
        //    var managedDate = patientRepository.GetPatientAdmissionDate(Current.AgencyId, PatientId, Id);
        //    if (managedDate != null)
        //    {
        //        managedDate.StartOfCareDate = Date;
        //        if (patientRepository.UpdatePatientAdmissionDate(managedDate))
        //        {
        //            Auditor.AddGeneralLog(LogDomain.Patient, PatientId, managedDate.Id.ToString(), LogType.ManagedDate, managedDate.Type == 2 ? LogAction.PatientDiscargedDateUpdated : LogAction.PatientStartOfCareUpdated, string.Empty);
        //            return View(new GridModel(patientRepository.GetPatientAdmissionDates(Current.AgencyId, PatientId)));
        //        }
        //    }
        //    var list = new List<PatientAdmissionDate>();
        //    return View(new GridModel(list));
        //}

        //[GridAction]
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult DeletePatientManagedDates(Guid PatientId, Guid Id)
        //{
        //     var managedDate = patientRepository.GetPatientAdmissionDate(Current.AgencyId, PatientId, Id);
        //     if (managedDate != null)
        //     {
        //         if (patientRepository.DeletePatientAdmissionDate(Current.AgencyId, PatientId, Id))
        //         {
        //             Auditor.AddGeneralLog(LogDomain.Patient, PatientId, Id.ToString(), LogType.ManagedDate, managedDate.Type == 2 ? LogAction.PatientDiscargedDateDeleted : LogAction.PatientDiscargedDateDeleted, string.Empty);
        //             return View(new GridModel(patientRepository.GetPatientAdmissionDates(Current.AgencyId, PatientId)));
        //         }
        //         else
        //         {
        //             var list = new List<PatientAdmissionDate>();
        //             return View(new GridModel(list));
        //         }
        //     }
        //     else
        //     {
        //         var list = new List<PatientAdmissionDate>();
        //         return View(new GridModel(list));
        //     }
        //}

        #endregion
    }
}
