﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;
    using System.Text;


    using Services;
    using ViewData;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;


    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Telerik.Web.Mvc;
    using Axxess.LookUp.Domain;
    using Axxess.OasisC.Enums;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class OasisController : BaseController
    {
        #region Constructor / Member

        private readonly IUserService userService;
        private readonly IDateService dateService;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientService patientService;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly ICachedDataRepository cachedDataRepository;
        private readonly IAssessmentRepository oasisAssessmentRepository;

        public OasisController(IOasisCDataProvider oasisDataProvider, IAgencyManagementDataProvider agencyManagementDataProvider, IAssessmentService assessmentService, IPatientService patientService, IUserService userService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(oasisDataProvider, "oasisDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.patientService = patientService;
            this.assessmentService = assessmentService;
            this.dateService = Container.Resolve<IDateService>();
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.cachedDataRepository = oasisDataProvider.CachedDataRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.oasisAssessmentRepository = oasisDataProvider.OasisAssessmentRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region OasisController Actions

        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Assessment(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new OasisViewData();
            var oasisAssessment = assessmentService.SaveAssessment(formCollection, Request.Files);
            if (oasisAssessment != null && oasisAssessment.ValidationError.IsNullOrEmpty())
            {
                viewData.Assessment = oasisAssessment;
                viewData.assessmentId = oasisAssessment.Id;
                viewData.isSuccessful = true;
            }
            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Get(Guid Id, string assessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            return Json(assessmentService.GetAssessment(Id, assessmentType).ToDictionary());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StartOfCare(Guid Id, Guid PatientId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("Assessments/StartofCare", assessmentService.GetAssessmentWithScheduleType(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult StartOfCareBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.StartOfCare));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult StartOfCarePTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.StartOfCare), "PT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult StartOfCareOTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.StartOfCare), "OT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StartOfCarePrintPreview(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Assessments/StartOfCarePrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.StartOfCare));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult StartOfCarePdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.StartOfCare));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Category(Guid Id, Guid PatientId, string AssessmentType, string Category) {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            var assessment = assessmentService.GetAssessmentWithScheduleType(Id, AssessmentType);
            if (assessment != null) {
                var medicationProfile = patientRepository.GetMedicationProfileByPatient(assessment.PatientId, Current.AgencyId);
                if (medicationProfile != null) assessment.MedicationProfile = medicationProfile.ToXml();
            }
            return PartialView("Assessments/Tabs/" + Category.Split('_').Last(), assessment);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [OutputCache(Duration = 86400, VaryByParam = "mooCode")]
        public JsonResult Guide(string mooCode)
        {
            if (mooCode.IsNullOrEmpty())
            {
                return Json(new OasisGuide());
            }
            else
            {
                var result = cachedDataRepository.GetOasisGuide(mooCode);
                return Json(result != null ? result : new OasisGuide());
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PlanOfCareMedication(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var medProfile = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            if (medProfile != null)
            {
                return View("485/Medication", medProfile);
            }
            return View("485/Medication", new MedicationProfile());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PlanOfCarePrintPreview(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("485/Print", assessmentService.GetPlanOfCarePrint(episodeId, patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult PlanOfCarePdf(Guid episodeId, Guid patientId, Guid eventId)
        {
            var doc = new PlanOfCarePdf(assessmentService.GetPlanOfCarePrint(episodeId, patientId, eventId));
            var stream = doc.GetPlanOfCareStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=HCFA-485_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New485(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");

            var viewData = patientService.GetPatientAndAgencyInfo(episodeId, patientId, eventId);
            if (viewData == null)
            {
                viewData = new PlanofCareViewData();
            }

            return View("485/New", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PlanofCareContent(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");

            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
            if (planofCare != null)
            {
                var assessment = oasisAssessmentRepository.Get(planofCare.AssessmentId, planofCare.AssessmentType, Current.AgencyId);
                if (assessment != null)
                {
                    planofCare.Questions = assessmentService.Get485FromAssessment(assessment);
                }
            }
            return PartialView("485/LocatorQuestions", planofCare);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit485(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");

            var planofCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
            if (planofCare != null)
            {
                var agency = agencyRepository.GetWithBranches(planofCare.AgencyId);
                var patient = patientRepository.GetPatientOnly(planofCare.PatientId, planofCare.AgencyId);
                planofCare.AgencyData = agency != null ? agency.ToXml() : string.Empty;
                planofCare.PatientData = patient != null ? patient.ToXml() : string.Empty;

                if (planofCare.Data.IsNotNullOrEmpty())
                {
                    planofCare.Questions = planofCare.Data.ToObject<List<Question>>();
                }

                var episodeRange = assessmentService.GetPlanofCareCertPeriod(planofCare.EpisodeId, planofCare.PatientId, planofCare.AssessmentId);
                if (episodeRange != null)
                {
                    planofCare.EpisodeEnd = episodeRange.EndDateFormatted;
                    planofCare.EpisodeStart = episodeRange.StartDateFormatted;
                }

                if (planofCare.PhysicianId.IsEmpty() && planofCare.PhysicianData.IsNotNullOrEmpty())
                {
                    var oldPhysician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                    if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                    {
                        planofCare.PhysicianId = oldPhysician.Id;
                    }
                }

                return View("485/Edit", planofCare);
            }
            return View("485/Edit", new PlanofCare());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Save485(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The 485 Plan of Care could not be saved." };
            var status = formCollection.Get("Status");
            if (status.IsNotNullOrEmpty() && status.IsInteger())
            {
                int statusId = status.ToInteger();

                if (statusId == (int)ScheduleStatus.OrderSaved)
                {
                    if (assessmentService.UpdatePlanofCare(formCollection))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The 485 Plan of Care has been saved.";
                    }
                }
                else if (statusId == (int)ScheduleStatus.OrderSubmittedPendingReview)
                {
                    var signatureText = formCollection.Get("SignatureText");
                    var signatureDate = formCollection.Get("SignatureDate");
                    if (signatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, signatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature to complete this Plan of Care.";
                    }
                    else if (signatureDate.IsNullOrEmpty())
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature date was not provided or is not in the correct format.";
                    }
                    else
                    {
                        if (assessmentService.UpdatePlanofCare(formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The 485 Plan of Care has been completed.";
                        }
                    }
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SavePlanofCareStandAlone(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The 485 Plan of Care could not be saved." };
            var status = formCollection.Get("Status");
            if (status.IsNotNullOrEmpty() && status.IsInteger())
            {
                int statusId = status.ToInteger();

                if (statusId == (int)ScheduleStatus.OrderSaved)
                {
                    if (assessmentService.UpdatePlanofCareStandAlone(formCollection))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The 485 Plan of Care has been saved.";
                    }
                }
                else if (statusId == (int)ScheduleStatus.OrderSubmittedPendingReview)
                {
                    var signatureText = formCollection.Get("SignatureText");
                    var signatureDate = formCollection.Get("SignatureDate");
                    if (signatureText.IsNullOrEmpty() || !userService.IsSignatureCorrect(Current.UserId, signatureText))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Please provide the correct signature to complete this Plan of Care.";
                    }
                    else if (signatureDate.IsNullOrEmpty())
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature date was not provided or is not in the correct format.";
                    }
                    else
                    {
                        if (assessmentService.UpdatePlanofCareStandAlone(formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The 485 Plan of Care has been completed.";
                        }
                    }
                }
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Validate(Guid Id, Guid patientId, Guid episodeId, string assessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            return View("~/Views/Oasis/Validation.aspx", assessmentService.Validate(Id, patientId, episodeId, assessmentType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult AuditPdf(Guid id, Guid patientId, Guid episodeId, string assessmentType)
        {
            Check.Argument.IsNotEmpty(id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");

            var doc = new OasisAuditPdf(assessmentService.Audit(id, patientId, episodeId, assessmentType));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=OASISLogicalCheck_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Recertification(Guid Id, Guid PatientId, string AssessmentType) {
            return PartialView("Assessments/Recertification", assessmentService.GetAssessmentWithScheduleType(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult RecertificationBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.Recertification));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult RecertificationPTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.Recertification), "PT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult RecertificationOTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.Recertification), "OT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RecertificationPrintPreview(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Assessments/RecertificationPrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.Recertification));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult RecertificationPdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.Recertification));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ResumptionOfCare(Guid Id, Guid PatientId, string AssessmentType) {
            return PartialView("Assessments/ResumptionofCare", assessmentService.GetAssessmentWithScheduleType(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult ResumptionOfCareBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.ResumptionOfCare));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult ResumptionOfCarePTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.ResumptionOfCare), "PT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult ResumptionOfCareOTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.ResumptionOfCare), "OT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ResumptionOfCarePrintPreview(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Assessments/ResumptionOfCarePrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.ResumptionOfCare));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult ResumptionOfCarePdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.ResumptionOfCare));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FollowUp(Guid Id, Guid PatientId, string AssessmentType) {
            return PartialView("Assessments/FollowUp", assessmentService.GetAssessmentWithScheduleType(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult FollowUpBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.FollowUp));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult FollowUpPTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.FollowUp), "PT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult FollowUpOTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.FollowUp), "OT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FollowUpPrintPreview(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Assessments/FollowUpPrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.FollowUp));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult FollowUpPdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.FollowUp));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DischargeFromAgencyDeath(Guid Id, Guid PatientId, string AssessmentType) {
            return PartialView("Assessments/DischargeFromAgencyDeath", assessmentService.GetAssessmentWithScheduleType(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult DischargeFromAgencyDeathBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.DischargeFromAgencyDeath));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult DischargeFromAgencyDeathPTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.DischargeFromAgencyDeath), "PT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult DischargeFromAgencyDeathOTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.DischargeFromAgencyDeath), "OT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargeFromAgencyDeathPrintPreview(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Assessments/DischargeFromAgencyDeathPrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.DischargeFromAgencyDeath));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult DischargeFromAgencyDeathPdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.DischargeFromAgencyDeath));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DischargeFromAgency(Guid Id, Guid PatientId, string AssessmentType) {
            return PartialView("Assessments/DischargeFromAgency", assessmentService.GetAssessmentWithScheduleType(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult DischargeFromAgencyBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.DischargeFromAgency));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult DischargeFromAgencyPTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.DischargeFromAgency), "PT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult DischargeFromAgencyOTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.DischargeFromAgency), "OT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargeFromAgencyPrintPreview(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Assessments/DischargeFromAgencyPrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.DischargeFromAgency));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult DischargeFromAgencyPdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.DischargeFromAgency));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TransferInPatientDischarged(Guid Id, Guid PatientId, string AssessmentType) {
            return PartialView("Assessments/TransferInPatientDischarged", assessmentService.GetAssessmentWithScheduleType(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult TransferInPatientDischargedBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.TransferInPatientDischarged));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult TransferInPatientDischargedPTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.TransferInPatientDischarged), "PT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult TransferInPatientDischargedOTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.TransferInPatientDischarged), "OT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TransferInPatientDischargedPrintPreview(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Assessments/TransferInPatientDischargedPrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.TransferInPatientDischarged));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult TransferInPatientDischargedPdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.TransferInPatientDischarged));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TransferInPatientNotDischarged(Guid Id, Guid PatientId, string AssessmentType) {
            return PartialView("Assessments/TransferInPatientNotDischarged", assessmentService.GetAssessmentWithScheduleType(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult TransferInPatientNotDischargedBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.TransferInPatientNotDischarged));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult TransferInPatientNotDischargedPTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.TransferInPatientNotDischarged), "PT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult TransferInPatientNotDischargedOTBlank()
        {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.TransferInPatientNotDischarged), "OT");
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TransferInPatientNotDischargedPrintPreview(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View("Assessments/TransferInPatientNotDischargedPrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.TransferInPatientNotDischarged));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult TransferInPatientNotDischargedPdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.TransferInPatientNotDischarged));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=Oasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Submit(Guid Id, Guid patientId, Guid episodeId, string assessmentType, string actionType, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your assessment was not submitted." };
            if (actionType == "Submit")
            {
                if (assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisCompletedPendingReview).ToString(), string.Empty))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Assessment was submitted successfully.";
                }
            }
            else if (actionType == "Approve")
            {
                if (assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), string.Empty))
                {
                    if ((assessmentType == DisciplineTasks.OASISCDischargeOT.ToString() || assessmentType == DisciplineTasks.OASISCDischarge.ToString() || assessmentType == DisciplineTasks.OASISCDischargePT.ToString() || assessmentType == "DischargeFromAgency") || (assessmentType == DisciplineTasks.OASISCTransferDischarge.ToString() || assessmentType == "OASISCTransferDischarge") || (assessmentType == DisciplineTasks.OASISCDeath.ToString() || assessmentType == DisciplineTasks.OASISCDeathOT.ToString() || assessmentType == DisciplineTasks.OASISCDeathPT.ToString() || assessmentType == "OASISCDeath"))
                    {
                        var assessment = oasisAssessmentRepository.Get(Id, assessmentType, Current.AgencyId);
                        if (assessment != null)
                        {
                            var assessmentData = assessment.ToDictionary();
                            var schedule = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, Id);
                            if (schedule != null)
                            {
                                var date = DateTime.MinValue;
                                var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                                if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate())
                                {
                                    date = dateAssessment.ToDateTime();
                                }
                                var eventDateSchedule = schedule.EventDate;
                                if (eventDateSchedule.IsNotNullOrEmpty() && eventDateSchedule.IsValidDate())
                                {
                                    date = date.Date > eventDateSchedule.ToDateTime().Date ? date : eventDateSchedule.ToDateTime();
                                }
                                var visitDateSchedule = schedule.VisitDate;
                                if (visitDateSchedule.IsNotNullOrEmpty() && visitDateSchedule.IsValidDate())
                                {
                                    date = date.Date > visitDateSchedule.ToDateTime().Date ? date : visitDateSchedule.ToDateTime();
                                }
                                if (date.Date > DateTime.MinValue.Date)
                                {
                                    patientService.DischargePatient(patientId, date, "Patient discharged due to discharge OASIS.");

                                }
                            }
                        }
                        else
                        {
                            var schedule = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, Id);
                            if (schedule != null)
                            {
                                var date = DateTime.MinValue;
                                var eventDateSchedule = schedule.EventDate;
                                if (eventDateSchedule.IsNotNullOrEmpty() && eventDateSchedule.IsValidDate())
                                {
                                    date = date.Date > eventDateSchedule.ToDateTime().Date ? date : eventDateSchedule.ToDateTime();
                                }
                                var visitDateSchedule = schedule.VisitDate;
                                if (visitDateSchedule.IsNotNullOrEmpty() && visitDateSchedule.IsValidDate())
                                {
                                    date = date.Date > visitDateSchedule.ToDateTime().Date ? date : visitDateSchedule.ToDateTime();
                                }
                                if (date.Date > DateTime.MinValue.Date)
                                {
                                    patientService.DischargePatient(patientId, date, "Patient discharged due to Discharge OASIS.");
                                }
                            }
                        }
                    }
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Assessment has been approved.";
                }
            }
            else if (actionType == "Return")
            {
                if (assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisReturnedForClinicianReview).ToString(), reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Assessment has been returned.";
                }
            }
            else if (actionType == "Exported")
            {
                if (assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisExported).ToString(), string.Empty))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Assessment has been exported.";
                }
            }
            else if (actionType == "ReOpen")
            {
                if (assessmentService.UpdateAssessmentStatus(Id, patientId, episodeId, assessmentType, ((int)ScheduleStatus.OasisReopened).ToString(), reason))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "The Assessment has been reopened.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SubmitOnly(FormCollection formCollection)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Assessment could not be submitted." };
            var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                string type = keys.Contains("OasisValidationType") && formCollection["OasisValidationType"].IsNotNullOrEmpty() ? formCollection["OasisValidationType"] : string.Empty;
                if (type.IsNotNullOrEmpty())
                {
                    var Id = keys.Contains(string.Format("{0}_Id", type)) && formCollection[string.Format("{0}_Id", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_Id", type)].IsGuid() ? formCollection.Get(string.Format("{0}_Id", type)).ToGuid() : Guid.Empty;
                    var episodeId = keys.Contains(string.Format("{0}_EpisodeId", type)) && formCollection[string.Format("{0}_EpisodeId", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_EpisodeId", type)].IsGuid() ? formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid() : Guid.Empty;
                    var patientId = keys.Contains(string.Format("{0}_PatientId", type)) && formCollection[string.Format("{0}_PatientId", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_PatientId", type)].IsGuid() ? formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid() : Guid.Empty;

                    if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                        rules.Add(new Validation(() => !keys.Contains(string.Format("{0}_ValidationClinician", type)) || (keys.Contains(string.Format("{0}_ValidationClinician", type)) && string.IsNullOrEmpty(formCollection[string.Format("{0}_ValidationClinician", type)])), "User Signature can't be empty.\n"));
                        rules.Add(new Validation(() => keys.Contains(string.Format("{0}_ValidationClinician", type)) && formCollection[string.Format("{0}_ValidationClinician", type)].IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, formCollection[string.Format("{0}_ValidationClinician", type)]) : false, "User Signature is not correct.\n"));

                        rules.Add(new Validation(() => !keys.Contains(string.Format("{0}_ValidationSignatureDate", type)) || (keys.Contains(string.Format("{0}_ValidationSignatureDate", type)) && string.IsNullOrEmpty(formCollection[string.Format("{0}_ValidationSignatureDate", type)])), "Signature date can't be empty.\n"));
                        rules.Add(new Validation(() => keys.Contains(string.Format("{0}_ValidationSignatureDate", type)) && formCollection[string.Format("{0}_ValidationSignatureDate", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_ValidationSignatureDate", type)].IsValidDate() ? false : true, "Signature date is not valid.\n"));
                        rules.Add(new Validation(() => keys.Contains(string.Format("{0}_ValidationSignatureDate", type)) && formCollection[string.Format("{0}_ValidationSignatureDate", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_ValidationSignatureDate", type)].IsValidDate() ? !(formCollection[string.Format("{0}_ValidationSignatureDate", type)].ToDateTime() >= episode.StartDate && formCollection[string.Format("{0}_ValidationSignatureDate", type)].ToDateTime() <= DateTime.Now) : true, "Signature date is not the in valid range.\n"));
                        if (type == AssessmentType.StartOfCare.ToString() || type == AssessmentType.ResumptionOfCare.ToString() || type == AssessmentType.Recertification.ToString() || type == AssessmentType.FollowUp.ToString())
                        {
                            rules.Add(new Validation(() => !keys.Contains(type + "_TimeIn") || (keys.Contains(type + "_TimeIn") && string.IsNullOrEmpty(formCollection[type + "_TimeIn"])), "Time-In can't be empty. \n"));
                            rules.Add(new Validation(() => !keys.Contains(type + "_TimeOut") || (keys.Contains(type + "_TimeOut") && string.IsNullOrEmpty(formCollection[type + "_TimeOut"])), "Time-Out can't be empty. \n"));
                        }

                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            if (assessmentService.UpdateAssessmentStatusForSubmit(Id, patientId, episodeId, type, ((int)ScheduleStatus.OasisCompletedPendingReview).ToString(), string.Format("Electronically Signed by: {0}", Current.UserFullName), formCollection[string.Format("{0}_ValidationSignatureDate", type)].ToDateTime(), formCollection[type + "_TimeIn"], formCollection[type + "_TimeOut"]))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "Your Assessment submitted successfully.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message;
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MarkExported(List<string> OasisSelected)
        {
            JsonViewData viewData = new JsonViewData { isSuccessful = false, errorMessage = "The selected OASIS Assessments could not be marked as exported." };
            if (assessmentService.MarkAsExported(OasisSelected))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The selected OASIS Assessments have been successfully marked as exported.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExportView()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("OasisExport");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Export(Guid BranchId, List<int> paymentSources)
        {
            return View(new GridModel(assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisCompletedExportReady, paymentSources)));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ExportedView()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            
            return PartialView("Exported");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Exported(Guid BranchId, int Status, DateTime StartDate, DateTime EndDate)
        {
            return View(new GridModel(assessmentService.GetAssessmentByStatus(BranchId, ScheduleStatus.OasisExported, Status, StartDate, EndDate)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult Generate(List<string> OasisSelected)
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                agencyRepository.GetMainLocation(Current.AgencyId);
            }
            string generateOasisHeader = assessmentService.OasisHeader(agencyRepository.Get(Current.AgencyId));
            var generateJsonOasis = string.Empty;
            int count = 0;
            var hl = generateOasisHeader.Length;
            if (OasisSelected != null && OasisSelected.Count > 0)
            {
                OasisSelected.ForEach(o =>
                {
                    string[] data = o.Split('|');
                    var assessment = oasisAssessmentRepository.Get(data[0].ToGuid(), data[1], Current.AgencyId);
                    if (assessment != null && assessment.SubmissionFormat != null)
                    {
                        generateJsonOasis += assessment.SubmissionFormat + "\r\n";
                        count++;
                    }
                });
            }
            var bl = generateJsonOasis.Length;
            string generateOasisFooter = assessmentService.OasisFooter(count + 2);
            var fl = generateOasisFooter.Length;
            UTF8Encoding encoding = new UTF8Encoding();
            string allString = generateOasisHeader + generateJsonOasis + generateOasisFooter;
            byte[] buffer = encoding.GetBytes(allString);
            Stream fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, allString.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis{0}.txt", DateTime.Now.ToString("MMddyyyy")));
            return new FileStreamResult(fileStream, "Text/Plain");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Inactivate(Guid Id, string type)
        {
            return Json(assessmentService.ValidateInactivate(Id, type));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult GenerateForCancel(Guid Id, string type)
        {
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                agencyRepository.GetMainLocation(Current.AgencyId);
            }
            string generateOasisHeadre = assessmentService.OasisHeader(agencyRepository.Get(Current.AgencyId));
            var generateJsonOasis = string.Empty;
            var hl = generateOasisHeadre.Length;

            var assessment = oasisAssessmentRepository.Get(Id, type, Current.AgencyId);
            if (assessment != null && assessment.CancellationFormat.IsNotNullOrEmpty())
            {
                generateJsonOasis = assessment.CancellationFormat + "\r\n";
            }

            var bl = generateJsonOasis.Length;
            string generateOasisFooter = assessmentService.OasisFooter(3);
            var fl = generateOasisFooter.Length;
            var encoding = new UTF8Encoding();
            string allString = generateOasisHeadre + generateJsonOasis + generateOasisFooter;
            byte[] buffer = encoding.GetBytes(allString);
            Stream fileStream = new MemoryStream();
            fileStream.Write(buffer, 0, allString.Length);
            fileStream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename=Oasis{0}.txt", DateTime.Now.ToString("MMddyyyy")));
            return new FileStreamResult(fileStream, "Text/Plain");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Asset Not Deleted." };
            if (assessmentService.DeleteWoundCareAsset(episodeId, patientId, eventId, assessmentType, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Asset Successfully Deleted.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Supply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType)
        {
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId, assessmentType)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, int supplyId, string quantity, string date)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNegativeOrZero(supplyId, "supplyId");
            Check.Argument.IsNotNull(quantity, "quantity");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The supply could not added. Please try again!" };
            if (assessmentService.AddSupply(episodeId, patientId, eventId, assessmentType, supplyId, quantity, date))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The Supply was added successfully.";
            }
            return Json(viewData);
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNull(supply, "supply");

            assessmentService.UpdateSupply(episodeId, patientId, eventId, assessmentType, supply);
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId, assessmentType)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, string assessmentType, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotNull(supply, "supply");

            assessmentService.DeleteSupply(episodeId, patientId, eventId, assessmentType, supply);
            return View(new GridModel(assessmentService.GetAssessmentSupply(episodeId, patientId, eventId, assessmentType)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BlankMasterCalendar(Guid episodeId, Guid patientId, string assessmentType)
        {
            var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
            var modelData = new EpisodeDateViewData();
            if (episode != null)
            {
                if (assessmentType.IsEqual("Recertification"))
                {
                    modelData.StartDate = episode.EndDate.AddDays(1);
                    modelData.EndDate = modelData.StartDate.AddDays(59);
                }
                else
                {
                    modelData.EndDate = episode.EndDate;
                    modelData.StartDate = episode.StartDate;
                }
            }
            return PartialView("BlankMasterCalendar", modelData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonOasisStartOfCare(Guid Id, Guid PatientId, string AssessmentType) {
            return PartialView("Assessments/NonOasisStartOfCare", assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult NonOasisStartOfCareBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.NonOasisStartOfCare));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=NonOasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NonOasisStartOfCarePrintPreview(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Assessments/NonOasisStartOfCarePrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.NonOasisStartOfCare));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult NonOasisStartOfCarePdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.NonOasisStartOfCare));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=NonOasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonOasisRecertification(Guid Id, Guid PatientId, string AssessmentType) {
            return PartialView("Assessments/NonOasisRecertification", assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult NonOasisRecertificationBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.NonOasisRecertification));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=NonOasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NonOasisRecertificationPrintPreview(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Assessments/NonOasisRecertificationPrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.NonOasisRecertification));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult NonOasisRecertificationPdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.NonOasisRecertification));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=NonOasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonOasisDischarge(Guid Id, Guid PatientId, string AssessmentType) {
            return PartialView("Assessments/NonOasisDischarge", assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public FileStreamResult NonOasisDischargeBlank() {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(AssessmentType.NonOasisDischarge));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=NonOasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NonOasisDischargePrintPreview(Guid episodeId, Guid patientId, Guid eventId) {
            return View("Assessments/NonOasisDischargePrint", assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.NonOasisDischarge));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult NonOasisDischargePdf(Guid episodeId, Guid patientId, Guid eventId) {
            var doc = new OasisPdf(assessmentService.GetAssessmentPrint(episodeId, patientId, eventId, AssessmentType.NonOasisDischarge));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=NonOasis_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NonOasisSignature(Guid Id, Guid PatientId, Guid EpisodeId, string AssessmentType)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotNull(AssessmentType, "AssessmentType");
            return PartialView("NonOasisSignature", assessmentService.GetAssessment(Id, AssessmentType));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LoadPrevious(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType, Guid previousAssessmentId, string previousAssessmentType)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(assessmentId, "eventId");
            Check.Argument.IsNotNull(assessmentType, "assessmentType");
            Check.Argument.IsNotEmpty(previousAssessmentId, "previousAssessmentId");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Previous Assessment data could not be saved." };
            if (oasisAssessmentRepository.UsePreviousAssessment(Current.AgencyId, episodeId, patientId, assessmentId, assessmentType, previousAssessmentId, previousAssessmentType))
            {
                var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, assessmentId);
                if (scheduleEvent != null)
                {
                    scheduleEvent.Status = ((int)ScheduleStatus.OasisSaved).ToString();
                    if (patientRepository.UpdateEpisode(Current.AgencyId, scheduleEvent))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Assessment was successfully loaded from the previous assessment.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Correction(Guid Id, Guid PatientId, Guid EpisodeId, string Type, int CorrectionNumber)
        {
            var export = new OasisExport();
            export.AssessmentId = Id;
            export.PatientId = PatientId;
            export.EpisodeId = EpisodeId;
            export.CorrectionNumber = CorrectionNumber;
            export.AssessmentType = Type;
            return PartialView(export);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetPlanofCareUrl(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new OasisPlanOfCareJson { isSuccessful = false, errorMessage = "No Plan of Care (485) found for this episode." };
            var scheduleEvent = patientRepository.GetSchedule(Current.AgencyId, episodeId, patientId, eventId);
            if (scheduleEvent != null)
            {
                var assessment = assessmentService.GetEpisodeAssessment(episodeId, patientId, scheduleEvent.EventDate.ToDateTime());
                if (assessment != null)
                {
                    var pocScheduleEvent = assessmentService.GetPlanofCareScheduleEvent(episodeId, patientId, assessment.Id, assessment.Type.ToString());
                    if (pocScheduleEvent != null)
                    {
                        viewData.isSuccessful = true;
                        viewData.url = "/Oasis/PlanOfCarePdf";
                        viewData.episodeId = pocScheduleEvent.EpisodeId;
                        viewData.patientId = pocScheduleEvent.PatientId;
                        viewData.eventId = pocScheduleEvent.EventId;
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CorrectionChange(Guid Id, Guid PatientId, Guid EpisodeId, string Type, int CorrectionNumber)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Correction Number Is Not Updated." };
            if (!Id.IsEmpty() && !PatientId.IsEmpty() && !EpisodeId.IsEmpty() && Type.IsNotNullOrEmpty())
            {
                if (assessmentService.UpdateAssessmentCorrectionNumber(Id, PatientId, EpisodeId, Type, CorrectionNumber))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your Correction Number Is Updated.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileStreamResult OasisProfilePdf(Guid Id, String type) {
            var doc = new OasisProfilePdf(oasisAssessmentRepository.Get(Id, type, Current.AgencyId), agencyRepository.GetWithBranches(Current.AgencyId));
            var stream = doc.GetStream();
            stream.Position = 0;
            HttpContext.Response.AddHeader("content-disposition", String.Format("attachment; filename=OASISProfile_{0}.pdf", DateTime.Now.Ticks.ToString()));
            return new FileStreamResult(stream, "application/pdf");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OasisProfilePrint(Guid Id, String type) {
            var note = oasisAssessmentRepository.Get(Id, type, Current.AgencyId);
            note.AgencyData = agencyRepository.Get(Current.AgencyId).ToXml();
            var xml = new OasisProfileXml(note);
            note.PrintViewJson = xml.GetJson();
            return View("Profile", note);
        }

        #endregion
    }
}
