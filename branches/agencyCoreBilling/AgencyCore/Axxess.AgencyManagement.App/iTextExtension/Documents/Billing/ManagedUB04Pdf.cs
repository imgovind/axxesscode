﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.App.Services;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class ManagedUB04Pdf : UB04Pdf {

        public ManagedUB04Pdf(UBOFourViewData data, IBillingService billingService) : base(data, billingService) { }
        protected override IElement[] BuildContent(UBOFourViewData data) {
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(new float[] { 24, 123, 73, 35, 39, 49, 49, 10 }, false) };
            Font font = AxxessPdf.sans;
            font.Size = 8;
            float[] padding = new float[] { 0, 1, .05F, 1 }, borders = new float[] { 0, 0, 0, 0 }, moneypad = new float[] { 0, 9, .05F, 1 };
            content[0].AddCell("0023", font, padding, borders);
            content[0].AddCell("HOME HEALTH SERVICES", font, padding, borders);
            content[0].AddCell(data.Claim != null && data.Claim.HippsCode.IsNotNullOrEmpty() ? data.Claim.HippsCode : string.Empty, font, padding, borders);
            content[0].AddCell(data.Claim != null && data.Claim.FirstBillableVisitDate.IsValid() ? data.Claim.FirstBillableVisitDate.ToString("MMddyyyy") : string.Empty, font, padding, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            content[0].AddCell("0.00", font, "Right", moneypad, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            double supply = 0;
            PPSStandard ppsStandard;
            if (data.Claim != null && data.Claim.SupplyTotal <= 0 && data.Claim.HippsCode.IsNotNullOrEmpty() && data.Claim.HippsCode.Length == 5) supply += billingService.GetSupplyReimbursement(data.Claim.HippsCode[4], data.Claim.EpisodeStartDate.Year, out ppsStandard);
            else if (data.Claim != null && data.Claim.SupplyTotal > 0) supply += data.Claim.SupplyTotal;
            content[0].AddCell(data.Claim.SupplyCode, font, padding, borders);
            content[0].AddCell("SERVICE SUPPLIES", font, padding, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            content[0].AddCell(data.Claim != null && data.Claim.EpisodeStartDate.IsValid() ? data.Claim.EpisodeStartDate.ToString("MMddyyyy") : string.Empty, font, padding, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            content[0].AddCell(String.Format("{0:0.00}", data.Claim != null ? data.Claim.SupplyTotal:0.00), font, "Right", moneypad, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            content[0].AddCell(string.Empty, font, padding, borders);
            var unitType = -1;
            var BillInfo = this.billingService.GetInsuranceUnit(data.Claim!=null? data.Claim.PrimaryInsuranceId: 0, out unitType);
            var visits = data.Claim != null && data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(v => v.EventDate.IsValidDate()).OrderBy(s => s.EventDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();
            foreach (var visit in visits) {
                visit.Discipline = visit.Discipline == "Nursing" ? "SN" : visit.Discipline;
                var code = visit.GIdentify();
                var unit = unitType == 1 ? 1 : (unitType == 2 ? (int)Math.Ceiling((double)visit.MinSpent / 60) : (unitType == 3 ? (int)Math.Ceiling((double)visit.MinSpent / 15) : 0)); 
                content[0].AddCell(code.IsNotNullOrEmpty() && BillInfo.ContainsKey(code) && BillInfo[code] != null ? BillInfo[code].CodeOne : string.Empty, font, padding, borders);
                content[0].AddCell(visit != null && visit.DisciplineTaskName.IsNotNullOrEmpty() ? visit.DisciplineTaskName.ToUpper() : string.Empty, font, padding, borders);
                content[0].AddCell(code.IsNotNullOrEmpty() && BillInfo.ContainsKey(code) && BillInfo[code] != null ? BillInfo[code].CodeTwo : string.Empty, font, padding, borders);
                content[0].AddCell(visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : visit != null && visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToString("MMddyyyy") : string.Empty, font, padding, borders);
                content[0].AddCell(unit > 0 ? unit.ToString() : string.Empty, font, padding, borders);
                content[0].AddCell(code.IsNotNullOrEmpty() && BillInfo.ContainsKey(code) && BillInfo[code] != null && BillInfo[code].Amount.IsNotNullOrEmpty() && BillInfo[code].Amount.IsDouble() ? string.Format("{0:#0.00}", (BillInfo[code].Amount.ToDouble() * unit)) : string.Empty, font, "Right", moneypad, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
                content[0].AddCell(string.Empty, font, padding, borders);
            }
            return content;
        }
        protected override double CalculateTotal(UBOFourViewData data) {
            double total = 0;
            //PPSStandard ppsStandard;
           // if (data.Claim != null && data.Claim.SupplyTotal <= 0 && data.Claim.HippsCode.IsNotNullOrEmpty() && data.Claim.HippsCode.Length == 5) total += billingService.GetSupplyReimbursement(data.Claim.HippsCode[4], data.Claim.EpisodeStartDate.Year, out ppsStandard);
            //else if (data.Claim != null && data.Claim.SupplyTotal > 0) 
            total += data.Claim.SupplyTotal;
            var unitType = -1;
            var billInfo = this.billingService.GetInsuranceUnit(data.Claim != null ? data.Claim.PrimaryInsuranceId : 0, out unitType);
            var visits = data.Claim != null && data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.EventDate.IsValidDate()).OrderBy(s => s.EventDate.ToDateTime().Date).ToList() : new List<ScheduleEvent>();
            foreach (var visit in visits) {
                var unit = unitType == 1 ? 1 : (unitType == 2 ? (int)Math.Ceiling((double)visit.MinSpent / 60) : (unitType == 3 ? (int)Math.Ceiling((double)visit.MinSpent / 15) : 0)); 
                visit.Discipline = visit.Discipline == "Nursing" ? "SN" : visit.Discipline;
                var code = visit.GIdentify();
                total += code.IsNotNullOrEmpty() && billInfo != null && billInfo.ContainsKey(code) && billInfo[code].Amount.IsNotNullOrEmpty() && billInfo[code].Amount.IsDouble() ? billInfo[code].Amount.ToDouble() * unit : 0;
            }
            return total;
        }
    }
}