﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class BillingFinalPdf : AxxessPdf {
        public BillingFinalPdf(Final data) {
            this.SetType(PdfDocs.BillingFinal);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            AxxessTable[] content = new AxxessTable[3];
            content[0] = new AxxessContentSection("Remarks", new Paragraph(data != null && data.Remark.IsNotNullOrEmpty() ? data.Remark : " "), fonts[0], 12);
            content[1] = new AxxessTable(new float[] { 1, 4, 3, 2, 2, 2 }, false);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F };
            content[1].AddCell(string.Empty, fonts[0], padding, borders);
            content[1].AddCell("Description", fonts[0], padding, borders);
            content[1].AddCell("HCPCS/HIPPS Code", fonts[0], padding, borders);
            content[1].AddCell("Service Date", fonts[0], padding, borders);
            content[1].AddCell("Service Unit", fonts[0], padding, borders);
            content[1].AddCell("Total Charges", fonts[0], padding, borders);
            content[1].HeaderRows = 1;
            content[1].AddCell("0023", fonts[0], padding, borders);
            content[1].AddCell("Home Health Services", fonts[0], padding, borders);
            content[1].AddCell(data.HippsCode, fonts[0], padding, borders);
            content[1].AddCell(data.FirstBillableVisitDate != null ? data.FirstBillableVisitDate.ToShortDateString() : string.Empty, fonts[0], padding, borders);
            content[1].AddCell(string.Empty, fonts[0], padding, borders);
            content[1].AddCell(string.Empty, fonts[0], padding, borders);
            content[1].AddCell("0272", fonts[0], padding, borders);
            content[1].AddCell("Service Supplies", fonts[0], padding, borders);
            content[1].AddCell(string.Empty, fonts[0], padding, borders);
            content[1].AddCell(data.FirstBillableVisitDate != null ? data.FirstBillableVisitDate.ToShortDateString() : string.Empty, fonts[0], padding, borders);
            content[1].AddCell(string.Empty, fonts[0], padding, borders);
            content[1].AddCell(String.Format("${0:#0.00}", data.SupplyTotal), fonts[0], padding, borders);
            var total = data.SupplyTotal;
            var visits = data != null && data.VerifiedVisits.IsNotNullOrEmpty() ? data.VerifiedVisits.ToObject<List<ScheduleEvent>>().OrderBy(s => s.EventDate.ToZeroFilled()).ToList() : new List<ScheduleEvent>();
            var billInfo = data.BillInformations != null ? data.BillInformations : new Dictionary<String,BillInfo>();
            foreach (var visit in visits) {
                var discipline = visit.GIdentify();
                total += billInfo.ContainsKey(discipline) && billInfo[discipline].Amount.IsNotNullOrEmpty() ? double.Parse(billInfo[discipline].Amount) : 0.00;
                content[1].AddCell(billInfo.ContainsKey(discipline) ? billInfo[discipline].CodeOne : string.Empty, fonts[0], padding, borders);
                content[1].AddCell("Visit", fonts[0], padding, borders);
                content[1].AddCell(billInfo.ContainsKey(discipline) ? billInfo[discipline].CodeTwo : string.Empty, fonts[0], padding, borders);
                content[1].AddCell( visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyyy"): (visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToShortDateString() : string.Empty), fonts[0], padding, borders);
                content[1].AddCell(visit.Unit > 0 ? visit.Unit.ToString() : string.Empty, fonts[0], padding, borders);
                content[1].AddCell(billInfo.ContainsKey(discipline) ? String.Format("${0:#0.00}", billInfo[discipline].Amount.ToString()) : string.Empty, fonts[0], padding, borders);
            }
            content[2] = new AxxessTable(new float[] { 12, 2 }, false);
            content[2].AddCell("Total", fonts[0], padding, borders);
            content[2].AddCell(String.Format("${0:#0.00}", total), fonts[0], padding, borders);
            this.SetContent(content);
            float[] margins = new float[] { 366, 28, 28, 28 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            var conditionCodes = data.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(data.ConditionCodes) : null;
            var diagnosis = data.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(data.DiagnosisCode) : null;
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            if (data.Agency != null)
            {
                var location = data.Agency.GetBranch(data.BranchId);
                fieldmap[0].Add("agency", (
                    data != null && data.Agency != null ?
                        (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : string.Empty) +
                        (location != null ?
                            (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) +
                            (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                            (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) +
                            (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                            (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : string.Empty) +
                            (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : string.Empty) +
                            (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty)
                        : string.Empty)
                    : string.Empty));
            }
            fieldmap[0].Add("patient", (
                data != null ?
                    (data.AddressLine1.IsNotNullOrEmpty() ? data.AddressLine1.ToTitleCase() + "\n" : string.Empty) +
                    (data.AddressLine2.IsNotNullOrEmpty() ? data.AddressLine2.ToTitleCase() + "\n" : string.Empty) +
                    (data.AddressCity.IsNotNullOrEmpty() ? data.AddressCity.ToTitleCase() + ", " : string.Empty) +
                    (data.AddressStateCode.IsNotNullOrEmpty() ? data.AddressStateCode.ToTitleCase() + "  " : string.Empty) +
                    (data.AddressZipCode.IsNotNullOrEmpty() ? data.AddressZipCode + "\n" : string.Empty) : string.Empty));
            fieldmap[0].Add("mcare", data != null && data.MedicareNumber.IsNotNullOrEmpty() ? data.MedicareNumber : string.Empty);
            fieldmap[0].Add("record", data != null && data.PatientIdNumber.IsNotNullOrEmpty() ? data.PatientIdNumber : string.Empty);
            fieldmap[0].Add("gender", data != null && data.Gender.IsNotNullOrEmpty() ? data.Gender : string.Empty);
            fieldmap[0].Add("dob", data != null && data.DOB.IsValid() ? data.DOB.ToShortDateString() : string.Empty);
            fieldmap[0].Add("hipps", data != null && data.HippsCode.IsNotNullOrEmpty() ? data.HippsCode : string.Empty);
            fieldmap[0].Add("matchkey", data != null && data.ClaimKey.IsNotNullOrEmpty() ? data.ClaimKey : string.Empty);
            fieldmap[0].Add("episode", (data != null && data.EpisodeStartDate.IsValid() ? data.EpisodeStartDate.ToShortDateString() : string.Empty) + "-" + (data != null && data.EpisodeEndDate.IsValid() ? data.EpisodeEndDate.ToShortDateString() : string.Empty));
            fieldmap[0].Add("soc", data != null && data.StartofCareDate.IsValid() ? data.StartofCareDate.ToShortDateString() : string.Empty);
            fieldmap[0].Add("billvisit", data != null && data.FirstBillableVisitDate.IsValid() ? data.FirstBillableVisitDate.ToShortDateString() : string.Empty);
            fieldmap[0].Add("phys", data != null ? 
                (data.PhysicianLastName.IsNotNullOrEmpty() ? data.PhysicianLastName.ToTitleCase() + ", " : string.Empty) +
                (data.PhysicianFirstName.IsNotNullOrEmpty() ? data.PhysicianFirstName.ToTitleCase() : string.Empty) : string.Empty);
            fieldmap[0].Add("physnpi", data != null && data.PhysicianNPI.IsNotNullOrEmpty() ? data.PhysicianNPI : string.Empty);
            fieldmap[0].Add("hippspay", data != null && data.ProspectivePay > 0 ? data.ProspectivePay.ToString() : string.Empty);
            fieldmap[0].Add("diag1", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : string.Empty);
            fieldmap[0].Add("diag2", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : string.Empty);
            fieldmap[0].Add("diag3", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : string.Empty);
            fieldmap[0].Add("diag4", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : string.Empty);
            fieldmap[0].Add("diag5", diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value : string.Empty);
            fieldmap[0].Add("diag6", diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value : string.Empty);
            fieldmap[1].Add("patientname", data != null ?
                (data.LastName.IsNotNullOrEmpty() ? data.LastName.ToLower().ToTitleCase() + ", " : string.Empty) +
                (data.FirstName.IsNotNullOrEmpty() ? data.FirstName.ToLower().ToTitleCase() : string.Empty) : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}