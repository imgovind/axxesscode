﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    class SNVisitPdf : VisitNotePdf {
        public SNVisitPdf(VisitNoteViewData data) : base(data, PdfDocs.SkilledNurseVisit) { }
        protected override IElement[] Content(VisitNoteXml xml) {
            AxxessTable[] content = new AxxessTable[xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in xml.GetLayout()) {
                content[count] = new AxxessTable(section.Cols > 0 ? section.Cols : 1);
                foreach (XmlPrintSection subsection in section.Subsection) {
                    AxxessCell sectionCell = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { 0, .5F, .5F, 0 });
                    AxxessContentSection contentSection = new AxxessContentSection(subsection, this.GetFonts(), true, 7.5F, this.IsOasis);
                    sectionCell.AddElement(contentSection);
                    content[count].AddCell(sectionCell);
                }
                count++;
            }
            return content;
        }
        protected override float[] Margins(VisitNoteViewData data) {
            return new float[] { 82, 28.3F, 43, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", data != null && data.Agency != null && data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() : string.Empty);
            fieldmap[0].Add("PatientName", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : string.Empty);
            fieldmap[0].Add("MR", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("DOB", data != null && data.Patient != null && data.Patient.DOBFormatted.IsNotNullOrEmpty() ? data.Patient.DOBFormatted : string.Empty);
            fieldmap[0].Add("VisitDate", data != null && data.Questions != null && data.Questions.ContainsKey("VisitDate") && data.Questions["VisitDate"].Answer.IsNotNullOrEmpty() ? data.Questions["VisitDate"].Answer : string.Empty);
            fieldmap[0].Add("TimeIn", data != null && data.Questions != null && data.Questions.ContainsKey("TimeIn") && data.Questions["TimeIn"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeIn"].Answer : string.Empty);
            fieldmap[0].Add("TimeOut", data != null && data.Questions != null && data.Questions.ContainsKey("TimeOut") && data.Questions["TimeOut"].Answer.IsNotNullOrEmpty() ? data.Questions["TimeOut"].Answer : string.Empty);
            fieldmap[0].Add("EpisodeRange", data != null && data.StartDate.IsValid() && data.EndDate.IsValid() ? data.StartDate.ToShortDateString().ToZeroFilled() + "-" + data.EndDate.ToShortDateString().ToZeroFilled() : string.Empty);
            fieldmap[0].Add("PrimaryDX", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis") && data.Questions["PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis"].Answer : string.Empty);
            fieldmap[0].Add("SecondaryDX", data != null && data.Questions != null && data.Questions.ContainsKey("PrimaryDiagnosis") && data.Questions["PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? data.Questions["PrimaryDiagnosis1"].Answer : string.Empty);
            fieldmap[0].Add("Clinician", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : string.Empty);
            fieldmap[0].Add("SignatureDate", data != null && data.SignatureDate.IsNotNullOrEmpty() ? data.SignatureDate : string.Empty);
            return fieldmap;
        }
    }
}