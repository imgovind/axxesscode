﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Extensions;
    class DischargeSummaryPdf : VisitNotePdf {
        public DischargeSummaryPdf(VisitNoteViewData data) : base(data, PdfDocs.DischargeSummary) { }
        protected override float[] Margins(VisitNoteViewData data) {
            return new float[] { 200, 28.3F, 80, 28.3F };
        }
        protected override List<Dictionary<string,string>> FieldMap(VisitNoteViewData data) {
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            if (location == null) location = data.Agency.GetMainOffice();
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[0].Add("visitdate", data != null && data.VisitDate != null && data.VisitDate.IsValidDate() ? data.VisitDate : "");
            fieldmap[0].Add("dcdate", data != null && data.Questions != null && data.Questions.ContainsKey("DischargeDate") && data.Questions["DischargeDate"].Answer.IsNotNullOrEmpty() ? data.Questions["DischargeDate"].Answer : "");
            fieldmap[0].Add("episode", data != null && data.StartDate != null && data.StartDate.IsValid() && data.EndDate != null && data.EndDate.IsValid() ? data.StartDate.ToShortDateString() + "-" + data.EndDate.ToShortDateString() : "");
            fieldmap[0].Add("mr", data != null && data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : "");
            fieldmap[0].Add("physician", data != null && data.PhysicianDisplayName != null ? data.PhysicianDisplayName.ToTitleCase() : "");
            fieldmap[0].Add("dcnotice", data != null && data.Questions != null && data.Questions.ContainsKey("IsNotificationDC") && data.Questions["IsNotificationDC"].Answer.Equals("1") ? (data.Questions.ContainsKey("NotificationDate") && data.Questions["NotificationDate"].Answer.IsNotNullOrEmpty() ? (data.Questions["NotificationDate"].Answer.Equals("0") ? "Yes" : "") + (data.Questions["NotificationDate"].Answer.Equals("1") ? "5 day" : "") + (data.Questions["NotificationDate"].Answer.Equals("2") ? "2 day" : "") + (data.Questions["NotificationDate"].Answer.Equals("3") ? "Other" : "") : "Yes") : "No");
            fieldmap[0].Add("dcreason", data != null && data.Questions != null && data.Questions.ContainsKey("ReasonForDC") && data.Questions["ReasonForDC"].Answer.IsNotNullOrEmpty() ?
                (data.Questions["ReasonForDC"].Answer.Equals("1") ? "Goals Met" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("2") ? "To Nursing Home" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("3") ? "Deceased" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("4") ? "Noncompliant" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("5") ? "To Hospital" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("6") ? "Moved from Service Area" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("7") ? "Refused Care" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("8") ? "No Longer Homebound" : "") +
                (data.Questions["ReasonForDC"].Answer.Equals("9") ? "Other" : "") : "");
            fieldmap[0].Add("sign", data != null && data.SignatureText.IsNotNullOrEmpty() ? data.SignatureText : "");
            fieldmap[0].Add("signdate", data != null && data.SignatureDate != null && data.SignatureDate.ToDateTime().IsValid() ? data.SignatureDate : "");
            fieldmap[1].Add("patientname", data != null && data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            return fieldmap;
        }
    }
}