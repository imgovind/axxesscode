﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using XmlParsing;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.OasisC.Domain;
    using Axxess.Core.Extension;
    using Axxess.OasisC.Extensions;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.LookUp.Repositories;
    using Axxess.LookUp.Domain;
    using Axxess.Core.Infrastructure;
    public class OasisProfilePdf : AxxessPdf {
        private OasisProfileXml xml;
        public OasisProfilePdf(Assessment data, Agency agency) {
            this.IsOasis = true;
            this.xml = new OasisProfileXml(data);
            this.SetType(PdfDocs.OasisProfile);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            this.SetContent(content);
            float[] margins = new float[] { 82, 28.3F, 28.3F, 28.3F };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", (
                data != null && agency != null ?
                    (agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + "\n" : "") +
                    (agency.MainLocation != null ?
                        (agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? agency.MainLocation.AddressLine1.ToTitleCase() : "") +
                        (agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? agency.MainLocation.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? agency.MainLocation.AddressCity.ToTitleCase() + ", " : "") +
                        (agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? agency.MainLocation.AddressStateCode.ToString().ToUpper() + "  " : "") +
                        (agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? agency.MainLocation.AddressZipCode : "") +
                        (agency.MainLocation.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + agency.MainLocation.PhoneWorkFormatted : "") +
                        (agency.MainLocation.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + agency.MainLocation.FaxNumberFormatted : "")
                    : "")
                : ""));
            this.SetFields(fieldmap);
        }
    }
}
