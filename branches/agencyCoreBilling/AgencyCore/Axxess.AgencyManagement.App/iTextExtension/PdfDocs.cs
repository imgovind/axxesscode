﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    public class PdfDocs {
        public static PdfDoc IncidentReport         = new PdfDoc(AppSettings.IncidentReportPdf, AppSettings.IncidentReportXml);
        public static PdfDoc InfectionReport        = new PdfDoc(AppSettings.InfectionReportPdf, AppSettings.InfectionReportXml);
        public static PdfDoc BillingClaims          = new PdfDoc(AppSettings.BillingClaimsPdf);
        public static PdfDoc BillingFinal           = new PdfDoc(AppSettings.BillingFinalPdf);
        public static PdfDoc BillingRap             = new PdfDoc(AppSettings.BillingRapPdf);
        public static PdfDoc Remittances            = new PdfDoc(AppSettings.RemittancesPdf);
        public static PdfDoc Remittance             = new PdfDoc(AppSettings.RemittancePdf);
        public static PdfDoc UB04                   = new PdfDoc(AppSettings.UB04Pdf);
        public static PdfDoc Oasis                  = new PdfDoc(AppSettings.OasisPdf, AppSettings.OasisXml);
        public static PdfDoc OasisAudit             = new PdfDoc(AppSettings.OasisAuditPdf);
        public static PdfDoc OasisProfile           = new PdfDoc(AppSettings.OasisProfilePdf, AppSettings.OasisProfileXml);
        public static PdfDoc HospitalizationLog     = new PdfDoc(AppSettings.HospitalizationLogPdf, AppSettings.HospitalizationLogXml);
        public static PdfDoc PlanOfCare485          = new PdfDoc(AppSettings.PlanOfCare485Pdf);
        public static PdfDoc PlanOfCare487          = new PdfDoc(AppSettings.PlanOfCare487Pdf);
        public static PdfDoc AllergyProfile         = new PdfDoc(AppSettings.AllergyProfilePdf);
        public static PdfDoc ComNote                = new PdfDoc(AppSettings.ComNotePdf);
        public static PdfDoc MedicareEligibility    = new PdfDoc(AppSettings.MedicareEligibiltyPdf);
        public static PdfDoc MedProfile             = new PdfDoc(AppSettings.MedProfilePdf);
        public static PdfDoc PatientProfile         = new PdfDoc(AppSettings.PatientProfilePdf);
        public static PdfDoc PhysicianOrder         = new PdfDoc(AppSettings.PhysicianOrderPdf);
        public static PdfDoc PhysFaceToFace         = new PdfDoc(AppSettings.PhysFaceToFacePdf, AppSettings.PhysFaceToFaceXml);
        public static PdfDoc PayrollSummary         = new PdfDoc(AppSettings.PayrollSummaryPdf);
        public static PdfDoc MasterCalendar         = new PdfDoc(AppSettings.MasterCalendarPdf);
        public static PdfDoc MonthCalendar          = new PdfDoc(AppSettings.MonthCalendarPdf);
        public static PdfDoc HHACarePlan            = new PdfDoc(AppSettings.HHACarePlanPdf, AppSettings.HHACarePlanXml);
        public static PdfDoc HHASVisit              = new PdfDoc(AppSettings.HHASVisitPdf, AppSettings.HHASVisitXml);
        public static PdfDoc HHAVisit               = new PdfDoc(AppSettings.HHAVisitPdf, AppSettings.HHAVisitXml);
        public static PdfDoc MissedVisit            = new PdfDoc(AppSettings.MissedVisitPdf);
        public static PdfDoc MSWEval                = new PdfDoc(AppSettings.MSWEvalPdf, AppSettings.MSWEvalXml);
        public static PdfDoc MSWProgress            = new PdfDoc(AppSettings.MSWProgressPdf, AppSettings.MSWProgressXml);
        public static PdfDoc MSWVisit               = new PdfDoc(AppSettings.MSWVisitPdf, AppSettings.MSWVisitXml);
        public static PdfDoc TransportationLog      = new PdfDoc(AppSettings.TransportationLogPdf, AppSettings.TransportationLogXml);
        public static PdfDoc DiabeticDaily          = new PdfDoc(AppSettings.DiabeticDailyPdf, AppSettings.DiabeticDailyXml);
        public static PdfDoc DischargeSummary       = new PdfDoc(AppSettings.DischargeSummaryPdf, AppSettings.DischargeSummaryXml);
        public static PdfDoc LVNSVisit              = new PdfDoc(AppSettings.LVNSVisitPdf, AppSettings.LVNSVisitXml);
        public static PdfDoc SixtyDaySummary        = new PdfDoc(AppSettings.SixtyDaySummaryPdf, AppSettings.SixtyDaySummaryXml);
        public static PdfDoc SkilledNurseVisit      = new PdfDoc(AppSettings.SkilledNurseVisitPdf, AppSettings.SkilledNurseVisitXml);
        public static PdfDoc TransferSummary        = new PdfDoc(AppSettings.TransferSummaryPdf, AppSettings.TransferSummaryXml);
        // public static PdfDoc PASCarePlan         = new PdfDoc();
        // public static PdfDoc PASVisit            = new PdfDoc();
        public static PdfDoc OTEval                 = new PdfDoc(AppSettings.OTEvalPdf, AppSettings.OTEvalXml);
        public static PdfDoc OTVisit                = new PdfDoc(AppSettings.OTVisitPdf, AppSettings.OTVisitXml);
        public static PdfDoc PTDischarge            = new PdfDoc(AppSettings.PTDischargePdf, AppSettings.PTDischargeXml);
        public static PdfDoc PTEval                 = new PdfDoc(AppSettings.PTEvalPdf, AppSettings.PTEvalXml);
        public static PdfDoc PTVisit                = new PdfDoc(AppSettings.PTVisitPdf, AppSettings.PTVisitXml);
        public static PdfDoc STDischarge            = new PdfDoc(AppSettings.STDischargePdf, AppSettings.STDischargeXml);
        public static PdfDoc STEval                 = new PdfDoc(AppSettings.STEvalPdf, AppSettings.STEvalXml);
        public static PdfDoc STVisit                = new PdfDoc(AppSettings.STVisitPdf, AppSettings.STVisitXml);
        public static PdfDoc DrugDrugInteraction    = new PdfDoc(AppSettings.DrugDrugInteractionPdf);

    }
}
