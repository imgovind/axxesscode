﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    class VisitNoteXml : BaseXml {
        private IDictionary<String, NotesQuestion> Data = new Dictionary<String, NotesQuestion>();
        public VisitNoteXml(VisitNoteViewData data, PdfDoc type) : base(type) {
            if (type == PdfDocs.SkilledNurseVisit && data.IsWoundCareExist) {
                this.Data = data.MergeDictionaries();
                this.Type = "woundcare";
            } else this.Data = data.Questions;
            this.Init();
            if (data.Questions != null) this.FilterEmptySections();
        }
        private void FilterEmptySections() {
            for (int sectionI = 0; sectionI < this.Layout.Count(); sectionI++) {
                for (int subsectionI = 0; subsectionI < this.Layout[sectionI].Subsection.Count(); subsectionI++) {
                    if (this.Layout[sectionI].Subsection[subsectionI].Type == "woundgraph") subsectionI = this.RemoveUnusedWounds(sectionI, subsectionI);
                }
            }
            this.NotaFilter();
        }
        private int RemoveUnusedWounds(int Section, int Subsection) {
            if (this.Layout[Section].Subsection[Subsection].Question[0].Subquestion[0].Data.Trim().IsNullOrEmpty()) this.Layout[Section].Subsection.RemoveAt(Subsection--);
            return Subsection;
        }
        public override String GetData(String Index) {
            return this.Data != null && this.Data.ContainsKey(Index) && this.Data[Index].Answer.IsNotNullOrEmpty() ? this.Data[Index].Answer : string.Empty;
        }
    }
}