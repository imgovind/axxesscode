﻿namespace Axxess.AgencyManagement.App.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Enums;
    using Services;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    using Axxess.AgencyManagement.Extensions;
    using Axxess.OasisC.Enums;

    public static class Url
    {
        public static void Set(ScheduleEvent scheduleEvent, bool addReassignLink, bool usePrintIcon)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0 && !scheduleEvent.EpisodeId.IsEmpty() && !scheduleEvent.PatientId.IsEmpty() && !scheduleEvent.EventId.IsEmpty())
            {
                string onclick = string.Empty;
                string reopenUrl = string.Empty;
                string printUrl = Print(scheduleEvent, usePrintIcon);
                string detailUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.GetTaskDetails('{0}', '{1}', '{2}');\">Details</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                string deleteUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.Delete('{0}','{1}','{2}','{3}','{4}');\" >Delete</a>", scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId, scheduleEvent.UserId, scheduleEvent.DisciplineTask);
                string reassignUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ReAssign($(this), '{0}','{1}','{2}','{3}');\" class=\"reassign\">Reassign</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, scheduleEvent.UserId);
                string oasisProfileUrl = string.Empty;
                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;

                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                        onclick = string.Format("StartOfCare.Load('{0}','{1}','StartOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        if (scheduleEvent.Status == ((int)ScheduleStatus.OasisExported).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString()|| scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString())
                        {
                            oasisProfileUrl = "<a href=\"javascript:void(0)\" onclick=\"" +
                                "Acore.OpenPrintView({ " +
                                    "Url: 'Oasis/Profile/" + scheduleEvent.EventId + "/" + AssessmentType.StartOfCare.ToString() + "'," +
                                    "PdfUrl: 'Oasis/OasisProfilePdf'," +
                                    "PdfData: { 'Id': '" + scheduleEvent.EventId + "', 'type': '" + AssessmentType.StartOfCare.ToString() + "' }" +
                                "})\"><span class=\"img icon money\"></span></a>";
                        }
                        break;
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.NonOASISStartofCare:
                        onclick = string.Format("NonOasisStartOfCare.Load('{0}','{1}','NonOasisStartOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        onclick = string.Format("ResumptionOfCare.Load('{0}','{1}','ResumptionOfCare');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        if (scheduleEvent.Status == ((int)ScheduleStatus.OasisExported).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString())
                        {
                            oasisProfileUrl = "<a href=\"javascript:void(0)\" onclick=\"" +
                                "Acore.OpenPrintView({ " +
                                    "Url: 'Oasis/Profile/" + scheduleEvent.EventId + "/" + AssessmentType.ResumptionOfCare.ToString() + "'," +
                                    "PdfUrl: 'Oasis/OasisProfilePdf'," +
                                    "PdfData: { 'Id': '" + scheduleEvent.EventId + "', 'type': '" + AssessmentType.ResumptionOfCare.ToString() + "' }" +
                                "})\"><span class=\"img icon money\"></span></a>";
                        }
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        onclick = string.Format("FollowUp.Load('{0}','{1}','FollowUp');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);

                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        onclick = string.Format("Recertification.Load('{0}','{1}','Recertification');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        if (scheduleEvent.Status == ((int)ScheduleStatus.OasisExported).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || scheduleEvent.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString())
                        {
                            oasisProfileUrl = "<a href=\"javascript:void(0)\" onclick=\"" +
                                "Acore.OpenPrintView({ " +
                                    "Url: 'Oasis/Profile/" + scheduleEvent.EventId + "/" + AssessmentType.Recertification.ToString() + "'," +
                                    "PdfUrl: 'Oasis/OasisProfilePdf'," +
                                    "PdfData: { 'Id': '" + scheduleEvent.EventId + "', 'type': '" + AssessmentType.Recertification.ToString() + "' }" +
                                "})\"><span class=\"img icon money\"></span></a>";
                        }
                        break;
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.NonOASISRecertification:
                        onclick = string.Format("NonOasisRecertification.Load('{0}','{1}','NonOasisRecertification');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        onclick = string.Format("TransferInPatientNotDischarged.Load('{0}','{1}','TransferInPatientNotDischarged');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                       
                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                        onclick = string.Format("TransferInPatientDischarged.Load('{0}','{1}','TransferInPatientDischarged');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                       
                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        onclick = string.Format("DischargeFromAgencyDeath.Load('{0}','{1}','DischargeFromAgencyDeath');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                       
                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                        onclick = string.Format("DischargeFromAgency.Load('{0}','{1}','DischargeFromAgency');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                       
                        break;
                    case DisciplineTasks.NonOASISDischarge:
                        onclick = string.Format("NonOasisDischarge.Load('{0}','{1}','NonOasisDischarge');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        onclick = string.Format("UserInterface.ShowEditOrder('{0}','{1}');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        // onclick = string.Format("UserInterface.ShowEditOrder('{0}','{1}');", scheduleEvent.EventId, scheduleEvent.PatientId);
                        scheduleEvent.Url = string.Format("{0}", scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HCFA485:
                        onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        onclick = string.Format("UserInterface.ShowEditPlanofCareStandAlone('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.NonOasisHCFA485:
                        onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.CommunicationNote:
                        onclick = string.Format("Patient.loadEditCommunicationNote('{0}','{1}');", scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.DischargeSummary:
                        onclick = string.Format("dischargeSummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVObservationAndAssessment:
                        onclick = string.Format("snVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        onclick = string.Format("snDiabeticDailyVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.SixtyDaySummary:
                        onclick = string.Format("sixtyDaySummary.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.TransferSummary:
                        onclick = string.Format("Schedule.loadTransferSummary('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.CoordinationOfCare:
                        onclick = string.Format("Schedule.loadCoordinationOfCare('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        onclick = string.Format("lvnSupVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        onclick = string.Format("hhaSupVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideVisit:
                        onclick = string.Format("hhaVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        onclick = string.Format("hhaCarePlan.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PASVisit:
                        onclick = string.Format("pasVisit.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PASCarePlan:
                        onclick = string.Format("pasCarePlan.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PTVisit:
                        onclick = string.Format("Schedule.loadPTVisit('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PTAVisit:
                        onclick = string.Format("Schedule.loadPTAVisit('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PTEvaluation:
                        onclick = string.Format("Schedule.loadPTEvaluation('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PTReEvaluation:
                        onclick = string.Format("Schedule.loadPTReEvaluation('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PTMaintenance:
                        onclick = string.Format("Schedule.loadPTMaintenance('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PTDischarge:
                        onclick = string.Format("Schedule.loadPTDischarge('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OTVisit:
                        onclick = string.Format("Schedule.loadOTVisit('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.COTAVisit:
                        onclick = string.Format("Schedule.loadCOTVisit('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OTEvaluation:
                        onclick = string.Format("Schedule.loadOTEvaluation('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OTReEvaluation:
                        onclick = string.Format("Schedule.loadOTReEvaluation('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OTMaintenance:
                        onclick = string.Format("Schedule.loadOTMaintenance('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.OTDischarge:
                        onclick = string.Format("Schedule.loadOTDischarge('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.STVisit:
                        onclick = string.Format("Schedule.loadSTVisit('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.STEvaluation:
                        onclick = string.Format("Schedule.loadSTEvaluation('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.STReEvaluation:
                        onclick = string.Format("Schedule.loadSTReEvaluation('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.STMaintenance:
                        onclick = string.Format("Schedule.loadSTMaintenance('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.STDischarge:
                        onclick = string.Format("Schedule.loadSTDischarge('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.IncidentAccidentReport:
                        onclick = string.Format("UserInterface.ShowEditIncident('{0}');", scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.InfectionReport:
                        onclick = string.Format("UserInterface.ShowEditInfection('{0}');", scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.MSWEvaluationAssessment:
                        onclick = string.Format("Schedule.loadMSWEvaluation('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.MSWAssessment:
                        onclick = string.Format("Schedule.loadMSWAssessment('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.MSWProgressNote:
                        onclick = string.Format("Schedule.loadMSWProgressNote('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.MSWDischarge:
                        onclick = string.Format("Schedule.loadMSWDischarge('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.MSWVisit:
                        onclick = string.Format("Schedule.loadMSWVisit('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.DriverOrTransportationNote:
                        onclick = string.Format("transportationLog.Load('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"{0}\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                }

                if (Current.HasRight(Permissions.EditTaskDetails))
                {
                    scheduleEvent.ActionUrl = detailUrl;
                }

                if (Current.HasRight(Permissions.DeleteTasks))
                {
                    if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                    {
                        scheduleEvent.ActionUrl += " | " + deleteUrl;
                    }
                    else
                    {
                        scheduleEvent.ActionUrl = deleteUrl;
                    }
                }

                if (addReassignLink && Current.HasRight(Permissions.ScheduleVisits))
                {
                    if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                    {
                        scheduleEvent.ActionUrl += " | " + reassignUrl;
                    }
                    else
                    {
                        scheduleEvent.ActionUrl = reassignUrl;
                    }
                }

                scheduleEvent.PrintUrl = printUrl;

                if (scheduleEvent.IsCompleted())
                {
                    scheduleEvent.ActionUrl = string.Empty;
                    if (Current.HasRight(Permissions.EditTaskDetails))
                    {
                        scheduleEvent.ActionUrl = detailUrl;
                    }
                    scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                }

                if (scheduleEvent.IsCompletelyFinished())
                {
                    if (Current.HasRight(Permissions.EditTaskDetails))
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty() && !scheduleEvent.ActionUrl.Contains(detailUrl))
                        {
                            scheduleEvent.ActionUrl += " | " + detailUrl;
                        }
                        else
                        {
                            scheduleEvent.ActionUrl = detailUrl;
                        }
                    }

                    if (Current.HasRight(Permissions.DeleteTasks))
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                        {
                            scheduleEvent.ActionUrl += " | " + deleteUrl;
                        }
                        else
                        {
                            scheduleEvent.ActionUrl = deleteUrl;
                        }
                    }

                    if (Current.HasRight(Permissions.ReopenDocuments))
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                        {
                            reopenUrl = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.ReOpen('{0}','{1}','{2}');{3}\" class=\"reopen\">Reopen Task</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, onclick);
                            scheduleEvent.ActionUrl += " | " + reopenUrl;
                        }
                        else
                        {
                            scheduleEvent.ActionUrl = reopenUrl;
                        }
                    }

                    if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                    {
                        scheduleEvent.ActionUrl = string.Empty;
                    }
                }

                if (scheduleEvent.IsMissedVisit)
                {
                    scheduleEvent.Url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.MissedVisitPopup($(this), '{0}');\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                    if (Current.HasRight(Permissions.EditTaskDetails))
                    {
                        scheduleEvent.ActionUrl = detailUrl;
                    }

                    if (Current.HasRight(Permissions.DeleteTasks))
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                        {
                            scheduleEvent.ActionUrl += " | " + deleteUrl;
                        }
                        else
                        {
                            scheduleEvent.ActionUrl = deleteUrl;
                        }
                    }
                }

                if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
                {
                }
                else if (Current.IsClinicianOrHHA)
                {
                    if (Current.UserId != scheduleEvent.UserId)
                    {
                        scheduleEvent.PrintUrl = string.Empty;
                        scheduleEvent.ActionUrl = string.Empty;
                        scheduleEvent.Status = string.Empty;
                        scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                    }
                }
                else
                {
                    scheduleEvent.PrintUrl = string.Empty;
                    scheduleEvent.ActionUrl = string.Empty;
                    scheduleEvent.Status = string.Empty;
                }

                if (scheduleEvent.IsOrphaned)
                {
                    scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                    if (Current.HasRight(Permissions.EditTaskDetails))
                    {
                        scheduleEvent.ActionUrl = detailUrl;
                    }

                    if (Current.HasRight(Permissions.DeleteTasks))
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                        {
                            scheduleEvent.ActionUrl += " | " + deleteUrl;
                        }
                        else
                        {
                            scheduleEvent.ActionUrl = deleteUrl;
                        }
                    }

                    if (scheduleEvent.UserName.IsEqual("Axxess"))
                    {
                        scheduleEvent.ActionUrl = string.Empty;
                    }
                }
                if (Current.HasRight(Permissions.ViewHHRGCalculations))
                {
                    scheduleEvent.OasisProfileUrl = oasisProfileUrl;
                }
            }
        }

        public static string Print(Guid episodeId, Guid patientId, Guid eventId, DisciplineTasks task, int status, bool usePrintIcon)
        {
            var scheduleEvent = new ScheduleEvent
            {
                EpisodeId = episodeId,
                PatientId = patientId,
                EventId = eventId,
                DisciplineTask = (int)task,
                Status = status.ToString()
            };

            return Print(scheduleEvent, usePrintIcon);
        }

        public static string Print(ScheduleEvent scheduleEvent, bool usePrintIcon)
        {
            string printUrl = string.Empty;
            string linkText = scheduleEvent.DisciplineTaskName;
            if (usePrintIcon) linkText = "<span class=\"img icon print\"></span>";
            if (scheduleEvent.IsMissedVisit)
                printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                    "Url: '/MissedVisit/View/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                    "PdfUrl: 'Schedule/MissedVisitPdf'," +
                    "PdfData: { 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                    "})\">" + linkText + "</a>";
            else
            {
                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;
                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/StartOfCare/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/StartOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','StartOfCare','Return','startofcare');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { StartOfCare.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','StartOfCare'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','StartOfCare','Approve','startofcare');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.NonOASISStartofCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/NonOasisStartofCare/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/NonOasisStartOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisStartOfCare','Return','nonoasisstartofcare');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { NonOasisStartOfCare.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','NonOasisStartOfCare'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisStartOfCare','Approve','nonoasisstartofcare');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/ResumptionOfCare/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/ResumptionOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','ResumptionOfCare','Return','resumptionofcare'); }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { ResumptionOfCare.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','ResumptionOfCare'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','ResumptionOfCare','Approve','resumptionofcare');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/FollowUp/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/FollowUpPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','FollowUp','Return','followup');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { FollowUp.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','FollowUp'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','FollowUp','Approve','followup');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Recertification/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/RecertificationPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','Recertification','Return','recertification');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Recertification.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','Recertification'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','Recertification','Approve','recertification');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.NonOASISRecertification:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/NonOasisRecertification/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/NonOasisRecertificationPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisRecertification','Return','nonoasisrecertification');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { NonOasisRecertification.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','NonOasisRecertification'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisRecertification','Approve','nonoasisrecertification');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/TransferInPatientNotDischarged/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/TransferInPatientNotDischargedPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientNotDischarged','Return','transfernotdischarge');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { TransferInPatientNotDischarged.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','TransferInPatientNotDischarged'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientNotDischarged','Approve','transfernotdischarge');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/TransferInPatientDischarged/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/TransferInPatientDischargedPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientDischarged','Return','transferinpatientdischarged');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { TransferInPatientDischarged.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','TransferInPatientDischarged'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','TransferInPatientDischarged','Approve','transferinpatientdischarged');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/DischargeFromAgencyDeath/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/DischargeFromAgencyDeathPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgencyDeath','Return','DischargeFromAgencyDeath'); }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { DischargeFromAgencyDeath.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','DischargeFromAgencyDeath'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgencyDeath','Approve','DischargeFromAgencyDeath'); } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/DischargeFromAgency/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/DischargeFromAgencyPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgency','Return','discharge');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { DischargeFromAgency.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','DischargeFromAgency'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','DischargeFromAgency','Approve','discharge');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.NonOASISDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/NonOasisDischarge/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/NonOasisDischargePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisDischarge','Return','nonoasisdischarge');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { NonOasisDischarge.Load('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','NonOasisDischarge'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Oasis.OasisStatusAction('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','NonOasisDischarge','Approve','nonoasisdischarge');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/FaceToFaceEncounter/View/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Patient/PhysicianFaceToFaceEncounterPdf'," +
                            "PdfData: { 'patientId': '" + scheduleEvent.PatientId + "', 'orderId': '" + scheduleEvent.EventId + "' }" +
                            "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Order/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Patient/PhysicianOrderPdf'," +
                            "PdfData: { 'orderId': '" + scheduleEvent.EventId + "', 'patientId': '" + scheduleEvent.PatientId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PhysicianOrder','Return');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditOrder('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PhysicianOrder','Approve');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/485/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/PlanOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCare','Return');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditPlanofCare('" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCare','Approve');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/485/PrintPreview/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Oasis/PlanOfCarePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCareStandAlone','Return');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditPlanofCareStandAlone('" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.UpdateOrderStatus('" + scheduleEvent.EventId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EpisodeId + "','PlanofCareStandAlone','Approve');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CommunicationNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/CommunicationNote/View/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Patient/CommunicationNotePdf'," +
                            "PdfData: { 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Patient.ProcessCommunicationNote('Return','" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Patient.loadEditCommunicationNote('" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Patient.ProcessCommunicationNote('Approve','" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.DischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/DischargeSummary/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "', " +
                            "PdfUrl: 'Schedule/DischargeSummaryPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { dischargeSummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVTeachingTraining:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/SNVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/SNVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'View Plan of Care'," +
                                    "Click: function() { Schedule.GetPlanofCareUrl('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); } }, {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/SNDiabeticDailyVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/SNDiabeticDailyVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { snDiabeticDailyVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SixtyDaySummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/SixtyDaySummary/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/SixtyDaySummaryPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { sixtyDaySummary.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.TransferSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/TransferSummary/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/TransferSummaryPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadTransferSummary('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CoordinationOfCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/TransferSummary/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/TransferSummaryPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadCoordinationOfCare('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/LVNSupervisoryVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/LVNSVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { lvnSupVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/HHAideSupervisoryVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/HHASVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { hhaSupVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/HHAVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/HHAVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { hhaVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/HHACarePlan/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Schedule/HHACarePlanPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { hhaCarePlan.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PASVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/PASVisitNote/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { pasVisit.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PASCarePlan:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/PASCarePlan/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { pasCarePlan.Load('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTAVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/PTVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/PTVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadPTVisit('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTMaintenance:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/PTEvaluation/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/PTEvaluationPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadPTEvaluation('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/PTDischarge/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/PTDischargePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadPTDischarge('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTMaintenance:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/OTEvaluation/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/OTEvaluationPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadOTEvaluation('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.COTAVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/OTVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/OTVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadOTVisit('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STMaintenance:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/STEvaluation/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/STEvaluationPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadSTEvaluation('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.STDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/STDischarge/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/STDischargePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadSTDischarge('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.STVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/STVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/STVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadSTVisit('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWAssessment:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/MSWEvaluation/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/MSWEvaluationPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadMSWEvaluation('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWProgressNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/MSWProgressNote/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/MSWProgressNotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadMSWProgressNote('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/MSWVisit/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/MSWVisitPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { Schedule.ProcessNote('Return','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { Schedule.loadMSWVisit('" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "', '" + task.ToString() + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { Schedule.ProcessNote('Approve','" + scheduleEvent.EpisodeId + "','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InfectionReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Infection/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Agency/InfectionReportPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { InfectionReport.ProcessInfection('Return','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditInfection('" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { InfectionReport.ProcessInfection('Approve','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: '/Incident/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: '/Agency/IncidentReportPdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            (!usePrintIcon ?
                                ", ReturnClick: function() { IncidentReport.ProcessIncident('Return','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  }" +
                                ", Buttons: [ {" +
                                    "Text: 'Edit'," +
                                    "Click: function() { UserInterface.ShowEditIncident('" + scheduleEvent.EventId + "'); UserInterface.CloseModal(); } }, {" +
                                    "Text: 'Approve'," +
                                    "Click: function() { IncidentReport.ProcessIncident('Approve','" + scheduleEvent.PatientId + "','" + scheduleEvent.EventId + "');  } } ]"
                            : "") + "})\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MedicareEligibilityReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'mcareEligibilityId': '" + scheduleEvent.EventId + "' });\">" + (usePrintIcon ? "<span class='img icon print'></span>" : linkText) + "</a>";
                        break;
                    case DisciplineTasks.DriverOrTransportationNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ " +
                            "Url: 'TransportationNote/View/" + scheduleEvent.EpisodeId + "/" + scheduleEvent.PatientId + "/" + scheduleEvent.EventId + "'," +
                            "PdfUrl: 'Schedule/TransportationNotePdf'," +
                            "PdfData: { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' }" +
                            "})\">" + linkText + "</a>";
                        break;
                }
            }
            return printUrl;
        }

        public static string Download(ScheduleEvent scheduleEvent, bool usePrintIcon)
        {
            string printUrl = string.Empty;
            string linkText = scheduleEvent.DisciplineTaskName;
            if (usePrintIcon) linkText = "<span class=\"img icon print\"></span>";
            if (scheduleEvent.IsMissedVisit)
            {
                printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/MissedVisitPdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
            }
            else
            {
                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;
                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/StartOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.NonOASISStartofCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/NonOasisStartOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/ResumptionOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/FollowUpPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/RecertificationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.NonOASISRecertification:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/NonOasisRecertificationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/TransferInPatientNotDischargedPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCTransferDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/TransferInPatientDischargedPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/DischargeFromAgencyDeathPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/DischargeFromAgencyPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.NonOASISDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/NonOasisDischargePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/PhysicianFaceToFaceEncounterPdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'orderId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/PhysicianOrderPdf', { 'orderId': '" + scheduleEvent.EventId + "', 'patientId': '" + scheduleEvent.PatientId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/PlanOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Oasis/PlanOfCarePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CommunicationNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/CommunicationNotePdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.DischargeSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/DischargeSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVTeachingTraining:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/SNVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SixtyDaySummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/SixtyDaySummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.TransferSummary:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/TransferSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CoordinationOfCare:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/TransferSummaryPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/LVNSVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideSupervisoryVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/HHASVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/HHAVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HHAideCarePlan:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/HHACarePlanPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PASVisit:
                        printUrl = string.Empty;
                        break;
                    case DisciplineTasks.PASCarePlan:
                        printUrl = string.Empty;
                        break;
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTAVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PTVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTMaintenance:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PTEvaluationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PTDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/PTDischargePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTMaintenance:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/OTEvaluationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.COTAVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/OTVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STMaintenance:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/STEvaluationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.STDischarge:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/STDischargePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.STVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/STVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWAssessment:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/MSWEvaluationPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWProgressNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/MSWProgressNotePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MSWVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/MSWVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InfectionReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Agency/InfectionReportPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Agency/IncidentReportPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MedicareEligibilityReport:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + scheduleEvent.PatientId + "', 'mcareEligibilityId': '" + scheduleEvent.EventId + "' });\">" + (usePrintIcon ? "<span class='img icon print'></span>" : linkText) + "</a>";
                        break;
                    case DisciplineTasks.DriverOrTransportationNote:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/TransportationNotePdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        printUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Schedule/SNDiabeticDailyVisitPdf', { 'episodeId': '" + scheduleEvent.EpisodeId + "', 'patientId': '" + scheduleEvent.PatientId + "', 'eventId': '" + scheduleEvent.EventId + "' });\">" + linkText + "</a>";
                        break;
                }
            }
            return printUrl;
        }
    }
}