﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class ReportModule : Module
    {
        public override string Name
        {
            get { return "Report"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region Patient
            routes.MapRoute(
                "ReportPatientRoster",
                "Report/Patient/Roster",
                new { controller = this.Name, action = "PatientRoster", id = UrlParameter.Optional });

            routes.MapRoute(
                  "ReportCAHPSReport",
                  "Report/Patient/Cahps",
                  new { controller = this.Name, action = "Cahps", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientEmergencyList",
               "Report/Patient/EmergencyList",
               new { controller = this.Name, action = "PatientEmergencyList", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientBirthdayList",
               "Report/Patient/Birthdays",
               new { controller = this.Name, action = "PatientBirthdayList", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientAddressList",
               "Report/Patient/AddressList",
               new { controller = this.Name, action = "PatientAddressList", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientPhysicians",
               "Report/Patient/Physician",
               new { controller = this.Name, action = "PatientByPhysicians", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportPatientSocCertPeriod",
             "Report/Patient/SocCertPeriod",
             new { controller = this.Name, action = "PatientSocCertPeriodListing", id = UrlParameter.Optional });

            routes.MapRoute(
             "PatientByResponsibleEmployee",
             "Report/Patient/ResponsibleEmployee",
             new { controller = this.Name, action = "PatientByResponsibleEmployeeListing", id = UrlParameter.Optional });

            routes.MapRoute(
            "PatientByResponsibleCaseManager",
            "Report/Patient/ResponsibleCaseManager",
            new { controller = this.Name, action = "PatientByResponsibleCaseManagerListing", id = UrlParameter.Optional });


            routes.MapRoute(
            "ReportStatisticalCensusByPrimaryInsurance",
            "Report/Statistical/CensusByPrimaryInsurance",
            new { controller = this.Name, action = "StatisticalCensusByPrimaryInsurance", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportPatientExpiringAuthorizations",
               "Report/Patient/ExpiringAuthorizations",
               new { controller = this.Name, action = "PatientExpiringAuthorizations", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportPatientSurveyCensus",
              "Report/Patient/SurveyCensus",
              new { controller = this.Name, action = "PatientSurveyCensus", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportPatientVitalSigns",
            "Report/Patient/VitalSigns",
            new { controller = this.Name, action = "PatientVitalSigns", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportPatientSixtyDaySummary",
            "Report/Patient/SixtyDaySummary",
            new { controller = this.Name, action = "PatientSixtyDaySummary", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportPatientDischargePatients",
            "Report/Patient/DischargePatients",
            new { controller = this.Name, action = "DischargePatients", id = UrlParameter.Optional });

            
            #endregion

            #region Clinical

            routes.MapRoute(
               "ReportClinicalOpenOasis",
               "Report/Clinical/OpenOasis",
               new { controller = this.Name, action = "ClinicalOpenOasis", id = UrlParameter.Optional });
            routes.MapRoute(
               "ReportClinicalOrders",
               "Report/Clinical/Orders",
               new { controller = this.Name, action = "ClinicalOrders", id = UrlParameter.Optional });
            routes.MapRoute(
              "ReportClinicalMissedVisits",
              "Report/Clinical/MissedVisits",
              new { controller = this.Name, action = "ClinicalMissedVisit", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportClinicalPhysicianOrderHistory",
             "Report/Clinical/PhysicianOrderHistory",
             new { controller = this.Name, action = "ClinicalPhysicianOrderHistory", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportClinicalPlanOfCareHistory",
              "Report/Clinical/PlanOfCareHistory",
              new { controller = this.Name, action = "ClinicalPlanOfCareHistory", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportClinicalThirteenAndNineteenVisitException",
            "Report/Clinical/ThirteenAndNineteenVisitException",
            new { controller = this.Name, action = "ClinicalThirteenAndNineteenVisitException", id = UrlParameter.Optional });

            routes.MapRoute(
          "ReportClinicalThirteenTherapyReevaluationException",
          "Report/Clinical/ThirteenTherapyReevaluationException",
          new { controller = this.Name, action = "ClinicalThirteenTherapyReevaluationException", id = UrlParameter.Optional });

            routes.MapRoute(
          "ReportClinicalNineteenTherapyReevaluationException",
          "Report/Clinical/NineteenTherapyReevaluationException",
          new { controller = this.Name, action = "ClinicalNineteenTherapyReevaluationException", id = UrlParameter.Optional });


            #endregion

            #region Schedule

            routes.MapRoute(
             "ReportSchedulePatientWeekly",
             "Report/Schedule/PatientWeekly",
             new { controller = this.Name, action = "SchedulePatientWeeklySchedule", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportSchedulePatientMonthlySchedule",
            "Report/Schedule/PatientMonthlySchedule",
            new { controller = this.Name, action = "PatientMonthlyScheduleListing", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportScheduleEmployeeWeekly",
             "Report/Schedule/EmployeeWeekly",
             new { controller = this.Name, action = "ScheduleEmployeeWeekly", id = UrlParameter.Optional });

            routes.MapRoute(
               "ReportScheduleMonthlyWork",
               "Report/Schedule/MonthlySchedule",
               new { controller = this.Name, action = "ScheduleMonthlyWork", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportSchedulePastDueVisits",
              "Report/Schedule/PastDueVisits",
              new { controller = this.Name, action = "SchedulePastDueVisits", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportSchedulePastDueVisitsByDiscipline",
            "Report/Schedule/PastDueVisitsByDiscipline",
            new { controller = this.Name, action = "SchedulePastDueVisitsByDiscipline", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportScheduleDailyWork",
             "Report/Schedule/DailySchedule",
             new { controller = this.Name, action = "ScheduleDailyWork", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportScheduleCaseManagerTask",
            "Report/Schedule/CaseManagerTask",
            new { controller = this.Name, action = "ScheduleCaseManagerTask", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportScheduleDeviation",
            "Report/Schedule/Deviation",
            new { controller = this.Name, action = "ScheduleDeviation", id = UrlParameter.Optional });

            routes.MapRoute(
           "ReportSchedulePastDueRecet",
           "Report/Schedule/PastDueRecet",
           new { controller = this.Name, action = "SchedulePastDueRecet", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportScheduleUpcomingRecet",
            "Report/Schedule/UpcomingRecet",
            new { controller = this.Name, action = "ScheduleUpcomingRecet", id = UrlParameter.Optional });



            #endregion

            #region Billing

            routes.MapRoute(
            "ReportSubmittedClaims",
            "Report/Billing/SubmittedClaims",
            new { controller = this.Name, action = "SubmittedClaims", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingOutstandingClaims",
             "Report/Billing/OutstandingClaims",
             new { controller = this.Name, action = "OutstandingClaims", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportBillingByStatusSummary",
              "Report/Billing/ByStatusSummary",
              new { controller = this.Name, action = "ClaimsByStatus", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingAccountsReceivable",
             "Report/Billing/AccountsReceivable",
             new { controller = this.Name, action = "AccountsReceivable", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingBillingBatch",
             "Report/Billing/BillingBatch",
             new { controller = this.Name, action = "BillingBatch", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportBillingAgedAccountsReceivable",
            "Report/Billing/AgedAccountsReceivable",
            new { controller = this.Name, action = "AgedAccountsReceivable", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingPPSRAPClaims",
             "Report/Billing/PPSRAPClaims",
             new { controller = this.Name, action = "PPSRAPClaims", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportBillingPPSFinalClaims",
             "Report/Billing/PPSFinalClaims",
             new { controller = this.Name, action = "PPSFinalClaims", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportBillingPotentialClaimAutoCancel",
            "Report/Billing/PotentialClaimAutoCancel",
            new { controller = this.Name, action = "PotentialClaimAutoCancel", id = UrlParameter.Optional });


            #endregion

            #region Employee

            routes.MapRoute(
               "ReportEmployeeRoster",
               "Report/Employee/Roster",
               new { controller = this.Name, action = "EmployeeRoster", id = UrlParameter.Optional });

            routes.MapRoute(
              "ReportEmployeeBirthdayList",
              "Report/Employee/Birthdays",
              new { controller = this.Name, action = "EmployeeBirthdayList", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportEmployeeLicenseListing",
             "Report/Employee/License",
             new { controller = this.Name, action = "EmployeeLicenseListing", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportEmployeeExpiringLicense",
            "Report/Employee/ExpiringLicense",
            new { controller = this.Name, action = "EmployeeExpiringLicense", id = UrlParameter.Optional });

            routes.MapRoute(
           "ReportEmployeeVisitByDateRange",
           "Report/Employee/VisitByDateRange",
           new { controller = this.Name, action = "EmployeeVisitByDateRange", id = UrlParameter.Optional });


            #endregion

            #region Statistical

            routes.MapRoute(
             "ReportStatisticalPatientVisitHistory",
             "Report/Statistical/PatientVisits",
             new { controller = this.Name, action = "StatisticalPatientVisitHistory", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportStatisticalEmployeeVisitHistory",
            "Report/Statistical/EmployeeVisits",
            new { controller = this.Name, action = "StatisticalEmployeeVisitHistory", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportStatisticalMonthlyAdmission",
             "Report/Statistical/MonthlyAdmission",
             new { controller = this.Name, action = "StatisticalMonthlyAdmissionListing", id = UrlParameter.Optional });

            routes.MapRoute(
             "ReportStatisticalAnnualAdmission",
             "Report/Statistical/AnnualAdmission",
             new { controller = this.Name, action = "StatisticalAnnualAdmissionListing", id = UrlParameter.Optional });

            routes.MapRoute(
            "ReportStatisticalUnduplicatedCensusReport",
            "Report/Statistical/UnduplicatedCensusReport",
            new { controller = this.Name, action = "StatisticalUnduplicatedCensusReportListing", id = UrlParameter.Optional });

            #endregion

        }
    }
}
