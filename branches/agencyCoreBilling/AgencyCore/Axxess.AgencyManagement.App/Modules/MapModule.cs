﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class MapModule : Module
    {
        public override string Name
        {
            get { return "Map"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
              "GoogleMap",
              "Map/Google/{patientId}",
              new { controller = this.Name, action = "GoogleMap", patientId = new IsGuid() }
            );
        }
    }
}
