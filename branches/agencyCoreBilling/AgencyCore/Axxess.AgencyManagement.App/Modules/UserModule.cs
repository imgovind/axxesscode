﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class UserModule : Module
    {
        public override string Name
        {
            get { return "User"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
               "EditProfile",
               "Profile/Edit",
               new { controller = this.Name, action = "Profile", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "ForgotSignature",
               "Signature/Forgot",
               new { controller = this.Name, action = "ForgotSignature", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "EmailSignature",
               "Signature/Email",
               new { controller = this.Name, action = "EmailSignature", id = UrlParameter.Optional }
            );
        }
    }
}
