﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class ScheduleModule : Module
    {
        public override string Name
        {
            get { return "Schedule"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "MissedVisit",
                "Visit/Miss",
                new { controller = this.Name, action = "MissedVisit", id = UrlParameter.Optional });

            routes.MapRoute(
                "MissedVisitBlank",
                "MissedVisit/View/Blank",
                new { controller = this.Name, action = "MissedVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "MissedVisitPrint",
                "MissedVisit/View/{patientId}/{eventId}",
                new { controller = this.Name, action = "MissedVisitPrint", patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "HHACarePlanBlank",
                "HHACarePlan/View/Blank",
                new { controller = this.Name, action = "HHACarePlanBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "HHACarePlanPrint",
                "HHACarePlan/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "HHACarePlanPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "TransferSummaryBlank",
                "TransferSummary/View/Blank",
                new { controller = this.Name, action = "TransferSummaryBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "TransferSummaryPrint",
                "TransferSummary/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "TransferSummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "HHAVisitBlank",
                "HHAVisit/View/Blank",
                new { controller = this.Name, action = "HHAVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "HHAVisitPrint",
                "HHAVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "HHAVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PASVisitNoteBlank",
                "PASVisitNote/View/Blank",
                new { controller = this.Name, action = "PASVisitNoteBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "PASVisitNotePrint",
                "PASVisitNote/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PASVisitNotePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PASCarePlanBlank",
                "PASCarePlan/View/Blank",
                new { controller = this.Name, action = "PASCarePlanBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "PASCarePlanPrint",
                "PASCarePlan/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PASCarePlanPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "SixtyDaySummaryBlank",
                "SixtyDaySummary/View/Blank",
                new { controller = this.Name, action = "SixtyDaySummaryBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "SixtyDaySummaryPrint",
                "SixtyDaySummary/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SixtyDaySummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "LVNSVisitBlank",
                "LVNSupervisoryVisit/View/Blank",
                new { controller = this.Name, action = "LVNSVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "LVNSVisitPrint",
                "LVNSupervisoryVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "LVNSVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "HHASVisitBlank",
                "HHAideSupervisoryVisit/View/Blank",
                new { controller = this.Name, action = "HHASVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "HHASVisitPrint",
                "HHAideSupervisoryVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "HHASVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "DischargeSummaryBlank",
                "DischargeSummary/View/Blank",
                new { controller = this.Name, action = "DischargeSummaryBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "DischargeSummaryPrint",
                "DischargeSummary/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "DischargeSummaryPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "SNVisitBlank",
                "SNVisit/View/Blank",
                new { controller = this.Name, action = "SNVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "SNVisitPrint",
                "SNVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SNVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTVisitBlank",
                "PTVisit/View/Blank",
                new { controller = this.Name, action = "PTVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "PTVisitPrint",
                "PTVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "PTEvaluationBlank",
               "PTEvaluation/View/Blank",
               new { controller = this.Name, action = "PTEvaluationBlank", id = UrlParameter.Optional });

            routes.MapRoute(
               "PTEvaluationPrint",
               "PTEvaluation/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "PTEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "PTDischargeBlank",
                "PTDischarge/View/Blank",
                new { controller = this.Name, action = "PTDischargeBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "PTDischargePrint",
                "PTDischarge/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PTDischargePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "OTVisitBlank",
                "OTVisit/View/Blank",
                new { controller = this.Name, action = "OTVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "OTVisitPrint",
                "OTVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OTVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "OTEvaluationBlank",
               "OTEvaluation/View/Blank",
               new { controller = this.Name, action = "OTEvaluationBlank", id = UrlParameter.Optional });

            routes.MapRoute(
               "OTEvaluationPrint",
               "OTEvaluation/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "OTEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "STVisitBlank",
                "STVisit/View/Blank",
                new { controller = this.Name, action = "STVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "STVisitPrint",
                "STVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "STVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "STEvaluationBlank",
               "STEvaluation/View/Blank",
               new { controller = this.Name, action = "STEvaluationBlank", id = UrlParameter.Optional });

            routes.MapRoute(
               "STEvaluationPrint",
               "STEvaluation/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "STEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "STDischargePrint",
               "STDischarge/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "STDischargePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "MSWVisitBlank",
                "MSWVisit/View/Blank",
                new { controller = this.Name, action = "MSWVisitBlank", id = UrlParameter.Optional });

            routes.MapRoute(
                "MSWVisitPrint",
                "MSWVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "MSWVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "MSWEvaluationBlank",
               "MSWEvaluation/View/Blank",
               new { controller = this.Name, action = "MSWEvaluationBlank", id = UrlParameter.Optional });

            routes.MapRoute(
               "MSWEvaluationPrint",
               "MSWEvaluation/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "MSWEvaluationPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "MSWProgressNoteBlank",
               "MSWProgressNote/View/Blank",
               new { controller = this.Name, action = "MSWProgressNoteBlank", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "MSWProgressNotePrint",
               "MSWProgressNote/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "MSWProgressNotePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
               "TransportationNotePrint",
               "TransportationNote/View/{episodeId}/{patientId}/{eventId}",
               new { controller = this.Name, action = "TransportationNotePrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });

            routes.MapRoute(
                "SNDiabeticDailyVisitPrint",
                "SNDiabeticDailyVisit/View/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "SNDiabeticDailyVisitPrint", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
        
        }
    }
}
