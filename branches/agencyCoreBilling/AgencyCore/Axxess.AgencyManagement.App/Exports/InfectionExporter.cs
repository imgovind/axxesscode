﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

   public class InfectionExporter: BaseExporter
    {
       private IList<Infection> infections;
       public InfectionExporter(IList<Infection> infections)
            : base()
        {
            this.infections = infections;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Infections";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Infections");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Infections");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Physician");
            headerRow.CreateCell(2).SetCellValue("Treatment/Antibiotic");
            headerRow.CreateCell(3).SetCellValue("Type Of Incident");
            headerRow.CreateCell(4).SetCellValue("MD Notified");
            headerRow.CreateCell(5).SetCellValue("Incident Date");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.infections.Count > 0)
            {
                int i = 2;
                this.infections.ForEach(incident =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(incident.PatientName);
                    dataRow.CreateCell(1).SetCellValue(incident.PhysicianName);
                    dataRow.CreateCell(2).SetCellValue(incident.Treatment);
                    dataRow.CreateCell(3).SetCellValue(incident.InfectionType);
                    dataRow.CreateCell(4).SetCellValue(incident.MDNotified);
                    dataRow.CreateCell(5).SetCellValue(incident.InfectionDateFormatted);
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Infections: {0}", infections.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
           
        }
    }
}
