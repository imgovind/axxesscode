﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    using Kent.Boogaart.KBCsv;

    public class CahpsExporter : BaseExporter
    {
        #region Private Members and Constructor

        private int sampleYear;
        private int sampleMonth;
        private List<int> paymentSources;
        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public CahpsExporter(int sampleMonth, int sampleYear, List<int> paymentSources) : base()
        {
            this.sampleYear = sampleYear;
            this.sampleMonth = sampleMonth;
            this.paymentSources = paymentSources;

            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                switch (agency.CahpsVendor)
                {
                    case (int)CahpsVendors.DSSResearch: // DSS Research
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("{0}{1}{2}.csv", this.sampleYear, this.sampleMonth.ToString().PadLeft(2, '0'), agency.MedicareProviderNumber);
                        break;
                    case (int)CahpsVendors.ArkansasFoundationMedicalCare: // Arkansas
                        break;
                    case (int)CahpsVendors.Novaetus: // Novaetus
                        this.FormatType = ExportFormatType.XLS;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.xls", agency.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.Deyta: // Deyta
                    case (int)CahpsVendors.FieldsResearch: // Fields Research
                        this.FormatType = ExportFormatType.XLS;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.xls", agency.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.Pinnacle: // Pinnacle
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.csv", agency.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.PressGaney: // PressGaney
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.csv", agency.CahpsVendorId, this.sampleMonth.ToString().PadLeft(2, '0'));
                        break;
                    case (int)CahpsVendors.Ocs: // Ocs
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_Version1.2_{1}.csv", agency.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.Fazzi: // Fazzi
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.csv", agency.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    case (int)CahpsVendors.Shp: // Strategic Healthcare
                        this.FormatType = ExportFormatType.CSV;
                        this.FileName = string.Format("HHCAHPS_{0}_{1}.csv", agency.MedicareProviderNumber, DateTime.Now.ToString("MMddyyyy"));
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        #region Excel CAHPS

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - CAHPS Survey";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("CAHPS");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 13;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            List<Dictionary<string, string>> data = null;
            if (paymentSources != null && paymentSources.Count > 0)
            {
                data = reportAgent.CahpsExport(Current.AgencyId, sampleMonth, sampleYear, paymentSources);
            }
            else
            {
                data = reportAgent.CahpsExport(Current.AgencyId, sampleMonth, sampleYear);
            }

            if (data != null && data.Count > 0)
            {
                var rowIndex = 0;
                var colIndex = 0;
                var columnSize = 0;
                var dictionary = data.FirstOrDefault();

                Row headerRow = sheet.CreateRow(rowIndex);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    headerRow.CreateCell(colIndex).SetCellValue(kvp.Key);
                    colIndex++;
                }
                headerRow.RowStyle = headerStyle;

                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;

                data.ForEach(list =>
                {
                    Row dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        dataRow.CreateCell(colIndex).SetCellValue(kvp.Value.Trim());
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });

                int resizeColCounter = 0;
                do
                {
                    sheet.AutoSizeColumn(resizeColCounter);
                    resizeColCounter++;
                }
                while (resizeColCounter < columnSize);
            }
            else
            {
                Row errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No CAHPS Data found!");
            }
        }

        #endregion

        #region CSV CAHPS

        protected override void WriteCommaSeparatedFile()
        {
            List<Dictionary<string, string>> data = null;
            if (paymentSources != null && paymentSources.Count > 0)
            {
                data = reportAgent.CahpsExport(Current.AgencyId, sampleMonth, sampleYear, paymentSources);
            }
            else
            {
                data = reportAgent.CahpsExport(Current.AgencyId, sampleMonth, sampleYear);
            }

            if (data != null && data.Count > 0)
            {
                var count = 0;
                var dictionary = data.FirstOrDefault();

                var headerRecord = new string[dictionary.Count];
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    if (kvp.Key.IsNotNullOrEmpty())
                    {
                        headerRecord[count] = kvp.Key;
                        count++;
                    }
                }
                this.csvWriter.WriteHeaderRecord(headerRecord);

                count = 0;

                data.ForEach(list =>
                {
                    var dataRecord = new string[list.Count];
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        if (kvp.Key.IsNotNullOrEmpty())
                        {
                            dataRecord[count] = kvp.Value.Trim();
                            count++;
                        }
                    }
                    this.csvWriter.WriteDataRecord(dataRecord);
                    count = 0;
                });
            }
            else
            {
                this.csvWriter.WriteHeaderRecord("No CAHPS Data found!");
            }
        }

        #endregion

    }
}
