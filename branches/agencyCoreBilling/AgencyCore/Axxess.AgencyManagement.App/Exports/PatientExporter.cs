﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class PatientExporter : BaseExporter
    {
        private IList<Patient> patients;
        public PatientExporter(IList<Patient> patients)
            : base()
        {
            this.patients = patients;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patients";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Patients");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patients");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Id");
            headerRow.CreateCell(1).SetCellValue("Medicare Number");
            headerRow.CreateCell(2).SetCellValue("Last Name");
            headerRow.CreateCell(3).SetCellValue("First Name");
            headerRow.CreateCell(4).SetCellValue("Middle Initial");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            headerRow.CreateCell(6).SetCellValue("Mobile Phone");
            headerRow.CreateCell(7).SetCellValue("Address 1");
            headerRow.CreateCell(8).SetCellValue("Address 2");
            headerRow.CreateCell(9).SetCellValue("City");
            headerRow.CreateCell(10).SetCellValue("State");
            headerRow.CreateCell(11).SetCellValue("Zip Code");
            headerRow.CreateCell(12).SetCellValue("Date of Birth");
            headerRow.CreateCell(13).SetCellValue("Gender");
            headerRow.CreateCell(14).SetCellValue("Email Address");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.patients.Count > 0)
            {
                int i = 2;
                this.patients.ForEach(p =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(p.PatientIdNumber);
                    dataRow.CreateCell(1).SetCellValue(p.MedicareNumber);
                    dataRow.CreateCell(2).SetCellValue(p.LastName.ToTitleCase());
                    dataRow.CreateCell(3).SetCellValue(p.FirstName.ToTitleCase());
                    dataRow.CreateCell(4).SetCellValue(p.MiddleInitial.ToTitleCase());
                    dataRow.CreateCell(5).SetCellValue(p.PhoneHome.ToPhone());
                    dataRow.CreateCell(6).SetCellValue(p.PhoneMobile.ToPhone());
                    dataRow.CreateCell(7).SetCellValue(p.AddressLine1.ToTitleCase());
                    dataRow.CreateCell(8).SetCellValue(p.AddressLine2.ToTitleCase());
                    dataRow.CreateCell(9).SetCellValue(p.AddressCity.ToTitleCase());
                    dataRow.CreateCell(10).SetCellValue(p.AddressStateCode);
                    dataRow.CreateCell(11).SetCellValue(p.AddressZipCode);
                    dataRow.CreateCell(12).SetCellValue(p.DOB.ToShortDateString());
                    dataRow.CreateCell(13).SetCellValue(p.Gender);
                    dataRow.CreateCell(14).SetCellValue(p.EmailAddress);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patients: {0}", patients.Count));
            }

            int columnCounter = 0;
            do
            {
                sheet.AutoSizeColumn(columnCounter);
                columnCounter++;
            }
            while (columnCounter < 15);
        }
    }
}
