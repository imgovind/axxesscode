﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;
  
   public class PatientOrderExport :BaseExporter
    {
        private IList<Order> orders;
        private DateTime StartDate;
        private DateTime EndDate;
        public PatientOrderExport(IList<Order> orders, DateTime StartDate, DateTime EndDate)
            : base()
        {
            this.orders = orders;
            this.StartDate = StartDate;
            this.EndDate = EndDate;

        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Orders";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Orders");
            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Orders");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Order Number");
            headerRow.CreateCell(1).SetCellValue("Type");
            headerRow.CreateCell(2).SetCellValue("Status");
            headerRow.CreateCell(3).SetCellValue("Physician");
            headerRow.CreateCell(4).SetCellValue("Order Date");
            headerRow.CreateCell(5).SetCellValue("Sent Date");
            headerRow.CreateCell(6).SetCellValue("Received Date");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.orders.Count > 0)
            {
                int i = 2;
                this.orders.ForEach(order =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(order.Number);
                    row.CreateCell(1).SetCellValue(order.Text);
                    row.CreateCell(2).SetCellValue(order.StatusName);
                    row.CreateCell(3).SetCellValue(order.PhysicianName);
                    row.CreateCell(4).SetCellValue(order.CreatedDate);
                    row.CreateCell(5).SetCellValue(order.SendDateFormatted);
                    row.CreateCell(6).SetCellValue(order.ReceivedDateFormatted);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Orders: {0}", orders.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            
        }
    }
}
