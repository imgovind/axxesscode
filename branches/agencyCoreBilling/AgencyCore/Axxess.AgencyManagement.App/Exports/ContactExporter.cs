﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class ContactExporter : BaseExporter
    {
        private IList<AgencyContact> contacts;
        public ContactExporter(IList<AgencyContact> contacts)
            : base()
        {
            this.contacts = contacts;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Contacts";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Contacts");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.GetCell(0).CellStyle = headerStyle;

            titleRow.CreateCell(1).SetCellValue("Agency Contacts");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Last Name");
            headerRow.CreateCell(1).SetCellValue("First Name");
            headerRow.CreateCell(2).SetCellValue("Type");
            headerRow.CreateCell(3).SetCellValue("Company");
            headerRow.CreateCell(4).SetCellValue("Address 1");
            headerRow.CreateCell(5).SetCellValue("Address 2");
            headerRow.CreateCell(6).SetCellValue("City");
            headerRow.CreateCell(7).SetCellValue("State");
            headerRow.CreateCell(8).SetCellValue("Zip Code");
            headerRow.CreateCell(9).SetCellValue("Primary Phone");
            headerRow.CreateCell(10).SetCellValue("Alternate Phone");
            headerRow.CreateCell(11).SetCellValue("Email Address");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.contacts.Count > 0)
            {
                int i = 2;
                this.contacts.ForEach(c =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(c.LastName);
                    dataRow.CreateCell(1).SetCellValue(c.FirstName);
                    dataRow.CreateCell(2).SetCellValue(c.ContactType);
                    dataRow.CreateCell(3).SetCellValue(c.CompanyName);
                    dataRow.CreateCell(4).SetCellValue(c.AddressLine1);
                    dataRow.CreateCell(5).SetCellValue(c.AddressLine2);
                    dataRow.CreateCell(6).SetCellValue(c.AddressCity);
                    dataRow.CreateCell(7).SetCellValue(c.AddressStateCode);
                    dataRow.CreateCell(8).SetCellValue(c.AddressZipCode);
                    dataRow.CreateCell(9).SetCellValue(c.PhonePrimaryFormatted);
                    dataRow.CreateCell(10).SetCellValue(c.PhoneAlternate.ToPhone());
                    dataRow.CreateCell(11).SetCellValue(c.EmailAddress);
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Agency Contacts: {0}", contacts.Count));
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
        }
    }
}
