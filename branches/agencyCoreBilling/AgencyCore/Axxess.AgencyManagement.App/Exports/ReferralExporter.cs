﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class ReferralExporter : BaseExporter
    {
        private IList<Referral> referrals;
        public ReferralExporter(IList<Referral> referrals)
            : base()
        {
            this.referrals = referrals;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Referrals";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Referrals");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Referrals");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Referral Date");
            headerRow.CreateCell(1).SetCellValue("Referral Source");
            headerRow.CreateCell(2).SetCellValue("Medicare Number");
            headerRow.CreateCell(3).SetCellValue("Last Name");
            headerRow.CreateCell(4).SetCellValue("First Name");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            headerRow.CreateCell(6).SetCellValue("Address 1");
            headerRow.CreateCell(7).SetCellValue("Address 2");
            headerRow.CreateCell(8).SetCellValue("City");
            headerRow.CreateCell(9).SetCellValue("State");
            headerRow.CreateCell(10).SetCellValue("Zip Code");
            headerRow.CreateCell(11).SetCellValue("Date of Birth");
            headerRow.CreateCell(12).SetCellValue("Gender");
            headerRow.CreateCell(13).SetCellValue("Email Address");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.referrals.Count > 0)
            {
                int i = 1;
                this.referrals.ForEach(r =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(r.ReferralDate.ToShortDateString());
                    dataRow.CreateCell(1).SetCellValue(r.AdmissionSource.ToSourceName());
                    dataRow.CreateCell(2).SetCellValue(r.MedicareNumber);
                    dataRow.CreateCell(3).SetCellValue(r.LastName.ToTitleCase());
                    dataRow.CreateCell(4).SetCellValue(r.FirstName.ToTitleCase());
                    dataRow.CreateCell(5).SetCellValue(r.PhoneHome.ToPhone());
                    dataRow.CreateCell(6).SetCellValue(r.AddressLine1.ToTitleCase());
                    dataRow.CreateCell(7).SetCellValue(r.AddressLine2.ToTitleCase());
                    dataRow.CreateCell(8).SetCellValue(r.AddressCity.ToTitleCase());
                    dataRow.CreateCell(9).SetCellValue(r.AddressStateCode);
                    dataRow.CreateCell(10).SetCellValue(r.AddressZipCode);
                    dataRow.CreateCell(11).SetCellValue(r.DOB.ToShortDateString());
                    dataRow.CreateCell(12).SetCellValue(r.Gender);
                    dataRow.CreateCell(13).SetCellValue(r.EmailAddress);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Referrals: {0}", referrals.Count));
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
            sheet.AutoSizeColumn(12);
            sheet.AutoSizeColumn(13);
        }
    }
}
