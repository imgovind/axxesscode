<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var BehaviorInterventions = data.AnswerArray("485IBehaviorInterventions"); %>
<%  var BehaviorGoals = data.AnswerArray("485BehaviorGoals"); %>
<%  if (BehaviorInterventions.Length > 0 || (data.ContainsKey("485BehaviorComments") && data["485BehaviorComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (BehaviorInterventions.Contains("1")) { %>
    printview.span("SN to notify physicain this pateint was screened for depression using PHQ-2 scale and meets criteria for further evaluation for depression.") +
    <%  } %>
    <%  if (BehaviorInterventions.Contains("2")) { %>
    printview.span("SN to perform a neurological assessment each visit.") +
    <%  } %>
    <%  if (BehaviorInterventions.Contains("3")) { %>
    printview.span("SN to assess/instruct on seizure disorder signs &#38; symptoms and appropriate actions during seizure activity.") +
    <%  } %>
    <%  if (BehaviorInterventions.Contains("4")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructSeizurePrecautionPerson", "Patient/Caregiver")%> on seizure precautions.") +
    <%  } %>
    <%  if (BehaviorInterventions.Contains("5")) { %>
    printview.span("SN to instruct caregiver on orientation techniques to use when patient becomes disoriented.") +
    <%  } %>
    <%  if (BehaviorInterventions.Contains("7")) { %>
    printview.span("Notify SN or Physician that this patient was screened for depression using the PHQ-2 scale and meets criteria for further evaluation for depression.") +
    <%  } %>
    <%  if (BehaviorInterventions.Contains("8")) { %>
    printview.span("SN to evaluate patient for signs and symptoms of depression.") +
    <%  } %>
    <%  if (BehaviorInterventions.Contains("9")) { %>
    printview.span("MSW: <%= data.AnswerOrEmptyString("485MSWProviderServicesVisits").Equals("1") ? "1-2" : string.Empty%><%= data.AnswerOrEmptyString("485MSWProviderServicesVisits").Equals("0") ? data.AnswerOrDefault("485MSWProviderServicesVisitsAmount", "<span class='short blank'></span>") : "<span class='short blank'></span>"%> visits, every 60 days for provider services.") +
    <%  } %>
    <%  if (BehaviorInterventions.Contains("10")) { %>
    printview.span("MSW: <%= data.AnswerOrEmptyString("485MSWLongTermPlanningVisits").Equals("1") ? "1-2" : string.Empty%><%= data.AnswerOrEmptyString("485MSWLongTermPlanningVisits").Equals("0") ? data.AnswerOrDefault("485MSWLongTermPlanningVisitsAmount", "<span class='short blank'></span>") : "<span class='short blank'></span>"%> visits, every 60 days for long term planning.") +
    <%  } %>
    <%  if (BehaviorInterventions.Contains("6")) { %>
    printview.span("MSW: <%= data.AnswerOrEmptyString("485MSWCommunityAssistanceVisits").Equals("1") ? "1-2" : string.Empty %><%= data.AnswerOrEmptyString("485MSWCommunityAssistanceVisits").Equals("0") ? data.AnswerOrDefault("485MSWCommunityAssistanceVisitAmount", "<span class='short blank'></span>") : "<span class='short blank'></span>" %> visits, every 60 days for community resource assistance.") +
    <%  } %>
    <%  if (data.ContainsKey("485BehaviorComments") && data["485BehaviorComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485BehaviorComments").Clean()%>") +
    <%  } %>
    "","Behavioral Interventions");
<%  } %>
<%  if (BehaviorGoals.Length > 0 || (data.ContainsKey("485BehaviorGoalComments") && data["485BehaviorGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (BehaviorGoals.Contains("1")) { %>
    printview.span("Neuro status will be within normal limits and free of S#38;S of complications or further deterioration.") +
    <%  } %>
    <%  if (BehaviorGoals.Contains("2")) { %>
    printview.span("Patient will exhibit stable neuro status evidenced by increased alertness, mobility and self-care by the end of the episode.") +
    <%  } %>
    <%  if (BehaviorGoals.Contains("3")) { %>
    printview.span("Patient will exhibit stable neuro status evidence by absence of seizure activity by the end of the episode.") +
    <%  } %>
    <%  if (BehaviorGoals.Contains("4")) { %>
    printview.span("Patient will have optimal cognitive functioning within parameters established for the stage of disease by the end of the episode.") +
    <%  } %>
    <%  if (BehaviorGoals.Contains("5")) { %>
    printview.span("Patient will remain free from increased confusion during the episode.") +
    <%  } %>
    <%  if (BehaviorGoals.Contains("6")) { %>
    printview.span("The <%= data.AnswerOrDefault("485VerbalizeSeizurePrecautionsPerson", "Patient/Caregiver")%> will verbalize understanding of seizure precautions.") +
    <%  } %>
    <%  if (BehaviorGoals.Contains("7")) { %>
    printview.span("Caregiver will verbalize understanding of proper orientation techniques to use when patient becomes disoriented.") +
    <%  } %>
    <%  if (BehaviorGoals.Contains("8")) { %>
    printview.span("Patient&#8217;s community resource needs will be met with assistance of social worker.") +
    <%  } %>
    <%  if (data.ContainsKey("485BehaviorGoalComments") && data["485BehaviorGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485BehaviorGoalComments").Clean()%>") +
    <%  } %>
    "","Behavioral Goals");
<%  } %>