<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var TherapyInterventions = data.AnswerArray("485TherapyInterventions"); %>
<%  if (TherapyInterventions.Length > 0 || (data.ContainsKey("485TherapyComments") && data["485TherapyComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (TherapyInterventions.Contains("1")) { %>
    printview.span("Physical therapist to evaluate and submit plan of treatment.") +
    <%  } %>
    <%  if (TherapyInterventions.Contains("2")) { %>
    printview.span("Occupational therapist to evaluate and submit plan of treatment.") +
    <%  } %>
    <%  if (TherapyInterventions.Contains("3")) { %>
    printview.span("Speech therapist to evaluate and submit plan of treatment.") +
    <%  } %>
    <%  if (data.ContainsKey("485TherapyComments") && data["485TherapyComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485TherapyComments").Clean()%>") +
    <%  } %>
    "","Therapy Need Interventions");
<%  } %>