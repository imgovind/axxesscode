<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var EndocrineInterventions = data.AnswerArray("485EndocrineInterventions"); %>
<%  var EndocrineGoals = data.AnswerArray("485EndocrineGoals"); %>
<%  if (EndocrineInterventions.Length > 0 || (data.ContainsKey("485EndocrineInterventionComments") && data["485EndocrineInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (EndocrineInterventions.Contains("1")) { %>
    printview.span("SN to assess <%= data.AnswerOrDefault("485AbilityToManageDiabetic","<span class='blank'></span>").Clean() %> ability to manage diabetic disease process.") +
    <%  } %>
    <%  if (EndocrineInterventions.Contains("2")) { %>
    printview.span("SN to perform BG checks every visit &#38; PRN.") +
    <%  } %>
    <%  if (EndocrineInterventions.Contains("3")) { %>
    printview.span("SN to instruct on insulin preparation, administration, site rotation and disposal of supplies.") +
    <%  } %>
    <%  if (EndocrineInterventions.Contains("4")) { %>
    printview.span("SN to assess/instruct on diabetic management to include: nail, skin &#38; foot care, medication administration and proper diet.") +
    <%  } %>
    <%  if (EndocrineInterventions.Contains("5")) { %>
    printview.span("SN to administer <%= data.AnswerOrDefault("485AmountOfInsuline", "<span class='blank'></span>").Clean() %> insulin as follows: <%= data.AnswerOrDefault("485AmountOfInsulineAsFollows", "<span class='blank'></span>").Clean()%>.") +
    <%  } %>
    <%  if (EndocrineInterventions.Contains("6")) { %>
    printview.span("SN to prefill syringes with <%= data.AnswerOrDefault("485PrefillOfInsuline", "<span class='blank'></span>").Clean()%> insulin as follows: <%= data.AnswerOrDefault("485PrefillOfInsulineAsFollows", "<span class='blank'></span>").Clean()%>.") +
    <%  } %>
    <%  if (data.ContainsKey("485EndocrineInterventionComments") && data["485EndocrineInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485EndocrineInterventionComments").Clean()%>",false,2) +
    <%  } %>
    "","Endocrine Interventions");
<%  } %>
<%  if (EndocrineGoals.Length > 0 || (data.ContainsKey("485EndocrineGoalComments") && data["485EndocrineGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (EndocrineGoals.Contains("1")) { %>
    printview.span("Patient will be free from signs and symptoms of hypo/hyperglycemia during the episode.") +
    <%  } %>
    <%  if (EndocrineGoals.Contains("2")) { %>
    printview.span("<%= data.AnswerOrDefault("485GlucometerUseIndependencePerson", "Patient/Caregiver").Clean()%> will be independent with glucometer use by the end of the episode.") +
    <%  } %>
    <%  if (EndocrineGoals.Contains("3")) { %>
    printview.span("<%= data.AnswerOrDefault("485IndependentInsulinAdministrationPerson", "Patient/Caregiver").Clean()%> will be independent with insulin administration by the end of the episode.") +
    <%  } %>
    <%  if (EndocrineGoals.Contains("4")) { %>
    printview.span("<%= data.AnswerOrDefault("485VerbalizeProperFootCarePerson", "Patient/Caregiver").Clean()%> will verbalize understanding of proper diabetic foot care by the end of the episode.") +
    <%  } %>
    <%  if (EndocrineGoals.Contains("5")) { %>
    printview.span("<%= data.AnswerOrDefault("485DiabetesManagement", "<span class='blank'></span>").Clean()%> will verbalize knowledge of diabetes management, S&#38;S of complications, hypo/hyperglycemia, foot care and management during illness or stress by the end of the episode.") +
    <%  } %>
    <%  if (EndocrineGoals.Contains("6")) { %>
    printview.span("<%= data.AnswerOrDefault("485AmountOfInsuline", "<span class='blank'></span>").Clean() %> will demonstrate correct insulin preparation, injection procedure, storage and disposal of supplies by the end of the episode.") +
    <%  } %>
    <%  if (EndocrineGoals.Contains("7")) { %>
    printview.span("<%= data.AnswerOrDefault("485GeneralVerbalization","<span class='blank'></span>").Clean() %> will verbalize understanding of the importance of keeping blood glucose levels within parameters by the end of the episode.") +
    <%  } %>
    <%  if (data.ContainsKey("485EndocrineGoalComments") && data["485EndocrineGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485EndocrineGoalComments").Clean()%>",false,2) +
    <%  } %>
    "","Endocrine Goals");
<%  } %>