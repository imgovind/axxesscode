<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var InstructInterventions = data.AnswerArray("485InstructInterventions"); %>
<%  var InstructGoals = data.AnswerArray("485InstructGoals"); %>
<%  if (InstructInterventions.Length > 0 || (data.ContainsKey("485IADLComments") && data["485IADLComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (InstructInterventions.Contains("1")) { %>
    printview.span("SN to instruct patient to wear proper footwear when ambulating.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("2")) { %>
    printview.span("SN to instruct patient to use prescribed assistive device when ambulating.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("3")) { %>
    printview.span("SN to instruct patient to change positions slowly.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("4")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructRemoveClutterPerson","Patient/Caregiver").Clean() %> to remove clutter from patient&#8217;s path such as clothes, books, shoes, electrical cords, or other items that may cause patient to trip.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("5")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructContactForFallPerson","Patient/Caregiver").Clean() %> to contact agency to report any fall with or without minor injury and to call 911 for fall resulting in serious injury or causing severe pain or immobility.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("6")) { %>
    printview.span("HHA to assist with ADL&#8217;s &#38; IADL&#8217;s per HHA care plan.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("7")) { %>
    printview.span("Therapist to instruct patient to wear proper footwear when ambulating.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("8")) { %>
    printview.span("Therapist to instruct patient to use prescribed assistive device when ambulating.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("9")) { %>
    printview.span("Therapist to instruct patient to change positions slowly.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("10")) { %>
    printview.span("Therapist to instruct the <%= data.AnswerOrDefault("485InstructRemoveRugsPerson", "Patient/Caregiver").Clean()%> to remove throw rugs or use double-sided tape to secure rug in place.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("11")) { %>
    printview.span("Therapist to instruct the <%= data.AnswerOrDefault("485InstructContactForFallPerson", "Patient/Caregiver").Clean()%> to remove clutter from patient&#8217;s path such as clothes, books, shoes, electrical cords, or other items that may cause patient to trip.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("12")) { %>
    printview.span("Therapist to instruct the <%= data.AnswerOrDefault("485InstructContactAgencyPerson", "Patient/Caregiver").Clean()%> to contact agency for increased dizziness or problems with balance.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("13")) { %>
    printview.span("Therapist to instruct the patient to use non-skid mats in tub/shower.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("12")) { %>
    printview.span("Therapist to instruct the <%= data.AnswerOrDefault("485InstructAdequateLightingPerson", "Patient/Caregiver").Clean()%> on importance of adequate lighting in patient area.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("13")) { %>
    printview.span("Therapist to instruct the <%= data.AnswerOrDefault("485InstructContactForFallTherapyPerson", "Patient/Caregiver").Clean()%> to contact agency to report any fall with or without minor injury and to call 911 for fall resulting in serious injury or causing severe pain or immobility.") +
    <%  } %>
    <%  if (InstructInterventions.Contains("14")) { %>
    printview.span("Therapist to request Physical Therapy Evaluation order from physician.") +
    <%  } %>
    <%  if (data.ContainsKey("485IADLComments") && data["485IADLComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485IADLComments").Clean() %>")
    <%  } %>
    "","IADL Interventions");
<%  } %>
<%  if (InstructGoals.Length > 0 || (data.ContainsKey("485IADLGoalComments") && data["485IADLGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (InstructGoals.Contains("1")) { %>
    printview.span("The patient will be free from falls during the episode.") +
    <%  } %>
    <%  if (InstructGoals.Contains("2")) { %>
    printview.span("The patient will be free from injury during the episode.") +
    <%  } %>
    <%  if (InstructGoals.Contains("3")) { %>
    printview.span("The <%= data.AnswerOrDefault("485InstructRemoveClutterGoalPerson", "Patient/Caregiver")%> will remove all clutter from patient&#8217;s path, such as clothes, books, shoes, electrical cords, and other items, that may cause patient to trip by <%= data.AnswerOrDefault("485InstructRemoveClutterGoalDate", "<span class='blank'></span>'")%>.") +
    <%  } %>
    <%  if (InstructGoals.Contains("4")) { %>
    printview.span("The <%= data.AnswerOrDefault("485InstructRemoveRugsGoalPerson", "Patient/Caregiver")%> will remove throw rugs or secure them with double-sided tape by <%= data.AnswerOrDefault("485InstructRemoveRugsGoalDate", "<span class='blank'></span>'")%>.") +
    <%  } %>
    <%  if (data.ContainsKey("485IADLGoalComments") && data["485IADLGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485IADLGoalComments").Clean() %>") +
    <%  } %>
    "","IADL Goals");
<%  } %>