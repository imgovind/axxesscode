<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var RespiratoryInterventions = data.AnswerArray("485RespiratoryInterventions"); %>
<%  var RespiratoryGoals = data.AnswerArray("485RespiratoryGoals"); %>
<%  if (RespiratoryInterventions.Length > 0 || (data.ContainsKey("485RespiratoryInterventionComments") && data["485RespiratoryInterventionComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (RespiratoryInterventions.Contains("1")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructNebulizerUsePerson", "Patient/Caregiver")%> on proper use of nebulizer/inhaler, and assess return demonstration. ") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("2")) { %>
    printview.span("SN to assess O2 saturation on room air (freq) <%= data.AnswerOrDefault("485AssessOxySaturationFrequency", "<span class='short blank'></span>")%>.") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("3")) { %>
    printview.span("SN to assess O2 saturation on O2 @<%= data.AnswerOrDefault("485AssessOxySatOnOxyAt", "<span class='short blank'></span>")%> LPM/<%= data.AnswerOrDefault("485AssessOxySatLPM", "<span class='short blank'></span>")%> (freq) <%= data.AnswerOrDefault("485AssessOxySatOnOxyFrequency", "<span class='short blank'></span>")%>.") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("4")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructSobFactorsPerson", "Patient/Caregiver")%> on factors that contribute to SOB. ") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("5")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485InstructAvoidSmokingPerson", "Patient/Caregiver")%> to avoid smoking or allowing people to smoke in patientís home. Instruct patient to avoid irritants/allergens known to increase SOB. ") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("6")) { %>
    printview.span("SN to instruct patient on energy conserving measures including frequent rest periods, small frequent meals, avoiding large meals/overeating, controlling stress.") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("7")) { %>
    printview.span("SN to instruct caregiver on proper suctioning technique.") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("8")) { %>
    printview.span("SN to instruct the <%= data.AnswerOrDefault("485RecognizePulmonaryDysfunctionPerson", "Patient/Caregiver") %> on methods to recognize pulmonary dysfunction and relieve complications. ") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("9")) { %>
    printview.span("Report to physician O<sub>2</sub> saturation less than <%= data.AnswerOrDefault("485OxySaturationLessThanPercent", "<span class='short blank'></span>")%>%.") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("10")) { %>
    printview.span("SN to assess/instruct on signs &#38; symptoms of pulmonary complications.") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("11")) { %>
    printview.span("SN to instruct on all aspects of trach care and equipment. SN to instruct PT/CG on emergency procedures for complications and dislodgment of tube.") +
    <%  } %>
    <%  if (RespiratoryInterventions.Contains("12")) { %>
    printview.span("SN to perform trach care using sterile technique (i.e. suctioning, dressing change).") +
    <%  } %>
    <%  if (data.ContainsKey("485RespiratoryInterventionComments") && data["485RespiratoryInterventionComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Orders:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485RespiratoryInterventionComments").Clean()%>")
    <%  } %>
    "","Respiratory Interventions");
<%  } %>
<%  if (RespiratoryGoals.Length > 0 || (data.ContainsKey("485RespiratoryGoalComments") && data["485RespiratoryGoalComments"].Answer.IsNotNullOrEmpty())) { %>
printview.addsection(
    <%  if (RespiratoryGoals.Contains("1")) { %>
    printview.span("Respiratory status will improve with reduced shortness of breath and improved lung sounds by the end of the episode.") +
    <%  } %>
    <%  if (RespiratoryGoals.Contains("2")) { %>
    printview.span("PT/CG will verbalize and demonstrate correct use and care of oxygen and equipment by the end of the episode.") +
    <%  } %>
    <%  if (RespiratoryGoals.Contains("3")) { %>
    printview.span("Patient will be free from signs and symptoms of respiratory distress during the episode.") +
    <%  } %>
    <%  if (RespiratoryGoals.Contains("4")) { %>
    printview.span("Patient and caregiver will verbalize an understanding of factors that contribute to shortness of breath by: <%= data.AnswerOrDefault("485VerbalizeFactorsSobDate", "<span class='blank'></span>")%>.") +
    <%  } %>
    <%  if (RespiratoryGoals.Contains("5")) { %>
    printview.span("Patient will verbalize an understanding of energy conserving measures by: <%= data.AnswerOrDefault("485VerbalizeEnergyConserveDate", "<span class='blank'></span>")%>.") +
    <%  } %>
    <%  if (RespiratoryGoals.Contains("6")) { %>
    printview.span("The <%= data.AnswerOrDefault("485VerbalizeSafeOxyManagementPerson", "Patient/Caregiver") %> will verbalize and demonstrate safe management of oxygen by: <%= data.AnswerOrDefault("485VerbalizeSafeOxyManagementPersonDate", "<span class='blank'></span>")%>.") +
    <%  } %>
    <%  if (RespiratoryGoals.Contains("7")) { %>
    printview.span("Patient will return demonstrate proper use of nebulizer treatment by <%= data.AnswerOrDefault("485DemonstrateNebulizerUseDate", "<span class='blank'></span>") %>.") +
    <%  } %>
    <%  if (data.ContainsKey("485RespiratoryGoalComments") && data["485RespiratoryGoalComments"].Answer.IsNotNullOrEmpty()) { %>
    printview.span("Additional Goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("485RespiratoryGoalComments").Clean()%>") +
    <%  } %>
    "","Respiratory Goals");
<%  } %>