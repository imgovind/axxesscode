﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        Model.TypeName,
        "OASIS-C Transfer, Not Discharge",
        (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "") + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "")) %>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#<%= Model.TypeName %>_Demographics" tooltip="M0010 &#8211; M0150">Clinical Record Items</a></li>
        <li><a href="#<%= Model.TypeName %>_RiskAssessment" tooltip="M1040 &#8211; M1055">Risk Assessment</a></li>
        <li><a href="#<%= Model.TypeName %>_Cardiac" tooltip="M1500 &#8211; M1510">Cardiac Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Medications" tooltip="M2004 &#8211; M2015">Medications</a></li>
        <li><a href="#<%= Model.TypeName %>_EmergentCare" tooltip="M2300 &#8211; M2310">Emergent Care</a></li>
        <li><a href="#<%= Model.TypeName %>_TransferDischargeDeath" tooltip="M0903 &#8211; M0906<br />M2400 &#8211; M2440">Transfer</a></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <div id="<%= Model.TypeName %>_RiskAssessment" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Cardiac" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Medications" class="general loading"></div>
    <div id="<%= Model.TypeName %>_EmergentCare" class="general loading"></div>
    <div id="<%= Model.TypeName %>_TransferDischargeDeath" class="general loading"></div>
</div>