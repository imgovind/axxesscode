﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        Model.TypeName,
        "Non-OASIS Discharge",
        (data.ContainsKey("M0040LastName") ? data["M0040LastName"].Answer : "") + (data.ContainsKey("M0040FirstName") ? ", " + data["M0040FirstName"].Answer : "")) %>
<div id="<%= Model.TypeName %>_Tabs" class="tabs vertical-tabs vertical-tabs-left OasisContainer">
    <ul class="verttab strong">
        <li><a href="#<%= Model.TypeName %>_Demographics">Clinical Record Items</a></li>
        <li><a href="#<%= Model.TypeName %>_RiskAssessment">Risk Assessment</a></li>
        <li><a href="#<%= Model.TypeName %>_Sensory">Sensory Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Pain">Pain</a></li>
        <li><a href="#<%= Model.TypeName %>_Integumentary">Integumentary Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Respiratory">Respiratory Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Cardiac">Cardiac Status</a></li>
        <li><a href="#<%= Model.TypeName %>_Elimination">Elimination Status</a></li>
        <li><a href="#<%= Model.TypeName %>_NeuroBehavioral">Neuro/Behaviourial Status</a></li>
        <li><a href="#<%= Model.TypeName %>_AdlIadl">ADL/IADLs</a></li>
        <li><a href="#<%= Model.TypeName %>_Medications">Medications</a></li>
        <li><a href="#<%= Model.TypeName %>_CareManagement">Care Management</a></li>
        <li><a href="#<%= Model.TypeName %>_EmergentCare">Emergent Care</a></li>
        <li><a href="#<%= Model.TypeName %>_TransferDischargeDeath">Discharge</a></li>
    </ul>
    <div id="<%= Model.TypeName %>_Demographics" class="general">
        <% Html.RenderPartial("~/Views/Oasis/Assessments/Tabs/Demographics.ascx", Model); %>
    </div>
    <div id="<%= Model.TypeName %>_RiskAssessment" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Sensory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Pain" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Integumentary" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Respiratory" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Cardiac" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Elimination" class="general loading"></div>
    <div id="<%= Model.TypeName %>_NeuroBehavioral" class="general loading"></div>
    <div id="<%= Model.TypeName %>_AdlIadl" class="general loading"></div>
    <div id="<%= Model.TypeName %>_Medications" class="general loading"></div>
    <div id="<%= Model.TypeName %>_CareManagement" class="general loading"></div>
    <div id="<%= Model.TypeName %>_EmergentCare" class="general loading"></div>
    <div id="<%= Model.TypeName %>_TransferDischargeDeath" class="general loading"></div>
</div>