<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "TherapyNeedForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "TherapyNeed")%> 
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
        <%  if (Model.AssessmentTypeNum == "05") { %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        <%  } else { %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <%  } %>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 5) { %>
        <ul class="float_right">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 6) { %>
    <fieldset class="oasis">
        <legend>Therapy Need</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M2200">
                <label for="<%= Model.TypeName %>_M2200NumberOfTherapyNeed" class="strong">
                    <a href="javascript:void(0)" title="More Information about M2200" class="green" onclick="Oasis.ToolTip('M2200')">(M2200)</a>
                    Therapy Need: In the home health plan of care for the Medicare payment episode for which this assessment will define a case mix group, what is the indicated need for therapy visits (total of reasonable and necessary physical, occupational, and speech-language pathology visits combined)?
                    <em>(Enter zero [&#8220;000&#8221;] if no therapy visits indicated)</em>
                </label>
                <div id="<%= Model.TypeName %>_M2200Right" class="float_right">
                    <label>Number of therapy visits indicated (total of physical, occupational and speech-language pathology combined).</label>
                    <%= Html.TextBox(Model.TypeName + "_M2200NumberOfTherapyNeed", data.AnswerOrEmptyString("M2200NumberOfTherapyNeed"), new { @id = Model.TypeName + "_M2200NumberOfTherapyNeed", @class = "vitals numeric", @maxlength = "3", @title = "(OASIS M2200) Therapy Need, Number of Visits" })%>
                </div>
                <div class="clear"></div>
                <div class="float_right oasis">
                    <%= Html.Hidden(Model.TypeName + "_M2200TherapyNeedNA", "", new { @id = Model.TypeName + "_M2200TherapyNeedNAHidden" })%>
                    <%= string.Format("<input title='(OASIS M2200) Therapy Need, No Case Mix Group' id='{0}_M2200TherapyNeedNA' class='radio' name='{0}_M2200TherapyNeedNA' value='1' type='checkbox' {1} onclick=\"$('#{0}_M2200NumberOfTherapyNeed').val('');\"/>",Model.TypeName, data.AnswerOrEmptyString("M2200TherapyNeedNA").Equals("1").ToChecked())%>
                    <label for="<%= Model.TypeName %>_M2200TherapyNeedNA">NA &#8211; Not Applicable: No case mix group defined by this assessment.</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2200')" title="More Information about M2200">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>Plan of Care Synopsis</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M2250">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M2250')" title="More Information about M2250">(M2250)</a>
                    Plan of Care Synopsis:
                    <em>(Check only one box in each row)</em><br />
                    Does the physician-ordered plan of care include the following:
                </label>
                <table class="form">
                    <thead class="align_center">
                        <tr>
                            <th colspan="3">Plan/Intervention</th>
                            <th class="fiexdwidthsamll align_center">0 &#8211; No</th>
                            <th class="fiexdwidthsamll align_center">1 &#8211; Yes</th>
                            <th colspan="3">NA &#8211; Not Applicable</th>
                        </tr>
                    </thead>
                    <tbody class="checkgroup align_left">
                        <tr>
                            <td colspan="3" class="strong">
                                <span class="float_left">a.</span>
                                <span class="radio">Patient-specific parameters for notifying physician of changes in vital signs or other clinical findings</span>
                                <%= Html.Hidden(Model.TypeName + "_M2250PatientParameters", "", new { @id = Model.TypeName + "_M2250PatientParametersHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250PatientParameters", "00", data.AnswerOrEmptyString("M2250PatientParameters").Equals("00"), new { @id = Model.TypeName + "_M2250PatientParameters0", @title = "(OASIS M2250) Plan of Care, Notifying Physician of Changes in Vitals/Other Findings, No" })%>
                                    <label for="<%= Model.TypeName %>_M2250PatientParameters0">
                                        <span class="float_left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250PatientParameters", "01", data.AnswerOrEmptyString("M2250PatientParameters").Equals("01"), new { @id = Model.TypeName + "_M2250PatientParameters1", @title = "(OASIS M2250) Plan of Care, Notifying Physician of Changes in Vitals/Other Findings, Yes" })%>
                                    <label for="<%= Model.TypeName %>_M2250PatientParameters1">
                                        <span class="float_left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="3">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250PatientParameters", "NA", data.AnswerOrEmptyString("M2250PatientParameters").Equals("NA"), new { @id = Model.TypeName + "_M2250PatientParametersNA", @title = "(OASIS M2250) Plan of Care, Notifying Physician of Changes in Vitals/Other Findings, Physician has Not Established Parameters" })%>
                                    <label for="<%= Model.TypeName %>_M2250PatientParametersNA">
                                        <span class="float_left">NA &#8211;</span>
                                        <span class="normal margin">Physician has chosen not to establish patient-specific parameters for this patient. Agency will use standardized clinical guidelines accessible for all care providers to reference</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="3" class="strong">
                                <span class="float_left">b.</span>
                                <span class="radio">Diabetic foot care including monitoring for the presence of skin lesions on the lower extremities and patient/caregiver education on proper foot care</span>
                                <%= Html.Hidden(Model.TypeName + "_M2250DiabeticFoot", "", new { @id = Model.TypeName + "_M2250DiabeticFootHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250DiabeticFoot", "00", data.AnswerOrEmptyString("M2250DiabeticFoot").Equals("00"), new { @id = Model.TypeName + "_M2250DiabeticFoot0", @title = "(OASIS M2250) Plan of Care, Diabetic Foot Care, No" })%>
                                    <label for="<%= Model.TypeName %>_M2250DiabeticFoot0">
                                        <span class="float_left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250DiabeticFoot", "01", data.AnswerOrEmptyString("M2250DiabeticFoot").Equals("01"), new { @id = Model.TypeName + "_M2250DiabeticFoot1", @title = "(OASIS M2250) Plan of Care, Diabetic Foot Care, Yes" })%>
                                    <label for="<%= Model.TypeName %>_M2250DiabeticFoot1">
                                        <span class="float_left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="3">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250DiabeticFoot", "NA", data.AnswerOrEmptyString("M2250DiabeticFoot").Equals("NA"), new { @id = Model.TypeName + "_M2250DiabeticFootNA", @title = "(OASIS M2250) Plan of Care, Diabetic Foot Care, Not Diabetic/Bilateral Amputee" })%>
                                    <label for="<%= Model.TypeName %>_M2250DiabeticFootNA">
                                        <span class="float_left">NA &#8211;</span>
                                        <span class="normal margin">Patient is not diabetic or is bilateral amputee</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="3" class="strong">
                                <span class="float_left">c.</span>
                                <span class="radio">Falls prevention interventions</span>
                                <%= Html.Hidden(Model.TypeName + "_M2250FallsPrevention", "", new { @id = Model.TypeName + "_M2250FallsPreventionHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250FallsPrevention", "00", data.AnswerOrEmptyString("M2250FallsPrevention").Equals("00"), new { @id = Model.TypeName + "_M2250FallsPrevention0", @title = "(OASIS M2250) Plan of Care, Fall Prevention, No" })%>
                                    <label for="<%= Model.TypeName %>_M2250FallsPrevention0">
                                        <span class="float_left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250FallsPrevention", "01", data.AnswerOrEmptyString("M2250FallsPrevention").Equals("01"), new { @id = Model.TypeName + "_M2250FallsPrevention1", @title = "(OASIS M2250) Plan of Care, Fall Prevention, Yes" })%>
                                    <label for="<%= Model.TypeName %>_M2250FallsPrevention1">
                                        <span class="float_left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="3">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250FallsPrevention", "NA", data.AnswerOrEmptyString("M2250FallsPrevention").Equals("NA"), new { @id = Model.TypeName + "_M2250FallsPreventionNA", @title = "(OASIS M2250) Plan of Care, Fall Prevention, Not Assessed to be at Risk for Falls" })%>
                                    <label for="<%= Model.TypeName %>_M2250FallsPreventionNA">
                                        <span class="float_left">NA &#8211;</span>
                                        <span class="normal margin">Patient is not assessed to be at risk for falls</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="3" class="strong">
                                <span class="float_left">d.</span>
                                <span class="radio">Depression intervention(s) such as medication, referral for other treatment, or a monitoring plan for current treatment</span>
                                <%= Html.Hidden(Model.TypeName + "_M2250DepressionPrevention", "", new { @id = Model.TypeName + "_M2250DepressionPreventionHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250DepressionPrevention", "00", data.AnswerOrEmptyString("M2250DepressionPrevention").Equals("00"), new { @id = Model.TypeName + "_M2250DepressionPrevention0", @title = "(OASIS M2250) Plan of Care, Depression Prevention, No" })%>
                                    <label for="<%= Model.TypeName %>_M2250DepressionPrevention0">
                                        <span class="float_left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250DepressionPrevention", "01", data.AnswerOrEmptyString("M2250DepressionPrevention").Equals("01"), new { @id = Model.TypeName + "_M2250DepressionPrevention1", @title = "(OASIS M2250) Plan of Care, Depression Prevention, Yes" })%>
                                    <label for="<%= Model.TypeName %>_M2250DepressionPrevention1">
                                        <span class="float_left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="3">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250DepressionPrevention", "NA", data.AnswerOrEmptyString("M2250DepressionPrevention").Equals("NA"), new { @id = Model.TypeName + "_M2250DepressionPreventionNA", @title = "(OASIS M2250) Plan of Care, Depression Prevention, No Diagnosis/Symptoms of Depression" })%>
                                    <label for="<%= Model.TypeName %>_M2250DepressionPreventionNA">
                                        <span class="float_left">NA &#8211;</span>
                                        <span class="normal margin">Patient has no diagnosis or symptoms of depression</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="3" class="strong">
                                <span class="float_left">e.</span>
                                <span class="radio">Intervention(s) to monitor and mitigate pain</span>
                                <%= Html.Hidden(Model.TypeName + "_M2250MonitorMitigatePainIntervention", "", new { @id = Model.TypeName + "_M2250MonitorMitigatePainInterventionHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250MonitorMitigatePainIntervention", "00", data.AnswerOrEmptyString("M2250MonitorMitigatePainIntervention").Equals("00"), new { @id = Model.TypeName + "_M2250MonitorMitigatePainIntervention0", @title = "(OASIS M2250) Plan of Care, Monitor/Mitigate Pain Intervention, No" })%>
                                    <label for="<%= Model.TypeName %>_M2250MonitorMitigatePainIntervention0">
                                        <span class="float_left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250MonitorMitigatePainIntervention", "01", data.AnswerOrEmptyString("M2250MonitorMitigatePainIntervention").Equals("01"), new { @id = Model.TypeName + "_M2250MonitorMitigatePainIntervention1", @title = "(OASIS M2250) Plan of Care, Monitor/Mitigate Pain Intervention, Yes" })%>
                                    <label for="<%= Model.TypeName %>_M2250MonitorMitigatePainIntervention1">
                                        <span class="float_left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="3">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250MonitorMitigatePainIntervention", "NA", data.AnswerOrEmptyString("M2250MonitorMitigatePainIntervention").Equals("NA"), new { @id = Model.TypeName + "_M2250MonitorMitigatePainInterventionNA", @title = "(OASIS M2250) Plan of Care, Monitor/Mitigate Pain Intervention, No Pain Identified" })%>
                                    <label for="<%= Model.TypeName %>_M2250MonitorMitigatePainInterventionNA">
                                        <span class="float_left">NA &#8211;</span>
                                        <span class="normal margin">No pain identified</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="3" class="strong">
                                <span class="float_left">f.</span>
                                <span class="radio">Intervention(s) to prevent pressure ulcers</span>
                                <%= Html.Hidden(Model.TypeName + "_M2250PressureUlcerIntervention", "", new { @id = Model.TypeName + "_M2250PressureUlcerInterventionHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250PressureUlcerIntervention", "00", data.AnswerOrEmptyString("M2250PressureUlcerIntervention").Equals("00"), new { @id = Model.TypeName + "_M2250PressureUlcerIntervention0", @title = "(OASIS M2250) Plan of Care, Pressure Ulcer Intervention, No" })%>
                                    <label for="<%= Model.TypeName %>_M2250PressureUlcerIntervention0">
                                        <span class="float_left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250PressureUlcerIntervention", "01", data.AnswerOrEmptyString("M2250PressureUlcerIntervention").Equals("01"), new { @id = Model.TypeName + "_M2250PressureUlcerIntervention1", @title = "(OASIS M2250) Plan of Care, Pressure Ulcer Intervention, Yes" })%>
                                    <label for="<%= Model.TypeName %>_M2250PressureUlcerIntervention1">
                                        <span class="float_left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="3">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250PressureUlcerIntervention", "NA", data.AnswerOrEmptyString("M2250PressureUlcerIntervention").Equals("NA"), new { @id = Model.TypeName + "_M2250PressureUlcerInterventionNA", @title = "(OASIS M2250) Plan of Care, Pressure Ulcer Intervention, Not Assessed to be at Risk for Pressure Ulcers" })%>
                                    <label for="<%= Model.TypeName %>_M2250PressureUlcerInterventionNA">
                                        <span class="float_left">NA &#8211;</span>
                                        <span class="normal margin">Patient is not assessed to be at risk for pressure ulcers</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="3" class="strong">
                                <span class="float_left">g.</span>
                                <span class="radio">Pressure ulcer treatment based on principles of moist wound healing OR order for treatment based on moist wound healing has been requested from physician</span>
                                <%= Html.Hidden(Model.TypeName + "_M2250PressureUlcerTreatment", "", new { @id = Model.TypeName + "_M2250PressureUlcerTreatmentHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250PressureUlcerTreatment", "00", data.AnswerOrEmptyString("M2250PressureUlcerTreatment").Equals("00"), new { @id = Model.TypeName + "_M2250PressureUlcerTreatment0", @title = "(OASIS M2250) Plan of Care, Ulcer Treatment, No" })%>
                                    <label for="<%= Model.TypeName %>_M2250PressureUlcerTreatment0">
                                        <span class="float_left">0 &#8211;</span>
                                        <span class="margin normal">No</span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250PressureUlcerTreatment", "01", data.AnswerOrEmptyString("M2250PressureUlcerTreatment").Equals("01"), new { @id = Model.TypeName + "_M2250PressureUlcerTreatment1", @title = "(OASIS M2250) Plan of Care, Ulcer Treatment, Yes" })%>
                                    <label for="<%= Model.TypeName %>_M2250PressureUlcerTreatment1">
                                        <span class="float_left">1 &#8211;</span>
                                        <span class="margin normal">Yes</span>
                                    </label>
                                </div>
                            </td>
                            <td colspan="3">
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2250PressureUlcerTreatment", "NA", data.AnswerOrEmptyString("M2250PressureUlcerTreatment").Equals("NA"), new { @id = Model.TypeName + "_M2250PressureUlcerTreatmentNA", @title = "(OASIS M2250) Plan of Care, Ulcer Treatment, No Pressure Ulcers" })%>
                                    <label for="<%= Model.TypeName %>_M2250PressureUlcerTreatmentNA">
                                        <span class="float_left">NA &#8211;</span>
                                        <span class="normal margin">Patient has no pressure ulcers with need for moist wound healing</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2250')" title="More Information about M2250">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/TherapyNeed.ascx", Model); %>
    <%  if (Model.AssessmentTypeNum.ToInteger() == 5) { %>
    <fieldset>
        <legend>Narrative</legend>
        <div class="wide_column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericNarrativeTemplate" class="strong">Narrative:</label>
                <%= Html.Templates(Model.TypeName + "_GenericDischargeNarrativeTemplate", new { @class = "Templates", @template = "#" + Model.TypeName + "_GenericDischargeNarrative" })%>
                <%= Html.TextArea(Model.TypeName + "_GenericDischargeNarrative", data.AnswerOrEmptyString("GenericDischargeNarrative"), 5, 70, new { @id = Model.TypeName + "_GenericDischargeNarrative", @title = "(Optional) Narrative" })%>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
        <%  if (Model.AssessmentTypeNum == "05") { %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        <%  } else { %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <%  } %>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 5) { %>
        <ul class="float_right">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div> 
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
    U.hideIfChecked($("#<%= Model.TypeName %>_M2200TherapyNeedNA"),  $("#<%= Model.TypeName %>_M2200Right"));
</script>