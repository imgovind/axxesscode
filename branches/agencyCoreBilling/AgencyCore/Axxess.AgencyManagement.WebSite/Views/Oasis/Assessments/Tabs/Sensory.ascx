<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "SensoryStatusForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit") %>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "Sensory")%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float_right">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    <fieldset>
        <legend>Eyes</legend>
        <%  string[] eyes = data.AnswerArray("GenericEyes"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericEyes", "", new { @id = Model.TypeName + "_GenericEyesHidden" })%>
        <div class="wide_column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Eyes, Within Normal Limits' type='checkbox' id='{0}_GenericEyes1' name='{0}_GenericEyes' value='1' {1} />", Model.TypeName, eyes.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEyes1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Eyes, Glasses' type='checkbox' id='{0}_GenericEyes2' name='{0}_GenericEyes' value='2' {1} />", Model.TypeName, eyes.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEyes2" class="radio">Glasses</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Eyes, Contacts Left' type='checkbox' id='{0}_GenericEyes3' name='{0}_GenericEyes' value='3' {1} />", Model.TypeName, eyes.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEyes3" class="radio">Contacts Left</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Eyes, Contacts Right' type='checkbox' id='{0}_GenericEyes4' name='{0}_GenericEyes' value='4' {1} />", Model.TypeName, eyes.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEyes4" class="radio">Contacts Right</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Eyes, Blurred Vision' type='checkbox' id='{0}_GenericEyes5' name='{0}_GenericEyes' value='5' {1} />", Model.TypeName, eyes.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEyes5" class="radio">Blurred Vision</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Eyes, Glaucoma' type='checkbox' id='{0}_GenericEyes6' name='{0}_GenericEyes' value='6' {1} />", Model.TypeName, eyes.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEyes6" class="radio">Glaucoma</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Eyes, Cataracts' type='checkbox' id='{0}_GenericEyes7' name='{0}_GenericEyes' value='7' {1} />", Model.TypeName, eyes.Contains("7").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEyes7" class="radio">Cataracts</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Eyes, Macular Degeneration' type='checkbox' id='{0}_GenericEyes8' name='{0}_GenericEyes' value='8' {1} />", Model.TypeName, eyes.Contains("8").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEyes8" class="radio">Macular Degeneration</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Eyes, Other' type='checkbox' id='{0}_GenericEyes13' name='{0}_GenericEyes' value='13' {1} />", Model.TypeName, eyes.Contains("13").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEyes13" class="radio more small">Other</label>
                        <div id="<%= Model.TypeName %>_GenericEyes13More">
                            <label for="<%= Model.TypeName %>_GenericEyesOtherDetails"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericEyesOtherDetails", data.AnswerOrEmptyString("GenericEyesOtherDetails"), new { @id = Model.TypeName + "_GenericEyesOtherDetails", @maxlength = "20", @title = "(Optional) " })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericEyesLastEyeExamDate" class="float_left">Date of Last Eye Exam</label>
                <div class="float_right"><input type="date" name="<%= Model.TypeName %>_GenericEyesLastEyeExamDate" value="<%= data.AnswerOrEmptyString("GenericEyesLastEyeExamDate") %>" id="<%= Model.TypeName %>_GenericEyesLastEyeExamDate" /></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Ears</legend>
        <%  string[] ears = data.AnswerArray("GenericEars"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericEars", "", new { @id = Model.TypeName + "_GenericEarsHidden" })%>
        <div class="wide_column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Ears, Within Normal Limits' type='checkbox' id='{0}_GenericEars1' name='{0}_GenericEars' value='1' {1} />", Model.TypeName, ears.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEars1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Ears, Deaf' type='checkbox' id='{0}_GenericEars3' name='{0}_GenericEars' value='3' {1} />", Model.TypeName, ears.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEars3" class="radio">Deaf</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Ears, Drainage' type='checkbox' id='{0}_GenericEars4' name='{0}_GenericEars' value='4' {1} />", Model.TypeName, ears.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEars4" class="radio">Drainage</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Ears, Pain' type='checkbox' id='{0}_GenericEars5' name='{0}_GenericEars' value='5' {1} />", Model.TypeName, ears.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEars5" class="radio">Pain</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Ears, Hearing Impaired' type='checkbox' id='{0}_GenericEars2' name='{0}_GenericEars' value='2' {1} />", Model.TypeName, ears.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEars2" class="radio more small">Hearing Impaired</label>
                        <div id="<%= Model.TypeName %>_GenericEars2More">
                            <%= Html.Hidden(Model.TypeName + "_GenericEarsHearingImpairedPosition")%>
                            <%= Html.RadioButton(Model.TypeName + "_GenericEarsHearingImpairedPosition", "0", data.AnswerOrEmptyString("GenericEarsHearingImpairedPosition").Equals("0"), new { @id = Model.TypeName + "_GenericEarsHearingImpairedPosition0", @class = "radio no_float", @title = "(Optional) Ears, Hearing Impaired Bilateral" })%>
                            <label for="<%= Model.TypeName %>_GenericEarsHearingImpairedPosition0" class="inlineradio">Bilateral</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericEarsHearingImpairedPosition", "1", data.AnswerOrEmptyString("GenericEarsHearingImpairedPosition").Equals("1"), new { @id = Model.TypeName + "_GenericEarsHearingImpairedPosition1", @class = "radio no_float", @title = "(Optional) Ears, Hearing Impaired Left" })%>
                            <label for="<%= Model.TypeName %>_GenericEarsHearingImpairedPosition1" class="inlineradio">Left</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericEarsHearingImpairedPosition", "2", data.AnswerOrEmptyString("GenericEarsHearingImpairedPosition").Equals("2"), new { @id = Model.TypeName + "_GenericEarsHearingImpairedPosition2", @class = "radio no_float", @title = "(Optional) Ears, Hearing Impaired Right" })%>
                            <label for="<%= Model.TypeName %>_GenericEarsHearingImpairedPosition2" class="inlineradio">Right</label>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Ears, Hearing Aids' type='checkbox' id='{0}_GenericEars6' name='{0}_GenericEars' value='6' {1} />", Model.TypeName, ears.Contains("6").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEars6" class="radio more small">Hearing Aids</label>
                        <div id="<%= Model.TypeName %>_GenericEars6More">
                            <%= Html.Hidden(Model.TypeName + "_GenericEarsHearingAidsPosition", "", new { @id = Model.TypeName + "_GenericEarsHearingAidsPositionHidden" })%>
                            <%= Html.RadioButton(Model.TypeName + "_GenericEarsHearingAidsPosition", "0", data.AnswerOrEmptyString("GenericEarsHearingAidsPosition").Equals("0"), new { @id = Model.TypeName + "_GenericEarsHearingAidsPosition0", @class = "radio no_float", @title = "(Optional) Ears, Hearing Aids Bilateral" })%>
                            <label for="<%= Model.TypeName %>_GenericEarsHearingAidsPosition0" class="inlineradio">Bilateral</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericEarsHearingAidsPosition", "1", data.AnswerOrEmptyString("GenericEarsHearingAidsPosition").Equals("1"), new { @id = Model.TypeName + "_GenericEarsHearingAidsPosition1", @class = "radio no_float", @title = "(Optional) Ears, Hearing Aids Left" })%>
                            <label for="<%= Model.TypeName %>_GenericEarsHearingAidsPosition1" class="inlineradio">Left</label>
                            <%= Html.RadioButton(Model.TypeName + "_GenericEarsHearingAidsPosition", "2", data.AnswerOrEmptyString("GenericEarsHearingAidsPosition").Equals("2"), new { @id = Model.TypeName + "_GenericEarsHearingAidsPosition2", @class = "radio no_float", @title = "(Optional) Ears, Hearing Aids Right" })%>
                            <label for="<%= Model.TypeName %>_GenericEarsHearingAidsPosition2" class="inlineradio">Right</label>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Ears, Other' type='checkbox' id='{0}_GenericEars7' name='{0}_GenericEars' value='7' {1} />", Model.TypeName, ears.Contains("7").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericEars7" class="radio more small">Other</label>
                        <div id="<%= Model.TypeName %>_GenericEars7More">
                            <label for="<%= Model.TypeName %>_GenericEarsOtherDetails"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericEarsOtherDetails", data.AnswerOrEmptyString("GenericEarsOtherDetails"), new { @id = Model.TypeName + "_GenericEarsOtherDetails", @maxlength = "20", @title = "(Optional) Ears, Specify Other" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Nose</legend>
        <%  string[] nose = data.AnswerArray("GenericNose"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericNose", "", new { @id = Model.TypeName + "_GenericNoseHidden" })%>
        <div class="wide_column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nose, Within Normal Limits' type='checkbox' id='{0}_GenericNose1' name='{0}_GenericNose' value='1' {1} />", Model.TypeName, nose.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericNose1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nose, Congestion' type='checkbox' id='{0}_GenericNose2' name='{0}_GenericNose' value='2' {1} />", Model.TypeName, nose.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericNose2" class="radio">Congestion</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nose, Loss of Smell' type='checkbox' id='{0}_GenericNose3' name='{0}_GenericNose' value='3' {1} />", Model.TypeName, nose.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericNose3" class="radio">Loss of Smell</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nose, Nose Bleeds' type='checkbox' id='{0}_GenericNose4' name='{0}_GenericNose' value='4' {1} />", Model.TypeName, nose.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericNose4" class="radio more small fixed">Nose Bleeds</label>
                        <div id="<%= Model.TypeName %>_GenericNose4More">
                            <label for="<%= Model.TypeName %>_GenericNoseBleedsFrequency"><em>How often?</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericNoseBleedsFrequency", data.AnswerOrEmptyString("GenericNoseBleedsFrequency"), new { @class = "oe", @id = Model.TypeName + "_GenericNoseBleedsFrequency", @maxlength = "10", @title = "(Optional) " })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Nose, Other' type='checkbox' id='{0}_GenericNose5' name='{0}_GenericNose' value='5' {1} />", Model.TypeName, nose.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericNose5" class="radio more small fixed">Other</label>
                        <div id="<%= Model.TypeName %>_GenericNose5More">
                            <label for="<%= Model.TypeName %>_GenericNoseOtherDetails"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericNoseOtherDetails", data.AnswerOrEmptyString("GenericNoseOtherDetails"), new { @id = Model.TypeName + "_GenericNoseOtherDetails", @maxlength = "20", @title = "(Optional) Nose, Specify Other" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Mouth</legend>
        <%  string[] mouth = data.AnswerArray("GenericMouth"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericMouth", "", new { @id = Model.TypeName + "_GenericMouthHidden" })%>
        <input type="hidden" name="<%= Model.TypeName %>_GenericMouth" value="" />
        <div class="wide_column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Mouth, Within Normal Limits' type='checkbox' id='{0}_GenericMouth1' name='{0}_GenericMouth' value='1' {1} />", Model.TypeName, mouth.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericMouth1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Mouth, Dentures' type='checkbox' id='{0}_GenericMouth2' name='{0}_GenericMouth' value='2' {1} />", Model.TypeName, mouth.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericMouth2" class="radio">Dentures</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Mouth, Difficulty Chewing' type='checkbox' id='{0}_GenericMouth3' name='{0}_GenericMouth' value='3' {1} />", Model.TypeName, mouth.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericMouth3" class="radio">Difficulty chewing</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Mouth, Dysphagia' type='checkbox' id='{0}_GenericMouth4' name='{0}_GenericMouth' value='4' {1} />", Model.TypeName, mouth.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericMouth4" class="radio">Dysphagia</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Mouth, Other' type='checkbox' id='{0}_GenericMouth5' name='{0}_GenericMouth' value='5' {1} />", Model.TypeName, mouth.Contains("5").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericMouth5" class="radio">Other</label>
                        <div id="<%= Model.TypeName %>_GenericMouth5More">
                            <label for="<%= Model.TypeName %>_GenericMouthOther"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericMouthOther", data.AnswerOrEmptyString("GenericMouthOther"), new { @id = Model.TypeName + "_GenericMouthOther", @maxlength = "20", @title = "(Optional) Mouth, Specify Other" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Throat</legend>
        <% string[] throat = data.AnswerArray("GenericThroat"); %>
        <%= Html.Hidden(Model.TypeName + "_GenericThroat", "", new { @id = Model.TypeName + "_GenericThroatHidden" })%>
        <div class="wide_column">
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Throat, Within Normal Limits' type='checkbox' id='{0}_GenericThroat1' name='{0}_GenericThroat' value='1' {1} />", Model.TypeName, throat.Contains("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericThroat1" class="radio">WNL (Within Normal Limits)</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Throat, Sore' type='checkbox' id='{0}_GenericThroat2' name='{0}_GenericThroat' value='2' {1} />", Model.TypeName, throat.Contains("2").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericThroat2" class="radio">Sore throat</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Throat, Hoarseness' type='checkbox' id='{0}_GenericThroat3' name='{0}_GenericThroat' value='3' {1} />", Model.TypeName, throat.Contains("3").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericThroat3" class="radio">Hoarseness</label>
                    </div>
                    <div class="option">
                        <%= string.Format("<input title='(Optional) Throat, Other' type='checkbox' id='{0}_GenericThroat4' name='{0}_GenericThroat' value='4' {1} />", Model.TypeName, throat.Contains("4").ToChecked())%>
                        <label for="<%= Model.TypeName %>_GenericThroat4" class="radio">Other</label>
                        <div id="<%= Model.TypeName %>_GenericThroat4More">
                            <label for="<%= Model.TypeName %>_GenericThroatOther"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_GenericThroatOther", data.AnswerOrEmptyString("GenericThroatOther"), new { @id = Model.TypeName + "_GenericThroatOther", @maxlength = "20", @title = "(Optional) Throat, Specify Other" })%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 6) { %>
    <fieldset class="oasis">
        <legend>Vision</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1200">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1200" class="green" onclick="Oasis.ToolTip('M1200')">(M1200)</a>
                    Vision (with corrective lenses if the patient usually wears them)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1200Vision", "", new { @id = Model.TypeName + "_M1200VisionHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1200Vision", "00", data.AnswerOrEmptyString("M1200Vision").Equals("00"), new { @id = Model.TypeName + "_M1200Vision00", @title = "(OASIS M1200) Vision, Normal" }) %>
                        <label for="<%= Model.TypeName %>_M1200Vision00">
                            <span class="float_left">0 &#8211;</span>
                            <span class="normal margin">Normal vision: sees adequately in most situations; can see medication labels, newsprint.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1200Vision", "01", data.AnswerOrEmptyString("M1200Vision").Equals("01"), new { @id = Model.TypeName + "_M1200Vision01", @title = "(OASIS M1200) Vision, Partial Impairment" })%>
                        <label for="<%= Model.TypeName %>_M1200Vision01">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Partially impaired: cannot see medication labels or newsprint, but can see obstacles in path, and the surrounding layout; can count fingers at arm&#8217;s length.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1200Vision", "02", data.AnswerOrEmptyString("M1200Vision").Equals("02"), new { @id = Model.TypeName + "_M1200Vision02", @title = "(OASIS M1200) Vision, Severe Impairment" })%>
                        <label for="<%= Model.TypeName %>_M1200Vision02">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Severely impaired: cannot locate objects without hearing or touching them or patient nonresponsive.</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1200')" title="More Information about M1200">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>Hearing</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1210">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1210" class="green" onclick="Oasis.ToolTip('M1210')">(M1210)</a>
                    Ability to hear (with hearing aid or hearing appliance if normally used)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1210Hearing", "", new { @id = Model.TypeName + "_M1210HearingHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1210Hearing", "00", data.AnswerOrEmptyString("M1210Hearing").Equals("00"), new { @id = Model.TypeName + "_M1210Hearing00", @title = "(OASIS M1200) Hearing, Adequate" })%>
                        <label for="<%= Model.TypeName %>_M1210Hearing00">
                            <span class="float_left">0 &#8211;</span>
                            <span class="normal margin">Adequate: hears normal conversation without difficulty.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1210Hearing", "01", data.AnswerOrEmptyString("M1210Hearing").Equals("01"), new { @id = Model.TypeName + "_M1210Hearing01", @title = "(OASIS M1200) Hearing, Mildly to Moderately Impaired" })%>
                        <label for="<%= Model.TypeName %>_M1210Hearing01">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Mildly to Moderately Impaired: difficulty hearing in some environments or speaker may need to increase volume or speak distinctly.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1210Hearing", "02", data.AnswerOrEmptyString("M1210Hearing").Equals("02"), new { @id = Model.TypeName + "_M1210Hearing02", @title = "(OASIS M1200) Hearing, Severely Impaired" })%>
                        <label for="<%= Model.TypeName %>_M1210Hearing02">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Severely Impaired: absence of useful hearing.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1210Hearing", "UK", data.AnswerOrEmptyString("M1210Hearing").Equals("UK"), new { @id = Model.TypeName + "_M1210HearingUK", @title = "(OASIS M1200) Hearing, Unknown" })%>
                        <label for="<%= Model.TypeName %>_M1210HearingUK">
                            <span class="float_left">UK &#8211;</span>
                            <span class="normal margin">Unable to assess hearing.</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1210')" title="More Information about M1210">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
    <fieldset class="oasis">
        <legend>Speech/Verbal</legend>
        <div class="wide_column">
            <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
            <div class="row" id="<%= Model.TypeName %>_M1220">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1220" class="green" onclick="Oasis.ToolTip('M1220')">(M1220)</a>
                    Understanding of Verbal Content in patient&#8217;s own language (with hearing aid or device if used)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1220VerbalContent", "", new { @id = Model.TypeName + "_M1220VerbalContentHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1220VerbalContent", "00", data.AnswerOrEmptyString("M1220VerbalContent").Equals("00"), new { @id = Model.TypeName + "_M1220VerbalContent00", @title = "(OASIS M1200) Understand Verbal Content, Understands" })%>
                        <label for="<%= Model.TypeName %>_M1220VerbalContent00">
                            <span class="float_left">0 &#8211;</span>
                            <span class="normal margin">Understands: clear comprehension without cues or repetitions.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1220VerbalContent", "01", data.AnswerOrEmptyString("M1220VerbalContent").Equals("01"), new { @id = Model.TypeName + "_M1220VerbalContent01", @title = "(OASIS M1200) Understand Verbal Content, Usually Understands" })%>
                        <label for="<%= Model.TypeName %>_M1220VerbalContent01">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Usually Understands: understands most conversations, but misses some part/intent of message. Requires cues at times to understand.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1220VerbalContent", "02", data.AnswerOrEmptyString("M1220VerbalContent").Equals("02"), new { @id = Model.TypeName + "_M1220VerbalContent02", @title = "(OASIS M1200) Understand Verbal Content, Sometimes Understands" })%>
                        <label for="<%= Model.TypeName %>_M1220VerbalContent02">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Sometimes Understands: understands only basic conversations or simple, direct phrases. Frequently requires cues to understand.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1220VerbalContent", "03", data.AnswerOrEmptyString("M1220VerbalContent").Equals("03"), new { @id = Model.TypeName + "_M1220VerbalContent03", @title = "(OASIS M1200) Understand Verbal Content, Rarely/Never Understands" })%>
                        <label for="<%= Model.TypeName %>_M1220VerbalContent03">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">Rarely/Never Understands.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1220VerbalContent", "UK", data.AnswerOrEmptyString("M1220VerbalContent").Equals("UK"), new { @id = Model.TypeName + "_M1220VerbalContentUK", @title = "(OASIS M1200) Understand Verbal Content, Unknown" })%>
                        <label for="<%= Model.TypeName %>_M1220VerbalContentUK">
                            <span class="float_left">UK &#8211;</span>
                            <span class="normal margin">Unable to assess understanding.</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1220')" title="More Information about M1220">?</div>
                </div>
            </div>
            <%  } %>
            <div class="row" id="<%= Model.TypeName %>_M1230">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1230" class="green" onclick="Oasis.ToolTip('M1230')">(M1230)</a>
                    Speech and Oral (Verbal) Expression of Language (in patient&#8217;s own language)
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1230SpeechAndOral", "", new { @id = Model.TypeName + "_M1230SpeechAndOralHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1230SpeechAndOral", "00", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("00"), new { @id = Model.TypeName + "_M1230SpeechAndOral00", @title = "(OASIS M1230) Speech Expression, Expresses Complex Ideas" })%>
                        <label for="<%= Model.TypeName %>_M1230SpeechAndOral00">
                            <span class="float_left">0 &#8211;</span>
                            <span class="normal margin">Expresses complex ideas, feelings, and needs clearly, completely, and easily in all situations with no observable impairment.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1230SpeechAndOral", "01", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("01"), new { @id = Model.TypeName + "_M1230SpeechAndOral01", @title = "(OASIS M1230) Speech Expression, Minimal Difficulty" })%>
                        <label for="<%= Model.TypeName %>_M1230SpeechAndOral01">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Minimal difficulty in expressing ideas and needs (may take extra time; makes occasional errors in word choice, grammar or speech intelligibility; needs minimal prompting or assistance).</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1230SpeechAndOral", "02", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("02"), new { @id = Model.TypeName + "_M1230SpeechAndOral02", @title = "(OASIS M1230) Speech Expression, Expresses Simple Ideas" })%>
                        <label for="<%= Model.TypeName %>_M1230SpeechAndOral02">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Expresses simple ideas or needs with moderate difficulty (needs prompting or assistance, errors in word choice, organization or speech intelligibility). Speaks in phrases or short sentences.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1230SpeechAndOral", "03", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("03"), new { @id = Model.TypeName + "_M1230SpeechAndOral03", @title = "(OASIS M1230) Speech Expression, Severe Difficulty" })%>
                        <label for="<%= Model.TypeName %>_M1230SpeechAndOral03">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">Has severe difficulty expressing basic ideas or needs and requires maximal assistance or guessing by listener. Speech limited to single words or short phrases.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1230SpeechAndOral", "04", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("04"), new { @id = Model.TypeName + "_M1230SpeechAndOral04", @title = "(OASIS M1230) Speech Expression, Unable" })%>
                        <label for="<%= Model.TypeName %>_M1230SpeechAndOral04">
                            <span class="float_left">4 &#8211;</span>
                            <span class="normal margin">Unable to express basic needs even with maximal prompting or assistance but is not comatose or unresponsive (e.g., speech is nonsensical or unintelligible).</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1230SpeechAndOral", "05", data.AnswerOrEmptyString("M1230SpeechAndOral").Equals("05"), new { @id = Model.TypeName + "_M1230SpeechAndOral05", @title = "(OASIS M1230) Speech Expression, Nonresponsive" })%>
                        <label for="<%= Model.TypeName %>_M1230SpeechAndOral05">
                            <span class="float_left">5 &#8211;</span>
                            <span class="normal margin">Patient nonresponsive or unable to speak.</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1230')" title="More Information about M1230">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <% if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Sensory.ascx", Model); %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float_right">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
    U.showIfChecked(
        $("#<%= Model.TypeName %>_GenericEyes13"),
        $("#<%= Model.TypeName %>_GenericEyes13More"));
    U.showIfChecked(
        $("#<%= Model.TypeName %>_GenericEars2"),
        $("#<%= Model.TypeName %>_GenericEars2More"));
    U.showIfChecked(
        $("#<%= Model.TypeName %>_GenericEars6"),
        $("#<%= Model.TypeName %>_GenericEars6More"));
    U.showIfChecked(
        $("#<%= Model.TypeName %>_GenericEars7"),
        $("#<%= Model.TypeName %>_GenericEars7More"));
    U.showIfChecked(
        $("#<%= Model.TypeName %>_GenericNose4"),
        $("#<%= Model.TypeName %>_GenericNose4More"));
    U.showIfChecked(
        $("#<%= Model.TypeName %>_GenericNose5"),
        $("#<%= Model.TypeName %>_GenericNose5More"));
    U.showIfChecked(
        $("#<%= Model.TypeName %>_GenericMouth5"),
        $("#<%= Model.TypeName %>_GenericMouth5More"));
    U.showIfChecked(
        $("#<%= Model.TypeName %>_GenericThroat4"),
        $("#<%= Model.TypeName %>_GenericThroat4More"));
</script>