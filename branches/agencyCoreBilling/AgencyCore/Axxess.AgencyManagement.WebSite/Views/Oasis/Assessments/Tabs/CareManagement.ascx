<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() < 4 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "ManagementForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id)%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit")%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId)%>
    <%= Html.Hidden("assessment", Model.TypeName) %>
    <%= Html.Hidden("categoryType", "Caremanagement")%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float_right">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <fieldset class="oasis">
        <legend>Types and Sources of Assistance</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M2100">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M2100')" title="More Information about M2100">(M2100)</a>
                    Types and Sources of Assistance: Determine the level of caregiver ability and willingness to provide assistance for the following activities, if assistance is needed.
                    <em>(Check only one box in each row)</em>
                </label>
                <table class="form">
                    <thead>
                        <tr>
                            <th colspan="2">Type of Assistance</th>
                            <th>No assistance needed in this area</th>
                            <th>Caregiver(s) currently provide assistance</th>
                            <th>Caregiver(s) need training/ supportive services to provide assistance</th>
                            <th>Caregiver(s) not likely to provide assistance</th>
                            <th>Unclear if Caregiver(s) will provide assistance</th>
                            <th>Assistance needed, but no Caregiver(s) available</th>
                        </tr>
                    </thead>
                    <tbody class="checkgroup padfix">
                        <tr>
                            <td colspan="2" class="strong">
                                <span class="float_left">a.</span>
                                <span class="radio">ADL assistance (e.g., transfer/ ambulation, bathing, dressing, toileting, eating/feeding)</span>
                                <%= Html.Hidden(Model.TypeName + "_M2100ADLAssistance", "", new { @id = Model.TypeName + "_M2100ADLAssistanceHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ADLAssistance", "00", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("00"), new { @id = Model.TypeName + "_M2100ADLAssistance0", @title = "(OASIS M2100) Types/Sources of Assistance, ADL assistance, No Assistance Needed" })%>
                                    <label for="<%= Model.TypeName %>_M2100ADLAssistance0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ADLAssistance", "01", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("01"), new { @id = Model.TypeName + "_M2100ADLAssistance1", @title = "(OASIS M2100) Types/Sources of Assistance, ADL assistance, Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100ADLAssistance1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ADLAssistance", "02", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("02"), new { @id = Model.TypeName + "_M2100ADLAssistance2", @title = "(OASIS M2100) Types/Sources of Assistance, ADL assistance, Caregiver Needs Training/Supportive Services" })%>
                                    <label for="<%= Model.TypeName %>_M2100ADLAssistance2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ADLAssistance", "03", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("03"), new { @id = Model.TypeName + "_M2100ADLAssistance3", @title = "(OASIS M2100) Types/Sources of Assistance, ADL assistance, Caregiver Not Likely to Assistance" })%>
                                    <label for="<%= Model.TypeName %>_M2100ADLAssistance3" class="radio">3</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ADLAssistance", "04", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("04"), new { @id = Model.TypeName + "_M2100ADLAssistance4", @title = "(OASIS M2100) Types/Sources of Assistance, ADL assistance, Unclear if Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100ADLAssistance4" class="radio">4</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ADLAssistance", "05", data.AnswerOrEmptyString("M2100ADLAssistance").Equals("05"), new { @id = Model.TypeName + "_M2100ADLAssistance5", @title = "(OASIS M2100) Types/Sources of Assistance, ADL assistance, No Caregiver" })%>
                                    <label for="<%= Model.TypeName %>_M2100ADLAssistance5" class="radio">5</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="2" class="strong">
                                <span class="float_left">b.</span>
                                <span class="radio">IADL assistance (e.g., meals, housekeeping, laundry, telephone, shopping, finances)</span>
                                <%= Html.Hidden(Model.TypeName + "_M2100IADLAssistance", "", new { @id = Model.TypeName + "_M2100IADLAssistanceHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100IADLAssistance", "00", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("00"), new { @id = Model.TypeName + "_M2100IADLAssistance0", @title = "(OASIS M2100) Types/Sources of Assistance, IADL assistance, No Assistance Needed" })%>
                                    <label for="<%= Model.TypeName %>_M2100IADLAssistance0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100IADLAssistance", "01", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("01"), new { @id = Model.TypeName + "_M2100IADLAssistance1", @title = "(OASIS M2100) Types/Sources of Assistance, IADL assistance, Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100IADLAssistance1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100IADLAssistance", "02", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("02"), new { @id = Model.TypeName + "_M2100IADLAssistance2", @title = "(OASIS M2100) Types/Sources of Assistance, IADL assistance, Caregiver Needs Training/Supportive Services" })%>
                                    <label for="<%= Model.TypeName %>_M2100IADLAssistance2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100IADLAssistance", "03", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("03"), new { @id = Model.TypeName + "_M2100IADLAssistance3", @title = "(OASIS M2100) Types/Sources of Assistance, IADL assistance, Caregiver Not Likely to Assistance" })%>
                                    <label for="<%= Model.TypeName %>_M2100IADLAssistance3" class="radio">3</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100IADLAssistance", "04", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("04"), new { @id = Model.TypeName + "_M2100IADLAssistance4", @title = "(OASIS M2100) Types/Sources of Assistance, IADL assistance, Unclear if Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100IADLAssistance4" class="radio">4</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100IADLAssistance", "05", data.AnswerOrEmptyString("M2100IADLAssistance").Equals("05"), new { @id = Model.TypeName + "_M2100IADLAssistance5", @title = "(OASIS M2100) Types/Sources of Assistance, IADL assistance, No Caregiver" })%>
                                    <label for="<%= Model.TypeName %>_M2100IADLAssistance5" class="radio">5</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="2" class="strong">
                                <span class="float_left">c.</span>
                                <span class="radio">Medication administration (e.g., oral, inhaled or injectable)</span>
                                <%= Html.Hidden(Model.TypeName + "_M2100MedicationAdministration", "", new { @id = Model.TypeName + "_M2100MedicationAdministrationHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicationAdministration", "00", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("00"), new { @id = Model.TypeName + "_M2100MedicationAdministration0", @title = "(OASIS M2100) Types/Sources of Assistance, Medication Administration, No Assistance Needed" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicationAdministration0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicationAdministration", "01", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("01"), new { @id = Model.TypeName + "_M2100MedicationAdministration1", @title = "(OASIS M2100) Types/Sources of Assistance, Medication Administration, Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicationAdministration1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicationAdministration", "02", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("02"), new { @id = Model.TypeName + "_M2100MedicationAdministration2", @title = "(OASIS M2100) Types/Sources of Assistance, Medication Administration, Caregiver Needs Training/Supportive Services" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicationAdministration2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicationAdministration", "03", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("03"), new { @id = Model.TypeName + "_M2100MedicationAdministration3", @title = "(OASIS M2100) Types/Sources of Assistance, Medication Administration, Caregiver Not Likely to Assistance" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicationAdministration3" class="radio">3</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicationAdministration", "04", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("04"), new { @id = Model.TypeName + "_M2100MedicationAdministration4", @title = "(OASIS M2100) Types/Sources of Assistance, Medication Administration, Unclear if Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicationAdministration4" class="radio">4</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicationAdministration", "05", data.AnswerOrEmptyString("M2100MedicationAdministration").Equals("05"), new { @id = Model.TypeName + "_M2100MedicationAdministration5", @title = "(OASIS M2100) Types/Sources of Assistance, Medication Administration, No Caregiver" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicationAdministration5" class="radio">5</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="2" class="strong">
                                <span class="float_left">d.</span>
                                <span class="radio">Medical procedures/ treatments (e.g., changing wound dressing)</span>
                                <%= Html.Hidden(Model.TypeName + "_M2100MedicalProcedures", "", new { @id = Model.TypeName + "_M2100MedicalProceduresHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicalProcedures", "00", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("00"), new { @id = Model.TypeName + "_M2100MedicalProcedures0", @title = "(OASIS M2100) Types/Sources of Assistance, Medical Procedures/Treatments, No Assistance Needed" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicalProcedures0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicalProcedures", "01", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("01"), new { @id = Model.TypeName + "_M2100MedicalProcedures1", @title = "(OASIS M2100) Types/Sources of Assistance, Medical Procedures/Treatments, Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicalProcedures1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicalProcedures", "02", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("02"), new { @id = Model.TypeName + "_M2100MedicalProcedures2", @title = "(OASIS M2100) Types/Sources of Assistance, Medical Procedures/Treatments, Caregiver Needs Training/Supportive Services" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicalProcedures2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicalProcedures", "03", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("03"), new { @id = Model.TypeName + "_M2100MedicalProcedures3", @title = "(OASIS M2100) Types/Sources of Assistance, Medical Procedures/Treatments, Caregiver Not Likely to Assistance" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicalProcedures3" class="radio">3</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicalProcedures", "04", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("04"), new { @id = Model.TypeName + "_M2100MedicalProcedures4", @title = "(OASIS M2100) Types/Sources of Assistance, Medical Procedures/Treatments, Unclear if Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicalProcedures4" class="radio">4</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100MedicalProcedures", "05", data.AnswerOrEmptyString("M2100MedicalProcedures").Equals("05"), new { @id = Model.TypeName + "_M2100MedicalProcedures5", @title = "(OASIS M2100) Types/Sources of Assistance, Medical Procedures/Treatments, No Caregiver" })%>
                                    <label for="<%= Model.TypeName %>_M2100MedicalProcedures5" class="radio">5</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="2" class="strong">
                                <span class="float_left">e.</span>
                                <span class="radio">Management of Equipment (includes oxygen, IV/infusion equipment, enteral/ parenteral nutrition, ventilator therapy equipment or supplies)</span>
                                <%= Html.Hidden(Model.TypeName + "_M2100ManagementOfEquipment", "", new { @id = Model.TypeName + "_M2100ManagementOfEquipmentHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ManagementOfEquipment", "00", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("00"), new { @id = Model.TypeName + "_M2100ManagementOfEquipment0", @title = "(OASIS M2100) Types/Sources of Assistance, Equipment Management, No Assistance Needed" })%>
                                    <label for="<%= Model.TypeName %>_M2100ManagementOfEquipment0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ManagementOfEquipment", "01", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("01"), new { @id = Model.TypeName + "_M2100ManagementOfEquipment1", @title = "(OASIS M2100) Types/Sources of Assistance, Equipment Management, Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100ManagementOfEquipment1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ManagementOfEquipment", "02", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("02"), new { @id = Model.TypeName + "_M2100ManagementOfEquipment2", @title = "(OASIS M2100) Types/Sources of Assistance, Equipment Management, Caregiver Needs Training/Supportive Services" })%>
                                    <label for="<%= Model.TypeName %>_M2100ManagementOfEquipment2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ManagementOfEquipment", "03", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("03"), new { @id = Model.TypeName + "_M2100ManagementOfEquipment3", @title = "(OASIS M2100) Types/Sources of Assistance, Equipment Management, Caregiver Not Likely to Assistance" })%>
                                    <label for="<%= Model.TypeName %>_M2100ManagementOfEquipment3" class="radio">3</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ManagementOfEquipment", "04", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("04"), new { @id = Model.TypeName + "_M2100ManagementOfEquipment4", @title = "(OASIS M2100) Types/Sources of Assistance, Equipment Management, Unclear if Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100ManagementOfEquipment4" class="radio">4</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100ManagementOfEquipment", "05", data.AnswerOrEmptyString("M2100ManagementOfEquipment").Equals("05"), new { @id = Model.TypeName + "_M2100ManagementOfEquipment5", @title = "(OASIS M2100) Types/Sources of Assistance, Equipment Management, No Caregiver" })%>
                                    <label for="<%= Model.TypeName %>_M2100ManagementOfEquipment5" class="radio">5</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="2" class="strong">
                                <span class="float_left">f.</span>
                                <span class="radio">Supervision and safety (e.g., due to cognitive impairment)</span>
                                <%= Html.Hidden(Model.TypeName + "_M2100SupervisionAndSafety", "", new { @id = Model.TypeName + "_M2100SupervisionAndSafetyHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100SupervisionAndSafety", "00", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("00"), new { @id = Model.TypeName + "_M2100SupervisionAndSafety0", @title = "(OASIS M2100) Types/Sources of Assistance, Supervision and Safety, No Assistance Needed" })%>
                                    <label for="<%= Model.TypeName %>_M2100SupervisionAndSafety0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100SupervisionAndSafety", "01", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("01"), new { @id = Model.TypeName + "_M2100SupervisionAndSafety1", @title = "(OASIS M2100) Types/Sources of Assistance, Supervision and Safety, Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100SupervisionAndSafety1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100SupervisionAndSafety", "02", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("02"), new { @id = Model.TypeName + "_M2100SupervisionAndSafety2", @title = "(OASIS M2100) Types/Sources of Assistance, Supervision and Safety, Caregiver Needs Training/Supportive Services" })%>
                                    <label for="<%= Model.TypeName %>_M2100SupervisionAndSafety2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100SupervisionAndSafety", "03", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("03"), new { @id = Model.TypeName + "_M2100SupervisionAndSafety3", @title = "(OASIS M2100) Types/Sources of Assistance, Supervision and Safety, Caregiver Not Likely to Assistance" })%>
                                    <label for="<%= Model.TypeName %>_M2100SupervisionAndSafety3" class="radio">3</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100SupervisionAndSafety", "04", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("04"), new { @id = Model.TypeName + "_M2100SupervisionAndSafety4", @title = "(OASIS M2100) Types/Sources of Assistance, Supervision and Safety, Unclear if Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100SupervisionAndSafety4" class="radio">4</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100SupervisionAndSafety", "05", data.AnswerOrEmptyString("M2100SupervisionAndSafety").Equals("05"), new { @id = Model.TypeName + "_M2100SupervisionAndSafety5", @title = "(OASIS M2100) Types/Sources of Assistance, Supervision and Safety, No Caregiver" })%>
                                    <label for="<%= Model.TypeName %>_M2100SupervisionAndSafety5" class="radio">5</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="8"><hr /></th>
                        </tr>
                        <tr>
                            <td colspan="2" class="strong">
                                <span class="float_left">g.</span>
                                <span class="radio">Advocacy or facilitation of patient&#8217;s participation in appropriate medical care (includes transportation to or from appointments)</span>
                                <%= Html.Hidden(Model.TypeName + "_M2100FacilitationPatientParticipation", "", new { @id = Model.TypeName + "_M2100FacilitationPatientParticipationHidden" })%>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100FacilitationPatientParticipation", "00", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("00"), new { @id = Model.TypeName + "_M2100FacilitationPatientParticipation0", @title = "(OASIS M2100) Types/Sources of Assistance, Advocacy/Facilitation of Participation in Care, No Assistance Needed" })%>
                                    <label for="<%= Model.TypeName %>_M2100FacilitationPatientParticipation0" class="radio">0</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100FacilitationPatientParticipation", "01", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("01"), new { @id = Model.TypeName + "_M2100FacilitationPatientParticipation1", @title = "(OASIS M2100) Types/Sources of Assistance, Advocacy/Facilitation of Participation in Care, Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100FacilitationPatientParticipation1" class="radio">1</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100FacilitationPatientParticipation", "02", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("02"), new { @id = Model.TypeName + "_M2100FacilitationPatientParticipation2", @title = "(OASIS M2100) Types/Sources of Assistance, Advocacy/Facilitation of Participation in Care, Caregiver Needs Training/Supportive Services" })%>
                                    <label for="<%= Model.TypeName %>_M2100FacilitationPatientParticipation2" class="radio">2</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100FacilitationPatientParticipation", "03", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("03"), new { @id = Model.TypeName + "_M2100FacilitationPatientParticipation3", @title = "(OASIS M2100) Types/Sources of Assistance, Advocacy/Facilitation of Participation in Care, Caregiver Not Likely to Assistance" })%>
                                    <label for="<%= Model.TypeName %>_M2100FacilitationPatientParticipation3" class="radio">3</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100FacilitationPatientParticipation", "04", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("04"), new { @id = Model.TypeName + "_M2100FacilitationPatientParticipation4", @title = "(OASIS M2100) Types/Sources of Assistance, Advocacy/Facilitation of Participation in Care, Unclear if Caregiver Assists" })%>
                                    <label for="<%= Model.TypeName %>_M2100FacilitationPatientParticipation4" class="radio">4</label>
                                </div>
                            </td>
                            <td>
                                <div class="option">
                                    <%= Html.RadioButton(Model.TypeName + "_M2100FacilitationPatientParticipation", "05", data.AnswerOrEmptyString("M2100FacilitationPatientParticipation").Equals("05"), new { @id = Model.TypeName + "_M2100FacilitationPatientParticipation5", @title = "(OASIS M2100) Types/Sources of Assistance, Advocacy/Facilitation of Participation in Care, No Caregiver" })%>
                                    <label for="<%= Model.TypeName %>_M2100FacilitationPatientParticipation5" class="radio">5</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2100')" title="More Information about M2100">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>ADL or IADL Assistance</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M2110">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M2110" class="green" onclick="Oasis.ToolTip('M2110')">(M2110)</a>
                    How Often does the patient receive ADL or IADL assistance from any caregiver(s) (other than home health agency staff)?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "", new { @id = Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "01", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("01"), new { @id = Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance1", @title = "(OASIS M2110) Caregiver ADL/IADL Assistance Frequency, Daily" })%>
                        <label for="<%= Model.TypeName %>_M2110FrequencyOfADLOrIADLAssistance1">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">At least daily</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "02", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("02"), new { @id = Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance2", @title = "(OASIS M2110) Caregiver ADL/IADL Assistance Frequency, 3+ Times Per Week" })%>
                        <label for="<%= Model.TypeName %>_M2110FrequencyOfADLOrIADLAssistance2">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Three or more times per week</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "03", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("03"), new { @id = Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance3", @title = "(OASIS M2110) Caregiver ADL/IADL Assistance Frequency, 1-2 Times Per Week" })%>
                        <label for="<%= Model.TypeName %>_M2110FrequencyOfADLOrIADLAssistance3">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">One to two times per week</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "04", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("04"), new { @id = Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance4", @title = "(OASIS M2110) Caregiver ADL/IADL Assistance Frequency, Less than Weekly" })%>
                        <label for="<%= Model.TypeName %>_M2110FrequencyOfADLOrIADLAssistance4">
                            <span class="float_left">4 &#8211;</span>
                            <span class="normal margin">Received, but less often than weekly</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "05", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("05"), new { @id = Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance5", @title = "(OASIS M2110) Caregiver ADL/IADL Assistance Frequency, No Assistance" })%>
                        <label for="<%= Model.TypeName %>_M2110FrequencyOfADLOrIADLAssistance5">
                            <span class="float_left">5 &#8211;</span>
                            <span class="normal margin">No assistance received</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance", "UK", data.AnswerOrEmptyString("M2110FrequencyOfADLOrIADLAssistance").Equals("UK"), new { @id = Model.TypeName + "_M2110FrequencyOfADLOrIADLAssistance6", @title = "(OASIS M2110) Caregiver ADL/IADL Assistance Frequency, Unknown" })%>
                        <label for="<%= Model.TypeName %>_M2110FrequencyOfADLOrIADLAssistance6">
                            <span class="float_left">UK &#8211;</span>
                            <span class="normal margin">Unknown </span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M2110')" title="More Information about M2110">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float_right">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
</script>