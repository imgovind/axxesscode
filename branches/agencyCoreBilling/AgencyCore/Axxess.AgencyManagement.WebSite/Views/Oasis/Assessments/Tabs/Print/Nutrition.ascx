<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<%  var Nutrition = data.AnswerArray("GenericNutrition"); %>
<%  var NutritionEnteralFeeding = data.AnswerArray("GenericNutritionEnteralFeeding"); %>
<%  var NutritionalHealth = data.AnswerArray("GenericNutritionalHealth"); %>
<%  var NutritionDiffect = data.AnswerArray("GenericNutritionDiffect"); %>
<%  var NutritionalReqs = data.AnswerArray("485NutritionalReqs"); %>
<%  var NutritionalReqsEnteral = data.AnswerArray("485NutritionalReqsEnteral"); %>
<%  var NutritionalReqsEnteralVia = data.AnswerArray("485NutritionalReqsEnteralVia"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    printview.addsection(
        printview.col(3,
            printview.checkbox("WNL (Within Normal Limits)",<%= Nutrition.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("Dysphagia",<%= Nutrition.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Appetite",<%= Nutrition.Contains("3").ToString().ToLower() %>,true)) +
        printview.col(4,
            printview.checkbox("Weight",<%= Nutrition.Contains("4").ToString().ToLower() %>,true) +
            printview.span("") + printview.checkbox("Loss",<%= data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Loss").ToString().ToLower() %>) +
            printview.checkbox("Gain",<%= data.AnswerOrEmptyString("GenericNutritionWeightGainLoss").Equals("Gain").ToString().ToLower() %>) +
            printview.checkbox("Diet",<%= Nutrition.Contains("5").ToString().ToLower() %>,true) +
            printview.span("") + printview.checkbox("Adequate",<%= data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Adequate").ToString().ToLower() %>) +
             printview.checkbox("Diet Type",<%= Nutrition.Contains("9").ToString().ToLower() %>,true) +
            printview.span("<%= Nutrition.Contains("9") ? data.AnswerOrEmptyString("GenericNutritionDietType") : string.Empty %>") +
            printview.checkbox("Inadequate",<%= data.AnswerOrEmptyString("GenericNutritionDietAdequate").Equals("Inadequate").ToString().ToLower() %>) +
            printview.checkbox("Enteral Feeding",<%= Nutrition.Contains("6").ToString().ToLower() %>,true) +
            printview.checkbox("NG",<%= (Nutrition.Contains("6") && NutritionEnteralFeeding.Contains("1")).ToString().ToLower() %>) +
            printview.checkbox("PEG",<%= (Nutrition.Contains("6") && NutritionEnteralFeeding.Contains("2")).ToString().ToLower() %>) +
            printview.checkbox("Dobhoff",<%= (Nutrition.Contains("6") && NutritionEnteralFeeding.Contains("3")).ToString().ToLower() %>) +
            printview.checkbox("Tube Placement Checked",<%= Nutrition.Contains("7").ToString().ToLower() %>,true) +
            printview.span("") + printview.checkbox("Residual Checked",<%= Nutrition.Contains("8").ToString().ToLower() %>,true) +
            printview.span("Amount: <%= data.AnswerOrDefault("GenericNutritionResidualCheckedAmount", "<span class='short blank'></span>").Clean() %>ml")) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericNutritionComments").Clean() %>",false,2),
        "Nutrition");
    printview.addsection(
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EWithout reason, has lost more than 10 lbs, in the last 3 months%3C/span%3E%3Cspan class=%22auto float_right%22%3E15%3C/span%3E",<%= NutritionalHealth.Contains("1").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EHas an illness or condition that made pt change the type and/or amount of food eaten%3C/span%3E%3Cspan class=%22auto float_right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("2").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EHas open decubitus, ulcer, burn or wound%3C/span%3E%3Cspan class=%22auto float_right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("3").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EEats fewer than 2 meals a day%3C/span%3E%3Cspan class=%22auto float_right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("4").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EHas a tooth/mouth problem that makes it hard to eat%3C/span%3E%3Cspan class=%22auto float_right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("5").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EHas 3 or more drinks of beer, liquor or wine almost every day%3C/span%3E%3Cspan class=%22auto float_right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("6").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EDoes not always have enough money to buy foods needed%3C/span%3E%3Cspan class=%22auto float_right%22%3E10%3C/span%3E",<%= NutritionalHealth.Contains("7").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EEats few fruits or vegetables, or milk products%3C/span%3E%3Cspan class=%22auto float_right%22%3E5%3C/span%3E",<%= NutritionalHealth.Contains("8").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EEats alone most of the time%3C/span%3E%3Cspan class=%22auto float_right%22%3E5%3C/span%3E",<%= NutritionalHealth.Contains("9").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3ETakes 3 or more prescribed or OTC medications a day%3C/span%3E%3Cspan class=%22auto float_right%22%3E5%3C/span%3E",<%= NutritionalHealth.Contains("10").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EIs not always physically able to cook and/or feed self and has no caregiver to assist%3C/span%3E%3Cspan class=%22auto float_right%22%3E5%3C/span%3E",<%= NutritionalHealth.Contains("11").ToString().ToLower() %>) +
        printview.checkbox("%3Cspan class=%22auto float_left%22%3EFrequently has diarrhea or constipation%3C/span%3E%3Cspan class=%22auto float_right%22%3E5%3C/span%3E",<%= NutritionalHealth.Contains("12").ToString().ToLower() %>) +
        printview.span("%3Cspan class=%22auto strong float_left%22%3ETotal%3C/span%3E%3Cspan class=%22auto float_right%22%3E<%= data.AnswerOrEmptyString("GenericGoodNutritionScore").Clean() %>%3C/span%3E") +
        printview.span("Nutritional Status Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericNutritionalStatusComments").Clean() %>",false,2) +
        printview.col(2,
            printview.checkbox("Non-compliant with prescribed diet",<%= NutritionDiffect.Contains("1") ? "true,true" : "false,false"%>) +
            printview.checkbox("Over/under weight by 10%",<%= NutritionDiffect.Contains("2") ? "true,true" : "false,false"%>) +
            printview.span("Meals prepared by:",true) +
            printview.span("<%= data.AnswerOrEmptyString("GenericMealsPreparedBy").Clean() %>",false,1)),
        "Nutritional Health Screen");
    printview.addsection(
        printview.col(4,
            printview.checkbox("Regular",<%= NutritionalReqs.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Mechanical Soft",<%= NutritionalReqs.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Heart Healthy",<%= NutritionalReqs.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Low Cholesterol",<%= NutritionalReqs.Contains("4").ToString().ToLower() %>) +
            printview.checkbox("Low Fat",<%= NutritionalReqs.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("<%= data.AnswerOrDefault("485NutritionalReqsSodiumAmount", "Low").Clean() %> Sodium",<%= NutritionalReqs.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("No Added Salt",<%= NutritionalReqs.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("<%= data.AnswerOrDefault("485NutritionalReqsCalorieADADietAmount", "<span class='short blank'></span>").Clean() %> Calorie ADA Diet",<%= NutritionalReqs.Contains("8").ToString().ToLower() %>) +
            printview.checkbox("No Concentrated Sweets",<%= NutritionalReqs.Contains("9").ToString().ToLower() %>) +
            printview.checkbox("Coumadin Diet",<%= NutritionalReqs.Contains("10").ToString().ToLower() %>) +
            printview.checkbox("Renal Diet",<%= NutritionalReqs.Contains("11").ToString().ToLower() %>) +
            printview.checkbox("Other: <%= NutritionalReqs.Contains("12") ? data.AnswerOrEmptyString("485NutritionalReqsPhyDietOtherName").Clean() : string.Empty %>",<%= NutritionalReqs.Contains("12").ToString().ToLower() %>) +
            printview.checkbox("Supplement: <%= NutritionalReqs.Contains("13") ? data.AnswerOrEmptyString("485NutritionalReqsSupplementType").Clean() : string.Empty %>",<%= NutritionalReqs.Contains("13").ToString().ToLower() %>) +
            printview.checkbox("Fluid Restriction: <%= NutritionalReqs.Contains("14") ? data.AnswerOrEmptyString("485NutritionalReqsFluidResAmount").Clean() : string.Empty %>ml/24 hours",<%= NutritionalReqs.Contains("14").ToString().ToLower() %>)) +
        printview.col(5,
            printview.checkbox("Enteral Nutrition",<%= NutritionalReqs.Contains("15").ToString().ToLower() %>) +
            printview.span("") + printview.span("") +
            printview.span("Formula: <%= data.AnswerOrEmptyString("485NutritionalReqsEnteralDesc").Clean() %>") +
            printview.span("Amount: <%= data.AnswerOrDefault("485NutritionalReqsEnteralAmount", "<span class='short blank'></span>") %>ml/hr") +
            printview.span("Per:") + 
            printview.checkbox("PEG",<%= NutritionalReqsEnteral.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("NG",<%= NutritionalReqsEnteral.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Dobhoff",<%= NutritionalReqsEnteral.Contains("3").ToString().ToLower() %>) +
            printview.checkbox("Other <%= data.AnswerOrEmptyString("485NutritionalReqsEnteralOtherName").Clean() %>",<%= NutritionalReqsEnteral.Contains("4").ToString().ToLower() %>) +
            printview.span("Via:") + 
            printview.span("") +
            printview.checkbox("Pump",<%= NutritionalReqsEnteralVia.Contains("1").ToString().ToLower() %>) +
            printview.checkbox("Gravity",<%= NutritionalReqsEnteralVia.Contains("2").ToString().ToLower() %>) +
            printview.checkbox("Bolus",<%= NutritionalReqsEnteralVia.Contains("3").ToString().ToLower() %>)) +
        printview.col(2,
            printview.checkbox("TPN",<%= NutritionalReqs.Contains("16").ToString().ToLower() %>) +
            printview.span("<%= data.AnswerOrEmptyString("485NutritionalReqsTPNAmount") %>@ml/hr via <%= data.AnswerOrEmptyString("485NutritionalReqsTPNVia").Clean() %>",false,1)),
        "Physician&#8217;s Orders or Diet Requirements");
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Cardiac.ascx", Model); %>
<%  } %>
</script>