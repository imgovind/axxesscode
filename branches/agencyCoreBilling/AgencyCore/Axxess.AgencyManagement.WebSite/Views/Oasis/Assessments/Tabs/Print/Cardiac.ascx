<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<%  var CardioVascular = data.AnswerArray("GenericCardioVascular"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() != 5 || Model.AssessmentTypeNum.ToInteger() != 8) { %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 > 5) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1500) " : string.Empty %>Symptoms in Heart Failure Patients: If patient has been diagnosed with heart failure, did the patient exhibit symptoms indicated by clinical heart failure guidelines (including dyspnea, orthopnea, edema, or weight gain) at any point since the previous OASIS assessment?",true) +
        printview.col(4,
            printview.checkbox("0 &#8211; No",<%= data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("00").ToString().ToLower() %>) +
            printview.checkbox("1 &#8211; Yes",<%= data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("01").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Not assessed",<%= data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("02").ToString().ToLower() %>) +
            printview.checkbox("NA &#8211; Patient does not have diagnosis of heart failure",<%= data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("NA").ToString().ToLower() %>)));
        <% if (data.AnswerOrEmptyString("M1500HeartFailureSymptons").Equals("01")) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1510) " : string.Empty %>Heart Failure Follow-up: If patient has been diagnosed with heart failure and has exhibited symptoms indicative of heart failure since the previous OASIS assessment, what action(s) has (have) been taken to respond? (Mark all that apply.)",true) +
        printview.checkbox("0 &#8211; No action taken",<%= data.AnswerOrEmptyString("M1510HeartFailureFollowupNoAction").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; Patient&#8217;s physician (or other primary care practitioner) contacted the same day",<%= data.AnswerOrEmptyString("M1510HeartFailureFollowupPhysicianCon").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; Patient advised to get emergency treatment (e.g., call 911 or go to emergency room)",<%= data.AnswerOrEmptyString("M1510HeartFailureFollowupAdvisedEmg").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; Implemented physician-ordered patient-specific established parameters for treatment",<%= data.AnswerOrEmptyString("M1510HeartFailureFollowupParameters").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; Patient education or other clinical interventions",<%= data.AnswerOrEmptyString("M1510HeartFailureFollowupInterventions").Equals("1").ToString().ToLower() %>) +
        printview.checkbox("5 &#8211; Obtained change in care plan orders (e.g., increased monitoring by agency, change in visit frequency, telehealth, etc.)",<%= data.AnswerOrEmptyString("M1510HeartFailureFollowupChange").Equals("1").ToString().ToLower() %>));
        <%  } %>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    printview.addsection(
        printview.col(4,
            printview.checkbox("WNL (Within Normal Limits)",<%= CardioVascular.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("<strong>Heart Rhythm:</strong> <%= CardioVascular.Contains("2") ? (data.AnswerOrEmptyString("GenericHeartSoundsType").Equals("1") ? "Regular/ WNL" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSoundsType").Equals("2") ? "Tachycardia" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSoundsType").Equals("3") ? "Bradycardia" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSoundsType").Equals("4") ? "Arrhythmia/ Dysrhythmia" : string.Empty) + (data.AnswerOrEmptyString("GenericHeartSoundsType").Equals("5") ? "Other" : string.Empty) : string.Empty %>",<%= CardioVascular.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("Cap Refill:",<%= CardioVascular.Contains("3").ToString().ToLower() %>,true) +
            printview.col(2,
                printview.checkbox("&#60;3 sec",<%= (CardioVascular.Contains("3") && data.AnswerOrEmptyString("GenericCapRefillLessThan3").Equals("1")).ToString().ToLower() %>) +
                printview.checkbox("&#62;3 sec",<%= (CardioVascular.Contains("3") && data.AnswerOrEmptyString("GenericCapRefillLessThan3").Equals("0")).ToString().ToLower() %>))) +
        printview.col(5,
            printview.checkbox("Pulses:",<%= CardioVascular.Contains("4").ToString().ToLower() %>,true) +
            printview.span("Radial <%= CardioVascular.Contains("4") ? (data.AnswerOrEmptyString("GenericCardiovascularRadial").Equals("1") ? "1+ Weak" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularRadial").Equals("2") ? "2+ Normal" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularRadial").Equals("3") ? "3+ Strong" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularRadial").Equals("4") ? "4+ Bounding" : string.Empty) : string.Empty %>") +
            printview.checkbox("Bilateral",<%= (CardioVascular.Contains("4") && data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("0")).ToString().ToLower() %>) +
            printview.checkbox("Left",<%= (CardioVascular.Contains("4") && data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("1")).ToString().ToLower() %>) +
            printview.checkbox("Right",<%= (CardioVascular.Contains("4") && data.AnswerOrEmptyString("GenericCardiovascularRadialPosition").Equals("2")).ToString().ToLower() %>) +
            printview.span("") +
            printview.span("Pedal <%= CardioVascular.Contains("4") ? (data.AnswerOrEmptyString("GenericCardiovascularPedal").Equals("1") ? "1+ Weak" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularPedal").Equals("2") ? "2+ Normal" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularPedal").Equals("3") ? "3+ Strong" : string.Empty) + (data.AnswerOrEmptyString("GenericCardiovascularPedal").Equals("4") ? "4+ Bounding" : string.Empty) : string.Empty %>") +
            printview.checkbox("Bilateral",<%= (CardioVascular.Contains("4") && data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("0")).ToString().ToLower() %>) +
            printview.checkbox("Left",<%= (CardioVascular.Contains("4") && data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("1")).ToString().ToLower() %>) +
            printview.checkbox("Right",<%= (CardioVascular.Contains("4") && data.AnswerOrEmptyString("GenericCardiovascularPedalPosition").Equals("2")).ToString().ToLower() %>)) +
        printview.col(5,
            printview.checkbox("<strong>Edema:</strong> <%= CardioVascular.Contains("5") ? data.AnswerOrEmptyString("GenericEdemaLocation").Clean() + " (location)" : string.Empty %>",<%= CardioVascular.Contains("5").ToString().ToLower() %>) +
            printview.checkbox("Pitting <%= CardioVascular.Contains("5") ? (data.AnswerOrEmptyString("GenericEdemaPitting").Equals("1") ? "1+" : string.Empty) + (data.AnswerOrEmptyString("GenericEdemaPitting").Equals("2") ? "2+" : string.Empty) + (data.AnswerOrEmptyString("GenericEdemaPitting").Equals("3") ? "3+" : string.Empty) + (data.AnswerOrEmptyString("GenericEdemaPitting").Equals("4") ? "4+" : string.Empty) : string.Empty %>",<%= (CardioVascular.Contains("5") && data.AnswerOrEmptyString("GenericPittingEdemaType").Equals("1")).ToString().ToLower() %>) +
            printview.span("") + printview.span("") + printview.span("") + printview.span("") +
            printview.checkbox("Nonpitting <%= CardioVascular.Contains("5") ? (data.AnswerOrEmptyString("GenericEdemaNonPitting").Equals("1") ? "N/A" : string.Empty) + (data.AnswerOrEmptyString("GenericEdemaNonPitting").Equals("2") ? "Mild" : string.Empty) + (data.AnswerOrEmptyString("GenericEdemaNonPitting").Equals("3") ? "Moderate" : string.Empty) + (data.AnswerOrEmptyString("GenericEdemaNonPitting").Equals("4") ? "Severe" : string.Empty) : string.Empty %>",<%= (CardioVascular.Contains("5") && data.AnswerOrEmptyString("GenericPittingEdemaType").Equals("2")).ToString().ToLower() %>) +
            printview.checkbox("Bilateral",<%= (CardioVascular.Contains("5") && data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("0")).ToString().ToLower() %>) +
            printview.checkbox("Left",<%= (CardioVascular.Contains("5") && data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("1")).ToString().ToLower() %>) +
            printview.checkbox("Right",<%= (CardioVascular.Contains("5") && data.AnswerOrEmptyString("GenericEdemaNonPittingPosition").Equals("2")).ToString().ToLower() %>)) +
        printview.col(3,
            printview.checkbox("<strong>Dizziness:</strong> <%= CardioVascular.Contains("6") ? data.AnswerOrEmptyString("GenericDizzinessDesc").Clean() : string.Empty %>",<%= CardioVascular.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("<strong>Chest Pain:</strong> <%= CardioVascular.Contains("7") ? data.AnswerOrEmptyString("GenericChestPainDesc").Clean() : string.Empty %>",<%= CardioVascular.Contains("7").ToString().ToLower() %>) +
            printview.checkbox("<strong>Neck Vein Distention:</strong> <%= CardioVascular.Contains("8") ? data.AnswerOrEmptyString("GenericNeckVeinDistentionDesc").Clean() : string.Empty %>",<%= CardioVascular.Contains("8").ToString().ToLower() %>)) +
        printview.col(2,
            printview.col(2,
                printview.span("<strong>Pacemaker:</strong>") +
                printview.span("<%= data.AnswerOrEmptyString("GenericPacemakerDate").Clean() %>",false,1)) +
            printview.col(2,
                printview.span("<strong>AICD:</strong>") +
                printview.span("<%= data.AnswerOrEmptyString("GenericAICDDate").Clean() %>",false,1))) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericCardiovascularComments").Clean() %>",false,2),
        "Cardiovascular");
    <%  Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Cardiac.ascx", Model); %>
    <%  } %>
<%  } %>
</script>