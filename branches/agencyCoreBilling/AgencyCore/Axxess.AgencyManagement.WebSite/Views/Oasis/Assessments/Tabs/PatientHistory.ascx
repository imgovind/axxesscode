<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "PatientHistoryForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id, new { @id = Model.TypeName + "_Id" })%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit", new { @id = Model.TypeName + "_Action" })%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId, new { @id = Model.TypeName + "_PatientGuid" })%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId, new { @id = Model.TypeName + "_EpisodeId" })%>
    <%= Html.Hidden("assessment", Model.TypeName, new { @id = Model.TypeName + "_AssessmentType" })%>
    <%= Html.Hidden("categoryType", "PatientHistory")%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (!Model.TypeName.Contains("NonOasis")) { %>
        <ul class="float_right">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <fieldset class="half float_left loc485">
        <legend>Allergies (Locator #17)</legend>
        <div class="column">
            <div class="row">
                <%= Html.Hidden(Model.TypeName + "_485Allergies", " ", new { @id = Model.TypeName + "_485AllergiesHidden" })%>
                <%= Html.RadioButton(Model.TypeName + "_485Allergies", "No", data.AnswerOrEmptyString("485Allergies").Equals("No"), new { @id = Model.TypeName + "Allergies_NKA", @title = "(485 Locator 17) Allergies, No Known Allergies", @class = "radio" })%>
                <label for="<%= Model.TypeName %>_Allergies_NKA">NKA (Food/Drugs/Latex)</label>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <%= Html.RadioButton(Model.TypeName + "_485Allergies", "Yes", data.AnswerOrEmptyString("485Allergies").Equals("Yes"), new { @id = Model.TypeName + "Allergies_Specify", @title = "(485 Locator 17) Allergies, Allergic To", @class = "radio" })%>
                <label for="<%= Model.TypeName %>_Allergies_Specify">Allergic to:</label>
                <%= Html.TextArea(Model.TypeName + "_485AllergiesDescription", data.AnswerOrEmptyString("485AllergiesDescription"), new { @id = Model.TypeName + "_485AllergiesDescription", @title = "(485 Locator 17) Allergies" })%>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float_right loc485">
        <legend>Vital Sign Parameters:</legend>
        <div class="column">
            <div class="row">
                <label class="float_left">Temperature</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_GenericTempGreaterThan">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericTempGreaterThan", data.AnswerOrEmptyString("GenericTempGreaterThan"), new { @id = Model.TypeName + "_GenericTempGreaterThan", @title = "(485 Locator 21) Orders, Temperature Maximum", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericTempLessThan">or less than (&#60;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericTempLessThan", data.AnswerOrEmptyString("GenericTempLessThan"), new { @id = Model.TypeName + "_GenericTempLessThan", @title = "(485 Locator 21) Orders, Temperature Minimum", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float_left">Pulse</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_GenericPulseGreaterThan">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericPulseGreaterThan", data.AnswerOrEmptyString("GenericPulseGreaterThan"), new { @id = Model.TypeName + "_GenericPulseGreaterThan", @title = "(485 Locator 21) Orders, Pulse Maximum", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericPulseLessThan">or less than (&#60;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericPulseLessThan", data.AnswerOrEmptyString("GenericPulseLessThan"), new { @id = Model.TypeName + "_GenericPulseLessThan", @title = "(485 Locator 21) Orders, Pulse Minimum", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float_left">Respirations</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_GenericRespirationGreaterThan">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericRespirationGreaterThan", data.AnswerOrEmptyString("GenericRespirationGreaterThan"), new { @id = Model.TypeName + "_GenericRespirationGreaterThan", @title = "(485 Locator 21) Orders, Respiration Maximum", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericRespirationLessThan">or less than (&#60;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericRespirationLessThan", data.AnswerOrEmptyString("GenericRespirationLessThan"), new { @id = Model.TypeName + "_GenericRespirationLessThan", @title = "(485 Locator 21) Orders, Respiration Minimum", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float_left">Systolic BP</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_GenericSystolicBPGreaterThan">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericSystolicBPGreaterThan", data.AnswerOrEmptyString("GenericSystolicBPGreaterThan"), new { @id = Model.TypeName + "_GenericSystolicBPGreaterThan", @title = "(485 Locator 21) Orders, Systolic Blood Pressure Maximum", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericSystolicBPLessThan">or less than (&#60;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericSystolicBPLessThan", data.AnswerOrEmptyString("GenericSystolicBPLessThan"), new { @id = Model.TypeName + "_GenericRespirationGreaterThan", @title = "(485 Locator 21) Orders, Systolic Blood Pressure Minimum", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float_left">Diastolic BP</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_GenericDiastolicBPGreaterThan">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericDiastolicBPGreaterThan", data.AnswerOrEmptyString("GenericDiastolicBPGreaterThan"), new { @id = Model.TypeName + "_GenericDiastolicBPGreaterThan", @title = "(485 Locator 21) Orders, Diastolic Blood Pressure Maximum", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericDiastolicBPLessThan">or less than (&#60;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericDiastolicBPLessThan", data.AnswerOrEmptyString("GenericDiastolicBPLessThan"), new { @id = Model.TypeName + "_GenericDiastolicBPLessThan", @title = "(485 Locator 21) Orders, Diastolic Blood Pressure Minimum", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float_left">O<sub>2</sub> Sat (percent)</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_Generic02SatLessThan">less than (&#60;)</label>
                    <%= Html.TextBox(Model.TypeName + "_Generic02SatLessThan", data.AnswerOrEmptyString("Generic02SatLessThan"), new { @id = Model.TypeName + "_Generic02SatLessThan", @title = "(485 Locator 21) Orders, Oxygen Saturation Minimum", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float_left">Fasting blood sugar</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_GenericFastingBloodSugarGreaterThan">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericFastingBloodSugarGreaterThan", data.AnswerOrEmptyString("GenericFastingBloodSugarGreaterThan"), new { @id = Model.TypeName + "_GenericFastingBloodSugarGreaterThan", @title = "(485 Locator 21) Orders, Fasting Blood Sugar Maximum", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericFastingBloodSugarLessThan">or less than (&#60;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericFastingBloodSugarLessThan", data.AnswerOrEmptyString("GenericFastingBloodSugarLessThan"), new { @id = Model.TypeName + "_GenericFastingBloodSugarLessThan", @title = "(485 Locator 21) Orders, Fasting Blood Sugar Minimum", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float_left">Random blood sugar</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_GenericRandomBloddSugarGreaterThan">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericRandomBloddSugarGreaterThan", data.AnswerOrEmptyString("GenericRandomBloddSugarGreaterThan"), new { @id = Model.TypeName + "_GenericRandomBloddSugarGreaterThan", @title = "(485 Locator 21) Orders, Random Blood Sugar Maximum", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericRandomBloodSugarLessThan">or less than (&#60;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericRandomBloodSugarLessThan", data.AnswerOrEmptyString("GenericRandomBloodSugarLessThan"), new { @id = Model.TypeName + "_GenericRandomBloodSugarLessThan", @title = "(485 Locator 21) Orders, Random Blood Sugar Minimum", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float_left">Weight Gain/Loss (lbs/7 days)</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_GenericWeightGreaterThan">greater than (&#62;)</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericWeightGreaterThan", data.AnswerOrEmptyString("GenericWeightGreaterThan"), new { @id = Model.TypeName + "_GenericWeightGreaterThan", @title = "(485 Locator 21) Orders, Weight Change Maximum", @class = "vitals" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset class="half float_left">
        <legend>Vital Signs</legend>
        <div class="column">
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPulseApical" class="float_left">Apical Pulse:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.TypeName + "_GenericPulseApical", data.AnswerOrEmptyString("GenericPulseApical"), new { @id = Model.TypeName + "_GenericPulseApical", @title = "(Optional) Apical Pulse", @class = "vitals" })%>
                    <%= string.Format("<input id='{0}_GenericPulseApicalRegular' class='radio' name='{0}_GenericPulseApicalRegular' value='1' type='checkbox' {0} title ='(Optional) Apical Pulse, Regular' />", data.AnswerOrEmptyString("GenericPulseApicalRegular").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericPulseApicalRegular">(Reg)</label>
                    <%= string.Format("<input id='{0}_GenericPulseApicalIrregular' class='radio' name='{0}_GenericPulseApicalRegular' value='2' type='checkbox' {0} title ='(Optional) Apical Pulse, Irregular' />", data.AnswerOrEmptyString("GenericPulseApicalRegular").Equals("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericPulseApicalIrregular">(Irreg)</label>
                </div> 
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericPulseRadial" class="float_left">Radial Pulse:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.TypeName + "_GenericPulseRadial", data.AnswerOrEmptyString("GenericPulseRadial"), new { @id = Model.TypeName + "_GenericPulseRadial", @title = "(Optional) Radial Pulse", @class = "vitals" })%>
                    <%= string.Format("<input id='{0}_GenericPulseRadialRegular' class='radio' name='{0}_GenericPulseRadialRegular' value='1' type='checkbox' {0} title ='(Optional) Radial Pulse, Regular' />", data.AnswerOrEmptyString("GenericPulseRadialRegular").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericPulseRadialRegular">(Reg)</label>
                    <%= string.Format("<input id='{0}_GenericPulseRadialIrregular' class='radio' name='{0}_GenericPulseRadialRegular' value='2' type='checkbox' {0} title ='(Optional) Radial Pulse, Irregular' />", data.AnswerOrEmptyString("GenericPulseRadialRegular").Equals("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericPulseRadialIrregular">(Irreg)</label>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericHeight" class="float_left">Height:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.TypeName + "_GenericHeight", data.AnswerOrEmptyString("GenericHeight"), new { @id = Model.TypeName + "_GenericHeight", @title = "(Optional) Height", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericWeight" class="float_left">Weight:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.TypeName + "_GenericWeight", data.AnswerOrEmptyString("GenericWeight"), new { @id = Model.TypeName + "_GenericWeight", @title = "(Optional) Weight", @class = "vitals" })%>
                    <%= string.Format("<input id='{0}_GenericWeightActual' class='radio' name='{0}_GenericWeightActualStated' value='1' type='checkbox' {0} title ='(Optional) Weight, Actual' />", data.AnswerOrEmptyString("GenericWeightActualStated").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericWeightActual">Actual</label>
                    <%= string.Format("<input id='{0}_GenericWeightStated' class='radio' name='{0}_GenericWeightActualStated' value='2' type='checkbox' {0} title ='(Optional) Weight, Stated' />", data.AnswerOrEmptyString("GenericWeightActualStated").Equals("2").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_GenericWeightStated">Stated</label>
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericTemp" class="float_left">Temp:</label>
                <div class="float_right"><%= Html.TextBox(Model.TypeName + "_GenericTemp", data.AnswerOrEmptyString("GenericTemp"), new { @id = Model.TypeName + "_GenericTemp", @title = "(Optional) Temperature", @class = "vitals" })%></div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_GenericResp" class="float_left">Resp:</label>
                <div class="float_right"><%= Html.TextBox(Model.TypeName + "_GenericResp", data.AnswerOrEmptyString("GenericResp"), new { @id = Model.TypeName + "_GenericResp", @title = "(Optional) Respirations", @class = "vitals" })%></div>
            </div>
            <div class="row">
                <label class="float_left">BP Lying</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_GenericBPLeftLying">Left</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPLeftLying", data.AnswerOrEmptyString("GenericBPLeftLying"), new { @id = Model.TypeName + "_GenericBPLeftLying", @title = "(Optional) Blood Pressure, Lying/Left", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericBPRightLying">Right</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPRightLying", data.AnswerOrEmptyString("GenericBPRightLying"), new { @id = Model.TypeName + "_GenericBPRightLying", @title = "(Optional) Blood Pressure, Lying/Right", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float_left">BP Sitting</label>
                <div class="float_right">
                    <label for="<%= Model.TypeName %>_GenericBPLeftSitting">Left</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPLeftSitting", data.AnswerOrEmptyString("GenericBPLeftSitting"), new { @id = Model.TypeName + "_GenericBPLeftSitting", @title = "(Optional) Blood Pressure, Sitting/Left", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericBPRightSitting">Right</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPRightSitting", data.AnswerOrEmptyString("GenericBPRightSitting"), new { @id = Model.TypeName + "_GenericBPRightSitting", @title = "(Optional) Blood Pressure, Sitting/Right", @class = "vitals" })%>
                </div>
            </div>
            <div class="row">
                <label class="float_left">BP Standing</label>
                <div class="float_right oasis">
                    <label for="<%= Model.TypeName %>_GenericBPLeftStanding">Left</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPLeftStanding", data.AnswerOrEmptyString("GenericBPLeftStanding"), new { @id = Model.TypeName + "_GenericBPLeftStanding", @title = "(Optional) Blood Pressure, Standing/Left", @class = "vitals" })%>
                    <label for="<%= Model.TypeName %>_GenericBPRightStanding">Right</label>
                    <%= Html.TextBox(Model.TypeName + "_GenericBPRightStanding", data.AnswerOrEmptyString("GenericBPRightStanding"), new { @id = Model.TypeName + "_GenericBPRightStanding", @title = "(Optional) Blood Pressure, Standing/Right", @class = "vitals" })%>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float_right">
        <legend>Immunizations</legend>
        <div class="column">
            <div class="row">
                <label class="float_left">Pneumonia</label>
                <%= Html.Hidden(Model.TypeName + "_485Pnemonia", " ", new { @id = Model.TypeName + "_485PnemoniaHidden" })%>
                <div class="float_right">
                    <%  var Pnemonia = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No"},
                            new SelectListItem { Text = "Unknown", Value = "UK" }
                        }, "Value", "Text", data.AnswerOrEmptyString("485Pnemonia")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485Pnemonia", Pnemonia, new { @class = "width100" })%>
                    <input type="date" name="<%= Model.TypeName %>_485PnemoniaDate" value="<%= data.AnswerOrEmptyString("485PnemoniaDate") %>" id="<%= Model.TypeName %>_485PnemoniaDate" class="width100" />
                </div>
            </div>
            <div class="row">
                <label class="float_left">Flu</label>
                <%= Html.Hidden(Model.TypeName + "_485Flu")%>
                <div class="float_right">
                    <%  var Flu = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No"},
                            new SelectListItem { Text = "Unknown", Value = "UK" }
                        }, "Value", "Text", data.AnswerOrEmptyString("485Flu")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485Flu", Flu, new { @class = "width100" })%>
                    <input type="date" name="<%= Model.TypeName %>_485FluDate" value="<%= data.AnswerOrEmptyString("485FluDate") %>" id="<%= Model.TypeName %>_485FluDate" class="width100" />
                </div>
            </div>
            <div class="row">
                <label class="float_left">TB</label>
                <%= Html.Hidden(Model.TypeName + "_485TB")%>
                <div class="float_right">
                    <%  var TB = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No"},
                            new SelectListItem { Text = "Unknown", Value = "UK" }
                        }, "Value", "Text", data.AnswerOrEmptyString("485TB")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485TB", TB, new { @class = "width100" })%>
                    <input type="date" name="<%= Model.TypeName %>_485TBDate" value="<%= data.AnswerOrEmptyString("485TBDate") %>" id="<%= Model.TypeName %>_485TBDate" class="width100" />
                </div>
            </div>
            <div class="row">
                <label class="float_left">TB Exposure</label>
                <%= Html.Hidden(Model.TypeName + "_485TBExposure")%>
                <div class="float_right">
                    <%  var TBExposure = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No"},
                            new SelectListItem { Text = "Unknown", Value = "UK" }
                        }, "Value", "Text", data.AnswerOrEmptyString("485TBExposure")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485TBExposure", TBExposure, new { @class = "width100" })%>
                    <input type="date" name="<%= Model.TypeName %>_485TBExposureDate" value="<%= data.AnswerOrEmptyString("485TBExposureDate") %>" id="<%= Model.TypeName %>_485TBExposureDate" class="width100" />
                </div>
            </div>
            <div class="row">
                <div class="align_center"><label class="strong">Additional Immunizations</label></div>
            </div>
            <div class="row">
                <div class="float_left"><%= Html.TextBox(Model.TypeName + "_485AdditionalImmunization1Name", data.AnswerOrEmptyString("485AdditionalImmunization1Name"), new { @id = Model.TypeName + "_485AdditionalImmunization1Name", @class = "fill" }) %></div>
                <%= Html.Hidden(Model.TypeName + "_485AdditionalImmunization1")%>
                <div class="float_right">
                    <%  var AdditionalImmunization1 = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No"},
                            new SelectListItem { Text = "Unknown", Value = "UK" }
                        }, "Value", "Text", data.AnswerOrEmptyString("485AdditionalImmunization1")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485AdditionalImmunization1", AdditionalImmunization1, new { @class = "width100" })%>
                    <input type="date" name="<%= Model.TypeName %>_485AdditionalImmunization1Date" value="<%= data.AnswerOrEmptyString("485AdditionalImmunization1Date") %>" id="<%= Model.TypeName %>_485AdditionalImmunization1Date" class="width100" />
                </div>
            </div>
            <div class="row">
                <div class="float_left"><%= Html.TextBox(Model.TypeName + "_485AdditionalImmunization2Name", data.AnswerOrEmptyString("485AdditionalImmunization2Name"), new { @id = Model.TypeName + "_485AdditionalImmunization2Name", @class = "fill" }) %></div>
                <%= Html.Hidden(Model.TypeName + "_485AdditionalImmunization2")%>
                <div class="float_right">
                    <%  var AdditionalImmunization2 = new SelectList(new[] {
                            new SelectListItem { Text = "", Value = "" },
                            new SelectListItem { Text = "Yes", Value = "Yes" },
                            new SelectListItem { Text = "No", Value = "No"},
                            new SelectListItem { Text = "Unknown", Value = "UK" }
                        }, "Value", "Text", data.AnswerOrEmptyString("485AdditionalImmunization2")); %>
                    <%= Html.DropDownList(Model.TypeName + "_485AdditionalImmunization2", AdditionalImmunization2, new { @class = "width100" })%>
                    <input type="date" name="<%= Model.TypeName %>_485AdditionalImmunization2Date" value="<%= data.AnswerOrEmptyString("485AdditionalImmunization2Date") %>" id="<%= Model.TypeName %>_485AdditionalImmunization2Date" class="width100" />
                </div>
            </div>
            <div class="row">
                <label for="<%= Model.TypeName %>_485ImmunizationComments" class="strong">Comments</label>
                <div><%= Html.TextArea(Model.TypeName + "_485ImmunizationComments", data.AnswerOrEmptyString("485ImmunizationComments"), 5, 70, new { @id = Model.TypeName + "_485ImmunizationComments" }) %></div>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 4) { %>
    <fieldset class="oasis">
        <legend>Inpatient Discharges in the Past 14 Days</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1000">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1000" class="green" onclick="Oasis.ToolTip('M1000')">(M1000)</a>
                    From which of the following Inpatient Facilities was the patient dischargedduring the past 14 days? (Mark all that apply)
                </label>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesLTC", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesLTCHidden" })%>
                        <%= string.Format("<input title='(OASIS M1000) Recent Inpatient Discharges, Nursing Facility' id='{0}_M1000InpatientFacilitiesLTC' name='{0}_M1000InpatientFacilitiesLTC' value='1' class='M1000' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesLTC">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Long-term nursing facility (NF)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesSNF", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesSNFHidden" })%>
                        <%= string.Format("<input title='(OASIS M1000) Recent Inpatient Discharges, Skilled Nursing Facility' id='{0}_M1000InpatientFacilitiesSNF' name='{0}_M1000InpatientFacilitiesSNF' value='1' class='M1000' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesSNF">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Skilled nursing facility (SNF / TCU)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesIPPS", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesIPPSHidden" })%>
                        <%= string.Format("<input title='(OASIS M1000) Recent Inpatient Discharges, Short-stay Acute Hospital' id='{0}_M1000InpatientFacilitiesIPPS' name='{0}_M1000InpatientFacilitiesIPPS' value='1' class='M1000' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1000InpatientFacilitiesIPPS").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesIPPS">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">Short-stay acute hospital (IPPS)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesLTCH", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesLTCHHidden" })%>
                        <%= string.Format("<input title='(OASIS M1000) Recent Inpatient Discharges, Long-term Care Hospital' id='{0}_M1000InpatientFacilitiesLTCH' name='{0}_M1000InpatientFacilitiesLTCH' value='1' class='M1000' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesLTCH">
                            <span class="float_left">4 &#8211;</span>
                            <span class="normal margin">Long-term care hospital (LTCH)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesIRF", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesIRFHidden" })%>
                        <%= string.Format("<input title='(OASIS M1000) Recent Inpatient Discharges, Rehabilitation Hospital/Unit' id='{0}_M1000InpatientFacilitiesIRF' name='{0}_M1000InpatientFacilitiesIRF' value='1' class='M1000' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesIRF">
                            <span class="float_left">5 &#8211;</span>
                            <span class="normal margin">Inpatient rehabilitation hospital or unit (IRF)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesPhych", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesPhychHidden" })%>
                        <%= string.Format("<input title='(OASIS M1000) Recent Inpatient Discharges, Psychiatric Hospital/Unit' id='{0}_M1000InpatientFacilitiesPhych' name='{0}_M1000InpatientFacilitiesPhych' value='1' class='M1000' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1000InpatientFacilitiesPhych").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesPhych">
                            <span class="float_left">6 &#8211;</span>
                            <span class="normal margin">Psychiatric hospital or unit</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesOTHR", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesOTHRHidden" })%>
                        <%= string.Format("<input title='(OASIS M1000) Recent Inpatient Discharges, Other' id='{0}_M1000InpatientFacilitiesOTHR' name='{0}_M1000InpatientFacilitiesOTHR' value='1' class='M1000' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesOTHR">
                            <span class="float_left">7 &#8211;</span>
                            <span class="normal margin">Other</span>
                        </label>
                        <div id="<%= Model.TypeName %>_M1000InpatientFacilitiesOTHRMore" class="normal margin">
                            <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesOther"><em>(Specify)</em></label>
                            <%= Html.TextBox(Model.TypeName + "_M1000InpatientFacilitiesOther", data.AnswerOrEmptyString("M1000InpatientFacilitiesOther"), new { @id = Model.TypeName + "_M1000InpatientFacilitiesOther", @title = "(OASIS M1000) Recent Inpatient Discharges, Specify Other" })%>
                        </div>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1000InpatientFacilitiesNone", " ", new { @id = Model.TypeName + "_M1000InpatientFacilitiesNoneHidden" })%>
                        <input type="hidden" name="<%= Model.TypeName %>_M1000InpatientFacilitiesNone" value="" />
                        <%= string.Format("<input title='(OASIS M1000) Recent Inpatient Discharges, No Recent Inpatient Discharges' id='{0}_M1000InpatientFacilitiesNone' name='{0}_M1000InpatientFacilitiesNone' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1000InpatientFacilitiesNone").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1000InpatientFacilitiesNone">
                            <span class="float_left">NA &#8211;</span>
                            <span class="normal margin">Patient was not discharged from an inpatient facility</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1000')" title="More Information about M1000">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Discharges Date</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1005">
                <label for="<%= Model.TypeName %>_M1005InpatientDischargeDate" class="float_left"><a href="javascript:void(0)" title="More Information about M1005" class="green" onclick="Oasis.ToolTip('M1005')">(M1005)</a> Inpatient Discharge Date (most recent)</label>
                <div class="float_right">
                    <input type="date" name="<%= Model.TypeName %>_M1005InpatientDischargeDate" value="<%= data.AnswerOrEmptyString("M1005InpatientDischargeDate") %>" id="<%= Model.TypeName %>_M1005InpatientDischargeDate" title="(OASIS M1005) Inpatient Discharge Date" />
                </div>
                <div class="clear"></div>
                <div class="align_right">
                    <%= Html.Hidden(Model.TypeName + "_M1005InpatientDischargeDateUnknown", " ", new { @id = Model.TypeName + "_M1005InpatientDischargeDateUnknownHidden" })%>
                    <%= string.Format("<input title='(OASIS M1005) Inpatient Discharge Date, Unknown' id='{0}_M1005InpatientDischargeDateUnknown' name='{0}_M1005InpatientDischargeDateUnknown' class='radio' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1005InpatientDischargeDateUnknown").Equals("1").ToChecked()) %>
                    <label for="<%= Model.TypeName %>_M1005InpatientDischargeDateUnknown">UK &#8211; Unknown</label>
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1000')" title="More Information about M1005">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Inpatient Diagnosis</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1010">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1010" class="green" onclick="Oasis.ToolTip('M1010')">(M1010)</a>
                    List each Inpatient Diagnosis and ICD-9-C M code at the level of highest specificity for only those conditions treated during an inpatient stay within the last 14 days
                    <em>(no E-codes, or V-codes)</em>
                </label>
                <ul class="sm-diagnoses-list">
                    <li class="title">
                        <span class="alphali"></span>
                        <span class="diagnosis-name">Inpatient Facility Diagnosis</span>
                        <span class="diagnosis-code">ICD-9-C M Code</span>
                    </li>
                    <li>
                        <span class="alphali">a.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis1", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis1"), new { @class = "diagnosis", @title = "(OASIS M1010) Inpatient Diagnosis" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode1", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode1"), new { @class = "icd", @title = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">b.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis2", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis2"), new { @class = "diagnosis", @title = "(OASIS M1010) Inpatient Diagnosis" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode2", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode2"), new { @class = "icd", @title = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">c.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis3", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis3"), new { @class = "diagnosis", @title = "(OASIS M1010) Inpatient Diagnosis" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode3", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode3"), new { @class = "icd", @title = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">d.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis4", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis4"), new { @class = "diagnosis", @title = "(OASIS M1010) Inpatient Diagnosis" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode4", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode4"), new { @class = "icd", @title = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">e.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis5", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis5"), new { @class = "diagnosis", @title = "(OASIS M1010) Inpatient Diagnosis" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode5", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode5"), new { @class = "icd", @title = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">f.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosis6", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosis6"), new { @class = "diagnosis", @title = "(OASIS M1010) Inpatient Diagnosis" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1010InpatientFacilityDiagnosisCode6", data.AnswerOrEmptyString("M1010InpatientFacilityDiagnosisCode6"), new { @class = "icd", @title = "(OASIS M1010) Inpatient Diagnosis, ICD-9-C M Code" })%></span>
                    </li>
                </ul>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1010')" title="More Information about M1010">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Inpatient Procedure</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1012">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1012" class="green" onclick="Oasis.ToolTip('M1012')">(M1012)</a>
                    List each Inpatient Procedure and the associated ICD-9-C M procedure code relevant to the plan of care.
                </label>
                <ul class="sm-diagnoses-list M1012">
                    <li class="title">
                        <span class="alphali"></span>
                        <span class="diagnosis-name">Inpatient Procedure (Locator #12)</span>
                        <span class="diagnosis-code">Procedure Code</span>
                    </li>
                    <li>
                        <span class="alphali">a.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedure1", data.AnswerOrEmptyString("M1012InpatientFacilityProcedure1"), new { @class = "procedureDiagnosis", @title = "(OASIS M1012) Inpatient Procedure" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedureCode1", data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode1"), new { @class = "procedureICD", @title = "(OASIS M1012) Inpatient Procedure, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">b.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedure2", data.AnswerOrEmptyString("M1012InpatientFacilityProcedure2"), new { @class = "procedureDiagnosis", @title = "(OASIS M1012) Inpatient Procedure" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedureCode2", data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode2"), new { @class = "procedureICD", @title = "(OASIS M1012) Inpatient Procedure, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">c.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedure3", data.AnswerOrEmptyString("M1012InpatientFacilityProcedure3"), new { @class = "procedureDiagnosis", @title = "(OASIS M1012) Inpatient Procedure" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedureCode3", data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode3"), new { @class = "procedureICD", @title = "(OASIS M1012) Inpatient Procedure, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">d.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedure4", data.AnswerOrEmptyString("M1012InpatientFacilityProcedure4"), new { @class = "procedureDiagnosis", @title = "(OASIS M1012) Inpatient Procedure" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1012InpatientFacilityProcedureCode4", data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCode4"), new { @class = "procedureICD", @title = "(OASIS M1012) Inpatient Procedure, ICD-9-C M Code" })%></span>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1012InpatientFacilityProcedureCodeNotApplicable", " ", new { @id = Model.TypeName + "_M1012InpatientFacilityProcedureCodeNotApplicableHidden" })%>
                        <%= string.Format("<input title='(OASIS M1012) Inpatient Procedure, Not Applicable' id='{0}_M1012InpatientFacilityProcedureCodeNotApplicable' name='{0}_M1012InpatientFacilityProcedureCodeNotApplicable' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCodeNotApplicable").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1012InpatientFacilityProcedureCodeNotApplicable">
                            <span class="float_left">NA &#8211;</span>
                            <span class="normal margin">Not applicable</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1012InpatientFacilityProcedureCodeUnknown", " ", new { @id = Model.TypeName + "_M1012InpatientFacilityProcedureCodeUnknownHidden" })%>
                        <%= string.Format("<input title='(OASIS M1012) Inpatient Procedure, Unknown' id='{0}_M1012InpatientFacilityProcedureCodeUnknown' name='{0}_M1012InpatientFacilityProcedureCodeUnknown' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1012InpatientFacilityProcedureCodeUnknown").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1012InpatientFacilityProcedureCodeUnknown">
                            <span class="float_left">UK &#8211;</span>
                            <span class="normal margin">Unknown</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Diagnoses Requiring Medical or Treatment Regimen Change</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1016">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1016" class="green" onclick="Oasis.ToolTip('M1016')">(M1016)</a>
                    Diagnoses Requiring Medical or Treatment Regimen Change Within Past 14 Days: List the patient's Medical Diagnoses and ICD-9-C M codes at the level of highest specificity for those conditions requiring changed medical or treatment regimen within the past 14 days
                    <em>(no surgical, E-codes, or V-codes)</em>
                </label>
                <ul class="sm-diagnoses-list">
                    <li class="title">
                        <span class="alphali"></span>
                        <span class="diagnosis-name">Changed Medical Regimen Diagnosis (Locator #17)</span>
                        <span class="diagnosis-code">ICD-9-C M Code</span>
                    </li>
                    <li>
                        <span class="alphali">a.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis1", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis1"), new { @class = "diagnosis", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode1", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode1"), new { @class = "icd", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">b.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis2", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis2"), new { @class = "diagnosis", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode2", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode2"), new { @class = "icd", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">c.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis3", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis3"), new { @class = "diagnosis", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode3", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode3"), new { @class = "icd", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">d.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis4", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis4"), new { @class = "diagnosis", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode4", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode4"), new { @class = "icd", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">e.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis5", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis5"), new { @class = "diagnosis", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode5", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode5"), new { @class = "icd", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code" })%></span>
                    </li>
                    <li>
                        <span class="alphali">f.</span>
                        <span class="diagnosis-name"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosis6", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosis6"), new { @class = "diagnosis", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_M1016MedicalRegimenDiagnosisCode6", data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisCode6"), new { @class = "icd", @title = "(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, ICD-9-C M Code" })%></span>
                    </li>
                </ul>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1016')" title="More Information about M1016">?</div>
                </div>
            </div>
            <div class="row">
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1016MedicalRegimenDiagnosisNotApplicable", " ", new { @id = Model.TypeName + "_M1016MedicalRegimenDiagnosisNotApplicableHidden" })%>
                        <%= string.Format("<input title='(OASIS M1016) Diagnosis Requiring Medical or Treatment Regimen Change, Not Applicable' id='{0}_M1016MedicalRegimenDiagnosisNotApplicable' name='{0}_M1016MedicalRegimenDiagnosisNotApplicable' class='radio' type='checkbox' value='1' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1016MedicalRegimenDiagnosisNotApplicable").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1016MedicalRegimenDiagnosisNotApplicable">
                            <span class="float_left">NA &#8211;</span>
                            <span class="normal margin">Not applicable <em>(no medical or treatment regimen changes within the past 14 days)</em></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Conditions Prior to Medical or Treatment Regimen Change</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1018">
                <label class="strong"><a href="javascript:void(0)" title="More Information about M1018" class="green" onclick="Oasis.ToolTip('M1018')">(M1018)</a> Conditions Prior to Medical or Treatment Regimen Change or Inpatient Stay Within Past 14 Days: If this patient experienced an inpatient facility discharge or change in medical or treatment regimen within the past 14 days, indicate any conditions which existed prior to the inpatient stay or change in medical or treatment regime. (Mark all that apply)</label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenUI", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenUIHidden" })%>
                        <%= string.Format("<input title='(OASIS M1018) Conditions Prior to Medical or Treatment Regimen Change, Urinary Incontinence' id='{0}_M1018ConditionsPriorToMedicalRegimenUI' name='{0}_M1018ConditionsPriorToMedicalRegimenUI' value='1' class='M1018' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenUI").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenUI">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Urinary incontinence</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenCATH", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenCATHHidden" })%>
                        <%= string.Format("<input title='(OASIS M1018) Conditions Prior to Medical or Treatment Regimen Change, Indwelling/Suprapubic Catheter' id='{0}_M1018ConditionsPriorToMedicalRegimenCATH' name='{0}_M1018ConditionsPriorToMedicalRegimenCATH' value='1' class='M1018' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenCATH").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenCATH">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Indwelling/suprapubic catheter</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenPain", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenPainHidden" })%>
                        <%= string.Format("<input title='(OASIS M1018) Conditions Prior to Medical or Treatment Regimen Change, Intractable Pain' id='{0}_M1018ConditionsPriorToMedicalRegimenPain' name='{0}_M1018ConditionsPriorToMedicalRegimenPain' value='1' class='M1018' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenPain").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenPain">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">Intractable pain</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDECSN", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDECSNHidden" })%>
                        <%= string.Format("<input title='(OASIS M1018) Conditions Prior to Medical or Treatment Regimen Change, Impaired Decision-Making' id='{0}_M1018ConditionsPriorToMedicalRegimenDECSN' name='{0}_M1018ConditionsPriorToMedicalRegimenDECSN' value='1' class='M1018' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenDECSN").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenDECSN">
                            <span class="float_left">4 &#8211;</span>
                            <span class="normal margin">Impaired decision-making</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDisruptive", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDisruptiveHidden" })%>
                        <%= string.Format("<input title='(OASIS M1018) Conditions Prior to Medical or Treatment Regimen Change, Disruptive Behavior' id='{0}_M1018ConditionsPriorToMedicalRegimenDisruptive' name='{0}_M1018ConditionsPriorToMedicalRegimenDisruptive' value='1' class='M1018' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenDisruptive").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenDisruptive">
                            <span class="float_left">5 &#8211;</span>
                            <span class="normal margin">Disruptive or socially inappropriate behavior</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenMemLoss", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenMemLossHidden" })%>
                        <%= string.Format("<input title='(OASIS M1018) Conditions Prior to Medical or Treatment Regimen Change, Memory Loss' id='{0}_M1018ConditionsPriorToMedicalRegimenMemLoss' name='{0}_M1018ConditionsPriorToMedicalRegimenMemLoss' value='1' class='M1018' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenMemLoss").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenMemLoss">
                            <span class="float_left">6 &#8211;</span>
                            <span class="normal margin">Memory loss to the extent that supervision required</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDisruptive", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenDisruptiveHidden" })%>
                        <input type="hidden" name="<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenNone" value="" />
                        <%= string.Format("<input title='(OASIS M1018) Conditions Prior to Medical or Treatment Regimen Change, None of the Above' id='{0}_M1018ConditionsPriorToMedicalRegimenNone' name='{0}_M1018ConditionsPriorToMedicalRegimenNone' value='1' class='M1018' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenNone").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenNone">
                            <span class="float_left">7 &#8211;</span>
                            <span class="normal margin">None of the above</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenNA", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenNAHidden" })%>
                        <%= string.Format("<input title='(OASIS M1018) Conditions Prior to Medical or Treatment Regimen Change, Not Applicable' id='{0}_M1018ConditionsPriorToMedicalRegimenNA' name='{0}_M1018ConditionsPriorToMedicalRegimenNA' value='1' class='M1018' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenNA").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenNA">
                            <span class="float_left">NA &#8211;</span>
                            <span class="normal margin">No inpatient facility discharge and no change in medical or treatment regimen in past 14 days</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenUK", " ", new { @id = Model.TypeName + "_M1018ConditionsPriorToMedicalRegimenUKHidden" })%>
                        <%= string.Format("<input title='(OASIS M1018) Conditions Prior to Medical or Treatment Regimen Change, Unknown' id='{0}_M1018ConditionsPriorToMedicalRegimenUK' name='{0}_M1018ConditionsPriorToMedicalRegimenUK' value='1' class='M1018' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1018ConditionsPriorToMedicalRegimenUK").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenUK">
                            <span class="float_left">UK &#8211;</span>
                            <span class="normal margin">Unknown</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1018')" title="More Information about M1018">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <fieldset class="oasis">
        <legend>Diagnoses, Symptom Control, and Payment Diagnoses</legend>
        <div class="wide_column">
            <div class="row">
                <div class="instructions">
                    <p>
                        List each diagnosis for which the patient is receiving home care (Column 1) and enter its ICD-9-C M code at the
                        level of highest specificity (no surgical/procedure codes) (Column 2). Diagnoses are listed in the order that best
                        reflect the seriousness of each condition and support the disciplines and services provided. Rate the degree of
                        symptom control for each condition (Column 2). Choose one value that represents the degree of symptom control
                        appropriate for each diagnosis: V-codes (for M1020 or M1022) or E-codes (for M1022 only) may be used. ICD-9-C M
                        sequencing requirements must be followed if multiple coding is indicated for any diagnoses. If a V-code is 
                        eported in place of a case mix diagnosis, then optional item M1024 Payment Diagnoses (Columns 3 and 4) may be
                        completed. A case mix diagnosis is a diagnosis that determines the Medicare P P S case mix group. Do not assign
                        symptom control ratings for V- or E-codes.
                    </p>
                    <strong>Code each row according to the following directions for each column:</strong>
                    <ul>
                        <li>
                            <span class="float_left">Column 1:</span>
                            <p>Enter the description of the diagnosis.</p>
                        </li>
                        <li>
                            <span class="float_left">Column 2:</span>
                            <p>Enter the ICD-9-C M code for the diagnosis described in Column 1;</p>
                            <ul>
                                <li>Rate the degree of symptom control for the condition listed in Column 1 using thefollowing scale:</li>
                                <li>
                                    <span class="float_left">0.</span>
                                    <p>Asymptomatic, no treatment needed at this time</p>
                                </li>
                                <li>
                                    <span class="float_left">1.</span>
                                    <p>Symptoms well controlled with current therapy</p>
                                </li>
                                <li>
                                    <span class="float_left">2.</span>
                                    <p>Symptoms controlled with difficulty, affecting daily functioning; patient needsongoing monitoring</p>
                                </li>
                                <li>
                                    <span class="float_left">3.</span>
                                    <p>Symptoms poorly controlled; patient needs frequent adjustment in treatment anddose monitoring</p>
                                </li>
                                <li>
                                    <span class="float_left">4.</span>
                                    <p>
                                        Symptoms poorly controlled; history of re-hospitalizations Note that in Column2 the rating for
                                        symptom control of each diagnosis should not be used to determinethe sequencing of the diagnoses
                                        listed in Column 1. These are separate items andsequencing may not coincide. Sequencing of
                                        diagnoses should reflect the seriousnessof each condition and support the disciplines and services
                                        provided.
                                    </p>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span class="float_left">Column 3:</span>
                            <p>
                                (OPTIONAL) If a V-code is assigned to any row in Column 2, in place ofa case mix diagnosis, it may be
                                necessary to complete optional item M1024 PaymentDiagnoses (Columns 3 and 4). See OASIS-C Guidance Manual.
                            </p>
                        </li>
                        <li>
                            <span class="float_left">Column 4:</span>
                            <p>
                                (OPTIONAL) If a V-code in Column 2 is reported in place of a case mixdiagnosis that requires multiple
                                diagnosis codes under ICD-9-C M coding guidelines,enter the diagnosis descriptions and the ICD-9-C M
                                codes in the same row in Columns 3 and 4. For example, if the case mix diagnosis is a manifestation code,
                                record the diagnosis description and ICD-9-C M code for the underlying condition in Column 3 of that row
                                and the diagnosis description and ICD-9-C M code for the manifestation in Column 4 of that row. Otherwise,
                                leave Column 4 blank in that row.
                            </p>
                        </li>
                    </ul>
                </div>
                <ul id="<%= Model.TypeName %>_OasisDiagnoses" class="oasis_diagnoses"></ul>
            </div>
        </div>
    </fieldset>
    <fieldset class="loc485">
        <legend>Surgical Procedure (Locator #12)</legend>
        <div class="wide_column">
            <div class="row">
                <ul class="sm-diagnoses-list">
                    <li class="title">
                        <span class="surgical-proc">Surgical Procedure</span>
                        <span class="diagnosis-code">Code</span>
                        <span class="surgical-date">Date</span>
                    </li>
                    <li>
                        <span class="surgical-proc"><%= Html.TextBox(Model.TypeName + "_485SurgicalProcedureDescription1", data.AnswerOrEmptyString("485SurgicalProcedureDescription1"), new { @class = "procedureDiagnosis", @id = Model.TypeName + "_485SurgicalProcedureDescription1", @title = "(485 Locator 12) Surgical Procedure" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_485SurgicalProcedureCode1", data.AnswerOrEmptyString("485SurgicalProcedureCode1"), new { @class = "procedureICD pad", @id = Model.TypeName + "_485SurgicalProcedureCode1", @title = "(485 Locator 12) Surgical Procedure, ICD-9-C M Code" })%></span>
                        <span class="surgical-date"><input type="date" name="<%= Model.TypeName %>_485SurgicalProcedureCode1Date" value="<%= data.AnswerOrEmptyString("485SurgicalProcedureCode1Date") %>" id="<%= Model.TypeName %>_485SurgicalProcedureCode1Date" title="(485 Locator 12) Surgical Procedure, Date" /></span>
                    </li>
                    <li>
                        <span class="surgical-proc"><%= Html.TextBox(Model.TypeName + "_485SurgicalProcedureDescription2", data.AnswerOrEmptyString("485SurgicalProcedureDescription2"), new { @class = "procedureDiagnosis", @id = Model.TypeName + "_485SurgicalProcedureDescription2", @title = "(485 Locator 12) Surgical Procedure" })%></span>
                        <span class="diagnosis-code"><%= Html.TextBox(Model.TypeName + "_485SurgicalProcedureCode2", data.AnswerOrEmptyString("485SurgicalProcedureCode2"), new { @class = "procedureICD pad", @id = Model.TypeName + "_485SurgicalProcedureCode2", @title = "(485 Locator 12) Surgical Procedure, ICD-9-C M Code" })%></span>
                        <span class="surgical-date"><input type="date" name="<%= Model.TypeName %>_485SurgicalProcedureCode2Date" value="<%= data.AnswerOrEmptyString("485SurgicalProcedureCode2Date") %>" id="<%= Model.TypeName %>_485SurgicalProcedureCode2Date" title="(485 Locator 12) Surgical Procedure, Date" /></span>
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    <fieldset class="oasis">
        <legend>Therapies the Patient Receives at Home</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1030">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1030" class="green" onclick="Oasis.ToolTip('M1030')">(M1030)</a>
                    Therapies the patient receives at home
                    <em>(Mark all that apply)</em>
                </label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1030HomeTherapiesInfusion", " ", new { @id = Model.TypeName + "_M1030HomeTherapiesInfusionHidden" })%>
                        <%= string.Format("<input title='(OASIS M1030) Therapies Receives at Home, Intravenous or Infusion Therapy' id='{0}_M1030HomeTherapiesInfusion' name='{0}_M1030HomeTherapiesInfusion' value='1' class='M1030' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1030HomeTherapiesInfusion").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1030HomeTherapiesInfusion">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Intravenous or infusion therapy <em>(excludes TPN)</em></span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1030HomeTherapiesParNutrition", " ", new { @id = Model.TypeName + "_M1030HomeTherapiesParNutritionHidden" })%>
                        <%= string.Format("<input title='(OASIS M1030) Therapies Receives at Home, Parenteral Nutrition' id='{0}_M1030HomeTherapiesParNutrition' name='{0}_M1030HomeTherapiesParNutrition' value='1' class='M1030' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1030HomeTherapiesParNutrition").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1030HomeTherapiesParNutrition">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Parenteral nutrition <em>(TPN or lipids)</em></span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1030HomeTherapiesEntNutrition", " ", new { @id = Model.TypeName + "_M1030HomeTherapiesEntNutritionHidden" })%>
                        <%= string.Format("<input title='(OASIS M1030) Therapies Receives at Home, Enteral Nutrition' id='{0}_M1030HomeTherapiesEntNutrition' name='{0}_M1030HomeTherapiesEntNutrition' value='1' class='M1030' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1030HomeTherapiesEntNutrition").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1030HomeTherapiesEntNutrition">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">Enteral nutrition <em>(nasogastric, gastrostomy, jejunostomy, or any other artificial entry into the alimentary canal)</em></span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1030HomeTherapiesNone", " ", new { @id = Model.TypeName + "_M1030HomeTherapiesNoneHidden" })%>
                        <input name="<%= Model.TypeName %>_M1030HomeTherapiesNone" value="" type="hidden" />
                        <%= string.Format("<input title='(OASIS M1030) Therapies Receives at Home, None of the Above' id='{0}_M1030HomeTherapiesNone' name='{0}_M1030HomeTherapiesNone' value='1' class='M1030' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1030HomeTherapiesNone").Equals("1").ToChecked()) %>
                        <label for="<%= Model.TypeName %>_M1030HomeTherapiesNone">
                            <span class="float_left">4 &#8211;</span>
                            <span class="normal margin">None of the above</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1030')" title="More Information about M1030">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (!Model.TypeName.Contains("NonOasis")) { %>
        <ul class="float_right">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
    U.changeToRadio("<%= Model.TypeName %>_GenericPulseApicalRegular");
    U.changeToRadio("<%= Model.TypeName %>_GenericPulseRadialRegular");
    U.changeToRadio("<%= Model.TypeName %>_GenericWeightActualStated");
    U.showIfRadioEquals("<%= Model.TypeName %>_485Allergies","Yes",
        $("#<%= Model.TypeName %>_485AllergiesDescription"));
    U.showIfChecked(
        $("#<%= Model.TypeName %>_M1000InpatientFacilitiesOTHR"),
        $("#<%= Model.TypeName %>_M1000InpatientFacilitiesOTHRMore"));
    U.noneOfTheAbove(
        $("#<%= Model.TypeName %>_M1000InpatientFacilitiesNone"),
        $("#<%= Model.TypeName %>_M1000 .M1000"));
    U.hideIfChecked(
        $("#<%= Model.TypeName %>_M1000InpatientFacilitiesNone"),
        $("#<%= Model.TypeName %>_M1005").closest("fieldset"));
    U.hideIfChecked(
        $("#<%= Model.TypeName %>_M1000InpatientFacilitiesNone"),
        $("#<%= Model.TypeName %>_M1010").closest("fieldset"));
    U.hideIfChecked(
        $("#<%= Model.TypeName %>_M1000InpatientFacilitiesNone"),
        $("#<%= Model.TypeName %>_M1012").closest("fieldset"));
    U.hideIfChecked(
        $("#<%= Model.TypeName %>_M1005InpatientDischargeDateUnknown"),
        $("#<%= Model.TypeName %>_M1005InpatientDischargeDate").parent());
    U.noneOfTheAbove(
        $("#<%= Model.TypeName %>_M1012InpatientFacilityProcedureCodeNotApplicable"),
        $("#<%= Model.TypeName %>_M1012InpatientFacilityProcedureCodeUnknown"));
    U.noneOfTheAbove(
        $("#<%= Model.TypeName %>_M1012InpatientFacilityProcedureCodeUnknown"),
        $("#<%= Model.TypeName %>_M1012InpatientFacilityProcedureCodeNotApplicable"));
    U.hideIfChecked(
        $("#<%= Model.TypeName %>_M1012InpatientFacilityProcedureCodeNotApplicable"),
        $("#<%= Model.TypeName %>_M1012 .sm-diagnoses-list"));
    U.hideIfChecked(
        $("#<%= Model.TypeName %>_M1012InpatientFacilityProcedureCodeUnknown"),
        $("#<%= Model.TypeName %>_M1012 .sm-diagnoses-list"));
    U.hideIfChecked(
        $("#<%= Model.TypeName %>_M1016MedicalRegimenDiagnosisNotApplicable"),
        $("#<%= Model.TypeName %>_M1016 .sm-diagnoses-list"));
    U.noneOfTheAbove(
        $("#<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenNone"),
        $("#<%= Model.TypeName %>_M1018 .M1018"));
    U.noneOfTheAbove(
        $("#<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenNA"),
        $("#<%= Model.TypeName %>_M1018 .M1018"));
    U.noneOfTheAbove(
        $("#<%= Model.TypeName %>_M1018ConditionsPriorToMedicalRegimenUK"),
        $("#<%= Model.TypeName %>_M1018 .M1018"));
    U.noneOfTheAbove(
        $("#<%= Model.TypeName %>_M1030HomeTherapiesNone"),
        $("#<%= Model.TypeName %>_M1030 .M1030"));
    $("#<%= Model.TypeName %>_OasisDiagnoses").DiagnosesList({
    <%  if (data.AnswerOrEmptyString("M1020PrimaryDiagnosis").IsNotNullOrEmpty()) { %>
            _M1020PrimaryDiagnosis: "<%= data.AnswerOrEmptyString("M1020PrimaryDiagnosis").EscapeApostrophe() %>",
            _M1020ICD9M: "<%= data.AnswerOrEmptyString("M1020ICD9M").EscapeApostrophe() %>",
            _M1024PaymentDiagnosesA3: "<%= data.AnswerOrEmptyString("M1024PaymentDiagnosesA3").EscapeApostrophe() %>",
            _M1024ICD9MA3: "<%= data.AnswerOrEmptyString("M1024ICD9MA3").EscapeApostrophe() %>",
            _M1024PaymentDiagnosesA4: "<%= data.AnswerOrEmptyString("M1024PaymentDiagnosesA4").EscapeApostrophe() %>",
            _M1024ICD9MA4: "<%= data.AnswerOrEmptyString("M1024ICD9MA4").EscapeApostrophe() %>",
            _M1020SymptomControlRating: "<%= data.AnswerOrEmptyString("M1020SymptomControlRating").EscapeApostrophe() %>",
            _485ExacerbationOrOnsetPrimaryDiagnosis: "<%= data.AnswerOrEmptyString("485ExacerbationOrOnsetPrimaryDiagnosis").EscapeApostrophe() %>",
            _M1020PrimaryDiagnosisDate: "<%= data.AnswerOrEmptyString("M1020PrimaryDiagnosisDate").EscapeApostrophe() %>",
    <%  }
        for (int i = 1; i < 26; i++) {
            if (data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i).IsNotNullOrEmpty()) { %>
            _M1022PrimaryDiagnosis<%= i %>: "<%= data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i).EscapeApostrophe()  %>",
            _M1022ICD9M<%= i %>: "<%= data.AnswerOrEmptyString("M1022ICD9M" + i).EscapeApostrophe() %>",
            _M1024PaymentDiagnoses<%= (char)(i + 65) %>3: "<%= data.AnswerOrEmptyString("M1024PaymentDiagnoses" + (char)(i + 65) + "3").EscapeApostrophe() %>",
            _M1024ICD9M<%= (char)(i + 65) %>3: "<%= data.AnswerOrEmptyString("M1024ICD9M" + (char)(i + 65) + "3").EscapeApostrophe() %>",
            _M1024PaymentDiagnoses<%= (char)(i + 65) %>4: "<%= data.AnswerOrEmptyString("M1024PaymentDiagnoses" + (char)(i + 65) + "4").EscapeApostrophe() %>",
            _M1024ICD9M<%= (char)(i + 65) %>4: "<%= data.AnswerOrEmptyString("M1024ICD9M" + (char)(i + 65) + "4").EscapeApostrophe() %>",
            _M1022OtherDiagnose<%= i %>Rating: "<%= data.AnswerOrEmptyString("M1022OtherDiagnose" + i + "Rating").EscapeApostrophe() %>",
            _485ExacerbationOrOnsetPrimaryDiagnosis<%= i %>: "<%= data.AnswerOrEmptyString("485ExacerbationOrOnsetPrimaryDiagnosis" + i).EscapeApostrophe() %>",
            _M1022PrimaryDiagnosis<%= i %>Date: "<%= data.AnswerOrEmptyString("M1022PrimaryDiagnosis" + i + "Date").EscapeApostrophe() %>",
    <%      }
        } %>
            Assessment: "<%= Model.TypeName %>"
    })
</script>