<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  var isOasis = !Model.TypeName.Contains("NonOasis"); %>
<%  var RespiratoryCondition = data.AnswerArray("GenericRespiratoryCondition"); %>
<%  var RespiratorySounds = data.AnswerArray("GenericRespiratorySounds"); %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 6 || Model.AssessmentTypeNum.ToInteger() % 10 > 8) { %>
    <%  if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) { %>
    printview.addsection(
        printview.col(3,
            printview.checkbox("WNL (Within Normal Limits)",<%= RespiratoryCondition.Contains("1").ToString().ToLower() %>,true) +
            printview.checkbox("<strong>Nebulizer:</strong> <%= RespiratoryCondition.Contains("6") ? data.AnswerOrEmptyString("GenericNebulizerText").Clean() : string.Empty %>",<%= RespiratoryCondition.Contains("6").ToString().ToLower() %>) +
            printview.checkbox("Tracheostomy",<%= RespiratoryCondition.Contains("7").ToString().ToLower() %>,true)) +
        printview.col(9,
            printview.checkbox("Sounds:",<%= RespiratoryCondition.Contains("2").ToString().ToLower() %>,true) +
            printview.checkbox("CTA",<%= (RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("1")).ToString().ToLower() %>) +
            printview.span("<%= RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("1") ? data.AnswerOrEmptyString("GenericLungSoundsCTAText").Clean() : string.Empty %>",0,1) +
            printview.checkbox("Rales",<%= (RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("2")).ToString().ToLower() %>) +
            printview.span("<%= RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("2") ? data.AnswerOrEmptyString("GenericLungSoundsRalesText").Clean() : string.Empty %>",0,1) +
            printview.checkbox("Rhonchi",<%= (RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("3")).ToString().ToLower() %>) +
            printview.span("<%= RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("3") ? data.AnswerOrEmptyString("GenericLungSoundsRhonchiText").Clean() : string.Empty %>",0,1) +
            printview.checkbox("Wheezes",<%= (RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("4")).ToString().ToLower() %>) +
            printview.span("<%= RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("4") ? data.AnswerOrEmptyString("GenericLungSoundsWheezesText").Clean() : string.Empty %>",0,1) +
            printview.span("") +
            printview.checkbox("Crackles",<%= (RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("5")).ToString().ToLower() %>) +
            printview.span("<%= RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("5") ? data.AnswerOrEmptyString("GenericLungSoundsCracklesText").Clean() : string.Empty %>",0,1) +
            printview.checkbox("Diminished",<%= (RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("6")).ToString().ToLower() %>) +
            printview.span("<%= RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("6") ? data.AnswerOrEmptyString("GenericLungSoundsDiminishedText").Clean() : string.Empty %>",0,1) +
            printview.checkbox("Absent",<%= (RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("7")).ToString().ToLower() %>) +
            printview.span("<%= RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("7") ? data.AnswerOrEmptyString("GenericLungSoundsAbsentText").Clean() : string.Empty %>",0,1) +
            printview.checkbox("Stridor",<%= (RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("8")).ToString().ToLower() %>) +
            printview.span("<%= RespiratoryCondition.Contains("2") && RespiratorySounds.Contains("8") ? data.AnswerOrEmptyString("GenericLungSoundsStridorText").Clean() : string.Empty %>",0,1) +
            printview.checkbox("Cough:",<%= RespiratoryCondition.Contains("3").ToString().ToLower() %>,true) +
            printview.span("<%= RespiratoryCondition.Contains("3") ? (data.AnswerOrEmptyString("GenericCoughList").Equals("1") ? "N/A" : string.Empty) + (data.AnswerOrEmptyString("GenericCoughList").Equals("2") ? "Productive" : string.Empty) + (data.AnswerOrEmptyString("GenericCoughList").Equals("3") ? "Nonproductive" : string.Empty) + (data.AnswerOrEmptyString("GenericCoughList").Equals("4") ? "Other" : string.Empty) : string.Empty %>",false,1) +
            printview.span("<%= RespiratoryCondition.Contains("3") ? "Describe: " + data.AnswerOrEmptyString("GenericCoughDescribe").Clean() : string.Empty %>",false,1) +
            printview.checkbox("O<sub>2</sub>:",<%= RespiratoryCondition.Contains("4").ToString().ToLower() %>,true) +
            printview.span("<%= RespiratoryCondition.Contains("4") ? "At: " + data.AnswerOrEmptyString("Generic02AtText").Clean() : string.Empty %>",false,1) +
            printview.span("<%= RespiratoryCondition.Contains("4") ? (data.AnswerOrEmptyString("GenericLPMVia").Equals("1") ? "LPM via: Nasal Cannula" : string.Empty) + (data.AnswerOrEmptyString("GenericLPMVia").Equals("2") ? "LPM via: Mask" : string.Empty) + (data.AnswerOrEmptyString("GenericLPMVia").Equals("3") ? "LPM via: Trach" : string.Empty) + (data.AnswerOrEmptyString("GenericLPMVia").Equals("4") ? "LPM via: Other" : string.Empty) : string.Empty %>",false,1) +
            printview.checkbox("O<sub>2</sub> Sat:",<%= RespiratoryCondition.Contains("5").ToString().ToLower() %>,true) +
            printview.span("<%= RespiratoryCondition.Contains("5") ? data.AnswerOrEmptyString("Generic02SatText").Clean() : string.Empty %>",false,1) +
            printview.span("<%= RespiratoryCondition.Contains("5") && data.AnswerOrEmptyString("Generic02SatList").Equals("0") ? string.Empty : "On " + data.AnswerOrEmptyString("Generic02SatList").Clean() %>",false,1)) +
        printview.span("Comments:",true) +
        printview.span("<%= data.AnswerOrEmptyString("GenericRespiratoryComments").Clean() %>",false,2),
        "Respiratory");
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() != 11 || Model.AssessmentTypeNum.ToInteger() != 14) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1400) " : string.Empty %>When is the patient dyspneic or noticeably Short of Breath?",true) +
        printview.checkbox("0 &#8211; Patient is not short of breath",<%= data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("00").ToString().ToLower() %>) +
        printview.checkbox("1 &#8211; When walking more than 20 feet, climbing stairs",<%= data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("01").ToString().ToLower() %>) +
        printview.checkbox("2 &#8211; With moderate exertion (e.g., while dressing, using commode or bedpan, walking distances less than 20 feet)",<%= data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("02").ToString().ToLower() %>) +
        printview.checkbox("3 &#8211; With minimal exertion (e.g., while eating, talking, or performing other ADLs) or with agitation",<%= data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("03").ToString().ToLower() %>) +
        printview.checkbox("4 &#8211; At rest (during day or night)",<%= data.AnswerOrEmptyString("M1400PatientDyspneic").Equals("04").ToString().ToLower() %>));
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 5 || Model.AssessmentTypeNum.ToInteger() % 10 == 9) { %>
    printview.addsection(
        printview.span("<%= isOasis ? "(M1410) " : string.Empty %>Respiratory Treatments utilized at home:",true) +
        printview.col(2,
            printview.checkbox("1 &#8211; Oxygen (intermittent or continuous)",<%= data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsOxygen").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("2 &#8211; Ventilator (continually or at night)",<%= data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsVentilator").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("3 &#8211; Continuous/ Bi-level positive airway pressure",<%= data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsContinuous").Equals("1").ToString().ToLower() %>) +
            printview.checkbox("4 &#8211; None of the above",<%= data.AnswerOrEmptyString("M1410HomeRespiratoryTreatmentsNone").Equals("1").ToString().ToLower() %>)));
    <%  } %>
    <% if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/Print/Respiratory.ascx", Model); %>
<%  } %>
</script>