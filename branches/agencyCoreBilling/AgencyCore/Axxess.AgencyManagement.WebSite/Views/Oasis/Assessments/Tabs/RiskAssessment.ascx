<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<%  var data = Model.ToDictionary(); %>
<%  if (Model.AssessmentTypeNum.ToInteger() % 10 != 4 && Model.AssessmentTypeNum.ToInteger() % 10 != 5) { %>
<div class="wrapper main">
<%  using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasis" + Model.TypeName + "RiskAssessmentForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id, new { @id = Model.TypeName + "_Id" })%>
    <%= Html.Hidden(Model.TypeName + "_Action", "Edit", new { @id = Model.TypeName + "_Action" })%>
    <%= Html.Hidden(Model.TypeName + "_PatientGuid", Model.PatientId, new { @id = Model.TypeName + "_PatientGuid" })%>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId, new { @id = Model.TypeName + "_EpisodeId" })%>
    <%= Html.Hidden("assessment", Model.TypeName, new { @id = Model.TypeName + "_AssessmentType" })%>
    <%= Html.Hidden("categoryType", "RiskAssessment")%>
    <%= Html.Hidden(Model.TypeName + "_Button", "", new { @id = Model.TypeName + "_Button" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float_right">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
    <%  if (Model.AssessmentTypeNum.ToInteger() < 4) { %>
    <fieldset class="oasis">
        <legend>Risk for Hospitalization</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1032">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1032" class="green" onclick="Oasis.ToolTip('M1032')">(M1032)</a>
                    Risk for Hospitalization: Which of the following signs or symptoms characterize this patient as at risk for hospitalization?
                    <em>(Mark all that apply)</em>
                </label>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskRecentDecline", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskRecentDeclineHidden" })%>
                        <%= string.Format("<input title='(OASIS M1032) Risk for Hospitalization, Recent Decline in Status' id='{0}_M1032HospitalizationRiskRecentDecline' name='{0}_M1032HospitalizationRiskRecentDecline' value='1' class='M1032' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1032HospitalizationRiskRecentDecline").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1032HospitalizationRiskRecentDecline">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Recent decline in mental, emotional, or behavioral status</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskMultipleHosp", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskMultipleHospHidden" })%>
                        <%= string.Format("<input title='(OASIS M1032) Risk for Hospitalization, Multiple Recent Hospitalizations' id='{0}_M1032HospitalizationRiskMultipleHosp' name='{0}_M1032HospitalizationRiskMultipleHosp' value='1' class='M1032' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1032HospitalizationRiskMultipleHosp").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1032HospitalizationRiskMultipleHosp">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Multiple hospitalizations (2 or more) in the past 12 months</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskHistoryOfFall", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskHistoryOfFallHidden" })%>
                        <%= string.Format("<input title='(OASIS M1032) Risk for Hospitalization, Recent History of Falls' id='{0}_M1032HospitalizationRiskHistoryOfFall' name='{0}_M1032HospitalizationRiskHistoryOfFall' value='1' class='M1032' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1032HospitalizationRiskHistoryOfFall").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1032HospitalizationRiskHistoryOfFall">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">History of falls (2 or more falls, or any fall with an injury, in the past year)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskMedications", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskMedicationsHidden" })%>
                        <%= string.Format("<input title='(OASIS M1032) Risk for Hospitalization, Taking Multiple Medications' id='{0}_M1032HospitalizationRiskMedications' name='{0}_M1032HospitalizationRiskMedications' value='1' class='M1032' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1032HospitalizationRiskMedications").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1032HospitalizationRiskMedications">
                            <span class="float_left">4 &#8211;</span>
                            <span class="normal margin">Taking five or more medications</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskFrailty", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskFrailtyHidden" })%>
                        <%= string.Format("<input title='(OASIS M1032) Risk for Hospitalization, Frailty Indicators' id='{0}_M1032HospitalizationRiskFrailty' name='{0}_M1032HospitalizationRiskFrailty' value='1' class='M1032' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1032HospitalizationRiskFrailty").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1032HospitalizationRiskFrailty">
                            <span class="float_left">5 &#8211;</span>
                            <span class="normal margin">Frailty indicators, e.g., weight loss, self-reported exhaustion</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskOther", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskOtherHidden" })%>
                        <%= string.Format("<input title='(OASIS M1032) Risk for Hospitalization, Other' id='{0}_M1032HospitalizationRiskOther' name='{0}_M1032HospitalizationRiskOther' value='1' class='M1032' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1032HospitalizationRiskOther").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1032HospitalizationRiskOther">
                            <span class="float_left">6 &#8211;</span>
                            <span class="normal margin">Other</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1032HospitalizationRiskNone", "", new { @id = Model.TypeName + "_M1032HospitalizationRiskNoneHidden" })%>
                        <%= string.Format("<input title='(OASIS M1032) Risk for Hospitalization, None of the Above' id='{0}_M1032HospitalizationRiskNone' name='{0}_M1032HospitalizationRiskNone' value='1' class='M1032' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1032HospitalizationRiskNone").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1032HospitalizationRiskNone">
                            <span class="float_left">7 &#8211;</span>
                            <span class="normal margin">None of the above</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1032')" title="More Information about M1032">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Overall Status</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1034">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1034" class="green" onclick="Oasis.ToolTip('M1034')">(M1034)</a>
                    Overall Status: Which description best fits the patient&#8217;s overall status?
                    <em>(Check one)</em>
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1034OverallStatus", "", new { @id = Model.TypeName + "_M1034OverallStatusHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1034OverallStatus", "00", data.AnswerOrEmptyString("M1034OverallStatus").Equals("00"), new { @id = Model.TypeName + "_M1034OverallStatus00", @title = "(OASIS M1034) Overall Status, Stable" }) %>
                        <label for="M1034OverallStatus00">
                            <span class="float_left">0 &#8211;</span>
                            <span class="normal margin">The patient is stable with no heightened risk(s) for serious complications and death (beyond those typical of the patient&#8217;s age).</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1034OverallStatus", "01", data.AnswerOrEmptyString("M1034OverallStatus").Equals("01"), new { @id = Model.TypeName + "_M1034OverallStatus01", @title = "(OASIS M1034) Overall Status, Temporarily High Risk" })%>
                        <label for="M1034OverallStatus01">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">The patient is temporarily facing high health risk(s) but is likely to return to being stable without heightened risk(s) for serious complications and death (beyond those typical of the patient&#8217;s age).</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1034OverallStatus", "02", data.AnswerOrEmptyString("M1034OverallStatus").Equals("02"), new { @id = Model.TypeName + "_M1034OverallStatus02", @title = "(OASIS M1034) Overall Status, Ongoing High Risk" })%>
                        <label for="M1034OverallStatus02">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">The patient is likely to remain in fragile health and have ongoing high risk(s) of serious complications and death.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1034OverallStatus", "03", data.AnswerOrEmptyString("M1034OverallStatus").Equals("03"), new { @id = Model.TypeName + "_M1034OverallStatus03", @title = "(OASIS M1034) Overall Status, Serious Progressive Conditions" })%>
                        <label for="M1034OverallStatus03">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">The patient has serious progressive conditions that could lead to death within a year.</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1034OverallStatus", "UK", data.AnswerOrEmptyString("M1034OverallStatus").Equals("UK"), new { @id = Model.TypeName + "_M1034OverallStatusUK", @title = "(OASIS M1034) Overall Status, Unknown" })%>
                        <label for="M1034OverallStatusUK">
                            <span class="float_left">UK &#8211;</span>
                            <span class="normal margin">The patient&#8217;s situation is unknown or unclear.</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1034')" title="More Information about M1034">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="oasis">
        <legend>Risk Factors</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1036">
                <label class="strong">
                    <a href="javascript:void(0)" title="More Information about M1036" class="green" onclick="Oasis.ToolTip('M1036')">(M1036)</a>
                    Risk Factors, either present or past, likely to affect current health status and/or outcome:
                    <em>(Mark all that apply)</em>
                </label>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsSmoking", "", new { @id = Model.TypeName + "_M1036RiskFactorsSmokingHidden" })%>
                        <%= string.Format("<input title='(OASIS M1036) Risk Factors, Smoking' id='{0}_M1036RiskFactorsSmoking' name='{0}_M1036RiskFactorsSmoking' value='1' class='M1036' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1036RiskFactorsSmoking").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1036RiskFactorsSmoking">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Smoking</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsObesity", "", new { @id = Model.TypeName + "_M1036RiskFactorsObesityHidden" })%>
                        <%= string.Format("<input title='(OASIS M1036) Risk Factors, Obesity' id='{0}_M1036RiskFactorsObesity' name='{0}_M1036RiskFactorsObesity' value='1' class='M1036' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1036RiskFactorsObesity").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1036RiskFactorsObesity">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Obesity</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsAlcoholism", "", new { @id = Model.TypeName + "_M1036RiskFactorsAlcoholismHidden" })%>
                        <%= string.Format("<input title='(OASIS M1036) Risk Factors, Alcohol Dependency' id='{0}_M1036RiskFactorsAlcoholism' name='{0}_M1036RiskFactorsAlcoholism' value='1' class='M1036' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1036RiskFactorsAlcoholism").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1036RiskFactorsAlcoholism">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">Alcohol dependency</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsDrugs", "", new { @id = Model.TypeName + "_M1036RiskFactorsDrugsHidden" })%>
                        <%= string.Format("<input title='(OASIS M1036) Risk Factors, Drug Dependency' id='{0}_M1036RiskFactorsDrugs' name='{0}_M1036RiskFactorsDrugs' value='1' class='M1036' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1036RiskFactorsDrugs").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1036RiskFactorsDrugs">
                            <span class="float_left">4 &#8211;</span>
                            <span class="normal margin">Drug dependency</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsNone", "", new { @id = Model.TypeName + "_M1036RiskFactorsNoneHidden" })%>
                        <%= string.Format("<input title='(OASIS M1036) Risk Factors, None of the Above' id='{0}_M1036RiskFactorsNone' name='{0}_M1036RiskFactorsNone' value='1' class='M1036' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1036RiskFactorsNone").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1036RiskFactorsNone">
                            <span class="float_left">5 &#8211;</span>
                            <span class="normal margin">None of the above</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.Hidden(Model.TypeName + "_M1036RiskFactorsUnknown", "", new { @id = Model.TypeName + "__M1036RiskFactorsUnknownHidden" })%>
                        <%= string.Format("<input title='(OASIS M1036) Risk Factors, Unknown' id='{0}_M1036RiskFactorsUnknown' name='{0}_M1036RiskFactorsUnknown' value='1' type='checkbox' {1} />", Model.TypeName, data.AnswerOrEmptyString("M1036RiskFactorsUnknown").Equals("1").ToChecked())%>
                        <label for="<%= Model.TypeName %>_M1036RiskFactorsUnknown">
                            <span class="float_left">UK &#8211;</span>
                            <span class="normal margin">Unknown</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1036')" title="More Information about M1036">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <%  if (Model.AssessmentTypeNum.ToInteger() > 5) { %>
    <fieldset class="oasis">
        <legend>Vaccinations</legend>
        <div class="wide_column">
            <div class="row" id="<%= Model.TypeName %>_M1040">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M1040')">(M1040)</a>
                    Influenza Vaccine: Did the patient receive the influenza vaccine from your agency for this year&#8217;s influenza season (October 1 through March 31) during this episode of care?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1040InfluenzaVaccine", "", new { @id = Model.TypeName + "_M1040InfluenzaVaccineHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1040InfluenzaVaccine", "00", data.AnswerOrEmptyString("M1040InfluenzaVaccine").Equals("00"), new { @id = Model.TypeName + "_M1040InfluenzaVaccine0" })%>
                        <label for="<%= Model.TypeName %>_M1040InfluenzaVaccine0">
                            <span class="float_left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1040InfluenzaVaccine", "01", data.AnswerOrEmptyString("M1040InfluenzaVaccine").Equals("01"), new { @id = Model.TypeName + "_M1040InfluenzaVaccine1" })%>
                        <label for="<%= Model.TypeName %>_M1040InfluenzaVaccine1">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1040InfluenzaVaccine", "NA", data.AnswerOrEmptyString("M1040InfluenzaVaccine").Equals("NA"), new { @id = Model.TypeName + "_M1040InfluenzaVaccineNA" })%>
                        <label for="<%= Model.TypeName %>_M1040InfluenzaVaccineNA">
                            <span class="float_left">NA &#8211;</span>
                            <span class="normal margin">Does not apply because entire episode of care (SOC/ROC to Transfer/Discharge) is outside this influenza season.</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1040')">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1045">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M1045')">(M1045)</a>
                    Reason Influenza Vaccine not received: If the patient did not receive the influenza vaccine from your agency during this episode of care, state reason:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "", new { @id = Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReasonHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "01", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("01"), new { @id = Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason1" })%>
                        <label for="<%= Model.TypeName %>_M1045InfluenzaVaccineNotReceivedReason1">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Received from another health care provider (e.g., physician)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "02", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("02"), new { @id = Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason2" })%>
                        <label for="<%= Model.TypeName %>_M1045InfluenzaVaccineNotReceivedReason2">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Received from your agency previously during this year&#8217;s flu season</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "03", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("03"), new { @id = Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason3" })%>
                        <label for="<%= Model.TypeName %>_M1045InfluenzaVaccineNotReceivedReason3">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">Offered and declined</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "04", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("04"), new { @id = Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason4" })%>
                        <label for="<%= Model.TypeName %>_M1045InfluenzaVaccineNotReceivedReason4">
                            <span class="float_left">4 &#8211;</span>
                            <span class="normal margin">Assessed and determined to have medical contraindication(s)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "05", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("05"), new { @id = Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason5" })%>
                        <label for="<%= Model.TypeName %>_M1045InfluenzaVaccineNotReceivedReason5">
                            <span class="float_left">5 &#8211;</span>
                            <span class="normal margin">Not indicated; patient does not meet age/condition guidelines for influenza vaccine</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "06", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("06"), new { @id = Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason6" })%>
                        <label for="<%= Model.TypeName %>_M1045InfluenzaVaccineNotReceivedReason6">
                            <span class="float_left">6 &#8211;</span>
                            <span class="normal margin">Inability to obtain vaccine due to declared shortage</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason", "07", data.AnswerOrEmptyString("M1045InfluenzaVaccineNotReceivedReason").Equals("07"), new { @id = Model.TypeName + "_M1045InfluenzaVaccineNotReceivedReason7" })%>
                        <label for="<%= Model.TypeName %>_M1045InfluenzaVaccineNotReceivedReason7">
                            <span class="float_left">7 &#8211;</span>
                            <span class="normal margin">None of the above</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1045');">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1050">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M1050')">(M1050)</a>
                    Pneumococcal Vaccine: Did the patient receive pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge)?
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1050PneumococcalVaccine", "", new { @id = Model.TypeName + "_M1050PneumococcalVaccineHidden" })%>
                <div class="checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1050PneumococcalVaccine", "0", data.AnswerOrEmptyString("M1050PneumococcalVaccine").Equals("0"), new { @id = Model.TypeName + "_M1050PneumococcalVaccine0" })%>
                        <label for="<%= Model.TypeName %>_M1050PneumococcalVaccine0">
                            <span class="float_left">0 &#8211;</span>
                            <span class="normal margin">No</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1050PneumococcalVaccine", "1", data.AnswerOrEmptyString("M1050PneumococcalVaccine").Equals("1"), new { @id = Model.TypeName + "_M1050PneumococcalVaccine1" })%>
                        <label for="<%= Model.TypeName %>_M1050PneumococcalVaccine1">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Yes</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1050')">?</div>
                </div>
            </div>
            <div class="row" id="<%= Model.TypeName %>_M1055">
                <label class="strong">
                    <a href="javascript:void(0)" class="green" onclick="Oasis.ToolTip('M1055')">(M1055)</a>
                    Reason PPV not received: If patient did not receive the pneumococcal polysaccharide vaccine (PPV) from your agency during this episode of care (SOC/ROC to Transfer/Discharge), state reason:
                </label>
                <%= Html.Hidden(Model.TypeName + "_M1055PPVNotReceivedReason", "", new { @id = Model.TypeName + "_M1055PPVNotReceivedReasonHidden" })%>
                <div class="wide checkgroup">
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1055PPVNotReceivedReason", "01", data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("01"), new { @id = Model.TypeName + "_M1055PPVNotReceivedReason1" })%>
                        <label for="<%= Model.TypeName %>_M1055PPVNotReceivedReason1">
                            <span class="float_left">1 &#8211;</span>
                            <span class="normal margin">Patient has received PPV in the past</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1055PPVNotReceivedReason", "02", data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("02"), new { @id = Model.TypeName + "_M1055PPVNotReceivedReason2" })%>
                        <label for="<%= Model.TypeName %>_M1055PPVNotReceivedReason2">
                            <span class="float_left">2 &#8211;</span>
                            <span class="normal margin">Offered and declined</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1055PPVNotReceivedReason", "03", data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("03"), new { @id = Model.TypeName + "_M1055PPVNotReceivedReason3" })%>
                        <label for="<%= Model.TypeName %>_M1055PPVNotReceivedReason3">
                            <span class="float_left">3 &#8211;</span>
                            <span class="normal margin">Assessed and determined to have medical contraindication(s)</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1055PPVNotReceivedReason", "04", data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("04"), new { @id = Model.TypeName + "_M1055PPVNotReceivedReason4" })%>
                        <label for="<%= Model.TypeName %>_M1055PPVNotReceivedReason4">
                            <span class="float_left">4 &#8211;</span>
                            <span class="normal margin">Not indicated; patient does not meet age/condition guidelines for PPV</span>
                        </label>
                    </div>
                    <div class="option">
                        <%= Html.RadioButton(Model.TypeName + "_M1055PPVNotReceivedReason", "05", data.AnswerOrEmptyString("M1055PPVNotReceivedReason").Equals("05"), new { @id = Model.TypeName + "_M1055PPVNotReceivedReason5" })%>
                        <label for="<%= Model.TypeName %>_M1055PPVNotReceivedReason5">
                            <span class="float_left">5 &#8211;</span>
                            <span class="normal margin">None of the above</span>
                        </label>
                    </div>
                </div>
                <div class="float_right oasis">
                    <div class="tooltip_oasis" onclick="Oasis.ToolTip('M1055')">?</div>
                </div>
            </div>
        </div>
    </fieldset>
    <%  } %>
    <% if (Model.AssessmentTypeNum.ToInteger() % 10 < 5) Html.RenderPartial("~/Views/Oasis/Assessments/InterventionsGoals/RiskAssessment.ascx", Model); %>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save</a></li>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Continue</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="$('#<%= Model.Type %>_Button').val($(this).html());<%= Model.TypeName %>.FormSubmit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this));">Save &#38; Exit</a></li>
        </ul>
        <%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
        <ul class="float_right">
            <li><a href="javascript:void(0)" onclick="<%= Model.TypeName %>.FormSubmit($(this),function(){UserInterface.ShowOasisValidationModal('<%= Model.Id %>','<%= Model.PatientId %>','<%= Model.EpisodeId %>','<%= Model.TypeName %>')})">Check for Errors</a></li>
        </ul>
        <%  } %>
    </div>
<%  } %>
</div>
<%  } %>
<script type="text/javascript">
<%  if (Model.AssessmentTypeNum.ToInteger() < 10) { %>
    $("fieldset.oasis.loc485").removeClass("loc485");
<%  } else { %>
    $("fieldset.oasis").removeClass("oasis");
    $("a.green,.tooltip_oasis").remove();
<%  } %>
    U.noneOfTheAbove(
        $("#<%= Model.TypeName %>_M1032HospitalizationRiskNone"),
        $("#<%= Model.TypeName %>_M1032 .M1032"));
    U.noneOfTheAbove(
        $("#<%= Model.TypeName %>_M1036RiskFactorsUnknown"),
        $("#<%= Model.TypeName %>_M1036 .M1036"));
</script>