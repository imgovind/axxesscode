﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<OasisExport>" %>
<%  using (Html.BeginForm("CorrectionChange", "Oasis", FormMethod.Post, new { @id = "oasisCorrectionChange", @style = "width: 100%; height: 100%;" })) { %>
<%= Html.Hidden("Id", Model.AssessmentId)%>
<%= Html.Hidden("Type", Model.AssessmentType)%>
<%= Html.Hidden("PatientId", Model.PatientId)%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId)%>
<fieldset> 
  <div style="padding: 10px; width: 600px;">
      <div style="margin-bottom: 10px;"><label><strong>The correction number should only be changed if you are retransmitting an OASIS assessment that was previously accepted and needs to be retransmitted because of corrections you made. If it is the first resubmission , use correction number 01, 2nd resubmission use number 02 , 3rd resubmission use number 03, etc.</strong></label></div>
      <div><strong>If an OASIS assessment was rejected and needs to be retransmitted after corrections have been made , use correction number 00.</strong></div>
      <% var selectionList = new List<SelectListItem>(); for (int i = 0; i <= 10; i++) { selectionList.Add(new SelectListItem { Text = string.Format("{0:00}", i), Value = i.ToString() }); } %>
      <div class="align_center"><label><strong>Correction Number :</strong></label> <%= Html.DropDownList("CorrectionNumber", new SelectList (selectionList,"Value","Text",Model.CorrectionNumber.ToString()) )%></div>
  </div>
  <div class="buttons">
    <ul>
        <li>
            <a href="javascript:void(0);" id="oasisExportCorrectionNumber" onclick="$(this).closest('form').submit();">Edit</a>
        </li><li>
            <a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a>
        </li>
    </ul>
</div>
</fieldset>
<%} %>
