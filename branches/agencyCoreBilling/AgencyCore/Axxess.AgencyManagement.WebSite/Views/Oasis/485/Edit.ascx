﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PlanofCare>" %>
<% var patient = Model.PatientData.IsNotNullOrEmpty() ? Model.PatientData.ToObject<Patient>() : null; %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "editplanofcare",
        "Edit 485 - Plan of Care (From Assessment)",
        patient != null ? patient.DisplayName : string.Empty)%>
<% using (Html.BeginForm("Save485", "Oasis", FormMethod.Post, new { @id = "edit485Form" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_485_Id" })%>
<%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_485_EpisodeId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_485_PatientId" })%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="3">Plan of Care</th></tr>
            <tr>
                <td>
                    <label for="Edit_485_PatientName" class="float_left">Patient&#8217;s Name</label>
                    <div class="float_right"><input readonly="readonly" id="Edit_485_PatientName" value="<%= patient != null ? patient.DisplayName : "" %>" /></div>
                    <div class="clear"></div>
                    <label for="Edit_485_EpisodePeriod" class="float_left">Episode Period</label>
                    <div class="float_right"><input readonly="readonly" id="Edit_485_EpisodePeriod" value="<%= Model != null && Model.EpisodeStart.IsNotNullOrEmpty() && Model.EpisodeEnd.IsNotNullOrEmpty() ? string.Format("{0} - {1}", Model.EpisodeStart, Model.EpisodeEnd) : "" %>" /></div>
                </td><td>
                    <em>Click on the button below to refresh this page from the OASIS Assessment associated with this episode.</em>
                    <div class="buttons"><ul><li>
                        <a href="javascript:void(0);" onclick="UserInterface.RefreshPlanofCare('<%= Model.EpisodeId  %>','<%= Model.PatientId  %>','<%= Model.Id  %>'); ">Refresh Data</a>
                    </li></ul></div>
                </td><td>
                    <label for="Edit_485_PhysicianId" class="float_left">Physician:</label>
                    <div class="float_right"><%= Html.TextBox("PhysicianId", Model != null ? Model.PhysicianId.ToString() : string.Empty, new { @id = "Edit_485_PhysicianId", @class = "Physicians requiredphysician" })%></div>
                    <div class="clear"></div>
                    <div class="float_right ancillary_button align_left"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
                </td>
            </tr>
        </tbody>
    </table>
<div id="planofCareContentId"><% Html.RenderPartial("~/Views/Oasis/485/LocatorQuestions.ascx", Model); %></div>
<%= Html.Hidden("Status", "", new { @id = "Edit_485_Status" })%>
<div class="buttons"><ul>
    <li><a href="javascript:void(0);" onclick="$('#Edit_485_Status').val('110');PlanOfCare.Submit($(this),'editplanofcare');">Save</a></li>
    <li><a href="javascript:void(0);" onclick="$('#Edit_485_Status').val('110');PlanOfCare.Submit($(this),'editplanofcare');">Save &#38; Close</a></li>
    <li><a href="javascript:void(0);" onclick="$('#Edit_485_Status').val('115');PlanOfCare.Submit($(this),'editplanofcare');">Complete</a></li>
    <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editplanofcare');">Close</a></li>
</ul></div>
<% } %>
</div>
