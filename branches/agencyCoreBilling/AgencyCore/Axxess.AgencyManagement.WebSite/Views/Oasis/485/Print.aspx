﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PlanofCare>" %><%
var data = Model.ToDictionary();
var medicationList = Model.MedicationProfileData.IsNotNullOrEmpty() ? Model.MedicationProfileData.ToObject<List<Medication>>() : new List<Medication>();
var patient = Model.PatientData.IsNotNullOrEmpty() ? Model.PatientData.ToObject<Patient>() : new Patient();
var agency = Model.AgencyData.IsNotNullOrEmpty() ? Model.AgencyData.ToObject<Agency>() : new Agency();
var location = agency.GetBranch(patient != null ? patient.AgencyLocationId : Guid.Empty);
if (location == null) location = agency.GetMainOffice();
var physician = Model.PhysicianData.IsNotNullOrEmpty() ? Model.PhysicianData.ToObject<AgencyPhysician>() : new AgencyPhysician();
var functionLimitations = data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer != "" ? data["485FunctionLimitations"].Answer.Split(',') : null;
var activitiesPermitted = data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer != "" ? data["485ActivitiesPermitted"].Answer.Split(',') : null;
var mentalStatus = data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer != "" ? data["485MentalStatus"].Answer.Split(',') : null;
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + " | " : "" %>Plan of Care<%= patient != null ? (" | " + (patient.LastName.IsNotNullOrEmpty() ? patient.LastName.ToTitleCase() + ", " : "") + (patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.ToTitleCase() + " " : "") + (patient.MiddleInitial.IsNotNullOrEmpty() ? patient.MiddleInitial.ToUpper() : "")) : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Oasis/planofcare.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head><%
String
    loc06 =
        (patient.LastName.IsNotNullOrEmpty() ? patient.LastName.Clean() + ", " : string.Empty) +
        (patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.Clean() : string.Empty) +
        (patient.MiddleInitial.IsNotNullOrEmpty() ? " " + patient.MiddleInitial.Clean() + ".<br />" : "<br />") +
        (patient.AddressLine1.IsNotNullOrEmpty() ? patient.AddressLine1.Clean().ToTitleCase() : string.Empty) +
        (patient.AddressLine2.IsNotNullOrEmpty() ? " " + patient.AddressLine2.Clean().ToTitleCase() : string.Empty) +
        (patient.AddressCity.IsNotNullOrEmpty() ? "<br />" + patient.AddressCity.Clean().ToTitleCase() : string.Empty) +
        (patient.AddressStateCode.IsNotNullOrEmpty() ? ", " + patient.AddressStateCode.Clean().ToUpper() : string.Empty) +
        (patient.AddressZipCode.IsNotNullOrEmpty() ? " &#160;" + patient.AddressZipCode.Clean() : string.Empty) +
        (patient.PhoneHomeFormatted.IsNotNullOrEmpty() ? "<br />" + patient.PhoneHomeFormatted.Clean() : string.Empty),
    loc06short = 
        (patient.LastName.IsNotNullOrEmpty() ? patient.LastName.Clean() + ", " : string.Empty) +
        (patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.Clean() : string.Empty) +
        (patient.MiddleInitial.IsNotNullOrEmpty() ? " " + patient.MiddleInitial.Clean() + "." : string.Empty),
    loc07 =
        (agency.Name.IsNotNullOrEmpty() ? agency.Name.Clean().ToTitleCase() + "<br />" : string.Empty) +
        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : string.Empty) +
        (location.AddressLine2.IsNotNullOrEmpty() ? " " + location.AddressLine2.Clean().ToTitleCase() : string.Empty) +
        (location.AddressCity.IsNotNullOrEmpty() ? "<br />" + location.AddressCity.Clean().ToTitleCase() : string.Empty) +
        (location.AddressStateCode.IsNotNullOrEmpty() ? ", " + location.AddressStateCode.Clean() : string.Empty) +
        (location.AddressZipCode.IsNotNullOrEmpty() ? " &#160;" + location.AddressZipCode.Clean() : string.Empty) +
        (location.PhoneWork.IsNotNullOrEmpty() ? "<br />Phone: (" + location.PhoneWork.Clean().Substring(0, 3) + ") " + location.PhoneWork.Clean().Substring(3, 3) + "-" + location.PhoneWork.Clean().Substring(6, 4) : string.Empty) +
        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted.Clean() : string.Empty) +
        (agency.ContactPersonEmail.IsNotNullOrEmpty() ? "<br />Email:" + agency.ContactPersonEmail.Clean() : string.Empty),
    loc13a = string.Empty,
    loc13b = string.Empty,
    loc13boe = string.Empty,
    loc13c = string.Empty,
    loc13 = string.Empty,
    loc24 =
        (physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName.Clean() : string.Empty) +
        (physician.NPI.IsNotNullOrEmpty() ? "&#160;&#160;&#160;&#160;&#160;NPI: " + physician.NPI.ToString() + "<br />" : "<br />") +
        (physician.AddressFirstRow.IsNotNullOrEmpty() ? physician.AddressFirstRow.Clean() + "<br />" : string.Empty) +
        (physician.AddressSecondRow.IsNotNullOrEmpty() ? physician.AddressSecondRow.Clean() + "<br />" : string.Empty) +
        (physician.PhoneWorkFormatted.IsNotNullOrEmpty() ? "Phone: " + physician.PhoneWorkFormatted.Clean() : string.Empty) +
        (physician.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + physician.FaxNumberFormatted.Clean() : string.Empty);
for (int i = 1; i < 12; i++) {
    if (i < 5) {
        loc13a += data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() ? (i > 1 ? "<br />" : string.Empty) + data["M1022ICD9M" + i].Answer.Clean() : string.Empty;
        loc13b += data.ContainsKey("M1022PrimaryDiagnosis" + i) && data["M1022PrimaryDiagnosis" + i].Answer.IsNotNullOrEmpty() ? (i > 1 ? "<br />" : string.Empty) + data["M1022PrimaryDiagnosis" + i].Answer.Clean() : string.Empty;
        loc13boe += (data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis" + i) ? (data["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer.Equals("1") ? "E" : "") + (data["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer.Equals("2") ? "O" : "") : string.Empty) + "<br />";
        loc13c += data.ContainsKey("M1022PrimaryDiagnosis" + i + "Date") && data["M1022PrimaryDiagnosis" + i + "Date"].Answer.IsNotNullOrEmpty() ? (i > 1 ? "<br />" : string.Empty) + data["M1022PrimaryDiagnosis" + i + "Date"].Answer.Clean() : string.Empty;
    } else loc13 +=
        (data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() ? (i > 5 ? " <br />" : "") + data["M1022ICD9M" + i].Answer : string.Empty) +
        (data.ContainsKey("M1022PrimaryDiagnosis" + i) && data["M1022PrimaryDiagnosis" + i].Answer.IsNotNullOrEmpty() ? " / " + data["M1022PrimaryDiagnosis" + i].Answer : string.Empty) +
        (data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis" + i) ? (data["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer.Equals("1") ? " (E)" : "") + (data["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer.Equals("2") ? " (O)" : "") : "") +
        (data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() && data.ContainsKey("M1022PrimaryDiagnosis" + i + "Date") ? " / " + data["M1022PrimaryDiagnosis" + i + "Date"].Answer : "");
}
%>
<body>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true) 
     .DefaultGroup(group => group
     .Add("jquery-1.6.2.min.js")
     .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
     .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))
     .OnDocumentReady(() => {  %>
        PdfPrint.Fields = {
            cms48501: "<%= patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber.Clean() : patient.MedicaidNumber.IsNotNullOrEmpty() ? patient.MedicaidNumber.Clean() : string.Empty %>",
            cms48502: "<%= patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? patient.StartOfCareDateFormatted.Clean() : string.Empty %>",
            cms48503a: "<%= Model.EpisodeStart.IsNotNullOrEmpty() ? Model.EpisodeStart.Clean() : string.Empty %>",
            cms48503b: "<%= Model.EpisodeEnd.IsNotNullOrEmpty() ? Model.EpisodeEnd.Clean() : string.Empty%>",
            cms48504: "<%= patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber.Clean() : string.Empty %>",
            cms48505: "<%= location.MedicareProviderNumber.IsNotNullOrEmpty() ? location.MedicareProviderNumber.Clean() : string.Empty %>",
            cms48506: "<%= loc06 %>",
            cms48507: "<%= loc07 %>",
            cms48508: "<%= patient.DOBFormatted.IsNotNullOrEmpty() ? patient.DOBFormatted.Clean() : string.Empty %>",
            cms48509m: "<%= patient.Gender.IsNotNullOrEmpty() && patient.Gender.Equals("Male") ? "X" : string.Empty %>",
            cms48509f: "<%= patient.Gender.IsNotNullOrEmpty() && patient.Gender.Equals("Female") ? "X" : string.Empty %>",
            cms48511a: "<%= data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer.Clean() : string.Empty %>",
            cms48511b: "<%= data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer.Clean() : string.Empty %>",
            cms48511boe: "<%= data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis") ? (data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer.Equals("1") ? "E" : "") + (data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer.Equals("2") ? "O" : "") : string.Empty%>",
            cms48511c: "<%= data.ContainsKey("M1020PrimaryDiagnosisDate") ? data["M1020PrimaryDiagnosisDate"].Answer.Clean() : string.Empty %>",
            cms48512a: "<%= data.ContainsKey("485SurgicalProcedureCode1") ? data["485SurgicalProcedureCode1"].Answer.Clean() : string.Empty %>",
            cms48512b: "<%= data.ContainsKey("485SurgicalProcedureDescription1") ? data["485SurgicalProcedureDescription1"].Answer.Clean() : string.Empty %>",
            cms48512c: "<%= data.ContainsKey("485SurgicalProcedureCode1Date") ? data["485SurgicalProcedureCode1Date"].Answer.Clean() : string.Empty %>",
            cms48513a: "<%= loc13a %>",
            cms48513b: "<%= loc13b %>",
            cms48513boe: "<%= loc13boe %>",
            cms48513c: "<%= loc13c %>",
            cms48518a1: "<%= functionLimitations != null && functionLimitations.Contains("1") ? "X" : string.Empty %>",
            cms48518a2: "<%= functionLimitations != null && functionLimitations.Contains("2") ? "X" : string.Empty %>",
            cms48518a3: "<%= functionLimitations != null && functionLimitations.Contains("3") ? "X" : string.Empty %>",
            cms48518a4: "<%= functionLimitations != null && functionLimitations.Contains("4") ? "X" : string.Empty %>",
            cms48518a5: "<%= functionLimitations != null && functionLimitations.Contains("5") ? "X" : string.Empty %>",
            cms48518a6: "<%= functionLimitations != null && functionLimitations.Contains("6") ? "X" : string.Empty %>",
            cms48518a7: "<%= functionLimitations != null && functionLimitations.Contains("7") ? "X" : string.Empty %>",
            cms48518a8: "<%= functionLimitations != null && functionLimitations.Contains("8") ? "X" : string.Empty %>",
            cms48518a9: "<%= functionLimitations != null && functionLimitations.Contains("9") ? "X" : string.Empty %>",
            cms48518aa: "<%= functionLimitations != null && functionLimitations.Contains("A") ? "X" : string.Empty %>",
            cms48518ab: "<%= functionLimitations != null && functionLimitations.Contains("B") ? "X" : string.Empty %>",
            cms48518b1: "<%= activitiesPermitted != null && activitiesPermitted.Contains("1") ? "X" : string.Empty %>",
            cms48518b2: "<%= activitiesPermitted != null && activitiesPermitted.Contains("2") ? "X" : string.Empty %>",
            cms48518b3: "<%= activitiesPermitted != null && activitiesPermitted.Contains("3") ? "X" : string.Empty %>",
            cms48518b4: "<%= activitiesPermitted != null && activitiesPermitted.Contains("4") ? "X" : string.Empty %>",
            cms48518b5: "<%= activitiesPermitted != null && activitiesPermitted.Contains("5") ? "X" : string.Empty %>",
            cms48518b6: "<%= activitiesPermitted != null && activitiesPermitted.Contains("6") ? "X" : string.Empty %>",
            cms48518b7: "<%= activitiesPermitted != null && activitiesPermitted.Contains("7") ? "X" : string.Empty %>",
            cms48518b8: "<%= activitiesPermitted != null && activitiesPermitted.Contains("8") ? "X" : string.Empty %>",
            cms48518b9: "<%= activitiesPermitted != null && activitiesPermitted.Contains("9") ? "X" : string.Empty %>",
            cms48518ba: "<%= activitiesPermitted != null && activitiesPermitted.Contains("A") ? "X" : string.Empty %>",
            cms48518bb: "<%= activitiesPermitted != null && activitiesPermitted.Contains("B") ? "X" : string.Empty %>",
            cms48518bc: "<%= activitiesPermitted != null && activitiesPermitted.Contains("C") ? "X" : string.Empty %>",
            cms48518bd: "<%= activitiesPermitted != null && activitiesPermitted.Contains("D") ? "X" : string.Empty %>",
            cms485191: "<%= mentalStatus != null && mentalStatus.Contains("1") ? "X" : string.Empty%>",
            cms485192: "<%= mentalStatus != null && mentalStatus.Contains("2") ? "X" : string.Empty%>",
            cms485193: "<%= mentalStatus != null && mentalStatus.Contains("3") ? "X" : string.Empty%>",
            cms485194: "<%= mentalStatus != null && mentalStatus.Contains("4") ? "X" : string.Empty%>",
            cms485195: "<%= mentalStatus != null && mentalStatus.Contains("5") ? "X" : string.Empty%>",
            cms485196: "<%= mentalStatus != null && mentalStatus.Contains("6") ? "X" : string.Empty%>",
            cms485197: "<%= mentalStatus != null && mentalStatus.Contains("7") ? "X" : string.Empty%>",
            cms485198: "<%= mentalStatus != null && mentalStatus.Contains("8") ? "X" : string.Empty%>",
            cms485201: "<%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Poor" ? "X" : string.Empty %>",
            cms485202: "<%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Guarded" ? "X" : string.Empty %>",
            cms485203: "<%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Fair" ? "X" : string.Empty %>",
            cms485204: "<%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Good" ? "X" : string.Empty %>",
            cms485205: "<%= data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Excellent" ? "X" : string.Empty %>",
            cms48523: "<%= Model != null ? Model.SignatureText : string.Empty %> <%= data.ContainsKey("M0102PhysicianOrderedDate") ? data["M0102PhysicianOrderedDate"].Answer.Clean() : (Model != null && Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString() : string.Empty)%>",
            cms48524: "<%= loc24 %>",
            cms48526a: "<%= Model.AssessmentType == "StartOfCare" ? "____" : string.Empty %>",
            cms48526b: "<%= Model.AssessmentType == "Recertification" || Model.AssessmentType == "ResumptionOfCare" ? "_____" : string.Empty %>",
            cms48701: "<%= patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber.Clean() : string.Empty %>",
            cms48702: "<%= patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? patient.StartOfCareDateFormatted.Clean() : string.Empty %>",
            cms48703a: "<%= Model.EpisodeStart.IsNotNullOrEmpty() ? Model.EpisodeStart.Clean() : string.Empty %>",
            cms48703b: "<%= Model.EpisodeEnd.IsNotNullOrEmpty() ? Model.EpisodeEnd.Clean() : string.Empty%>",
            cms48704: "<%= patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber.Clean() : string.Empty %>",
            cms48705: "<%= location.MedicareProviderNumber.IsNotNullOrEmpty() ? location.MedicareProviderNumber.Clean() : string.Empty %>",
            cms48706: "<%= loc06short %>",
            cms48707: "<%= agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() : string.Empty %>",
            cms487sign: "<%= Model != null ? Model.SignatureText : string.Empty %>",
            cms487date: "<%= Model != null && Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString() : string.Empty %>"
        };
        PdfPrint.BuildPlanOfCare([{
            Loc: 10,
            Title: "Medications",
            Data: "<%= data.ContainsKey("485Medications") && data["485Medications"].Answer.IsNotNullOrEmpty() ? data["485Medications"].Answer.Clean().Replace(", ", "<br/>") : string.Empty %>"
        },{
            Loc: 12,
            Title: "Surgical Procedure",
            Data: "<%= 
                (data.ContainsKey("485SurgicalProcedureCode2") && data["485SurgicalProcedureCode2"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode2"].Answer.Clean() : string.Empty) +
                (data.ContainsKey("485SurgicalProcedureDescription2") && data["485SurgicalProcedureDescription2"].Answer.IsNotNullOrEmpty() ? " / " + data["485SurgicalProcedureDescription2"].Answer : string.Empty) +
                (data.ContainsKey("485SurgicalProcedureCode2Date") && data["485SurgicalProcedureCode2Date"].Answer.IsNotNullOrEmpty() ? " / " + data["485SurgicalProcedureCode2Date"].Answer : string.Empty) %>"
        },{
            Loc: 13,
            Title: "Diagnoses",
            Data: "<%= loc13 %>"
        },{
            Loc: 14,
            Title: "DME",
            Data: "<%=
                (data.ContainsKey("485DME") && data["485DME"].Answer.IsNotNullOrEmpty() ? PlanofCareXml.LookupText("DME", data["485DME"].Answer).Clean() : string.Empty) +
                (data.ContainsKey("485DMEComments") && data["485DMEComments"].Answer.IsNotNullOrEmpty() ? ", " + data["485DMEComments"].Answer.Clean() : string.Empty) +
                (data.ContainsKey("485Supplies") && data["485Supplies"].Answer.IsNotNullOrEmpty() ? ", " + PlanofCareXml.LookupText("Supplies", data["485Supplies"].Answer).Clean() : string.Empty) +
                (data.ContainsKey("485SuppliesComment") && data["485SuppliesComment"].Answer.IsNotNullOrEmpty() ? ", " + data["485SuppliesComment"].Answer.Clean() : string.Empty) %>"
        },{
            Loc: 15,
            Title: "Safety Measures",
            Data: "<%=
                (data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.IsNotNullOrEmpty() ? PlanofCareXml.LookupText("SafetyMeasures", data["485SafetyMeasures"].Answer).Clean() : string.Empty) +
                (data.ContainsKey("485OtherSafetyMeasures") && data["485OtherSafetyMeasures"].Answer.IsNotNullOrEmpty() ? ", " + data["485OtherSafetyMeasures"].Answer.Clean() : string.Empty) %>"
        },{
            Loc: 16,
            Title: "Nutrition",
            Data: "<%= data.ContainsKey("485NutritionalReqs") ? data["485NutritionalReqs"].Answer.Clean() : string.Empty  %>"
        },{
            Loc: 17,
            Title: "Allergies",
            Data: "<%= data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "No" ? "NKA (Food/Drugs/Latex)" : "Allergic to:" + (data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer.Clean() : string.Empty) %>"
        },{
            Loc: "18A",
            Title: "Functional Limitations",
            Data: "<%= functionLimitations != null && functionLimitations.Contains("B") && data.ContainsKey("485FunctionLimitationsOther") && data["485FunctionLimitationsOther"].Answer.IsNotNullOrEmpty() ? data["485FunctionLimitationsOther"].Answer.Clean() : string.Empty %>"
        },{
            Loc: "18B",
            Title: "Activities Permitted",
            Data: "<%= activitiesPermitted != null && activitiesPermitted.Contains("D") && data.ContainsKey("485ActivitiesPermittedOther") && data["485ActivitiesPermittedOther"].Answer.IsNotNullOrEmpty() ? data["485ActivitiesPermittedOther"].Answer.Clean() : string.Empty%>"
        },{
            Loc: 19,
            Title: "Mental Status",
            Data: "<%= mentalStatus != null && mentalStatus.Contains("8") && data.ContainsKey("485MentalStatusOther") && data["485MentalStatusOther"].Answer.IsNotNullOrEmpty() ? data["485MentalStatusOther"].Answer.Clean() : string.Empty %>"
        },{
            Loc: 21,
            Title: "Orders",
            Data: "<%= data.ContainsKey("485Interventions") ? data["485Interventions"].Answer.Clean() : string.Empty%>"
        },{
            Loc: 22,
            Title: "Goals",
            Data: "<%= data.ContainsKey("485Goals") ? data["485Goals"].Answer.Clean() : string.Empty%>"
        }]);
    <% }).Render(); %>
</body>
</html>