﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PlanofCare>" %>
<% var data = Model.ToDictionary(); %>
<% string[] dME = data.ContainsKey("485DME") && data["485DME"].Answer != "" ? data["485DME"].Answer.Split(',') : null; %>
<% string[] supplies = data.ContainsKey("485Supplies") && data["485Supplies"].Answer != "" ? data["485Supplies"].Answer.Split(',') : null; %>
<% string[] safetyMeasure = data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer != "" ? data["485SafetyMeasures"].Answer.Split(',') : null; %>
<% string[] functionLimitations = data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer != "" ? data["485FunctionLimitations"].Answer.Split(',') : null; %>
<% string[] activitiesPermitted = data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer != "" ? data["485ActivitiesPermitted"].Answer.Split(',') : null; %>
<% string[] mentalStatus = data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer != "" ? data["485MentalStatus"].Answer.Split(',') : null; %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th>Medications (Locator #10): Dose/Frequency/Route (N)ew (C)hanged</th>
        </tr><tr>
            <td>
                <div class="buttons float_left">
                    <ul>
                        <li><a href="javascript:void(0);" onclick="UserInterface.ShowMedicationModal('<%= Model.PatientId %>', 'false');">Add/Edit Medications</a></li>
                    </ul>
                </div>
                <div class="clear"></div>
                <%= Html.TextArea("485Medications", data.ContainsKey("485Medications") && data["485Medications"].Answer.IsNotNullOrEmpty() ? data["485Medications"].Answer : "", new { @id = "Edit_485_Medications", @class = "fill", @style = "height: 180px; font-size: 16px;" })%>
            </td>
        </tr><tr>
            <th>Principal Diagnosis (Locator #11)/Other Pertinent Diagnosis (Locator #13)</th>
        </tr><tr>
            <td>
                <ul class="oasis_diagnoses cms485_diagnoses"></ul>
            </td>
        </tr><tr>
            <th>Surgical Procedure (Locator #12)</th>
        </tr><tr>
            <td>
                <table class="form layout_auto align_center">
                    <thead>
                        <tr>
                            <th>Surgical Procedure</th>
                            <th>Code</th>
                            <th>Date</th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td><%= Html.TextBox("485SurgicalProcedureDescription1", data.ContainsKey("485SurgicalProcedureDescription1") && data["485SurgicalProcedureDescription1"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureDescription1"].Answer : "", new { @class = "procedureDiagnosis", @id = "Edit_485_485SurgicalProcedureDescription1", @maxlength = "200" })%></td>
                            <td><%= Html.TextBox("485SurgicalProcedureCode1", data.ContainsKey("485SurgicalProcedureCode1") && data["485SurgicalProcedureCode1"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode1"].Answer : "", new { @class = "procedureICD pad", @id = "Edit_485_485SurgicalProcedureCode1", @maxlength = "7" })%></td>
                            <td><input type="date" name="485SurgicalProcedureCode1Date" value="<%= data.ContainsKey("485SurgicalProcedureCode1Date") && data["485SurgicalProcedureCode1Date"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode1Date"].Answer : string.Empty %>" id="Edit_485_SurgicalProcedureCode1Date" /></td>
                        </tr><tr>
                            <td><%= Html.TextBox("485SurgicalProcedureDescription2", data.ContainsKey("485SurgicalProcedureDescription2") && data["485SurgicalProcedureDescription2"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureDescription2"].Answer : "", new { @class = "procedureDiagnosis", @id = "Edit_485_SurgicalProcedureDescription2", @maxlength = "200" })%></td>
                            <td><%= Html.TextBox("485SurgicalProcedureCode2", data.ContainsKey("485SurgicalProcedureCode2") && data["485SurgicalProcedureCode2"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode2"].Answer : "", new { @class = "procedureICD pad", @id = "Edit_485_SurgicalProcedureCode2", @maxlength = "7" })%></td>
                            <td><input type="date" name="485SurgicalProcedureCode2Date" value="<%= data.ContainsKey("485SurgicalProcedureCode2Date") && data["485SurgicalProcedureCode2Date"].Answer.IsNotNullOrEmpty() ? data["485SurgicalProcedureCode2Date"].Answer : string.Empty %>" id="Date1" /></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th>DME and Supplies (Locator #14)</th>
        </tr><tr>
            <td>
                <label class="float_left">DME:</label>
                <div class="clear"></div>
                <input type="hidden" name="485DME" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='485DME1' class='radio float_left' name='485DME' value='1' type='checkbox' {0} />", dME != null && dME.Contains("1") ? "checked='checked'" : "") %>
                                <label for="485DME1" class="radio">Bedside Commode</label>
                            </td><td>
                                <%= string.Format("<input id='485DME2' class='radio float_left' name='485DME' value='2' type='checkbox' {0} />", dME != null && dME.Contains("2") ? "checked='checked'" : "") %>
                                <label for="485DME2" class="radio">Cane</label>
                            </td><td>
                                <%= string.Format("<input id='485DME3' class='radio float_left' name='485DME' value='3' type='checkbox' {0} />", dME != null && dME.Contains("3") ? "checked='checked'" : "") %>
                                <label for="485DME3" class="radio">Elevated Toilet Seat</label>
                            </td><td>
                                <%= string.Format("<input id='485DME4' class='radio float_left' name='485DME' value='4' type='checkbox' {0} />", dME != null && dME.Contains("4") ? "checked='checked'" : "") %>
                                <label for="485DME4" class="radio">Grab Bars</label>
                            </td><td>
                                <%= string.Format("<input id='485DME5' class='radio float_left' name='485DME' value='5' type='checkbox' {0} />", dME != null && dME.Contains("5") ? "checked='checked'" : "") %>
                                <label for="485DME5" class="radio">Hospital Bed</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='485DME6' class='radio float_left' name='485DME' value='6' type='checkbox' {0} />", dME != null && dME.Contains("6") ? "checked='checked'" : "") %>
                                <label for="485DME6" class="radio">Nebulizer</label>
                            </td><td>
                                <%= string.Format("<input id='485DME7' class='radio float_left' name='485DME' value='7' type='checkbox' {0} />", dME != null && dME.Contains("7") ? "checked='checked'" : "") %>
                                <label for="485DME7" class="radio">Oxygen</label>
                            </td><td>
                                <%= string.Format("<input id='485DME8' class='radio float_left' name='485DME' value='8' type='checkbox' {0} />", dME != null && dME.Contains("8") ? "checked='checked'" : "") %>
                                <label for="485DME8" class="radio">Tub/Shower Bench</label>
                            </td><td>
                                <%= string.Format("<input id='485DME9' class='radio float_left' name='485DME' value='9' type='checkbox' {0} />", dME != null && dME.Contains("9") ? "checked='checked'" : "") %>
                                <label for="485DME9" class="radio">Walker</label>
                            </td><td>
                                <%= string.Format("<input id='485DME10' class='radio float_left' name='485DME' value='10' type='checkbox' {0} />", dME != null && dME.Contains("10") ? "checked='checked'" : "") %>
                                <label for="485DME10" class="radio">Wheelchair</label>
                            </td>
                        </tr><tr>
                            <td colspan="5">
                                <label for="485DMEComments" class="float_left">Other:</label>
                                <div class="clear"></div>
                                <%= Html.TextArea("485DMEComments", data.ContainsKey("485DMEComments") ? data["485DMEComments"].Answer : "", new { @id = "Edit_485_485DMEComments", @class = "fill", @style = "height: 90px; font-size: 16px;" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <label class="float_left">Supplies:</label>
                <div class="clear"></div>
                <input type="hidden" name="485Supplies" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='485Supplies1' class='radio float_left' name='485Supplies' value='1' type='checkbox' {0} />", supplies != null && supplies.Contains("1") ? "checked='checked'" : "")%>
                                <label for="485Supplies1" class="radio">ABDs</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies2' class='radio float_left' name='485Supplies' value='2' type='checkbox' {0} />", supplies != null && supplies.Contains("2") ? "checked='checked'" : "")%>
                                <label for="485Supplies2" class="radio">Ace Wrap</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies3' class='radio float_left' name='485Supplies' value='3' type='checkbox' {0} />", supplies != null && supplies.Contains("3") ? "checked='checked'" : "")%>
                                <label for="485Supplies3" class="radio">Alcohol Pads</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies4' class='radio float_left' name='485Supplies' value='4' type='checkbox' {0} />", supplies != null && supplies.Contains("4") ? "checked='checked'" : "")%>
                                <label for="485Supplies4" class="radio">Chux/Underpads</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies5' class='radio float_left' name='485Supplies' value='5' type='checkbox' {0} />", supplies != null && supplies.Contains("5") ? "checked='checked'" : "")%>
                                <label for="485Supplies5" class="radio">Diabetic Supplies</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='485Supplies6' class='radio float_left' name='485Supplies' value='6' type='checkbox' {0} />", supplies != null && supplies.Contains("6") ? "checked='checked'" : "")%>
                                <label for="485Supplies6" class="radio">Drainage Bag</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies7' class='radio float_left' name='485Supplies' value='7' type='checkbox' {0} />", supplies != null && supplies.Contains("7") ? "checked='checked'" : "")%>
                                <label for="485Supplies7" class="radio">Dressing Supplies</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies8' class='radio float_left' name='485Supplies' value='8' type='checkbox' {0} />", supplies != null && supplies.Contains("8") ? "checked='checked'" : "")%>
                                <label for="485Supplies8" class="radio">Duoderm</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies9' class='radio float_left' name='485Supplies' value='9' type='checkbox' {0} />", supplies != null && supplies.Contains("9") ? "checked='checked'" : "")%>
                                <label for="485Supplies9" class="radio">Exam Gloves</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies10' class='radio float_left' name='485Supplies' value='10' type='checkbox' {0} />", supplies != null && supplies.Contains("10") ? "checked='checked'" : "")%>
                                <label for="485Supplies10" class="radio">Foley Catheter</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='485Supplies11' class='radio float_left' name='485Supplies' value='11' type='checkbox' {0} />", supplies != null && supplies.Contains("11") ? "checked='checked'" : "")%>
                                <label for="485Supplies11" class="radio">Gauze Pads</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies12' class='radio float_left' name='485Supplies' value='12' type='checkbox' {0} />", supplies != null && supplies.Contains("12") ? "checked='checked'" : "")%>
                                <label for="485Supplies12" class="radio">Insertion Kit</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies13' class='radio float_left' name='485Supplies' value='13' type='checkbox' {0} />", supplies != null && supplies.Contains("13") ? "checked='checked'" : "")%>
                                <label for="485Supplies13" class="radio">Irrigation Set</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies14' class='radio float_left' name='485Supplies' value='14' type='checkbox' {0} />", supplies != null && supplies.Contains("14") ? "checked='checked'" : "")%>
                                <label for="485Supplies14" class="radio">Irrigation Solution</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies15' class='radio float_left' name='485Supplies' value='15' type='checkbox' {0} />", supplies != null && supplies.Contains("15") ? "checked='checked'" : "")%>
                                <label for="485Supplies15" class="radio">Kerlix Rolls</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='485Supplies16' class='radio float_left' name='485Supplies' value='16' type='checkbox' {0} />", supplies != null && supplies.Contains("16") ? "checked='checked'" : "")%>
                                <label for="485Supplies16" class="radio">Leg Bag</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies17' class='radio float_left' name='485Supplies' value='17' type='checkbox' {0} />", supplies != null && supplies.Contains("17") ? "checked='checked'" : "")%>
                                <label for="485Supplies17" class="radio">Needles</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies18' class='radio float_left' name='485Supplies' value='18' type='checkbox' {0} />", supplies != null && supplies.Contains("18") ? "checked='checked'" : "")%>
                                <label for="485Supplies18" class="radio">NG Tube</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies19' class='radio float_left' name='485Supplies' value='19' type='checkbox' {0} />", supplies != null && supplies.Contains("19") ? "checked='checked'" : "")%>
                                <label for="485Supplies19" class="radio">Probe Covers</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies20' class='radio float_left' name='485Supplies' value='20' type='checkbox' {0} />", supplies != null && supplies.Contains("20") ? "checked='checked'" : "")%>
                                <label for="485Supplies20" class="radio">Sharps Container</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='485Supplies21' class='radio float_left' name='485Supplies' value='21' type='checkbox' {0} />", supplies != null && supplies.Contains("21") ? "checked='checked'" : "")%>
                                <label for="485Supplies21" class="radio">Sterile Gloves</label>
                            </td><td>
                                <%= string.Format("<input id='485Supplies22' class='radio float_left' name='485Supplies' value='22' type='checkbox' {0} />", supplies != null && supplies.Contains("22") ? "checked='checked'" : "")%>
                                <label for="485Supplies22" class="radio">Syringe</label>
                            </td><td colspan="3">
                                <%= string.Format("<input id='485Supplies23' class='radio float_left' name='485Supplies' value='23' type='checkbox' {0} />", supplies != null && supplies.Contains("23") ? "checked='checked'" : "")%>
                                <label for="485Supplies23" class="radio">Tape</label>
                            </td>
                        </tr><tr>
                            <td colspan="5">
                                <label for="485SuppliesComment" class="float_left">Other:</label>
                                <div class="clear"></div>
                                <%= Html.TextArea("485SuppliesComment", data.ContainsKey("485SuppliesComment") ? data["485SuppliesComment"].Answer : "", 5, 70, new { @id = "485SuppliesComment", @class = "fill", @style = "height: 90px; font-size: 16px;" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>        
        </tr><tr>
            <th>Safety Measures (Locator #15)</th>
        </tr><tr>
            <td>
                <input type="hidden" name="485SafetyMeasures" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures1' name='485SafetyMeasures' value='1' {0} />", safetyMeasure != null && safetyMeasure.Contains("1") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures1" class="radio">Anticoagulant Precautions</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures2' name='485SafetyMeasures' value='2' {0} />", safetyMeasure != null && safetyMeasure.Contains("2") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures2" class="radio">Emergency Plan Developed</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures3' name='485SafetyMeasures' value='3' {0} />", safetyMeasure != null && safetyMeasure.Contains("3") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures3" class="radio">Fall Precautions</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures4' name='485SafetyMeasures' value='4' {0} />", safetyMeasure != null && safetyMeasure.Contains("4") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures4" class="radio">Keep Pathway Clear</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures5' name='485SafetyMeasures' value='5' {0} />", safetyMeasure != null && safetyMeasure.Contains("5") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures5" class="radio">Keep Side Rails Up</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures6' name='485SafetyMeasures' value='6' {0} />", safetyMeasure != null && safetyMeasure.Contains("6") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures6" class="radio">Neutropenic Precautions</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures7' name='485SafetyMeasures' value='7' {0} />", safetyMeasure != null && safetyMeasure.Contains("7") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures7" class="radio">O2 Precautions</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures8' name='485SafetyMeasures' value='8' {0} />", safetyMeasure != null && safetyMeasure.Contains("8") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures8" class="radio">Proper Position During Meals</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures9' name='485SafetyMeasures' value='9' {0} />", safetyMeasure != null && safetyMeasure.Contains("9") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures9" class="radio">Safety in ADLs</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures10' name='485SafetyMeasures' value='10' {0} />", safetyMeasure != null && safetyMeasure.Contains("10") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures10" class="radio">Seizure Precautions</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures11' name='485SafetyMeasures' value='11' {0} />", safetyMeasure != null && safetyMeasure.Contains("11") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures11" class="radio">Sharps Safety</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures12' name='485SafetyMeasures' value='12' {0} />", safetyMeasure != null && safetyMeasure.Contains("12") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures12" class="radio">Slow Position Change</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures13' name='485SafetyMeasures' value='13' {0} />", safetyMeasure != null && safetyMeasure.Contains("13") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures13" class="radio">Standard Precautions/ Infection Control</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures14' name='485SafetyMeasures' value='14' {0} />", safetyMeasure != null && safetyMeasure.Contains("14") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures14" class="radio">Support During Transfer and Ambulation</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures15' name='485SafetyMeasures' value='15' {0} />", safetyMeasure != null && safetyMeasure.Contains("15") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures15" class="radio">Use of Assistive Devices</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures16' name='485SafetyMeasures' value='16' {0} />", safetyMeasure != null && safetyMeasure.Contains("16") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures16" class="radio">Instructed on safe utilities management</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures17' name='485SafetyMeasures' value='17' {0} />", safetyMeasure != null && safetyMeasure.Contains("17") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures17" class="radio">Instructed on mobility safety</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures18' name='485SafetyMeasures' value='18' {0} />", safetyMeasure != null && safetyMeasure.Contains("18") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures18" class="radio">Instructed on DME &#38; electrical safety</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures19' name='485SafetyMeasures' value='19' {0} />", safetyMeasure != null && safetyMeasure.Contains("19") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures19" class="radio">Instructed on sharps container</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures20' name='485SafetyMeasures' value='20' {0} />", safetyMeasure != null && safetyMeasure.Contains("20") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures20" class="radio">Instructed on medical gas</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures21' name='485SafetyMeasures' value='21' {0} />", safetyMeasure != null && safetyMeasure.Contains("21") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures21" class="radio">Instructed on disaster/ emergency plan</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures22' name='485SafetyMeasures' value='22' {0} />", safetyMeasure != null && safetyMeasure.Contains("22") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures22" class="radio">Instructed on safety measures</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' type='checkbox' id='485SafetyMeasures23' name='485SafetyMeasures' value='23' {0} />", safetyMeasure != null && safetyMeasure.Contains("23") ? "checked='checked'" : "") %>
                                <label for="485SafetyMeasures23" class="radio">Instructed on proper handling of biohazard waste</label>
                            </td><td>
                            </td>
                        </tr><tr>
                            <td colspan="3">
                                <label for="485SafetyMeasuresComments" class="float_left">Other:</label>
                                <div class="clear"></div>
                                <%= Html.TextArea("485OtherSafetyMeasures", data.ContainsKey("485OtherSafetyMeasures") ? data["485OtherSafetyMeasures"].Answer : "", new { @id = "485OtherSafetyMeasures", @class = "fill", @style = "height: 90px; font-size: 16px;" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th>Nutritional Req. (Locator #16)</th>
        </tr><tr>
            <td>
                <%= Html.Templates("485NutritionTemplates", new { @class = "Templates float_left", @template = "#485NutritionalReqs" })%>
                <div class="clear"></div>
                <%= Html.TextArea("485NutritionalReqs", data.ContainsKey("485NutritionalReqs") && data["485NutritionalReqs"].Answer.IsNotNullOrEmpty() ? data["485NutritionalReqs"].Answer : "", new { @id = "485NutritionalReqs", @class = "fill", @style = "height: 180px; font-size: 16px;" })%>
            </td>
        </tr><tr>
            <th>Allergies (Locator #17)</th>
        </tr><tr>
            <td>
                <%= Html.Hidden("485Allergies"," ", new { @id = "" }) %>
                <%= Html.RadioButton("485Allergies", "No", data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "No" ? true : false, new { @id = "Edit_485_Allergies_NKA", @class = "radio float_left" })%>
                <label for="Edit_485_Allergies_NKA" class="float_left">NKA (Food/Drugs/Latex)</label>
                <div class="clear"></div>
                <%= Html.RadioButton("485Allergies", "Yes", data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "Yes" ? true : false, new { @id = "Edit_485_Allergies_Yes", @class = "radio float_left" })%>
                <label for="Edit_485_Allergies_Yes" class="float_left">Allergic to:</label>
                <div class="clear"></div>
                <%= Html.TextArea("485AllergiesDescription", data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : "", new { @id = "Edit_485_Allergies_Specify", @class = "fill", @style = "height: 90px; font-size: 16px;" })%>
            </td>
        </tr><tr>
            <th>Functional Limitations (Locator #18.A)</th>
        </tr><tr>
            <td>
                <input type="hidden" name="485FunctionLimitations" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='485FunctionLimitations1' name='485FunctionLimitations' value='1' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("1") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitations1" class="radio">Amputation</label>
                            </td><td>
                                <%= string.Format("<input id='485FunctionLimitations5' name='485FunctionLimitations' value='5' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("5") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitations5" class="radio">Paralysis</label>
                            </td><td>
                                <%= string.Format("<input id='485FunctionLimitations9' name='485FunctionLimitations' value='9' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("9") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitations9" class="radio">Legally Blind</label>
                            </td><td>
                                <%= string.Format("<input id='485FunctionLimitations2' name='485FunctionLimitations' value='2' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("2") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitations2" class="radio">Bowel/Bladder Incontinence</label>
                            </td><td>
                                <%= string.Format("<input id='485FunctionLimitations6' name='485FunctionLimitations' value='6' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("6") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitations6" class="radio">Endurance</label></td>
                        </tr><tr> 
                            <td>
                                <%= string.Format("<input id='485FunctionLimitationsA' name='485FunctionLimitations' value='A' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("A") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitationsA" class="radio">Dyspnea with Minimal Exertion</label>
                            </td><td>
                                <%= string.Format("<input id='485FunctionLimitations3' name='485FunctionLimitations' value='3' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("3") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitations3" class="radio">Contracture</label>
                            </td><td>
                                <%= string.Format("<input id='485FunctionLimitations7' name='485FunctionLimitations' value='7' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("7") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitations7" class="radio">Ambulation</label>
                            </td><td>
                                <%= string.Format("<input id='485FunctionLimitations4' name='485FunctionLimitations' value='4' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("4") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitations4" class="radio">Hearing</label>
                            </td><td>
                                <%= string.Format("<input id='485FunctionLimitations8' name='485FunctionLimitations' value='8' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("8") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitations8" class="radio">Speech</label>
                            </td>
                        </tr><tr>
                            <td colspan="5">
                                <%= string.Format("<input id='485FunctionLimitationsB' name='485FunctionLimitations' value='B' class='radio float_left' type='checkbox' {0} />", functionLimitations != null && functionLimitations.Contains("B") ? "checked='checked'" : "")%>
                                <label for="485FunctionLimitationsB" class="radio">Other (Specify)</label>
                                <div class="clear"></div>
                                <%= Html.TextArea("485FunctionLimitationsOther", data.ContainsKey("485FunctionLimitationsOther") ? data["485FunctionLimitationsOther"].Answer : "", new { @id = "485FunctionLimitationsOther", @class = "fill", @style = "height: 90px; font-size: 16px;" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th>Activities Permitted (Locator #18.B)</th>
        </tr><tr>
            <td>
                <input type="hidden" name="485ActivitiesPermitted" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='485ActivitiesPermitted1' name='485ActivitiesPermitted' value='1' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermitted1" class="radio">Complete bed rest</label>
                            </td><td>
                                <%= string.Format("<input id='485ActivitiesPermitted3' name='485ActivitiesPermitted' value='3' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermitted3" class="radio">Up as tolerated</label>
                            </td><td>
                                <%= string.Format("<input id='485ActivitiesPermitted5' name='485ActivitiesPermitted' value='5' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermitted5" class="radio">Exercise prescribed</label>
                            </td><td>
                                <%= string.Format("<input id='485ActivitiesPermitted7' name='485ActivitiesPermitted' value='7' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("7") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermitted7" class="radio">Independent at home</label>
                            </td><td>
                                <%= string.Format("<input id='485ActivitiesPermitted9' name='485ActivitiesPermitted' value='9' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("9") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermitted9" class="radio">Cane</label>
                            </td><td>
                                <%= string.Format("<input id='485ActivitiesPermittedB' name='485ActivitiesPermitted' value='B' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("B") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermittedB" class="radio">Walker</label>
                            </td>
                        </tr><tr> 
                            <td>
                                <%= string.Format("<input id='485ActivitiesPermitted2' name='485ActivitiesPermitted' value='2' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermitted2" class="radio">Bed rest with BRP</label>
                            </td><td>
                                <%= string.Format("<input id='485ActivitiesPermitted4' name='485ActivitiesPermitted' value='4' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermitted4" class="radio">Transfer bed-chair</label>
                            </td><td>
                                <%= string.Format("<input id='485ActivitiesPermitted6' name='485ActivitiesPermitted' value='6' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermitted6" class="radio">Partial weight bearing</label>
                            </td><td>
                                <%= string.Format("<input id='485ActivitiesPermitted8' name='485ActivitiesPermitted' value='8' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("8") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermitted8" class="radio">Crutches</label>
                            </td><td>
                                <%= string.Format("<input id='485ActivitiesPermittedA' name='485ActivitiesPermitted' value='A' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("A") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermittedA" class="radio">Wheelchair</label>
                            </td><td>
                                <%= string.Format("<input id='485ActivitiesPermittedC' name='485ActivitiesPermitted' value='C' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("C") ? "checked='checked'" : "" ) %>
                                <label for="485ActivitiesPermittedC" class="radio">No Restrictions</label>
                            </td>
                        </tr><tr>
                            <td colspan="6">
                                <%= string.Format("<input id='485ActivitiesPermittedD' name='485ActivitiesPermitted' value='D' class='radio float_left' type='checkbox' {0} />", activitiesPermitted != null && activitiesPermitted.Contains("D") ? "checked='checked'" : "")%>
                                <label for="485ActivitiesPermittedD" class="radio">Other (Specify)</label>
                                <div class="clear"></div>
                                <%= Html.TextArea("485ActivitiesPermittedOther", data.ContainsKey("485ActivitiesPermittedOther") ? data["485ActivitiesPermittedOther"].Answer : "", new { @id = "485ActivitiesPermittedOther", @class = "fill", @style = "height: 90px; font-size: 16px;" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th>Mental Status (Locator #19)</th>
        </tr><tr>
            <td>
                <input type="hidden" name="485MentalStatus" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='485MentalStatus1' class='radio float_left' name='485MentalStatus' value='1' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("1") ? "checked='checked'" : "" ) %>
                                <label for="485MentalStatus1" class="radio">Oriented</label>
                            </td><td>
                                <%= string.Format("<input id='485MentalStatus2' class='radio float_left' name='485MentalStatus' value='2' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("2") ? "checked='checked'" : "" ) %>
                                <label for="485MentalStatus2" class="radio">Comatose</label>
                            </td><td>
                                <%= string.Format("<input id='485MentalStatus3' class='radio float_left' name='485MentalStatus' value='3' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("3") ? "checked='checked'" : "" ) %>
                                <label for="485MentalStatus3" class="radio">Forgetful</label>
                            </td><td>
                                <%= string.Format("<input id='485MentalStatus4' class='radio float_left' name='485MentalStatus' value='4' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("4") ? "checked='checked'" : "" ) %>
                                <label for="485MentalStatus4" class="radio">Depressed</label>
                            </td><td>
                                <%= string.Format("<input id='485MentalStatus5' class='radio float_left' name='485MentalStatus' value='5' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("5") ? "checked='checked'" : "" ) %>
                                <label for="485MentalStatus5" class="radio">Disoriented</label>
                            </td><td>
                                <%= string.Format("<input id='485MentalStatus6' class='radio float_left' name='485MentalStatus' value='6' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("6") ? "checked='checked'" : "" ) %>
                                <label for="485MentalStatus6" class="radio">Lethargic</label>
                            </td><td>
                                <%= string.Format("<input id='485MentalStatus7' class='radio float_left' name='485MentalStatus' value='7' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("7") ? "checked='checked'" : "" ) %>
                                <label for="485MentalStatus7" class="radio">Agitated</label>
                            </td>
                        </tr><tr>
                            <td colspan="7">
                                <%= string.Format("<input id='485MentalStatus8' class='radio float_left' name='485MentalStatus' value='8' type='checkbox' {0} />", mentalStatus != null && mentalStatus.Contains("8") ? "checked='checked'" : "" ) %>
                                <label for="485MentalStatus8" class="radio">Other:</label>
                                <div class="clear"></div>
                                <%= Html.TextArea("485MentalStatusOther", data.ContainsKey("485MentalStatusOther") ? data["485MentalStatusOther"].Answer : "", new { @id = "485MentalStatusOther", @class = "fill", @style = "height: 90px; font-size: 16px;" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th>Prognosis (Locator #20)</th>
        </tr><tr>
            <td>
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= Html.RadioButton("485Prognosis", "Guarded", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Guarded" ? true : false, new { @id = "485PrognosisGuarded", @class = "radio float_left" } )%>
                                <label for="485PrognosisGuarded" class="radio">Guarded</label>
                            </td><td>
                                <%= Html.RadioButton("485Prognosis", "Poor", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Poor" ? true : false, new { @id = "485PrognosisPoor", @class = "radio float_left" })%>
                                <label for="485PrognosisPoor" class="radio">Poor</label>
                            </td><td>
                                <%= Html.RadioButton("485Prognosis", "Fair", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Fair" ? true : false, new { @id = "485PrognosisFair", @class = "radio float_left" })%>
                                <label for="485PrognosisFair" class="radio">Fair</label>
                            </td><td>
                                <%= Html.RadioButton("485Prognosis", "Good", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Good" ? true : false, new { @id = "485PrognosisGood", @class = "radio float_left" })%>
                                <label for="485PrognosisGood" class="radio">Good</label>
                            </td><td>
                                <%= Html.RadioButton("485Prognosis", "Excellent", data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Excellent" ? true : false, new { @id = "485PrognosisExcellent", @class = "radio float_left" })%>
                                <label for="485PrognosisExcellent" class="radio">Excellent</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th>Orders for Discipline and Treatments (Specify Amount/Frequency/Duration) (#21)</th>
        </tr><tr>
            <td>
                <%= Html.Templates("485InterventionsTemplates", new { @class = "Templates float_left", @template = "#Edit_485_Interventions" })%>
                <div class="clear"></div>
                <%= Html.TextArea("485Interventions", data.ContainsKey("485Interventions") && data["485Interventions"].Answer.IsNotNullOrEmpty() ? data["485Interventions"].Answer : "", new { @id = "Edit_485_Interventions", @class = "fill", @style = "height: 180px;font-size: 16px;" })%>
            </td>
        </tr><tr>
            <th>Goals/Rehabilitation Potential/Discharge Plans (#22)</th>
        </tr><tr>
            <td>
                <%= Html.Templates("485GoalsTemplates", new { @class = "Templates float_left", @template = "#Edit_485_Goals" })%>
                <div class="clear"></div>
                <%= Html.TextArea("485Goals", data.ContainsKey("485Goals") && data["485Goals"].Answer.IsNotNullOrEmpty() ? data["485Goals"].Answer : "", new { @id = "Edit_485_Goals", @class = "fill", @style = "height: 180px;font-size: 16px;" })%>
            </td>
        <tr>
            <th>Electronic Signature</th>
        </tr><tr>
            <td>
                <% Model.SignatureDate = DateTime.Today; %>
                <div class="third">
                    <label for="Edit_485_ClinicianSignature" class="float_left">Staff Signature:</label>
                    <div class="float_right"><%= Html.Password("SignatureText","", new { @id = "Edit_485_ClinicianSignature" })%></div>
                </div><div class="third"></div><div class="third">
                    <label for="Edit_485_SignatureDate" class="float_left">Date:</label>
                    <div class="float_right"><input type="date" name="SignatureDate" value="<%= DateTime.Today.ToShortDateString() %>" id="Edit_485_SignatureDate" /></div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<script type="text/javascript">
    $("#window_editplanofcare .oasis_diagnoses").DiagnosesList({
    <%  if (data != null && data.ContainsKey("M1020PrimaryDiagnosis") && data["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty()) { %>
            _M1020PrimaryDiagnosis: "<%= data["M1020PrimaryDiagnosis"].Answer.EscapeApostrophe() %>",
            _M1020ICD9M: "<%= data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer.EscapeApostrophe() : "" %>",
            _485ExacerbationOrOnsetPrimaryDiagnosis: "<%= data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis") ? data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer.EscapeApostrophe() : "" %>",
            _M1020PrimaryDiagnosisDate: "<%= data.ContainsKey("M1020PrimaryDiagnosisDate") ? data["M1020PrimaryDiagnosisDate"].Answer.EscapeApostrophe() : "" %>",
    <%  }
        for (int i = 1; i < 26; i++) {
            if (data != null && data.ContainsKey("M1022PrimaryDiagnosis" + i) && data["M1022PrimaryDiagnosis" + i].Answer.IsNotNullOrEmpty()) { %>
            _M1022PrimaryDiagnosis<%= i %>: "<%= data["M1022PrimaryDiagnosis" + i].Answer.EscapeApostrophe() %>",
            _M1022ICD9M<%= i %>: "<%= data.ContainsKey("M1022ICD9M" + i) ? data["M1022ICD9M" + i].Answer.EscapeApostrophe() : "" %>",
            _485ExacerbationOrOnsetPrimaryDiagnosis<%= i %>: "<%= data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis" + i) ? data["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer.EscapeApostrophe() : "" %>",
            _M1022PrimaryDiagnosis<%= i %>Date: "<%= data.ContainsKey("M1022PrimaryDiagnosis" + i + "Date") ? data["M1022PrimaryDiagnosis" + i + "Date"].Answer.EscapeApostrophe() : "" %>",
    <%      }
        } %>
            Assessment: "Edit485"
    })
    Template.OnChangeInit();
</script>