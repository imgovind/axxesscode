﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("SubmitOnly", "Oasis", FormMethod.Post, new { @id = "NonOasisSignatureForm" })) { %>
    <%= Html.Hidden(Model.TypeName + "_PatientId", Model.PatientId) %>
    <%= Html.Hidden(Model.TypeName + "_EpisodeId", Model.EpisodeId) %>
    <%= Html.Hidden(Model.TypeName + "_Id", Model.Id) %>
    <%= Html.Hidden("OasisValidationType", Model.TypeName)%>
    <%= Html.Hidden("oasisPageName", Model.TypeName)%>
    <div class="wrapper main">
        <fieldset>
            <legend>Signature</legend>
            <div class="column">
                <% if (Model.TypeName != "NonOasisDischarge") { %>
                <div class="row">
                    <label for="<%= Model.TypeName %>_TimeIn" class="float_left">Time In</label>
                    <div class="float_right">
                        <input type="time" name="<%= Model.TypeName %>_TimeIn" id="<%= Model.TypeName %>_TimeIn" value="<%= Model.TimeIn.IsNotNullOrEmpty() ? Model.TimeIn : string.Empty %>" />
                    </div>
                </div>
                <% } %>
                <div class="row">
                    <label for="<%= Model.TypeName %>_ValidationClinician" class="float_left">Signature</label>
                    <div class="float_right">
                        <%= Html.Password(Model.TypeName + "_ValidationClinician", Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText : "", new { @id = Model.TypeName + "_ValidationClinician", @class = "required" })%>
                    </div>
                </div>
            </div><div class="column">
                <% if (Model.TypeName != "NonOasisDischarge") { %>
                <div class="row">
                    <label for="<%= Model.TypeName %>_TimeIn" class="float_left">Time Out</label>
                    <div class="float_right">
                        <input type="time" name="<%= Model.TypeName %>_TimeOut" id="<%= Model.TypeName %>_TimeOut" value="<%= Model.TimeOut.IsNotNullOrEmpty() ? Model.TimeOut : string.Empty %>" />
                    </div>
                </div>
                <% } %>
                <div class="row">
                    <label for="<%= Model.TypeName %>_SignatureDate" class="float_left">Signature Date</label>
                    <div class="float_right">
                        <input type="date" name="<%= Model.TypeName %>_ValidationSignatureDate" id="<%= Model.TypeName %>_ValidationSignatureDate" value="<%= Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString() : string.Empty %>" class="required" />
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Complete</a></li>
                <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
            </ul>
        </div>
    </div>
<% } %>