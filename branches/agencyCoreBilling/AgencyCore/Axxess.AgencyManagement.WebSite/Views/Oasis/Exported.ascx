﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "oasisExported",
        "List of Exported OASIS Assessments",
        Current.AgencyName)%>
<% var visible = Current.HasRight(Permissions.ReopenDocuments); %>
<% using (Html.BeginForm("ExportedOasis", "Export", FormMethod.Post)) { %>
<div class="wrapper grid-bg">
    <table><tr><td><label class="float_left">Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "ExportedOasis_BranchCode" })%></div></td><td><label class="float_left">Status:</label><div class="float_left"><select id="ExportedOasis_Status" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></td><td><div class="row"><label  class="float_left">Exported Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="ExportedOasis_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="ExportedOasis_EndDate" class="shortdate" /></div></div></td> <td><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Agency.RebindExportedOasis();">Generate</a></li></ul></div></td><td><div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportedOasis", "Export", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = "ExportedOasis_ExportLink" })%></li></ul></div></td></tr></table>   
    <%  Html.Telerik().Grid<AssessmentExport>().Name("exportedOasisGrid").HtmlAttributes(new { @style = "top:30px;" }).Columns(columns =>
        {
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
            columns.Bound(o => o.AssessmentName).Title("Assessment").Sortable(true);
            columns.Bound(o => o.AssessmentDate).Format("{0:MM/dd/yyyy}").Width(120).Title("Assessment Date").Sortable(true);
            columns.Bound(o => o.EpisodeRange).Format("{0:MM/dd/yyyy}").Width(150).Title("Episode").Sortable(true);
            columns.Bound(o => o.ExportedDate).Format("{0:MM/dd/yyyy}").Width(120).Title("Exported Date").Sortable(true);
            columns.Bound(o => o.Insurance).Sortable(true);
            columns.Bound(o => o.AssessmentId).Title("Cancel").Width(110).Template(o =>{%><%= string.Format("<a href=\"javascript:void(0)\" onclick=\"Oasis.LoadCancel('{0}','{1}');\" >Generate Cancel</a>", o.AssessmentId,  o.AssessmentType)%><%}).ClientTemplate("<a href=\"javascript:void(0)\" onclick=\"Oasis.LoadCancel('<#= AssessmentId#>','<#= AssessmentType#>');\" >Generate Cancel</a>").Sortable(false).Visible(visible).Sortable(false);
            columns.Bound(o => o.AssessmentId).Title("Action").Width(60).Template(o =>{%><%= string.Format("<a href=\"javascript:void(0)\" onclick=\"Oasis.Reopen('{0}','{1}','{2}','{3}','{4}');\" >Reopen</a>", o.AssessmentId, o.PatientId, o.EpisodeId, o.AssessmentType, "ReOpen")%><%}).ClientTemplate("<a href=\"javascript:void(0)\" onclick=\"Oasis.Reopen('<#= AssessmentId#>','<#= PatientId#>','<#= EpisodeId#>','<#= AssessmentType#>','ReOpen');\" >Reopen</a>").Sortable(false).Visible(visible);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("Exported", "Oasis", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Scrollable().Sortable().Footer(false).Render(); %>
</div>
<script type="text/javascript">$("#window_oasisExported #exportedOasisGrid .t-grid-content").css({"height": "auto","top": "26px"});</script>
<% } %>

