﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","oasisExport","OASIS Export",Current.AgencyName)%>
<%  using (Html.BeginForm("Generate", "Oasis", FormMethod.Post, new { @id = "generateOasis" })) { %>
    <div class="wraper grid-bg" >
       <table>
           <tr><td><label class="float_left">Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "OasisExport_BranchCode" })%></div></td>
           <td><label class="float_left">Payment Source:</label><div class="float_left">
                <select id="OasisExport_PaymentSources" name="paymentSources" multiple="multiple">
                    <option value="0">None; no charge for current services</option>
                    <option value="1" selected="selected">Medicare (traditional fee-for-service)</option>
                    <option value="2">Medicare (HMO/ managed care)</option>
                    <option value="3">Medicaid (traditional fee-for-service)</option>
                    <option value="4">Medicaid (HMO/ managed care)</option>
                    <option value="5">Workers' compensation</option>
                    <option value="6">Title programs (e.g., Titile III,V, or XX)</option>
                    <option value="7">Other government (e.g.,CHAMPUS,VA,etc)</option>
                    <option value="8">Private insurance</option>
                    <option value="9">Private HMO/ managed care</option>
                    <option value="10">Self-pay</option>
                    <option value="11">Unknown</option>
                    <option value="12">Other</option>
                </select>
           </div></td>
           <td><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Agency.RebindOasisToExport();">Generate</a></li></ul></div></td><td><div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "OasisExport", "Export", new { BranchId = ViewData["BranchId"], paymentSources = "1" }, new { id = "OasisExport_ExportLink" })%></li></ul></div></td></tr>
           <tr><td colspan="3"> <div class="float_left"><input id="selectAllOasisExport" type="checkbox" />&#160;<label for="selectAllOasisExport">Check/Uncheck All</label></div></td> </tr>
       </table>   
        <%  Html.Telerik().Grid<AssessmentExport>().Name("generateOasisGrid").HtmlAttributes(new { @style = "top:58px;bottom:40px;" }).Columns(columns =>
            {
                columns.Bound(o => o.AssessmentId).Template(t =>
                { %><%= string.Format("<input name=\"OasisSelected\" type=\"checkbox\" value=\"{0}\" />", t.Identifier)%><% }).ClientTemplate("<input name='OasisSelected' type='checkbox' value='<#= Identifier #>'/>").Title("").Width(50).HtmlAttributes(new { style = "text-align:center" }).Sortable(false);
                columns.Bound(o => o.PatientName).Title("Patient Name");
                columns.Bound(o => o.AssessmentName).Title("Assessment Type").Sortable(true);
                columns.Bound(o => o.AssessmentDateFormatted).Title("Assessment Date").Width(120).Sortable(true);
                columns.Bound(o => o.EpisodeRange).Width(150).Title("Episode").Sortable(true);
                columns.Bound(o => o.Insurance).Sortable(true);
                columns.Bound(o => o.CorrectionNumberFormat).Title("Correction #").Width(100);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("Export", "Oasis", new { BranchId = ViewData["BranchId"], paymentSources = "1" })).Footer(false).Scrollable().Sortable().Render(); %>
        <div id="OasisExportBottomPanel" style="position:absolute;right:0;bottom:0;left:0"><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="GenerateSubmit($(this));">Generate OASIS File</a></li><li><a href="javascript:void(0);" onclick="MarkAsSubmited();">Mark Selected As Exported</a></li></ul></div></div>
    </div>
<% } %>
<script type="text/javascript">
    $("#OasisExport_PaymentSources").multiSelect({
        selectAll: false,
        noneSelected: '- Select Payment Source(s) -',
        oneOrMoreSelected: '*'
    });
    $("#window_oasisExport .t-group-indicator").hide();
    $("#window_oasisExport .t-grouping-header").remove();
    $("#window_oasisExport .t-grid-content").css({ "height": "auto", "position": "absolute",  "top": "25px"});
    function formatJSONDate(jsonDate) { var date = new Date(parseInt(jsonDate.substr(6))); return date; }
    function GenerateSubmit(control) { if ($("input[name=OasisSelected]:checked").length > 0) $(control).closest('form').submit(); else U.growl("Select at least one OASIS assessment to generate.", "error"); }
    function MarkAsSubmited() {
        if ($("input[name=OasisSelected]:checked").length > 0) {
            Acore.Modal({
                Name: "OASIS Mark as Exported",
                Content: $("<div/>", { "id": "oasisMarkAsExportDialog", "class": "wrapper main" }).append(
                    $("<fieldset/>").append(
                        $("<div/>").html("Please verify that your OASIS submission was accepted by CMS (Center for Medicare &#38; Medicaid Services) before you complete this step. If this OASIS file was not accepted, do not mark as exported.")).append(
                        $("<div/>").addClass("strong align_center").html("Are you sure that this OASIS file has been accepted by CMS?"))).append(
                    $("<div/>").Buttons([
                        { Text: "Yes, Mark as exported", Click: function() { Oasis.MarkAsExported("#generateOasis"); $(this).closest(".window").Close() } },
                        { Text: "No, Cancel", Click: function() { $(this).closest(".window").Close() } }
                    ])),
                Width: "700px",
                Height: "150px",
                WindowFrame: false
            })
        } else U.growl("Select at least one OASIS assessment to mark as exported.", "error");
    }
    $("#selectAllOasisExport").change(function() { $("#generateOasisGrid input[name='OasisSelected']").each(function() { $(this).prop("checked", $("#selectAllOasisExport").prop("checked")); }) });
</script>