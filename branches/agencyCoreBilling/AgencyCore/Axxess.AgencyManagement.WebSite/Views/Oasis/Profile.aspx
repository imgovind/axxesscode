﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Assessment>" %><%
var data = Model.ToDictionary();
var agency = Model.AgencyData != null ? Model.AgencyData.ToObject<Agency>() : new Agency(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><%= agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + " | " : "" %>OASIS Profile<%= data != null && data.ContainsKey("M0040LastName") && data["M0040LastName"].Answer.IsNotNullOrEmpty() ? " | " + data["M0040LastName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040Suffix") && data["M0040Suffix"].Answer.IsNotNullOrEmpty() ? " " + data["M0040Suffix"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040FirstName") && data["M0040FirstName"].Answer.IsNotNullOrEmpty() ? ", " + data["M0040FirstName"].Answer : string.Empty %><%= data != null && data.ContainsKey("M0040MI") && data["M0040MI"].Answer.IsNotNullOrEmpty() ? " " + data["M0040MI"].Answer : string.Empty %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Oasis/profile.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.6.2.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        PdfPrint.Fields = {
            "agency": "<%= (agency != null ? (agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + "<br />" : "") + (agency.MainLocation != null ? (agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? agency.MainLocation.AddressLine1.ToTitleCase() : "") + (agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? agency.MainLocation.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? agency.MainLocation.AddressCity.ToTitleCase() + ", " : "") + (agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? agency.MainLocation.AddressStateCode.ToString().ToUpper() + "  " : "") + (agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? agency.MainLocation.AddressZipCode.Clean() : "") : "") : "").Clean() %>"
        };
        PdfPrint.BuildSections(<%= Model.PrintViewJson %>); <%
    }).Render(); %>
</body>
</html>
