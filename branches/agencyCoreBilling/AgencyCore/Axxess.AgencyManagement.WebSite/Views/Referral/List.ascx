﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "listreferrals",
        "Existing Referral List",
        Current.AgencyName)%>
<%  using (Html.BeginForm("Referrals", "Export", FormMethod.Post)) {
        var action = "";
        var visible = false;
        if (Current.HasRight(Permissions.ManageReferrals)) {
            visible = true;
            action = "<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditReferral('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Referral.Delete('<#=Id#>');\" class=\"deleteReferral\">Delete</a> | <a href=\"javascript:void(0);\" onclick=\"Referral.Admit('<#=Id#>');\">Admit</a> | <a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowNonAdmitReferralModal('<#=Id#>');\" class=\"\">Non-Admit</a>";
        } %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<ReferralData>()
        .Name("List_Referral")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(r => r.ReferralDate).Format("{0:MM/dd/yyyy}").Title("Referral Date").Width(95);
            columns.Bound(r => r.DisplayName).Title("Name");
            columns.Bound(r => r.AdmissionSource).Title("Referral Source");
            columns.Bound(r => r.DateOfBirth).Title("Date of Birth").Width(95);
            columns.Bound(r => r.Gender).Title("Gender").Width(75);
            columns.Bound(r => r.Status).Title("Status").Width(75).Sortable(true);
            columns.Bound(r => r.CreatedBy).Title("Created By").Width(175).Sortable(true);
            columns.Bound(r => r.Id).ClientTemplate(action).Title("Action").Width(200).Sortable(false).Visible(visible);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("Grid", "Referral"))
        .ClientEvents(events => events.OnDataBound("Referral.BindGridButton"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_Referral .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ManageReferrals)) { %>.append(
        $("<div/>").addClass("float_left").Buttons([ { Text: "New Referral", Click: UserInterface.ShowNewReferral } ])
    )<% } 
        if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
        $("<div/>").addClass("float_right").Buttons([ { Text: "Excel Export", Click: function() { $(this).closest('form').submit() } } ])
    )<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>