﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Referral>" %>
<% using (Html.BeginForm("AddNonAdmit", "Referral", FormMethod.Post, new { @id = "newNonAdmitReferralForm" }))   { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "NonAdmit_Referral_Id" })%>
<%= Html.Hidden("IsAdmit", "false", new { @id = "NonAdmit_Referral_IsAdmit" })%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Non-Admission Details</legend>
        <div><span class="bigtext align_center"><%= Model.DisplayName %></span></div>
        <div class="column">
            <div class="row"><label for="NonAdmit_Referral_Date" class="float_left">Date:</label><div class="float_right"><input type="date" name="NonAdmitDate" id="NonAdmit_Referral_Date" class="required" /></div></div>
        </div>
        <table class="form"><tbody>
            <tr><td colspan="4"><label for="Comment"><strong>Reason Not Admitted:</strong></label></td></tr>
            <tr>
                <td><input id="NonAdmit_Referral_ReasonInAppropriate" type="checkbox" value="Inappropriate For Home Care" name="Reason" class="required radio float_left" /><label for="NonAdmit_Referral_ReasonInAppropriate" class="radio">Inappropriate For Home Care</label></td>
                <td><input id="NonAdmit_Referral_ReasonRefused" type="checkbox" value="Referral Refused Service" name="Reason" class="required radio float_left" /><label for="NonAdmit_Referral_ReasonRefused" class="radio">Referral Refused Service</label></td>
                <td><input id="NonAdmit_Referral_ReasonOutofService" type="checkbox" value="Out of Service Area" name="Reason" class="required radio float_left" /><label for="NonAdmit_Referral_ReasonOutofService" class="radio">Out of Service Area</label></td>
                <td><input id="NonAdmit_Referral_ReasonOnService" type="checkbox" value="On Service with another agency" name="Reason" class="required radio float_left" /><label for="NonAdmit_Referral_ReasonOnService" class="radio">On Service with another agency</label></td>
            </tr><tr>
                <td><input id="NonAdmit_Referral_ReasonNotaProvider" type="checkbox" value="Not a Provider" name="Reason" class="required radio float_left" /><label for="NonAdmit_Referral_ReasonNotaProvider" class="radio">Not a Provider</label></td>
                <td><input id="NonAdmit_Referral_ReasonNotHomebound" type="checkbox" value="Not Homebound" name="Reason" class="required radio float_left" /><label for="NonAdmit_Referral_ReasonNotHomebound" class="radio">Not Homebound</label></td>
                <td><input id="NonAdmit_Referral_ReasonRedirected" type="checkbox" value="Redirected to alternate care facility" name="Reason" class="required radio float_left" /><label for="NonAdmit_Referral_ReasonRedirected" class="radio">Redirected to alternate care facility</label></td>
                <td><input id="NonAdmit_Referral_ReasonOther" type="checkbox" value="Other" name="Reason" class="required radio float_left" /><label for="NonAdmit_Referral_ReasonOther" class="radio">Other (specify in Comments)</label></td>
            </tr>
        </tbody></table>
        <table class="form"><tbody>           
            <tr class="linesep vert">
               <td><label for="Comment"><strong>Comments:</strong></label>
                 <div ><%= Html.TextArea("Comments", "", new { @id = "NonAdmit_Referral_Comments" })%></div></td>
            </tr>
        </tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>
<%} %>

