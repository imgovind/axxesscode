﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="mailcontrols">
    <span class="float_left"><a href='javascript:void(0);' onclick="UserInterface.ShowMessages();">Inbox</a> &#8211; <a href='javascript:void(0);' onclick="UserInterface.ShowNewMessage();">Compose Mail</a></span>
    <select class="float_right">
        <option>Actions...</option>
        <option>Delete</option>
    </select>
    <div class="clear"></div>
</div>
<div style="position: relative; height: 181px;" id="messagesWidgetContent"></div>
<div onclick="UserInterface.ShowMessages();" id="messageWidgetMore" class="widget-more"><a href="javascript:void(0);">More &#187;</a></div>

