﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<MissedVisit>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model != null &&  Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : string.Empty %>Missed Visit<%= Model != null && Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : string.Empty %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("pdfprint.css").Add("Print/Schedule/Missed/visit.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<body>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.6.2.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "pdfprint.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => {  %>
        PdfPrint.Fields = {
            "agency": "<%= (Model != null && Model.Agency != null ? (Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + "<br />" : "") + (location != null ? (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") + (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "<br />" : "<br />") + (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : "") : "") : "").Clean() %>",
            "patientname": "<%= (Model != null && Model.Patient != null ? (Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.ToLower().ToTitleCase() + ", " : "") + (Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.ToLower().ToTitleCase() + " " : "") + (Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? Model.Patient.MiddleInitial.ToUpper() + "<br />" : "<br />") : "").Clean() %>",
            "visittype": "<%= Model != null && Model.VisitType.IsNotNullOrEmpty() ? Model.VisitType.Clean() : string.Empty %>",
            "visitdate": "<%= Model != null && Model.Date.IsValid() ? Model.Date.ToShortDateString().Clean() : string.Empty %>",
            "ordergen": "<%= Model != null && Model.Patient != null ? (Model.IsOrderGenerated ? "Yes" : "No") : string.Empty %>",
            "physnotice": "<%= Model != null && Model.Patient != null ? (Model.IsPhysicianOfficeNotified ? "Yes" : "No") : string.Empty %>",
            "reason": "<%= Model != null && Model.Reason.IsNotNullOrEmpty() ? Model.Reason.Clean() : string.Empty %>",
            "sign": "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : string.Empty %>",
            "signdate": "<%= Model != null && Model.SignatureDate.IsValid() ? Model.SignatureDate.ToShortDateString().Clean() : string.Empty %>"
        };
        PdfPrint.BuildBasic("<%= Model != null && Model.Comments.IsNotNullOrEmpty() ? Model.Comments.Clean() : string.Empty %>"); <%
    }).Render(); %>
</body>
</html>