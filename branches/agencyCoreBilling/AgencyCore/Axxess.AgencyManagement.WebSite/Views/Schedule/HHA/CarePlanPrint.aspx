﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>Home Health Aide Care Plan<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body><%
string[] diet = data != null && data.ContainsKey("IsDiet") && data["IsDiet"].Answer != "" ? data["IsDiet"].Answer.Split(',') : null;
string[] allergies = data != null && data.ContainsKey("Allergies") && data["Allergies"].Answer != "" ? data["Allergies"].Answer.Split(',') : null;
string[] isVitalSignParameter = data != null && data.ContainsKey("IsVitalSignParameter") && data["IsVitalSignParameter"].Answer != "" ? data["IsVitalSignParameter"].Answer.Split(',') : null;
string[] safetyMeasure = data != null && data.ContainsKey("SafetyMeasures") && data["SafetyMeasures"].Answer != "" ? data["SafetyMeasures"].Answer.Split(',') : null;
string[] functionLimitations = data != null && data.ContainsKey("FunctionLimitations") && data["FunctionLimitations"].Answer != "" ? data["FunctionLimitations"].Answer.Split(',') : null;
string[] activitiesPermitted = data != null && data.ContainsKey("ActivitiesPermitted") && data["ActivitiesPermitted"].Answer != "" ? data["ActivitiesPermitted"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.6.2.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => {  %>
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3EHome Health Aide%3Cbr /%3ECare Plan%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EHHA Frequency:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("HHAFrequency") && data["HHAFrequency"].Answer.IsNotNullOrEmpty() ? data["HHAFrequency"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EEpisode Period:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && Model.StartDate.IsValid() && Model.EndDate.IsValid()? string.Format(" {0} &#8211; {1}", Model.StartDate.ToShortDateString(), Model.EndDate.ToShortDateString()) : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EMR#%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= Model.Patient != null ? Model.Patient.PatientIdNumber.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EDNR:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? "Yes" : "" %><%= data != null && data.ContainsKey("DNR") && data["DNR"].Answer == "0" ? "No" : "" %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EPrimary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3ESecondary Diagnosis:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3EHome Health Aide%3Cbr /%3ECare Plan%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "";
        printview.addsection(
            printview.col(2,
                printview.checkbox("Diet:",<%= diet != null && diet.Contains("1") ? "true" : "false" %>) +
                printview.checkbox("Allergies:",<%= allergies != null && allergies.Contains("Yes") ? "true" : "false" %>) +
                printview.span("<%= data != null && data.ContainsKey("Diet") ? data["Diet"].Answer.Clean() : string.Empty %>",false,1) +
                printview.span("<%= data != null && data.ContainsKey("AllergiesDescription") ? data["AllergiesDescription"].Answer.Clean() : string.Empty %>",false,1)));
        printview.addsection(<%
            if (isVitalSignParameter != null && isVitalSignParameter.Contains("1")) { %>
                printview.checkbox("N/A",true),<%
            } else { %>
                "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth%3E%3C/th%3E%3Cth%3ESBP%3C/th%3E%3Cth%3EDBP%3C/th%3E%3Cth%3EHR%3C/th%3E%3Cth%3EResp%3C/th%3E%3Cth%3ETemp%3C/th%3E%3Cth%3EWeight%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth%3Egreater than (&#62;)%3C/th%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("SystolicBPGreaterThan") ? data["SystolicBPGreaterThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("DiastolicBPGreaterThan") ? data["DiastolicBPGreaterThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PulseGreaterThan") ? data["PulseGreaterThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("RespirationGreaterThan") ? data["RespirationGreaterThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("TempGreaterThan") ? data["TempGreaterThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("WeightGreaterThan") ? data["WeightGreaterThan"].Answer.Clean() : string.Empty %>" + 
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth%3Eor less than (&#60;)%3C/th%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("SystolicBPLessThan") ? data["SystolicBPLessThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("DiastolicBPLessThan") ? data["DiastolicBPLessThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PulseLessThan") ? data["PulseLessThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("RespirationLessThan") ? data["RespirationLessThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("TempLessThan") ? data["TempLessThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("WeightLessThan") ? data["WeightLessThan"].Answer.Clean() : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E",<%
            } %>
            "Vital Sign Ranges");
        printview.addsection(
            printview.col(3,
                printview.checkbox("Anticoagulant Precautions",<%= safetyMeasure != null && safetyMeasure.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Neutropenic Precautions",<%= safetyMeasure != null && safetyMeasure.Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Sharps Safety",<%= safetyMeasure != null && safetyMeasure.Contains("11") ? "true" : "false"%>) +
                printview.checkbox("Emergency Plan Developed",<%= safetyMeasure != null && safetyMeasure.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("O<sub>2</sub> Precautions",<%= safetyMeasure != null && safetyMeasure.Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Slow Position Change",<%= safetyMeasure != null && safetyMeasure.Contains("12") ? "true" : "false"%>) +
                printview.checkbox("Fall Precautions",<%= safetyMeasure != null && safetyMeasure.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Proper Position During Meals",<%= safetyMeasure != null && safetyMeasure.Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Safety in ADLs",<%= safetyMeasure != null && safetyMeasure.Contains("13") ? "true" : "false"%>) +
                printview.checkbox("Keep Pathway Clear",<%= safetyMeasure != null && safetyMeasure.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Safety in ADLs",<%= safetyMeasure != null && safetyMeasure.Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Support During Transfer &#38; Ambulation",<%= safetyMeasure != null && safetyMeasure.Contains("14") ? "true" : "false"%>) +
                printview.checkbox("Keep Side Rails Up",<%= safetyMeasure != null && safetyMeasure.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Seizure Precautions",<%= safetyMeasure != null && safetyMeasure.Contains("10") ? "true" : "false"%>) +
                printview.checkbox("Use of Assistive Devices",<%= safetyMeasure != null && safetyMeasure.Contains("15") ? "true" : "false"%>)) +
            printview.span("%3Cstrong%3EOther%3C/strong%3E: <%= data != null && data.ContainsKey("OtherSafetyMeasures") ? data["OtherSafetyMeasures"].Answer.Clean() : string.Empty %>",false,2),
            "Safety Precautions");
        printview.addsection(
            printview.col(3,
                printview.checkbox("Amputation",<%= functionLimitations != null && functionLimitations.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Hearing",<%= functionLimitations != null && functionLimitations.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Ambulation",<%= functionLimitations != null && functionLimitations.Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Bowel/Bladder Incontinence",<%= functionLimitations != null && functionLimitations.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Paralysis",<%= functionLimitations != null && functionLimitations.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Speech",<%= functionLimitations != null && functionLimitations.Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Contracture",<%= functionLimitations != null && functionLimitations.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Endurance",<%= functionLimitations != null && functionLimitations.Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Legally Blind",<%= functionLimitations != null && functionLimitations.Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Dyspnea with Minimal Exertion",<%= functionLimitations != null && functionLimitations.Contains("A") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= functionLimitations != null && functionLimitations.Contains("B") ? "true" : "false"%>) +
                printview.span("<%= data != null && data.ContainsKey("FunctionLimitationsOther") ? data["FunctionLimitationsOther"].Answer.Clean() : string.Empty %>",0,1) ),
            "Functional Limitations");
        printview.addsection(
            printview.col(3,
                printview.checkbox("Complete bed rest",<%= activitiesPermitted != null && activitiesPermitted.Contains("1") ? "true" : "false"%>) +
                printview.checkbox("Exercise prescribed",<%= activitiesPermitted != null && activitiesPermitted.Contains("5") ? "true" : "false"%>) +
                printview.checkbox("Cane",<%= activitiesPermitted != null && activitiesPermitted.Contains("9") ? "true" : "false"%>) +
                printview.checkbox("Bed rest with BRP",<%= activitiesPermitted != null && activitiesPermitted.Contains("2") ? "true" : "false"%>) +
                printview.checkbox("Partial weight bearing",<%= activitiesPermitted != null && activitiesPermitted.Contains("6") ? "true" : "false"%>) +
                printview.checkbox("Wheelchair",<%= activitiesPermitted != null && activitiesPermitted.Contains("10") ? "true" : "false"%>) +
                printview.checkbox("Up as tolerated",<%= activitiesPermitted != null && activitiesPermitted.Contains("3") ? "true" : "false"%>) +
                printview.checkbox("Independent at home",<%= activitiesPermitted != null && activitiesPermitted.Contains("7") ? "true" : "false"%>) +
                printview.checkbox("Walker",<%= activitiesPermitted != null && activitiesPermitted.Contains("11") ? "true" : "false"%>) +
                printview.checkbox("Transfer bed-chair",<%= activitiesPermitted != null && activitiesPermitted.Contains("4") ? "true" : "false"%>) +
                printview.checkbox("Crutches",<%= activitiesPermitted != null && activitiesPermitted.Contains("8") ? "true" : "false"%>) +
                printview.checkbox("Other",<%= activitiesPermitted != null && activitiesPermitted.Contains("12") ? "true" : "false"%>)) +
            printview.span("<%= data != null && data.ContainsKey("ActivitiesPermittedOther") ? data["ActivitiesPermittedOther"].Answer.Clean() : string.Empty %>",0,1),
            "Activities Permitted");
        printview.addsection(
            printview.span("%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Cth colspan=%222%22%3E%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Cth class=%22align_left%22%3EAssignment%3C/th%3E%3Cth colspan=%224%22%3EStatus%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align_left%22%3EVital Signs%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3ETemperature%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsTemperature") && data["VitalSignsTemperature"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsTemperature") && data["VitalSignsTemperature"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsTemperature") && data["VitalSignsTemperature"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsTemperature") && data["VitalSignsTemperature"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EBlood Presure%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsBloodPressure") && data["VitalSignsBloodPressure"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsBloodPressure") && data["VitalSignsBloodPressure"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsBloodPressure") && data["VitalSignsBloodPressure"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsBloodPressure") && data["VitalSignsBloodPressure"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EHeart Rate%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsHeartRate") && data["VitalSignsHeartRate"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsHeartRate") && data["VitalSignsHeartRate"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsHeartRate") && data["VitalSignsHeartRate"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsHeartRate") && data["VitalSignsHeartRate"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3ERespirations%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsRespirations") && data["VitalSignsRespirations"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsRespirations") && data["VitalSignsRespirations"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsRespirations") && data["VitalSignsRespirations"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsRespirations") && data["VitalSignsRespirations"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EWeight%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsWeight") && data["VitalSignsWeight"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsWeight") && data["VitalSignsWeight"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsWeight") && data["VitalSignsWeight"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("VitalSignsWeight") && data["VitalSignsWeight"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align_left%22%3EPersonal Care%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EBed Bath%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EAssist with Chair Bath%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3ETub Bath%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EShower%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EShower w/Chair%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EShampoo Hair%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EHair Care/Comb Hair%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EOral Care%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3ESkin Care%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EPericare%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3ENail Care%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EShave%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EAssist with Dressing%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EMedication Reminder%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "3" ? "&#x2713;" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "2" ? "&#x2713;" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "1" ? "&#x2713;" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "0" ? "&#x2713;" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/td%3E%3Ctd%3E%3Ctable%3E%3Ctbody%3E%3Ctr%3E%3Cth class=%22align_left%22%3EAssignment%3C/th%3E%3Cth colspan=%224%22%3EStatus%3C/th%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align_left%22%3EElimination%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EAssist with Bed Pan/Urinal%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EAssist with BSC%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EIncontinence Care%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EEmpty Drainage Bag%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3ERecord Bowel Movement%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3ECatheter Care%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align_left%22%3EActivity%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EDangle on Side of Bed%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3ETurn &#38; Position%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EAssist with Transfer%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3ERange of Motion%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EAssist with Ambulation%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EEquipment Care%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "3" ? "&#x2713;" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "2" ? "&#x2713;" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "1" ? "&#x2713;" : string.Empty %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "0" ? "&#x2713;" : string.Empty %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align_left%22%3EHousehold Task%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EMake Bed%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EChange Linen%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3ELight Housekeeping%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Cth class=%22align_left%22%3ENutrition%3C/th%3E%3Cth%3EQV%3C/th%3E%3Cth%3EQW%3C/th%3E%3Cth%3EPR%3C/th%3E%3Cth%3EN/A%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EMeal Set-up%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3Ctr%3E%3Ctd class=%22align_left%22%3EAssist with Feeding%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "3" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "2" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "1" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3Ctd%3E" +
                "<%= data != null && data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "0" ? "&#x2713;" : "" %>" +
                "%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E"));
        printview.addsection(printview.span("%3Cem class=%22small float_right%22%3E* QV = Every Visit; QW = Every Week; PR = Patient Request; N/A = Not Applicable%3C/em%3E"));
        printview.addsection(printview.span("<%= data != null && data.ContainsKey("Comment") && data["Comment"].Answer.IsNotNullOrEmpty() ? data["Comment"].Answer.Clean() : string.Empty %>",false,3),"Comments / Additional Instructions")
        printview.addsection(
            printview.checkbox("Reviewed with Home Health Aide",<%= data != null && data.ContainsKey("ReviewedWithHHA") && data["ReviewedWithHHA"] != null && data["ReviewedWithHHA"].Answer.IsNotNullOrEmpty() && data["ReviewedWithHHA"].Answer.IsEqual("Yes") ? "true" : "false"%>),
                "Notifications");
        printview.addsection(
            printview.col(2,
                printview.span("Clinician Signature:",true) +
                printview.span("Date:",true) +
                printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : string.Empty %>",0,1) +
                printview.span("<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : string.Empty %>",0,1))); <%
}).Render(); %>
</html>