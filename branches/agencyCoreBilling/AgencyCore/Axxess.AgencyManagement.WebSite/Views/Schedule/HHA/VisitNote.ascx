﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "HHAideVisitForm" })) {
        var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "hhAideVisit",
        "Home Health Aide Progress Note",
        Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "")%>
<%= Html.Hidden("HHAideVisit_PatientId", Model.PatientId)%>
<%= Html.Hidden("HHAideVisit_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("HHAideVisit_EventId", Model.EventId)%>
<%= Html.Hidden("DisciplineTask", "54")%>
<%= Html.Hidden("Type", "HHAideVisit")%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Home Health Aide Progress Note</th></tr>
            <tr><td colspan="3"><span class="bigtext"><%= Model.Patient.DisplayName %></span></td><td><% if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %><div class="buttons"><ul><li><%= Model.CarePlanOrEvalUrl%></li></ul></div><% } %></td></tr>
            <tr>
                <td colspan="2">
                    <div><label for="HHAideVisit_MR" class="float_left">MR#:</label><div class="float_right"><%= Html.TextBox("HHAideVisit_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty, new { @id = "HHAideVisit_MR", @readonly = "readonly" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="HHAideVisit_EpsPeriod" class="float_left">Episode Period:</label><div class="float_right"><%= Html.TextBox("HHAideVisit_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "HHAideVisit_EpsPeriod", @readonly = "readonly" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="HHAideVisit_VisitDate" class="float_left">Visit Date:</label><div class="float_right"><input type="date" name="HHAideVisit_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="HHAideVisit_VisitDate" class="required" /></div></div>
                    <div class="clear"></div>
                    <div><label for="HHAideVisit_TimeIn" class="float_left">Time In:</label><div class="float_right"><%= Html.TextBox("HHAideVisit_TimeIn", data.ContainsKey("TimeIn") ? data["TimeIn"].Answer : string.Empty, new { @id = "HHAideVisit_TimeIn", @class = "loc" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="HHAideVisit_TimeOut" class="float_left">Time Out:</label><div class="float_right"><%= Html.TextBox("HHAideVisit_TimeOut", data.ContainsKey("TimeOut") ? data["TimeOut"].Answer : string.Empty, new { @id = "HHAideVisit_TimeOut", @class = "loc" })%></div></div>
                </td>
                <td colspan="2">
                    <% if (Current.HasRight(Permissions.ViewPreviousNotes)) { %><div><label for="HHAideVisit_PreviousNotes" class="float_left">Previous Notes:</label><div class="float_right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = "HHAideVisit_PreviousNotes" })%></div></div>
                    <div class="clear"></div><% } %>
                    <div><label for="HHAideVisit_HHAFrequency" class="float_left">HHA Frequency:</label><div class="float_right"><%= Html.TextBox("HHAideVisit_HHAFrequency", data.ContainsKey("HHAFrequency") ? data["HHAFrequency"].Answer : string.Empty, new { @id = "HHAideVisit_HHAFrequency", @readonly = "readonly" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="HHAideVisit_PrimaryDiagnosis" class="float_left">Primary Diagnosis:</label><div class="float_right"><%= Html.TextBox("HHAideVisit_PrimaryDiagnosis", data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : string.Empty, new { @id = "HHAideVisit_PrimaryDiagnosis", @readonly = "readonly" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="HHAideVisit_PrimaryDiagnosis1" class="float_left">Secondary Diagnosis:</label><div class="float_right"><%= Html.TextBox("HHAideVisit_PrimaryDiagnosis1", data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : string.Empty, new { @id = "HHAideVisit_PrimaryDiagnosis1", @readonly = "readonly" })%></div></div>
                    <div class="clear"></div>
                    <div><label for="HHAideVisit_DNR" class="float_left">DNR:</label><div class="float_right"><%= Html.RadioButton("HHAideVisit_DNR", "1", data.ContainsKey("DNR") && data["DNR"].Answer == "1" ? true : false, new { @id = "HHAideVisit_DNR1", @class = "radio" })%><label for="HHAideVisit_DNR1" class="inlineradio">Yes</label><%= Html.RadioButton("HHAideVisit_DNR", "0", data.ContainsKey("DNR") && data["DNR"].Answer == "0" ? true : false, new { @id = "HHAideVisit_DNR2", @class = "radio" })%><label for="HHAideVisit_DNR2" class="inlineradio">No</label></div></div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="hhaVisitContentId"><% Html.RenderPartial("~/Views/Schedule/HHA/VisitNoteContent.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>       
            <tr><th colspan="2">Electronic Signature</th></tr>
            <tr>
                <td colspan="2">
                    <div class="third">
                        <label for="HHAideVisit_ClinicianSignature" class="float_left">Clinician Signature:</label>
                        <div class="float_right"><%= Html.Password("HHAideVisit_Clinician", "", new { @id = "HHAideVisit_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="HHAideVisit_ClinicianSignatureDate" class="float_left">Date:</label>
                        <div class="float_right">
                        <%  var maxDate = DateTime.Now >= Model.StartDate && DateTime.Now <= Model.EndDate ? DateTime.Now : Model.EndDate;
                            var date = data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() && data["SignatureDate"].Answer.IsValidDate() && data["SignatureDate"].Answer.ToDateTime() >= Model.StartDate && data["SignatureDate"].Answer.ToDateTime() <= maxDate ? data["SignatureDate"].Answer : ""; %>
                        <input type="date" name="HHAideVisit_SignatureDate" value="<%= date %>" id="HHAideVisit_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="2">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="HHAideVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="HHAideVisitRemove(); hhaVisit.Submit($(this));">Save</a></li>
            <li><a href="javascript:void(0);" onclick="HHAideVisitAdd(); hhaVisit.Submit($(this));">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="HHAideVisitRemove(); hhaVisit.Submit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="HHAideVisitRemove(); hhaVisit.Submit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" onclick="HHAideVisitRemove(); UserInterface.CloseWindow('hhAideVisit');">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    Template.OnChangeInit();
    $("#HHAideVisit_IsVitalSignParameter").click(function() {
        if ($('#HHAideVisit_IsVitalSignParameter').is(':checked')) $("#window_hhAideVisit .vitalsigns").each(function() { $(this).hide(); });
        else $("#window_hhAideVisit .vitalsigns").each(function() { $(this).show(); });
    });

    if ($('#HHAideVisit_IsVitalSignParameter').is(':checked')) {
        $("#window_hhAideVisit .vitalsigns").each(function() { $(this).hide(); });
    } else {
        $("#window_hhAideVisit .vitalsigns").each(function() { $(this).show(); });
    }

    $("#HHAideVisit_IsVitalSigns").click(function() {
        if ($('#HHAideVisit_IsVitalSigns').is(':checked')) {
            $("#window_hhAideVisit .vitalsignparameter").each(function() { $(this).hide(); });
        } else {
            $("#window_hhAideVisit .vitalsignparameter").each(function() { $(this).show(); });
        }
    });

    if ($('#HHAideVisit_IsVitalSigns').is(':checked')) {
        $("#window_hhAideVisit .vitalsignparameter").each(function() { $(this).hide(); });
    } else {
        $("#window_hhAideVisit .vitalsignparameter").each(function() { $(this).show(); });
    }

    U.timePicker($("#HHAideVisit_TimeIn"));
    U.timePicker($("#HHAideVisit_TimeOut"));
    
    $("#HHAideVisit_MR").attr('readonly', true);
    $("#HHAideVisit_EpsPeriod").attr('readonly', true);
    
    function HHAideVisitAdd() {
        $("#HHAideVisit_TimeIn").removeClass('required').addClass('required');
        $("#HHAideVisit_TimeOut").removeClass('required').addClass('required');
        $("#HHAideVisit_Clinician").removeClass('required').addClass('required');
        $("#HHAideVisit_SignatureDate").removeClass('required').addClass('required');
    }
    
    function HHAideVisitRemove() {
        $("#HHAideVisit_TimeIn").removeClass('required');
        $("#HHAideVisit_TimeOut").removeClass('required');
        $("#HHAideVisit_Clinician").removeClass('required');
        $("#HHAideVisit_SignatureDate").removeClass('required');
    }
    
    $("#HHAideVisit_PreviousNotes").change(function() {
        $("#hhaVisitContentId").load("/Schedule/HHAVisitContent", { patientId: $("#HHAideVisit_PatientId").val(), noteId: $("#HHAideVisit_PreviousNotes").val() }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
            }
            else if (textStatus == "success") {
            }
        });
    });
</script>
