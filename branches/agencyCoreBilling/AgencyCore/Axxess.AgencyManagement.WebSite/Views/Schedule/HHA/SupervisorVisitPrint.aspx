﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : string.Empty %>HHA Supervisory Visit<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : string.Empty %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body>
<% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
        .Add("jquery-1.6.2.min.js")
        .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
        .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
    ).OnDocumentReady(() => { %>
        printview.cssclass = "largerfont";
        printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3EHHA Supervisory Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3Cbr /%3E%3Cspan class=%22quadcol%22%3E%3Cspan%3E%3Cstrong%3EHome Health Aide:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("HealthAide") && data["HealthAide"].Answer.IsNotNullOrEmpty() && data["HealthAide"].Answer.IsGuid() ? UserEngine.GetName(data["HealthAide"].Answer.ToGuid(), Current.AgencyId) : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAide Present:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("AidePresent") && data["AidePresent"].Answer.IsNotNullOrEmpty() && data["AidePresent"].Answer == "1" ? "Yes" : "No"%>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("VisitDate") && data["VisitDate"].Answer.IsNotNullOrEmpty() ? data["VisitDate"].Answer.ToDateTime().ToString("MM/dd/yyy").Clean() : string.Empty %>" +
            "%3C/span%3E%3Cspan%3E%3Cstrong%3EAssociated Milage:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
            "<%= data != null && data.ContainsKey("AssociatedMileage") && data["AssociatedMileage"].Answer.IsNotNullOrEmpty() ? data["AssociatedMileage"].Answer.Clean() : string.Empty %>" +
            "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
            '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
            "%3C/td%3E%3Cth class=%22h1%22%3EHHA Supervisory Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
            "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
            "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
        printview.footer = "";
        printview.addsection(
            "%3Col%3E%3Cli%3E" +
            printview.span("Arrives for assigned visits as scheduled", true) +
            printview.span("<%= data != null && data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "1" ? "Yes" : string.Empty %><%= data != null && data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "0" ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Follows client&#8217;s plan of care", true) +
            printview.span("<%= data != null && data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "1" ? "Yes" : string.Empty %><%= data != null && data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "0" ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Demonstrates positive and helpful attitude towards the client and others", true) +
            printview.span("<%= data != null && data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "1" ? "Yes" : string.Empty %><%= data != null && data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "0" ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Informs Nurse Supervisor of client needs and changes in condition as appropriate", true) +
            printview.span("<%= data != null && data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "1" ? "Yes" : string.Empty %><%= data != null && data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "0" ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Aide implements Universal Precautions per agency policy", true) +
            printview.span("<%= data != null && data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "1" ? "Yes" : string.Empty %><%= data != null && data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "0" ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Any changes made to client&#8217;s plan of care at this time", true) +
            printview.span("<%= data != null && data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "1" ? "Yes" : string.Empty %><%= data != null && data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "0" ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Patient/CG satisfied with care and services provided by aide", true) +
            printview.span("<%= data != null && data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "1" ? "Yes" : string.Empty %><%= data != null && data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "0" ? "No" : string.Empty %>",0,1) +
            "%3C/li%3E%3Cli%3E" +
            printview.span("Additional Comments/Findings", true) +
            printview.span("<%= data != null && data.ContainsKey("AdditionalComments") && data["AdditionalComments"].Answer.IsNotNullOrEmpty() ? data["AdditionalComments"].Answer.Clean() : string.Empty %>",0,10) +
            "%3C/li%3E%3C/ol%3E");
        printview.addsection(
            printview.col(2,
                printview.span("Signature:",true) +
                printview.span("Date:",true) +
                printview.span("<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : string.Empty %>",0,1) +
                printview.span("<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : string.Empty %>",0,1))); <%
    }).Render(); %>
</html>