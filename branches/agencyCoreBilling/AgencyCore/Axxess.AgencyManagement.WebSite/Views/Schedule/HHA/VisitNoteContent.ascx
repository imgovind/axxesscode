﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <% string[] isVitalSignParameter = data.ContainsKey("IsVitalSignParameter") && data["IsVitalSignParameter"].Answer != "" ? data["IsVitalSignParameter"].Answer.Split(',') : null; %>
            <input name="HHAideVisit_IsVitalSignParameter" value=" " type="hidden" />
            <th>Vital Sign Parameters &#8212; <%= string.Format("<input class='radio' id='HHAideVisit_IsVitalSignParameter' name='HHAideVisit_IsVitalSignParameter' value='1' type='checkbox' {0} />", isVitalSignParameter != null && isVitalSignParameter.Contains("1") ? "checked='checked'" : "")%><label for="HHAideVisit_IsVitalSignParameter">N/A</label></th>
            <% string[] isVitalSigns = data.ContainsKey("IsVitalSigns") && data["IsVitalSigns"].Answer != "" ? data["IsVitalSigns"].Answer.Split(',') : null; %>
            <input name="HHAideVisit_IsVitalSigns" value=" " type="hidden" />
            <th>Vital Signs&#8212; <%= string.Format("<input class='radio' id='HHAideVisit_IsVitalSigns' name='HHAideVisit_IsVitalSigns' value='1' type='checkbox' {0} />", isVitalSigns != null && isVitalSigns.Contains("1") ? "checked='checked'" : "")%><label for="HHAideVisit_IsVitalSigns">N/A</label></th>
        </tr><tr>
            <td>
                <table class="fixed vitalsigns">
                    <tbody>
                        <tr><th></th><th>SBP</th><th>DBP</th><th>HR</th><th>Resp</th><th>Temp</th><th>Weight</th></tr>
                        <tr>
                            <th>greater than (&#62;)</th>
                            <td><%= Html.TextBox("HHAideVisit_SystolicBPGreaterThan", data.ContainsKey("SystolicBPGreaterThan") ? data["SystolicBPGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_SystolicBPGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_DiastolicBPGreaterThan", data.ContainsKey("DiastolicBPGreaterThan") ? data["DiastolicBPGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_DiastolicBPGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_PulseGreaterThan", data.ContainsKey("PulseGreaterThan") ? data["PulseGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_PulseGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_RespirationGreaterThan", data.ContainsKey("RespirationGreaterThan") ? data["RespirationGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_RespirationGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_TempGreaterThan", data.ContainsKey("TempGreaterThan") ? data["TempGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_TempGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_WeightGreaterThan", data.ContainsKey("WeightGreaterThan") ? data["WeightGreaterThan"].Answer : string.Empty, new { @id = "HHAideVisit_WeightGreaterThan", @class = "fill" })%></td>
                        </tr><tr>
                            <th>less than (&#60;)</th>
                            <td><%= Html.TextBox("HHAideVisit_SystolicBPLessThan", data.ContainsKey("SystolicBPLessThan") ? data["SystolicBPLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_SystolicBPLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_DiastolicBPLessThan", data.ContainsKey("DiastolicBPLessThan") ? data["DiastolicBPLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_DiastolicBPLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_PulseLessThan", data.ContainsKey("PulseLessThan") ? data["PulseLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_PulseLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_RespirationLessThan", data.ContainsKey("RespirationLessThan") ? data["RespirationLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_RespirationLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_TempLessThan", data.ContainsKey("TempLessThan") ? data["TempLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_TempLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_WeightLessThan", data.ContainsKey("WeightLessThan") ? data["WeightLessThan"].Answer : string.Empty, new { @id = "HHAideVisit_WeightLessThan", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td><td>
                <table class="fixed vitalsignparameter">
                    <tbody>
                        <tr><th></th><th>BP</th><th>HR</th><th>Temp</th><th>Resp</th><th>Weight</th></tr>
                        <tr>
                            <th>Val.</th>
                            <td><%= Html.TextBox("HHAideVisit_VitalSignBPVal", data.ContainsKey("VitalSignBPVal") ? data["VitalSignBPVal"].Answer : "", new { @id = "HHAideVisit_VitalSignBPVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_VitalSignHRVal", data.ContainsKey("VitalSignHRVal") ? data["VitalSignHRVal"].Answer : "", new { @id = "HHAideVisit_VitalSignHRVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_VitalSignTempVal", data.ContainsKey("VitalSignTempVal") ? data["VitalSignTempVal"].Answer : "", new { @id = "HHAideVisit_VitalSignTempVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_VitalSignRespVal", data.ContainsKey("VitalSignRespVal") ? data["VitalSignRespVal"].Answer : "", new { @id = "HHAideVisit_VitalSignRespVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("HHAideVisit_VitalSignWeightVal", data.ContainsKey("VitalSignWeightVal") ? data["VitalSignWeightVal"].Answer : "", new { @id = "HHAideVisit_VitalSignWeightVal", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><th colspan="2">Tasks</th></tr>
        <tr>
            <td>
                <table class="fixed">
                    <thead>
                        <tr><th colspan="3" class="align_left">Assignment</th><th colspan="3">Status</th></tr>
                        <tr><th colspan="3" class="align_left">Personal Care</th><th>Completed</th><th>Refuse</th><th>N/A</th></tr>
                        <tr>
                            <td colspan="3" class="align_left">Bed Bath</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareBedBath", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareBedBath", "2", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareBedBath", "1", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareBedBath", "0", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with Chair Bath</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareAssistWithChairBath", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareAssistWithChairBath", "2", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareAssistWithChairBath", "1", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareAssistWithChairBath", "0", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Tub Bath</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareTubBath", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareTubBath", "2", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareTubBath", "1", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareTubBath", "0", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Shower</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareShower", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareShower", "2", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareShower", "1", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareShower", "0", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Shower w/Chair</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareShowerWithChair", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareShowerWithChair", "2", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareShowerWithChair", "1", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareShowerWithChair", "0", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Shampoo Hair</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareShampooHair", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareShampooHair", "2", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareShampooHair", "1", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareShampooHair", "0", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Hair Care/Comb Hair</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareHairCare", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareHairCare", "2", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareHairCare", "1", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareHairCare", "0", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Oral Care</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareOralCare", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareOralCare", "2", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareOralCare", "1", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareOralCare", "0", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Skin Care</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareSkinCare", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareSkinCare", "2", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareSkinCare", "1", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareSkinCare", "0", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Pericare</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCarePericare", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCarePericare", "2", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCarePericare", "1", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCarePericare", "0", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Nail Care</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareNailCare", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareNailCare", "2", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareNailCare", "1", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareNailCare", "0", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Shave</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareShave", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareShave", "2", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareShave", "1", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareShave", "0", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with Dressing</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareAssistWithDressing", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareAssistWithDressing", "2", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareAssistWithDressing", "1", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareAssistWithDressing", "0", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Medication Reminder</td>
                            <td><%= Html.Hidden("HHAideVisit_PersonalCareMedicationReminder", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_PersonalCareMedicationReminder", "2", data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareMedicationReminder", "1", data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_PersonalCareMedicationReminder", "0", data.ContainsKey("PersonalCareMedicationReminder") && data["PersonalCareMedicationReminder"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <th colspan="3" class="align_left">Nutrition</th><th>Completed</th><th>Refuse</th><th>N/A</th>
                        </tr><tr>
                            <td colspan="3" class="align_left">Meal Set-up</td>
                            <td><%= Html.Hidden("HHAideVisit_NutritionMealSetUp", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_NutritionMealSetUp", "2", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_NutritionMealSetUp", "1", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_NutritionMealSetUp", "0", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with Feeding</td>
                            <td><%= Html.Hidden("HHAideVisit_NutritioAssistWithFeeding", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_NutritioAssistWithFeeding", "2", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_NutritioAssistWithFeeding", "1", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_NutritioAssistWithFeeding", "0", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr>
                    </thead>
                </table>
            </td><td>
                <table class="fixed">
                    <thead>
                        <tr><th colspan="3" class="align_left">Assignment</th><th colspan="3">Status</th></tr>
                        <tr><th colspan="3" class="align_left">Elimination</th><th>Completed</th><th>Refuse</th><th>N/A</th></tr>
                        <tr>
                            <td colspan="3" class="align_left">Assist with Bed Pan/Urinal</td>
                            <td><%= Html.Hidden("HHAideVisit_EliminationAssistWithBedPan", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_EliminationAssistWithBedPan", "2", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationAssistWithBedPan", "1", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationAssistWithBedPan", "0", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with BSC</td>
                            <td><%= Html.Hidden("HHAideVisit_EliminationAssistBSC", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_EliminationAssistBSC", "2", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationAssistBSC", "1", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationAssistBSC", "0", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Incontinence Care</td>
                            <td><%= Html.Hidden("HHAideVisit_EliminationIncontinenceCare", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_EliminationIncontinenceCare", "2", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationIncontinenceCare", "1", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationIncontinenceCare", "0", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Empty Drainage Bag</td>
                            <td><%= Html.Hidden("HHAideVisit_EliminationEmptyDrainageBag", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_EliminationEmptyDrainageBag", "2", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationEmptyDrainageBag", "1", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationEmptyDrainageBag", "0", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Record Bowel Movement</td>
                            <td><%= Html.Hidden("HHAideVisit_EliminationRecordBowelMovement", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_EliminationRecordBowelMovement", "2", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationRecordBowelMovement", "1", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationRecordBowelMovement", "0", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Catheter Care</td>
                            <td><%= Html.Hidden("HHAideVisit_EliminationCatheterCare", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_EliminationCatheterCare", "2", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationCatheterCare", "1", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_EliminationCatheterCare", "0", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <th colspan="3" class="align_left">Activity</th><th>Completed</th><th>Refuse</th><th>N/A</th>
                        </tr><tr>
                            <td colspan="3" class="align_left">Dangle on Side of Bed</td>
                            <td><%= Html.Hidden("HHAideVisit_ActivityDangleOnSideOfBed", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_ActivityDangleOnSideOfBed", "2", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityDangleOnSideOfBed", "1", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityDangleOnSideOfBed", "0", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Turn &#38; Position</td>
                            <td><%= Html.Hidden("HHAideVisit_ActivityTurnPosition", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_ActivityTurnPosition", "2", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityTurnPosition", "1", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityTurnPosition", "0", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with Transfer</td>
                            <td><%= Html.Hidden("HHAideVisit_ActivityAssistWithTransfer", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_ActivityAssistWithTransfer", "2", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityAssistWithTransfer", "1", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityAssistWithTransfer", "0", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with Ambulation</td>
                            <td><%= Html.Hidden("HHAideVisit_ActivityAssistWithAmbulation", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_ActivityAssistWithAmbulation", "2", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityAssistWithAmbulation", "1", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityAssistWithAmbulation", "0", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Range of Motion</td>
                            <td><%= Html.Hidden("HHAideVisit_ActivityRangeOfMotion", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_ActivityRangeOfMotion", "2", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityRangeOfMotion", "1", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityRangeOfMotion", "0", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Equipment Care</td>
                            <td><%= Html.Hidden("HHAideVisit_ActivityEquipmentCare", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_ActivityEquipmentCare", "2", data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityEquipmentCare", "1", data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_ActivityEquipmentCare", "0", data.ContainsKey("ActivityEquipmentCare") && data["ActivityEquipmentCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <th colspan="3" class="align_left">Household Task</th><th>Completed</th><th>Refuse</th><th>N/A</th>
                        </tr><tr>
                            <td colspan="3" class="align_left">Make Bed</td>
                            <td><%= Html.Hidden("HHAideVisit_HouseholdTaskMakeBed", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_HouseholdTaskMakeBed", "2", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_HouseholdTaskMakeBed", "1", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_HouseholdTaskMakeBed", "0", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Change Linen</td>
                            <td><%= Html.Hidden("HHAideVisit_HouseholdTaskChangeLinen", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_HouseholdTaskChangeLinen", "2", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_HouseholdTaskChangeLinen", "1", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_HouseholdTaskChangeLinen", "0", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Light Housekeeping</td>
                            <td><%= Html.Hidden("HHAideVisit_HouseholdTaskLightHousekeeping", " ", new { @id = "" })%><%= Html.RadioButton("HHAideVisit_HouseholdTaskLightHousekeeping", "2", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_HouseholdTaskLightHousekeeping", "1", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("HHAideVisit_HouseholdTaskLightHousekeeping", "0", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Other (Describe):</td><td colspan="3"><%= Html.TextBox("HHAideVisit_HouseholdTaskOther", data.ContainsKey("HouseholdTaskOther") ? data["HouseholdTaskOther"].Answer : string.Empty, new { @id = "HHAideVisit_HouseholdTaskOther", @class = "fill" })%></td>
                        </tr>
                    </thead>
                </table>
            </td>
        </tr>
        <tr><th colspan="2">Comments</th></tr>
        <tr><td colspan="2">
            <%= Html.Templates("HHAideVisit_CommentTemplates", new { @class = "Templates", @template = "#HHAideVisit_Comment" })%><br />
            <%= Html.TextArea("HHAideVisit_Comment", data.ContainsKey("Comment") ? data["Comment"].Answer : string.Empty, new { @id = "HHAideVisit_Comment", @class = "fill" })%>
        </td></tr>
    </tbody>    
</table>
