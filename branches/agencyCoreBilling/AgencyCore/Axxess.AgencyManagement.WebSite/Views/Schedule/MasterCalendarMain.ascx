﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "masterCalendarMain",
        "Master Calendar",
        Model.DisplayName.IsNotNullOrEmpty() ? Model.DisplayName.ToTitleCase() : "")%>
<div id="masterCalendarResult"><% Html.RenderPartial("~/Views/Schedule/MasterCalendar.ascx", Model); %></div>



