﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "scheduledeviation",
        "Schedule Deviation",
        Current.AgencyName)%>
<div class="wrapper main">
   <fieldset><div class="wide_column"><label  class="float_left">Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "AgencyScheduleDeviation_BranchCode", "", new { @id = "AgencyScheduleDeviation_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> <label  class="float_left">Date Range:</label><div class="float_left"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="AgencyScheduleDeviation_StartDate" class="shortdate" /><label > To </label><input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="AgencyScheduleDeviation_EndDate" class="shortdate" /></div> <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="RebindAgencyScheduleDeviation();">Generate Report</a></li><li><%= Html.ActionLink("Excel Export", "ExportScheduleDeviation", "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "AgencyScheduleDeviation_ExportLink", @class = "excel" })%></li></ul></div></div></fieldset>
   <% =Html.Telerik().Grid<ScheduleEvent>().Name("AgencyScheduleDeviationGrid")        
             .Columns(columns =>
                 {
                 columns.Bound(m => m.DisciplineTaskName).Title("Task");
                 columns.Bound(m => m.PatientName).Title("Patient Name");
                 columns.Bound(m => m.PatientIdNumber).Title("MR#").Width(70);
                 columns.Bound(m => m.StatusName).Title("Status");
                 columns.Bound(p => p.UserName).Title("Employee").Width(155);
                 columns.Bound(m => m.EventDate).Title("Schedule Date").Format("{0:MM/dd/yyyy}").Width(90);
                 columns.Bound(m => m.VisitDate).Title("Visit Date").Format("{0:MM/dd/yyyy}").Width(80);
                 }).DataBinding(dataBinding => dataBinding.Ajax().Select("ScheduleDeviation", "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
                  .Sortable().Selectable().Scrollable()%>
</div>
<script type="text/javascript">
    $('#AgencyScheduleDeviationGrid .t-grid-content').css({ 'height': 'auto' });
    $('#AgencyScheduleDeviationGrid').css({ 'top': '90px', 'bottom': '' });
    function RebindAgencyScheduleDeviation() {
        var grid = $('#AgencyScheduleDeviationGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgencyScheduleDeviation_BranchCode").val(), StartDate: $('#AgencyScheduleDeviation_StartDate').val(), EndDate: $('#AgencyScheduleDeviation_EndDate').val() }); }
        var $exportLink = $('#AgencyScheduleDeviation_ExportLink');
        if ($exportLink != null) {
            var href = $exportLink.attr('href');
            if (href != null) {
                href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyScheduleDeviation_BranchCode").val());
                href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AgencyScheduleDeviation_StartDate").val());
                href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#AgencyScheduleDeviation_EndDate").val());
                $exportLink.attr('href', href);
            }
        }
    }
    
</script>