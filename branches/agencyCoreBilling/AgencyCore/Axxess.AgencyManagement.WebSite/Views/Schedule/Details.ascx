﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<%= string.Format("{0}{1}{2}",
        "<script type='text/javascript'>acore.renamewindow('Task Details | ",
        Model.DisciplineTaskName.ToTitleCase() + " | " + Model.PatientName.ToTitleCase(),
        "','scheduledetails');</script>")%>
<% using (Html.BeginForm("EditDetails", "Schedule", FormMethod.Post, new { @id = "scheduleDetailsForm" })) { %>
<%= Html.Hidden("EventId", Model.EventId) %>
<%= Html.Hidden("UserName", Model.UserName)%>
<%= Html.Hidden("PatientId", Model.PatientId) %>
<%= Html.Hidden("EpisodeId", Model.EpisodeId) %>
<%= Html.Hidden("Discipline", Model.Discipline)%>
<% var visible = (Model.DisciplineTask == ((int)DisciplineTasks.CommunicationNote) ||  Model.Discipline == Disciplines.ReportsAndNotes.ToString()) ? false : true; %>
<div class="wrapper main">
    <fieldset>
        <legend>Details</legend>
        <div>
            <table class="form">
                   <tbody>
                        <tr>
                              <td><span class="bigtext"><%= Model.PatientName + (Model.PatientIdNumber.IsNotNullOrEmpty()? "("+Model.PatientIdNumber+")":"") %></span></td>  
                              <td><span class="bigtext">Episode:<%= Model.StartDate.ToShortDateString().ToZeroFilled() %> - <%= Model.EndDate.ToShortDateString().ToZeroFilled() %></span></td>  
                        </tr>
                    </tbody>
             </table>
        </div>
        <table class="form"><tbody>
            <tr>
                 <td>
                     <table>
                        <tbody>
                             <tr><td><label for="Schedule_Detail_Task" class="float_left">Task:</label><%= Model.DisciplineTaskName %></td></tr>
                             <tr><td><label for="Schedule_Detail_VisitDate" class="float_left">Scheduled Date:</label><br /><%= Html.Telerik().DatePicker().Name("EventDate").Value(Model.EventDate.ToDateTime()).HtmlAttributes(new { @id = "Schedule_Detail_EventDate", @class = "text required date" })%></td></tr> 
                             <tr><td><label for="Schedule_Detail_Status" class="float_left">Status:</label><br /><%= Html.Status("Status", Model.Status, Model.DisciplineTask, new { @id = "Schedule_Detail_Status" }) %></td></tr>       
                        </tbody>
                     </table>
                 </td>
                 <td>
                     <table>
                        <tbody>
                             <tr> <td> <% if (visible){ %><label for="Schedule_Detail_MissedVisit" class="float_left">Missed Visit:</label><%= Html.CheckBox("IsMissedVisit", Model.IsMissedVisit, new { @id = "Schedule_Detail_MissedVisit", @class = "radio" })%> <%} %></td></tr>
                             <tr> <td> <% if (visible){ %><label for="Schedule_Detail_Status" class="float_left">Actual Visit Date:</label><br /><%= Html.Telerik().DatePicker().Name("VisitDate").Value(Model.VisitDate.ToDateTime()).HtmlAttributes(new { @id = "Schedule_Detail_VisitDate", @class = "text required date" })%> <%} else {%><%= Html.Hidden("VisitDate", Model.EventDate, new { @id = "" })%><%} %></td></tr>
                             <tr> <td><label for="Schedule_Detail_AssignedTo" class="float_left">Clinician:</label><br /><% if (!Model.IsComplete){ %><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "Schedule_Detail_AssignedTo", @class = "Users requireddropdown" })%><% } else { %><%= Model.UserName %><% } %></td></tr>
                        </tbody>
                     </table>
                 </td>
                 <% if (visible){ %>
                 <td>
                   <table>
                        <tbody>
                             <tr><td><label for="Schedule_Detail_Billable" class="float_left">Billable:</label><%= Html.CheckBox("IsBillable", Model.IsBillable, new { @id = "Schedule_Detail_Billable", @class = "radio" })%></td></tr>
                             <tr><td><label for="Schedule_Detail_TimeIn" class="float_left">Time In:</label><br /><input type="text" size="10" id="Schedule_Detail_TimeIn" name="TimeIn" class="spinners" value="<%= Model.TimeIn %>" /></td></tr>
                             <tr><td><label for="Schedule_Detail_Surcharge" class="float_left">Surcharge:</label><br /><%= Html.TextBox("Surcharge", Model.Surcharge, new { @id = "Schedule_Detail_Surcharge", @class = "text digits input_wrapper", @maxlength = "5" })%></td></tr>
                        </tbody>
                     </table>
                 </td>
                 <td>
                       <table>
                            <tbody>
                               <tr><td><label for="Schedule_Detail_IsActive" class="float_left">Is Inactive:</label><%= Html.CheckBox("IsDeprecated", Model.IsDeprecated, new { @id = "Schedule_Detail_IsDeprecated", @class = "radio" })%></td></tr>
                               <tr><td><label for="Schedule_Detail_TimeOut" class="float_left">Time Out:</label><br /><input type="text" size="10" id="Schedule_Detail_TimeOut" name="TimeOut" class="spinners" value="<%= Model.TimeOut %>" /></td></tr>
                               <tr><td><label for="Schedule_Detail_AssociatedMileage" class="float_left">Associated Mileage:</label><br /><%= Html.TextBox("AssociatedMileage", Model.AssociatedMileage, new { @id = "Schedule_Detail_AssociatedMileage", @class = "text digits input_wrapper", @maxlength = "5" })%></td></tr>
                             </tbody>
                       </table>
                 </td>
                 <%} %>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide_column">
            <div class="row">
                <textarea id="Schedule_Detail_Comments" name="Comments" cols="5" rows="6"><%= Model.Comments %></textarea>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Attachments</legend>
        <div class="wide_column">
            <div class="row">
                <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                <span>There are <%= Model.Assets.Count.ToString() %> attachment(s).</span>
                <% foreach (Guid assetId in Model.Assets) { %>
                     <%= Html.Asset(assetId) %>
                <% } %>
                <br /><br /><span>Use the upload fields below to upload files associated with this scheduled task.</span><br />
                <table class="form"><tbody>
                    <tr><td><input id="Schedule_Detail_File1" type="file" name="Attachment1" /></td></tr>
                    <tr><td><input id="Schedule_Detail_File2" type="file" name="Attachment2" /></td></tr>
                    <tr><td><input id="Schedule_Detail_File3" type="file" name="Attachment3" /></td></tr>
                </tbody></table>
            </div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('scheduledetails');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    $("#Schedule_Detail_TimeIn").timeEntry({ spinnerImage: '/Images/sprite.png',
        spinnerSize: [15, 16, 0], spinnerIncDecOnly: true
    });
    $("#Schedule_Detail_TimeOut").timeEntry({ spinnerImage: '/Images/sprite.png',
        spinnerSize: [15, 16, 0], spinnerIncDecOnly: true
    });
</script>