﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %><%
var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>();
string[] genericReasonForDischarge = data.ContainsKey("GenericReasonForDischarge") && data["GenericReasonForDischarge"].Answer != "" ? data["GenericReasonForDischarge"].Answer.Split(',') : null;
string[] genericTreatmentPlan = data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer != "" ? data["GenericTreatmentPlan"].Answer.Split(',') : null; %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="4">Bed Mobility</th>
            <th colspan="6">Physical Assessment</th>
        </tr><tr>
            <td colspan="4">
                <table class="fixed">
                    <thead>
                        <tr>
                            <th colspan="2"></th>
                            <th><strong>Assistive Device</strong></th>
                            <th><strong>% Assist</strong></th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td class="align_left" >
                                <label for="<%= Model.Type %>_GenericBedMobilityRollingAssistiveDevice" class="strong">Rolling</label>
                            </td><td colspan="2">
                                <%= Html.TextBox(Model.Type+"_GenericBedMobilityRollingAssistiveDevice", data.ContainsKey("GenericBedMobilityRollingAssistiveDevice") ? data["GenericBedMobilityRollingAssistiveDevice"].Answer : string.Empty, new { @id = Model.Type+"_GenericBedMobilityRollingAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericBedMobilityRollingAssist", data.ContainsKey("GenericBedMobilityRollingAssist") ? data["GenericBedMobilityRollingAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericBedMobilityRollingAssist" })%> %
                            </td>
                        </tr><tr>
                            <td class="align_left" >
                                <label for="<%= Model.Type %>_GenericBedMobilitySitStandSitAssistiveDevice" class="strong">Sit Stand Sit</label>
                            </td><td colspan="2">
                                <%= Html.TextBox(Model.Type+"_GenericBedMobilitySitStandSitAssistiveDevice", data.ContainsKey("GenericBedMobilitySitStandSitAssistiveDevice") ? data["GenericBedMobilitySitStandSitAssistiveDevice"].Answer : string.Empty, new { @id = Model.Type+"_GenericBedMobilitySitStandSitAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericBedMobilitySitStandSitAssist", data.ContainsKey("GenericBedMobilitySitStandSitAssist") ? data["GenericBedMobilitySitStandSitAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericBedMobilitySitStandSitAssist" })%> %
                            </td>
                        </tr><tr>
                            <td class="align_left" >
                                <label for="<%= Model.Type %>_GenericBedMobilitySupToSitAssistiveDevice" class="strong">Sup to Sit</label>
                            </td><td colspan="2">
                                <%= Html.TextBox(Model.Type+"_GenericBedMobilitySupToSitAssistiveDevice", data.ContainsKey("GenericBedMobilitySupToSitAssistiveDevice") ? data["GenericBedMobilitySupToSitAssistiveDevice"].Answer : string.Empty, new { @id = Model.Type+"_GenericBedMobilitySupToSitAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericBedMobilitySupToSitAssist", data.ContainsKey("GenericBedMobilitySupToSitAssist") ? data["GenericBedMobilitySupToSitAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericBedMobilitySupToSitAssist" })%> %
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td><td colspan="6" rowspan="8">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2"></th>
                            <th colspan="2"><strong>ROM</strong></th>
                            <th colspan="2"><strong>Strength</strong></th>
                        </tr><tr>
                            <th class="align_left"><strong>Part</strong></th>
                            <th class="align_left"><strong>Action</strong></th>
                            <th><strong>Right</strong></th>
                            <th><strong>Left</strong></th>
                            <th><strong>Right</strong></th>
                            <th><strong>Left</strong></th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td class="align_left">
                                <strong>Shoulder</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderFlexionROMRight", data.ContainsKey("GenericShoulderFlexionROMRight") ? data["GenericShoulderFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderFlexionROMLeft", data.ContainsKey("GenericShoulderFlexionROMLeft") ? data["GenericShoulderFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderFlexionStrengthRight", data.ContainsKey("GenericShoulderFlexionStrengthRight") ? data["GenericShoulderFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderFlexionStrengthLeft", data.ContainsKey("GenericShoulderFlexionStrengthLeft") ? data["GenericShoulderFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderExtensionROMRight", data.ContainsKey("GenericShoulderExtensionROMRight") ? data["GenericShoulderExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderExtensionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderExtensionROMLeft", data.ContainsKey("GenericShoulderExtensionROMLeft") ? data["GenericShoulderExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderExtensionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderExtensionStrengthRight", data.ContainsKey("GenericShoulderExtensionStrengthRight") ? data["GenericShoulderExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderExtensionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderExtensionStrengthLeft", data.ContainsKey("GenericShoulderExtensionStrengthLeft") ? data["GenericShoulderExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderExtensionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Abduction
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderAbductionROMRight", data.ContainsKey("GenericShoulderAbductionROMRight") ? data["GenericShoulderAbductionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderAbductionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderAbductionROMLeft", data.ContainsKey("GenericShoulderAbductionROMLeft") ? data["GenericShoulderAbductionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderAbductionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderAbductionStrengthRight", data.ContainsKey("GenericShoulderAbductionStrengthRight") ? data["GenericShoulderAbductionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderAbductionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderAbductionStrengthLeft", data.ContainsKey("GenericShoulderAbductionStrengthLeft") ? data["GenericShoulderAbductionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderAbductionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Int Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderIntRotROMRight", data.ContainsKey("GenericShoulderIntRotROMRight") ? data["GenericShoulderIntRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderIntRotROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderIntRotROMLeft", data.ContainsKey("GenericShoulderIntRotROMLeft") ? data["GenericShoulderIntRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderIntRotROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderIntRotStrengthRight", data.ContainsKey("GenericShoulderIntRotStrengthRight") ? data["GenericShoulderIntRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderIntRotStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderIntRotStrengthLeft", data.ContainsKey("GenericShoulderIntRotStrengthLeft") ? data["GenericShoulderIntRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderIntRotStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Ext Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderExtRotROMRight", data.ContainsKey("GenericShoulderExtRotROMRight") ? data["GenericShoulderExtRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderExtRotROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderExtRotROMLeft", data.ContainsKey("GenericShoulderExtRotROMLeft") ? data["GenericShoulderExtRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderExtRotROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderExtRotStrengthRight", data.ContainsKey("GenericShoulderExtRotStrengthRight") ? data["GenericShoulderExtRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderExtRotStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericShoulderExtRotStrengthLeft", data.ContainsKey("GenericShoulderExtRotStrengthLeft") ? data["GenericShoulderExtRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericShoulderExtRotStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Elbow</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericElbowFlexionROMRight", data.ContainsKey("GenericElbowFlexionROMRight") ? data["GenericElbowFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericElbowFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericElbowFlexionROMLeft", data.ContainsKey("GenericElbowFlexionROMLeft") ? data["GenericElbowFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericElbowFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericElbowFlexionStrengthRight", data.ContainsKey("GenericElbowFlexionStrengthRight") ? data["GenericElbowFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericElbowFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericElbowFlexionStrengthLeft", data.ContainsKey("GenericElbowFlexionStrengthLeft") ? data["GenericElbowFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericElbowFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericElbowExtensionROMRight", data.ContainsKey("GenericElbowExtensionROMRight") ? data["GenericElbowExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericElbowExtensionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericElbowExtensionROMLeft", data.ContainsKey("GenericElbowExtensionROMLeft") ? data["GenericElbowExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericElbowExtensionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericElbowExtensionStrengthRight", data.ContainsKey("GenericElbowExtensionStrengthRight") ? data["GenericElbowExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericElbowExtensionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericElbowExtensionStrengthLeft", data.ContainsKey("GenericElbowExtensionStrengthLeft") ? data["GenericElbowExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericElbowExtensionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Finger</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericFingerFlexionROMRight", data.ContainsKey("GenericFingerFlexionROMRight") ? data["GenericFingerFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericFingerFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericFingerFlexionROMLeft", data.ContainsKey("GenericFingerFlexionROMLeft") ? data["GenericFingerFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericFingerFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericFingerFlexionStrengthRight", data.ContainsKey("GenericFingerFlexionStrengthRight") ? data["GenericFingerFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericFingerFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericFingerFlexionStrengthLeft", data.ContainsKey("GenericFingerFlexionStrengthLeft") ? data["GenericFingerFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericFingerFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericFingerExtensionROMRight", data.ContainsKey("GenericFingerExtensionROMRight") ? data["GenericFingerExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericFingerExtensionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericFingerExtensionROMLeft", data.ContainsKey("GenericFingerExtensionROMLeft") ? data["GenericFingerExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericFingerExtensionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericFingerExtensionStrengthRight", data.ContainsKey("GenericFingerExtensionStrengthRight") ? data["GenericFingerExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericFingerExtensionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericFingerExtensionStrengthLeft", data.ContainsKey("GenericFingerExtensionStrengthLeft") ? data["GenericFingerExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericFingerExtensionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Wrist</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericWristFlexionROMRight", data.ContainsKey("GenericWristFlexionROMRight") ? data["GenericWristFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericWristFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericWristFlexionROMLeft", data.ContainsKey("GenericWristFlexionROMLeft") ? data["GenericWristFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericWristFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericWristFlexionStrengthRight", data.ContainsKey("GenericWristFlexionStrengthRight") ? data["GenericWristFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericWristFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericWristFlexionStrengthLeft", data.ContainsKey("GenericWristFlexionStrengthLeft") ? data["GenericWristFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericWristFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericWristExtensionROMRight", data.ContainsKey("GenericWristExtensionROMRight") ? data["GenericWristExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericWristExtensionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericWristExtensionROMLeft", data.ContainsKey("GenericWristExtensionROMLeft") ? data["GenericWristExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericWristExtensionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericWristExtensionStrengthRight", data.ContainsKey("GenericWristExtensionStrengthRight") ? data["GenericWristExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericWristExtensionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericWristExtensionStrengthLeft", data.ContainsKey("GenericWristExtensionStrengthLeft") ? data["GenericWristExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericWristExtensionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Hip</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipFlexionROMRight", data.ContainsKey("GenericHipFlexionROMRight") ? data["GenericHipFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipFlexionROMLeft", data.ContainsKey("GenericHipFlexionROMLeft") ? data["GenericHipFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipFlexionStrengthRight", data.ContainsKey("GenericHipFlexionStrengthRight") ? data["GenericHipFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipFlexionStrengthLeft", data.ContainsKey("GenericHipFlexionStrengthLeft") ? data["GenericHipFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipExtensionROMRight", data.ContainsKey("GenericHipExtensionROMRight") ? data["GenericHipExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipExtensionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipExtensionROMLeft", data.ContainsKey("GenericHipExtensionROMLeft") ? data["GenericHipExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipExtensionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipExtensionStrengthRight", data.ContainsKey("GenericHipExtensionStrengthRight") ? data["GenericHipExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipExtensionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipExtensionStrengthLeft", data.ContainsKey("GenericHipExtensionStrengthLeft") ? data["GenericHipExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipExtensionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Abduction
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipAbductionROMRight", data.ContainsKey("GenericHipAbductionROMRight") ? data["GenericHipAbductionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipAbductionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipAbductionROMLeft", data.ContainsKey("GenericHipAbductionROMLeft") ? data["GenericHipAbductionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipAbductionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipAbductionStrengthRight", data.ContainsKey("GenericHipAbductionStrengthRight") ? data["GenericHipAbductionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipAbductionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipAbductionStrengthLeft", data.ContainsKey("GenericHipAbductionStrengthLeft") ? data["GenericHipAbductionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipAbductionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Int Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipIntRotROMRight", data.ContainsKey("GenericHipIntRotROMRight") ? data["GenericHipIntRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipIntRotROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipIntRotROMLeft", data.ContainsKey("GenericHipIntRotROMLeft") ? data["GenericHipIntRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipIntRotROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipIntRotStrengthRight", data.ContainsKey("GenericHipIntRotStrengthRight") ? data["GenericHipIntRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipIntRotStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipIntRotStrengthLeft", data.ContainsKey("GenericHipIntRotStrengthLeft") ? data["GenericHipIntRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipIntRotStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Ext Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipExtRotROMRight", data.ContainsKey("GenericHipExtRotROMRight") ? data["GenericHipExtRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipExtRotROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipExtRotROMLeft", data.ContainsKey("GenericHipExtRotROMLeft") ? data["GenericHipExtRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipExtRotROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipExtRotStrengthRight", data.ContainsKey("GenericHipExtRotStrengthRight") ? data["GenericHipExtRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipExtRotStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericHipExtRotStrengthLeft", data.ContainsKey("GenericHipExtRotStrengthLeft") ? data["GenericHipExtRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericHipExtRotStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Knee</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericKneeFlexionROMRight", data.ContainsKey("GenericKneeFlexionROMRight") ? data["GenericKneeFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericKneeFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericKneeFlexionROMLeft", data.ContainsKey("GenericKneeFlexionROMLeft") ? data["GenericKneeFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericKneeFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericKneeFlexionStrengthRight", data.ContainsKey("GenericKneeFlexionStrengthRight") ? data["GenericKneeFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericKneeFlexionStrengthRight" })%>
                            </td><td><%= Html.TextBox(Model.Type+"_GenericKneeFlexionStrengthLeft", data.ContainsKey("GenericKneeFlexionStrengthLeft") ? data["GenericKneeFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericKneeFlexionStrengthLeft" })%></td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericKneeExtensionROMRight", data.ContainsKey("GenericKneeExtensionROMRight") ? data["GenericKneeExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericKneeExtensionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericKneeExtensionROMLeft", data.ContainsKey("GenericKneeExtensionROMLeft") ? data["GenericKneeExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericKneeExtensionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericKneeExtensionStrengthRight", data.ContainsKey("GenericKneeExtensionStrengthRight") ? data["GenericKneeExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericKneeExtensionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericKneeExtensionStrengthLeft", data.ContainsKey("GenericKneeExtensionStrengthLeft") ? data["GenericKneeExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericKneeExtensionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Ankle</strong>
                            </td><td class="align_left">
                                Plantarflexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericAnklePlantFlexionROMRight", data.ContainsKey("GenericAnklePlantFlexionROMRight") ? data["GenericAnklePlantFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericAnklePlantFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericAnklePlantFlexionROMLeft", data.ContainsKey("GenericAnklePlantFlexionROMLeft") ? data["GenericAnklePlantFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericAnklePlantFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericAnklePlantFlexionStrengthRight", data.ContainsKey("GenericAnklePlantFlexionStrengthRight") ? data["GenericAnklePlantFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericAnklePlantFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericAnklePlantFlexionStrengthLeft", data.ContainsKey("GenericAnklePlantFlexionStrengthLeft") ? data["GenericAnklePlantFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericAnklePlantFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Dorsiflexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericAnkleFlexionROMRight", data.ContainsKey("GenericAnkleFlexionROMRight") ? data["GenericAnkleFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericAnkleFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericAnkleFlexionROMLeft", data.ContainsKey("GenericAnkleFlexionROMLeft") ? data["GenericAnkleFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericAnkleFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericAnkleFlexionStrengthRight", data.ContainsKey("GenericAnkleFlexionStrengthRight") ? data["GenericAnkleFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericAnkleFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericAnkleFlexionStrengthLeft", data.ContainsKey("GenericAnkleFlexionStrengthLeft") ? data["GenericAnkleFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericAnkleFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Trunk</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkFlexionROMRight", data.ContainsKey("GenericTrunkFlexionROMRight") ? data["GenericTrunkFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkFlexionROMLeft", data.ContainsKey("GenericTrunkFlexionROMLeft") ? data["GenericTrunkFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkFlexionStrengthRight", data.ContainsKey("GenericTrunkFlexionStrengthRight") ? data["GenericTrunkFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkFlexionStrengthLeft", data.ContainsKey("GenericTrunkFlexionStrengthLeft") ? data["GenericTrunkFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Rotation
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkRotationROMRight", data.ContainsKey("GenericTrunkRotationROMRight") ? data["GenericTrunkRotationROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkRotationROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkRotationROMLeft", data.ContainsKey("GenericTrunkRotationROMLeft") ? data["GenericTrunkRotationROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkRotationROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkRotationStrengthRight", data.ContainsKey("GenericTrunkRotationStrengthRight") ? data["GenericTrunkRotationStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkRotationStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkRotationStrengthLeft", data.ContainsKey("GenericTrunkRotationStrengthLeft") ? data["GenericTrunkRotationStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkRotationStrengthLeft" })%>
                                </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkExtensionROMRight", data.ContainsKey("GenericTrunkExtensionROMRight") ? data["GenericTrunkExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkExtensionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkExtensionROMLeft", data.ContainsKey("GenericTrunkExtensionROMLeft") ? data["GenericTrunkExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkExtensionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTrunkExtensionStrengthRight", data.ContainsKey("GenericTrunkExtensionStrengthRight") ? data["GenericTrunkExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkExtensionStrengthRight" })%>
                            </td><td><%= Html.TextBox(Model.Type+"_GenericTrunkExtensionStrengthLeft", data.ContainsKey("GenericTrunkExtensionStrengthLeft") ? data["GenericTrunkExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericTrunkExtensionStrengthLeft" })%></td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Neck</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckFlexionROMRight", data.ContainsKey("GenericNeckFlexionROMRight") ? data["GenericNeckFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckFlexionROMLeft", data.ContainsKey("GenericNeckFlexionROMLeft") ? data["GenericNeckFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckFlexionStrengthRight", data.ContainsKey("GenericNeckFlexionStrengthRight") ? data["GenericNeckFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckFlexionStrengthLeft", data.ContainsKey("GenericNeckFlexionStrengthLeft") ? data["GenericNeckFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckExtensionROMRight", data.ContainsKey("GenericNeckExtensionROMRight") ? data["GenericNeckExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckExtensionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckExtensionROMLeft", data.ContainsKey("GenericNeckExtensionROMLeft") ? data["GenericNeckExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckExtensionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckExtensionStrengthRight", data.ContainsKey("GenericNeckExtensionStrengthRight") ? data["GenericNeckExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckExtensionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckExtensionStrengthLeft", data.ContainsKey("GenericNeckExtensionStrengthLeft") ? data["GenericNeckExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckExtensionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Lat Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckLatFlexionROMRight", data.ContainsKey("GenericNeckLatFlexionROMRight") ? data["GenericNeckLatFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckLatFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckLatFlexionROMLeft", data.ContainsKey("GenericNeckLatFlexionROMLeft") ? data["GenericNeckLatFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckLatFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckLatFlexionStrengthRight", data.ContainsKey("GenericNeckLatFlexionStrengthRight") ? data["GenericNeckLatFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckLatFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckLatFlexionStrengthLeft", data.ContainsKey("GenericNeckLatFlexionStrengthLeft") ? data["GenericNeckLatFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckLatFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Long Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckLongFlexionROMRight", data.ContainsKey("GenericNeckLongFlexionROMRight") ? data["GenericNeckLongFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckLongFlexionROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckLongFlexionROMLeft", data.ContainsKey("GenericNeckLongFlexionROMLeft") ? data["GenericNeckLongFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckLongFlexionROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckLongFlexionStrengthRight", data.ContainsKey("GenericNeckLongFlexionStrengthRight") ? data["GenericNeckLongFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckLongFlexionStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckLongFlexionStrengthLeft", data.ContainsKey("GenericNeckLongFlexionStrengthLeft") ? data["GenericNeckLongFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckLongFlexionStrengthLeft" })%>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Rotation
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckRotationROMRight", data.ContainsKey("GenericNeckRotationROMRight") ? data["GenericNeckRotationROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckRotationROMRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckRotationROMLeft", data.ContainsKey("GenericNeckRotationROMLeft") ? data["GenericNeckRotationROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckRotationROMLeft" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckRotationStrengthRight", data.ContainsKey("GenericNeckRotationStrengthRight") ? data["GenericNeckRotationStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckRotationStrengthRight" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericNeckRotationStrengthLeft", data.ContainsKey("GenericNeckRotationStrengthLeft") ? data["GenericNeckRotationStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type+"_GenericNeckRotationStrengthLeft" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="4">Transfer</th>
        </tr><tr>
            <td colspan="4">
                <table class="fixed">
                    <tbody>
                        <tr>
                            <th colspan="2"></th>
                            <th><strong>Assistive Device</strong></th>
                            <th><strong>% Assist</strong></th>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferBedChairAssistiveDevice" class="strong">Bed-Chair</label>
                            </td><td colspan="2">
                                <%= Html.TextBox(Model.Type+"_GenericTransferBedChairAssistiveDevice", data.ContainsKey("GenericTransferBedChairAssistiveDevice") ? data["GenericTransferBedChairAssistiveDevice"].Answer : string.Empty, new { @id = Model.Type+"_GenericTransferBedChairAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTransferBedChairAssist", data.ContainsKey("GenericTransferBedChairAssist") ? data["GenericTransferBedChairAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericTransferBedChairAssist" })%> %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferChairBedAssistiveDevice" class="strong">Chair-Bed</label>
                            </td><td colspan="2">
                                <%= Html.TextBox(Model.Type+"_GenericTransferChairBedAssistiveDevice", data.ContainsKey("GenericTransferChairBedAssistiveDevice") ? data["GenericTransferChairBedAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type+"_GenericTransferChairBedAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTransferChairBedAssist", data.ContainsKey("GenericTransferChairBedAssist") ? data["GenericTransferChairBedAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericTransferChairBedAssist" })%> %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferChairToWCAssistiveDevice" class="strong">Chair to W/C</label>
                            </td><td colspan="2">
                                <%= Html.TextBox(Model.Type+"_GenericTransferChairToWCAssistiveDevice", data.ContainsKey("GenericTransferChairToWCAssistiveDevice") ? data["GenericTransferChairToWCAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type+"_GenericTransferChairToWCAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTransferChairToWCAssist", data.ContainsKey("GenericTransferChairToWCAssist") ? data["GenericTransferChairToWCAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericTransferChairToWCAssist" })%> %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferToiletOrBSCAssistiveDevice" class="strong">Toilet or BSC</label>
                            </td><td colspan="2">
                                <%= Html.TextBox(Model.Type+"_GenericTransferToiletOrBSCAssistiveDevice", data.ContainsKey("GenericTransferToiletOrBSCAssistiveDevice") ? data["GenericTransferToiletOrBSCAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type+"_GenericTransferToiletOrBSCAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTransferToiletOrBSCAssist", data.ContainsKey("GenericTransferToiletOrBSCAssist") ? data["GenericTransferToiletOrBSCAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericTransferToiletOrBSCAssist" })%> %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferCanVanAssistiveDevice" class="strong">Car/Van</label>
                            </td><td colspan="2">
                                <%= Html.TextBox(Model.Type+"_GenericTransferCanVanAssistiveDevice", data.ContainsKey("GenericTransferCanVanAssistiveDevice") ? data["GenericTransferCanVanAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type+"_GenericTransferCanVanAssistiveDevice" })%>
                            </td><td >
                                <%= Html.TextBox(Model.Type+"_GenericTransferCanVanAssist", data.ContainsKey("GenericTransferCanVanAssist") ? data["GenericTransferCanVanAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericTransferCanVanAssist" })%> %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferTubShowerAssistiveDevice" class="strong">Tub/Shower</label>
                            </td><td colspan="2">
                                <%= Html.TextBox(Model.Type+"_GenericTransferTubShowerAssistiveDevice", data.ContainsKey("GenericTransferTubShowerAssistiveDevice") ? data["GenericTransferTubShowerAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type+"_GenericTransferTubShowerAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTransferTubShowerAssist", data.ContainsKey("GenericTransferTubShowerAssist") ? data["GenericTransferTubShowerAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericTransferTubShowerAssist" })%> %
                            </td>
                        </tr><tr>
                            <th colspan="2"></th>
                            <th><strong>Static</strong></th>
                            <th><strong>Dynamic</strong></th>
                        </tr><tr>
                            <td colspan="2">
                                <label for="<%= Model.Type %>_GenericTransferSittingBalanceStatic" class="strong">Sitting Balance</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTransferSittingBalanceStatic", data.ContainsKey("GenericTransferSittingBalanceStatic") ? data["GenericTransferSittingBalanceStatic"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericTransferSittingBalanceStatic" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTransferSittingBalanceDynamic", data.ContainsKey("GenericTransferSittingBalanceDynamic") ? data["GenericTransferSittingBalanceDynamic"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericTransferSittingBalanceDynamic" })%> %
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <label for="<%= Model.Type %>_GenericTransferStandBalanceStatic" class="strong">Stand Balance</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTransferStandBalanceStatic", data.ContainsKey("GenericTransferStandBalanceStatic") ? data["GenericTransferStandBalanceStatic"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericTransferStandBalanceStatic" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericTransferStandBalanceDynamic", data.ContainsKey("GenericTransferStandBalanceDynamic") ? data["GenericTransferStandBalanceDynamic"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericTransferStandBalanceDynamic" })%> %
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="4">Gait</th>
        </tr><tr>
            <td colspan="4">
                <div>
                    <label for="" class="float_left">Level</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type+"_GenericGaitLevelAssist", data.ContainsKey("GenericGaitLevelAssist") ? data["GenericGaitLevelAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericGaitLevelAssist" })%>
                        X <%= Html.TextBox(Model.Type+"_GenericGaitLevelFeet", data.ContainsKey("GenericGaitLevelFeet") ? data["GenericGaitLevelFeet"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericGaitLevelFeet" })%> Feet
                    </div>
                    <div class="clear"></div>
                </div><div>
                    <label for="" class="float_left">Unlevel</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type+"_GenericGaitUnLevelAssist", data.ContainsKey("GenericGaitUnLevelAssist") ? data["GenericGaitUnLevelAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericGaitUnLevelAssist" })%>
                        X <%= Html.TextBox(Model.Type+"_GenericGaitUnLevelFeet", data.ContainsKey("GenericGaitUnLevelFeet") ? data["GenericGaitUnLevelFeet"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericGaitUnLevelFeet" })%> Feet
                    </div>
                    <div class="clear"></div>
                </div><div>
                    <label for="" class="float_left">Step/ Stair</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type+"_GenericGaitStepStairAssist", data.ContainsKey("GenericGaitStepStairAssist") ? data["GenericGaitStepStairAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericGaitStepStairAssist" })%>
                        X <%= Html.TextBox(Model.Type+"_GenericGaitStepStairFeet", data.ContainsKey("GenericGaitStepStairFeet") ? data["GenericGaitStepStairFeet"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericGaitStepStairFeet" })%> Feet
                    </div>
                    <div class="clear"></div>
                </div>
            </td>
        </tr><tr>
            <th colspan="4">W/C Mobility</th>
        </tr><tr>
            <td colspan="4">
                <table class="fixed">
                    <tbody>
                        <tr>
                            <th>
                                <label for="<%= Model.Type %>_GenericWCMobilityLevel">Level</label>
                            </th><th>
                                <label for="<%= Model.Type %>_GenericWCMobilityRamp">Ramp</label>
                            </th><th>
                                <label for="<%= Model.Type %>_GenericWCMobilityManeuver">Maneuver</label>
                            </th>
                        </tr><tr>
                            <td>
                                <%= Html.TextBox(Model.Type+"_GenericWCMobilityLevel", data.ContainsKey("GenericWCMobilityLevel") ? data["GenericWCMobilityLevel"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericWCMobilityLevel" })%> %
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericWCMobilityRamp", data.ContainsKey("GenericWCMobilityRamp") ? data["GenericWCMobilityRamp"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericWCMobilityRamp" })%> %
                            </td><td>
                                <%= Html.TextBox(Model.Type+"_GenericWCMobilityManeuver", data.ContainsKey("GenericWCMobilityManeuver") ? data["GenericWCMobilityManeuver"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericWCMobilityManeuver" })%> %
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <td colspan="4">
                <div>
                    <label for="<%= Model.Type %>_GenericPainLevel" class="float_left">Pain Level</label>
                    <div class="float_right"><%
                        var genericPainLevel = new SelectList(new[] {
                            new SelectListItem { Text = "0 = No Pain", Value = "0" },
                            new SelectListItem { Text = "1", Value = "1" },
                            new SelectListItem { Text = "2", Value = "2" },
                            new SelectListItem { Text = "3", Value = "3" },
                            new SelectListItem { Text = "4", Value = "4" },
                            new SelectListItem { Text = "Moderate Pain", Value = "5" },
                            new SelectListItem { Text = "6", Value = "6" },
                            new SelectListItem { Text = "7", Value = "7" },
                            new SelectListItem { Text = "8", Value = "8" },
                            new SelectListItem { Text = "9", Value = "9" },
                            new SelectListItem { Text = "10", Value = "10" }
                        }, "Value", "Text", data.ContainsKey("GenericPainLevel") ? data["GenericPainLevel"].Answer : "0");%>
                        <%= Html.DropDownList(Model.Type+"_GenericPainLevel", genericPainLevel, new { @id = Model.Type+"_GenericPainLevel", @class = "oe" })%>
                    </div>
                    <div class="clear"></div>
                </div><div>
                    <label for="<%= Model.Type %>_GenericADL" class="float_left">ADL</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type+"_GenericADL", data.ContainsKey("GenericADL") ? data["GenericADL"].Answer : string.Empty, new { @class = "sn", @id = Model.Type+"_GenericADL" })%> %
                    </div>
                    <div class="clear"></div>
                </div><div>
                    <label for="<%= Model.Type %>_GenericEndurance" class="float_left">Endurance</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type+"_GenericEndurance", data.ContainsKey("GenericEndurance") ? data["GenericEndurance"].Answer : string.Empty, new { @class = "",  @id = Model.Type+"_GenericEndurance" })%>
                    </div>
                    <div class="clear"></div>
                </div>
            </td>
        </tr><tr>
            <th colspan="5">Reason for Discharge</th>
            <th colspan="5">Treatment Plan</th>
        </tr><tr>
            <td colspan="5">
                <input type="hidden" name="<%= Model.Type %>_GenericReasonForDischarge" value="" />
                <table class="fixed align_left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge1' class='radio' name='{1}_GenericReasonForDischarge' value='1' type='checkbox' {0} />", genericReasonForDischarge != null && genericReasonForDischarge.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge1">Reached Maximum Potential</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge2' class='radio' name='{1}_GenericReasonForDischarge' value='2' type='checkbox' {0} />", genericReasonForDischarge != null && genericReasonForDischarge.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge2">No Longer Homebound</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge3' class='radio' name='{1}_GenericReasonForDischarge' value='3' type='checkbox' {0} />", genericReasonForDischarge != null && genericReasonForDischarge.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge3">Per Patient/Family Request</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge4' class='radio' name='{1}_GenericReasonForDischarge' value='4' type='checkbox' {0} />", genericReasonForDischarge != null && genericReasonForDischarge.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge4">Prolonged On-Hold Status</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge5' class='radio' name='{1}_GenericReasonForDischarge' value='5' type='checkbox' {0} />", genericReasonForDischarge != null && genericReasonForDischarge.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge5">Prolonged On-Hold Status</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge6' class='radio' name='{1}_GenericReasonForDischarge' value='6' type='checkbox' {0} />", genericReasonForDischarge != null && genericReasonForDischarge.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge6">Hospitalized</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericReasonForDischarge7' class='radio' name='{1}_GenericReasonForDischarge' value='7' type='checkbox' {0} />", genericReasonForDischarge != null && genericReasonForDischarge.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericReasonForDischarge7">Expired</label>
                            </td><td>
                                <label for="<%= Model.Type %>_GenericReasonForDischargeOther" class="float_left">Other</label>
                                <%= Html.TextBox(Model.Type+"_GenericReasonForDischargeOther", data.ContainsKey("GenericReasonForDischargeOther") ? data["GenericReasonForDischargeOther"].Answer : string.Empty, new { @class = "", @id = Model.Type+"_GenericReasonForDischargeOther" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td><td colspan="5">
                <input type="hidden" name="<%= Model.Type %>_GenericTreatmentPlan" value="" />
                <table class="fixed align_left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan1' class='radio' name='{1}_GenericTreatmentPlan' value='1' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan1">Thera Ex</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan2' class='radio' name='{1}_GenericTreatmentPlan' value='2' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan2">Bed Mobility Training</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan3' class='radio' name='{1}_GenericTreatmentPlan' value='3' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan3">Transfer Training</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan4' class='radio' name='{1}_GenericTreatmentPlan' value='4' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan4">Balance Training</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan5' class='radio' name='{1}_GenericTreatmentPlan' value='5' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan5">Gait Training</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan6' class='radio' name='{1}_GenericTreatmentPlan' value='6' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan6">HEP</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan7' class='radio' name='{1}_GenericTreatmentPlan' value='7' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan7">Electrotherapy</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan8' class='radio' name='{1}_GenericTreatmentPlan' value='8' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan8">Ultrasound</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan9' class='radio' name='{1}_GenericTreatmentPlan' value='9' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("9") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan9">Prosthetic Training</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan10' class='radio' name='{1}_GenericTreatmentPlan' value='10' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("10") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan10">Manual Therapy</label>
                            </td><td>
                                <label for="<%= Model.Type %>_GenericTreatmentCodesOther" class="float_left">Other</label>
                                <%= Html.TextBox(Model.Type+"_GenericTreatmentCodesOther", data.ContainsKey("GenericTreatmentCodesOther") ? data["GenericTreatmentCodesOther"].Answer : string.Empty, new { @class = "", @id = Model.Type+"_GenericTreatmentCodesOther" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <td colspan="10">
                <div class="third">
                    <label for="<%= Model.Type %>_GenericFrequency" class="float_left">Frequency:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type+"_GenericFrequency", data.ContainsKey("GenericFrequency") ? data["GenericFrequency"].Answer : string.Empty, new { @id = Model.Type+"_GenericFrequency" })%>
                    </div>
                </div><div class="third">
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericDuration" class="float_left">Duration:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type+"_GenericDuration", data.ContainsKey("GenericDuration") ? data["GenericDuration"].Answer : string.Empty, new { @id = Model.Type+"_GenericDuration" })%>
                    </div>
                </div>
            </td>
        </tr><tr>
            <th colspan="10">Narrative</th>
        </tr><tr>
            <td colspan="10">
                <div><%= Html.Templates(Model.Type + "_GenericNarrativeCommentTemplates", new { @class = "Templates", @template = "#" + Model.Type + "_GenericNarrativeComment" })%>
                <%= Html.TextArea(Model.Type + "_GenericNarrativeComment", data.ContainsKey("GenericNarrativeComment") ? data["GenericNarrativeComment"].Answer : string.Empty, 8, 20, new { @id = Model.Type + "_GenericNarrativeComment", @class = "fill" })%></div>
            </td>
         </tr>
    </tbody>
</table>