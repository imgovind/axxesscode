﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<VisitNoteViewData>" %>
<%var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %><%= Model.Type == "COTAVisit" ? "COTA" : "Occupational Therapist" %> Visit<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
    <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion))%>
</head>
<% var location = Model.Agency.GetBranch(Model.Patient != null ? Model.Patient.AgencyLocationId : Guid.Empty); %>
<% if (location == null) location = Model.Agency.GetMainOffice(); %>
<body></body><%
string[] genericHomeBoundStatus = data.ContainsKey("GenericHomeBoundStatus") && data["GenericHomeBoundStatus"].Answer != "" ? data["GenericHomeBoundStatus"].Answer.Split(',') : null;
string[] genericFunctionalLimitations = data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer != "" ? data["GenericFunctionalLimitations"].Answer.Split(',') : null;
string[] genericSupervisoryVisit = data.ContainsKey("GenericSupervisoryVisit") && data["GenericSupervisoryVisit"].Answer != "" ? data["GenericSupervisoryVisit"].Answer.Split(',') : null;
string[] genericTherapeuticExercises = data.ContainsKey("GenericTherapeuticExercises") && data["GenericTherapeuticExercises"].Answer != "" ? data["GenericTherapeuticExercises"].Answer.Split(',') : null;
string[] genericTherapyTraning = data.ContainsKey("GenericTherapyTraning") && data["GenericTherapyTraning"].Answer != "" ? data["GenericTherapyTraning"].Answer.Split(',') : null;
string[] genericTeaching = data.ContainsKey("GenericTeaching") && data["GenericTeaching"].Answer != "" ? data["GenericTeaching"].Answer.Split(',') : null;
string[] genericWalkDirection = data.ContainsKey("GenericWalkDirection") && data["GenericWalkDirection"].Answer != "" ? data["GenericWalkDirection"].Answer.Split(',') : null;
Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
    .Add("jquery-1.6.2.min.js")
    .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
    .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
).OnDocumentReady(() => { %>
    printview.firstheader = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.Clean().ToString().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%= Model.Type == "COTAVisit" ? "COTA" : "Occupational Therapist" %> Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22octocol%22%3E%3Cspan%3E%3Cstrong%3EMR:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model.Patient != null ? Model.Patient.PatientIdNumber : "" %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3EVisit Date%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("VisitDate") ? data["VisitDate"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime In:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeIn") ? data["TimeIn"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3Cspan%3E%3Cstrong%3ETime Out:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= data != null && data.ContainsKey("TimeOut") ? data["TimeOut"].Answer.Clean() : string.Empty %>" +
        "%3C/span%3E%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd colspan=%222%22%3E" +
        '<%= Model.Agency.Name.Clean().IsNotNullOrEmpty() ? Model.Agency.Name.Clean().ToTitleCase() + "%3Cbr /%3E" : ""%><%= location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.Clean().ToTitleCase() : ""%><%= location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.Clean().ToTitleCase() : ""%>%3Cbr /%3E<%= location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.Clean().ToTitleCase() + ", " : ""%><%= location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : ""%><%= location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode.Clean() : ""%>' +
        "%3C/td%3E%3Cth class=%22h1%22%3E<%= Model.Type == "COTAVisit" ? "COT" : "Occupational Therapist" %> Visit%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd colspan=%223%22%3E%3Cspan class=%22big%22%3EPatient Name: " +
        "<%= Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).Clean().ToTitleCase() : string.Empty %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "%3Cspan class=%22bicol%22%3E%3Cspan%3E%3Cstrong%3EClinician Signature:%3C/strong%3E%3C/span%3E%3Cspan%3E%3Cstrong%3EDate:%3C/strong%3E%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureText.IsNotNullOrEmpty() ? Model.SignatureText.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3Cspan%3E" +
        "<%= Model != null && Model.SignatureDate.IsNotNullOrEmpty() && Model.SignatureDate != "1/1/0001" ? Model.SignatureDate.Clean() : "%3Cspan class=%22blank_line%22%3E%3C/span%3E" %>" +
        "%3C/span%3E%3C/span%3E";
    printview.addsection(
            printview.col(6,
            printview.span("Req. Assistance With",true) +
            printview.checkbox("Gait",<%= data != null && data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer.Split(',').Contains("1") ? "true" : "false" %>) +
            printview.checkbox("Leaving the Home",<%= data != null && data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer.Split(',').Contains("2") ? "true" : "false" %>) +
            printview.checkbox("Transfers",<%= data != null && data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer.Split(',').Contains("3") ? "true" : "false" %>) +
            printview.checkbox("SOB/Endurance",<%= data != null && data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer.Split(',').Contains("4") ? "true" : "false" %>) +
            printview.checkbox("Orientation",<%= data != null && data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer.Split(',').Contains("5") ? "true" : "false" %>) +
            printview.span("Func. Limitations",true) +
            printview.checkbox("Transfer",<%= data != null && data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Gait",<%= data != null && data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Strength",<%= data != null && data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Bed Mobility",<%= data != null && data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("Safety Techniques",<%= data != null && data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.span("") +
            printview.checkbox("ADL&#8217;s",<%= data != null && data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("ROM",<%= data != null && data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("W/C Mobility",<%= data != null && data.ContainsKey("GenericFunctionalLimitations") && data["GenericFunctionalLimitations"].Answer.Split(',').Contains("8") ? "true" : "false"%>) +
            printview.span("Other") +
            printview.span("<%= data != null && data.ContainsKey("GenericFunctionalLimitationsOther") && data["GenericFunctionalLimitationsOther"].Answer.IsNotNullOrEmpty() ? data["GenericFunctionalLimitationsOther"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Subjective",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericSubjective") && data["GenericSubjective"].Answer.IsNotNullOrEmpty() ? data["GenericSubjective"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Pain Location",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericPainAssessmentLocation") && data["GenericPainAssessmentLocation"].Answer.IsNotNullOrEmpty() ? data["GenericPainAssessmentLocation"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Pain Level",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericIntensityOfPain") && data["GenericIntensityOfPain"].Answer.IsNotNullOrEmpty() ? data["GenericIntensityOfPain"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Objective",true) +
            printview.checkbox("IND",<%= data != null && data.ContainsKey("GenericObjectiveLevelOfAssist") && data["GenericObjectiveLevelOfAssist"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("SUP",<%= data != null && data.ContainsKey("GenericObjectiveLevelOfAssist") && data["GenericObjectiveLevelOfAssist"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("SBA",<%= data != null && data.ContainsKey("GenericObjectiveLevelOfAssist") && data["GenericObjectiveLevelOfAssist"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("CGA",<%= data != null && data.ContainsKey("GenericObjectiveLevelOfAssist") && data["GenericObjectiveLevelOfAssist"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("MIN",<%= data != null && data.ContainsKey("GenericObjectiveLevelOfAssist") && data["GenericObjectiveLevelOfAssist"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.span("") +
            printview.checkbox("MOD",<%= data != null && data.ContainsKey("GenericObjectiveLevelOfAssist") && data["GenericObjectiveLevelOfAssist"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("MAX",<%= data != null && data.ContainsKey("GenericObjectiveLevelOfAssist") && data["GenericObjectiveLevelOfAssist"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.checkbox("DEP",<%= data != null && data.ContainsKey("GenericObjectiveLevelOfAssist") && data["GenericObjectiveLevelOfAssist"].Answer.Split(',').Contains("8") ? "true" : "false"%>)));
    printview.addsection(
        printview.col(6,
            printview.span("Bathing",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLBathing") && data["GenericADLBathing"].Answer.IsNotNullOrEmpty() ? data["GenericADLBathing"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("UE Dressing",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLUEDressing") && data["GenericADLUEDressing"].Answer.IsNotNullOrEmpty() ? data["GenericADLUEDressing"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("LE Dressing",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLLEDressing") && data["GenericADLLEDressing"].Answer.IsNotNullOrEmpty() ? data["GenericADLLEDressing"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Grooming",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLGrooming") && data["GenericADLGrooming"].Answer.IsNotNullOrEmpty() ? data["GenericADLGrooming"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Toileting",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLToileting") && data["GenericADLToileting"].Answer.IsNotNullOrEmpty() ? data["GenericADLToileting"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Feeding",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLFeeding") && data["GenericADLFeeding"].Answer.IsNotNullOrEmpty() ? data["GenericADLFeeding"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Meal Prep",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLMealPrep") && data["GenericADLMealPrep"].Answer.IsNotNullOrEmpty() ? data["GenericADLMealPrep"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("House Cleaning",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLHouseCleaning") && data["GenericADLHouseCleaning"].Answer.IsNotNullOrEmpty() ? data["GenericADLHouseCleaning"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("House Safety",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLHouseSafety") && data["GenericADLHouseSafety"].Answer.IsNotNullOrEmpty() ? data["GenericADLHouseSafety"].Answer.Clean() : string.Empty %>",0,1)) +
        printview.col(2,
            printview.span("Adaptive Equipment/Assistive Device Use or Needs",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericADLAdaptiveEquipment") && data["GenericADLAdaptiveEquipment"].Answer.IsNotNullOrEmpty() ? data["GenericADLAdaptiveEquipment"].Answer.Clean() : string.Empty %>",0,1)),
        "ADL Training");
    printview.addsection(
        printview.col(4,
            printview.span("Bed Mobility",true) +
            printview.span("Rolling") +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityRollingReps") && data["GenericBedMobilityRollingReps"].Answer.IsNotNullOrEmpty() ? "x" + data["GenericBedMobilityRollingReps"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityRollingAssist") && data["GenericBedMobilityRollingAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilityRollingAssist"].Answer.Clean() + "% Assist" : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Supine to Sit") +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilitySupineToSitReps") && data["GenericBedMobilitySupineToSitReps"].Answer.IsNotNullOrEmpty() ? "x" + data["GenericBedMobilitySupineToSitReps"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilitySupineToSitAssist") && data["GenericBedMobilitySupineToSitAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilitySupineToSitAssist"].Answer.Clean() + "% Assist" : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Dynamic Reaching") +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityDynamicReachingReps") && data["GenericBedMobilityDynamicReachingReps"].Answer.IsNotNullOrEmpty() ? "x" + data["GenericBedMobilityDynamicReachingReps"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityDynamicReachingAssist") && data["GenericBedMobilityDynamicReachingAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilityDynamicReachingAssist"].Answer.Clean() + "% Assist" : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Gross/Fine Motor Coord") +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityGrossFineMotorCoordReps") && data["GenericBedMobilityGrossFineMotorCoordReps"].Answer.IsNotNullOrEmpty() ? "x" + data["GenericBedMobilityGrossFineMotorCoordReps"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityGrossFineMotorCoordAssist") && data["GenericBedMobilityGrossFineMotorCoordAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilityGrossFineMotorCoordAssist"].Answer.Clean() + "% Assist" : string.Empty %>",0,1) +
            printview.span("Transfers",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTransfersType") && data["GenericTransfersType"].Answer.IsNotNullOrEmpty() ? "Type: " + data["GenericTransfersType"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityGrossFineMotorCoordReps") && data["GenericBedMobilityGrossFineMotorCoordReps"].Answer.IsNotNullOrEmpty() ? "x" + data["GenericBedMobilityGrossFineMotorCoordReps"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericBedMobilityGrossFineMotorCoordAssist") && data["GenericBedMobilityGrossFineMotorCoordAssist"].Answer.IsNotNullOrEmpty() ? data["GenericBedMobilityGrossFineMotorCoordAssist"].Answer.Clean() + "% Assist" : string.Empty %>",0,1) +
            printview.span("") +
            printview.span("Correct Unfolding") +
            printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericCorrectUnfolding") && data["GenericCorrectUnfolding"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("No",<%= data != null && data.ContainsKey("GenericCorrectUnfolding") && data["GenericCorrectUnfolding"].Answer == "0" ? "true" : "false"%>) +
            printview.span("") +
            printview.span("Correct Foot Placement") +
            printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericCorrectFootPlacement") && data["GenericCorrectFootPlacement"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("No",<%= data != null && data.ContainsKey("GenericCorrectFootPlacement") && data["GenericCorrectFootPlacement"].Answer == "0" ? "true" : "false"%>) +
            printview.span("") +
            printview.span("Assistive Device") +
            printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer == "1" ? "true" : "false"%>) +
            printview.checkbox("No",<%= data != null && data.ContainsKey("GenericAssistiveDevice") && data["GenericAssistiveDevice"].Answer == "0" ? "true" : "false"%>)),
        "Therapeutic/Dynamic Activities");
    printview.addsection(
        printview.span("Propulsion with",true) +
        printview.col(6,
            printview.checkbox("RUE",<%= data != null && data.ContainsKey("GenericPropulsionWith") && data["GenericPropulsionWith"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("LUe",<%= data != null && data.ContainsKey("GenericPropulsionWith") && data["GenericPropulsionWith"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("BUE",<%= data != null && data.ContainsKey("GenericPropulsionWith") && data["GenericPropulsionWith"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("RLE",<%= data != null && data.ContainsKey("GenericPropulsionWith") && data["GenericPropulsionWith"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("LLE",<%= data != null && data.ContainsKey("GenericPropulsionWith") && data["GenericPropulsionWith"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("BLE",<%= data != null && data.ContainsKey("GenericPropulsionWith") && data["GenericPropulsionWith"].Answer.Split(',').Contains("6") ? "true" : "false"%>)) +
        printview.col(4,
            printview.span("Distance",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericDistanceFT") && data["GenericDistanceFT"].Answer.IsNotNullOrEmpty() ? data["GenericDistanceFT"].Answer.Clean() + "ft" : string.Empty %><%= data != null && data.ContainsKey("GenericDistanceFTReps") && data["GenericDistanceFTReps"].Answer.IsNotNullOrEmpty() ? " x" + data["GenericDistanceFTReps"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("Management",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericManagement") && data["GenericManagement"].Answer.IsNotNullOrEmpty() ? data["GenericManagement"].Answer.Clean() : string.Empty %><%= data != null && data.ContainsKey("GenericManagementAssist") && data["GenericManagementAssist"].Answer.IsNotNullOrEmpty() ? data["GenericManagementAssist"].Answer.Clean() + "% Assist" : string.Empty %>",0,1)),
        "W/C Training");
    printview.addsection(
        printview.col(5,
            printview.span("Sitting Balance Activities",true) +
            printview.span("Static") +
            printview.span("<%= data != null && data.ContainsKey("GenericSittingBalanceActivitiesStaticAssist") && data["GenericSittingBalanceActivitiesStaticAssist"].Answer.IsNotNullOrEmpty() ? data["GenericSittingBalanceActivitiesStaticAssist"].Answer.Clean() + "% Assist" : string.Empty %>",0,1) +
            printview.span("Dynamic") +
            printview.span("<%= data != null && data.ContainsKey("GenericSittingBalanceActivitiesDynamicAssist") && data["GenericSittingBalanceActivitiesDynamicAssist"].Answer.IsNotNullOrEmpty() ? data["GenericSittingBalanceActivitiesDynamicAssist"].Answer.Clean() + "% Assist" : string.Empty %>",0,1) +
            printview.span("Standing Balance Activities",true) +
            printview.span("Static") +
            printview.span("<%= data != null && data.ContainsKey("GenericStandingBalanceActivitiesStaticAssist") && data["GenericStandingBalanceActivitiesStaticAssist"].Answer.IsNotNullOrEmpty() ? data["GenericStandingBalanceActivitiesStaticAssist"].Answer.Clean() + "% Assist" : string.Empty %>",0,1) +
            printview.span("Dynamic") +
            printview.span("<%= data != null && data.ContainsKey("GenericStandingBalanceActivitiesDynamicAssist") && data["GenericStandingBalanceActivitiesDynamicAssist"].Answer.IsNotNullOrEmpty() ? data["GenericStandingBalanceActivitiesDynamicAssist"].Answer.Clean() + "% Assist" : string.Empty %>",0,1)) +
        printview.checkbox("UE Weight-Bearing Activities",<%= data != null && data.ContainsKey("GenericUEWeightBearing") && data["GenericUEWeightBearing"].Answer == "1" ? "true" : "false"%>),
        "Neuromuscular Reeducation");
    printview.addsection(
        printview.col(4,
            printview.span("ROM",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseROM") && data["GenericTherapeuticExerciseROM"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseROM"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseROMSet") && data["GenericTherapeuticExerciseROMSet"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseROMSet"].Answer.Clean() + "set(s)" : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseROMReps") && data["GenericTherapeuticExerciseROMReps"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseROMReps"].Answer.Clean() + "reps" : string.Empty %>",0,1) +
            printview.span("AROM/AAROM",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseAROMAAROM") && data["GenericTherapeuticExerciseAROMAAROM"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseAROMAAROM"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseAROMAAROMSet") && data["GenericTherapeuticExerciseAROMAAROMSet"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseAROMAAROMSet"].Answer.Clean() + "set(s)" : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseAROMAAROMReps") && data["GenericTherapeuticExerciseAROMAAROMReps"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseAROMAAROMReps"].Answer.Clean() + "reps" : string.Empty %>",0,1) +
            printview.span("Resistive (Type)",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseResistive") && data["GenericTherapeuticExerciseResistive"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseResistive"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseResistiveSet") && data["GenericTherapeuticExerciseResistiveSet"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseResistiveSet"].Answer.Clean() + "set(s)" : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseResistiveReps") && data["GenericTherapeuticExerciseResistiveReps"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseResistiveReps"].Answer.Clean() + "reps" : string.Empty %>",0,1) +
            printview.span("Stretching",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseStretching") && data["GenericTherapeuticExerciseStretching"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseStretching"].Answer.Clean() : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseStretchingSet") && data["GenericTherapeuticExerciseStretchingSet"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseStretchingSet"].Answer.Clean() + "set(s)" : string.Empty %>",0,1) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseStretchingReps") && data["GenericTherapeuticExerciseStretchingReps"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseStretchingReps"].Answer.Clean() + "reps" : string.Empty %>",0,1) +
            printview.span("Other",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTherapeuticExerciseOther") && data["GenericTherapeuticExerciseOther"].Answer.IsNotNullOrEmpty() ? data["GenericTherapeuticExerciseOther"].Answer.Clean() : string.Empty %>",0,1)),
        "Therapeutic Exercise");
    printview.addsection(
        printview.col(3,
            printview.checkbox("Patient/Family",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.checkbox("Caregiver",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("Correct Use of Adaptive Equipment",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("Safety Technique",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("4") ? "true" : "false"%>) +
            printview.checkbox("ADLs",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
            printview.checkbox("HEP",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("6") ? "true" : "false"%>) +
            printview.checkbox("Correct Use of Assistive Device",<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.Split(',').Contains("7") ? "true" : "false"%>) +
            printview.span("Other",true) +
            printview.span("<%= data != null && data.ContainsKey("GenericTeachingOther") && data["GenericTeachingOther"].Answer.IsNotNullOrEmpty() ? data["GenericTeachingOther"].Answer.Clean() : string.Empty %>",0,1)),
        "Teaching");
    printview.addsection(
        printview.span("<%= data != null && data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer.IsNotNullOrEmpty() ? data["GenericAssessment"].Answer.Clean() : string.Empty %>",0,10),
        "Assessment");
    printview.addsection(
        printview.col(3,
            printview.checkbox("Continue Prescribed Plan",<%= data != null && data.ContainsKey("GenericPlan") && data["GenericPlan"].Answer.Split(',').Contains("1") ? "true" : "false"%>) +
            printview.span("Change Prescribed Plan") +
            printview.span("<%= data != null && data.ContainsKey("GenericPlanChangePrescribed") && data["GenericPlanChangePrescribed"].Answer.IsNotNullOrEmpty() ? data["GenericPlanChangePrescribed"].Answer.Clean() : string.Empty %>",0,1) +
            printview.checkbox("Plan Discharge",<%= data != null && data.ContainsKey("GenericPlan") && data["GenericPlan"].Answer.Split(',').Contains("2") ? "true" : "false"%>) +
            printview.checkbox("In Progress",<%= data != null && data.ContainsKey("GenericPlan") && data["GenericPlan"].Answer.Split(',').Contains("3") ? "true" : "false"%>) +
            printview.checkbox("As of Today",<%= data != null && data.ContainsKey("GenericPlan") && data["GenericPlan"].Answer.Split(',').Contains("4") ? "true" : "false"%>)) +
        printview.checkbox("Patient/Family Notified 5 Days Prior to Discharge",<%= data != null && data.ContainsKey("GenericPlan") && data["GenericPlan"].Answer.Split(',').Contains("5") ? "true" : "false"%>) +
        printview.col(2,
            printview.span("Agency Notification 5 Days Prior?",true) +
            printview.col(2,
                printview.checkbox("Yes",<%= data != null && data.ContainsKey("GenericPlanIsAgencyNotification") && data["GenericPlanIsAgencyNotification"].Answer == "1" ? "true" : "false"%>) +
                printview.checkbox("No",<%= data != null && data.ContainsKey("GenericPlanIsAgencyNotification") && data["GenericPlanIsAgencyNotification"].Answer == "0" ? "true" : "false"%>))),
        "Plan"); <%
}).Render(); %>
</html>
