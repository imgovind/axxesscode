﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>();
   string[] medicalDiagnosisOnset = data.ContainsKey("GenericMedicalDiagnosisOnset") && data["GenericMedicalDiagnosisOnset"].Answer != "" ? data["GenericMedicalDiagnosisOnset"].Answer.Split(',') : null;
   string[] ptDiagnosis = data.ContainsKey("GenericOTDiagnosisOnset") && data["GenericOTDiagnosisOnset"].Answer != "" ? data["GenericOTDiagnosisOnset"].Answer.Split(',') : null;
   string[] homeboundStatusAssist = data.ContainsKey("GenericHomeboundStatusAssist") && data["GenericHomeboundStatusAssist"].Answer != "" ? data["GenericHomeboundStatusAssist"].Answer.Split(',') : null;
   string[] genericTreatmentCodes = data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer != "" ? data["GenericTreatmentCodes"].Answer.Split(',') : null;
   string[] genericAssessment = data.ContainsKey("GenericAssessment") && data["GenericAssessment"].Answer != "" ? data["GenericAssessment"].Answer.Split(',') : null; %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="4">Diagnosis</th>
        </tr><tr>
            <td colspan="4">
                <div class="third">
                    <label for="<%= Model.Type %>_GenericMedicalDiagnosis" class="float_left">Medical Diagnosis:</label>
                    <div class="float_right align_right">
                        <%= Html.TextBox(Model.Type + "_GenericMedicalDiagnosis", data.ContainsKey("GenericMedicalDiagnosis") ? data["GenericMedicalDiagnosis"].Answer : string.Empty, new { @id = Model.Type + "_GenericMedicalDiagnosis" }) %><br />
                        <input type="hidden" name="<%= Model.Type %>_GenericMedicalDiagnosisOnset" value="" />
                        <%= string.Format("<input class='radio float_left' id='{1}_GenericMedicalDiagnosisOnset1' name='{1}_GenericMedicalDiagnosisOnset' value='1' type='checkbox' {0} />", medicalDiagnosisOnset != null && medicalDiagnosisOnset.Contains("1") ? "checked='checked'" : "",Model.Type) %>
                        <label for="<%= Model.Type %>_GenericMedicalDiagnosisOnset1" class="float_left radio">Onset</label>
                        <input type="date" name="<%= Model.Type %>_MedicalDiagnosisDate" value="<%= DateTime.Now.ToShortDateString() %>" id=Model.Type + "_MedicalDiagnosisDate", @class = "date shortdate" }) %>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericMedicalDiagnosis" class="float_left">OT Diagnosis:</label>
                    <div class="float_right align_right">
                        <%= Html.TextBox(Model.Type + "_GenericOTDiagnosis", data.ContainsKey("GenericOTDiagnosis") ? data["GenericOTDiagnosis"].Answer : string.Empty, new { @id = Model.Type + "_GenericOTDiagnosis" }) %><br />
                        <input type="hidden" name="<%= Model.Type %>_GenericOTDiagnosisOnset" value="" />
                        <%= string.Format("<input class='radio float_left' id='{1}_GenericOTDiagnosisOnset1' name='{1}_GenericOTDiagnosisOnset' value='1' type='checkbox' {0} />", ptDiagnosis != null && ptDiagnosis.Contains("1") ? "checked='checked'" : "",Model.Type) %>
                        <label for="<%= Model.Type %>_GenericOTDiagnosisOnset1" class="float_left radio">Onset</label>
                        <input type="date" name="<%= Model.Type %>_OTDiagnosisDate" value="<%= DateTime.Now.ToShortDateString() %>" id=Model.Type + "_OTDiagnosisDate", @class = "date shortdate" }) %>
                    </div>
                </div><div class="third">
                    <label class="float_left">Pain</label>
                    <div class="float_right align_right">
                        <label for="<%= Model.Type %>_GenericPainAssessmentLocation">Location:</label>
                        <%= Html.TextBox(Model.Type + "_GenericPainAssessmentLocation", data.ContainsKey("GenericPainAssessmentLocation") ? data["GenericPainAssessmentLocation"].Answer : string.Empty, new { @id = Model.Type + "_GenericPainAssessmentLocation" }) %><br />
                        <label for="<%= Model.Type %>_GenericIntensityOfPain">Level:</label>
                        <%  var GenericIntensityOfPain = new SelectList(new[] {
                                new SelectListItem { Text = "0 = No Pain", Value = "0" },
                                new SelectListItem { Text = "1", Value = "1" },
                                new SelectListItem { Text = "2", Value = "2" },
                                new SelectListItem { Text = "3", Value = "3" },
                                new SelectListItem { Text = "4", Value = "4" },
                                new SelectListItem { Text = "Moderate Pain", Value = "5" },
                                new SelectListItem { Text = "6", Value = "6" },
                                new SelectListItem { Text = "7", Value = "7" },
                                new SelectListItem { Text = "8", Value = "8" },
                                new SelectListItem { Text = "9", Value = "9" },
                                new SelectListItem { Text = "10", Value = "10" }
                            }, "Value", "Text", data.ContainsKey("GenericIntensityOfPain") ? data["GenericIntensityOfPain"].Answer : "0"); %>
                        <%= Html.DropDownList(Model.Type + "_GenericIntensityOfPain", GenericIntensityOfPain, new { @id = Model.Type + "_GenericIntensityOfPain", @class = "oe" }) %>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericPastMedicalHistory" class="float_left">Past Medical/Surgical History:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericPastMedicalHistory", data.ContainsKey("GenericPastMedicalHistory") ? data["GenericPastMedicalHistory"].Answer : string.Empty, new { @id = Model.Type + "_GenericGenericPastMedicalHistory" }) %>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericPriorFunctionalStatus" class="float_left">Prior Functional Status:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericPriorFunctionalStatus", data.ContainsKey("GenericPriorFunctionalStatus") ? data["GenericPriorFunctionalStatus"].Answer : string.Empty, new { @id = Model.Type + "_GenericPriorFunctionalStatus" }) %>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericMentalStatus" class="float_left">Mental Status:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericMentalStatus", data.ContainsKey("GenericMentalStatus") ? data["GenericMentalStatus"].Answer : string.Empty, new { @class = "loc", @id = Model.Type + "_GenericMentalStatus" }) %>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericPrecautions" class="float_left">Precautions:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericPrecautions", data.ContainsKey("GenericPrecautions") ? data["GenericPrecautions"].Answer : string.Empty, new { @class = "loc", @id = Model.Type + "_GenericPrecautions" }) %>
                    </div>
                </div><div class="third">
                    <label class="float_left">Sensation:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericSensation", data.ContainsKey("GenericSensation") ? data["GenericSensation"].Answer : string.Empty, new { @class = "loc", @id = Model.Type + "_GenericSensation" }) %>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericMuscleTone" class="float_left">Muscle Tone:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericMuscleTone", data.ContainsKey("GenericMuscleTone") ? data["GenericMuscleTone"].Answer : string.Empty, new { @class = "loc", @id = Model.Type + "_GenericMuscleTone" }) %>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericEndurance" class="float_left">Endurance:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericEndurance", data.ContainsKey("GenericEndurance") ? data["GenericEndurance"].Answer : string.Empty, new { @class = "loc", @id = Model.Type + "_GenericEndurance" }) %>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericEdema" class="float_left">Edema:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericEdema", data.ContainsKey("GenericEdema") ? data["GenericEdema"].Answer : string.Empty, new { @class = "loc", @id = Model.Type + "_GenericEdema" }) %>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericCoordination" class="float_left">Coordination:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericCoordination", data.ContainsKey("GenericCoordination") ? data["GenericCoordination"].Answer : string.Empty, new { @class = "loc", @id = Model.Type + "_GenericCoordination" }) %>
                    </div>
                </div>
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td colspan="3">
                                <label class="strong">Homebound Status, Requires Assistance With:</label>
                                <input type="hidden" name="<%= Model.Type %>_GenericHomeboundStatusAssist" value="" />
                            </td><td>
                                <%= string.Format("<input class='radio float_left' id='{1}_GenericHomeboundStatusAssist1' name='{1}_GenericHomeboundStatusAssist' value='1' type='checkbox' {0} />", homeboundStatusAssist != null && homeboundStatusAssist.Contains("1") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist1" class="float_left radio">Gait</label></td>
                            <td>
                                <%= string.Format("<input class='radio float_left' id='{1}_GenericHomeboundStatusAssist2' name='{1}_GenericHomeboundStatusAssist' value='2' type='checkbox' {0} />", homeboundStatusAssist != null && homeboundStatusAssist.Contains("2") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist2" class="float_left radio">Leaving the Home</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' id='{1}_GenericHomeboundStatusAssist3' name='{1}_GenericHomeboundStatusAssist' value='3' type='checkbox' {0} />", homeboundStatusAssist != null && homeboundStatusAssist.Contains("3") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist3" class="float_left radio">Transfers</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' id='{1}_GenericHomeboundStatusAssist4' name='{1}_GenericHomeboundStatusAssist' value='4' type='checkbox' {0} />", homeboundStatusAssist != null && homeboundStatusAssist.Contains("4") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericHomeboundStatusAssist4" class="float_left radio">SOB/Endurance</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <label for="<%= Model.Type %>_GenericMedicalHistory" class="float_left">Medical History:</label>
                <div class="clear"></div>
                <%= Html.TextArea(Model.Type + "_GenericMedicalHistory", data.ContainsKey("GenericMedicalHistory") ? data["GenericMedicalHistory"].Answer : string.Empty, new { @id = Model.Type + "_GenericMedicalHistory", @class = "fill" }) %>
            </td>
        </tr><tr>
            <th colspan="2">ADLs/Functional Mobility Level/Level of Assist</th>
            <th colspan="2">Physical Assessment</th>
        </tr><tr>
            <td colspan="2">
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLBathing" class="float_left">Bathing:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericADLBathing", data.ContainsKey("GenericADLBathing") ? data["GenericADLBathing"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLBathing" }) %>
                    </div>
                </div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLBathing" class="float_left">UE Dressing:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericADLUEDressing", data.ContainsKey("GenericADLUEDressing") ? data["GenericADLUEDressing"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLUEDressing" }) %>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLLEDressing" class="float_left">LE Dressing:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericADLLEDressing", data.ContainsKey("GenericADLLEDressing") ? data["GenericADLLEDressing"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLLEDressing" }) %>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLGrooming" class="float_left">Grooming:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericADLGrooming", data.ContainsKey("GenericADLGrooming") ? data["GenericADLGrooming"].Answer : string.Empty, new {  @id = Model.Type + "_GenericADLGrooming" }) %>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLToileting" class="float_left">Toileting:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericADLToileting", data.ContainsKey("GenericADLToileting") ? data["GenericADLToileting"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLToileting" }) %>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLFeeding" class="float_left">Feeding:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericADLFeeding", data.ContainsKey("GenericADLFeeding") ? data["GenericADLFeeding"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLFeeding" }) %>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLMealPrep" class="float_left">Meal Prep:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericADLMealPrep", data.ContainsKey("GenericADLMealPrep") ? data["GenericADLMealPrep"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLMealPrep" }) %>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLHouseCleaning" class="float_left">House Cleaning:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericADLHouseCleaning", data.ContainsKey("GenericADLHouseCleaning") ? data["GenericADLHouseCleaning"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLHouseCleaning" }) %>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericADLAdaptiveEquipment" class="float_left">Adaptive Equipment/Assistive Device Use or Needs:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericADLAdaptiveEquipment", data.ContainsKey("GenericADLAdaptiveEquipment") ? data["GenericADLAdaptiveEquipment"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLAdaptiveEquipment" }) %>
                    </div>
                </div><div class="padnoterow">
                    <label class="float_left">Bed Mobility:</label>
                    <div class="margin">
                        <label for="<%= Model.Type %>_GenericADLBedMobilityRolling" class="float_left">Rolling:</label>
                        <div class="float_right">
                            <%= Html.TextBox(Model.Type + "_GenericADLBedMobilityRolling", data.ContainsKey("GenericADLBedMobilityRolling") ? data["GenericADLBedMobilityRolling"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLBedMobilityRolling" }) %>
                        </div>
                        <div class="clear"></div>
                        <label for="<%= Model.Type %>_GenericADLBedMobilitySupineToSit" class="float_left">Supine to Sit:</label>
                        <div class="float_right">
                            <%= Html.TextBox(Model.Type + "_GenericADLBedMobilitySupineToSit", data.ContainsKey("GenericADLBedMobilitySupineToSit") ? data["GenericADLBedMobilitySupineToSit"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLBedMobilitySupineToSit" }) %>
                        </div>
                        <div class="clear"></div>
                        <label for="<%= Model.Type %>_GenericADLBedMobilitySitToStand" class="float_left">Sit to Stand:</label>
                        <div class="float_right">
                            <%= Html.TextBox(Model.Type + "_GenericADLBedMobilitySitToStand", data.ContainsKey("GenericADLBedMobilitySitToStand") ? data["GenericADLBedMobilitySitToStand"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLBedMobilitySitToStand" }) %>
                        </div>
                    </div>
                </div><div class="padnoterow">
                    <label class="float_left">Transfers:</label>
                    <div class="margin">
                        <label for="<%= Model.Type %>_GenericADLTransfersTubShowerToilet" class="float_left">Tub/Shower/Toilet:</label>
                        <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericADLTransfersTubShowerToilet", data.ContainsKey("GenericADLTransfersTubShowerToilet") ? data["GenericADLTransfersTubShowerToilet"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLTransfersTubShowerToilet" }) %></div>
                        <div class="clear"></div>
                        <label for="<%= Model.Type %>_GenericADLTransfersBedToChairWheelchair" class="float_left">Bed to Chair/Wheelchair:</label><div class="float_right"><%= Html.TextBox(Model.Type + "_GenericADLTransfersBedToChairWheelchair", data.ContainsKey("GenericADLTransfersBedToChairWheelchair") ? data["GenericADLTransfersBedToChairWheelchair"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLTransfersBedToChairWheelchair" }) %></div>
                    </div>
                </div>
                <div class="padnoterow"><label for="<%= Model.Type %>_GenericADLWCMobility" class="float_left">W/C Mobility:</label><div class="float_right"><%= Html.TextBox(Model.Type + "_GenericADLWCMobility", data.ContainsKey("GenericADLWCMobility") ? data["GenericADLWCMobility"].Answer : string.Empty, new { @id = Model.Type + "_GenericADLWCMobility" }) %></div></div>
                <div class="padnoterow"><label class="float_left">Sitting Balance:</label>
                    <div class="margin">
                        <label class="float_left">Static:</label><div class="float_right"><%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceStatic", "2", data.ContainsKey("GenericADLSittingBalanceStatic") && data["GenericADLSittingBalanceStatic"].Answer == "2" ? true : false, new { @id = Model.Type + "_GenericADLSittingBalanceStatic2", @class = "radio" }) %><label for="<%= Model.Type %>_GenericADLSittingBalanceStatic2" class="inlineradio">Good</label><%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceStatic", "1", data.ContainsKey("GenericADLSittingBalanceStatic") && data["GenericADLSittingBalanceStatic"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericADLSittingBalanceStatic1", @class = "radio" }) %><label for="<%= Model.Type %>_GenericADLSittingBalanceStatic1" class="inlineradio">Fair</label><%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceStatic", "0", data.ContainsKey("GenericADLSittingBalanceStatic") && data["GenericADLSittingBalanceStatic"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericADLSittingBalanceStatic0", @class = "radio" }) %><label for="<%= Model.Type %>_GenericADLSittingBalanceStatic0" class="inlineradio">Poor</label></div>
                        <div class="clear"></div>
                        <label class="float_left">Dynamic:</label>
                        <div class="float_right">
                            <%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceDynamic", "2", data.ContainsKey("GenericADLSittingBalanceDynamic") && data["GenericADLSittingBalanceDynamic"].Answer == "2" ? true : false, new { @id = Model.Type + "_GenericADLSittingBalanceDynamic2", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericADLSittingBalanceDynamic2" class="inlineradio">Good</label>
                            <%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceDynamic", "1", data.ContainsKey("GenericADLSittingBalanceDynamic") && data["GenericADLSittingBalanceDynamic"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericADLSittingBalanceDynamic1", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericADLSittingBalanceDynamic1" class="inlineradio">Fair</label>
                            <%= Html.RadioButton(Model.Type + "_GenericADLSittingBalanceDynamic", "0", data.ContainsKey("GenericADLSittingBalanceDynamic") && data["GenericADLSittingBalanceDynamic"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericADLSittingBalanceDynamic0", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericADLSittingBalanceDynamic0" class="inlineradio">Poor</label>
                        </div>
                    </div>
                </div><div class="padnoterow">
                    <label class="float_left">Standing Balance:</label>
                    <div class="margin">
                        <label class="float_left">Static:</label>
                        <div class="float_right">
                            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceStatic", "2", data.ContainsKey("GenericADLStandingBalanceStatic") && data["GenericADLStandingBalanceStatic"].Answer == "2" ? true : false, new { @id = Model.Type + "_GenericADLStandingBalanceStatic2", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericADLStandingBalanceStatic2" class="inlineradio">Good</label>
                            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceStatic", "1", data.ContainsKey("GenericADLStandingBalanceStatic") && data["GenericADLStandingBalanceStatic"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericADLStandingBalanceStatic1", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericADLStandingBalanceStatic1" class="inlineradio">Fair</label>
                            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceStatic", "0", data.ContainsKey("GenericADLStandingBalanceStatic") && data["GenericADLStandingBalanceStatic"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericADLStandingBalanceStatic0", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericADLStandingBalanceStatic0" class="inlineradio">Poor</label>
                        </div>
                        <div class="clear"></div>
                        <label class="float_left">Dynamic:</label>
                        <div class="float_right">
                            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceDynamic", "2", data.ContainsKey("GenericADLStandingBalanceDynamic") && data["GenericADLStandingBalanceDynamic"].Answer == "2" ? true : false, new { @id = Model.Type + "_GenericADLStandingBalanceDynamic2", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericADLStandingBalanceDynamic2" class="inlineradio">Good</label>
                            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceDynamic", "1", data.ContainsKey("GenericADLStandingBalanceDynamic") && data["GenericADLStandingBalanceDynamic"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericADLStandingBalanceDynamic1", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericADLStandingBalanceDynamic1" class="inlineradio">Fair</label>
                            <%= Html.RadioButton(Model.Type + "_GenericADLStandingBalanceDynamic", "0", data.ContainsKey("GenericADLStandingBalanceDynamic") && data["GenericADLStandingBalanceDynamic"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericADLStandingBalanceDynamic0", @class = "radio" }) %>
                            <label for="<%= Model.Type %>_GenericADLStandingBalanceDynamic0" class="inlineradio">Poor</label>
                        </div>
                    </div>
                </div>
            </td>
            <td colspan="2" rowspan="3">
                <table>
                    <thead class="strong">
                        <tr>
                            <th colspan="2"></th>
                            <th colspan="2">ROM</th>
                            <th colspan="2">Strength</th>
                        </tr><tr>
                            <th class="align_left">Part</th>
                            <th class="align_left">Action</th>
                            <th>Right</th>
                            <th>Left</th>
                            <th>Right</th>
                            <th>Left</th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td class="align_left strong">
                                Shoulder
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderFlexionROMRight", data.ContainsKey("GenericShoulderFlexionROMRight") ? data["GenericShoulderFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderFlexionROMLeft", data.ContainsKey("GenericShoulderFlexionROMLeft") ? data["GenericShoulderFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderFlexionStrengthRight", data.ContainsKey("GenericShoulderFlexionStrengthRight") ? data["GenericShoulderFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderFlexionStrengthLeft", data.ContainsKey("GenericShoulderFlexionStrengthLeft") ? data["GenericShoulderFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtensionROMRight", data.ContainsKey("GenericShoulderExtensionROMRight") ? data["GenericShoulderExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtensionROMLeft", data.ContainsKey("GenericShoulderExtensionROMLeft") ? data["GenericShoulderExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtensionStrengthRight", data.ContainsKey("GenericShoulderExtensionStrengthRight") ? data["GenericShoulderExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtensionStrengthLeft", data.ContainsKey("GenericShoulderExtensionStrengthLeft") ? data["GenericShoulderExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Abduction
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderAbductionROMRight", data.ContainsKey("GenericShoulderAbductionROMRight") ? data["GenericShoulderAbductionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderAbductionROMLeft", data.ContainsKey("GenericShoulderAbductionROMLeft") ? data["GenericShoulderAbductionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderAbductionStrengthRight", data.ContainsKey("GenericShoulderAbductionStrengthRight") ? data["GenericShoulderAbductionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderAbductionStrengthLeft", data.ContainsKey("GenericShoulderAbductionStrengthLeft") ? data["GenericShoulderAbductionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Int Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderIntRotROMRight", data.ContainsKey("GenericShoulderIntRotROMRight") ? data["GenericShoulderIntRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderIntRotROMLeft", data.ContainsKey("GenericShoulderIntRotROMLeft") ? data["GenericShoulderIntRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderIntRotStrengthRight", data.ContainsKey("GenericShoulderIntRotStrengthRight") ? data["GenericShoulderIntRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderIntRotStrengthLeft", data.ContainsKey("GenericShoulderIntRotStrengthLeft") ? data["GenericShoulderIntRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Ext Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtRotROMRight", data.ContainsKey("GenericShoulderExtRotROMRight") ? data["GenericShoulderExtRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtRotROMLeft", data.ContainsKey("GenericShoulderExtRotROMLeft") ? data["GenericShoulderExtRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtRotStrengthRight", data.ContainsKey("GenericShoulderExtRotStrengthRight") ? data["GenericShoulderExtRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtRotStrengthLeft", data.ContainsKey("GenericShoulderExtRotStrengthLeft") ? data["GenericShoulderExtRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left strong">
                                Elbow
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowFlexionROMRight", data.ContainsKey("GenericElbowFlexionROMRight") ? data["GenericElbowFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowFlexionROMLeft", data.ContainsKey("GenericElbowFlexionROMLeft") ? data["GenericElbowFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowFlexionStrengthRight", data.ContainsKey("GenericElbowFlexionStrengthRight") ? data["GenericElbowFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowFlexionStrengthLeft", data.ContainsKey("GenericElbowFlexionStrengthLeft") ? data["GenericElbowFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowExtensionROMRight", data.ContainsKey("GenericElbowExtensionROMRight") ? data["GenericElbowExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowExtensionROMLeft", data.ContainsKey("GenericElbowExtensionROMLeft") ? data["GenericElbowExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowExtensionStrengthRight", data.ContainsKey("GenericElbowExtensionStrengthRight") ? data["GenericElbowExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowExtensionStrengthLeft", data.ContainsKey("GenericElbowExtensionStrengthLeft") ? data["GenericElbowExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left strong">
                                Finger
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerFlexionROMRight", data.ContainsKey("GenericFingerFlexionROMRight") ? data["GenericFingerFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerFlexionROMLeft", data.ContainsKey("GenericFingerFlexionROMLeft") ? data["GenericFingerFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerFlexionStrengthRight", data.ContainsKey("GenericFingerFlexionStrengthRight") ? data["GenericFingerFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerFlexionStrengthLeft", data.ContainsKey("GenericFingerFlexionStrengthLeft") ? data["GenericFingerFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerExtensionROMRight", data.ContainsKey("GenericFingerExtensionROMRight") ? data["GenericFingerExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerExtensionROMLeft", data.ContainsKey("GenericFingerExtensionROMLeft") ? data["GenericFingerExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerExtensionStrengthRight", data.ContainsKey("GenericFingerExtensionStrengthRight") ? data["GenericFingerExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerExtensionStrengthLeft", data.ContainsKey("GenericFingerExtensionStrengthLeft") ? data["GenericFingerExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left strong">
                                Wrist
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristFlexionROMRight", data.ContainsKey("GenericWristFlexionROMRight") ? data["GenericWristFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristFlexionROMLeft", data.ContainsKey("GenericWristFlexionROMLeft") ? data["GenericWristFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristFlexionStrengthRight", data.ContainsKey("GenericWristFlexionStrengthRight") ? data["GenericWristFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristFlexionStrengthLeft", data.ContainsKey("GenericWristFlexionStrengthLeft") ? data["GenericWristFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristExtensionROMRight", data.ContainsKey("GenericWristExtensionROMRight") ? data["GenericWristExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristExtensionROMLeft", data.ContainsKey("GenericWristExtensionROMLeft") ? data["GenericWristExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristExtensionStrengthRight", data.ContainsKey("GenericWristExtensionStrengthRight") ? data["GenericWristExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristExtensionStrengthLeft", data.ContainsKey("GenericWristExtensionStrengthLeft") ? data["GenericWristExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left strong">
                                Hip
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipFlexionROMRight", data.ContainsKey("GenericHipFlexionROMRight") ? data["GenericHipFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipFlexionROMLeft", data.ContainsKey("GenericHipFlexionROMLeft") ? data["GenericHipFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipFlexionStrengthRight", data.ContainsKey("GenericHipFlexionStrengthRight") ? data["GenericHipFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipFlexionStrengthLeft", data.ContainsKey("GenericHipFlexionStrengthLeft") ? data["GenericHipFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtensionROMRight", data.ContainsKey("GenericHipExtensionROMRight") ? data["GenericHipExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtensionROMLeft", data.ContainsKey("GenericHipExtensionROMLeft") ? data["GenericHipExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtensionStrengthRight", data.ContainsKey("GenericHipExtensionStrengthRight") ? data["GenericHipExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtensionStrengthLeft", data.ContainsKey("GenericHipExtensionStrengthLeft") ? data["GenericHipExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Abduction
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipAbductionROMRight", data.ContainsKey("GenericHipAbductionROMRight") ? data["GenericHipAbductionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipAbductionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipAbductionROMLeft", data.ContainsKey("GenericHipAbductionROMLeft") ? data["GenericHipAbductionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipAbductionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipAbductionStrengthRight", data.ContainsKey("GenericHipAbductionStrengthRight") ? data["GenericHipAbductionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipAbductionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipAbductionStrengthLeft", data.ContainsKey("GenericHipAbductionStrengthLeft") ? data["GenericHipAbductionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipAbductionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Int Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipIntRotROMRight", data.ContainsKey("GenericHipIntRotROMRight") ? data["GenericHipIntRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipIntRotROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipIntRotROMLeft", data.ContainsKey("GenericHipIntRotROMLeft") ? data["GenericHipIntRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipIntRotROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipIntRotStrengthRight", data.ContainsKey("GenericHipIntRotStrengthRight") ? data["GenericHipIntRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipIntRotStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipIntRotStrengthLeft", data.ContainsKey("GenericHipIntRotStrengthLeft") ? data["GenericHipIntRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipIntRotStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Ext Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtRotROMRight", data.ContainsKey("GenericHipExtRotROMRight") ? data["GenericHipExtRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtRotROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtRotROMLeft", data.ContainsKey("GenericHipExtRotROMLeft") ? data["GenericHipExtRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtRotROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtRotStrengthRight", data.ContainsKey("GenericHipExtRotStrengthRight") ? data["GenericHipExtRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtRotStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtRotStrengthLeft", data.ContainsKey("GenericHipExtRotStrengthLeft") ? data["GenericHipExtRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtRotStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left strong">
                                Knee
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeFlexionROMRight", data.ContainsKey("GenericKneeFlexionROMRight") ? data["GenericKneeFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeFlexionROMLeft", data.ContainsKey("GenericKneeFlexionROMLeft") ? data["GenericKneeFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeFlexionStrengthRight", data.ContainsKey("GenericKneeFlexionStrengthRight") ? data["GenericKneeFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeFlexionStrengthLeft", data.ContainsKey("GenericKneeFlexionStrengthLeft") ? data["GenericKneeFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeExtensionROMRight", data.ContainsKey("GenericKneeExtensionROMRight") ? data["GenericKneeExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeExtensionROMLeft", data.ContainsKey("GenericKneeExtensionROMLeft") ? data["GenericKneeExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeExtensionStrengthRight", data.ContainsKey("GenericKneeExtensionStrengthRight") ? data["GenericKneeExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeExtensionStrengthLeft", data.ContainsKey("GenericKneeExtensionStrengthLeft") ? data["GenericKneeExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left strong">
                                Ankle
                            </td><td class="align_left">
                                Plantarflexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnklePlantFlexionROMRight", data.ContainsKey("GenericAnklePlantFlexionROMRight") ? data["GenericAnklePlantFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnklePlantFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnklePlantFlexionROMLeft", data.ContainsKey("GenericAnklePlantFlexionROMLeft") ? data["GenericAnklePlantFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnklePlantFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnklePlantFlexionStrengthRight", data.ContainsKey("GenericAnklePlantFlexionStrengthRight") ? data["GenericAnklePlantFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnklePlantFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnklePlantFlexionStrengthLeft", data.ContainsKey("GenericAnklePlantFlexionStrengthLeft") ? data["GenericAnklePlantFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnklePlantFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Dorsiflexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnkleFlexionROMRight", data.ContainsKey("GenericAnkleFlexionROMRight") ? data["GenericAnkleFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnkleFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnkleFlexionROMLeft", data.ContainsKey("GenericAnkleFlexionROMLeft") ? data["GenericAnkleFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnkleFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnkleFlexionStrengthRight", data.ContainsKey("GenericAnkleFlexionStrengthRight") ? data["GenericAnkleFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnkleFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnkleFlexionStrengthLeft", data.ContainsKey("GenericAnkleFlexionStrengthLeft") ? data["GenericAnkleFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnkleFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left strong">
                                Trunk
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkFlexionROMRight", data.ContainsKey("GenericTrunkFlexionROMRight") ? data["GenericTrunkFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkFlexionROMLeft", data.ContainsKey("GenericTrunkFlexionROMLeft") ? data["GenericTrunkFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkFlexionStrengthRight", data.ContainsKey("GenericTrunkFlexionStrengthRight") ? data["GenericTrunkFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkFlexionStrengthLeft", data.ContainsKey("GenericTrunkFlexionStrengthLeft") ? data["GenericTrunkFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Rotation
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkRotationROMRight", data.ContainsKey("GenericTrunkRotationROMRight") ? data["GenericTrunkRotationROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkRotationROMLeft", data.ContainsKey("GenericTrunkRotationROMLeft") ? data["GenericTrunkRotationROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkRotationStrengthRight", data.ContainsKey("GenericTrunkRotationStrengthRight") ? data["GenericTrunkRotationStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkRotationStrengthLeft", data.ContainsKey("GenericTrunkRotationStrengthLeft") ? data["GenericTrunkRotationStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkExtensionROMRight", data.ContainsKey("GenericTrunkExtensionROMRight") ? data["GenericTrunkExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkExtensionROMLeft", data.ContainsKey("GenericTrunkExtensionROMLeft") ? data["GenericTrunkExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkExtensionStrengthRight", data.ContainsKey("GenericTrunkExtensionStrengthRight") ? data["GenericTrunkExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkExtensionStrengthLeft", data.ContainsKey("GenericTrunkExtensionStrengthLeft") ? data["GenericTrunkExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left strong">
                                Neck
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckFlexionROMRight", data.ContainsKey("GenericNeckFlexionROMRight") ? data["GenericNeckFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckFlexionROMLeft", data.ContainsKey("GenericNeckFlexionROMLeft") ? data["GenericNeckFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckFlexionStrengthRight", data.ContainsKey("GenericNeckFlexionStrengthRight") ? data["GenericNeckFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckFlexionStrengthLeft", data.ContainsKey("GenericNeckFlexionStrengthLeft") ? data["GenericNeckFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckExtensionROMRight", data.ContainsKey("GenericNeckExtensionROMRight") ? data["GenericNeckExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckExtensionROMLeft", data.ContainsKey("GenericNeckExtensionROMLeft") ? data["GenericNeckExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckExtensionStrengthRight", data.ContainsKey("GenericNeckExtensionStrengthRight") ? data["GenericNeckExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckExtensionStrengthLeft", data.ContainsKey("GenericNeckExtensionStrengthLeft") ? data["GenericNeckExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Lat Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionROMRight", data.ContainsKey("GenericNeckLatFlexionROMRight") ? data["GenericNeckLatFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionROMLeft", data.ContainsKey("GenericNeckLatFlexionROMLeft") ? data["GenericNeckLatFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionStrengthRight", data.ContainsKey("GenericNeckLatFlexionStrengthRight") ? data["GenericNeckLatFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionStrengthLeft", data.ContainsKey("GenericNeckLatFlexionStrengthLeft") ? data["GenericNeckLatFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Long Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionROMRight", data.ContainsKey("GenericNeckLongFlexionROMRight") ? data["GenericNeckLongFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionROMLeft", data.ContainsKey("GenericNeckLongFlexionROMLeft") ? data["GenericNeckLongFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionStrengthRight", data.ContainsKey("GenericNeckLongFlexionStrengthRight") ? data["GenericNeckLongFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionStrengthLeft", data.ContainsKey("GenericNeckLongFlexionStrengthLeft") ? data["GenericNeckLongFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Rotation
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckRotationROMRight", data.ContainsKey("GenericNeckRotationROMRight") ? data["GenericNeckRotationROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckRotationROMLeft", data.ContainsKey("GenericNeckRotationROMLeft") ? data["GenericNeckRotationROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckRotationStrengthRight", data.ContainsKey("GenericNeckRotationStrengthRight") ? data["GenericNeckRotationStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckRotationStrengthLeft", data.ContainsKey("GenericNeckRotationStrengthLeft") ? data["GenericNeckRotationStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationStrengthLeft" }) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><th colspan="2">Treatment Codes</th></tr>
        <tr>
            <td colspan="2">
                <input type="hidden" name="<%= Model.Type %>_GenericTreatmentCodes" value="" />
                <table class="fixed align_left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes1' class='radio' name='{1}_GenericTreatmentCodes' value='1' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("1") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes1">B1 Evaluation</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes2' class='radio' name='{1}_GenericTreatmentCodes' value='2' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("2") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes2">B2 Thera Ex</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes3' class='radio' name='{1}_GenericTreatmentCodes' value='3' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("3") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes3">B3 Transfer Training</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes4' class='radio' name='{1}_GenericTreatmentCodes' value='4' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("4") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes4">B4 Home Program</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes5' class='radio' name='{1}_GenericTreatmentCodes' value='5' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("5") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes5">B5 Gait Training</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes6' class='radio' name='{1}_GenericTreatmentCodes' value='6' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("6") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes6">B6 Chest PT</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes7' class='radio' name='{1}_GenericTreatmentCodes' value='7' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("7") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes7">B7 Ultrasound</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes8' class='radio' name='{1}_GenericTreatmentCodes' value='8' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("8") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes8">B8 Electrother</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes9' class='radio' name='{1}_GenericTreatmentCodes' value='9' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("9") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes9">B9 Prosthetic Training</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes10' class='radio' name='{1}_GenericTreatmentCodes' value='10' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("10") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes10">B10 Muscle Re-ed</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes11' class='radio' name='{1}_GenericTreatmentCodes' value='11' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("11") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes11">B11 Muscle Re-ed</label>
                            </td><td>
                                <label class="float_left">Other</label>
                                <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericTreatmentCodesOther", data.ContainsKey("GenericTreatmentCodesOther") ? data["GenericTreatmentCodesOther"].Answer : string.Empty, new { @id = Model.Type + "_GenericTreatmentCodesOther" }) %></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><th colspan="4">Assessment</th></tr>
        <tr>
            <td colspan="4">
                <input type="hidden" name="<%= Model.Type %>_GenericAssessment" value="" />
                <table class="fixed align_left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericAssessment1' class='radio' name='{1}_GenericAssessment' value='1' type='checkbox' {0} />", genericAssessment != null && genericAssessment.Contains("1") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment1">Decreased Strength</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericAssessment2' class='radio' name='{1}_GenericAssessment' value='2' type='checkbox' {0} />", genericAssessment != null && genericAssessment.Contains("2") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment2">Decreased ROM</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericAssessment3' class='radio' name='{1}_GenericAssessment' value='3' type='checkbox' {0} />", genericAssessment != null && genericAssessment.Contains("3") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment3">Decreased ADL</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericAssessment4' class='radio' name='{1}_GenericAssessment' value='4' type='checkbox' {0} />", genericAssessment != null && genericAssessment.Contains("4") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment4">Ind</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericAssessment5' class='radio' name='{1}_GenericAssessment' value='5' type='checkbox' {0} />", genericAssessment != null && genericAssessment.Contains("5") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment5">Decreased Mobility</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericAssessment6' class='radio' name='{1}_GenericAssessment' value='6' type='checkbox' {0} />", genericAssessment != null && genericAssessment.Contains("6") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment6">Pain</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericAssessment7' class='radio' name='{1}_GenericAssessment' value='7' type='checkbox' {0} />", genericAssessment != null && genericAssessment.Contains("7") ? "checked='checked'" : "", Model.Type) %>
                                <label for="<%= Model.Type %>_GenericAssessment7">Decreased Endurance</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="third">
                    <label for="<%= Model.Type %>_GenericRehabPotential" class="float_left">Rehab Potential</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericRehabPotential", data.ContainsKey("GenericRehabPotential") ? data["GenericRehabPotential"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericRehabPotential" }) %>
                    </div>
                </div><div class="third">
                    <label for="<%= Model.Type %>_GenericPrognosis" class="float_left">Prognosis</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericPrognosis", data.ContainsKey("GenericPrognosis") ? data["GenericPrognosis"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPrognosis" }) %>
                    </div>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericAssessmentMore" class="float_left">Comments:</label>
                <div class="clear"></div>
                <%= Html.TextArea(Model.Type + "_GenericAssessmentMore", data.ContainsKey("GenericAssessmentMore") ? data["GenericAssessmentMore"].Answer : string.Empty, new { @id = Model.Type + "_GenericAssessmentMore", @class = "fill" }) %>
            </td>
        </tr><tr>
            <th colspan="2">Short Term Goals</th>
            <th colspan="2">Long Term Goals</th>
        </tr><tr>
            <td colspan="2">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2"></th>
                            <th class="strong">Time Frame</th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td>
                                1.
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShortTermGoal1", data.ContainsKey("GenericShortTermGoal1") ? data["GenericShortTermGoal1"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal1" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShortTermGoal1TimeFrame", data.ContainsKey("GenericShortTermGoal1TimeFrame") ? data["GenericShortTermGoal1TimeFrame"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal1TimeFrame" }) %>
                            </td>
                        </tr><tr>
                            <td>
                                2.
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShortTermGoal2", data.ContainsKey("GenericShortTermGoal2") ? data["GenericShortTermGoal2"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal2" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShortTermGoal2TimeFrame", data.ContainsKey("GenericShortTermGoal2TimeFrame") ? data["GenericShortTermGoal2TimeFrame"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal2TimeFrame" })%>
                            </td>
                        </tr><tr>
                            <td>
                                3.
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShortTermGoal3", data.ContainsKey("GenericShortTermGoal3") ? data["GenericShortTermGoal3"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal3" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShortTermGoal3TimeFrame", data.ContainsKey("GenericShortTermGoal3TimeFrame") ? data["GenericShortTermGoal3TimeFrame"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal3TimeFrame" })%>
                            </td>
                        </tr><tr>
                            <td>
                                4.
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShortTermGoal4", data.ContainsKey("GenericShortTermGoal4") ? data["GenericShortTermGoal4"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal4" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShortTermGoal4TimeFrame", data.ContainsKey("GenericShortTermGoal4TimeFrame") ? data["GenericShortTermGoal4TimeFrame"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal4TimeFrame" })%>
                            </td>
                        </tr><tr>
                            <td>
                                5.
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShortTermGoal5", data.ContainsKey("GenericShortTermGoal5") ? data["GenericShortTermGoal5"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal5" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShortTermGoal5TimeFrame", data.ContainsKey("GenericShortTermGoal5TimeFrame") ? data["GenericShortTermGoal5TimeFrame"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericShortTermGoal5TimeFrame" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericShortTermFrequency" class="float_left">Frequency:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericShortTermFrequency", data.ContainsKey("GenericShortTermFrequency") ? data["GenericShortTermFrequency"].Answer : string.Empty, new { @id = Model.Type + "_GenericShortTermFrequency" }) %>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericShortTermDuration" class="float_left">Duration:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericShortTermDuration", data.ContainsKey("GenericShortTermDuration") ? data["GenericShortTermDuration"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermDuration" }) %>
                    </div>
                </div>
            </td>
            <td colspan="2">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2"></th>
                            <th class="strong">Time Frame</th>
                        </tr>
                    </thead><tbody>
                        <tr>
                            <td>
                                1.
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericLongTermGoal1", data.ContainsKey("GenericLongTermGoal1") ? data["GenericLongTermGoal1"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal1" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericLongTermGoal1TimeFrame", data.ContainsKey("GenericLongTermGoal1TimeFrame") ? data["GenericLongTermGoal1TimeFrame"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal1TimeFrame" })%>
                            </td>
                        </tr><tr>
                            <td>
                                2.
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericLongTermGoal2", data.ContainsKey("GenericLongTermGoal2") ? data["GenericLongTermGoal2"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal2" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericLongTermGoal2TimeFrame", data.ContainsKey("GenericLongTermGoal2TimeFrame") ? data["GenericLongTermGoal2TimeFrame"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal2TimeFrame" })%>
                            </td>
                        </tr><tr>
                            <td>
                                3.
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericLongTermGoal3", data.ContainsKey("GenericLongTermGoal3") ? data["GenericLongTermGoal3"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal3" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericLongTermGoal3TimeFrame", data.ContainsKey("GenericLongTermGoal3TimeFrame") ? data["GenericLongTermGoal3TimeFrame"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal3TimeFrame" })%>
                            </td>
                        </tr><tr>
                            <td>
                                4.
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericLongTermGoal4", data.ContainsKey("GenericLongTermGoal4") ? data["GenericLongTermGoal4"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal4" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericLongTermGoal4TimeFrame", data.ContainsKey("GenericLongTermGoal4TimeFrame") ? data["GenericLongTermGoal4TimeFrame"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal4TimeFrame" })%>
                            </td>
                        </tr><tr>
                            <td>
                                5.
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericLongTermGoal5", data.ContainsKey("GenericLongTermGoal5") ? data["GenericLongTermGoal5"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal5" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericLongTermGoal5TimeFrame", data.ContainsKey("GenericLongTermGoal5TimeFrame") ? data["GenericLongTermGoal5TimeFrame"].Answer : string.Empty, new { @class = "fill", @id = Model.Type + "_GenericLongTermGoal5TimeFrame" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericLongTermFrequency" class="float_left">Frequency:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericLongTermFrequency", data.ContainsKey("GenericLongTermFrequency") ? data["GenericLongTermFrequency"].Answer : string.Empty, new { @id = Model.Type + "_GenericLongTermFrequency" })%>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericLongTermDuration" class="float_left">Duration:</label>
                    <div class="float_right">
                        <%= Html.TextBox(Model.Type + "_GenericLongTermDuration", data.ContainsKey("GenericLongTermDuration") ? data["GenericLongTermDuration"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermDuration" }) %>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>