﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>();
   string[] genericHomeboundReason = data.ContainsKey("GenericHomeboundReason") && data["GenericHomeboundReason"].Answer != "" ? data["GenericHomeboundReason"].Answer.Split(',') : null;
   string[] liquids = data.ContainsKey("GenericLiquids") && data["GenericLiquids"].Answer != "" ? data["GenericLiquids"].Answer.Split(',') : null;
   string[] genericReferralFor = data.ContainsKey("GenericReferralFor") && data["GenericReferralFor"].Answer != "" ? data["GenericReferralFor"].Answer.Split(',') : null;
   string[] genericPlanOfCare = data.ContainsKey("GenericPlanOfCare") && data["GenericPlanOfCare"].Answer != "" ? data["GenericPlanOfCare"].Answer.Split(',') : null;
   string[] genericDischargeDiscussedWith = data.ContainsKey("GenericDischargeDiscussedWith") && data["GenericDischargeDiscussedWith"].Answer != "" ? data["GenericDischargeDiscussedWith"].Answer.Split(',') : null;
   string[] genericCareCoordination = data.ContainsKey("GenericCareCoordination") && data["GenericCareCoordination"].Answer != "" ? data["GenericCareCoordination"].Answer.Split(',') : null;
%>
   
<table class="fixed nursing">
    <tbody>
        <tr><th colspan="3">Diagnosis</th></tr>
        <tr>
            <td colspan="3">
                <input type="hidden" name="<%= Model.Type %>_GenericHomeboundReason" value="" />
                <table class="fixed align_left">
                    <tbody>
                        <tr class="border-bottom">
                            <td>
                                <label for="<%= Model.Type %>_GenericEvaluationType" class="float_left">Evaluation Type</label>
                            </td><td>
                                <%= Html.RadioButton(Model.Type + "_GenericEvaluationType", "2", data.ContainsKey("GenericEvaluationType") && data["GenericEvaluationType"].Answer == "2" ? true : false, new { @id = Model.Type + "_GenericEvaluationType2", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericEvaluationType2" class="inlineradio">Initial</label>
                            </td><td>
                                <%= Html.RadioButton(Model.Type + "_GenericEvaluationType", "1", data.ContainsKey("GenericEvaluationType") && data["GenericEvaluationType"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericEvaluationType1", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericEvaluationType1" class="inlineradio">Interim</label>
                            </td><td>
                                <%= Html.RadioButton(Model.Type + "_GenericEvaluationType", "0", data.ContainsKey("GenericEvaluationType") && data["GenericEvaluationType"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericEvaluationType0", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericEvaluationType0" class="inlineradio">Final</label>
                            </td>
                        </tr><tr>
                            <td>
                                <label class="float_left">Homebound Reason</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason1' class='radio' name='{1}_GenericHomeboundReason' value='1' type='checkbox' {0} />", genericHomeboundReason != null && genericHomeboundReason.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason1">Needs assistance for all activities</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason2' class='radio' name='{1}_GenericHomeboundReason' value='2' type='checkbox' {0} />", genericHomeboundReason != null && genericHomeboundReason.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason2">Residual weakness</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason3' class='radio' name='{1}_GenericHomeboundReason' value='3' type='checkbox' {0} />", genericHomeboundReason != null && genericHomeboundReason.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason3">Requires assistance to ambulate</label>
                            </td>
                        </tr><tr>
                            <td></td><td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason4' class='radio' name='{1}_GenericHomeboundReason' value='4' type='checkbox' {0} />", genericHomeboundReason != null && genericHomeboundReason.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason4">Confusion, unable to go out of home alone</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason5' class='radio' name='{1}_GenericHomeboundReason' value='5' type='checkbox' {0} />", genericHomeboundReason != null && genericHomeboundReason.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason5">Unable to safely leave home unassisted</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason6' class='radio' name='{1}_GenericHomeboundReason' value='6' type='checkbox' {0} />", genericHomeboundReason != null && genericHomeboundReason.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason6">Severe SOB, SOB upon exertion</label>
                            </td>
                        </tr><tr class="border-bottom">
                            <td></td><td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason7' class='radio' name='{1}_GenericHomeboundReason' value='7' type='checkbox' {0} />", genericHomeboundReason != null && genericHomeboundReason.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason7">Unable to safely leave home unassisted</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason8' class='radio' name='{1}_GenericHomeboundReason' value='8' type='checkbox' {0} />", genericHomeboundReason != null && genericHomeboundReason.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason8">Medical Restrictions</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericHomeboundReason9' class='radio' name='{1}_GenericHomeboundReason' value='9' type='checkbox' {0} />", genericHomeboundReason != null && genericHomeboundReason.Contains("9") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericHomeboundReason9">Other:</label>
                                <%= Html.TextBox(Model.Type+"_GenericHomeboundReasonOther", data.ContainsKey("GenericHomeboundReasonOther") ? data["GenericHomeboundReasonOther"].Answer : string.Empty, new { @class = "float_right", @id = Model.Type+"_GenericHomeboundReasonOther" })%>
                            </td>
                        </tr><tr class="border-bottom">
                            <td><label class="float_left">Orders for Evaluation Only?</label></td>
                            <td class="align_right">
                                <%= Html.RadioButton(Model.Type + "_GenericOrdersForEvaluationOnly", "1", data.ContainsKey("GenericOrdersForEvaluationOnly") && data["GenericOrdersForEvaluationOnly"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericOrdersForEvaluationOnly1", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericOrdersForEvaluationOnly1" class="inlineradio">Yes</label>
                                <%= Html.RadioButton(Model.Type + "_GenericOrdersForEvaluationOnly", "0", data.ContainsKey("GenericOrdersForEvaluationOnly") && data["GenericOrdersForEvaluationOnly"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericOrdersForEvaluationOnly0", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericOrdersForEvaluationOnly0" class="inlineradio">No</label>
                                <span id="<%= Model.Type %>_GenericIfNoOrdersAreSpan">
                                    <br />
                                    <label for="<%= Model.Type %>_GenericIfNoOrdersAre">If No, orders are</label>
                                    <%= Html.TextBox(Model.Type + "_GenericIfNoOrdersAre", data.ContainsKey("GenericIfNoOrdersAre") ? data["GenericIfNoOrdersAre"].Answer : string.Empty, new { @id = Model.Type + "_GenericIfNoOrdersAre" })%>
                                </span>
                            </td><td><label for="<%= Model.Type %>_GenericMedicalDiagnosis" class="float_left">Medical Diagnosis/Treatment Diagnosis:</label></td>
                            <td class="align_right">
                                <%= Html.TextArea(Model.Type + "_GenericMedicalDiagnosis", data.ContainsKey("GenericMedicalDiagnosis") ? data["GenericMedicalDiagnosis"].Answer : string.Empty, new { @id = Model.Type + "_GenericMedicalDiagnosis", @class = "fill" })%><br />
                                <label for="<%= Model.Type %>_GenericMedicalDiagnosisOnsetDate">Onset</label>
                                <input type="date" name="<%= Model.Type %>_GenericMedicalDiagnosisOnsetDate" value="<%= data.ContainsKey("GenericMedicalDiagnosisOnsetDate") && data["GenericMedicalDiagnosisOnsetDate"].Answer.IsValidDate() ? data["GenericMedicalDiagnosisOnsetDate"].Answer : "" %>" id="<%= Model.Type %>_GenericMedicalDiagnosisOnsetDate", @class = "date shortdate" })%>
                            </td>
                        </tr><tr class="border-bottom">
                            <td><label for="<%= Model.Type %>_GenericMedicalPrecautions" class="float_left">Medical Precautions:</label></td>
                            <td class="align_right"><%= Html.TextArea(Model.Type + "_GenericMedicalPrecautions", data.ContainsKey("GenericMedicalPrecautions") ? data["GenericMedicalPrecautions"].Answer : string.Empty, new { @id = Model.Type + "_GenericMedicalPrecautions", @class = "fill" })%></td>
                            <td><label for="<%= Model.Type %>_GenericPriorLevelOfFunctioning" class="float_left">Prior Level of Functioning:</label></td>
                            <td class="align_right"><%= Html.TextArea(Model.Type + "_GenericPriorLevelOfFunctioning", data.ContainsKey("GenericPriorLevelOfFunctioning") ? data["GenericPriorLevelOfFunctioning"].Answer : string.Empty, new { @id = Model.Type + "_GenericPriorLevelOfFunctioning", @class = "fill" })%></td>
                        </tr><tr class="border-bottom">
                            <td><label for="<%= Model.Type %>_GenericLivingSituation" class="float_left">Living Situation/Support System</label></td>
                            <td class="align_right"><%= Html.TextArea(Model.Type + "_GenericLivingSituation", data.ContainsKey("GenericLivingSituation") ? data["GenericLivingSituation"].Answer : string.Empty, new { @id = Model.Type + "_GenericLivingSituation", @class = "fill" })%></td>
                            <td><label for="<%= Model.Type %>_GenericPreviousMedicalHistory" class="float_left">Describe pertinent medical/social history and/or previous therapy provided:</label></td>
                            <td class="align_right"><%= Html.TextArea(Model.Type + "_GenericPreviousMedicalHistory", data.ContainsKey("GenericPreviousMedicalHistory") ? data["GenericPreviousMedicalHistory"].Answer : string.Empty, new { @id = Model.Type + "_GenericPreviousMedicalHistory", @class = "fill" })%></td>
                        </tr><tr class="border-bottom">
                            <td><label class="float_left">Safe Swallowing Evaluation?</label></td>
                            <td class="align_right">
                                <%= Html.RadioButton(Model.Type + "_GenericIsSSE", "1", data.ContainsKey("GenericIsSSE") && data["GenericIsSSE"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericIsSSE1", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsSSE1" class="inlineradio">Yes</label>
                                <%= Html.RadioButton(Model.Type + "_GenericIsSSE", "0", data.ContainsKey("GenericIsSSE") && data["GenericIsSSE"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericIsSSE0", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsSSE0" class="inlineradio">No</label>
                                <span id="<%= Model.Type %>_GenericGenericSSESpecifySpan">
                                    <br />
                                    <label for="<%= Model.Type %>_GenericGenericSSESpecify">Specify date, facility and physician</label>
                                    <%= Html.TextArea(Model.Type + "_GenericGenericSSESpecify", data.ContainsKey("GenericGenericSSESpecify") ? data["GenericGenericSSESpecify"].Answer : string.Empty, new { @id = Model.Type + "_GenericGenericSSESpecify", @class = "fill" })%>
                                </span>
                            </td>
                            <td><label class="float_left">Video Fluoroscopy?</label></td>
                            <td class="align_right">
                                <%= Html.RadioButton(Model.Type + "_GenericIsVideoFluoroscopy", "1", data.ContainsKey("GenericIsVideoFluoroscopy") && data["GenericIsVideoFluoroscopy"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericIsVideoFluoroscopy1", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsVideoFluoroscopy1" class="inlineradio">Yes</label>
                                <%= Html.RadioButton(Model.Type + "_GenericIsVideoFluoroscopy", "0", data.ContainsKey("GenericIsVideoFluoroscopy") && data["GenericIsVideoFluoroscopy"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericIsVideoFluoroscopy0", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsVideoFluoroscopy0" class="inlineradio">No</label>
                                <span id="<%= Model.Type %>_GenericVideoFluoroscopySpecifySpan">
                                    <br />
                                    <label for="<%= Model.Type %>_GenericVideoFluoroscopySpecify">Specify date, facility and physician</label>
                                    <%= Html.TextArea(Model.Type + "_GenericVideoFluoroscopySpecify", data.ContainsKey("GenericVideoFluoroscopySpecify") ? data["GenericVideoFluoroscopySpecify"].Answer : string.Empty, new { @id = Model.Type + "_GenericVideoFluoroscopySpecify", @class = " fill" })%>
                                </span>
                            </td>
                        </tr><tr class="border-bottom">
                            <td><label for="<%= Model.Type %>_GenericCurrentDietTexture" class="float_left">Current Diet Texture</label></td>
                            <td class="align_right"><%= Html.TextArea(Model.Type + "_GenericCurrentDietTexture", data.ContainsKey("GenericCurrentDietTexture") ? data["GenericCurrentDietTexture"].Answer : string.Empty, new { @id = Model.Type + "_GenericCurrentDietTexture", @class = "fill" })%></td>
                            <td><label for="<%= Model.Type %>_GenericPainDescription" class="float_left">Pain (describe):</label></td>
                            <td class="align_right">
                                <%= Html.TextArea(Model.Type + "_GenericPainDescription", data.ContainsKey("GenericPainDescription") ? data["GenericPainDescription"].Answer : string.Empty, new { @id = Model.Type + "_GenericPainDescription", @class = "fill" })%><br />
                                <label>Impact on Therapy Care Plan</label>
                                <%= Html.RadioButton(Model.Type + "_GenericIsPainImpactCarePlan", "1", data.ContainsKey("GenericIsPainImpactCarePlan") && data["GenericIsPainImpactCarePlan"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericIsPainImpactCarePlan1", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsPainImpactCarePlan1" class="inlineradio">Yes</label>
                                <%= Html.RadioButton(Model.Type + "_GenericIsPainImpactCarePlan", "0", data.ContainsKey("GenericIsPainImpactCarePlan") && data["GenericIsPainImpactCarePlan"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericIsPainImpactCarePlan0", @class = "radio" })%>
                                <label for="<%= Model.Type %>_GenericIsPainImpactCarePlan0" class="inlineradio">No</label>
                            </td>
                        </tr><tr>
                            <td><label class="float_left">Liquids</label></td>
                            <td>
                                <input type="hidden" name="<%= Model.Type %>_GenericLiquids" value="" />
                                <%= string.Format("<input class='radio float_left' id='{1}_GenericLiquids1' name='{1}_GenericLiquids' value='1' type='checkbox' {0} />", liquids != null && liquids.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericLiquids1" class="float_left radio">Thin</label>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' id='{1}_GenericLiquids2' name='{1}_GenericLiquids' value='2' type='checkbox' {0} />", liquids != null && liquids.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericLiquids2" class="float_left radio">Thickened (specify)</label>
                                <%= Html.TextBox(Model.Type + "_GenericLiquidsThick", data.ContainsKey("GenericLiquidsThick") ? data["GenericLiquidsThick"].Answer : string.Empty, new { @class = "float_right", @id = Model.Type + "_GenericLiquidsThick" })%>
                            </td><td>
                                <%= string.Format("<input class='radio float_left' id='{1}_GenericLiquids3' name='{1}_GenericLiquids' value='3' type='checkbox' {0} />", liquids != null && liquids.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericLiquids3" class="float_left radio">Other (specify)</label>
                                <%= Html.TextBox(Model.Type + "_GenericLiquidsOther", data.ContainsKey("GenericLiquidsOther") ? data["GenericLiquidsOther"].Answer : string.Empty, new { @class = "float_right", @id = Model.Type + "_GenericLiquidsOther" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <th colspan="2">
                Speech/Language Evaluation<br />
                <label style="font-size:xx-small;font-style:italic;">4 &#8211; WFL (Within Functional Limits) &#160; 3 &#8211; Mild Impairment &#160; 2 &#8211; Moderate Impairment &#160; 1 &#8211; Severe Impairment &#160; 0 &#8211; Unable to Assess/Did Not Test</label>
            </th><td rowspan="5">
                <div class="padnoterow">
                    <input type="hidden" name="<%= Model.Type %>_GenericReferralFor" value="" />
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <label class="float_left">Referral for:</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericReferralFor1' class='float_left radio' name='{1}_GenericReferralFor' value='1' type='checkbox' {0} />", genericReferralFor != null && genericReferralFor.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericReferralFor1" class="float_left radio">Vision</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericReferralFor2' class='float_left radio' name='{1}_GenericReferralFor' value='2' type='checkbox' {0} />", genericReferralFor != null && genericReferralFor.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericReferralFor2" class="float_left radio">Hearing</label>
                                </td>
                            </tr><tr>
                                <td></td><td>
                                    <%= string.Format("<input id='{1}_GenericReferralFor3' class='float_left radio' name='{1}_GenericReferralFor' value='3' type='checkbox' {0} />", genericReferralFor != null && genericReferralFor.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericReferralFor3" class="float_left radio">Swallowing</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericReferralFor4' class='float_left radio' name='{1}_GenericReferralFor' value='4' type='checkbox' {0} />", genericReferralFor != null && genericReferralFor.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericReferralFor4" class="float_left radio">Other:</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericReferralForOther", data.ContainsKey("GenericReferralForOther") ? data["GenericReferralForOther"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericReferralForOther" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericDiagnosis" class="float_left">Diagnosis:</label>
                    <div class="float_right align_right">
                        <%= Html.TextBox(Model.Type + "_GenericDiagnosis", data.ContainsKey("GenericDiagnosis") ? data["GenericDiagnosis"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericDiagnosis" })%><br />
                        <label for="<%= Model.Type %>_GenericDiagnosisOnsetDate" class="float_left radio">Onset</label>
                        <input type="date" name="<%= Model.Type %>_GenericDiagnosisOnsetDate" value="<%= data.ContainsKey("GenericDiagnosisOnsetDate") && data["GenericDiagnosisOnsetDate"].Answer.IsValidDate() ? data["GenericDiagnosisOnsetDate"].Answer : "" %>" id="<%= Model.Type %>_GenericDiagnosisOnsetDate", @class = "date shortdate" })%>
                    </div>
                </div><div class="padnoterow">
                    <div class="align_left strong">Analysis of Evaluation/Test Scores</div>
                    <div class="align_left margin">
                        <label for="<%= Model.Type %>" class="strong">Patient Desired Outcomes</label><br />
                        <%= Html.TextArea(Model.Type + "_GenericPatientDesiredOutcomes", data.ContainsKey("GenericPatientDesiredOutcomes") ? data["GenericPatientDesiredOutcomes"].Answer : string.Empty, new { @id = Model.Type + "_GenericPatientDesiredOutcomes", @class = "fill" })%><br />
                        <label for="<%= Model.Type %>" class="strong">Short Term Outcomes</label><br />
                        <%= Html.TextArea(Model.Type + "_GenericShortTermOutcomes", data.ContainsKey("GenericShortTermOutcomes") ? data["GenericShortTermOutcomes"].Answer : string.Empty, new { @id = Model.Type + "_GenericShortTermOutcomes", @class = "fill" })%><br />
                        <label for="<%= Model.Type %>" class="strong">Long Term Outcomes</label><br />
                        <%= Html.TextArea(Model.Type + "_GenericLongTermOutcomes", data.ContainsKey("GenericLongTermOutcomes") ? data["GenericLongTermOutcomes"].Answer : string.Empty, new { @id = Model.Type + "_GenericLongTermOutcomes", @class = "fill" })%>
                    </div>
                </div><div class="padnoterow">
                    <div class="strong">Plan of Care (check all that apply)</div>
                    <input type="hidden" name="<%= Model.Type %>_GenericPlanOfCare" value="" />
                    <table class="align_left" style="vertical-align:top;">
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare1' class='radio' name='{1}_GenericPlanOfCare' value='1' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare1">Evaluation (C1)</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare15' class='radio' name='{1}_GenericPlanOfCare' value='15' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("15") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare15">Language Processing</label>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare2' class='radio' name='{1}_GenericPlanOfCare' value='2' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare2">Establish Rehab Program</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare16' class='radio' name='{1}_GenericPlanOfCare' value='16' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("16") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare16">Food Texture Recommendations</label>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare3' class='radio' name='{1}_GenericPlanOfCare' value='3' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare3">Given to Patient</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare17' class='radio' name='{1}_GenericPlanOfCare' value='17' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("17") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare17">Safe Swallowing Evaluation</label>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare4' class='radio' name='{1}_GenericPlanOfCare' value='4' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare4">Attached to Chart</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare18' class='radio' name='{1}_GenericPlanOfCare' value='18' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("18") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare18">Therapy to Increase Articulation, Proficiency, Verbal Expression</label>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare5' class='radio' name='{1}_GenericPlanOfCare' value='5' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare5">Patient/Family Education</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare19' class='radio' name='{1}_GenericPlanOfCare' value='19' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("19") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare19">Lip, Tongue, Facial Exercises to Improve Swallowing/Vocal Skills</label>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare6' class='radio' name='{1}_GenericPlanOfCare' value='6' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare6">Voice Disorders</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare20' class='radio' name='{1}_GenericPlanOfCare' value='20' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("20") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare20">Pain Management</label>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare7' class='radio' name='{1}_GenericPlanOfCare' value='7' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare7">Speech Articulation Disorders</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare21' class='radio' name='{1}_GenericPlanOfCare' value='21' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("21") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare21">Speech Dysphagia Instruction Program</label>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare8' class='radio' name='{1}_GenericPlanOfCare' value='8' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare8">Dysphagia Treatments</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare22' class='radio' name='{1}_GenericPlanOfCare' value='22' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("22") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare22">Care of Voice Prosthesis &#8212; Removal, Cleaning, Site Maint</label>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare9' class='radio' name='{1}_GenericPlanOfCare' value='9' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("9") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare9">Language Disorders</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare23' class='radio' name='{1}_GenericPlanOfCare' value='23' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("23") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare23">Teach/Develop Comm. System</label>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare10' class='radio' name='{1}_GenericPlanOfCare' value='10' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("10") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare10">Aural Rehabilitation (C6)</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare24' class='radio' name='{1}_GenericPlanOfCare' value='24' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("24") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare24">Trach Inst. and Care</label>
                                </td>
                            </tr><tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare11' class='radio' name='{1}_GenericPlanOfCare' value='11' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("11") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare11">Non-Oral Communication (C8)</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare25' class='radio' name='{1}_GenericPlanOfCare' value='25' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("25") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare25">Other</label>
                                </td>
                            </tr><tr>
                                <td colspan="2">
                                    <%= string.Format("<input id='{1}_GenericPlanOfCare12' class='radio' name='{1}_GenericPlanOfCare' value='12' type='checkbox' {0} />", genericPlanOfCare != null && genericPlanOfCare.Contains("12") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericPlanOfCare12">Alaryngeal Speech Skills</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericFrequencyAndDuration" class="float_left">Frequency and Duration</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericFrequencyAndDuration", data.ContainsKey("GenericFrequencyAndDuration") ? data["GenericFrequencyAndDuration"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericFrequencyAndDuration" })%></div>
                </div><div class="padnoterow">
                    <label class="float_left">Rehab Potential</label>
                    <div class="float_right">
                        <%= Html.RadioButton(Model.Type + "_GenericRehabPotential", "2", data.ContainsKey("GenericRehabPotential") && data["GenericRehabPotential"].Answer == "2" ? true : false, new { @id = Model.Type + "_GenericRehabPotential2", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericRehabPotential2" class="inlineradio">Good</label>
                        <%= Html.RadioButton(Model.Type + "_GenericRehabPotential", "1", data.ContainsKey("GenericRehabPotential") && data["GenericRehabPotential"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericRehabPotential1", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericRehabPotential1" class="inlineradio">Fair</label>
                        <%= Html.RadioButton(Model.Type + "_GenericRehabPotential", "0", data.ContainsKey("GenericRehabPotential") && data["GenericRehabPotential"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericRehabPotential0", @class = "radio" })%>
                        <label for="<%= Model.Type %>_GenericRehabPotential0" class="inlineradio">Poor</label>
                    </div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericEquipmentRecommendations" class="float_left">Equipment Recommendations</label>
                    <%= Html.TextArea(Model.Type + "_GenericEquipmentRecommendations", data.ContainsKey("GenericEquipmentRecommendations") ? data["GenericEquipmentRecommendations"].Answer : string.Empty, new { @id = Model.Type + "_GenericEquipmentRecommendations", @class = "fill" })%>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSafetyIssues" class="float_left">Safety Issues/ Instruction/ Education</label>
                    <%= Html.TextArea(Model.Type + "_GenericSafetyIssues", data.ContainsKey("GenericSafetyIssues") ? data["GenericSafetyIssues"].Answer : string.Empty, new { @id = Model.Type + "_GenericSafetyIssues", @class = "fill" })%>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>" class="float_left">Comments/ Additional Information</label>
                    <%= Html.TextArea(Model.Type + "_GenericAdditionalInformation", data.ContainsKey("GenericAdditionalInformation") ? data["GenericAdditionalInformation"].Answer : string.Empty, new { @id = Model.Type + "_GenericAdditionalInformation", @class = "fill" })%>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>" class="float_left">Patient/ Caregiver Response to Plan of Care</label>
                    <%= Html.TextArea(Model.Type + "_GenericResponseToPOC", data.ContainsKey("GenericResponseToPOC") ? data["GenericResponseToPOC"].Answer : string.Empty, new { @id = Model.Type + "_GenericResponseToPOC", @class = "fill" })%>
                </div><div class="padnoterow">
                    <label class="float_left">Discharge discussed with:</label>
                    <input type="hidden" name="<%= Model.Type %>_GenericDischargeDiscussedWith" value="" />
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericDischargeDiscussedWith1' class='float_left radio' name='{1}_GenericDischargeDiscussedWith' value='1' type='checkbox' {0} />", genericDischargeDiscussedWith != null && genericDischargeDiscussedWith.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDischargeDiscussedWith1" class="float_left radio">Patient Family</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericDischargeDiscussedWith2' class='float_left radio' name='{1}_GenericDischargeDiscussedWith' value='2' type='checkbox' {0} />", genericDischargeDiscussedWith != null && genericDischargeDiscussedWith.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDischargeDiscussedWith2" class="float_left radio">Case Manager</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericDischargeDiscussedWith3' class='float_left radio' name='{1}_GenericDischargeDiscussedWith' value='3' type='checkbox' {0} />", genericDischargeDiscussedWith != null && genericDischargeDiscussedWith.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDischargeDiscussedWith3" class="float_left radio">Physician</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericDischargeDiscussedWith4' class='float_left radio' name='{1}_GenericDischargeDiscussedWith' value='4' type='checkbox' {0} />", genericDischargeDiscussedWith != null && genericDischargeDiscussedWith.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericDischargeDiscussedWith4" class="float_left radio">Other</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericDischargeDiscussedWithOther", data.ContainsKey("GenericDischargeDiscussedWithOther") ? data["GenericDischargeDiscussedWithOther"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericDischargeDiscussedWithOther" })%></div>
                </div><div class="padnoterow">
                    <label class="float_left">Care Coordination:</label>
                    <input type="hidden" name="<%= Model.Type %>_GenericCareCoordination" value="" />
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination1' class='float_left radio' name='{1}_GenericCareCoordination' value='1' type='checkbox' {0} />", genericCareCoordination != null && genericCareCoordination.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination1" class="float_left radio">Physician</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination2' class='float_left radio' name='{1}_GenericCareCoordination' value='2' type='checkbox' {0} />", genericCareCoordination != null && genericCareCoordination.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination2" class="float_left radio">PT</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination3' class='float_left radio' name='{1}_GenericCareCoordination' value='3' type='checkbox' {0} />", genericCareCoordination != null && genericCareCoordination.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination3" class="float_left radio">OT</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination4' class='float_left radio' name='{1}_GenericCareCoordination' value='4' type='checkbox' {0} />", genericCareCoordination != null && genericCareCoordination.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination4" class="float_left radio">ST</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination5' class='float_left radio' name='{1}_GenericCareCoordination' value='5' type='checkbox' {0} />", genericCareCoordination != null && genericCareCoordination.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination5" class="float_left radio">MSW</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination6' class='float_left radio' name='{1}_GenericCareCoordination' value='6' type='checkbox' {0} />", genericCareCoordination != null && genericCareCoordination.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination6" class="float_left radio">SN</label>
                                </td><td>
                                    <%= string.Format("<input id='{1}_GenericCareCoordination7' class='float_left radio' name='{1}_GenericCareCoordination' value='7' type='checkbox' {0} />", genericCareCoordination != null && genericCareCoordination.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericCareCoordination7" class="float_left radio">Other</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericCareCoordinationOther", data.ContainsKey("GenericCareCoordinationOther") ? data["GenericCareCoordinationOther"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericCareCoordinationOther" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericPlanForNextVisit" class="float_left">Plan for Next Visit</label>
                    <%= Html.TextArea(Model.Type + "_GenericPlanForNextVisit", data.ContainsKey("GenericPlanForNextVisit") ? data["GenericPlanForNextVisit"].Answer : string.Empty, new { @id = Model.Type + "_GenericPlanForNextVisit", @class = "fill" })%>
                </div>                        
            </td>
        </tr>
        <tr>
            <td>
                <div class="align_center strong">Cognition Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOrientationScore" class="float_left">Orientation (Person/Place/Time)</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericOrientationScore", data.ContainsKey("GenericOrientationScore") ? data["GenericOrientationScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericOrientationScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericAttentionSpanScore" class="float_left">Attention Span</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericAttentionSpanScore", data.ContainsKey("GenericAttentionSpanScore") ? data["GenericAttentionSpanScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAttentionSpanScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericShortTermMemoryScore" class="float_left">Short Term Memory</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericShortTermMemoryScore", data.ContainsKey("GenericShortTermMemoryScore") ? data["GenericShortTermMemoryScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShortTermMemoryScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericLongTermMemoryScore" class="float_left">Long Term Memory</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericLongTermMemoryScore", data.ContainsKey("GenericLongTermMemoryScore") ? data["GenericLongTermMemoryScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericLongTermMemoryScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericJudgmentScore" class="float_left">Judgment</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericJudgmentScore", data.ContainsKey("GenericJudgmentScore") ? data["GenericJudgmentScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericJudgmentScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericProblemSolvingScore" class="float_left">Problem Solving</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericProblemSolvingScore", data.ContainsKey("GenericProblemSolvingScore") ? data["GenericProblemSolvingScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericProblemSolvingScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOrganizationScore" class="float_left">Organization</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericOrganizationScore", data.ContainsKey("GenericOrganizationScore") ? data["GenericOrganizationScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericOrganizationScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericCognitionFunctionEvaluatedOtherScore" class="float_left">Other:</label><%= Html.TextBox(Model.Type + "_GenericCognitionFunctionEvaluatedOther", data.ContainsKey("GenericCognitionFunctionEvaluatedOther") ? data["GenericCognitionFunctionEvaluatedOther"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericCognitionFunctionEvaluatedOther" })%>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericCognitionFunctionEvaluatedOtherScore", data.ContainsKey("GenericCognitionFunctionEvaluatedOtherScore") ? data["GenericCognitionFunctionEvaluatedOtherScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericCognitionFunctionEvaluatedOtherScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericCognitionFunctionEvaluatedComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericCognitionFunctionEvaluatedComment", data.ContainsKey("GenericCognitionFunctionEvaluatedComment") ? data["GenericCognitionFunctionEvaluatedComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericCognitionFunctionEvaluatedComment", @class = "fill" })%></div>
                </div>
            </td><td>
                <div class="align_center strong">Speech/Voice Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOralFacialExamScore" class="float_left">Oral/Facial Exam</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericOralFacialExamScore", data.ContainsKey("GenericOralFacialExamScore") ? data["GenericOralFacialExamScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericOralFacialExamScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericArticulationScore" class="float_left">Articulation</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericArticulationScore", data.ContainsKey("GenericArticulationScore") ? data["GenericArticulationScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericArticulationScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericProsodyScore" class="float_left">Prosody</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericProsodyScore", data.ContainsKey("GenericProsodyScore") ? data["GenericProsodyScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericProsodyScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVoiceRespirationScore" class="float_left">Voice/Respiration</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericVoiceRespirationScore", data.ContainsKey("GenericVoiceRespirationScore") ? data["GenericVoiceRespirationScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericVoiceRespirationScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechIntelligibilityScore" class="float_left">Speech Intelligibility</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericSpeechIntelligibilityScore", data.ContainsKey("GenericSpeechIntelligibilityScore") ? data["GenericSpeechIntelligibilityScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericSpeechIntelligibilityScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechFunctionEvaluatedOtherScore" class="float_left">Other: <%= Html.TextBox(Model.Type + "_GenericSpeechFunctionEvaluatedOther", data.ContainsKey("GenericSpeechFunctionEvaluatedOther") ? data["GenericSpeechFunctionEvaluatedOther"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericSpeechFunctionEvaluatedOther" })%></label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericSpeechFunctionEvaluatedOtherScore", data.ContainsKey("GenericSpeechFunctionEvaluatedOtherScore") ? data["GenericSpeechFunctionEvaluatedOtherScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericSpeechFunctionEvaluatedOtherScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechFunctionEvaluatedComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericSpeechFunctionEvaluatedComment", data.ContainsKey("GenericSpeechFunctionEvaluatedComment") ? data["GenericSpeechFunctionEvaluatedComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericSpeechFunctionEvaluatedComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="align_center strong">Auditory Comprehension Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWordDiscriminationScore" class="float_left">Word Discrimination</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericWordDiscriminationScore", data.ContainsKey("GenericWordDiscriminationScore") ? data["GenericWordDiscriminationScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWordDiscriminationScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOneStepDirectionsScore" class="float_left">One Step Directions</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericOneStepDirectionsScore", data.ContainsKey("GenericOneStepDirectionsScore") ? data["GenericOneStepDirectionsScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericOneStepDirectionsScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericTwoStepDirectionsScore" class="float_left">Two Step Directions</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericTwoStepDirectionsScore", data.ContainsKey("GenericTwoStepDirectionsScore") ? data["GenericTwoStepDirectionsScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTwoStepDirectionsScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericComplexSentencesScore" class="float_left">Complex Sentences</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericComplexSentencesScore", data.ContainsKey("GenericComplexSentencesScore") ? data["GenericComplexSentencesScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericComplexSentencesScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericConversationScore" class="float_left">Conversation</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericConversationScore", data.ContainsKey("GenericConversationScore") ? data["GenericConversationScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericConversationScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSpeechReadingScore" class="float_left">Speech Reading</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericSpeechReadingScore", data.ContainsKey("GenericSpeechReadingScore") ? data["GenericSpeechReadingScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericSpeechReadingScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericACFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericACFEComment", data.ContainsKey("GenericACFEComment") ? data["GenericACFEComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericACFEComment", @class = "fill" })%></div>
</div>
            </td><td>
                <div class="align_center strong">Swallowing Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericChewingAbilityScore" class="float_left">Chewing Ability</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericChewingAbilityScore", data.ContainsKey("GenericChewingAbilityScore") ? data["GenericChewingAbilityScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericChewingAbilityScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericOralStageManagementScore" class="float_left">Oral Stage Management</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericOralStageManagementScore", data.ContainsKey("GenericOralStageManagementScore") ? data["GenericOralStageManagementScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericOralStageManagementScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericPharyngealStageManagementScore" class="float_left">Pharyngeal Stage Management</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericPharyngealStageManagementScore", data.ContainsKey("GenericPharyngealStageManagementScore") ? data["GenericPharyngealStageManagementScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericPharyngealStageManagementScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericReflexTimeScore" class="float_left">Reflex Time</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericReflexTimeScore", data.ContainsKey("GenericReflexTimeScore") ? data["GenericReflexTimeScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericReflexTimeScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSwallowingFunctionEvaluatedOtherScore" class="float_left">Other:</label><%= Html.TextBox(Model.Type + "_GenericSwallowingFunctionEvaluatedOther", data.ContainsKey("GenericSwallowingFunctionEvaluatedOther") ? data["GenericSwallowingFunctionEvaluatedOther"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericSwallowingFunctionEvaluatedOther" })%>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericSwallowingFunctionEvaluatedOtherScore", data.ContainsKey("GenericSwallowingFunctionEvaluatedOtherScore") ? data["GenericSwallowingFunctionEvaluatedOtherScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericSwallowingFunctionEvaluatedOtherScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSwallowingFunctionEvaluatedComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericSwallowingFunctionEvaluatedComment", data.ContainsKey("GenericSwallowingFunctionEvaluatedComment") ? data["GenericSwallowingFunctionEvaluatedComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericSwallowingFunctionEvaluatedComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="align_center strong">Verbal Expression Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericAugmentativeMethodsScore" class="float_left">Augmentative Methods</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericAugmentativeMethodsScore", data.ContainsKey("GenericAugmentativeMethodsScore") ? data["GenericAugmentativeMethodsScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAugmentativeMethodsScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericNamingScore" class="float_left">Naming</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericNamingScore", data.ContainsKey("GenericNamingScore") ? data["GenericNamingScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNamingScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericAppropriateScore" class="float_left">Appropriate</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericAppropriateScore", data.ContainsKey("GenericAppropriateScore") ? data["GenericAppropriateScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAppropriateScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVEFEComplexSentencesScore" class="float_left">Complex Sentences</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericVEFEComplexSentencesScore", data.ContainsKey("GenericVEFEComplexSentencesScore") ? data["GenericVEFEComplexSentencesScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericVEFEComplexSentencesScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVEFEConversationScore" class="float_left">Conversation</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericVEFEConversationScore", data.ContainsKey("GenericVEFEConversationScore") ? data["GenericVEFEConversationScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericVEFEConversationScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericVEFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericVEFEComment", data.ContainsKey("GenericVEFEComment") ? data["GenericVEFEComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericVEFEComment", @class = "fill" })%></div>
                </div>
            </td><td>
                <div class="align_center strong">Reading Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFELettersNumbersScore" class="float_left">Letters/Numbers</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericRFELettersNumbersScore", data.ContainsKey("GenericRFELettersNumbersScore") ? data["GenericRFELettersNumbersScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericRFELettersNumbersScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFEWordsScore" class="float_left">Words</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericRFEWordsScore", data.ContainsKey("GenericRFEWordsScore") ? data["GenericRFEWordsScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericRFEWordsScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFESimpleSentencesScore" class="float_left">Simple Sentences</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericRFESimpleSentencesScore", data.ContainsKey("GenericRFESimpleSentencesScore") ? data["GenericRFESimpleSentencesScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericRFESimpleSentencesScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFEComplexSentencesScore" class="float_left">Complex Sentences</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericRFEComplexSentencesScore", data.ContainsKey("GenericRFEComplexSentencesScore") ? data["GenericRFEComplexSentencesScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericRFEComplexSentencesScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericParagraphScore" class="float_left">Paragraph</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericParagraphScore", data.ContainsKey("GenericParagraphScore") ? data["GenericParagraphScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericParagraphScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericRFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericRFEComment", data.ContainsKey("GenericRFEComment") ? data["GenericRFEComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericRFEComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="align_center strong">Writing Function Evaluated</div>
                <div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFELettersNumbersScore" class="float_left">Letters/Numbers</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericWFELettersNumbersScore", data.ContainsKey("GenericWFELettersNumbersScore") ? data["GenericWFELettersNumbersScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWFELettersNumbersScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFEWordsScore" class="float_left">Words</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericWFEWordsScore", data.ContainsKey("GenericWFEWordsScore") ? data["GenericWFEWordsScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWFEWordsScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFESentencesScore" class="float_left">Sentences</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericWFESentencesScore", data.ContainsKey("GenericWFESentencesScore") ? data["GenericWFESentencesScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWFESentencesScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFESpellingScore" class="float_left">Spelling</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericWFESpellingScore", data.ContainsKey("GenericWFESpellingScore") ? data["GenericWFESpellingScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWFESpellingScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericFormulationScore" class="float_left">Formulation</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericFormulationScore", data.ContainsKey("GenericFormulationScore") ? data["GenericFormulationScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFormulationScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericSimpleAdditionSubtractionScore" class="float_left">Simple Addition/Subtraction</label>
                    <div class="float_right"><%= Html.TextBox(Model.Type + "_GenericSimpleAdditionSubtractionScore", data.ContainsKey("GenericSimpleAdditionSubtractionScore") ? data["GenericSimpleAdditionSubtractionScore"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericSimpleAdditionSubtractionScore" })%></div>
                </div><div class="padnoterow">
                    <label for="<%= Model.Type %>_GenericWFEComment" class="strong">Comment:</label>
                    <div><%= Html.TextArea(Model.Type + "_GenericWFEComment", data.ContainsKey("GenericWFEComment") ? data["GenericWFEComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericWFEComment", @class = "fill" })%></div>
                </div>
            </td>
        </tr>   
    </tbody>
</table>
