﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %><%
var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>();
string[] medicalDiagnosisOnset = data.ContainsKey("GenericMedicalDiagnosisOnset") && data["GenericMedicalDiagnosisOnset"].Answer != "" ? data["GenericMedicalDiagnosisOnset"].Answer.Split(',') : null;
string[] ptDiagnosis = data.ContainsKey("GenericPTDiagnosis") && data["GenericPTDiagnosis"].Answer != "" ? data["GenericPTDiagnosis"].Answer.Split(',') : null;
string[] genericBedMobilityRolling = data.ContainsKey("GenericBedMobilityRolling") && data["GenericBedMobilityRolling"].Answer != "" ? data["GenericBedMobilityRolling"].Answer.Split(',') : null;
string[] genericTransferSittingBalance = data.ContainsKey("GenericTransferSittingBalance") && data["GenericTransferSittingBalance"].Answer != "" ? data["GenericTransferSittingBalance"].Answer.Split(',') : null;
string[] genericTransferStandBalance = data.ContainsKey("GenericTransferStandBalance") && data["GenericTransferStandBalance"].Answer != "" ? data["GenericTransferStandBalance"].Answer.Split(',') : null;
string[] genericTreatmentCodes = data.ContainsKey("GenericTreatmentCodes") && data["GenericTreatmentCodes"].Answer != "" ? data["GenericTreatmentCodes"].Answer.Split(',') : null;
string[] genericTreatmentPlan = data.ContainsKey("GenericTreatmentPlan") && data["GenericTreatmentPlan"].Answer != "" ? data["GenericTreatmentPlan"].Answer.Split(',') : null;
string[] genericDisciplineRecommendation = data.ContainsKey("GenericDisciplineRecommendation") && data["GenericDisciplineRecommendation"].Answer != "" ? data["GenericDisciplineRecommendation"].Answer.Split(',') : null; %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th colspan="10">Vital Signs</th>
        </tr><tr>
            <td colspan="4">
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericTemp" class="float_left">Temp:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericTemp", data.ContainsKey("GenericTemp") ? data["GenericTemp"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTemp" })%>
                                </div>
                            </td><td>
                                <label for="<%= Model.Type %>_GenericResp" class="float_left">Resp:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericResp", data.ContainsKey("GenericResp") ? data["GenericResp"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericResp" })%>
                                </div>
                            </td>
                        </tr><tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericPulse" class="float_left">Pulse:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericPulse", data.ContainsKey("GenericPulse") ? data["GenericPulse"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericPulse" })%>
                                </div>
                            </td><td>
                                <label for="<%= Model.Type %>_GenericWeight" class="float_left">Weight:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericWeight", data.ContainsKey("GenericWeight") ? data["GenericWeight"].Answer:string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWeight" })%>
                                </div>
                            </td>
                        </tr><tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericBloodPressure" class="float_left">Blood Pressure:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericBloodPressure", data.ContainsKey("GenericBloodPressure") ? data["GenericBloodPressure"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericBloodPressure" })%>/ <%= Html.TextBox(Model.Type + "_GenericBloodPressurePer", data.ContainsKey("GenericBloodPressurePer") ? data["GenericBloodPressurePer"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericBloodPressurePer" })%>
                                </div>
                            </td><td>
                                <label for="<%= Model.Type %>_GenericBloodSugar" class="float_left">Blood Sugar:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericBloodSugar", data.ContainsKey("GenericBloodSugar") ? data["GenericBloodSugar"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericBloodSugar" })%>
                                </div>
                            </td>
                        </tr>
                     </tbody>
                </table>
            </td><td colspan="6">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericMedicalDiagnosis" class="float_left">Medical Diagnosis:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericMedicalDiagnosis", data.ContainsKey("GenericMedicalDiagnosis") ? data["GenericMedicalDiagnosis"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericMedicalDiagnosis" })%>
                                </div>
                            </td><td>
                                <input type="hidden" name="<%= Model.Type %>_GenericMedicalDiagnosisOnset" value="" />
                                <%= string.Format("<input class='radio float_left' id='{1}_GenericMedicalDiagnosisOnset1' name='{1}_GenericMedicalDiagnosisOnset' value='1' type='checkbox' {0} />", medicalDiagnosisOnset != null && medicalDiagnosisOnset.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericMedicalDiagnosisOnset1" class="float_left radio">Onset</label>
                                <div class="float_right">
                                    <input type="date" name="<%= Model.Type %>_MedicalDiagnosisDate" value="<%= DateTime.Now.ToShortDateString() %>" id=Model.Type + "_MedicalDiagnosisDate", @class = "date shortdate" })%>
                                </div>
                            </td>
                        </tr><tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericPTDiagnosis" class="float_left">PT Diagnosis:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericPTDiagnosis", data.ContainsKey("GenericPTDiagnosis") ? data["GenericPTDiagnosis"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPTDiagnosis" })%>
                                </div>
                            </td><td>
                                <input type="hidden" name="<%= Model.Type %>_GenericPTDiagnosis" value="" />
                                <%= string.Format("<input class='radio float_left' id='{1}_GenericPTDiagnosis1' name='{1}_GenericPTDiagnosis' value='1' type='checkbox' {0} />", ptDiagnosis != null && ptDiagnosis.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                                <label for="<%= Model.Type %>_GenericPTDiagnosis1" class="float_left radio">Onset</label>
                                <div class="float_right">
                                    <input type="date" name="<%= Model.Type %>_PTDiagnosisDate" value="<%= DateTime.Now.ToShortDateString() %>" id=Model.Type + "_PTDiagnosisDate", @class = "date shortdate" }) %>
                                </div>
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <label for="<%= Model.Type %>_GenericMedicalHistory" class="strong">Medical History:</label>
                                <%= Html.TextArea(Model.Type + "_GenericMedicalHistory", data.ContainsKey("GenericMedicalHistory") ? data["GenericMedicalHistory"].Answer : string.Empty, new { @id = Model.Type + "_GenericMedicalHistory", @class = "fill" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="4">Mental Assessment</th>
            <th colspan="6">Physical Assessment</th>
        </tr><tr>
            <td colspan="4">
                <label for="<%= Model.Type %>_GenericMentalAssessmentOrientation" class="float_left">Orientation:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericMentalAssessmentOrientation", data.ContainsKey("GenericMentalAssessmentOrientation") ? data["GenericMentalAssessmentOrientation"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericMentalAssessmentOrientation" })%>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericMentalAssessmentLOC" class="float_left">LOC:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericMentalAssessmentLOC", data.ContainsKey("GenericMentalAssessmentLOC") ? data["GenericMentalAssessmentLOC"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericMentalAssessmentLOC" })%>
                </div>
            </td><td colspan="6" rowspan="9">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2"></th>
                            <th colspan="2"><strong>ROM</strong></th>
                            <th colspan="2"><strong>Strength</strong></th>
                        </tr><tr>
                            <th class="align_left"><strong>Part</strong></th>
                            <th class="align_left"><strong>Action</strong></th>
                            <th><strong>Right</strong></th>
                            <th><strong>Left</strong></th>
                            <th><strong>Right</strong></th>
                            <th><strong>Left</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="align_left">
                                <strong>Shoulder</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderFlexionROMRight", data.ContainsKey("GenericShoulderFlexionROMRight") ? data["GenericShoulderFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderFlexionROMLeft", data.ContainsKey("GenericShoulderFlexionROMLeft") ? data["GenericShoulderFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderFlexionStrengthRight", data.ContainsKey("GenericShoulderFlexionStrengthRight") ? data["GenericShoulderFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderFlexionStrengthLeft", data.ContainsKey("GenericShoulderFlexionStrengthLeft") ? data["GenericShoulderFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtensionROMRight", data.ContainsKey("GenericShoulderExtensionROMRight") ? data["GenericShoulderExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtensionROMLeft", data.ContainsKey("GenericShoulderExtensionROMLeft") ? data["GenericShoulderExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtensionStrengthRight", data.ContainsKey("GenericShoulderExtensionStrengthRight") ? data["GenericShoulderExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtensionStrengthLeft", data.ContainsKey("GenericShoulderExtensionStrengthLeft") ? data["GenericShoulderExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Abduction
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderAbductionROMRight", data.ContainsKey("GenericShoulderAbductionROMRight") ? data["GenericShoulderAbductionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderAbductionROMLeft", data.ContainsKey("GenericShoulderAbductionROMLeft") ? data["GenericShoulderAbductionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderAbductionStrengthRight", data.ContainsKey("GenericShoulderAbductionStrengthRight") ? data["GenericShoulderAbductionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderAbductionStrengthLeft", data.ContainsKey("GenericShoulderAbductionStrengthLeft") ? data["GenericShoulderAbductionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderAbductionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Int Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderIntRotROMRight", data.ContainsKey("GenericShoulderIntRotROMRight") ? data["GenericShoulderIntRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderIntRotROMLeft", data.ContainsKey("GenericShoulderIntRotROMLeft") ? data["GenericShoulderIntRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderIntRotStrengthRight", data.ContainsKey("GenericShoulderIntRotStrengthRight") ? data["GenericShoulderIntRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderIntRotStrengthLeft", data.ContainsKey("GenericShoulderIntRotStrengthLeft") ? data["GenericShoulderIntRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderIntRotStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Ext Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtRotROMRight", data.ContainsKey("GenericShoulderExtRotROMRight") ? data["GenericShoulderExtRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtRotROMLeft", data.ContainsKey("GenericShoulderExtRotROMLeft") ? data["GenericShoulderExtRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtRotStrengthRight", data.ContainsKey("GenericShoulderExtRotStrengthRight") ? data["GenericShoulderExtRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericShoulderExtRotStrengthLeft", data.ContainsKey("GenericShoulderExtRotStrengthLeft") ? data["GenericShoulderExtRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericShoulderExtRotStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Elbow</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowFlexionROMRight", data.ContainsKey("GenericElbowFlexionROMRight") ? data["GenericElbowFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowFlexionROMLeft", data.ContainsKey("GenericElbowFlexionROMLeft") ? data["GenericElbowFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowFlexionStrengthRight", data.ContainsKey("GenericElbowFlexionStrengthRight") ? data["GenericElbowFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowFlexionStrengthLeft", data.ContainsKey("GenericElbowFlexionStrengthLeft") ? data["GenericElbowFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowExtensionROMRight", data.ContainsKey("GenericElbowExtensionROMRight") ? data["GenericElbowExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowExtensionROMLeft", data.ContainsKey("GenericElbowExtensionROMLeft") ? data["GenericElbowExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowExtensionStrengthRight", data.ContainsKey("GenericElbowExtensionStrengthRight") ? data["GenericElbowExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericElbowExtensionStrengthLeft", data.ContainsKey("GenericElbowExtensionStrengthLeft") ? data["GenericElbowExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericElbowExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Finger</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerFlexionROMRight", data.ContainsKey("GenericFingerFlexionROMRight") ? data["GenericFingerFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerFlexionROMLeft", data.ContainsKey("GenericFingerFlexionROMLeft") ? data["GenericFingerFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerFlexionStrengthRight", data.ContainsKey("GenericFingerFlexionStrengthRight") ? data["GenericFingerFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerFlexionStrengthLeft", data.ContainsKey("GenericFingerFlexionStrengthLeft") ? data["GenericFingerFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerExtensionROMRight", data.ContainsKey("GenericFingerExtensionROMRight") ? data["GenericFingerExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerExtensionROMLeft", data.ContainsKey("GenericFingerExtensionROMLeft") ? data["GenericFingerExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerExtensionStrengthRight", data.ContainsKey("GenericFingerExtensionStrengthRight") ? data["GenericFingerExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericFingerExtensionStrengthLeft", data.ContainsKey("GenericFingerExtensionStrengthLeft") ? data["GenericFingerExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericFingerExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Wrist</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristFlexionROMRight", data.ContainsKey("GenericWristFlexionROMRight") ? data["GenericWristFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristFlexionROMLeft", data.ContainsKey("GenericWristFlexionROMLeft") ? data["GenericWristFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristFlexionStrengthRight", data.ContainsKey("GenericWristFlexionStrengthRight") ? data["GenericWristFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristFlexionStrengthLeft", data.ContainsKey("GenericWristFlexionStrengthLeft") ? data["GenericWristFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristExtensionROMRight", data.ContainsKey("GenericWristExtensionROMRight") ? data["GenericWristExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristExtensionROMLeft", data.ContainsKey("GenericWristExtensionROMLeft") ? data["GenericWristExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristExtensionStrengthRight", data.ContainsKey("GenericWristExtensionStrengthRight") ? data["GenericWristExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWristExtensionStrengthLeft", data.ContainsKey("GenericWristExtensionStrengthLeft") ? data["GenericWristExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericWristExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Hip</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipFlexionROMRight", data.ContainsKey("GenericHipFlexionROMRight") ? data["GenericHipFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipFlexionROMLeft", data.ContainsKey("GenericHipFlexionROMLeft") ? data["GenericHipFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipFlexionStrengthRight", data.ContainsKey("GenericHipFlexionStrengthRight") ? data["GenericHipFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipFlexionStrengthLeft", data.ContainsKey("GenericHipFlexionStrengthLeft") ? data["GenericHipFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtensionROMRight", data.ContainsKey("GenericHipExtensionROMRight") ? data["GenericHipExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type  +"_GenericHipExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtensionROMLeft", data.ContainsKey("GenericHipExtensionROMLeft") ? data["GenericHipExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type  +"_GenericHipExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtensionStrengthRight", data.ContainsKey("GenericHipExtensionStrengthRight") ? data["GenericHipExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtensionStrengthLeft", data.ContainsKey("GenericHipExtensionStrengthLeft") ? data["GenericHipExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Abduction
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipAbductionROMRight", data.ContainsKey("GenericHipAbductionROMRight") ? data["GenericHipAbductionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipAbductionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipAbductionROMLeft", data.ContainsKey("GenericHipAbductionROMLeft") ? data["GenericHipAbductionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipAbductionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipAbductionStrengthRight", data.ContainsKey("GenericHipAbductionStrengthRight") ? data["GenericHipAbductionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipAbductionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipAbductionStrengthLeft", data.ContainsKey("GenericHipAbductionStrengthLeft") ? data["GenericHipAbductionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipAbductionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Int Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipIntRotROMRight", data.ContainsKey("GenericHipIntRotROMRight") ? data["GenericHipIntRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipIntRotROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipIntRotROMLeft", data.ContainsKey("GenericHipIntRotROMLeft") ? data["GenericHipIntRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipIntRotROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipIntRotStrengthRight", data.ContainsKey("GenericHipIntRotStrengthRight") ? data["GenericHipIntRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipIntRotStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipIntRotStrengthLeft", data.ContainsKey("GenericHipIntRotStrengthLeft") ? data["GenericHipIntRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipIntRotStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Ext Rot
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtRotROMRight", data.ContainsKey("GenericHipExtRotROMRight") ? data["GenericHipExtRotROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtRotROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtRotROMLeft", data.ContainsKey("GenericHipExtRotROMLeft") ? data["GenericHipExtRotROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtRotROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtRotStrengthRight", data.ContainsKey("GenericHipExtRotStrengthRight") ? data["GenericHipExtRotStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtRotStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericHipExtRotStrengthLeft", data.ContainsKey("GenericHipExtRotStrengthLeft") ? data["GenericHipExtRotStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericHipExtRotStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Knee</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeFlexionROMRight", data.ContainsKey("GenericKneeFlexionROMRight") ? data["GenericKneeFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeFlexionROMLeft", data.ContainsKey("GenericKneeFlexionROMLeft") ? data["GenericKneeFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeFlexionStrengthRight", data.ContainsKey("GenericKneeFlexionStrengthRight") ? data["GenericKneeFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeFlexionStrengthLeft", data.ContainsKey("GenericKneeFlexionStrengthLeft") ? data["GenericKneeFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeExtensionROMRight", data.ContainsKey("GenericKneeExtensionROMRight") ? data["GenericKneeExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeExtensionROMLeft", data.ContainsKey("GenericKneeExtensionROMLeft") ? data["GenericKneeExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeExtensionStrengthRight", data.ContainsKey("GenericKneeExtensionStrengthRight") ? data["GenericKneeExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericKneeExtensionStrengthLeft", data.ContainsKey("GenericKneeExtensionStrengthLeft") ? data["GenericKneeExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericKneeExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Ankle</strong>
                            </td><td class="align_left">
                                Plantarflexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnklePlantFlexionROMRight", data.ContainsKey("GenericAnklePlantFlexionROMRight") ? data["GenericAnklePlantFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnklePlantFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnklePlantFlexionROMLeft", data.ContainsKey("GenericAnklePlantFlexionROMLeft") ? data["GenericAnklePlantFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnklePlantFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnklePlantFlexionStrengthRight", data.ContainsKey("GenericAnklePlantFlexionStrengthRight") ? data["GenericAnklePlantFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnklePlantFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnklePlantFlexionStrengthLeft", data.ContainsKey("GenericAnklePlantFlexionStrengthLeft") ? data["GenericAnklePlantFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnklePlantFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Dorsiflexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnkleFlexionROMRight", data.ContainsKey("GenericAnkleFlexionROMRight") ? data["GenericAnkleFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnkleFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnkleFlexionROMLeft", data.ContainsKey("GenericAnkleFlexionROMLeft") ? data["GenericAnkleFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnkleFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnkleFlexionStrengthRight", data.ContainsKey("GenericAnkleFlexionStrengthRight") ? data["GenericAnkleFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnkleFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericAnkleFlexionStrengthLeft", data.ContainsKey("GenericAnkleFlexionStrengthLeft") ? data["GenericAnkleFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericAnkleFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Trunk</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkFlexionROMRight", data.ContainsKey("GenericTrunkFlexionROMRight") ? data["GenericTrunkFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkFlexionROMLeft", data.ContainsKey("GenericTrunkFlexionROMLeft") ? data["GenericTrunkFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkFlexionStrengthRight", data.ContainsKey("GenericTrunkFlexionStrengthRight") ? data["GenericTrunkFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkFlexionStrengthLeft", data.ContainsKey("GenericTrunkFlexionStrengthLeft") ? data["GenericTrunkFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Rotation
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkRotationROMRight", data.ContainsKey("GenericTrunkRotationROMRight") ? data["GenericTrunkRotationROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkRotationROMLeft", data.ContainsKey("GenericTrunkRotationROMLeft") ? data["GenericTrunkRotationROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkRotationStrengthRight", data.ContainsKey("GenericTrunkRotationStrengthRight") ? data["GenericTrunkRotationStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkRotationStrengthLeft", data.ContainsKey("GenericTrunkRotationStrengthLeft") ? data["GenericTrunkRotationStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkRotationStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkExtensionROMRight", data.ContainsKey("GenericTrunkExtensionROMRight") ? data["GenericTrunkExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkExtensionROMLeft", data.ContainsKey("GenericTrunkExtensionROMLeft") ? data["GenericTrunkExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkExtensionStrengthRight", data.ContainsKey("GenericTrunkExtensionStrengthRight") ? data["GenericTrunkExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTrunkExtensionStrengthLeft", data.ContainsKey("GenericTrunkExtensionStrengthLeft") ? data["GenericTrunkExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericTrunkExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <strong>Neck</strong>
                            </td><td class="align_left">
                                Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckFlexionROMRight", data.ContainsKey("GenericNeckFlexionROMRight") ? data["GenericNeckFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckFlexionROMLeft", data.ContainsKey("GenericNeckFlexionROMLeft") ? data["GenericNeckFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckFlexionStrengthRight", data.ContainsKey("GenericNeckFlexionStrengthRight") ? data["GenericNeckFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckFlexionStrengthLeft", data.ContainsKey("GenericNeckFlexionStrengthLeft") ? data["GenericNeckFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Extension
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckExtensionROMRight", data.ContainsKey("GenericNeckExtensionROMRight") ? data["GenericNeckExtensionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckExtensionROMLeft", data.ContainsKey("GenericNeckExtensionROMLeft") ? data["GenericNeckExtensionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckExtensionStrengthRight", data.ContainsKey("GenericNeckExtensionStrengthRight") ? data["GenericNeckExtensionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckExtensionStrengthLeft", data.ContainsKey("GenericNeckExtensionStrengthLeft") ? data["GenericNeckExtensionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckExtensionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Lat Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionROMRight", data.ContainsKey("GenericNeckLatFlexionROMRight") ? data["GenericNeckLatFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionROMLeft", data.ContainsKey("GenericNeckLatFlexionROMLeft") ? data["GenericNeckLatFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionStrengthRight", data.ContainsKey("GenericNeckLatFlexionStrengthRight") ? data["GenericNeckLatFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLatFlexionStrengthLeft", data.ContainsKey("GenericNeckLatFlexionStrengthLeft") ? data["GenericNeckLatFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLatFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Long Flexion
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionROMRight", data.ContainsKey("GenericNeckLongFlexionROMRight") ? data["GenericNeckLongFlexionROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionROMLeft", data.ContainsKey("GenericNeckLongFlexionROMLeft") ? data["GenericNeckLongFlexionROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionStrengthRight", data.ContainsKey("GenericNeckLongFlexionStrengthRight") ? data["GenericNeckLongFlexionStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckLongFlexionStrengthLeft", data.ContainsKey("GenericNeckLongFlexionStrengthLeft") ? data["GenericNeckLongFlexionStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckLongFlexionStrengthLeft" }) %>
                            </td>
                        </tr><tr>
                            <td>
                            </td><td class="align_left">
                                Rotation
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckRotationROMRight", data.ContainsKey("GenericNeckRotationROMRight") ? data["GenericNeckRotationROMRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationROMRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckRotationROMLeft", data.ContainsKey("GenericNeckRotationROMLeft") ? data["GenericNeckRotationROMLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationROMLeft" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckRotationStrengthRight", data.ContainsKey("GenericNeckRotationStrengthRight") ? data["GenericNeckRotationStrengthRight"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationStrengthRight" }) %>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericNeckRotationStrengthLeft", data.ContainsKey("GenericNeckRotationStrengthLeft") ? data["GenericNeckRotationStrengthLeft"].Answer : string.Empty, new { @class = "vitals", @id = Model.Type + "_GenericNeckRotationStrengthLeft" }) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="4">Pain Assessment</th>
        </tr><tr>
            <td colspan="4">
                <label for="<%= Model.Type %>_GenericIntensityOfPain" class="float_left">Pain level:</label>
                <div class="float_right"><%
                    var GenericIntensityOfPain = new SelectList(new[] {
                        new SelectListItem { Text = "0 = No Pain", Value = "0" },
                        new SelectListItem { Text = "1", Value = "1" },
                        new SelectListItem { Text = "2", Value = "2" },
                        new SelectListItem { Text = "3", Value = "3" },
                        new SelectListItem { Text = "4", Value = "4" },
                        new SelectListItem { Text = "Moderate Pain", Value = "5" },
                        new SelectListItem { Text = "6", Value = "6" },
                        new SelectListItem { Text = "7", Value = "7" },
                        new SelectListItem { Text = "8", Value = "8" },
                        new SelectListItem { Text = "9", Value = "9" },
                        new SelectListItem { Text = "10", Value = "10" }
                    }, "Value", "Text", data.ContainsKey("GenericIntensityOfPain") ? data["GenericIntensityOfPain"].Answer : "0"); %>
                    <%= Html.DropDownList(Model.Type + "_GenericIntensityOfPain", GenericIntensityOfPain, new { @id = Model.Type + "_GenericIntensityOfPain", @class = "oe" })%>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPainAssessmentLocation" class="float_left">Location:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPainAssessmentLocation", data.ContainsKey("GenericPainAssessmentLocation") ? data["GenericPainAssessmentLocation"].Answer : string.Empty, new { @id = Model.Type + "_GenericPainAssessmentLocation" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPainAssessmentIncreasedBy" class="float_left">Increased by:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPainAssessmentIncreasedBy", data.ContainsKey("GenericPainAssessmentIncreasedBy") ? data["GenericPainAssessmentIncreasedBy"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPainAssessmentIncreasedBy" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPainAssessmentRelievedBy" class="float_left">Relieved by:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPainAssessmentRelievedBy", data.ContainsKey("GenericPainAssessmentRelievedBy") ? data["GenericPainAssessmentRelievedBy"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPainAssessmentRelievedBy" }) %>
                </div>
                <div class="clear"></div>
            </td>
        </tr><tr>
            <th colspan="4">DME</th>
        </tr><tr>
            <td colspan="4">
                <label for="<%= Model.Type %>_GenericDMEAvailable" class="float_left">Available:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericDMEAvailable", data.ContainsKey("GenericDMEAvailable") ? data["GenericDMEAvailable"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericDMEAvailable" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericDMENeeds" class="float_left">Needs:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericDMENeeds", data.ContainsKey("GenericDMENeeds") ? data["GenericDMENeeds"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericDMENeeds" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericDMESuggestion" class="float_left">Suggestion:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericDMESuggestion", data.ContainsKey("GenericDMESuggestion") ? data["GenericDMESuggestion"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericDMESuggestion" }) %>
                </div>
                <div class="clear"></div>
            </td>
        </tr><tr>
            <th colspan="4">Home Safety Evaluation</th>
        </tr><tr>
            <td colspan="4">
                <label class="float_left">Level</label>
                <div class="float_right">
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLevel", "1", data.ContainsKey("GenericHomeSafetyEvaluationLevel") && data["GenericHomeSafetyEvaluationLevel"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericHomeSafetyEvaluationLevel1", @class = "radio" }) %>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLevel1" class="inlineradio">One</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLevel", "0", data.ContainsKey("GenericHomeSafetyEvaluationLevel") && data["GenericHomeSafetyEvaluationLevel"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericHomeSafetyEvaluationLevel0", @class = "radio" }) %>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLevel0" class="inlineradio">Multiple</label>
                </div>
                <div class="clear"></div>
                <label class="float_left">Stairs</label>
                <div class="float_right">
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationStairs", "1", data.ContainsKey("GenericHomeSafetyEvaluationStairs") && data["GenericHomeSafetyEvaluationStairs"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericHomeSafetyEvaluationStairs1", @class = "radio" }) %>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs1" class="inlineradio">Yes</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationStairs", "0", data.ContainsKey("GenericHomeSafetyEvaluationStairs") && data["GenericHomeSafetyEvaluationStairs"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericHomeSafetyEvaluationStairs0", @class = "radio" }) %>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationStairs0" class="inlineradio">No</label>
                </div>
                <div class="clear"></div>
                <label class="float_left">Lives</label>
                <div class="float_right">
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLives", "2", data.ContainsKey("GenericHomeSafetyEvaluationLives") && data["GenericHomeSafetyEvaluationLives"].Answer == "2" ? true : false, new { @id = Model.Type + "_GenericHomeSafetyEvaluationLives2", @class = "radio" }) %>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLives2" class="inlineradio">Alone</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLives", "1", data.ContainsKey("GenericHomeSafetyEvaluationLives") && data["GenericHomeSafetyEvaluationLives"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericHomeSafetyEvaluationLives1", @class = "radio" }) %>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLives1" class="inlineradio">Family</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationLives", "0", data.ContainsKey("GenericHomeSafetyEvaluationLives") && data["GenericHomeSafetyEvaluationLives"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericHomeSafetyEvaluationLives0", @class = "radio" }) %>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationLives0" class="inlineradio">Friends</label>
                </div>
                <div class="clear"></div>
                <label class="float_left">Support</label>
                <div class="float_right">
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationSupport", "1", data.ContainsKey("GenericHomeSafetyEvaluationSupport") && data["GenericHomeSafetyEvaluationSupport"].Answer == "1" ? true : false, new { @id = Model.Type + "_GenericHomeSafetyEvaluationSupport1", @class = "radio" }) %>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationSupport1" class="inlineradio">Retirement</label>
                    <%= Html.RadioButton(Model.Type + "_GenericHomeSafetyEvaluationSupport", "0", data.ContainsKey("GenericHomeSafetyEvaluationSupport") && data["GenericHomeSafetyEvaluationSupport"].Answer == "0" ? true : false, new { @id = Model.Type + "_GenericHomeSafetyEvaluationSupport0", @class = "radio" }) %>
                    <label for="<%= Model.Type %>_GenericHomeSafetyEvaluationSupport0" class="inlineradio">Assisted Living</label>
                </div>
            </td>
        </tr><tr>
            <th colspan="4">Physical Assessment</th>
        </tr><tr>
            <td colspan="4">
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSpeech" class="float_left">Speech:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSpeech", data.ContainsKey("GenericPhysicalAssessmentSpeech") ? data["GenericPhysicalAssessmentSpeech"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSpeech" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentVision" class="float_left">Vision:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentVision", data.ContainsKey("GenericPhysicalAssessmentVision") ? data["GenericPhysicalAssessmentVision"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentVision" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentHearing" class="float_left">Hearing:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentHearing", data.ContainsKey("GenericPhysicalAssessmentHearing") ? data["GenericPhysicalAssessmentHearing"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentHearing" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSkin" class="float_left">Skin:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSkin", data.ContainsKey("GenericPhysicalAssessmentSkin") ? data["GenericPhysicalAssessmentSkin"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSkin" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentEdema" class="float_left">Edema:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentEdema", data.ContainsKey("GenericPhysicalAssessmentEdema") ? data["GenericPhysicalAssessmentEdema"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentEdema" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentMuscleTone" class="float_left">Muscle Tone:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentMuscleTone", data.ContainsKey("GenericPhysicalAssessmentMuscleTone") ? data["GenericPhysicalAssessmentMuscleTone"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentMuscleTone" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentCoordination" class="float_left">Coordination:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentCoordination", data.ContainsKey("GenericPhysicalAssessmentCoordination") ? data["GenericPhysicalAssessmentCoordination"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentCoordination" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentSensation" class="float_left">Sensation:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentSensation", data.ContainsKey("GenericPhysicalAssessmentSensation") ? data["GenericPhysicalAssessmentSensation"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentSensation" }) %>
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericPhysicalAssessmentEndurance" class="float_left">Endurance:</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericPhysicalAssessmentEndurance", data.ContainsKey("GenericPhysicalAssessmentEndurance") ? data["GenericPhysicalAssessmentEndurance"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPhysicalAssessmentEndurance" })%>
                </div>
            </td>
        </tr><tr>
            <th colspan="5">Bed Mobility</th>
            <th colspan="5">Gait</th>
        </tr><tr>
            <td colspan="5">
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th><strong>Assistive Device</strong></th>
                            <th><strong>% Assist</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericBedMobilityRollingAssistiveDevice" class="strong">Rolling</label>
                                <input type="hidden" name="<%= Model.Type %>_GenericBedMobilityRolling" value="" />
                                <div class="float_right">
                                    <%= string.Format("<input id='{1}_GenericBedMobilityRolling1' class='radio' name='{1}_GenericBedMobilityRolling' value='1' type='checkbox' {0} />", genericBedMobilityRolling != null && genericBedMobilityRolling.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericBedMobilityRolling1" class="inlineradio">L</label>
                                    <%= string.Format("<input id='{1}_GenericBedMobilityRolling2' class='radio' name='{1}_GenericBedMobilityRolling' value='2' type='checkbox' {0} />", genericBedMobilityRolling != null && genericBedMobilityRolling.Contains("2") ? "checked='checked'" : "",Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericBedMobilityRolling2" class="inlineradio">R</label>
                                </div>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingAssistiveDevice", data.ContainsKey("GenericBedMobilityRollingAssistiveDevice") ? data["GenericBedMobilityRollingAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericBedMobilityRollingAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericBedMobilityRollingAssist", data.ContainsKey("GenericBedMobilityRollingAssist") ? data["GenericBedMobilityRollingAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilityRollingAssist" })%> %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericBedMobilitySitStandSitAssistiveDevice" class="strong">Sit Stand Sit</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice", data.ContainsKey("GenericBedMobilitySitStandSitAssistiveDevice") ? data["GenericBedMobilitySitStandSitAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericBedMobilitySitStandSitAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericBedMobilitySitStandSitAssist", data.ContainsKey("GenericBedMobilitySitStandSitAssist") ? data["GenericBedMobilitySitStandSitAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilitySitStandSitAssist" })%> %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericBedMobilitySupToSitAssistiveDevice" class="strong">Sup to Sit</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice", data.ContainsKey("GenericBedMobilitySupToSitAssistiveDevice") ? data["GenericBedMobilitySupToSitAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericBedMobilitySupToSitAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericBedMobilitySupToSitAssist", data.ContainsKey("GenericBedMobilitySupToSitAssist") ? data["GenericBedMobilitySupToSitAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericBedMobilitySupToSitAssist" })%> %
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td><td colspan="5">
                <label for="<%= Model.Type %>_GenericGaitLevelAssist" class="float_left">Level</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericGaitLevelAssist", data.ContainsKey("GenericGaitLevelAssist") ? data["GenericGaitLevelAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericGaitLevelAssist" }) %>
                    X
                    <%= Html.TextBox(Model.Type + "_GenericGaitLevelFeet", data.ContainsKey("GenericGaitLevelFeet") ? data["GenericGaitLevelFeet"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericGaitLevelFeet" }) %>
                    Feet
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericGaitUnLevelAssist" class="float_left">Unlevel</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericGaitUnLevelAssist", data.ContainsKey("GenericGaitUnLevelAssist") ? data["GenericGaitUnLevelAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericGaitUnLevelAssist" }) %>
                    X
                    <%= Html.TextBox(Model.Type + "_GenericGaitUnLevelFeet", data.ContainsKey("GenericGaitUnLevelFeet") ? data["GenericGaitUnLevelFeet"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericGaitUnLevelFeet" }) %>
                    Feet
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericGaitStepStairAssist" class="float_left">Step/Stair</label>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericGaitStepStairAssist", data.ContainsKey("GenericGaitStepStairAssist") ? data["GenericGaitStepStairAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericGaitStepStairAssist" })%>
                    X
                    <%= Html.TextBox(Model.Type + "_GenericGaitStepStairFeet", data.ContainsKey("GenericGaitStepStairFeet") ? data["GenericGaitStepStairFeet"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericGaitStepStairFeet" }) %>
                    Feet
                </div>
                <div class="clear"></div>
                <label for="<%= Model.Type %>_GenericGaitComment" class="strong">Comment</label>
                <%= Html.TextArea(Model.Type + "_GenericGaitComment", data.ContainsKey("GenericGaitComment") ? data["GenericGaitComment"].Answer : string.Empty, new { @id = Model.Type + "_GenericGaitComment", @class = "fill" }) %>
            </td>
        </tr><tr>
            <th colspan="5">Transfer</th>
            <th colspan="5">WBS</th>
        </tr><tr>
            <td colspan="5" rowspan="5">
                <table>
                    <tbody>
                        <tr>
                            <th>
                            </th><th>
                                <strong>Assistive Device</strong>
                            </th><th>
                                <strong>% Assist</strong>
                            </th>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferBedChairAssistiveDevice" class="strong">Bed-Chair</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferBedChairAssistiveDevice", data.ContainsKey("GenericTransferBedChairAssistiveDevice") ? data["GenericTransferBedChairAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericTransferBedChairAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferBedChairAssist", data.ContainsKey("GenericTransferBedChairAssist") ? data["GenericTransferBedChairAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTransferBedChairAssist" })%>
                                %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferChairBedAssistiveDevice" class="strong">Chair-Bed</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferChairBedAssistiveDevice", data.ContainsKey("GenericTransferChairBedAssistiveDevice") ? data["GenericTransferChairBedAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericTransferChairBedAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferChairBedAssist", data.ContainsKey("GenericTransferChairBedAssist") ? data["GenericTransferChairBedAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTransferChairBedAssist" })%>
                                %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferChairToWCAssistiveDevice" class="strong">Chair to W/C</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferChairToWCAssistiveDevice", data.ContainsKey("GenericTransferChairToWCAssistiveDevice") ? data["GenericTransferChairToWCAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericTransferChairToWCAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferChairToWCAssist", data.ContainsKey("GenericTransferChairToWCAssist") ? data["GenericTransferChairToWCAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTransferChairToWCAssist" })%>
                                %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferToiletOrBSCAssistiveDevice" class="strong">Toilet or BSC</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice", data.ContainsKey("GenericTransferToiletOrBSCAssistiveDevice") ? data["GenericTransferToiletOrBSCAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericTransferToiletOrBSCAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferToiletOrBSCAssist", data.ContainsKey("GenericTransferToiletOrBSCAssist") ? data["GenericTransferToiletOrBSCAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTransferToiletOrBSCAssist" })%>
                                %
                            </td>
                        </tr><tr>
                            <td class="align_left">
                                <label for="<%= Model.Type %>_GenericTransferCanVanAssistiveDevice" class="strong">Car/Van</label>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferCanVanAssistiveDevice", data.ContainsKey("GenericTransferCanVanAssistiveDevice") ? data["GenericTransferCanVanAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericTransferCanVanAssistiveDevice" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferCanVanAssist", data.ContainsKey("GenericTransferCanVanAssist") ? data["GenericTransferCanVanAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTransferCanVanAssist" })%>
                                %
                            </td>
                        </tr><tr>
                            <td class="align_left"><label for="<%= Model.Type %>_GenericTransferTubShowerAssistiveDevice" class="strong">Tub/Shower</label></td>
                            <td><%= Html.TextBox(Model.Type + "_GenericTransferTubShowerAssistiveDevice", data.ContainsKey("GenericTransferTubShowerAssistiveDevice") ? data["GenericTransferTubShowerAssistiveDevice"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericTransferTubShowerAssistiveDevice" })%></td>
                            <td><%= Html.TextBox(Model.Type + "_GenericTransferTubShowerAssist", data.ContainsKey("GenericTransferTubShowerAssist") ? data["GenericTransferTubShowerAssist"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTransferTubShowerAssist" })%> %</td>
                        </tr>
                    </tbody>
                </table><table>
                    <tbody> 
                        <tr>
                            <th colspan="2">
                            </th><th>
                                <strong>Static</strong>
                            </th><th>
                                <strong>Dynamic</strong>
                            </th>
                        </tr><tr>
                            <td colspan="2">
                                <label for="<%= Model.Type %>_GenericTransferSittingBalanceStatic" class="float_left">Sitting Balance</label>
                                <input type="hidden" name="<%= Model.Type %>_GenericTransferSittingBalance" value="" />
                                <div class="clear"></div>
                                <div class="float_left">
                                    <%= string.Format("<input id='{1}_GenericTransferSittingBalance1' class='radio' name='{1}_GenericTransferSittingBalance' value='1' type='checkbox' {0} />", genericTransferSittingBalance != null && genericTransferSittingBalance.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTransferSittingBalance1" class="inlineradio">Supported</label>
                                </div><div class="float_left">
                                    <%= string.Format("<input id='{1}_GenericTransferSittingBalance2' class='radio' name='{1}_GenericTransferSittingBalance' value='2' type='checkbox' {0} />", genericTransferSittingBalance != null && genericTransferSittingBalance.Contains("2") ? "checked='checked'" : "",Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTransferSittingBalance2" class="inlineradio">Unsupported</label>
                                </div>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferSittingBalanceStatic", data.ContainsKey("GenericTransferSittingBalanceStatic") ? data["GenericTransferSittingBalanceStatic"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTransferSittingBalanceStatic" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferSittingBalanceDynamic", data.ContainsKey("GenericTransferSittingBalanceDynamic") ? data["GenericTransferSittingBalanceDynamic"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTransferSittingBalanceDynamic" })%>
                                %
                            </td>
                        </tr><tr>
                            <td colspan="2">
                                <label for="<%= Model.Type %>_GenericTransferStandBalanceStatic" class="float_left">Stand Balance</label>
                                <input type="hidden" name="<%= Model.Type %>_GenericTransferStandBalance" value="" />
                                <div class="clear"></div>
                                <div class="float_left">
                                    <%= string.Format("<input id='{1}_GenericTransferStandBalance1' class='radio' name='{1}_GenericTransferStandBalance' value='1' type='checkbox' {0} />", genericTransferStandBalance != null && genericTransferStandBalance.Contains("1") ? "checked='checked'" : "",Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTransferStandBalance1" class="inlineradio">Supported</label>
                                </div><div class="float_left">
                                    <%= string.Format("<input id='{1}_GenericTransferStandBalance2' class='radio' name='{1}_GenericTransferStandBalance' value='2' type='checkbox' {0} />", genericTransferStandBalance != null && genericTransferStandBalance.Contains("2") ? "checked='checked'" : "",Model.Type)%>
                                    <label for="<%= Model.Type %>_GenericTransferStandBalance2" class="inlineradio">Unsupported</label>
                                </div>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferStandBalanceStatic", data.ContainsKey("GenericTransferStandBalanceStatic") ? data["GenericTransferStandBalanceStatic"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTransferStandBalanceStatic" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericTransferStandBalanceDynamic", data.ContainsKey("GenericTransferStandBalanceDynamic") ? data["GenericTransferStandBalanceDynamic"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericTransferStandBalanceDynamic" })%>
                                %
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td><td colspan="5">
                <table>
                    <thead>
                        <tr>
                            <th>
                                <strong>Assistive Device</strong>
                            </th><th>
                                <strong>Description</strong>
                            </th><th>
                                <strong>Posture</strong>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <%= Html.TextBox(Model.Type + "_GenericWBSAssistiveDevice", data.ContainsKey("GenericWBSAssistiveDevice") ? data["GenericWBSAssistiveDevice"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericWBSAssistiveDevice" })%>
                                %
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWBSDescription", data.ContainsKey("GenericWBSDescription") ? data["GenericWBSDescription"].Answer : string.Empty, new { @class = "",@style="width:90%", @id = Model.Type + "_GenericWBSDescription" })%>
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWBSPosture", data.ContainsKey("GenericWBSPosture") ? data["GenericWBSPosture"].Answer : string.Empty, new { @class = "", @style = "width:90%", @id = Model.Type + "_GenericWBSPosture" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="5">W/C Mobility</th>
        </tr><tr>
            <td colspan="5">
                <table>
                    <thead>
                        <tr>
                            <th>
                                <label for="<%= Model.Type %>_GenericWCMobilityLevel" class="strong">Level</label>
                            </th><th>
                                <label for="<%= Model.Type %>_GenericWCMobilityRamp" class="strong">Ramp</label>
                            </th><th>
                                <label for="<%= Model.Type %>_GenericWCMobilityManeuver" class="strong">Maneuver</label>
                            </th><th>
                                <label for="<%= Model.Type %>_GenericWCMobilityADL" class="strong">ADL</label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <%= Html.TextBox(Model.Type + "_GenericWCMobilityLevel", data.ContainsKey("GenericWCMobilityLevel") ? data["GenericWCMobilityLevel"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericWCMobilityLevel" })%>
                                %
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWCMobilityRamp", data.ContainsKey("GenericWCMobilityRamp") ? data["GenericWCMobilityRamp"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericWCMobilityRamp" })%>
                                %
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWCMobilityManeuver", data.ContainsKey("GenericWCMobilityManeuver") ? data["GenericWCMobilityManeuver"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericWCMobilityManeuver" })%>
                                %
                            </td><td>
                                <%= Html.TextBox(Model.Type + "_GenericWCMobilityADL", data.ContainsKey("GenericWCMobilityADL") ? data["GenericWCMobilityADL"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericWCMobilityADL" })%>
                                %
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="5">Assessment</th>
        </tr><tr>
            <td colspan="5">
                <%= Html.TextArea(Model.Type + "_GenericAssessmentComment", data.ContainsKey("GenericAssessmentComment") ? data["GenericAssessmentComment"].Answer : string.Empty, 3,20,new { @id = Model.Type + "_GenericAssessmentComment", @class = "fill" })%>
            </td>
        </tr><tr>
            <th colspan="5">Treatment Codes</th>
            <th colspan="5">Treatment Plan</th>
        </tr><tr>
            <td colspan="5">
                <input type="hidden" name="<%= Model.Type %>_GenericTreatmentCodes" value="" />
                <table class="fixed align_left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes1' class='radio' name='{1}_GenericTreatmentCodes' value='1' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes1">B1 Evaluation</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes2' class='radio' name='{1}_GenericTreatmentCodes' value='2' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes2">B2 Thera Ex</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes3' class='radio' name='{1}_GenericTreatmentCodes' value='3' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes3">B3 Transfer Training</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes4' class='radio' name='{1}_GenericTreatmentCodes' value='4' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes4">B4 Home Program</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes5' class='radio' name='{1}_GenericTreatmentCodes' value='5' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes5">B5 Gait Training</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes6' class='radio' name='{1}_GenericTreatmentCodes' value='6' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes6">B6 Chest PT</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes7' class='radio' name='{1}_GenericTreatmentCodes' value='7' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes7">B7 Ultrasound</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes8' class='radio' name='{1}_GenericTreatmentCodes' value='8' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes8">B8 Electrother</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes9' class='radio' name='{1}_GenericTreatmentCodes' value='9' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("9") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes9">B9 Prosthetic Training</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes10' class='radio' name='{1}_GenericTreatmentCodes' value='10' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("10") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes10">B10 Muscle Re-ed</label>
                            </td><td colspan="2">
                                <%= string.Format("<input id='{1}_GenericTreatmentCodes11' class='radio' name='{1}_GenericTreatmentCodes' value='11' type='checkbox' {0} />", genericTreatmentCodes != null && genericTreatmentCodes.Contains("11") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentCodes11">B11 Muscle Re-ed</label>
                            </td>
                        </tr><tr>
                            <td colspan="3">
                                <label for="<%= Model.Type %>_GenericTreatmentCodesOther" class="strong">Other</label>
                                <%= Html.TextBox(Model.Type + "_GenericTreatmentCodesOther", data != null && data.ContainsKey("GenericTreatmentCodesOther") && data["GenericTreatmentCodesOther"].Answer.IsNotNullOrEmpty() ? data["GenericTreatmentCodesOther"].Answer : string.Empty, new { @id = Model.Type + "_GenericTreatmentCodesOther" }) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td><td colspan="5">
                <input type="hidden" name="<%= Model.Type %>_GenericTreatmentPlan" value="" />
                <table class="fixed align_left">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan1' class='radio' name='{1}_GenericTreatmentPlan' value='1' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan1">Thera Ex</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan2' class='radio' name='{1}_GenericTreatmentPlan' value='2' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan2">Bed Mobility Training</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan3' class='radio' name='{1}_GenericTreatmentPlan' value='3' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan3">Transfer Training</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan4' class='radio' name='{1}_GenericTreatmentPlan' value='4' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan4">Balance Training</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan5' class='radio' name='{1}_GenericTreatmentPlan' value='5' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("5") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan5">Gait Training</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan6' class='radio' name='{1}_GenericTreatmentPlan' value='6' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("6") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan6">HEP</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan7' class='radio' name='{1}_GenericTreatmentPlan' value='7' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("7") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan7">Electrotherapy</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan8' class='radio' name='{1}_GenericTreatmentPlan' value='8' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("8") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan8">Ultrasound</label>
                            </td><td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan9' class='radio' name='{1}_GenericTreatmentPlan' value='9' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("9") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan9">Prosthetic Training</label>
                            </td>
                        </tr><tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericTreatmentPlan10' class='radio' name='{1}_GenericTreatmentPlan' value='10' type='checkbox' {0} />", genericTreatmentPlan != null && genericTreatmentPlan.Contains("10") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericTreatmentPlan10">Manual Therapy</label>
                            </td><td colspan="2">
                                <label for="<%= Model.Type %>_GenericTreatmentPlanOther" class="strong">Other</label>
                                <%= Html.TextBox(Model.Type + "_GenericTreatmentPlanOther", data != null && data.ContainsKey("GenericTreatmentPlanOther") && data["GenericTreatmentPlanOther"].Answer.IsNotNullOrEmpty() ? data["GenericTreatmentPlanOther"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericTreatmentCodesOther" })%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="5">Short Term Goals</th>
            <th colspan="5">Long Term Goals</th>
        </tr><tr>
            <td colspan="5">
                <div class="float_right strong">Time Frame</div>
                <div class="clear"></div>
                <div class="float_left">
                    <label for="<%= Model.Type %>_GenericShortTermGoal1">1.</label>
                    <%= Html.TextBox(Model.Type + "_GenericShortTermGoal1", data.ContainsKey("GenericShortTermGoal1") ? data["GenericShortTermGoal1"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermGoal1" })%>
                </div>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericShortTermGoal1TimeFrame", data.ContainsKey("GenericShortTermGoal1TimeFrame") ? data["GenericShortTermGoal1TimeFrame"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermGoal1TimeFrame" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <label for="<%= Model.Type %>_GenericShortTermGoal2">2.</label>
                    <%= Html.TextBox(Model.Type + "_GenericShortTermGoal2", data.ContainsKey("GenericShortTermGoal2") ? data["GenericShortTermGoal2"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermGoal2" })%>
                </div>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericShortTermGoal2TimeFrame", data.ContainsKey("GenericShortTermGoal2TimeFrame") ? data["GenericShortTermGoal2TimeFrame"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermGoal2TimeFrame" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <label for="<%= Model.Type %>_GenericShortTermGoal3">3.</label>
                    <%= Html.TextBox(Model.Type + "_GenericShortTermGoal3", data.ContainsKey("GenericShortTermGoal3") ? data["GenericShortTermGoal3"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermGoal3" })%>
                </div>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericShortTermGoal3TimeFrame", data.ContainsKey("GenericShortTermGoal3TimeFrame") ? data["GenericShortTermGoal3TimeFrame"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermGoal3TimeFrame" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <label for="<%= Model.Type %>_GenericShortTermGoal4">4.</label>
                    <%= Html.TextBox(Model.Type + "_GenericShortTermGoal4", data.ContainsKey("GenericShortTermGoal4") ? data["GenericShortTermGoal4"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermGoal4" })%>
                </div>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericShortTermGoal4TimeFrame", data.ContainsKey("GenericShortTermGoal4TimeFrame") ? data["GenericShortTermGoal4TimeFrame"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermGoal4TimeFrame" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <label for="<%= Model.Type %>_GenericShortTermGoal5">5.</label>
                    <%= Html.TextBox(Model.Type + "_GenericShortTermGoal5", data.ContainsKey("GenericShortTermGoal5") ? data["GenericShortTermGoal5"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermGoal5" })%>
                </div>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericShortTermGoal5TimeFrame", data.ContainsKey("GenericShortTermGoal5TimeFrame") ? data["GenericShortTermGoal5TimeFrame"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericShortTermGoal5TimeFrame" })%>
                </div>
            </td><td colspan="5">
                <div class="float_right strong">Time Frame</div>
                <div class="clear"></div>
                <div class="float_left">
                    <label for="<%= Model.Type %>_GenericLongTermGoal1">1.</label>
                    <%= Html.TextBox(Model.Type + "_GenericLongTermGoal1", data.ContainsKey("GenericLongTermGoal1") ? data["GenericLongTermGoal1"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermGoal1" })%>
                </div>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericLongTermGoal1TimeFrame", data.ContainsKey("GenericLongTermGoal1TimeFrame") ? data["GenericLongTermGoal1TimeFrame"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermGoal1TimeFrame" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <label for="<%= Model.Type %>_GenericLongTermGoal2">2.</label>
                    <%= Html.TextBox(Model.Type + "_GenericLongTermGoal2", data.ContainsKey("GenericLongTermGoal2") ? data["GenericLongTermGoal2"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermGoal2" })%>
                </div>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericLongTermGoal2TimeFrame", data.ContainsKey("GenericLongTermGoal2TimeFrame") ? data["GenericLongTermGoal2TimeFrame"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermGoal2TimeFrame" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <label for="<%= Model.Type %>_GenericLongTermGoal3">3.</label>
                    <%= Html.TextBox(Model.Type + "_GenericLongTermGoal3", data.ContainsKey("GenericLongTermGoal3") ? data["GenericLongTermGoal3"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermGoal3" })%>
                </div>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericLongTermGoal3TimeFrame", data.ContainsKey("GenericLongTermGoal3TimeFrame") ? data["GenericLongTermGoal3TimeFrame"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermGoal3TimeFrame" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <label for="<%= Model.Type %>_GenericLongTermGoal4">4.</label>
                    <%= Html.TextBox(Model.Type + "_GenericLongTermGoal4", data.ContainsKey("GenericLongTermGoal4") ? data["GenericLongTermGoal4"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermGoal4" })%>
                </div>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericLongTermGoal4TimeFrame", data.ContainsKey("GenericLongTermGoal4TimeFrame") ? data["GenericLongTermGoal4TimeFrame"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermGoal4TimeFrame" })%>
                </div>
                <div class="clear"></div>
                <div class="float_left">
                    <label for="<%= Model.Type %>_GenericLongTermGoal5">5.</label>
                    <%= Html.TextBox(Model.Type + "_GenericLongTermGoal5", data.ContainsKey("GenericLongTermGoal5") ? data["GenericLongTermGoal5"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermGoal5" })%>
                </div>
                <div class="float_right">
                    <%= Html.TextBox(Model.Type + "_GenericLongTermGoal5TimeFrame", data.ContainsKey("GenericLongTermGoal5TimeFrame") ? data["GenericLongTermGoal5TimeFrame"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericLongTermGoal5TimeFrame" })%>
                </div>
            </td>
        </tr><tr>
            <th colspan="10">Other Discipline Recommendation</th>
        </tr><tr>
            <td colspan="10">
                <input type="hidden" name="<%= Model.Type %>_GenericDisciplineRecommendation" value="" />
                <table class="fixed">
                    <tbody>
                        <tr>
                            <td>
                                <%= string.Format("<input id='{1}_GenericDisciplineRecommendation1' class='radio' name='{1}_GenericDisciplineRecommendation' value='1' type='checkbox' {0} />", genericDisciplineRecommendation != null && genericDisciplineRecommendation.Contains("1") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendation1">OT</label>
                                <%= string.Format("<input id='{1}_GenericDisciplineRecommendation2' class='radio' name='{1}_GenericDisciplineRecommendation' value='2' type='checkbox' {0} />", genericDisciplineRecommendation != null && genericDisciplineRecommendation.Contains("2") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendation2">MSW</label>
                                <%= string.Format("<input id='{1}_GenericDisciplineRecommendation3' class='radio' name='{1}_GenericDisciplineRecommendation' value='3' type='checkbox' {0} />", genericDisciplineRecommendation != null && genericDisciplineRecommendation.Contains("3") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendation3">ST</label>
                                <%= string.Format("<input id='{1}_GenericDisciplineRecommendation4' class='radio' name='{1}_GenericDisciplineRecommendation' value='4' type='checkbox' {0} />", genericDisciplineRecommendation != null && genericDisciplineRecommendation.Contains("4") ? "checked='checked'" : "", Model.Type)%>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendation4">Podiatrist</label>
                            </td><td>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendationOther" class="float_left">Other</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericDisciplineRecommendationOther", data.ContainsKey("GenericDisciplineRecommendationOther") ? data["GenericDisciplineRecommendationOther"].Answer : string.Empty, new { @id = Model.Type + "_GenericDisciplineRecommendationOther" })%>
                                </div>
                            </td><td>
                                <label for="<%= Model.Type %>_GenericDisciplineRecommendationReason" class="float_left">Reason</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericDisciplineRecommendationReason", data.ContainsKey("GenericDisciplineRecommendationReason") ? data["GenericDisciplineRecommendationReason"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericDisciplineRecommendationReason" })%>
                                </div>
                            </td>
                        </tr><tr>
                            <td>
                                <label for="<%= Model.Type %>_GenericFrequency" class="float_left">Frequency:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericFrequency", data.ContainsKey("GenericFrequency") ? data["GenericFrequency"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericFrequency" })%>
                                    X wk for
                                    <%= Html.TextBox(Model.Type + "_GenericFrequencyFor", data.ContainsKey("GenericFrequencyFor") ? data["GenericFrequencyFor"].Answer : string.Empty, new { @class = "sn", @id = Model.Type + "_GenericFrequencyFor" })%>
                                </div>
                            </td><td>
                                <label for="<%= Model.Type %>_GenericRehabPotential" class="float_left">Rehab Potential:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericRehabPotential", data.ContainsKey("GenericRehabPotential") ? data["GenericRehabPotential"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericRehabPotential" })%>
                                </div>
                            </td><td>
                                <label for="<%= Model.Type %>_GenericPrognosis" class="float_left">Prognosis:</label>
                                <div class="float_right">
                                    <%= Html.TextBox(Model.Type + "_GenericPrognosis", data.ContainsKey("GenericPrognosis") ? data["GenericPrognosis"].Answer : string.Empty, new { @class = "", @id = Model.Type + "_GenericPrognosis" })%>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr><tr>
            <th colspan="5">Care Coordination</th>
            <th colspan="5">Skilled Care Provided This Visit</th>
        </tr><tr>
            <td colspan="5">
                <%= Html.TextArea(Model.Type + "_GenericCareCoordination", data.ContainsKey("GenericCareCoordination") ? data["GenericCareCoordination"].Answer : string.Empty, 4, 20, new { @id = Model.Type + "_GenericCareCoordination", @class = "fill" })%>
            </td><td colspan="5">
                <%= Html.TextArea(Model.Type + "_GenericSkilledCareProvided", data.ContainsKey("GenericSkilledCareProvided") ? data["GenericSkilledCareProvided"].Answer : string.Empty, 3, 20, new { @id = Model.Type + "_GenericSkilledCareProvided", @class = "fill" })%>
            </td>
        </tr>       
    </tbody>
</table>