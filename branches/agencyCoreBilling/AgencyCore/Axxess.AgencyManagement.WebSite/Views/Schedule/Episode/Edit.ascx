﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientEpisode>" %>
<% using (Html.BeginForm("UpdateEpisode", "Schedule", FormMethod.Post, new { @id = "editEpisodeForm" })){ %>
   <%= Html.Hidden("Id", Model.Id) %>
   <%= Html.Hidden("PatientId", Model.PatientId) %>
<div class="wrapper main">
    <span class="bigtext align_center">Edit Episode: <%=Model.DisplayName %></span>
    <fieldset>
        <legend>Patient</legend>
        <div class="column"><div class="row"><span class="bigtext"><%= Model.DisplayName %></span></div></div>
        <div class="column"><div class="row"><label for="Edit_Episode_StartOfCareDate" class="float_left">Start Of Care Date:</label><div class="float_right"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @class = "Edit_Episode_AdmissionId" })%></div></div></div>
    </fieldset>
    <% if (Model.PreviousEpisode != null) { %><label class="bold">Tip:</label><em>Previous episode end date is: <%= Model.PreviousEpisode.EndDate.ToShortDateString() %></em><% } %>
    <% if (Model.NextEpisode != null) { %><label class="bold">Tip:</label><em>Next episode start date is: <%= Model.NextEpisode.StartDate.ToShortDateString() %></em><% } %>
    <fieldset>
        <legend>Details</legend>
        <table class="form"><tbody>
            <tr>
                <td><label for="Edit_Episode_TargetDate" class="float_left">Episode Start Date:</label><br /><input type="date" name="StartDate" value="<%= Model.StartDate.ToShortDateString() %>" onchange="Schedule.editEpisodeStartDateOnChange()" id="Edit_Episode_StartDate" class="required" style="width:205px" /></td>
                <td><label for="Edit_Episode_VisitDate" class="float_left">Episode End Date:</label><br /><input type="date" name="EndDate" value="<%= Model.EndDateFormatted %>" id="Edit_Episode_EndDate" class="required" /></td>
                <td><label for="Edit_Episode_IsActive" class="float_left">Inactivate Episode:</label><br /><%= Html.CheckBox("IsActive",!Model.IsActive, new { @id = "Edit_Episode_IsActive", @class = "radio" })%></td>
            </tr>
            <tr>
                <td><label for="Edit_Episode_CaseManager" class="float_left">Case Manager:</label><br /><%= Html.LookupSelectList(SelectListTypes.Users, "Detail.CaseManager", Model.Detail.CaseManager, new { @id = "Edit_Episode_CaseManager", @class = "Users required valid" })%></td>
                <td><label for="Edit_Episode_PrimaryInsurance" class="float_left">Primary Insurance:</label><br /><%= Html.Insurances("Detail.PrimaryInsurance", Model.Detail.PrimaryInsurance, false, new { @id = "Edit_Episode_PrimaryInsurance", @class = "Insurances" })%></td>
                <td> <label for="Edit_Episode_SecondaryInsurance" class="float_left">Secondary Insurance:</label><br /><%= Html.Insurances("Detail.SecondaryInsurance", Model.Detail.SecondaryInsurance, false, new { @id = "Edit_Episode_SecondaryInsurance", @class = "Insurances" })%></td>
            </tr>
            <tr>
                <td><label for="Edit_Episode_PrimaryPhysician" class="float_left">Primary Physician:</label><br /><%= Html.TextBox("Detail.PrimaryPhysician", Model.Detail.PrimaryPhysician, new { @id = "Edit_Episode_PrimaryPhysician", @class = "Physicians" })%></td>
                <td></td>
                <td></td>
            </tr>
        </tbody></table>
    </fieldset>
    <fieldset>
        <legend>Comments <span class="img icon note_blue"></span><span style="font-weight: normal;">(Blue Sticky Note)</span></legend>
        <div class="wide_column"><div class="row"><textarea id="Edit_Episode_Comments" name="Detail.Comments" cols="" rows="10"><%= Model.Detail.Comments %></textarea></div></div>
        <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadEpisodeLog('{0}','{1}');\" >Activity Logs</a>", Model.Id, Model.PatientId)%></div>
    </fieldset>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Save</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li></ul></div>
</div>
<script type="text/javascript">$("#Edit_Episode_PrimaryPhysician").PhysicianInput();</script>
<% } %>
