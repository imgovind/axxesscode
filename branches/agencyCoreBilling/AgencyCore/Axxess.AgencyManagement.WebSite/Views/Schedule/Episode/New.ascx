﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NewEpisodeData>" %>
<% using (Html.BeginForm("AddEpisode", "Schedule", FormMethod.Post, new { @id = "newEpisodeForm" })){%>
<div class="wrapper main">
<span class="bigtext align_center">New Episode<%= Model.PatientId != Guid.Empty ? ": " + Model.DisplayName : "" %></span>
    <fieldset>
        <legend>Patient</legend>
        <div class="column"><div class="row"><label for="New_Episode_PatientId" class="float_left">Patient:</label><div class="float_right"><%= Html.Hidden("PatientId", Model.PatientId.ToString(), new { @id = "New_Episode_PatientId"})%><%=Model.DisplayName %></div></div></div>
        <div class="column"><div class="row"><label for="New_Episode_AdmissionId" class="float_left">Start Of Care Date:</label><div class="float_right"><%= Html.DropDownList("AdmissionId", Model.AdmissionDates, new { @class = "New_Episode_AdmissionId" })%> </div></div></div>
    </fieldset>
    <span id="newEpisodeTip"><% if (Model != null && Model.EndDate != DateTime.MinValue){  %><label class="bold" >Tip:</label><em> Last Episode end date is :- <%= Model.EndDateFormatted %></em><% } %></span>
    <fieldset>
        <legend>Details</legend>
        <div class="column">
            <div class="row"><label for="New_Episode_TargetDate" class="float_left">Episode Start Date:</label><div class="float_right"><input type="date" name="StartDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(1).ToShortDateString() : DateTime.Today.ToShortDateString() %>" onchange="Schedule.newEpisodeStartDateOnChange()" id="New_Episode_StartDate" class="required" /></div></div>
            <div class="row"><label for="New_Episode_EndDate" class="float_left">Episode End Date:</label><div class="float_right"><input type="date" name="EndDate" value="<%= Model.EndDate != DateTime.MinValue ? Model.EndDate.AddDays(60).ToShortDateString() : DateTime.Today.AddDays(59).ToShortDateString() %>" id="New_Episode_EndDate" class="required" /></div></div>
            <div class="row"><label for="New_Episode_CaseManager" class="float_left">Case Manager:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Users, "Detail.CaseManager", Model.CaseManager.IsNotNullOrEmpty() ? Model.CaseManager : Guid.Empty.ToString(), new { @id = "New_Episode_CaseManager", @class = "Users required valid" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Episode_PrimaryPhysician" class="float_left">Primary Physician:</label><div class="float_right"><%= Html.TextBox("Detail.PrimaryPhysician", Model.PrimaryPhysician, new { @id = "New_Episode_PrimaryPhysician", @class = "Physicians" })%></div></div>
            <div class="row"><label for="New_Episode_PrimaryInsurance" class="float_left">Primary Insurance:</label><div class="float_right"><%= Html.Insurances("Detail.PrimaryInsurance", Model.PrimaryInsurance, false, new { @id = "New_Episode_PrimaryInsurance", @class = "Insurances" })%></div></div>
            <div class="row"><label for="New_Episode_SecondaryInsurance" class="float_left">Secondary Insurance:</label><div class="float_right"><%= Html.Insurances("Detail.SecondaryInsurance", Model.SecondaryInsurance, false, new { @id = "New_Episode_SecondaryInsurance", @class = "Insurances" })%></div></div>
        </div>
    </fieldset>
    <fieldset><legend>Comments <span class="img icon note_blue"></span><span style="font-weight: normal;">(Blue Sticky Note)</span></legend><div class="wide_column"><div class="row"><textarea id="New_Episode_Comments" name="Detail.Comments" rows="10"  cols=""></textarea></div></div></fieldset>
    <div class="clear"></div>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Save</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li></ul></div>
</div>
<%} %>
<script type="text/javascript">    $("#New_Episode_PrimaryPhysician").PhysicianInput(); </script>