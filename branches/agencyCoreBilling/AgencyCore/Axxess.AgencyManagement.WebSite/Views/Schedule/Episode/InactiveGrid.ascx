﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<EpisodeLean>>" %>
 <ul><li class="align_center"><h3>Inactive Episode(s)</h3></li><li><span class="range align_center">Episode Range</span><span class="action align_center">Action</span></li></ul>
 <ol>
    <%if (Model != null && Model.Count > 0)
      {
          int i = 1;
          foreach (var episode in Model)
          { %>
        <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd" : "even"))%>
            <span class="range align_center"><%=episode.Range%></span>
            <span class="action align_center"><a href="javascript:void(0);" onclick="Schedule.ActivateEpisode('<%=episode.Id %>', '<%= episode.PatientId %>');" >Activate</a> | <a href="javascript:void(0);" onclick="UserInterface.ShowEditEpisodeModal('<%=episode.Id %>','<%= episode.PatientId %>');">Edit</a></span>
        </li>
    <% i++;
          }
      }
      else
      { %><li class="align_center"><span ><strong>There is no inactive episode for this patient.</strong></span> </li> <%} %>
  </ol>
