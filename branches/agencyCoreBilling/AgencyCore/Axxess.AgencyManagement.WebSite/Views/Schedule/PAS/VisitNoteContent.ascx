﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
     <% string[] isVitalSignParameter = data.ContainsKey("IsVitalSignParameter") && data["IsVitalSignParameter"].Answer != "" ? data["IsVitalSignParameter"].Answer.Split(',') : null; %><input name="PASVisit_IsVitalSignParameter" value=" " type="hidden" />
        <tr><th>Vital Sign Parameters &#8212; <%= string.Format("<input class='radio' id='PASVisit_IsVitalSignParameter' name='PASVisit_IsVitalSignParameter' value='1' type='checkbox' {0} />", isVitalSignParameter != null && isVitalSignParameter.Contains("1") ? "checked='checked'" : "")%><label for="PASVisit_IsVitalSignParameter">N/A</label></th><% string[] isVitalSigns = data.ContainsKey("IsVitalSigns") && data["IsVitalSigns"].Answer != "" ? data["IsVitalSigns"].Answer.Split(',') : null; %><input name="PASVisit_IsVitalSigns" value=" " type="hidden" /><th>Vital Signs&#8212; <%= string.Format("<input class='radio' id='PASVisit_IsVitalSigns' name='PASVisit_IsVitalSigns' value='1' type='checkbox' {0} />", isVitalSigns != null && isVitalSigns.Contains("1") ? "checked='checked'" : "")%><label for="PASVisit_IsVitalSigns">N/A</label></th></tr>
        <tr>
            <td>
                <table class="fixed vitalsigns">
                    <tbody>
                        <tr><th></th><th>SBP</th><th>DBP</th><th>HR</th><th>Resp</th><th>Temp</th><th>Weight</th></tr>
                        <tr>
                            <th>greater than (&#62;)</th>
                            <td><%= Html.TextBox("PASVisit_SystolicBPGreaterThan", data.ContainsKey("SystolicBPGreaterThan") ? data["SystolicBPGreaterThan"].Answer : string.Empty, new { @id = "PASVisit_SystolicBPGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_DiastolicBPGreaterThan", data.ContainsKey("DiastolicBPGreaterThan") ? data["DiastolicBPGreaterThan"].Answer : string.Empty, new { @id = "PASVisit_DiastolicBPGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_PulseGreaterThan", data.ContainsKey("PulseGreaterThan") ? data["PulseGreaterThan"].Answer : string.Empty, new { @id = "PASVisit_PulseGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_RespirationGreaterThan", data.ContainsKey("RespirationGreaterThan") ? data["RespirationGreaterThan"].Answer : string.Empty, new { @id = "PASVisit_RespirationGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_TempGreaterThan", data.ContainsKey("TempGreaterThan") ? data["TempGreaterThan"].Answer : string.Empty, new { @id = "PASVisit_TempGreaterThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_WeightGreaterThan", data.ContainsKey("WeightGreaterThan") ? data["WeightGreaterThan"].Answer : string.Empty, new { @id = "PASVisit_WeightGreaterThan", @class = "fill" })%></td>
                        </tr><tr>
                            <th>less than (&#60;)</th>
                            <td><%= Html.TextBox("PASVisit_SystolicBPLessThan", data.ContainsKey("SystolicBPLessThan") ? data["SystolicBPLessThan"].Answer : string.Empty, new { @id = "PASVisit_SystolicBPLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_DiastolicBPLessThan", data.ContainsKey("DiastolicBPLessThan") ? data["DiastolicBPLessThan"].Answer : string.Empty, new { @id = "PASVisit_DiastolicBPLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_PulseLessThan", data.ContainsKey("PulseLessThan") ? data["PulseLessThan"].Answer : string.Empty, new { @id = "PASVisit_PulseLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_RespirationLessThan", data.ContainsKey("RespirationLessThan") ? data["RespirationLessThan"].Answer : string.Empty, new { @id = "PASVisit_RespirationLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_TempLessThan", data.ContainsKey("TempLessThan") ? data["TempLessThan"].Answer : string.Empty, new { @id = "PASVisit_TempLessThan", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_WeightLessThan", data.ContainsKey("WeightLessThan") ? data["WeightLessThan"].Answer : string.Empty, new { @id = "PASVisit_WeightLessThan", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td><td>
                <table class="fixed vitalsignparameter">
                    <tbody>
                        <tr><th></th><th>BP</th><th>HR</th><th>Temp</th><th>Resp</th><th>Weight</th></tr>
                        <tr>
                            <th>Val.</th>
                            <td><%= Html.TextBox("PASVisit_VitalSignBPVal", data.ContainsKey("VitalSignBPVal") ? data["VitalSignBPVal"].Answer : "", new { @id = "PASVisit_VitalSignBPVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_VitalSignHRVal", data.ContainsKey("VitalSignHRVal") ? data["VitalSignHRVal"].Answer : "", new { @id = "PASVisit_VitalSignHRVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_VitalSignTempVal", data.ContainsKey("VitalSignTempVal") ? data["VitalSignTempVal"].Answer : "", new { @id = "PASVisit_VitalSignTempVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_VitalSignRespVal", data.ContainsKey("VitalSignRespVal") ? data["VitalSignRespVal"].Answer : "", new { @id = "PASVisit_VitalSignRespVal", @class = "fill" })%></td>
                            <td><%= Html.TextBox("PASVisit_VitalSignWeightVal", data.ContainsKey("VitalSignWeightVal") ? data["VitalSignWeightVal"].Answer : "", new { @id = "PASVisit_VitalSignWeightVal", @class = "fill" })%></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><th colspan="2">Tasks</th></tr>
        <tr>
            <td>
                <table class="fixed">
                    <thead>
                        <tr><th colspan="3" class="align_left">Assignment</th><th colspan="3">Status</th></tr>
                        <tr><th colspan="3" class="align_left">Personal Care</th><th>Completed</th><th>Refuse</th><th>N/A</th></tr>
                        <tr>
                            <td colspan="3" class="align_left">Bed Bath</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareBedBath", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareBedBath", "2", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareBedBath", "1", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareBedBath", "0", data.ContainsKey("PersonalCareBedBath") && data["PersonalCareBedBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with Chair Bath</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareAssistWithChairBath", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareAssistWithChairBath", "2", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareAssistWithChairBath", "1", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareAssistWithChairBath", "0", data.ContainsKey("PersonalCareAssistWithChairBath") && data["PersonalCareAssistWithChairBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Tub Bath</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareTubBath", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareTubBath", "2", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareTubBath", "1", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareTubBath", "0", data.ContainsKey("PersonalCareTubBath") && data["PersonalCareTubBath"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Shower</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareShower", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareShower", "2", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareShower", "1", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareShower", "0", data.ContainsKey("PersonalCareShower") && data["PersonalCareShower"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Shower w/Chair</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareShowerWithChair", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareShowerWithChair", "2", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareShowerWithChair", "1", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareShowerWithChair", "0", data.ContainsKey("PersonalCareShowerWithChair") && data["PersonalCareShowerWithChair"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Shampoo Hair</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareShampooHair", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareShampooHair", "2", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareShampooHair", "1", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareShampooHair", "0", data.ContainsKey("PersonalCareShampooHair") && data["PersonalCareShampooHair"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Hair Care/Comb Hair</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareHairCare", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareHairCare", "2", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareHairCare", "1", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareHairCare", "0", data.ContainsKey("PersonalCareHairCare") && data["PersonalCareHairCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Oral Care</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareOralCare", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareOralCare", "2", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareOralCare", "1", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareOralCare", "0", data.ContainsKey("PersonalCareOralCare") && data["PersonalCareOralCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Skin Care</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareSkinCare", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareSkinCare", "2", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareSkinCare", "1", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareSkinCare", "0", data.ContainsKey("PersonalCareSkinCare") && data["PersonalCareSkinCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Pericare</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCarePericare", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCarePericare", "2", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCarePericare", "1", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCarePericare", "0", data.ContainsKey("PersonalCarePericare") && data["PersonalCarePericare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Nail Care</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareNailCare", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareNailCare", "2", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareNailCare", "1", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareNailCare", "0", data.ContainsKey("PersonalCareNailCare") && data["PersonalCareNailCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Shave</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareShave", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareShave", "2", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareShave", "1", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareShave", "0", data.ContainsKey("PersonalCareShave") && data["PersonalCareShave"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with Dressing</td>
                            <td><%= Html.Hidden("PASVisit_PersonalCareAssistWithDressing", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_PersonalCareAssistWithDressing", "2", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareAssistWithDressing", "1", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_PersonalCareAssistWithDressing", "0", data.ContainsKey("PersonalCareAssistWithDressing") && data["PersonalCareAssistWithDressing"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <th colspan="3" class="align_left">Nutrition</th><th>Completed</th><th>Refuse</th><th>N/A</th>
                        </tr><tr>
                            <td colspan="3" class="align_left">Meal Set-up</td>
                            <td><%= Html.Hidden("PASVisit_NutritionMealSetUp", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_NutritionMealSetUp", "2", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_NutritionMealSetUp", "1", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_NutritionMealSetUp", "0", data.ContainsKey("NutritionMealSetUp") && data["NutritionMealSetUp"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with Feeding</td>
                            <td><%= Html.Hidden("PASVisit_NutritioAssistWithFeeding", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_NutritioAssistWithFeeding", "2", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_NutritioAssistWithFeeding", "1", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_NutritioAssistWithFeeding", "0", data.ContainsKey("NutritioAssistWithFeeding") && data["NutritioAssistWithFeeding"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr>
                    </thead>
                </table>
            </td><td>
                <table class="fixed">
                    <thead>
                        <tr><th colspan="3" class="align_left">Assignment</th><th colspan="3">Status</th></tr>
                        <tr><th colspan="3" class="align_left">Elimination</th><th>Completed</th><th>Refuse</th><th>N/A</th></tr>
                        <tr>
                            <td colspan="3" class="align_left">Assist with Bed Pan/Urinal</td>
                            <td><%= Html.Hidden("PASVisit_EliminationAssistWithBedPan", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_EliminationAssistWithBedPan", "2", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationAssistWithBedPan", "1", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationAssistWithBedPan", "0", data.ContainsKey("EliminationAssistWithBedPan") && data["EliminationAssistWithBedPan"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with BSC</td>
                            <td><%= Html.Hidden("PASVisit_EliminationAssistBSC", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_EliminationAssistBSC", "2", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationAssistBSC", "1", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationAssistBSC", "0", data.ContainsKey("EliminationAssistBSC") && data["EliminationAssistBSC"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Incontinence Care</td>
                            <td><%= Html.Hidden("PASVisit_EliminationIncontinenceCare", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_EliminationIncontinenceCare", "2", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationIncontinenceCare", "1", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationIncontinenceCare", "0", data.ContainsKey("EliminationIncontinenceCare") && data["EliminationIncontinenceCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Empty Drainage Bag</td>
                            <td><%= Html.Hidden("PASVisit_EliminationEmptyDrainageBag", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_EliminationEmptyDrainageBag", "2", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationEmptyDrainageBag", "1", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationEmptyDrainageBag", "0", data.ContainsKey("EliminationEmptyDrainageBag") && data["EliminationEmptyDrainageBag"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Record Bowel Movement</td>
                            <td><%= Html.Hidden("PASVisit_EliminationRecordBowelMovement", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_EliminationRecordBowelMovement", "2", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationRecordBowelMovement", "1", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationRecordBowelMovement", "0", data.ContainsKey("EliminationRecordBowelMovement") && data["EliminationRecordBowelMovement"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Catheter Care</td>
                            <td><%= Html.Hidden("PASVisit_EliminationCatheterCare", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_EliminationCatheterCare", "2", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationCatheterCare", "1", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_EliminationCatheterCare", "0", data.ContainsKey("EliminationCatheterCare") && data["EliminationCatheterCare"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <th colspan="3" class="align_left">Activity</th><th>Completed</th><th>Refuse</th><th>N/A</th>
                        </tr><tr>
                            <td colspan="3" class="align_left">Dangle on Side of Bed</td>
                            <td><%= Html.Hidden("PASVisit_ActivityDangleOnSideOfBed", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_ActivityDangleOnSideOfBed", "2", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_ActivityDangleOnSideOfBed", "1", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_ActivityDangleOnSideOfBed", "0", data.ContainsKey("ActivityDangleOnSideOfBed") && data["ActivityDangleOnSideOfBed"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Turn &#38; Position</td>
                            <td><%= Html.Hidden("PASVisit_ActivityTurnPosition", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_ActivityTurnPosition", "2", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_ActivityTurnPosition", "1", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_ActivityTurnPosition", "0", data.ContainsKey("ActivityTurnPosition") && data["ActivityTurnPosition"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with Transfer</td>
                            <td><%= Html.Hidden("PASVisit_ActivityAssistWithTransfer", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_ActivityAssistWithTransfer", "2", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_ActivityAssistWithTransfer", "1", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_ActivityAssistWithTransfer", "0", data.ContainsKey("ActivityAssistWithTransfer") && data["ActivityAssistWithTransfer"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Assist with Ambulation</td>
                            <td><%= Html.Hidden("PASVisit_ActivityAssistWithAmbulation", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_ActivityAssistWithAmbulation", "2", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_ActivityAssistWithAmbulation", "1", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_ActivityAssistWithAmbulation", "0", data.ContainsKey("ActivityAssistWithAmbulation") && data["ActivityAssistWithAmbulation"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Range of Motion</td>
                            <td><%= Html.Hidden("PASVisit_ActivityRangeOfMotion", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_ActivityRangeOfMotion", "2", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_ActivityRangeOfMotion", "1", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_ActivityRangeOfMotion", "0", data.ContainsKey("ActivityRangeOfMotion") && data["ActivityRangeOfMotion"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <th colspan="3" class="align_left">Household Task</th><th>Completed</th><th>Refuse</th><th>N/A</th>
                        </tr><tr>
                            <td colspan="3" class="align_left">Make Bed</td>
                            <td><%= Html.Hidden("PASVisit_HouseholdTaskMakeBed", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_HouseholdTaskMakeBed", "2", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_HouseholdTaskMakeBed", "1", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_HouseholdTaskMakeBed", "0", data.ContainsKey("HouseholdTaskMakeBed") && data["HouseholdTaskMakeBed"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Change Linen</td>
                            <td><%= Html.Hidden("PASVisit_HouseholdTaskChangeLinen", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_HouseholdTaskChangeLinen", "2", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_HouseholdTaskChangeLinen", "1", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_HouseholdTaskChangeLinen", "0", data.ContainsKey("HouseholdTaskChangeLinen") && data["HouseholdTaskChangeLinen"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Light Housekeeping</td>
                            <td><%= Html.Hidden("PASVisit_HouseholdTaskLightHousekeeping", " ", new { @id = "" })%><%= Html.RadioButton("PASVisit_HouseholdTaskLightHousekeeping", "2", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "2" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_HouseholdTaskLightHousekeeping", "1", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "1" ? true : false, new { @id = "", @class = "radio" })%></td>
                            <td><%= Html.RadioButton("PASVisit_HouseholdTaskLightHousekeeping", "0", data.ContainsKey("HouseholdTaskLightHousekeeping") && data["HouseholdTaskLightHousekeeping"].Answer == "0" ? true : false, new { @id = "", @class = "radio" })%></td>
                        </tr><tr>
                            <td colspan="3" class="align_left">Other (Describe):</td><td colspan="3"><%= Html.TextBox("PASVisit_HouseholdTaskOther", data.ContainsKey("HouseholdTaskOther") ? data["HouseholdTaskOther"].Answer : string.Empty, new { @id = "PASVisit_HouseholdTaskOther", @class = "fill" })%></td>
                        </tr>
                    </thead>
                </table>
            </td>
        </tr>
        <tr><th colspan="2">Comments</th></tr>
        <tr><td colspan="2"><%= Html.TextArea("PASVisit_Comment", data.ContainsKey("Comment") ? data["Comment"].Answer : string.Empty, new { @id = "PASVisit_Comment", @class = "fill" })%></td></tr>
    </tbody>    
</table>
