﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ScheduleEvent>" %>
<fieldset>
    <legend>Details</legend>
    <table class="form"><tbody>
        <tr>
            <td><label for="Schedule_Detail_Task" class="bold">Task:</label></td>
            <td><label for="Schedule_Detail_Billable" class="bold">Billable:</label></td>
            <td colspan="2"><label for="Schedule_Detail_IsActive" class="float_left">Is Inactive:</label></td>
        </tr>
        <tr>
            <td><%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, new { @id = "Schedule_Detail_DisciplineTask", @class = "requireddropdown" })%></td>
            <td><%= Html.CheckBox("IsBillable", Model.IsBillable, new { @id = "Schedule_Detail_Billable", @class = "radio" })%></td>
            <td colspan="2"><%= Html.CheckBox("IsDeprecated", Model.IsDeprecated, new { @id = "Schedule_Detail_IsDeprecated", @class = "radio" })%></td>
        </tr>
        <tr><td colspan="4"></td></tr>
        <tr>
            <td><label for="Schedule_Detail_VisitDate" class="bold">Scheduled Date:</label></td>
            <td><label for="Schedule_Detail_Status" class="bold">Actual Visit Date:</label></td>
            <td><label for="Schedule_Detail_TimeIn" class="bold">Time In:</label></td>
            <td><label for="Schedule_Detail_TimeOut" class="bold">Time Out:</label></td>
        </tr>
        <tr>
            <td><input type="date" name="EventDate" value="<%= Model.EventDate %>" id="Schedule_Detail_EventDate" class="required" /></td>
            <td><input type="date" name="VisitDate" value="<%= Model.VisitDate %>" id="Schedule_Detail_VisitDate" class="required" /></td>
            <td><input type="text" size="10" id="Schedule_Detail_TimeIn" name="TimeIn" class="spinners" value="<%= Model.TimeIn %>" /></td>
            <td><input type="text" size="10" id="Schedule_Detail_TimeOut" name="TimeOut" class="spinners" value="<%= Model.TimeOut %>" /></td>
        </tr>
        <tr><td colspan="4"></td></tr>
        <tr>
            <td><label for="Schedule_Detail_Status" class="bold">Status:</label></td>
            <td><label for="Schedule_Detail_AssignedTo" class="bold">Clinician:</label></td>
            <td><label for="Schedule_Detail_Surcharge" class="bold">Surcharge:</label></td>
            <td><label for="Schedule_Detail_AssociatedMileage" class="bold">Associated Mileage:</label></td>
        </tr>
        <tr>
            <td><%= Html.Status("Status", Model.Status, Model.DisciplineTask, new { @id = "Schedule_Detail_Status" }) %></td>
            <td><% if (!Model.IsComplete || Model.DisciplineTask == (int) DisciplineTasks.FaceToFaceEncounter){ %><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", Model.UserId.ToString(), new { @id = "Schedule_Detail_AssignedTo", @class = "Users requireddropdown" })%><% } else { %><%= Html.Hidden("UserId", Model.UserId)%><%= Model.UserName %><% } %></td>
            <td><%= Html.TextBox("Surcharge", Model.Surcharge, new { @id = "Schedule_Detail_Surcharge", @class = "text digits input_wrapper", @maxlength = "5" })%></td>
            <td><%= Html.TextBox("AssociatedMileage", Model.AssociatedMileage, new { @id = "Schedule_Detail_AssociatedMileage", @class = "text digits input_wrapper", @maxlength = "5" })%></td>
        </tr><% if (Model.Discipline.IsEqual(Disciplines.Orders.ToString())) { %><tr><td colspan="4"></td></tr>
        <tr>
            <td colspan="4"><label for="Schedule_Detail_PhysicianId" class="bold">Physician:</label></td>
        </tr>
        <tr>
            <td colspan="4">
                <%= Html.TextBox("PhysicianId", Model.PhysicianId.ToString(), new { @id = "Schedule_Detail_PhysicianId", @class = "Physicians" })%>
                <script type="text/javascript"> $("#Schedule_Detail_PhysicianId").PhysicianInput(); </script>
            </td>
        </tr><% } %>
    </tbody></table>               
</fieldset>
    