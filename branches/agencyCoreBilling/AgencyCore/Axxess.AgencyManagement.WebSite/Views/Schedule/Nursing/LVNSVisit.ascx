﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "LVNSVisitForm" })) {
        var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "lvnsVisit",
        "LVN Supervisory Visit",
        Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "")%>
<%= Html.Hidden("LVNSVisit_PatientId", Model.PatientId)%>
<%= Html.Hidden("LVNSVisit_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("LVNSVisit_EventId", Model.EventId)%>
<%= Html.Hidden("Type", "LVNSVisit")%>
<%= Html.Hidden("DisciplineTask", Model.DisciplineTask)%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Licensed Vocational Nurse Supervisory Visit</th></tr>
            <tr><td colspan="3"><span class="bigtext"><%= Model.Patient.DisplayName %></span></td><td><% if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %><div class="buttons"><ul><li><%= Model.CarePlanOrEvalUrl %></li></ul></div><% } %></td></tr>
            <tr><td colspan="4">
                <div class="half">
                    <label for="LVNSVisit_VisitDate" class="float_left">LVN:</label>
                    <div class="float_right"><%= Html.LVNs("LVN", data.ContainsKey("LVN") ? data["LVN"].Answer : "", new { @id = "LVNSVisit_HealthAide" })%></div>
                </div><div class="half">
                    <label for="LVNSVisit_LVNPresentY" class="float_left">LVN Present:</label>
                    <div class="float_right">
                        <%= Html.RadioButton("LVNSVisit_LVNPresent", "1", data.ContainsKey("LVNPresent") && data["LVNPresent"].Answer == "1" ? true : false, new { @id = "LVNSVisit_LVNPresentY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_LVNPresentY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_LVNPresent", "0", data.ContainsKey("LVNPresent") && data["LVNPresent"].Answer == "0" ? true : false, new { @id = "LVNSVisit_LVNPresentN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_LVNPresentN">No</label>
                    </div>
                </div><div class="clear"></div><div class="half">
                    <label for="LVNSVisit_VisitDate" class="float_left">Visit Date:</label>
                    <div class="float_right"><input type="date" name="LVNSVisit_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="LVNSVisit_VisitDate" class="required" /></div>
                </div><div class="half">
                    <label for="LVNSVisit_AssociatedMileage" class="float_left">Associated Mileage:</label>
                    <div class="float_right"><%= Html.TextBox("LVNSVisit_AssociatedMileage", data.ContainsKey("AssociatedMileage") ? data["AssociatedMileage"].Answer : "", new { @class = "st digits", @maxlength = 6, @id = "LVNSVisit_AssociatedMileage" })%></div>
                </div><div class="clear"></div>
            </td></tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Evaluation</th></tr>
            <tr>
                <td colspan="3"><div class="float_left"><span class="alphali">1.</span>Arrives for assigned visits as scheduled:</div></td>
                <td>
                    <div class="float_left">
                        <%= Html.RadioButton("LVNSVisit_ArriveOnTime", "1", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "1" ? true : false, new { @id = "LVNSVisit_ArriveOnTimeY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_ArriveOnTimeY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_ArriveOnTime", "0", data.ContainsKey("ArriveOnTime") && data["ArriveOnTime"].Answer == "0" ? true : false, new { @id = "LVNSVisit_ArriveOnTimeN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_ArriveOnTimeN">No</label>
                    </div>
                </td>
            </tr><tr>
                <td colspan="3"><div class="float_left"><span class="alphali">2.</span>Follows client&#8217;s plan of care:</div></td>
                <td>
                    <div class="float_left">
                        <%= Html.RadioButton("LVNSVisit_FollowPOC", "1", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "1" ? true : false, new { @id = "LVNSVisit_FollowPOCY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_FollowPOCY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_FollowPOC", "0", data.ContainsKey("FollowPOC") && data["FollowPOC"].Answer == "0" ? true : false, new { @id = "LVNSVisit_FollowPOCN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_FollowPOCN">No</label>
                    </div>
                </td>
            </tr><tr>
                <td colspan="3"><div class="float_left"><span class="alphali">3.</span>Demonstrates positive and helpful attitude towards the client and others:</div></td>
                <td>
                    <div class="float_left">
                        <%= Html.RadioButton("LVNSVisit_HasPositiveAttitude", "1", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "1" ? true : false, new { @id = "LVNSVisit_HasPositiveAttitudeY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_HasPositiveAttitudeY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_HasPositiveAttitude", "0", data.ContainsKey("HasPositiveAttitude") && data["HasPositiveAttitude"].Answer == "0" ? true : false, new { @id = "LVNSVisit_HasPositiveAttitudeN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_HasPositiveAttitudeN">No</label>
                    </div>
                </td>
            </tr><tr>
                <td colspan="3"><div class="float_left"><span class="alphali">4.</span>Informs Nurse Supervisor/Case Manager of client needs and changes in condition:</div></td>
                <td>
                    <div class="float_left">
                        <%= Html.RadioButton("LVNSVisit_InformChanges", "1", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "1" ? true : false, new { @id = "LVNSVisit_InformChangesY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_InformChangesY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_InformChanges", "0", data.ContainsKey("InformChanges") && data["InformChanges"].Answer == "0" ? true : false, new { @id = "LVNSVisit_InformChangesN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_InformChangesN">No</label>
                    </div>
                </td>
            </tr><tr>
                <td colspan="3"><div class="float_left"><span class="alphali">5.</span>Implements Universal Precautions per agency policy:</div></td>
                <td>
                    <div class="float_left">
                        <%= Html.RadioButton("LVNSVisit_IsUniversalPrecautions", "1", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "1" ? true : false, new { @id = "LVNSVisit_IsUniversalPrecautionsY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_IsUniversalPrecautionsY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_IsUniversalPrecautions", "0", data.ContainsKey("IsUniversalPrecautions") && data["IsUniversalPrecautions"].Answer == "0" ? true : false, new { @id = "LVNSVisit_IsUniversalPrecautionsN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_IsUniversalPrecautionsN">No</label>
                    </div>
                </td>
            </tr><tr>
                <td colspan="3"><div class="float_left"><span class="alphali">6.</span>Any changes made to client plan of care at this time:</div></td>
                <td>
                    <div class="float_left">
                        <%= Html.RadioButton("LVNSVisit_POCChanges", "1", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "1" ? true : false, new { @id = "LVNSVisit_POCChangesY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_POCChangesY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_POCChanges", "0", data.ContainsKey("POCChanges") && data["POCChanges"].Answer == "0" ? true : false, new { @id = "LVNSVisit_POCChangesN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_POCChangesN">No</label>
                    </div>
                </td>
            </tr><tr>
                <td colspan="3"><div class="float_left"><span class="alphali">7.</span>Patient/CG satisfied with care and services provided by LVN/LPN:</div></td>
                <td>
                    <div class="float_left">
                        <%= Html.RadioButton("LVNSVisit_IsServicesSatisfactory", "1", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "1" ? true : false, new { @id = "LVNSVisit_IsServicesSatisfactoryY", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_IsServicesSatisfactoryY">Yes</label>
                        <%= Html.RadioButton("LVNSVisit_IsServicesSatisfactory", "0", data.ContainsKey("IsServicesSatisfactory") && data["IsServicesSatisfactory"].Answer == "0" ? true : false, new { @id = "LVNSVisit_IsServicesSatisfactoryN", @class = "radio" })%><label class="inlineradio" for="LVNSVisit_IsServicesSatisfactoryN">No</label>
                    </div>
                </td>
            </tr><tr>
                <td colspan="4">
                    <div class="float_left"><span class="alphali">8.</span>Additional Comments/Findings:</div><div class="clear"></div>
                    <div class="float_left" style="width: 99%;"><%= Html.TextArea("LVNSVisit_AdditionalComments", data.ContainsKey("AdditionalComments") ? data["AdditionalComments"].Answer : "", new { @style = "height: 80px; width: 100%;" })%></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Electronic Signature</th></tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="LVNSVisit_Signature" class="float_left">Signature</label>
                        <div class="float_right"><%= Html.Password("LVNSVisit_Clinician", "", new { @class = "", @id = "LVNSVisit_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="LVNSVisit_SignatureDate" class="float_left">Date:</label>
                        <div class="float_right"><input type="date" name="LVNSVisit_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : string.Empty %>" id="LVNSVisit_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="LVNSVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="LVNSVisitRemove(); lvnSupVisit.Submit($(this));">Save</a></li>
            <li><a href="javascript:void(0);" onclick="LVNSVisitAdd(); lvnSupVisit.Submit($(this));">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="LVNSVisitRemove(); lvnSupVisit.Submit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="LVNSVisitRemove(); lvnSupVisit.Submit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" onclick="LVNSVisitRemove(); UserInterface.CloseWindow('lvnsVisit');">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>  
<script type="text/javascript">
    U.timePicker($("#LVNSVisit_TimeIn"));
    U.timePicker($("#LVNSVisit_TimeOut"));
    function LVNSVisitAdd() {
        $("#LVNSVisit_Clinician").removeClass('required').addClass('required');
        $("#LVNSVisit_SignatureDate").removeClass('required').addClass('required');
    }
    function LVNSVisitRemove() {
        $("#LVNSVisit_Clinician").removeClass('required');
        $("#LVNSVisit_SignatureDate").removeClass('required');
    }
</script>