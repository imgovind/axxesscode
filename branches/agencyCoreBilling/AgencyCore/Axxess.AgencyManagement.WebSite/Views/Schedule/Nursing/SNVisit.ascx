﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "snVisit",
        "Skilled Nursing Visit Note",
        Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "")%>
<div class="wrapper main">
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = Model.Type + "Form" })) { %>
    <%= Html.Hidden(Model.Type + "_PatientId", Model.PatientId, new { @id = Model.Type + "_PatientId" })%>
    <%= Html.Hidden(Model.Type + "_EpisodeId", Model.EpisodeId, new { @id = Model.Type + "_EpisodeId" })%>
    <%= Html.Hidden(Model.Type + "_EventId", Model.EventId, new { @id = Model.Type + "_Id" })%>
    <%= Html.Hidden("Type", Model.Type)%>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Skilled Nursing Visit Note</th>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="bigtext"><%= Model.Patient.DisplayName %></span>
                </td>
                <td>
                    <%= Html.DisciplineTypes("DisciplineTask", Model.DisciplineTask, new { @id = Model.Type + "_DisciplineTask", @class = "requireddropdown" })%>
                </td>
                <td>
    <%  if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %>
                    <div class="buttons">
                        <ul>
                            <li><%= Model.CarePlanOrEvalUrl%></li>
                        </ul>
                    </div>
    <%  } %>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label for="<%= Model.Type %>_VisitDate" class="float_left">Visit Date:</label>
                        <div class="float_right"><input type="date" name="<%= Model.Type %>_VisitDate" value="<%= Model.VisitDate.IsValidDate() ? Model.VisitDate : string.Empty %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="<%= Model.Type %>_VisitDate" /></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_TimeIn" class="float_left">Time In:</label>
                        <div class="float_right"><%= Html.TextBox(Model.Type + "_TimeIn", data.AnswerOrEmptyString("TimeIn"), new { @id = Model.Type + "_TimeIn", @class = "loc" })%></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_TimeOut" class="float_left">Time Out:</label>
                        <div class="float_right"><%= Html.TextBox(Model.Type + "_TimeOut", data.AnswerOrEmptyString("TimeOut"), new { @id = Model.Type + "_TimeOut", @class = "loc" })%></div>
                    </div>
                </td>
                <td colspan="2">
    <%  if (Current.HasRight(Permissions.ViewPreviousNotes)) { %>
                    <div>
                        <label for="<%= Model.Type %>_PreviousNotes" class="float_left">Previous Notes:</label>
                        <div class="float_right"><%= Html.PreviousNotes(Model.PreviousNotes, new { @id = Model.Type + "_PreviousNotes" })%></div>
                    </div>
                    <div class="clear"></div>
    <%  } %>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis" class="float_left">Primary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis", data.AnswerOrEmptyString("PrimaryDiagnosis")) %>
                        <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis") %></span>
                        <%= Html.Hidden(Model.Type + "_ICD9M", data.AnswerOrEmptyString("ICD9M")) %>
    <%  if (data.AnswerOrEmptyString("ICD9M").IsNotNullOrEmpty()) { %>
                        <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
    <%  } %>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <label for="<%= Model.Type %>_PrimaryDiagnosis1" class="float_left">Secondary Diagnosis:</label>
                        <%= Html.Hidden(Model.Type + "_PrimaryDiagnosis1", data.AnswerOrEmptyString("PrimaryDiagnosis1")) %>
                        <span><%= data.AnswerOrEmptyString("PrimaryDiagnosis1")%></span>
                        <%= Html.Hidden(Model.Type + "_ICD9M1", data.AnswerOrEmptyString("ICD9M1")) %>
    <%  if (data.AnswerOrEmptyString("ICD9M1").IsNotNullOrEmpty()) { %>
                        <a class="teachingguide" href="http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c=<%= data.AnswerOrEmptyString("ICD9M1") %>&informationRecipient.languageCode.c=en" target="_blank">Teaching Guide</a>
    <%  } %>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div id="<%= Model.Type %>_ContentId"><% Html.RenderPartial("~/Views/Schedule/Nursing/SNVisitContent.ascx", Model); %></div>
    <table class="fixed nursing">
        <tbody>
            <tr>
                <th colspan="4">Electronic Signature</th>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="<%= Model.Type %>_Clinician" class="float_left">Clinician:</label>
                        <div class="float_right"><%= Html.Password(Model.Type + "_Clinician", string.Empty, new { @id = Model.Type + "_Clinician" }) %></div>
                    </div>
                    <div class="third"></div>
                    <div class="third">
                        <label for="<%= Model.Type %>_SignatureDate" class="float_left">Date:</label>
                        <div class="float_right"><input type="date" name="<%= Model.Type %>_SignatureDate" value="<%= data.AnswerOrEmptyString("SignatureDate") %>" id="<%= Model.Type %>_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div class="buttons">
    <%  if (Model.IsSupplyExist) { %>
                        <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadNoteSupplyWorkSheet('{0}','{1}','{2}');\" >Edit Supply Worksheet</a>", Model.EpisodeId, Model.PatientId, Model.EventId)%></li>
    <%  } else { %>
                        <li><%= string.Format("<a href=\"javascript:void(0);\" onclick=\"Schedule.loadNoteSupplyWorkSheet('{0}','{1}','{2}');\" >Add Supply Worksheet</a>", Model.EpisodeId, Model.PatientId, Model.EventId)%></li>
    <%  } %>
                        <%  if (Current.HasRight(Permissions.ScheduleVisits)) { %>
                        <li style="float: right;"><a href="javascript:void(0);" onclick="UserInterface.ShowMultipleDayScheduleModal('<%= Model.EpisodeId %>', '<%= Model.PatientId %>');" title="Schedule Supervisory Visit">Schedule Supervisory Visit</a></li>
                        <%  } %>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="SNVisit_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="SNVisitRemove();snVisit.Submit($(this));">Save</a></li>
            <li><a href="javascript:void(0);" onclick="SNVisitAdd(); snVisit.Submit($(this));">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="SNVisitRemove(); snVisit.Submit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="SNVisitRemove(); snVisit.Submit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" onclick="SNVisitRemove(); UserInterface.CloseWindow('snVisit');">Exit</a></li>
        </ul>
    </div>
</div>
<%  } %>
<script type="text/javascript">
    U.timePicker("#<%= Model.Type %>_TimeIn");
    U.timePicker("#<%= Model.Type %>_TimeOut");

    $("#<%= Model.Type %>_MR").attr('readonly', true);
    function SNVisitAdd() {
        $("#<%= Model.Type %>_TimeIn").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_TimeOut").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_Clinician").removeClass('required').addClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required').addClass('required');
    }
    function SNVisitRemove() {
        $("#<%= Model.Type %>_TimeIn").removeClass('required');
        $("#<%= Model.Type %>_TimeOut").removeClass('required');
        $("#<%= Model.Type %>_Clinician").removeClass('required');
        $("#<%= Model.Type %>_SignatureDate").removeClass('required');
    }
    $("#<%= Model.Type %>_PreviousNotes").change(function() {
    if ($("#<%= Model.Type %>_PreviousNotes").val().length > 0) {
            if (confirm("Are you sure you want to load this note?")) {
                $("#<%= Model.Type %>_ContentId").load("/Schedule/SNVisitContent", { patientId: $("#<%= Model.Type %>_PatientId").val(), noteId: $("#<%= Model.Type %>_Id").val(), previousNoteId: $("#<%= Model.Type %>_PreviousNotes").val() }, function(responseText, textStatus, XMLHttpRequest) {
                    if (textStatus == 'error') {
                    }
                    else if (textStatus == "success") {
                        Template.OnChangeInit();
                    }
                });
            }
        } else { U.growl("Please select a previous note first", "error"); }
    });
    $("#<%= Model.Type %>_GenericBloodSugarSiteText").Autocomplete({ source: ["Thumb", "Index", "Middle", "Forearm", "1st  digit", "2nd  digit", "3rd  digit", "4th digit", "5th digit"] });
    U.hideIfChecked($("#<%= Model.Type %>_GenericIsIVApplied1"), $("#<%= Model.Type %>_IVContainer"));
    U.hideIfChecked($("#<%= Model.Type %>_GenericIsDiabeticCareApplied1"), $("#<%= Model.Type %>_DiabeticCareContainer"));
    U.hideIfChecked($("#<%= Model.Type %>_GenericIsPhlebotomyApplied1"), $("#<%= Model.Type %>_GenericPhlebotomyContainer"));
    U.hideOptions();    
</script>