﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
printview.col(2,
    printview.span("Care Plan:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericCarePlan").Clean() %>") +
    printview.span("Progress toward goals:",true) +
    printview.span("<%= data.AnswerOrEmptyString("GenericCarePlanProgressTowardsGoals").Clean() %>")) +
printview.span("New/Changed Orders or Updates to Care Plan:",true) +
printview.col(2,
    printview.checkbox("Yes",<%= data.AnswerOrEmptyString("GenericCarePlanIsChanged").Equals("1").ToString().ToLower() %>) +
    printview.checkbox("No",<%= data.AnswerOrEmptyString("GenericCarePlanIsChanged").Equals("0").ToString().ToLower() %>)) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericCarePlanComment").Clean() %>"),
"Care Plan"