﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%  string[] genericGU = data.AnswerArray("GenericGU"); %>
<%  string[] genericGUUrine = data.AnswerArray("GenericGUUrine"); %>
printview.checkbox("WNL (Within Normal Limits)",<%= genericGU.Contains("1").ToString().ToLower() %>) +
printview.col(2,
    printview.checkbox("Incontinence",<%= genericGU.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Bladder Distention",<%= genericGU.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Discharge",<%= genericGU.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Frequency",<%= genericGU.Contains("5").ToString().ToLower() %>) +
    printview.checkbox("Dysuria",<%= genericGU.Contains("6").ToString().ToLower() %>) +
    printview.checkbox("Retention",<%= genericGU.Contains("7").ToString().ToLower() %>) +
    printview.checkbox("Urgency",<%= genericGU.Contains("8").ToString().ToLower() %>) +
    printview.checkbox("Oliguria",<%= genericGU.Contains("9").ToString().ToLower() %>) +
    printview.checkbox("Catheter/Device",<%= genericGU.Contains("10").ToString().ToLower() %>) +
    printview.span("<%= data.AnswerOrEmptyString("GenericGUCatheterList").Clean() %>")) +
printview.span("Last changed: <%= data.AnswerOrEmptyString("GenericGUCatheterLastChanged").Clean() %> <%= data.AnswerOrEmptyString("GenericGUCatheterFrequency").Clean()%>Fr <%= data.AnswerOrEmptyString("GenericGUCatheterAmount").Clean() %>cc") +
printview.checkbox("Urine:",<%= genericGU.Contains("11").ToString().ToLower() %>) +
printview.col(2,
    printview.checkbox("Clear",<%= genericGUUrine.Contains("6").ToString().ToLower() %>) +
    printview.checkbox("Cloudy",<%= genericGUUrine.Contains("1").ToString().ToLower() %>) +
    printview.checkbox("Odorous",<%= genericGUUrine.Contains("2").ToString().ToLower() %>) +
    printview.checkbox("Sediment",<%= genericGUUrine.Contains("3").ToString().ToLower() %>) +
    printview.checkbox("Hematuria",<%= genericGUUrine.Contains("4").ToString().ToLower() %>) +
    printview.checkbox("Other <%= data.AnswerOrEmptyString("GenericGUOtherText").Clean() %>",<%= genericGUUrine.Contains("5").ToString().ToLower() %>)) +
printview.span("Comments:",true) +
printview.span("<%= data.AnswerOrEmptyString("GenericGenitourinaryComment").Clean() %>"),
"Genitourinary"