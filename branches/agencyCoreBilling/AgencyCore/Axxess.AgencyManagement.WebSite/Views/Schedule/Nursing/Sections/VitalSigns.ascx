﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<label for="<%= Model.Type %>_GenericTemp" class="float_left">Temp:</label>
<div class="float_right">
    <%= Html.TextBox(Model.Type + "_GenericTemp", data.AnswerOrEmptyString("GenericTemp"), new { @class = "vitals", @id = Model.Type + "_GenericTemp" }) %>
    <% var temp = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "Axillary ", Value = "Axillary" },
        new SelectListItem { Text = "Oral", Value = "Oral" },
        new SelectListItem { Text = "Tympanic", Value = "Tympanic" },
        new SelectListItem { Text = "Temporal", Value = "Temporal" }
    }, "Value", "Text", data.AnswerOrDefault("GenericTempType", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericTempType", temp, new { @id = Model.Type + "_GenericTempType", @class = "loc" })%>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericResp" class="float_left">Resp:</label>
<div class="float_right">
    <%= Html.TextBox(Model.Type + "_GenericResp", data.AnswerOrEmptyString("GenericResp"), new { @class = "vitals", @id = Model.Type + "_GenericResp" })%>
    <% var resp = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "Regular ", Value = "Regular" },
        new SelectListItem { Text = "Irregular", Value = "Irregular" }
    }, "Value", "Text", data.AnswerOrDefault("GenericRespType", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericRespType", resp, new { @id = Model.Type + "_GenericRespType", @class = "loc" })%>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericPulseApical" class="float_left">Apical Pulse:</label>
<div class="float_right">
    <%= Html.TextBox(Model.Type + "_GenericPulseApical", data.AnswerOrEmptyString("GenericPulseApical"), new { @class = "vitals", @id = Model.Type + "_GenericPulseApical" })%>
    <% var pulseApical = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "Regular ", Value = "Regular" },
        new SelectListItem { Text = "Irregular", Value = "Irregular" }
    }, "Value", "Text", data.AnswerOrDefault("GenericPulseApicalRegular", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericPulseApicalRegular", pulseApical, new { @id = Model.Type + "_GenericPulseApicalRegular", @class = "loc" })%>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericPulseRadial" class="float_left">Radial Pulse:</label>
<div class="float_right">
    <%= Html.TextBox(Model.Type + "_GenericPulseRadial", data.AnswerOrEmptyString("GenericPulseRadial"), new { @class = "vitals", @id = Model.Type + "_GenericPulseRadial" })%>
    <% var pulseRadial = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "Regular ", Value = "Regular" },
        new SelectListItem { Text = "Irregular", Value = "Irregular" }
    }, "Value", "Text", data.AnswerOrDefault("GenericPulseRadialRegular", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericPulseRadialRegular", pulseRadial, new { @id = Model.Type + "_GenericPulseRadialRegular", @class = "loc" })%>
</div>
<table class="fixed">
    <thead>
        <tr>
            <th class="align_left">BP</th>
            <th>Lying</th>
            <th>Sitting</th>
            <th>Standing</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th class="align_left">Left</th>
            <td><%= Html.TextBox(Model.Type + "_GenericBPLeftLying", data.AnswerOrEmptyString("GenericBPLeftLying"), new { @class = "vitals", @id = Model.Type + "_GenericBPLeftLying" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericBPLeftSitting", data.AnswerOrEmptyString("GenericBPLeftSitting"), new { @class = "vitals", @id = Model.Type + "_GenericBPLeftSitting" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericBPLeftStanding", data.AnswerOrEmptyString("GenericBPLeftStanding"), new { @class = "vitals", @id = Model.Type + "_GenericBPLeftStanding" })%></td>
        </tr><tr>
            <th class="align_left">Right</th>
            <td><%= Html.TextBox(Model.Type + "_GenericBPRightLying", data.AnswerOrEmptyString("GenericBPRightLying"), new { @class = "vitals", @id = Model.Type + "_GenericBPRightLying" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericBPRightSitting", data.AnswerOrEmptyString("GenericBPRightSitting"), new { @class = "vitals", @id = Model.Type + "_GenericBPRightSitting" })%></td>
            <td><%= Html.TextBox(Model.Type + "_GenericBPRightStanding", data.AnswerOrEmptyString("GenericBPRightStanding"), new { @class = "vitals", @id = Model.Type + "_GenericBPRightStanding" })%></td>
        </tr>
    </tbody>
</table>
<label for="<%= Model.Type %>_GenericWeight" class="float_left">Weight:</label>
<div class="float_right">
    <%= Html.TextBox(Model.Type + "_GenericWeight", data.AnswerOrEmptyString("GenericWeight"), new { @class = "vitals", @id = Model.Type + "_GenericWeight" })%>
    <% var weight = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "N/A ", Value = "N/A" },
        new SelectListItem { Text = "kg", Value = "kg" },
        new SelectListItem { Text = "lbs", Value = "lbs" }
    }, "Value", "Text", data.AnswerOrDefault("GenericWeightUnit", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericWeightUnit", weight, new { @id = Model.Type + "_GenericWeightUnit", @class = "loc" })%>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericPulseOximetry" class="float_left">Pulse Oximetry:</label>
<div class="float_right">
    <%= Html.TextBox(Model.Type + "_GenericPulseOximetry", data.AnswerOrEmptyString("GenericPulseOximetry"), new { @class = "vitals", @id = Model.Type + "_GenericPulseOximetry" })%>
    <% var pulseOximetry = new SelectList(new[] {
        new SelectListItem { Text = "", Value = "" },
        new SelectListItem { Text = "N/A ", Value = "N/A" },
        new SelectListItem { Text = "On O2", Value = "On O2" },
        new SelectListItem { Text = "on RA", Value = "on RA" }
    }, "Value", "Text", data.AnswerOrDefault("GenericPulseOximetryUnit", "0")); %>
    <%= Html.DropDownList(Model.Type + "_GenericPulseOximetryUnit", pulseOximetry, new { @id = Model.Type + "_GenericPulseOximetryUnit", @class = "loc" })%>
</div>
<div class="clear"></div>
<label for="<%= Model.Type %>_GenericVitlaSignComment" class="strong">Comment:</label>
<div class="align_center"><%= Html.TextArea(Model.Type + "_GenericVitlaSignComment", data.AnswerOrEmptyString("GenericVitlaSignComment"), new { @id = Model.Type + "_GenericVitlaSignComment", @class = "fill" })%></div>