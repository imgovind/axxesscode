﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
        <% string[] responseToTeachingInterventions = data.ContainsKey("GenericResponseToTeachingInterventions") && data["GenericResponseToTeachingInterventions"].Answer != "" ? data["GenericResponseToTeachingInterventions"].Answer.Split(',') : null; %>
        <%= Html.Hidden(Model.Type + "_GenericResponseToTeachingInterventions", string.Empty)%>
        <div class="align_left">
            <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions1' class='radio float_left' name='{0}_GenericResponseToTeachingInterventions' value='1' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("1") ? "checked='checked'" : "") %>
            <span class="radio">
                <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions1">Verbalize</label>
                <%= Html.TextBox(Model.Type + "_GenericResponseToTeachingInterventionsUVI", data.ContainsKey("GenericResponseToTeachingInterventionsUVI") ? data["GenericResponseToTeachingInterventionsUVI"].Answer : "", new { @id = Model.Type + "_GenericResponseToTeachingInterventionsUVI", @class = "oe" }) %>
                <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions1">understanding of verbal instructions/teaching given.</label>
            </span>
        </div>
        <div class="align_left">
            <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions2' class='radio float_left' name='{0}_GenericResponseToTeachingInterventions' value='2' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("2") ? "checked='checked'" : "")%>
            <span class="radio">
                <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions2">Able to return</label>
                <%= Html.TextBox(Model.Type + "_GenericResponseToTeachingInterventionsCDP", data.ContainsKey("GenericResponseToTeachingInterventionsCDP") ? data["GenericResponseToTeachingInterventionsCDP"].Answer : "", new { @id = Model.Type + "_GenericResponseToTeachingInterventionsCDP", @class = "oe" }) %>
                <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions2">correct demonstration of procedure/technique instructed on.</label>
            </span>
        </div>
        <div class="align_left">
            <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions3' class='radio float_left' name='{0}_GenericResponseToTeachingInterventions' value='3' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("3") ? "checked='checked'" : "")%>
            <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions3" class="radio">Treatment/Procedure well tolerated by patient.</label>
        </div>
        <div class="align_left">
            <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions4' class='radio float_left' name='{0}_GenericResponseToTeachingInterventions' value='4' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("4") ? "checked='checked'" : "")%>
            <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions4" class="radio">Infusion well tolerated by patient.</label>
        </div>
        <div class="align_left">
            <%= string.Format("<input id='{0}_GenericResponseToTeachingInterventions5' class='radio float_left' name='{0}_GenericResponseToTeachingInterventions' value='5' type='checkbox' {1} />", Model.Type, responseToTeachingInterventions != null && responseToTeachingInterventions.Contains("5") ? "checked='checked'" : "")%>
            <span class="radio">
                <label for="<%= Model.Type %>_GenericResponseToTeachingInterventions5">Other:</label>
                <%= Html.TextBox(Model.Type + "_GenericResponseToTeachingInterventionsOther", data.ContainsKey("GenericResponseToTeachingInterventionsOther") ? data["GenericResponseToTeachingInterventionsOther"].Answer : "", new { @id = Model.Type + "_GenericResponseToTeachingInterventionsOther" })%>
            </span>
        </div>
 