﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<%  var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<table class="fixed nursing">
    <tbody>
        <tr>
            <th>Vital Signs</th>
            <th>Pain Profile</th>
            <th>Skin</th>
            <th>Respiratory</th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/VitalSigns.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/PainProfile.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Skin.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Respiratory.ascx", Model); %></td>
        </tr>
        <tr>
            <th>Cardiovascular</th>
            <th>Neurological</th>
            <th>Musculoskeletal</th>
            <th>Gastrointestinal</th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Cardiovascular.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Neurological.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Musculoskeletal.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Gastrointestinal.ascx", Model); %></td>
        </tr>
        <tr>
            <th>Nutrition</th>
            <th>Genitourinary</th>
            <th>
                Diabetic Care &#8212;
                <%  string[] isDiabeticCareApplied = data.AnswerArray("GenericIsDiabeticCareApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsDiabeticCareApplied", string.Empty, new { @id = Model.Type + "_GenericIsDiabeticCareAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsDiabeticCareApplied1' name='{0}_GenericIsDiabeticCareApplied' value='1' type='checkbox' {1} />", Model.Type, isDiabeticCareApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsDiabeticCareApplied1">N/A</label>
            </th>
            <th>
                IV &#8212;
                <%  string[] isIVApplied = data.AnswerArray("GenericIsIVApplied"); %>
                <%= Html.Hidden(Model.Type + "isIVApplied", string.Empty, new { @id = Model.Type + "_GenericIsIVAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsIVApplied1' name='{0}_GenericIsIVApplied' value='1' type='checkbox' {1} />", Model.Type, isIVApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>isIVApplied1">N/A</label>
            </th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Nutrition.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Genitourinary.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/DiabeticCare.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/IV.ascx", Model); %></td>
        </tr>
        <tr>
            <th>Infection Control</th>
            <th>Care Coordination</th>
            <th>Care Plan</th>
            <th>Discharge Planning</th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/InfectionControl.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/CareCoordination.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/CarePlan.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/DischargePlanning.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Interventions</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Interventions.ascx", Model); %></td>
        </tr>
        <tr>
            <th colspan="4">Narrative &#38; Teaching</th>
        </tr>
        <tr>
            <td colspan="4"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Narrative.ascx", Model); %></td>
        </tr>
        <tr>
            <th>Response to Teaching/Interventions</th>
            <th>Home Bound Status</th>
            <th colspan="2">
                Phlebotomy &#8212;
                <%  string[] isPhlebotomyApplied = data.AnswerArray("GenericIsPhlebotomyApplied"); %>
                <%= Html.Hidden(Model.Type + "_GenericIsPhlebotomyApplied", string.Empty, new { @id = Model.Type + "_GenericIsPhlebotomyAppliedHidden" })%>
                <%= string.Format("<input class='radio' id='{0}_GenericIsPhlebotomyApplied1' name='{0}_GenericIsPhlebotomyApplied' value='1' type='checkbox' {1} />", Model.Type, isPhlebotomyApplied.Contains("1").ToChecked()) %>
                <label for="<%= Model.Type %>_GenericIsPhlebotomyApplied1">N/A</label>
            </th>
        </tr>
        <tr>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Response.ascx", Model); %></td>
            <td><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/HomeBoundStatus.ascx", Model); %></td>
            <td colspan="2"><% Html.RenderPartial("~/Views/Schedule/Nursing/Sections/Phlebotomy.ascx", Model); %></td>
        </tr>
    </tbody>
</table>