﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VisitNoteViewData>" %>
<% using (Html.BeginForm("Notes", "Schedule", FormMethod.Post, new { @id = "dischargeSummaryForm" })) {
       var data = Model != null ? Model.Questions : new Dictionary<string, NotesQuestion>(); %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "dischargeSummary",
        "Discharge Summary",
        Model.Patient != null ? (Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "")%>
<%= Html.Hidden("DischargeSummary_PatientId", Model.PatientId)%>
<%= Html.Hidden("DischargeSummary_EpisodeId", Model.EpisodeId)%>
<%= Html.Hidden("DischargeSummary_EventId", Model.EventId)%>
<%= Html.Hidden("DisciplineTask", "18")%>
<%= Html.Hidden("Type", "DischargeSummary")%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Discharge Summary</th></tr>
            <tr><td colspan="3"><span class="bigtext"><%= Model.Patient.DisplayName %></span></td><td><% if (Model.CarePlanOrEvalUrl.IsNotNullOrEmpty()) { %><div class="buttons"><ul><li><%= Model.CarePlanOrEvalUrl%></li></ul></div><% } %></td></tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="DischargeSummary_DateCompleted" class="float_left">Visit Date:</label>
                        <div class="float_right"><input type="date" name="DischargeSummary_VisitDate" value="<%= Model.VisitDate %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="DischargeSummary_VisitDate" class="required" /></div>
                    </div><div class="third">
                        <label for="DischargeSummary_DischargeDate" class="float_left">Discharge Date:</label>
                        <div class="float_right"><input type="date" name="DischargeSummary_DischargeDate" value="<%= data.ContainsKey("DischargeDate") && data["DischargeDate"].Answer.IsNotNullOrEmpty() && data["DischargeDate"].Answer.IsValidDate() ? data["DischargeDate"].Answer : "" %>" maxdate="<%= Model.EndDate.ToShortDateString() %>" mindate="<%= Model.StartDate.ToShortDateString() %>" id="DischargeSummary_DischargeDate" /></div>
                    </div><div class="third">
                        <label for="DischargeSummary_EpsPeriod" class="float_left">Episode/Period:</label>
                        <div class="float_right"><%= Html.TextBox("DischargeSummary_EpsPeriod", Model != null ? Model.StartDate.ToShortDateString() + " — " + Model.EndDate.ToShortDateString() : string.Empty, new { @id = "DischargeSummary_EpsPeriod", @readonly = "readonly" })%></div>
                    </div><div class="third">
                        <label for="DischargeSummary_MR" class="float_left">MR#:</label>
                        <div class="float_right"><%= Html.TextBox("DischargeSummary_MR", Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty, new { @id = "DischargeSummary_MR", @readonly = "readonly" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="DischargeSummary_Physician" class="float_left">Physician:</label>
                        <div class="float_right"><%= Html.TextBox("DischargeSummary_Physician", Model.PhysicianId.ToString(), new { @id = "DischargeSummary_Physician", @class = "Physicians" })%></div>
                    </div><div class="clear"></div><div class="third">
                        <label class="float_left">Notification of Discharge given to patient:</label>
                        <div class="float_right">
                            <%= Html.Hidden("DischargeSummary_IsNotificationDC", " ", new { @id = "" })%>
                            <%= Html.RadioButton("DischargeSummary_IsNotificationDC", "1", data.ContainsKey("IsNotificationDC") && data["IsNotificationDC"].Answer == "1" ? true : false, new { @id = "DischargeSummary_IsNotificationDCY", @class = "radio" })%>
                            <label for="DischargeSummary_IsNotificationDCY" class="inlineradio">Yes</label>
                            <%= Html.RadioButton("DischargeSummary_IsNotificationDC", "0", data.ContainsKey("IsNotificationDC") && data["IsNotificationDC"].Answer == "0" ? true : false, new { @id = "DischargeSummary_IsNotificationDCN", @class = "radio" })%>
                            <label for="DischargeSummary_IsNotificationDCN" class="inlineradio">No</label>
                        </div><div class="clear"></div><div class="float_right">
                            <label for="DischargeSummary_NotificationDate">If Yes:</label>
                            <%  var patientReceived = new SelectList(new[] {
                                    new SelectListItem { Text = "", Value = "0" },
                                    new SelectListItem { Text = "5 day", Value = "1" },
                                    new SelectListItem { Text = "2 day", Value = "2" },
                                    new SelectListItem { Text = "Other", Value = "3" }
                                }, "Value", "Text", data.ContainsKey("NotificationDate") ? data["NotificationDate"].Answer : "0"); %>
                            <%= Html.DropDownList("DischargeSummary_NotificationDate", patientReceived, new { @id = "DischargeSummary_NotificationDate" }) %>
                            <%= Html.TextBox("DischargeSummary_NotificationDateOther", data.ContainsKey("NotificationDateOther") ? data["NotificationDateOther"].Answer : "", new { @id = "DischargeSummary_NotificationDateOther", @style = "display:none;"}) %>
                        </div>
                    </div><div class="third"></div><div class="third">
                        <label for="DischargeSummary_ReasonForDC" class="float_left">Reason for Discharge:</label>
                        <%  var reasonForDC = new SelectList(new[] {
                                new SelectListItem { Text = "", Value = "0" },
                                new SelectListItem { Text = "Goals Met", Value = "1" },
                                new SelectListItem { Text = "To Nursing Home", Value = "2" },
                                new SelectListItem { Text = "Deceased", Value = "3" },
                                new SelectListItem { Text = "Noncompliant", Value = "4" },
                                new SelectListItem { Text = "To Hospital", Value = "5" },
                                new SelectListItem { Text = "Moved from Service Area", Value = "6" },
                                new SelectListItem { Text = "Refused Care", Value = "7" },
                                new SelectListItem { Text = "No Longer Homebound", Value = "8" },
                                new SelectListItem { Text = "Other", Value = "9" }
                            }, "Value", "Text", data.ContainsKey("ReasonForDC") ? data["ReasonForDC"].Answer : "0"); %>
                        <%= Html.DropDownList("DischargeSummary_ReasonForDC", reasonForDC, new { @id = "DischargeSummary_ReasonForDC", @class = "float_right" })%>
                        <%= Html.TextBox("DischargeSummary_ReasonForDCOther",data.ContainsKey("ReasonForDCOther") ? data["ReasonForDCOther"].Answer : "", new { @id = "DischargeSummary_ReasonForDCOther", @style = "display:none;", @class = "float_right" })%>
                    </div>
                </td>
            </tr>
            <tr><th colspan="2">Patient Condition and Outcomes</th><th colspan="2">Service(s) Provided</th></tr>
            <tr>
                <td colspan="2"><%string[] patientCondition = data.ContainsKey("PatientCondition") && data["PatientCondition"].Answer != "" ? data["PatientCondition"].Answer.Split(',') : null; %><input name="DischargeSummary_PatientCondition" value=" " type="hidden" />
                    <table class="fixed align_left"><tbody>
                        <tr>
                            <td><%= string.Format("<input id='DischargeSummary_PatientConditionStable' name='DischargeSummary_PatientCondition' value='1' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("1") ? "checked='checked'" : "")%><label for="DischargeSummary_PatientConditionStable" class="radio">Stable</label></td>
                            <td><%= string.Format("<input id='DischargeSummary_PatientConditionImproved' name='DischargeSummary_PatientCondition' value='2' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("2") ? "checked='checked'" : "")%><label for="DischargeSummary_PatientConditionImproved" class="radio">Improved</label></td>
                        </tr><tr>
                            <td><%= string.Format("<input id='DischargeSummary_PatientConditionUnchanged' name='DischargeSummary_PatientCondition' value='3' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("3") ? "checked='checked'" : "")%><label for="DischargeSummary_PatientConditionUnchanged" class="radio">Unchanged</label></td>
                            <td><%= string.Format("<input id='DischargeSummary_PatientConditionUnstable' name='DischargeSummary_PatientCondition' value='4' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("4") ? "checked='checked'" : "")%><label for="DischargeSummary_PatientConditionUnstable" class="radio">Unstable</label></td>
                        </tr><tr>
                            <td><%= string.Format("<input id='DischargeSummary_PatientConditionDeclined' name='DischargeSummary_PatientCondition' value='5' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("5") ? "checked='checked'" : "")%><label for="DischargeSummary_PatientConditionDeclined" class="radio">Declined</label></td>
                            <td><%= string.Format("<input id='DischargeSummary_PatientConditionGoalsMet' name='DischargeSummary_PatientCondition' value='6' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("6") ? "checked='checked'" : "")%><label for="DischargeSummary_PatientConditionGoalsMet" class="radio">Goals Met</label></td>
                        </tr><tr>
                            <td><%= string.Format("<input id='DischargeSummary_PatientConditionGoalsPartiallyMet' name='DischargeSummary_PatientCondition' value='7' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("7") ? "checked='checked'" : "")%><label for="DischargeSummary_PatientConditionGoalsPartiallyMet" class="radio">GoalsNot Met</label></td>
                            <td><%= string.Format("<input id='DischargeSummary_PatientConditionGoalsNotMet' name='DischargeSummary_PatientCondition' value='8' class='radio float_left' type='checkbox' {0} />", patientCondition != null && patientCondition.Contains("8") ? "checked='checked'" : "")%><label for="DischargeSummary_PatientConditionGoalsNotMet" class="radio">Goals Partially Met</label></td>
                        </tr>
                    </tbody></table>
                </td><td colspan="2">
                    <table class="fixed align_left"><tbody>
                        <tr><%string[] serviceProvided = data.ContainsKey("ServiceProvided") && data["ServiceProvided"].Answer != "" ? data["ServiceProvided"].Answer.Split(',') : null; %><input name="DischargeSummary_ServiceProvided" value=" " type="hidden" />
                            <td><%= string.Format("<input id='DischargeSummary_ServiceProvidedSN' name='DischargeSummary_ServiceProvided' value='1' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("1") ? "checked='checked'" : "")%><label for="DischargeSummary_ServiceProvidedSN" class="radio">SN</label></td>
                            <td><%= string.Format("<input id='DischargeSummary_ServiceProvidedPT' name='DischargeSummary_ServiceProvided' value='2' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("2") ? "checked='checked'" : "")%><label for="DischargeSummary_ServiceProvidedPT" class="radio">PT</label></td>
                        </tr><tr>
                            <td><%= string.Format("<input id='DischargeSummary_ServiceProvidedOT' name='DischargeSummary_ServiceProvided' value='3' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("3") ? "checked='checked'" : "")%><label for="DischargeSummary_ServiceProvidedOT" class="radio">OT</label></td>
                            <td><%= string.Format("<input id='DischargeSummary_ServiceProvidedST' name='DischargeSummary_ServiceProvided' value='4' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("4") ? "checked='checked'" : "")%><label for="DischargeSummary_ServiceProvidedST" class="radio">ST</label></td>
                        </tr><tr>
                            <td><%= string.Format("<input id='DischargeSummary_ServiceProvidedMSW' name='DischargeSummary_ServiceProvided' value='5' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("5") ? "checked='checked'" : "")%><label for="DischargeSummary_ServiceProvidedMSW" class="radio">MSW</label></td>
                            <td><%= string.Format("<input id='DischargeSummary_ServiceProvidedHHA' name='DischargeSummary_ServiceProvided' value='6' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("6") ? "checked='checked'" : "")%><label for="DischargeSummary_ServiceProvidedHHA" class="radio">HHA</label></td>
                        </tr><tr>
                            <td colspan="2"><%= string.Format("<input id='DischargeSummary_ServiceProvidedOther' name='DischargeSummary_ServiceProvided' value='7' class='radio float_left' type='checkbox' {0} />", serviceProvided != null && serviceProvided.Contains("7") ? "checked='checked'" : "")%><label for="DischargeSummary_ServiceProvidedOther" class="radio">Other</label><%= Html.TextBox("DischargeSummary_ServiceProvidedOtherValue", data.ContainsKey("ServiceProvidedOtherValue") ? data["ServiceProvidedOtherValue"].Answer : "", new { @id = "DischargeSummary_ServiceProvidedOtherValue", @class = "oe" })%></td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
            <tr><th colspan="2">Care Summary</th><th colspan="2">Condition of Discharge</th></tr>
            <tr>
                <td colspan="2">
                    <em>(Care Given, Progress, Regress including Therapies)</em>
                    <div class="float_right"><label for="DischargeSummary_CareSummaryTemplates">Templates:</label><%= Html.Templates("DischargeSummary_CareSummaryTemplates", new { @class = "Templates", @template = "#DischargeSummary_CareSummary" })%></div>
                    <%= Html.TextArea("DischargeSummary_CareSummary",data.ContainsKey("CareSummary") ? data["CareSummary"].Answer : "", new { @class = "fill", @id = "DischargeSummary_CareSummary", @style="height:150px;" })%>
                </td><td colspan="2">
                    <em>(Include VS, BS, Functional and Overall Status)</em>
                    <div class="float_right"><label for="DischargeSummary_ConditionOfDischargeTemplates">Templates:</label><%= Html.Templates("DischargeSummary_ConditionOfDischargeTemplates", new { @class = "Templates", @template = "#DischargeSummary_ConditionOfDischarge" })%></div>
                    <%= Html.TextArea("DischargeSummary_ConditionOfDischarge", data.ContainsKey("ConditionOfDischarge") ? data["ConditionOfDischarge"].Answer : "", new { @class = "fill", @id = "DischargeSummary_ConditionOfDischarge", @style = "height:150px;" })%>
                </td>
            </tr>
            <tr><th colspan="4">Discharge Details</th></tr>
            <tr class="align_left">
                <td colspan="2">
                    <label class="strong">Discharge Disposition: Where is the Patient after Discharge from your Agency?</label><%= Html.Hidden("DischargeSummary_DischargeDisposition", " ", new { @id = "" })%>
                    <div><%= Html.RadioButton("DischargeSummary_DischargeDisposition", "01", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "01" ? true : false, new { @id = "DischargeSummary_DischargeDisposition1", @class = "radio float_left" })%><label for="DischargeSummary_DischargeDisposition1"><span class="float_left">1 &#8211;</span><span class="normal margin">Patient remained in the community (without formal assistive services)</span></label></div>
                    <div><%= Html.RadioButton("DischargeSummary_DischargeDisposition", "02", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "02" ? true : false, new { @id = "DischargeSummary_DischargeDisposition2", @class = "radio float_left" })%><label for="DischargeSummary_DischargeDisposition2"><span class="float_left">2 &#8211;</span><span class="normal margin">Patient remained in the community (with formal assistive services)</span></label></div>
                    <div><%= Html.RadioButton("DischargeSummary_DischargeDisposition", "03", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "03" ? true : false, new { @id = "DischargeSummary_DischargeDisposition3", @class = "radio float_left" })%><label for="DischargeSummary_DischargeDisposition3"><span class="float_left">3 &#8211;</span><span class="normal margin">Patient transferred to a non-institutional hospice)</span></label></div>
                    <div><%= Html.RadioButton("DischargeSummary_DischargeDisposition", "04", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "04" ? true : false, new { @id = "DischargeSummary_DischargeDisposition4", @class = "radio float_left" })%><label for="DischargeSummary_DischargeDisposition4"><span class="float_left">4 &#8211;</span><span class="normal margin">Unknown because patient moved to a geographic location not served by this agency</span></label></div>
                    <div><%= Html.RadioButton("DischargeSummary_DischargeDisposition", "UK", data.ContainsKey("DischargeDisposition") && data["DischargeDisposition"].Answer == "UK" ? true : false, new { @id = "DischargeSummary_DischargeDispositionUK", @class = "radio float_left" })%><label for="DischargeSummary_DischargeDispositionUK"><span class="float_left">UK &#8211;</span><span class="normal margin">Other unknown</span></label></div>
                </td><td colspan="2"><%string[] dischargeInstructionsGivenTo = data.ContainsKey("DischargeInstructionsGivenTo") && data["DischargeInstructionsGivenTo"].Answer != "" ? data["DischargeInstructionsGivenTo"].Answer.Split(',') : null; %><input name="DischargeSummary_DischargeInstructionsGivenTo" value=" " type="hidden" />
                    <label class="float_left">Discharge Instructions Given To:</label>
                    <div class="float_left">
                        <div class="float_left"><%= string.Format("<input id='DischargeSummary_DischargeInstructionsGivenTo1' name='DischargeSummary_DischargeInstructionsGivenTo' value='1' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("1") ? "checked='checked'" : "")%><label for="DischargeSummary_DischargeInstructionsGivenTo1" class="fixed radio">Patient</label></div>
                        <div class="float_left"><%= string.Format("<input id='DischargeSummary_DischargeInstructionsGivenTo2' name='DischargeSummary_DischargeInstructionsGivenTo' value='2' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("2") ? "checked='checked'" : "")%><label for="DischargeSummary_DischargeInstructionsGivenTo2" class="fixed radio">Caregiver</label></div>
                        <div class="float_left"><%= string.Format("<input id='DischargeSummary_DischargeInstructionsGivenTo3' name='DischargeSummary_DischargeInstructionsGivenTo' value='3' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("3") ? "checked='checked'" : "")%><label for="DischargeSummary_DischargeInstructionsGivenTo3" class="fixed radio">N/A</label></div>
                        <div class="float_left"><%= string.Format("<input id='DischargeSummary_DischargeInstructionsGivenTo4' name='DischargeSummary_DischargeInstructionsGivenTo' value='4' class='radio' type='checkbox' {0} />", dischargeInstructionsGivenTo != null && dischargeInstructionsGivenTo.Contains("4") ? "checked='checked'" : "")%><label for="DischargeSummary_DischargeInstructionsGivenTo4" class="radio">Other:</label><%= Html.TextBox("DischargeSummary_DischargeInstructionsGivenToOther", data.ContainsKey("DischargeInstructionsGivenToOther") ? data["DischargeInstructionsGivenToOther"].Answer : "", new { @id = "DischargeSummary_DischargeInstructionsGivenToOther", @class="oe" })%></div>
                    </div>
                    <div class="clear"></div>
                    <label class="float_left">Verbalized understanding:</label>
                    <div class="float_left"><%= Html.Hidden("DischargeSummary_IsVerbalizedUnderstanding", " ", new { @id = "" })%><%= Html.RadioButton("DischargeSummary_IsVerbalizedUnderstanding", "1", data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "1" ? true : false, new { @id = "DischargeSummary_IsVerbalizedUnderstandingY", @class = "radio" })%><label for="DischargeSummary_IsVerbalizedUnderstandingY" class="inlineradio">Yes</label><%= Html.RadioButton("DischargeSummary_IsVerbalizedUnderstanding", "0", data.ContainsKey("IsVerbalizedUnderstanding") && data["IsVerbalizedUnderstanding"].Answer == "0" ? true : false, new { @id = "DischargeSummary_IsVerbalizedUnderstandingN", @class = "radio" })%><label for="DischargeSummary_IsVerbalizedUnderstandingN" class="inlineradio">No</label></div>
                    <div class="clear"></div><%string[] differentTasks = data.ContainsKey("DifferentTasks") && data["DifferentTasks"].Answer != "" ? data["DifferentTasks"].Answer.Split(',') : null; %><input name="DischargeSummary_DifferentTasks" value=" " type="hidden" />
                    <div><%= string.Format("<input id='DischargeSummary_DifferentTasks1' name='DischargeSummary_DifferentTasks' value='1' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("1") ? "checked='checked'" : "")%><label for="DischargeSummary_DifferentTasks1" style="margin-left: 20px;">All services notified and discontinued</label></div>
                    <div><%= string.Format("<input id='DischargeSummary_DifferentTasks2' name='DischargeSummary_DifferentTasks' value='2' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("2") ? "checked='checked'" : "")%><label for="DischargeSummary_DifferentTasks2" style="margin-left: 20px;">Order and summary completed</label></div>
                    <div><%= string.Format("<input id='DischargeSummary_DifferentTasks3' name='DischargeSummary_DifferentTasks' value='3' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("3") ? "checked='checked'" : "")%><label for="DischargeSummary_DifferentTasks3" style="margin-left: 20px;">Information provided to patient for continuing needs</label></div>
                    <div><%= string.Format("<input id='DischargeSummary_DifferentTasks4' name='DischargeSummary_DifferentTasks' value='4' class='radio' type='checkbox' {0} />", differentTasks != null && differentTasks.Contains("4") ? "checked='checked'" : "")%><label for="DischargeSummary_DifferentTasks4" style="margin-left: 20px;">Physician notified</label></div>
                </td>
            </tr>
            <tr><th colspan="4">Electronic Signature</th></tr>
            <tr>
                <td colspan="4">
                    <div class="third">
                        <label for="DischargeSummary_ClinicianSignature" class="float_left">Clinician Signature:</label>
                        <div class="float_right"><%= Html.Password("DischargeSummary_Clinician", "", new { @id = "DischargeSummary_Clinician" })%></div>
                    </div><div class="third"></div><div class="third">
                        <label for="DischargeSummary_ClinicianSignatureDate" class="float_left">Date:</label>
                        <div class="float_right"><input type="date" name="DischargeSummary_SignatureDate" value="<%= data.ContainsKey("SignatureDate") && data["SignatureDate"].Answer.IsNotNullOrEmpty() ? data["SignatureDate"].Answer : "" %>" id="DischargeSummary_SignatureDate" /></div>
                    </div>
                </td>
            </tr>
            <% if (Current.HasRight(Permissions.AccessCaseManagement) && !Current.UserId.ToString().IsEqual(Model.UserId.ToString())) {  %>
            <tr>
                <td colspan="4">
                    <div><%= string.Format("<input class='radio' id='{0}_ReturnForSignature' name='{0}_ReturnForSignature' type='checkbox' />", Model.Type)%> Return to Clinician for Signature</div>
                </td>
            </tr>
            <% } %>
        </tbody>
    </table>
    <input type="hidden" name="button" value="" id="DischargeSummary_Button" />
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="DischargeSummaryRemove(); dischargeSummary.Submit($(this));">Save</a></li>
            <li><a href="javascript:void(0);" onclick="DischargeSummaryAdd(); dischargeSummary.Submit($(this));">Complete</a></li>
        <% if (Current.HasRight(Permissions.AccessCaseManagement)) {  %>
            <li><a href="javascript:void(0);" onclick="DischargeSummaryRemove(); dischargeSummary.Submit($(this));">Approve</a></li>
            <% if (!Current.UserId.ToString().IsEqual(Model.UserId.ToString())) { %>
            <li><a href="javascript:void(0);" onclick="DischargeSummaryRemove(); dischargeSummary.Submit($(this));">Return</a></li>
            <% } %>
        <% } %>
            <li><a href="javascript:void(0);" onclick="DischargeSummaryRemove(); UserInterface.CloseWindow('dischargeSummary');">Exit</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $("#DischargeSummary_MR").attr('readonly', true);
    $("#DischargeSummary_EpsPeriod").attr('readonly', true);
    function DischargeSummaryAdd() {
        $("#DischargeSummary_Clinician").removeClass('required').addClass('required');
        $("#DischargeSummary_SignatureDate").removeClass('required').addClass('required');
    }
    function DischargeSummaryRemove() {
        $("#DischargeSummary_Clinician").removeClass('required');
        $("#DischargeSummary_SignatureDate").removeClass('required');
    }
</script>