﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Message>" %>
<% if (Model != null) { %>
<div class="messageContent" id="messageContent">
    <input type="hidden" id="messageId" value="<%= Model.Id %>" />
    <input type="hidden" id="messageType" value="<%= (int)Model.Type %>" />
    <div class="messageContentPanel" id="messageContentPanel">
        <div class="messageHeaderContainer">
            <div class="messageHeaderRow">
                <label for="messageSender">
                    From:</label><span id="messageSender"><%= Model.FromName %></span></div>
            <div class="messageHeaderRow">
                <label for="messageRecipients">
                    To:</label><span id="messageRecipients"><%= Model.RecipientNames %></span></div>
            <div class="messageHeaderRow">
                <label for="messageDate">
                    Sent:</label><span id="messageDate"><%= Model.MessageDate %></span></div>
            <div class="messageHeaderRow">
                <label for="messageSubject">
                    Subject:</label><span id="messageSubject"><%= Model.Subject %></span></div>
            <% if (Model.Type == MessageType.User) { %>
            <div class="messageHeaderRow"><label for="messageRegarding">Regarding:</label><span id="messageRegarding"><%= Model.PatientName %></span></div>
            <% if (Model.HasAttachment) { %><div class="messageHeaderRow"><label for="messageAttachment">Attachment:</label><span id="messageAttachment"><%= Html.Asset(Model.AttachmentId) %></span></div><% } %>
            <div class="messageHeaderRow messageHeaderButtons">
                <div class="buttons float_left"><ul>
                    <li><a href="javascript:void(0);" id="MessageReplyButton">Reply</a></li>
                    <li><a href="javascript:void(0);" id="MessageForwardButton">Forward</a></li>
                    <li><a href="javascript:void(0);" id="MessageDeleteButton">Delete</a></li>
                </ul></div>
            </div>
            <% } else { %>
            <div class="messageHeaderRow messageHeaderButtons">
                <div class="buttons float_left"><ul>
                    <li><a href="javascript:void(0);" id="MessageDeleteButton">Delete</a></li>
                </ul></div>
            </div>
            <% } %>
        </div>
        <div id="messageBodyContainer" class="messageBodyContainer">
            <div id="messageBody">
                <%= Model.Body %>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#messageContent .messageHeaderRow").each(function() {
        if ($(this).find("span").prop("scrollHeight") > $(this).find("span").prop("offsetHeight")) $(this).append(unescape("%3Ca href=%22javascript:void(0);%22 class=%22more%22 tooltip=%22" + $(this).find("span").html() + "%22%3EMore&#187;%3C/a%3E"));
        $(this).find("span").css("overflow", "hidden")
    });
    U.tooltip($("#messageContent .messageHeaderRow a.more"), "calday");
</script>
<% } else { %> 
<script type="text/javascript">
    $("#messageInfoResult").html(
        $("<div/>", { "class": "ajaxerror" }).append(
            $("<h1/>", { "text": "No Messages found." })).append(
            $("<div/>", { "class": "heading" }).Buttons([{
                Text: "Add New Message",
                Click: function() {
                    UserInterface.ShowNewMessage()
                }
            }])
        )
    );
</script>
<% } %>
