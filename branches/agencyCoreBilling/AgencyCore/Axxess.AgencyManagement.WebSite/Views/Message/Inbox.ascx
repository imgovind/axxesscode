﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "messageinbox",
        Current.DisplayName + "&#8217;s Messages",
        Current.AgencyName)%>
<div id="messagingContainer" class="wrapper layout">
    <div class="layout_left" style="min-width: 300px;">
        <% Html.RenderPartial("~/Views/Message/Grid.ascx"); %>
    </div>
    <div id="messageInfoResult" class="layout_main">
        <% Html.RenderPartial("~/Views/Message/Info.ascx"); %>
    </div>
</div>
<script type="text/javascript">
    $('#window_messageinbox .layout').layout({ west: { paneSelector: '.layout_left', size: 275 } });
</script>

