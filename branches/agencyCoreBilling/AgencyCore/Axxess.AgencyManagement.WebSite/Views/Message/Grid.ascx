﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="messageList" id="messageList">
    <% Html.Telerik().Grid<Message>()
        .Name("List_Messages")
        .ToolBar(commnds => commnds.Custom().HtmlAttributes(new { @id = "List_Messages_NewButton", @href = "javascript:void(0);", @onclick = "UserInterface.ShowNewMessage();" }).Text("New Message"))
        .Columns(columns =>
        {
            columns.Bound(m => m.FromName)
                .ClientTemplate("<div id='<#=Id#>' class='message <#=MarkAsRead#>'>"
                    + "<div><span class='float_right'>"
                    + "<#=Date#></span></div>"
                    + "<div><span><#=FromName#></span>"
                    + "<br /><span class='normal'><#=Subject#></span></div></div>").Title("Inbox").HeaderHtmlAttributes(new { style = "font-size: 14px;" });
            columns.Bound(m => m.Id).Title("").Hidden(true).HeaderHtmlAttributes(new { @style = "display: none" });
            columns.Bound(m => m.Type).Title("").Hidden(true).HeaderHtmlAttributes(new { @style = "display: none" });
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Message", new { inboxType = "inbox" }))
        .ClientEvents(events => events.OnDataBound("Message.OnDataBound").OnRowSelected("Message.OnRowSelected"))
        .Pageable(paging => paging.PageSize(50)).Sortable().Selectable().Footer(false)
        .Scrollable(scrolling => scrolling.Enabled(true)).Render();
    %>
</div>

<script type="text/javascript">
    $("#List_Messages .t-grid-toolbar").css({ 'height': '40px'});
    $("#List_Messages .t-grid-toolbar").html(unescape("%3Cdiv style=%22margin-top: 10px;margin-bottom: 5px;%22%3E%3Cdiv class=%22float_left%22%3E%3Cselect id=%22inboxType%22%3E%3Coption value=%22inbox%22%3EInbox%3C/option%3E%3Coption value=%22sent%22%3ESent Messages%3C/option%3E%3C/select%3E%3C/div%3E%3Cdiv class=%22buttons float_right%22%3E%3Cul%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewMessage(); return false;%22%3ENew Message%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
    $(".t-grid-content").css({ 'height': 'auto', 'bottom': '0' });
</script>