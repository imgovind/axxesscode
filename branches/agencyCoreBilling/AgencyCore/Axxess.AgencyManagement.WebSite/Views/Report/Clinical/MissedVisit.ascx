﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ClinicalMissedVisit"; %>
<div class="wrapper main">
    <fieldset>
         <legend> Missed Visit</legend>
         <div class="column">
            <div class="row"><label for="BranchCode" class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label  class="float_left">Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="ReportClinicalMissedVisit_StartDate" class="shortdate" /><label> To </label><input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="ReportClinicalMissedVisit_EndDate" class="shortdate" /></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindClinicalMissedVisit();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportClinicalMissedVisit", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>Result" class="ReportGrid">
        <% =Html.Telerik().Grid<MissedVisit>().Name(pagename + "Grid")        
                 .Columns(columns =>
                 {
                 columns.Bound(m => m.PatientIdNumber).Title("ID").Width(70);
                 columns.Bound(m => m.PatientName).Title("Patient Name");
                 columns.Bound(m => m.Date).Title("Date").Format("{0:MM/dd/yyyy}").Width(80);
                 columns.Bound(m => m.DisciplineTaskName).Title("Task");
                 columns.Bound(p => p.UserName).Title("User Name").Width(155);
               })
               .DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new {BranchId=Guid.Empty, StartDate = DateTime.Now.AddDays(-60), EndDate = DateTime.Now }))
               .Sortable().Selectable().Scrollable().Footer(false) %>
    </div>
</div>
<script type="text/javascript">    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>
