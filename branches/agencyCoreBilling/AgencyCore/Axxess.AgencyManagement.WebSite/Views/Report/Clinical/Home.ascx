﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul>
    <li class="widget">
        <div class="widget-head"><h5>Clinical Reports</h5></div>
        <div class="widget-content" style="height: 100%;">
            <div class="report_links">
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Clinical/OpenOasis" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Open OASIS</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/MissedVisits" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Missed Visit Report</span></a></li> 
                </ul>
                <ul class="float_left half"> 
                    <li class="link"><a href="javascript:void(0);" onclick="Acore.Open('ordersmanagement');"><span class="title">Orders Management</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/PhysicianOrderHistory" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Physician Order History</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/PlanOfCareHistory" onclick="Report.Show(2, '#clinical_reports', $(this).attr('href')); return false;"><span class="title">Plan Of Care History</span></a></li> 
                </ul>
                <div class="clear">&#160;</div>
            </div>
        </div>
    </li>
</ul>