﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "StatisticalEmployeeVisitHistory"; %>
<div class="wrapper">
    <fieldset>
        <legend> Employee Visit History </legend>
        <div class="column">
            <div class="row"><label  class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label  class="float_left">Status:</label><div class="float_right"><select id="StatisticalEmployeeVisitHistory_Status" name="StatisticalEmployeeVisitHistory_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Inactive</option></select></div></div>
            <div class="row"><label  class="float_left">Employee:</label><div class="float_right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, pagename + "_Users", "", Guid.Empty, 1, new { @id = pagename + "_Users", @class = "report_input valid" })%></div> </div>
            <div class="row"><label  class="float_left">Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="shortdate" /></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindStatisticalEmployeeVisitHistory();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportStatisticalEmployeeVisitHistory", new { userId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="StatisticalEmployeeVisitHistory_ReportGrid" class="ReportGrid">
        <%= Html.Telerik().Grid<UserVisit>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(s => s.TaskName).Title("Task").ReadOnly();
               columns.Bound(s => s.StatusName).Title("Status").ReadOnly();
               columns.Bound(s => s.ScheduleDate).Title("Schedule Date").ReadOnly();
               columns.Bound(s => s.VisitDate).Title("Visit Date").ReadOnly();
               columns.Bound(s => s.PatientName).Title("Patient");
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { userId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $("#StatisticalEmployeeVisitHistory_ReportGrid").css({ 'top': '190px' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_Status').change(function() { Report.loadUsersDropDown('<%= pagename %>'); }); 
</script>

