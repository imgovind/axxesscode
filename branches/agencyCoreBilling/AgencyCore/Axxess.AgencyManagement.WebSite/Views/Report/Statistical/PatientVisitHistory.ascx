﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "StatisticalPatientVisitHistory"; %>
<div class="wrapper">
    <fieldset>
        <legend> Patient Visit History </legend>
        <div class="column">
            <div class="row"><label  class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label  class="float_left">Status:</label><div class="float_right"><select id="StatisticalPatientVisitHistory_Status" name="StatisticalPatientVisitHistory_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
            <div class="row"><label  class="float_left">Patient:</label> <div class="float_right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Patients, pagename + "_Patients", "",Guid.Empty,1, new { @id = pagename + "_Patients", @class = "report_input valid" })%></div> </div>
            <div class="row"><label  class="float_left">Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="shortdate" /></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindStatisticalPatientVisitHistory();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportStatisticalPatientVisitHistory", new { patientId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="StatisticalPatientVisitHistory_ReportGrid" class="ReportGrid">
        <%= Html.Telerik().Grid<ScheduleEvent>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(s => s.DisciplineTaskName).Title("Task").Sortable(false).ReadOnly();
               columns.Bound(s => s.StatusName).Title("Status").Sortable(false).ReadOnly();
               columns.Bound(s => s.EventDate).Title("Schedule Date").Sortable(false).ReadOnly();
               columns.Bound(s => s.VisitDate).Title("Visit Date").Sortable(false).ReadOnly();
               columns.Bound(s => s.UserName).Title("Employee");
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { patientId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $("#StatisticalPatientVisitHistory_ReportGrid").css({'top':'190px'});
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_Status').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); }); 
</script>
