﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ClaimsByStatus"; %>
<div class="wrapper">
    <fieldset>
        <legend> Claims By The Status </legend>
        <div class="column">
            <div class="row"><label  class="float_left">Branch:</label><div class="float_right"><%= Html.ReportBranchList("AddressBranchCode", "", new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label  class="float_left">Bill Type:</label><div class="float_right"><% var billType = new SelectList(new[] { new SelectListItem { Text = "All", Value = "All" }, new SelectListItem { Text = "RAP", Value = "RAP" }, new SelectListItem { Text = "Final", Value = "Final" }}, "Value", "Text", "All");%><%= Html.DropDownList(pagename + "_BillType", billType, new { @id = pagename + "_BillType", @class = "oe" })%></div> </div>
            <div class="row"><label  class="float_left">Bill Status:</label><div class="float_right"><%var status = new SelectList(new[] { new SelectListItem { Value = "300", Text = "Claim Created" }, new SelectListItem { Value = "305", Text = "Claim Submitted" }, new SelectListItem { Value = "310", Text = "Claim Rejected" }, new SelectListItem { Value = "315", Text = "Payment Pending" }, new SelectListItem { Value = "320", Text = "Claim Accepted/Processing" }, new SelectListItem { Value = "325", Text = "Claim With Errors" }, new SelectListItem { Value = "330", Text = "Paid Claim" }, new SelectListItem { Value = "335", Text = "Cancelled Claim" } }, "Value", "Text", 300);%><%= Html.DropDownList(pagename+"_Status", status)%></div></div> 
            <div class="row"><label  class="float_left">Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="shortdate" /></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindClaimsByStatus();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportClaimsByStatus", new { BranchId = Guid.Empty, type = "All", Status = 300, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now}, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>_ReportGrid" class="ReportGrid">
        <%= Html.Telerik().Grid<ClaimLean>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("ID").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient Name");
               columns.Bound(p => p.EpisodeRange).Title("Episode Range").Width(150);
               columns.Bound(p => p.Type).Width(60);
               columns.Bound(p => p.ClaimAmount).Title("Claim Amount").Width(95);
               columns.Bound(p => p.ClaimDateFormatted).Title("Claim Date").Width(80);
               columns.Bound(p => p.PaymentAmount).Title("Payment Amount").Width(110);
               columns.Bound(p => p.PaymentDateFormatted).Title("Payment Date").Width(90);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, type = "All", Status = 300, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); $("#<%= pagename %>_ReportGrid").css({ 'top': '190px' });</script>
