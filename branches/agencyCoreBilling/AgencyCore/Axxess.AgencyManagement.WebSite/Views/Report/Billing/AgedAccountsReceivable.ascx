﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "AgedAccountsReceivable"; %>
<div class="wrapper">
    <fieldset>
        <legend>Aged Accounts Receivable</legend>
        <div class="column">
          <div class="row"><label  class="float_left">Branch:</label><div class="float_right"><%= Html.ReportBranchList("AddressBranchCode", "", new { @id = pagename +"_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
          <div class="row"><label  class="float_left">Insurance:</label><div class="float_right"><%= Html.InsurancesMedicare("Insurance", "0",true,"All", new { @id = pagename + "_Insurance", @class = "report_input" })%></div></div>
          <div class="row"><label  class="float_left">Bill Type:</label><div class="float_right"><% var billType = new SelectList(new[] { new SelectListItem { Text = "All", Value = "All" }, new SelectListItem { Text = "RAP", Value = "RAP" }, new SelectListItem { Text = "Final", Value = "Final" }}, "Value", "Text", "All");%><%= Html.DropDownList(pagename + "_BillType", billType, new { @id = pagename + "_BillType", @class = "oe" })%></div> </div>
          <div class="row"><label  class="float_left">Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="shortdate" /></div></div>
        </div>
        <div class="column"><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindAgedAccountsReceivable();">Generate Report</a></li></ul></div><div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportAgedAccountsReceivable", new { BranchId = Guid.Empty, Insurance = 0, type = "All", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>_ReportGrid" class="ReportGrid">
        <%= Html.Telerik().Grid<ClaimLean>().Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(p => p.DisplayName).Title("Patient Name");
               columns.Bound(p => p.EpisodeRange).Title("Episode Range").Width(150);
               columns.Bound(p => p.Type).Width(50);
               columns.Bound(p => p.StatusName).Title("Status");
               columns.Bound(p => p.ClaimDateFormatted).Title("Claim Date").Width(80);
               columns.Bound(p => p.Amount30).Format("{0:0.00}").Title("1-30").Width(60);
               columns.Bound(p => p.Amount60).Format("{0:0.00}").Title("31-60").Width(60);
               columns.Bound(p => p.Amount90).Format("{0:0.00}").Title("61-90").Width(60);
               columns.Bound(p => p.AmountOver90).Format("{0:0.00}").Title("> 90").Width(60);
               columns.Bound(p => p.ClaimAmount).Format("{0:0.00}").Title("Total");
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, Insurance = 0, type = "All", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); $("#<%= pagename %>_ReportGrid").css({ 'top': '190px' });</script>
