﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "BillingBatch"; %>
<div class="wrapper">
    <fieldset>
        <legend>Billing Batch</legend>
        <div class="column">
              <div class="row"><label class="float_left">Claim Type:</label><div class="float_right"><%= Html.ClaimTypes("ClaimType", new { @id = string.Format("{0}_ClaimType", pagename) })%></div></div>
              <div class="row"><label class="float_left">Date:</label><div class="float_right"><input type="date" name="BatchDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_BatchDate" class="shortdate" /></div></div>
        </div>
        <div class="column"><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindBillingBatch();">Generate Report</a></li></ul></div><div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportBillingBatch", new { BranchId = Guid.Empty, Insurance = 0, type = "All", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="<%= pagename %>_ReportGrid" class="ReportGrid">
        <%= Html.Telerik().Grid<ClaimInfoDetail>().Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(p => p.PatientIdNumber).Title("MRN").Width(80);
               columns.Bound(p => p.DisplayName).Title("Patient Name");
               columns.Bound(p => p.Range).Title("Episode Range").Width(150);
               columns.Bound(p => p.MedicareNumber).Title("Medicare Number");
               columns.Bound(p => p.ProspectivePay).Format("${0:0.00}").Title("Amount");
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { ClaimType = "ALL", BatchDate = DateTime.Now })).Sortable().Scrollable().Footer(false)%>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); $("#<%= pagename %>_ReportGrid").css({ 'top': '190px' });</script>
