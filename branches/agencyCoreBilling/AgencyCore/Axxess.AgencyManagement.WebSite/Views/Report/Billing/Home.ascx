﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul>
    <li class="widget">
        <div class="widget-head"><h5>Billing Reports</h5></div>
        <div class="widget-content" style="height: 100%;">
            <div class="report_links">
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Billing/OutstandingClaims" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Outstanding Claims</span></a></li> 
                    <li class="link"><a href="/Report/Billing/ByStatusSummary" onclick="Report.Show(4, '#billing_reports', $(this).attr('href')); return false;"><span class="title">Claims History By Status</span></a></li> 
                        </ul>
                <ul class="float_left half"> 
                </ul>
                <div class="clear">&#160;</div>
            </div>
        </div>
    </li>
</ul>