﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "EmployeeVisitByDateRange"; %>
<div class="wrapper">
    <fieldset>
        <legend> Employee Visit By Date Range </legend>
        <div class="column">
            <div class="row"><label  class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label  class="float_left">Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-15).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.AddDays(15).ToShortDateString() %>" id="<%= pagename %>_EndDate" class="shortdate" /></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindEmployeeVisitByDateRange();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportEmployeeVisitByDateRange", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-15), EndDate = DateTime.Now.AddDays(15) }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="EmployeeVisitByDateRange_ReportGrid" class="ReportGrid">
        <%= Html.Telerik().Grid<UserVisit>().Name(pagename+"Grid").Columns(columns =>
           {
               columns.Bound(s => s.UserDisplayName).Title("Employee").ReadOnly();
               columns.Bound(s => s.PatientName).Title("Patient");
               columns.Bound(s => s.TaskName).Title("Task").ReadOnly();
               columns.Bound(s => s.ScheduleDate).Title("Schedule Date").Width(90).ReadOnly();
               columns.Bound(s => s.VisitDate).Title("Visit Date").Width(90).ReadOnly();
               columns.Bound(s => s.StatusName).Title("Status").ReadOnly();
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-15), EndDate = DateTime.Now.AddDays(15) })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>
