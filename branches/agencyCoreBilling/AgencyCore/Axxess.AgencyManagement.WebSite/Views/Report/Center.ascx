﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "reportcenter",
        "Report Center",
        Current.AgencyName)%>
<div id="reportingTabs" class="tabs vertical-tabs vertical-tabs-left ReportingContainer">
    <ul id="reportingTabContainer" class="verttab strong">
        <li><a href="#all_reports" title="/Report/Dashboard">Reports Home</a></li>
       
    </ul>
    <div id="all_reports" class="general">
        <% Html.RenderPartial("~/Views/Report/Dashboard.ascx"); %>
    </div>
</div>
