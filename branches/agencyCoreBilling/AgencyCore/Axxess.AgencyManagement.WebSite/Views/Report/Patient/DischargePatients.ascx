﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "DischargePatients"; %>
<div class="wrapper">
    <fieldset>
        <legend> Discharge Patients</legend>
        <div class="column">
           <div class="row"><label for="AddressBranchCode" class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div></div>
           <div class="row"><label  class="float_left">Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="shortdate" /></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindDischargePatients();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportDischargePatients", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>Result" class="ReportGrid">
        <% =Html.Telerik().Grid<DischargePatient>().Name(pagename + "Grid").Columns(columns =>
         {
         columns.Bound(p => p.PatientIdNumber).Title("MR#").Width(90);
         columns.Bound(p => p.LastName).Title("Last Name");
         columns.Bound(p => p.FirstName).Title("First Name");
         columns.Bound(p => p.StartofCareDate).Format("{0:MM/dd/yyyy}").Title("SOC Date").Width(100);
         columns.Bound(p => p.DischargeDate).Format("{0:MM/dd/yyyy}").Title("Discharge Date").Width(100);
         columns.Bound(p => p.DischargeReason).Title("Reason");
         }).DataBinding(dataBinding => dataBinding.Ajax().Select("DischargePatients", "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Selectable().Scrollable().Footer(false)%>
    </div>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>
