﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul>
    <li class="widget">
        <div class="widget-head"><h5>Patient Reports</h5></div>
        <div class="widget-content" style="height: 100%;">
            <div class="report_links">
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Patient/Roster" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient Roster</span></a></li> 
                    <li class="link"><a href="/Report/Patient/EmergencyList" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Emergency Contact Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/Birthdays" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient Birthday Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/AddressList" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient Address Listing</span></a></li> 
                </ul>
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Patient/Physician" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient By Physician Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/SocCertPeriod" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient SOC Cert Period Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/ResponsibleEmployee" onclick="Report.Show(1, '#patient_reports', $(this).attr('href')); return false;"><span class="title">Patient by Responsible Employee Listing</span></a></li> 
                </ul>
                <div class="clear">&#160;</div>
            </div>
        </div>
    </li>
</ul>