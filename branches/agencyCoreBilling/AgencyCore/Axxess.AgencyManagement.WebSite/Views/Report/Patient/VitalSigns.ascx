﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientVitalSigns"; %>
<div class="wrapper">
    <fieldset>
        <legend> Vital Signs Report </legend>
        <div class="column">
            <div class="row"><label  class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid", @tabindex = "3" })%></div> </div>
            <div class="row"><label  class="float_left">Status:</label><div class="float_right"><select id="PatientVitalSigns_Status" name="PatientVitalSigns_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
            <div class="row"><label  class="float_left">Patient:</label> <div class="float_right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Patients, pagename + "_Patients", "",Guid.Empty,1, new { @id = pagename + "_Patients", @class = "report_input valid", @tabindex = "1" })%></div> </div>
            <div class="row"><label  class="float_left">Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="shortdate" /></div></div>
        </div>
        <div class="column">
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientVitalSigns();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "VitalSigns","Export", new { patientId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div id="PatientVitalSigns_ReportGrid" class="ReportGrid">
        <%= Html.Telerik().Grid<VitalSign>().Name(pagename + "Grid").Columns(columns =>
           {
               columns.Bound(s => s.VisitDate).Width(70).Title("Visit Date").Sortable(true).ReadOnly();
               columns.Bound(s => s.UserDisplayName).Width(100).Title("Employee Name").Sortable(false).ReadOnly();
               columns.Bound(s => s.DisciplineTask).Width(100).Title("Task").Sortable(false).ReadOnly();
               columns.Bound(s => s.BPLying).Title("BP Lying").Sortable(false).Width(70).ReadOnly();
               columns.Bound(s => s.BPSitting).Title("BP Sit").Sortable(false).Width(70).ReadOnly();
               columns.Bound(s => s.BPStanding).Title("BP Stand").Sortable(false).Width(70).ReadOnly();
               columns.Bound(s => s.Temp).Title("Temp").Sortable(false).Width(45);
               columns.Bound(s => s.Resp).Title("Resp").Sortable(false).Width(45).ReadOnly();
               columns.Bound(s => s.ApicalPulse).Title("Apical Pulse").Sortable(false).Width(80);
               columns.Bound(s => s.RadialPulse).Title("Radial Pulse").Width(80).Sortable(false).ReadOnly();
               columns.Bound(s => s.BS).Title("BS").Sortable(false).Width(30);
               columns.Bound(s => s.Weight).Sortable(false).Title("Weight").Width(50);
               columns.Bound(s => s.PainLevel).Sortable(false).Title("Pain Level").Width(60);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { patientId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $("#PatientVitalSigns_ReportGrid").css({ 'top': '190px' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_Status').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); }); 
</script>
