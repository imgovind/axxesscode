﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ExpiringAuthorizations"; %>
<div class="wrapper">
    <fieldset>
        <legend>Expiring Authorizations</legend>
        <div class="column">
            <div class="row"><label for="PatientRoster_BranchCode" class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Branches, "PatientRoster_BranchCode", "", new { @id = "PatientRoster_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label for="ExpiringAuthorizations_Status" class="float_left">Status:</label><div class="float_right"><select id="ExpiringAuthorizations_Status" name="Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindExpiringAuthorizations();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportExpiringAuthorizations", new { BranchCode = Guid.Empty, Status = 1 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="ReportGrid">
         <%= Html.Telerik().Grid<Authorization>().Name(pagename + "Grid").Columns(columns =>
            {
                columns.Bound(r => r.DisplayName).Title("Patient");
                columns.Bound(r => r.Status).Title("Status");
                columns.Bound(r => r.Number).Title("Number");
                columns.Bound(r => r.StartDateFormatted).Title("Start Date");
                columns.Bound(r => r.EndDateFormatted).Title("End Date");
            }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { Status = 1, BranchCode = Guid.Empty })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript">    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });</script>
