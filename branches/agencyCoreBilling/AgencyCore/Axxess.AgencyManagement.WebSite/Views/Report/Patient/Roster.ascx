﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientRoster"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Roster</legend>
        <div class="column">
            <div class="row"><label for="PatientRoster_BranchCode" class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @id = "PatientRoster_BranchCode", @class = "AddressBranchCode report_input" })%></div></div>
            <div class="row"><label for="PatientRoster_Status" class="float_left">Status:</label><div class="float_right"><select id="PatientRoster_Status" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
            <div class="row"><label class="float_left">Insurance:</label><div class="float_right"><%=Html.Insurances("InsuranceId", "0",new { @id = pagename + "_Insurance", @class = "report_input" })%></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientRoster();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientRoster", new { BranchCode = Guid.Empty, StatusId = 1, InsuranceId = 0 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div class="clear">&#160;</div>
    <div class="ReportGrid">
         <%= Html.Telerik().Grid<PatientRoster>().Name(pagename + "Grid").Columns(columns =>
            {
                columns.Bound(r => r.PatientId).Title("MR#").Width(70);
                columns.Bound(r => r.PatientDisplayName).Title("Patient");
                columns.Bound(r => r.PatientMedicareNumber).Title("Medicare #").Width(85);
                columns.Bound(r => r.PatientSoC).Title("SOC");
                columns.Bound(r => r.PatientDischargeDate).Title("Discharge Date");
                columns.Bound(r => r.AddressFull).Title("Address");
                columns.Bound(r => r.PatientDOB).Format("{0:MM/dd/yyyy}").Title("DOB").Width(75);
                columns.Bound(r => r.PatientPhone).Title("Home Phone").Width(100);
                columns.Bound(r => r.PatientGender).Title("Gender").Width(60);
                columns.Bound(r => r.Triage).Title("Triage").Width(60);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { BranchCode = Guid.Empty, StatusId = 1, InsuranceId = 0 })).Sortable().Scrollable().Footer(false)
        %>
    </div>
</div>
<script type="text/javascript"> $('#PatientRosterGrid .t-grid-content').css({ 'height': 'auto' }); </script>