﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientRoster>>" %>
<% string pagename = "PatientByResponsibleEmployeeListing"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient By Responsible Employee Listing</legend>
        <div class="column">
            <div class="row"><label  class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div></div>
            <div class="row"><label  class="float_left">Status:</label><div class="float_right"><select id="PatientByResponsibleEmployeeListing_Status" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected="selected">Active</option><option value="2">Inactive</option></select></div></div>
            <div class="row"><label  class="float_left">Employee:</label><div class="float_right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Users, "UserId", "",Guid.Empty,1, new { @id = pagename + "_Users", @class = "report_input valid" })%></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientByResponsibleEmployee();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientByResponsibleEmployeeListing", new { UserId = Guid.Empty, BranchId = Guid.Empty, StatusId = 1 }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>Result" class="ReportGrid">
    <%= Html.Telerik().Grid<PatientRoster>().Name(pagename + "Grid").Columns(columns => {
        columns.Bound(p => p.PatientId).Title("MRN").Width(80);
        columns.Bound(p => p.PatientLastName).Title("Last Name").Width(150);
        columns.Bound(p => p.PatientFirstName).Title("First Name").Width(150);
        columns.Bound(p => p.AddressFull).Title("Address");
        columns.Bound(p => p.PatientSoC).Title("SOC Date").Width(80);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { UserId = Guid.Empty, BranchId = Guid.Empty, StatusId = 1 })).Sortable().Selectable().Scrollable().Footer(false) %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_Status').change(function() { Report.loadUsersDropDown('<%= pagename %>'); });
 </script>