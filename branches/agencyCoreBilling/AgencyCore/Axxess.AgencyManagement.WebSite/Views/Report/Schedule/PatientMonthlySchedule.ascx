﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "PatientMonthlySchedule"; %>
<div class="wrapper">
    <fieldset>
        <legend>Patient Monthly Schedule</legend>
        <div class="column">
           <div class="row"><label class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
           <div class="row"><label class="float_left">Status:</label><div class="float_right"><select id="PatientMonthlySchedule_Status" name="PatientMonthlySchedule_Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></div>
           <div class="row"><label class="float_left">Patient:</label> <div class="float_right"><%= Html.LookupSelectListWithBranchAndStatus(SelectListTypes.Patients, pagename + "_Patients", "",Guid.Empty,1, new { @id = pagename + "_Patients", @class = "report_input valid" })%></div> </div>
           <div class="row"><label class="float_left">Month:</label><div class="float_right"><% var months = new SelectList(new[] { new SelectListItem { Text = "Select Month", Value = "0" }, new SelectListItem { Text = "January", Value = "1" }, new SelectListItem { Text = "February", Value = "2" }, new SelectListItem { Text = "March", Value = "3" }, new SelectListItem { Text = "April", Value = "4" }, new SelectListItem { Text = "May", Value = "5" }, new SelectListItem { Text = "June", Value = "6" }, new SelectListItem { Text = "July", Value = "7" }, new SelectListItem { Text = "August", Value = "8" }, new SelectListItem { Text = "September", Value = "9" }, new SelectListItem { Text = "October", Value = "10" }, new SelectListItem { Text = "November", Value = "11" }, new SelectListItem { Text = "December", Value = "12" } }, "Value", "Text", DateTime.Now.Month);%><%= Html.DropDownList(pagename + "_Month", months, new { @id = pagename + "_Month", @class = "oe" })%></div> </div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindPatientMonthlySchedule();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportPatientMonthlySchedule", new { patientId = Guid.Empty, month = DateTime.Now.Month }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>_ReportGrid" class="ReportGrid">
        <% =Html.Telerik().Grid<ScheduleEvent>().Name(pagename + "Grid")        
             .Columns(columns =>
                     {
                     columns.Bound(s => s.DisciplineTaskName).Title("Task");
                     columns.Bound(p => p.StatusName).Title("Status");
                     columns.Bound(p => p.EventDate).Title("Schedule Date").Width(100);
                     columns.Bound(p => p.VisitDate).Title("Visit Date").Width(80);
                     columns.Bound(s => s.UserName).Title("Employee");
                     }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename, "Report", new { patientId = Guid.Empty, month = DateTime.Now.Month }))
                       .Sortable().Selectable().Scrollable() .Footer(false)%>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $("#<%= pagename %>_ReportGrid").css({ 'top': '190px' });
    $('#<%= pagename %>_BranchCode').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); });
    $('#<%= pagename %>_Status').change(function() { Report.loadPatientsDropDown('<%= pagename %>'); }); 
</script>
