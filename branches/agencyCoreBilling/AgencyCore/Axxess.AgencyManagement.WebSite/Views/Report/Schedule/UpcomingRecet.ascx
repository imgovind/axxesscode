﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<% string pagename = "ScheduleUpcomingRecet"; %>
<div class="wrapper main">
    <fieldset>
        <legend> Upcoming Recet.</legend>
        <div class="column">
            <div class="row"><label class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label class="float_left">Insurance:</label><div class="float_right"><%= Html.Insurances("InsuranceId", Model, new { @id = pagename + "_InsuranceId", @class = "Insurances" })%></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindScheduleUpcomingRecet();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleUpcomingRecet", new { BranchId = Guid.Empty, InsuranceId = Model }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>Result" class="ReportGrid">
         <%= Html.Telerik().Grid<RecertEvent>().Name(pagename + "Grid").Columns(columns =>
{
    columns.Bound(r => r.PatientName).Sortable(true);
    columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(true).Width(120);
    columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true);
    columns.Bound(r => r.Status).Title("Status").Sortable(true);
    columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(120).Sortable(true);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("ScheduleRecertsUpcoming", "Report", new { BranchId = Guid.Empty, InsuranceId = Model })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Footer(false)
    %>
    </div>
</div>
<script type="text/javascript"> $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>

