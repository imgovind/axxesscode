﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "SchedulePastDueVisitsByDiscipline"; %>
<div class="wrapper main">
    <fieldset>
        <legend> Past Due Visits By Discipline</legend>
             <div class="column">
                <div class="row"><label class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
                <div class="row"><label class="float_left">Discipline:</label><div class="float_right"><%  var discipline = new SelectList(new[]{new SelectListItem { Text = "Nursing", Value = "Nursing" },new SelectListItem { Text = "Physical Therapy", Value = "PT" },new SelectListItem { Text = "Speech Therapy", Value = "ST" },new SelectListItem { Text = "Occupational Therapy", Value = "OT" },new SelectListItem { Text = "Social Worker", Value = "MSW" },new SelectListItem { Text = "Home Health Aide", Value = "HHA"},new SelectListItem { Text = "Orders", Value = "Orders" }}, "Value", "Text", "Nursing");%><%= Html.DropDownList(pagename + "_Discipline", discipline, new { @id = pagename + "_Discipline" })%></div></div>
                <div class="row"><label class="float_left">Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="<%= pagename %>_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="<%= pagename %>_EndDate" class="shortdate" /></div></div>
            </div>
            <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindSchedulePastDueVisitsByDiscipline();">Generate Report</a></li></ul></div>
            <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportSchedulePastDueVisitsByDiscipline", new { BranchId = Guid.Empty, discipline = "Nursing", StartDate = DateTime.Now.AddDays(-60), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
        </fieldset>
    <div id="<%= pagename %>Result" class="ReportGrid">
        <% =Html.Telerik().Grid<ScheduleEvent>().Name(pagename + "Grid")        
             .Columns(columns =>
         {
         columns.Bound(m => m.PatientIdNumber).Title("ID").Width(70);
         columns.Bound(m => m.PatientName).Title("Patient Name");
         columns.Bound(m => m.EventDate).Title("Schedule Date").Format("{0:MM/dd/yyyy}").Width(100);
         columns.Bound(m => m.DisciplineTaskName).Title("Task");
         columns.Bound(p => p.UserName).Title("User Name").Width(155);
       }).DataBinding(dataBinding => dataBinding.Ajax().Select(pagename + "Result", "Report", new { BranchId = Guid.Empty, discipline = "Nursing", StartDate = DateTime.Now.AddDays(-60), EndDate = DateTime.Now }))
       .Sortable().Selectable().Scrollable().Footer(false)
      
        %>
    </div>
</div>
<script type="text/javascript">
    $('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' });
    $("#<%= pagename %>Result").css({ 'top': '180px' });
 </script>
