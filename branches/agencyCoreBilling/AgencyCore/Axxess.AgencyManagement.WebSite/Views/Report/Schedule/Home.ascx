﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul>
    <li class="widget">
        <div class="widget-head"><h5>Schedule Reports</h5></div>
        <div class="widget-content" style="height: 100%;">
            <div class="report_links">
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Schedule/PatientWeekly" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Patient Weekly Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/EmployeeWeekly" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Employee Weekly Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/DailySchedule" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Daily Work Schedule</span></a></li> 
                </ul>
                <ul class="float_left half"> 
                    <li class="link"><a href="/Report/Schedule/MonthlySchedule" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Monthly Work Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/PastDueVisits" onclick="Report.Show(3, '#schedule_reports', $(this).attr('href')); return false;"><span class="title">Past Due Visits</span></a></li> 
                </ul>
                <div class="clear">&#160;</div>
            </div>
        </div>
    </li>
</ul>