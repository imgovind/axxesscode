﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% string pagename = "ScheduleDeviation"; %>
<div class="wrapper main">
    <fieldset>
         <legend>Schedule Deviation</legend>
         <div class="column">
            <div class="row"><label  class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, pagename + "_BranchCode", "", new { @id = pagename + "_BranchCode", @class = "AddressBranchCode report_input valid" })%></div> </div>
            <div class="row"><label  class="float_left">Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="ScheduleDeviation_StartDate" class="shortdate" /><label > To </label><input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="ScheduleDeviation_EndDate" class="shortdate" /></div></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Report.RebindScheduleDeviation();">Generate Report</a></li></ul></div>
        <div class="buttons"><ul><li><%= Html.ActionLink("Export to Excel", "ExportScheduleDeviation", "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = pagename + "_ExportLink" })%></li></ul></div>
    </fieldset>
    <div id="<%= pagename %>Result" class="ReportGrid">
        <% =Html.Telerik().Grid<ScheduleEvent>().Name(pagename + "Grid")        
             .Columns(columns =>
             {
             columns.Bound(m => m.DisciplineTaskName).Title("Task");
             columns.Bound(m => m.PatientName).Title("Patient Name");
             columns.Bound(m => m.PatientIdNumber).Title("MR#").Width(70);
             columns.Bound(m => m.StatusName).Title("Status");
             columns.Bound(p => p.UserName).Title("Employee").Width(155);
             columns.Bound(m => m.EventDate).Title("Schedule Date").Format("{0:MM/dd/yyyy}").Width(90);
             columns.Bound(m => m.VisitDate).Title("Visit Date").Format("{0:MM/dd/yyyy}").Width(80);
           }).DataBinding(dataBinding => dataBinding.Ajax().Select("ScheduleDeviation", "Report", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
           .Sortable().Selectable().Scrollable().Footer(false) %>
    </div>
</div>
<script type="text/javascript">$('#<%= pagename %>Grid .t-grid-content').css({ 'height': 'auto' }); </script>