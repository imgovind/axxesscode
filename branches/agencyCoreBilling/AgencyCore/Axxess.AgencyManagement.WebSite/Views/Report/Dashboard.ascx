﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul class="widgets">
    <li class="widget" style="height:350px;" >
        <div class="widget-head"><h5>Patient Reports</h5></div>
        <div class="widget-content" style="height:335px;">
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Patient/Roster" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient Roster</span></a></li> 
                    <li class="link"><a href="/Report/Patient/Cahps" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">CAHPS Report</span></a></li> 
                    <li class="link"><a href="/Report/Patient/EmergencyList" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Emergency Contact Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/Birthdays" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient Birthday Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/AddressList" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient Address Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/Physician" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient By Physician Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/SocCertPeriod" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient SOC Cert Period Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/ResponsibleEmployee" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient By Responsible Employee Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/ResponsibleCaseManager" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient By Responsible CaseManager Listing</span></a></li> 
                    <li class="link"><a href="/Report/Patient/ExpiringAuthorizations" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Expiring Authorizations</span></a></li> 
                    <li class="link"><a href="/Report/Patient/SurveyCensus" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Survey Census</span></a></li>
                    <li class="link"><a href="/Report/Patient/VitalSigns" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient Vital Sign Report</span></a></li>
                    <li class="link"><a href="/Report/Patient/SixtyDaySummary" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">60 Day Summary By Patient</span></a></li>
                    <li class="link"><a href="/Report/Patient/DischargePatients" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Discharge Patients</span></a></li>
                </ul>
            </div>
        </div>
    </li>
    <li class="widget">
        <div class="widget-head"><h5>Billing / Financial Reports</h5></div>
        <div class="widget-content" >
            <div class="report_links">
                <ul> 
                     <li class="link"><a href="/Report/Billing/OutstandingClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Outstanding Claims</span></a></li> 
                     <li class="link"><a href="/Report/Billing/SubmittedClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Submitted Claims</span></a></li> 
                     <li class="link"><a href="/Report/Billing/ByStatusSummary" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Claims History By Status</span></a></li> 
                     <li class="link"><a href="/Report/Billing/AccountsReceivable" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Accounts Receivable Report</span></a></li> 
                     <li class="link"><a href="/Report/Billing/AgedAccountsReceivable" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Aged Accounts Receivable Report</span></a></li> 
                     <li class="link"><a href="/Report/Billing/PPSRAPClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">PPS RAP Claims Needed</span></a></li> 
                     <li class="link"><a href="/Report/Billing/PPSFinalClaims" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">PPS Final Claims Needed</span></a></li> 
                     <li class="link"><a href="/Report/Billing/PotentialClaimAutoCancel" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Potential Claim Auto Cancel</span></a></li> 
                     <li class="link"><a href="/Report/Billing/BillingBatch" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Billing Batch Report</span></a></li> 
                </ul>
            </div>
        </div>
    </li>
</ul>
<ul class="widgets">
    <li class="widget">
        <div class="widget-head"><h5>Clinical Reports</h5></div>
        <div class="widget-content" >
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Clinical/OpenOasis" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Open OASIS</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/MissedVisits" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Missed Visit Report</span></a></li> 
                    <li class="link"><a href="javascript:void(0);" onclick="Acore.Open('orderstobesent');"><span class="title">Orders To Be Sent</span></a></li> 
                    <li class="link"><a href="javascript:void(0);" onclick="Acore.Open('orderspendingsignature');"><span class="title">Orders Pending Signature</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/PhysicianOrderHistory" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Physician Order History</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/PlanOfCareHistory" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Plan Of Care History</span></a></li>  
                    <li class="link"><a href="/Report/Clinical/ThirteenAndNineteenVisitException" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">13th And 19th Therapy Visit Exception</span></a></li> 
                    <li class="link"><a href="/Report/Clinical/ThirteenTherapyReevaluationException" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">13th Therapy Re-evaluation Exception Reports</span></a></li>   
                    <li class="link"><a href="/Report/Clinical/NineteenTherapyReevaluationException" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">19th Therapy Re-evaluation Exception Reports</span></a></li>   
                </ul>
            </div>
        </div>
    </li>
    <li class="widget">
        <div class="widget-head"><h5>Employee Reports</h5></div>
        <div class="widget-content" >
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Employee/Roster" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Employee Roster</span></a></li> 
                    <li class="link"><a href="/Report/Employee/License" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Employee License Listing</span></a></li> 
                    <li class="link"><a href="/Report/Employee/ExpiringLicense" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Expiring Licenses</span></a></li> 
                    <li class="link"><a href="/Report/Employee/VisitByDateRange" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Employee Visit By Date Range</span></a></li> 
                </ul>
            </div>
        </div>
    </li>
</ul>
<ul class="widgets">
    <li class="widget" style="height:310px;" >
        <div class="widget-head"><h5>Schedule Reports</h5></div>
        <div class="widget-content" style="height:305px;">
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Schedule/PatientWeekly" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient Weekly Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/PatientMonthlySchedule" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient Monthly Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/EmployeeWeekly" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Employee Weekly Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/DailySchedule" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Employee Daily Work Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/MonthlySchedule" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Employee Monthly Work Schedule</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/PastDueVisits" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Past Due Visits</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/PastDueVisitsByDiscipline" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Past Due Visits By Discipline</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/CaseManagerTask" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Case Manager Task List</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/Deviation" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Schedule Deviation</span></a></li> 
                    
                    <li class="link"><a href="/Report/Schedule/PastDueRecet" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Past Due Recertification</span></a></li> 
                    <li class="link"><a href="/Report/Schedule/UpcomingRecet" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Upcoming Recertification</span></a></li> 
                    
                </ul>
            </div>
        </div>
    </li>
    <li class="widget">
        <div class="widget-head"><h5>Statistical Reports</h5></div>
        <div class="widget-content" >
            <div class="report_links">
                <ul> 
                    <li class="link"><a href="/Report/Statistical/PatientVisits" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Patient Visit History</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/EmployeeVisits" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Employee Visit History</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/CensusByPrimaryInsurance" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Census By Primary Insurance</span></a></li> 
                    <li class="link"><a href="/Report/Statistical/MonthlyAdmission" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Monthly Admission Report</span></a></li>
                    <li class="link"><a href="/Report/Statistical/AnnualAdmission" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Annual Admission Report</span></a></li>
                    <li class="link"><a href="/Report/Statistical/UnduplicatedCensusReport" onclick="Report.Show(0, '#all_reports', $(this).attr('href')); return false;"><span class="title">Unduplicated Census Report</span></a></li>
                </ul>
            </div>
        </div>
    </li>
</ul>
<script type="text/javascript">
    $('#ClaimsByStatusGrid .t-grid-content').css({ 'height': 'auto' });
    $('#OutstandingClaimsGrid .t-grid-content').css({ 'height': 'auto' });
    
    $('#ClinicalMissedVisitGrid .t-grid-content').css({ 'height': 'auto' });
    $("#ClinicalOpenOasisGrid .t-grid-content").css({ 'height': 'auto' });
    $('#ClinicalPhysicianOrderHistoryGrid .t-grid-content').css({ 'height': 'auto' });
    $('#ClinicalPlanOfCareHistoryGrid .t-grid-content').css({ 'height': 'auto' });

    $('#EmployeeBirthdayListGrid .t-grid-content').css({ 'height': 'auto' });
    $('#EmployeeLicenseListingGrid .t-grid-content').css({ 'height': 'auto' });
    $('#EmployeeLicenseListing_BranchCode').change(function() { Report.loadUsersDropDown('EmployeeLicenseListing'); });
    $('#EmployeeLicenseListing_Status').change(function() { Report.loadUsersDropDown('EmployeeLicenseListing'); });
    $('#EmployeeRosterGrid .t-grid-content').css({ 'height': 'auto' });

    $('#PatientRosterGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientEmergencyListGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientBirthdayListGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientAddressListGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientByPhysiciansGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientSocCertPeriodListingGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientByResponsibleEmployeeListingGrid .t-grid-content').css({ 'height': 'auto' });
    $('#PatientByResponsibleEmployeeListing_BranchCode').change(function() { Report.loadUsersDropDown('PatientByResponsibleEmployeeListing'); });
    $('#PatientByResponsibleEmployeeListing_Status').change(function() { Report.loadUsersDropDown('PatientByResponsibleEmployeeListing'); });

    $('#SchedulePatientWeeklyScheduleGrid .t-grid-content').css({ 'height': 'auto' });
    $('#SchedulePatientWeeklySchedule_BranchCode').change(function() { Report.loadPatientsDropDown('SchedulePatientWeeklySchedule'); });
    $('#SchedulePatientWeeklySchedule_Status').change(function() { Report.loadPatientsDropDown('SchedulePatientWeeklySchedule'); });
    $('#ScheduleEmployeeWeeklyGrid .t-grid-content').css({ 'height': 'auto' });
    $('#ScheduleEmployeeWeekly_BranchCode').change(function() { Report.loadUsersDropDown('ScheduleEmployeeWeekly'); });
    $('#ScheduleEmployeeWeekly_Status').change(function() { Report.loadUsersDropDown('ScheduleEmployeeWeekly'); });
    $('#ScheduleDailyWorkGrid .t-grid-content').css({ 'height': 'auto' });
    $('#ScheduleMonthlyWorkGrid .t-grid-content').css({ 'height': 'auto' });
    $('#ScheduleMonthlyWork_BranchCode').change(function() { Report.loadUsersDropDown('ScheduleMonthlyWork'); });
    $('#ScheduleMonthlyWork_Status').change(function() { Report.loadUsersDropDown('ScheduleMonthlyWork'); });
    $('#SchedulePastDueVisitsGrid .t-grid-content').css({ 'height': 'auto' });

    $('#StatisticalEmployeeVisitHistoryGrid .t-grid-content').css({ 'height': 'auto' });
    $('#StatisticalEmployeeVisitHistory_BranchCode').change(function() { Report.loadUsersDropDown('StatisticalEmployeeVisitHistory'); });
    $('#StatisticalEmployeeVisitHistory_Status').change(function() { Report.loadUsersDropDown('StatisticalEmployeeVisitHistory'); });
    $('#StatisticalPatientVisitHistoryGrid .t-grid-content').css({ 'height': 'auto' });
    $('#StatisticalPatientVisitHistory_BranchCode').change(function() { Report.loadPatientsDropDown('StatisticalPatientVisitHistory'); });
    $('#StatisticalPatientVisitHistory_Status').change(function() { Report.loadPatientsDropDown('StatisticalPatientVisitHistory'); }); 
    
</script>
