﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "listusers",
        "List Users",
        Current.AgencyName)%>
<div class="fixedGridHeight">
<% var activeAction = string.Empty;
   var inactiveAction = string.Empty;
   var visible = false;
   if (Current.HasRight(Permissions.ManageUsers))
   {
       visible = true;
       activeAction = "<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditUser('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"User.Deactivate('<#=Id#>');\">Deactivate</a> | <a href=\"javascript:void(0);\" onclick=\"User.Delete('<#=Id#>');\">Delete</a>";
       inactiveAction = "<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditUser('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"User.Activate('<#=Id#>');\">Activate</a> | <a href=\"javascript:void(0);\" onclick=\"User.Delete('<#=Id#>');\">Delete</a>";
   }    
 %>
<% using (Html.BeginForm("ActiveUsers", "Export", FormMethod.Post)) { %>
    <%= Html.Telerik().Grid<User>().Name("List_User").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(u => u.DisplayName).Title("Name").Sortable(true);
    columns.Bound(u => u.DisplayTitle).Title("Title").Sortable(true).Width(200);
    columns.Bound(u => u.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(200).Sortable(true);
    columns.Bound(u => u.HomePhone).Title("Phone").Sortable(false).Width(110);
    columns.Bound(u => u.EmploymentType).Sortable(true).Width(120);
    columns.Bound(u => u.StatusName).Title("Status").Sortable(false).Width(80);
    columns.Bound(u => u.Id).Width(180).Sortable(false).ClientTemplate(activeAction).Visible(visible).Title("Action");
}).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "User", new { status = (int)UserStatus.Active })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
<%} %>    
</div><div class="absoluteGrid">
<% using (Html.BeginForm("InactiveUsers", "Export", FormMethod.Post)) { %>
    <%= Html.Telerik().Grid<User>().Name("List_UserInactive").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(u => u.DisplayName).Title("Name").Sortable(true);
    columns.Bound(u => u.TitleType).Title("Title").Sortable(true).Width(200);
    columns.Bound(u => u.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(200).Sortable(true);
    columns.Bound(u => u.HomePhone).Title("Phone").Sortable(false).Width(110);
    columns.Bound(u => u.EmploymentType).Sortable(true).Width(120);
    columns.Bound(u => u.StatusName).Title("Status").Sortable(false).Width(80);
    columns.Bound(u => u.Id).Width(180).Sortable(false).ClientTemplate(inactiveAction).Visible(visible).Title("Action");
}).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "User", new { status = (int)UserStatus.Inactive })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
<%} %>    
</div>
<script type="text/javascript">
    $("#List_User .t-grid-toolbar").html("");
    $("#List_UserInactive .t-grid-toolbar").html("");
    $("#List_User .t-grid-content").css({ 'height': '200px' });
    $("#List_UserInactive .t-grid-content").css({ 'height': 'auto' });
    var newUser = "", activeExport = "", inactiveExport = "";
<% var newUser = string.Empty; %>
<% var activeExport = string.Empty; %>
<% if (Current.HasRight(Permissions.ManageUsers)) { %>
    newUser = "%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_left%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 onclick=%22UserInterface.ShowNewUser(); return false;%22%3ENew User%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E";
<% } %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    activeExport = "%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export (Active)%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E";
<% } %>
$("#List_User .t-grid-toolbar").append(unescape("%3Cdiv class=%22align_center%22%3E" + newUser + "%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3EActive Users%3C/div%3E" + activeExport + "%3C/div%3E"));
<% var inactiveExport = string.Empty; %>
<% if (Current.HasRight(Permissions.ExportListToExcel)) { %>
    inactiveExport = "%3Cdiv class=%22buttons%22%3E%3Cul class=%22float_right%22%3E%3Cli%3E%3Ca href=%22javascript:void(0);%22 class=%22excel%22 onclick=%22$(this).closest('form').submit();%22%3EExcel Export (Inactive)%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E";
<% } %>
$("#List_UserInactive .t-grid-toolbar").append(unescape("%3Cdiv class=%22align_center%22%3E%3Cdiv class=%22abs bigtext%22 style=%22left: 150px; right: 150px;%22%3EInactive Users%3C/div%3E" + inactiveExport + "%3C/div%3E"));
</script>
