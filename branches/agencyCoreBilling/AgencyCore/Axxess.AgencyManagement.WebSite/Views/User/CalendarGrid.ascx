﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DateTime>" %>
<% Html.Telerik().Grid<UserVisit>().Name("List_UserCalander_Schedule").Columns(columns =>
   {
       columns.Bound(v => v.PatientName);
       columns.Bound(v => v.TaskName).ClientTemplate("<#=Url#>").Title("Task").Width(250);
       columns.Bound(v => v.VisitDate).Title("Date").Width(100);
       columns.Bound(v => v.StatusName).Width(200).Title("Status");
       columns.Bound(v => v.StatusComment).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip red_note\" href=\"javascript:void(0);\" tooltip=\"<#=StatusComment#>\"> </a>");
       columns.Bound(v => v.VisitNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=VisitNotes#>\"></a>");
       columns.Bound(v => v.EpisodeNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip blue_note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>");
       columns.Bound(v => v.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowMissedVisitModal('<#=EpisodeId#>','<#=PatientId#>','<#=Id#>');\">Missed Visit Form</a>").Title(" ").Width(150);
   }).DataBinding(dataBinding => dataBinding.Ajax().Select("UserVisits", "User", new { month = Model.Month, year = Model.Year }))
    .ClientEvents(c => c.OnRowDataBound("Schedule.tooltip"))
    .Footer(false).Sortable().Render(); %>
<script type="text/javascript">
    $("#List_UserCalander_Schedule").css({ 'height': 'auto', 'position': 'relative'})
    $("#List_UserCalander_Schedule .t-grid-content").css({ 'height': 'auto', 'position': 'relative', 'top': '0px', 'bottom': '0px' });
</script>