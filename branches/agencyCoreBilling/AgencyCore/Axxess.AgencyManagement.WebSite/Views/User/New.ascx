﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<bool>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "newuser",
        "New User",
        Current.AgencyName)%>
<div id="newUserContent" class="wrapper main">
<% using (Html.BeginForm("Add", "User", FormMethod.Post, new { @id = "newUserForm" })) { %>
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row"><label for="New_User_EmailAddress" class="float_left">E-mail Address:</label><div class="float_right"> <%= Html.TextBox("EmailAddress", "", new { @id = "New_User_EmailAddress", @class = "text required input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_User_FirstName" class="float_left">First Name:</label><div class="float_right"> <%= Html.TextBox("FirstName", "", new { @id = "New_User_FirstName", @class = "text required names input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_User_LastName" class="float_left">Last Name:</label><div class="float_right"> <%= Html.TextBox("LastName", "", new { @id = "New_User_LastName", @class = "text required names input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_User_Credentials" class="float_left">Credentials:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.CredentialTypes, "Credentials", "", new { @id = "New_User_Credentials", @class = "required" })%></div></div>
            <div class="row"><label for="New_User_OtherCredentials" class="float_left">Other Credentials (specify):</label><div class="float_right"><%= Html.TextBox("CredentialsOther", "", new { @id = "New_User_OtherCredentials", @class = "text input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_User_TitleType" class="float_left">Title:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.TitleTypes, "TitleType", "", new { @id = "New_User_TitleType", @class = "TitleType required" })%></div></div>
            <div class="row"><label for="New_User_OtherTitleType" class="float_left">Other Title (specify):</label><div class="float_right"><%= Html.TextBox("TitleTypeOther", "", new { @id = "New_User_OtherTitleType", @class = "text input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label class="float_left">Employment Type:</label><div class="float_right"><%= Html.RadioButton("EmploymentType", "Employee", new { @id = "New_User_EmploymentType_E", @class = "required radio" })%><label for="New_User_EmploymentType_E" class="inlineradio">Employee</label><%= Html.RadioButton("EmploymentType", "Contractor", new { @id = "New_User_EmploymentType_C", @class = "required radio" })%><label for="New_User_EmploymentType_C" class="inlineradio">Contractor</label></div></div>
        </div><div class="column">
            <div class="row"><label for="New_User_CustomId" class="float_left">Agency Custom Employee Id:</label><div class="float_right"> <%= Html.TextBox("CustomId", "", new { @id = "New_User_CustomId", @class = "text input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_User_LocationId" class="float_left">Agency Branch:</label><div class="float_right"> <%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "New_User_LocationId", @class = "BranchLocation" })%></div></div>
            <div class="row"><label for="New_User_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%= Html.TextBox("Profile.AddressLine1", "", new { @id = "New_User_AddressLine1", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_User_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%= Html.TextBox("Profile.AddressLine2", "", new { @id = "New_User_AddressLine2", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_User_AddressCity" class="float_left">City:</label><div class="float_right"><%= Html.TextBox("Profile.AddressCity", "", new { @id = "New_User_AddressCity", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_User_AddressStateCode" class="float_left">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", "", new { @id = "New_User_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("Profile.AddressZipCode", "", new { @id = "New_User_AddressZipCode", @class = "text numeric input_wrapper zip", @size = "5", @maxlength = "5" })%></div></div>
            <div class="row"><label for="New_User_HomePhoneArray1" class="float_left">Home Phone:</label><div class="float_right"><%= Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("HomePhoneArray", "", new { @id = "New_User_HomePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="New_User_MobilePhoneArray1" class="float_left">Mobile Phone:</label><div class="float_right"><%= Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("MobilePhoneArray", "", new { @id = "New_User_MobilePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Roles</legend>
        <table class="form">
            <tbody>
                <tr class="firstrow">
                    <td><input id="New_User_Role_1" type="checkbox" class="radio required" name="AgencyRoleList" value="1" />&#160;<label for="New_User_Role_1">Administrator</label></td>
                    <td><input id="New_User_Role_2" type="checkbox" class="radio required" name="AgencyRoleList" value="2" />&#160;<label for="New_User_Role_2">Director of Nursing</label></td>
                    <td><input id="New_User_Role_3" type="checkbox" class="radio required" name="AgencyRoleList" value="3" />&#160;<label for="New_User_Role_3">Case Manager</label></td>
                    <td><input id="New_User_Role_4" type="checkbox" class="radio required" name="AgencyRoleList" value="4" />&#160;<label for="New_User_Role_4">Nursing</label></td>
                    <td><input id="New_User_Role_5" type="checkbox" class="radio required" name="AgencyRoleList" value="5" />&#160;<label for="New_User_Role_5">Clerk (non-clinical)</label></td>
                </tr><tr>
                    <td><input id="New_User_Role_6" type="checkbox" class="radio required" name="AgencyRoleList" value="6" />&#160;<label for="New_User_Role_6">Physical Therapist</label></td>
                    <td><input id="New_User_Role_7" type="checkbox" class="radio required" name="AgencyRoleList" value="7" />&#160;<label for="New_User_Role_7">Occupational Therapist</label></td>
                    <td><input id="New_User_Role_8" type="checkbox" class="radio required" name="AgencyRoleList" value="8" />&#160;<label for="New_User_Role_8">Speech Therapist</label></td>
                    <td><input id="New_User_Role_9" type="checkbox" class="radio required" name="AgencyRoleList" value="9" />&#160;<label for="New_User_Role_9">Medical Social Worker</label></td>
                    <td><input id="New_User_Role_10" type="checkbox" class="radio required" name="AgencyRoleList" value="10" />&#160;<label for="New_User_Role_10">Home Health Aide</label></td>
                </tr><tr>
                    <td><input id="New_User_Role_11" type="checkbox" class="radio required" name="AgencyRoleList" value="11" />&#160;<label for="New_User_Role_11">Scheduler</label></td>
                    <td><input id="New_User_Role_12" type="checkbox" class="radio required" name="AgencyRoleList" value="12" />&#160;<label for="New_User_Role_12">Biller</label></td>
                    <td><input id="New_User_Role_13" type="checkbox" class="radio required" name="AgencyRoleList" value="13" />&#160;<label for="New_User_Role_13">Quality Assurance</label></td>
                    <td><input id="New_User_Role_14" type="checkbox" class="radio required" name="AgencyRoleList" value="14" />&#160;<label for="New_User_Role_14">Physician</label></td>
                    <td><input id="New_User_Role_15" type="checkbox" class="radio required" name="AgencyRoleList" value="15" />&#160;<label for="New_User_Role_15">Office Manager</label></td>
                </tr><tr>
                    <td><input id="New_User_Role_16" type="checkbox" class="radio required" name="AgencyRoleList" value="16" />&#160;<label for="New_User_Role_16">Community Liason Officer/Marketer</label></td>
                    <td><input id="New_User_Role_17" type="checkbox" class="radio required" name="AgencyRoleList" value="17" />&#160;<label for="New_User_Role_17">External Referral Source</label></td>
                    <td colspan="3"><input id="New_User_Role_18" type="checkbox" class="radio required" name="AgencyRoleList" value="18" />&#160;<label for="New_User_Role_18">Driver/Transportation</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Permissions</legend>
        <div class="wide_column"><input id="New_User_AllPermissions" type="checkbox" class="radio" value="" />&#160;<label for="New_User_AllPermissions">Select all Permissions</label></div>
        <div class="column">
             <%= Html.PermissionList("Clerical", new List<string>())%>
             <%= Html.PermissionList("Clinical", new List<string>())%>
             <%= Html.PermissionList("OASIS", new List<string>())%>
             <%= Html.PermissionList("Reporting", new List<string>())%>
         </div><div class="column">
             <%= Html.PermissionList("QA", new List<string>())%>
             <%= Html.PermissionList("Schedule Management", new List<string>())%>
             <%= Html.PermissionList("Billing", new List<string>())%>
             <%= Html.PermissionList("Administration", new List<string>())%>
         </div>
    </fieldset>
    <fieldset>
        <legend>Access & Restrictions</legend>
        <div class="column">
            <div class="row"><label for="New_User_AllowWeekendAccess" class="float_left">Allow Weekend Access?</label><div class="float_right"><%= Html.CheckBox("AllowWeekendAccess", true, new { @id="New_User_AllowWeekendAccess" })%></div></div>
            <div class="row"><label for="New_User_EarliestLoginTime" class="float_left">Earliest Login Time:</label><div class="float_right"><input type="text" size="10" id="New_User_EarliestLoginTime" name="EarliestLoginTime" class="spinners" /></div></div>
            <div class="row"><label for="New_User_AutomaticLogoutTime" class="float_left">Automatic Logout Time:</label><div class="float_right"><input type="text" size="10" id="New_User_AutomaticLogoutTime" name="AutomaticLogoutTime" class="spinners" /></div></div>
        </div>
    </fieldset>
     <fieldset>
        <legend>Comments</legend>
        <div class="wide_column">
            <div class="row">
                <p class="charsRemaining"></p>
                <textarea id="New_User_Comments" name="Comments" cols="5" rows="6" maxcharacters="500" style="height:100px;"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add User</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newuser');">Cancel</a></li>
    </ul></div>
<%} %>
</div>

<script type="text/javascript">
    <%  if (Model) { %>
    $("#newUserContent").html(U.Error("You have reached your maximum number of user accounts", "Please contact Axxess about upgrading your account."));
    <%  } else { %>
    $("#New_User_EarliestLoginTime,#New_User_AutomaticLogoutTime").TimePicker();
    $(".row .required,.row .requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
    <%  } %>
</script>
