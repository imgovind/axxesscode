﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="buttons">
    <ul class="float_right">
        <li><a href="javascript:void(0);" onclick="User.RebindScheduleList();">Refresh</a></li>
        <li><a href="Export/ScheduleList" >Excel Export</a></li>
    </ul>
    <ul>
        <li><a href="javascript:void(0);" onclick="User.LoadUserSchedule('PatientName');">Group By Patient</a></li>
        <li><a href="javascript:void(0);" onclick="User.LoadUserSchedule('VisitDate');">Group By Date</a></li>
        <li><a href="javascript:void(0);" onclick="User.LoadUserSchedule('TaskName');">Group By Task</a></li>
    </ul>
</div>
<div class="clear"></div>
<div class="buttons">Note: This list shows you items/tasks dated 3 months into the past and 2 weeks into the future. To find older items, look in the Patient's Chart or Schedule Center.</div>
<div id="myScheduledTasksContentId"><% Html.RenderPartial("~/Views/User/ScheduleGrid.ascx"); %></div>