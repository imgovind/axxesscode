﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="main">
    <% using (Html.BeginForm("AddLicense", "User", FormMethod.Post, new { @id = "editUserLicenseForm" })) { %>
    <%= Html.Hidden("UserId", Model, new { @id = "Edit_UserLicense_Id" }) %>
        <fieldset>
            <legend></legend>
            <table class="form">
                <tbody>
                    <tr>
                        <td>License Type</td>
                        <td>Issue Date</td>
                        <td>Expiration Date</td>
                        <td colspan="2">Attachment</td>
                    </tr>
                    <tr>
                        <td>
                            <%= Html.LookupSelectList(SelectListTypes.LicenseTypes, "LicenseType", "", new { @id = "Edit_UserLicense_Type", @class = "valid fill" })%>
                            <%= Html.TextBox("OtherLicenseType", "", new { @id = "Edit_UserLicense_TypeOther", @class = "valid fill", @maxlength = "25" }) %>
                        </td>
                        <td><input type="date" name="InitiationDate" id="Edit_UserLicense_InitDate" /></td>
                        <td><input type="date" name="ExpirationDate" id="Edit_UserLicense_ExpDate" /></td>
                        <td><input type="file" name="Attachment" id="Edit_UserLicense_Attachment" /></td>
                        <td>
                            <div class="buttons">
                                <ul>
                                    <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add License</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
    <% } %>
    <div class="currentlicenses">
        <%= Html.Telerik().Grid<License>().Name("List_User_Licenses").DataKeys(keys => { keys.Add(M => M.Id).RouteKey("Id");  }).Columns(columns =>
{
            columns.Bound(l => l.LicenseType).ReadOnly().Sortable(false);
            columns.Bound(l => l.InitiationDateFormatted).ReadOnly().Title("Issue Date").Sortable(false);
            columns.Bound(l => l.ExpirationDate).Title("Expiration Date").Sortable(false);
            columns.Bound(l => l.AssetUrl).Title("Attachment").ReadOnly().Sortable(false);
            columns.Command(commands =>
            {
                commands.Edit();
                commands.Delete();
            }).Width(135).Title("Action");
            }).DataBinding(dataBinding => dataBinding.Ajax()
                .Select("LicenseList", "User", new { userId = Model })
                .Update("UpdateLicense", "User", new { userId = Model })
                .Delete("DeleteLicense", "User", new { userId = Model }))
            .Sortable().Footer(false).Scrollable(scrolling => scrolling.Enabled(true))
        %>
    </div>
</div>
<script type="text/javascript">
    U.showIfSelectEquals(
        $("#Edit_UserLicense_Type"), "Other",
        $("#Edit_UserLicense_TypeOther"));
</script>