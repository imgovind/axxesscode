﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<User>" %>
<% using (Html.BeginForm("Update", "User", FormMethod.Post, new { @id = "editUserForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_User_Id" }) %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>User Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_User_FirstName" class="float_left">First Name:</label><div class="float_right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_User_FirstName", @maxlength = "75", @class = "required" })%></div></div>
            <div class="row"><label for="Edit_User_LastName" class="float_left">Last Name:</label><div class="float_right"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_User_LastName", @maxlength = "75", @class = "required" })%></div></div>
            <div class="row"><label for="Edit_User_Suffix" class="float_left">Suffix:</label><div class="float_right"><%= Html.TextBox("Suffix", Model.Suffix, new { @id = "Edit_User_Suffix", @maxlength = "50", @class = "" })%></div></div>
            <div class="row"><label for="Edit_User_Credentials" class="float_left">Credentials:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.CredentialTypes, "Credentials", Model.Credentials, new { @id = "Edit_User_Credentials", @class = "required" })%></div></div>
            <div class="row"><label for="Edit_User_OtherCredentials" class="float_left">Other Credentials (specify):</label><div class="float_right"><%= Html.TextBox("CredentialsOther", Model.CredentialsOther, new { @id = "Edit_User_OtherCredentials", @class = "text input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="Edit_User_TitleType" class="float_left">Title:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.TitleTypes, "TitleType", Model.TitleType, new { @id = "Edit_User_TitleType", @class = "TitleType required" })%></div></div>
            <div class="row"><label for="Edit_User_OtherTitleType" class="float_left">Other Title (specify):</label><div class="float_right"><%= Html.TextBox("TitleTypeOther", Model.TitleTypeOther, new { @id = "Edit_User_OtherTitleType", @class = "text input_wrapper", @maxlength = "20" })%></div></div>
           <div class="row"><label class="float_left">Employment Type:</label><div class="float_right"><%= Html.RadioButton("EmploymentType", "Employee", new { @id = "Edit_User_EmploymentType_E", @class = "required radio" })%><label for="Edit_User_EmploymentType_E" class="inlineradio">Employee</label><%= Html.RadioButton("EmploymentType", "Contractor", new { @id = "Edit_User_EmploymentType_C", @class = "required radio" })%><label for="Edit_User_EmploymentType_C" class="inlineradio">Contractor</label></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_User_CustomId" class="float_left">Agency Custom Employee Id:</label><div class="float_right"> <%= Html.TextBox("CustomId", Model.CustomId, new { @id = "Edit_User_CustomId", @class = "text input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="Edit_User_LocationId" class="float_left">Agency Branch:</label><div class="float_right"> <%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.AgencyLocationId.ToString(), new { @id = "Edit_User_LocationId", @class = "BranchLocation" })%></div></div>
            <div class="row"><label for="Edit_User_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%= Html.TextBox("Profile.AddressLine1", Model.Profile.AddressLine1, new { @id = "Edit_User_AddressLine1", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_User_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%= Html.TextBox("Profile.AddressLine2", Model.Profile.AddressLine2, new { @id = "Edit_User_AddressLine2", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_User_AddressCity" class="float_left">City:</label><div class="float_right"><%= Html.TextBox("Profile.AddressCity", Model.Profile.AddressCity, new { @id = "Edit_User_AddressCity", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_User_AddressStateCode" class="float_left">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "Profile.AddressStateCode", Model.Profile.AddressStateCode, new { @id = "Edit_User_AddressStateCode", @class = "AddressStateCode valid" })%><%= Html.TextBox("Profile.AddressZipCode", Model.Profile.AddressZipCode, new { @id = "Edit_User_AddressZipCode", @class = "text numeric input_wrapper zip", @size = "5", @maxlength = "5" })%></div></div>
            <div class="row"><label for="Edit_User_HomePhoneArray1" class="float_left">Home Phone:</label><div class="float_right"><%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length >= 3 ? Model.Profile.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_User_HomePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length >= 6 ? Model.Profile.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_User_HomePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("HomePhoneArray", Model.Profile.PhoneHome.IsNotNullOrEmpty() && Model.Profile.PhoneHome.Length >= 10 ? Model.Profile.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_User_HomePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_User_MobilePhoneArray1" class="float_left">Mobile Phone:</label><div class="float_right"><%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() && Model.Profile.PhoneMobile.Length >= 3 ? Model.Profile.PhoneMobile.Substring(0, 3) : "", new { @id = "Edit_User_MobilePhoneArray1", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "4" })%>&#160;-&#160;<%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() && Model.Profile.PhoneMobile.Length >= 6 ? Model.Profile.PhoneMobile.Substring(3, 3) : "", new { @id = "Edit_User_MobilePhoneArray2", @class = "input_wrappermultible autotext digits phone_short", @maxlength = "3", @size = "3" })%>&#160;-&#160;<%= Html.TextBox("MobilePhoneArray", Model.Profile.PhoneMobile.IsNotNullOrEmpty() && Model.Profile.PhoneMobile.Length >= 10 ? Model.Profile.PhoneMobile.Substring(6, 4) : "", new { @id = "Edit_User_MobilePhoneArray3", @class = "input_wrappermultible autotext digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Roles</legend>
        <table class="form">
            <tbody><%string[] roles = Model.Roles.IsNotNullOrEmpty() ? Model.Roles.Split(';') : null;  %>
                <tr class="firstrow">
                    <td><%= string.Format("<input id='Edit_User_Role_1' type='checkbox' value='1' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("1") ? "checked='checked'" : "")%><label for="Edit_User_Role_1">Administrator</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_2' type='checkbox' value='2' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("2") ? "checked='checked'" : "")%><label for="Edit_User_Role_2">Director of Nursing</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_3' type='checkbox' value='3' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("3") ? "checked='checked'" : "")%><label for="Edit_User_Role_3">Case Manager</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_4' type='checkbox' value='4' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("4") ? "checked='checked'" : "")%><label for="Edit_User_Role_4">Nursing</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_5' type='checkbox' value='5' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("5") ? "checked='checked'" : "")%><label for="Edit_User_Role_5">Clerk (non-clinical)</label></td>
                </tr><tr>
                    <td><%= string.Format("<input id='Edit_User_Role_6' type='checkbox' value='6' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("6") ? "checked='checked'" : "")%><label for="Edit_User_Role_6">Physical Therapist</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_7' type='checkbox' value='7' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("7") ? "checked='checked'" : "")%><label for="Edit_User_Role_7">Occupational Therapist</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_8' type='checkbox' value='8' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("8") ? "checked='checked'" : "")%><label for="Edit_User_Role_8">Speech Therapist</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_9' type='checkbox' value='9' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("9") ? "checked='checked'" : "")%><label for="Edit_User_Role_9">Medical Social Worker</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_10' type='checkbox' value='10' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("10") ? "checked='checked'" : "")%><label for="Edit_User_Role_10">Home Health Aide</label></td>
                </tr><tr>
                    <td><%= string.Format("<input id='Edit_User_Role_11' type='checkbox' value='11' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("11") ? "checked='checked'" : "")%><label for="Edit_User_Role_11">Scheduler</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_12' type='checkbox' value='12' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("12") ? "checked='checked'" : "")%><label for="Edit_User_Role_12">Biller</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_13' type='checkbox' value='13' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("13") ? "checked='checked'" : "")%><label for="Edit_User_Role_13">Quality Assurance</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_14' type='checkbox' value='14' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("14") ? "checked='checked'" : "")%><label for="Edit_User_Role_14">Physician</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_15' type='checkbox' value='15' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("15") ? "checked='checked'" : "")%><label for="Edit_User_Role_15">Office Manager</label></td>
                </tr><tr>
                    <td><%= string.Format("<input id='Edit_User_Role_16' type='checkbox' value='16' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("16") ? "checked='checked'" : "")%><label for="Edit_User_Role_16">Community Liason Officer/Marketer</label></td>
                    <td><%= string.Format("<input id='Edit_User_Role_17' type='checkbox' value='17' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("17") ? "checked='checked'" : "")%><label for="Edit_User_Role_17">External Referral Source</label></td>
                    <td colspan="3"><%= string.Format("<input id='Edit_User_Role_18' type='checkbox' value='18' name='AgencyRoleList' class='required radio' {0} />&#160;", roles != null && roles.Contains("18") ? "checked='checked'" : "")%><label for="Edit_User_Role_18">Driver/Transportation</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Access & Restrictions</legend>
        <div class="column">
            <div class="row"><label for="Edit_User_AllowWeekendAccess" class="float_left">Allow Weekend Access?</label><div class="float_right"><%= Html.CheckBox("AllowWeekendAccess", Model.AllowWeekendAccess ? true : false, new { @id="Edit_User_AllowWeekendAccess" })%></div></div>
            <div class="row"><label for="Edit_User_EarliestLoginTime" class="float_left">Earliest Login Time:</label><div class="float_right"><input value="<%= Model.EarliestLoginTime %>" type="text" size="10" id="Edit_User_EarliestLoginTime" name="EarliestLoginTime" class="spinners" /></div></div>
            <div class="row"><label for="Edit_User_AutomaticLogoutTime" class="float_left">Automatic Logout Time:</label><div class="float_right"><input  value="<%= Model.AutomaticLogoutTime %>" type="text" size="10" id="Edit_User_AutomaticLogoutTime" name="AutomaticLogoutTime" class="spinners" /></div></div>
        </div>
    </fieldset>
    <fieldset><legend>Comments</legend><div class="wide_column"><div class="row"> <textarea id="Edit_User_Comments" name="Comments" cols="5" rows="30" style="height:100px;"><%= Model.Comments %></textarea></div></div></fieldset>
    <div class="buttons"><ul>
        <li><a id="Edit_User_SaveButton" href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('edituser');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    U.timePicker($("#Edit_User_EarliestLoginTime"));
    U.timePicker($("#Edit_User_AutomaticLogoutTime"));
</script>
<% Html.Telerik().TabStrip().Name("editUserTabStrip").Items(tabstrip => { 
       tabstrip.Add().Text("Licenses").HtmlAttributes(new { id = "editUserLicenses_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
            <% Html.RenderPartial("Licenses", Model.Id); %>
        <% });%>
        <% tabstrip.Add().Text("Permissions").HtmlAttributes(new { id = "editPermissions_Tab" }).ContentHtmlAttributes(new { style = "overflow: auto;" }).Content(() => { %>
        <% using (Html.BeginForm("UpdatePermissions", "User", FormMethod.Post, new { @id = "editUserPermissionsForm" })) { %>
            <%= Html.Hidden("UserId", Model.Id, new { @id = "Edit_UserPermission_UserId" }) %>
                <div class="main">
                    <fieldset>
                        <legend></legend>
                        <div class="wide_column"><input id="Edit_User_AllPermissions" type="checkbox" class="radio" value="" />&#160;<label for="Edit_User_AllPermissions">Select all permissions</label></div>
                        <div class="column">
                             <%= Html.PermissionList("Clerical", Model.PermissionsArray)%>
                             <%= Html.PermissionList("Clinical", Model.PermissionsArray)%>
                             <%= Html.PermissionList("OASIS", Model.PermissionsArray)%>
                             <%= Html.PermissionList("Reporting", Model.PermissionsArray)%>
                         </div><div class="column">
                             <%= Html.PermissionList("QA", Model.PermissionsArray)%>
                             <%= Html.PermissionList("Schedule Management", Model.PermissionsArray)%>
                             <%= Html.PermissionList("Billing", Model.PermissionsArray)%>
                             <%= Html.PermissionList("Administration", Model.PermissionsArray)%>
                         </div>
                     </fieldset>
                     <div class="buttons"><ul>
                        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Update Permissions</a></li>
                    </ul></div>
                 </div>
             <% } %>
        <% });%>
<% }).SelectedIndex(0).Render();%>
<div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadUserLog('{0}');\" >Activity Logs</a>", Model.Id)%></div>


