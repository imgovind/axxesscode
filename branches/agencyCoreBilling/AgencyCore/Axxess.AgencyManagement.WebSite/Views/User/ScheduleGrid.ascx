﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "listuserschedule",
        Current.DisplayName + "&#8217;s Schedule and Tasks",
        Current.AgencyName)%>
<%= Html.Telerik().Grid<UserVisit>().Name("List_User_Schedule").HtmlAttributes(new  { @style="top:49px;"}).Columns(columns =>
{
    columns.Bound(v => v.PatientName);
    columns.Bound(v => v.TaskName).ClientTemplate("<#=Url#>").Title("Task").Width(250);
    columns.Bound(v => v.VisitDate).Title("Date").Width(100);
    columns.Bound(v => v.StatusName).Width(200).Title("Status");
    columns.Bound(v => v.StatusComment).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip red_note\" href=\"javascript:void(0);\" tooltip=\"<#=StatusComment#>\"> </a>");
    columns.Bound(v => v.VisitNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=VisitNotes#>\"></a>");
    columns.Bound(v => v.EpisodeNotes).Title(" ").Width(30).ClientTemplate("<a class=\"tooltip blue_note\" href=\"javascript:void(0);\" tooltip=\"<#=EpisodeNotes#>\"></a>");
    columns.Bound(v => v.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowMissedVisitModal('<#=EpisodeId#>','<#=PatientId#>','<#=Id#>');\">Missed Visit Form</a>").Title(" ").Width(150);
})
    .Groupable(settings => settings.Groups(groups =>
    {
        var data = ViewData["UserScheduleGroupName"].ToString();
        if (data == "PatientName")
        {
            groups.Add(s => s.PatientName);
        }
        else if (data == "VisitDate")
        {
            groups.Add(s => s.VisitDate);
        }
        else if (data == "TaskName")
        {
            groups.Add(s => s.TaskName);
        }
        else
        {
            groups.Add(s => s.VisitDate);
        }
    }))
    .ClientEvents(c => c.OnRowDataBound("Schedule.tooltip"))
    .DataBinding(dataBinding => dataBinding.Ajax().Select("ScheduleList", "User"))
    .Scrollable().Sortable()%>
<script type="text/javascript">
    $("#listuserschedule .t-group-indicator").hide();
    $("#listuserschedule .t-grouping-header").remove();
    $("#List_User_Schedule .t-grid-content").css({ 'height': 'auto', 'position': 'absolute', 'top': '57px', 'bottom': '25px' });
</script>