﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<% using (Html.BeginForm("Reset", "Signature", FormMethod.Post, new { @id = "resetSignatureForm" })) { %>
    <div class="wrapper main">
        <fieldset>
            <legend>Electronic Signature Reset</legend>
            <div class="wide_column">
                <div class="row">Please check your e-mail and enter the temporary signature along with your new signature below to complete the signature reset request.</div>
                <div class="row"><label for="Reset_Signature_CurrentSignature">Temporary Signature:</label><div class="float_right"><%= Html.Password("CurrentSignature", "", new { @id = "Reset_Signature_CurrentSignature", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
                <div class="row"><label for="Reset_Signature_NewSignature">New Signature:</label><div class="float_right"><%= Html.Password("NewSignature", "", new { @id = "Reset_Signature_NewSignature", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
                <div class="row"><label for="Reset_Signature_NewSignatureConfirm">Confirm New Signature:</label><div class="float_right"> <%= Html.Password("NewSignatureConfirm", "", new { @id = "Reset_Signature_NewSignatureConfirm", @class = "input_wrapper required", @maxlength = "15" })%></div></div>
            </div>
        </fieldset>
        <div class="buttons"><ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
        </ul></div>
    </div>
<%} %>