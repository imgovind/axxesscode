﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Account>" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Axxess&#8482; AgencyCore&#8482; | Forgot Password</title>
        <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("Mobile/account.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
        <link href="/Images/favicon.ico" rel="shortcut icon"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    </head>
    <body>
        <div id="login_wrapper" class="reset hidden">
            <div class="row">
                <%= Html.LabelFor(a => a.EmailAddress)%>
                <%= Html.TextBoxFor(a => a.EmailAddress, new { @class = "required" })%>
            </div>
            <div class="row">
                <%= Html.LabelFor(a => a.captchaValid) %>
                <%= Html.GenerateCaptcha() %>
            </div>
            <input type="button" value="Cancel" class="button" />
            <input type="submit" value="Send" class="button"/>
        </div>
        <div id="top_wrapper">
            <div class="logo"></div>
            <span class="mobile">mobile</span>
        </div>
        <% Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
                .Add("jquery-1.6.2.min.js")
                .Add("Plugins/Other/blockui.min.js")
                .Add("Plugins/Other/form.min.js")
                .Add("Plugins/Other/validate.min.js")
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
                .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
            ).OnDocumentReady(() => { %>
                ResetPassword.InitMobile(); <%
            }).Render(); %>
    </body>
</html>
