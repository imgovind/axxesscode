﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Logon>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Axxess&#8482; AgencyCore&#8482; | Login</title>
        <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("Mobile/account.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
        <link href="/Images/favicon.ico" rel="shortcut icon"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    </head>
    <body>
        <div id="login_wrapper" class="hidden">
            <div id="messages"></div>
            <%  using (Html.BeginForm("LogOn", "Account", FormMethod.Post, new { @id = "loginForm", @class = "login", @returnUrl = Request.QueryString["returnUrl"] })) { %>
            <div class="row">
                <%= Html.LabelFor(m => m.UserName) %>
                <%= Html.TextBoxFor(m => m.UserName, new { @id = "Login_UserName", @class = "required" })%>
            </div>
            <div class="row">
                <%= Html.LabelFor(m => m.Password) %>
                <%= Html.PasswordFor(m => m.Password, new { @id = "Login_Password", @class = "required" })%>
            </div>
            <div id="Login_Forgot" class="forgot">
                <a href="javascript:void(0)">Forgot your password?</a>
            </div>
            <div id="Login_RememberMe" class="row">
                <input type="checkbox" id="Login_RememberMe" checked="checked" class="checkbox" name="RememberMe" value="true" />
                <label class="checkbox" for="Login_RememberMe">Remember me</label>
            </div>
            <input id="Login_Button" type="submit" value="Login" class="button" />
            <%  } %>
        </div>
        <div id="top_wrapper" class="hidden">
            <div class="logo hidden"></div>
            <span class="mobile hidden">mobile</span>
        </div>
        <noscript>
            <div id="javascript-window">
                <div class="notification error">
                    <p>Your browser version does not meet our minimum browser requirements.</p>
                    <p>Our software requires JavaScript to be enabled in your browser.</p>
                    <p>Please contact Axxess or your company&#8217;s IT for assistance on how to enable JavaScript.</p>
                </div>
            </div>
        </noscript>
        <%  Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
                .Add("jquery-1.6.2.min.js")
                .Add("Plugins/Other/blockui.min.js")
                .Add("Plugins/Other/form.min.js")
                .Add("Plugins/Other/validate.min.js")
                .Add("Plugins/Other/jgrowl.min.js")
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
                .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
                .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
            ).OnDocumentReady(() => { %>
                Logon.InitMobile();
        <%  }).Render(); %>
        <% if (Model.IsLocked) { %>
        <script type="text/javascript">
            $("#login_wrapper").html($("<p/>").html("You exceeded the maximum number of login attempts. Please note that your AgencyCore online access will be disabled for 5 minutes. If you cannot remember your password, please click <a href='/Forgot'>here</a> to reset your password."));
        </script>
        <% } %>
    </body>
</html>