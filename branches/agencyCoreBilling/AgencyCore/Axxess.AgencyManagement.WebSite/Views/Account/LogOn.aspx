﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Logon>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login - Axxess Home Health Management System</title>
    <%= Html.Telerik()
        .StyleSheetRegistrar()
        .DefaultGroup(group => group
        .Add("account.css")
        .Combined(true)
        .Compress(true)
        .CacheDurationInDays(1)
        .Version(Current.AssemblyVersion))
    %>
    <link rel="stylesheet" href="/Content/css3.aspx" type="text/css" />
    <link href="/Images/favicon.ico" rel="shortcut icon" />
</head>
<body>
    <div id="login-wrapper">
        <% var browser = HttpContext.Current.Request.Browser; %>
        <% if (AppSettings.BrowserCompatibilityCheck == "false" || browser.IsBrowserAllowed())
           { %>
        <div id="login-window" class="hidden">
            <div class="box-header"><span class="img icon axxess"></span><span class="title">Axxess&#8482; Login</span></div>
            <div class="box">
            <div id="messages"></div>
            <% using (Html.BeginForm("LogOn", "Account", FormMethod.Post, new { @id = "loginForm", @class = "login", @returnUrl = Request.QueryString["returnUrl"] })) %>
            <% { %>
            <span class="img axxesslogo"></span>
            <div class="row">
                <%= Html.LabelFor(m => m.UserName)%>
                <%= Html.TextBoxFor(m => m.UserName, new { @id = "Login_UserName", @class = "required" })%>
            </div>
            <div class="row">
                <%= Html.LabelFor(m => m.Password)%>
                <%= Html.PasswordFor(m => m.Password, new { @id = "Login_Password", @class = "required" })%>
            </div>
            <div id="Login_Forgot" class="forgot">
                <a href="/Forgot">Forgot your password?</a>
            </div>
            <div id="Login_RememberMe" class="row tl">
                <input type="checkbox" id="Login_RememberMe" checked="checked" class="checkbox" name="RememberMe" value="true" />
                <label class="checkbox" for="Login_RememberMe">Remember me</label>
            </div>
            <input id="Login_Button" type="submit" value="Login" class="button" />
            <div class="row">&#160;</div>
            <% } %>
        </div>
        </div>
        <% }
           else
           { %>
        <div id="browser-window" class="hidden">
            <div class="box-header"><span class="img icon axxess"></span><span class="title">Browser Compatibility Check</span></div>
            <div class="box">
                <div class="notification info"><span>Your browser version does not meet our minimum browser requirements. <br /><br />Our software supports Internet Explorer 8 and FireFox 3+.<br /><br />Please download Internet Explorer 8 or Firefox by clicking on the links below.</span></div>
                <div>
                    <div class="fl">
                        <a style="font-size: 17px;" href="http://www.microsoft.com/windows/internet-explorer/worldwide-sites.aspx" title="Internet Explorer 8 Download">Download Internet Explorer 8</a><br />
                        <a style="font-size: 17px;" href="http://www.mozilla.com/en-US/firefox/" title="Firefox Download">Download Firefox 3.5 or higher</a><br /><br /><br /><br /><br />
                    </div>
                    <div class="fr"><span class="img axxesslogo"></span></div>
                </div>
                <div class="cl"></div>
            </div>
        </div>
        <% }
           if (DateTime.Now.Month < 5 && DateTime.Now.Day < 28)
           { %>
        <div id="bottom_wrapper">
            <div id="bottom_gradient_box">
                <div id="bottom_image_box"></div>
                <div id="bottom_button">
                    <a target="_blank" href="http://axxessconsult.com/seminars">Learn More</a>
                </div>
            </div>
        </div><%
} %>
        <noscript>
            <div id="javascript-window">
                <div class="box-header"><span class="img icon axxess"></span><span class="title">JavaScript Check</span></div>
                <div class="box">
                    <div class="notification info"><span>Your browser version does not meet our minimum browser requirements. <br /><br /><strong>Our software requires JavaScript to be enabled in your browser.</strong><br /><br />Please contact us at support@axxessweb.com for assistance on how to enable JavaScript.</span></div>
                </div>
            </div>
        </noscript> 
    </div>
    <div id="Account_Modal_Container" class="agencySelection hidden" style="text-align: center;">
        <span id="accountInUseMessage">This user is already logged in on another computer. If you choose to proceed, the user will be automatically logged off the other computer and their work will not be saved.</span><br />
        Are you sure you want to continue?<br />
        <input id="Login_Continue_Button" type="submit" value="Continue" class="button" />
        <input id="Login_Cancel_Button" type="submit" value="Cancel" class="button" />
    </div>
    <div id="Agency_Selection_Container" class="agencySelection hidden"></div>
        <% Html.Telerik().ScriptRegistrar().jQuery(false).DefaultGroup(group => group
            .Add("jquery-1.6.2.min.js")
            .Add("Plugins/Other/blockui.min.js")
            .Add("Plugins/Other/form.min.js")
            .Add("Plugins/Other/validate.min.js")
            .Add("Plugins/Other/jgrowl.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Account.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "Utility.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)
        ).OnDocumentReady(() => { %>
            Logon.Init(); <%
        }).Render(); %>
    <% if (Model.IsLocked) { %>
    <script type="text/javascript">
        $("#Login_UserName").attr("disabled", "disabled");
        $("#Login_Password").attr("disabled", "disabled");
        $("#Login_RememberMe").hide();
        $("#Login_Forgot").hide();
        $("#Login_Button").hide();
        $("#messages").empty().removeClass().addClass("notification error").append('<span>You have made too many failed login attempts. Please contact your Agency/Companys Administrator.</span>');
    </script>
    <% } %>
</body>
</html>

