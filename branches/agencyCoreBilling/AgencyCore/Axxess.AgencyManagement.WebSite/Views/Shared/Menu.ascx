﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
$("body").AcoreDesktop();
// -- Build Menu -->
Acore.AddMenu({ Name: "Home", Id: "home", IconX: "121", IconY: "86" });
    Acore.AddMenu({ Name: "My Account", Id: "account", Parent: "home" });
Acore.AddMenu({ Name: "Create", Id: "create", IconX: "140", IconY: "108" });
    Acore.AddMenu({ Name: "New", Id: "createnew", Parent: "create" });
Acore.AddMenu({ Name: "View", Id: "view", IconX: "161", IconY: "108" });
    <% if (Current.HasRight(Permissions.ViewLists)) { %>
    Acore.AddMenu({ Name: "Lists", Id: "viewlist", Parent: "view" });
    <% } %>
Acore.AddMenu({ Name: "Patients", Id: "patients", IconX: "183", IconY: "108" });
Acore.AddMenu({ Name: "Schedule", Id: "schedule", IconX: "205", IconY: "108" });
<% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
Acore.AddMenu({ Name: "Billing", Id: "billing", IconX: "227", IconY: "108" });
<% } %>
Acore.AddMenu({ Name: "Admin", Id: "admin", IconX: "142", IconY: "86" });
    Acore.AddMenu({ Name: "New", Id: "adminadd", Parent: "admin" });
    <% if (Current.HasRight(Permissions.ViewLists)) { %>
    Acore.AddMenu({ Name: "Lists", Id: "adminlist", Parent: "admin" });
    <% } %>
<% if (Current.HasRight(Permissions.AccessReports)) { %>
Acore.AddMenu({ Name: "Reports", Id: "reports", IconX: "164", IconY: "86" });
<% } %>
Acore.AddMenu({ Name: "Help", Id: "help", IconX: "186", IconY: "86" });
// -- Home Menu -->
Acore.AddWindow({ Name: "Edit Profile", Id: "editprofile", Url: "User/Profile", OnLoad: User.InitProfile, Menu: "account" });
Acore.AddWindow({ Name: "Reset Signature", Id: "forgotsignature", Url: "Signature/Forgot", OnLoad: User.InitForgotSignature, Menu: "account", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "200px", Width: "500px" });
Acore.AddWindow({ Name: "Dashboard", MenuName: "My Dashboard", Id: "homepage", Url: "Home/Dashboard", OnLoad: Widgets.Init, Menu: "home" });
Acore.AddWindow({ Name: "My Messages", Id: "messageinbox", Url: "Message/Inbox", OnLoad: Message.Init, Menu: "home" });
Acore.AddWindow({ Name: "My Schedule/Tasks", Id: "listuserschedule", Url: "User/Schedule", Menu: [ "home", "schedule" ] });
Acore.AddWindow({ Name: "My Monthly Calendar", Id: "userschedulemonthlycalendar", Url: "User/UserCalendar", Menu: "home" });
<% if (Current.HasRight(Permissions.AccessCaseManagement)) { %>
Acore.AddWindow({ Name: "Quality Assurance (QA) Center", Id: "caseManagement", Url: "Agency/CaseManagement", Menu: "home" });
<% } %>
// -- Create Menu (Some Duplicated to Admin Menu) -->
<% if (Current.HasRight(Permissions.ManageReferrals)) { %>
Acore.AddWindow({ Name: "New Referral", MenuName: "Referral", Id: "newreferral", Url: "Referral/New", OnLoad: Referral.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManagePatients)) { %>
Acore.AddWindow({ Name: "New Patient", MenuName: "Patient", Id: "newpatient", Url: "Patient/New", OnLoad: Patient.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.EditEpisode)) { %>
Acore.AddWindow({ Name: "New Episode", MenuName: "Episode", Id: "newepisode", Url: "Schedule/NewPatientEpisode", OnLoad: Schedule.InitTopMenuNewEpisode, Menu: [ "createnew", "adminadd" ] });
<% } %>
Acore.AddWindow({ Name: "Compose Message", MenuName: "Message", Id: "newmessage", Url: "Message/New", OnLoad: Message.InitNew, Menu: [ "createnew", "adminadd" ] });
Acore.AddWindow({ Name: "New Communication Note", MenuName: "Communication Note", Id: "newcommnote", Url: "CommunicationNote/New", OnLoad: Patient.InitNewCommunicationNote, Menu: [ "createnew", "adminadd" ] });
<% if (Current.HasRight(Permissions.ManagePatients)) { %>
Acore.AddWindow({ Name: "New Authorization", MenuName: "Authorization", Id: "newauthorization", Url: "Authorization/New", OnLoad: Patient.InitNewAuthorization, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>
Acore.AddWindow({ Name: "New Order", MenuName: "Order", Id: "neworder", Url: "Order/New", OnLoad: Patient.InitNewOrder, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %>
Acore.AddWindow({ Name: "New Physician Face-to-face Encounter", MenuName: "Physician Face-to-face Encounter", Id: "newfacetofaceencounter", Url: "FaceToFaceEncounter/New", OnLoad: Patient.InitNewFaceToFaceEncounter, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManageHospital)) { %>
Acore.AddWindow({ Name: "New Hospital", MenuName: "Hospital", Id: "newhospital", Url: "Hospital/New", OnLoad: Hospital.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManageInsurance)) { %>
Acore.AddWindow({ Name: "New Insurance/Payor", MenuName: "Insurance/Payor", Id: "newinsurance", Url: "Insurance/New", OnLoad: Insurance.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManagePhysicians)) { %>
Acore.AddWindow({ Name: "New Physician", MenuName: "Physician", Id: "newphysician", Url: "Physician/New", OnLoad: Physician.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManageContact)) { %>
Acore.AddWindow({ Name: "New Contact", MenuName: "Contact", Id: "newcontact", Url: "Contact/New", OnLoad: Contact.InitNew, Menu: [ "createnew", "adminadd" ] });
<% } %>
<% if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport)) { %>
Acore.AddWindow({ Name: "New Incident/Accident Report", MenuName: "Incident/Accident Report", Id: "newincidentreport", Url: "Incident/New", OnLoad: IncidentReport.InitNew, Menu: "createnew" });
Acore.AddWindow({ Name: "New Infection Report", MenuName: "Infection Report", Id: "newinfectionreport", Url: "Infection/New", OnLoad: InfectionReport.InitNew, Menu: "createnew" });
<% } %>
<% if (Current.HasRight(Permissions.CreateOasisSubmitFile)) { %>
Acore.AddWindow({ Name: "OASIS Export", Id: "oasisExport", Url: "Oasis/ExportView", Menu: "create" });
<% } %>
// -- View Menu (Some Duplicated to Admin Menu) -->
<% if (Current.HasRight(Permissions.ViewLists)) { %>
Acore.AddWindow({ Name: "List Patients", MenuName: "Patients", Id: "listpatients", Url: "Patient/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "List Referrals", MenuName: "Referrals", Id: "listreferrals", Url: "Referral/List", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "List Contacts", MenuName: "Contacts", Id: "listcontacts", Url: "Contact/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "List Templates", MenuName: "Templates", Id: "listtemplates", Url: "Template/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "List Hospital", MenuName: "Hospital", Id: "listhospitals", Url: "Hospital/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "List Insurances/Payors", MenuName: "Insurances/Payors", Id: "listinsurances", Url: "Insurance/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "List Physicians", MenuName: "Physicians", Id: "listphysicians", Url: "Physician/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "List Users", MenuName: "Users", Id: "listusers", Url: "User/Grid", Menu: [ "viewlist", "adminlist" ] });
Acore.AddWindow({ Name: "List Incident Reports", MenuName: "Incident Reports", Id: "listincidents", Url: "Incident/Grid", Menu: "viewlist" });
Acore.AddWindow({ Name: "List Infection Reports", MenuName: "Infection Reports", Id: "listinfections", Url: "Infection/Grid", Menu: "viewlist" });
Acore.AddWindow({ Name: "List Communication Notes", MenuName: "Communication Notes", Id: "communicationnoteslist", Url: "CommunicationNote/List", Menu: "viewlist" });
<% } %>
Acore.AddWindow({ Name: "Blank Forms", Id: "blankforms", Url: "Agency/Blankforms", Menu: "view" });
<% if (Current.HasRight(Permissions.ViewExportedOasis)) { %>
Acore.AddWindow({ Name: "Exported OASIS", Id: "oasisExported", Url: "Oasis/ExportedView", Menu: "view" });
<% } %>
<% if (Current.IsQA || Current.IsOfficeManager || Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsClerk || Current.IsScheduler || Current.IsBiller) { %>
Acore.AddWindow({ Name: "Past Due Recerts", Id: "listpastduerecerts", Url: "Agency/RecertsPastDueGrid", Menu: "view" });
Acore.AddWindow({ Name: "Upcoming Recerts", Id: "listupcomingrecerts", Url: "Agency/RecertsUpcomingGrid", Menu: "view" });
<% } %>
<% if (Current.HasRight(Permissions.AccessOrderManagementCenter)) { %>
Acore.AddMenu({ Name: "Orders Management", Id: "ordersmanagement", Parent: "view" });
Acore.AddWindow({ Name: "Orders To Be Sent", Id: "orderstobesent", Url: "Agency/OrdersToBeSentView", Menu: "ordersmanagement" });
Acore.AddWindow({ Name: "Orders Pending Signature", Id: "orderspendingsignature", Url: "Agency/OrdersPendingSignatureView", Menu: "ordersmanagement" });
Acore.AddWindow({ Name: "Orders History", Id: "ordersHistory", Url: "Agency/OrdersHistory", Menu: "ordersmanagement" });
<% } %>
<% if (Current.HasRight(Permissions.PrintClinicalDocuments)) { %>
Acore.AddWindow({ Name: "Print Queue", Id: "printqueue", Url: "Agency/PrintQueue", Menu: "view" });
<% } %>
// -- Patient Menu -->
Acore.AddWindow({ Name: "Patient Charts", Id: "patientcenter", Url: "Patient/Center", OnLoad: Patient.InitCenter, Menu: "patients" });
<% if (Current.HasRight(Permissions.ViewExisitingReferrals)) { %>
Acore.AddWindow({ Name: "Existing Referrals", Id: "existingreferrals", Url: "Referral/List", Menu: "patients" });
<% } %>
<% if (Current.HasRight(Permissions.ViewLists)) { %>
Acore.AddWindow({ Name: "Pending Admissions", Id: "listpendingpatients", Url: "Patient/PendingGrid", Menu: "patients" });
Acore.AddWindow({ Name: "Non-Admissions", Id: "listnonadmit", Url: "Patient/NonAdmitGrid", Menu: "patients" });
Acore.AddWindow({ Name: "Hospitalization Logs", Id: "listhospitalization", Url: "Patient/HospitalizationGrid", Menu: "patients" });
<% } %>
// -- Schedule Menu -->
Acore.AddWindow({ Name: "Schedule Center", Id: "schedulecenter", Url: "Schedule/Center", OnLoad: Schedule.InitCenter, Menu: "schedule" });
<% if (Current.HasRight(Permissions.AccessReports)) { %>
Acore.AddWindow({ Name: "Schedule Deviation Report", Id: "scheduledeviation", Url: "Schedule/Deviation", Menu: "schedule" });
<% } %>
<% if (Current.HasRight(Permissions.ScheduleVisits)) { %>
Acore.AddWindow({ Name: "Reassign Schedule", Id: "schedulereassign", Url: "Schedule/ReAssignSchedules", OnLoad: function () { Schedule.reassignScheduleInit("All") }, Menu: "schedule" });
<% } %>
// -- Billing Menu -->
<% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
Acore.AddMenu({ Name: "Medicare / Medicare HMO", Id: "medicareclaim", Parent: "billing" });
Acore.AddWindow({ Name: "Create Claims", Id: "billingcenter", Url: "Billing/Center", OnLoad: Billing.InitCenter, Menu: "medicareclaim" });
Acore.AddWindow({ Name: "Pending Claims", Id: "pendingClaim", Url: "Billing/PendingClaims", OnLoad: Billing.InitPendingClaimUpdate, Menu: "medicareclaim" });
Acore.AddWindow({ Name: "Billing/Claims History", Id: "billingHistory", Url: "Billing/History", OnLoad: Billing.InitCenter, Menu: "medicareclaim" });
Acore.AddWindow({ Name: "Remittance Advice", Id: "remittances", Url: "Billing/Remittance", OnLoad:Billing.InitRemittance, Menu: "medicareclaim" });
Acore.AddWindow({ Name: "Eligibility Report", Id: "medicareeligibilityreport", Url: "Billing/EligibilityReport", Menu: "medicareclaim" });
Acore.AddMenu({ Name: "Managed Care", Id: "managedcareclaim", Parent: "billing" });
Acore.AddWindow({ Name: "Create Claims", Id: "createmanagedclaims", Url: "Billing/ManagedCenter", OnLoad: ManagedBilling.InitCenter, Menu: "managedcareclaim" });
Acore.AddWindow({ Name: "Claims History", Id: "managedclaims", Url: "Billing/ManagedHistory", OnLoad: ManagedBilling.InitCenter, Menu: "managedcareclaim" });
Acore.AddWindow({ Name: "Claim Submission History", Id: "submittedclaims",Url: "Billing/SubmittedList" ,Menu: "billing" });
Acore.AddWindow({ Name: "Submitted Claim Details", Id: "submittedclaimdetail", Url: "Billing/SubmittedClaimDetail" });
<% } %>
// -- Admin Menu -->
Acore.AddWindow({ Name: "New Template", MenuName: "Template", Id: "newtemplate", Url: "Template/New", OnLoad: Template.InitNew, Menu: "adminadd" });
<% if (Current.HasRight(Permissions.ManageUsers)) { %>
Acore.AddWindow({ Name: "New User", MenuName: "User", Id: "newuser", Url: "User/New", OnLoad: User.InitNew, Menu: "adminadd" });
<% } %>

<% if (Current.HasRight(Permissions.ManagePayroll)) { %>
Acore.AddWindow({ Name: "Payroll Summary", Id: "payrollsummary", Url: "Payroll/Search", OnLoad: Payroll.InitSearch, Menu: "admin" });
<% } %>
// -- Reports Menu -->
<% if (Current.HasRight(Permissions.AccessReports)) { %>
Acore.AddWindow({ Name: "Report Center", Id: "reportcenter", Url: "Report/Center", OnLoad: Report.Init, Menu: "reports" });
<% } %>
// -- Help Menu -->
Acore.AddMenuItem({ Name: "Discussion Forum", Href: "/Forum", Parent: "help" });
// -- Windows not found in the menus -->
Acore.AddWindow({ Name: "New Hospitalization Log", Id: "newhospitalizationlog", Url: "Patient/NewHospitalizationLog" });
Acore.AddWindow({ Name: "Edit Hospitalization Log", Id: "edithospitalizationlog", Url: "Patient/EditHospitalizationLog" });
Acore.AddWindow({ Name: "Hospitalization Logs", Id: "patienthospitalizationlogs", Url: "Patient/HospitalizationLogs" });
Acore.AddWindow({ Name: "Allergy Profile", Id: "allergyprofile", Url: "Patient/AllergyProfile" });
Acore.AddWindow({ Name: "Medicare Eligibility Reports", Id: "medicareeligibilitylist", Url: "Patient/MedicareEligibilityList" });
Acore.AddWindow({ Name: "Medication Profile SnapShot", Id: "medicationprofilesnapshot", Url: "Patient/MedicationProfileSnapShot", OnLoad: Patient.InitMedicationProfileSnapshot });
Acore.AddWindow({ Name: "Medication Profile", Id: "medicationprofile", Url: "Patient/MedicationHistoryView" });
Acore.AddWindow({ Name: "Signed Medication Profiles", Id: "medicationprofilesnapshothistory", Url: "Patient/MedicationHistorySnapshotView" });
Acore.AddWindow({ Name: "Master Calendar", Id: "masterCalendarMain", Url: "Schedule/MasterCalendarMain" });
Acore.AddWindow({ Name: "Wound Care Flowsheet", Id: "woundcare", Url: "Schedule/WoundCare", OnLoad: Schedule.WoundCareInit });
Acore.AddWindow({ Name: "SN Visit", Id: "snVisit", Url: "Schedule/SNVisit" });
Acore.AddWindow({ Name: "PT Visit", Id: "ptVisit", Url: "Schedule/PTVisit" });
Acore.AddWindow({ Name: "PTA Visit", Id: "ptaVisit", Url: "Schedule/PTVisit" });
Acore.AddWindow({ Name: "PT Evaluation", Id: "ptEvaluation", Url: "Schedule/PTEvaluation" });
Acore.AddWindow({ Name: "PT Re-Evaluation", Id: "ptReEvaluation", Url: "Schedule/PTEvaluation" });
Acore.AddWindow({ Name: "PT Maintenance", Id: "ptMaintenance", Url: "Schedule/PTEvaluation" });
Acore.AddWindow({ Name: "PT Discharge", Id: "ptDischarge", Url: "Schedule/PTDischarge" });
Acore.AddWindow({ Name: "OT Evaluation", Id: "otEvaluation", Url: "Schedule/OTEvaluation" });
Acore.AddWindow({ Name: "OT Re-Evaluation", Id: "otReEvaluation", Url: "Schedule/OTEvaluation" });
Acore.AddWindow({ Name: "OT Maintenance", Id: "otMaintenance", Url: "Schedule/OTEvaluation" });
Acore.AddWindow({ Name: "OT Discharge", Id: "otDischarge", Url: "Schedule/OTDischarge" });
Acore.AddWindow({ Name: "OT Visit", Id: "otVisit", Url: "Schedule/OTVisit" });
Acore.AddWindow({ Name: "COT Visit", Id: "cotVisit", Url: "Schedule/OTVisit" });
Acore.AddWindow({ Name: "ST Visit", Id: "stVisit", Url: "Schedule/STVisit" });
Acore.AddWindow({ Name: "ST Evaluation", Id: "stEvaluation", Url: "Schedule/STEvaluation" });
Acore.AddWindow({ Name: "ST Re-Evaluation", Id: "stReEvaluation", Url: "Schedule/STEvaluation" });
Acore.AddWindow({ Name: "ST Maintenance", Id: "stMaintenance", Url: "Schedule/STEvaluation" });
Acore.AddWindow({ Name: "MSW Evaluation", Id: "mswEvaluation", Url: "Schedule/MSWEvaluation" });
Acore.AddWindow({ Name: "MSW Visit", Id: "mswVisit", Url: "Schedule/MSWVisit" });
Acore.AddWindow({ Name: "MSW Assessment", Id: "mswAssessment", Url: "Schedule/MSWEvaluation" });
Acore.AddWindow({ Name: "MSW Progress Note", Id: "mswProgressNote", Url: "Schedule/MSWProgressNote" });
Acore.AddWindow({ Name: "MSW Discharge", Id: "mswDischarge", Url: "Schedule/MSWEvaluation" });
Acore.AddWindow({ Name: "ST Discharge", Id: "stDischarge", Url: "Schedule/STEvaluation" });
Acore.AddWindow({ Name: "OASIS-C Start of Care", Id: "StartOfCare", Url: "Oasis/StartOfCare" });
Acore.AddWindow({ Name: "Non-OASIS Start of Care", Id: "NonOasisStartOfCare", Url: "Oasis/NonOasisStartOfCare" });
Acore.AddWindow({ Name: "OASIS-C Recertification", Id: "Recertification", Url: "Oasis/Recertification" });
Acore.AddWindow({ Name: "Non-OASIS Recertification", Id: "NonOasisRecertification", Url: "Oasis/NonOasisRecertification" });
Acore.AddWindow({ Name: "OASIS-C Resumption of Care", Id: "ResumptionOfCare", Url: "Oasis/ResumptionOfCare" });
Acore.AddWindow({ Name: "OASIS-C Follow Up", Id: "FollowUp", Url: "Oasis/FollowUp" });
Acore.AddWindow({ Name: "OASIS-C Death at Home", Id: "DischargeFromAgencyDeath", Url: "Oasis/DischargeFromAgencyDeath" });
Acore.AddWindow({ Name: "OASIS-C Discharge from Agency", Id: "DischargeFromAgency", Url: "Oasis/DischargeFromAgency" });
Acore.AddWindow({ Name: "Non-OASIS Discharge", Id: "NonOasisDischarge", Url: "Oasis/NonOasisDischarge" });
Acore.AddWindow({ Name: "OASIS-C Transfer For Discharge", Id: "TransferInPatientDischarged", Url: "Oasis/TransferInPatientDischarged" });
Acore.AddWindow({ Name: "OASIS-C Transfer Not Discharge", Id: "TransferInPatientNotDischarged", Url: "Oasis/TransferInPatientNotDischarged" });
Acore.AddWindow({ Name: "LVN Supervisory Visit", Id: "lvnsVisit", Url: "Schedule/LVNSVisit" });
Acore.AddWindow({ Name: "Diabetic Daily Visit Nursing Note", Id: "snDiabeticDailyVisit", Url: "Schedule/SNDiabeticDailyVisit" });
Acore.AddWindow({ Name: "HHA Supervisory Visit", Id: "hhasVisit", Url: "Schedule/HHASVisit" });
Acore.AddWindow({ Name: "Discharge Summary", Id: "dischargeSummary", Url: "Schedule/DischargeSummary", OnLoad: Schedule.dischargeSummaryInit });
Acore.AddWindow({ Name: "Home Health Aide Progress Note", Id: "hhAideVisit", Url: "Schedule/HHAVisit", OnLoad: Schedule.hhaVisitInit });
Acore.AddWindow({ Name: "Home Health Aide Care Plan", Id: "hhaCarePlan", Url: "Schedule/HHACarePlan" });
Acore.AddWindow({ Name: "60 Day Summary/Case Conference", Id: "sixtyDaySummary", Url: "Schedule/SixtyDaySummary" });
Acore.AddWindow({ Name: "Transfer Summary", Id: "transferSummary", Url: "Schedule/TransferSummary"});
Acore.AddWindow({ Name: "Coordination of Care", Id: "coordinationofcare", Url: "Schedule/TransferSummary", OnLoad: Schedule.transferSummaryInit });
Acore.AddWindow({ Name: "Personal Care Services Progress Note", Id: "pasVisit", Url: "Schedule/PASVisitNote", OnLoad: Schedule.pasVisitNoteInit });
Acore.AddWindow({ Name: "Personal Care Services Care Plan", Id: "pasCarePlan", Url: "Schedule/PASCarePlan" });
Acore.AddWindow({ Name: "Edit Infection Report", Id: "editinfectionreport" });
Acore.AddWindow({ Name: "Edit Incident/Accident Report", Id: "editincidentreport" });
Acore.AddWindow({ Name: "Authorization List", Id: "listauthorizations" });
Acore.AddWindow({ Name: "Communication Notes", Id: "patientcommunicationnoteslist" });
Acore.AddWindow({ Name: "Patient Orders History", Id: "patientordershistory" });
Acore.AddWindow({ Name: "Patient Deleted Tasks History", Id: "patientdeletedtaskhistory" });
Acore.AddWindow({ Name: "Patient 60 Day Summary", Id: "patientsixtydaysummary" });
Acore.AddWindow({ Name: "Driver / Transportation Note", Id: "transportationnote" });
Acore.AddWindow({ Name: "Patient Vital Signs", Id: "patientvitalsigns" });
Acore.AddWindow({ Name: "RAP", Id: "rap" });
Acore.AddWindow({ Name: "Final", Id: "final" });
Acore.AddWindow({ Name: "Managed Claim", Id: "managedclaimedit" });
Acore.AddWindow({ Name: "Task Details", Id: "scheduledetails" });
Acore.AddWindow({ Name: "Edit Episode", Id: "editepisode" });
Acore.AddWindow({ Name: "Edit Order", Id: "editorder" });
Acore.AddWindow({ Name: "Edit Contact", Id: "editcontact" });
Acore.AddWindow({ Name: "Edit Template", Id: "edittemplate" });
Acore.AddWindow({ Name: "Edit Hospital", Id: "edithospital" });
Acore.AddWindow({ Name: "Edit Location", Id: "editlocation" });
Acore.AddWindow({ Name: "Edit Physician", Id: "editphysician" });
Acore.AddWindow({ Name: "Edit Insurance", Id: "editinsurance" });
Acore.AddWindow({ Name: "Edit User", Id: "edituser" });
Acore.AddWindow({ Name: "Edit Patient", Id: "editpatient" });
Acore.AddWindow({ Name: "Admit Patient", Id: "admitpatient" });
Acore.AddWindow({ Name: "Patient Non-Admission", Id: "nonadmitpatient" });
Acore.AddWindow({ Name: "Edit Referral", Id: "editreferral" });
Acore.AddWindow({ Name: "Validation Result", Id: "validation" });
Acore.AddWindow({ Name: "Missed Visit Report", Id: "newmissedvisit" });
Acore.AddWindow({ Name: "Supply Worksheet", Id: "notessupplyworksheet", Url: "Schedule/SupplyWorksheet" });
Acore.AddWindow({ Name: "Edit Emergency Contact", Id: "editemergencycontact", Url: "Patient/EditEmergencyContactContent" });
Acore.AddWindow({ Name: "New Emergency Contact", Id: "newemergencycontact", Url: "Patient/NewEmergencyContactContent" });
Acore.AddWindow({ Name: "Edit 485 - Plan of Care (From Assessment)", Id: "editplanofcare" });
Acore.AddWindow({ Name: "Plan of Treatment/Care", Id: "newplanofcare" });
Acore.AddWindow({ Name: "Edit Communication Note", Id: "editcommunicationnote" });
Acore.AddWindow({ Name: "Edit Authorization", Id: "editauthorization" });
Acore.AddWindow({ Name: "Remittance Detail", Id: "remittancedetail" });
Acore.AddWindow({ Name: "Schedule Event Logs", Id: "schdeuleeventlogs" });
Acore.AddWindow({ Name: "List of In-active episodes", Id: "inactiveepisode" });
Acore.AddWindow({ Name: "List of Episode logs", Id: "episodelogs" });
Acore.AddWindow({ Name: "List of Patient logs", Id: "patientlogs" });
Acore.AddWindow({ Name: "List of Patient Medication logs", Id: "medicationlogs" });
Acore.AddWindow({ Name: "List of Physicain logs", Id: "physicianlogs" });
Acore.AddWindow({ Name: "List of User logs", Id: "userlogs" });
Acore.AddWindow({ Name: "List of referral logs", Id: "referrallogs" });
Acore.AddWindow({ Name: "List of contact logs", Id: "contactlogs" });
Acore.AddWindow({ Name: "List of insurance logs", Id: "insurancelogs" });
Acore.AddWindow({ Name: "List of location logs", Id: "locationlogs" });
Acore.AddWindow({ Name: "List of hospital logs", Id: "hospitallogs" });
Acore.AddWindow({ Name: "List of template logs", Id: "templatelogs" });
Acore.AddWindow({ Name: "List of claim logs", Id: "claimlogs" });
Acore.AddWindow({ Name: "Edit Visit Rates", Id: "visitrates"});
Acore.AddWindow({ Name: "Patient Managed Dates", Id: "patientmanageddates"});
Acore.AddWindow({ Name: "Episode Orders", Id: "patientepisodeorders"});


<% if (Current.HasRight(Permissions.AccessBillingCenter)) { %>
Acore.AddWindow({ Name: "Claim Summary", Id: "claimSummary", Url: "Billing/ClaimSummary" });
Acore.AddWindow({ Name: "Managed Claim Summary", Id: "managedclaimsummary", Url: "Billing/ManagedClaimSummary" });
<% } %>
$("#mainmenu").superfish();
Acore.GetRemoteContent = <%= AppSettings.GetRemoteContent %>;
Acore.Open("homepage");
</script>

