﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<script type="text/javascript">
$("body").AcoreDesktop();
// -- Build Menu -->
Acore.AddMenu({ Name: "Home", Id: "home", IconX: "121", IconY: "86" });
    Acore.AddMenu({ Name: "My Account", Id: "account", Parent: "home" });
Acore.AddMenu({ Name: "Create", Id: "create", IconX: "140", IconY: "108" });
    Acore.AddMenu({ Name: "New", Id: "createnew", Parent: "create" });
Acore.AddMenu({ Name: "View", Id: "view", IconX: "161", IconY: "108" });
    <% if (Current.HasRight(Permissions.ViewLists)) { %>
    Acore.AddMenu({ Name: "Lists", Id: "viewlist", Parent: "view" });
    <% } %>
Acore.AddMenu({ Name: "Patients", Id: "patients", IconX: "183", IconY: "108" });
Acore.AddMenu({ Name: "Help", Id: "help", IconX: "186", IconY: "86" });
// -- Home Menu -->
Acore.AddWindow({ Name: "Edit Profile", Id: "editprofile", Url: "User/Profile", OnLoad: User.InitProfile, Menu: "account" });
Acore.AddWindow({ Name: "Reset Signature", Id: "forgotsignature", Url: "Signature/Forgot", OnLoad: User.InitForgotSignature, Menu: "account", Resize: false, StatusBar: false, Center: true, IgnoreMinSize: true, Height: "200px", Width: "500px" });
Acore.AddWindow({ Name: "Dashboard", MenuName: "My Dashboard", Id: "homepage", Url: "Home/Dashboard", OnLoad: Widgets.Init, Menu: "home" });
Acore.AddWindow({ Name: "My Messages", Id: "messageinbox", Url: "Message/Inbox", OnLoad: Message.Init, Menu: "home" });

// -- Create Menu (Some Duplicated to Admin Menu) -->
Acore.AddWindow({ Name: "New Referral", MenuName: "Referral", Id: "newreferral", Url: "Referral/New", OnLoad: Referral.InitNew, Menu: [ "createnew" ] });
Acore.AddWindow({ Name: "Compose Message", MenuName: "Message", Id: "newmessage", Url: "Message/New", OnLoad: Message.InitNew, Menu: [ "createnew" ] });

// -- View Menu (Some Duplicated to Admin Menu) -->
Acore.AddWindow({ Name: "List Referrals", MenuName: "Referrals", Id: "listreferrals", Url: "Referral/List", Menu: [ "viewlist", "adminlist" ] });
// -- Patient Menu -->
Acore.AddWindow({ Name: "Existing Referrals", Id: "existingreferrals", Url: "Referral/List", Menu: "patients" });
// -- Help Menu -->
Acore.AddMenuItem({ Name: "Discussion Forum", Href: "/Forum", Parent: "help" });
// -- Windows not found in the menus -->
Acore.AddWindow({ Name: "Edit Referral", Id: "editreferral" });

$("ul#mainmenu").superfish();
Acore.GetRemoteContent = <%= AppSettings.GetRemoteContent %>;
Acore.Open("homepage");
</script>

