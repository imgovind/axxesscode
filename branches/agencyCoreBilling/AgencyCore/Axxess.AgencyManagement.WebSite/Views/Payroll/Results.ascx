﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PayrollSummaryViewData>" %>
<% if (Model != null && Model.VisitSummary != null && Model.VisitSummary.Count > 0){ %>
<ul>
    <li class="align_center">Summary [<a href="javascript:void(0);" onclick="Payroll.LoadDetails();">View Details</a>] <%= Html.ActionLink("Export to Excel", "PayrollSummary", "Export", new { StartDate = Model.StartDate, EndDate = Model.EndDate, payrollStatus = Model.PayrollStatus }, new { id = "ExportPayrollSummary_ExportLink" })%><a href="javascript:void(0);" onclick="U.GetAttachment('Payroll/SummaryPdf', { 'payrollStartDate': '<%=Model.StartDate %>', 'payrollEndDate': '<%=Model.EndDate %>', 'payrollStatus': '<%=Model.PayrollStatus %>' });"> [ Print PDF ]</a></li>
    <li><span class="payrolluser strong">User</span><span class="payrollcount strong">Count</span><span class="payrollaction strong">Action</span></li>
</ul>

<ol><%
    int i = 1;
    foreach (var summary in Model.VisitSummary)
    { %>
    <%= i % 2 != 0 ? "<li class='odd'>" : "<li class='even'>" %>
        <span class="payrolluser"><%= summary.UserName %></span>
        <span class="payrollcount"><%= summary.VisitCount.ToString() %></span>
        <span class="payrollaction"><%= summary.VisitCount > 0 ? "<a href='javascript:void(0);' onclick=\"Payroll.LoadDetail('" + summary.UserId.ToString() + "');\">Detail</a>" : ""%></span>
    </li><% i++; } %>
</ol>
<script type="text/javascript">$(".payroll ol").each(function() { $("li:first", this).addClass("first"); $("li:last", this).addClass("last"); });</script>
<%} else { %><div class="strong align_center"><span class="img warning"></span>Your search yielded 0 results.</div><%} %>
<%= Html.Hidden("PayrollSearchType", "", new { @id="payrollSearchType" }) %>