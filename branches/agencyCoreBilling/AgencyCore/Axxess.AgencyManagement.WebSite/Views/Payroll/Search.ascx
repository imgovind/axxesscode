﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DateTime>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "payrollsummary",
        "Payroll Summary",
        Current.AgencyName) %>
<% using (Html.BeginForm("Search", "Payroll", FormMethod.Post, new { @id = "searchPayrollForm" })) { %>
<div id="Payroll_SummaryContent" class="main wrapper align_center">
    <div class="payroll_header">
        <span class="strong">Payroll Summary</span> &#151;
        <label for="payrollStartDate">From: </label>
        <input type="date" id="payrollStartDate" name="payrollStartDate" value="<%= Model.ToShortDateString() %>" class="required " />
        <label for="payrollEndDate">To: </label>
        <input type="date" id="payrollEndDate" name="payrollEndDate" value="<%= DateTime.Today.ToShortDateString() %>" maxdate="<%= DateTime.Today.ToShortDateString() %>" class="required" />
        <label for="payrollStatus">Staus: </label>
        <select name="payrollStatus" id="payrollStatus"><option value="All">All</option><option value="true">Paid</option><option value="false" selected>Unpaid</option></select>
        <div class="buttons float_right">
            <ul>
                <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Generate</a></li>
            </ul>
        </div>
    </div>
    <div id="payrollSearchResult" class="payroll align_left"></div>
    <div id="payrollSearchResultDetails" class="align_left"></div>
    <div id="payrollSearchResultDetail" class="align_left"></div>
    <%= Html.Hidden("PayrollSearchResultView", string.Empty, new { @id = "markAsPaidButtonId"} ) %>
    <div id="payrollMarkAsPaidButton" class="buttons hidden"><ul><li><a href="javascript:void(0);" onclick="Payroll.MarkAsPaid();">Mark As Paid</a></li><li><a href="javascript:void(0);" onclick="Payroll.MarkAsUnpaid();">Mark As Unpaid</a></li></ul></div>
</div>
<% } %>