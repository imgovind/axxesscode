﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PendingPatient>" %>
<% using (Html.BeginForm("AddAdmit", "Patient", FormMethod.Post, new { @id = "newAdmitPatientForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Admit_Patient_Id" })%>
<%= Html.Hidden("IsAdmit", "true", new { @id = "NonAdmit_Patient_IsAdmit" })%>
<%= Html.Hidden("Type", Model.Type)%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Admit Patient <span class="bigtext align_center">(<%= Model.DisplayName %>)</span></legend>
        <div class="column">
            <div class="row"><label for="Admit_Patient_PatientID" class="float_left"><span class="green">(M0020)</span> Patient ID:</label><div class="float_right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "Admit_Patient_PatientID", @class = "text input_wrapper", @maxlength = "50" }) %></div></div>
            <div class="row"><label for="Admit_Patient_StartOfCareDate" class="float_left"><span class="green">(M0030)</span> Start of Care Date:</label><div class="float_right"><input type="date" name="StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" id="Admit_Patient_StartOfCareDate" class="required" onchange="OnSocChange();" /></div></div>
            <div class="row"><label for="Admit_Patient_EpisodeStartDate" class="float_left">Episode Start Date:</label><div class="float_right"><input type="date" name="EpisodeStartDate" value="<%= DateTime.Today.ToShortDateString() %>" id="Admit_Patient_EpisodeStartDate" class="required" /></div></div>
            <div class="row"><label for="Admit_Patient_PatientReferralDate" class="float_left">Referral Date:</label><div class="float_right"><input type="date" name="ReferralDate" value="<%= (Model.ReferralDate > DateTime.MinValue) ? Model.ReferralDate.ToShortDateString() : "" %>" id="Admit_Patient_PatientReferralDate" /></div></div>
            <div class="row"><label for="Admit_Patient_CaseManager" class="float_left">Case Manager:</label><div class="float_right"><%= Html.CaseManagers("CaseManagerId", (Model != null && !Model.CaseManagerId.IsEmpty()) ? Model.CaseManagerId.ToString() : "", new { @id = "Admit_Patient_CaseManager", @class = "Users requireddropdown valid" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Admit_Patient_LocationId" class="float_left">Agency Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", !Model.AgencyLocationId.IsEmpty() ? Model.AgencyLocationId.ToString() : string.Empty, new { @id = "Admit_Patient_LocationId", @class = "BranchLocation requireddropdown" })%></div></div>
            <div class="row"><label for="Admit_Patient_PrimaryInsurance" class="float_left">Primary Insurance:</label><div class="float_right"><%= Html.Insurances("PrimaryInsurance", Model.PrimaryInsurance, false, new { @id = "Admit_Patient_PrimaryInsurance" })%></div></div>
            <div class="row"><label for="Admit_Patient_SecondaryInsurance" class="float_left">Secondary Insurance:</label><div class="float_right"><%= Html.Insurances("SecondaryInsurance", Model.SecondaryInsurance, false, new { @id = "Admit_Patient_SecondaryInsurance" })%></div></div>
            <div class="row"><label for="Admit_Patient_TertiaryInsurance" class="float_left">Tertiary Insurance:</label><div class="float_right"><%= Html.Insurances("TertiaryInsurance", Model.TertiaryInsurance, false, new { @id = "Admit_Patient_TertiaryInsurance" })%></div></div>
            <div class="row"><label for="Admit_Patient_Assign" class="float_left">Assign to Clinician:</label><div class="float_right"><%= Html.Clinicians("UserId", (Model != null && !Model.UserId.IsEmpty()) ? Model.UserId.ToString() : "", new { @id = "Admit_Patient_Assign", @class = "requireddropdown Users  valid" })%></div></div>
            <div class="row"><input id="Admit_Patient_IsFaceToFaceEncounterCreated" type="checkbox" checked="checked" value="true" name="IsFaceToFaceEncounterCreated" class="radio float_left" />Create a face to face encounter <em>(this is applicable for SOC date after 01/01/2011)</em></div>
        </div>
        
    </fieldset>
    <div class="clear"></div>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Admit</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
    function OnSocChange() {
        var e = $("#Admit_Patient_StartOfCareDate").datepicker("getDate");
        $("#Admit_Patient_EpisodeStartDate").datepicker("setDate", e);
        $("#Admit_Patient_EpisodeStartDate").datepicker("option", "mindate", e);
    }
</script>
<% } %>