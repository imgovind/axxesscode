﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "newfacetofaceencounter",
        "Physician Face-to-face Encounter",
        Current.AgencyName)%>
<% using (Html.BeginForm("Add", "FaceToFaceEncounter", FormMethod.Post, new { @id = "newFaceToFaceEncounterForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <div class="column">
            <div class="row"><label for="New_FaceToFaceEncounter_PatientName" class="float_left">Patient Name:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", !Model.IsEmpty() ? Model.ToString() : "" , new { @id = "New_FaceToFaceEncounter_PatientName", @class="requireddropdown" })%></div></div>
            <div class="row"><label for="New_FaceToFaceEncounter_EpisodeId" class="float_left">Episode Associated:</label><div class="float_right"> <%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_FaceToFaceEncounter_EpisodeId", @class = "requireddropdown" })%></div> </div>
            <div class="row">
                <label for="New_FaceToFaceEncounter_PhysicianDropDown" class="float_left">Physician:</label>
                <div class="float_right"><%= Html.TextBox("PhysicianId", "", new { @id = "New_FaceToFaceEncounter_PhysicianDropDown", @class = "Physicians requiredphysician" })%></div>
                <div class="clear"></div>
                <div class="float_right ancillary_button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
            <div class="row"><label for="New_FaceToFaceEncounter_Date" class="float_left">Request Date:</label><div class="float_right"><input type="date" name="RequestDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now %>" id="New_FaceToFaceEncounter_Date" class="required" /></div></div>
        </div>
        <div class="wide_column"> 
            <table>
                <tbody>
                    <tr>  
                        <td ><%= Html.RadioButton("Certification", "1",true, new { @id = "New_FaceToFaceEncounter_Certification1", @class = "radio" })%> <label>POC Certifying Physician</label></td>
                        <td ><%= Html.RadioButton("Certification", "2", new { @id = "New_FaceToFaceEncounter_Certification2", @class = "radio" })%><label>Non POC Certifying Physician</label></td>
                     </tr>
                </tbody>
            </table>
            <p>*Note:<em>Completing this document creates a Physician Face to Face Encounter request document that must be submitted to the physician. The physician will be required to certify that the patient is homebound and the home health services provided are medically necessary. The physician will certify by signing the face to face encounter document and returning to the home health agency.</em></p>
        </div>
        <div class="clear"></div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_FaceToFaceEncounter_Status" })%>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$('#New_FaceToFaceEncounter_Status').val('115');$(this).closest('form').submit();">Submit</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newfacetofaceencounter');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $("#New_FaceToFaceEncounter_PhysicianDropDown").PhysicianInput();
    Schedule.loadEpisodeDropDown('New_FaceToFaceEncounter_EpisodeId', $('#New_FaceToFaceEncounter_PatientName'));
    $('#New_FaceToFaceEncounter_PatientName').change(function() { Schedule.loadEpisodeDropDown('New_FaceToFaceEncounter_EpisodeId', $(this)); }); 
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
</script>
<%} %>
