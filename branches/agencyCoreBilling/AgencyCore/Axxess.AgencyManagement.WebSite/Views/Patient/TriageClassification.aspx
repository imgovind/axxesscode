﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PatientProfile>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
     <title><%= Model.Agency.Name.IsNotNullOrEmpty() ? Model.Agency.Name.ToTitleCase() + " | " : "" %>EMERGENCY PREPAREDNESS PLAN/TRIAGE CLASSIFICATION<%= Model.Patient != null ? (" | " + Model.Patient.LastName + ", " + Model.Patient.FirstName + " " + Model.Patient.MiddleInitial).ToTitleCase() : "" %></title>
     <%= Html.Telerik().StyleSheetRegistrar().DefaultGroup(group => group.Add("print.css").Combined(true).Compress(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)) %>
     <% Html.Telerik().ScriptRegistrar().jQuery(false).Globalization(true).DefaultGroup(group => group
            .Add("jquery-1.6.2.min.js")
            .Add("Modules/" + (AppSettings.UseMinifiedJs ? "min/" : "") + "printview.js")
            .Compress(true).Combined(true).CacheDurationInDays(1).Version(Current.AssemblyVersion)).Render(); %>
</head>
<body>
<script type="text/javascript">
    printview.firstheader = printview.header = "%3Ctable class=%22fixed%22%3E%3Ctbody%3E%3Ctr%3E%3Ctd%3E" +
        "<%= Model.Agency != null && Model.Agency.Name.IsNotNullOrEmpty() ? "%3Cspan class=%22big%22%3E" + Model.Agency.Name.Clean().ToTitleCase() + "%3C/span%3E%3Cbr /%3E" : string.Empty %>" +
        "<%= Model.Agency.MainLocation != null && Model.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine1.Clean().ToTitleCase() : string.Empty %>" +
        "<%= Model.Agency.MainLocation != null && Model.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressLine2.Clean().ToTitleCase() + "%3Cbr /%3E" : "%3Cbr /%3E"%>" +
        "<%= Model.Agency.MainLocation != null && Model.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressCity.Clean().ToTitleCase() + ", " : string.Empty %>" +
        "<%= Model.Agency.MainLocation != null && Model.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressStateCode.ToString().Clean().ToUpper() + "&#160; " : string.Empty %>" +
        "<%= Model.Agency.MainLocation != null && Model.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? Model.Agency.MainLocation.AddressZipCode.Clean() : string.Empty %>" +
        "%3C/td%3E%3Cth class=%22h1%22%3EEmergency Preparedness Plan/%3Cbr /%3ETriage Classification%3C/th%3E%3C/tr%3E%3Ctr%3E%3Ctd%3E%3Cspan class=%22medium%22%3EPatient Name : &#160;" +
        "<%= Model.Patient != null && Model.Patient.LastName.IsNotNullOrEmpty() ? Model.Patient.LastName.Clean() + ", " : "" %>" +
        "<%= Model.Patient != null && Model.Patient.FirstName.IsNotNullOrEmpty() ? Model.Patient.FirstName.Clean() : "" %>" +
        "<%= Model.Patient != null && Model.Patient.MiddleInitial.IsNotNullOrEmpty() ? " " + Model.Patient.MiddleInitial.Clean() + "." : "" %>" +
        "%3C/span%3E %3Cbr /%3E %3Cspan class=%22medium%22%3E Patient address : &#160;" +
        "<%= Model.Patient != null && Model.Patient.AddressLine1.IsNotNullOrEmpty() ? Model.Patient.AddressLine1.Clean() : "" %>" +
        "<%= Model.Patient != null && Model.Patient.AddressLine2.IsNotNullOrEmpty() ? ", " + Model.Patient.AddressLine2.Clean() + "%3Cbr /%3E" : "%3Cbr /%3E" %>" +
        "<%= Model.Patient != null && Model.Patient.AddressCity.IsNotNullOrEmpty() ? Model.Patient.AddressCity.Clean() + ", " : "" %>" +
        "<%= Model.Patient != null && Model.Patient.AddressStateCode.IsNotNullOrEmpty() ? Model.Patient.AddressStateCode.Clean() + " &#160;" : "" %>" +
        "<%= Model.Patient != null && Model.Patient.AddressZipCode.IsNotNullOrEmpty() ? Model.Patient.AddressZipCode.Clean() : "" %>" +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22medium%22%3EPatient Tel# : &#160;" +
        "<%= Model.Patient != null && Model.Patient.PhoneHome.IsNotNullOrEmpty() ? Model.Patient.PhoneHome.ToPhone().Clean() : "" %>" +
        "%3C/span%3E %3C/td%3E%3Ctd%3E%3Cspan class=%22medium%22%3EEmergency Contact Person : &#160;" +
        "<%= Model.EmergencyContact != null && Model.EmergencyContact.DisplayName.IsNotNullOrEmpty() ? Model.EmergencyContact.DisplayName.Clean() : "" %>" +
        "%3C/span%3E%3Cbr /%3E%3Cspan class=%22medium%22%3EContact Tel # : &#160;" +
        "<%= Model.EmergencyContact != null && Model.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty() ? Model.EmergencyContact.PrimaryPhone.ToPhone().Clean() : "" %>" +
        "%3C/span%3E%3C/td%3E%3C/tr%3E%3C/tbody%3E%3C/table%3E";
    printview.footer = "";
    printview.addsection(
        printview.span("%3Cbr/%3E") +
        printview.checkbox("%3Cspan class=%22customtext abs%22%3E 1. %3C/span%3E%3Cspan class=%22mediumtext customtext%22%3E%3Cstrong%3ELife threatening (or potential) and requires ongoing medical treatment. When necessary, appropriate arrangements for evacuation to an acute care facility will be made.%3C/strong%3E%3Cbr /%3E%3Cspan class=%22margin%22%3EUnable to evacuate/ transport self. Unable to withstand any interruption in power supply. No readily available family or caregiver or family/ caregiver unable to provide needed care. Reqires transport to an acute care facilty or specialized shelter situation. Patient is equipment-dependent e.g. equipment for life support e.g. patients dependent on Ventilator, LVAD. Patient lives in the vicinity of the disaster and lives alone and has no family members. Patient has no available transport. Notify EMS and Ambulance Company for transportation from the immediate area. Patients in this category who require ventilator may also need to be assessed for notification of the electric company upon admittance to services, to ensure continuity of electric power should the power fail.%3C/span%3E%3C/span%3E%3Cbr /%3E%3Cbr /%3E%3Cbr /%3E%3Cbr /%3E ",<%= Model.Patient.Triage == 1  ? "true" : "false" %>) +
        printview.checkbox("%3Cspan class=%22customtext abs%22%3E 2. %3C/span%3E%3Cspan class=%22mediumtext customtext%22%3E%3Cstrong%3ENot life threatening but would suffer severe adverse effects from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage.)%3C/strong%3E%3Cbr /%3E%3Cspan class=%22margin%22%3EAble to withstand up to 48 hour power interruption. Unable to transport self or no transportation available from caregiver. Examples are Oxygen-dependent patients with COPD, insuline dependent and unable to self-inject, large open draining wound with potential for sepsis, IV antibiotics. Patients with renal problems, heart failure or other very high risk patients who should be triage quickly. Patient services may be postponed for up 48 hours without averse effect to patient. If necessary, call the police or appropriate local authorities and give name and address of patient.%3C/span%3E%3C/span%3E%3Cbr /%3E%3Cbr /%3E%3Cbr /%3E%3Cbr /%3E",<%= Model.Patient.Triage == 2  ? "true" : "false" %>) +
        printview.checkbox("%3Cspan class=%22customtext abs%22%3E 3. %3C/span%3E%3Cspan class=%22mediumtext customtext%22%3E%3Cstrong%3EVisits could be postponed 24&#8211;48 hours without adverse effects (i.e., new insulin dependent diabetic able to self-inject, sterile wound care with a minimal amount to no drainage.)%3C/strong%3E%3Cbr /%3E%3Cspan class=%22margin%22%3EAble to care for self or willing and able caregiver available. Transportation available from family, firends, volunteers or caregiver. Examples are oxygen-dependent with adequate 0%3Csub%3E2%3C/sub%3E supply and has means to have tanks fefilled. New insulin dependent diabetic who can self-inject and perform glucometer checks but needs phone support or further education, tube feeding. Patients with limited in home support that can be moblized with moderate assistance. Patient lives alone but has a caregiver e.g. a neighbor. Patient and/or caregiver can administer all medications, including injections safely.%3C/span%3E%3C/span%3E%3Cbr /%3E%3Cbr /%3E%3Cbr /%3E%3Cbr /%3E",<%= Model.Patient.Triage == 3  ? "true" : "false" %>) +
        printview.checkbox("%3Cspan class=%22customtext abs%22%3E 4. %3C/span%3E%3Cspan class=%22mediumtext customtext%22%3E%3Cstrong%3EVisits could be postponed 72&#8211;96 hours without adverse effects (i.e., post op with no open wound, anticipated discharge within the next 10&#8211;14 days, routine catheter changes.)%3C/strong%3E%3Cbr /%3E%3Cspan class=%22margin%22%3EPatient is indpendent in most ADLs. Transportaion is available from family memebers, friends, volunteers or caregiver. This type of patient requires no assistance from the agency during time of natural disaster except for phone contact to identify that the patient is managing well. Family members are available to assist the patient. Examples are pateints being seen for Blood pressure monitoring, Foley Catheter changes, Personal Care only.%3C/span%3E%3C/span%3E%3Cbr /%3E%3Cbr /%3E%3Cbr /%3E",<%= Model.Patient.Triage == 4  ? "true" : "false" %>));
</script>
</body>
</html>



