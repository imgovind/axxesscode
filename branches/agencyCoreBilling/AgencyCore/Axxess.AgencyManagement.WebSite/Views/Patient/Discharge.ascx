﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("SaveDischarge", "Patient", FormMethod.Post, new { @id = "dischargePatientForm" }))%><%  { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <%= Html.Hidden("PatientId", Model)%>
    <fieldset>
            <legend>Patient Discharge Information</legend>
            <div class="column"><div class="row"><label for="DischargeDate" class="float_left">&#160;&#160;Date of discharge:</label><div class="float_right"><input type="date" name="DischargeDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" class="required" /></div></div></div>
            <div class="clear"></div>
            <div class="wide_column" ><label for="Comment" class="float_left">Reason for discharge:</label><div class="row"><%= Html.TextArea("Comment", "", new { @class = "required" })%></div></div>
    </fieldset>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Discharge</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li></ul></div>
</div>
<% } %>
<script type="text/javascript"> $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>"); $(".row .required").closest(".row").prepend("<span class='req_red abs_right'>*</span>"); </script>