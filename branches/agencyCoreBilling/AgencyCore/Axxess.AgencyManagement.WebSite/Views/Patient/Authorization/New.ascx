﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "newauthorization",
        "New Authorization",
        Current.AgencyName)%>
<% using (Html.BeginForm("Add", "Authorization", FormMethod.Post, new { @id = "newAuthorizationForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Detail</legend>
        <div class="column">
            <div class="row"><label for="New_Authorization_PatientName" class="float_left">Patient Name:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "New_Authorization_PatientName", @class="requireddropdown" })%></div></div>
            <div class="row"><label for="New_Authorization_StartDate" class="float_left">Start Date:</label><div class="float_right"><input type="date" name="StartDate" id="New_Authorization_StartDate" class="required" /></div></div>
            <div class="row"><label for="New_Authorization_EndDate" class="float_left">End Date:</label><div class="float_right"><input type="date" name="EndDate" id="New_Authorization_EndDate" class="required" /></div></div>
            <div class="row"><label for="New_Authorization_LocationId" class="float_left">Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", "", new { @id = "New_Authorization_LocationId", @class = "BranchLocation" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Authorization_Status" class="float_left">Status:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.AuthorizationStatus, "Status", "", new { @id = "New_Authorization_Status" })%></div></div>
            <div class="row"><label for="New_Authorization_Insurance" class="float_left">Insurance:</label><div class="float_right"><%= Html.Insurances("Insurance", "", true, new { @id = "New_Authorization_Insurance", @class = "Insurances requireddropdown" })%></div></div>
            <div class="row"><label for="New_Authorization_AuthNumber" class="float_left">Authorization Number:</label><div class="float_right"><%= Html.TextBox("Number", "", new { @id = "New_Authorization_AuthNumber", @class = "text input_wrapper required", @maxlength = "30" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Visits or Hours Authorized</legend>
        <% var countVisitType = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", "1"); %>
        <% var countHourType = new SelectList(new[] { new SelectListItem { Text = "Visits", Value = "1" }, new SelectListItem { Text = "Hours", Value = "2" } }, "Value", "Text", "2"); %>
        <div class="column">
            <div class="row"><label for="New_Authorization_SNVisit" class="float_left">SN Count:</label><div class="float_right"><%= Html.TextBox("SNVisit", "", new { @id = "New_Authorization_SNVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("SNVisitCountType", countVisitType, new { @id = "New_Authorization_SNVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_PTVisit" class="float_left">PT Count:</label><div class="float_right"><%= Html.TextBox("PTVisit", "", new { @id = "New_Authorization_PTVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("PTVisitCountType", countVisitType, new { @id = "New_Authorization_PTVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_OTVisit" class="float_left">OT Count:</label><div class="float_right"><%= Html.TextBox("OTVisit", "", new { @id = "New_Authorization_OTVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("OTVisitCountType", countVisitType, new { @id = "New_Authorization_OTVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_STVisit" class="float_left">ST Count:</label><div class="float_right"><%= Html.TextBox("STVisit", "", new { @id = "New_Authorization_STVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("STVisitCountType", countVisitType, new { @id = "New_Authorization_STVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_MSWVisit" class="float_left">MSW Count:</label><div class="float_right"><%= Html.TextBox("MSWVisit", "", new { @id = "New_Authorization_MSWVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("MSWVisitCountType", countVisitType, new { @id = "New_Authorization_MSWVisitCountType", @class = "shortselect" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Authorization_HHAVisit" class="float_left">HHA Count:</label><div class="float_right"><%= Html.TextBox("HHAVisit", "", new { @id = "New_Authorization_HHAVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("HHAVisitCountType", countVisitType, new { @id = "New_Authorization_HHAVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_DieticianVisit" class="float_left">Dietician Count:</label><div class="float_right"><%= Html.TextBox("DieticianVisit", "", new { @id = "New_Authorization_DieticianVisit", @class = "numeric sn", @maxlength = "4" }) %><%= Html.DropDownList("DieticianVisitCountType", countVisitType, new { @id = "New_Authorization_DieticianVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_RNVisit" class="float_left">RN Count:</label><div class="float_right"><%= Html.TextBox("RNVisit", "", new { @id = "New_Authorization_RNVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("RNVisitCountType", countHourType, new { @id = "New_Authorization_RNVisitCountType", @class = "shortselect" })%></div></div>
            <div class="row"><label for="New_Authorization_LVNVisit" class="float_left">LVN Count:</label><div class="float_right"><%= Html.TextBox("LVNVisit", "", new { @id = "New_Authorization_LVNVisit", @class = "numeric sn", @maxlength = "4" })%><%= Html.DropDownList("LVNVisitCountType", countHourType, new { @id = "New_Authorization_LVNVisitCountType", @class = "shortselect" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Comments</legend>
        <div class="wide_column">
            <div class="row"><p class="charsRemaining"></p><textarea id="New_Authorization_Comments" name="Comments" cols="5" rows="6" maxcharacters="500"></textarea></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newauthorization');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>
<%}%>
