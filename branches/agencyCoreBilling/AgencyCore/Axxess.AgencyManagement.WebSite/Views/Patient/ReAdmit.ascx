﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("PatientReadmit", "Patient", FormMethod.Post, new { @id = "readmitForm" })) { %>
<%= Html.Hidden("PatientId", Model.Id)%>
 <fieldset>
   <legend>Re-admission  Information</legend>
   <div class="column"><div class="row"><label for="ChangeStatus_ReadmissionDate" class="float_left">Re-admission Date (Start Of Care):</label><div class="float_right"><%= Html.Telerik().DatePicker().Name("ReadmissionDate").Value(DateTime.Today).MinDate(DateTime.MinValue).MaxDate(DateTime.Now).HtmlAttributes(new { @id = "ChangeStatus_ReadmissionDate", @class = "text required" })%></div></div></div>
   <div class="clear"></div>
   <div class="buttons"><ul><li><a  href="javascript:void(0);" onclick="$(this).closest('form').submit();">Re-admit</a></li><li><a  href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li></ul></div>
</fieldset>
<%} %>
