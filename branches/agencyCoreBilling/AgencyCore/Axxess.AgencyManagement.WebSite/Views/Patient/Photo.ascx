﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("AddPhoto", "Patient", FormMethod.Post, new { @id = "changePatientPhotoForm" }))   { %>
   <%= Html.Hidden("patientId", Model.Id)%>
<div class="wrapper main">
     <fieldset>
        <legend>Change Patient Photo</legend>
        <div class="align_center">
        <%= Model.DisplayName %><br />
        <% if (!Model.PhotoId.IsEmpty()) { %><img src="/Asset/<%= Model.PhotoId.ToString() %>" alt="User Photo" /><% } else { %><img src="/Images/blank_user.jpeg" alt="User Photo" /><% } %><div class="clear"></div>
        </div>
        <table class="form"><tbody>
            <tr><td>Browse Photo:&#160;<input id="Change_Patient_Photo" type="file" name="Photo1" onchange = "validateImage();" /></td></tr>
        </tbody></table>
    </fieldset>
    <div id="changePatientPhotoError" class="errormessage"></div>
    <div class="buttons"><ul>
        <li><a id="Change_PatientPhoto_Submit" href="javascript:void(0);" onclick="$(this).closest('form').submit()">Upload</a></li>
        <% if (!Model.PhotoId.IsEmpty()) { %><li><a href="javascript:void(0);" onclick="Patient.RemovePhoto('<%= Model.Id %>')">Remove</a></li><% } %>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Exit</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $("#Change_PatientPhoto_Submit").hide();
    function validateImage() {
        var ext = $('#Change_Patient_Photo').val().split('.').pop().toLowerCase();
        var allow = new Array('gif', 'png', 'jpg', 'jpeg');
        if (jQuery.inArray(ext, allow) == -1) {
            alert('Please select a valid image extension!');
            $('#Change_Patient_Photo').val('');
            $("#Change_PatientPhoto_Submit").hide();
        } else {
            $("#Change_PatientPhoto_Submit").show();
        }
    }
</script>
<% } %>