﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Medication>" %>
<% using (Html.BeginForm("UpdatePatientMedication", "Patient", FormMethod.Post, new { @id = "editMedicationForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Medication_Id" })%>
<%= Html.Hidden("ProfileId", Model.ProfileId, new { @id = "Edit_Medication_ProfileId" })%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset class="editmed">
        <legend>Edit Medication</legend>
        <div class="wide_column">
            <div class="row">
                <div class="longstanding float_left">
                    <%= Html.CheckBox("IsLongStanding", Model.IsLongStanding, new { @id = "Edit_Medication_IsLongStanding", @class = "bigradio" })%>
                    <label for="Edit_Medication_IsLongStanding" class="bold">Long Standing</label>
                </div><div class="float_left">
                    <span>Start Date:</span>
                    <input type="date" name="StartDate" value="<%= Model.StartDate.IsValid() ? Model.StartDate.ToShortDateString() : string.Empty %>" id="Edit_Medication_StartDate" />
                </div>
            </div>
            <div class="row">
                <label for="Edit_Medication_MedicationDosage" class="float_left">Medication &#38; Dosage:</label>
                <div class="float_left"><%= Html.TextBox("MedicationDosage", Model.MedicationDosage.IsNotNullOrEmpty() ? Model.MedicationDosage : string.Empty, new { @id = "Edit_Medication_MedicationDosage", @class = "longtext input_wrapper required", @maxlength = "120" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Medication_Frequency" class="float_left">Frequency:</label>
                <div class="float_left"><%= Html.TextBox("Frequency", Model.Frequency.IsNotNullOrEmpty() ? Model.Frequency : string.Empty, new { @id = "Edit_Medication_Frequency", @class = "text input_wrapper Frequency", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Medication_Route" class="float_left">Route:</label>
                <div class="float_left"><%= Html.TextBox("Route", Model.Route.IsNotNullOrEmpty() ? Model.Route : string.Empty, new { @id = "Edit_Medication_Route", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
            <div class="row">
                <label for="Edit_Medication_Type" class="float_left">Type:</label>
                <div class="float_left"><%
       var medicationTypes = new SelectList(new[] { 
           new SelectListItem { Text = "New", Value = "N" },
           new SelectListItem { Text = "Changed", Value = "C" },
           new SelectListItem { Text = "Unchanged", Value = "U" }
       }, "Value", "Text", Model.MedicationType.Value.IsNotNullOrEmpty() ? Model.MedicationType.Value : "N" ); %>
                    <%= Html.DropDownList("medicationType", medicationTypes)%>
                </div>
            </div>
            <div class="row">
                <label for="Edit_Medication_Classification" class="float_left">Classification:</label>
                <div class="float_left"><%= Html.TextBox("Classification", Model.Classification.IsNotNullOrEmpty() ? Model.Classification : string.Empty, new { @id = "Edit_Medication_Classification", @class = "text input_wrapper", @maxlength = "100" })%></div>
            </div>
         </div>   
    </fieldset>
    <%= Html.Hidden("AddAnother", "", new { @id="Edit_Medication_AddAnother" })%>   
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Update</a></li>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Close</a></li>
    </ul></div>
</div>
<%} %>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    if ($("#Edit_Medication_StartDate").val() == "1/1/0001") $("#Edit_Medication_StartDate").val("");
</script>