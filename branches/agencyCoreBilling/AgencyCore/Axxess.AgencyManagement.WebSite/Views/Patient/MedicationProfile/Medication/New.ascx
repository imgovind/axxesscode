﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("InsertNewMedication", "Patient", FormMethod.Post, new { @id = "newMedicationForm" })) { %>
<%= Html.Hidden("medicationProfileId", Model, new { @id = "New_Medication_ProfileId" })%>
<%= Html.Hidden("LexiDrugId", "", new { @id = "New_Medication_DrugId" })%>   
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset class="newmed">
        <legend>New Medication</legend>
        <div class="wide_column">
            <div class="row"><div class="longstanding float_left"><%= Html.CheckBox("IsLongStanding", false, new { @id = "New_Medication_IsLongStanding", @class = "bigradio" })%><label for="New_Medication_IsLongStanding" class="bold">Long Standing</label></div><div class="float_left"><span>Start Date:</span>&nbsp;<input type="date" name="StartDate" id = "New_Medication_StartDate" /></div></div>
            <div class="row"><label for="New_Medication_MedicationDosage" class="float_left">Medication &#38; Dosage:</label><div class="float_left"><%= Html.TextBox("MedicationDosage", "", new { @id = "New_Medication_MedicationDosage", @class = "longtext input_wrapper required", @maxlength = "120" })%></div></div>
            <div class="row"><label for="New_Medication_Frequency" class="float_left">Frequency:</label><div class="float_left"><%= Html.TextBox("Frequency", "", new { @id = "New_Medication_Frequency", @class = "text input_wrapper Frequency", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Medication_Route" class="float_left">Route:</label><div class="float_left"><%= Html.TextBox("Route", "", new { @id = "New_Medication_Route", @class = "text input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Medication_Type" class="float_left">Type:</label><div class="float_left">
                <% var medicationTypes = new SelectList(new[]
                    { 
                       new SelectListItem { Text = "New", Value = "N" },
                       new SelectListItem { Text = "Changed", Value = "C" },               
                       new SelectListItem { Text = "Unchanged", Value = "U" }               
                       
                    }, "Value", "Text");%>
                <%= Html.DropDownList("medicationType", medicationTypes)%>
            </div></div>
            <div class="row"><label for="New_Medication_Classification" class="float_left">Classification:</label><div class="float_left"><%= Html.TextBox("Classification", "", new { @id = "New_Medication_Classification", @class = "text input_wrapper", @maxlength = "100" })%></div></div>
         </div>   
    </fieldset><%= Html.Hidden("AddAnother", "", new { @id="New_Medication_AddAnother" })%>   
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick='$("#New_Medication_AddAnother").val("");$(this).closest("form").submit();'>Save &#38; Exit</a></li>
        <li><a href="javascript:void(0);" onclick='$("#New_Medication_AddAnother").val("AddAnother");$(this).closest("form").submit();'>Save &#38; Add Another</a></li>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
    </ul></div>
</div>
<%} %>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>