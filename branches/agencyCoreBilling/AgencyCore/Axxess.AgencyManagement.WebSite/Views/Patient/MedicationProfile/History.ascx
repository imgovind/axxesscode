﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MedicationProfileViewData>" %>
<% using (Html.BeginForm("SaveMedicationHistory", "Patient", FormMethod.Post, new { @id = "newMedicationProfileForm" }))
   { %>
<%
    var data = Model != null && Model.Questions != null ? Model.Questions : new Dictionary<string, Question>(); %>
<% = Html.Hidden("Id", Model.MedicationProfile.Id, new  {@id="medicationProfileHistroyId" })%>
<% = Html.Hidden("PatientId", Model.MedicationProfile.PatientId)%>
<div class="visitContainer">
    <table class="visitNote" style="border-top: solid 0.25px #8B8878;" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <div class="newRow">
                        <div class="two">
                            <label for="HHACarePlan_PatientName">
                                Patient Name:</label>
                             <%= Html.Label(Model.Patient.FirstName+" "+ Model.Patient.LastName ) %>  (<%= Html.Label( Model != null && Model.Patient != null ? Model.Patient.PatientIdNumber : string.Empty )%>)
                        </div>
                            <div class="two">
                            <label for="HHACarePlan_MR">
                                Episode/Period:</label>
                            <%= Html.Label(string.Format(" {0} - {1}", Model!=null&&Model.StartDate!=null? Model.StartDate.ToShortDateString():string.Empty, Model!=null&&Model.StartDate!=null?Model.EndDate.ToShortDateString():string.Empty))%>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="newRow">
                       
                        <div class="three">
                            <label for="TransferSummary_PrimaryDiagnosis">Primary Diagnosis:</label><%= Html.TextBox("MedicationProfile_PrimaryDiagnosis", data.ContainsKey("PrimaryDiagnosis") ? data["PrimaryDiagnosis"].Answer : string.Empty, new { @id = "MedicationProfile_PrimaryDiagnosis", @class = "diagnosisBox" })%>
                          </div>
                         <div class="three">
                           <label for="TransferSummary_PrimaryDiagnosis">Secondary Diagnosis:</label>
                            <%= Html.TextBox("MedicationProfile_PrimaryDiagnosis1", data.ContainsKey("PrimaryDiagnosis1") ? data["PrimaryDiagnosis1"].Answer : string.Empty, new { @id = "MedicationProfile_PrimaryDiagnosis1", @class = "diagnosisBox" })%>
                          </div>
                         <div class="three">  
                            <label for="TransferSummary_PrimaryDiagnosis">Tertiary Diagnosis:</label><%= Html.TextBox("MedicationProfile_PrimaryDiagnosis2", data.ContainsKey("PrimaryDiagnosis2") ? data["PrimaryDiagnosis2"].Answer : string.Empty, new { @id = "MedicationProfile_PrimaryDiagnosis2", @class = "diagnosisBox" })%>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="newRow">
                        <div class="two">
                            <% string[] allergies = data.ContainsKey("485Allergies") && data["485Allergies"].Answer != "" ? data["485Allergies"].Answer.Split(',') : null; %>
                            <input name="MedicationProfile_485Allergies" value="" type="hidden" />
                            <input name="MedicationProfile_485Allergies" value="Yes" type="checkbox" '<% if (allergies!=null && allergies.Contains("Yes")) { %>checked="checked"<% } %>'" /><label>
                                Allergies :
                            </label>
                            <%= Html.TextBox("MedicationProfile_485AllergiesDescription", data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer : string.Empty, new { @id = "MedicationProfile_485AllergiesDescription" })%>
                        </div>
                        <div class="two">
                            <div class="two">
                                Pharmacy:
                                <%= Html.TextBox("PharmacyName", Model.PharmacyName, new { @id = "MedicationProfile_PharmacyName" })%>
                            </div>
                            <div class="two">
                                Phone:<%= Html.TextBox("PharmacyPhone", Model.PharmacyPhone, new { @id = "MedicationProfile_PharmacyPhone" })%>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
      <div class="buttons" style="text-align: right;"><ul>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="Patient.loadMedicationProfileSnapshot('<%= Model.Patient.Id%>');">Sign Medication Profile</a></li>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="Acore.OpenPrintView({ Url: '/Patient/MedicationProfilePrint/<%=Model.Patient.Id %>', PdfUrl: '/Patient/MedicationProfilePdf', PdfData: { 'id': '<%=Model.Patient.Id %>' } });">Print Medication Profile</a></li>
    </ul></div>
    <div class="newRow one medication">
        <%= Html.Telerik().Grid<Medication>()
        .Name("MedicatonProfileGridActive")
        .DataKeys(keys =>
        {
            keys.Add(M => M.Id);
        })
                                  .ToolBar(commands => commands.Insert().ButtonType(GridButtonType.ImageAndText).HtmlAttributes(new { id = "MedicatonProfileGridInsertButton", style = "margin-left:0" }))
        .DataBinding(dataBinding =>
        {
            dataBinding.Ajax()
                .Select("Medication", "Patient", new { medId = Model.MedicationProfile.Id, medicationCategory = "Active", assessmentType = "" })
                .Insert("InsertMedication", "Patient", new { medId = Model.MedicationProfile.Id, medicationCategory = "Active", assessmentType = "" })
                .Update("UpdateMedication", "Patient", new { medId = Model.MedicationProfile.Id, medicationCategory = "Active", assessmentType = "" })
                .Delete("DeleteMedication", "Patient", new { medId = Model.MedicationProfile.Id, medicationCategory = "Active", assessmentType = "" });
        })
        .Columns(columns =>
        {
            columns.Bound(M => M.IsLongStanding)
          .ClientTemplate("<input type='checkbox' disabled='disabled' name='IsLongStanding' <#=IsLongStanding? checked='checked' : '' #> />").Width(30);
            columns.Bound(M => M.StartDate).Format("{0:d}").Width(90);
            columns.Bound(M => M.MedicationDosage).Title("Medication & Dosage");
            columns.Bound(M => M.Route).Title("Route");
            columns.Bound(M => M.Frequency).Title("Frequency");
            columns.Bound(M => M.MedicationType).ClientTemplate("<label><#=MedicationType!=null&&MedicationType!=''? MedicationType.Text : ''#> </label>");
            columns.Bound(M => M.Classification);
            columns.Bound(M => M.DCDate).Format("{0:d}").Title("Discharge").Width(100); ;
            columns.Command(commands =>
            {
                commands.Edit();
                commands.Delete();
            }).Width(120).Title("Commands");
        })
             .ClientEvents(e => e.OnEdit("Patient.onMedicationProfileEdit").OnRowDataBound("Patient.onMedicationProfileRowDataBound").OnDataBound("Patient.OnDataBound"))
             .Editable(editing => editing.Mode(GridEditMode.InLine))
        .Pageable()
        .Sortable().Footer(false) 
        %>
    </div>
    <div class="newRow one medication">
        <%= Html.Telerik().Grid<Medication>()
        .Name("MedicatonProfileGridDC")
        .DataKeys(keys =>
        {
            keys.Add(M => M.Id);
        })

        .DataBinding(dataBinding =>
        {
            dataBinding.Ajax()
                .Select("Medication", "Patient", new { medId = Model.MedicationProfile.Id, medicationCategory = "DC", assessmentType = "" })
                .Update("UpdateMedication", "Patient", new { medId = Model.MedicationProfile.Id, medicationCategory = "DC", assessmentType = "" })
                .Delete("DeleteMedication", "Patient", new { medId = Model.MedicationProfile.Id, medicationCategory = "DC", assessmentType = "" });
        })
        .Columns(columns =>
        {
            columns.Bound(M => M.IsLongStanding)
          .ClientTemplate("<input type='checkbox' disabled='disabled' name='IsLongStanding' <#=IsLongStanding? checked='checked' : '' #> />").Width(30);
            columns.Bound(M => M.StartDate).Format("{0:d}").Width(90);
            columns.Bound(M => M.MedicationDosage).Title("Medication & Dosage");
            columns.Bound(M => M.Route).Title("Route");
            columns.Bound(M => M.Frequency).Title("Frequency");
            columns.Bound(M => M.MedicationType).ClientTemplate("<label><#=MedicationType!=null&&MedicationType!=''? MedicationType.Text : ''#> </label>");
            columns.Bound(M => M.Classification);
            columns.Bound(M => M.DCDate).Format("{0:d}").Title("Date").Width(100); ;
            columns.Command(commands =>
            {
                commands.Edit();
                commands.Delete();
            }).Width(120).Title("Commands");
        })
             .ClientEvents(e => e.OnEdit("Patient.onMedicationProfileEdit").OnRowDataBound("Patient.onMedicationProfileRowDataBound"))
             .Editable(editing => editing.Mode(GridEditMode.InLine))
        .Pageable()
        .Sortable().Footer(false) 
        %>
    </div>
   <div class="newRow one medication">
    <table class="visitNote" style="border-top: solid 0.25px #8B8878;" cellpadding="0">
        <tfoot>
            <tr>
                <td>
                    <div>
                        <div class="buttons"><ul>
                            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
                            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('medicationprofile');">Close</a></li>
                        </ul></div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
    </div>
</div>
<%} %>

