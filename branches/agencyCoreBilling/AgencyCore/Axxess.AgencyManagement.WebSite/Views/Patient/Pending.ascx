﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "listpendingpatients",
        "Pending Patient Admissions",
        Current.AgencyName)%>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<PendingPatient>()
        .Name("List_PatientPending_Grid")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.DisplayName).Title("Name").Sortable(false);
            columns.Bound(p => p.PatientIdNumber).Title("MR#").Width(120);
            columns.Bound(p => p.PrimaryInsuranceName).Title("Primary Insurance").Sortable(false);
            columns.Bound(p => p.Branch).Title("Branch").Sortable(false);
            columns.Bound(p => p.CreatedDateFormatted).Title("Date Added").Width(100).Sortable(false);
            columns.Bound(p => p.Id).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPatient('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowAdmitPatientModal('<#=Id#>', '<#=Type#>');\" class=\"\">Admit</a> | <a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowNonAdmitPatientModal('<#=Id#>');\" class=\"\">Non-Admit</a>").Title("Action").Width(180);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("PendingList", "Patient"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_PatientPending_Grid .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ManagePatients)) { %>.append(
        $("<div/>").addClass("float_left").Buttons([ { Text: "New Patient", Click: UserInterface.ShowNewPatient } ])
    )<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>