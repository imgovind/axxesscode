﻿<div class="wrapper main">
    <div>
        <h3>Patient saved successfully.</h3>
        What would you like to do next?
    </div>
    <ul id="NextStep_Options"></ul>
</div>
<script type="text/javascript">
    $("#window_ModalWindow").addClass("modal-gray");
    $("#NextStep_Options").append(
        $("<li/>").append(
            $("<span/>", { "class": "img icon greenarrow" })
        ).append(
            $("<a/>", { "href": "javascript:void(0);", "text": "Add Another Patient" }).click(function() {
                UserInterface.CloseModal();
                UserInterface.CloseWindow('newpatient');
                UserInterface.ShowNewPatient()
            })
        )
    ).append(
        $("<li/>").append(
            $("<span/>", { "class": "img icon greenarrow" })
        ).append(
            $("<a/>", { "href": "javascript:void(0);", "text": "Go to Patient Charts" }).click(function() {
                UserInterface.CloseModal();
                UserInterface.CloseWindow('newpatient');
                UserInterface.ShowPatientCenter()
            })
        )
    ).append(
        $("<li/>").append(
            $("<span/>", { "class": "img icon greenarrow" })
        ).append(
            $("<a/>", { "href": "javascript:void(0);", "text": "Close Window" }).click(function() {
                UserInterface.CloseModal();
                UserInterface.CloseWindow('newpatient')
            })
        )
    );
</script>
