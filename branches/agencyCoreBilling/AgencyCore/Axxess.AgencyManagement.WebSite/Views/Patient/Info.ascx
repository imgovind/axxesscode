﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<div class="winmenu">
    <ul>
        <% if (Current.HasRight(Permissions.ManagePhysicianOrders)) { %><li><a href="javascript:void(0);" onclick="Patient.LoadNewOrder('<%=Model.Id %>');">New Order</a></li><% } %>
        <li><a href="javascript:void(0);" onclick="Patient.LoadNewCommunicationNote('<%=Model.Id %>');">New Communication Note</a></li>
        <% if (Current.HasRight(Permissions.ManageInsurance)) { %><li><a href="javascript:void(0);" onclick="Patient.LoadNewAuthorization('<%=Model.Id %>');">New Authorization</a></li><% } %>
        <% if (Current.HasRight(Permissions.ScheduleVisits)) { %><li><a href="javascript:void(0);" onclick="UserInterface.ShowMultipleReassignModal('<%= Guid.Empty %>', '<%= Model.Id %>','Patient');" title="Reassign Schedules">Reassign Schedules</a></li><%}%>
    </ul>
</div>
<div class="twothirds">
    <div class="float_left"><% if (!Model.PhotoId.IsEmpty()) { %><img src="/Asset/<%= Model.PhotoId %>" alt="User Photo" /><% } else { %><img src="/Images/blank_user.jpeg" alt="User Photo" /><% } %><div class="clear"></div><% if (Current.HasRight(Permissions.ManagePatients)) { %><div class="align_center">[ <a href="javascript:void(0);" onclick="UserInterface.ShowNewPhotoModal('<%= Model.Id%>');">Change Photo</a> ] </div><% } %></div>
    <div class="column"><span class="bigtext"><%= Model.DisplayName  %></span><div class="buttons float_right"><ul><li><a href="javascript:void(0);" onclick="Patient.loadInfoAndActivity('<%= Model.Id %>');">Refresh</a></li><% if (Current.HasRight(Permissions.ManagePatients)) { %><li><a href="javascript:void(0);" onclick="UserInterface.ShowChangeStatusModal('<%= Model.Id%>');">Change Status</a></li><% } %></ul></div></div>
    <div class="shortrow"><label class="float_left">MR #:</label><span><%= Model.PatientIdNumber%></span><% if (Current.HasRight(Permissions.ManagePatients) && Model.IsDischarged) { %><div class="buttons float_right"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowReadmitPatientModal('<%= Model.Id%>');">Re-Admit</a></li></ul></div><% } %></div>
    <div class="shortrow"><label class="float_left">Birthday:</label><span><%= Model.DOB.ToString("MMMM dd, yyyy")%></span></div>
    <div class="shortrow"><label class="float_left">Start of Care Date:</label><span><%= Model.StartOfCareDateFormatted %></span></div>
    <% if (Model.PhoneHome.IsNotNullOrEmpty()) { %><div class="shortrow"><label class="float_left">Primary Phone:</label><span><%= Model.PhoneHome.ToPhone() %></span></div><% } %>
    <% var physician = Model.PhysicianContacts != null && Model.PhysicianContacts.Count > 0 ? Model.PhysicianContacts.SingleOrDefault(p => p.Primary) : null; %>
    <% if (physician != null) { %><div class="shortrow"><label class="float_left">Physician Name:</label><span><%= physician != null ? physician.DisplayName.Trim() : string.Empty %></span></div><% } %>
    <div class="shortrow">
        <% if (Current.HasRight(Permissions.ManagePatients)) { %>[ <a href="javascript:void(0);" onclick="UserInterface.ShowEditPatient('<%= Model.Id%>'); ">Edit</a> ]&#160;<% } %>
        [ <a href="javascript:void(0);" onclick="Patient.InfoPopup($(this));">More</a> ]&#160;
        [ <a href="http://<%= Request.ServerVariables["HTTP_HOST"] %>/Map/Google/<%= Model.Id %>" target="_blank">Directions</a> ]&#160;
    </div>
</div>
<div class="onethird encapsulation">
    <h4>Quick Reports<span class="mobile_hidden"> For This Patient</span></h4>
    <ul>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="Acore.OpenPrintView({ Url: '/Patient/Profile/<%=Model.Id %>', PdfUrl: '/Patient/PatientProfilePdf', PdfData: { 'id': '<%=Model.Id %>' } })">Patient Profile</a></li>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="Patient.loadMedicationProfile('<%= Model.Id%>');">Medication Profile(s)</a></li>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="UserInterface.ShowPatientAllergies('<%= Model.Id%>');">Allergy Profile</a></li>
        <% if (Current.HasRight(Permissions.ManageInsurance)) { %><li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="UserInterface.ShowPatientAuthorizations('<%= Model.Id%>');">Authorizations Listing</a></li><% } %>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="Patient.loadPatientCommunicationNotes('<%= Model.Id%>');">Communication Notes</a></li>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="UserInterface.ShowPatientOrdersHistory('<%= Model.Id%>');">Orders And Care Plans</a></li>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="UserInterface.ShowPatientSixtyDaySummary('<%= Model.Id%>');">60 Day Summaries</a></li>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="UserInterface.ShowPatientVitalSigns('<%= Model.Id%>');">Vital Signs Log</a></li>
        <li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="Acore.OpenPrintView({ Url: '/Patient/TriageClassification/<%=Model.Id %>' });">Triage Classification</a></li>
        <% if (Current.HasRight(Permissions.DeleteTasks)) { %><li><a href="javascript:void(0);" onmouseover="$(this).addClass('t-state-hover');" onmouseout="$(this).removeClass('t-state-hover');" onclick="UserInterface.ShowDeletedTaskHistory('<%= Model.Id%>');">Deleted Tasks/Doc<span class="mobile_hidden">ument</span>s</a></li><% } %>
    </ul>
</div>
<div class="float_left">
    <% if (Current.HasRight(Permissions.ScheduleVisits)) { %><div class="buttons float_left"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowScheduleCenter('<%= Model.Id %>');">Schedule Activity</a></li></ul></div><% } %>
    <fieldset class="notelegend float_left"><ul><li><span class="img icon note"></span>Visit Comments</li><li><span class="img icon note_blue"></span>Episode Comments</li><li><span class="img icon note_red"></span>Missed/Returned</li></ul></fieldset>
</div>
<div id="discharge_patient_Container" class="dischargepatientmodal hidden"></div>
<div id="activate_patient_Container" class="dischargepatientmodal hidden"></div>
<div id="reassign_Patient_Container" class="reassignpatientmodal hidden"></div>
<div id="readmit_Patient_Container" class="readmitpatientmodal hidden"></div>
