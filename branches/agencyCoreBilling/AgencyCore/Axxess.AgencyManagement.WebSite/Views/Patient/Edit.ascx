﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<% using (Html.BeginForm("Edit", "Patient", FormMethod.Post, new { @id = "editPatientForm" })) { %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "editpatient",
        "Edit Patient",
        Model != null ? (Model.LastName + ", " + Model.FirstName + " " + Model.MiddleInitial).ToTitleCase() : "")%>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Patient_Id" })%>     
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>  
        <legend>Patient Demographics</legend>
        <div class="column">
            <div class="row"><label for="Edit_Patient_FirstName" class="float_left"><span class="green">(M0040)</span> First Name:</label><div class="float_right"><%= Html.TextBox("FirstName", Model.FirstName, new { @id = "Edit_Patient_FirstName", @class = "text  input_wrapper required", @maxlength = "100" })%></div></div>
            <div class="row"><label for="Edit_Patient_MiddleInitial" class="float_left">MI:</label><div class="float_right"><%= Html.TextBox("MiddleInitial",(Model.MiddleInitial), new { @id = "Edit_Patient_MiddleInitial", @class = "text input_wrapper", @style = "width:20px;", @maxlength="1" })%></div></div>
            <div class="row"><label for="Edit_Patient_LastName" class="float_left">Last Name:</label><div class="float_right"><%= Html.TextBox("LastName", Model.LastName, new { @id = "Edit_Patient_LastName", @class = "text input_wrapper required", @maxlength = "100" })%></div></div>
            <div class="row"><label class="float_left"><span class="green">(M0069)</span> Gender:</label><div class="float_right"><%= Html.RadioButton("Gender", "Female", Model.Gender == "Female" ? true : false, new { @id = "", @class = "required radio" })%><label for="" class="inlineradio">Female</label><%= Html.RadioButton("Gender", "Male", Model.Gender == "Male" ? true : false, new { @id = "", @class = "required radio" })%><label for="" class="inlineradio">Male</label></div></div>
            <div class="row"><label for="Edit_Patient_DOB" class="float_left"><span class="green">(M0066)</span> Date of Birth:</label><div class="float_right"><input type="date" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" id="Edit_Patient_DOB" class="required" /></div></div>
            <div class="row"><label for="Edit_Patient_MaritalStatus" class="float_left">Marital Status:</label><div class="float_right"><%var maritalStatus = new SelectList(new[] { new SelectListItem { Text = "** Select **", Value = "0" }, new SelectListItem { Text = "Married", Value = "Married" }, new SelectListItem { Text = "Divorce", Value = "Divorce" }, new SelectListItem { Text = "Widowed", Value = "Widowed"}, new SelectListItem { Text = "Single", Value = "Single" } }, "Value", "Text", Model.MaritalStatus); %><%= Html.DropDownList("MaritalStatus", maritalStatus, new { @id = "Edit_Patient_MaritalStatus", @class = "input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Patient_LocationId" class="float_left">Agency Branch:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.AgencyLocationId.ToString(), new { @id = "Edit_Patient_LocationId", @class = "BranchLocation" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Patient_PatientID" class="float_left"><span class="green">(M0020)</span> Patient ID:</label><div class="float_right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @id = "Edit_Patient_PatientID", @class = "text required input_wrapper", @maxlength = "15" })%></div></div>
            <div class="row"><label for="Edit_Patient_MedicareNumber" class="float_left"><span class="green">(M0063)</span> Medicare Number:</label><div class="float_right"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @id = "Edit_Patient_MedicareNumber", @class = "text input_wrapper", @maxlength = "11" })%></div></div>
            <div class="row"><label for="Edit_Patient_MedicaidNumber" class="float_left"><span class="green">(M0065)</span> Medicaid Number:</label><div class="float_right"><%= Html.TextBox("MedicaidNumber", Model.MedicaidNumber, new { @id = "Edit_Patient_MedicaidNumber", @class = "text input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="Edit_Patient_SSN" class="float_left"><span class="green">(M0064)</span> SSN:</label><div class="float_right"><%= Html.TextBox("SSN", Model.SSN, new { @id = "Edit_Patient_SSN", @class = "text digits input_wrapper", @maxlength = "9" })%></div></div>
            <div class="row"><label for="Edit_Patient_StartOfCareDate" class="float_left"><span class="green">(M0030)</span> Start of Care Date:</label><div class="float_right"><input type="date" name="StartOfCareDate" value="<%= !Model.StartofCareDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.StartofCareDate.ToString("MM/dd/yyyy") : string.Empty %>" id="Edit_Patient_StartOfCareDate" class="required" /></div></div>
            <div class="row"><label for="Edit_Patient_CaseManager" class="float_left">Case Manager:</label><div class="float_right"><%= Html.CaseManagers("CaseManagerId", Model.CaseManagerId.ToString(), new { @id = "Edit_Patient_CaseManager", @class = "Users requireddropdown" })%></div></div>
            <div class="row"><label for="Edit_Patient_Assign" class="float_left">Clinician:</label><div class="float_right"><%= Html.Clinicians("UserId",  Model.UserId.ToString(), new { @id = "Edit_Patient_Assign", @class = "Employees input_wrapper requireddropdown" })%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0140)</span> Race/Ethnicity</legend>
        <table class="form">
            <tbody>
                <%string[] ethnicities = Model.Ethnicities != null && Model.Ethnicities != "" ? Model.Ethnicities.Split(';') : null;  %>
                <tr>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces0' type='checkbox' value='0' name='EthnicRaces' class='required radio float_left' {0} />", ethnicities != null && ethnicities.Contains("0") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces0" class="radio">American Indian or Alaska Native</label></td>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces1' type='checkbox' value='1' name='EthnicRaces' class='required radio float_left' {0} />", ethnicities != null && ethnicities.Contains("1") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces1" class="radio">Asian</label></td>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces2' type='checkbox' value='2' name='EthnicRaces' class='required radio float_left' {0} />", ethnicities != null && ethnicities.Contains("2") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces2" class="radio">Black or African-American</label></td>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces3' type='checkbox' value='3' name='EthnicRaces' class='required radio float_left' {0} />", ethnicities != null && ethnicities.Contains("3") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces3" class="radio">Hispanic or Latino</label></td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces4' type='checkbox' value='4' name='EthnicRaces' class='required radio float_left' {0} />", ethnicities != null && ethnicities.Contains("4") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces4" class="radio">Native Hawaiian or Pacific Islander</label></td>
                    <td><%= string.Format("<input id='EditPatient_EthnicRaces5' type='checkbox' value='5' name='EthnicRaces' class='required radio float_left' {0} />", ethnicities != null && ethnicities.Contains("5") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces5" class="radio">White</label></td>
                    <td colspan="2"><%= string.Format("<input id='EditPatient_EthnicRaces6' type='checkbox' value='6' name='EthnicRaces' class='required radio float_left' {0} />", ethnicities != null && ethnicities.Contains("6") ? "checked='checked'" : "")%><label for="EditPatient_EthnicRaces6" class="radio">Unknown</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend><span class="green">(M0150)</span> Payment Source <span class="light">(Mark all that apply)</span></legend>
        <table class="form">
            <tbody>
                <%string[] paymentSources = Model.PaymentSource != null && Model.PaymentSource != "" ? Model.PaymentSource.Split(';') : null;  %>
                <tr>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceNone' type='checkbox' value='0' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("0") ? "checked='checked'" : "" )%><label for="Edit_Patient_PaymentSourceNone" class="radio">None; no charge for current services</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceMedicare' type='checkbox' value='1' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("1") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceMedicare" class="radio">Medicare (traditional fee-for-service)</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceMedicareHmo' type='checkbox' value='2' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("2") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceMedicareHmo" class="radio">Medicare (HMO/ managed care)</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceMedicaid' type='checkbox' value='3' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("3") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceMedicaid" class="radio">Medicaid (traditional fee-for-service)</label></td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceMedicaidHmo' type='checkbox' value='4' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("4") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceMedicaidHmo" class="radio">Medicaid (HMO/ managed care)</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceWorkers' type='checkbox' value='5' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("5") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceWorkers" class="radio">Workers&#8217; compensation</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceTitleProgram' type='checkbox' value='6' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("6") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceTitleProgram" class="radio">Title programs (e.g., Titile III,V, or XX)</label></td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceOtherGovernment' type='checkbox' value='7' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("7") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceOtherGovernment" class="radio">Other government (e.g.,CHAMPUS,VA,etc)</td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourcePrivate' type='checkbox' value='8' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("8") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourcePrivate" class="radio">Private insurance</td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourcePrivateHmo' type='checkbox' value='9' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("9") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourcePrivateHmo" class="radio">Private HMO/ managed care</td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceSelf' type='checkbox' value='10' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("10") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceSelf" class="radio">Self-pay</td>
                    <td><%= string.Format("<input id='Edit_Patient_PaymentSourceUnknown' type='checkbox' value='11' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("11") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSourceUnknown" class="radio">Unknown</td>
                </tr>
                <tr>
                    <td colspan='4'><%= string.Format("<input id='Edit_Patient_PaymentSource' type='checkbox' value='12' name='PaymentSources' class='required radio float_left' {0} />", paymentSources != null && paymentSources.Contains("12") ? "checked='checked'" : "")%><label for="Edit_Patient_PaymentSource" class="radio more">Other (specify)</label><%= Html.TextBox("OtherPaymentSource", Model.OtherPaymentSource, new { @id = "Edit_Patient_OtherPaymentSource", @class = "text", @style = "display:none;" })%></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Patient Address</legend>
        <div class="marginBreak">
            <div class="column">
                <div class="row"><label for="Edit_Patient_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%= Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Patient_AddressLine1", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Patient_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%= Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Patient_AddressLine2", @class = "text input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Patient_AddressCity" class="float_left">City:</label><div class="float_right"><%= Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Patient_AddressCity", @class = "text required input_wrapper", @maxlength = "50" })%></div></div>
                <div class="row"><label for="Edit_Patient_AddressStateCode" class="float_left"><span class="green">(M0050)</span> State, <span class="green">(M0060)</span> Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Patient_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Patient_AddressZipCode", @class = "text required digits isValidUSZip zip", @size = "5", @maxlength = "5" })%></div></div>
            </div>
            <div class="column">
                <div class="row"><label class="float_left">Home Phone:</label><div class="float_right"><span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >=3 ? Model.PhoneHome.Substring(0, 3) : "", new { @id = "Edit_Patient_HomePhone1", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >=6  ? Model.PhoneHome.Substring(3, 3) : "", new { @id = "Edit_Patient_HomePhone2", @class = "autotext required digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneHomeArray", Model.PhoneHome.IsNotNullOrEmpty() && Model.PhoneHome.Length >= 10  ? Model.PhoneHome.Substring(6, 4) : "", new { @id = "Edit_Patient_HomePhone3", @class = "autotext required digits phone_long", @maxlength = "4", @size = "5" })%></span></div></div>
                <div class="row"><label class="float_left">Mobile Phone:</label><div class="float_right"><span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 3 ? Model.PhoneMobile.Substring(0, 3) : "", new { @id = "Edit_Patient_MobilePhone1", @class = "autotext digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 6 ? Model.PhoneMobile.Substring(3, 3) : "", new { @id = "Edit_Patient_MobilePhone2", @class = "autotext digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PhoneMobileArray", Model.PhoneMobile.IsNotNullOrEmpty() && Model.PhoneMobile.Length >= 10 ? Model.PhoneMobile.Substring(6, 4) : "", new { @id = "Edit_Patient_MobilePhone3", @class = "autotext digits phone_long", @maxlength = "4", @size = "5" })%></span></div></div>
                <div class="row"><label for="Edit_Patient_Email" class="float_left">Email:</label><div class="float_right"><%= Html.TextBox("Email", Model.EmailAddress, new { @id = "Edit_Patient_Email", @class = "text input_wrapper", @maxlength = "50" })%></div></div>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Pharmacy</legend>
        <div class="halfRow">
            <div class="marginBreak">
                <div class="column"><div class="row"><label for="Edit_Patient_PharmacyName" class="float_left">Name:</label><div class="float_right"><%= Html.TextBox("PharmacyName", Model.PharmacyName, new { @id = "Edit_Patient_PharmacyName", @class = "text input_wrapper", @maxlength = "100" })%></div></div></div>
                <div class="column"><div class="row"><label for="Edit_Patient_PharmacyPhone1" class="float_left">Phone:</label><div class="float_right"><span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 3 ? Model.PharmacyPhone.Substring(0, 3) : "", new { @id = "Edit_Patient_PharmacyPhone1", @class = "autotext digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 6 ? Model.PharmacyPhone.Substring(3, 3) : "", new { @id = "Edit_Patient_PharmacyPhone2", @class = "autotext digits phone_short", @maxlength = "3", @size = "3" })%></span>- <span class="input_wrappermultible"><%= Html.TextBox("PharmacyPhoneArray", Model.PharmacyPhone.IsNotNullOrEmpty() && Model.PharmacyPhone.Length >= 10 ? Model.PharmacyPhone.Substring(6, 4) : "", new { @id = "Edit_Patient_PharmacyPhone3", @class = "autotext digits phone_long", @maxlength = "4", @size = "5" })%></span></div></div></div>
            </div>
        </div>
    </fieldset>
    <fieldset class="half float_left">
        <legend>Insurance / Payor Information</legend>
        <div class="column">
            <div class="row"><% var htmlAttributesPrimary = new Dictionary<string, string>(); htmlAttributesPrimary.Add("id", "Edit_Patient_PrimaryInsurance"); htmlAttributesPrimary.Add("class", "Insurances requireddropdown");%><label for="Edit_Patient_PrimaryInsurance" class="float_left">Primary:</label><div class="float_right"><%= Html.Insurances("PrimaryInsurance", Model.PrimaryInsurance, true, htmlAttributesPrimary)%></div><div class="row hidden" id="Edit_Patient_PrimaryHealthPlanIdContent"><label for="Edit_Patient_PrimaryHealthPlanId" class="float_left">Primary Insurance Health Plan Id:</label><div class="float_right"><%= Html.TextBox("PrimaryHealthPlanId", Model.PrimaryHealthPlanId, new { @id = "Edit_Patient_PrimaryHealthPlanId", @class = "" })%></div></div></div>
            <div class="row"><% var htmlAttributesSecondary = new Dictionary<string, string>(); htmlAttributesSecondary.Add("id", "Edit_Patient_SecondaryInsurance"); htmlAttributesSecondary.Add("class", "Insurances"); %><label for="Edit_Patient_SecondaryInsurance" class="float_left">Secondary:</label><div class="float_right"><%= Html.Insurances("SecondaryInsurance", Model.SecondaryInsurance, true, htmlAttributesSecondary)%></div><div class="row hidden" id="Edit_Patient_SecondaryHealthPlanIdContent"><label for="Edit_Patient_SecondaryHealthPlanId" class="float_left">Secondary Insurance Health Plan Id:</label><div class="float_right"><%= Html.TextBox("SecondaryHealthPlanId", Model.SecondaryHealthPlanId, new { @id = "Edit_Patient_SecondaryHealthPlanId", @class = "" })%></div></div></div>
            <div class="row"><% var htmlAttributesTertiary = new Dictionary<string, string>(); htmlAttributesTertiary.Add("id", "Edit_Patient_TertiaryInsurance"); htmlAttributesTertiary.Add("class", "Insurances"); %><label for="Edit_Patient_TertiaryInsurance" class="float_left">Tertiary:</label><div class="float_right"><%= Html.Insurances("TertiaryInsurance", Model.TertiaryInsurance, true, htmlAttributesTertiary)%></div><div class="row hidden" id="Edit_Patient_TertiaryHealthPlanIdContent"><label for="Edit_Patient_TertiaryHealthPlanId" class="float_left">Tertiary Insurance Health Plan Id:</label><div class="float_right"><%= Html.TextBox("TertiaryHealthPlanId", Model.TertiaryHealthPlanId, new { @id = "Edit_Patient_TertiaryHealthPlanId", @class = "" })%></div></div></div>
        </div>
    </fieldset>
    <fieldset class="half float_right">
        <legend>Emergency Triage <span class="light">(Select one)</span></legend>
        <div class="column">
            <div class="row"><%= Html.RadioButton("Triage", "1", Model.Triage == 1 ? true : false, new { @id = "Edit_Patient_Triage1", @class = "required radio Triage float_left" })%><div class="float_left">&#160;1.&#160;</div><label class="normal margin" for="Edit_Patient_Triage1">Life threatening (or potential) and requires ongoing medical treatment. When necessary,appropriate arrangements for evacuation to an acute care facility will be made.</label></div>
            <div class="row"><%= Html.RadioButton("Triage", "2", Model.Triage == 2 ? true : false, new { @id = "Edit_Patient_Triage2", @class = "required radio Triage float_left" })%><div class="float_left">&#160;2.&#160;</div><label class="normal margin" for="Edit_Patient_Triage2">Not life threatening but would suffer severe adverse effects from interruption of services (i.e., daily insulin, IV medications, sterile wound care of a wound with a large amount of drainage.)</label></div>
            <div class="row"><%= Html.RadioButton("Triage", "3", Model.Triage == 3 ? true : false, new { @id = "Edit_Patient_Triage3", @class = "required radio Triage float_left" })%><div class="float_left">&#160;3.&#160;</div><label class="normal margin" for="Edit_Patient_Triage3">Visits could be postponed 24-48 hours without adverse effects (i.e., new insulin dependent diabetic able to self inject, sterile wound care with a minimal amount to no drainage)</label></div>
            <div class="row"><%= Html.RadioButton("Triage", "4", Model.Triage == 4 ? true : false, new { @id = "Edit_Patient_Triage4", @class = "required radio Triage float_left" })%><div class="float_left">&#160;4.&#160;</div><label class="normal margin" for="Edit_Patient_Triage4"> Visits could be postponed 72-96 hours without adverse effects (i.e., post op withno open wound, anticipated discharge within the next 10-14 days, routine catheter changes)</label></div>
        </div>
    </fieldset>
    <div class="clear"></div>
    <fieldset>
        <legend>Services Required <span class="light">(Optional)</span></legend>
        <table class="form">
            <tbody><%string[] servicesRequired = Model.ServicesRequired != null && Model.ServicesRequired != "" ? Model.ServicesRequired.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="ServicesRequiredCollection" />
                <tr>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection0' type='checkbox' value='0' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("0") ? "checked='checked'" : "" )%><label for="ServicesRequiredCollection0" class="radio">SNV</label></td>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection1' type='checkbox' value='1' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("1") ? "checked='checked'" : "")%><label for="ServicesRequiredCollection1" class="radio">HHA</label></td>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection2' type='checkbox' value='2' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("2") ? "checked='checked'" : "")%><label for="ServicesRequiredCollection2" class="radio">PT</label></td>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection3' type='checkbox' value='3' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("3") ? "checked='checked'" : "")%><label for="ServicesRequiredCollection3" class="radio">OT</label></td>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection4' type='checkbox' value='4' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("4") ? "checked='checked'" : "")%><label for="ServicesRequiredCollection4" class="radio">ST</label></td>
                    <td><%= string.Format("<input id ='ServicesRequiredCollection5' type='checkbox' value='5' class='radio float_left' name='ServicesRequiredCollection' {0} />", servicesRequired != null && servicesRequired.Contains("5") ? "checked='checked'" : "")%><label for="ServicesRequiredCollection5" class="radio">MSW</label></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>DME Needed <span class="light">(Optional)</span></legend>
        <table class="form"><%string[] DME = Model.DME != null && Model.DME != "" ? Model.DME.Split(';') : null;  %><input type="hidden" value=" " class="radio" name="DMECollection" />
            <tbody>
                <tr class="firstrow">
                    <td><%= string.Format("<input id='DMECollection0' type='checkbox' value='0' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("0") ? "checked='checked'" : "")%><label for="DMECollection0" class="radio">Bedside Commode</label></td>
                    <td><%= string.Format("<input id='DMECollection1' type='checkbox' value='1' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("1") ? "checked='checked'" : "")%><label for="DMECollection1" class="radio">Cane</label></td>
                    <td><%= string.Format("<input id='DMECollection2' type='checkbox' value='2' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("2") ? "checked='checked'" : "")%><label for="DMECollection2" class="radio">Elevated Toilet Seat</label></td>
                    <td><%= string.Format("<input id='DMECollection3' type='checkbox' value='3' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("3") ? "checked='checked'" : "")%><label for="DMECollection3" class="radio">Grab Bars</label></td>
                    <td><%= string.Format("<input id='DMECollection4' type='checkbox' value='4' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("4") ? "checked='checked'" : "")%><label for="DMECollection4" class="radio">Hospital Bed</label></td>
                </tr>
                <tr>
                    <td><%= string.Format("<input id='DMECollection5' type='checkbox' value='5' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("5") ? "checked='checked'" : "")%><label for="DMECollection5" class="radio">Nebulizer</label></td>
                    <td><%= string.Format("<input id='DMECollection6' type='checkbox' value='6' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("6") ? "checked='checked'" : "")%><label for="DMECollection6" class="radio">Oxygen</label></td>
                    <td><%= string.Format("<input id='DMECollection7' type='checkbox' value='7' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("7") ? "checked='checked'" : "")%><label for="DMECollection7" class="radio">Tub/Shower Bench</label></td>
                    <td><%= string.Format("<input id='DMECollection8' type='checkbox' value='8' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("8") ? "checked='checked'" : "")%><label for="DMECollection8" class="radio">Walker</label></td>
                    <td><%= string.Format("<input id='DMECollection9' type='checkbox' value='9' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("9") ? "checked='checked'" : "")%><label for="DMECollection9" class="radio">Wheelchair</label></td>
                </tr>
                <tr>
                    <td colspan="5"><%= string.Format("<input id='DMECollection10' type='checkbox' value='10' class='radio float_left' name='DMECollection' {0} />", DME != null && DME.Contains("10") ? "checked='checked'" : "")%><label for="DMECollection10" class="radio">Other</label><%= Html.TextBox("OtherDME", Model.OtherDME, new { @id = "Edit_Patient_OtherDME", @class = "text", @style = "display:none;" })%></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
    <fieldset>
        <legend>Referral Source</legend>
        <div class="column">
            <div class="row">
                <label for="New_Patient_ReferralPhysician" class="float_left">Physician:</label>
                <div class="float_right"><%= Html.TextBox("ReferrerPhysician",Model.ReferrerPhysician.ToString(), new { @id = "Edit_Patient_ReferrerPhysician", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float_right ancillary_button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
            <div class="row"><label for="New_Patient_AdmissionSource" class="float_left">Admission Source:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", Model.AdmissionSource, new { @id = "Edit_Patient_AdmissionSource", @class = "AdmissionSource" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Patient_OtherReferralSource" class="float_left">Other Referral Source:</label><div class="float_right"><%= Html.TextBox("OtherReferralSource", Model.OtherReferralSource, new { @id = "Edit_Patient_OtherReferralSource", @class = "text input_wrapper", @maxlength = "30" })%></div></div>
            <div class="row"><label for="New_Patient_PatientReferralDate" class="float_left">Referral Date:</label><div class="float_right"><input type="date" name="ReferralDate" value="<%= !Model.ReferralDate.ToString("MM/dd/yyyy").IsEqual("01/01/0001") ? Model.ReferralDate.ToString("MM/dd/yyyy") : string.Empty %>" id="Edit_Patient_PatientReferralDate" /></div></div>
            <div class="row"><label for="New_Patient_InternalReferral" class="float_left">Internal Referral:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Users, "InternalReferral", Model.InternalReferral.ToString(), new { @id = "Edit_Patient_InternalReferral", @class = "Users valid" })%></div></div>
        </div>
    </fieldset>
    <fieldset class="medication">
        <legend>Emergency Contact</legend>
        <div class="wide_column"><div class="row"><div class="buttons float_left"><ul><li><a href="javascript:void(0);" onclick="Patient.loadNewEmergencyContact('<%=Model.Id %>');">Add New Emergency Contact</a></li></ul></div></div></div>
        <div class="clear"></div>
        <%= Html.Telerik().Grid<PatientEmergencyContact>().Name("Edit_patient_EmergencyContact_Grid").Columns(columns => {
                columns.Bound(c => c.FirstName);
                columns.Bound(c => c.LastName);
                columns.Bound(c => c.PrimaryPhoneFormatted);
                columns.Bound(c => c.Relationship);
                columns.Bound(c => c.EmailAddress); 
                columns.Bound(c => c.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\" Patient.loadEditEmergencyContact('" + Model.Id + "','<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.DeleteEmergencyContact('<#=Id#>','" + Model.Id + "');\" class=\"deleteReferral\">Delete</a>").Title("Action").Width(100);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetEmergencyContacts", "Patient", new { PatientId = Model.Id })).Sortable().Footer(false) %>
    </fieldset>
    <fieldset class="medication">
        <legend>Physicians</legend>
        <div class="wide_column">
            <div class="row">
                <div class="float_left"><%= Html.TextBox("AgencyPhysicians", "", new { @id = "EditPatient_PhysicianSelector", @class = "Physicians" })%></div>
                <div class="buttons float_left">
                    <ul>
                        <li>
                            <a id="EditPatient_NewPhysician" href="javascript:void(0);">Add Selected Physician</a>
                        </li>
                    </ul>
                </div>
                <div class="float_right ancillary_button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>
        </div>
        <div class="clear"></div>
        <%= Html.Telerik().Grid<AgencyPhysician>().Name("EditPatient_PhysicianGrid").Columns(columns => {
                columns.Bound(c => c.FirstName);
                columns.Bound(c => c.LastName);
                columns.Bound(c => c.PhoneWorkFormatted).Title("Work Phone");
                columns.Bound(c => c.FaxNumberFormatted);
                columns.Bound(c => c.EmailAddress);
                columns.Bound(c => c.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Patient.DeletePhysician('<#=Id#>','" + Model.Id + "');\" class=\"deleteReferral\">Delete</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.SetPrimaryPhysician('<#=Id #>','" + Model.Id + "');\" class=<#= !Primary ? \"\" : \"hidden\" #>><#=Primary ? \"\" : \"Make Primary\" #></a>").Title("Action").Width(135);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("GetPhysicians", "Patient", new { PatientId = Model.Id })).Sortable().Footer(false) %>
    </fieldset>
    <fieldset><legend>Comments</legend><div class="wide_column"><div class="row"><textarea id="Edit_Patient_Comments" name="Comments" cols="5" rows="6"><%= Model.Comments %></textarea></div></div></fieldset>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li><li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editpatient');">Cancel</a></li></ul></div>
</div>
<% } %>
<script type="text/javascript">
    $("#window_editpatient select.Insurances").change(function() { var selectList = this; if ($(this).val() == "new") { Acore.Open('newinsurance'); selectList.selectedIndex = 0; } else if ($(this).val() == "spacer") { selectList.selectedIndex = 0; } });

    if ($("#Edit_Patient_PrimaryInsurance").find(":selected").attr("ishmo") == "1") { $("#Edit_Patient_PrimaryHealthPlanIdContent").show(); $("#Edit_Patient_PrimaryHealthPlanId").addClass("required"); } else { $("#Edit_Patient_PrimaryHealthPlanIdContent").hide(); $("#Edit_Patient_PrimaryHealthPlanId").removeClass("required error"); } 
    if ($("#Edit_Patient_SecondaryInsurance").find(":selected").attr("ishmo") == "1") { $("#Edit_Patient_SecondaryHealthPlanIdContent").show(); $("#Edit_Patient_SecondaryHealthPlanId").addClass("required"); } else { $("#Edit_Patient_SecondaryHealthPlanIdContent").hide(); $("#Edit_Patient_SecondaryHealthPlanId").removeClass("required error"); } 
    if ($("#Edit_Patient_TertiaryInsurance").find(":selected").attr("ishmo") == "1") { $("#Edit_Patient_TertiaryHealthPlanIdContent").show(); $("#Edit_Patient_TertiaryHealthPlanId").addClass("required"); } else { $("#Edit_Patient_TertiaryHealthPlanIdContent").hide(); $("#Edit_Patient_TertiaryHealthPlanId").removeClass("required error"); }
    
    $("#Edit_Patient_PrimaryInsurance").change(function() { if ($(this).find(":selected").attr("ishmo") == "1") { $("#Edit_Patient_PrimaryHealthPlanIdContent").show(); $("#Edit_Patient_PrimaryHealthPlanId").addClass("required"); } else { $("#Edit_Patient_PrimaryHealthPlanIdContent").hide(); $("#Edit_Patient_PrimaryHealthPlanId").removeClass("required error"); } });
    $("#Edit_Patient_SecondaryInsurance").change(function() { if ($(this).find(":selected").attr("ishmo") == "1") { $("#Edit_Patient_SecondaryHealthPlanIdContent").show(); $("#Edit_Patient_SecondaryHealthPlanId").addClass("required"); } else { $("#Edit_Patient_SecondaryHealthPlanIdContent").hide(); $("#Edit_Patient_SecondaryHealthPlanId").removeClass("required error"); } });
    $("#Edit_Patient_TertiaryInsurance").change(function() { if ($(this).find(":selected").attr("ishmo") == "1") { $("#Edit_Patient_TertiaryHealthPlanIdContent").show(); $("#Edit_Patient_TertiaryHealthPlanId").addClass("required"); } else { $("#Edit_Patient_TertiaryHealthPlanIdContent").hide(); $("#Edit_Patient_TertiaryHealthPlanId").removeClass("required error"); } });
   
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row div.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
    $("#Edit_Patient_SSN").mask("999999999", { placeholder: "" });
</script>