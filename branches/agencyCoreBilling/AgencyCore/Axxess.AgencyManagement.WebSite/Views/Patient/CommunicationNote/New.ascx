﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "newcommnote",
        "New Communication Note",
        Current.AgencyName)%>
<% using (Html.BeginForm("Add", "CommunicationNote", FormMethod.Post, new { @id = "newCommunicationNoteForm" })) { %>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Communication Note</th></tr>
            <tr><td colspan="2">
                <div><label for="New_CommunicationNote_PatientName" class="float_left width200">Patient Name:</label>
                <div class="float_left"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", (!Model.IsEmpty()) ? Model.ToString() : "", new { @id = "New_CommunicationNote_PatientName", @class="requireddropdown" })%></div></div>
                <div class="clear"></div>
                <div><label for="New_CommunicationNote_EpisodeList" class="float_left width200">Episode Associated:</label>
                <div class="float_left"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_CommunicationNote_EpisodeList", @class = "requireddropdown" })%></div></div>
                <div class="clear"></div>
                <div><label for="New_CommunicationNote_Date" class="float_left width200">Date:</label>
                <div class="float_left"><input type="date" name="Created" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="New_CommunicationNote_Date" class="required" /></div></div>
                <div class="clear"></div>
            </td><td colspan="2">
                <div><label for="New_CommunicationNote_PhysicianDropDown" class="float_left width200">Physician:</label>
                <div class="float_left"><%= Html.TextBox("PhysicianId", "", new { @id = "New_CommunicationNote_PhysicianDropDown", @class = "Physicians" })%></div></div>
                <div class="clear"></div>
                <div><label class="float_left width200">&nbsp;</label>
                <div class="float_left ancillary_button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div></div>
            </td></tr>
            <tr>
                <td colspan="4" class="align_left">
                    <div><label class="width200 float_left">Communication Text</label>
                    <div class="float_left"><%= Html.Templates("New_CommunicationNote_Templates", new { @class = "Templates mobile_fr", @template = "#New_CommunicationNote_Text" })%></div></div>
                    <div class="clear"></div>
                    <div class="align_center"><%= Html.TextArea("Text", string.Empty, 8, 20, new { @class = "fill", @id = "New_CommunicationNote_Text", @maxcharacters = "1500" })%><p class="charsRemaining"></p></div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div><label for="New_CommunicationNote_SendAsMessage" class="float_left width200">Send note as Message:</label>
                    <div class="float_left"><%= Html.CheckBox("SendAsMessage", false, new { @id = "New_CommunicationNote_SendAsMessage", @class = "bigradio" })%></div></div>
                    <div class="clear"></div>
                    <div id="New_CommunicationNote_Recipients"><%= Html.Recipients("New_CommunicationNote", new List<Guid>()) %></div>
                </td>
            </tr>
            <tr><th colspan="4">Electronic Signature</th></tr>
            <tr>
                <td colspan="4">
                    <div class="third"><label for="New_CommunicationNote_ClinicianSignature" class="float_left">Staff Signature:</label><div class="float_right"><%= Html.Password("SignatureText", "", new { @id = "New_CommunicationNote_ClinicianSignature" })%></div></div>
                    <div class="third"></div>
                    <div class="third"><label for="New_CommunicationNote_ClinicianSignatureDate" class="float_left">Signature Date:</label><div class="float_right"><input type="date" name="SignatureDate" id="New_CommunicationNote_ClinicianSignatureDate" /></div></div>
                </td>
            </tr>
        </tbody>
    </table>
    <%= Html.Hidden("Status", "", new { @id = "New_CommunicationNote_Status" })%>
    <div class="buttons"><ul>
         <li><a href="javascript:void(0);" onclick="NewCommNoteRemove();$('#New_CommunicationNote_Status').val('415');$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="NewCommNoteAdd();$('#New_CommunicationNote_Status').val('420');$(this).closest('form').submit();">Complete</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newcommnote');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $("#New_CommunicationNote_Recipients").hide();
    $("#New_CommunicationNote_SendAsMessage").click(function() { $("#New_CommunicationNote_Recipients").toggle(); });
    $("#New_CommunicationNote_PhysicianDropDown").PhysicianInput();
    Schedule.loadEpisodeDropDown('New_CommunicationNote_EpisodeList', $('#New_CommunicationNote_PatientName'));
    $('#New_CommunicationNote_PatientName').change(function() { Schedule.loadEpisodeDropDown('New_CommunicationNote_EpisodeList', $(this)); });
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    function NewCommNoteAdd() {
        $("#New_CommunicationNote_ClinicianSignature").removeClass('required').addClass('required');
        $("#New_CommunicationNote_ClinicianSignatureDate").removeClass('required').addClass('required');
    }
    function NewCommNoteRemove() {
        $("#New_CommunicationNote_ClinicianSignature").removeClass('required');
        $("#New_CommunicationNote_ClinicianSignatureDate").removeClass('required');
    }
</script>
<%} %>
