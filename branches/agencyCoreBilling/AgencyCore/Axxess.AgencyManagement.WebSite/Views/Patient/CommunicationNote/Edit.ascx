﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<CommunicationNote>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "editcommunicationnote",
        "Edit Communication Note",
        Current.AgencyName)%>
        
<% using (Html.BeginForm("Update", "CommunicationNote", FormMethod.Post, new { @id = "editCommunicationNoteForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_CommunicationNote_Id" })%>
<%= Html.Hidden("UserId", Model.UserId, new { @id = "Edit_CommunicationNote_UserId" })%>
<%= Html.Hidden("PatientId", Model.PatientId, new { @id = "Edit_CommunicationNote_PatientId" })%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Communication Note</th></tr>
            <tr><td colspan="2">
                <div><label for="Edit_CommunicationNote_PatientName" class="float_left width200">Patient Name:</label>
                <div class="float_left"><span id = "Edit_CommunicationNote_PatientName"><%= Model != null && Model.DisplayName.IsNotNullOrEmpty() ? Model.DisplayName : string.Empty %></span></div></div>
                <div class="clear"></div>
                <div><label for="Edit_CommunicationNote_EpisodeList" class="float_left width200">Episode Associated:</label><div class="float_left">
                <% if (Model.EpisodeId.IsEmpty()) { %>
                    <%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Model.PatientId, "-- Select Episode --", new { @id = "Edit_CommunicationNote_EpisodeList", @class = "requireddropdown" })%>
                <%}else{ %>
                    <span id="Edit_CommunicationNote_EpisodeList"><%=string.Format("{0} - {1}", Model.EpisodeStartDate, Model.EpisodeEndDate) %></span><%= Html.Hidden("EpisodeId", Model.EpisodeId, new { @id = "Edit_CommunicationNote_EpisodeId" })%>
                <%} %>
                </div></div>
                <div class="clear"></div>
                <div><label for="Edit_CommunicationNote_Date" class="float_left width200">Date:</label>
                <div class="float_left"><span id = "Edit_CommunicationNote_Date"></span><input type="date" name="Created" value="<%= Model.Created.ToShortDateString() %>" id="Edit_CommunicationNote_Date" class="required" /></div></div>
                <div class="clear"></div>
            </td><td colspan="2">
                <div><label for="Edit_CommunicationNote_PhysicianDropDown" class="float_left width200">Physician:</label>
                <div class="float_left"><%= Html.TextBox("PhysicianId", !Model.PhysicianId.IsEmpty() ? Model.PhysicianId.ToString() : "", new { @id = "Edit_CommunicationNote_PhysicianDropDown", @class = "Physicians" })%></div></div>
                <div class="clear"></div>
                <div><label class="float_left width200">&nbsp;</label>
                <div class="float_left ancillary_button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div></div>
            </td></tr>
            <tr>
                <td colspan="4" class="align_left">
                    <div><label class="width200 float_left">Communication Text</label>
                    <div class="float_left"><%= Html.Templates("Edit_CommunicationNote_Templates", new { @class = "Templates mobile_fr", @template = "#Edit_CommunicationNote_Text" })%></div></div>
                    <div class="clear"></div>
                    <div class="align_center"><%= Html.TextArea("Text", Model.Text, 8, 20, new { @class = "fill", @id = "Edit_CommunicationNote_Text", @maxcharacters = "1500" })%><p class="charsRemaining"></p></div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <div><label for="Edit_CommunicationNote_SendAsMessage" class="float_left width200">Send note as Message:</label>
                    <div class="float_left"><%= Html.CheckBox("SendAsMessage", false, new { @id = "Edit_CommunicationNote_SendAsMessage", @class = "bigradio" })%></div></div>
                    <div class="clear"></div>
                    <div id="Edit_CommunicationNote_Recipients"><%= Html.Recipients("Edit_CommunicationNote", Model.RecipientArray) %></div>
                </td>
            </tr>
            <tr><th colspan="4">Electronic Signature</th></tr>
            <tr>
                <td colspan="4">
                    <div class="third"><label for="Edit_CommunicationNote_ClinicianSignature" class="float_left">Staff Signature:</label><div class="float_right"><%= Html.Password("SignatureText", "", new { @id = "Edit_CommunicationNote_ClinicianSignature" })%></div></div>
                    <div class="third"></div>
                    <div class="third"><label for="Edit_CommunicationNote_ClinicianSignatureDate" class="float_left">Signature Date:</label><div class="float_right"><input type="date" name="SignatureDate" id="Edit_CommunicationNote_ClinicianSignatureDate" /></div></div>
                </td>
            </tr>
        </tbody>
    </table>
    <%= Html.Hidden("Status", "", new { @id = "Edit_CommunicationNote_Status" })%>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="EditCommNoteRemove();$('#Edit_CommunicationNote_Status').val('415');$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="EditCommNoteAdd();$('#Edit_CommunicationNote_Status').val('420');$(this).closest('form').submit();">Complete</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editcommunicationnote');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $("#Edit_CommunicationNote_SendAsMessage").click(function() { $("#Edit_CommunicationNote_Recipients").toggle(); });
    $("#Edit_CommunicationNote_PhysicianDropDown").PhysicianInput();
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    function EditCommNoteAdd() {
        $("#Edit_CommunicationNote_ClinicianSignature").removeClass('required').addClass('required');
        $("#Edit_CommunicationNote_ClinicianSignatureDate").removeClass('required').addClass('required');
    }
    function EditCommNoteRemove() {
        $("#Edit_CommunicationNote_ClinicianSignature").removeClass('required');
        $("#Edit_CommunicationNote_ClinicianSignatureDate").removeClass('required');
    }
</script>
<%} %>
