﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "communicationnoteslist",
        "Communication Notes",
        Current.AgencyName)%>
<div class="wrapper grid-bg" style="height:100%">
    <div class="wide_column">
        <div class="row"><label class="float_left">Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "AgencyCommunicationNote_BranchCode" })%></div><label class="float_left">Status:</label><div class="float_left"><select id="AgencyCommunicationNote_Status" name="Status" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div><div class="row"><label class="float_left">Date Range:</label><div class="float_left"><%= Html.Telerik().DatePicker().Name("StartDate").Value(DateTime.Now.AddDays(-59)).HtmlAttributes(new { @id = "AgencyCommunicationNote_StartDate", @class = "shortdate date" })%>To<%= Html.Telerik().DatePicker().Name("EndDate").Value(DateTime.Now).HtmlAttributes(new { @id = "AgencyCommunicationNote_EndDate", @class = "shortdate date" })%></div><div class="float_left"><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Agency.RebindAgencyCommunicationNotes();">Generate</a></li></ul></div></div><div class="buttons"><ul class="float_right"><li><%= Html.ActionLink("Excel Export", "CommunicationNotes", "Export", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "AgencyCommunicationNote_ExportLink", @class = "excel" })%></li></ul></div></div></div>
        <div class="clear"></div>
        <div class="row"><div id="AgencyCommunicationNote_Search" ></div></div>
    </div>
    <%= Html.Telerik()
            .Grid<CommunicationNote>().Name("List_CommunicationNote").HtmlAttributes(new { @style = "top:50px;" })
            .DataKeys(keys => { keys.Add(o => o.Id).RouteKey("id"); })
            .Columns(columns =>
            {
                columns.Bound(c => c.DisplayName).Title("Patient Name").Sortable(true).ReadOnly();
                columns.Bound(c => c.UserDisplayName).Title("Employee Name").Sortable(true).ReadOnly();
                columns.Bound(c => c.Created).Title("Date").Format("{0:MM/dd/yyyy}").Sortable(true).ReadOnly();
                columns.Bound(c => c.StatusName).Title("Status").Sortable(true).ReadOnly();
                columns.Bound(c => c.Id).Title(" ").ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({ Url: '/CommunicationNote/View/<#=PatientId#>/<#=Id#>', PdfUrl: 'Patient/CommunicationNotePdf', PdfData: {  'patientId': '<#=PatientId#>', 'eventId': '<#=Id#>' }})\"><span class=\"img icon print\"></span></a>").Width(35).Sortable(false);
            }).DataBinding(dataBinding => dataBinding.Ajax().Select("CommunicationNotes", "Patient", new { BranchId = ViewData["BranchId"], Status = 1, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }))
            .Sortable()
            .Scrollable(scrolling => scrolling.Enabled(true))%>
    </div>
    <script type="text/javascript">
        $("#AgencyCommunicationNote_Search").append($("<div/>").GridSearchById("#List_CommunicationNote"));
        $("#List_CommunicationNote .t-grid-content").css({ 'height': 'auto', 'bottom': '23px' });
    </script>
