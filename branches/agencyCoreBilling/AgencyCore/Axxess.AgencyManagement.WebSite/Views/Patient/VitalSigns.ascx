﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","patientvitalsigns","Patient Vital Signs",Model.DisplayName)%>
<div class="wrapper">
    <fieldset><%= Html.Hidden("Patient_VitalSigns_PatientId",Model.Id) %><div class="wide_column"><div class="row"><label class="float_left">Date Range:</label><div class="float_left"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="Patient_VitalSigns_StartDate" class="shortdate" />To<input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="Patient_VitalSigns_EndDate" class="shortdate" /></div><div class="float_left"> <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Patient.RebindVitalSigns();">Generate</a></li></ul></div></div><div class="buttons"><ul class="float_right"><li><%= Html.ActionLink("Excel Export", "VitalSigns", "Export", new { patientId = Model.Id, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "Patient_VitalSigns_ExportLink", @class = "excel" })%></li></ul></div></div></div></fieldset>
    <%= Html.Telerik().Grid<VitalSign>().Name("List_PatientVitalSigns").Columns(columns => {
           columns.Bound(s => s.VisitDate).Width(75).Title("Visit Date").Sortable(true).ReadOnly();
           columns.Bound(s => s.DisciplineTask).Title("Task").Sortable(true).ReadOnly();
           columns.Bound(s => s.UserDisplayName).Title("Employee Name").Sortable(true).ReadOnly();
           columns.Bound(s => s.BPLying).Title("BP Lying").Sortable(false).Width(70).ReadOnly();
           columns.Bound(s => s.BPSitting).Title("BP Sit").Sortable(false).Width(70).ReadOnly();
           columns.Bound(s => s.BPStanding).Title("BP Stand").Sortable(false).Width(70).ReadOnly();
           columns.Bound(s => s.Temp).Title("Temp").Sortable(false).Width(40);
           columns.Bound(s => s.Resp).Title("Resp").Sortable(false).Width(40).ReadOnly();
           columns.Bound(s => s.ApicalPulse).Sortable(false).Title("Apical").Width(45);
           columns.Bound(s => s.RadialPulse).Sortable(false).Title("Radial").Width(45).Sortable(false).ReadOnly();
           columns.Bound(s => s.BS).Sortable(false).Title("BS").Width(30);
           columns.Bound(s => s.Weight).Sortable(false).Title("Weight").Width(50);
           columns.Bound(s => s.PainLevel).Sortable(false).Title("Pain").Width(40);
       }).DataBinding(dataBinding => dataBinding.Ajax().Select("PatientVitalSigns", "Report", new { patientId = Model.Id, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable().Footer(false)
    %>
</div>
<script type="text/javascript">
    $("#List_PatientVitalSigns .t-grid-content").css({ 'height': 'auto' });
    $("#List_PatientVitalSigns").css({ 'top': '80px' });
    $("#List_PatientVitalSigns .t-toolbar.t-grid-toolbar").empty();
</script>
