﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "newhospitalizationlog",
        "New Hospitalization Log",
        Current.AgencyName)%>
<% using (Html.BeginForm("InsertHospitalizationLog", "Patient", FormMethod.Post, new { @id = "newHospitalizationLogForm" })) { %>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">New Hospitalization Log</th></tr>
            <tr><td colspan="2">
                <div><label for="New_HospitalizationLog_PatientName" class="float_left width200">Patient:</label>
                <div class="float_left"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", (!Model.IsEmpty()) ? Model.ToString() : "", new { @id = "New_HospitalizationLog_PatientName", @class="requireddropdown" })%></div></div>
                <div class="clear"></div>
                <div><label for="New_HospitalizationLog_EpisodeList" class="float_left width200">Episode:</label>
                <div class="float_left"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_HospitalizationLog_EpisodeList", @class = "requireddropdown" })%></div></div>
                <div class="clear"></div>
            </td><td colspan="2">
                <div><label for="New_HospitalizationLog_User" class="float_left width200">User:</label>
                <div class="float_left"><%= Html.LookupSelectList(SelectListTypes.Users, "UserId", "", new { @id = "New_HospitalizationLog_User", @class="requireddropdown" })%></div></div>
                <div class="clear"></div>
            </td></tr>
            <tr><th colspan="2">OASIS M0903</th><th colspan="2">OASIS M0906</th></tr>
            <tr><td colspan="2">
                <div><label for="New_HospitalizationLog_M0903LastHomeVisitDate" class="float_left"><span class="green">(M0903)</span> Date of Last (Most Recent) Home Visit:</label></div>
                <div class="clear"></div>
                <div class="float_left">
                    <%= Html.TextBox("M0903LastHomeVisitDate", string.Empty, new { @id = "New_HospitalizationLog_M0903LastHomeVisitDate" })%>
                </div>
            </td><td colspan="2">
                <div><label for="New_HospitalizationLog_M0906DischargeDate" class="float_left"><span class="green">(M0906)</span>Transfer Date: Enter the date of the transfer of the patient.</label></div>
                <div class="clear"></div>
                <div class="float_left">
                    <%= Html.TextBox("M0906DischargeDate", string.Empty, new { @id = "New_HospitalizationLog_M0906DischargeDate" })%>
                </div>
            </td></tr>
            <tr><th colspan="2">OASIS M2410</th><th colspan="2">OASIS M2440</th></tr>
            <tr><td colspan="2">
                <div class="float_left strong"><span class="green">(M2410)</span> To which Inpatient Facility has the patient been admitted?</div>
                <%= Html.Hidden("M2410TypeOfInpatientFacility", " ", new { @id = "" })%>
                <div class="clear"></div>
                <div class="margin">
                    <div>
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "01", false, new { @id = "New_HospitalizationLog_M2410TypeOfInpatientFacility1", @class = "radio float_left" })%>
                        <label for="New_HospitalizationLog_M2410TypeOfInpatientFacility1"><span class="float_left">1 &#8211;</span><span class="normal margin">Hospital</span></label>
                    </div><div>
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "02", false, new { @id = "New_HospitalizationLog_M2410TypeOfInpatientFacility2", @class = "radio float_left" })%>
                        <label for="New_HospitalizationLog_M2410TypeOfInpatientFacility2"><span class="float_left">2 &#8211;</span><span class="normal margin">Rehabilitation facility</span></label>
                    </div><div>
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "03", false, new { @id = "New_HospitalizationLog_M2410TypeOfInpatientFacility3", @class = "radio float_left" })%>
                        <label for="New_HospitalizationLog_M2410TypeOfInpatientFacility3"><span class="float_left">3 &#8211;</span><span class="normal margin">Nursing home</span></label>
                    </div><div>
                        <%= Html.RadioButton("M2410TypeOfInpatientFacility", "04", false, new { @id = "New_HospitalizationLog_M2410TypeOfInpatientFacility4", @class = "radio float_left" })%>
                        <label for="New_HospitalizationLog_M2410TypeOfInpatientFacility4"><span class="float_left">4 &#8211;</span><span class="normal margin">Hospice</span></label>
                    </div>
                </div>
            </td><td colspan="2">
                <div class="float_left strong"><span class="green">(M2440)</span> For what Reason(s) was the patient Admitted to a Nursing Home? (Mark all that apply.)</div>
                <div class="clear"></div>
                <div class="margin">
                    <div>
                        <input name="M2440ReasonPatientAdmittedTherapy" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2440ReasonPatientAdmittedTherapy' class='radio float_left' name='M2440ReasonPatientAdmittedTherapy' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2440ReasonPatientAdmittedTherapy"><span class="float_left">1 &#8211;</span><span class="margin normal">Therapy services</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedRespite" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2440ReasonPatientAdmittedRespite' class='radio float_left' name='M2440ReasonPatientAdmittedRespite' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2440ReasonPatientAdmittedRespite"><span class="float_left">2 &#8211;</span><span class="margin normal">Respite care</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedHospice" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2440ReasonPatientAdmittedHospice' class='radio float_left' name='M2440ReasonPatientAdmittedHospice' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2440ReasonPatientAdmittedHospice"><span class="float_left">3 &#8211;</span><span class="margin normal">Hospice care</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedPermanent" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2440ReasonPatientAdmittedPermanent' class='radio float_left' name='M2440ReasonPatientAdmittedPermanent' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2440ReasonPatientAdmittedPermanent"><span class="float_left">4 &#8211;</span><span class="margin normal">Permanent placement</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedUnsafe" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2440ReasonPatientAdmittedUnsafe' class='radio float_left' name='M2440ReasonPatientAdmittedUnsafe' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2440ReasonPatientAdmittedUnsafe"><span class="float_left">5 &#8211;</span><span class="margin normal">Unsafe for care at home</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedOther" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2440ReasonPatientAdmittedOther' class='radio float_left' name='M2440ReasonPatientAdmittedOther' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2440ReasonPatientAdmittedOther"><span class="float_left">6 &#8211;</span><span class="margin normal">Other</span></label>
                    </div><div>
                        <input name="M2440ReasonPatientAdmittedUnknown" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2440ReasonPatientAdmittedUnknown' class='radio float_left' name='M2440ReasonPatientAdmittedUnknown' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2440ReasonPatientAdmittedUnknown"><span class="float_left">UK &#8211;</span><span class="margin normal">Unknown</span></label>
                    </div>
                </div>
            </td></tr>
            <tr><th colspan="4">OASIS M2430</th></tr>
            <tr><td colspan="4">
                <div class="float_left strong"><span class="green">(M2430)</span> Reason for Hospitalization: For what reason(s) did the patient require hospitalization? (Mark all that apply.)</div>
                <div class="clear"></div>
                <div class="margin">
                    <div>
                        <input name="M2430ReasonForHospitalizationMed" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationMed' class='radio float_left' name='M2430ReasonForHospitalizationMed' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationMed"><span class="float_left">1 &#8211;</span><span class="margin normal">Improper medication administration, medication side effects, toxicity, anaphylaxis</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationFall" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationFall' class='radio float_left' name='M2430ReasonForHospitalizationFall' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationFall"><span class="float_left">2 &#8211;</span><span class="margin normal">Injury caused by fall</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationInfection" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationInfection' class='radio float_left' name='M2430ReasonForHospitalizationInfection' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationInfection"><span class="float_left">3 &#8211;</span><span class="margin normal">Respiratory infection (e.g., pneumonia, bronchitis)</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationOtherRP" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationOtherRP' class='radio float_left' name='M2430ReasonForHospitalizationOtherRP' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationOtherRP"><span class="float_left">4 &#8211;</span><span class="margin normal">Other respiratory problem</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationHeartFail" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationHeartFail' class='radio float_left' name='M2430ReasonForHospitalizationHeartFail' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationHeartFail"><span class="float_left">5 &#8211;</span><span class="margin normal">Heart failure (e.g., fluid overload)</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationCardiac" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationCardiac' class='radio float_left' name='M2430ReasonForHospitalizationCardiac' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationCardiac"><span class="float_left">6 &#8211;</span><span class="margin normal">Cardiac dysrhythmia (irregular heartbeat)</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationMyocardial" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationMyocardial' class='radio float_left' name='M2430ReasonForHospitalizationMyocardial' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationMyocardial"><span class="float_left">7 &#8211;</span><span class="margin normal">Myocardial infarction or chest pain</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationHeartDisease" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationHeartDisease' class='radio float_left' name='M2430ReasonForHospitalizationHeartDisease' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationHeartDisease"><span class="float_left">8 &#8211;</span><span class="margin normal">Other heart disease</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationStroke" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationStroke' class='radio float_left' name='M2430ReasonForHospitalizationStroke' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationStroke"><span class="float_left">9 &#8211;</span><span class="margin normal">Stroke (CVA) or TIA</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationHypo" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationHypo' class='radio float_left' name='M2430ReasonForHospitalizationHypo' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationHypo"><span class="float_left">10 &#8211;</span><span class="margin normal">Hypo/Hyperglycemia, diabetes out of control</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationGI" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationGI' class='radio float_left' name='M2430ReasonForHospitalizationGI' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationGI"><span class="float_left">11 &#8211;</span><span class="margin normal">GI bleeding, obstruction, constipation, impaction</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationDehMal" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationDehMal' class='radio float_left' name='M2430ReasonForHospitalizationDehMal' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationDehMal"><span class="float_left">12 &#8211;</span><span class="margin normal">Dehydration, malnutrition</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationUrinaryInf" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationUrinaryInf' class='radio float_left' name='M2430ReasonForHospitalizationUrinaryInf' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationUrinaryInf"><span class="float_left">13 &#8211;</span><span class="margin normal">Urinary tract infection</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationIV" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationIV' class='radio float_left' name='M2430ReasonForHospitalizationIV' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationIV"><span class="float_left">14 &#8211;</span><span class="margin normal">IV catheter-related infection or complication</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationWoundInf" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationWoundInf' class='radio float_left' name='M2430ReasonForHospitalizationWoundInf' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationWoundInf"><span class="float_left">15 &#8211;</span><span class="margin normal">Wound infection or deterioration</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationUncontrolledPain" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationUncontrolledPain' class='radio float_left' name='M2430ReasonForHospitalizationUncontrolledPain' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationUncontrolledPain"><span class="float_left">16 &#8211;</span><span class="margin normal">Uncontrolled pain</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationMental" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationMental' class='radio float_left' name='M2430ReasonForHospitalizationMental' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationMental"><span class="float_left">17 &#8211;</span><span class="margin normal">Acute mental/behavioral health problem</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationDVT" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationDVT' class='radio float_left' name='M2430ReasonForHospitalizationDVT' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationDVT"><span class="float_left">18 &#8211;</span><span class="margin normal">Deep vein thrombosis, pulmonary embolus</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationScheduled" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationScheduled' class='radio float_left' name='M2430ReasonForHospitalizationScheduled' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationScheduled"><span class="float_left">19 &#8211;</span><span class="margin normal">Scheduled treatment or procedure</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationOther" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationOther' class='radio float_left' name='M2430ReasonForHospitalizationOther' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationOther"><span class="float_left">20 &#8211;</span><span class="margin normal">Other than above reasons</span></label>
                    </div><div>
                        <input name="M2430ReasonForHospitalizationUK" type="hidden" value=" " />
                        <input id='New_HospitalizationLog_M2430ReasonForHospitalizationUK' class='radio float_left' name='M2430ReasonForHospitalizationUK' type='checkbox' value='1' />
                        <label for="New_HospitalizationLog_M2430ReasonForHospitalizationUK"><span class="float_left">UK &#8211;</span><span class="margin normal">Reason unknown</span></label>
                    </div>
                </div>
            </td></tr>
            <tr>
                <td colspan="4" class="align_left">
                    <div><label class="width200 float_left">Narrative</label>
                    <div class="float_left"><%= Html.Templates("Templates", new { @class = "Templates mobile_fr", @template = "#New_HospitalizationLog_GenericDischargeNarrative" })%></div></div>
                    <div class="clear"></div>
                    <div class="align_center"><%= Html.TextArea("GenericDischargeNarrative", string.Empty, 8, 20, new { @class = "fill", @id = "New_HospitalizationLog_GenericDischargeNarrative", @maxcharacters = "1500" })%><p class="charsRemaining"></p></div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="buttons"><ul>
         <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newhospitalizationlog');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    Schedule.loadEpisodeDropDown('New_HospitalizationLog_EpisodeList', $('#New_HospitalizationLog_PatientName'));
    $('#New_HospitalizationLog_PatientName').change(function() { Schedule.loadEpisodeDropDown('New_HospitalizationLog_EpisodeList', $(this)); });
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    Template.OnChangeInit();
</script>
<%} %>
