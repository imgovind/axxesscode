﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<HospitalizationViewData>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "patienthospitalizationlogs",
        "Hospitalization Logs",
        Model.Patient.DisplayName)%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Hospitalization Logs</th></tr>
            <tr><td colspan="4"><span class="bigtext"><%= Model.Patient.DisplayName %></span></td></tr>
        </tbody>
    </table>
    <div class="buttons">
        <ul class="float_left">
            <li><a href="javascript:void(0);" onclick="HospitalizationLog.Add('<%= Model.Patient.Id %>');">Add Hospitalization Log</a></li>
        </ul>
    </div><div class="clear"></div>
    <div id="Hospitalization_list" class="standard-chart">
        <ul>
            <li>
                <span class="source">Source</span>
                <span class="user">User</span>
                <span class="date">Date</span>
                <span class="printicon"></span>
                <span class="action">Action</span>
            </li>
        </ul><ol><%
        if (Model != null) {
            int i = 1;
            var logs = Model.Logs.OrderBy(l => l.Created).ToList();
            if (logs != null && logs.Count > 0)
            {
                foreach (var log in logs) { %>
                    <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd" : "even")) %>
                        <span class="source"><%= log.Source %></span>
                        <span class="user"><%= log.User %></span>
                        <span class="date"><%= log.HospitalizationDateFormatted %></span>
                        <span class="printicon"><%= log.PrintUrl %></span>
                        <span class="action"><a href="javascript:void(0);" onclick="HospitalizationLog.Edit('<%=log.PatientId %>', '<%= log.Id %>');" >Edit</a> | <a href="javascript:void(0);" onclick="HospitalizationLog.Delete('<%=log.PatientId %>', '<%= log.Id %>');">Delete</a></span>
                </li><%
                    i++;
                }
            } else { %>
                <li class="align_center"><span class="darkred">None</span></li>
        <% } } %>
        </ol>
    </div>
</div>
<script type="text/javascript">
    $(".standard-chart ol").each(function() { $("li:first", this).addClass("first"); $("li:last", this).addClass("last"); });
</script>


