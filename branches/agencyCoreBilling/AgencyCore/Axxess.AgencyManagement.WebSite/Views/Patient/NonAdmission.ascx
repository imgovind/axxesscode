﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="nonAdmissionContainer" class="hidden">
    <fieldset>
        <legend>Non-Admission Information</legend>
        <div class="column"><div class="row"><label for="ChangeStatus_NonAdmitDate" class="float_left">Non-Admit Date:</label><div class="float_right"><input type="date" name="NonAdmitDate" value="<%= DateTime.Today.ToString("MM/dd/yyyy") %>" id="ChangeStatus_NonAdmitDate" class="text required" /></div></div></div>
        <table class="form"><tbody>
            <tr><td colspan="4"><label for="ChangeStatus_ReasonInAppropriate"><strong>Reason Not Admitted:</strong></label></td></tr>
            <tr>
                <td><input id="ChangeStatus_ReasonInAppropriate" type="checkbox" value="Inappropriate For Home Care" name="Reason" class="required radio float_left" /><label for="ChangeStatus_ReasonInAppropriate" class="radio">Inappropriate For Home Care</label></td>
                <td><input id="ChangeStatus_ReasonRefused" type="checkbox" value="Patient Refused Service" name="Reason" class="required radio float_left" /><label for="ChangeStatus_ReasonRefused" class="radio">Patient Refused Service</label></td>
                <td><input id="ChangeStatus_ReasonOutofService" type="checkbox" value="Out of Service Area" name="Reason" class="required radio float_left" /><label for="ChangeStatus_ReasonOutofService" class="radio">Out of Service Area</label></td>
                <td><input id="ChangeStatus_ReasonOnService" type="checkbox" value="On Service with another agency" name="Reason" class="required radio float_left" /><label for="ChangeStatus_ReasonOnService" class="radio">On Service with another agency</label></td>
            </tr><tr>
                <td><input id="ChangeStatus_ReasonNotaProvider" type="checkbox" value="Not a Provider" name="Reason" class="required radio float_left" /><label for="ChangeStatus_ReasonNotaProvider" class="radio">Not a Provider</label></td>
                <td><input id="ChangeStatus_ReasonNotHomebound" type="checkbox" value="Not Homebound" name="Reason" class="required radio float_left" /><label for="ChangeStatus_ReasonNotHomebound" class="radio">Not Homebound</label></td>
                <td><input id="ChangeStatus_ReasonRedirected" type="checkbox" value="Redirected to alternate care facility" name="Reason" class="required radio float_left" /><label for="ChangeStatus_ReasonRedirected" class="radio">Redirected to alternate care facility</label></td>
                <td><input id="ChangeStatus_ReasonOther" type="checkbox" value="Other" name="Reason" class="required radio float_left" /><label for="ChangeStatus_ReasonOther" class="radio">Other (specify in Comments)</label></td>
            </tr>
        </tbody></table>
        <table class="form"><tbody>           
            <tr class="linesep vert">
               <td><label><strong>Comments:</strong></label>
                 <div><%= Html.TextArea("Comments", "")%></div></td>
            </tr>
        </tbody></table>
    </fieldset>
</div>