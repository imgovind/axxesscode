﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "patientdeletedtaskhistory",
        "Patient Deleted Tasks/Documents History",
        Model.DisplayName)%>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<ScheduleEvent>()
        .Name("List_Patient_DeletedTasks")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(e => e.DisciplineTaskName).Title("Task / Document").Sortable(true);
            columns.Bound(e => e.EventDateSortable).Title("Scheduled Date").Width(100).Sortable(true);
            columns.Bound(e => e.StatusName).Title("Status").Sortable(true);
            columns.Bound(e => e.UserName).Title("User").Sortable(true);
            columns.Bound(e => e.EventId).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Schedule.Restore('<#=EpisodeId#>','<#=PatientId#>','<#=EventId#>');\">Restore</a>").Title("Action").Width(100);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("DeletedTaskHistoryList", "Patient", new { patientId = Model.Id }))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_Patient_DeletedTasks .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    );
    $(".t-grid-content").css("height", "auto");
</script>