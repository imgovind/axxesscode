﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("SaveActivate", "Patient", FormMethod.Post, new { @id = "activatePatientForm" }))%>
<%  { %>
<div class="wrapper main">
    <%= Html.Hidden("PatientId", Model)%>
    <fieldset> 
        <div class="wide_column align_center">
            <div class="row"><label for="">  <b>Are you sure you want to activate this patient? </b></label></div>
        </div>
        <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Yes</a></li><li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">No</a></li></ul></div>
    </fieldset>
</div>
<% } %>

