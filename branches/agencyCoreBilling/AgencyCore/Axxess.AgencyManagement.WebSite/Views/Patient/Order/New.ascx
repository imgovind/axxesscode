﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "neworder",
        "New Order",
        Current.AgencyName) %>
<% using (Html.BeginForm("Add", "Order", FormMethod.Post, new { @id = "newOrderForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <div class="column">
            <div class="row">
                <label for="New_Order_PatientName" class="float_left">Patient Name:</label>
                <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", !Model.IsEmpty() ? Model.ToString() : "" , new { @id = "New_Order_PatientName", @class="requireddropdown" })%></div>
            </div><div class="row">
                <label class="float_left">Episode Associated:</label>
                <div class="float_right"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_Order_EpisodeList", @class = "requireddropdown" })%></div>
            </div><div class="row">
                <label for="New_Order_PhysicianDropDown" class="float_left">Physician:</label>
                <div class="float_right"><%= Html.TextBox("PhysicianId", "", new { @id = "New_Order_PhysicianDropDown", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float_right ancillary_button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div><div class="row">
                <label for="New_Order_Date" class="float_left">Date:</label>
                <div class="float_right"><input type="date" name="OrderDate" value="<%= DateTime.Now.ToShortDateString() %>" maxdate="<%= DateTime.Now.ToShortDateString() %>" id="New_Order_Date" class="required" /></div>
            </div><div class="row">
                <label for="New_Order_IsOrderForNextEpisode" class="float_left">Check here if this order is for the next episode</label>
                <div class="float_right"><%= Html.CheckBox("IsOrderForNextEpisode", false, new { @id = "New_Order_IsOrderForNextEpisode", @class = "radio float_left" }) %></div>
            </div>
        </div><div class="wide_column">
            <div class="row">
                <label for="New_Order_Summary" class="float_left">Summary/Title</label>
                <div class="mobile_fr"><%= Html.TextBox("Summary", "", new { @id = "New_Order_Summary", @class = "required longtext", @maxlength = "100" })%></div>
            </div><div class="row">
                <label for="New_Order_Text" class="strong">Order Description</label>
                <%= Html.Templates("New_Order_OrderTemplates", new { @class = "Templates mobile_fr", @template = "#New_Order_Text" })%>
                <div class="align_center"><textarea id="New_Order_Text" name="Text" cols="5" rows="12" style="height: auto;"></textarea></div>
            </div><div class="row">
                <%= Html.CheckBox("IsOrderReadAndVerified", false, new { @id = "New_Order_IsOrderReadAndVerified", @class = "radio float_left" })%>
                <label for="New_Order_IsOrderReadAndVerified" class="float_left">Order read back and verified.</label>
            </div>
       </div>
    </fieldset><fieldset>
        <div class="column">
            <div class="row">
                <label for="New_Order_ClinicianSignature" class="bigtext float_left">Clinician Signature:</label>
                <div class="float_right"><%= Html.Password("SignatureText", "", new { @id = "New_Order_ClinicianSignature", @class = "" })%></div>
            </div>
        </div>
        <div class="column">
            <div class="row">
                <label for="New_Order_ClinicianSignatureDate" class="bigtext float_left">Signature Date:</label>
                <div class="float_right"><input type="date" name="SignatureDate" id="New_Order_ClinicianSignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_Order_Status" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#New_Order_ClinicianSignatureDate').removeClass('required');$('#New_Order_Status').val('110');$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#New_Order_ClinicianSignatureDate').addClass('required');$('#New_Order_Status').val('115');$(this).closest('form').submit();">Create Order</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('neworder');">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    Schedule.loadEpisodeDropDown('New_Order_EpisodeList', $('#New_Order_PatientName'));
    $('#New_Order_PatientName').change(function() { Schedule.loadEpisodeDropDown('New_Order_EpisodeList',$(this)); });
    $(".row input.required,.row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
</script>
<% } %>