﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<PatientCenterViewData>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "patientcenter",
        "Patient Charts",
        Current.AgencyName)%>
<div class="wrapper layout">
    <div class="layout_left">
        <div class="top">
            <div class="buttons heading"><ul><% if (Current.HasRight(Permissions.ManagePatients)) { %><li><a href="javascript:void(0);" onclick="javascript:Acore.Open('newpatient');" title="Add New Patient">Add New Patient</a></li><% } %></ul></div>
            <div class="row"><label>Branch:</label><div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new {  @class = "PatientBranchCode" })%></div></div>
            <div class="row"><label>View:</label><div><select name="list" class="PatientStatusDropDown"><option value="1">Active Patients</option><option value="2">Discharged Patients</option></select></div></div>
            <div class="row"><label>Filter:</label><div><select name="list" class="PatientPaymentDropDown"><option value="0">All</option><option value="1">Medicare (traditional)</option><option value="2">Medicare (HMO/managed care)</option><option value="3">Medicaid (traditional)</option><option value="4">Medicaid (HMO/managed care)</option><option value="5">Workers' compensation</option><option value="6">Title programs</option><option value="7">Other government</option><option value="8">Private</option><option value="9">Private HMO/managed care</option><option value="10">Self Pay</option><option value="11">Unknown</option></select></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_Patient_Selection" class="text" name="" value="" type="text" /></div></div>
        </div>
        <div class="bottom"><% Html.RenderPartial("Patients"); %></div>
    </div>
    <div id="PatientMainResult" class="layout_main"><% if (Model == null || Model.Count == 0) { %><div class="abs center"><p>No Patients found that fit your search criteria.</p></div><% } %></div>
</div>