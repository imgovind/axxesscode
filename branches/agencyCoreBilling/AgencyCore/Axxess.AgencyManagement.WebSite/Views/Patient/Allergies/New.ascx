﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<% using (Html.BeginForm("InsertAllergy", "Patient", FormMethod.Post, new { @id = "newAllergyForm" })) { %>
<%= Html.Hidden("ProfileId", Model, new { @id = "New_Allergy_ProfileId" })%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset class="newallergy">
        <legend>New Allergy</legend>
        <div class="wide_column">
            <div class="row"><label for="New_Allergy_Name" class="float_left">Name:</label><div class="float_left"><%= Html.TextBox("Name", "", new { @id = "New_Allergy_Name", @class = "longtext input_wrapper required", @maxlength = "120" })%></div></div>
            <div class="row"><label for="New_Allergy_Type" class="float_left">Type:</label><div class="float_left"><%= Html.TextBox("Type", "", new { @id = "New_Allergy_Type", @class = "input_wrapper", @maxlength = "50" })%><br /><em>(e.g. Medication, Food, Animal, Plants, Environmental)</em></div></div>
         </div>   
    </fieldset><%= Html.Hidden("AddAnother", "", new { @id="New_Allergy_AddAnother" })%>   
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick='$("#New_Allergy_AddAnother").val("");$(this).closest("form").submit();'>Save &#38; Exit</a></li>
        <li><a href="javascript:void(0);" onclick='$("#New_Allergy_AddAnother").val("AddAnother");$(this).closest("form").submit();'>Save &#38; Add Another</a></li>
        <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
    </ul></div>
</div>
<%} %>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>