﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfile>" %>
<div id="AllergyProfile_active" class="standard-chart">
    <ul>
        <li class="align_center"><h3>Active Allergies</h3></li>
        <li>
            <span class="name">Name</span>
            <span class="type">Type</span>
            <span class="action">Action</span>
        </li>
    </ul><ol><%
    if (Model != null) {
        int i = 1;
        var allergies = Model.Allergies.ToObject<List<Allergy>>().FindAll(a => a.IsDeprecated == false).OrderBy(a => a.Name).ToList();
        if (allergies != null && allergies.Count > 0)
        {
            foreach (var allergy in allergies) { %>
                <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd" : "even")) %>
                    <span class="name"><%= allergy.Name %></span>
                    <span class="type"><%= allergy.Type %></span>
                    <span class="action"><a href="javascript:void(0);" onclick="Allergy.Edit('<%=Model.Id %>', '<%= allergy.Id %>');" >Edit</a> | <a href="javascript:void(0);" onclick="Allergy.Delete('<%=Model.Id %>', '<%= allergy.Id %>');">Delete</a></span>
            </li><%
                i++;
            }
        } else { %>
            <li class="align_center"><span class="darkred">No Active Allergies</span></li>
    <% } } %>
    </ol>
</div>
<div id="AllergyProfile_inactive" class="standard-chart">
    <ul>
        <li class="align_center"><h3>Deleted Allergies</h3></li>
        <li>
            <span class="name">Name</span>
            <span class="type">Type</span>
            <span class="action">Action</span>
        </li>
    </ul><ol><%
    if (Model != null) {
        int j = 1;
        var allergies = Model.Allergies.ToObject<List<Allergy>>().FindAll(a => a.IsDeprecated == true).OrderBy(a => a.Name).ToList();
        if (allergies != null && allergies.Count > 0) {
            foreach (var allergy in allergies) { %>
                <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (j % 2 != 0 ? "odd" : "even")) %>
                    <span class="name"><%= allergy.Name %></span>
                    <span class="type"><%= allergy.Type %></span>
                    <span class="action"><a href="javascript:void(0);" onclick="Allergy.Edit('<%=Model.Id %>', '<%= allergy.Id %>');" >Edit</a> | <a href="javascript:void(0);" onclick="Allergy.Restore('<%=Model.Id %>', '<%= allergy.Id %>');">Restore</a></span>
            </li><%
                j++;
            }
        } else { %>
            <li class="align_center"><span class="darkred">No Deleted Allergies</span></li>
    <% } } %>
    </ol>
</div>
<script type="text/javascript">
    $(".standard-chart ol").each(function() { $("li:first", this).addClass("first"); $("li:last", this).addClass("last"); });
</script>