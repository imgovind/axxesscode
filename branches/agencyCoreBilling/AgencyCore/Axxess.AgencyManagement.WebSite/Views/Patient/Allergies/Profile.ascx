﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AllergyProfileViewData>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "allergyprofile",
        "Allergy Profile",
        Model.Patient.DisplayName)%>
<%= Html.Hidden("Id", Model.AllergyProfile.Id, new {@id = "allergyProfileId" })%>
<%= Html.Hidden("PatientId", Model.AllergyProfile.PatientId)%>
<div class="wrapper main">
    <table class="fixed nursing">
        <tbody>
            <tr><th colspan="4">Allergy Profile</th></tr>
            <tr><td colspan="4"><span class="bigtext"><%= Model.Patient.DisplayName %></span></td></tr>
        </tbody>
    </table>
    <div class="buttons">
        <ul class="float_left">
            <li><a href="javascript:void(0);" onclick="Allergy.Add('<%= Model.AllergyProfile.Id %>');">Add Allergy</a></li>
            <li><a href="javascript:void(0);" onclick="Allergy.Refresh('<%= Model.AllergyProfile.Id %>');">Refresh Allergies</a></li>
            <li><a href="javascript:void(0);" onclick="Acore.OpenPrintView({ Url: '/Patient/AllergyProfilePrint/<%=Model.AllergyProfile.PatientId %>', PdfUrl: '/Patient/AllergyProfilePdf', PdfData: { 'id': '<%=Model.AllergyProfile.PatientId %>' } });">Print Allergy Profile</a></li>
        </ul>
    </div><div class="clear"></div>
    <div id="AllergyProfile_list"><% Html.RenderPartial("~/Views/Patient/Allergies/List.ascx", Model.AllergyProfile); %></div>
</div>
