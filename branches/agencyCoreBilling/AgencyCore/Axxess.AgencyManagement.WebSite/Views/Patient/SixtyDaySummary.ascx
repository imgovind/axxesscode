﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Patient>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "patientsixtydaysummary",
        "Patient 60 Day Summary",
        Model.DisplayName)%>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<VisitNoteViewData>()
        .Name("List_PatientSixtyDaySummary")
        .ToolBar(commnds => commnds.Custom())
        .DataKeys(keys => { keys.Add(o => o.EventId).RouteKey("id"); })
        .Columns(columns => {
            columns.Bound(v => v.UserDisplayName).Title("Employee Name").Sortable(false).ReadOnly();
            columns.Bound(v => v.VisitDate).Title("Visit Date").Sortable(false).ReadOnly();
            columns.Bound(v => v.SignatureDate).Title("Signature Date").Sortable(false).ReadOnly();
            columns.Bound(v => v.EpisodeRange).Title("Episode Date").Sortable(false).ReadOnly();
            columns.Bound(v => v.PhysicianDisplayName).Title("Physician Name").Sortable(false).ReadOnly();
            columns.Bound(v => v.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(30);
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("SixtyDaySummaryList", "Patient", new { patientId = Model.Id }))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_PatientSixtyDaySummary .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    );
    $(".t-grid-content").css("height", "auto");
</script>
