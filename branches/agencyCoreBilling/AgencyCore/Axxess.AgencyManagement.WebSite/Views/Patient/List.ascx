﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "listpatients",
        "List Patients",
        Current.AgencyName)%>
<% using (Html.BeginForm("Patients", "Export", FormMethod.Post)) { %>
<div class="wrapper">
<%= Html
        .Telerik()
        .Grid<PatientData>()
        .Name("List_Patient_Grid")
        .ToolBar(commnds => commnds.Custom())
        .Columns(columns => {
            columns.Bound(p => p.PatientIdNumber).Title("MR#").Width(90);
            columns.Bound(p => p.DisplayName).Title("Name").Width(180);
            columns.Bound(p => p.Address).Title("Address").Sortable(false);
            columns.Bound(p => p.DateOfBirth).Title("Date of Birth").Width(100).Sortable(true);
            columns.Bound(p => p.Gender).Width(80).Sortable(true);
            columns.Bound(p => p.Phone).Title("Phone").Width(120).Sortable(false);
            columns.Bound(p => p.Status).Title("Status").Width(80).Sortable(true);
            columns.Bound(p => p.Id).Width(90).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditPatient('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.Delete('<#=Id#>');\" class=\"deletePatient\">Delete</a>").Title("Action");
        })
        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Patient"))
        .ClientEvents(events => events.OnDataBound("Patient.BindGridButton"))
        .Sortable()
        .Scrollable(scrolling => scrolling.Enabled(true)) %>
</div>
<script type="text/javascript">
    $("#List_Patient_Grid .t-grid-toolbar").html("").append(
        $("<div/>").GridSearch()
    )<% if (Current.HasRight(Permissions.ManagePatients)) { %>.append(
        $("<div/>").addClass("float_left").Buttons([ { Text: "New Patient", Click: UserInterface.ShowNewPatient } ])
    )<% } 
        if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
        $("<div/>").addClass("float_right").Buttons([ { Text: "Excel Export", Click: function() { $(this).closest('form').submit() } } ])
    )<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>
