﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Incidents", "Export", FormMethod.Post)) { %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "listincidents",
        "List Incidents",
        Current.AgencyName)%>
<% using (Html.BeginForm("Incidents", "Export", FormMethod.Post)) { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<Incident>().Name("List_IncidentReport").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(i => i.PatientName).Title("Patient Name").Sortable(true);
    columns.Bound(i => i.PhysicianName).Title("Physician").Sortable(true);
    columns.Bound(i => i.IncidentType).Title("Type of Incident").Sortable(true);
    columns.Bound(i => i.IncidentDateFormatted).Title("Incident Date").Sortable(true);
    columns.Bound(i => i.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false);
    columns.Bound(i => i.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditIncident('<#=Id#>');\">Edit</a>").Sortable(false).Title("Action").Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Incident")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<%} %>
<script type="text/javascript">
$("#List_IncidentReport .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageIncidentAccidentInfectionReport)) { %>.append(
    $("<div/>").addClass("float_left").Buttons([ { Text: "New Incident Report", Click: UserInterface.ShowNewIncidentReport } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float_right").Buttons([ { Text: "Excel Export", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>
<% } %>