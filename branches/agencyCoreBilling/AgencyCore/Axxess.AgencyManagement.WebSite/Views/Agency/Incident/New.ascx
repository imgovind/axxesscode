﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "newincidentreport",
        "New Incident/Accident Report",
        Current.AgencyName)%>
<% using (Html.BeginForm("Add", "Incident", FormMethod.Post, new { @id = "newIncidentReportForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Incident_PatientId" class="float_left">Patient:</label>
                <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "New_Incident_PatientId", @class="requireddropdown" })%></div>
            </div><div class="row">
                <label class="float_left">Episode Associated:</label>
                <div class="float_right"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_Incident_EpisodeList", @class = "requireddropdown" })%></div>
            </div><div class="row">
                <label for="New_Incident_PhysicianId" class="float_left">Physician:</label>
                <div class="float_right"><%= Html.TextBox("PhysicianId", "", new { @id = "New_Incident_PhysicianId", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float_right ancillary_button">
                    <a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a>
                </div>
            </div>
         </div><div class="column">
            <div class="row">
                <label for="New_Incident_IncidentDate" class="float_left">Date of Incident:</label>
                <div class="float_right"><input type="date" name="IncidentDate" id="New_Incident_IncidentDate" class="required" /></div>
            </div><div class="row">
                <label for="New_Incident_IncidentType" class="float_left">Type of Incident:</label>
                <div class="float_right"><%= Html.TextBox("IncidentType", "", new { @id = "New_Incident_IncidentType", @class = "text input_wrapper required", @maxlength = "100" })%></div>
            </div><div class="row">
                <label for="New_Incident_MDNotifiedYes" class="float_left">M.D. Notified?</label>
                <div class="float_right">
                    <%= Html.RadioButton("MDNotified", "Yes", new { @id = "New_Incident_MDNotifiedYes", @class = "radio" })%>
                    <label for="New_Incident_MDNotifiedYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", new { @id = "New_Incident_MDNotifiedNo", @class = "radio" })%>
                    <label for="New_Incident_MDNotifiedNo" class="inlineradio">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", new { @id = "New_Incident_MDNotifiedNA", @class = "radio", @checked = "checked" })%>
                    <label for="New_Incident_MDNotifiedNA" class="inlineradio">N/A</label>
                </div>
            </div><div class="row">
                <label for="New_Incident_FamilyNotifiedYes" class="float_left">Family/CG Notified?</label>
                <div class="float_right">
                    <%= Html.RadioButton("FamilyNotified", "Yes", new { @id = "New_Incident_FamilyNotifiedYes", @class = "radio" })%>
                    <label for="New_Incident_FamilyNotifiedYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("FamilyNotified", "No", new { @id = "New_Incident_FamilyNotifiedNo", @class = "radio" })%>
                    <label for="New_Incident_FamilyNotifiedNo" class="inlineradio">No</label>
                    <%= Html.RadioButton("FamilyNotified", "NA", new { @id = "New_Incident_FamilyNotifiedNA", @class = "radio", @checked = "checked" })%>
                    <label for="New_Incident_FamilyNotifiedNA" class="inlineradio">N/A</label>
                </div>
            </div><div class="row">
                <label for="New_Incident_NewOrdersYes" class="float_left">New Orders?</label>
                <div class="float_right">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", new { @id = "New_Incident_NewOrdersYes", @class = "radio" })%>
                    <label for="New_Incident_NewOrdersYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", new { @id = "New_Incident_NewOrdersNo", @class = "radio" })%>
                    <label for="New_Incident_NewOrdersNo" class="inlineradio">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", new { @id = "New_Incident_NewOrdersNA", @class = "radio", @checked = "checked" })%>
                    <label for="New_Incident_NewOrdersNA" class="inlineradio">N/A</label>
                </div>
            </div>
         </div><div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Patient.LoadNewOrder('<%= Guid.Empty %>');" title="Add New Order">Add New Order</a></li>
            </ul>
        </div>
    </fieldset><fieldset>
        <legend>Individual(s) involved:</legend>
         <table class="form">
            <tbody>
                <tr>
                    <td>
                        <input id="New_Incident_IndividualInvolved1" type="checkbox" value="Patient" name="IndividualInvolvedArray" class="radio float_left" />
                        <label for="New_Incident_IndividualInvolved1" class="radio">Patient</label>
                    </td><td>
                        <input id="New_Incident_IndividualInvolved2" type="checkbox" value="Caregiver" name="IndividualInvolvedArray" class="radio float_left" />
                        <label for="New_Incident_IndividualInvolved2" class="radio">Caregiver</label>
                    </td><td>
                        <input id="New_Incident_IndividualInvolved3" type="checkbox" value="Employee/Contractor" name="IndividualInvolvedArray" class="radio float_left" />
                        <label for="New_Incident_IndividualInvolved3" class="radio">Employee/Contractor</label>
                    </td><td>
                        <input id="New_Incident_IndividualInvolved4" type="checkbox" value="Other" name="IndividualInvolvedArray" class="radio float_left" />
                        <label for="New_Incident_IndividualInvolved4" class="radio">Other (specify) &#160;</label>
                    </td><td>
                        <%= Html.TextBox("IndividualInvolvedOther", "", new { @id = "New_Incident_IndividualInvolvedOther", @class = "text input_wrapper", @maxlength = "100" })%>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset><fieldset>
        <legend>Description</legend>
        <div class="wide_column">
            <div class="row">
                <label for="Description" class="strong">Describe Incident/Accident:</label>
                <%= Html.Templates("DescriptionTemplates", new { @class = "Templates", @template = "#New_Incident_Description" })%>
                <div><%= Html.TextArea("Description", new { @id = "New_Incident_Description", @style = "height: 180px;" })%></div>
            </div><div class="row">
                <label for="ActionTaken" class="strong">Action Taken/Interventions Performed:</label>
                <%= Html.Templates("ActionTakenTemplates", new { @class = "Templates", @template = "#New_Incident_ActionTaken" })%>
                <div><%= Html.TextArea("ActionTaken", new { @id = "New_Incident_ActionTaken", @style = "height: 180px;" })%></div>
            </div><div class="row">
                <label for="NarrativeTemplates" class="strong">Narrative: </label>
                <%= Html.Templates("NarrativeTemplates", new { @class = "Templates", @template = "#New_Incident_Narrative" })%>
                <div><%= Html.TextArea("Orders", new { @id = "New_Incident_Narrative", @style = "height: 180px;" })%></div>
            </div><div class="row">
                <label for="FollowUp" class="strong">Follow Up: </label>
                <div><%= Html.TextArea("FollowUp", new { @id = "New_Incident_FollowUp", @style = "height: 180px;" })%></div>
            </div>
        </div>  
    </fieldset><fieldset>
        <div class="column">
            <div class="row">
                <label for="New_Incident_ClinicianSignature" class="bigtext float_left">Clinician Signature:</label>
                <div class="float_right"><%= Html.Password("SignatureText", "", new { @id = "New_Incident_ClinicianSignature" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_Incident_SignatureDate" class="bigtext float_left">Date:</label>
                <div class="float_right"><input type="date" name="SignatureDate" id="New_Incident_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_Incident_Status" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#New_Incident_Status').val('515');$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#New_Incident_Status').val('520');$(this).closest('form').submit();">Complete</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newincidentreport');">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    Schedule.loadEpisodeDropDown('New_Incident_EpisodeList', $('#New_Incident_PatientId'));
    $('#New_Incident_PatientId').change(function() { Schedule.loadEpisodeDropDown('New_Incident_EpisodeList', $(this)); });
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row div.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
    Template.OnChangeInit();
</script>
<% } %>