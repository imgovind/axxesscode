﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyTemplate>" %>
<%  using (Html.BeginForm("Update", "Template", FormMethod.Post, new { @id = "editTemplateForm" })) { %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "edittemplate",
        "Edit Template",
        Current.AgencyName) %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Template_Id" }) %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
       <div class="wide_column">
         <div><label for="Edit_Template_Title" class="strong">Name</label><br /><%= Html.TextBox("Title", Model.Title, new { @id = "Edit_Template_Title", @class = "required", @maxlength = "100", @style = "width: 500px;" })%><span class='req_red'>*</span></div><br />
         <div><label for="Edit_Template_Text" class="strong">Text</label><br /><textarea id="Edit_Template_Text" name="Text" cols="5" rows="6" style="height: 200px;"  maxcharacters="5000"><%= Model.Text %></textarea><p class="charsRemaining"></p></div>
       </div>
    </fieldset>
    <div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadTemplateLog('{0}');\" >Activity Logs</a>", Model.Id)%></div>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('edittemplate');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>
