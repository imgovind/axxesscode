﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "newtemplate",
        "New Template",
        Current.AgencyName) %>
<% using (Html.BeginForm("Add", "Template", FormMethod.Post, new { @id = "newTemplateForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
       <div class="wide_column">
         <div><label for="New_Template_Title" class="strong">Name</label><br /><%= Html.TextBox("Title", "", new { @id = "New_Template_Title", @class = "required", @maxlength = "100", @style = "width: 500px;" })%><span class='req_red'>*</span></div><br />
         <div><label for="New_Template_Text" class="strong">Text</label><br /><textarea id="New_Template_Text" name="Text" cols="5" rows="6" style="height: 200px; overflow: hidden;"  maxcharacters="5000"></textarea><p class="charsRemaining"></p></div>
       </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newtemplate');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
