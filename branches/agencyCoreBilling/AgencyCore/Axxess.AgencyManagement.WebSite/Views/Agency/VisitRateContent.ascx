﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<% var data = Model != null ? Model.ToCostRateDictionary() : new Dictionary<string, CostRate>(); %>
 <table class="fixed insurance">
     <col/><col  />
         <tbody>
                    <tr><th colspan="2" class="align_center borderBottom">Visit Cost Rates <em>(Employee Pay Rates)</em></th></tr>
                    <tr><th class="borderBottom align_left">Disciplines</th><th class="borderBottom  align_center">Cost Per Visit </th></tr>
                    <tr>
                        <td><label class="float_left">Skilled Nurse</label><%= Html.Hidden("RateDiscipline", "SkilledNurse")%>  </td>
                        <td class="align_center"><%= Html.TextBox("SkilledNurse_PerUnit", data.ContainsKey("SkilledNurse") ? data["SkilledNurse"].PerUnit : string.Empty, new { @id = "New_Insurance_SkilledNursePerUnit", @class = "rates currency" })%></td>
                    </tr>
                     <tr>
                        <td><label class="float_left">Skilled Nurse (Teaching) </label><%= Html.Hidden("RateDiscipline", "SkilledNurseTeaching")%>  </td>
                        <td class="align_center"><%= Html.TextBox("SkilledNurseTeaching_PerUnit", data.ContainsKey("SkilledNurseTeaching") ? data["SkilledNurseTeaching"].PerUnit : string.Empty, new { @id = "New_Insurance_SkilledNurseTeachingPerUnit", @class = "rates currency" })%></td>
                    </tr>
                     <tr>
                        <td><label class="float_left">Skilled Nurse (Observation)</label><%= Html.Hidden("RateDiscipline", "SkilledNurseObservation")%>  </td>
                        <td class="align_center"><%= Html.TextBox("SkilledNurseObservation_PerUnit", data.ContainsKey("SkilledNurseObservation") ? data["SkilledNurseObservation"].PerUnit : string.Empty, new { @id = "New_Insurance_SkilledNurseObservationPerUnit", @class = "rates currency" })%></td>
                    </tr>
                     <tr>
                        <td><label class="float_left">Skilled Nurse (Management)</label><%= Html.Hidden("RateDiscipline", "SkilledNurseManagement")%>  </td>
                        <td class="align_center"><%= Html.TextBox("SkilledNurseManagement_PerUnit", data.ContainsKey("SkilledNurseManagement") ? data["SkilledNurseManagement"].PerUnit : string.Empty, new { @id = "New_Insurance_SkilledNurseManagementPerUnit", @class = "rates currency" })%></td>
                    </tr>
                    <tr>  
                        <td><label class="float_left">Physical Therapy</label><%= Html.Hidden("RateDiscipline", "PhysicalTherapy")%>  </td>
                        <td class="align_center"><%= Html.TextBox("PhysicalTherapy_PerUnit", data.ContainsKey("PhysicalTherapy") ? data["PhysicalTherapy"].PerUnit : string.Empty, new { @id = "New_Insurance_PhysicalTherapyPerUnit", @class = "rates currency" })%></td>
                    </tr>
                     <tr>  
                        <td><label class="float_left">Physical Therapy Assistant</label><%= Html.Hidden("RateDiscipline", "PhysicalTherapyAssistance")%>  </td>
                        <td class="align_center"><%= Html.TextBox("PhysicalTherapyAssistance_PerUnit", data.ContainsKey("PhysicalTherapyAssistance") ? data["PhysicalTherapyAssistance"].PerUnit : string.Empty, new { @id = "New_Insurance_PhysicalTherapyAssistancePerUnit", @class = "rates currency" })%></td>
                    </tr>
                     <tr>  
                        <td><label class="float_left">Physical Therapy (Maintenance)</label><%= Html.Hidden("RateDiscipline", "PhysicalTherapyMaintenance")%>  </td>
                        <td class="align_center"><%= Html.TextBox("PhysicalTherapyMaintenance_PerUnit", data.ContainsKey("PhysicalTherapyMaintenance") ? data["PhysicalTherapyMaintenance"].PerUnit : string.Empty, new { @id = "New_Insurance_PhysicalTherapyMaintenancePerUnit", @class = "rates currency" })%></td>
                    </tr>
                    <tr>  
                        <td><label class="float_left">Occupational Therapy</label><%= Html.Hidden("RateDiscipline", "OccupationalTherapy")%></td>
                        <td class="align_center"><%= Html.TextBox("OccupationalTherapy_PerUnit", data.ContainsKey("OccupationalTherapy") ? data["OccupationalTherapy"].PerUnit : string.Empty, new { @id = "New_Insurance_OccupationalTherapyPerUnit", @class = "rates currency" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Occupational Therapy Assistant</label><%= Html.Hidden("RateDiscipline", "OccupationalTherapyAssistance")%></td>
                        <td class="align_center"><%= Html.TextBox("OccupationalTherapyAssistance_PerUnit", data.ContainsKey("OccupationalTherapyAssistance") ? data["OccupationalTherapyAssistance"].PerUnit : string.Empty, new { @id = "New_Insurance_OccupationalTherapyAssistancePerUnit", @class = "rates currency" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Occupational Therapy (Maintenance)</label><%= Html.Hidden("RateDiscipline", "OccupationalTherapyMaintenance")%></td>
                        <td class="align_center"><%= Html.TextBox("OccupationalTherapyMaintenance_PerUnit", data.ContainsKey("OccupationalTherapyMaintenance") ? data["OccupationalTherapyMaintenance"].PerUnit : string.Empty, new { @id = "New_Insurance_OccupationalTherapyMaintenancePerUnit", @class = "rates currency" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Speech Therapy</label><%= Html.Hidden("RateDiscipline", "SpeechTherapy")%></td>
                        <td class="align_center"><%= Html.TextBox("SpeechTherapy_PerUnit", data.ContainsKey("SpeechTherapy") ? data["SpeechTherapy"].PerUnit : string.Empty, new { @id = "New_Insurance_SpeechTherapyPerUnit", @class = "rates currency" })%></td>
                    </tr>
                     <tr>  
                        <td><label class="float_left">Speech Therapy (Maintenance)</label><%= Html.Hidden("RateDiscipline", "SpeechTherapyMaintenance")%></td>
                        <td class="align_center"><%= Html.TextBox("SpeechTherapyMaintenance_PerUnit", data.ContainsKey("SpeechTherapyMaintenance") ? data["SpeechTherapyMaintenance"].PerUnit : string.Empty, new { @id = "New_Insurance_SpeechTherapyMaintenancePerUnit", @class = "rates currency" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Medicare Social Worker</label><%= Html.Hidden("RateDiscipline", "MedicareSocialWorker")%></td>
                        <td class="align_center"><%= Html.TextBox("MedicareSocialWorker_PerUnit", data.ContainsKey("MedicareSocialWorker") ? data["MedicareSocialWorker"].PerUnit : string.Empty, new { @id = "New_Insurance_MedicareSocialWorkerPerUnit", @class = "rates currency" })%></td>
                    </tr>
                     <tr>
                        <td><label class="float_left">Home Health Aide</label><%= Html.Hidden("RateDiscipline", "HomeHealthAide")%></td>
                        <td class="align_center"><%= Html.TextBox("HomeHealthAide_PerUnit", data.ContainsKey("HomeHealthAide") ? data["HomeHealthAide"].PerUnit : string.Empty, new { @id = "New_Insurance_HomeHealthAidePerUnit", @class = "rates currency" })%></td>
                    </tr>
                    <tr>  
                        <td><label class="float_left">Attendant</label><%= Html.Hidden("RateDiscipline", "Attendant")%></td>
                        <td class="align_center"><%= Html.TextBox("Attendant_PerUnit", data.ContainsKey("Attendant") ? data["Attendant"].PerUnit : string.Empty, new { @id = "New_Insurance_AttendantPerUnit", @class = "rates currency" })%></td>
                    </tr>
                    <tr>  
                        <td><label class="float_left">Companion Care</label><%= Html.Hidden("RateDiscipline", "CompanionCare")%></td>
                        <td class="align_center"><%= Html.TextBox("CompanionCare_PerUnit", data.ContainsKey("CompanionCare") ? data["CompanionCare"].PerUnit : string.Empty, new { @id = "New_Insurance_CompanionCarePerUnit", @class = "rates currency" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Homemaker Services </label><%= Html.Hidden("RateDiscipline", "HomemakerServices")%></td>
                        <td class="align_center"><%= Html.TextBox("HomemakerServices_PerUnit", data.ContainsKey("HomemakerServices") ? data["HomemakerServices"].PerUnit : string.Empty, new { @id = "New_Insurance_HomemakerServicesPerUnit", @class = "rates currency" })%></td>
                    </tr>
                      <tr>  
                        <td><label class="float_left">Private Duty Sitter</label><%= Html.Hidden("RateDiscipline", "PrivateDutySitter")%></td>
                        <td class="align_center"><%= Html.TextBox("PrivateDutySitter_PerUnit", data.ContainsKey("PrivateDutySitter") ? data["PrivateDutySitter"].PerUnit : string.Empty, new { @id = "New_Insurance_PrivateDutySitterPerUnit", @class = "rates currency" })%></td>
                    </tr>
       </tbody>
  </table>
