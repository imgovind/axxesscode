﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("Add", "Location", FormMethod.Post, new { @id = "newLocationForm" })) { %>
<div class="wrapper main">
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row"><label for="New_Location_Name" class="float_left">Location Name:</label><div class="float_right"> <%=Html.TextBox("Name", "", new { @id = "New_Location_Name", @class = "text input_wrapper required", @maxlength = "20" })%></div></div>
            <div class="row"><label for="New_Location_CustomId" class="float_left">Custom Id:</label><div class="float_right"> <%=Html.TextBox("CustomId", "", new { @id = "New_Location_CustomId", @class = "text input_wrapper required" })%></div></div>
         </div>   
        <div class="column"> <div class="row"><label for="New_Location_MedicareProviderNumber" class="float_left">Medicare Provider Number:</label><div class="float_right"><%=Html.TextBox("MedicareProviderNumber", "", new { @id = "New_Location_MedicareProviderNumber", @class = "text input_wrapper required", @maxlength = "10" })%></div></div></div>
    </fieldset>  
    <fieldset>
        <legend>Address</legend>
        <div class="column">
            <div class="row"><label for="New_Location_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"> <%=Html.TextBox("AddressLine1", "", new { @id = "New_Location_AddressLine1", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="New_Location_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"> <%=Html.TextBox("AddressLine2", "", new { @id = "New_Location_AddressLine2", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Location_AddressCity" class="float_left">City:</label><div class="float_right"> <%=Html.TextBox("AddressCity", "", new { @id = "New_Location_AddressCity", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="New_Location_AddressStateCode" class="float_left"> State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Location_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Location_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div></div>
         </div>   
        <div class="column">   
            <div class="row"><label for="New_Location_PhoneArray1" class="float_left">Primary Phone:</label><div class="float_right"><input type="text" class="input_wrappermultible autotext required digits phone_short" name="PhoneArray" id="New_Location_PhoneArray1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext required digits phone_short" name="PhoneArray" id="New_Location_PhoneArray2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext required digits phone_long" name="PhoneArray" id="New_Location_PhoneArray3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Location_FaxNumberArray1" class="float_left">Fax Number:</label><div class="float_right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Location_FaxNumberArray1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Location_FaxNumberArray2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="FaxNumberArray" id="New_Location_FaxNumberArray3" maxlength="4" /></div></div> 
            <div class="row"><label for="New_Location_IsMainOffice">Is Main Office?</label><div class="float_right"><input type="checkbox" name="IsMainOffice" class="radio" id="New_Location_IsMainOffice" /></div></div>
        </div>
        <table class="form"><tbody><tr class="linesep vert"><td><label for="New_Location_Comments">Comments:</label><div><%= Html.TextArea("Comments", "", new { @id = "New_Location_Comments" })%></div></td></tr></tbody></table>
    </fieldset>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li><li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newlocation');">Cancel</a></li></ul></div>
</div>
<%} %>