﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "listlocations",
        "List Location",
        Current.AgencyName)%>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyLocation>().Name("List_Location").ToolBar(commnds => commnds.Custom()).Columns(columns => {
    columns.Bound(l => l.Name).Title("Company Name").Sortable(false);
    columns.Bound(l => l.MedicareProviderNumber).Title("Provider Number").Sortable(false).Width(150);
    columns.Bound(l => l.AddressFull).Title("Address").Sortable(true);
    columns.Bound(l => l.PhoneWork).Title("Phone Number").Width(120);
    columns.Bound(l => l.FaxNumber).Title("Fax Number").Width(120);
    columns.Bound(l => l.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditLocation('<#=Id#>');\">Edit</a>").Title("Action").Width(100);
    }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Location")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
    $("#List_Location .t-grid-toolbar").html("");
    $(".t-grid-content").css({ 'height': 'auto' });
</script>
