﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Guid>" %>
<div class="main">
    <% using (Html.BeginForm("AddLicense", "Physician", FormMethod.Post, new { @id = "editPhysicainLicenseForm" })) { %>
    <%= Html.Hidden("PhysicainId", Model, new { @id = "Edit_PhysicainLicense_Id" })%>
        <fieldset>
            <table class="form layout_auto"><tbody>
                <tr>
                    <td>License Number</td>
                    <td>State</td>
                    <td>Issue Date</td>
                    <td>Expiration Date</td>
                </tr>
                <tr>
                    <td><%= Html.TextBox("LicenseNumber", "", new { @id = "Edit_PhysicainLicense_LicenseNumber", @class = "valid", @maxlength = "25" })%></td>
                    <td><%= Html.LookupSelectList(SelectListTypes.States, "State", "", new { @id = "Edit_UserLicense_Type", @class = "valid" })%></td>
                    <td><input type="date" name="InitiationDate" id="Edit_PhysicainLicense_InitDate" class="required" /></td>
                    <td><input type="date" name="ExpirationDate" id="Edit_PhysicainLicense_ExpDate" class="required" /></td>
                    <td><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Add License</a></li></ul></div></td>
                </tr>
            </tbody></table>
        </fieldset>
    <% } %>
    <div class="currentlicenses">
        <%= Html.Telerik().Grid<PhysicainLicense>().Name("List_Physicain_Licenses").DataKeys(keys => { keys.Add(M => M.Id); }).Columns(columns =>
{
            columns.Bound(l => l.LicenseNumber).ReadOnly().Sortable(false);
            columns.Bound(l => l.State).ReadOnly().Sortable(false);
            columns.Bound(l => l.InitiationDateFormatted).ReadOnly().Title("Issue Date").Sortable(false);
            columns.Bound(l => l.ExpirationDate).Title("Expiration Date").Sortable(false);
            columns.Command(commands =>
            {
                commands.Edit();
                commands.Delete();
            }).Width(135).Title("Action");
            }).DataBinding(dataBinding => dataBinding.Ajax()
                                                                .Select("LicenseList", "Physician", new { physicianId = Model })
                                                                .Update("UpdateLicense", "Physician", new { physicianId = Model })
                                                                .Delete("DeleteLicense", "Physician", new { physicianId = Model }))
            .Sortable().Footer(false).Scrollable(scrolling => scrolling.Enabled(true))
        %>
    </div>
</div>
