﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type='text/javascript'>$('#window_{0}').Rename('{1} | {2}'); </script>","orderstobesent","Orders To Be Sent",Current.AgencyName)%>
<div id="List_OrdersToBeSent_Container" class="wrapper">
    <div class="float_right">
        <div class="buttons">
            <ul><li><a id="List_OrdersToBeSent_SendButton" href="javascript:void(0);" onclick="Agency.MarkOrdersAsSent('#List_OrdersToBeSent_Container');">Send Electronically</a></li></ul>
            <br />
            <ul><li><%= Html.ActionLink("Excel Export", "OrdersToBeSent", "Export", new { BranchId = Guid.Empty, sendAutomatically = true, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "OrdersToBeSent_ExportLink", @class = "excel" })%></li></ul>
        </div>
    </div>
    <fieldset class="orders-filter">
        <label class="float_left">Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "OrdersToBeSent_BranchId" })%></div>
        <label class="strong" for="OrdersToBeSent_StartDate">Date Range:</label><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="OrdersToBeSent_StartDate" class="shortdate" />
        <label class="strong" for="OrdersToBeSent_EndDate">To</label><input type="date" name="EndtDate" value="<%= DateTime.Now.ToShortDateString() %>" id="OrdersToBeSent_EndDate" class="shortdate" />
        <div class="buttons float_right"><ul><li><a href="javascript:void(0);" onclick="Agency.RebindOrdersToBeSent();">Generate</a></li></ul></div><div class="clear"></div>
        <label class="strong" for="List_OrdersToBeSent_SendType">Filter by:</label><select id="List_OrdersToBeSent_SendType" class="SendAutomatically" name="SendAutomatically"><option value="true">Electronic Orders</option><option value="false">Manual Orders (Fax, Mail, etc)</option></select>
    </fieldset>
    <div id="OrdersToBeSent_Search"></div>
    <%= Html.Telerik().Grid<Order>().Name("List_OrdersToBeSent").Columns(columns => {
    columns.Bound(o => o.Type).Title(" ").ClientTemplate("<input type='checkbox' class='OrdersToBeSent' name='<#=Type#>' value='<#=Id#>_<#=PatientId#>' />").Width(35).Sortable(false);
            columns.Bound(o => o.Number).Title("Order").Width(80).Sortable(true);
            columns.Bound(o => o.PatientName).Title("Patient").Sortable(true);
            columns.Bound(o => o.Text).Title("Type").Sortable(true);
            columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(true);
            columns.Bound(o => o.CreatedDate).Title("Order Date").Width(100).Sortable(true);
            columns.Bound(o => o.PrintUrl).Title(" ").ClientTemplate("<#=PrintUrl#>").Width(35).Sortable(false);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersToBeSent", "Agency", new { BranchId = Guid.Empty, sendAutomatically = true, StartDate = DateTime.Now.AddDays(-59).ToShortDateString(), EndDate = DateTime.Now.ToShortDateString() })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>
<script type="text/javascript">
    $("#OrdersToBeSent_Search").append(
        $("<div/>").GridSearchById("#List_OrdersToBeSent")
    );
    $("#window_orderstobesent_content").css({
        "background-color": "#d6e5f3"
    }).find("#List_OrdersToBeSent").css({
        "height": "auto",
        "top": "100px"
    }).find(".t-grid-content").css({
        "height": "auto",
        "top": "26px"
    });
    $("#List_OrdersToBeSent_SendType").change(function() {
        Agency.RebindOrdersToBeSent();
        if ($(this).val() == 'true') $("#List_OrdersToBeSent_SendButton").text("Send Electronically");
        else $("#List_OrdersToBeSent_SendButton").text("Mark as Sent to Physician");
    });
</script>