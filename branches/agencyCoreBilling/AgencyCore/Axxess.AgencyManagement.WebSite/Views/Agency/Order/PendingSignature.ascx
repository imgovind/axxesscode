﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type='text/javascript'>$('#window_{0}').Rename('{1} | {2}');  </script>","orderspendingsignature","Orders Pending Signature",Current.AgencyName)%>
<div class="wrapper grid-bg" style="height:100%">
        <div class="wide_column">
           <div class="row">
                <label class="float_left">Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "OrdersPendingSignature_BranchId" })%></div>
                <label class="float_left">Date Range:</label><div class="float_left"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="OrdersPendingSignature_StartDate" class="shortdate" />To<input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="OrdersPendingSignature_EndDate" class="shortdate" /></div>
                <div class="float_left"><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Agency.RebindPendingOrders();">Generate</a></li></ul></div></div>
                <div class="buttons"><ul class="float_right"><li><%= Html.ActionLink("Excel Export", "OrdersPendingSignature", "Export", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "OrdersPendingSignature_ExportLink", @class = "excel" })%></li></ul></div>
           </div>
           <div class="clear"></div>
           <div class="row"><div id="OrdersPendingSignature_Search" ></div> </div>
        </div>
    <%= Html.Telerik().Grid<Order>().Name("List_OrdersPendingSignature").HtmlAttributes(new { @style = "top:60px;" }).DataKeys(keys =>
{
    keys.Add(o => o.Id).RouteKey("id"); keys.Add(o => o.PatientId).RouteKey("patientId"); keys.Add(o => o.Type).RouteKey("type"); keys.Add(o => o.ReceivedDate).RouteKey("receivedDate"); keys.Add(o => o.StartDate).RouteKey("startDate"); keys.Add(o => o.EndDate).RouteKey("endDate"); keys.Add(o => o.BranchId).RouteKey("branchId");
        }).Columns(columns => {
            columns.Bound(o => o.Number).Title("Order").Width(70).Sortable(true).ReadOnly();
            columns.Bound(o => o.PatientName).Title("Patient").Width(200).Sortable(true).ReadOnly();
            columns.Bound(o => o.Text).Title("Type").Sortable(true).ReadOnly();
            columns.Bound(o => o.PhysicianName).Title("Physician").Sortable(true).ReadOnly();
            columns.Bound(o => o.CreatedDate).Title("Order Date").Width(100).Sortable(true).ReadOnly();
            columns.Bound(o => o.SentDate).Title("Sent Date").Width(100).Sortable(true).ReadOnly();
            columns.Bound(o => o.ReceivedDate).ClientTemplate("<#=''#>").Title("Received Date").Width(120).Sortable(true);
            columns.Command(commands => { commands.Edit(); }).Width(160).Title("Action");
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersPendingSignature", "Agency", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }).Update("MarkOrderAsReturned", "Agency")).Editable(editing => editing.Mode(GridEditMode.InLine)).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).ClientEvents(events => events.OnRowDataBound("Agency.OrderCenterOnload").OnEdit("Agency.PendingSignatureOrdersOnEdit"))%>
</div>
<script type="text/javascript">
    $("#OrdersPendingSignature_Search").append($("<div/>").GridSearchById("#List_OrdersPendingSignature"));
    $("#List_OrdersPendingSignature .t-grid-content").css({ 'height': 'auto', 'bottom': '23px' });
</script>