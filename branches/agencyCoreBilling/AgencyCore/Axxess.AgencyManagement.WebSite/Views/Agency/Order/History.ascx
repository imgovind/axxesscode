﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "ordersHistory",
        "Order History",
        Current.AgencyName)%>
<div class="wrapper grid-bg" style="height:100%">
        <div class="wide_column">
            <div class="row">
                <label class="float_left">Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "OrdersHistory_BranchId" })%></div>
                <label class="float_left">Date Range:</label><div class="float_left"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="OrdersHistory_StartDate" class="shortdate" />To<input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="OrdersHistory_EndDate" class="shortdate" /></div>
                <div class="float_left"><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Agency.RebindOrdersHistory();">Generate</a></li></ul></div></div>
                <div class="buttons"><ul class="float_right"><li><%= Html.ActionLink("Excel Export", "OrdersHistory", "Export", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "OrdersHistory_ExportLink", @class = "excel" })%></li></ul></div>
            </div>
        </div>
    <%= Html.Telerik().Grid<Order>().Name("List_OrdersHistory").HtmlAttributes(new { @style = "top:30px;" }).DataKeys(keys =>
{
            keys.Add(o => o.Id).RouteKey("id");
            keys.Add(o => o.Type).RouteKey("type");
            keys.Add(o => o.ReceivedDate).RouteKey("receivedDate");
            keys.Add(o => o.SendDate).RouteKey("sendDate");
        }).Columns(columns => {
            columns.Bound(o => o.Number).Title("Order").Width(70).Sortable(true).ReadOnly();
            columns.Bound(o => o.PatientName).Title("Patient").Width(180).Sortable(true).ReadOnly();
            columns.Bound(o => o.Text).Title("Type").Sortable(true).ReadOnly();
            columns.Bound(o => o.PhysicianName).Title("Physician").Width(180).Sortable(true).ReadOnly();
            columns.Bound(o => o.OrderDate).Title("Order Date").Width(100).Sortable(true).ReadOnly();
            columns.Bound(o => o.SendDate).Format("{0:MM/dd/yyyy}").Title("Sent Date").Width(100).Sortable(true);
            columns.Bound(o => o.ReceivedDate).Format("{0:MM/dd/yyyy}").Title("Received Date").Width(120).Sortable(true);
            columns.Bound(o => o.Id).Sortable(false).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowOrdersHistoryModal('<#=Id#>','<#=PatientId#>','<#=Type#>');\">Edit</a>").Title("Action").Width(90);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("OrdersHistoryList", "Agency", new { BranchId = Guid.Empty, StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
</div>
<div id="OrdersHistory_Edit_Container" class="ordershistoryeditmodal hidden"></div>
<script type="text/javascript">
    $("#List_OrdersHistory .t-grid-content").css({ 'height': 'auto' });
    $("#List_OrdersHistory .t-toolbar.t-grid-toolbar").empty();
</script>