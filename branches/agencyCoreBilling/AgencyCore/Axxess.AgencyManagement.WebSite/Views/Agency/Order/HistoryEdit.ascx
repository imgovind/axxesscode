﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Order>" %>
<% using (Html.BeginForm("EditOrders", "Agency", FormMethod.Post, new { @id = "updateOrderHistry" })) { %>
<div class="form_wrapper">
    <%= Html.Hidden("Id", Model.Id) %>
    <%= Html.Hidden("PatientId", Model.PatientId) %>
    <%= Html.Hidden("Type", Model.Type) %>
    <fieldset>
        <legend>Update <%= Model.Text %></legend>
        <div class="wide_column">
            <div class="row">
                <label class="float_left">Sent Date:</label>
                <div class="float_right"><input type="date" name="SendDate" value="<%= Model.SendDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.SendDate.ToShortDateString() %>" /></div>
            </div><div class="row">
                <label class="float_left">Received Date:</label>
                <div class="float_right"><input type="date" name="ReceivedDate" value="<%= Model.ReceivedDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.ReceivedDate.ToShortDateString() %>" /></div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Update</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
        </ul>
    </div>
</div>
<% } %>