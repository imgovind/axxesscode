﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="form_wrapper">
    <fieldset>
        <legend>OASIS Forms</legend>
        <div class="column">
            <div class="row">
                <a href="Oasis/StartOfCareBlank" target="_blank">OASIS-C Start of Care</a>
                &#160;&#8212;&#160;
                <a href="Oasis/StartOfCarePTBlank" target="_blank">(PT)</a>
                <a href="Oasis/StartOfCareOTBlank" target="_blank">(OT)</a>
            </div>
            <div class="row">
                <a href="Oasis/ResumptionOfCareBlank" target="_blank">OASIS-C Resumption of Care</a>
                &#160;&#8212;&#160;
                <a href="Oasis/ResumptionOfCarePTBlank" target="_blank">(PT)</a>
                <a href="Oasis/ResumptionOfCareOTBlank" target="_blank">(OT)</a>
            </div>
            <div class="row">
                <a href="Oasis/RecertificationBlank" target="_blank">OASIS-C Recertification</a>
                &#160;&#8212;&#160;
                <a href="Oasis/RecertificationPTBlank" target="_blank">(PT)</a>
                <a href="Oasis/RecertificationOTBlank" target="_blank">(OT)</a>
            </div>
            <div class="row">
                <a href="Oasis/FollowUpBlank" target="_blank">OASIS-C Follow-Up</a>
                &#160;&#8212;&#160;
                <a href="Oasis/FollowUpPTBlank" target="_blank">(PT)</a>
                <a href="Oasis/FollowUpOTBlank" target="_blank">(OT)</a>
            </div>
            <div class="row">
                <a href="Oasis/TransferInPatientNotDischargedBlank" target="_blank">OASIS-C Transfer Not Discharged</a>
                &#160;&#8212;&#160;
                <a href="Oasis/TransferInPatientNotDischargedPTBlank" target="_blank">(PT)</a>
                <a href="Oasis/TransferInPatientNotDischargedOTBlank" target="_blank">(OT)</a>
            </div>
            <div class="row">
                <a href="Oasis/TransferInPatientDischargedBlank" target="_blank">OASIS-C Transfer and Discharge</a>
                &#160;&#8212;&#160;
                <a href="Oasis/TransferInPatientDischargedPTBlank" target="_blank">(PT)</a>
                <a href="Oasis/TransferInPatientDischargedOTBlank" target="_blank">(OT)</a>
            </div>
        </div><div class="column">
            <div class="row">
                <a href="Oasis/DischargeFromAgencyDeathBlank" target="_blank">OASIS-C Death at Home</a>
                &#160;&#8212;&#160;
                <a href="Oasis/DischargeFromAgencyDeathPTBlank" target="_blank">(PT)</a>
                <a href="Oasis/DischargeFromAgencyDeathOTBlank" target="_blank">(OT)</a>
            </div>
            <div class="row">
                <a href="Oasis/DischargeFromAgencyBlank" target="_blank">OASIS-C Discharge from Agency</a>
                &#160;&#8212;&#160;
                <a href="Oasis/DischargeFromAgencyPTBlank" target="_blank">(PT)</a>
                <a href="Oasis/DischargeFromAgencyOTBlank" target="_blank">(OT)</a>
            </div>
            <div class="row"><a href="Oasis/NonOasisStartOfCareBlank" target="_blank">Non-OASIS Start of Care</a></div>
            <div class="row"><a href="Oasis/NonOasisRecertificationBlank" target="_blank">Non-OASIS Recertification</a></div>
            <div class="row"><a href="Oasis/NonOasisDischargeBlank" target="_blank">Non-OASIS Discharge from Agency</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Physician Forms</legend>
        <div class="column">
            <div class="row"><a href="Patient/ViewFaceToFaceEncounterPdfBlank" target="_blank">Face to Face Encounter</a></div>
        </div><div class="column">
            <div class="row"><a href="Patient/PhysicianOrderPdfBlank" target="_blank">Physician Order</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Nursing Forms</legend>
        <div class="column">
            <div class="row"><a href="Schedule/TransferSummaryPdfBlank" target="_blank">Transfer Summary</a></div>
            <div class="row"><a href="Schedule/CoordinationOfCarePdfBlank" target="_blank">Coordination of Care</a></div>
            <div class="row"><a href="Schedule/SixtyDaySummaryPdfBlank" target="_blank">60 Day Summary</a></div>
            <div class="row"><a href="Schedule/MissedVisitPdfBlank" target="_blank">Missed Visit Form</a></div>
        </div><div class="column">
            <div class="row"><a href="Schedule/LVNSVisitPdfBlank" target="_blank">LVN Supervisory Visit</a></div>
            <div class="row"><a href="Schedule/DischargeSummaryPdfBlank" target="_blank">Discharge Summary</a></div>
            <div class="row"><a href="Schedule/SNVisitPdfBlank" target="_blank">Skilled Nurse Visit</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Home Health Aide Forms</legend>
        <div class="column">
            <div class="row"><a href="Schedule/HHACarePlanPdfBlank" target="_blank">HHA Care Plan</a></div>
            <div class="row"><a href="Schedule/HHAVisitPdfBlank" target="_blank">HHA Visit Note</a></div>
        </div><div class="column">
            <div class="row"><a href="Schedule/HHASVisitPdfBlank" target="_blank">HHA Supervisory Visit</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Medical Social Work Forms</legend>
        <div class="column">
            <div class="row"><a href="Schedule/MSWEvaluationPdfBlank" target="_blank">MSW Evaluation</a></div>
            <div class="row"><a href="Schedule/MSWProgressNotePdfBlank" target="_blank">MSW Progress Note</a></div>
            <div class="row"><a href="Schedule/MSWVisitPdfBlank" target="_blank">MSW Visit</a></div>
        </div><div class="column">
            <div class="row"><a href="Schedule/MSWAssessmentPdfBlank" target="_blank">MSW Assessment</a></div>
            <div class="row"><a href="Schedule/MSWDischargePdfBlank" target="_blank">MSW Discharge</a></div>
            <div class="row"><a href="Schedule/TransportationNotePdfBlank" target="_blank">Driver/Transportation Log</a></div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Therapy Forms</legend>
        <div class="column">
            <div class="row"><a href="Schedule/PTDischargePdfBlank" target="_blank">PT Discharge</a></div>
            <div class="row"><a href="Schedule/PTEvaluationPdfBlank" target="_blank">PT Evaluation</a></div>
            <div class="row"><a href="Schedule/PTReEvaluationPdfBlank" target="_blank">PT Re-Evaluation</a></div>
            <div class="row"><a href="Schedule/PTMaintenancePdfBlank" target="_blank">PT Maintenance</a></div>
            <div class="row"><a href="Schedule/PTVisitPdfBlank" target="_blank">PT Visit</a></div>
            <div class="row"><a href="Schedule/PTAVisitPdfBlank" target="_blank">PTA Visit</a></div>
            <div class="row"><a href="Schedule/OTEvaluationPdfBlank" target="_blank">OT Evaluation</a></div>
            <div class="row"><a href="Schedule/OTReEvaluationPdfBlank" target="_blank">OT Re-Evaluation</a></div>
            <div class="row"><a href="Schedule/OTDischargePdfBlank" target="_blank">OT Discharge</a></div>
        </div><div class="column">
            <div class="row"><a href="Schedule/OTMaintenancePdfBlank" target="_blank">OT Maintenance</a></div>
            <div class="row"><a href="Schedule/OTVisitPdfBlank" target="_blank">OT Visit</a></div>
            <div class="row"><a href="Schedule/COTAVisitPdfBlank" target="_blank">COTA Visit</a></div>
            <div class="row"><a href="Schedule/STEvaluationPdfBlank" target="_blank">ST Evaluation</a></div>
            <div class="row"><a href="Schedule/STReEvaluationPdfBlank" target="_blank">ST Re-Evaluation</a></div>
            <div class="row"><a href="Schedule/STMaintenancePdfBlank" target="_blank">ST Maintenance</a></div>
            <div class="row"><a href="Schedule/STDischargePdfBlank" target="_blank">ST Discharge</a></div>
            <div class="row"><a href="Schedule/STVisitPdfBlank" target="_blank">ST Visit</a></div>
        </div>
    </fieldset>
</div>