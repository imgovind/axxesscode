﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "newhospital",
        "New Hospital",
        Current.AgencyName)%>
<% using (Html.BeginForm("Add", "Hospital", FormMethod.Post, new { @id = "newHospitalForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Hospital Information</legend>
        <div class="column">
            <div class="row"><label for="New_Hospital_Name" class="float_left">Hospital Name:</label><div class="float_right"> <%= Html.TextBox("Name", "", new { @id = "New_Hospital_Name", @class = "text input_wrapper required", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Hospital_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"> <%= Html.TextBox("AddressLine1", "", new { @id = "New_Hospital_AddressLine1", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Hospital_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"> <%= Html.TextBox("AddressLine2", "", new { @id = "New_Hospital_AddressLine2", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Hospital_AddressCity" class="float_left">City:</label><div class="float_right"> <%= Html.TextBox("AddressCity", "", new { @id = "New_Hospital_AddressCity", @class = "text input_wrapper required", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Hospital_AddressZipCode" class="float_left"> State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Hospital_AddressStateCode", @class = "AddressStateCode requireddropdown valid" })%><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Hospital_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div></div>
         </div>   
        <div class="column">   
            <div class="row"><label for="New_Hospital_ContactPersonFirstName" class="float_left">Contact First Name:</label><div class="float_right"><%= Html.TextBox("ContactPersonFirstName", "", new { @id = "New_Hospital_ContactPersonFirstName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Hospital_ContactPersonLastName" class="float_left">Contact Last Name:</label><div class="float_right"><%= Html.TextBox("ContactPersonLastName", "", new { @id = "New_Hospital_ContactPersonLastName", @class = "text input_wrapper", @maxlength = "75" })%></div></div>
            <div class="row"><label for="New_Hospital_EmailAddress" class="float_left">Email :</label><div class="float_right"><%= Html.TextBox("EmailAddress", "", new { @id = "New_Hospital_EmailAddress", @class = "text email input_wrapper", @maxlength = "50" })%></div></div>
            <div class="row"><label for="New_Hospital_PhoneArray1" class="float_left">Primary Phone:</label><div class="float_right"><input type="text" class="input_wrappermultible autotext required digits phone_short" name="PhoneArray" id="New_Hospital_PhoneArray1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext required digits phone_short" name="PhoneArray" id="New_Hospital_PhoneArray2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext required digits phone_long" name="PhoneArray" id="New_Hospital_PhoneArray3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Hospital_FaxNumberArray1" class="float_left">Fax Number:</label><div class="float_right"><input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Hospital_FaxNumberArray1" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_short" name="FaxNumberArray" id="New_Hospital_FaxNumberArray2" maxlength="3" /> - <input type="text" class="input_wrappermultible autotext digits phone_long" name="FaxNumberArray" id="New_Hospital_FaxNumberArray3" maxlength="4" /></div></div> 
        </div>
        <div class="wide_column">
            <div class="row"><label for="Comment" class="strong">Comment:</label><div class="align_center"><%= Html.TextArea("Comment", "")%></div></div>
        </div>
    </fieldset> 
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newhospital');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>
<%} %>

