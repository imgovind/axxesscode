﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "visitrates",
        "Edit Visit Rates",
        Current.AgencyName) %>
<% using (Html.BeginForm("EditCost", "Agency", FormMethod.Post, new { @id = "editVisitCostForm" })){ %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <div class="wide_column">
            <div class="row">
                <label for="Edit_VisitRate_LocationId" class="float_left">Agency Branch:</label>
                <%= Html.LookupSelectList(SelectListTypes.Branches, "AgencyLocationId", Model.Id.ToString(), new { @id = "Edit_VisitRate_LocationId", @class = "BranchLocation required" }) %>
            </div><div class="row">
                <div id="Edit_VisitRate_Container">
                    <% Html.RenderPartial("~/Views/Agency/VisitRateContent.ascx", Model); %>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('visitrates');">Close</a></li>
        </ul>
    </div>
</div>
<% } %>
<script type="text/javascript">
    $('#Edit_VisitRate_LocationId').change(function() {
        Agency.loadVisitRateContent($(this).val());
    });
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>