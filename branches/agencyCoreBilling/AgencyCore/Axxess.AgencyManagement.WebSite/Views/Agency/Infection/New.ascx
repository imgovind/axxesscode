﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "newinfectionreport",
        "New Infection Report",
        Current.AgencyName)%>
<% using (Html.BeginForm("Add", "Infection", FormMethod.Post, new { @id = "newInfectionReportForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Information</legend>
        <div class="column">
            <div class="row">
                <label for="New_Infection_PatientId" class="float_left">Patient:</label>
                <div class="float_right"><%= Html.LookupSelectList(SelectListTypes.Patients, "PatientId", "", new { @id = "New_Infection_PatientId", @class = "requireddropdown" })%></div>
            </div><div class="row">
                <label class="float_left">Episode Associated:</label>
                <div class="float_right"><%= Html.PatientEpisodes("EpisodeId", Guid.Empty.ToString(), Guid.Empty, "-- Select Episode --", new { @id = "New_Infection_EpisodeList", @class = "requireddropdown" })%></div>
            </div><div class="row">
                <label for="New_Infection_PhysicianId" class="float_left">Physician:</label>
                <div class="float_right"><%= Html.TextBox("PhysicianId", "", new { @id = "New_Infection_PhysicianId", @class = "Physicians" })%></div>
                <div class="clear"></div>
                <div class="float_right ancillary_button"><a href="javascript:void(0);" onclick="UserInterface.ShowNewPhysicianModal();">New Physician</a></div>
            </div>                
         </div><div class="column">
            <div class="row">
                <label for="New_Infection_InfectionDate" class="float_left">Date of Infection:</label>
                <div class="float_right"><input type="date" name="InfectionDate" id="New_Infection_InfectionDate" class="required" /></div>
            </div><div class="row">
                <label for="New_Infection_TreatmentPrescribedYes" class="float_left">Treatment Prescribed?</label>
                <div class="float_right">
                    <%= Html.RadioButton("TreatmentPrescribed", "Yes", new { @id = "New_Infection_TreatmentPrescribedYes", @class = "radio" })%>
                    <label for="New_Infection_TreatmentPrescribedYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "No", new { @id = "New_Infection_TreatmentPrescribedNo", @class = "radio" })%>
                    <label for="New_Infection_TreatmentPrescribedNo" class="inlineradio">No</label>
                    <%= Html.RadioButton("TreatmentPrescribed", "NA", new { @id = "New_Infection_TreatmentPrescribedNA", @class = "radio" })%>
                    <label for="New_Infection_TreatmentPrescribedNA" class="inlineradio">N/A</label>
                </div>
            </div><div class="row">
                <label for="New_Infection_MDNotifiedYes" class="float_left">M.D. Notified?</label>
                <div class="float_right">
                    <%= Html.RadioButton("MDNotified", "Yes", new { @id = "New_Infection_MDNotifiedYes", @class = "radio" })%>
                    <label for="New_Infection_MDNotifiedYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("MDNotified", "No", new { @id = "New_Infection_MDNotifiedNo", @class = "radio" })%>
                    <label for="New_Infection_MDNotifiedNo" class="inlineradio">No</label>
                    <%= Html.RadioButton("MDNotified", "NA", new { @id = "New_Infection_MDNotifiedNA", @class = "radio" })%>
                    <label for="New_Infection_MDNotifiedNA" class="inlineradio">N/A</label>
                </div>
            </div><div class="row">
                <label for="New_Infection_NewOrdersYes" class="float_left">New Orders?</label>
                <div class="float_right">
                    <%= Html.RadioButton("NewOrdersCreated", "Yes", new { @id = "New_Infection_NewOrdersYes", @class = "radio" })%>
                    <label for="New_Infection_NewOrdersYes" class="inlineradio">Yes</label>
                    <%= Html.RadioButton("NewOrdersCreated", "No", new { @id = "New_Infection_NewOrdersNo", @class = "radio" })%>
                    <label for="New_Infection_NewOrdersNo" class="inlineradio">No</label>
                    <%= Html.RadioButton("NewOrdersCreated", "NA", new { @id = "New_Infection_NewOrdersNA", @class = "radio" })%>
                    <label for="New_Infection_NewOrdersNA" class="inlineradio">N/A</label>
                </div>
            </div>
         </div>
         <div class="buttons">
            <ul>
                <li><a href="javascript:void(0);" onclick="Patient.LoadNewOrder('<%= Guid.Empty %>');" title="Add New Order">Add New Order</a></li>
            </ul>
        </div>
    </fieldset><fieldset>
        <legend>Type of Infection</legend>
        <table class="form">
            <tbody>
                <tr>
                    <td>
                        <input id="New_Infection_InfectionType1" type="checkbox" value="Gastrointestinal" name="InfectionTypeArray" class="radio float_left" />
                        <label for="New_Infection_InfectionType1" class="radio">Gastrointestinal</label>
                    </td><td>
                        <input id="New_Infection_InfectionType2" type="checkbox" value="Respiratory" name="InfectionTypeArray" class="radio float_left" />
                        <label for="New_Infection_InfectionType2" class="radio">Respiratory</label>
                    </td><td>
                        <input id="New_Infection_InfectionType3" type="checkbox" value="Skin" name="InfectionTypeArray" class="radio float_left" />
                        <label for="New_Infection_InfectionType3" class="radio">Skin</label>
                    </td><td>
                        <input id="New_Infection_InfectionType4" type="checkbox" value="Wound" name="InfectionTypeArray" class="radio float_left" />
                        <label for="New_Infection_InfectionType4" class="radio">Wound</label>
                    </td>
                </tr><tr>
                    <td>
                        <input id="New_Infection_InfectionType5" type="checkbox" value="Urinary" name="InfectionTypeArray" class="radio float_left" />
                        <label for="New_Infection_InfectionType5" class="radio">Urinary</label>
                    </td><td>
                        <input id="New_Infection_InfectionType6" type="checkbox" value="Other" name="InfectionTypeArray" class="radio float_left" />
                        <label for="New_Infection_InfectionType6" class="radio">Other (specify)</label>
                    </td><td colspan="2">
                        <%= Html.TextBox("InfectionTypeOther", "", new { @id = "New_Infection_InfectionTypeOther", @class = "text input_wrapper", @maxlength = "100" })%>
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset><fieldset>
        <legend>Treatment</legend>
        <div class="wide_column">
            <div class="row">
                <label for="Treatment" class="strong">Treatment/Antibiotic:</label>
                <%= Html.Templates("TreatmentTemplates", new { @class = "Templates", @template = "#New_Infection_Treatment" })%>
                <div><%= Html.TextArea("Treatment", new { @id="New_Infection_Treatment", @style = "height: 180px;" })%></div>
            </div><div class="row">
                <label for="Orders" class="strong">Narrative: </label>
                <%= Html.Templates("NarrativeTemplates", new { @class = "Templates", @template = "#New_Infection_Narrative" })%>
                <div><%= Html.TextArea("Orders", new { @id = "New_Infection_Narrative", @style = "height: 180px;" })%></div>
            </div><div class="row">
                <label for="FollowUp" class="strong">Follow Up: </label>
                <div><%= Html.TextArea("FollowUp", new { @id = "New_Infection_FollowUp", @style = "height: 180px;" })%></div>
            </div>
        </div>
    </fieldset><fieldset>
        <div class="column">
            <div class="row">
                <label for="New_Infection_ClinicianSignature" class="bigtext float_left">Clinician Signature:</label>
                <div class="float_right"><%= Html.Password("SignatureText", "", new { @id = "New_Infection_ClinicianSignature" })%></div>
            </div>
        </div><div class="column">
            <div class="row">
                <label for="New_Infection_SignatureDate" class="bigtext float_left">Date:</label>
                <div class="float_right"><input type="date" name="SignatureDate" id="New_Infection_SignatureDate" /></div>
            </div>
        </div>
    </fieldset>
    <%= Html.Hidden("Status", "", new { @id = "New_Infection_Status" })%>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$('#New_Infection_Status').val('515');$(this).closest('form').submit();">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$('#New_Infection_Status').val('520');$(this).closest('form').submit();">Complete</a></li>
            <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newinfectionreport');">Cancel</a></li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    Schedule.loadEpisodeDropDown('New_Infection_EpisodeList', $('#New_Infection_PatientId'));
    $('#New_Infection_PatientId').change(function() { Schedule.loadEpisodeDropDown('New_Infection_EpisodeList', $(this)); });
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row div.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
    Template.OnChangeInit();
</script>
<% } %>