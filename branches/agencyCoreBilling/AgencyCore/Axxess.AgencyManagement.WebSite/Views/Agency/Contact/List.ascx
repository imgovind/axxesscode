﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "listcontacts",
        "List Contact",
        Current.AgencyName)%>
<%  using (Html.BeginForm("Contacts", "Export", FormMethod.Post)) { %>
<div class="wrapper">
    <%= Html.Telerik().Grid<AgencyContact>().Name("List_Contact").ToolBar(commnds => commnds.Custom()).Columns(columns => {
            columns.Bound(c => c.DisplayName).Title("Name").Width(150).Sortable(true);
            columns.Bound(c => c.CompanyName).Title("Company").Sortable(true);
            columns.Bound(c => c.ContactType).Title("Type").Width(150).Sortable(true);
            columns.Bound(c => c.PhonePrimaryFormatted).Title("Phone").Width(120).Sortable(false);
            columns.Bound(c => c.EmailAddress).ClientTemplate("<a href='mailto:<#=EmailAddress#>'><#=EmailAddress#></a>").Title("Email").Width(150).Sortable(true);
            columns.Bound(c => c.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"UserInterface.ShowEditContact('<#=Id#>');\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Contact.Delete('<#=Id#>');\" class=\"deleteContact\">Delete</a>").Title("Action").Sortable(false).Width(100);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Contact")).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<% } %>
<script type="text/javascript">
$("#List_Contact .t-grid-toolbar").html("").append(
    $("<div/>").GridSearch()
)<% if (Current.HasRight(Permissions.ManageContact)) { %>.append(
    $("<div/>").addClass("float_left").Buttons([ { Text: "New Contact", Click: UserInterface.ShowNewContact } ])
)<% } 
    if (Current.HasRight(Permissions.ExportListToExcel)) { %>.append(
    $("<div/>").addClass("float_right").Buttons([ { Text: "Excel Export", Click: function() { $(this).closest('form').submit() } } ])
)<% } %>;
    $(".t-grid-content").css("height", "auto");
</script>