﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "newcontact",
        "New Contact",
        Current.AgencyName)%>
<% using (Html.BeginForm("Add", "Contact", FormMethod.Post, new { @id = "newContactForm" })) { %>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Contact Information</legend>
        <div class="column">
            <div class="row"><label for="New_Contact_CompanyName">Company Name:</label><div class="float_right"><%= Html.TextBox("CompanyName", "", new { @id = "New_Contact_CompanyName", @maxlength = "100", @class = "text" })%></div></div>
            <div class="row"><label for="New_Contact_FirstName">Contact First Name:</label><div class="float_right"><%= Html.TextBox("FirstName", "", new { @id = "New_Contact_FirstName", @maxlength = "75", @class = "required" })%></div></div>
            <div class="row"><label for="New_Contact_LastName">Contact Last Name:</label><div class="float_right"><%= Html.TextBox("LastName", "", new { @id = "New_Contact_LastName", @maxlength = "75", @class = "required" })%></div></div>
            <div class="row"><label for="New_Contact_Email">Contact Email:</label><div class="float_right"><%= Html.TextBox("EmailAddress", "", new { @id = "New_Contact_EmailAddress", @class = "text email input_wrapper", @maxlength = "100" })%></div></div>
            <div class="row"><label for="New_Contact_Type">Contact Type:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.ContactTypes, "ContactType", "", new { @id = "New_Contact_Type", @class = "ContactType required valid" })%></div></div>
            <div class="row"><label for="New_Contact_OtherContactType">Other Contact Type (specify):</label><div class="float_right"><%= Html.TextBox("ContactTypeOther", "", new { @id = "New_Contact_OtherContactType", @class = "text input_wrapper", @maxlength = "100" })%></div></div>
        </div><div class="column">
            <div class="row"><label for="New_Contact_AddressLine1">Address:</label><div class="float_right"><%= Html.TextBox("AddressLine1", "", new { @id = "New_Contact_AddressLine1", @maxlength = "75", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Contact_AddressLine2">&#160;</label><div class="float_right"><%= Html.TextBox("AddressLine2", "", new { @id = "New_Contact_AddressLine2", @maxlength = "75", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="New_Contact_AddressCity">City:</label><div class="float_right"><%= Html.TextBox("AddressCity", "", new { @id = "New_Contact_AddressCity", @maxlength = "75", @class = "text required input_wrapper" })%></div></div>
            <div class="row"><label for="New_Contact_AddressStateCode">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", "", new { @id = "New_Contact_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", "", new { @id = "New_Contact_AddressZipCode", @class = "text numeric required input_wrapper zip", @size = "5", @maxlength = "5" })%></div></div>
            <div class="row"><label for="New_Contact_PhonePrimary1">Office Phone:</label><div class="float_right"><input type="text" class="autotext numeric required phone_short" name="PhonePrimaryArray" id="New_Contact_PhonePrimary1" maxlength="3" /> - <input type="text" class="autotext numeric required phone_short" name="PhonePrimaryArray" id="New_Contact_PhonePrimary2" maxlength="3" /> - <input type="text" class="autotext numeric required phone_long" name="PhonePrimaryArray" id="New_Contact_PhonePrimary3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Contact_PhoneAlternate1">Mobile Phone:</label><div class="float_right"><input type="text" class="autotext numeric phone_short" name="PhoneAlternateArray" id="New_Contact_PhoneAlternate1" maxlength="3" /> - <input type="text" class="autotext numeric phone_short" name="PhoneAlternateArray" id="New_Contact_PhoneAlternate2" maxlength="3" /> - <input type="text" class="autotext numeric phone_long" name="PhoneAlternateArray" id="New_Contact_PhoneAlternate3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Contact_Fax1">Fax Number:</label><div class="float_right"><input type="text" class="autotext numeric phone_short" name="FaxNumberArray" id="New_Contact_Fax1" maxlength="3" /> - <input type="text" class="autotext numeric phone_short" name="FaxNumberArray" id="New_Contact_Fax2" maxlength="3" /> - <input type="text" class="autotext numeric phone_long" name="FaxNumberArray" id="New_Contact_Fax3" maxlength="4" /></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newcontact');">Cancel</a></li>
    </ul></div>
</div>
<%} %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>
