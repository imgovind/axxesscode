﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","listpastduerecerts","Past Due Recerts",Current.AgencyName)%>
<div class="wrapper grid-bg" style="height:100%">
     <div class="wide_column">
        <div class="row"><label class="float_left">Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "AgencyPastDueRecet_BranchCode" })%></div><label class="float_left">Insurance:</label><div class="float_left"><%= Html.Insurances("InsuranceId", Model, new { @id = "AgencyPastDueRecet_InsuranceId", @class = "Insurances" })%></div></td><td><div class="row"><label class="float_left">Due Date From :</label><div class="float_left"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="AgencyPastDueRecet_StartDate" class="shortdate" /> </div></div><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Agency.RebindAgencyPastDueRecet();">Generate</a></li><% if (Current.HasRight(Permissions.ExportListToExcel)){ %><li><%= Html.ActionLink("Export to Excel", "RecertsPastDue", "Export", new { BranchId = Guid.Empty, InsuranceId = Model, StartDate = DateTime.Now.AddDays(-60) }, new { id = "AgencyPastDueRecet_ExportLink" })%></li><%} %></ul></div></div>
        <div class="clear"></div>
        <div class="row"><div id="AgencyPastDueRecerts_Search"></div></div>
    </div>   
    <%= Html.Telerik().Grid<RecertEvent>().Name("List_PastDueRecerts").HtmlAttributes(new { @style = "top:50px;" }).Columns(columns =>
{
    columns.Bound(r => r.PatientName).Sortable(true);
    columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(true).Width(120);
    columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true);
    columns.Bound(r => r.Status).Title("Status").Sortable(true);
    columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(120).Sortable(true);
    columns.Bound(r => r.DateDifference).Title("Past Dates").Width(60);
}).DataBinding(dataBinding => dataBinding.Ajax().Select("RecertsPastDue", "Agency", new { BranchId = Guid.Empty, InsuranceId=Model , StartDate = DateTime.Now.AddDays(-60) })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))
    %>
</div>
<script type="text/javascript">
$("#AgencyPastDueRecerts_Search").append( $("<div/>").GridSearchById("#List_PastDueRecerts"));
$("#window_listpastduerecerts #List_PastDueRecerts .t-grid-content").css({"height":"auto","top":"26px"});
</script>
