﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","listupcomingrecerts","Upcoming Recerts",Current.AgencyName)%>
<div class="wrapper grid-bg" style="height:100%">
    <div class="wide_column">
        <div class="row"><label class="float_left">Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", Guid.Empty.ToString(), new { @id = "AgencyUpcomingRecet_BranchCode" })%></div><label class="float_left">Insurance:</label><div class="float_left"><%= Html.Insurances("InsuranceId", Model, new { @id = "AgencyUpcomingRecet_InsuranceId", @class = "Insurances" })%></div> <div class="buttons"><ul> <li><a href="javascript:void(0);" onclick="Agency.RebindAgencyUpcomingRecet();">Generate</a></li><%if (Current.HasRight(Permissions.ExportListToExcel)){ %><li><%= Html.ActionLink("Export to Excel", "RecertsUpcoming", "Export", new { BranchId = Guid.Empty, InsuranceId = Model }, new { id = "AgencyUpcomingRecet_ExportLink" })%></li><%} %></ul></div></div>
        <div class="clear"></div>
        <div class="row"><div id="AgencyUpcomingRecet_Search"></div></div>
    </div>
    <%= Html.Telerik()
        .Grid<RecertEvent>().Name("List_UpcomingRecerts").HtmlAttributes(new { @style = "top:50px;" })
        .Columns(columns =>
        {
            columns.Bound(r => r.PatientName).Sortable(true);
            columns.Bound(r => r.PatientIdNumber).Title("MR#").Sortable(true).Width(120);
            columns.Bound(r => r.AssignedTo).Title("Employee Responsible").Sortable(true);
            columns.Bound(r => r.Status).Title("Status").Sortable(true);
            columns.Bound(r => r.TargetDate).Format("{0:MM/dd/yyyy}").Title("Due Date").Width(120).Sortable(true);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("RecertsUpcoming", "Agency", new { BranchId = Guid.Empty, InsuranceId = Model })).Sortable().Scrollable(scrolling => scrolling.Enabled(true))%>
 </div>
<script type="text/javascript">
    $("#AgencyUpcomingRecet_Search").append( $("<div/>").GridSearchById("#List_UpcomingRecerts"));
    $("#window_listupcomingrecerts #List_UpcomingRecerts .t-grid-content").css({"height":"auto","top":"26px"});
</script>
