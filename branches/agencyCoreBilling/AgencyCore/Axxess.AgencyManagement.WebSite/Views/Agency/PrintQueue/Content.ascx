﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= Html.Telerik().Grid<PatientEpisodeEvent>().Name("printQueueGrid").Columns(columns => {
            columns.Bound(s => s.CustomValue).ClientTemplate("<input name='CustomValue' type='checkbox' value='<#= CustomValue #>'/>").Title("").Width(50).HtmlAttributes(new { style = "text-align:center" }).Sortable(false);
            columns.Bound(s => s.PatientName);
            columns.Bound(s => s.EventDate).Width(100);
            columns.Bound(s => s.TaskName).ClientTemplate("<#=PrintUrl#>").Title("Task").Width(250);
            columns.Bound(s => s.Status).Width(200);
            columns.Bound(s => s.UserName).Width(150);
        })
        .Groupable(settings => settings.Groups(groups => {
            var data = ViewData["GroupName"].ToString();
            if (data == "PatientName")
            {
                groups.Add(s => s.PatientName);
            }
            else if (data == "EventDate")
            {
                groups.Add(s => s.EventDate);
            }
            else if (data == "DisciplineTaskName")
            {
                groups.Add(s => s.TaskName);
            }
            else if (data == "UserName")
            {
                groups.Add(s => s.UserName);
            }
            else
            {
                groups.Add(s => s.EventDate);
            }
        })).DataBinding(dataBinding => dataBinding.Ajax().Select("PrintQueueGrid", "Agency")).Scrollable().Sortable().Footer(false)%>
