﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "printqueue",
        "Print Queue",
        Current.AgencyName)%>
<%  using (Html.BeginForm("BulkUpdate", "Schedule", FormMethod.Post, new { @id = "printQueueForm", @style = "width: 100%; height: 100%;" })) { %>
<div class="buttons abs_top">
    <ul class="float_right">
        <li><a href="javascript:void(0);" onclick="Agency.RebindPrintQueue();">Refresh</a></li>
        <li><a href="Export/PrintQueueList">Excel Export</a></li>
    </ul><ul>
        <li><a href="javascript:void(0);" onclick="Agency.loadPrintQueue('PatientName');">Group By Patient</a></li>
        <li><a href="javascript:void(0);" onclick="Agency.loadPrintQueue('EventDate');">Group By Date</a></li>
        <li><a href="javascript:void(0);" onclick="Agency.loadPrintQueue('DisciplineTaskName');">Group By Task</a></li>
        <li><a href="javascript:void(0);" onclick="Agency.loadPrintQueue('UserName');">Group By Clinician</a></li>
    </ul>
</div>
<div id="printQueueContentId"><% Html.RenderPartial("~/Views/Agency/PrintQueue/Content.ascx", Model); %></div>
<div class="buttons abs_bottom">
    <%= Html.Hidden("CommandType", "Print", new {@id="PrintQueueUpdate_Type" })%>
    <ul>
        <li><a href="javascript:void(0);" onclick="MarkAsPrinted('Print');">Mark As Printed</a></li>
    </ul>
</div>
<% } %>
<script type="text/javascript">
    $("#printQueueContentId .t-group-indicator").hide();
    $("#printQueueContentId .t-grouping-header").remove();
    $(".t-grid-content", "#window_printqueue").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
    function MarkAsPrinted(type) {
        if ($("input[name=CustomValue]:checked").length > 0) {
            Schedule.BulkUpdate('#printQueueForm');
        } else { $.jGrowl("Select at least one item to mark as printed.", { theme: 'error', life: 5000 }); }
    }
</script>