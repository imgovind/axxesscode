﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","caseManagement","Quality Assurance (QA) Center",Current.AgencyName)%>
<%  using (Html.BeginForm("BulkUpdate", "Schedule", FormMethod.Post, new { @id = "caseManagementForm", @style = "width: 100%; height: 100%;" })) { %>
<div class="buttons abs_top">
    <table><tr><td><label class="float_left">Branch:</label><div class="float_left"><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchId", ViewData["BranchId"].ToString(), new { @id = "CaseManagement_BranchCode" })%></div></td><td><label  class="float_left">Status:</label><div class="float_left"><select id="CaseManagement_Status" name="StatusId" class="PatientStatusDropDown"><option value="0">All</option><option value="1" selected>Active</option><option value="2">Discharged</option></select></div></td> <td><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Agency.loadCaseManagement('<%=ViewData["GroupName"]%>');">Generate</a></li></ul></div></td></tr></table>   
    <ul class="float_right"><li><a href="javascript:void(0);" onclick="Agency.loadCaseManagement('<%=ViewData["GroupName"]%>');">Refresh</a></li><li><%= Html.ActionLink("Export to Excel", "QAScheduleList", "Export", new { BranchId = ViewData["BranchId"], Status = 1 }, new { id = "CaseManagement_ExportLink" })%></li></ul>
    <ul>
        <li><a href="javascript:void(0);" onclick="Agency.loadCaseManagement('PatientName');">Group By Patient</a></li>
        <li><a href="javascript:void(0);" onclick="Agency.loadCaseManagement('EventDate');">Group By Date</a></li>
        <li><a href="javascript:void(0);" onclick="Agency.loadCaseManagement('DisciplineTaskName');">Group By Task</a></li>
        <li><a href="javascript:void(0);" onclick="Agency.loadCaseManagement('UserName');">Group By Clinician</a></li>
    </ul>
</div>
<div id="caseManagementContentId"><% Html.RenderPartial("~/Views/Agency/QA/CaseManagementContent.ascx", Model); %></div>
<div class="buttons abs_bottom"><%= Html.Hidden("CommandType", "", new {@id="BulkUpdate_Type" })%><ul><li><a href="javascript:void(0);" onclick="BulkUpdate('Approve');">Approve Selected</a></li><li><a href="javascript:void(0);" onclick="BulkUpdate('Return');">Return Selected</a></li></ul></div>
<% } %>
<script type="text/javascript">
    $("#caseManagementContentId .t-group-indicator").hide();
    $("#caseManagementContentId .t-grouping-header").remove();
    $(".t-grid-content", "#window_caseManagement").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
    function BulkUpdate(type) {if ($("input[name=CustomValue]:checked").length > 0) { $("#BulkUpdate_Type").val(type); Schedule.BulkUpdate('#caseManagementForm'); } else { $.jGrowl("Select at least one item to approve or return.", { theme: 'error', life: 5000 }); }}
</script>