﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<PatientEpisodeEvent>>" %>
<%var data = ViewData["GroupName"].ToString();%>
<%= Html.Hidden("CaseManagement_GroupName",data)%>
<%= Html.Telerik().Grid(Model).Name("caseManagementGrid").Columns(columns =>
{
    columns.Bound(s => s.CustomValue).Template(s => string.Format("<input name='CustomValue' type='checkbox' value='{0}'/>", s.CustomValue)).ClientTemplate("<input name='CustomValue' type='checkbox' value='<#= CustomValue #>'/>").Title("").Width(50).HtmlAttributes(new { style = "text-align:center" }).Sortable(false);
    columns.Bound(s => s.PatientName);
    columns.Bound(s => s.EventDate).Width(100);
    columns.Bound(s => s.TaskName).Template(s => s.PrintUrl).Title("Task").ClientTemplate("<#=PrintUrl#>").Title("Task").Width(250);
    columns.Bound(s => s.Status).Width(200);
    columns.Bound(s => s.RedNote).Title(" ").Width(30).Template( s=>s.RedNote.IsNotNullOrEmpty()?  string.Format("<a class=\"tooltip red_note\" href=\"javascript:void(0);\" tooltip='{0}'> </a>",s.RedNote):string.Empty).ClientTemplate("<a class=\"tooltip red_note\" href=\"javascript:void(0);\" tooltip=\"<#=RedNote#>\"> </a>");
    columns.Bound(s => s.YellowNote).Title(" ").Width(30).Template(s => s.YellowNote.IsNotNullOrEmpty()? string.Format("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip='{0}' ></a>", s.YellowNote):string.Empty).ClientTemplate("<a class=\"tooltip\" href=\"javascript:void(0);\" tooltip=\"<#=YellowNote#>\"></a>");
    columns.Bound(s => s.BlueNote).Title(" ").Width(30).Template(s => s.BlueNote.IsNotNullOrEmpty()? string.Format("<a class=\"tooltip blue_note\" href=\"javascript:void(0);\" tooltip='{0}'></a>", s.BlueNote):string.Empty).ClientTemplate("<a class=\"tooltip blue_note\" href=\"javascript:void(0);\" tooltip=\"<#=BlueNote#>\"></a>");
    columns.Bound(s => s.UserName).Width(150);
})
        .Groupable(settings => settings.Groups(groups =>
        {
            
            if (data == "PatientName")
            {
                groups.Add(s => s.PatientName);
            }
            else if (data == "EventDate")
            {
                groups.Add(s => s.EventDate);
            }
            else if (data == "DisciplineTaskName")
            {
                groups.Add(s => s.TaskName);
            }
            else if (data == "UserName")
            {
                groups.Add(s => s.UserName);
            }
            else
            {
                groups.Add(s => s.EventDate);
            }
        })).ClientEvents(c => c.OnRowDataBound("Schedule.tooltip")).Scrollable().Footer(false)%>
<script type="text/javascript">Agency.tooltip();</script>