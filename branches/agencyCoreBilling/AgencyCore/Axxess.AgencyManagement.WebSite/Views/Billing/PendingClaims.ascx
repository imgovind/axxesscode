﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "pendingClaim",
        "Pending Claims",
        Current.AgencyName)%>
<div class="trical">
 <span class="strong">Branch:</span><%= Html.DropDownList("BranchId", Model.Branches, new { @id = "PendingClaim_BranchCode", @class = "valid" })%> <span><%= Html.DropDownList("PrimaryInsurance", Model.Insurances, new { @id = "PendingClaim_PrimaryInsurance", @class = "Insurances" })%></span>
 <div class="buttons editeps" ><ul> <li><a href="javascript:void(0);" onclick="Billing.ReLoadPendingClaimRap($('#PendingClaim_BranchCode').val(), $('#PendingClaim_PrimaryInsurance').val()); Billing.ReLoadPendingClaimFinal($('#PendingClaim_BranchCode').val(), $('#PendingClaim_PrimaryInsurance').val());">Refresh</a></li></ul></div></div>
<div id="pendingClaim_rapContent"><%Html.RenderPartial("/Views/Billing/PendingClaimRap.ascx",Model); %></div>
<div id="pendingClaim_finalContent"><% Html.RenderPartial("/Views/Billing/PendingClaimFinal.ascx", Model); %></div>
<script type="text/javascript">
    $('#PendingClaim_BranchCode').change(function() { Billing.ReLoadPendingClaimRap($(this).val(), $('#PendingClaim_PrimaryInsurance').val()); Billing.ReLoadPendingClaimFinal($(this).val(), $('#PendingClaim_PrimaryInsurance').val()); });
    $('#PendingClaim_PrimaryInsurance').change(function() { Billing.ReLoadPendingClaimRap($('#PendingClaim_BranchCode').val(), $(this).val()); Billing.ReLoadPendingClaimFinal($('#PendingClaim_BranchCode').val(), $(this).val()); }); 
</script>