﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RemittanceLean>>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","remittances","Remittance",Current.AgencyName)%>
<div class="wrapper grid-bg">
  <table style="width:100%">
     <tr><td colspan="2"><div class="float_left"><label class="float_left">Remittance Date Range:</label><div class="float_right"><input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="BillingRemittance_StartDate" class="shortdate" /> To <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="BillingRemittance_EndDate" class="shortdate" /></div></div><div class="buttons float_left"><ul><li><a href="javascript:void(0);" onclick="Billing.RemittanceContentReload();">Generate</a></li></ul></div></td></tr>
     <tr>
       <td><div class="float_left buttons"><ul><li><%= Html.ActionLink("Print", "RemittancesPdf", "Billing", new { StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { id = "BillingRemittance_PrintLink", @target = "_blank" })%></li></ul></div></td>
       <td><div class="float_right"><% using (Html.BeginForm("RemittanceUpload", "Billing", FormMethod.Post, new { @id = "externalRemittanceUploadForm", @class = "align_right" })) { %><label class="strong">Upload a Remittance File:</label><input id="Billing_ExternalRemittanceUpload" type="file" name="Upload" /><div class="buttons float_right"><ul><li><a href="javascript:void(0)" onclick="$(this).closest('form').submit()">Upload The File</a></li></ul></div><% } %></div></td>
     </tr>
  </table>
    <div id="BillingRemittance_Content" class="billing remlist"><%Html.RenderPartial("RemittanceContent", Model); %></div>
</div>    
<script type="text/javascript">
    $("#BillingRemittance_Content ol li:first").addClass("first");
    $("#BillingRemittance_Content ol li:last").addClass("last");
</script>
