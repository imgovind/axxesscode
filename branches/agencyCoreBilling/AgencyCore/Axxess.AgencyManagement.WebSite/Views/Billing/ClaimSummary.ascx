﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<BillLean>" %>
<% using (Html.BeginForm("Generate", "Billing", FormMethod.Post, new { @id = "generateBilling" }))%><% { %>
<%if ((Model.Finals != null && Model.Finals.Count > 0) || (Model.Raps != null && Model.Raps.Count > 0)){%>
<%} else {%><div class="row"><b>Select the claim you want to generate.</b></div><%} %>
<% if (Model != null){ %>
<% if (Model.Raps != null && Model.Raps.Count > 0){%>
<div class="row">
    <table class="claim" width="100%">
        <thead><tr><th>Patient Name</th><th>Medicare No</th><th>Episode</th><th>Claim Amount</th></tr></thead>
        <tbody>
            <%int j = 1;%>
            <% foreach (var rap in Model.Raps){%>
            <% if (j % 2 == 0){%>
            <tr class="even"><%}
               else{ %><tr class="odd"><%} %>
                    <td><%=string.Format("<input type='hidden' id='rapSelected{0}' name='rapSelected' value='{0}' />", rap.Id)%> <%=j %>.&#160;&#160;&#160;<%=rap.DisplayName + "(" + rap.PatientIdNumber + ")"%></td>
                    <td><%=rap.MedicareNumber%></td>
                    <td align="center"><% if (rap.EpisodeStartDate != null) {%><%=rap.EpisodeStartDate.IsValid()? rap.EpisodeStartDate.ToString("MM/dd/yyyy"):string.Empty%><%} %> <% if (rap.EpisodeEndDate != null){%>-<%=rap.EpisodeEndDate .IsValid()?rap.EpisodeEndDate.ToString("MM/dd/yyyy"):string.Empty%><%} %></td>
                    <td> <%=rap.ProspectivePay %></td>
                </tr><% j++;} %>
        </tbody>
    </table>
</div><%} %>
<% if (Model.Finals != null && Model.Finals.Count > 0){%>
<div class="row">
    <table class="claim">
        <thead><tr><th>Patient Name</th><th>Medicare No</th><th>Episode Date</th><th>Claim Amount</th></tr></thead>
        <tbody>
            <% int j = 1;%>
            <% foreach (var final in Model.Finals){%>
            <% if (j % 2 == 0){%><tr class="even"><%}else{ %>
                <tr class="odd"><%} %>
                    <td><%=string.Format("<input type='hidden' id='finalSelected{0}' name='finalSelected' value='{0}' />", final.Id)%><%=j %>.&#160;&#160;&#160;<%=final.DisplayName + "(" + final.PatientIdNumber + ")"%></td>
                    <td><%=final.MedicareNumber%></td>
                    <td align="center"><% if (final.EpisodeStartDate != null){%><%=final.EpisodeStartDate.IsValid()? final.EpisodeStartDate.ToString("MM/dd/yyyy"): string.Empty%> <%} %><% if (final.EpisodeEndDate != null){%>-<%=final.EpisodeEndDate.IsValid() ? final.EpisodeEndDate.ToString("MM/dd/yyyy") : string.Empty%><%} %></td>
                    <td><%=final.ProspectivePay %></td>
                </tr>
                <% j++;} %>
        </tbody>
    </table>
</div><% }%><div class="clear"></div><% } %>
<%if ((Model.Finals != null && Model.Finals.Count > 0) || (Model.Raps != null && Model.Raps.Count > 0)){%>
<% = Html.Hidden("StatusType","Submit") %>
<% = Html.Hidden("BranchId", Model.BranchId)%>
<% = Html.Hidden("PrimaryInsurance", Model.Insurance)%>
<div class="buttons"><ul class=""><% if (Model.IsElectronicSubmssion){ %><li class="align_left"> <a href="javascript:void(0);" onclick="Billing.SubmitClaimDirectly('#generateBilling');">Submit Electronically</a></li><%} else{ %><li class="align_left"> <a href="javascript:void(0);" onclick="Billing.LoadGeneratedClaim('#generateBilling');">Download Claim(s)</a></li><%}%></ul></div>
 <% if (!Model.IsElectronicSubmssion){ %><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="Billing.UpdateStatus('#generateBilling');">Mark Claim(s) As Submitted</a></li></ul></div><%} %>
<%}%><%}%>
