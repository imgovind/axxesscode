﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimBill>>" %>
<ul>
    <li class="align_center"><h3>FINAL(S)</h3></li>
    <li>
        <span class="rapcheck"></span>
        <span class="rapname pointer" onclick="Billing.Sort('Final', 'rapname');">Patient Name</span>
        <span class="rapid pointer" onclick="Billing.Sort('Final', 'rapid');">MR#</span>
        <span class="rapeps pointer" onclick="Billing.Sort('Final', 'rapeps');">Episode Date</span>
        <span class="finalicon">RAP</span>
        <span class="finalicon">Visit</span>
        <span class="finalicon">Order</span>
        <span class="finalicon">Verified</span>
    </li>
</ul><ol><%
if (Model != null) {
    var finals = Model.Where(c =>c.EpisodeEndDate < DateTime.Now);
    int i = 1;
    foreach (var final in finals) { %>
    <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd " : "even ") + (final.IsRapGenerated ? "ready" : "notready"))%>
        <span class="rapcheck"><%= i %>. <%= (final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified && final.AreOrdersComplete && final.IsRapGenerated) ? "<input name='FinalSelected' class='radio' type='checkbox' value='" + final.Id + "' id='FinalSelected" + final.Id + "' />" : string.Empty %></span>
        <%= String.Format("<a class='float_right' href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/FinalPdf', {{ 'episodeId': '{0}', 'patientId': '{1}' }});\"><span class='img icon print'></span></a>", final.EpisodeId, final.PatientId) %>
        <a href='javascript:void(0);' onclick="<%= (final.IsRapGenerated) ? "UserInterface.ShowFinal('" + final.EpisodeId + "','" + final.PatientId + "');\" title=\"Final Ready" : "U.growl('Error: Rap Not Generated', 'error');\" title=\"Not Complete" %>">
            <span class="rapname"><%= final.LastName %>, <%= final.FirstName %></span>
            </a>
            <span class="rapid"><%= final.PatientIdNumber%></span>
            <span class="rapeps"><span class="very_hidden"><%= final.EpisodeStartDate != null ? final.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span><%= final.EpisodeStartDate != null ? final.EpisodeStartDate.ToString("MM/dd/yyy") : string.Empty %><%= final.EpisodeEndDate != null ? "&#8211;" + final.EpisodeEndDate.ToString("MM/dd/yyy") : string.Empty %></span>
            <span class="finalicon"><span class='img icon <%= final.IsRapGenerated ? "success_small" : "error_small" %>'></span></span>
            <span class="finalicon"><span class='img icon <%= final.AreVisitsComplete? "success_small" : "error_small" %>'></span></span>
            <span class="finalicon"><a href="javascript:void(0);" onclick="UserInterface.ShowEpisodeOrders('<%=final.EpisodeId %>','<%=final.PatientId %>');"><span class='img icon <%= final.AreOrdersComplete ? "success_small" : "error_small" %>'></span></a></span>
            <span class="finalicon"><span class='img icon <%= final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified ? "success_small" : "error_small" %>'></span></span>
        
    </li><%
        i++;
    }
} %>
</ol>
<script type="text/javascript">
    $("#Billing_CenterContentFinal ol li:first").addClass("first");
    $("#Billing_CenterContentFinal ol li:last").addClass("last");
</script>