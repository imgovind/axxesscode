﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimInfoSnapShotViewData>" %>
 <div class="twothirds" style="height:146px;"> 
      <table width="100%">
          <thead><tr><th colspan="2"><b><%=Model.Type %></b></th></tr> </thead>
          <tbody>
              <tr>
                  <td>
                      <table>
                          <tbody>
                              <tr><td class="align_right">Patient Name :</td><td ><label class="strong"> <%=Model.PatientName %></label></td></tr>
                              <tr><td class="align_right">Patient ID # :</td><td><label class="strong"><%=Model.PatientIdNumber %></label></td></tr>
                              <tr><td class="align_right">Medicare # :</td><td><label class="strong"><%=Model.MedicareNumber %></label></td></tr>
                              <tr><td class="align_right">Insurance/Payer :</td><td><label class="strong"><%=Model.PayorName %></label></td></tr>
                              <tr><td class="align_center"><div class="activity-log"><% = string.Format("<a href=\"javascript:void(0);\" onclick=\"Log.LoadClaimLog('{0}','{1}','{2}');\" >Activity Logs</a>",Model.Type,Model.Id, Model.PatientId)%></div></td></tr>
                          </tbody>
                      </table>
                  </td>
                  <td>
                   <% if (Model.Visible){ %>
                       <table>
                              <tbody>
                                  <tr><td class="align_left">HHRG(Grouper) :&#160;</td><td class="align_left"><label class="strong"><%=Model.HHRG%></label></td></tr>
                                  <tr><td class="align_left">HIPPS :&#160; </td><td class="align_left"><label class="strong"><%=Model.HIPPS%></label></td></tr>
                                  <tr><td class="align_left">AUTH :&#160;</td><td class="align_left"><label class="strong"><%=Model.ClaimKey%></label></td></tr>
                                  <tr><td class="align_left">Episode Payment Rate :&#160;</td><td class="align_left"><label class="strong"><%=Model.StandardEpisodeRate != 0 ? string.Format("{0:c}", Model.StandardEpisodeRate) : string.Empty %></label></td></tr>
                                  <tr><td class="align_left">Supply Reimbursement :&#160;</td><td class="align_left"><label class="strong"><%=Model.SupplyReimbursement != 0 ? string.Format("{0:c}", Model.SupplyReimbursement) : string.Empty %></label></td></tr>
                                  <tr><td class="align_left">Prospective Pay :&#160;</td><td class="align_left"> <label class="strong"><%= Model.ProspectivePay != 0 ? string.Format("{0:c}", Model.ProspectivePay) : string.Empty %></label></td></tr>
                             </tbody>
                       </table>
                    <%} %>
                  </td>
              </tr>
          </tbody>
      </table>
  </div>
 <div class="onethird encapsulation hidden"><h4>Billing Report For This Patient</h4>
       <ul>
            <li><a href="javascript:void(0);">PPS Analysis</a> </li>
            <li><a href="javascript:void(0);">Claim to be transmited</a> </li>
            <li><a href="javascript:void(0);">Claim transmitted and paid</a> </li>
            <li><a href="javascript:void(0);">Claim transmited only</a> </li>
       </ul>
  </div>
