﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimInfoDetail>>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","submittedclaimdetail","Submitted Claims Detail",Current.AgencyName)%>
<div class="wrapper">
<%= Html.Telerik().Grid(Model)
        .Name("ClaimInfoDetail_List")
        .Columns(columns =>
        {
            columns.Bound(o => o.DisplayName);
            columns.Bound(o => o.PatientIdNumber).Width(200);
            columns.Bound(o => o.Range).Title("Episode");
            columns.Bound(o => o.BillType).Format("{0:MM/dd/yyyy}").Width(120);
        }).Footer(false)
%>
</div>