﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Rap>" %>
<% using (Html.BeginForm("RapVerify", "Billing", FormMethod.Post, new { @id = "rapVerification" })) { %>
<% var conditionCodes = Model.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(Model.ConditionCodes) : null; %>
<% var diagnosis = Model.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(Model.DiagnosisCode) : null; %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","rap","Rap",Model != null ? (Model.LastName + ", " + Model.FirstName).ToTitleCase() : "")%>
<div class="wrapper main">
    <div class="req_legend"><span class="req_red">*</span> = Required Field</div>
    <div class="buttons"><ul><%= String.Format("<li><a href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/RapPdf', {{ 'episodeId': '{0}', 'patientId': '{1}' }});\">Print</a></li>", Model.EpisodeId, Model.PatientId)%></ul></div>
    <fieldset>
        <legend>Patient</legend>
        <%= Html.Hidden("Id",Model.Id) %>
        <%= Html.Hidden("PatientId", Model.PatientId)%>
        <%= Html.Hidden("IsMedicareHMO", Model.IsMedicareHMO)%>
        <div class="column">
            <div class="row"><label for="FirstName" class="float_left">Patient First Name:</label><div class="float_right"><%= Html.TextBox("FirstName", Model.FirstName, new { @class = "text {validate:{required:true}}", @maxlength = "20" }) %></div></div>
            <div class="row"><label for="LastName" class="float_left">Patient Last Name:</label><div class="float_right"><%= Html.TextBox("LastName", Model.LastName, new { @class = "text {validate:{required:true}}", @maxlength = "20" }) %></div></div>
            <div class="row"><label for="MedicareNumber" class="float_left">Medicare #:</label><div class="float_right"><%= Html.TextBox("MedicareNumber", Model.MedicareNumber, new { @class = "text required", @maxlength = "11" }) %></div></div>
            
            <div class="row"><label for="PrimaryInsuranceId" class="float_left">Insurance:</label><div class="float_right"><%= Html.InsurancesMedicare("PrimaryInsuranceId", (Model != null ? Model.PrimaryInsuranceId.ToString() : "0"), true, "-- Select Insurnace --", new { @class = "requireddropdown", @id = "Rap_PrimaryInsuranceId" })%></div></div>
           
            <div id="Rap_MedicareHMOInformations">
             <div class="row"><label for="HealthPlanId" class="float_left">Primary Insurance Health Plan Id:</label><div class="float_right"><%= Html.TextBox("HealthPlanId", Model.HealthPlanId, new { @class = "hmoflag" })%></div></div>
             <div class="row"><label for="AuthorizationNumber" class="float_left">Authorization Number:</label><div class="float_right"><%= Html.TextBox("AuthorizationNumber", Model.AuthorizationNumber, new { @class = "text input_wrapper hmoflag", @maxlength = "30" })%></div></div>
            </div>
           
            <div class="row"><label for="PatientIdNumber" class="float_left">Patient Record #:</label><div class="float_right"><%= Html.TextBox("PatientIdNumber", Model.PatientIdNumber, new { @class = "text required", @maxlength = "11" }) %></div></div>
            <div class="row"><label for="Gender" class="float_left">Gender:</label><div class="float_right"><%= Html.RadioButton("Gender", "Female", Model.Gender == "Female" ? true : false, new { @id = "GenderFemale", @class = "radio required" })%><label for="GenderFemale" class="inlineradio">Female</label><%= Html.RadioButton("Gender", "Male", Model.Gender == "Male" ? true : false, new { @id = "GenderMale", @class = "radio required" })%><label for="GenderMale" class="inlineradio">Male</label></div></div>
            <div class="row"><label for="DOB" class="float_left">Date of Birth:</label><div class="float_right"><input type="date" name="DOB" value="<%= Model.DOB.ToShortDateString() %>" class="required" id="Rap_DOB" /></div></div>
            <div class="row"><label for="Type" class="float_left">Bill Type:</label><% var billType = new SelectList(new[] {new SelectListItem { Text = "Initial Rap", Value = "0" },new SelectListItem { Text = "Rap Cancellation", Value = "1" }}, "Value", "Text",Model.Type); %><div class="float_right"><%= Html.DropDownList("Type", billType)%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="StartOfCareDate" class="float_left">Admission Date:</label><div class="float_right"><input type="date" name="StartOfCareDate" value="<%= Model.StartofCareDate.ToShortDateString() %>" class="required" id="Rap_StartOfCareDate" /></div></div>
            <div class="row"><label for="AdmissionSource" class="float_left">Admission Source:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.AdmissionSources, "AdmissionSource", (Model != null && Model.AdmissionSource.IsNotNullOrEmpty() && int.Parse(Model.AdmissionSource) > 0) ? Model.AdmissionSource.ToString() : "", new { @class = "AdmissionSource requireddropdown" })%></div></div>
            <div class="row"><label for="PatientStatus" class="float_left">Patient Status:</label><div class="float_right"><%=Html.UB4PatientStatus("UB4PatientStatus", Model.UB4PatientStatus, new { @id = "Rap_PatientStatus", @class = "requireddropdown" })%></div></div>
            <div class="row" id="RapPatientStatusRow" style="<%= Model.UB4PatientStatus != "30" && Model.UB4PatientStatus != "0"  ? "" : "display:none;" %>"><label for="DischargeDate" class="float_left">Discharge Date:</label><div class="float_right"><input type="date" name="DischargeDate" value="<%= Model.DischargeDate.ToShortDateString() %>" id="Rap_DischargeDate" /></div></div>
            <div class="row"><label for="AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%= Html.TextBox("AddressLine1",Model.AddressLine1 , new { @class = "text required" }) %></div></div>
            <div class="row"><label for="AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%= Html.TextBox("AddressLine2",Model.AddressLine2, new { @class = "text" }) %></div></div>
            <div class="row"><label for="AddressCity" class="float_left">City:</label><div class="float_right"><%= Html.TextBox("AddressCity",Model.AddressCity, new { @class = "text required" }) %></div></div>
            <div class="row"><label for="AddressStateCode" class="float_left">State, Zip Code:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @class = "input_wrapper AddressStateCode" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @class = "required digits isValidUSZip zip", @maxlength = "5" }) %></div></div>
        </div>
        <div class="wide_column">
            <label class="float_left">Condition Codes:</label>
            <div class="clear"></div>
            <table>
                 <tbody>
                      <tr>
                            <td><label class="float_left">18.</label><div class="float_left"><%= Html.TextBox("ConditionCode18", conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                            <td><label class="float_left">19.</label><div class="float_left"><%= Html.TextBox("ConditionCode19", conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                            <td><label class="float_left">20.</label><div class="float_left"><%= Html.TextBox("ConditionCode20", conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                            <td><label class="float_left">21.</label><div class="float_left"><%= Html.TextBox("ConditionCode21", conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                            <td><label class="float_left">22.</label><div class="float_left"><%= Html.TextBox("ConditionCode22", conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                            <td><label class="float_left">23.</label><div class="float_left"><%= Html.TextBox("ConditionCode23", conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                            <td><label class="float_left">24.</label><div class="float_left"><%= Html.TextBox("ConditionCode24", conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                            <td><label class="float_left">25.</label><div class="float_left"><%= Html.TextBox("ConditionCode25", conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                            <td><label class="float_left">26.</label><div class="float_left"><%= Html.TextBox("ConditionCode26", conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                            <td><label class="float_left">27.</label><div class="float_left"><%= Html.TextBox("ConditionCode27", conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                            <td><label class="float_left">28.</label><div class="float_left"><%= Html.TextBox("ConditionCode28", conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : "", new { @class = "sn", @maxlength = "2" })%></div></td>
                      </tr>
                 </tbody>
            </table>
        </div>
    </fieldset>
    <fieldset>
        <legend>Episode</legend>
        <div class="column">
            <%= Html.Hidden("AssessmentType", Model.AssessmentType)%>
            <div class="row"><label for="HippsCode" class="float_left">HIPPS Code:</label><div class="float_right"><%= Html.TextBox("HippsCode", Model.HippsCode, new { @class = "text required", @maxlength = "20" }) %></div></div>
            <div class="row"><label for="ClaimKey" class="float_left">OASIS Matching Key:</label><div class="float_right"><%= Html.TextBox("ClaimKey", Model.ClaimKey, new { @class = "text required", @maxlength = "18" }) %></div></div>
            <div class="row"><label for="EpisodeStartDate" class="float_left">Episode Start Date:</label><div class="float_right"><input type="date" name="EpisodeStartDate" value="<%= Model.EpisodeStartDate.ToShortDateString() %>" class="required" id="Rap_EpisodeStartDate" /></div></div>
            <div class="row">
                <span class="float-right">Recommended / Previously Entered First Billable Date: <%=Model.FirstBillableVisitDateFormat %></span>
                <div class="clear"></div>
                <label for="FirstBillableVisitDateFormatInput" class="float_left">Date Of First Billable Visit:</label><div class="float_right"><input type="date" name="FirstBillableVisitDateFormatInput" value="<%= Model.FirstBillableVisitDateFormat %>" mindate="<%= Model.EpisodeStartDate.ToShortDateString() %>" maxdate="<%= Model.EpisodeEndDate.ToShortDateString() %>" class="required" id="Rap_FirstBillableVisitDateFormat" /></div>
                <br />
                <em>Please Verifiy the first billable visit date from the schedule.</em>
                <div class="buttons float_right"><ul><li><a href="javascript:void(0);" onclick="UserInterface.ShowScheduleCenter('<%= Model.PatientId %>');">View Schedule</a></li></ul></div>
            </div>
            <div class="row"><label for="PhysicianLastName" class="float_left">Physician Last Name:</label><div class="float_right"><%= Html.TextBox("PhysicianLastName", Model.PhysicianLastName, new { @class = "text required", @maxlength = "20" })%></div></div>
            <div class="row"><label for="PhysicianFirstName" class="float_left">Physician First Name:</label><div class="float_right"><%= Html.TextBox("PhysicianFirstName", Model.PhysicianFirstName, new { @class = "text required", @maxlength = "20" })%></div></div>
            <div class="row"><label for="PhysicianNPI" class="float_left">Physician NPI #:</label><div class="float_right"><%= Html.TextBox("PhysicianNPI", Model.PhysicianNPI, new { @class = "text required", @maxlength = "20" }) %></div></div>
            <div class="row"><label for="ProspectivePay" class="float_left">HIPPS Code Payment:</label><div class="float_right"><%= Html.TextBox("ProspectivePay", Model.ProspectivePay, new { @class = "text ", @maxlength = "20" })%></div></div>
        </div>
        <div class="column">
            <div class="row">
                <div><strong>Diagnosis Codes:</strong></div>
                <div class="margin">
                    <label for="Primary" class="float_left">Primary</label><div class="float_right"><%= Html.TextBox("Primary", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : "")%></div>
                    <div class="clear"></div>
                    <label for="Second" class="float_left">Second</label><div class="float_right"><%= Html.TextBox("Second", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : "")%></div>
                    <div class="clear"></div>
                    <label for="Third" class="float_left">Third</label><div class="float_right"><%= Html.TextBox("Third", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : "")%></div>
                    <div class="clear"></div>
                    <label for="Fourth" class="float_left">Fourth</label><div class="float_right"><%= Html.TextBox("Fourth", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : "")%></div>
                    <div class="clear"></div>
                    <label for="Fifth" class="float_left">Fifth</label><div class="float_right"><%= Html.TextBox("Fifth", diagnosis != null && diagnosis.Element("code5") != null ? diagnosis.Element("code5").Value : "")%></div>
                    <div class="clear"></div>
                    <label for="Sixth" class="float_left">Sixth</label><div class="float_right"><%= Html.TextBox("Sixth", diagnosis != null && diagnosis.Element("code6") != null ? diagnosis.Element("code6").Value : "")%></div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="row"><label for="RapRemark">Remark:</label><%= Html.TextArea("Remark", Model.Remark, new {  }) %></div>
        </div>
    </fieldset>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Verify</a></li><%= String.Format("<li><a href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/RapPdf', {{ 'episodeId': '{0}', 'patientId': '{1}' }});\">Print</a></li>", Model.EpisodeId, Model.PatientId)%></ul></div>
    <% } %>
</div>
<script type="text/javascript">
    $(".row :input.required,.row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    if ($("#Rap_PatientStatus").val() != '30' && $("#Rap_PatientStatus").val() !== "0") { $("#Rap_DischargeDate").addClass("required"); }
    if ($("#Rap_PrimaryInsuranceId").val() > 1000 ) { $("#Rap_MedicareHMOInformations").show(); $("#Rap_MedicareHMOInformations input.hmoflag").addClass("required"); } else { $("#Rap_MedicareHMOInformations").hide(); $("#Rap_MedicareHMOInformations input.hmoflag").removeClass("required"); }
    $("#Rap_PatientStatus").change(function() { if ($(this).val() != '30' && $(this).val()  !== "0") { $("#RapPatientStatusRow").show(); $("#Rap_DischargeDate").addClass("required"); } else { $("#RapPatientStatusRow").hide(); $("#Rap_DischargeDate").removeClass("required"); } });
    $("#Rap_PrimaryInsuranceId").change(function() { if ($(this).val() >1000 ) { $("#Rap_MedicareHMOInformations").show(); $("#Rap_MedicareHMOInformations input.hmoflag").addClass("required"); } else { $("#Rap_MedicareHMOInformations").hide(); $("#Rap_MedicareHMOInformations input.hmoflag").removeClass("required"); } });
</script>