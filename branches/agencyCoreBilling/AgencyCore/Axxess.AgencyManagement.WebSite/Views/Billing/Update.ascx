﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ClaimViewData>" %>
<% using (Html.BeginForm("SaveUpdate", "Billing", FormMethod.Post, new { @id = "updateClaimForm" })) { %>
<div class="form_wrapper">
    <%= Html.Hidden("Id",Model.Id) %>
    <%= Html.Hidden("PatientId",Model.PatientId) %>
    <%= Html.Hidden("Type",Model.Type) %>
    <fieldset>
        <legend>Update Information</legend>
        <div class="column">
            <%  var status = new SelectList(new[] {
                    new SelectListItem { Value = "300", Text = "Claim Created" },
                    new SelectListItem { Value = "305", Text = "Claim Submitted" },
                    new SelectListItem { Value = "310", Text = "Claim Rejected" },
                    new SelectListItem { Value = "315", Text = "Payment Pending" },
                    new SelectListItem { Value = "320", Text = "Claim Accepted/Processing" },
                    new SelectListItem { Value = "325", Text = "Claim With Errors" },
                    new SelectListItem { Value = "330", Text = "Paid Claim" },
                    new SelectListItem { Value = "335", Text = "Cancelled Claim" }
                }, "Value", "Text", Model.Status); %>
            <div class="row"><label for="Status" class="float_left">Claim Status:</label><div class="float_right"><%= Html.DropDownList("Status", status)%></div></div>
            <div class="row"><label for="PrimaryInsuranceId" class="float_left">Insurance:</label><div class="float_right"><%= Html.InsurancesMedicare("PrimaryInsuranceId", Model.PrimaryInsuranceId.ToString(), true, "Unassigend Insurance", new {  @class = "Insurances" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="PaymentAmount" class="float_left">Payment Amount:</label><div class="float_right">$<%= Html.TextBox("PaymentAmount", Model.PaymentAmount>0?Model.PaymentAmount.ToString():"", new {@class = "text input_wrapper", @maxlength = "" })%></div></div>
            <div class="row"><label for="PaymentDateValue" class="float_left">Payment Date:</label><div class="float_right"><input type="date" name="PaymentDateValue" value="<%= Model.PaymentDate <= DateTime.MinValue ? DateTime.Now.ToShortDateString() : Model.PaymentDate.ToShortDateString() %>" /></div></div>
        </div>
         <div class="wide_column"><label for="Comment" class="float_left">Comment:</label><div class="row"><%= Html.TextArea("Comment",Model.Comment) %></div></div>
        
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Save</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
        </ul>
    </div>
</div>
<% } %>