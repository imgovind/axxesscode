﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ClaimBill>>" %>
<ul>
    <li class="align_center"><h3>RAP(S)</h3></li>
    <li>
        <span class="rapcheck"></span>
        <span class="rapname pointer" onclick="Billing.Sort('Rap', 'rapname');">Patient Name</span>
        <span class="rapid pointer" onclick="Billing.Sort('Rap', 'rapid');">MR#</span>
        <span class="rapeps pointer" onclick="Billing.Sort('Rap', 'rapeps');">Episode Date</span>
        <span class="rapicon">OASIS</span>
        <span class="rapicon">Billable Visit</span>
        <span class="rapicon">Verified</span>
    </li>
</ul><ol><%
if (Model != null) {
    int i = 1;
    foreach (var rap in Model) { %>
    <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd " : "even ") + (rap.IsOasisComplete && rap.IsFirstBillableVisit ? "ready" : "notready")) %>
        <span class="rapcheck"><%= i %>. <%=  rap.IsOasisComplete &&  rap.IsFirstBillableVisit && rap.IsVerified ? "<input name='RapSelected' class='radio' type='checkbox' value='" + rap.Id + "' id='RapSelected" + rap.Id + "' />" : "" %></span>
        <%= String.Format("<a class='float_right' href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/RapPdf', {{ 'episodeId': '{0}', 'patientId': '{1}' }});\"><span class='img icon print'></span></a>", rap.EpisodeId, rap.PatientId) %>
        <a href='javascript:void(0);' onclick="<%= (rap.IsOasisComplete && rap.IsFirstBillableVisit) ? "UserInterface.ShowRap('" + rap.EpisodeId + "','" + rap.PatientId + "');" : "U.growl('Error: Not Completed', 'error');" %>">
            <span class="rapname"><%= rap.LastName %>, <%= rap.FirstName %></span>
             </a>
            <span class="rapid"><%= rap.PatientIdNumber%></span>
            <span class="rapeps"><span class="very_hidden"><%= rap.EpisodeStartDate != null ? rap.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span><%= rap.EpisodeStartDate != null ? rap.EpisodeStartDate.ToString("MM/dd/yyyy") : string.Empty %><%= rap.EpisodeEndDate != null ? "&#8211;" + rap.EpisodeEndDate.ToString("MM/dd/yyyy") : string.Empty %></span>
            <span class="rapicon"><span class='img icon <%= rap.IsOasisComplete ? "success_small" : "error_small" %>'></span></span>
            <span class="rapicon"><span class='img icon <%= rap.IsFirstBillableVisit ? "success_small" : "error_small" %>'></span></span>
            <span class="rapicon"><span class='img icon <%= rap.IsOasisComplete && rap.IsFirstBillableVisit && rap.IsVerified ? "success_small" : "error_small" %>'></span></span>
       
    </li><%
        i++;
    }
} %>
</ol>
<script type="text/javascript">
    $("#Billing_CenterContentRap ol li:first").addClass("first");
    $("#Billing_CenterContentRap ol li:last").addClass("last");
</script>
