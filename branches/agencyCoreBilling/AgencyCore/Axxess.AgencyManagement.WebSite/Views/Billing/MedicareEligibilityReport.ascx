﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>",
        "medicareeligibilityreport",
        "Medicare Eligibility Report",
        Current.AgencyName) %>
<div id="MedicareEligiblity_Report" class="main wrapper">
<div class="trical">
    <span class="strong">Patient:&nbsp;</span><%= Html.Patients("Patients", Guid.Empty.ToString(), (int)PatientStatus.Active, "-- Select Patient --", new { @id = "MedicareEligiblity_Report_Patients", @class = "report_input valid" })%> 
    <div class="buttons editeps" ><ul>
    <li><a href="javascript:void(0);" onclick="Billing.ReloadEligibilityList($('#MedicareEligiblity_Report_Patients').val());">Refresh</a></li>
    </ul></div>
</div>
    <div class="billing">
        <div id="MedicareEligiblity_ReportContent"></div>
    </div>
</div>
<script type="text/javascript">
    $('#MedicareEligiblity_Report_Patients').change(function() {
        Billing.ReloadEligibilityList($(this).val());
    });
</script>