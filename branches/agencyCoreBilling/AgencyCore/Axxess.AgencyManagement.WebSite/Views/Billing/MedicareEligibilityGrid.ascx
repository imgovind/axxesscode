﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<MedicareEligibility>>" %>
<ul>
    <li>
        <span class="medeligtask">Task</span>
        <span class="medeligepisode">Episode</span>
        <span class="medeligassigned">Assigned</span>
        <span class="medeligstatus">Status</span>
        <span class="medeligcreated">Date</span>
        <span class="medeligicon"></span>
    </li>
</ul><ol><%
if (Model != null) {
    int i = 1;
    if (Model.Count > 0) {
        foreach (var item in Model) { %>
            <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd" : "even")) %>
                <span class="medeligtask"><%= item.TaskName %></span>
                <span class="medeligepisode"><%= item.EpisodeRange %></span>
                <span class="medeligassigned"><%= item.AssignedTo %></span>
                <span class="medeligstatus"><%= item.StatusName %></span>
                <span class="medeligcreated"><%= item.Created.ToShortDateString().ToZeroFilled() %></span>
                <span class="medeligicon"><%= item.PrintUrl %></span>
            </li><%
            i++;
        }
    } else { %>
            <li class="align_center"><span class="darkred">None</span></li>
<% } 
} %>
</ol>
<script type="text/javascript">
    $("#Billing_CenterContentRap ol li:first").addClass("first");
    $("#Billing_CenterContentRap ol li:last").addClass("last");
</script>
