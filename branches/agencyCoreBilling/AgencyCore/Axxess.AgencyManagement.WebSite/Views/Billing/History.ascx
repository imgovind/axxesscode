﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","billingHistory","Billing History",Current.AgencyName)%>
<div class="wrapper layout">
    <div class="layout_left">
        <div class="top" style="margin-top: 10px;">
            <div class="row"><label>Branch:</label><div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @class = "billingBranchCode" })%></div></div>
            <div class="row"><label>View:</label><div><select name="list" class="billingStatusDropDown"><option value="1">Active Patients</option><option value="2">Discharged Patients</option></select></div></div>
            <div class="row"><label>Filter:</label><div><%= Html.DropDownList("list", Model.Insurances, new {  @class = "billingPaymentDropDown" })%></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_billing_Selection" class="text" name="" value="" type="text" /></div></div>
        </div>
        <div class="bottom" style="top: 110px;"><%Html.RenderPartial("PatientList", Model.SelecetdInsurance); %></div>
    </div>
    <div id="billingMainResult" class="layout_main">
        <div class="top">
            <div class="winmenu"><ul id="billingHistoryTopMenu"><li><a href="javascript:void(0);" onclick="UserInterface.ShowNewClaimModal('RAP');">New RAP</a></li><li><a href="javascript:void(0);" onclick="UserInterface.ShowNewClaimModal('Final');">New Final</a></li></ul></div>
            <div id="billingHistoryClaimData"></div>
        </div>
        <div class="bottom" style="top: 180px;"><% Html.RenderPartial("HistoryActivity", Guid.Empty); %></div>   
    </div>
</div>
<script type="text/javascript">
    $('#window_billingHistory .layout').layout({ west__paneSelector: '.layout_left' });
    $('#window_billingHistory .t-grid-content').css({ height: 'auto', top: "26px" });
</script>
<div id="Claim_Update_Container" class="claimupdatemodal hidden"></div>
<div id="Claim_New_Container" class="claimnewmodal hidden"></div>

