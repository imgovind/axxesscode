﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<RemittanceLean>>" %>
 <ul>
	        <li>
		    <span class="remNum"></span>
		    <span class="remId" >Remittance Id</span>
		    <span class="remDate">Remittance Date</span>
		    <span class="remAmount">Provider Payment</span>
		    <span class="remCount">Claim Count</span>
		    <span class="remAction">Action</span>
	        </li>
	    </ul>
	    <ol><%
	    int count = 1;
	    foreach (var remittance in Model) { %>
	        <%= string.Format("<li class=\"{0} ready\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", count % 2 != 0 ? "odd" : "even") %>
		        <span class="remNum"><%= count++ %>.</span>
		        <span class="remId"><%= remittance.RemitId %></span>
		        <span class="remDate"><%=  remittance.RemittanceDate > DateTime.MinValue ? remittance.RemittanceDate.ToString("MM/dd/yyyy") : string.Empty%></span>
		        <span class="remAmount"><%= string.Format("${0:#,0.00}", remittance.PaymentAmount) %></span>
		        <span class="remCount"><%= remittance.TotalClaims %></span>
		        <span class="remActionSub"><a href="javaScript:void(0);" onclick="UserInterface.ShowRemittanceDetail('<%= remittance.Id %>');">View Details</a></span>
		        <span class="remActionSub"><a href="javaScript:void(0);" onclick="Billing.RemittanceDelete('<%= remittance.Id %>');">Delete</a></span>
	        </li><%
	    } %>
	    </ol>
