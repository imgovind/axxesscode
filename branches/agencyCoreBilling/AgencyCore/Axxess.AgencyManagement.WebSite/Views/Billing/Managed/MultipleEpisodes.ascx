﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<SelectListItem>>" %>
 <div class="form_wrapper">
    <fieldset>
        <legend>Select Associated Episode</legend>
         <div class="wide_column">
            <span>To create this claim, you will need assessment, diagnosis and authorization information from an associated episode. Please select the episode from the list below to auto populate/pull the information.</span>
            <div class="row">
                <label  class="float_left">Select Episode:</label>
                <div class="float_right"><%= Html.DropDownList("Episode", Model.AsEnumerable(), new { @id="Managed_MultipleEpisodes_EpisodeId" })%></div>
            </div>
        </div>
    </fieldset>
     <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="SelectManagedClaimEpisode();">Select</a></li><li><a href="javascript:void(0);" onclick="UserInterface.CloseModal();">Cancel</a></li></ul></div>
</div>
<script type="text/javascript">
    function SelectManagedClaimEpisode() {
        var input = "input=" + $("#Managed_MultipleEpisodes_EpisodeId").val();
        U.postUrl("/Billing/ManagedClaimAssessmentData", input, function(managedClaim) {
            if (assessmentType = managedClaim.AssessmentType) $("#ManagedClaim_AssessmentType").val(assessmentType);
            if (claimKey = managedClaim.ClaimKey) $("#ManagedClaim_ClaimKey").val(claimKey);
            if (hippsCode = managedClaim.HippsCode) $("#ManagedClaim_HippsCode").val(hippsCode);
            if (prospectivePay = managedClaim.ProspectivePay) $("#ManagedClaim_ProspectivePay").val(prospectivePay);
            if (diagnosisCode1 = managedClaim.DiagnosisCode1) $("#ManagedClaim_DiagnosisCode1").val(diagnosisCode1);
            if (diagnosisCode2 = managedClaim.DiagnosisCode2) $("#ManagedClaim_DiagnosisCode2").val(diagnosisCode2);
            if (diagnosisCode3 = managedClaim.DiagnosisCode3) $("#ManagedClaim_DiagnosisCode3").val(diagnosisCode3);
            if (diagnosisCode4 = managedClaim.DiagnosisCode4) $("#ManagedClaim_DiagnosisCode4").val(diagnosisCode4);
            if (diagnosisCode5 = managedClaim.DiagnosisCode5) $("#ManagedClaim_DiagnosisCode5").val(diagnosisCode5);
            if (diagnosisCode6 = managedClaim.DiagnosisCode6) $("#ManagedClaim_DiagnosisCode6").val(diagnosisCode6);
            UserInterface.CloseModal();
        });
    }
</script>
