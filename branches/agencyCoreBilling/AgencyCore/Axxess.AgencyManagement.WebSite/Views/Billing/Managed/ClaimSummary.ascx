﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedBillViewData>" %>
<% using (Html.BeginForm("Generate", "Billing", FormMethod.Post, new { @id = "generateManageBilling" }))%><% { %>
<%if ((Model.Bills != null && Model.Bills.Count > 0)){%>
<%} else {%><div class="row"><b>Select the claim you want to generate.</b></div><%} %>
<% if (Model != null){ %>
<% if (Model.Bills != null && Model.Bills.Count > 0){%>
<div class="row">
    <table class="claim">
        <thead><tr><th>Patient Name</th><th>Episode Date</th></tr></thead>
        <tbody>
            <% int j = 1;%>
            <% foreach (var final in Model.Bills){%>
            <% if (j % 2 == 0){%><tr class="even"><%}else{ %>
                <tr class="odd"><%} %>
                    <td><%=string.Format("<input type='hidden' id='ManagedClaimSelected{0}' name='ManagedClaimSelected' value='{0}' />", final.Id)%><%=j %>.&#160;&#160;&#160;<%=final.DisplayName + "(" + final.PatientIdNumber + ")"%></td>
                    <td align="center"><% if (final.EpisodeStartDate != null){%><%=final.EpisodeStartDate.IsValid()? final.EpisodeStartDate.ToString("MM/dd/yyyy"): string.Empty%> <%} %><% if (final.EpisodeEndDate != null){%>-<%=final.EpisodeEndDate.IsValid() ? final.EpisodeEndDate.ToString("MM/dd/yyyy") : string.Empty%><%} %></td>
                </tr>
                <% j++;} %>
        </tbody>
    </table>
</div><% }%><div class="clear"></div><% } %>
<%if ((Model.Bills != null && Model.Bills.Count > 0) ){%>
<% = Html.Hidden("StatusType","Submit") %>
<% = Html.Hidden("BranchId", Model.BranchId)%>
<% = Html.Hidden("PrimaryInsurance", Model.Insurance)%>
<div class="buttons"><ul class=""><% if (Model.IsElectronicSubmssion){ %><li class="align_left"> <a href="javascript:void(0);" onclick="ManagedBilling.SubmitClaimDirectly('#generateManageBilling');">Submit Electronically</a></li><%} else{ %><li class="align_left"> <a href="javascript:void(0);" onclick="ManagedBilling.LoadGeneratedManagedClaim('#generateManageBilling');">Download Claim(s)</a></li><%}%></ul></div>
 <% if (!Model.IsElectronicSubmssion){ %><div class="buttons"><ul><li><a href="javascript:void(0);" onclick="ManagedBilling.UpdateStatus('#generateManageBilling');">Mark Claim(s) As Submitted</a></li></ul></div><%} %>
<%}%><%}%>

