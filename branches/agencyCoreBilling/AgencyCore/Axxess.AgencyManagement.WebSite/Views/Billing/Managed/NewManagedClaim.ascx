﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl <NewManagedClaimViewData>" %>
<% using (Html.BeginForm("CreateManagedClaim", "Billing", FormMethod.Post, new { @id = "createManagedClaimForm" })) { %>
 <%= Html.Hidden("PatientId",Model.PatientId) %>
 <div class="form_wrapper">
    <fieldset>
        <legend>Claim Information</legend>
         <div class="wide_column">
            <div class="row">
                <label class="float_left">Insurances:</label>
                <div class="float_right"><%= Html.DropDownList("InsuranceId", Model != null && Model.Insurances != null ? Model.Insurances : new List<SelectListItem>()) %></div>
            </div><div ="row">
            </div><div class="row">
                <label class="float_left">Date Range:</label>
                <div class="float_right">
                    <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="StartDate" class="shortdate" />
                    To
                    <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="EndDate" class="shortdate" />
                </div>
            </div>
         </div>
    </fieldset>
    <div class="buttons">
        <ul>
            <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit()">Add Claim</a></li>
            <li><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()">Cancel</a></li>
        </ul>
    </div>
</div>
<% } %>