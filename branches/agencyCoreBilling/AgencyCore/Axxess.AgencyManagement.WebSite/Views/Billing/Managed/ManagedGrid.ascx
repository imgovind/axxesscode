﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ManagedBill>>" %>
<ul>
    <li class="align_center"><h3>Managed Claim(s)</h3></li>
    <li>
        <span class="rapcheck"></span>
        <span class="rapname pointer" onclick="Billing.Sort('Final', 'rapname');">Patient Name</span>
        <span class="rapid pointer" onclick="Billing.Sort('Final', 'rapid');">Patient ID</span>
        <span class="rapeps pointer" onclick="Billing.Sort('Final', 'rapeps');">Episode Date</span>
        <span class="finalicon">Detail</span>
        <span class="finalicon">Visit</span>
        <span class="finalicon">Supply</span>
        <span class="finalicon">Verified</span>
    </li>
</ul><ol><%
if (Model != null && Model.Count>0) {
    var claims = Model.Where(c =>c.EpisodeEndDate < DateTime.Now);
    int i = 1;
    foreach (var claim in claims)
    { %>
    <%= string.Format("<li class=\"{0}\" onmouseover=\"$(this).addClass('hover');\" onmouseout=\"$(this).removeClass('hover');\">", (i % 2 != 0 ? "odd " : "even ") )%>
        <span class="rapcheck"><%= i %>. <%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? "<input name='ManagedClaimSelected' class='radio' type='checkbox' value='" + claim.Id + "' id='ManagedClaimSelected" + claim.Id + "' />" : string.Empty %></span>
        <%= (claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified) ? string.Format("<a class='float_right' href='javascript:void(0);' onclick=\"U.GetAttachment('Billing/ManagedUB04Pdf', {{ 'Id': '{0}' ,'patientId': '{1}' }});\"><span class='img icon print'></span></a>", claim.Id, claim.PatientId):string.Empty%>
        <a href='javascript:void(0);' onclick="<%= (true) ? "UserInterface.ShowManagedClaim('" + claim.Id + "','" + claim.PatientId + "');\" title=\"Final Ready" : "U.growl('Error: Rap Not Generated', 'error');\" title=\"Not Complete" %>">
            <span class="rapname"><%= claim.LastName%>, <%= claim.FirstName%></span>
            <span class="rapid"><%= claim.PatientIdNumber%></span>
            <span class="rapeps"><span class="very_hidden"><%= claim.EpisodeStartDate != null ? claim.EpisodeStartDate.ToString("yyyyMMdd") : string.Empty %></span><%= claim.EpisodeStartDate != null ? claim.EpisodeStartDate.ToString("MM/dd/yyy") : string.Empty %><%= claim.EpisodeEndDate != null ? "&#8211;" + claim.EpisodeEndDate.ToString("MM/dd/yyy") : string.Empty %></span>
            <span class="finalicon"><span class='img icon <%= claim.IsInfoVerified? "success_small" : "error_small" %>'></span></span>
            <span class="finalicon"><span class='img icon <%= claim.IsVisitVerified? "success_small" : "error_small" %>'></span></span>
            <span class="finalicon"><span class='img icon <%= claim.IsSupplyVerified ? "success_small" : "error_small" %>'></span></span>
            <span class="finalicon"><span class='img icon <%= claim.IsVisitVerified && claim.IsSupplyVerified && claim.IsInfoVerified ? "success_small" : "error_small" %>'></span></span>
        </a>
    </li><%
        i++;
    }
} %>
</ol>
<script type="text/javascript">
    $("#Billing_CenterContentFinal ol li:first").addClass("first");
    $("#Billing_CenterContentFinal ol li:last").addClass("last");
</script>

