﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FilterViewData>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>", "managedclaims","Managed Claim History",Current.AgencyName)%>
<div class="wrapper layout">
    <div class="layout_left">
        <div class="top" style="margin-top: 10px;">
            <div class="row"><label>Branch:</label><div><%= Html.LookupSelectList(SelectListTypes.BranchesReport, "BranchCode", "", new { @class = "managedBillingBranchCode" })%></div></div>
            <div class="row"><label>View:</label><div><select name="list" class="managedBillingStatusDropDown"><option value="1">Active Patients</option><option value="2">Discharged Patients</option></select></div></div>
            <div class="row"><label>Filter:</label><div><%= Html.DropDownList("list", Model.Insurances, new { @class = "managedBillingInsuranceDropDown" })%></div></div>
            <div class="row"><label>Find:</label><div><input id="txtSearch_managedBilling_Selection" class="text" name="" value="" type="text" /></div></div>
        </div>
        <div class="bottom" style="top: 110px;"><%Html.RenderPartial("~/Views/Billing/Managed/PatientList.ascx", Model.SelecetdInsurance.IsNotNullOrEmpty() ? Model.SelecetdInsurance : string.Empty); %></div>
    </div>
    <div id="managedBillingMainResult" class="layout_main">
        <div class="top">
            <div class="winmenu"><ul id="managedBillingTopMenu"><li><a href="javascript:void(0);" onclick="UserInterface.ShowNewManagedClaimModal();">New Claim</a></li></ul></div>
            <div id="managedBillingClaimData"></div>
        </div>
        <div class="bottom" style="top: 220px;">
            <div style="width:100%; background:url('/Content/Office2007/sprite.png') repeat-x scroll 0 -352px #C3D8F1; border:1px solid #AAAAAA; height:30px; padding-top:3.5px; padding-left:5px;"><label class="float_left">Insurance:</label><div class="float_left"><%= Html.InsurancesFilter("PrimaryInsurance", "0", new { @id = "ManagedBillingHistory_InsuranceId", @class = "Insurances" })%></div></div>
            <% Html.RenderPartial("~/Views/Billing/Managed/Claims.ascx", Guid.Empty); %>
        </div>   
    </div>
</div>
<script type="text/javascript">
    $('#window_managedclaims .layout').layout({ west__paneSelector: '.layout_left' });
    $('#window_managedclaims .t-grid-content').css({ height: 'auto',top:'26px' });
    $("#ManagedBillingHistory_InsuranceId").change(function() { ManagedBilling.RebindActivity(ManagedBilling._patientId); });
</script>
<div id="ManagedClaim_New_Container" class="claimupdatemodal hidden"></div>
<div id="ManagedClaim_UpdateStatus_Container" class="claimupdatemodal hidden"></div>

