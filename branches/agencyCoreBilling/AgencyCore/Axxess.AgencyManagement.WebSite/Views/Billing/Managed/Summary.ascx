﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedClaim>" %>
<div class="wrapper main">
    <fieldset><%= Html.Hidden("Id", Model.Id)%>
        <div class="column">
            <div class="row"><div class="float_left">Patient First Name:</div><div class="float_right light"><%= Model.FirstName%></div></div>
            <div class="row"><div class="float_left">Patient Last Name:</div><div class="float_right light"><%= Model.LastName%></div></div>
            <div class="row"><div class="float_left">Medicare #:</div><div class="float_right light"><%= Model.IsuranceIdNumber%></div></div>
            <div class="row"><div class="float_left">Patient Record #:</div><div class="float_right light"><%= Model.PatientIdNumber%></div></div>
            <div class="row"><div class="float_left">Gender:</div><div class="float_right light"><%= Model.Gender%></div></div>
            <div class="row"><div class="float_left">Date of Birth:</div><div class="float_right light"><%= Model.DOB != null ? Model.DOB.ToString("MM/dd/yyyy") : string.Empty %></div></div>
        </div>
        <div class="column">
            <div class="row"><div class="float_left">Episode Start Date:</div><div class="float_right light"><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToShortDateString() : string.Empty %></div></div>
            <div class="row"><div class="float_left">Admission Date:</div><div class="float_right light"><%= Model.StartofCareDate != null ? Model.StartofCareDate.ToShortDateString() : string.Empty %></div></div>
            <div class="row"><div class="float_left">Address Line 1:</div><div class="float_right light"><%= Model.AddressLine1%></div></div>
            <div class="row"><div class="float_left">Address Line 2:</div><div class="float_right light"><%= Model.AddressLine2%></div></div>
            <div class="row"><div class="float_left">City:</div><div class="float_right light"><%= Model.AddressCity%></div></div>
            <div class="row"><div class="float_left">State, Zip Code:</div><div class="float_right light"><%= Model.AddressStateCode + Model.AddressZipCode%></div></div>
        </div>
    </fieldset>
    <fieldset>
        <div class="column"><% var total = Model.SupplyTotal; %>
            <div class="row"><div class="float_left">HIPPS Code:</div><div class="float_right light"><%= Model.HippsCode%></div></div>
            <div class="row"><div class="float_left">OASIS Matching Key:</div><div class="float_right light"><%= Model.ClaimKey%></div></div>
            <div class="row"><div class="float_left">Date Of First Billable Visit:</div><div class="float_right light"><%= Model.FirstBillableVisitDate != null ? Model.FirstBillableVisitDate.ToShortDateString() : string.Empty %></div></div>
            <div class="row"><div class="float_left">Physician Last Name, F.I.:</div><div class="float_right light"><%= (Model.PhysicianLastName )+" "+ ( Model.PhysicianFirstName.IsNotNullOrEmpty() ? Model.PhysicianFirstName.Substring(0, 1)+"." : "")%></div></div>
            <div class="row"><div class="float_left">Physician NPI #:</div><div class="float_right light"><%= Model.PhysicianNPI%></div></div>
            <div class="row"><div>Remark:</div><div class="margin light"><p><%= Model.Remark%></p></div></div>
        </div>
        <div class="column">
            <div class="row"><% var diagnosis = XElement.Parse(Model.DiagnosisCode); var val = (diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : ""); %>
                <div>Diagonasis Codes:</div>
                <div class="margin">
                    <div class="float_left">Primary</div><div class="float_right light"><%= diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float_left">Second</div><div class="float_right light"><%= diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float_left">Third</div><div class="float_right light"><%= diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float_left">Fourth</div><div class="float_right light"><%= diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float_left">Fifth</div><div class="float_right light"><%= diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                    <div class="float_left">Sixth</div><div class="float_right light"><%= diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : string.Empty %></div><div class="clear"></div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="billing">
        <ul><li><span class="rapcheck"></span><span class="sumdesc">Description</span><span class="visittype">HCPCS/HIPPS Code</span><span class="visitdate">Service Date</span><span class="rapicon">Service Unit</span><span class="rapicon">Total Charges</span></li></ul>
        <ol>
            <li class="first even"><span class="rapcheck">0023</span><span class="sumdesc">Home Health Services</span><span class="visittype"><%= Model.HippsCode %></span><span class="visitdate"><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToString("MM/dd/yyy") : string.Empty %></span><span class="rapicon"></span><span class="rapicon"></span></li>
            <li class="odd"><span class="rapcheck"><%=Model.SupplyCode %></span><span class="sumdesc">Service Supplies</span><span class="visittype"></span><span class="visitdate"><%= Model.EpisodeStartDate != null ? Model.EpisodeStartDate.ToString("MM/dd/yyy") : "" %></span><span class="rapicon"></span><span class="rapicon"><%= string.Format("${0:#0.00}", Model.SupplyTotal)%></span></li>
            <% var visits = Model != null && Model.VerifiedVisits.IsNotNullOrEmpty() ? Model.VerifiedVisits.ToObject<List<ScheduleEvent>>().OrderBy(s => s.EventDate).ToList() : new List<ScheduleEvent>(); var billInfo = Model.BillInformations!=null?Model.BillInformations: new Dictionary<string,BillInfo>();var i = 0;
            foreach (var visit in visits) { %>
            <% var discipline = visit.GIdentify();  var unit = Model.UnitType == 1 ? 1 : (Model.UnitType == 2 ? (int)Math.Ceiling((double)visit.MinSpent / 60) : (Model.UnitType == 3 ? (int)Math.Ceiling((double)visit.MinSpent / 15) : 0));
                var tempValue = billInfo.ContainsKey(discipline) && billInfo[discipline].Amount.IsNotNullOrEmpty() && billInfo[discipline].Amount.IsDouble() ? double.Parse(billInfo[discipline].Amount) * unit : 0.00; %>
            <li class="<%= i % 2 != 0 ? "odd" : "even" %>">
                  <span class="rapcheck"><% =billInfo.ContainsKey(discipline)?billInfo[discipline].CodeOne:string.Empty %></span>
                  <span class="sumdesc">Visit</span><span class="visittype"><% =billInfo.ContainsKey(discipline)?billInfo[discipline].CodeTwo:string.Empty %></span>
                  <span class="visitdate"><%= visit.VisitDate.IsNotNullOrEmpty() && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM/dd/yyyy") : visit.EventDate.IsNotNullOrEmpty() && visit.EventDate.IsValidDate() ? visit.EventDate.ToDateTime().ToString("MM/dd/yyyy") : ""%></span><span class="rapicon"><% = unit %> </span>
                  <span class="rapicon"><% = string.Format("${0}",tempValue)%></span>
            </li>
            <% i++; total += tempValue;} %>
        </ol>
        <ul class="total"><li class="align_right"><label for="Total">Total:</label> <label> <%= string.Format("${0:#0.00}", total)%></label></li></ul>
    </div>
    <div class="buttons"><ul><li><a href="javascript:void(0);" onclick="ManagedBilling.NavigateBack(2);">Back</a></li><li><a href="javascript:void(0);" onclick="ManagedBilling.ManagedComplete('<%=Model.Id%>','<%=Model.PatientId%>');">Complete</a></li></ul></div>
</div>
<script type="text/javascript">$("#ManagedClaimTabStrip-4 ol li:last").addClass("last"); </script>