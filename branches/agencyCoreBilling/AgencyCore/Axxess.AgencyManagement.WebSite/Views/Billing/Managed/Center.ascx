﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ManagedBillViewData>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","createmanagedclaims","Create Managed Care Claims",Current.AgencyName) %>
<div id="Billing_ManagedClaimCenterContentMain" class="main wrapper">
    <div class="trical">
         <span class="strong">Branch:</span><%= Html.LookupSelectList(SelectListTypes.Branches, "BranchId", Model.BranchId.ToString(), new { @id = "Billing_ManagedClaimCenterBranchCode", @class = "report_input valid" })%> <span><%= Html.InsurancesNoneMedicare("PrimaryInsurance", Model.Insurance.ToString(), false,"", new { @id = "Billing_ManagedClaimCenterPrimaryInsurance", @class = "Insurances requireddropdown" })%></span>
         <div class="buttons editeps" ><ul><li><a href="javascript:void(0);" onclick="ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(), $('#Billing_ManagedClaimCenterPrimaryInsurance').val());">Refresh</a></li><li><a href="javascript:void(0);" U.GetAttachment('Billing/ClaimsPdf', { 'branchId': $('#Billing_ManagedClaimCenterBranchCode').val(), 'insuranceid':$('#Billing_ManagedClaimCenterPrimaryInsurance').val() });>Print</a></li></ul></div>
    </div>
    <div class="billing"><div id="Billing_ManagedClaimCenterContent"><% Html.RenderPartial("Managed/ManagedGrid", Model.Bills); %></div></div>
    <% if (Current.HasRight(Permissions.GenerateClaimFiles)) { %><div class="buttons" id="Billing_ManagedClaimCenterButtons"><ul><li><a href="javascript:void(0);" onclick="ManagedBilling.loadGenerate('#Billing_ManagedClaimCenterContentMain');">Generate Selected</a></li><li><a href="javascript:void(0);" onclick="ManagedBilling.GenerateAllCompleted('#Billing_ManagedClaimCenterContentMain');">Generate All Completed</a></li></ul></div><% } %>
</div>
<script type="text/javascript">
    $('#Billing_ManagedClaimCenterBranchCode').change(function() { ManagedBilling.ReLoadUnProcessedManagedClaim($(this).val(), $('#Billing_ManagedClaimCenterPrimaryInsurance').val()); });
    if ($('#Billing_ManagedClaimCenterPrimaryInsurance').val() == "0") { $('#Billing_ManagedClaimCenterButtons').hide(); } else { $('#Billing_ManagedClaimCenterButtons').show(); }
    $('#Billing_ManagedClaimCenterPrimaryInsurance').change(function() { if ($(this).val() == "0") { $('#Billing_ManagedClaimCenterButtons').hide(); } else { $('#Billing_ManagedClaimCenterButtons').show(); } ManagedBilling.ReLoadUnProcessedManagedClaim($('#Billing_ManagedClaimCenterBranchCode').val(), $(this).val()); }); 
</script>
