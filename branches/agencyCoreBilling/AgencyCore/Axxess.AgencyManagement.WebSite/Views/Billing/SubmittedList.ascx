﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>", "submittedclaims", "Claim Submission History", Current.AgencyName)%>
<div class="wrapper">
    <fieldset>
        <div class="wide_column">
            <div class="row">
                <label class="float_left">Claim Type:</label>
                <div class="float_left"><%= Html.ClaimTypes("ClaimType", new { @id = "SubmittedClaims_ClaimType" })%></div>
                <label class="float_left">Date Range:</label>
                <div class="float_left">
                    <input type="date" name="StartDate" value="<%= DateTime.Now.AddDays(-59).ToShortDateString() %>" id="SubmittedClaims_StartDate" class="shortdate" />
                    To
                    <input type="date" name="EndDate" value="<%= DateTime.Now.ToShortDateString() %>" id="SubmittedClaims_EndDate" class="shortdate" />
                </div>
                <div class="float_left">
                    <div class="buttons">
                        <ul>
                            <li><a href="javascript:void(0);" onclick="Billing.RebindSubmittedBatchClaims();">Generate</a></li>
                        </ul>
                    </div>
                </div>
                <div class="buttons">
                    <ul class="float_right">
                        <li><%= Html.ActionLink("Excel Export", "SubmittedBatchClaims", "Export", new { ClaimType = "ALL", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now }, new { @id = "SubmittedClaims_ExportLink", @class = "excel" })%></li>
                    </ul>
                </div>
            </div>
        </div>
    </fieldset>
    <%= Html.Telerik().Grid<ClaimDataLean>().Name("List_SubmittedClaims").Columns(columns => {
            columns.Bound(o => o.Id).Title("Batch Id").Width(80).Sortable(true).ReadOnly();
            columns.Bound(o => o.Created).Format("{0:MM/dd/yyyy}").Title("Submission Date").Width(120).Sortable(true).ReadOnly();
            columns.Bound(o => o.Count).Title("# of claims").Sortable(false).ReadOnly();
            columns.Bound(o => o.RAPCount).Title("# of RAPs").Sortable(false).ReadOnly();
            columns.Bound(o => o.FinalCount).Title("# of Finals").Sortable(false).ReadOnly();
            columns.Bound(o => o.Id).ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Billing.SubmittedClaimDetail('<#=Id#>');\">View Claims</a>").Title("Action").Width(90);
        }).DataBinding(dataBinding => dataBinding.Ajax().Select("ClaimSubmittedList", "Billing", new { ClaimType="ALL", StartDate = DateTime.Now.AddDays(-59), EndDate = DateTime.Now })).Sortable().Scrollable(scrolling => scrolling.Enabled(true)).Footer(false) %>
</div>
<div id="OrdersHistory_Edit_Container" class="ordershistoryeditmodal hidden"></div>
<script type="text/javascript">
    $("#List_SubmittedClaims .t-grid-content").css({ 'height': 'auto' });
    $("#List_SubmittedClaims").css({ 'top': '80px' });
    $("#List_SubmittedClaims .t-toolbar.t-grid-toolbar").empty();
</script>