﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Bill>" %>
<%= string.Format("<script type=\"text/javascript\">$(\"#window_{0}\").Rename(\"{1} | {2}\");</script>","billingcenter","Create Claims",Current.AgencyName) %>
<div id="Billing_CenterContent" class="main wrapper">
    <div class="trical">
         <span class="strong">Branch:&nbsp;</span><%= Html.LookupSelectList(SelectListTypes.Branches, "BranchId", Model.BranchId.ToString(), new { @id = "Billing_CenterBranchCode", @class = "report_input valid" })%> <span><%= Html.InsurancesMedicare("PrimaryInsurance", Model.Insurance.ToString(),true,"Unassigned Insurance", new { @id = "Billing_CenterPrimaryInsurance", @class = "Insurances requireddropdown" })%></span>
         <div class="buttons editeps" ><ul><li><a href="javascript:void(0);" onclick="Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val());">Refresh</a></li><li><a href="javascript:void(0);" onclick="U.GetAttachment('Billing/ClaimsPdf', { 'branchId': $('#Billing_CenterBranchCode').val(), 'insuranceid':$('#Billing_CenterPrimaryInsurance').val() });" >Print</a></li></ul></div>
    </div>
    <div class="billing"><div id="Billing_CenterContentRap"><% Html.RenderPartial("RapGrid", Model.RapClaims); %></div><div id="Billing_CenterContentFinal"><% Html.RenderPartial("FinalGrid", Model.FinalClaims); %></div></div>
    <% if (Current.HasRight(Permissions.GenerateClaimFiles)) { %><div class="buttons" id="Billing_CenterButtons"><ul><li><a href="javascript:void(0);" onclick="Billing.loadGenerate('#Billing_CenterContent');">Generate Selected</a></li><li><a href="javascript:void(0);" onclick="Billing.GenerateAllCompleted('#Billing_CenterContent');">Generate All Completed</a></li></ul></div><% } %>
</div>
<script type="text/javascript">
    $('#Billing_CenterBranchCode').change(function() { Billing.ReLoadUpProcessedRap($(this).val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedFinal($(this).val(), $('#Billing_CenterPrimaryInsurance').val()); });
    if ($("#Billing_CenterPrimaryInsurance").val() == "0") { $("#Billing_CenterButtons").hide(); } else { $("#Billing_CenterButtons").show(); }
    $('#Billing_CenterPrimaryInsurance').change(function() { if ($(this).val() == "0") { $("#Billing_CenterButtons").hide(); } else { $("#Billing_CenterButtons").show(); } Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $(this).val()); Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $(this).val()); }); 
</script>