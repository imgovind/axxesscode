﻿<%@ Page Language="C#" MasterPageFile="~/Tool/Tool.Master" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ToolHeader">Axxess Current Log Page</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ToolContent">
    <% if (!Current.IsIpAddressRestricted) { %>
        <table id="newspaper-a">
            <thead>
                <tr><th colspan="4" style="text-align: center;">Logs</th></tr>
                <tr>
                    <th style="width: 75%; text-align: left;">Message</th>
                    <th style="width: 10%; text-align: left;">Type</th>
                    <th style="width: 15%; text-align: left;">Date</th>
                </tr>
            </thead>
            <tbody>
            <% var logCount = 0; %>
            <% LogEngine.Instance.Cache.ForEach(error => { %>
                <tr><td><a href="javascript:void(0);" onclick="showDetails('<%= error.Detail.ToString() %>');"><%= error.Message %></a></td><td><%= error.Type %></td><td><%= error.Created.ToString("MM/dd/yyyy @ hh:mm:ss:tt") %></td></tr>
                <% logCount++; %>
            <% }); %>
            </tbody>
            <tfoot>
                <tr><td align="center" colspan="4"><em>There are <%= logCount %> current errors.</em></td></tr>
            </tfoot>
        </table>
        <div id="popupContainer" class="modalWindow" style="display: none;">
            <div id="errorMesage"></div>
            <div style="text-align: center; margin: 10px;"><a href="javascript:void(0);" onclick="$(this).closest('.window').Close()" >Close</a></div>
        </div>
        <script type="text/javascript">
            function showDetails(details) {
                $("#errorMesage").html(details);
                U.showDialog("#popupContainer");
            }
        </script>
    <% } %>
</asp:Content>