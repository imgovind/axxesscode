﻿var Patient = {
    _patientId: "",
    _patientName: "",
    _EpisodeId: "",
    _patientRowIndex: 0,
    GetId: function() {
        return Patient._patientId;
    },
    SetId: function(patientId) {
        Patient._patientId = patientId;
    },
    SetPatientRowIndex: function(patientRowIndex) {
        Patient._patientRowIndex = patientRowIndex;
    },
    GetEpisodeId: function() {
        return Patient._EpisodeId;
    },
    SetEpisodeId: function(EpisodeId) {
        Patient._EpisodeId = EpisodeId;
    },
    RebindList: function() {
        U.rebindTGrid($('#List_Patient_Grid'));
        U.rebindTGrid($('#List_PatientPending_Grid'));
        U.rebindTGrid($('#List_Referral'));
    },
    Filter: function(text) {
        search = text.split(" ");
        $("tr", "#PatientSelectionGrid .t-grid-content").removeClass("match").hide();
        for (var i = 0; i < search.length; i++) {
            $("td", "#PatientSelectionGrid .t-grid-content").each(function() {
                if ($(this).html().toLowerCase().indexOf(search[i].toLowerCase()) > -1) $(this).parent().addClass("match");
            });
        }
        $("tr.match", "#PatientSelectionGrid .t-grid-content").removeClass("t-alt").show();
        $("tr.match:even", "#PatientSelectionGrid .t-grid-content").addClass("t-alt");
    },
    InitNew: function() {
        var $triage = $('input.Triage');
        $triage.click(function() {
            $triage.removeAttr('checked');
            $(this).attr('checked', true);
        });
        U.PhoneAutoTab("New_Patient_HomePhone");
        U.PhoneAutoTab("New_Patient_MobilePhone");
        U.PhoneAutoTab("New_Patient_PharmacyPhone");
        U.PhoneAutoTab("New_Patient_EmergencyContactPhonePrimary");
        U.PhoneAutoTab("New_Patient_EmergencyContactPhoneAlternate");
        U.showIfChecked($("#New_Patient_DMEOther"), $("#New_Patient_OtherDME"));
        U.showIfChecked($("#New_Patient_PaymentSource"), $("#New_Patient_OtherPaymentSource"));
        U.InitNewTemplate("Patient", function() {
            if ($('#New_Patient_Status').val() == "1") {
                Patient.Rebind();
                UserInterface.ShowPatientPrompt();
            }
        });
        $("#window_newpatient .Physicians").PhysicianInput();
    },
    InitEdit: function() {
        U.PhoneAutoTab("Edit_Patient_HomePhone");
        U.PhoneAutoTab("Edit_Patient_MobilePhone");
        U.PhoneAutoTab("Edit_Patient_PharmacyPhone");
        $("#window_editpatient .Physicians").PhysicianInput();
        $("#EditPatient_NewPhysician").click(function() {
            var PhysId = $("#EditPatient_PhysicianSelector").next("input[type=hidden]").val();
            $("#EditPatient_PhysicianSelector").AjaxAutocomplete("reset");
            if (U.IsGuid(PhysId)) Patient.AddPhysician(PhysId, $("#Edit_Patient_Id").val());
            else U.growl("Please Select a Physician", "error");
        });
        U.InitEditTemplate("Patient");
    },
    Verify: function(medicareNumber, lastName, firstName, dob, gender) {
        var input = "medicareNumber=" + medicareNumber + "&lastName=" + lastName + "&firstName=" + firstName + "&dob=" + dob + "&gender=" + gender;
        U.postUrl("/Patient/Verify", input, function(result) {
            alert(result);
        });
    },
    InitCenter: function() {
        $("#txtSearch_Patient_Selection").keyup(function() {
            if ($(this).val()) {
                Patient.Filter($(this).val())
                if ($("tr.match:even", "#PatientSelectionGrid .t-grid-content").length) $("tr.match:first", "#PatientSelectionGrid .t-grid-content").click();
                else $("#PatientMainResult").html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Ch1%3ENo Patients Found to Meet Your Search Requirements%3C/h1%3E%3C" +
                    "div class=%22buttons%22%3E%3Cul%3E%3Cli%3E%3Ca title=%22Add New Patient%22 onclick=%22javascript:Acore.Open('newpatient');%22 href=%22javasc" +
                    "ript:void(0);%22%3EAdd New Patient%3C/a%3E%3C/li%3E%3C/ul%3E%3C/div%3E%3C/div%3E"));
            } else {
                $("tr", "#PatientSelectionGrid .t-grid-content").removeClass("match t-alt").show();
                $("tr:even", "#PatientSelectionGrid .t-grid-content").addClass("t-alt");
                $("tr:first", "#PatientSelectionGrid .t-grid-content").click();
            }
        });
        $("#window_patientcenter .top select").change(function() { Patient._patientId = ""; U.FilterResults("Patient"); });
        $("#patientActivityDropDown").change(function() {
            var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
            PatientActivityGrid.rebind({ patientId: Patient._patientId, discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val() });
        });
        $('#window_patientcenter_content .t-grid-content').css({ height: 'auto' });
        $('#window_patientcenter .layout').layout({ west__paneSelector: '.layout_left' });
    },
    InitPatientReadmitted: function() { U.InitTemplate($("#readmitForm"), function() { UserInterface.CloseModal(); Patient.Rebind(); }, "Patient Re-admitted successfully."); },
    InitChangePatientStatus: function() { U.InitTemplate($("#changePatientStatusForm"), function() { UserInterface.CloseModal(); Patient.Rebind(); }, "Patient status updated successfully."); },
    InitDischargePatient: function() { U.InitTemplate($("#dischargePatientForm"), function() { UserInterface.CloseModal(); Patient._patientId = ""; U.FilterResults("Patient"); }, "Patient is discharged successfully."); },
    InitActivatePatient: function() { U.InitTemplate($("#activatePatientForm"), function() { UserInterface.CloseModal(); Patient._patientId = ""; U.FilterResults("Patient"); }, "Patient is activated successfully."); },
    InitNewPhoto: function() {
        $("#changePatientPhotoForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            $.jGrowl("Patient photo updated successfully.", { theme: 'success', life: 5000 });
                            Patient.Rebind();
                            UserInterface.CloseModal();
                        }
                        else {
                            alert(result.responseText);
                        }
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            $.jGrowl("Patient photo updated successfully.", { theme: 'success', life: 5000 });
                            Patient.Rebind();
                            UserInterface.CloseModal();
                        }
                        else {
                            alert(result.responseText);
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    RemovePhoto: function(patientId) {
        if (confirm("Are you sure you want to remove this photo?")) {
            U.postUrl("/Patient/RemovePhoto", "patientId=" + patientId, function(result) {
                if (result.isSuccessful) {
                    U.growl("Patient photo has been removed successfully.", "success");
                    Patient.Rebind();
                    UserInterface.CloseModal();
                } else U.growl(result.errorMessage, "error");
            });
        }
    },
    activityRowDataBound: function(e) {
        var dataItem = e.dataItem;
        $("a.tooltip", e.row).each(function() {
            if ($(this).hasClass("blue_note")) var c = "blue_note";
            if ($(this).hasClass("red_note")) var c = "red_note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue_note") ? "blue" : "") + ($(this).hasClass("red_note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
        if (dataItem.IsComplete) { $(e.row).addClass('darkgreen'); }
        if (dataItem.IsOrphaned) {
            $(e.row).addClass('black').tooltip({
                track: true,
                showURL: false,
                top: 5,
                left: 5,
                extraClass: "calday error",
                bodyHandler: function() { return "WARNING: This event date is out of episode range.<br />Please click on Details and edit the date accordingly."; }
            });
            $(e.row.cells[1]).addClass('darkred');
        }
        $(e.row).bind("contextmenu", function(Event) {
            var Menu = $("<ul/>");
            if (dataItem.IsComplete) Menu.append($("<li/>", { "text": "Reopen Task" }).click(function() {
                $(e.row).find("a:contains('Reopen Task')").click();
            }));
            else if (!dataItem.IsOrphaned) Menu.append($("<li/>", { "text": "Edit Note" }).click(function() {
                $(e.row).find("a:first").click();
            }));
            Menu.append($("<li/>", { "text": "Details" }).click(function() {
                $(e.row).find("a:contains('Details')").click();
            })).append($("<li/>", { "text": "Delete" }).click(function() {
                $(e.row).find("a:contains('Delete')").click();
            })).append($("<li/>", { "text": "Print" }).click(function() {
                $(e.row).find(".print").parent().click();
            }));
            Menu.ContextMenu(Event);
        });
    },
    OnPatientRowSelected: function(e) {
        if (e.row.cells[2] != undefined) {
            var scroll = $(e.row).position().top + $(e.row).closest(".t-grid-content").scrollTop() - 24;
            $(e.row).closest(".t-grid-content").animate({ scrollTop: scroll }, 'slow');
            var patientId = e.row.cells[2].innerHTML;
            Patient.loadInfoAndActivity(patientId);
        }
    },
    loadInfoAndActivity: function(patientId) {
        $('#PatientMainResult').empty().addClass('loading').load('Patient/Data', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) {
            $('#PatientMainResult').removeClass('loading');
            Patient.SetId(patientId);
            if (textStatus == 'error') $('#PatientMainResult').html(U.AjaxError);
            $('#window_patientcenter_content .t-grid-content').css({ height: 'auto' });
        });
    },
    loadEditPatient: function(id) {
        Acore.Open("editpatient", 'Patient/EditPatientContent', function() {
            Patient.InitEdit();
            Lookup.loadAdmissionSources();
            Lookup.loadRaces();
            Lookup.loadUsers();
            Lookup.loadReferralSources();
        }, { patientId: id });
    },
    loadEditAuthorization: function(patientId, id) {
        Acore.Open("editauthorization", 'Authorization/Edit', function() {
            Patient.InitEditAuthorization();
        }, { patientId: patientId, Id: id });
    },
    InitNewOrder: function() {
        Template.OnChangeInit();
        $("#New_Order_PhysicianDropDown").PhysicianInput();
        $("#newOrderForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindOrders();
                            UserInterface.CloseWindow('neworder');
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    SubmitOrder: function(control, page) {
        control.closest("form").validate();
        if (control.closest("form").valid()) {
            var options = {
                dataType: 'json',
                beforeSubmit: function(values, form, options) {
                },
                success: function(result) {
                    if (result.isSuccessful) {
                        if (control.html() == "Save") {
                            U.growl("The Physician Order has been saved successfully.", "success");
                        }
                        if (control.html() == "Save &amp; Close") {
                            UserInterface.CloseAndRefresh(page);
                            U.growl("The Physician Order has been saved successfully.", "success");
                        }
                        if (control.html() == "Complete") {
                            UserInterface.CloseAndRefresh(page);
                            U.growl("The Physician Order has been saved and completed successfully.", "success");
                        }
                    } else U.growl(result.errorMessage, "error");
                }
            };
            $(control.closest("form")).ajaxSubmit(options);
            return false;
        } else U.ValidationError(control);
    },
    InitNewFaceToFaceEncounter: function() {
        $("#newFaceToFaceEncounterForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            Patient.Rebind();
                            Schedule.Rebind();
                            Agency.RebindOrders();
                            UserInterface.CloseWindow('newfacetofaceencounter');
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewCommunicationNote: function() {
        Template.OnChangeInit();
        $('textarea[maxcharacters]').limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings('.charsRemaining').text("You have " + remaining + " characters remaining");
                if (remaining > 0) {
                    $(this).css('background-color', 'white');
                }
            },
            onLimit: function() {
                $(this).css('background-color', '#ecbab3');
            }
        });
        $("#newCommunicationNoteForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("New Communication Note saved successfully.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newcommnote');
                            Patient.Rebind();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNewAuthorization: function() {
        $(".numeric").numeric();
        $('textarea[maxcharacters]').limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings('.charsRemaining').text("You have " + remaining + " characters remaining");
                if (remaining > 0) {
                    $(this).css('background-color', 'white');
                }
            },
            onLimit: function() {
                $(this).css('background-color', '#ecbab3');
            }
        });
        $("#newAuthorizationForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newauthorization');
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditAuthorization: function() {

        $(".numeric").numeric();
        $('textarea[maxcharacters]').limitMaxlength({
            onEdit: function(remaining) {
                $(this).siblings('.charsRemaining').text("You have " + remaining + " characters remaining");
                if (remaining > 0) {
                    $(this).css('background-color', 'white');
                }
            },
            onLimit: function() {
                $(this).css('background-color', '#ecbab3');
            }
        });
        $("#editAuthorizationForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('editauthorization');
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitEditOrder: function() {
        Template.OnChangeInit();
        $("#Edit_Order_PhysicianDropDown").PhysicianInput();
    },
    Rebind: function() {
        U.rebindTGrid($('#PatientSelectionGrid'));
        U.rebindTGrid($('#List_Patient_Grid'));
    },
    LoadNewOrder: function(patientId) {
        Acore.Open("neworder", 'Order/New', function() {
            Patient.InitNewOrder();
        }, { patientId: patientId });
    },
    DeleteOrder: function(id, patientId, episodeId) {
        if (confirm("Are you sure you want to delete this task?")) {
            U.postUrl("/Patient/DeleteOrder", { id: id, patientId: patientId, episodeId: episodeId }, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                    var grid = $('#List_PatientOrdersHistory').data('tGrid');
                    if (grid != null) { grid.rebind({ patientId: patientId }); }

                    var grid = $('#List_EpisodeOrders').data('tGrid');
                    if (grid != null) { grid.rebind({ patientId: patientId, episodeId: episodeId }); }

                    if ($("#Billing_CenterContent").val() != null && $("#Billing_CenterContent").val() != undefined) {
                        Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val());
                    }

                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    LoadNewCommunicationNote: function(patientId) {
        Acore.Open("newcommnote", 'CommunicationNote/New', function() {
            Patient.InitNewCommunicationNote();
        }, { patientId: patientId });
    },
    LoadNewAuthorization: function(patientId) {
        Acore.Open("newauthorization", 'Authorization/New', function() {
            Patient.InitNewAuthorization();
        }, { patientId: patientId });
    },
    LoadNewInfectionReport: function(patientId) {
        Acore.Open("newinfectionreport", 'Infection/New', function() {
            InfectionReport.InitNew();
        }, { patientId: patientId });
    },
    LoadNewIncidentReport: function(patientId) {
        Acore.Open("newincidentreport", 'Incident/New', function() { IncidentReport.InitNew(); }, { patientId: patientId });
    },
    InfoPopup: function(e) {
        var $dialog = $("#patientInfo");
        $dialog.dialog({
            width: 500,
            position: [$(e).offset().left, $(e).offset().top],
            modal: false,
            resizable: false,
            close: function() {
                $dialog.dialog("destroy");
                $dialog.hide();
            }
        }).show();
        $('#ui-dialog-title-patientInfo').html('Patient Info');
    },
    addPhysician: function(el) { $(el).closest('.column').find('.row.hidden').first().removeClass('hidden').find("input:first").blur(); },
    removePhysician: function(el) { $(el).closest('.row').addClass('hidden').appendTo($(el).closest('.column')).find('select').val('00000000-0000-0000-0000-000000000000'); },
    InitMedicationProfileSnapshot: function() {
        U.PhoneAutoTab("MedProfile_PharmacyPhone");
        $("#window_medicationprofilesnapshot .Physicians").PhysicianInput();
        $("#newMedicationProfileSnapShotForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('medicationprofilesnapshot');
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    loadMedicationProfileSnapshot: function(patientId) {
        Acore.Open("medicationprofilesnapshot", 'Patient/MedicationProfileSnapShot', function() { Patient.InitMedicationProfileSnapshot(); }, { patientId: patientId });
    },
    loadMedicationProfile: function(patientId) {
        Acore.Open("medicationprofile", 'Patient/MedicationProfile', function() { }, { patientId: patientId });
    },
    loadMedicationProfileSnapshotHistory: function(patientId) {
        Acore.Open("medicationprofilesnapshothistory", 'Patient/MedicationProfileSnapShotHistory', function() { }, { patientId: patientId });
    },
    loadPatientCommunicationNotes: function(patientId) {
        Acore.Open("patientcommunicationnoteslist", 'Patient/PatientCommunicationNotesView', function() { }, { patientId: patientId });
    },
    onMedicationProfileEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find('.MedicationType').data('tDropDownList').select(function(dataItem) {
                return dataItem.Value == e.dataItem['MedicationType'].Value;
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest('td').prevAll().length == 7) {
                    if (e.dataItem['MedicationCategory'] != 'DC') {
                        $(this).html('');
                    }
                }
            });
            $(e.form).find("tr td").each(function() {
                if ($(this).closest('td').prevAll().length == 6) {
                    if (e.dataItem['Classification'] == null || e.dataItem['Classification'] == '') {
                        $(this).find("input[name=MedicationClassification]").val('');
                    }
                }
            });
        }
    },
    onPlanofCareMedicationEdit: function(e) {
        if (e.dataItem != undefined) {
            $(e.form).find('.MedicationType').data('tDropDownList').select(function(dataItem) {
                return dataItem.Value == e.dataItem['MedicationType'].Value;
            });
        }
    },
    onMedicationProfileRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem.MedicationCategory == "DC") {
            $(e.row).removeClass('t-alt').addClass('red');
            e.row.cells[7].innerHTML = dataItem.DCDateFormated;
        }
        else {
            e.row.cells[7].innerHTML = '<a class="t-grid-action t-button t-state-default" href="javascript:void(0);" onclick="' + dataItem.DischargeUrl + '">D/C</a>';
        }

        if (dataItem.DrugId != null && dataItem.DrugId.length > 0) {
            var medline = 'http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.69&mainSearchCriteria.v.c=' + dataItem.DrugId;
            e.row.cells[2].innerHTML = '<a href="' + medline + '" target="_blank">' + dataItem.MedicationDosage + '</a>';
        }

        e.row.cells[1].innerHTML = dataItem.StartDateSortable;
    },
    onMedicationProfileSnapShotRowDataBound: function(e) {
        var dataItem = e.dataItem;
        if (dataItem != undefined) {
            e.row.cells[1].innerHTML = dataItem.StartDateSortable;
        }
    },
    onMedProfileSnapShotHistoryRowDataBound: function(e) {
        var dataItem = e.dataItem;
        e.row.cells[1].innerHTML = dataItem.SignedDateFormatted;
    },
    OnDataBound: function(e) {
        var id = $("#medicationProfileHistroyId").val();
        var medicatonProfileGridDC = $('#MedicatonProfileGridDC').data('tGrid');
        medicatonProfileGridDC.rebind({ medId: id, medicationCategry: "DC", assessmentType: "" });
    },

    DischargeMed: function(medId, patientId, assessmentType) {
        $("#Discharge_Medication_Container").load("/Patient/MedicationDischarge", function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $.jGrowl("Could not load the Medication Discharge page.", { theme: 'error', life: 5000 });
            }
            else if (textStatus == "success") {
                U.showDialog("#Discharge_Medication_Container", function() {
                    if (assessmentType != undefined || assessmentType != null && assessmentType.length > 0) {
                        Patient.InitDischargeMedication(medId, patientId, function() { Patient.RefreshOasisMedProfile(medId, assessmentType); });
                    } else {
                        Patient.InitDischargeMedication(medId, patientId, function() { Patient.RefreshMedProfile(medId); });
                    }
                });
            }
        });
    },
    InitDischargeMedication: function(medId, patientId, callback) {
        $("#dischargeMedicationProfileForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    data: { medId: medId, patientId: patientId },
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            UserInterface.CloseModal();
                            if (callback != undefined && typeof (callback) == 'function') callback();
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    RefreshMedProfile: function(medId) {
        var medicatonProfileGridActive = $('#MedicatonProfileGridActive').data('tGrid');
        medicatonProfileGridActive.rebind({ medId: medId, medicationCategry: "Active", assessmentType: "" });

        var medicatonProfileGridDC = $('#MedicatonProfileGridDC').data('tGrid');
        medicatonProfileGridDC.rebind({ medId: medId, medicationCategry: "DC", assessmentType: "" });
    },
    RefreshOasisMedProfile: function(medId, assessmentType) {
        var oasisMedGrid = $('#Oasis' + assessmentType + 'MedicationGrid').data('tGrid');
        if (oasisMedGrid != null) {
            oasisMedGrid.rebind({ medId: medId, medicationCategry: "Active", assessmentType: assessmentType });
        }
    },
    DateRange: function(id) {
        var data = 'dateRangeId=' + id + "&patientId=" + $("#PatientCenter_PatientId").val();
        $.ajax({
            url: '/Patient/DateRange',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                $("#dateRangeText").empty();
                if (result.StartDateFormatted == "01/01/0001" || result.EndDateFormatted == "01/01/0001") {
                    $("#dateRangeText").append("No Episodes Found");
                }
                else {
                    $("#dateRangeText").append(result.StartDateFormatted + "-" + result.EndDateFormatted);
                }
            }
        });
    },
    CustomDateRange: function() {
        var PatientActivityGrid = $('#PatientActivityGrid').data('tGrid');
        if (PatientActivityGrid != null) {
            PatientActivityGrid.rebind({ patientId: $("#PatientCenter_PatientId").val(), discipline: $("select.patientActivityDropDown").val(), dateRangeId: $("select.patientActivityDateDropDown").val(), rangeStartDate: $("#patientActivityFromDate").val(), rangeEndDate: $("#patientActivityToDate").val() });
        }
    },
    DeleteSchedule: function(episodeId, patientId, eventId, employeeId) {
        if (confirm("Are you sure you want to delete this task?")) {
            var input = "patientId=" + patientId + "&eventId=" + eventId + "&employeeId=" + employeeId + "&episodeId=" + episodeId;
            U.postUrl("/Schedule/Delete", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    Patient.CustomDateRange();
                    var scheduleActivityGrid = $('#ScheduleActivityGrid').data('tGrid');
                    if (scheduleActivityGrid != null) {
                        scheduleActivityGrid.rebind({ episodeId: episodeId, patientId: patientId, discipline: Schedule._Discipline });
                    }
                    Schedule.loadCalendarNavigation(episodeId, patientId);
                }
            });
        }
    },
    Delete: function(patientId) {
        if (confirm("Are you sure you want to delete this patient?")) {
            var input = "patientId=" + patientId;
            U.postUrl("/Patient/Delete", input, function(result) {
                var resultObject = eval(result);
                if (resultObject.isSuccessful) {
                    Patient.Rebind();
                    Schedule.Rebind();
                    $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    InitAdmit: function(patientId) {
        $("#newAdmitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Patient admission successful.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('admitpatient');
                            UserInterface.CloseModal();
                            Patient.Rebind();
                            Schedule.Rebind();
                            U.rebindTGrid($('#List_Patient_NonAdmit_Grid'));
                            U.rebindTGrid($('#List_PatientPending_Grid'));
                            UserInterface.ShowPatientPrompt();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    InitNonAdmit: function() {
        $("#NonAdmit_Patient_Date").datepicker("hide");
        $("#newNonAdmitPatientForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Patient non-admission successful.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('nonadmitpatient');
                            UserInterface.CloseModal();
                            U.rebindTGrid($('#List_PatientPending_Grid'));
                            Patient.Rebind();
                            Schedule.Rebind();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    loadEditCommunicationNote: function(patientId, id) {
        Acore.Open("editcommunicationnote", 'Patient/EditCommunicationNote', function() {
            $("#editCommunicationNoteForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                $.jGrowl(resultObject.errorMessage, { theme: 'success', life: 5000 });
                                UserInterface.CloseWindow('editcommunicationnote');
                                Patient.Rebind();
                                Schedule.Rebind();
                            } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });

        }, { patientId: patientId, Id: id });
    },
    loadEditEmergencyContact: function(patientId, id) {
        Acore.Open("editemergencycontact", 'Patient/EditEmergencyContactContent', function() {
            U.PhoneAutoTab("Edit_EmergencyContact_PrimaryPhoneArray");
            U.PhoneAutoTab("Edit_EmergencyContact_AlternatePhoneArray");
            $("#editEmergencyContactForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            if (result.isSuccessful) {
                                $.jGrowl("Edit emergency contact is successful.", { theme: 'success', life: 5000 });
                                var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                                if (emergencyContact != null) {
                                    emergencyContact.rebind({ PatientId: patientId });
                                }
                                UserInterface.CloseWindow('editemergencycontact');
                            } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });

        }, { patientId: patientId, Id: id });
    },
    loadNewEmergencyContact: function(id) {
        Acore.Open("newemergencycontact", 'Patient/NewEmergencyContactContent', function() {
            U.PhoneAutoTab("New_EmergencyContact_PrimaryPhoneArray");
            U.PhoneAutoTab("New_EmergencyContact_AlternatePhoneArray");
            $("#newEmergencyContactForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            if (result.isSuccessful) {
                                $.jGrowl("New emergency contact added successful.", { theme: 'success', life: 5000 });
                                var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                                if (emergencyContact != null) {
                                    emergencyContact.rebind({ PatientId: id });
                                }
                                UserInterface.CloseWindow('newemergencycontact');
                            } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            });
        }, { patientId: id });
    },
    DeleteEmergencyContact: function(id, patientId) {
        if (confirm("Are you sure you want to delete")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Patient/DeleteEmergencyContact",
                data: "id=" + id + "&patientId=" + patientId,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        var emergencyContact = $('#Edit_patient_EmergencyContact_Grid').data('tGrid');
                        if (emergencyContact != null) {
                            emergencyContact.rebind({ PatientId: patientId });
                        }
                    }
                }
            }
            );
        }
    },
    DeleteCommunicationNote: function(Id, patientId) {
        if (confirm("Are you sure you want to delete this task?")) {
            U.postUrl("/Patient/DeleteCommunicationNote", { Id: Id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    var communicationNoteGrid = $('#List_CommunicationNote').data('tGrid');
                    if (communicationNoteGrid != null) {
                        $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                        communicationNoteGrid.rebind();
                    }
                }
                else {
                    $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                }
            });
        }
    },
    AddPhysician: function(id, patientId) {
        U.TGridAjax("/Patient/AddPatientPhysicain", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
        $("#EditPatient_PhysicianSelector").AjaxAutocomplete("reset").next("input[type=hidden]").val("");
    },
    DeletePhysician: function(id, patientId) {
        if (confirm("Are you sure you want to delete this physician?")) U.TGridAjax("/Patient/DeletePhysicianContact", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
    },
    DeleteAuthorization: function(patientId, id) {
        if (confirm("Are you sure you want to delete this authorization?")) {
            U.postUrl("/Patient/DeleteAuthorization", { Id: id, patientId: patientId }, function(result) {
                if (result.isSuccessful) {
                    var authorizationGrid = $('#List_Authorizations').data('tGrid');
                    if (authorizationGrid != null) {
                        $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                        authorizationGrid.rebind({ patientId: patientId });
                    }
                }
                else {
                    $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                }
            });
        }
    },
    SetPrimaryPhysician: function(id, patientId) {
        if (confirm("Are you sure you want to set this physician as primary?")) U.TGridAjax("/Physician/SetPrimary", { "id": id, "patientId": patientId }, $('#EditPatient_PhysicianGrid'));
    },
    OnSocChange: function() {
        var date = $("#New_Patient_StartOfCareDate").datepicker("getDate");
        $("#New_Patient_EpisodeStartDate").datepicker("setDate", date)
        $("#New_Patient_EpisodeStartDate").datepicker("option", "mindate", date);
    },
    PatientListDataBound: function() {
        if ($("#PatientSelectionGrid .t-grid-content tr").length) {
            if (Patient._patientId == "") $('#PatientSelectionGrid .t-grid-content tr').eq(0).click();
            else $('td:contains(' + Patient._patientId + ')', $('#PatientSelectionGrid')).closest('tr').click();
        } else $("#PatientMainResult").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>");
        if ($("#PatientSelectionGrid .t-state-selected").length) $("#PatientSelectionGrid .t-grid-content").scrollTop($("#PatientSelectionGrid .t-state-selected").position().top - 50);
        Patient.Filter($("#txtSearch_Patient_Selection").val());
    },
    UpdateOrderStatus: function(eventId, patientId, episodeId, orderType, actionType) {
        var reason = "";
        if (actionType == "Return") {
            if ($("#printreturnreason").is(":hidden")) {
                $("#printcontrols li a").each(function() { if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide(); });
                $("#printreturncancel").parent().removeClass("very_hidden");
                $("#printreturnreason").slideDown('slow');
            } else {
                reason = $("#printreturnreason textarea").val();
                U.postUrl('Patient/UpdateOrderStatus', { eventId: eventId, patientId: patientId, episodeId: episodeId, orderType: orderType, actionType: actionType, reason: reason }, function(data) {
                    if (data.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        $.jGrowl(data.errorMessage, { theme: 'success', life: 5000 });
                    } else $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 });
                });
            }
        } else {
            U.postUrl('Patient/UpdateOrderStatus', { eventId: eventId, patientId: patientId, episodeId: episodeId, orderType: orderType, actionType: actionType, reason: reason }, function(data) {
                if (data.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    $.jGrowl(data.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(data.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    RebindVitalSigns: function() {
        var grid = $('#List_PatientVitalSigns').data('tGrid');
        if (grid != null) {
            grid.rebind({ patientId: $("#Patient_VitalSigns_PatientId").val(), StartDate: $("#Patient_VitalSigns_StartDate").val(), EndDate: $("#Patient_VitalSigns_EndDate").val() });
        }
        var $exportLink = $('#Patient_VitalSigns_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/patientId=([^&]*)/, 'patientId=' + $("#Patient_VitalSigns_PatientId").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#Patient_VitalSigns_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#Patient_VitalSigns_EndDate").val());
        $exportLink.attr('href', href);
    },
    ProcessCommunicationNote: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#printreturnreason").is(":hidden")) {
                $("#printcontrols li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very_hidden");
                $("#printreturnreason").slideDown('slow');
            } else {
                reason = $("#printreturnreason textarea").val();
                U.postUrl("/Patient/ProcessCommunicationNotes", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                    } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                });
            }
        } else {
            U.postUrl("/Patient/ProcessCommunicationNotes", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement(); 
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    RebindOrdersHistory: function() {
        var grid = $('#List_PatientOrdersHistory').data('tGrid');
        if (grid != null) { grid.rebind({ patientId: $("#PatientOrdersHistory_PatientId").val(), StartDate: $("#PatientOrdersHistory_StartDate-input").val(), EndDate: $("#PatientOrdersHistory_EndDate-input").val() }); }
        var $exportLink = $('#PatientOrdersHistory_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/patientId=([^&]*)/, 'patientId=' + $("#PatientOrdersHistory_PatientId").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#PatientOrdersHistory_StartDate-input").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#PatientOrdersHistory_EndDate-input").val());
        $exportLink.attr('href', href);
    },
    loadPatientManagedDates: function(Id) { Acore.Open("patientmanageddates", 'Patient/ManagedDates', function() { }, { patientId: Id }); }
}
