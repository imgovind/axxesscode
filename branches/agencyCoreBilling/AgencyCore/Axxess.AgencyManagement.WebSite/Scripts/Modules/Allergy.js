﻿var Allergy = {
    assessmentType: "",
    Add: function(allergyProfileId, assessmentType) {
        Acore.Modal({
            "Name": "Add New Allergy",
            "Url": "Patient/NewAllergy",
            "Input": { allergyProfileId: allergyProfileId },
            "OnLoad": function() { Allergy.InitNew(assessmentType) },
            "Width": "650px",
            "Height": "200px",
            "WindowFrame": false
        })
    },
    Delete: function(allergyProfileId, allergyId, assessmentType) {
        if (confirm("Are you sure you want to delete this allergy?")) {
            U.postUrl('Patient/UpdateAllergyStatus', { allergyProfileId: allergyProfileId, allergyId: allergyId, isDeprecated: true }, function(result) {
                if (result.isSuccessful) {
                    U.growl(result.errorMessage, "success");
                    Allergy.Refresh(allergyProfileId);
                } else U.growl(result.errorMessage, "error");
            })
        }
    },
    Edit: function(allergyProfileId, allergyId, assessmentType) {
        Acore.Modal({
            "Name": "Add New Allergy",
            "Url": "Patient/EditAllergy",
            "Input": { allergyProfileId: allergyProfileId, allergyId: allergyId },
            "OnLoad": function() { Allergy.InitEdit(assessmentType) },
            "Width": "650px",
            "Height": "200px",
            "WindowFrame": false
        })
    },
    InitAutocomplete: function() {
        $("input[name=Type]").Autocomplete({
            source: [
                "Medication",
                "Food",
                "Animals",
                "Plants",
                "Latex",
                "Environmental"
            ]
        })
    },
    InitNew: function(assessmentType) {
        if (assessmentType != undefined) this.assessmentType = assessmentType;
        this.InitAutocomplete();
        $("#newAllergyForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            var allergyProfileId = $("#New_Allergy_ProfileId").val();
                            Allergy.Refresh(allergyProfileId);
                            U.growl(result.errorMessage, "success");
                            if ($("#New_Allergy_AddAnother").val().length > 0) {
                                $("#newAllergyForm").clearForm();
                                $("#New_Allergy_StartDate").removeAttr("disabled");
                                $("#New_Allergy_ProfileId").val(allergyProfileId);
                            } else {
                                $("#window_ModalWindow").Close();
                            }
                            //if ($("#New_Allergy_AddAnother").val() == "AddAnother") $("#newAllergyForm").clearForm().find("#New_Allergy_ProfileId").val(allergyProfileId);
                            //else $("#window_ModalWindow").Close();
                        } else U.growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    InitEdit: function(assessmentType) {
        if (assessmentType != undefined) this.assessmentType = assessmentType;
        this.InitAutocomplete();
        $("#editAllergyForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            var allergyProfileId = $("#Edit_Allergy_ProfileId").val();
                            Allergy.Refresh(allergyProfileId);
                            U.growl(result.errorMessage, "success");
                            $("#window_ModalWindow").Close();
                        } else U.growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        })
    },
    Refresh: function(allergyProfileId, prefix) {
        if (prefix == undefined && this.assessmentType.length) prefix = this.assessmentType
        else if (prefix == undefined) prefix = "AllergyProfile";
        $("#" + prefix + "_active ol").addClass("loading");
        $("#" + prefix + "_inactive ol").addClass("loading");
        $("#" + prefix + "_list").load(
            (prefix == "AllergyProfile" ? "Patient/Allergies" : "Patient/AllergiesForOasis"),
            (prefix == "AllergyProfile" ? { "allergyProfileId": allergyProfileId} : { "allergyProfileId": allergyProfileId, "assessmentType": prefix }),
            function(responseText, textStatus, XMLHttpRequest) {
                $('#' + prefix + '_active ol').removeClass("loading");
                $('#' + prefix + '_inactive ol').removeClass("loading");
                if (textStatus == 'error') $('#' + prefix + '_list').html(U.AjaxError);
            }
        );
    },
    Restore: function(allergyProfileId, allergyId, assessmentType) {
        if (confirm("Are you sure you want to restore this allergy?")) {
            U.postUrl('Patient/UpdateAllergyStatus', { allergyProfileId: allergyProfileId, allergyId: allergyId, isDeprecated: false }, function(result) {
                if (result.isSuccessful) {
                    U.growl(result.errorMessage, "success");
                    Allergy.Refresh(allergyProfileId);
                } else U.growl(result.errorMessage, "error");
            })
        }
    }
}