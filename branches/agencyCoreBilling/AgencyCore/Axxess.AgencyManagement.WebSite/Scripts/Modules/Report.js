﻿var Report = {
    Init: function() {
        $("#reportingTabs.tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
        $("#reportingTabs.tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
        $("#reportingTabs").find('a').each(function() {
            var tab = $(this);
            tab.bind("click", function(event) {
                $(tab.attr('href')).empty();
                $(tab.attr('href')).addClass("loading");
                $(tab.attr('href')).load(tab.attr('title'), function(responseText, textStatus, XMLHttpRequest) {
                    if (textStatus == 'error') {
                        $(tab.attr('href')).html('<p>There was an error making the AJAX request</p>');
                    }
                    else if (textStatus == "success") {
                        $(tab.attr('href')).removeClass("loading");
                    }
                });
            });
        });
    },
    loadUsersDropDown: function(reportName) {
        U.postUrl("/User/BranchList", { branchId: $("#" + reportName + "_BranchCode").val(), status: $("#" + reportName + "_Status").val() }, function(data) {
            var s = $("select#" + reportName + "_Users");
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select User --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false);
            });
        });
    },
    loadPatientsDropDown: function(reportName) {
        U.postUrl("/Patient/BranchList", { branchId: $("#" + reportName + "_BranchCode").val(), status: $("#" + reportName + "_Status").val() }, function(data) {
            var s = $("select#" + reportName + "_Patients");
            s.children('option').remove();
            s.get(0).options[0] = new Option("-- Select Patient --", "00000000-0000-0000-0000-000000000000", false, false);
            $.each(data, function(index, itemData) {
                s.get(0).options[s.get(0).options.length] = new Option(itemData.Name, itemData.Id, false, false);
            });
        });
    },
    Show: function(tabIndex, container, url) {
        var $tabs = $("#reportingTabs.tabs").tabs();
        $tabs.tabs('select', tabIndex);
        $(container).empty();
        $(container).load(url, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') $(container).html('<p>There was an error making the AJAX request</p>');
            else if (textStatus == "success") {
                $(container).removeClass("loading");
                Acore.OnWindowLoad($(container));
            }
        });
    },
    RebindPatientRoster: function() {
        var grid = $('#PatientRosterGrid').data('tGrid');
        if (grid != null) {
            grid.rebind({ BranchCode: $("#PatientRoster_BranchCode").val(), StatusId: $("#PatientRoster_Status").val(), InsuranceId: $("#PatientRoster_Insurance").val() });
        }
        var $exportLink = $('#PatientRoster_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchCode=([^&]*)/, 'BranchCode=' + $("#PatientRoster_BranchCode").val());
        href = href.replace(/StatusId=([^&]*)/, 'StatusId=' + $("#PatientRoster_Status").val());
        href = href.replace(/InsuranceId=([^&]*)/, 'InsuranceId=' + $("#PatientRoster_Insurance").val());
        $exportLink.attr('href', href);
    },
    RebindPatientEmergencyListing: function() {
        var grid = $('#PatientEmergencyListGrid').data('tGrid');
        if (grid != null) {
            grid.rebind({ StatusId: $("#PatientEmergencyList_Status").val(), BranchCode: $("#PatientEmergencyList_BranchCode").val() });
        }
        var $exportLink = $('#PatientEmergencyList_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchCode=([^&]*)/, 'BranchCode=' + $("#PatientEmergencyList_BranchCode").val());
        href = href.replace(/StatusId=([^&]*)/, 'StatusId=' + $("#PatientEmergencyList_Status").val());
        $exportLink.attr('href', href);
    },
    RebindPatientBirthdayListing: function() {
        var grid = $('#PatientBirthdayListGrid').data('tGrid');
        if (grid != null) {
            grid.rebind({ BranchId: $("#PatientBirthdayList_BranchCode").val(), month: $("#PatientBirthdayList_Month").val() });
        }
        var $exportLink = $('#PatientBirthdayList_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#PatientBirthdayList_BranchCode").val());
        href = href.replace(/month=([^&]*)/, 'month=' + ($("#PatientBirthdayList_Month").val()));
        $exportLink.attr('href', href);
    },
    RebindPatientAddressListing: function() {
        var grid = $('#PatientAddressListGrid').data('tGrid');
        if (grid != null) {
            grid.rebind({ BranchId: $("#PatientAddressList_BranchCode").val(), StatusId: $("#PatientAddressList_Status").val() });
        }
        var $exportLink = $('#PatientAddressList_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/StatusId=([^&]*)/, 'StatusId=' + $("#PatientAddressList_Status").val());
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#PatientAddressList_BranchCode").val());
        $exportLink.attr('href', href);
    },
    RebindPatientPhysicians: function() {
        var grid = $('#PatientByPhysiciansGrid').data('tGrid');
        if (grid != null) {
            grid.rebind({ AgencyPhysicianId: $("#PatientByPhysicians_PhysicanId").next().val() });
        }
        var $exportLink = $('#PatientByPhysicians_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/AgencyPhysicianId=([^&]*)/, 'AgencyPhysicianId=' + $("#PatientByPhysicians_PhysicanId").next().val());
        $exportLink.attr('href', href);
    },
    RebindClinicalOrders: function() {
        var grid = $('#Report_Patient_Orders_Grid').data('tGrid');
        if (grid != null) { grid.rebind({ StatusId: $("#Report_Patient_Orders_Status").val() }); }
    },
    RebindPatientSocCertPeriod: function() {
        var grid = $('#PatientSocCertPeriodListingGrid').data('tGrid');
        if (grid != null) { grid.rebind({ StatusId: $("#PatientSocCertPeriodListing_Status").val(), BranchId: $("#PatientSocCertPeriodListing_BranchCode").val(), StartDate: $("#PatientSocCertPeriodListing_StartDate").val(), EndDate: $("#PatientSocCertPeriodListing_EndDate").val() }); }
        var $exportLink = $('#PatientSocCertPeriodListing_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#PatientSocCertPeriodListing_BranchCode").val());
        href = href.replace(/StatusId=([^&]*)/, 'StatusId=' + $("#PatientSocCertPeriodListing_Status").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#PatientSocCertPeriodListing_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#PatientSocCertPeriodListing_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindPatientByResponsibleEmployee: function() {
        var grid = $('#PatientByResponsibleEmployeeListingGrid').data('tGrid');
        if (grid != null) { grid.rebind({ UserId: $("#PatientByResponsibleEmployeeListing_Users").val(), BranchCode: $("#PatientByResponsibleEmployeeListing_BranchCode").val(), StatusId: $("#PatientByResponsibleEmployeeListing_Status").val() }); }
        var $exportLink = $('#PatientByResponsibleEmployeeListing_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#PatientByResponsibleEmployeeListing_BranchCode").val());
        href = href.replace(/UserId=([^&]*)/, 'UserId=' + $("#PatientByResponsibleEmployeeListing_Users").val());
        href = href.replace(/StatusId=([^&]*)/, 'StatusId=' + $("#PatientByResponsibleEmployeeListing_Status").val());
        $exportLink.attr('href', href);
    },
    RebindPatientByResponsibleCaseManager: function() {
        var grid = $('#PatientByResponsibleCaseManagerGrid').data('tGrid');
        if (grid != null) { grid.rebind({ CaseManagerId: $("#PatientByResponsibleCaseManager_Users").val(), BranchCode: $("#PatientByResponsibleCaseManager_BranchCode").val(), StatusId: $("#PatientByResponsibleCaseManager_Status").val() }); }
        var $exportLink = $('#PatientByResponsibleCaseManager_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#PatientByResponsibleCaseManager_BranchCode").val());
        href = href.replace(/CaseManagerId=([^&]*)/, 'CaseManagerId=' + $("#PatientByResponsibleCaseManager_Users").val());
        href = href.replace(/StatusId=([^&]*)/, 'StatusId=' + $("#PatientByResponsibleCaseManager_Status").val());
        $exportLink.attr('href', href);
    },
    RebindClinicalMissedVisit: function() {
        var grid = $('#ClinicalMissedVisitGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ClinicalMissedVisit_BranchCode").val(), StartDate: $("#ReportClinicalMissedVisit_StartDate").val(), EndDate: $("#ReportClinicalMissedVisit_EndDate").val() }); }
        var $exportLink = $('#ClinicalMissedVisit_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchCode=([^&]*)/, 'BranchCode=' + $("#ClinicalMissedVisit_BranchCode").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#ReportClinicalMissedVisit_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#ReportClinicalMissedVisit_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindClinicalOpenOasis: function() {
        var grid = $('#ClinicalOpenOasisGrid').data('tGrid');
        if (grid != null) {
            grid.rebind({ BranchId: $("#ClinicalOpenOasis_BranchCode").val(), StartDate: $("#ClinicalOpenOasis_StartDate").val(), EndDate: $("#ClinicalOpenOasis_EndDate").val() });
        }
        var $exportLink = $('#ClinicalOpenOasis_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchCode=([^&]*)/, 'BranchCode=' + $("#ClinicalOpenOasis_BranchCode").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#ClinicalOpenOasis_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#ClinicalOpenOasis_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindSchedulePatientWeeklySchedule: function() {
        var grid = $('#SchedulePatientWeeklyScheduleGrid').data('tGrid');
        if (grid != null) {
            grid.rebind({ patientId: $("#SchedulePatientWeeklySchedule_Patients").val() });
        }
        var $exportLink = $('#SchedulePatientWeeklySchedule_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/patientId=([^&]*)/, 'patientId=' + $("#SchedulePatientWeeklySchedule_Patients").val());
        $exportLink.attr('href', href);
    },
    RebindScheduleEmployeeWeekly: function() {
        var grid = $('#ScheduleEmployeeWeeklyGrid').data('tGrid');
        if (grid != null) {
            grid.rebind({ BranchId: $("#ScheduleEmployeeWeekly_BranchCode").val(), userId: $("#ScheduleEmployeeWeekly_Users").val(), StartDate: $("#ScheduleEmployeeWeekly_StartDate").val(), EndDate: $("#ScheduleEmployeeWeekly_EndDate").val() });
        }
        var $exportLink = $('#ScheduleEmployeeWeekly_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/userId=([^&]*)/, 'userId=' + $("#ScheduleEmployeeWeekly_Users").val());
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ScheduleEmployeeWeekly_BranchCode").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#ScheduleEmployeeWeekly_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#ScheduleEmployeeWeekly_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindPatientMonthlySchedule: function() {
        var grid = $('#PatientMonthlyScheduleGrid').data('tGrid');
        if (grid != null) { grid.rebind({ patientId: $("#PatientMonthlySchedule_Patients").val(), month: $("#PatientMonthlySchedule_Month").val() }); }
        var $exportLink = $('#PatientMonthlySchedule_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/patientId=([^&]*)/, 'patientId=' + $("#PatientMonthlySchedule_Patients").val());
        href = href.replace(/month=([^&]*)/, 'month=' + ($("#PatientMonthlySchedule_Month").val()));
        $exportLink.attr('href', href);
    },
    RebindScheduleMonthlyWork: function() {
        var grid = $('#ScheduleMonthlyWorkGrid').data('tGrid');
        if (grid != null) { grid.rebind({ userId: $("#ScheduleMonthlyWork_Users").val(), month: $("#ScheduleMonthlyWork_Month").val(), BranchId: $("#ScheduleMonthlyWork_BranchCode").val() }); }
        var $exportLink = $('#ScheduleMonthlyWork_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/userId=([^&]*)/, 'userId=' + $("#ScheduleMonthlyWork_Users").val());
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ScheduleMonthlyWork_BranchCode").val());
        href = href.replace(/month=([^&]*)/, 'month=' + ($("#ScheduleMonthlyWork_Month").val()));
        $exportLink.attr('href', href);
    },
    RebindSchedulePastDueVisits: function() {
        var grid = $('#SchedulePastDueVisitsGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#SchedulePastDueVisits_BranchCode").val(), StartDate: $("#SchedulePastDueVisits_StartDate").val(), EndDate: $("#SchedulePastDueVisits_EndDate").val() }); }
        var $exportLink = $('#SchedulePastDueVisits_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#SchedulePastDueVisits_BranchCode").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#SchedulePastDueVisits_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#SchedulePastDueVisits_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindScheduleDailyWork: function() {
        var grid = $('#ScheduleDailyWorkGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ScheduleDailyWork_BranchCode").val(), Date: $("#ScheduleDailyWork_Date").val() }); }
        var $exportLink = $('#ScheduleDailyWork_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ScheduleDailyWork_BranchCode").val());
        href = href.replace(/Date=([^&]*)/, 'Date=' + ($("#ScheduleDailyWork_Date").val()));
        $exportLink.attr('href', href);
    },
    RebindOutstandingClaims: function() {
        var grid = $('#OutstandingClaimsGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#OutstandingClaims_BranchCode").val(), type: $("#OutstandingClaims_BillType").val() }); }
        var $exportLink = $('#OutstandingClaims_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#OutstandingClaims_BranchCode").val());
        href = href.replace(/type=([^&]*)/, 'type=' + $("#OutstandingClaims_BillType").val());
        $exportLink.attr('href', href);
    },
    RebindEmployeeRoster: function() {
        var grid = $('#EmployeeRosterGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#EmployeeRoster_BranchCode").val(), Status: $("#EmployeeRoster_Status").val() }); }
        var $exportLink = $('#EmployeeRoster_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#EmployeeRoster_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#EmployeeRoster_Status").val());
        $exportLink.attr('href', href);
    },
    RebindEmployeeBirthdayList: function() {
        var grid = $('#EmployeeBirthdayListGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#EmployeeBirthdayList_BranchCode").val(), Status: $("#EmployeeBirthdayList_Status").val(), month: $("#EmployeeBirthdayList_Month").val() }); }
        var $exportLink = $('#EmployeeBirthdayList_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#EmployeeBirthdayList_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#EmployeeBirthdayList_Status").val());
        href = href.replace(/month=([^&]*)/, 'month=' + ($("#EmployeeBirthdayList_Month").val()));
        $exportLink.attr('href', href);
    },
    RebindClaimsByStatus: function() {
        var grid = $('#ClaimsByStatusGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ClaimsByStatus_BranchCode").val(), type: $("#ClaimsByStatus_BillType").val(), Status: $("#ClaimsByStatus_Status").val(), StartDate: $("#ClaimsByStatus_StartDate").val(), EndDate: $("#ClaimsByStatus_EndDate").val() }); }
        var $exportLink = $('#ClaimsByStatus_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ClaimsByStatus_BranchCode").val());
        href = href.replace(/type=([^&]*)/, 'type=' + $("#ClaimsByStatus_BillType").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#ClaimsByStatus_Status").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#ClaimsByStatus_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#ClaimsByStatus_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindSubmittedClaims: function() {
        var grid = $('#SubmittedClaimsGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#SubmittedClaims_BranchCode").val(), type: $("#SubmittedClaims_BillType").val(), StartDate: $("#SubmittedClaims_StartDate").val(), EndDate: $("#SubmittedClaims_EndDate").val() }); }
        var $exportLink = $('#SubmittedClaims_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#SubmittedClaims_BranchCode").val());
        href = href.replace(/type=([^&]*)/, 'type=' + $("#SubmittedClaims_BillType").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#SubmittedClaims_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#SubmittedClaims_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindClinicalPhysicianOrderHistory: function() {
        var grid = $('#ClinicalPhysicianOrderHistoryGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ClinicalPhysicianOrderHistory_BranchCode").val(), Status: $("#ClinicalPhysicianOrderHistory_Status").val(), StartDate: $("#ClinicalPhysicianOrderHistory_StartDate").val(), EndDate: $("#ClinicalPhysicianOrderHistory_EndDate").val() }); }
        var $exportLink = $('#ClinicalPhysicianOrderHistory_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ClinicalPhysicianOrderHistory_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#ClinicalPhysicianOrderHistory_Status").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#ClinicalPhysicianOrderHistory_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#ClinicalPhysicianOrderHistory_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindClinicalPlanOfCareHistory: function() {
        var grid = $('#ClinicalPlanOfCareHistoryGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ClinicalPlanOfCareHistory_BranchCode").val(), Status: $("#ClinicalPlanOfCareHistory_Status").val(), StartDate: $("#ClinicalPlanOfCareHistory_StartDate").val(), EndDate: $("#ClinicalPlanOfCareHistory_EndDate").val() }); }
        var $exportLink = $('#ClinicalPlanOfCareHistory_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ClinicalPlanOfCareHistory_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#ClinicalPlanOfCareHistory_Status").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#ClinicalPlanOfCareHistory_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#ClinicalPlanOfCareHistory_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindEmployeeLicenseListing: function() {
        var grid = $('#EmployeeLicenseListingGrid').data('tGrid');
        if (grid != null) { grid.rebind({ userId: $("#EmployeeLicenseListing_Users").val() }); }
        var $exportLink = $('#EmployeeLicenseListing_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/userId=([^&]*)/, 'userId=' + $("#EmployeeLicenseListing_Users").val());
        $exportLink.attr('href', href);
    },
    RebindStatisticalPatientVisitHistory: function() {
        var grid = $('#StatisticalPatientVisitHistoryGrid').data('tGrid');
        if (grid != null) { grid.rebind({ patientId: $("#StatisticalPatientVisitHistory_Patients").val(), StartDate: $("#StatisticalPatientVisitHistory_StartDate").val(), EndDate: $("#StatisticalPatientVisitHistory_EndDate").val() }); }
        var $exportLink = $('#StatisticalPatientVisitHistory_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/patientId=([^&]*)/, 'patientId=' + $("#StatisticalPatientVisitHistory_Patients").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#StatisticalPatientVisitHistory_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#StatisticalPatientVisitHistory_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindStatisticalEmployeeVisitHistory: function() {
        var grid = $('#StatisticalEmployeeVisitHistoryGrid').data('tGrid');
        if (grid != null) { grid.rebind({ userId: $("#StatisticalEmployeeVisitHistory_Users").val(), StartDate: $("#StatisticalEmployeeVisitHistory_StartDate").val(), EndDate: $("#StatisticalEmployeeVisitHistory_EndDate").val() }); }
        var $exportLink = $('#StatisticalEmployeeVisitHistory_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/userId=([^&]*)/, 'userId=' + $("#StatisticalEmployeeVisitHistory_Users").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#StatisticalEmployeeVisitHistory_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#StatisticalEmployeeVisitHistory_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindAccountsReceivable: function() {
        var grid = $('#AccountsReceivableGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AccountsReceivable_BranchCode").val(), type: $("#AccountsReceivable_BillType").val(), Insurance: $("#AccountsReceivable_Insurance").val(), StartDate: $("#AccountsReceivable_StartDate").val(), EndDate: $("#AccountsReceivable_EndDate").val() }); }
        var $exportLink = $('#AccountsReceivable_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AccountsReceivable_BranchCode").val());
        href = href.replace(/Insurance=([^&]*)/, 'Insurance=' + $("#AccountsReceivable_Insurance").val());
        href = href.replace(/type=([^&]*)/, 'type=' + $("#AccountsReceivable_BillType").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AccountsReceivable_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#AccountsReceivable_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindBillingBatch: function() {
        var grid = $('#BillingBatchGrid').data('tGrid');
        if (grid != null) { grid.rebind({ ClaimType: $("#BillingBatch_ClaimType").val(), BatchDate: $("#BillingBatch_BatchDate").val() }); }
        var $exportLink = $('#BillingBatch_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/type=([^&]*)/, 'ClaimType=' + $("#BillingBatch_ClaimType").val());
        href = href.replace(/StartDate=([^&]*)/, 'BatchDate=' + $("#BillingBatch_BatchDate").val());
        $exportLink.attr('href', href);
    },
    RebindAgedAccountsReceivable: function() {
        var grid = $('#AgedAccountsReceivableGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgedAccountsReceivable_BranchCode").val(), type: $("#AgedAccountsReceivable_BillType").val(), Insurance: $("#AgedAccountsReceivable_Insurance").val(), StartDate: $("#AgedAccountsReceivable_StartDate").val(), EndDate: $("#AgedAccountsReceivable_EndDate").val() }); }
        var $exportLink = $('#AgedAccountsReceivable_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgedAccountsReceivable_BranchCode").val());
        href = href.replace(/Insurance=([^&]*)/, 'Insurance=' + $("#AgedAccountsReceivable_Insurance").val());
        href = href.replace(/type=([^&]*)/, 'type=' + $("#AgedAccountsReceivable_BillType").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AgedAccountsReceivable_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#AgedAccountsReceivable_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindPPSRAPClaims: function() {
        var grid = $('#PPSRAPClaimsGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#PPSRAPClaims_BranchCode").val() }); }
        var $exportLink = $('#PPSRAPClaims_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#PPSRAPClaims_BranchCode").val());
        $exportLink.attr('href', href);
    },
    RebindPPSFinalClaims: function() {
        var grid = $('#PPSFinalClaimsGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#PPSFinalClaims_BranchCode").val() }); }
        var $exportLink = $('#PPSFinalClaims_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#PPSFinalClaims_BranchCode").val());
        $exportLink.attr('href', href);
    },
    RebindSchedulePastDueVisitsByDiscipline: function() {
        var grid = $('#SchedulePastDueVisitsByDisciplineGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#SchedulePastDueVisitsByDiscipline_BranchCode").val(), discipline: $("#SchedulePastDueVisitsByDiscipline_Discipline").val(), StartDate: $("#SchedulePastDueVisitsByDiscipline_StartDate").val(), EndDate: $("#SchedulePastDueVisitsByDiscipline_EndDate").val() }); }
        var $exportLink = $('#SchedulePastDueVisitsByDiscipline_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#SchedulePastDueVisitsByDiscipline_BranchCode").val());
        href = href.replace(/discipline=([^&]*)/, 'discipline=' + $("#SchedulePastDueVisitsByDiscipline_Discipline").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#SchedulePastDueVisitsByDiscipline_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#SchedulePastDueVisitsByDiscipline_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindScheduleCaseManagerTask: function() {
        var grid = $('#ScheduleCaseManagerTaskGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ScheduleCaseManagerTask_BranchCode").val(), StartDate: $("#ScheduleCaseManagerTask_StartDate").val(), EndDate: $("#ScheduleCaseManagerTask_EndDate").val() }); }
        var $exportLink = $('#ScheduleCaseManagerTask_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ScheduleCaseManagerTask_BranchCode").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#ScheduleCaseManagerTask_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#ScheduleCaseManagerTask_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindPotentialClaimAutoCancel: function() {
        var grid = $('#PotentialClaimAutoCancelGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#PotentialClaimAutoCancel_BranchCode").val() }); }
        var $exportLink = $('#PotentialClaimAutoCancel_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#PotentialClaimAutoCancel_BranchCode").val());
        $exportLink.attr('href', href);
    },
    RebindCensusByPrimaryInsurance: function() {
        var grid = $('#CensusByPrimaryInsuranceGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#CensusByPrimaryInsurance_BranchCode").val(), insurance: $("#CensusByPrimaryInsurance_Insurance").val(), Status: $("#CensusByPrimaryInsurance_Status").val() }); }
        var $exportLink = $('#CensusByPrimaryInsurance_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#CensusByPrimaryInsurance_BranchCode").val());
        href = href.replace(/insurance=([^&]*)/, 'insurance=' + $("#CensusByPrimaryInsurance_Insurance").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#CensusByPrimaryInsurance_Status").val());
        $exportLink.attr('href', href);
    },
    RebindExpiringAuthorizations: function() {
        var grid = $('#ExpiringAuthorizationsGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ExpiringAuthorizations_BranchCode").val(), Status: $("#ExpiringAuthorizations_Status").val() }); }
        var $exportLink = $('#ExpiringAuthorizations_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ExpiringAuthorizations_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#ExpiringAuthorizations_Status").val());
        $exportLink.attr('href', href);
    },
    RebindEmployeeExpiringLicense: function() {
        var grid = $('#EmployeeExpiringLicenseGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#EmployeeExpiringLicense_BranchCode").val(), Status: $("#EmployeeExpiringLicense_Status").val() }); }
        var $exportLink = $('#EmployeeExpiringLicense_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#EmployeeExpiringLicense_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#EmployeeExpiringLicense_Status").val());
        $exportLink.attr('href', href);
    },
    RebindPatientSurveyCensus: function() {
        var grid = $('#PatientSurveyCensusGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#PatientSurveyCensus_BranchCode").val(), Status: $("#PatientSurveyCensus_Status").val() }); }
        var $exportLink = $('#PatientSurveyCensus_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#PatientSurveyCensus_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#PatientSurveyCensus_Status").val());
        $exportLink.attr('href', href);
    },
    RebindPatientVitalSigns: function() {
        var grid = $('#PatientVitalSignsGrid').data('tGrid');
        if (grid != null) { grid.rebind({ patientId: $("#PatientVitalSigns_Patients").val(), StartDate: $("#PatientVitalSigns_StartDate").val(), EndDate: $("#PatientVitalSigns_EndDate").val() }); }
        var $exportLink = $('#PatientVitalSigns_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/patientId=([^&]*)/, 'patientId=' + $("#PatientVitalSigns_Patients").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#PatientVitalSigns_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#PatientVitalSigns_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindPatientSixtyDaySummary: function() {
        var grid = $('#PatientSixtyDaySummaryGrid').data('tGrid');
        if (grid != null) { grid.rebind({ patientId: $("#PatientSixtyDaySummary_Patients").val() }); }
        var $exportLink = $('#PatientSixtyDaySummary_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/patientId=([^&]*)/, 'patientId=' + $("#PatientSixtyDaySummary_Patients").val());
        $exportLink.attr('href', href);
    },
    RebindScheduleDeviation: function() {
        var grid = $('#ScheduleDeviationGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ScheduleDeviation_BranchCode").val(), StartDate: $("#ScheduleDeviation_StartDate").val(), EndDate: $("#ScheduleDeviation_EndDate").val() }); }
        var $exportLink = $('#ScheduleDeviation_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ScheduleDeviation_BranchCode").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#ScheduleDeviation_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#ScheduleDeviation_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindClinicalThirteenAndNineteenVisitException: function() {
        var grid = $('#ClinicalThirteenAndNineteenVisitExceptionGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ClinicalThirteenAndNineteenVisitException_BranchCode").val() }); }
        var $exportLink = $('#ClinicalThirteenAndNineteenVisitException_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ClinicalThirteenAndNineteenVisitException_BranchCode").val());
        $exportLink.attr('href', href);
    },
    RebindClinicalNineteenTherapyReevaluationException: function() {
        var grid = $('#ClinicalNineteenTherapyReevaluationExceptionGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ClinicalNineteenTherapyReevaluationException_BranchCode").val() }); }
        var $exportLink = $('#ClinicalNineteenTherapyReevaluationException_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ClinicalNineteenTherapyReevaluationException_BranchCode").val());
        $exportLink.attr('href', href);
    },
    RebindClinicalThirteenTherapyReevaluationException: function() {
        var grid = $('#ClinicalThirteenTherapyReevaluationExceptionGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ClinicalThirteenTherapyReevaluationException_BranchCode").val() }); }
        var $exportLink = $('#ClinicalThirteenTherapyReevaluationException_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ClinicalThirteenTherapyReevaluationException_BranchCode").val());
        $exportLink.attr('href', href);
    },
    RebindStatisticalMonthlyAdmission: function() {
        var grid = $('#StatisticalMonthlyAdmissionGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#StatisticalMonthlyAdmission_BranchCode").val(), Status: $("#StatisticalMonthlyAdmission_Status").val(), month: $("#StatisticalMonthlyAdmission_Month").val(), year: $("#StatisticalMonthlyAdmission_Year").val() }); }
        var $exportLink = $('#StatisticalMonthlyAdmission_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#StatisticalMonthlyAdmission_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#StatisticalMonthlyAdmission_Status").val());
        href = href.replace(/month=([^&]*)/, 'month=' + ($("#StatisticalMonthlyAdmission_Month").val()));
        href = href.replace(/year=([^&]*)/, 'year=' + ($("#StatisticalMonthlyAdmission_Year").val()));
        $exportLink.attr('href', href);
    },
    RebindStatisticalAnnualAdmission: function() {
        var grid = $('#StatisticalAnnualAdmissionGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#StatisticalAnnualAdmission_BranchCode").val(), Status: $("#StatisticalAnnualAdmission_Status").val(), year: $("#StatisticalAnnualAdmission_Year").val() }); }
        var $exportLink = $('#StatisticalAnnualAdmission_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#StatisticalAnnualAdmission_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#StatisticalAnnualAdmission_Status").val());
        href = href.replace(/year=([^&]*)/, 'year=' + ($("#StatisticalAnnualAdmission_Year").val()));
        $exportLink.attr('href', href);
    },
    RebindStatisticalUnduplicatedCensusReport: function() {
        var grid = $('#StatisticalUnduplicatedCensusReportGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#StatisticalUnduplicatedCensusReport_BranchCode").val(), Status: $("#StatisticalUnduplicatedCensusReport_Status").val(), StartDate: $("#StatisticalUnduplicatedCensusReport_StartDate").val(), EndDate: $("#StatisticalUnduplicatedCensusReport_EndDate").val() }); }
        var $exportLink = $('#StatisticalUnduplicatedCensusReport_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#StatisticalUnduplicatedCensusReport_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#StatisticalUnduplicatedCensusReport_Status").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#StatisticalUnduplicatedCensusReport_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#StatisticalUnduplicatedCensusReport_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindEmployeeVisitByDateRange: function() {
        var grid = $('#EmployeeVisitByDateRangeGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#EmployeeVisitByDateRange_BranchCode").val(), StartDate: $("#EmployeeVisitByDateRange_StartDate").val(), EndDate: $("#EmployeeVisitByDateRange_EndDate").val() }); }
        var $exportLink = $('#EmployeeVisitByDateRange_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#EmployeeVisitByDateRange_BranchCode").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#EmployeeVisitByDateRange_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#EmployeeVisitByDateRange_EndDate").val());
        $exportLink.attr('href', href);
    },
    RebindSchedulePastDueRecet: function() {
        var grid = $('#SchedulePastDueRecetGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#SchedulePastDueRecet_BranchCode").val(), InsuranceId: $("#SchedulePastDueRecet_InsuranceId").val(), StartDate: $("#SchedulePastDueRecet_StartDate").val() }); }
        var $exportLink = $('#SchedulePastDueRecet_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#SchedulePastDueRecet_BranchCode").val());
        href = href.replace(/InsuranceId=([^&]*)/, 'InsuranceId=' + $("#SchedulePastDueRecet_InsuranceId").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#SchedulePastDueRecet_StartDate").val());
        $exportLink.attr('href', href);
    },
    RebindScheduleUpcomingRecet: function() {
        var grid = $('#ScheduleUpcomingRecetGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#ScheduleUpcomingRecet_BranchCode").val(), InsuranceId: $("#ScheduleUpcomingRecet_InsuranceId").val() }); }
        var $exportLink = $('#ScheduleUpcomingRecet_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ScheduleUpcomingRecet_BranchCode").val());
        href = href.replace(/InsuranceId=([^&]*)/, 'InsuranceId=' + $("#ScheduleUpcomingRecet_InsuranceId").val());
        $exportLink.attr('href', href);
    },
    RebindDischargePatients: function() {
        var grid = $('#DischargePatientsGrid').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#DischargePatients_BranchCode").val(), StartDate: $("#DischargePatients_StartDate").val(), EndDate: $("#DischargePatients_EndDate").val() }); }
        var $exportLink = $('#DischargePatients_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#DischargePatients_BranchCode").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#DischargePatients_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#DischargePatients_EndDate").val());
        $exportLink.attr('href', href);
    }
}


