﻿var HospitalizationLog = {
    assessmentType: "",
    Add: function(patientId) {
        Acore.Open("newhospitalizationlog", 'Patient/NewHospitalizationLog', function() { HospitalizationLog.InitNew(); }, { patientId: patientId });
    },
    InitNew: function() {
        $("#newHospitalizationLogForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.growl(result.errorMessage, "success");
                        } else U.growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Delete: function(patientId, hospitalizationLogId) {
        if (confirm("Are you sure you want to delete this Hospitalization Log?")) {
            U.postUrl('Patient/UpdateHospitalizationLogStatus', { patientId: patientId, hospitalizationLogId: hospitalizationLogId, isDeprecated: true }, function(result) {
                if (result.isSuccessful) {
                    U.growl(result.errorMessage, "success");
                } else U.growl(result.errorMessage, "error");
            })
        }
    },
    Edit: function(patientId, hospitalizationLogId) {
        Acore.Open("edithospitalizationlog", 'Patient/EditHospitalizationLog', function() { HospitalizationLog.InitEdit(); }, { patientId: patientId, hospitalizationLogId: hospitalizationLogId });
    },
    InitEdit: function(assessmentType) {
        $("#editHospitalizationLogForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    success: function(result) {
                        if (result.isSuccessful) {
                            U.growl(result.errorMessage, "success");
                        } else U.growl(result.errorMessage, "error");
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    }
}