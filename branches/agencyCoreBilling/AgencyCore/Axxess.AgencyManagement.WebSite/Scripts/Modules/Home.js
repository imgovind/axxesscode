﻿var Home = {
    Init: function() {
        Message.initWidget();

        if (Acore.GetRemoteContent) {
            U.postUrl("/Message/CustomWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(message) {
                if (message != undefined && message.Text != null && message.Text.length > 0) {
                    $('#customMessageWidget').html(message.Text);
                }
                else {
                    $('#customMessageWidget').html("<div class='align_center'><span class='bigtext'>Welcome to</span><br /><span class='img acorelogo'></span></div><div>AgencyCore&#8482; is a secure Enterpise Agency Management software designed from the ground up to provide powerful online capabilities that enables real-time collaboration for you and your team.</div>");
                }
            });
        }

        U.postUrl("/Report/PatientBirthdayWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data.Data != undefined) {
                for (var i = 0; i < data.Data.length && i < 5; i++) $('#birthdayWidgetContent').append("<tr><td>" + data.Data[i].BirthDay + "</td><td>" + data.Data[i].Age + "</td><td>" + data.Data[i].Name + "</td><td>" + data.Data[i].PhoneHomeFormatted + "</td></tr>");
            } else {
                $('#birthdayWidgetContent').append("<tr><td colspan='5' class='align_center'>No Messages found.</td></tr>");
            }
        });

        U.postUrl("/User/ScheduleWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data != undefined) {
                for (var i = 0; i < data.length && i < 5; i++) $('#scheduleWidgetContent').append("<tr><td>" + data[i].PatientName + "</td><td>" + data[i].TaskName + "</td><td>" + data[i].EventDate + "</td></tr>");
            }
            else {
                $('#scheduleWidgetContent').append("<tr><td colspan='5' class='align_center'><h1>No Scheduled Tasks found.</h1></td></tr>");
                $('#userScheduleWidgetMore').hide();
            }
        });

        U.postUrl("/Agency/RecertsPastDueWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data != undefined && data.length > 0) {
                for (var i = 0; i < data.length && i < 5; i++) $('#recertPastDueWidgetContent').append("<tr><td>" + data[i].PatientName + "</td><td>" + data[i].PatientIdNumber + "</td><td>" + data[i].TargetDateFormatted + "</td></tr>");
            } else {
                $('#recertPastDueWidgetContent').append("<tr><td colspan='3' class='align_center'><h1>No Past Due Recertifications found.</h1></td></tr>");
                $('#pastDueRecertsMore').hide();
            }
        });

        U.postUrl("/Agency/RecertsUpcomingWidget", "page=1&size=5&orderBy=&groupBy=&filter=", function(data) {
            if (data != undefined && data.length > 0) {
                for (var i = 0; i < data.length && i < 5; i++) $('#recertUpcomingWidgetContent').append("<tr><td>" + data[i].PatientName + "</td><td>" + data[i].PatientIdNumber + "</td><td>" + data[i].TargetDateFormatted + "</td></tr>");
            } else {
                $('#recertUpcomingWidgetContent').append("<tr><td colspan='3' class='align_center'><h1>No Upcoming Recertifications found.</h1></td></tr>");
                $('#upcomingRecertsMore').hide();
            }
        });

        U.postUrl("/Billing/Unprocessed", "", function(data) {
            if (data != undefined && data.length > 0) for (var i = 0; i < data.length && i < 5; i++) $('#claimsWidgetContent').append("<tr><td>" + data[i].LastName+ ", "+data[i].FirstName + "</td><td>" + data[i].Type + "</td><td>" + data[i].EpisodeRange + "</td></tr>");
            else $('#claimsWidgetContent').append("<tr><td colspan='3' class='align_center'><h1>No Bills found.</h1></td></tr>").closest('widget').find('#claimsWidgetMore').hide();
        });
    }
}