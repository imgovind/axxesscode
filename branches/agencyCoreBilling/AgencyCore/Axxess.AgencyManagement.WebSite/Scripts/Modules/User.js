﻿var User = {
    InitNew: function() {
        if ($("#newUserForm").length) {
            $(".numeric").numeric();
            $(".names").alpha({ nocaps: false });
            $('#New_User_HomePhoneArray1').autotab({ target: 'New_User_HomePhoneArray2', format: 'numeric' });
            $('#New_User_HomePhoneArray2').autotab({ target: 'New_User_HomePhoneArray3', format: 'numeric', previous: 'New_User_HomePhoneArray1' });
            $('#New_User_HomePhoneArray3').autotab({ target: 'New_User_MobilePhoneArray1', format: 'numeric', previous: 'New_User_HomePhoneArray2' });
            $('#New_User_MobilePhoneArray1').autotab({ target: 'New_User_MobilePhoneArray2', format: 'numeric' });
            $('#New_User_MobilePhoneArray2').autotab({ target: 'New_User_MobilePhoneArray3', format: 'numeric', previous: 'New_User_MobilePhoneArray1' });
            $('#New_User_MobilePhoneArray3').autotab({ target: 'New_User_TitleType', format: 'numeric', previous: 'New_User_MobilePhoneArray2' });
            U.showIfOtherSelected($("#New_User_TitleType"), $("#New_User_OtherTitleType"));
            U.showIfOtherSelected($("#New_User_Credentials"), $("#New_User_OtherCredentials"));
            $('#New_User_AllPermissions').change(function() {
                if ($('#New_User_AllPermissions').attr('checked')) {
                    $('input[name="PermissionsArray"]').each(function() {
                        if (!$(this).attr('checked')) {
                            $(this).attr('checked', true);
                        }
                    });
                }
                else {
                    $('input[name="PermissionsArray"]').each(function() {
                        if ($(this).attr('checked')) {
                            $(this).attr('checked', false);
                        }
                    });
                }
            });
            $("#newUserForm").validate({
                submitHandler: function(form) {
                    var options = {
                        dataType: 'json',
                        clearForm: false,
                        beforeSubmit: function(values, form, options) {
                        },
                        success: function(result) {
                            var resultObject = eval(result);
                            if (resultObject.isSuccessful) {
                                User.RebindList();
                                $.jGrowl("New user successfully added.", { theme: 'success', life: 5000 });
                                UserInterface.CloseWindow('newuser');
                            } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return true;
                }
            })
        }
    },
    InitEdit: function() {
        $('#Edit_User_HomePhoneArray1').autotab({ target: 'Edit_User_HomePhoneArray2', format: 'numeric' });
        $('#Edit_User_HomePhoneArray2').autotab({ target: 'Edit_User_HomePhoneArray3', format: 'numeric', previous: 'Edit_User_HomePhoneArray1' });
        $('#Edit_User_HomePhoneArray3').autotab({ target: 'Edit_User_MobilePhoneArray1', format: 'numeric', previous: 'Edit_User_HomePhoneArray2' });

        $('#Edit_User_MobilePhoneArray1').autotab({ target: 'Edit_User_MobilePhoneArray2', format: 'numeric' });
        $('#Edit_User_MobilePhoneArray2').autotab({ target: 'Edit_User_MobilePhoneArray3', format: 'numeric', previous: 'Edit_User_MobilePhoneArray1' });
        $('#Edit_User_MobilePhoneArray3').autotab({ target: 'Edit_User_SaveButton', format: 'numeric', previous: 'Edit_User_MobilePhoneArray2' });

        $('#Edit_User_AllPermissions').change(function() {
            if ($('#Edit_User_AllPermissions').attr('checked')) {
                $('input[name="PermissionsArray"]').each(function() {
                    if (!$(this).attr('checked')) {
                        $(this).attr('checked', true);
                    }
                });
            }
            else {
                $('input[name="PermissionsArray"]').each(function() {
                    if ($(this).attr('checked')) {
                        $(this).attr('checked', false);
                    }
                });
            }
        });

        U.showIfOtherSelected($("#Edit_User_TitleType"), $("#Edit_User_OtherTitleType"));
        U.showIfOtherSelected($("#Edit_User_Credentials"), $("#Edit_User_OtherCredentials"));

        $("#editUserForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            User.RebindList();
                            $.jGrowl("User successfully updated.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('edituser');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        var file_input_index = 0;
        $('#editUserLicenseForm input[type=file]').each(function() {
            file_input_index++;
            $(this).wrap('<div id="Edit_UserLicense_Attachment_Container_' + file_input_index + '"></div>');
        });
        $("#editUserLicenseForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            $.jGrowl("License added successfully.", { theme: 'success', life: 5000 });
                            var grid = $('#List_User_Licenses').data('tGrid');
                            if (grid != null) {
                                grid.rebind();
                            }
                            $("#editUserLicenseForm").clearForm();
                            $("#Edit_UserLicense_Attachment_Container_1").html($("#Edit_UserLicense_Attachment_Container_1").html());
                        } else $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            $.jGrowl("License added successfully.", { theme: 'success', life: 5000 });
                            var grid = $('#List_User_Licenses').data('tGrid');
                            if (grid != null) {
                                grid.rebind();
                            }
                            $("#editUserLicenseForm").clearForm();
                            $("#Edit_UserLicense_Attachment_Container_1").html($("#Edit_UserLicense_Attachment_Container_1").html());
                        } else $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });

        $("#editUserPermissionsForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl("User permissions updated successfully.", { theme: 'success', life: 5000 });
                        } else $.jGrowl($.trim(result.errorMessage), { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitProfile: function() {
        U.PhoneAutoTab("Edit_Profile_PhonePrimary");
        U.PhoneAutoTab("Edit_Profile_PhoneAlternate");
        $("#editProfileForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Profile updated successfully.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('editprofile');
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitForgotSignature: function() {
        $("#lnkRequestSignatureReset").click(function() {
            U.postUrl("/Signature/Email", null, function(result) {
                if (result.isSuccessful) {
                    $.jGrowl("Your request to reset your signature was successful. Please check your e-mail.", { theme: 'success', life: 5000 });
                    UserInterface.CloseWindow('forgotsignature');
                }
                else {
                    $("#resetSignatureMessage").addClass("errormessage").html(result.errorMessage);
                }
            });
        });
    },
    InitMissedVisit: function() {
        $("#newMissedVisitForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $.jGrowl("Missed Visit successfully created.", { theme: 'success', life: 5000 });
                            UserInterface.CloseWindow('newmissedvisit');
                            User.RebindScheduleList();
                            UserInterface.CloseModal();
                        } else $.jGrowl(resultObject.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    SelectPermissions: function(control, className) {
        if (control.attr('checked')) {
            $(className).each(function() {
                if (!$(this).attr('checked')) {
                    $(this).attr('checked', true);
                }
            });
        }
        else {
            $(className).each(function() {
                if ($(this).attr('checked')) {
                    $(this).attr('checked', false);
                }
            });
        }
    },
    RebindList: function() {
        var activegrid = $('#List_User').data('tGrid');
        if (activegrid != null) {
            activegrid.rebind();
        }

        var inactivegrid = $('#List_UserInactive').data('tGrid');
        if (inactivegrid != null) {
            inactivegrid.rebind();
        }
    },
    RebindScheduleList: function() {
        var grid = $('#List_User_Schedule').data('tGrid');
        if (grid != null) {
            grid.rebind();
        }
    },
    LoadUserSchedule: function(groupName) {
        $("#myScheduledTasksContentId").empty();
        $("#myScheduledTasksContentId").addClass("loading");
        $("#myScheduledTasksContentId").load('User/ScheduleGrouped', { groupName: groupName }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $.jGrowl('Schedule List could not be grouped. Please close this window and try again.', { theme: 'error', life: 5000 });
            }
            else if (textStatus == "success") {
                $("#myScheduledTasksContentId").removeClass("loading");
            }
        });
    },
    Delete: function(userId) {
        if (confirm("Are you sure you want to delete this user?")) {
            var input = "userId=" + userId;
            U.postUrl("/User/Delete", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    Activate: function(userId) {
        if (confirm("Are you sure you want to activate this user?")) {
            var input = "userId=" + userId;
            U.postUrl("/User/Activate", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    Deactivate: function(userId) {
        if (confirm("Are you sure you want to deactivate this user?")) {
            var input = "userId=" + userId;
            U.postUrl("/User/Deactivate", input, function(result) {
                if (result.isSuccessful) {
                    User.RebindList();
                    User.RebindScheduleList();
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    },
    NavigateMonthlyCalendar: function(month, year) {
        $('#window_userschedulemonthlycalendar_content').load('/User/UserCalendarNavigate', { month: month, year: year }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#window_userschedulemonthlycalendar_content').html(unescape("%3Cdiv class=%22ajaxerror%22%3E%3Cspan class=%22img icon error%22%3E%3C/span%3E%3Ch1%3EThere " +
                    "was an error loading this window.%3C/h1%3E%3Cbr /%3EPlease exit out and try again. If this problem persists, contact Axxess for fu" +
                    "rther assistance.%3C/div%3E"));
            }

        });
    }
}