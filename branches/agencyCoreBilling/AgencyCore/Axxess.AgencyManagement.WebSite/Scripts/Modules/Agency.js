﻿var Agency = {
    Edit: function(Id) { Acore.Open("editagency", 'Agency/Edit', function() { Agency.InitEdit(); }, { id: Id }); },
    InitEdit: function() { U.InitEditTemplate("Agency"); },
    InitEditCost: function() { U.InitTemplate($("#editVisitCostForm"), function() { UserInterface.CloseWindow('visitrates'); }, "Visit rate successfully updated"); },
    InitNew: function() {
        Lookup.loadStates();
        U.PhoneAutoTab('New_Agency_Phone');
        U.PhoneAutoTab('New_Agency_Fax');
        U.PhoneAutoTab('New_Agency_SubmitterPhone');
        U.PhoneAutoTab('New_Agency_SubmitterFax');
        U.PhoneAutoTab('New_Agency_ContactPhone');
        $(".names").alpha({ nocaps: false });

        $("input[name=New_Agency_SameAsAdmin]").click(function() {
            if ($(this).is(':checked')) {
                $("#New_Agency_ContactPersonEmail").val($("#New_Agency_AdminUsername").val());
                $("#New_Agency_ContactPersonFirstName").val($("#New_Agency_AdminFirstName").val());
                $("#New_Agency_ContactPersonLastName").val($("#New_Agency_AdminLastName").val());
            } else $("#New_Agency_ContactPersonEmail").add("#New_Agency_ContactPersonFirstName").add("#New_Agency_ContactPersonLastName").val('');
        });
        $("input[name=New_Agency_AxxessBiller]").click(function() {
            if ($(this).is(':checked')) {
                $("#New_Agency_SubmitterId").val("SW23071");
                $("#New_Agency_SubmitterName").val("Axxess Healthcare Consult");
                $("#New_Agency_SubmitterPhone1").val("214");
                $("#New_Agency_SubmitterPhone2").val("575");
                $("#New_Agency_SubmitterPhone3").val("7711");
                $("#New_Agency_SubmitterFax1").val("214");
                $("#New_Agency_SubmitterFax2").val("575");
                $("#New_Agency_SubmitterFax3").val("7722");
            }
            else {
                $("#New_Agency_SubmitterId").add("#New_Agency_SubmitterName").add("input[name=SubmitterPhoneArray]").add("input[name=SubmitterFaxArray]").val("");
            }
        });
        $("input[name=IsAgreementSigned]").change(function() {
            if ($(this).val() == "true") $("#New_Agency_TrialPeriod").val("").attr("disabled", "disabled");
            else $("#New_Agency_TrialPeriod").removeAttr("disabled");
        });
        U.InitNewTemplate("Agency");
    },
    loadCaseManagement: function(groupName) {
        $("#caseManagementContentId").empty().addClass("loading").load('Agency/CaseManagementContent', { groupName: groupName, BranchId: $("#CaseManagement_BranchCode").val(), Status: $("#CaseManagement_Status").val() }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.growl('Case Management List could not be grouped. Please close this window and try again.', 'error');
            }
            else if (textStatus == "success") {
                $("#caseManagementContentId").removeClass("loading");
                $("#caseManagementContentId .t-group-indicator").hide();
                $("#caseManagementContentId .t-grouping-header").remove();
                $(".t-grid-content", "#window_caseManagement").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
                var $exportLink = $('#CaseManagement_ExportLink');
                var href = $exportLink.attr('href');
                if (href != null) {
                    href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#CaseManagement_BranchCode").val());
                    href = href.replace(/Status=([^&]*)/, 'Status=' + $("#CaseManagement_Status").val());
                    $exportLink.attr('href', href);
                }
            }
        });
    },
    tooltip: function() {
        $("a.tooltip", $("#caseManagementGrid")).each(function() {
            if ($(this).hasClass("blue_note")) var c = "blue_note";
            if ($(this).hasClass("red_note")) var c = "red_note";
            if ($(this).attr("tooltip")) {
                $(this).click(function() { UserInterface.ShowNoteModal($(this).attr("tooltip"), ($(this).hasClass("blue_note") ? "blue" : "") + ($(this).hasClass("red_note") ? "red" : "")) });
                $(this).tooltip({
                    track: true,
                    showURL: false,
                    top: 5,
                    left: -15,
                    extraClass: c,
                    bodyHandler: function() {
                        return $(this).attr("tooltip");
                    }
                });
            } else $(this).hide();
        });
    },
    loadPrintQueue: function(groupName) {
        $("#printQueueContentId").empty().addClass("loading").load('Agency/PrintQueueContent', { groupName: groupName }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                U.growl('Print Queue could not be grouped. Please close this window and try again.', 'error');
            }
            else if (textStatus == "success") {
                $("#printQueueContentId .t-group-indicator").hide();
                $("#printQueueContentId .t-grouping-header").remove();
                $(".t-grid-content", "#window_printqueue").css({ 'height': 'auto', 'position': 'absolute', 'top': '25px' });
                $("#printQueueContentId").removeClass("loading");
            }
        });
    },
    loadVisitRate: function() { Acore.Open("visitrates", 'Agency/VisitRates', function() { Agency.InitEditCost(); }); },
    loadVisitRateContent: function(branchId) {
        $("#Edit_VisitRate_Container").empty().addClass("loading").load('Agency/VisitRateContent', { branchId: branchId }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') U.growl('Visit rates could not be loaded based on the selected branch. Please close this window and try again.', 'error');
            else if (textStatus == "success") $("#Edit_VisitRate_Container").removeClass("loading");
        });
    },
    MarkOrder: function(mark, control) {
        fields = $("select.SendAutomatically, input.OrdersToBeSent:checked", $(control)).serializeArray();
        U.postUrl("/Agency/MarkOrdersAs" + mark, fields, function(data) {
            if (data.isSuccessful) {
                Agency.RebindOrders();
                Patient.Rebind();
                Schedule.Rebind();
            } else U.growl(data.errorMessage, 'error');
        });
    },
    MarkOrdersAsReturned: function(container) { Agency.MarkOrder("Returned", container); },
    MarkOrdersAsSent: function(container) { Agency.MarkOrder("Sent", container); },
    RebindCaseManagement: function() { Agency.loadCaseManagement($("#CaseManagement_GroupName").val()); },
    RebindPrintQueue: function() { U.rebindTGrid($('#printQueueGrid')); },
    RebindList: function() { U.rebindTGrid($('#List_Agencies')); },
    RebindOrders: function() { U.rebindTGrid($('#List_OrdersToBeSent')); U.rebindTGrid($('#List_OrdersPendingSignature')); },
    OrderHistoryEditInit: function() { U.InitTemplate($("#updateOrderHistry"), function() { Agency.RebindOrdersHistory(); UserInterface.CloseModal(); }, "Order successfully updated"); },
    PendingSignatureOrdersOnEdit: function(e) {
        var form = e.form;
        var dataItem = e.dataItem;
        if (dataItem != null) {
            dataItem.StartDate = $("#OrdersPendingSignature_StartDate").val();
            dataItem.EndDate = $("#OrdersPendingSignature_EndDate").val();
            dataItem.BranchId = $("#OrdersPendingSignature_BranchId").val();
            e.dateItem = dataItem;
        }
    },
    OrderCenterOnload: function(e) { $('.t-grid-edit', e.row).html('Receive Order'); },
    RebindOrdersHistory: function() { Agency.GenerateClick("OrdersHistory"); },
    RebindPendingOrders: function() { Agency.GenerateClick("OrdersPendingSignature"); },
    RebindOrdersToBeSent: function() { Agency.GenerateClick("OrdersToBeSent"); },
    GenerateClick: function(type) {
        var grid = $("#List_" + type).data('tGrid'),
            data = { BranchId: $("#" + type + "_BranchId").val(), StartDate: $("#" + type + "_StartDate").val(), EndDate: $("#" + type + "_EndDate").val() };
        if (type == "OrdersToBeSent") data.sendAutomatically = $("#List_OrdersToBeSent_SendType").val();
        if (grid != null) grid.rebind(data);
        var link = $("#" + type + "_ExportLink").attr("href");
        if (link) {
            link = link.replace(/BranchId=([^&]*)/, "BranchId=" + $("#" + type + "_BranchId").val());
            link = link.replace(/StartDate=([^&]*)/, "StartDate=" + escape($("#" + type + "_StartDate").val()));
            link = link.replace(/EndDate=([^&]*)/, "EndDate=" + escape($("#" + type + "_EndDate").val()));
            if (type == "OrdersToBeSent") { link = link.replace(/sendAutomatically=([^&]*)/, "sendAutomatically=" + $("#List_OrdersToBeSent_SendType").val()); }
            $("#" + type + "_ExportLink").attr("href", link);
        }
    },
    RebindAgencyCommunicationNotes: function() {
        var grid = $('#List_CommunicationNote').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgencyCommunicationNote_BranchCode").val(), Status: $("#AgencyCommunicationNote_Status").val(), StartDate: $("#AgencyCommunicationNote_StartDate-input").val(), EndDate: $("#AgencyCommunicationNote_EndDate-input").val() }); }
        var $exportLink = $('#AgencyCommunicationNote_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyCommunicationNote_BranchCode").val());
        href = href.replace(/Status=([^&]*)/, 'Status=' + $("#AgencyCommunicationNote_Status").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AgencyCommunicationNote_StartDate-input").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#AgencyCommunicationNote_EndDate-input").val());
        $exportLink.attr('href', href);
    },
    RebindAgencyPastDueRecet: function() {
        var grid = $('#List_PastDueRecerts').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgencyPastDueRecet_BranchCode").val(), InsuranceId: $("#AgencyPastDueRecet_InsuranceId").val(), StartDate: $("#AgencyPastDueRecet_StartDate").val() }); }
        var $exportLink = $('#AgencyPastDueRecet_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyPastDueRecet_BranchCode").val());
        href = href.replace(/InsuranceId=([^&]*)/, 'InsuranceId=' + $("#AgencyPastDueRecet_InsuranceId").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#AgencyPastDueRecet_StartDate").val());
        $exportLink.attr('href', href);
    },
    RebindAgencyUpcomingRecet: function() {
        var grid = $('#List_UpcomingRecerts').data('tGrid');
        if (grid != null) { grid.rebind({ BranchId: $("#AgencyUpcomingRecet_BranchCode").val(), InsuranceId: $("#AgencyUpcomingRecet_InsuranceId").val() }); }
        var $exportLink = $('#AgencyUpcomingRecet_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#AgencyUpcomingRecet_BranchCode").val());
        href = href.replace(/InsuranceId=([^&]*)/, 'InsuranceId=' + $("#AgencyUpcomingRecet_InsuranceId").val());
        $exportLink.attr('href', href);
    },
    RebindExportedOasis: function() {
        var grid = $('#exportedOasisGrid').data('tGrid'); if (grid != null) { grid.rebind({ BranchId: $("#ExportedOasis_BranchCode").val(), Status: $("#ExportedOasis_Status").val(), StartDate: $("#ExportedOasis_StartDate").val(), EndDate: $("#ExportedOasis_EndDate").val() }); }
        var $exportLink = $('#ExportedOasis_ExportLink');
        var href = $exportLink.attr('href');
        if (href != null) {
            href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#ExportedOasis_BranchCode").val());
            href = href.replace(/Status=([^&]*)/, 'Status=' + $("#ExportedOasis_Status").val());
            href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#ExportedOasis_StartDate").val());
            href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#ExportedOasis_EndDate").val());
            $exportLink.attr('href', href);
        }
    },
    RebindOasisToExport: function() {
        var sources = [];
        $("#generateOasis input[name='paymentSources']:checked").each(function() {
            sources.push($(this).val());
        });
        var grid = $('#generateOasisGrid').data('tGrid'); if (grid != null) { grid.rebind({ BranchId: $("#OasisExport_BranchCode").val(), paymentSources: sources }); }
        var $exportLink = $('#OasisExport_ExportLink');
        var href = $exportLink.attr('href');
        if (href != null) {
            href = href.replace(/BranchId=([^&]*)/, 'BranchId=' + $("#OasisExport_BranchCode").val());
            href = href.replace(/paymentSources=([^&]*)/, 'paymentSources=' + sources);
            $exportLink.attr('href', href);
        }
    }
}