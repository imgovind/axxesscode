﻿var Referral = {
    Delete: function(Id) { U.DeleteTemplate("Referral", Id); },
    InitEdit: function() {
        U.PhoneAutoTab("Edit_Referral_HomePhone");
        U.InitEditTemplate("Referral");
        $("#window_editreferral .Physicians").PhysicianInput();
        $("#EditReferral_NewPhysician").click(function() {
            var PhysId = $("#EditReferral_PhysicianSelector").next("input[type=hidden]").val();
            $("#EditReferral_PhysicianSelector").AjaxAutocomplete("reset");
            if (U.IsGuid(PhysId)) Patient.AddPhysician(PhysId, $("#Edit_Referral_Id").val());
            else U.growl("Please Select a Physician", "error");
        });
    },
    InitNew: function() {
        U.PhoneAutoTab("New_Referral_HomePhone");
        U.showIfChecked($("#New_Referral_DMEOther"), $("#New_Referral_OtherDME"));
        $("#window_newreferral .Physicians").PhysicianInput();
        U.InitNewTemplate("Referral");
    },
    InitNonAdmit: function() {
        $("#NonAdmit_Referral_Date").datepicker("hide");
        $("#newNonAdmitReferralForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl("Referral non-admission successful.", { theme: 'success', life: 5000 });
                            Referral.RebindList();
                            UserInterface.CloseModal();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return true;
            }
        });
    },
    RebindList: function() { U.rebindTGrid($('#List_Referral')); },
    Admit: function(id) {
        Acore.Open("newpatient", 'Patient/New', function() { Patient.InitNew(); }, { referralId: id });
    },
    AddPhysician: function(id, referralId) {
        U.TGridAjax("/Referral/AddPhysician", { "id": id, "referralId": referralId }, $('#EditReferral_PhysicianGrid'));
        $("#EditReferral_PhysicianSelector").AjaxAutocomplete("reset").next("input[type=hidden]").val("");
    },
    DeletePhysician: function(id, referralId) {
        if (confirm("Are you sure you want to delete")) U.TGridAjax("/Referral/DeletePhysician", { "id": id, "referralId": referralId }, $('#EditReferral_PhysicianGrid'));
    },
    SetPrimaryPhysician: function(id, referralId) {
        if (confirm("Are you sure you want to set this physician as primary?")) U.TGridAjax("/Referral/SetPrimaryPhysician", { "id": id, "referralId": referralId }, $('#EditReferral_PhysicianGrid'));
    }
}