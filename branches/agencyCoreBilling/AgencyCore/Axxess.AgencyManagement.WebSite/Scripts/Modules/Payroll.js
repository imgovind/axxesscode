﻿var Payroll = {
    InitSearch: function() {
        $("#searchPayrollForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'html',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        $("#payrollSearchResultDetails").hide();
                        $("#payrollSearchResultDetail").hide();
                        $("#payrollMarkAsPaidButton").hide();
                        $("#payrollSearchResult").show();
                        $('#payrollSearchResult').html(result);
                        $('#payroll_print').removeClass('hidden').bind("mouseup", Payroll.Print);
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    Print: function() {
        if ($("#payrollSearchResult").is(":visible")) $("a", this).click(function() {
            U.GetAttachment("Payroll/SummaryPdf", { 'payrollStartDate': $("#payrollStartDate").val(), 'payrollEndDate': $("#payrollEndDate").val(), 'payrollStatus': $("#payrollStatus").val() });
        });
        else if ($("#payrollSearchResultDetails ul").length > 1) $("a", this).click(function() {
            U.GetAttachment("Payroll/SummaryDetailsPdf", { 'payrollStartDate': $("#payrollStartDate").val(), 'payrollEndDate': $("#payrollEndDate").val(), 'payrollStatus': $("#payrollStatus").val() });
        });
        else $("a", this).click(function() {
            U.GetAttachment("Payroll/SummaryDetailPdf", { 'userId': $("#payrollUserId").text(), 'payrollStartDate': $("#payrollStartDate").val(), 'payrollEndDate': $("#payrollEndDate").val(), 'payrollStatus': $("#payrollStatus").val() });
        });
    },
    LoadDetail: function(userId) {
        $("#payrollSearchType").val(userId);
        $('#payrollSearchResultDetail').load('Payroll/Detail', { userId: userId, payrollStartDate: $("#payrollStartDate").val(), payrollEndDate: $("#payrollEndDate").val(), payrollStatus: $("#payrollStatus").val() }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') $('#payrollSearchResultDetail').html('<p>There was an error making the AJAX request</p>');
            else if (textStatus == "success") { $("#payrollSearchResult").hide(); $("#payrollSearchResultDetails").hide(); $("#markAsPaidButtonId").val("#payrollSearchResultDetail"); }
            $("#payrollSearchResultDetail").show();
            $("#payrollMarkAsPaidButton").show();
        });
    },
    LoadDetails: function() {
        $("#payrollSearchType").val("");
        $('#payrollSearchResultDetails').load('Payroll/Details', { payrollStartDate: $("#payrollStartDate").val(), payrollEndDate: $("#payrollEndDate").val(), payrollStatus: $("#payrollStatus").val() }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') $('#payrollSearchResultDetails').html('<p>There was an error making the AJAX request</p>');
            else if (textStatus == "success") { $("#payrollSearchResult").hide(); $("#payrollSearchResultDetail").hide(); $("#markAsPaidButtonId").val("#payrollSearchResultDetails"); }
            $("#payrollSearchResultDetails").show();
            $("#payrollMarkAsPaidButton").show();
        });
    },
    MarkAsPaid: function() {
        var control = $("#markAsPaidButtonId").val();
        if ($("input[name=visitSelected]:checked").length > 0) {
            U.postUrl('Payroll/PayVisit', $("input[name=visitSelected]:checked", $(control)).serializeArray(), function(result) {
                if (result.isSuccessful) {
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                    if ($("#payrollSearchType").val().length > 0) Payroll.LoadDetail($("#payrollSearchType").val());
                    else Payroll.LoadDetails();
                } else { $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 }); }
            }, null);
        } else { $.jGrowl("Select at least one visit to mark as paid.", { theme: 'error', life: 5000 }); }
    },
    MarkAsUnpaid: function() {
        var control = $("#markAsPaidButtonId").val();
        if ($("input[name=visitSelected]:checked").length > 0) {
            U.postUrl('Payroll/UnpayVisit', $("input[name=visitSelected]:checked", $(control)).serializeArray(), function(result) {
                if (result.isSuccessful) {
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                    if ($("#payrollSearchType").val().length > 0) Payroll.LoadDetail($("#payrollSearchType").val());
                    else Payroll.LoadDetails();
                } else { $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 }); }
            }, null);
        } else { $.jGrowl("Select at least one visit to set as unpaid.", { theme: 'error', life: 5000 }); }
    }
}