﻿var PdfPrint = {
    Fields: {},
    BuildBasic: function(Content) {
        // This is for print views with a single text ares for possible page spanning
        this.Loading();
        Content = this.SplitTextArea(Content);
        this.NewPage();
        do {
            this.GetDomPointer().append(Content[0] + " ");
            if (this.DetectOverflow()) Content[0] = this.PageBreak(Content[0]);
            else Content.shift();
        } while (Content.length);
        this.Finalize();
    },
    BuildPlanOfCare: function(Sections) {
        // This is just for the 485/487 Print Views
        this.NewPage();
        $(".content").remove();
        do {
            $(".page:first").append($("<div/>", { "class": "cms485" + Sections[0].Loc }));
            var DomPointer = $(".cms485" + Sections[0].Loc);
            var Content = this.SplitTextArea(Sections[0].Data);
            do {
                DomPointer.append(Content[0] + " ");
                if (this.DetectOverflow(DomPointer) || this.DetectOverflow($(".content:last"))) {
                    if ($(".content").length == 0 || this.DetectOverflow($(".content:last"))) Content[0] = this.PageBreak(Content[0], DomPointer);
                    $(".content:last").append(
                        $("<span/>", { "class": "cms487" + Sections[0].Loc, "text": Sections[0].Loc + ". " + Sections[0].Title }).append(
                            $("<br/>")
                        )
                    );
                    DomPointer = $(".cms487" + Sections[0].Loc + ":last");
                } else Content.shift();
            } while (Content.length);
            Sections.shift();
        } while (Sections.length);
        $(".page").addClass("cms487");
        $(".page:first").removeClass("cms487").addClass("cms485");
        $(".cms485 div").each(function() { if ($(this).attr("class").match(/cms487/)) $(this).remove() });
        $(".cms487 div").each(function() { if ($(this).attr("class").match(/cms485/)) $(this).remove() });
        this.Finalize();
    },
    BuildSections: function(Sections) {
        // This is for standard non-nested section based JSON print views
        this.NewPage();
        do {
            this.PrepareContentSection(Sections[0]);
            var Content = this.GenerateContent(Sections[0]);
            do {
                this.GetDomPointer().append(Content[0] + " ");
                if (this.DetectOverflow()) {
                    Content[0] = this.PageBreak(Content[0]);
                    this.PrepareContentSection(Sections[0]);
                } else Content.shift();
            } while (Content.length);
            Sections.shift();
        } while (Sections.length);
        this.Finalize();
    },
    BuildNestedSections: function(Sections) {
        // This is for complex nested section based JSON print views
        this.NewPage();
        do {
            this.PrepareContentSection(Sections[0]);
            do {
                this.PrepareContentSection(Sections[0].Section[0]);
                var Content = this.GenerateContent(Sections[0].Section[0]);
                do {
                    this.GetDomPointer().append(Content[0] + " ");
                    if (this.DetectOverflow()) {
                        Content[0] = this.PageBreak(Content[0]);
                        this.PrepareContentSection(Sections[0].Section[0]);
                    } else Content.shift();
                } while (Content.length);
                Sections[0].Section.shift();
            } while (Sections[0].Section.length);
            Sections.shift();
        } while (Sections.length);
        this.Finalize();
    },
    CheckBox: function(Label, Checked) {
        return "<span class='checkbox'>" + (Checked ? "X" : "&#160;") + "</span>" + Label;
    },
    DetectOverflow: function(DomPointer) {
        if (DomPointer == undefined) DomPointer = $(".content:last");
        return DomPointer.prop("scrollHeight") > DomPointer.prop("offsetHeight");
    },
    Finalize: function() {
        if ($(".page").length == 0) this.NewPage();
        $(".content").each(function() {
            $(this).css("overflow-y", "hidden");
            if ($(this).find("table:last").length) $(this).find("table:last").css("border-bottom", "0 none");
        });
        $("#loading").remove();
    },
    GenerateContent: function(Section) {
        if (Section.Content.length == 1 && Section.Content[0].length == 1 && Section.Content[0][0].match(/<[^>]+>/)) return new Array(Section.Content[0][0]);
        else if (Section.Content.length == 1 && Section.Content[0].length == 1) return this.SplitTextArea(Section.Content[0][0]);
        else if (Section.Content.length) {
            var Table = $("<tbody/>");
            for (var i = 0; i < Section.Content.length; i++) {
                Table.append($("<tr/>"));
                for (var j = 0; j < Section.Content[i].length; j++)
                    Table.find("tr:last").append(
                        $("<td/>", { "class": "col" + j, "html": Section.Content[i][j] }));
            }
            return new Array(Table.html());
        } else return new Array();
    },
    GetDomPointer: function() {
        if ($(".content:last table:last td.dompointer").length) return $(".content:last table:last td.dompointer:last");
        else if ($(".content:last table:last tbody").length) return $(".content:last table:last tbody");
        else return $(".content:last");
    },
    Loading: function() {
        $("body").append($("<div/>", { "id": "loading" }));
    },
    NewPage: function() {
        $("body").append(
            $("<div/>", { "class": "page" }).append(
                $("<div/>", { "class": "content" })));
        $.each(this.Fields, function(Index, Value) {
            $(".page:last").append(
                $("<div/>", { "class": Index, "html": Value }));
        });
    },
    PageBreak: function(Content, DomPointer) {
        if (DomPointer == undefined) DomPointer = this.GetDomPointer();
        DomPointer.html(DomPointer.html().substring(0, (DomPointer.html().length - Content.length - 1)));
        if (DomPointer.text().length == 0) $(".content:last table:last").remove();
        this.NewPage();
        return Content;
    },
    PrepareContentSection: function(Section) {
        if (Section.Section != undefined && Section.Title != undefined)
            $(".content:last").append(
                $("<table/>").append(
                    $("<thead/>").append(
                        $("<tr/>").append(
                            $("<th/>", { "text": Section.Title }).attr("colspan", Section.Content[0].length)))));
        else {
            $(".content:last").append(
                $("<table/>").append(
                    $("<tbody/>")));
            if (Section.Title != undefined)
                $(".content:last table:last").prepend(
                    $("<thead/>").append(
                        $("<tr/>").append(
                            $("<th/>", { "text": Section.Title }).attr("colspan", Section.Content[0].length))));
            if (Section.Content.length == 1 && Section.Content[0].length == 1)
                $(".content:last tbody:last").append(
                    $("<tr/>").append(
                        $("<td/>", { "class": "dompointer" })));
        }
    },
    SplitTextArea: function(Text) {
        return Text.replace(/\<br \/\>/g, "<br/> ").split(" ");
    }
};