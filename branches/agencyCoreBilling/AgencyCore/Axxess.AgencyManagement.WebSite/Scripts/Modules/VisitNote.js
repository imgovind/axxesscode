﻿var V = {
    Init: function() {
        Template.OnChangeInit();
    },
    HandleSubmit: function(page, form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) {
            },
            success: function(result) {
                if (result.isSuccessful) {
                    if (control.html() == "Save") {
                        U.growl("The note has been saved.", "success");
                        UserInterface.Refresh();
                    } else if (control.html() == "Complete") {
                        UserInterface.CloseAndRefresh(page);
                        U.growl("The note has been saved and completed.", "success");
                    } else if (control.html() == "Approve") {
                        UserInterface.CloseAndRefresh(page);
                        U.growl("The note has been approved.", "success");
                    } else if (control.html() == "Return") {
                        UserInterface.CloseAndRefresh(page);
                        U.growl("The note has been returned.", "success");
                    }
                } else U.growl(result.errorMessage, "error");
            },
            error: function(result) {
                if (result.isSuccessful) {
                    if (control.html() == "Save") {
                        U.growl("The note has been saved.", "success");
                        UserInterface.Refresh();
                    } else if (control.html() == "Complete") {
                        UserInterface.CloseAndRefresh(page);
                        U.growl("The note has been saved and completed.", "success");
                    } else if (control.html() == "Approve") {
                        UserInterface.CloseAndRefresh(page);
                        U.growl("The note has been approved.", "success");
                    } else if (control.html() == "Return") {
                        UserInterface.CloseAndRefresh(page);
                        U.growl("The note has been returned.", "success");
                    }
                } else U.growl(result.errorMessage, "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    HandleWoundSubmit: function(page, form, control) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) {
                if ($.trim(result.responseText) == 'Success') U.growl("Wound care saved successfully.", "success");
                else U.growl($.trim(result.responseText), "error");
            },
            error: function(result) {
                if ($.trim(result.responseText) == 'Success') U.growl("Wound care saved successfully.", "success");
                else U.growl($.trim(result.responseText), "error");
            }
        };
        $(form).ajaxSubmit(options);
        return false;
    }
}
var snVisit = {
    Submit: function(control) {
        $('#SNVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snVisit", 'Schedule/SNVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var sixtyDaySummary = {
    Submit: function(control) {
        $('#SixtyDaySummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("sixtyDaySummary", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("sixtyDaySummary", 'Schedule/SixtyDaySummary', function() { V.Init(); Schedule.sixtyDaySummaryInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var dischargeSummary = {
    Submit: function(control) {
        $('#DischargeSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("dischargeSummary", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("dischargeSummary", 'Schedule/DischargeSummary', function() { V.Init(); Schedule.dischargeSummaryInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var lvnSupVisit = {
    Submit: function(control) {
        $('#LVNSVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("lvnsVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("lvnsVisit", 'Schedule/LVNSVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var transferSummary = {
    Submit: function(control) {
        $('#TransferSummary_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("transferSummary", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("transferSummary", 'Schedule/TransferSummary', function() { V.Init(); Schedule.transferSummaryInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var woundCare = {
    Submit: function(control) {
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleWoundSubmit("woundcare", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("woundcare", 'Schedule/WoundCare', function() { V.Init(); Schedule.WoundCareInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var hhaCarePlan = {
    Submit: function(control) {
        $('#HHACarePlan_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("hhaCarePlan", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("hhaCarePlan", 'Schedule/HHACarePlan', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var hhaSupVisit = {
    Submit: function(control) {
        $('#HHASVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("hhasVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("hhasVisit", 'Schedule/HHASVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var hhaVisit = {
    Submit: function(control) {
        $('#HHAideVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("hhAideVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("hhAideVisit", 'Schedule/HHAVisit', function() { V.Init(); Schedule.hhaVisitInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var pasCarePlan = {
    Submit: function(control) {
        $('#PASCarePlan_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("pasCarePlan", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("pasCarePlan", 'Schedule/PASCarePlan', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var pasVisit = {
    Submit: function(control) {
        $('#PASVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("pasVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("pasVisit", 'Schedule/PASVisitNote', function() { V.Init(); Schedule.pasVisitNoteInit(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var transportationLog = {
    Submit: function(control) {
        $('#DriverOrTransportationNote_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("transportationnote", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("transportationnote", 'Schedule/TransportationNote', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}
var snDiabeticDailyVisit = {
    Submit: function(control) {
        $('#SNDiabeticDailyVisit_Button').val(control.html());
        control.closest("form").validate();
        if (control.closest("form").valid()) V.HandleSubmit("snDiabeticDailyVisit", control.closest("form"), control);
        else U.ValidationError(control);
    },
    Load: function(episodeId, patientId, eventId) {
        Acore.Open("snDiabeticDailyVisit", 'Schedule/SNDiabeticDailyVisit', function() { V.Init(); }, { episodeId: episodeId, patientId: patientId, eventId: eventId });
    }
}