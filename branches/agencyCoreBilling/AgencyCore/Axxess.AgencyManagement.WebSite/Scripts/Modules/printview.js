﻿var printview = (function($) {
    return {
        firstheader: "",
        header: "",
        firstfooter: "",
        footer: "",
        cssclass: "",
        subsection: 0,
        lastcol: 0,
        addsection: function(content, title) {
            if ($('.page').length < 1) $('body').html(unescape("%3Cdiv class=%22page " + this.cssclass + "%22%3E%3Cdiv class=%22header%22%3E" + (this.firstheader ==
                "" ? this.header : this.firstheader) + "%3C/div%3E%3C/div%3E"));
            else $('.footer:last').remove();
            if (content == "485") content = "";
            else if (title == undefined) content = "%3Cdiv class=%22content%22%3E" + content + "%3C/div%3E";
            else content = "%3Ch3%3E" + title + "%3C/h3%3E%3Cdiv class=%22content%22%3E" + content + "%3C/div%3E";
            $('.page:last').append(unescape(content + "%3Cdiv class=%22footer%22%3E" + ($('.page').length == 1 && this.firstfooter != "" ? this.firstfooter :
                (this.footer)) + "%3C/div%3E"));
            if ($('.footer:last').html().length == 0) $('.footer:last').remove();
            if ($('.page:last').prop("scrollHeight") > $('.page:last').prop("offsetHeight")) {
                if (title != undefined) $('h3:last').remove();
                $('.content:last').remove();
                $('body').append(unescape("%3Cdiv class=%22page " + this.cssclass + "%22%3E%3Cdiv class=%22header%22%3E" + printview.header + "%3C/div%3E" + content
                    + "%3Cdiv class=%22footer%22%3E" + this.footer + "%3C/div%3E%3C/div%3E"));
            }
        },
        addsection485: function(text, delimiter, loc, title, defaultheight) {
            this.addsection("485");
            text = text.split(delimiter);
            loc485 = $("#loc" + loc);
            if (loc485.length) {
                while (text.length > 0 && loc485.prop("scrollHeight") <= loc485.prop("offsetHeight")) {
                    loc485.append(text[0] + delimiter);
                    if (loc485.prop("scrollHeight") <= loc485.prop("offsetHeight")) text.shift();
                }
                loc485.html(loc485.html().replace(new RegExp("(" + delimiter + text[0] + ")?" + delimiter + "$"), ""));
            }
            if (text.length || defaultheight) {
                this.addsection(
                    this.span(loc + ". " + title) +
                    this.span(text.join(delimiter), 0, defaultheight));
            }
        },
        addsubsection: function(content, title, col) {
            if (this.subsection == this.lastcol) {
                this.subsection = 0;
                this.addsection("%3Ctable class=%22fixed%22%3E%3Ctr%3E%3Ctd%3E" + (title != undefined ? "%3Ch3%3E" + title + "%3C/h3%3E" : "") + content + "%3C/t" +
                    "d%3E%3C/tr%3E%3C/table%3E");
            } else $("table.fixed:last tr").append(unescape("%3Ctd%3E" + (title != undefined ? "%3Ch3%3E" + title + "%3C/h3%3E" : "") + content + "%3C/td%3E"));
            this.subsection++;
            if (col != undefined && parseInt(col) != NaN) this.lastcol = col;
        },
        col: function(num, content, strong, height) {
            switch (num) {
                case 1: css = ""; break;
                case 2: css = "bicol "; break;
                case 3: css = "tricol "; break;
                case 4: css = "quadcol "; break;
                case 5: css = "pentcol "; break;
                case 6: css = "hexcol "; break;
                case 7: css = "septcol "; break;
                case 8: css = "octocol "; break;
                case 9: css = "novcol "; break;
            }
            if (content == "" && height > 0) {
                while (height > 0) {
                    content += "%3Cspan class=%22blank_line%22%3E%3C/span%3E";
                    height--;
                }
            }
            return "%3Cspan class=%22" + css + "%22%3E" + (strong ? "%3Cstrong%3E" : "") + content + (strong ? "%3C/strong%3E" : "") + "%3C/span%3E";
        },
        checkbox: function(label, checked, strong) {
            return "%3Cspan class=%22checklabel%22%3E%3Cspan class=%22checkbox%22%3E" + (checked ? "X" : "&#160;") + "%3C/span%3E" + (strong ? "%3Cstrong%3E" : "") + label + (strong ? "%3C/strong%3E" : "") + "%3C/span%3E";
        },
        span: function(content, strong, height) {
            if (content == undefined) content = "";
            return printview.col(1, content, strong, height);
        },
        setpagenumbers: function() {
            for (var i = 1; i <= $('.page').length; i++) {
                $('.pagenum', '.page:nth-child(' + i + ')').html("Page " + i + " of " + $('.page').length);
            }
        }
    };
})(jQuery);