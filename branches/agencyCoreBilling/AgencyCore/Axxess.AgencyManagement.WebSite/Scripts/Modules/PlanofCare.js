﻿var PlanOfCare = {
    InitNew: function() {
        $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis").IcdInput();
        $("#New_485_PhysicianId").PhysicianInput();
    },
    Submit: function(control, page) {
        if (control.closest("form").valid()) {
            var options = {
                dataType: 'json',
                beforeSubmit: function(values, form, options) { },
                success: function(result) {
                    if (result.isSuccessful) {
                        if (control.html() == "Save") U.growl("The Plan of Care (485) has been saved successfully.", "success");
                        else if (control.html() == "Save &amp; Close") {
                            UserInterface.CloseAndRefresh(page);
                            U.growl("The Plan of Care (485) has been saved successfully.", "success");
                        } else if (control.html() == "Complete") {
                            UserInterface.CloseAndRefresh(page);
                            U.growl("The Plan of Care (485) has been saved and completed successfully.", "success");
                        }
                    } else U.growl(result.errorMessage, "error");
                }
            };
            $(control.closest("form")).ajaxSubmit(options);
            return false;
        } else U.ValidationError(control);
    },
    InitEdit: function() {
        $(".diagnosis,.icd,.procedureICD,.procedureDiagnosis").IcdInput();
        $("#Edit_485_PhysicianId").PhysicianInput();
        U.InitEditTemplate("485");
    },
    SaveMedications: function(patientId) {
        U.postUrl("/Patient/LastestMedications", { patientId: patientId }, function(data) {
            if ($("#PlanofCareMedicationIsNew").val() == "true") $("#New_485_Medications").val(data);
            else $("#Edit_485_Medications").val(data);
            UserInterface.CloseModal();
        });
    }
}