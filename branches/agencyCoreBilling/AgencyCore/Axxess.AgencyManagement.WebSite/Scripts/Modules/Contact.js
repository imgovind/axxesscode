﻿var Contact = {
    Delete: function(Id) { U.DeleteTemplate("Contact", Id); },
    InitEdit: function() {
        U.showIfOtherSelected($("#Edit_Contact_Type"), $("#Edit_Contact_OtherContactType"));
        U.PhoneAutoTab("Edit_Contact_PhonePrimary");
        U.PhoneAutoTab("Edit_Contact_PhoneAlternate");
        U.PhoneAutoTab("Edit_Contact_Fax");
        U.InitEditTemplate("Contact");
    },
    InitNew: function() {
        U.showIfOtherSelected($("#New_Contact_Type"), $("#New_Contact_OtherContactType"));
        U.PhoneAutoTab("New_Contact_PhonePrimary");
        U.PhoneAutoTab("New_Contact_PhoneAlternate");
        U.PhoneAutoTab("New_Contact_Fax");
        U.InitNewTemplate("Contact");
    },
    RebindList: function() { U.rebindTGrid($('#List_Contact')); }
}