﻿var Billing = {
    _patientId: "",
    _ClaimId: "",
    _ClaimType: "",
    _patientRowIndex: 0,
    _sort: {},
    BillingHandler: function(index, form) {
        var options = {
            dataType: 'json',
            beforeSubmit: function(values, form, options) { },
            success: function(result) { if (result.isSuccessful) { var tabstrip = $("#FinalTabStrip").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); var $link = $(item).find('.t-link'); var contentUrl = $link.data('ContentUrl'); if (contentUrl != null) { Billing.LoadContent(contentUrl, $("#FinalTabStrip").find("div.t-content.t-state-active")); } } else U.growl(result.errorMessage, 'error'); },
            error: function() { }
        };
        $(form).ajaxSubmit(options);
        return false;
    },
    finalTabStripOnSelect: function(e) { Billing.LoadContent($(e.item).find('.t-link').data('ContentUrl'), e.contentElement); },
    finalTabStripOnLoad: function(e) { setTimeout(function() { Acore.OnWindowLoad($(e.target)) }, 500) },
    InitCenter: function() {
        $("#txtSearch_billing_Selection").keyup(function() { $('#billingHistoryClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#BillingHistoryActivityGrid').find('.t-grid-content tbody').empty(); Billing._patientId = ""; Billing.RebindPatientList(); });
        $("select.billingBranchCode").change(function() { $('#billingHistoryClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#BillingHistoryActivityGrid').find('.t-grid-content tbody').empty(); Billing._patientId = ""; Billing.RebindPatientList(); });
        $("select.billingStatusDropDown").change(function() { $('#billingHistoryClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#BillingHistoryActivityGrid').find('.t-grid-content tbody').empty(); Billing._patientId = ""; Billing.RebindPatientList(); });
        $("select.billingPaymentDropDown").change(function() { $('#billingHistoryClaimData').html('<p>No patients found matching your search criteria!</p>'); $('#BillingHistoryActivityGrid').find('.t-grid-content tbody').empty(); Billing._patientId = ""; Billing.RebindPatientList(); });
    },
    RebindPatientList: function() { var patientGrid = $('#BillingSelectionGrid').data('tGrid'); if (patientGrid != null) { patientGrid.rebind({ branchId: $("select.billingBranchCode").val(), statusId: $("select.billingStatusDropDown").val(), paymentSourceId: $("select.billingPaymentDropDown").val(), name: $("#txtSearch_billing_Selection").val() }); } },
    HideNewRapAndFinalMenu: function() { var gridTr = $('#BillingSelectionGrid').find('.t-grid-content tbody tr'); if (gridTr != undefined && gridTr != null) { if (gridTr.length > 0) { $("#billingHistoryTopMenu").show(); } else { $("#billingHistoryTopMenu").hide(); } } },
    InitRap: function() { U.InitTemplate($("#rapVerification"), function() { UserInterface.CloseWindow('rap'); Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); }, "Rap is successfully verified."); },
    InitFinal: function() { Acore.OnWindowLoad($(contentElement)); },
    ReLoadUpProcessedRap: function(branchId, insuranceId) { $("#Billing_CenterContentRap ol").addClass('loading'); $("#Billing_CenterContentRap").load('Billing/RapGrid', { branchId: branchId, insuranceId: insuranceId }, function(responseText, textStatus, XMLHttpRequest) { $("#Billing_CenterContentRap ol").removeClass("loading"); if (textStatus == 'error') $('#Billing_CenterContentRap').html(U.AjaxError); }); },
    ReLoadUpProcessedFinal: function(branchId, insuranceId) { $("#Billing_CenterContentFinal ol").addClass('loading'); $("#Billing_CenterContentFinal").load('Billing/FinalGrid', { branchId: branchId, insuranceId: insuranceId }, function(responseText, textStatus, XMLHttpRequest) { $("#Billing_CenterContentFinal ol").removeClass("loading"); if (textStatus == 'error') $('#Billing_CenterContentFinal').html(U.AjaxError); }); },
    LoadContent: function(contentUrl, contentElement) { if (contentUrl) $(contentElement).html("&#160;").addClass("loading").load(contentUrl, null, function() { $(contentElement).removeClass("loading"); Acore.OnWindowLoad($(contentElement)); }) },
    FinalTabLoad: function(e) { Acore.OnWindowLoad($(e.target)) },
    loadGenerate: function(control) { Acore.Open("claimSummary", 'Billing/ClaimSummary', function() { }, $(":input", $(control)).serializeArray()); },
    Navigate: function(index, id) {
        $(id).validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) { if (result.isSuccessful) { var tabstrip = $("#FinalTabStrip").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); var $link = $(item).find('.t-link'); var contentUrl = $link.data('ContentUrl'); if (contentUrl != null) { Billing.LoadContent(contentUrl, $("#FinalTabStrip").find("div.t-content.t-state-active")); }  if(index==1 || index==2){Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val());} } else U.growl(result.errorMessage, 'error'); },
                    error: function() { }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    NavigateBack: function(index) { var tabstrip = $("#FinalTabStrip").data("tTabStrip"); var item = $("li", tabstrip.element)[index]; tabstrip.select(item); },
    NoPatientBind: function(id) { Billing.RebindActivity(id); },
    PatientListDataBound: function() { if ($("#BillingSelectionGrid .t-grid-content tr").length) { if (Billing._patientId == "") $('#BillingSelectionGrid .t-grid-content tr').eq(0).click(); else $('td:contains(' + Billing._patientId + ')', $('#BillingSelectionGrid')).closest('tr').click(); $("#billingHistoryTopMenu").show(); } else { $("#billingHistoryClaimData").removeClass("loading").html("<p>No Patients found that fit your search criteria.</p>"); $("#billingHistoryTopMenu").hide(); } },
    OnPatientRowSelected: function(e) { if (e.row.cells[2] != undefined) { var patientId = e.row.cells[2].innerHTML; Billing._patientId = patientId; Billing._ClaimId = ""; Billing._ClaimType = ""; Billing.RebindActivity(patientId); } $("#billingHistoryClaimData").empty(); },
    RebindActivity: function(id) { var rebindActivity = $('#BillingHistoryActivityGrid').data('tGrid'); if (rebindActivity != null) { rebindActivity.rebind({ patientId: id, insuranceId: 0 }); } if ($('#BillingHistoryActivityGrid').find('.t-grid-content tbody').is(':empty')) { $("#billingHistoryClaimData").empty(); } },
    UpdateStatus: function(control) { U.postUrl('Billing/UpdateStatus', $(":input", $(control)).serializeArray(), function(result) { if (result.isSuccessful) { Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); UserInterface.CloseWindow('claimSummary'); $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 }); } else { $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 }); } }, null); },
    FinalComplete: function(id) { U.postUrl('Billing/FinalComplete', { id: id }, function(result) { if (result.isSuccessful) { Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); UserInterface.CloseWindow('final'); $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 }); } else { $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 }); } }, null); },
    GenerateAllCompleted: function(control) { $("input[name=RapSelected]", $(control)).each(function() { $(this).attr("checked", true) }); $("input[name=FinalSelected]", $(control)).each(function() { $(this).attr("checked", true) }); Billing.loadGenerate(control); },
    InitClaim: function() { U.InitTemplate($("#updateClaimForm"), function() { UserInterface.CloseModal(); Billing.RebindActivity(Billing._patientId); Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); }, "Claim update is successfully."); },
    InitNewClaim: function() { U.InitTemplate($("#createClaimForm"), function() { UserInterface.CloseModal(); Billing.RebindActivity(Billing._patientId); Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); }, "Claim is created successfully."); },
    InitPendingClaimUpdate: function() { U.InitTemplate($("#updatePendingForm"), function() { Billing.loadPendingClaimRap(); Billing.loadPendingClaimFinal(); Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); }, "Claim update is successfully."); },
    DeleteClaim: function(patientId, id, type) {
        if (confirm("Are you sure you want to delete this claim?")) {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Billing/DeleteClaim",
                data: "patientId=" + patientId + "&id=" + id + "&type=" + type,
                success: function(result) { var resultObject = eval(result); if (resultObject.isSuccessful) { Billing.RebindActivity(patientId); Billing.ReLoadUpProcessedRap($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); Billing.ReLoadUpProcessedFinal($('#Billing_CenterBranchCode').val(), $('#Billing_CenterPrimaryInsurance').val()); U.growl(result.errorMessage, "success"); } else U.growl(result.errorMessage, "error"); }
            });
        }
    },
    OnClaimDataBound: function() {
        if ($("#BillingHistoryActivityGrid .t-grid-content tr").length) {
            // if (Billing._ClaimId == "") {
            $('#BillingHistoryActivityGrid .t-grid-content tbody tr.t-master-row').eq(0).click();
            //            }
            //            else {
            //                $('td:contains(' + Billing._ClaimId + ')', $('#BillingHistoryActivityGrid')).closest('tr.t-master-row').click();
            //            }
        }
        else { $("#billingHistoryClaimData").empty(); }
    },
    OnClaimRowSelected: function(e) { if (e.row.cells[1] != undefined && e.row.cells[9] != undefined && Billing._patientId != undefined && Billing._patientId != "") { var claimType = e.row.cells[1].innerHTML; var claimId = e.row.cells[9].innerHTML; Billing._ClaimId = claimId; Billing._ClaimType = claimType; Billing.loadClaimInfo(Billing._patientId, claimId, claimType); } },
    loadClaimInfo: function(patientId, claimId, claimType) { $("#billingHistoryClaimData").empty().addClass("loading").load('Billing/ClaimInfo', { patientId: patientId, claimId: claimId, claimType: claimType }, function(responseText, textStatus, XMLHttpRequest) { $("#billingHistoryClaimData").removeClass("loading"); if (textStatus == 'error') $('#billingHistoryClaimData').html(U.AjaxError); }); },
    loadPendingClaimRap: function() { $("#pendingClaim_rapContent").empty().addClass("loading").load('Billing/PendingClaimRap', null, function(responseText, textStatus, XMLHttpRequest) { $("#pendingClaim_rapContent").removeClass("loading"); if (textStatus == 'error') $('#pendingClaim_rapContent').html(U.AjaxError); }); },
    loadPendingClaimFinal: function() { $("#pendingClaim_finalContent").empty().addClass("loading").load('Billing/PendingClaimFinal', null, function(responseText, textStatus, XMLHttpRequest) { $("#pendingClaim_finalContent").removeClass("loading"); if (textStatus == 'error') $('#pendingClaim_finalContent').html(U.AjaxError); }); },
    OpenUBOFour: function(patientId, claimId, claimType) { U.GetAttachment("Billing/UB04Pdf", { patientId: patientId, Id: claimId, type: claimType }); },
    Sort: function(type, sortBy) {
        $('#Billing_CenterContent' + type + ' ol').addClass('loading');
        if (Billing._sort[type] == sortBy) { var invert = true; Billing._sort[type] = ""; }
        else { var invert = false; Billing._sort[type] = sortBy; }
        setTimeout(function() {
            var items = new Array();
            if (sortBy == "rapeps") $('#Billing_CenterContent' + type + ' ol .' + sortBy + ' .very_hidden').each(function() { items.push($(this).text()) });
            else $('#Billing_CenterContent' + type + ' ol span.' + sortBy).each(function() { items.push($(this).text()) });
            items = items.sort();
            if (invert) items.reverse();
            for (var i = 0; i < items.length; i++) $('#Billing_CenterContent' + type + ' ol').append($("#Billing_CenterContent" + type + " ol ." + sortBy + ":contains('" + items[i] + "')").closest('li'));
            for (var i = 0; i < items.length; i++) $('#Billing_CenterContent' + type + ' ol li:eq(' + i + ') .rapcheck').html((i + 1) + "." + $('#Billing_CenterContent' + type + ' ol li:eq(' + i + ') .rapcheck').html().split(".")[1]);
            $('#Billing_CenterContent' + type + ' ol li:nth-child(even)').removeClass('odd').addClass('even');
            $('#Billing_CenterContent' + type + ' ol li:nth-child(odd)').removeClass('even').addClass('odd');
            $('#Billing_CenterContent' + type + ' ol').removeClass('loading');
        }, 100);
    },
    SubmitClaimDirectly: function(control) { U.postUrl('Billing/SubmitClaimDirectly', $(":input", $(control)).serializeArray(), function(result) { if (result.isSuccessful) { UserInterface.CloseWindow('claimSummary'); $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 }); } else { $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 }); } }, null); },
    LoadGeneratedClaim: function(control) { U.postUrl("/Billing/CreateANSI", $(":input", $(control)).serializeArray(), function(result) { if (result.isSuccessful) { jQuery('<form action="/Billing/Generate" method="post"><input type="hidden" name="ansiId" value="' + result.Id + '"></input></form>').appendTo('body').submit().remove(); } else { $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 }); } }, function(result) { }); },
    onRapDetailViewCollapse: function(e) { if (e.detailRow != undefined) { $(e.detailRow).remove(); } },
    onRapDetailViewExpand: function(e) { $("#Raps table tbody tr.t-detail-row").not(e.detailRow).remove(); $("#Raps table tbody tr.t-master-row").not(e.masterRow).find("td.t-hierarchy-cell a.t-icon").removeClass('t-minus').addClass("t-plus"); },
    onEditRap: function(e) { if (e.dataItem != undefined) { $(e.form).find('.Status').data('tDropDownList').select(function(dataItem) { return dataItem.Value == e.dataItem['Status']; }); } },
    onFinalDetailViewCollapse: function(e) { if (e.detailRow != undefined) { $(e.detailRow).remove(); } },
    onFinalDetailViewExpand: function(e) { $("#Finals table tbody tr.t-detail-row").not(e.detailRow).remove(); $("#Finals table tbody tr.t-master-row").not(e.masterRow).find("td.t-hierarchy-cell a.t-icon").removeClass('t-minus').addClass("t-plus"); },
    onEditFinal: function(e) { if (e.dataItem != undefined) { $(e.form).find('.Status').data('tDropDownList').select(function(dataItem) { return dataItem.Value == e.dataItem['Status']; }); } },
    onEditRapSnapShot: function(e) { if (e.dataItem != undefined) { $(e.form).find('.Status').data('tDropDownList').select(function(dataItem) { return dataItem.Value == e.dataItem['Status']; }); } },
    onEditFinalSnapShot: function(e) { if (e.dataItem != undefined) { $(e.form).find('.Status').data('tDropDownList').select(function(dataItem) { return dataItem.Value == e.dataItem['Status']; }); } },
    onEditSnapShotClaim: function(e) { if (e.dataItem != undefined) { $(e.form).find('.Status').data('tDropDownList').select(function(dataItem) { return dataItem.Value == e.dataItem['Status']; }); } },
    onClaimDetailViewCollapse: function(e) { if (e.detailRow != undefined) { $(e.detailRow).remove(); } },
    onClaimDetailViewExpand: function(e) { $("#BillingHistoryActivityGrid table tbody tr.t-detail-row").not(e.detailRow).remove(); $("#BillingHistoryActivityGrid table tbody tr.t-master-row").not(e.masterRow).find("td.t-hierarchy-cell a.t-icon").removeClass('t-minus').addClass("t-plus"); },
    expandFirstRow: function(grid, row) { if (grid.$rows().index(row) == 0) { grid.expandRow(row); } },
    onRapOnRowDataBound: function(e) { var grid = $("#Raps").data('tGrid'); Billing.expandFirstRow(grid, e.row); },
    ReLoadPendingClaimRap: function(branchId, insuranceId) { var grid = $('#Raps').data('tGrid'); if (grid != null) { grid.rebind({ branchId: branchId, insuranceId: insuranceId }); } },
    ReLoadPendingClaimFinal: function(branchId, insuranceId) { var grid = $('#Finals').data('tGrid'); if (grid != null) { grid.rebind({ branchId: branchId, insuranceId: insuranceId }); } },
    InitRemittance: function() {
        $("#externalRemittanceUploadForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) { },
                    success: function(result) { if ($.trim(result.responseText) == 'Success') { $.jGrowl("The upload is successful.", { theme: 'success', life: 5000 }); $("#Billing_ExternalRemittanceUpload").val(''); } else $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 }); },
                    error: function(result) { if ($.trim(result.responseText) == 'Success') { $.jGrowl("The upload is successful.", { theme: 'success', life: 5000 }); $("#Billing_ExternalRemittanceUpload").val(''); } else { $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 }); } }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    RebindSubmittedBatchClaims: function() {
        var grid = $('#List_SubmittedClaims').data('tGrid');
        if (grid != null) { grid.rebind({ ClaimType: $("#SubmittedClaims_ClaimType").val(), StartDate: $("#SubmittedClaims_StartDate").val(), EndDate: $("#SubmittedClaims_EndDate").val() }); }
        var $exportLink = $('#SubmittedClaims_ExportLink');
        var href = $exportLink.attr('href');
        href = href.replace(/ClaimType=([^&]*)/, 'ClaimType=' + $("#SubmittedClaims_ClaimType").val());
        href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#SubmittedClaims_StartDate").val());
        href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#SubmittedClaims_EndDate").val());
        $exportLink.attr('href', href);
    },
    ReloadEligibilityList: function(patientId) { $("#MedicareEligiblity_ReportContent ol").addClass('loading'); $("#MedicareEligiblity_ReportContent").load('Billing/EligibilityList', { patientId: patientId }, function(responseText, textStatus, XMLHttpRequest) { $("#MedicareEligiblity_ReportContent ol").removeClass("loading"); if (textStatus == 'error') $('#MedicareEligiblity_ReportContent').html(U.AjaxError); }); },
    SubmittedClaimDetail: function(Id) { Acore.Open("submittedclaimdetail", 'Billing/SubmittedClaimDetail', function() { }, { Id: Id }); },
    RemittanceContentReload: function() {
        $("#BillingRemittance_Content").load("/Billing/RemittanceContent", { StartDate: $("#BillingRemittance_StartDate").val(), EndDate: $("#BillingRemittance_EndDate").val() }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') { } else if (textStatus == "success") {
                var $exportLink = $('#BillingRemittance_PrintLink');
                var href = $exportLink.attr('href');
                href = href.replace(/StartDate=([^&]*)/, 'StartDate=' + $("#BillingRemittance_StartDate").val());
                href = href.replace(/EndDate=([^&]*)/, 'EndDate=' + $("#BillingRemittance_EndDate").val());
                $exportLink.attr('href', href);
            }
        });
    },
    RemittanceDelete: function(id) { if (confirm("Are you sure you want to delete this remittance?")) { U.postUrl("/Billing/DeleteRemittance", { Id: id }, function(result) { if (result.isSuccessful) { $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 }); Billing.RemittanceContentReload(); } else { $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });} }); } }
}