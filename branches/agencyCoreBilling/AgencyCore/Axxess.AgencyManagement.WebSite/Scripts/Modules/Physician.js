﻿var Physician = {
    Delete: function(Id) { U.DeleteTemplate("Physician", Id); },
    InitEdit: function() {
        U.PhoneAutoTab("Edit_Physician_Phone");
        U.PhoneAutoTab("Edit_Physician_AltPhone");
        U.PhoneAutoTab("Edit_Physician_Fax");
        U.InitEditTemplate("Physician", function() { Lookup.loadPhysicians(); Physician.RebindList(); UserInterface.CloseModal(); });
        $("#editPhysicainLicenseForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        if (result.isSuccessful) {
                            $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                            var grid = $('#List_Physician_Licenses').data('tGrid');
                            if (grid != null) { grid.rebind({ physicianId: $("#Edit_PhysicainLicense_Id").val() }); }
                            $("#editPhysicainLicenseForm").clearForm();
                        } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
    },
    InitNew: function(isModal) {
        var prefix = "New_Physician_" + (isModal ? "Modal_" : "");
        if (isModal) {
            $("#window_ModalWindow_content label").each(function() {
                var forValue = $(this).attr("for");
                forValue = forValue ? forValue.replace("New_Physician_", prefix) : "";
                $(this).attr("for", forValue);
            });
            $("#window_ModalWindow_content :input").each(function() {
                var idValue = $(this).attr("id");
                idValue = idValue ? idValue.replace("New_Physician_", prefix) : "";
                $(this).attr("id", idValue);
            });
            $("#New_Physician_PecosCheck").attr("id", prefix + "PecosCheck");
        }
        U.PhoneAutoTab(prefix + "Phone");
        U.PhoneAutoTab(prefix + "AltPhone");
        U.PhoneAutoTab(prefix + "Fax");
        $("#" + prefix + "NpiNumber").blur(function() {
            var npiNumber = $("#" + prefix + "NpiNumber").val();
            if (npiNumber.length > 0) {
                U.postUrl("Physician/CheckPecos", "npi=" + npiNumber, function(data) {
                    if (data.isSuccessful) $("#" + prefix + "PecosCheck").html("<span class=\"img icon success_small\"></span>")
                    else $("#" + prefix + "PecosCheck").html("<span class=\"img icon error_small\"></span>")
                })
            }
            else $(prefix + "PecosCheck").hide();
        });
        $("#" + prefix + "NpiSearch").AjaxAutocomplete({
            minLength: 1,
            SourceUrl: "LookUp/Npis",
            Format: function(jsonResult) {
                return jsonResult.Id + " &#8211 " + jsonResult.ProviderFirstName + "  " + jsonResult.ProviderLastName
            },
            Select: function(physicianJson, input) {
                input.val(physicianJson.Id);
                if (id = physicianJson.Id) $("#" + prefix + "NpiNumber").val(id).blur();
                if (fname = physicianJson.ProviderFirstName) $("#" + prefix + "FirstName").val(U.toTitleCase(fname));
                if (mname = physicianJson.ProviderMiddleName) $("#" + prefix + "MiddleIntial").val(mname.substring(0, 1).toUpperCase());
                if (lname = physicianJson.ProviderLastName) $("#" + prefix + "LastName").val(U.toTitleCase(lname));
                if (cred = physicianJson.ProviderCredentialText) $("#" + prefix + "Credentials").val(cred);
                if (add1 = physicianJson.ProviderFirstLineBusinessPracticeLocationAddress) $("#" + prefix + "AddressLine1").val(U.toTitleCase(add1));
                if (add2 = physicianJson.ProviderSecondLineBusinessPracticeLocationAddress) $("#" + prefix + "AddressLine2").val(U.toTitleCase(add2));
                if (city = physicianJson.ProviderBusinessPracticeLocationAddressCityName) $("#" + prefix + "AddressCity").val(U.toTitleCase(city));
                if (state = physicianJson.ProviderBusinessPracticeLocationAddressStateName) $("#" + prefix + "AddressStateCode").val(state);
                if (zip = physicianJson.ProviderBusinessPracticeLocationAddressPostalCode) $("#" + prefix + "AddressZipCode").val(zip.substring(0, 5));
                if (phone = physicianJson.ProviderBusinessPracticeLocationAddressTelephoneNumber) {
                    $("#" + prefix + "Phone1").val(phone.substring(0, 3));
                    $("#" + prefix + "Phone2").val(phone.substring(3, 6));
                    $("#" + prefix + "Phone3").val(phone.substring(6, 10))
                }
                if (fax = physicianJson.ProviderBusinessPracticeLocationAddressFaxNumber) {
                    $("#" + prefix + "Fax1").val(fax.substring(0, 3));
                    $("#" + prefix + "Fax2").val(fax.substring(3, 6));
                    $("#" + prefix + "Fax3").val(fax.substring(6, 10))
                }
            }
        });
        U.InitNewTemplate("Physician", function() {
            Lookup.loadPhysicians();
            Physician.RebindList();
            UserInterface.CloseModal()
        })
    },
    RebindList: function() { U.rebindTGrid($('#List_Physician')); }
};