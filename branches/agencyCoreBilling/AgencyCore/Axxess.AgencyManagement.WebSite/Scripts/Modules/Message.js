﻿var Message = {
    _ckeloaded: false,
    _cki: 0,
    _tokenlist: null,
    defaultMessage: "",
    AddRemoveRecipient: function(input) {
        if ($('#' + input).attr('checked')) $.data($('#' + input).get(0), "tokenbox", { "token": Message._tokenlist.insertToken($('#' + input).attr('value'), $('#' + input).attr('title')) });
        else Message._tokenlist.removeToken($.data($('#' + input).get(0), "tokenbox").token);
        Message.positionBottom();
    },
    Cancel: function() { UserInterface.CloseWindow('newmessage'); },
    Delete: function(id, type, bypass) { U.Delete("Message", "Message/Delete", { Id: id, Type: type }, function() { Message.Rebind(); }, bypass); },
    Init: function(messageId, messageType) {
        if (messageId != undefined) Message.defaultMessage = messageId;
        $("#MessageReplyButton").click(function() {
            Acore.Open("newmessage", 'Message/New', function() { Message.InitNew('reply', $("#messageId").val()); });
        });
        $("#MessageForwardButton").click(function() {
            Acore.Open("newmessage", 'Message/New', function() { Message.InitNew('forward', $("#messageId").val()); });
        });
        $("#MessageDeleteButton").click(function() { Message.Delete($("#messageId").val(), $("#messageType").val()); });
        $("#inboxType").change(function() {
            Message.Rebind();
            $("#List_Messages .t-grid-header .t-header").eq(0).find("a").text($("#inboxType").find(":selected").text());
        });
    },
    initCKE: function() {
        CKEDITOR.replace('New_Message_Body', {
            skin: 'office2003',
            resize_enabled: false,
            height: '100%',
            removePlugins: 'elementspath',
            toolbarCanCollapse: false,
            toolbar: [['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
                'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Font', 'FontSize', 'TextColor', '-', 'SpellChecker', 'Scayt']]
        });
    },
    InitNew: function(type, id) {
        $('#New_Message_CheckallRecipients').change(function() {
            $('input[name="Recipients"]').each(function() {
                if ($('#New_Message_CheckallRecipients').attr('checked') != $(this).attr('checked')) {
                    $(this).attr('checked', $('#New_Message_CheckallRecipients').attr('checked'));
                    Message.AddRemoveRecipient($(this).attr('id'));
                }
            });
        });
        $("#newMessageBodyDiv").html(unescape("%3Cdiv%3E%3Ctextarea id=%22New_Message_Body%22 name=%22Body%22%3E%3C/textarea%3E%3C/div%3E"));

        $("#newMessageForm").validate({
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    beforeSubmit: function(values, form, options) {
                        if ($('#newMessageForm .token-input-token-facebook').length == 0) U.growl("Please Select a Recipient.", "error");
                        $("textarea[name=Body]").val(CKEDITOR.instances['New_Message_Body'].getData());
                    },
                    success: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            UserInterface.CloseWindow("newmessage");
                            $.jGrowl("Message has been sent successfully.", { theme: 'success', life: 5000 });
                        } else $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                    },
                    error: function(result) {
                        if ($.trim(result.responseText) == 'Success') {
                            UserInterface.CloseWindow("newmessage");
                            $.jGrowl("Message has been sent successfully.", { theme: 'success', life: 5000 });
                        } else $.jGrowl($.trim(result.responseText), { theme: 'error', life: 5000 });
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });
        Message._tokenlist = $.fn.tokenInput("#New_Message_Recipents", "/Message/Recipients", {
            classes: { tokenList: "token-input-list-facebook", token: "token-input-token-facebook", tokenDelete: "token-input-delete-token-facebook",
                selectedToken: "token-input-selected-token-facebook", highlightedToken: "token-input-highlighted-token-facebook",
                dropdown: "token-input-dropdown-facebook", dropdownItem: "token-input-dropdown-item-facebook", dropdownItem2: "token-input-dropdown-item2-facebook",
                selectedDropdownItem: "token-input-selected-dropdown-item-facebook", inputToken: "token-input-input-token-facebook"
            }
        });
        $("#messageTypeHeader").html("New Message");
        if (type == "reply" || type == "forward") {
            U.postUrl("/Message/Get", "id=" + id, function(message) {
                if (type == "reply") {
                    $("#messageTypeHeader").html("Reply Message");
                    if (message.Subject.substring(0, 3) != "RE:") message.Subject = "RE: " + message.Subject;
                    if ($('input[name="Recipients"][value=' + message.FromId + ']').length)
                        Message.AddRemoveRecipient($('input[name="Recipients"][value=' + message.FromId + ']').attr('checked', true).attr('id'));
                } else {
                    $("#messageTypeHeader").html("Forward Message");
                    if (message.Subject.substring(0, 3) != "FW:") message.Subject = "FW: " + message.Subject;
                }
                $("#New_Message_Subject").val(message.Subject);
                $("#New_Message_PatientId").val(message.PatientId);
                $("#New_Message_Body").val(message.ReplyForwardBody);
                if (!this._ckeloaded && !Acore.Mobile) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() { Message._ckeloaded = true; Message.initCKE() });
                else if (!Acore.Mobile) Message.initCKE();
            });
        } else {
            if (!this._ckeloaded && !Acore.Mobile) $.getScript("/Scripts/Plugins/ckeditor/ckeditor.js", function() { Message._ckeloaded = true; Message.initCKE() });
            else if (!Acore.Mobile) Message.initCKE();
        }
    },
    initWidget: function() {
        U.postUrl("/Message/WidgetList", "", function(data) {
            if (data != undefined && data.Data != undefined && data.Data.length > 0) {
                for (var i = 0; i < data.Data.length; i++) $('#messagesWidgetContent').append(unescape("%3Cdiv" + (data.Data[i].MarkAsRead ? "" : " clas" +
                    "s=%22strong%22") + "%3E%3Cinput type=%22checkbox%22 class=%22radio float_left%22 messagetype=%22" + data.Data[i].Type + "%22 value=%22" + data.Data[i].Id + "%22 /%3E%3Ca href=%22javasc" +
                    "ript:void(0);%22 onclick=%22$(this).parent().removeClass('strong'); if($('#window_messageinbox').length == 0) { Acore.Open('messageinbox', '" +
                    "Message/Inbox', function() { Message.Init('" + data.Data[i].Id + "','" + data.Data[i].Type + "'); }); } else { $('#window_messageinbox').WinFocus(); $('#" + data.Data[i].Id +
                    "').closest('tr').click(); }%22 onmouseover=%22$(this).addClass('t-state-hover');%22 onmouseout=%22$(this).removeClass('t-state-hover');%22 c" +
                    "lass=%22message%22%3E" + data.Data[i].FromName + " &#8211; " + data.Data[i].Subject + "%3C/a%3E%3C/div%3E"));
            } else $('#messagesWidgetContent').append(unescape("%3Ch1 class=%22align_center%22%3ENo Messages found.%3C/h1%3E"))
                .parent().find('.widget-more').remove();
        });
        $(".mailcontrols select").bind("change", function() {
            if ($(this).find("option:first").is(":not(:selected)")) {
                if ($("#messagesWidgetContent input:checked").length) {
                    if ($(this).find(":selected").html() == "Archive") { U.growl("This feature coming soon."); }
                    if ($(this).find(":selected").html() == "Delete") {
                        if (confirm("Are you sure you want to delete all selected messages?")) {
                            $("#messagesWidgetContent input:checked").each(function() {
                                Message.Delete($(this).val(), $(this).attr("messagetype"), true);
                                $(this).parent().remove();
                            });
                        }
                    }
                    if ($(this).find(":selected").html() == "Mark as Read") { U.growl("This feature coming soon."); }
                    if ($(this).find(":selected").html() == "Mark as Unread") { U.growl("This feature coming soon."); }
                } else {
                    $.jGrowl("Error: No messages selected to " + $(this).find(":selected").html(), { theme: 'error', life: 5000 });
                    $(this).find("option:first").attr("selected", true);
                }
            }
        });
    },
    OnDataBound: function(e) {
        if (Message.defaultMessage != "" && $("#" + Message.defaultMessage).length) {
            $("#" + Message.defaultMessage).closest("tr").click();
        } else {
            $("#List_Messages .t-grid-content tr").eq(0).click();
            if ($("#List_Messages .t-grid-content tr").length == 0) {
                $("#messageInfoResult").html(
                    $("<div/>", { "class": "ajaxerror" }).append(
                        $("<h1/>", { "text": "No Messages found." })).append(
                        $("<div/>", { "class": "heading" }).Buttons([{
                            Text: "Add New Message",
                            Click: function() {
                                UserInterface.ShowNewMessage()
                            }
                        }])
                    )
                );
            }
        }
        Message.defaultMessage = "";
    },
    OnRowSelected: function(e) {
        if (e.row.cells[1] != undefined) {
            var messageId = e.row.cells[1].innerHTML;
            var messageType = e.row.cells[2].innerHTML;
            $('#messageInfoResult').empty().addClass('loading').load('Message/Data', { id: messageId, type: messageType }, function(responseText, textStatus, XMLHttpRequest) {
                Message.Init(messageId, messageType);
                $('#messageInfoResult').removeClass('loading');
                if (textStatus == 'error') $('#messageInfoResult').html(U.AjaxError);
            });
            $("#" + messageId).removeClass('false').addClass('true');
        }
    },
    positionBottom: function() { $('#newMessageBodyDiv').css({ top: (parseInt($('.token-input-list-facebook').attr('clientHeight') + 100).toString() + "px") }); },
    Rebind: function() { U.rebindTGrid($('#List_Messages'), { inboxType: $("#inboxType").val() }); }
}