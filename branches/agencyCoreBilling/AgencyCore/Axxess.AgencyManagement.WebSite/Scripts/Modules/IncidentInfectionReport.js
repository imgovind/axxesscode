﻿var IncidentReport = {
    Delete: function(Id) { U.DeleteTemplate("IncidentReport", Id); },
    InitEdit: function() {
        U.InitEditTemplate("IncidentReport", function() {
            Patient.Rebind();
            Schedule.Rebind();
        });
        $("#window_editincidentreport .Physicians").PhysicianInput();
    },
    InitNew: function() {
        U.InitNewTemplate("IncidentReport", function() {
            Patient.Rebind();
            Schedule.Rebind();

        });
        U.showIfOtherChecked($("#New_Incident_IndividualInvolved4"), $("#New_Incident_IndividualInvolvedOther"));
        $("#window_newincidentreport .Physicians").PhysicianInput();
    },
    RebindList: function() { U.rebindTGrid($('#List_IncidentReport')); },
    ProcessIncident: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#printreturnreason").is(":hidden")) {
                $("#printcontrols li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very_hidden");
                $("#printreturnreason").slideDown('slow');
            } else {
                reason = $("#printreturnreason textarea").val();
                U.postUrl("/Agency/ProcessIncident", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                    } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                });
            }
        } else {
            U.postUrl("/Agency/ProcessIncident", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    }

}

var InfectionReport = {
    Delete: function(Id) { U.DeleteTemplate("InfectionReport", Id); },
    InitEdit: function() {
        U.InitEditTemplate("InfectionReport", function() {
            Patient.Rebind();
            Schedule.Rebind();
        });
        $("#window_editinfectionreport .Physicians").PhysicianInput();
    },
    InitNew: function() {
        U.InitNewTemplate("InfectionReport", function() {
            Patient.Rebind();
            Schedule.Rebind();
        });
        U.showIfOtherChecked($("#New_Infection_InfectionType6"), $("#New_Infection_InfectionTypeOther"));
        $("#window_newinfectionreport .Physicians").PhysicianInput();
    },
    RebindList: function() { U.rebindTGrid($('#List_InfectionReport')); },
    ProcessInfection: function(button, patientId, eventId) {
        var reason = "";
        if (button == "Return") {
            if ($("#printreturnreason").is(":hidden")) {
                $("#printcontrols li a").each(function() {
                    if ($(this).attr("id") != "printreturn" && $(this).attr("id") != "printreturncancel") $(this).hide();
                });
                $("#printreturncancel").parent().removeClass("very_hidden");
                $("#printreturnreason").slideDown('slow');
            } else {
                reason = $("#printreturnreason textarea").val();
                U.postUrl("/Agency/ProcessInfection", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                    if (result.isSuccessful) {
                        UserInterface.CloseModal();
                        Agency.RebindCaseManagement();
                        Patient.Rebind();
                        Schedule.Rebind();
                        User.RebindScheduleList();
                        $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                    } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
                });
            }
        } else {
            U.postUrl("/Agency/ProcessInfection", { button: button, patientId: patientId, eventId: eventId, reason: reason }, function(result) {
                if (result.isSuccessful) {
                    UserInterface.CloseModal();
                    Agency.RebindCaseManagement();
                    Patient.Rebind();
                    Schedule.Rebind();
                    User.RebindScheduleList();
                    $.jGrowl(result.errorMessage, { theme: 'success', life: 5000 });
                } else $.jGrowl(result.errorMessage, { theme: 'error', life: 5000 });
            });
        }
    }
}