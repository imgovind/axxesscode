﻿var Acore = {
    AutocompleteId: 0,                  // Autocomplete iterator
    CascadeGap: 25,                     // Pixel gap for window cascading
    CssTop: 0,                          // CSS top iterator, used for window cascading
    CssLeft: 0,                         // CSS left iterator, used for window cascading
    GetRemoteContent: true,             // Enable remote content loading
    IdleTime: 0,                        // System idle timer
    MaxWindows: 8,                      // Maximum allowed number of concurrent open windows
    MinWinWidth: 1000,                  // Minimum width of any window (may be overloaded per window)
    MinWinHeight: 555,                  // Minimum height of any window (may be overloaded per window)
    Mobile: false,                      // Mobile site switch
    OpenWindows: 0,                     // Open window counter
    PrintId: 0,                         // Print lightbox iterator
    Timeout: 6,                         // Auto-logout to occur at this cycle number
    TimeoutCount: 1,                    // Number of cycles to display auto-logout warning
    TimeoutInterval: (5 * 60 * 1000),   // Auto-logout interval (cycle length in milliseconds)
    Windows: {},                        // Registry of available windows
    PingTimer: null,                    // Timer for keep-alive pings
    TimeoutTimer: null,                 // Timer for idle timeout counting
    AddMenu: function(Options) {
        //  Options:
        //      Name    Display name for the menu
        //      Id      DOM identifier for the menu
        //      Parent  DOM identifier for the parent menu
        //      IconX   Pixel position in sprite for menu icon X axis
        //      IconY   Pixel position in sprite for menu icon Y axis
        if (Options.Name != null && (Options.Parent == null || $("#" + Options.Parent).length)) {
            // Create basic menu option
            var menu =
                $("<li/>").append(
                    $("<a/>", { "href": "javascript:void(0);", "class": "menu_trigger", "text": Options.Name })).append(
                    $("<ul/>", { "id": Options.Id == undefined ? Options.Name.replace(/[^0-9a-zA-Z]/, "") : Options.Id, "class": "menu" })
                );
            // If icon x position in sprite defined, add icon (default y is 86)
            if (Options.IconX != null)
                $("a", menu).prepend(
                    $("<span/>", { "class": "img icon", "style": "background-position:-" + (parseInt(Options.IconX) > 0 ? parseInt(Options.IconX) : 0) + "px -" + (parseInt(Options.IconY) > 0 ? parseInt(Options.IconY) : "86") + "px;" })
                );
            // If it is located in a submenu, add the more icon
            if (Options.Parent != null && Options.Parent != "mainmenu")
                $("a", menu).append(
                    $("<span/>", { "class": "img icon more" })
                );
            // Add menu into main or submenu depending upon the Parent option
            $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(menu);
        }
    },
    AddMenuItem: function(Options) {
        //  Options:
        //      Id      Window identifier
        //      Name    Name for menu item
        //      Href    URL for menu link
        //      Parent  DOM identifier for parent menu
        // If external link
        if (Options.Name != null && Options.Href != null) {
            $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(
                $("<li/>").append(
                    $("<a/>", { "href": Options.Href, "target": "_blank", "text": Options.Name })
                )
            );
        }
        // If internal window load
        if (Options.Id != null && this.Windows[Options.Id] != null) {
            $(Options.Parent == null ? "#mainmenu" : "#" + Options.Parent).append(
                $("<li/>").click(function() {
                    Acore.Open(Options.Id, Acore.Windows[Options.Id].Url, Acore.Windows[Options.Id].OnLoad)
                }).append(
                    $("<a/>", { "href": "javascript:void(0);", "text": this.Windows[Options.Id].MenuName })
                )
            );
        }
    },
    AddWindow: function(Options) {
        //  Options:
        //      Name        Master window name
        //      WindowName  Name as seen on the top of the window
        //      MenuName    Name as seen on the top navigation bar menus
        //      TaskName    Name as seen on the bottom task bar
        //      Status      Default text seen on the status bar of the window
        //      Id          Identifier for window in acore's window registry
        //      Menu        DOM identifier for parent menu (leave null if not on menu, pass array if in multiple menus)
        //      Url         AJAX URL for window's content
        //      OnLoad      Window onLoad/Callback event
        //      WindowFrame Bool switch to display window with or without the standard frame
        //      StatusBar   Bool switch to display window with or without the status bar
        //      Width       Overloaded window default width
        //      Height      Overloaded window default height
        //      Resize      Bool switch to allow or disallow window resizing
        //      Center      Bool switch to display window centered as opposed to cascading
        //      Modal       Bool switch to shade out background
        //      IconX       Pixel position in sprite for Window/Taskbar icon X axis
        //      IconY       Pixel position in sprite for Window/Taskbar icon Y axis
        // Ensure Id and Name are set and that the Id isn't already used
        if (Options.Id != null && Options.Name != null && this.Windows[Options.Id] == null) {
            // If more specific names are not set, set them to the default name
            Options.WindowName = Options.WindowName == null ? Options.Name : Options.WindowsName;
            Options.MenuName = Options.MenuName == null ? Options.Name : Options.MenuName;
            Options.TaskName = Options.TaskName == null ? Options.Name : Options.TaskName;
            // Set default bools
            Options.IgnoreMinSize = Options.IgnoreMinSize ? true : false;
            Options.Centered = Options.IgnoreMinSize ? true : false;
            // Set options
            this.Windows[Options.Id] = Options;
            // Add the items to the menu accordingly
            if (Options.Menu != null && Options.Menu.constructor == Array) for (i = 0; i < Options.Menu.length; i++) this.AddMenuItem({ Id: Options.Id, Parent: Options.Menu[i] });
            else if (Options.Menu != null) this.AddMenuItem({ Id: Options.Id, Parent: Options.Menu });
        }
    },
    IncrementIdle: function() {
        // Add to idle time
        Acore.IdleTime++;
        // If idle for long enough to start the count down
        if (Acore.IdleTime + Acore.TimeoutCount >= Acore.Timeout) {
            // Open AcoreIdle Modal
            var WindowOptions = {
                "Name": "Idle Logout",
                "Height": "75px",
                "Width": "450px",
                "WindowFrame": false
            };
            // If there is already a modal window open, close it first
            if (Acore.Windows.ModalWindow != null) $("#window_ModalWindow").Close();
            Acore.Modal(WindowOptions);
            $("#window_ModalWindow_content").AcoreIdle();
        }
    },
    Modal: function(Options) {
        //  Options:
        //      Name        Master window name
        //      Url         AJAX URL for window's content
        //      Content     Alternate to URL, pass DOM object to append to window content
        //      Input       Variables to post to Url
        //      OnLoad      Window onLoad/Callback event
        //      Width       Overloaded window default width
        //      Height      Overloaded window default height
        //      WindowFrame Bool switch to display window with or without the standard frame
        //      IconX       Pixel position in sprite for Window/Taskbar icon X axis
        //      IconY       Pixel position in sprite for Window/Taskbar icon Y axis
        // Ensure all temporary modal windows have the ModalWindow identifier
        Options.Id = "ModalWindow";
        // If no name is specified, set default name
        if (Options.Name == null) Options.Name = "Axxess Agencycore";
        // Add window
        this.AddWindow(Options);
        // Set modal overrides
        this.Windows.ModalWindow.Modal = true;
        this.Windows.ModalWindow.IgnoreMinSize = true;
        this.Windows.ModalWindow.Center = true;
        this.Windows.ModalWindow.Resize = false;
        this.Windows.ModalWindow.Status = false;
        this.Windows.ModalWindow.TaskName = null;
        // Open modal window
        this.Open(Options.Id, Options.Url, Options.OnLoad, Options.Input);
    },
    OnWindowLoad: function(WindowContent) {
        $("input[type=date]", WindowContent).DatePicker();
        $("input[type=time]", WindowContent).TimePicker();
        $(".checkgroup .option input[type=checkbox],.checkgroup .option input[type=radio]", WindowContent).each(function() {
            if ($(this).prop("checked")) $(this).closest(".option").addClass("selected");
            else $(this).closest(".option").removeClass("selected");
            $(this).change(function() {
                $(".checkgroup .option input[type=checkbox],.checkgroup .option input[type=radio]", WindowContent).each(function() {
                    if ($(this).prop("checked")) $(this).closest(".option").addClass("selected");
                    else $(this).closest(".option").removeClass("selected");
                })
            })
        });
    },
    Open: function(Id, Url, OnLoad, Inputs) {
        // Ensure the top menus are all collapsed
        $("ul.menu").hide();
        // If window exists
        if (this.Windows[Id] != null) {
            // If window is already open, focus and growl
            if (this.Windows[Id].IsOpen) {
                U.growl("You already have a open window for " + this.Windows[Id].Name + ". Please close down this window before opening another one like it.", "error");
                $("#window_" + Id).WinFocus();
                // If number of windows open is less than maximum number of windows
            } else if (this.OpenWindows < this.MaxWindows) {
                // Set window options
                var Options = Acore.Windows[Id];
                Options.Id = Id;
                // Set window overrides
                if (Url != undefined) Options.Url = Url;
                if (OnLoad != undefined) Options.OnLoad = OnLoad;
                Options.Inputs = Inputs;

                // Add window onto the desktop
                $("#desktop").append(
                    $("<div/>").AcoreWindow(Options)
                );
                if (!Acore.Mobile) {
                    // If window is smaller than minimum sizes, set to minimum size
                    if (Options.IgnoreMinSize == false && $("#window_" + Id).width() < Acore.MinWinWidth) $("#window_" + Id).css("width", Acore.MinWinWidth);
                    if (Options.IgnoreMinSize == false && $("#window_" + Id).height() < Acore.MinWinHeight) $("#window_" + Id).css("height", Acore.MinWinHeight);
                    // If window is larger than desktop, auto-maximize window
                    if (($("#window_" + Id).width() > $("#desktop").width()) || ($("#window_" + Id).height() > $("#desktop").height())) $("#window_" + Id).Maximize();
                    // If window is to be centered, set top and left accordingly
                    if (Options.Center) $("#window_" + Id).css({
                        "top": ($("#desktop").height() - $("#window_" + Id).height()) / 2 + "px",
                        "left": ($("#desktop").width() - $("#window_" + Id).width()) / 2 + "px"
                    });
                    // If window is to be cascaded, set top and left accordingly
                    else {
                        // Increment left and top counters, and if window can't fit reset to zero
                        if (++Acore.CssLeft * Acore.CascadeGap + $("#window_" + Id).width() > $("#desktop").width()) Acore.CssLeft = 0;
                        if (++Acore.CssTop * Acore.CascadeGap + $("#window_" + Id).height() > $("#desktop").height()) Acore.CssTop = 0;
                        // Set top and left accordingly
                        $("#window_" + Id).css({
                            "top": (Acore.CssTop * Acore.CascadeGap) + "px",
                            "left": (Acore.CssLeft * Acore.CascadeGap) + "px"
                        });
                    }
                }
                // Set IsOpen flag to true
                this.Windows[Id].IsOpen = true;
                // Else growl to user that maximum number of windows are already open
            } else U.growl("You can only have " + this.MaxWindows + " concurrent windows at once. Please close down some of your windows to proceed", "error");
            // Else growl to user that window cannot be found
        } else U.growl("Not able to find window: " + Id + ".  Please contact Axxess for assistance in troubleshooting.", "error");
    },
    OpenPrintView: function(Options) {
        //  Options:
        //      Url         URL to print preview
        //      PdfUrl      URL to fetch PDF
        //      PdfData     Data for PDF Print POST
        //      ReturnClick Function for return button
        //      Buttons     Buttons array
        //          Text        Button text
        //          Click       Button click function
        // Open AcorePrintView modal
        var WindowOptions = {
            "Name": "Print View",
            "Height": "95%",
            "Width": "950px",
            "WindowFrame": false
        };
        this.Modal(WindowOptions);
        $("#window_ModalWindow_content").AcorePrintView(Options);
    },
    Ping: function() {
        // Make AJAX POST to ping server
        U.postUrl("Ping", null, function(data) {
            if (!data.isSuccessful) $(location).attr("href", "Login");
        })
    },
    RemoveWindow: function(Id) {
        // If window is found, remove from Acore window information registry
        if (this.Windows[Id].Menu == undefined) this.Windows[Id] = null;
        // Else growl to user that window cannot be found
        else U.growl("Cannot remove a window which is referenced in the main menu.", "error");
    }
};
(function($) {
    $.extend($.fn, {
        AcoreDesktop: function() {
            return this.each(function() {
                // If loded in frame, reload out of the frame
                if (window.location !== window.top.location) window.top.location = window.location;
                // If mobile device detected, set mobile flag to true and import stylesheet
                var browser = navigator.userAgent;
                if (browser.match(/Android/i) || browser.match(/webOS/i) || browser.match(/iPhone/i) || browser.match(/iPod/i) || browser.match(/iPad/i)) Acore.Mobile = true;
                if (Acore.Mobile)
                    $("head").append(
                        $("<link/>", { "rel": "stylesheet", "type": "text/css", "href": "Content/handheld.css" })
                    );
                // Build DOM for desktop
                $(this).append(
                    $("<div/>", { "class": "abs", "id": "desktop" }).bind("contextmenu", function(Event) {
                        $("#mainmenu").clone(true).ContextMenu(Event);
                    })).append(
                    $("<div/>", { "class": "abs", "id": "bar_top", "style": "z-index:" + parseInt(Acore.MaxWindows + 1) + ";" }).append(
                        $("<span/>", { "class": "float_right" }).append(
                            $("<a/>", { "href": "Logout", "text": "Logout" }).prepend(
                                $("<span/>", { "class": "img icon", "style": "background-position:-208px -84px" })))).append(
                        $("<ul/>", { "id": "mainmenu" })
                    )).append(
                    $("<div/>", { "id": "tab", "style": "z-index:" + parseInt(Acore.MaxWindows + 1) + ";" })).append(
                    $("<div/>", { "class": "abs", "id": "bar_bottom", "style": "z-index:" + parseInt(Acore.MaxWindows + 1) + ";" }).append(
                        $("<ul/>", { "id": "task" })).append(
                        $("<span/>", { "class": "float_right", "html": "&#169; 2009 &#8211; " + (new Date).getFullYear() + " Axxess&#8482; Technology Solutions, All Rights Reserved" })
                    )
                );
                // Set keyboard shortcuts
                $(document).keypress(function(e) {
                    // ALT+R JavaScript run prompt
                    if (e.which == 114 && e.altKey) eval(prompt("Run:\nRun the following JavaScript command:", ""));
                    // ALT+W Open acore window by id
                    if (e.which == 119 && e.altKey) eval("Acore.Open('" + prompt("Load Window:\nOpen the following AgencyCore window:", "") + "')");
                    // ALT+L Logout of software
                    if (e.which == 108 && e.altKey) if (confirm("Are you sure you want to logout?")) $(location).attr("href", "/Logout");
                    // ALT+X Close current acore window
                    if (e.which == 120 && e.altKey) if ($(".window.active").length) $(".window.active").Close();
                });
                // Bind mouse and keyboard events to trigger resetting idle counter
                $(document).bind("mousemove keydown DOMMouseScroll mousewheel mousedown", function() {
                    Acore.IdleTime = 0;
                });
                // Set thread to keep server connection alive
                Acore.PingTimer = setInterval(Acore.Ping, (Acore.Timeout > 1 ? Acore.Timeout - 1 : 1) * Acore.TimeoutInterval);
                // Set thread to count idle time
                Acore.TimeoutTimer = setInterval(Acore.IncrementIdle, Acore.TimeoutInterval);
            });
        },
        // Initialize idle modal
        AcoreIdle: function() {
            return this.each(function() {
                // Stop thread to count idle time
                clearInterval(Acore.TimeoutTimer);
                // Build idle countsown modal
                $(this).append(
                    $("<div/>", { "id": "idlemodal", "text": "Due to inactivity, you will automatically be logged off in " }).append(
                        $("<span/>", { "id": "timeoutsec" })
                    ).append(
                        $("<div/>", { "class": "buttons" }).append(
                            $("<ul/>").append(
                                $("<li/>").append(
                                    $("<a/>", { "href": "javascript:void(0);", "id": "idlemodal_stay", "text": "Stay Logged In" }).click(function() {
                                        // Stop thread for second by second updates
                                        clearInterval(IdleCountdown);
                                        // Close idle countdown modal
                                        $(this).closest(".window").Close();
                                        // Reset thread to count idle time
                                        Acore.TimeoutTimer = setInterval(Acore.IncrementIdle, Acore.TimeoutInterval);
                                    })
                                )
                            ).append(
                                $("<li/>").append(
                                    $("<a/>", { "href": "javascript:void(0);", "id": "idlemodal_logout", "text": "Logout" }).click(function() {
                                        // Logout of software
                                        $(location).attr("href", "/Logout");
                                    })
                                )
                            )
                        )
                    )
                );
                // Calculate auto-logout time
                var logout = new Date();
                logout.setTime(logout.getTime() + Acore.TimeoutCount * Acore.TimeoutInterval);
                // Set thread for second by second updates
                IdleCountdown = setInterval(function() {
                    // Calulate time remaining till auto-logout
                    var currentTime = new Date();
                    var remaining = new Date();
                    remaining.setTime(logout.getTime() - currentTime.getTime());
                    // If time is up, logout of software
                    if (parseInt(remaining.getMinutes()) == 59) $(location).attr("href", "/Logout");
                    // Else display amount of time remaining
                    else $("#timeoutsec").html(remaining.getMinutes() + ":" + (remaining.getSeconds() < 10 ? "0" + remaining.getSeconds() : remaining.getSeconds()));
                }, 1000);
            });
        },
        AcorePrintView: function(Options) {
            return this.each(function() {
                // Build basic print view
                $(this).css("overflow", "hidden").append(
                    $("<div/>", { "id": "printbox" }).append(
                        $("<iframe/>", { "name": "printview" + Acore.PrintId, "id": "printview", "class": "loading", "src": Options.Url }).load(function() {
                            $(this).removeClass("loading");
                        })
                    )).append(
                    $("<div/>", { "id": "printcontrols", "class": "buttons" }).append(
                        $("<ul/>").append(
                            $("<li/>").append(
                                $("<a/>", { "id": "printbutton", "text": "Print" })
                            )).append(
                            $("<li/>").append(
                                $("<a/>", { "href": "javascript:void(0);", "text": "Close" }).click(function() {
                                    $(this).closest(".window").Close();
                                })
                            )
                        )
                    )
                );
                // If PDF Print, set click function
                if (Options.PdfUrl != null) $("#printbutton").click(function() {
                    U.GetAttachment(Options.PdfUrl, Options.PdfData);
                });
                // If HTML Print, set click function
                else
                    $("#printbutton").attr("href", "javascript:void(0);").click(function() {
                        window.frames["printview" + Acore.PrintId].focus();
                        window.frames["printview" + Acore.PrintId].print();
                        Acore.PrintId++;
                    });
                // If return button is enabled, add return reason field and proper button functionality
                if (Options.ReturnClick != null && typeof (Options.ReturnClick) == "function") {
                    $(this).find("#printbox").after(
                        $("<div/>", { "id": "printreturnreason" }).append(
                            $("<label/>", { "class": "strong", "text": "Reason for Return:" })).append(
                            $("<textarea/>")
                        )
                    );
                    $("#printcontrols ul").prepend(
                        $("<li/>", { "class": "very_hidden" }).append(
                            $("<a/>", { "href": "javascript:void(0);", "id": "printreturncancel", "text": "Cancel" }).click(function() {
                                $("#printcontrols li").removeClass("very_hidden");
                                $("#printreturnreason textarea").text("");
                                $("#printreturnreason").slideUp("slow");
                                $("#printreturncancel").parent().addClass("very_hidden");
                            })
                        )).prepend(
                        $("<li/>").append(
                            $("<a/>", { "href": "javascript:void(0);", "id": "printreturn", "text": "Return" }).click(function() {
                                if ($("#printreturncancel").parent().hasClass("very_hidden")) {
                                    $("#printreturnreason").show("slow");
                                    $("#printcontrols li").addClass("very_hidden");
                                    $("#printreturn").parent().add($("#printreturncancel").parent()).removeClass("very_hidden");
                                } else Options.ReturnClick();
                            })
                        )
                    );
                }
                // If custom buttons are defined, add them to the start of the button list
                if (Options.Buttons != null) for (i = Options.Buttons.length; i > 0; i--) {
                    $("#printcontrols ul").prepend(
                        $("<li/>").append(
                            $("<a/>", { "href": "javascript:void(0);", "text": Options.Buttons[i - 1].Text }).click(Options.Buttons[i - 1].Click)
                        )
                    );
                }
            });
        },
        AcoreTask: function(Options) {
            return this.each(function() {
                // Build task bar item
                $(this).attr({
                    "id": "task_" + Options.Id,
                    "class": "task active",
                    "style": "width:" + 100 / Acore.MaxWindows + "%;"
                }).append(
                    $("<a/>", { "href": "javascript:void(0);", "title": Options.TaskName, "text": Options.TaskName }).click(function() {
                        if ($(this).closest(".task").GetWindow().hasClass("active")) $(this).closest(".task").GetWindow().Minimize();
                        else $(this).closest(".task").GetWindow().WinFocus();
                    }).prepend(
                        $("<span/>", { "class": "img icon", "style": "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" })
                    )
                ).bind("contextmenu", function(Event) {
                    // Build context menu
                    var Menu = $("<ul/>");
                    var AcoreWindow = $(this).GetWindow();
                    // If not active window, add option to bring to front
                    if (!AcoreWindow.hasClass("active")) Menu.append(
                        $("<li/>", { "text": "Bring to Front" }).click(function() {
                            AcoreWindow.WinFocus();
                        })
                    );
                    // If not minimized, add option to minimize
                    if (!AcoreWindow.hasClass("minimized")) Menu.append(
                        $("<li/>", { "text": "Minimize" }).click(function() {
                            AcoreWindow.Minimize();
                        })
                    );
                    // If not maximized, add option to maximize
                    if (!AcoreWindow.hasClass("maximized")) Menu.append(
                        $("<li/>", { "text": "Maximize" }).click(function() {
                            AcoreWindow.Maximize();
                        })
                    );
                    // If maximized, add option to restore
                    else Menu.append(
                        $("<li/>", { "text": "Restore" }).click(function() {
                            AcoreWindow.Restore();
                        })
                    );
                    // Add option to close
                    Menu.append(
                        $("<li/>", { "text": "Close" }).click(function() {
                            AcoreWindow.Close();
                        })
                    );
                    // Enable context menu
                    Menu.ContextMenu(Event);
                });
            });
        },
        AcoreWindow: function(Overrides) {
            // Window default values
            var Defaults = {
                "Width": "85%",
                "Height": "85%",
                "IgnoreMinSize": false,
                "WindowFrame": true,
                "StatusBar": true,
                "Resize": true,
                "Center": false,
                "Modal": false,
                "IconX": 220,
                "IconY": 12
            };
            // Merge defaults with overrides
            var Options = $.extend({}, Defaults, Overrides);
            return this.each(function() {
                if (Acore.Mobile) {
                    // Minimize all windows
                    $(".window").Minimize();
                    // Build basic mobile window
                    $(this).attr({ "id": "window_" + Options.Id, "class": "window" }).append(
                        $("<div/>", { "class": "window_top" }).append(
                            $("<span/>", { "class": "float_left" }).append(
                                $("<span/>", { "class": "img icon", "style": "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" })
                            )).append(
                            $("<span/>", { "class": "abs", "text": Options.WindowName })).append(
                            $("<span/>", { "class": "float_right" }).append(
                                $("<a/>", { "href": "javascript:void(0);", "class": "window_min" }).click(function() { $(this).closest(".window").Minimize() })).append(
                                $("<a/>", { "href": "javascript:void(0);", "class": "window_close" }).click(function() { $(this).closest(".window").Close() })
                            )
                        )).append(
                        $("<div/>", { "id": "window_" + Options.Id + "_content", "class": "abs window_content loading" })
                    );
                    // Load window content
                    if (Options.Content) $(".window_content", this).removeClass("loading").append(Options.Content)
                    else if (Options.Url) $(this).GetData(Options.Url, Options.OnLoad, Options.Inputs);
                    else $(".window_content", this).removeClass("loading");
                    // Build task bar item for window
                    $("#task").append($("<li/>").AcoreTask(Options));
                    // Bring window to focus
                    $(this).WinFocus();
                } else {
                    // Build basic window
                    $(this).attr({
                        "id": "window_" + Options.Id,
                        "class": "abs window"
                    }).css({
                        "position": "absolute",
                        "z-index": ++Acore.OpenWindows
                    }).mousedown(function() {
                        if (!$(this).hasClass("active")) $(this).WinFocus();
                    }).append(
                        $("<div/>", { "class": "abs window_inner" }).append(
                            $("<div/>", { "id": "window_" + Options.Id + "_content", "class": "abs window_content loading" }).scroll(function() {
                                // Collapse date picker on scroll
                                $(".ui-datepicker").hide();
                            })
                        )
                    );
                    // Load window content
                    if (Options.Content) $(".window_content", this).removeClass("loading").append(Options.Content)
                    else if (Options.Url) $(this).GetData(Options.Url, Options.OnLoad, Options.Inputs);
                    else $(".window_content", this).removeClass("loading");
                    // If window has frame, add top bar
                    if (Options.WindowFrame)
                        $(".window_inner", this).prepend(
                            $("<div/>", { "class": "window_top" }).dblclick(function() {
                                if ($(this).closest(".window").hasClass("maximized")) $(this).closest(".window").Restore();
                                else $(this).closest(".window").Maximize();
                            }).append(
                                $("<span/>", { "class": "float_left" }).append(
                                    $("<span/>", { "class": "img icon", "style": "background-position:-" + Options.IconX + "px -" + Options.IconY + "px" }).dblclick(function() {
                                        $(this).closest(".window").Close();
                                    })
                                )).append(
                                $("<span/>", { "class": "abs", "text": Options.WindowName })).append(
                                $("<span/>", { "class": "float_right" }).append(
                                    $("<a/>", { "href": "javascript:void(0);", "class": "window_min" }).click(function() {
                                        $(this).closest(".window").Minimize();
                                    })).append(
                                    $("<a/>", { "href": "javascript:void(0);", "class": "window_resize" }).click(function() {
                                        if ($(this).closest(".window").hasClass("maximized")) $(this).closest(".window").Restore();
                                        else $(this).closest(".window").Maximize();
                                    })).append(
                                    $("<a/>", { "href": "javascript:void(0);", "class": "window_close" }).click(function() {
                                        $(this).closest(".window").Close();
                                    })
                                )
                            )
                        );
                    // If unframed window, set content top accordingly
                    else
                        $(".window_content", this).css({
                            "top": "0",
                            "bottom": "0"
                        });
                    // If window is resizable, enable resizing
                    if (Options.Resize)
                        $(this).resizable({
                            containment: "parent",
                            handles: "n, ne, e, se, s, sw, w, nw",
                            minWidth: Acore.MinWinWidth,
                            minHeight: Acore.MinWinHeight
                        });
                    // If window is not resizable and framed, remove resize button
                    else if (Options.WindowFrame) $(".window_resize", this).remove();
                    // If framed window has status bar, add in status bar and set content bottom accordingly
                    if (Options.WindowFrame && Options.StatusBar) {
                        $(".window_inner", this).append($("<div/>", { "class": "abs window_bottom", "text": Options.Status }));
                        $(".window_content", this).css("bottom", "21px");
                    }
                    // Set window size
                    if (parseInt(Options.Width) > 0) $(this).css("width", Options.Width);
                    if (parseInt(Options.Height) > 0) $(this).css("height", Options.Height);
                    // If modal window, shade out the background and increase the z-index
                    if (Options.Modal) {
                        $("body").append(
                            $("<div/>", { "id": "shade", "style": "z-index:" + parseInt(Acore.MaxWindows + 1) }).bind("contextmenu", function(Event) {
                                Event.preventDefault();
                            })
                        );
                        $(this).css("z-index", parseInt(Acore.MaxWindows + 2));
                        // If framed non-modal window, enable draggable
                    } else if (Options.WindowFrame)
                        $(this).draggable({
                            containment: "parent",
                            handle: ".window_top .abs"
                        });
                    // Build task bar item for window
                    $("#task").append($("<li/>").AcoreTask(Options));
                    // Bring window to focus
                    $(this).WinFocus();
                }
            });
        },
        Close: function() {
            return this.each(function() {
                // If object is a window
                if ($(this).hasClass("window")) {
                    // If CKEditor found in window, remove instance
                    if ($(".cke_editor", this).length) CKEDITOR.remove(CKEDITOR.instances["New_Message_Body"]);
                    // If background is shaded, remove shade
                    if ($("#shade").length) $("#shade").remove();
                    // Set IsOpen flag to false
                    Acore.Windows[$(this).GetAcoreId()].IsOpen = false;
                    // Decrement number of open windows
                    Acore.OpenWindows--;
                    // If temporary modal window, remove window from registry
                    if ($(this).GetAcoreId() == "ModalWindow") Acore.RemoveWindow("ModalWindow");
                    // Remove task bar
                    $(this).GetTask().remove();
                    // Remove window
                    $(this).remove();
                } else U.growl("Error: Unexpected call to close non-window element. Cannot close element that is not a window.", "error");
            });
        },
        ContextMenu: function(Event) {
            return this.each(function() {
                // If no other context menus are open and the DOM focus is not upon an input of type text nor textarea
                if ($(".context_menu").length == 0 && $("input.t-input:focus,input[type=text]:focus,textarea:focus").length == 0) {
                    // If no text is highlighted
                    if ((document.getSelection && document.getSelection() == "") || (document.selection && document.selection.createRange().text == "")) {
                        // Prevent the default context menu from popping up
                        Event.preventDefault();
                        // Build context menu into the DOM
                        $("body").append(
                            $("<div/>").css({
                                "position": "fixed",
                                "top": "0",
                                "left": "0",
                                "width": "100%",
                                "height": "100%",
                                "z-index": "999"
                            }).click(function() {
                                $(this).remove();
                            }).bind("contextmenu", function(Event) {
                                Event.preventDefault();
                                // If menu is too tall to display below the cursor, then display above
                                if (Event.pageY + $(".context_menu").height() > $("body").height()) $(".context_menu").css({ "bottom": $("body").height() - Event.pageY + "px", "top": "auto" });
                                else $(".context_menu").css({ "top": Event.pageY + "px", "bottom": "auto" });
                                // If menu is too wide to display to the right of the cursor, then display to the left
                                if (Event.pageX + $(".context_menu").width() > $("body").width()) $(".context_menu").css({ "right": $("body").width() - Event.pageX + "px", "left": "auto" });
                                else $(".context_menu").css({ "left": Event.pageX + "px", "right": "auto" });
                            }).append(
                                $(this).addClass("context_menu").css("z-index", "1000")
                            )
                        );
                        // If menu is too tall to display below the cursor, then display above
                        if (Event.pageY + $(".context_menu").height() > $("body").height()) $(".context_menu").css("bottom", $("body").height() - Event.pageY + "px");
                        else $(".context_menu").css("top", Event.pageY + "px");
                        // If menu is too wide to display to the right of the cursor, then display to the left
                        if (Event.pageX + $(".context_menu").width() > $("body").width()) $(".context_menu").css("right", $("body").width() - Event.pageX + "px");
                        else $(".context_menu").css("left", Event.pageX + "px");
                        // If context menu has submenus, enable superfish plugin
                        if ($(".context_menu ul").length) $(".context_menu").superfish();
                        // Apply hover class on mouse over and remove on mouse out
                        $(".context_menu li").each(function() { $(this).mouseover(function() { $(this).addClass("hover") }) });
                        $(".context_menu li").each(function() { $(this).mouseout(function() { $(this).removeClass("hover") }) });
                    }
                }
            });
        },
        GetAcoreId: function() {
            // If object is window or task, extrapolate the string literal for the Acore Id and return it
            if (($(this).hasClass("window") || $(this).hasClass("task") != null) && $(this).attr("id") != null) return $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
        },
        GetData: function(Url, OnLoad, Inputs) {
            return this.each(function() {
                // If object is a window
                if ($(this).hasClass("window")) {
                    // Load window content
                    $(".window_content", this).load(Url, Inputs, function(ResponseText, TextStatus, XMLHttpRequest) {
                        $(this).removeClass("loading");
                        // If it was returned an error, display the generic AJAX error message
                        if (TextStatus == "error") $(this).html(U.AjaxError);
                        // Call centralised OnLoad function
                        Acore.OnWindowLoad(this);
                        // If OnLoad function defined, launch it
                        if (OnLoad != null && typeof (OnLoad) == "function") OnLoad();
                    });
                }
            });
        },
        GetTask: function() {
            // If object is a window, find and return its corresponding task bar item
            if ($(this).hasClass("window") && $(this).attr("id") != null) {
                var task = "#task_" + $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
                if ($(task).length) return $(task);
            }
            return false;
        },
        GetWindow: function() {
            // If object is a task bar item, find and return its corresponding window
            if (($(this).hasClass("context") || $(this).hasClass("task")) && $(this).attr("id") != null) {
                var window = "#window_" + $(this).attr("id").substring($(this).attr("id").lastIndexOf("_") + 1, $(this).attr("id").length);
                if ($(window).length) return $(window);
            }
            return false;
        },
        Maximize: function() {
            return this.each(function() {
                // If object is a window, set restore attributes, and then set CSS to maximize
                if ($(this).hasClass("window")) {
                    $(this).attr({
                        "restore-top": $(this).css("top"),
                        "restore-left": $(this).css("left"),
                        "restore-right": $(this).css("right"),
                        "restore-bottom": $(this).css("bottom"),
                        "restore-width": $(this).css("width"),
                        "restore-height": $(this).css("height")
                    }).addClass("maximized").removeClass("minimized").css({
                        "top": $(this).attr("id") == "window_ModalWindow" ? "20px" : "0",
                        "left": $(this).attr("id") == "window_ModalWindow" ? "20px" : "0",
                        "right": $(this).attr("id") == "window_ModalWindow" ? "20px" : "0",
                        "bottom": $(this).attr("id") == "window_ModalWindow" ? "20px" : "0",
                        "width": "auto",
                        "height": "auto"
                    });
                    // If not the active window, then bring to front
                    if (!$(this).hasClass("active")) $(this).WinFocus();
                } else U.growl("Error: Unexpected call to maximize non-window element. Cannot maximize element that is not a window.", "error");
            });
        },
        Minimize: function() {
            return this.each(function() {
                // If object is a window, minimize and ensure not active
                if ($(this).hasClass("window")) $(this).removeClass("active").addClass("minimized").css("z-index", "-1").GetTask().removeClass("active");
                else U.growl("Error: Unexpected call to minimize non-window element. Cannot minimize element that is not a window.", "error");
            });
        },
        Rename: function(NewName) {
            return this.each(function() {
                // If object is a window
                if ($(this).hasClass("window")) {
                    // If window has frame, rename at the top of the window
                    if ($(".window_top .abs", this).length) $(".window_top .abs", this).html(NewName);
                    // Rename task bar item
                    $(this).GetTask().find("a").html(
                        $(this).GetTask().find(".img")
                    ).append(NewName).attr("title", $(this).GetTask().text());
                } else U.growl("Error: Unexpected call to rename non-framed window element. Cannot rename element that is not a window with a frame.", "error");
            });
        },
        Restore: function() {
            return this.each(function() {
                // If object is a window and maximized, restore css to old numbers
                if ($(this).hasClass("window") && $(this).hasClass("maximized")) {
                    $(this).removeClass("maximized").css({
                        "top": $(this).attr("restore-top"),
                        "left": $(this).attr("restore-left"),
                        "right": $(this).attr("restore-right"),
                        "bottom": $(this).attr("restore-bottom"),
                        "width": $(this).attr("restore-width"),
                        "height": $(this).attr("restore-height")
                    });
                } else U.growl("Error: Unexpected call to restore non-window element. Cannot restore element that is not a window.", "error");
            });
        },
        Status: function(Message) {
            return this.each(function() {
                // If object is a window with a status bar, update message
                if ($(this).hasClass("window") && $(".window_bottom", this).length) $(".window_bottom", this).text(Message);
                else U.growl("Error: Unexpected call to status update invalid element. Cannot update status bar on non-window elements or windows that do not contain status bars.", "error");
            });
        },
        WinFocus: function() {
            return this.each(function() {
                // If object is a window
                if ($(this).hasClass("window")) {
                    // Clear current active window
                    $(".window.active", "#desktop").removeClass("active");
                    $(".active", "#task").removeClass("active");
                    $(".ui-menu").hide();
                    // Make current window active
                    $(this).addClass("active").removeClass("minimized");
                    $(this).GetTask().addClass("active");
                    // Recalculate windows' z-index
                    $(".window:not(.active)").each(function() {
                        if ($(this).css("z-index") >= $(".window.active").css("z-index")) $(this).css("z-index", parseInt($(this).css("z-index") - 1));
                    });
                    $(".window.active").css("z-index", Acore.OpenWindows);
                } else U.growl("Error: Unexpected call to window focus non-window element. Cannot restore element that is not a window.", "error");
            });
        }
    })
})(jQuery);