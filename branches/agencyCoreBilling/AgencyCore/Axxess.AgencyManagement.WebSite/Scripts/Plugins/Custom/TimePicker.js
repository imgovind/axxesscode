(function($) {
    $.extend($.fn, {
        TimePicker: function() {
            return this.each(function() {
                var $input = $(this);
                // If of input type time, change to type text
                if ($input.attr("type") == "time") {
                    var $newinput = $("<input/>", { "type": "text", "name": $input.attr("name"), "id": $input.attr("id"), "class": $input.attr("class"), "value": $input.attr("value") });
                    $input.after($newinput);
                    $input.remove();
                    $input = $newinput;
                }
                // Initialize time picker
                $input.addClass("time").timepicker({
                    timeFormat: 'hh:mm p',
                    minHour: null,
                    minMinutes: null,
                    minTime: null,
                    maxHour: null,
                    maxMinutes: null,
                    maxTime: null,
                    startHour: 7,
                    startMinutes: 0,
                    startTime: null,
                    interval: 15,
                    change: function(time) { }
                })
            })
        }
    })
})(jQuery);