﻿namespace Axxess.AgencyManagement.SupportApp
{
    using System;
    using System.Web;
    using System.Security.Principal;

    using Enums;
    using Domain;
    using Security;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;

    using Axxess.AgencyManagement.Extensions;

    public static class Current
    {
        public static Func<DateTime> Time = () => DateTime.UtcNow;

        public static AxxessSupportIdentity User
        {
            get
            {
                AxxessSupportIdentity identity = null;
                if (HttpContext.Current.User is WindowsIdentity)
                {
                    throw new InvalidOperationException("Windows authentication is not supported.");
                }

                if (HttpContext.Current.User is AxxessSupportPrincipal)
                {
                    AxxessSupportPrincipal principal = (AxxessSupportPrincipal)HttpContext.Current.User;
                    identity = (AxxessSupportIdentity)principal.Identity;
                }

                return identity;
            }
        }

        public static Guid LoginId
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.LoginId;
                }

                return Guid.Empty;
            }
        }

        public static string DisplayName
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.DisplayName;
                }

                return string.Empty;
            }
        }

        public static string IpAddress
        {
            get
            {
                var request = HttpContext.Current.Request;
                return request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.ServerVariables["REMOTE_ADDR"];
            }
        }


        public static bool IsAxxessAdmin
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.IsAxxessAdmin;
                }
                return false;
            }
        }

        public static bool IsAxxessSupport
        {
            get
            {
                if (User != null && User.Session != null)
                {
                    return User.Session.IsAxxessSupport;
                }
                return false;
            }
        }
    }
}
