﻿namespace Axxess.AgencyManagement.SupportApp.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    public class AgencyService : IAgencyService
    {
        #region Private Members /Constructor

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, ILookUpDataProvider lookUpDataProvider)
        {
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region IAgencyService Members

        public bool CreateAgency(Agency agency)
        {
            try
            {
                agency.IsSuspended = false;
                if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count == 3)
                {
                    agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count == 3)
                {
                    agency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count == 3)
                {
                    agency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                }

                if (agencyRepository.Add(agency))
                {
                    var location = new AgencyLocation();
                    location.Id = Guid.NewGuid();
                    location.Name = agency.LocationName;
                    location.AddressLine1 = agency.AddressLine1;
                    location.AddressLine2 = agency.AddressLine2;
                    location.AddressCity = agency.AddressCity;
                    location.AddressStateCode = agency.AddressStateCode;
                    location.AddressZipCode = agency.AddressZipCode;
                    location.AgencyId = agency.Id;
                    location.IsMainOffice = true;
                    location.IsDeprecated = false;
                    location.MedicareProviderNumber = agency.MedicareProviderNumber;
                    location.SubmitterId = agency.SubmitterId;
                    location.SubmitterName = agency.SubmitterName;
                    location.Payor = agency.Payor;
                    location.SubmitterPhone = agency.SubmitterPhone;
                    location.SubmitterFax = agency.SubmitterFax;

                    if (agency.PhoneArray != null && agency.PhoneArray.Count > 0)
                    {
                        location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (agency.FaxArray != null && agency.FaxArray.Count > 0)
                    {
                        location.FaxNumber = agency.FaxArray.ToArray().PhoneEncode();
                    }

                    var defaultMedicareRates = lookupRepository.DefaultMedicareRates();
                    if (defaultMedicareRates != null)
                    {
                        var costRates = new List<CostRate>();
                        defaultMedicareRates.ForEach(r =>
                        {
                            costRates.Add(new CostRate { PerUnit = r.PerVisitRate, RateDiscipline = r.Discipline });
                        });
                        location.Cost = costRates.ToXml();
                    }

                    var zipCode = lookupRepository.GetZipCode(agency.AddressZipCode);
                    location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;

                    if (agencyRepository.AddLocation(location))
                    {
                        var user = new User
                        {
                            AgencyId = agency.Id,
                            AgencyName = agency.Name,
                            AllowWeekendAccess = true,
                            EmploymentType = "Employee",
                            AgencyLocationId = location.Id,
                            Status = (int)UserStatus.Active,
                            LastName = agency.AgencyAdminLastName,
                            FirstName = agency.AgencyAdminFirstName,
                            PermissionsArray = GeneratePermissions(),
                            EmailAddress = agency.AgencyAdminUsername,
                            Credentials = CredentialTypes.None.GetDescription(),
                            TitleType = TitleTypes.Administrator.GetDescription(),
                            Profile = new UserProfile() { EmailWork = agency.AgencyAdminUsername },
                            AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() }
                        };

                        IUserService userService = Container.Resolve<IUserService>();
                        return userService.CreateUser(user);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            return false;
        }

        public bool UpdateAgency(Agency agency)
        {
            bool result = false;
            var existingAgency = agencyRepository.Get(agency.Id);
            if (agency != null)
            {
                existingAgency.Name = agency.Name;
                existingAgency.TaxId = agency.TaxId;
                existingAgency.IsAgreementSigned = agency.IsAgreementSigned;
                existingAgency.TrialPeriod = agency.TrialPeriod;
                existingAgency.Package = agency.Package;
                existingAgency.ContactPersonEmail = agency.ContactPersonEmail;
                existingAgency.ContactPersonFirstName = agency.ContactPersonFirstName;
                existingAgency.ContactPersonLastName = agency.ContactPersonLastName;
                existingAgency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                existingAgency.TaxIdType = agency.TaxIdType;
                existingAgency.Payor = agency.Payor;
                existingAgency.SubmitterId = agency.SubmitterId;
                existingAgency.SubmitterName = agency.SubmitterName;
                existingAgency.NationalProviderNumber = agency.NationalProviderNumber;
                existingAgency.MedicareProviderNumber = agency.MedicareProviderNumber;
                existingAgency.MedicaidProviderNumber = agency.MedicaidProviderNumber;
                existingAgency.HomeHealthAgencyId = agency.HomeHealthAgencyId;
                existingAgency.CahpsVendor = agency.CahpsVendor;
                existingAgency.IsAxxessTheBiller = agency.IsAxxessTheBiller;
                if (!agency.IsAxxessTheBiller)
                {
                    existingAgency.SubmitterId = agency.SubmitterId;
                    existingAgency.SubmitterName = agency.SubmitterName;
                    existingAgency.Payor = agency.Payor;
                    if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count > 0)
                    {
                        existingAgency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                    }
                    if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count > 0)
                    {
                        existingAgency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                    }
                }
                if (agencyRepository.Update(existingAgency))
                {
                    var main = agencyRepository.GetMainLocation(agency.Id);
                    if (main != null)
                    {
                        main.MedicareProviderNumber = agency.MedicareProviderNumber;
                        if (agencyRepository.UpdateLocation(main))
                        {
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            try
            {
                location.Id = Guid.NewGuid();
                if (location.IsSubmitterInfoTheSame)
                {
                    var agency = agencyRepository.Get(location.AgencyId);
                    if (agency != null)
                    {
                        location.SubmitterId = agency.SubmitterId;
                        location.SubmitterName = agency.SubmitterName;
                        location.Payor = agency.Payor;
                        location.SubmitterPhone = agency.SubmitterPhone;
                        location.SubmitterFax = agency.SubmitterFax;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                    {
                        location.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                    }

                    if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                    {
                        location.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                    }
                }
                var zipCode = lookupRepository.GetZipCode(location.AddressZipCode);
                location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;

                if (agencyRepository.AddLocation(location))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }

            return result;
        }

        public Agency GetAgency(Guid Id)
        {
            Agency agencyViewData = agencyRepository.Get(Id);
            if (agencyViewData != null)
            {
                return agencyViewData;
            }
            return null;
        }

        #endregion

        #region Private Methods

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();

            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }

            return list;
        }

        #endregion
    }
}
