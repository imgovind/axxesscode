﻿namespace Axxess.AgencyManagement.SupportApp.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Services;
    using Security;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Repositories;

    using Telerik.Web.Mvc;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AgencyController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;

        private ISupportMembershipService membershipService = Container.Resolve<ISupportMembershipService>();

        public AgencyController(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, IAgencyService agencyService, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyService, "agencyService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.agencyService = agencyService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        #endregion

        #region Agency Actions

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult New()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserGrid(Guid agencyId)
        {
            return PartialView("Users", agencyRepository.GetById(agencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserList(Guid agencyId)
        {
            IList<UserSelection> userList = new List<UserSelection>();
            var users = userRepository.GetAgencyUsers(agencyId);
            users.ForEach(u =>
            {
                var login = loginRepository.Find(u.LoginId);
                if (login != null)
                {
                    userList.Add(new UserSelection
                    {
                        Id = u.Id,
                        LoginId = login.Id,
                        LastName = u.LastName,
                        FirstName = u.FirstName,
                        Credential = u.DisplayTitle,
                        DisplayName = u.IsDeprecated ? u.DisplayName + " [Deprecated]" : u.DisplayName,
                        EmailAddress = login.EmailAddress,
                        LoginCreated = login.CreatedFormatted,
                        IsLoginActive = login.IsActive
                    });
                }
            });

            return View(new GridModel(userList.OrderBy(u => u.DisplayName)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add([Bind] Agency agency)
        {
            Check.Argument.IsNotNull(agency, "agency");

            var viewData = new JsonViewData();

            if (agency.IsValid)
            {
                if (!agencyService.CreateAgency(agency))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the agency.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Agency was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agency.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id)
        {
            return PartialView(agencyRepository.Get(id));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] Agency agency)
        {
            Check.Argument.IsNotNull(agency, "agency");

            var viewData = new JsonViewData();

            if (agency.IsValid)
            {
                if (!agencyService.UpdateAgency(agency))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the agency.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Agency was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = agency.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult List()
        {
            return PartialView();
        }

        [GridAction]
        public ActionResult Grid()
        {
            var agencies = new List<AgencyLite>();
            var agencyList = agencyRepository.AllAgencies();
            agencyList.ForEach(a =>
            {
                agencies.Add(new AgencyLite { 
                    Id = a.Id, 
                    Name = a.Name,
                    IsSuspended = a.IsSuspended,
                    IsDeprecated = a.IsDeprecated,
                    ContactPersonEmail = a.ContactPersonEmail,
                    ContactPersonDisplayName = a.ContactPersonDisplayName,
                    ContactPersonPhoneFormatted = a.ContactPersonPhoneFormatted,
                    City = a.MainLocation != null ? a.MainLocation.AddressCity : string.Empty,
                    State = a.MainLocation != null ? a.MainLocation.AddressStateCode : string.Empty,
                    ActionText = a.IsDeprecated ? "Restore" : "Suspend"
                });
            });
            return View(new GridModel(agencies));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Impersonate(Guid agencyId, Guid userId)
        {
            var url = membershipService.GetImpersonationUrl(agencyId, userId);
            if (url.IsNotNullOrEmpty())
            {
                return Redirect(url);
            }
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Location Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewLocation(Guid agencyId)
        {
            return PartialView("Location/New", agencyRepository.Get(agencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid)
            {
                if (!agencyService.CreateLocation(location))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the Location.";
                }
                else
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Location was saved successfully";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationGrid(Guid agencyId)
        {
            return PartialView("Location/List", agencyRepository.Get(agencyId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LocationList(Guid agencyId)
        {
            return View(new GridModel(agencyRepository.GetBranches(agencyId).OrderBy(a => a.Name)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditLocation(Guid agencyId, Guid locationId)
        {
            return PartialView("Location/Edit", agencyRepository.FindLocation(agencyId, locationId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLocation([Bind] AgencyLocation location)
        {
            Check.Argument.IsNotNull(location, "location");

            var viewData = new JsonViewData();

            if (location.IsValid)
            {
                if (agencyRepository.FindLocation(location.AgencyId, location.Id) != null)
                {
                    if (!agencyRepository.EditLocation(location))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in editing the location.";
                    }
                    else
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Location was edited successfully";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Selected location don't exist.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = location.ValidationMessage;
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Could not complete this action for this agency." };
            if (agencyRepository.ToggleDelete(id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Agency has been updated.";
            }
            return Json(viewData);
        }

        #endregion
    }
}
