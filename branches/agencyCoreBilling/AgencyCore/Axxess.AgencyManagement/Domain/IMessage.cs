﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Enums;

    public interface IMessage
    {
        Guid Id { get; set; }
        string Body { get; set; }
        string Subject { get; set; }
        MessageType Type { get; set; }
        DateTime Created { get; set; }
    }
}
