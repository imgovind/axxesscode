﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class RecertEvent
    {
        public string Task { get; set; }
        public string Status { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string AssignedTo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime TargetDate { get; set; }
        public string Schedule { get; set; }

        public string TargetDateFormatted { get {return this.TargetDate.ToString("MM/dd/yyyy") ;}}
        public int DateDifference { get; set; }
    }
}
