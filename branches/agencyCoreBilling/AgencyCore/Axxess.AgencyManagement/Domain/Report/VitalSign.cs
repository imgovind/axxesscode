﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class VitalSign
    {
        public string BS { get; set; }
        public string Temp { get; set; }
        public string Resp { get; set; }
        public string Weight { get; set; }
        public string PainLevel { get; set; }
        public string VisitDate { get; set; }
        public string ApicalPulse { get; set; }
        public string RadialPulse { get; set; }
        public string DisciplineTask { get; set; }
        public string UserDisplayName { get; set; }
        public string PatientDisplayName { get; set; }

        public string BPLyingLeft { get; set; }
        public string BPLyingRight { get; set; }
        public string BPSittingLeft { get; set; }
        public string BPSittingRight { get; set; }
        public string BPStandingLeft { get; set; }
        public string BPStandingRight { get; set; }

        public string BPLying { get; set; }
        public string BPSitting { get; set; }
        public string BPStanding { get; set; }


    }
}
