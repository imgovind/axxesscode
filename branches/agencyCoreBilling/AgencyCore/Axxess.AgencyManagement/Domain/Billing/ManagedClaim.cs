﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class ManagedClaim : EntityBase
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }

        public string PatientIdNumber { get; set; }
        public string IsuranceIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }

        public DateTime StartofCareDate { get; set; }

        public string PhysicianNPI { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }

        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }

        public string Type { get; set; }

        public bool AreOrdersComplete { get; set; }
        public bool IsOasisComplete { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public DateTime FirstBillableVisitDate { get; set; }
        public bool AreVisitsComplete { get; set; }
        public bool IsGenerated { get; set; }
        public string DiagnosisCode { get; set; }
        public string ConditionCodes { get; set; }
        public string HippsCode { get; set; }
        public string ClaimKey { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        public bool IsRapGenerated { get; set; }
        public string VerifiedVisits { get; set; }
        public string Supply { get; set; }
        public double SupplyTotal { get; set; }
        public int PrimaryInsuranceId { get; set; }
        public bool IsVisitVerified { get; set; }
        public bool IsSupplyVerified { get; set; }
        public bool IsInfoVerified { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public double ProspectivePay { get; set; }
        public double Payment { get; set; }
        public DateTime PaymentDate { get; set; }
        
        public string AssessmentType { get; set; }
        public string AdmissionSource { get; set; }
        public int PatientStatus { get; set; }
        public string UB4PatientStatus { get; set; }
        public DateTime ClaimDate { get; set; }
        public string Comment { get; set; }
        public DateTime DischargeDate { get; set; }
        public string HealthPlanId { get; set; }
        public string AuthorizationNumber { get; set; }
        public string CBSA { get; set; }
        public string SupplyCode { get; set; }

        [SubSonicIgnore]
        public bool HasMultipleEpisodes { get; set; }
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Format("{0} {1}",this.FirstName,this.LastName);
            }
        }
        [SubSonicIgnore]
        public string ClaimDateRange
        {
            get
            {
                return string.Format("{0} - {1}",this.EpisodeStartDate.ToString("MM/dd/yyyy"),this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }
        [SubSonicIgnore]
        public List<ScheduleEvent> Visits { get; set; }
        [SubSonicIgnore]
        public string Primary { get; set; }
        [SubSonicIgnore]
        public string Second { get; set; }
        [SubSonicIgnore]
        public string Third { get; set; }
        [SubSonicIgnore]
        public string Fourth { get; set; }
        [SubSonicIgnore]
        public string Fifth { get; set; }
        [SubSonicIgnore]
        public string Sixth { get; set; }
        [SubSonicIgnore]
        public Dictionary<string, BillInfo> BillInformations { get; set; }

        [SubSonicIgnore]
        public string ConditionCode18 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode19 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode20 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode21 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode22 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode23 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode24 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode25 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode26 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode27 { get; set; }
        [SubSonicIgnore]
        public string ConditionCode28 { get; set; }

        [SubSonicIgnore]
        public bool IsMedicareHMO { get; set; }

        [SubSonicIgnore]
        public int UnitType { get; set; }

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Patient first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Patient last name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PatientIdNumber), "Patient record number is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.IsuranceIdNumber), "Patient medicare number is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.HealthPlanId), "Patient insurance health plan Id is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AuthorizationNumber), "Patient health plan Id authorization number is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Gender), "Patient gender has to be selected."));
            AddValidationRule(new Validation(() => !this.DOB.IsValid() || (this.DOB.Date <= DateTime.MinValue.Date), "DOB is not valid date."));
            if (this.DOB.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.DOB.Date < DateTime.Now.Date), "DOB has to be less than todays date."));
            }
            AddValidationRule(new Validation(() => this.EpisodeStartDate.Date <= DateTime.MinValue.Date, "Claim start Date don't have a correct date value."));
            AddValidationRule(new Validation(() => this.EpisodeEndDate.Date <= DateTime.MinValue.Date, "Claim end Date don't have a correct date value."));
            AddValidationRule(new Validation(() => this.EpisodeStartDate.Date > this.EpisodeEndDate.Date, "Claim start date must be less than claim end date."));

            if (this.StartofCareDate.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.EpisodeStartDate.Date >= this.StartofCareDate.Date), "Episode start date has to be greater than or equal to Admission date."));
                AddValidationRule(new Validation(() => !(this.StartofCareDate.Date <= DateTime.Now.Date), "Admission date has to be less than or equal to todays date."));
                if (this.DOB.IsValid())
                {
                    AddValidationRule(new Validation(() => !(this.StartofCareDate.Date > this.DOB.Date), "Admission date has to be greater than DOB."));
                }
            }

            AddValidationRule(new Validation(() => (this.FirstBillableVisitDate.Date <= DateTime.MinValue.Date), "First billable visit date is not valid date."));
            if (this.FirstBillableVisitDate.IsValid())
            {
                AddValidationRule(new Validation(() => !(this.FirstBillableVisitDate.Date >= this.StartofCareDate.Date), "First billable visit date has to be greater or equal to Admission date."));
                AddValidationRule(new Validation(() => !(this.FirstBillableVisitDate.Date >= this.EpisodeStartDate.Date), "First billable visit date has to be greater or equal to episode start date."));
            }
            AddValidationRule(new Validation(() => !this.StartofCareDate.IsValid() || (this.StartofCareDate.Date <= DateTime.MinValue.Date), "Admission Date is not valid date."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Patient address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Patient city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Patient state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Patient zip is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianLastName), "Physician last name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianFirstName), "Physician first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PhysicianNPI), "Physician NPI is required."));

        }

        #endregion
    }
}
