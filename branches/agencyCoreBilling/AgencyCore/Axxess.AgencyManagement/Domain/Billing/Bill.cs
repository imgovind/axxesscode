﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    public class Bill
    {
        public Bill()
        {
            this.RapClaims = new List<ClaimBill>();
            this.FinalClaims = new List<ClaimBill>();
        }

        public IList<Rap> Raps { get; set; }
        public IList<Final> Finals { get; set; }

        public IList<ClaimBill> RapClaims { get; set; }
        public IList<ClaimBill> FinalClaims { get; set; }

        public bool IsElectronicSubmssion { get; set; }
        public Guid BranchId { get; set; }
        public int Insurance { get; set; }
        public bool IsMedicareHMO { get; set; }

        public Agency Agency { get; set; }

    }
}
