﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public class Remittance
    {
       public Guid Id { get; set; }
       public Guid AgencyId { get; set; }
       public string Data { get; set; }
       public string RemitId { get; set; }
       public int TotalClaims { get; set; }
       public double CoveredAmount { get; set; }
       public double ChargedAmount { get; set; }
       public double PaymentAmount { get; set; }
       public DateTime RemittanceDate { get; set; }
       public bool IsDeprecated { get; set; }
    }
}
