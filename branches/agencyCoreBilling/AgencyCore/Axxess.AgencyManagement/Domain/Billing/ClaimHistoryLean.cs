﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Extension;

    public class ClaimHistoryLean
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime PaymentDate { get; set; }
        public double PaymentAmount { get; set; }
        public string Type { get; set; }
        public int Status { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public DateTime ClaimDate { get; set; }
        public double ClaimAmount { get; set; }

        public string EpisodeRange
        {
            get
            {
                return string.Format("{0}-{1}", this.EpisodeStartDate != null ? this.EpisodeStartDate.ToString("MM/dd/yyyy") : "", this.EpisodeEndDate != null ? this.EpisodeEndDate.ToString("MM/dd/yyyy") : "");
            }
        }
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), this.Status));

            }
        }
        public string PaymentDateFormatted
        {
            get
            {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToShortDateString().ToZeroFilled() : string.Empty;
            }
        }
    }
}
