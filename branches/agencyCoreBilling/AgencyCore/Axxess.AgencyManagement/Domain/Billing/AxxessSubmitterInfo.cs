﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.SqlGeneration.Schema;

namespace Axxess.AgencyManagement.Domain
{
   public class AxxessSubmitterInfo
    {
       public int Id { get; set; }
       public string Code { get; set; }
       public string Name { get; set; }
       public string SubmitterId { get; set; }
       public string SubmitterName { get; set; }
       public string Phone { get; set; }
       public string Fax { get; set; }

       [SubSonicIgnore]
       public string HMOPayerId { get; set; }
       [SubSonicIgnore]
       public string HMOPayerName { get; set; }
       [SubSonicIgnore]
       public string HMOSubmitterId { get; set; }
       [SubSonicIgnore]
       public string ProviderId { get; set; }
       [SubSonicIgnore]
       public string OtherProviderId { get; set; }
       [SubSonicIgnore]
       public string ProviderSubscriberId { get; set; }
       [SubSonicIgnore]
       public string Ub04Locator81cca { get; set; }

       [SubSonicIgnore]
       public string ClearingHouse { get; set; }
       [SubSonicIgnore]
       public string InterchangeReceiverId { get; set; }
       [SubSonicIgnore]
       public bool IsAxxessTheBiller { get; set; }
       [SubSonicIgnore]
       public string ClearingHouseSubmitterId { get; set; }
       [SubSonicIgnore]
       public string BillType { get; set; }
    }
}
