﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Extension;
    using System.Web.Script.Serialization;

   public class ClaimDataLean
    {
       public int Id { get; set; }
       public string ClaimType { get; set; }
       public DateTime Created { get; set; }
       [ScriptIgnore]
       public List<ClaimInfo>  Claims { get; set; }
       public int Count { get { return this.Claims != null ? this.Claims.Count : 0; } }
       public string RAPCount { get { return this.ClaimType.ToUpperCase()=="MAN"?"NA": this.Claims != null && this.Claims.Count > 0 ? this.Claims.Where(c => c.ClaimType == "322" || c.ClaimType.ToUpperCase() == "RAP").ToList().Count.ToString() : "0"; } }
       public string FinalCount { get { return this.ClaimType.ToUpperCase() == "MAN" ? "NA" : this.Claims != null && this.Claims.Count > 0 ? this.Claims.Where(c => c.ClaimType == "329" || c.ClaimType.ToUpperCase() == "FINAL").ToList().Count.ToString() : "0"; } }
    }
}
