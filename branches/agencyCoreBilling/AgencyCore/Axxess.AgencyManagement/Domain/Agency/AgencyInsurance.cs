﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;

    public class AgencyInsurance : EntityBase
    {
        #region Members

        public int Id { get; set; }
        public Guid AgencyId { get; set; }
        public int PayorType { get; set; }
        public int InvoiceType { get; set; }
        public int ChargeGrouping { get; set; }
        public int ChargeType { get; set; }
        public int ParentInsurance { get; set; }
        public string ClearingHouse { get; set; }
        public string InterchangeReceiverId { get; set; }
        public string PayorId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string ProviderId { get; set; }
        public string OtherProviderId { get; set; }
        public string SubmitterId { get; set; }
        public string HealthPlanId { get; set; }
        public string ProviderSubscriberId { get; set; }
        public string Ub04Locator81cca { get; set; }
        public string Charge { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string ContactEmailAddress { get; set; }
        public bool IsAxxessTheBiller { get; set; }
        public bool IsInsuranceRateUsed { get; set; }
        public decimal CurrentBalance { get; set; }
        public int WorkWeekStartDay { get; set; }
        public bool IsVisitAuthorizationRequired { get; set; }
        public bool DefaultFiscalIntermediary { get; set; }
        public bool IsDeprecated { get; set; }
        public string SubmitterName { get; set; }
        public string SubmitterPhone { get; set; }
        public string ClearingHouseSubmitterId { get; set; }
        public string BillType { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string ContactPerson
        {
            get
            {
                return string.Format("{0} {1}", this.ContactPersonFirstName, this.ContactPersonLastName);
            }
        }


        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                if (this.AddressLine1 != null)
                {
                    return string.Format("{0} {1} {2}", this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
                }
                return string.Empty;
            }
        }

        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}, {2} ,{3}  {4}", this.AddressLine1.Trim(), this.AddressLine2.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return string.Format("{0} , {1} ,{2}  {3}", this.AddressLine1.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public List<string> SubmitterPhoneArray { get; set; }
        [SubSonicIgnore]
        public List<string> PhoneNumberArray { get; set; }
        [SubSonicIgnore]
        public List<string> FaxNumberArray { get; set; }

        [SubSonicIgnore]
        public string InvoiceTypeName
        {
            get
            {
                return ((InvoiceType)Enum.ToObject(typeof(InvoiceType), this.InvoiceType)).GetDescription();
            }
        }

        [SubSonicIgnore]
        public string PayerTypeName
        {
            get
            {
                return ((PayerTypes)Enum.ToObject(typeof(PayerTypes), this.PayorType)).GetDescription();
            }
        }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Name), "Insurance Name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PayorId), "Payor Id is required."));
        }

        #endregion

    }
}
