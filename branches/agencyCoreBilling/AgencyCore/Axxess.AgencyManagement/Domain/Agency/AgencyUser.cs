﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using Axxess.Core.Extension;
    public class AgencyUser
    {
        public Guid UserId { get; set; }
        public Guid AgencyId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Suffix { get; set; }
        public bool IsDeprecated { get; set; }
        public string Credentials { get; set; }
        public string CredentialsOther { get; set; }
        public string DisplayName
        {
            get
            {
                var nameBuilder = new StringBuilder();

                if (Credentials.IsEqual("None"))
                {
                    if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                    if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                    if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.Suffix);
                    if (IsDeprecated) nameBuilder.Append(" [deleted]");

                    return nameBuilder.ToString().TrimEnd();
                }

                if (CredentialsOther.IsNotNullOrEmpty())
                {
                    if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                    if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                    if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.Suffix);
                    if (this.CredentialsOther.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.CredentialsOther);
                    if (IsDeprecated) nameBuilder.Append(" [deleted]");
                    return nameBuilder.ToString().TrimEnd();
                }

                if (this.FirstName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.FirstName.ToTitleCase());
                if (this.LastName.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.LastName.ToTitleCase());
                if (this.Suffix.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0} ", this.Suffix);
                if (this.Credentials.IsNotNullOrEmpty()) nameBuilder.AppendFormat("{0}", this.Credentials);
                if (IsDeprecated) nameBuilder.Append(" [deleted]");
                return nameBuilder.ToString().TrimEnd();
            }
        }
    }
}
