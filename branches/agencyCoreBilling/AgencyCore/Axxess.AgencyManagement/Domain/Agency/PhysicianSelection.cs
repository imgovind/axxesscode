﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core.Extension;

    public class PhysicianSelection
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string Credentials { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string ListName { get; set; }
    }
}
