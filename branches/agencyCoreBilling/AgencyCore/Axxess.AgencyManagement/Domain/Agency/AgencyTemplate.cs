﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    public class AgencyTemplate : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public Guid AgencyId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string CreatedDateString { get { return this.Created.ToShortDateString().ToZeroFilled(); } }
        [SubSonicIgnore]
        public string ModifiedDateString { get { return this.Modified.ToShortDateString().ToZeroFilled(); } }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Title), "Title is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Text), "Text is required. <br />"));
        }

        #endregion

    }
}
