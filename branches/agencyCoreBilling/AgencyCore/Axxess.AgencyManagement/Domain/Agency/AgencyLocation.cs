﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using System.Xml.Serialization;

    public class AgencyLocation : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Name { get; set; }
        public string CustomId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string PhoneWork { get; set; }
        public string FaxNumber { get; set; }
        public string Comments { get; set; }
        public bool IsMainOffice { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string MedicareProviderNumber { get; set; }
        public string CBSA { get; set; }
        public string Cost { get; set; }
        public bool IsSubmitterInfoTheSame { get; set; }
        public string SubmitterId { get; set; }
        public string SubmitterName { get; set; }
        public string SubmitterPhone { get; set; }
        public string SubmitterFax { get; set; }
        public string Payor { get; set; }
        public string BranchId { get; set; }
        public string BranchIdOther { get; set; }
        

        #endregion

        #region Domain
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PhoneArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> FaxNumberArray { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> SubmitterPhoneArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> SubmitterFaxArray { get; set; }
       
        [SubSonicIgnore]
        public string PhoneWorkFormatted { get { return PhoneWork.ToPhone(); } }
        [SubSonicIgnore]
        public string FaxNumberFormatted { get { return FaxNumber.ToPhone(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}", this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());

            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}, {2} ,{3}  {4}", this.AddressLine1.Trim(), this.AddressLine2.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return string.Format("{0} , {1} ,{2}  {3}", this.AddressLine1.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                return string.Empty;
            }
        }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Name), "Location Name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.MedicareProviderNumber), "Location Medicare Provider Number is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Location Address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Location City is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Location State is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Location Zipcode is required."));
            if (!this.IsSubmitterInfoTheSame)
            {
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.SubmitterId), "Submitter Id is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.SubmitterName), "Submitter Name is required."));
            }
        }

        #endregion
    }
}
