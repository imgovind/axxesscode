﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;
    using System.ComponentModel.DataAnnotations;

    [XmlRoot("License")]
    public class PhysicainLicense
    {
        #region Members
        public Guid Id { get; set; }
        public string LicenseNumber { get; set; }
        public string State { get; set; }
        public DateTime InitiationDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ExpirationDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        #endregion

        #region Domain

        [XmlIgnore]
        public Guid PhysicainId { get; set; }

        [XmlIgnore]
        public string InitiationDateFormatted { get { return this.InitiationDate.ToString("MM/dd/yyyy"); } }

        [XmlIgnore]
        public string ExpirationDateFormatted { get { return this.ExpirationDate.ToString("MM/dd/yyyy"); } }

        [XmlIgnore]
        public string UserDispalyName { get; set; }

        #endregion
    }
}
