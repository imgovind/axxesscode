﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using System.Xml.Serialization;

    public class Incident : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid UserId { get; set; }
        public Guid PhysicianId { get; set; }
        public int Status { get; set; }
        public Guid EpisodeId { get; set; }
        public string IndividualInvolved { get; set; }
        public string IndividualInvolvedOther { get; set; }
        public string IncidentType { get; set; }
        public string Description { get; set; }
        public string ActionTaken { get; set; }
        public string Orders { get; set; }
        public string MDNotified { get; set; }
        public string FamilyNotified { get; set; }
        public string NewOrdersCreated { get; set; }
        public DateTime IncidentDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string SignatureText { get; set; }
        public DateTime SignatureDate { get; set; }
        public bool IsDeprecated { get; set; }
        public string FollowUp { get; set; }
        #endregion

        #region Domain
        [SubSonicIgnore]
        public List<string> IndividualInvolvedArray { get; set; }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        [SubSonicIgnore]
        public string PhysicianName { get; set; }

        [SubSonicIgnore]
        public string IncidentDateFormatted { get { return IncidentDate.ToShortDateString(); } }

        [SubSonicIgnore]
        public string EpisodeStartDate { get; set; }

        [SubSonicIgnore]
        public string EpisodeEndDate { get; set; }

        [SubSonicIgnore]
        public Agency Agency { get; set; }

        [SubSonicIgnore]
        public Patient Patient { get; set; }

        [SubSonicIgnore]
        public string PrintUrl { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.PatientId.IsEmpty(), "Patient is required. "));
            AddValidationRule(new Validation(() => this.EpisodeId.IsEmpty(), "Episode is required. "));
            AddValidationRule(new Validation(() => this.IncidentType.IsNullOrEmpty(), "Incident type is required. "));
            AddValidationRule(new Validation(() => !IncidentDate.IsValid(), "Incident Date is not valid."));
        }

        #endregion
    }
}
