﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core.Extension;
    using System.Xml.Serialization;

    public class UserVisit
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public Guid UserId { get; set; }
        public string EpisodeNotes { get; set; }
        public string StatusComment { get; set; }
        public string VisitNotes { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public string TaskName { get; set; }
        public string ScheduleDate { get; set; }
        public string VisitDate { get; set; }
        public string VisitRate { get; set; }
        public string Surcharge { get; set; }
        public string StatusName { get; set; }
        public string Status{ get; set; }
        public string PatientName { get; set; }
        public string UserDisplayName { get; set; }
        public bool IsMissedVisit { get; set; }
        public bool IsVisitPaid { get; set; }
        public string AssociatedMileage { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }

        [XmlIgnore]
        public int MinSpent
        {
            get
            {
                var timeIn = DateTime.Now;
                var timeOut = DateTime.Now;
                if (this.TimeOut.IsNotNullOrEmpty() && this.TimeOut.HourToDateTime(ref timeOut) && this.TimeIn.IsNotNullOrEmpty() && this.TimeIn.HourToDateTime(ref timeIn) && timeOut >= timeIn)
                {
                    return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
                }
                return 0;
            }
        }

    }
}
