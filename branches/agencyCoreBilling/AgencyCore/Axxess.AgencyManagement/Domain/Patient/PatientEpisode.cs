﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using System.Web.Mvc;

    public class PatientEpisode
    {
        public PatientEpisode() {
            this.Detail = new EpisodeDetail();
        }

        public Guid Id { get; set; }
        public bool IsActive { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public string Details { get; set; }
        public string Schedule { get; set; }
        public Guid AssessmentId { get; set; }
        public string AssessmentType { get; set; }
        public bool IsRecertCompleted { get; set; }
        public bool IsDischarged { get; set; }
        public bool IsLinkedToDischarge { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StartOfCareDate { get; set; }
        public Guid AdmissionId { get; set; }

        [SubSonicIgnore]
        public bool HasNext { get { return this.NextEpisode != null; } }
        [SubSonicIgnore]
        public PatientEpisode NextEpisode { get; set; }
        [SubSonicIgnore]
        public bool HasPrevious { get { return this.PreviousEpisode != null; } }
        [SubSonicIgnore]
        public PatientEpisode PreviousEpisode { get; set; }
        [SubSonicIgnore]
        public string StartDateFormatted { get { return this.StartDate.ToShortDateString(); } }
        [SubSonicIgnore]
        public string EndDateFormatted { get { return this.EndDate.ToShortDateString(); } }
        [SubSonicIgnore]
        public string StartOfCareDateFormatted { get ; set; }
        [SubSonicIgnore]
        public string DisplayName { get; set; }
        [SubSonicIgnore]
        public EpisodeDetail Detail { get; set; }
        [SubSonicIgnore]
        public string PatientIdNumber { get; set; }
        [SubSonicIgnore]
        public List<SelectListItem> AdmissionDates { get; set; }
    }
}
