﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;

    public class HospitalizationLog
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public Guid UserId { get; set; }
        public string Data { get; set; }
        public int SourceId { get; set; }
        public DateTime ReturnDate { get; set; }
        public DateTime LastHomeVisitDate { get; set; }
        public DateTime HospitalizationDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [SubSonicIgnore]
        public string Source { get { return SourceId.ToEnum<TransferSourceTypes>(TransferSourceTypes.User).GetDescription(); } }
        [SubSonicIgnore]
        public string User { get { return UserEngine.GetName(this.UserId, this.AgencyId); } }
        [SubSonicIgnore]
        public string LastHomeVisitDateFormatted { get { return this.LastHomeVisitDate != DateTime.MinValue ? this.LastHomeVisitDate.ToString("MM/dd/yyyy") : string.Empty; } }
        [SubSonicIgnore]
        public string HospitalizationDateFormatted { get { return this.HospitalizationDate != DateTime.MinValue ? this.HospitalizationDate.ToString("MM/dd/yyyy") : string.Empty; } }
        [SubSonicIgnore]
        public string ReturnDateFormatted { get { return this.ReturnDate != DateTime.MinValue ? this.ReturnDate.ToString("MM/dd/yyyy") : string.Empty; } }
        [SubSonicIgnore]
        public string PrintUrl { get; set; }
        [SubSonicIgnore]
        public Agency Agency { get; set; }
        [SubSonicIgnore]
        public Patient Patient { get; set; }
        [SubSonicIgnore]
        public string EpisodeRange { get; set; }
    }
}
