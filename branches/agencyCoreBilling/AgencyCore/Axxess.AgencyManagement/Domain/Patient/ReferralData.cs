﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;

    using Enums;

    public class ReferralData
    {
        public Guid Id { get; set; }
        public int StatusId { get; set; }
        public string Gender { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string CreatedBy { get; set; }
        public string DateOfBirth { get; set; }
        public string ReferralDate { get; set; }
        public int AdmissionSourceId { get; set; }
        public string AdmissionSource { get; set; }
        public string PatientIdNumber { get; set; }

        public string Status
        {
            get
            {
                return this.StatusId.ToEnum<ReferralStatus>(ReferralStatus.Pending).GetDescription();
            }
        }

        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName, ", ", this.FirstName).ToTitleCase();
            }
        }

    }
}
