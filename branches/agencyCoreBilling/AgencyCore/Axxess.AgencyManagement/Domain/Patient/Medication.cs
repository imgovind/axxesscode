﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    using Axxess.Core.Extension;

    [XmlRoot()]
    [KnownType(typeof(Medication))]
    [XmlInclude(typeof(MedicationType))]
    public class Medication
    {
        [XmlAttribute]
        [ScaffoldColumn(false)]
        public Guid Id { get; set; }

        [XmlAttribute]
        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [XmlAttribute]
        [DisplayName("LS")]
        public bool IsLongStanding { get; set; }

        [XmlAttribute]
        [UIHint("MedicationDosage")]
        [DataType(DataType.Text)]
        public string MedicationDosage { get; set; }

        [XmlAttribute]
        [UIHint("MedicationRoute")]
        [DataType(DataType.Text)]
        public string Route { get; set; }

        [XmlAttribute]
        [UIHint("MedicationFrequency")]
        public string Frequency { get; set; }

        [XmlElement]
        [UIHint("MedicationType"), Required]
        public MedicationType MedicationType { get; set; }

        [XmlAttribute]
        [UIHint("MedicationClassification")]
        public string Classification { get; set; }

        [XmlAttribute]
        [UIHint("MedicationCategory")]
        public string MedicationCategory { get; set; }

        [XmlAttribute]
        [UIHint("DCDate")]
        public DateTime DCDate { get; set; }

        [XmlAttribute]
        public DateTime LastChangedDate { get; set; }

        [XmlIgnore]
        [UIHint("DischargeUrl")]
        public string DischargeUrl
        {
            get;
            set;
        }

        [XmlIgnore]
        public string DCDateFormated
        {
            get
            {
                if (this.DCDate != null && !(this.DCDate <= DateTime.MinValue))
                {
                    return this.DCDate.ToShortDateString();
                }
                else
                {
                    return string.Empty;
                }
            }

        }

        [XmlIgnore]
        public string DCDateSortable
        {
            get
            {
                return DCDate != null && DCDate.ToShortDateString().IsNotNullOrEmpty() ? "<span class='float_right'>" + DCDate.ToShortDateString().ToZeroFilled().Split('/')[2] + "</span><span class='float_right'>/</span><span class='float_right'>" + DCDate.ToShortDateString().ToZeroFilled().Split('/')[0] + "/" + DCDate.ToShortDateString().ToZeroFilled().Split('/')[1] + "</span>" : "";
            }
        }

        [XmlIgnore]
        public string StartDateSortable
        {
            get
            {
                return StartDate != null && StartDate.ToShortDateString().IsNotNullOrEmpty() ? "<span class='float_right'>" + StartDate.ToShortDateString().ToZeroFilled().Split('/')[2] + "</span><span class='float_right'>/</span><span class='float_right'>" + StartDate.ToShortDateString().ToZeroFilled().Split('/')[0] + "/" + StartDate.ToShortDateString().ToZeroFilled().Split('/')[1] + "</span>" : "";
            }
        }

        [XmlIgnore]
        public Guid ProfileId { get; set; }

        [XmlAttribute]
        public string LexiDrugId { get; set; }
    }
}
