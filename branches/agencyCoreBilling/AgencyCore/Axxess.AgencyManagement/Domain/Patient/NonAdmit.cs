﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;

    using Enums;

    public class NonAdmit
    {
        public Guid Id { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string DateOfBirth { get; set; }
        public string NonAdmitDate { get; set; }
        public string MiddleInitial { get; set; }
        public string PatientIdNumber { get; set; }
        public string NonAdmissionReason { get; set; }

        public NonAdmitTypes Type { get; set; }

        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName, ", ", this.FirstName, " ", this.MiddleInitial.IsNotNullOrEmpty() ? this.MiddleInitial + "." : string.Empty).ToTitleCase();
            }
        }

    }
}
