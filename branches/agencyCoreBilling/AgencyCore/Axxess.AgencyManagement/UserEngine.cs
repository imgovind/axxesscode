﻿namespace Axxess.AgencyManagement
{
    using System;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Api;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Repositories;
    using Axxess.Api.Contracts;

    public static class UserEngine
    {
        #region Private Members

        private static readonly CacheAgent cacheAgent = new CacheAgent();
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();

        #endregion

        #region Public Methods

        public static string GetName(Guid userId, Guid agencyId)
        {
            var name = cacheAgent.GetUserDisplayName(userId, agencyId);

            if (name.IsNullOrEmpty() && !userId.IsEmpty())
            {
                var user = dataProvider.UserRepository.Get(userId, agencyId);
                if (user != null)
                {
                    name = user.DisplayName;
                }
            }
            return name;
        }

        public static void Refresh(Guid agencyId)
        {
            cacheAgent.RefreshUsers(agencyId);
        }

        #endregion
    }
}
