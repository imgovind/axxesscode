﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Enums;

    using SubSonic.Repository;

    public class AgencyRepository : IAgencyRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region Agency Methods

        public bool ToggleDelete(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.IsDeprecated = !agency.IsDeprecated;
                agency.IsSuspended = !agency.IsSuspended;
                agency.Modified = DateTime.Now;
                database.Update<Agency>(agency);
                return true;
            }
            return false;
        }

        public List<AgencyUser> GetUserNames()
        {
            var script = @"SELECT agencies.Id as AgencyId, users.Id as UserId, users.FirstName, users.LastName, users.Suffix, users.Credentials, " +
                "users.CredentialsOther, users.IsDeprecated FROM agencies INNER JOIN users ON agencies.Id = users.AgencyId ORDER BY agencies.Id";

            return new FluentCommand<AgencyUser>(script)
                .SetConnection("AgencyManagementConnectionString")
                .SetMap(reader => new AgencyUser
                {
                    UserId = reader.GetGuid("UserId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    LastName = reader.GetStringNullable("LastName"),
                    FirstName = reader.GetStringNullable("FirstName"),
                    Suffix = reader.GetStringNullable("Suffix"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther")
                })
                .AsList();
        }

        public List<AgencyUser> GetUserNames(Guid agencyId)
        {
            var script = @"SELECT agencies.Id as AgencyId, users.Id as UserId, users.FirstName, users.LastName, users.Suffix, users.Credentials, " +
                "users.CredentialsOther, users.IsDeprecated FROM agencies INNER JOIN users ON agencies.Id = users.AgencyId " +
                "Where agencies.Id = @agencyid";

            return new FluentCommand<AgencyUser>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("@agencyid", agencyId)
                .SetMap(reader => new AgencyUser
                {
                    UserId = reader.GetGuid("UserId"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    LastName = reader.GetString("LastName"),
                    FirstName = reader.GetString("FirstName"),
                    Suffix = reader.GetStringNullable("Suffix"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther")
                })
                .AsList();
        }

        public Agency Get(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id && a.IsDeprecated == false);
            if (agency != null)
            {
                agency.MainLocation = GetMainLocation(id);
            }

            return agency;
        }

        public Agency GetWithBranches(Guid agencyId)
        {
            var agency = database.Single<Agency>(a => a.Id == agencyId && a.IsDeprecated == false);
            if (agency != null)
            {
                agency.Branches = database.Find<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false).ToList();
            }

            return agency;
        }

        public Agency GetById(Guid id)
        {
            var agency = database.Single<Agency>(a => a.Id == id);
            if (agency != null)
            {
                agency.MainLocation = GetMainLocation(id);
            }
            return agency;
        }

        public bool Add(Agency agency)
        {
            if (agency != null)
            {
                agency.Id = Guid.NewGuid();
                agency.OasisAuditVendor = (int)OasisAuditVendors.HHG;
                agency.Created = DateTime.Now;
                agency.Modified = DateTime.Now;
                database.Add<Agency>(agency);
                return true;
            }
            return false;
        }

        public bool AddLocation(AgencyLocation agencyLocation)
        {
            if (agencyLocation != null)
            {
                if (agencyLocation.PhoneArray != null && agencyLocation.PhoneArray.Count > 0)
                {
                    agencyLocation.PhoneWork = agencyLocation.PhoneArray.ToArray().PhoneEncode();
                }
                if (agencyLocation.FaxNumberArray != null && agencyLocation.FaxNumberArray.Count > 0)
                {
                    agencyLocation.FaxNumber = agencyLocation.FaxNumberArray.ToArray().PhoneEncode();
                }

                agencyLocation.Created = DateTime.Now;
                agencyLocation.Modified = DateTime.Now;

                database.Add<AgencyLocation>(agencyLocation);
                return true;
            }

            return false;
        }

        public bool UpdateLocation(AgencyLocation agencyLocation)
        {
            var result = false;
            if (agencyLocation != null)
            {
                database.Update<AgencyLocation>(agencyLocation);
                result = true;
            }
            return result;
        }

        public AgencyLocation GetMainLocation(Guid agencyId)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
        }

        public IList<AgencyLocation> GetBranches(Guid agencyId)
        {
            return database.GetPaged<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false, "Created", 0, 20).ToList();
        }

        public AgencyLocation FindLocation(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id );
        }

        public bool EditLocation(AgencyLocation location)
        {
            var result = false;
            var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id && l.IsDeprecated == false);
            if (location != null && existingLocation != null)
            {
                if (location.PhoneArray != null && location.PhoneArray.Count > 0)
                {
                    existingLocation.PhoneWork = location.PhoneArray.ToArray().PhoneEncode();
                }
                if (location.FaxNumberArray != null && location.FaxNumberArray.Count > 0)
                {
                    existingLocation.FaxNumber = location.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingLocation.IsSubmitterInfoTheSame = location.IsSubmitterInfoTheSame;
                if (location.IsSubmitterInfoTheSame)
                {
                    var agency = this.Get(location.AgencyId);
                    if (agency != null)
                    {
                        
                        existingLocation.SubmitterId = agency.SubmitterId;
                        existingLocation.SubmitterName = agency.SubmitterName;
                        existingLocation.Payor = agency.Payor;
                        existingLocation.SubmitterPhone = agency.SubmitterPhone;
                        existingLocation.SubmitterFax = agency.SubmitterFax;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (location.SubmitterPhoneArray != null && location.SubmitterPhoneArray.Count > 0)
                    {
                        existingLocation.SubmitterPhone = location.SubmitterPhoneArray.ToArray().PhoneEncode();
                    }

                    if (location.SubmitterFaxArray != null && location.SubmitterFaxArray.Count > 0)
                    {
                        existingLocation.SubmitterFax = location.SubmitterFaxArray.ToArray().PhoneEncode();
                    }
                }
                existingLocation.BranchId = location.BranchId;
                existingLocation.BranchIdOther = location.BranchIdOther;
                existingLocation.Name = location.Name;
                existingLocation.CustomId = location.CustomId;
                existingLocation.MedicareProviderNumber = location.MedicareProviderNumber;
                existingLocation.AddressLine1 = location.AddressLine1;
                existingLocation.AddressLine2 = location.AddressLine2;
                existingLocation.AddressCity = location.AddressCity;
                existingLocation.AddressStateCode = location.AddressStateCode;
                existingLocation.AddressZipCode = location.AddressZipCode;
                existingLocation.Comments = location.Comments;
                existingLocation.Modified = DateTime.Now;
                database.Update<AgencyLocation>(existingLocation);
                result = true;
            }
            return result;
        }

        public bool EditBranchCost(AgencyLocation location)
        {
            var result = false;
            var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
            if (location != null && existingLocation != null)
            {
                existingLocation.Cost = location.Cost;
                existingLocation.Modified = DateTime.Now;
                database.Update<AgencyLocation>(existingLocation);
                result = true;
            }
            return result;
        }

        public IEnumerable<Agency> All()
        {
            return database.All<Agency>().AsEnumerable<Agency>();
        }

        public IList<Agency> AllAgencies()
        {
            var agencies = database.All<Agency>().ToList();
            if (agencies != null && agencies.Count > 0)
            {
                agencies.ForEach(a =>
                {
                    a.MainLocation = GetMainLocation(a.Id);
                });
            }
            return agencies.OrderBy(a => a.Name).ToList();
        }

        public bool Update(Agency agency)
        {
            bool result = false;
            if (agency != null)
            {
                database.Update<Agency>(agency);
                result = true;
            }
            return result;
        }

        #endregion

        #region Contact Methods

        public bool AddContact(AgencyContact contact)
        {
            var result = false;
            if (contact != null)
            {
                
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    contact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    contact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                if (contact.FaxNumberArray.Count > 0)
                {
                    contact.FaxNumber = contact.FaxNumberArray.ToArray().PhoneEncode();
                }
                contact.Created = DateTime.Now;
                contact.Modified = DateTime.Now;

                database.Add<AgencyContact>(contact);
                result = true;
            }
            return result;
        }

        public IList<AgencyContact> GetContacts(Guid agencyId)
        {
            return database.Find<AgencyContact>(c => c.AgencyId == agencyId && c.IsDeprecated == false).ToList();
        }

        public AgencyContact FindContact(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyContact>(c => c.Id == Id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public bool EditContact(AgencyContact contact)
        {
            var result = false;
            var existingContact = database.Single<AgencyContact>(c => c.Id == contact.Id && c.AgencyId == contact.AgencyId && c.IsDeprecated == false);
            if (contact != null)
            {
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    existingContact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    existingContact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                if (contact.FaxNumberArray.Count > 0)
                {
                    existingContact.FaxNumber = contact.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingContact.FirstName = contact.FirstName;
                existingContact.LastName = contact.LastName;
                existingContact.CompanyName = contact.CompanyName;
                existingContact.AddressLine1 = contact.AddressLine1;
                existingContact.AddressLine2 = contact.AddressLine2;
                existingContact.AddressCity = contact.AddressCity;
                existingContact.AddressStateCode = contact.AddressStateCode;
                existingContact.AddressZipCode = contact.AddressZipCode;
                existingContact.EmailAddress = contact.EmailAddress;
                existingContact.ContactType = contact.ContactType;
                existingContact.ContactTypeOther = contact.ContactTypeOther;
                existingContact.Modified = DateTime.Now;
                database.Update<AgencyContact>(existingContact);
                result = true;
            }
            return result;
        }

        public bool DeleteContact(Guid agencyId, Guid id)
        {
            var contact = database.Single<AgencyContact>(c => c.Id == id && c.AgencyId == agencyId);
            if (contact != null)
            {
                contact.IsDeprecated = true;
                contact.Modified = DateTime.Now;
                database.Update<AgencyContact>(contact);
                return true;
            }
            return false;
        }

        #endregion

        #region Insurance

        public bool AddInsurance(AgencyInsurance insurance)
        {
            var result = false;
            if (insurance != null)
            {
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    insurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.SubmitterPhoneArray.Count > 0)
                {
                    insurance.SubmitterPhone = insurance.SubmitterPhoneArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    insurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                insurance.Created = DateTime.Now;
                insurance.Modified = DateTime.Now;

                database.Add<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public AgencyInsurance GetInsurance(int insuranceId, Guid agencyId)
        {
            return database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == insuranceId );
        }

        public IList<AgencyInsurance> GetInsurances(Guid agencyId)
        {
            return database.Find<AgencyInsurance>(i => i.AgencyId == agencyId && i.IsDeprecated == false).ToList();
        }

        public IList<InsuranceLean> GetLeanInsurances(Guid agencyId)
        {
            var script = @"SELECT agencyinsurances.Id, agencyinsurances.PayorType, agencyinsurances.Name, agencyinsurances.InvoiceType, agencyinsurances.ContactPersonFirstName, agencyinsurances.ContactPersonLastName, " +
               "agencyinsurances.PhoneNumber, agencyinsurances.PayorId " +
               "FROM agencyinsurances  WHERE agencyinsurances.AgencyId = @agencyid AND agencyinsurances.IsDeprecated = 0";

            return new FluentCommand<InsuranceLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new InsuranceLean
                {
                    Id = reader.GetInt("Id"),
                    Name = reader.GetStringNullable("Name"),
                    PayorType = reader.GetInt("PayorType"),
                    InvoiceType = reader.GetInt("InvoiceType"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName").ToUpperCase(),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName").ToUpperCase(),
                    PhoneNumber = reader.GetStringNullable("PhoneNumber").ToUpperCase(),
                    PayorId = reader.GetStringNullable("PayorId"),
                    IsTradtionalMedicare=false
                })
                .AsList();
        }

        public AgencyInsurance FindInsurance(Guid agencyId, int Id)
        {
            return database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == Id && i.IsDeprecated == false);
        }

        public bool EditInsurance(AgencyInsurance insurance)
        {
            var result = false;
            var existingInsurance = database.Single<AgencyInsurance>(I =>I.AgencyId == insurance.AgencyId && I.Id == insurance.Id );
            if (insurance != null && existingInsurance!=null)
            {
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    existingInsurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.SubmitterPhoneArray.Count > 0)
                {
                    insurance.SubmitterPhone = insurance.SubmitterPhoneArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    existingInsurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingInsurance.PayorType = insurance.PayorType;
                existingInsurance.InvoiceType = insurance.InvoiceType;
                existingInsurance.ChargeGrouping = insurance.ChargeGrouping;
                existingInsurance.ChargeType = insurance.ChargeType;
                existingInsurance.ParentInsurance = insurance.ParentInsurance;
                existingInsurance.Name = insurance.Name;
                existingInsurance.AddressLine1 = insurance.AddressLine1;
                existingInsurance.AddressLine2 = insurance.AddressLine2;
                existingInsurance.AddressCity = insurance.AddressCity;
                existingInsurance.AddressStateCode = insurance.AddressStateCode;
                existingInsurance.AddressZipCode = insurance.AddressZipCode;
                existingInsurance.ProviderId = insurance.ProviderId;
                existingInsurance.ProviderSubscriberId = insurance.ProviderSubscriberId;
                existingInsurance.OtherProviderId = insurance.OtherProviderId;
                existingInsurance.Ub04Locator81cca = insurance.Ub04Locator81cca;
                existingInsurance.PayorId = insurance.PayorId;
                existingInsurance.ContactPersonFirstName = insurance.ContactPersonFirstName;
                existingInsurance.ContactPersonLastName = insurance.ContactPersonLastName;
                existingInsurance.ContactEmailAddress = insurance.ContactEmailAddress;
                existingInsurance.CurrentBalance = insurance.CurrentBalance;
                existingInsurance.WorkWeekStartDay = insurance.WorkWeekStartDay;
                existingInsurance.IsVisitAuthorizationRequired = insurance.IsVisitAuthorizationRequired;
                existingInsurance.Charge = insurance.Charge;
                existingInsurance.SubmitterId = insurance.SubmitterId;
                existingInsurance.BillType = insurance.BillType;
                existingInsurance.SubmitterPhone = insurance.SubmitterPhone;
                existingInsurance.IsAxxessTheBiller = insurance.IsAxxessTheBiller;
                existingInsurance.ClearingHouse = insurance.ClearingHouse;
                existingInsurance.InterchangeReceiverId = insurance.InterchangeReceiverId;
                existingInsurance.ClearingHouseSubmitterId = insurance.ClearingHouseSubmitterId;
                existingInsurance.SubmitterName = insurance.SubmitterName;
                existingInsurance.SubmitterPhone = insurance.SubmitterPhone;
                existingInsurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(existingInsurance);
                result = true;
            }
            return result;
        }

        public bool DeleteInsurance(Guid agencyId, int Id)
        {
            var insurance = database.Single<AgencyInsurance>(i =>i.AgencyId == agencyId && i.Id == Id  );
            if (insurance != null)
            {
                insurance.IsDeprecated = true;
                insurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(insurance);
                return true;
            }
            return false;
        }

        public bool IsMedicareHMO(Guid agencyId, int Id)
        {
            var result = false;
            var insurance = database.Single<AgencyInsurance>(i =>i.AgencyId == agencyId && i.Id == Id );
            if (insurance != null)
            {
                result= (insurance.PayorType == 2);
            }
            return result;
        }

        #endregion

        #region Hospital

        public bool AddHospital(AgencyHospital hospital)
        {
            var result = false;
            if (hospital != null)
            {
               
                if (hospital.PhoneArray.Count > 0)
                {
                    hospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    hospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                hospital.Created = DateTime.Now;
                hospital.Modified = DateTime.Now;

                database.Add<AgencyHospital>(hospital);
                result = true;
            }
            return result;
        }

        public IList<AgencyHospital> GetHospitals(Guid agencyId)
        {
            return database.Find<AgencyHospital>(h => h.AgencyId == agencyId && h.IsDeprecated == false).ToList();
        }

        public AgencyHospital FindHospital(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId && h.IsDeprecated == false);
        }

        public bool EditHospital(AgencyHospital hospital)
        {
            var result = false;
            var existingHospital = database.Single<AgencyHospital>(h => h.Id == hospital.Id && h.AgencyId == hospital.AgencyId);
            if (hospital != null)
            {

                if (hospital.PhoneArray.Count > 0)
                {
                    existingHospital.Phone = hospital.PhoneArray.ToArray().PhoneEncode();
                }
                if (hospital.FaxNumberArray.Count > 0)
                {
                    existingHospital.FaxNumber = hospital.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingHospital.Name = hospital.Name;
                existingHospital.ContactPersonFirstName = hospital.ContactPersonFirstName;
                existingHospital.ContactPersonLastName = hospital.ContactPersonLastName;
                existingHospital.AddressLine1 = hospital.AddressLine1;
                existingHospital.AddressLine2 = hospital.AddressLine2;
                existingHospital.AddressCity = hospital.AddressCity;
                existingHospital.AddressStateCode = hospital.AddressStateCode;
                existingHospital.AddressZipCode = hospital.AddressZipCode;
                existingHospital.EmailAddress = hospital.EmailAddress;
                existingHospital.Comment = hospital.Comment;
                existingHospital.Modified = DateTime.Now;

                database.Update<AgencyHospital>(existingHospital);
                result = true;
            }
            return result;

        }

        public bool DeleteHospital(Guid agencyId, Guid Id)
        {
            var hospital = database.Single<AgencyHospital>(h => h.Id == Id && h.AgencyId == agencyId);
            if (hospital != null)
            {
                hospital.IsDeprecated = true;
                hospital.Modified = DateTime.Now;
                database.Update<AgencyHospital>(hospital);
                return true;
            }
            return false;
        }

        #endregion

        #region Infection Report

        public bool AddInfection(Infection infection)
        {
            var result = false;
            if (infection != null)
            {
                if (infection.InfectionTypeArray != null && infection.InfectionTypeArray.Count > 0)
                {
                    infection.InfectionType = infection.InfectionTypeArray.ToArray().AddColons();
                }
                infection.Created = DateTime.Now;
                infection.Modified = DateTime.Now;

                database.Add<Infection>(infection);
                result = true;
            }
            return result;
        }

        public IList<Infection> GetInfections(Guid agencyId)
        {
            return database.Find<Infection>(i => i.AgencyId == agencyId).ToList();
        }

        public bool MarkInfectionsAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (infection != null)
            {
                infection.IsDeprecated = isDeprecated;
                infection.Modified = DateTime.Now;
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        public bool ReassignInfectionsUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (infection != null)
            {
                try
                {
                    infection.UserId = employeeId;
                    infection.Modified = DateTime.Now;
                    database.Update<Infection>(infection);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public Infection GetInfectionReport(Guid agencyId, Guid infectionId)
        {
            return database.Single<Infection>(i => i.AgencyId == agencyId && i.Id == infectionId);
        }

        public bool UpdateInfection(Infection infection)
        {
            var result = false;
            var existingInfection = GetInfectionReport(infection.AgencyId, infection.Id);
            if (existingInfection != null)
            {
                existingInfection.Orders = infection.Orders;
                existingInfection.Treatment = infection.Treatment;
                existingInfection.MDNotified = infection.MDNotified;
                existingInfection.NewOrdersCreated = infection.NewOrdersCreated;
                existingInfection.TreatmentPrescribed = infection.TreatmentPrescribed;

                existingInfection.PhysicianId = infection.PhysicianId;
                existingInfection.InfectionDate = infection.InfectionDate;
                existingInfection.Status = infection.Status;
                if (infection.InfectionTypeArray != null && infection.InfectionTypeArray.Count > 0)
                {
                    existingInfection.InfectionType = infection.InfectionTypeArray.ToArray().AddColons();
                }
                existingInfection.InfectionTypeOther = infection.InfectionTypeOther;
                existingInfection.FollowUp = infection.FollowUp;
                existingInfection.SignatureDate = infection.SignatureDate;
                existingInfection.SignatureText = infection.SignatureText;
                existingInfection.Modified = DateTime.Now;

                database.Update<Infection>(existingInfection);
                result = true;
            }
            return result;
        }

        public bool UpdateInfectionModal(Infection infection)
        {
            var result = false;
            if (infection != null)
            {
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        #endregion

        #region Incident Report

        public bool AddIncident(Incident incident)
        {
            var result = false;
            if (incident != null)
            {
                if (incident.IndividualInvolvedArray != null && incident.IndividualInvolvedArray.Count > 0)
                {
                    incident.IndividualInvolved = incident.IndividualInvolvedArray.ToArray().AddColons();
                }
                incident.Created = DateTime.Now;
                incident.Modified = DateTime.Now;

                database.Add<Incident>(incident);
                result = true;
            }
            return result;
        }

        public IList<Incident> GetIncidents(Guid agencyId)
        {
            return database.Find<Incident>(i => i.AgencyId == agencyId).ToList();
        }

        public bool MarkIncidentAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (incident != null)
            {
                incident.IsDeprecated = isDeprecated;
                incident.Modified = DateTime.Now;
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool ReassignIncidentUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (incident != null)
            {
                try
                {
                    incident.UserId = employeeId;
                    incident.Modified = DateTime.Now;
                    database.Update<Incident>(incident);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public Incident GetIncidentReport(Guid agencyId, Guid incidentId)
        {
            return database.Single<Incident>(i => i.AgencyId == agencyId && i.Id == incidentId);
        }

        public bool UpdateIncident(Incident incident)
        {
            var result = false;
            var existingIncident = GetIncidentReport(incident.AgencyId, incident.Id);
            if (existingIncident != null)
            {
                existingIncident.Orders = incident.Orders;
                existingIncident.Description = incident.Description;
                existingIncident.ActionTaken = incident.ActionTaken;

                existingIncident.MDNotified = incident.MDNotified;
                existingIncident.NewOrdersCreated = incident.NewOrdersCreated;
                existingIncident.FamilyNotified = incident.FamilyNotified;

                existingIncident.PhysicianId = incident.PhysicianId;
                existingIncident.IncidentDate = incident.IncidentDate;
                existingIncident.IndividualInvolvedOther = incident.IndividualInvolvedOther;
                existingIncident.Status = incident.Status;

                if (incident.IndividualInvolvedArray != null && incident.IndividualInvolvedArray.Count > 0)
                {
                    existingIncident.IndividualInvolved = incident.IndividualInvolvedArray.ToArray().AddColons();
                }
                existingIncident.IndividualInvolvedOther = incident.IndividualInvolvedOther;
                existingIncident.FollowUp = incident.FollowUp;
                existingIncident.SignatureDate = incident.SignatureDate;
                existingIncident.SignatureText = incident.SignatureText;
                existingIncident.Modified = DateTime.Now;

                database.Update<Incident>(existingIncident);
                result = true;
            }
            return result;
        }

        public bool UpdateIncidentModal(Incident incident)
        {
            var result = false;
            if (incident != null)
            {
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        #endregion

        #region Template Methods

        public bool AddTemplate(AgencyTemplate template)
        {
            var result = false;
            if (template != null)
            {
               
                template.Created = DateTime.Now;
                template.Modified = DateTime.Now;

                database.Add<AgencyTemplate>(template);
                result = true;
            }
            return result;
        }

        public IList<AgencyTemplate> GetTemplates(Guid agencyId)
        {
            return database.Find<AgencyTemplate>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Title).ToList();
        }

        public AgencyTemplate GetTemplate(Guid agencyId, Guid id)
        {
            return database.Single<AgencyTemplate>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public bool UpdateTemplate(AgencyTemplate template)
        {
            var result = false;
            if (template != null)
            {
                template.Modified = DateTime.Now;
                database.Update<AgencyTemplate>(template);
                result = true;
            }
            return result;
        }

        public bool DeleteTemplate(Guid agencyId, Guid id)
        {
            var template = database.Single<AgencyTemplate>(c => c.AgencyId == agencyId && c.Id == id );
            if (template != null)
            {
                template.IsDeprecated = true;
                template.Modified = DateTime.Now;
                database.Update<AgencyTemplate>(template);
                return true;
            }
            return false;
        }

        #endregion

    }
}
