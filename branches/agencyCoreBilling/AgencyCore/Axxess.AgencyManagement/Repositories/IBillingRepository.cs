﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;

    public interface IBillingRepository
    {
        bool AddRap(Rap rap);
        Rap GetRap(Guid agencyId, Guid claimId);
        Final GetFinal(Guid agencyId, Guid claimId);
        Rap GetRap(Guid agencyId, Guid patientId, Guid episodeId);
        Final GetFinal(Guid agencyId, Guid patientId, Guid episodeId);
        bool AddFinal(Final final);
        bool UpdateFinal(Final final);
        bool UpdateRap(Rap rap);
        bool UpdateFinalStatus(Final final);
        bool UpdateRapStatus(Rap rap);
        bool VerifyRap(Guid agencyId, Rap rap);
        bool VerifyInfo(Guid agencyId, Final final);
       
        List<RapLean> GetRapsByIds(Guid agencyId, List<Guid> rapIds);
        List<FinalLean> GetFinalsByIds(Guid agencyId, List<Guid> finalIds);

        List<Rap> GetRapsToGenerateByIds(Guid agencyId, List<Guid> rapIds);
        List<Final> GetFinalsToGenerateByIds(Guid agencyId, List<Guid> finalIds);


        List<ClaimHistoryLean> GetRapsHistory(Guid agencyId, Guid patientId, int insuranceId);
        List<ClaimHistoryLean> GetFinalsHistory(Guid agencyId, Guid patientId, int insuranceId);
       
        List<TypeOfBill> GetRapsByStatus(Guid agencyId, int status);
        IList<TypeOfBill> GetOutstandingRaps(Guid agencyId);
        List<TypeOfBill> GetFinalsByStatus(Guid agencyId, int status);
        IList<TypeOfBill> GetOutstandingFinals(Guid agencyId);

        IList<ClaimBill> GetOutstandingRapClaims(Guid agencyId, Guid branchId, int insuranceId);
        IList<ClaimBill> GetOutstandingFinalClaims(Guid agencyId, Guid branchId, int insuranceId);

        IList<ClaimBill> GetPotentialCliamAutoCancels(Guid agencyId, Guid branchId);
        IList<ClaimBill> GetPPSRapClaims(Guid agencyId, Guid branchId);
        IList<ClaimBill> GetPPSFinalClaims(Guid agencyId, Guid branchId);

        bool DeleteRap(Guid agencyId, Guid patientId, Guid episodeId);
        bool DeleteFinal(Guid agencyId, Guid patientId, Guid episodeId);
        long AddClaimData(ClaimData claimData);
        bool UpdateClaimData(ClaimData claimData);
        bool DeleteClaimData(Guid agencyId, long claimId);
        List<Guid> GetClaimIds(Guid agencyId, Guid patientId, string type);
        ClaimData GetClaimData(Guid agencyId, int ansiId);
        List<ClaimData> GetClaimData(Guid agencyId, string claimType, DateTime batchDate);
        AxxessSubmitterInfo SubmitterInfo(int payerId);
        void MarkRapsAsSubmitted(Guid agencyId, List<Rap> raps);
        void MarkFinalsAsSubmitted(Guid agencyId, List<Final> finalss);
        bool AddRapSnapShots(List<Rap> raps, long batchId);
        bool AddFinaSnapShots(List<Final> finals, long batchId);
        bool DeleteRapSnapShots(long batchId);
        bool DeleteFinaSnapShots(long batchId);
        List<RapSnapShot> GetRapSnapShots(Guid agencyId, Guid Id);
        List<FinalSnapShot> GetFinalSnapShots(Guid agencyId, Guid Id);
        RapSnapShot GetRapSnapShot(Guid agencyId, Guid Id, long batchId);
        RapSnapShot GetLastRapSnapShot(Guid agencyId, Guid Id);
        long GetLastRapSnapShotBatchId(Guid agencyId, Guid Id);
        FinalSnapShot GetFinalSnapShot(Guid agencyId, Guid Id, long batchId);
        FinalSnapShot GetLastFinalSnapShot(Guid agencyId, Guid Id);
        long GetLastFinalSnapShotBatchId(Guid agencyId, Guid Id);
        bool UpdateRapSnapShots(RapSnapShot rapSnapShot);
        bool UpdateFinalSnapShots(FinalSnapShot finalSnapShot);
        bool AddManagedClaim(ManagedClaim managedClaim);
        void MarkManagedClaimsAsSubmitted(Guid agencyId, List<ManagedClaim> managedClaims);
        IList<ManagedClaimLean> GetManagedClaimsPerPatient(Guid agencyId, Guid patientId, int insuranceId);
        IList<ManagedBill> GetManagedClaims(Guid agencyId, Guid branchId, int insuranceId, int status);
        List<ManagedBill> GetManagedClaimByIds(Guid agencyId, Guid branchId, int insuranceId, List<Guid> claimIds);
        List<ManagedClaim> GetManagedClaimsToGenerateByIds(Guid agencyId, List<Guid> managedClaimIds);
        ManagedClaim GetManagedClaim(Guid agencyId, Guid Id);
        ManagedClaim GetManagedClaim(Guid agencyId, Guid patientId, Guid Id);
        bool ManagedVerifyInfo(ManagedClaim claim, Guid agencyId);
        bool UpdateManagedClaim(ManagedClaim managedClaim);
        bool UpdateManagedClaimForVisitVerify(ManagedClaim managedClaim);
        bool UpdateManagedClaimForSupplyVerify(ManagedClaim managedClaim);
        bool DeleteManagedClaim(Guid agencyId, Guid patientId, Guid Id);
        IList<RemittanceLean> GetRemittances(Guid agencyId, DateTime startDate, DateTime endDate);
        Remittance GetRemittance(Guid agencyId, Guid Id);
        bool DeleteRemittance(Guid agencyId, Guid Id);

        List<ClaimLean> GetFinalClaims(Guid agencyId, Guid branchId,  int status, DateTime startDate, DateTime endDate);
        List<ClaimLean> GetRapClaims(Guid agencyId, Guid branchId,  int status, DateTime startDate, DateTime endDate);

        List<ClaimLean> GetFinalClaimsBySubmissionDate(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);
        List<ClaimLean> GetRapClaimsBySubmissionDate(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);

        List<ClaimLean> GetAccountsReceivableRaps(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        List<ClaimLean> GetAccountsReceivableFinals(Guid agencyId, Guid branchId, int insurance, DateTime startDate, DateTime endDate);
        List<PendingClaimLean> PendingClaimRaps(Guid agencyId, Guid branchId, int insurance);
        List<PendingClaimLean> PendingClaimFinals(Guid agencyId, Guid branchId, int insurance);

        bool AddRemitQueue(RemitQueue remitQueue);
        List<ClaimDataLean> ClaimDatas(Guid agencyId, DateTime startDate, DateTime endDate, string claimType);
        List<ClaimInfoDetail> GetManagedClaimInfoDetails(Guid agencyId, List<Guid> claimIds);
        List<ClaimInfoDetail> GetMedicareClaimInfoDetails(Guid agencyId, List<Guid> claimIds, string type);
        List<ClaimEpisode> GetEpisodeNeedsClaim(Guid agencyId, Guid patientId, string type);
    }
}
