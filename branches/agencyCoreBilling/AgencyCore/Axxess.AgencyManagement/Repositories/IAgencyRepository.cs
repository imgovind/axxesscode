﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IAgencyRepository
    {
        bool ToggleDelete(Guid id);
        Agency Get(Guid id);
        Agency GetWithBranches(Guid agencyId);
        Agency GetById(Guid id);
        IEnumerable<Agency> All();
        IList<Agency> AllAgencies();
        bool Add(Agency agency);
        bool Update(Agency agency);

        List<AgencyUser> GetUserNames();
        List<AgencyUser> GetUserNames(Guid agencyId);

        bool AddLocation(AgencyLocation agencyLocation);
        AgencyLocation GetMainLocation(Guid agencyId);
        IList<AgencyLocation> GetBranches(Guid agencyId);
        AgencyLocation FindLocation(Guid agencyId, Guid Id);
        bool EditLocation(AgencyLocation location);
        bool UpdateLocation(AgencyLocation location);
        bool EditBranchCost(AgencyLocation location);

        bool AddContact(AgencyContact contact);
        IList<AgencyContact> GetContacts(Guid agencyId);
        AgencyContact FindContact(Guid agencyId, Guid Id);
        bool EditContact(AgencyContact contact);
        bool DeleteContact(Guid agencyId, Guid id);

        bool AddHospital(AgencyHospital hospital);
        IList<AgencyHospital> GetHospitals(Guid agencyId);
        AgencyHospital FindHospital(Guid agencyId, Guid Id);
        bool EditHospital(AgencyHospital hospital);
        bool DeleteHospital(Guid agencyId, Guid Id);
        
        bool AddInsurance(AgencyInsurance insurance);
        IList<AgencyInsurance> GetInsurances(Guid agencyId);
        IList<InsuranceLean> GetLeanInsurances(Guid agencyId);
        AgencyInsurance GetInsurance(int insuranceId, Guid agencyId);
        AgencyInsurance FindInsurance(Guid agencyId, int Id);
        bool EditInsurance(AgencyInsurance insurance);
        bool DeleteInsurance(Guid agencyId, int Id);
        bool IsMedicareHMO(Guid agencyId, int Id);

        bool AddTemplate(AgencyTemplate template);
        IList<AgencyTemplate> GetTemplates(Guid agencyId);
        AgencyTemplate GetTemplate(Guid agencyId, Guid id);
        bool UpdateTemplate(AgencyTemplate template);
        bool DeleteTemplate(Guid agencyId, Guid id);

        bool AddInfection(Infection infection);
        bool UpdateInfection(Infection infection);
        bool UpdateInfectionModal(Infection infection);
        IList<Infection> GetInfections(Guid agencyId);
        bool MarkInfectionsAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
        bool ReassignInfectionsUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        Infection GetInfectionReport(Guid agencyId, Guid id);

        bool AddIncident(Incident incident);
        bool UpdateIncident(Incident incident);
        bool UpdateIncidentModal(Incident incident);
        IList<Incident> GetIncidents(Guid agencyId);
        bool MarkIncidentAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
        bool ReassignIncidentUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        Incident GetIncidentReport(Guid agencyId, Guid id);

      
    }
}
