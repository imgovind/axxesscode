﻿namespace Axxess.AgencyManagement.Enums
{
    using System;
    using Axxess.Core.Infrastructure;

    [Flags]
    public enum Permissions : ulong
    {
        None = 0,
        [Permission("Add/Edit/Delete Users", "Manage Users", "Administration")]
        ManageUsers = 1,
        [Permission("Delete Any Task", "Delete scheduled tasks", "Administration")]
        DeleteTasks = 1 << 1,
        [Permission("Manage Payroll", "Run Payroll Reports", "Administration")]
        ManagePayroll = 1 << 2,
        [Permission("Access Billing Center", "Access Billing Center", "Billing")]
        AccessBillingCenter = 1 << 3,
        [Permission("Receive Eligibility Report", "Receive Eligibility Report", "Billing")]
        ReceiveEligibilityReport = 1 << 4,
        [Permission("Manage Claims", "Manage Claims (RAP & Final)", "Billing")]
        ManageClaims = 1 << 5,
        [Permission("Generate Claim files", "Generate Claim files", "Billing")]
        GenerateClaimFiles = 1 << 6,
        [Permission("View HHRG Calculations", "View HHRG Calculations", "Billing")]
        ViewHHRGCalculations = 1 << 7,
        [Permission("Access Quality Assurance (QA) Center", "Access Quality Assurance (QA) Center", "QA")]
        AccessCaseManagement = 1 << 8,
        [Permission("Re-open Previously Approved Documents", "Re-open Previously Approved Documents", "QA")]
        ReOpenPreviousDocuments = 1 << 9,
        [Permission("Schedule PRN", "Schedule PRN", "QA")]
        SchedulePRN = 1 << 10,
        [Permission("Add/Edit/Delete Patients", "Manage Patients", "Clerical")]
        ManagePatients = 1 << 11,
        [Permission("Add/Edit/Delete Referrals", "Manage Referral", "Clerical")]
        ManageReferrals = 1 << 12,
        [Permission("View Existing Referrals", "View Existing Referrals", "Clerical")]
        ViewExisitingReferrals = 1 << 13,
        [Permission("Add/Edit/Delete Hospital Information", "Manage Hospital Information", "Clerical")]
        ManageHospital = 1 << 14,
        [Permission("Add/Edit/Delete Insurance Information", "Manage Insurance Information", "Clerical")]
        ManageInsurance = 1 << 15,
        [Permission("Add/Edit/Delete Contact Information", "Manage Contact Information", "Clerical")]
        ManageContact = 1 << 16,
        [Permission("View Lists", "View lists of  patients, referrals, contacts, users, etc", "Clerical")]
        ViewLists = 1 << 17,
        [Permission("Access Patient Charts", "Access Patient Charts", "None")]
        AccessPatientCenter = 1 << 18,
        [Permission("Access Print Queue", "Access Print Queue", "Clerical")]
        PrintClinicalDocuments = 1 << 19,
        [Permission("Export List to Excel", "Export List to Excel", "Clerical")]
        ExportListToExcel = 1 << 20,
        [Permission("Edit Task Details", "Edit Task Details", "Clerical")]
        EditTaskDetails = 1 << 21,
        [Permission("View Completed Documents/Tasks", "View Completed Documents/Tasks", "Clerical")]
        ViewCompletedDocumentsTasks = 1 << 22,
        [Permission("Create/Edit Physician Orders", "Create/Edit Physician Orders", "Clinical")]
        ManagePhysicianOrders = 1 << 23,
        [Permission("Order Management Center", "Order Management Center", "Clerical")]
        AccessOrderManagementCenter = 1 << 24,
        [Permission("Manage Medication Profile/History/Snapshot", "Manage Medication Profile/History/Snapshot", "Clinical")]
        ViewMedicationProfile = 1 << 25,
        [Permission("Dashboard", "Dashboard", "General")]
        Dashboard = 1 << 26,
        [Permission("Messaging", "Messaging", "General")]
        Messaging = 1 << 27,
        [Permission("Access Personal Profile", "Access Personal Profile", "General")]
        AccessPersonalProfile = 1 << 28,
        [Permission("View Scheduled Tasks", "View Scheduled Tasks", "General")]
        ViewScheduledTasks = 1 << 29,
        [Permission("View Patient Profile", "View Patient Profile", "General")]
        ViewPatientProfile = 1 << 30,
        [Permission("View Exported OASIS", "View Exported OASIS", "OASIS")]
        ViewExportedOasis = (ulong)1 << 31,
        [Permission("Create OASIS Export file", "Create OASIS Export file", "OASIS")]
        CreateOasisSubmitFile = (ulong)1 << 32,
        [Permission("Approve Completed Documents", "Approve Completed Documents", "QA")]
        ApproveCompletedDocuments = (ulong)1 << 33,
        [Permission("Bypass Case Management", "Bypass Case Management", "Clinical")]
        BypassCaseManagement = (ulong)1 << 34,
        [Permission("Reopen Documents", "Reopen Documents", "QA")]
        ReopenDocuments = (ulong)1 << 35,
        [Permission("Access Reports Center", "Access Reports Center", "Reporting")]
        AccessReports = (ulong)1 << 36,
        [Permission("Access Schedule Center", "Access Schedule Center", "None")]
        AccessScheduleCenter = (ulong)1 << 37,
        [Permission("Schedule Visits/Activites", "Schedule Visits/Activites", "Schedule Management")]
        ScheduleVisits = (ulong)1 << 38,
        [Permission("Edit Episode Information", "Edit Episode Information", "Schedule Management")]
        EditEpisode = (ulong)1 << 39,
        [Permission("Add/Edit/Delete Physician Information", "Manage Physician Information", "Clerical")]
        ManagePhysicians = (ulong)1 << 40,
        [Permission("Create Incident/Accident & Infection Report", "Create Incident/Accident & Infection Report", "Clerical")]
        ManageIncidentAccidentInfectionReport = (ulong)1 << 41,
        [Permission("View Previous Notes", "View Previous Notes", "Clinical")]
        ViewPreviousNotes = (ulong)1 << 42
    }
}
