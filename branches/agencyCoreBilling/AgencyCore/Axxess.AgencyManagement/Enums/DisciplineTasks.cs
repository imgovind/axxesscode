﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    using Axxess.Core.Infrastructure;

    public enum DisciplineTasks
    {
        [CustomDescription("No Discipline", "No Discipline", "None", "None","None")]
        NoDiscipline = 0,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes","SN")]
        SkilledNurseVisit = 1,
        [CustomDescription("OASIS-B Follow-up", "Follow-up", "OASIS", "Assessment", "OASISBFollowUp")]
        OASISBFollowUp = 2,
        [CustomDescription("OASIS-B Recertification", "Recert.", "OASIS", "Assessment", "OASISBRecertification")]
        OASISBRecertification = 3,
        [CustomDescription("OASIS-B Resumption of Care", "ROC", "OASIS", "Assessment", "OASISBResumptionofCare")]
        OASISBResumptionofCare = 4,
        [CustomDescription("OASIS-C Death", "Death", "OASIS", "Assessment", "OASISCDeath")]
        OASISCDeath = 5,
        [CustomDescription("OASIS-C Discharge", "Discharge", "OASIS", "Assessment", "OASISCDischarge")]
        OASISCDischarge = 6,
        [CustomDescription("OASIS-C Follow-up", "Follow-up", "OASIS", "Assessment", "OASISCFollowUp")]
        OASISCFollowUp = 7,
        [CustomDescription("OASIS-C Recertification", "Recert.", "OASIS", "Assessment", "OASISCRecertification")]
        OASISCRecertification = 8,
        [CustomDescription("OASIS-C Resumption of Care", "ROC", "OASIS", "Assessment", "OASISCResumptionofCare")]
        OASISCResumptionofCare = 9,
        [CustomDescription("OASIS-B Discharge", "Discharge", "OASIS", "Assessment", "OASISBDischarge")]
        OASISBDischarge = 10,
        [CustomDescription("OASIS-B Start of Care", "SOC", "OASIS", "Assessment", "OASISBStartofCare")]
        OASISBStartofCare = 11,
        [CustomDescription("OASIS-B Transfer", "Transfer", "OASIS", "Assessment", "OASISBTransfer")]
        OASISBTransfer = 12,
        [CustomDescription("OASIS-C Start of Care", "SOC", "OASIS", "Assessment", "OASISCStartofCare")]
        OASISCStartofCare = 13,
        [CustomDescription("OASIS-C Transfer", "Transfer", "OASIS", "Assessment", "OASISCTransfer")]
        OASISCTransfer = 14,
        [CustomDescription("OASIS B Death at Home", "Death", "OASIS", "Assessment", "OASISBDeathatHome")]
        OASISBDeathatHome = 15,
        [CustomDescription("SN Insulin AM Visit", "SNV", "SN", "Notes", "SN")]
        SNInsulinAM = 16,
        [CustomDescription("SN Insulin PM Visit", "SNV", "SN", "Notes", "SN")]
        SNInsulinPM = 17,
        [CustomDescription("Discharge Summary", "DS", "SN", "Notes", "DS")]
        DischargeSummary = 18,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        FoleyCathChange = 19,
        [CustomDescription("LVN Supervisory Visit", "LVN Sup Visit", "SN", "Notes", "LVNSupervisoryVisit")]
        LVNSupervisoryVisit = 20,
        [CustomDescription("SN B12 Injection Visit", "SNV", "SN", "Notes", "SN")]
        SNB12INJ = 21,
        [CustomDescription("SN BMP Visit", "SNV", "SN", "Notes", "SN")]
        SNBMP = 22,
        [CustomDescription("SN CBC Visit", "SNV", "SN", "Notes", "SN")]
        SNCBC = 23,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNHaldolInj = 24,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        PICCMidlinePlacement = 25,
        [CustomDescription("SN PRN Foley Change Visit", "SNV", "SN", "Notes", "SN")]
        PRNFoleyChange = 26,
        [CustomDescription("SN PRN Visit", "SNV", "SN", "Notes", "SN")]
        PRNSNV = 27,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        PRNVPforCMP = 28,
        [CustomDescription("PT w/ INR", "PT", "PT", "Notes", "PTWithINR")]
        PTWithINR = 29,
        [CustomDescription("PT w/ INR PRN", "SNV", "SN", "Notes", "SN")]
        PTWithINRPRNSNV = 30,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SkilledNurseHomeInfusionSD = 31,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SkilledNurseHomeInfusionSDAdditional = 32,
        [CustomDescription("Skilled Nurse Assessment (Start of Care)", "SNV", "SN", "Notes", "SNAssessment")]
        SNAssessment = 33,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNDC = 34,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNEvaluation = 35,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNFoleyLabs = 36,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNFoleyChange = 37,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNInjection = 38,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNInjectionLabs = 39,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNLabsSN = 40,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNVPsychNurse = 41,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNVwithAideSupervision = 42,
        [CustomDescription("Skilled Nurse Visit", "SNV", "SN", "Notes", "SN")]
        SNVDCPlanning = 43,
        [CustomDescription("PT Evaluation", "PT", "PT", "Notes", "PTNotes")]
        PTEvaluation = 44,
        [CustomDescription("PT Visit", "PT", "PT", "Notes","PTVisit")]
        PTVisit = 45,
        [CustomDescription("PT Discharge", "PT", "PT", "Notes", "PTDischarge")]
        PTDischarge = 46,
        [CustomDescription("OT Evaluation", "OT", "OT", "Notes", "OTNotes")]
        OTEvaluation = 47,
        [CustomDescription("OT Re-Evaluation", "OT", "OT", "Notes", "OTNotes")]
        OTReEvaluation = 48,
        [CustomDescription("OT Visit", "OT", "OT", "Notes", "OTVisit")]
        OTVisit = 49,
        [CustomDescription("ST Visit", "ST", "ST", "Notes", "STVisit")]
        STVisit = 50,
        [CustomDescription("ST Evaluation", "ST", "ST", "Notes", "STNotes")]
        STEvaluation = 51,
        [CustomDescription("ST Discharge", "ST", "ST", "Notes", "STNotes")]
        STDischarge = 52,
        [CustomDescription("MSW Evaluation", "MSW", "MSW", "Notes", "MSWEvaluationAssessment")]
        MSWEvaluationAssessment = 53,
        [CustomDescription("HHA Visit", "HHA", "HHA", "Notes", "HHAideVisit")]
        HHAideVisit = 54,
        [CustomDescription("HHA Supervisory Visit", "HHA Sup Visit", "SN", "Notes", "HHAideSupervisoryVisit")]
        HHAideSupervisoryVisit = 55,
        [CustomDescription("MSW Visit", "MSW", "MSW", "Notes", "MSWVisit")]
        MSWVisit = 56,
        [CustomDescription("MSW Discharge", "MSW", "MSW", "Notes", "MSWDischarge")]
        MSWDischarge = 57,
        [CustomDescription("Dietician Visit", "DV", "DV", "Notes", "DieticianVisit")]
        DieticianVisit = 58,
        [CustomDescription("PTA Visit", "PT", "PT", "Notes", "PTVisit")]
        PTAVisit = 59,
        [CustomDescription("PT Re-Evaluation", "PT", "PT", "Notes", "PTNotes")]
        PTReEvaluation = 60,
        [CustomDescription("OASIS-C Start of Care (PT)", "SOC", "OASIS", "Assessment", "OASISCStartofCarePT")]
        OASISCStartofCarePT = 61,
        [CustomDescription("OASIS-C Resumption of Care (PT)", "ROC", "OASIS", "Assessment", "OASISCResumptionofCarePT")]
        OASISCResumptionofCarePT = 62,
        [CustomDescription("OASIS-C Death (PT)", "Death", "OASIS", "Assessment", "OASISCDeathPT")]
        OASISCDeathPT = 63,
        [CustomDescription("OASIS-C Discharge (PT)", "Discharge", "OASIS", "Assessment", "OASISCDischargePT")]
        OASISCDischargePT = 64,
        [CustomDescription("OASIS-C Follow-up (PT)", "Follow-up", "OASIS", "Assessment", "OASISCFollowupPT")]
        OASISCFollowupPT = 65,
        [CustomDescription("OASIS-C Recertification (PT)", "Recert.", "OASIS", "Assessment", "OASISCRecertificationPT")]
        OASISCRecertificationPT = 66,
        [CustomDescription("OASIS-C Transfer (PT)", "Transfer", "OASIS", "Assessment", "OASISCTransferPT")]
        OASISCTransferPT = 67,
        [CustomDescription("COTA Visit", "OT", "OT", "Notes", "OTVisit")]
        COTAVisit = 68,
        [CustomDescription("OASIS-C Resumption of Care (OT)", "ROC", "OASIS", "Assessment", "OASISCResumptionofCareOT")]
        OASISCResumptionofCareOT = 69,
        [CustomDescription("OASIS-C Death (OT)", "Death", "OASIS", "Assessment", "OASISCDeathOT")]
        OASISCDeathOT = 70,
        [CustomDescription("OASIS-C Discharge (OT)", "Discharge", "OASIS", "Assessment", "OASISCDischargeOT")]
        OASISCDischargeOT = 71,
        [CustomDescription("OASIS-C Follow-up (OT)", "Follow-up", "OASIS", "Assessment", "OASISCFollowupOT")]
        OASISCFollowupOT = 72,
        [CustomDescription("OASIS-C Recertification (OT)", "Recert.", "OASIS", "Assessment", "OASISCRecertificationOT")]
        OASISCRecertificationOT = 73,
        [CustomDescription("OASIS-C Transfer (OT)", "Transfer", "OASIS", "Assessment", "OASISCTransferOT")]
        OASISCTransferOT = 74,
        [CustomDescription("HHA Care Plan", "HHA", "HHA", "Notes", "HHAideCarePlan")]
        HHAideCarePlan = 75,
        [CustomDescription("MSW Assessment", "MSW", "MSW", "Notes", "MSWAssessment")]
        MSWAssessment = 76,
        [CustomDescription("MSW Progress Note", "MSW", "MSW", "Notes", "MSWProgressNote")]
        MSWProgressNote = 77,
        [CustomDescription("485 Plan of Care (From Assessment)", "485", "485", "Orders", "HCFA485")]
        HCFA485 = 78,
        [CustomDescription("486 Plan Of Care", "486", "486", "Orders", "HCFA486")]
        HCFA486 = 79,
        [CustomDescription("Physician Order", "Order", "Order", "Orders", "PhysicianOrder")]
        PhysicianOrder = 80,
        [CustomDescription("Physician Order", "Order", "Order", "Orders", "PostHospitalizationOrder")]
        PostHospitalizationOrder = 81,
        [CustomDescription("Medicaid POC", "POC", "Order", "Orders", "MedicaidPOC")]
        MedicaidPOC = 82,
        [CustomDescription("Rap", "Rap", "Claim", "Claims", "Rap")]
        Rap = 83,
        [CustomDescription("Final", "Final", "Claim", "Claims", "Final")]
        Final = 84,
        [CustomDescription("60 Day Summary/Case Conference", "SDS", "SN", "Notes", "SixtyDaySummary")]
        SixtyDaySummary = 85,
        [CustomDescription("Transfer Summary", "TS", "SN", "Notes", "TransferSummary")]
        TransferSummary = 86,
        [CustomDescription("Communication Note", "Com Note", "Notes", "Notes", "CommunicationNote")]
        CommunicationNote = 87,
        [CustomDescription("OASIS-C Transfer Discharge", "Transfer", "OASIS", "Assessment", "OASISCTransferDischarge")]
        OASISCTransferDischarge = 88,
        [CustomDescription("Non-OASIS Start of Care", "NonOasisSOC", "NonOASIS", "Assessment", "NonOASISStartofCare")]
        NonOASISStartofCare = 89,
        [CustomDescription("Non-OASIS Recertification", "NonOasisRecert", "NonOASIS", "Assessment", "NonOASISRecertification")]
        NonOASISRecertification = 90,
        [CustomDescription("Non-OASIS Discharge", "NonOasisDischarge", "NonOASIS", "Assessment", "NonOASISDischarge")]
        NonOASISDischarge = 91,
        [CustomDescription("Non-OASIS Plan of Care", "NonOasis485", "NonOasis485", "Assessment", "NonOasisHCFA485")]
        NonOasisHCFA485 = 92,
        [CustomDescription("Incident / Accident Report", "Incident/Accident", "ReportsAndNotes", "ReportsAndNotes", "IncidentAccidentReport")]
        IncidentAccidentReport = 93,
        [CustomDescription("Infection Report", "InfectionReport", "ReportsAndNotes", "ReportsAndNotes", "InfectionReport")]
        InfectionReport = 94,
        [CustomDescription("SNV-Teaching/Training", "SNV", "SN", "Notes", "SN")]
        SNVTeachingTraining = 95,
        [CustomDescription("SNV-Observation & Assessment", "SNV", "SN", "Notes", "SN")]
        SNVObservationAndAssessment = 96,
        [CustomDescription("SNV-Management & Evaluation", "SNV", "SN", "Notes", "SN")]
        SNVManagementAndEvaluation = 97,
        [CustomDescription("Plan of Treatment/Care", "485", "485", "Orders", "HCFA485StandAlone")]
        HCFA485StandAlone = 98,
        [CustomDescription("PAS Note", "HHA", "HHA", "Notes", "PASVisit")]
        PASVisit = 99,
        [CustomDescription("PAS Care Plan", "HHA", "HHA", "Notes", "PASCarePlan")]
        PASCarePlan = 100,
        [CustomDescription("Skilled Nurse Assessment (Recertification)", "SNV", "SN", "Notes", "SNAssessmentRecert")]
        SNAssessmentRecert = 101,
        [CustomDescription("Physician Face-to-face Encounter", "Order", "Order", "Orders", "FaceToFaceEncounter")]
        FaceToFaceEncounter = 102,
        [CustomDescription("OT Discharge", "OT", "OT", "Notes", "OTNotes")]
        OTDischarge = 103,
        [CustomDescription("ST Re-Evaluation", "ST", "ST", "Notes", "STNotes")]
        STReEvaluation = 104,
        [CustomDescription("PT Maintenance Visit", "PT", "PT", "Notes", "PTNotes")]
        PTMaintenance = 105,
        [CustomDescription("OT Maintenance Visit", "OT", "OT", "Notes","OTNotes")]
        OTMaintenance = 106,
        [CustomDescription("ST Maintenance Visit", "ST", "ST", "Notes", "STNotes")]
        STMaintenance = 107,
        [CustomDescription("Medicare Eligibility Report", "MER", "MER", "ReportsAndNotes", "MedicareEligibilityReport")]
        MedicareEligibilityReport = 108,
        [CustomDescription("Coordination of Care", "COC", "SN", "Notes", "CoordinationOfCare")]
        CoordinationOfCare = 109,
        [CustomDescription("Driver / Transportation Log", "MSW", "MSW", "Notes", "DriverOrTransportationNote")]
        DriverOrTransportationNote = 110,
        [CustomDescription("SN Diabetic Daily Visit", "SNV", "SN", "Notes", "SN")]
        SNDiabeticDailyVisit = 111,
       [CustomDescription("OASIS-C Start of Care (OT)", "SOC", "OASIS", "Assessment", "OASISCStartofCareOT")]
        OASISCStartofCareOT = 112
    }
}
