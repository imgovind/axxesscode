﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum CahpsVendors : byte
    {
        [Description("DSS Research")]
        DSSResearch = 1,
        [Description("Arkansas Foundation for Medical Care")]
        ArkansasFoundationMedicalCare = 2,
        [Description("Novaetus, Inc")]
        Novaetus = 3,
        [Description("Deyta Satisfaction Experts")]
        Deyta = 4,
        [Description("Pinnacle Quality Insight")]
        Pinnacle = 5,
        [Description("Fields Research")]
        FieldsResearch = 6,
        [Description("Press Ganey")]
        PressGaney = 7,
        [Description("OCS HomeCare")]
        Ocs = 8,
        [Description("Fazzi Standard")]
        Fazzi = 9,
        [Description("Strategic Healthcare Programs")]
        Shp = 10
    }
}
