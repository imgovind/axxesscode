﻿namespace Axxess.AgencyManagement.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;

    public enum UB4PatientStatus
    {
        [Description("Still a patient")]
        StillPatient = 30,
        [Description("Discharge to home (Self-care)")]
        DischargeToHomeOrSelfCare = 01,
        [Description("Discharge to a short-term Hospital")]
        DischargeToShortTermHospital = 02,
        [Description("Discharge to SNF")]
        DischargeToSNF = 03,
        //[Description("Discharge to Intermediate Care Facility")]
        //DischargeToICF = 04,
        //[Description("Discharge to another type of institution")]
        //DischargeToAnotherTypeOfInstitution = 05,
        //[Description("Discharge to home under care of HHS")]
        //DischargeToHomeUnderCareOfHHS = 06,
        //[Description("Left against medical advice or discontinued care")]
        //LeftAgainstMedicalAdviceOrDiscontinuedCare = 07,
        //[Description("Expired")]
        //Expired = 20,
        //[Description("Expired at home")]
        //ExpiredAtHome = 40,
        //[Description("Expired in a medical facility")]
        //ExpiredInMedicalFacility = 41,
        //[Description("Expired, place unknown")]
        //ExpiredPlaceUnknown = 42,
        //[Description("Discharge to a Federal Health Care Facility")]
        //DischargeToFHCF = 43,
        [Description("Discharge to Hospice - home")]
        DischargeToHospiceHome = 50,
        [Description("Discharge to Hospice - Medicare Facility")]
        DischargeToHospiceMedicareFacility = 51


    }
}
