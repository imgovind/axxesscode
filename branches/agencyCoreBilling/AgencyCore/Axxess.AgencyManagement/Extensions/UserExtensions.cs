﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;

    public static class UserExtensions
    {
        public static List<UserSelection> ForSelection(this IList<User> users)
        {
            var result = new List<UserSelection>();

            if (users != null && users.Count > 0)
            {
                users.ForEach(p =>
                {
                    result.Add(new UserSelection { Id = p.Id, FirstName = p.FirstName, LastName = p.LastName, DisplayName = p.DisplayName, });
                });
            }

            return result.OrderBy(u => u.DisplayName).ToList();
        }

        public static bool AllowWeekendAccess(this User user)
        {
            var result = false;

            if (user != null)
            {
                if (DateTime.Today.IsWeekend())
                {
                    if (user.AllowWeekendAccess)
                    {
                        if (user.EarliestLoginTime.IsNotNullOrEmpty())
                        {
                            var earliestTime = new Time(user.EarliestLoginTime);
                            if (earliestTime != null && earliestTime.TimeOfDay <= DateTime.Now.TimeOfDay)
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }

            return result;
        }

        public static bool IsAgencyAdmin(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string admin = ((int)AgencyRoles.Administrator).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(admin))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsDirectorOfNursing(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string don = ((int)AgencyRoles.DoN).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(don))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsCaseManager(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string cm = ((int)AgencyRoles.CaseManager).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(cm))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsNurse(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string nurse = ((int)AgencyRoles.Nurse).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(nurse))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsClinician(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string nurse = ((int)AgencyRoles.Nurse).ToString();
                string pt = ((int)AgencyRoles.PhysicalTherapist).ToString();
                string ot = ((int)AgencyRoles.OccupationalTherapist).ToString();
                string st = ((int)AgencyRoles.SpeechTherapist).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(nurse) || agencyRole.IsEqual(pt) || agencyRole.IsEqual(ot) || agencyRole.IsEqual(st))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsClinicianOrHHA(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string nurse = ((int)AgencyRoles.Nurse).ToString();
                string pt = ((int)AgencyRoles.PhysicalTherapist).ToString();
                string ot = ((int)AgencyRoles.OccupationalTherapist).ToString();
                string st = ((int)AgencyRoles.SpeechTherapist).ToString();
                string hha = ((int)AgencyRoles.HHA).ToString();
                string msw = ((int)AgencyRoles.MedicalSocialWorker).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(nurse) || agencyRole.IsEqual(pt) || agencyRole.IsEqual(ot) || agencyRole.IsEqual(st) || agencyRole.IsEqual(hha) || agencyRole.IsEqual(msw))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsHHA(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string hha = ((int)AgencyRoles.HHA).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(hha))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsBiller(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string biller = ((int)AgencyRoles.Biller).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(biller))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsScheduler(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string scheduler = ((int)AgencyRoles.Scheduler).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(scheduler))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsClerk(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string clerk = ((int)AgencyRoles.Clerk).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(clerk))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsQA(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string qa = ((int)AgencyRoles.QA).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(qa))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsOfficeManager(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string om = ((int)AgencyRoles.OfficeManager).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(om))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsCommunityLiason(this string roles)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string clo = ((int)AgencyRoles.CommunityLiasonOfficer).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(clo))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static bool IsRole(this string roles, AgencyRoles role)
        {
            var result = false;
            if (roles.IsNotNullOrEmpty())
            {
                string clo = ((int)role).ToString();

                var agencyRoles = roles.Split(';');
                if (agencyRoles.Length > 0)
                {
                    foreach (string agencyRole in agencyRoles)
                    {
                        if (agencyRole.IsEqual(clo))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            return result;
        }
    }
}
