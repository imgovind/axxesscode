﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Extension;


    public static class ClaimExtensions
    {
        public static bool IsRapDischage(this Rap rap)
        {
            var result = false;
            if (rap != null && rap.UB4PatientStatus.IsNotNullOrEmpty() && rap.UB4PatientStatus.IsInteger() && Enum.IsDefined(typeof(UB4PatientStatus), rap.UB4PatientStatus))
            {
                var status = (UB4PatientStatus)rap.UB4PatientStatus.ToInteger();

                switch (status)
                {
                    case UB4PatientStatus.StillPatient:
                        result = false;
                        break;
                    case UB4PatientStatus.DischargeToHomeOrSelfCare:
                    case UB4PatientStatus.DischargeToHospiceHome:
                    case UB4PatientStatus.DischargeToHospiceMedicareFacility:
                    case UB4PatientStatus.DischargeToShortTermHospital:
                    case UB4PatientStatus.DischargeToSNF:
                        result = true;
                        break;
                }
            }
            return result;
        }

        public static bool IsFinalDischage(this Final final)
        {
            var result = false;
            if (final != null && final.UB4PatientStatus.IsNotNullOrEmpty() && final.UB4PatientStatus.IsInteger() && Enum.IsDefined(typeof(UB4PatientStatus), final.UB4PatientStatus))
            {
                var status = (UB4PatientStatus)final.UB4PatientStatus.ToInteger();

                switch (status)
                {
                    case UB4PatientStatus.StillPatient:
                        result = false;
                        break;
                    case UB4PatientStatus.DischargeToHomeOrSelfCare:
                    case UB4PatientStatus.DischargeToHospiceHome:
                    case UB4PatientStatus.DischargeToHospiceMedicareFacility:
                    case UB4PatientStatus.DischargeToShortTermHospital:
                    case UB4PatientStatus.DischargeToSNF:
                        result = true;
                        break;
                }
            }
            return result;
        }

        public static bool IsManagedClamDischage(this ManagedClaim managedClaim)
        {
            var result = false;
            if (managedClaim != null && managedClaim.UB4PatientStatus.IsNotNullOrEmpty() && managedClaim.UB4PatientStatus.IsInteger() && Enum.IsDefined(typeof(UB4PatientStatus), managedClaim.UB4PatientStatus))
            {
                var status = (UB4PatientStatus)managedClaim.UB4PatientStatus.ToInteger();

                switch (status)
                {
                    case UB4PatientStatus.StillPatient:
                        result = false;
                        break;
                    case UB4PatientStatus.DischargeToHomeOrSelfCare:
                    case UB4PatientStatus.DischargeToHospiceHome:
                    case UB4PatientStatus.DischargeToHospiceMedicareFacility:
                    case UB4PatientStatus.DischargeToShortTermHospital:
                    case UB4PatientStatus.DischargeToSNF:
                        result = true;
                        break;
                }
            }
            return result;
        }
    }
}
