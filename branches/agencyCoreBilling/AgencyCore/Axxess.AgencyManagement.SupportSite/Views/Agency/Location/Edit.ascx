﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AgencyLocation>" %>
<%  using (Html.BeginForm("Update", "Location", FormMethod.Post, new { @id = "editLocationForm" })) { %>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Location_Id" })%>
<%= Html.Hidden("AgencyId", Model.AgencyId, new { @id = "Edit_Location_AgencyId" })%>
<%= string.Format("{0}{1}{2}", "<script type='text/javascript'>acore.renamewindow('Edit Location | ", Model != null ? Model.Name.ToTitleCase() : "", "','editlocation');</script>")%>
<div class="wrapper main">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Location Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Location_Name" class="float_left">Name:</label><div class="float_right"><%=Html.TextBox("Name", Model.Name, new { @id = "Edit_Location_Name", @class = "text input_wrapper required", @maxlength = "40" })%></div></div>
        
            <div class="row"><label for="Edit_Location_CustomId" class="float_left">Custom ID:</label><div class="float_right"><%=Html.TextBox("CustomId", Model.CustomId, new { @id = "Edit_Location_CustomId", @class = "text input_wrapper" })%></div></div>
        </div>
         <div class="column">               
            <div class="row"><label for="Edit_Location_MedicareProviderNumber" class="float_left">Medicare Provider Number:</label><div class="float_right"><%=Html.TextBox("MedicareProviderNumber", Model.MedicareProviderNumber, new { @id = "Edit_Location_MedicareProviderNumber", @class = "text input_wrapper required", @maxlength = "10" })%></div></div>
        </div>
    </fieldset>
      <fieldset>
        <legend>Submitter Information (Medicare)</legend>
        <div class="float_left"><%=Html.CheckBox("IsSubmitterInfoTheSame", Model.IsSubmitterInfoTheSame, new { @id = "Edit_Location_IsSubmitterInfoTheSame", @class = "radio" })%>&nbsp;<label for="Edit_Location_IsSubmitterInfoTheSame">Check here if this branch office uses the same submitter info with the main branch.</label></div>
        <div class="clear"></div>
        <div id="Edit_Location_SubmiterInfoContent">
        <div class="column">
            <div class="row"><label for="New_Agency_SubmitterId">Submitter Id:</label><div class="float_right"><%=Html.TextBox("SubmitterId", Model.SubmitterId, new { @id = "Edit_Location_SubmitterId", @maxlength = "15", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="New_Agency_SubmitterName">Submitter Name:</label><div class="float_right"><%=Html.TextBox("SubmitterName", Model.SubmitterName, new { @id = "Edit_Location_SubmitterName", @maxlength = "50", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="New_Agency_Payor">Payor/Medicare Regional HH Intermediary:</label><div class="float_right"><%var payor = new SelectList(new[] { new SelectListItem { Text = "-- Select Payor --", Value = "0" }, new SelectListItem { Text = "Palmetto GBA", Value = "1" }, new SelectListItem { Text = "National Government Services (formerly known as United Government Services)", Value = "2" }, new SelectListItem { Text = "Blue Cross Blue Shield of Alabama (AKA Chaba GBA)", Value = "3" }, new SelectListItem { Text = "Anthem Health Plans of Maine", Value = "4" } }, "Value", "Text", Model.Payor); %><%= Html.DropDownList("Payor", payor, new { @id = "New_Agency_Payor" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="New_Agency_SubmitterPhone1">Submitter Phone Number:</label><div class="float_right"><input type="text" class="autotext numeric phone_short required" name="SubmitterPhoneArray" id="Edit_Location_SubmitterPhone1" maxlength="3" /> - <input type="text" class="autotext numeric phone_short required" name="SubmitterPhoneArray" id="Edit_Location_SubmitterPhone2" maxlength="3" /> - <input type="text" class="autotext numeric phone_long required" name="SubmitterPhoneArray" id="Edit_Location_SubmitterPhone3" maxlength="4" /></div></div>
            <div class="row"><label for="New_Agency_SubmitterFax1">Submitter Fax Number:</label><div class="float_right"><input type="text" class="autotext numeric phone_short required" name="SubmitterFaxArray" id="Edit_Location_SubmitterFax1" maxlength="3" /> - <input type="text" class="autotext numeric phone_short required" name="SubmitterFaxArray" id="Edit_Location_SubmitterFax2" maxlength="3" /> - <input type="text" class="autotext numeric phone_long required" name="SubmitterFaxArray" id="Edit_Location_SubmitterFax3" maxlength="4" /></div></div>
        </div>
        </div>
        <div class="wide_column"><div class="row"><label for="Edit_Location_BranchID" class="float_left">(M0016)Branch ID Number(For OASIS Submission):</label><div class="float_left"><%var branchId = new SelectList(new[] { new SelectListItem { Text = "-- Select Branch Id --", Value = "0" }, new SelectListItem { Text = "N", Value = "N" }, new SelectListItem { Text = "P", Value = "P" }, new SelectListItem { Text = "Other", Value = "Other" }}, "Value", "Text", Model.BranchId.IsNotNullOrEmpty()? Model.BranchId: "0"); %><%= Html.DropDownList("BranchId", branchId, new { @id = "Edit_Location_BranchId", @class = "requireddropdown" })%></div><%=Html.TextBox("BranchIdOther", Model.BranchIdOther, new { @id = "Edit_Location_BranchIdOther", @class = string.Format("text input_wrapper {0}",Model.BranchId=="Other"? "":"hidden") })%> </div></div>
    </fieldset>
    <fieldset>
        <legend>Location Address</legend>
        <div class="column">
            <div class="row"><label for="Edit_Location_AddressLine1" class="float_left">Address Line 1:</label><div class="float_right"><%=Html.TextBox("AddressLine1", Model.AddressLine1, new { @id = "Edit_Location_AddressLine1", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressLine2" class="float_left">Address Line 2:</label><div class="float_right"><%=Html.TextBox("AddressLine2", Model.AddressLine2, new { @id = "Edit_Location_AddressLine2", @class = "text input_wrapper" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressCity" class="float_left">City:</label><div class="float_right"><%=Html.TextBox("AddressCity", Model.AddressCity, new { @id = "Edit_Location_AddressCity", @class = "text input_wrapper required" })%></div></div>
            <div class="row"><label for="Edit_Location_AddressStateCode" class="float_left">State, Zip:</label><div class="float_right"><%= Html.LookupSelectList(SelectListTypes.States, "AddressStateCode", Model.AddressStateCode, new { @id = "Edit_Location_AddressStateCode", @class = "AddressStateCode required valid" })%><%= Html.TextBox("AddressZipCode", Model.AddressZipCode, new { @id = "Edit_Location_AddressZipCode", @class = "text required digits isValidUSZip zip", @maxlength = "5" })%></div></div>
        </div>
        <div class="column">
            <div class="row"><label for="Edit_Location_PhoneArray1" class="float_left">Primary Phone:</label><div class="float_right"><%=Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(0, 3) : "", new { @id = "Edit_Location_PhoneArray1", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(3, 3) : "", new { @id = "Edit_Location_PhoneArray2", @class = "input_wrappermultible autotext required digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("PhoneArray", Model.PhoneWork.IsNotNullOrEmpty() ? Model.PhoneWork.Substring(6, 4) : "", new { @id = "Edit_Location_PhoneArray3", @class = "input_wrappermultible autotext required digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
            <div class="row"><label for="Edit_Location_FaxNumberArray1" class="float_left">Fax Number:</label><div class="float_right"><%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(0, 3) : "", new { @id = "Edit_Location_FaxNumberArray1", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "4" })%>&nbsp;-&nbsp;<%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(3, 3) : "", new { @id = "Edit_Location_FaxNumberArray2", @class = "input_wrappermultible autotext  digits phone_short", @maxlength = "3", @size = "3" })%>&nbsp;-&nbsp;<%=Html.TextBox("FaxNumberArray", Model.FaxNumber.IsNotNullOrEmpty() ? Model.FaxNumber.Substring(6, 4) : "", new { @id = "Edit_Location_FaxNumberArray3", @class = "input_wrappermultible autotext  digits phone_long", @maxlength = "4", @size = "5" })%></div></div>
        </div>
        <table class="form"><tbody>
            <tr class="linesep vert">
                <td><label for="Edit_Location_Comments">Comments:</label><div><%= Html.TextArea("Comments", Model.Comments, new { @id = "Edit_Location_Comments" })%></div></td>
            </tr>
        </tbody></table>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('editlocation');">Cancel</a></li>
    </ul></div>
</div>
<% } %>
<script type="text/javascript">
    if ('<%=Model.IsSubmitterInfoTheSame %>') { $("#Edit_Location_SubmiterInfoContent").hide(); $("#Edit_Location_SubmiterInfoContent input[type=text]").removeClass("required"); $("#Edit_Location_SubmiterInfoContent select").removeClass("requireddropdown"); } else { $("#Edit_Location_SubmiterInfoContent").show(); $("#Edit_Location_SubmiterInfoContent input[type=text]").addClass("required"); $("#Edit_Location_SubmiterInfoContent select").addClass("requireddropdown"); }
    $("#Edit_Location_IsSubmitterInfoTheSame").click(function() { if ($(this).is(':checked')) { $("#Edit_Location_SubmiterInfoContent").hide(); $("#Edit_Location_SubmiterInfoContent input[type=text]").removeClass("required"); $("#Edit_Location_SubmiterInfoContent select").removeClass("requireddropdown"); } else { $("#Edit_Location_SubmiterInfoContent").show(); $("#Edit_Location_SubmiterInfoContent input[type=text]").addClass("required"); $("#Edit_Location_SubmiterInfoContent select").addClass("requireddropdown"); } });
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $("#Edit_Location_BranchId").change(function() {  if($(this).val() == 'Other') { $("#Edit_Location_BranchIdOther").removeClass("hidden"); } else { $("#Edit_Location_BranchIdOther").removeClass("hidden").addClass("hidden"); } });
</script>
