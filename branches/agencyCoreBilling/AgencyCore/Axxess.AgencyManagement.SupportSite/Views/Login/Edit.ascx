﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Axxess.Membership.Domain.Login>" %>
<% using (Html.BeginForm("Update", "Login", FormMethod.Post, new { @id = "editLoginForm" })) {%>
<%= Html.Hidden("Id", Model.Id, new { @id = "Edit_Login_Id" }) %>
<div class="form_wrapper">
    <div class="abs req_legend"><span class="req_red">*</span> = Required Field</div>
    <fieldset>
        <legend>Login Information</legend>
        <div class="column">
            <div class="row"><label for="Edit_Login_EmailAddress">E-mail Address:</label><div class="float_right"><span class="bigtext"><%= Model.EmailAddress %></span></div></div>
            <div class="row"><label for="Edit_Login_DisplayName">Display Name:</label><div class="float_right"><%=Html.TextBox("DisplayName", Model.DisplayName, new { @id = "Edit_Login_DisplayName", @class = "text required names input_wrapper", @maxlength = "20" })%></div></div>
            <div class="row"><label for="Edit_Login_IsAxxessAdmin">Is Axxess Admin?</label><div class="float_right"><%= Html.CheckBox("IsAxxessAdmin", Model.IsAxxessAdmin ? true : false, new { @id = "Edit_Login_IsAxxessAdmin" })%></div></div>
            <div class="row"><label for="Edit_Login_IsAxxessSupport">Is Axxess Support?</label><div class="float_right"><%= Html.CheckBox("IsAxxessSupport", Model.IsAxxessSupport ? true : false, new { @id = "Edit_Login_IsAxxessSupport" })%></div></div>
        </div>
    </fieldset>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="$(this).closest('form').submit();">Save</a></li>
        <li><a href="javascript:void(0);" onclick="acore.close('editlogin');">Cancel</a></li>
    </ul></div>
</div>
<script type="text/javascript">
    $(".row input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row div.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row select.requireddropdown").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
    $(".row input[type='checkbox'].required").closest(".row").find(".req_red").remove();
    $("input[type='checkbox'].required").closest("fieldset").append("<span class='req_red abs_right'>*</span>");
</script>
<% } %>
