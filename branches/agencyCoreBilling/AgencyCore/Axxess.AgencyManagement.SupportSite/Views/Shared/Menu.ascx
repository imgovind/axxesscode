﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">
acore.init();
<% if (Current.IsAxxessAdmin) { %>
acore.addmenu("Create", "create", "mainmenu", 22);
acore.addwindow("newlogin", "User", "Login/New", function() { Login.InitNew(); }, "create");
acore.addwindow("newagency", "Agency", "Agency/New", function() { Agency.InitNew(); }, "create");
acore.addmenu("Message", "createnew", "create");
acore.addwindow("newsystemmessage", "System Message", "System/NewMessage", function() { SystemMessage.InitNew(); }, "createnew");
acore.addwindow("newdashboardmessage", "Dashboard Message", "System/NewDashboardMessage", function() { DashboardMessage.InitNew(); }, "createnew");
acore.addmenu("View", "view", "mainmenu", 43);
acore.addwindow("listlogins", "Users", "Login/List", function() { }, "view");
acore.addwindow("listagencies", "Agencies", "Agency/List", function() { }, "view");
acore.addwindow("editlogin", "Edit Login", null, null, false);
acore.addwindow("editagency", "Edit Agency", null, null, false);
acore.addwindow("listagencylocations", "Agency Locations", null, null, false);
acore.addwindow("newlocation", "New Location", null, null, false);
acore.addwindow("editlocation", "Edit Location", null, null, false);
<% } else { %>
acore.addmenu("Agency", "agency", "mainmenu", 0);
acore.addwindow("listagencies", "View Agencies", "Agency/List", function() { }, "agency");
<% } %>

acore.addwindow("listagencyusers", "Agency Users", null, null, false);

$('ul#mainmenu').superfish();

</script>

