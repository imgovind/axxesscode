﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% using (Html.BeginForm("AddDashboardMessage", "System", FormMethod.Post, new { @id = "newDashboardMessageForm" })) { %>

<div id="dashboardMessageContainer" class="wrapper">
    <div class="newDashboardContent" id="newDashboardContent">
        <div class="inboxSubHeader"><span>New Dashboard Message</span>
            <div class="buttons float_right"><ul>
                <li><a href="javascript:void(0);" onclick="UserInterface.PreviewDashboardMessage(CKEDITOR.instances['New_DashboardMessage_Body'].getData());">Preview</a></li>
                <li><a href="javascript:void(0);" onclick="UserInterface.CloseWindow('newdashboardmessage');">Cancel</a></li>
            </ul></div>
        </div>
        <div class="buttons float_left"><ul><li class="send"><a href="javascript:void(0);" onclick="$(this).closest('form').submit();"><span class="img icon send"></span><br />Send</a></li></ul></div>
        <div class="newMessageContentPanel clear">
            <div class="newMessageRow"><label for="New_DashboardMessage_Subject">Title:</label><input type="text" id="New_DashboardMessage_Subject" name="Title" maxlength="150" /></div>
            <div class="newMessageRow"><label for="New_DashboardMessage_CreatedBy">Created By:</label><span id="New_DashboardMessage_CreatedBy"><%= Current.DisplayName %></span></div>
            <div class="newMessageRow"><label for="New_DashboardMessage_CreatedDate">Created By:</label><span id="New_DashboardMessage_CreatedDate"><%= DateTime.Now.ToShortDateString() %></span></div>
        </div>
        <div class="newMessageRow" id="newDashboardMessageBodyDiv">
            <div>
                <textarea id="New_DashboardMessage_Body" name="Text"></textarea>
            </div>
        </div>
    </div>
</div>
<%} %>
<script type="text/javascript">
    $(".row :input.required").closest(".row").prepend("<span class='req_red abs_right'>*</span>");
</script>

<div id="newDashboardMessagePreview" class="dashboardMessagePreviewModal hidden">
    <ul id="widget_col1" class="widgets">
        <li class="widget" id="intro_widget">
            <div class="widget-head"><h5>Hello,&nbsp;<%= Current.DisplayName %>!</h5></div>
            <div id="customMessageWidget" class="widget-content"></div>
        </li>
    </ul>
    <div class="buttons"><ul>
        <li><a href="javascript:void(0);" onclick="U.closeDialog();">Close</a></li>
    </ul></div>
</div>