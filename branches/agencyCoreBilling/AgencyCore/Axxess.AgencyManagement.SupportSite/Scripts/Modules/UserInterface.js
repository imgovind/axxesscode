﻿var UserInterface = {
    ShowEditLogin: function(Id) {
        acore.open("editlogin", 'Login/Edit', function() { Login.InitEdit(); }, { loginId: Id });
    },
    ShowNewLocation: function(agencyId) {
        acore.open("newlocation", 'Location/New', function() { Location.InitNew(); }, { agencyId: agencyId });
    },
    ShowEditLocation: function(agencyId, locationId) {
        acore.open("editlocation", 'Location/Edit', function() { Location.InitEdit(); }, { agencyId: agencyId, locationId: locationId });
    },
    PreviewDashboardMessage: function(text) {
        U.showDialog("#newDashboardMessagePreview", function() {
            $("#customMessageWidget").html(text);
        });
    },
    ResendActivationLink: function(userId, agencyId) {
        if (confirm("Are you sure you want to resend the activation link for this user?")) {
            U.postUrl("/Account/ResendLink", "userId=" + userId + "&agencyId=" + agencyId, function(result) {
                if (result.isSuccessful) U.growl("Activation Link sent successfully", "success");
                else U.growl(result.errorMessage, "error");
            });
        }
    },
    CloseWindow: function(window) {
        acore.close(window);
    }
}
