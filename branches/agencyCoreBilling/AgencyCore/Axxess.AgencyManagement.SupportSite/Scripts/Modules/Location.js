﻿var Location = {
    Delete: function(Id) { U.DeleteTemplate("Location", Id); },
    InitEdit: function() {
        U.PhoneAutoTab('Edit_Location_PhoneArray');
        U.PhoneAutoTab('Edit_Location_FaxNumberArray');
        U.InitEditTemplate("Location");
    },
    InitNew: function() {
        U.PhoneAutoTab('New_Location_PhoneArray');
        U.PhoneAutoTab('New_Location_FaxNumberArray');
        U.InitNewTemplate("Location");
    },
    RebindList: function() { U.rebindTGrid($('#List_Agency_Locations')); }
}