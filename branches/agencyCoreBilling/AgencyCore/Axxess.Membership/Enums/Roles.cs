﻿namespace Axxess.Membership.Enums
{
    public enum Roles : byte
    {
        AxxessAdmin = 1,
        ApplicationUser = 2,
        Physician = 3,
        Therapist = 4,
        Clinician = 5
    }
}
