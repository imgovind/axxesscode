﻿namespace Axxess.Membership.Enums
{
    public enum Environments
    {
        Development = 1,
        QualityAssurance = 2,
        UserAcceptance = 3,
        Production = 4
    }
}
