﻿namespace Axxess.Physician.App
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Threading;
    using System.Web.Routing;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Logging;
    
    using Security;

    public class AxxessPhysicianApplication : HttpApplication
    {
        public AxxessPhysicianApplication()
        {
            this.AuthenticateRequest += new EventHandler(AxxessApplication_AuthenticateRequest);
        }

        protected void AxxessApplication_AuthenticateRequest(object sender, EventArgs e)
        {
            if (Context.User != null)
            {
                string username = HttpContext.Current.User.Identity.Name;
                IPhysicianMembershipService membershipService = Container.Resolve<IPhysicianMembershipService>();
                AxxessPhysicianPrincipal principal = membershipService.Get(username);

                if (principal != null)
                {
                    Thread.CurrentPrincipal = principal;
                    HttpContext.Current.User = principal;
                }
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.Run();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                Logger.Exception(exception);
            }
        }
    }
}
