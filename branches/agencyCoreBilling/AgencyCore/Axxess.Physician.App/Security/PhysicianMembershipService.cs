﻿namespace Axxess.Physician.App.Security
{
    using System;
    using System.Web;
    using System.Text;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership;
    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    public class PhysicianMembershipService : IPhysicianMembershipService
    {
        #region Private Members

        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPhysicianRepository physicianRepository;

        #endregion

        #region Constructor

        public PhysicianMembershipService(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.agencyRepository = agencyManagementProvider.AgencyRepository;
            this.physicianRepository = agencyManagementProvider.PhysicianRepository;
        }

        #endregion

        #region IMembershipService Members

        public bool Activate(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string passwordSalt = string.Empty;
                string passwordHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Password, out passwordHash, out passwordSalt);
                login.PasswordSalt = passwordSalt;
                login.SignatureSalt = passwordSalt;
                login.PasswordHash = passwordHash;
                login.SignatureHash = passwordHash;
                login.LastLoginDate = DateTime.Now;
                if (loginRepository.Update(login))
                {
                    AxxessPhysicianPrincipal principal = Get(login.EmailAddress);
                    if (principal != null)
                    {
                        account.UserName = login.EmailAddress;
                        return true;
                    }
                }
            }

            return false;
        }

        public AxxessPhysicianPrincipal Get(string userName)
        {
            var principal = Cacher.Get<AxxessPhysicianPrincipal>(userName);
            if (principal == null)
            {
                var login = loginRepository.Find(userName);
                if (login != null)
                {
                    Roles role = login.Role.ToEnum<Roles>(Roles.Physician);
                    if (role == Roles.Physician)
                    {
                        var physicianList = physicianRepository.GetByLoginId(login.Id);
                        if (physicianList != null && physicianList.Count > 0)
                        {
                            var identity = new AxxessPhysicianIdentity(login.Id, login.EmailAddress);
                            identity.Session =
                                new PhysicianSession
                                {
                                    LoginId = login.Id,
                                    DisplayName = login.DisplayName,
                                    AgencyPhysicianIdentifiers = GetPhysicianIdentifiers(physicianList)
                                };

                            principal = new AxxessPhysicianPrincipal(identity);
                            Cacher.Set<AxxessPhysicianPrincipal>(userName, principal);
                        }
                    }
                }
            }

            return principal;
        }

        public LoginAttemptType Validate(string userName, string password)
        {
            var loginAttempt = LoginAttemptType.Failed;
            var login = loginRepository.Find(userName);

            if (login != null)
            {
                var saltedHash = new SaltedHash();
                if (saltedHash.VerifyHashAndSalt(password.Trim(), login.PasswordHash, login.PasswordSalt))
                {
                    Roles role = login.Role.ToEnum<Roles>(Roles.Physician);
                    if (role == Roles.Physician)
                    {
                        var physicianList = physicianRepository.GetByLoginId(login.Id);
                        if (physicianList != null && physicianList.Count > 0)
                        {
                            var identity = new AxxessPhysicianIdentity(login.Id, login.EmailAddress);
                            identity.Session =
                                new PhysicianSession
                                {
                                    LoginId = login.Id,
                                    DisplayName = login.DisplayName,
                                    AgencyPhysicianIdentifiers = GetPhysicianIdentifiers(physicianList)
                                };

                            login.LastLoginDate = DateTime.Now;
                            if (loginRepository.Update(login))
                            {
                                loginAttempt = LoginAttemptType.Success;
                            }

                            var principal = new AxxessPhysicianPrincipal(identity);
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                            Cacher.Set<AxxessPhysicianPrincipal>(userName, principal);
                        }
                    }
                }
            }

            return loginAttempt;
        }

        public void LogOff(string userName)
        {
            SessionStore.Abandon();
            Cacher.Remove(userName);
        }

        public ResetAttemptType Validate(string userName)
        {
            var resetAttempt = ResetAttemptType.Failed;

            var login = loginRepository.Find(userName);
            if (login != null)
            {
                if (login.IsLocked)
                {
                    resetAttempt = ResetAttemptType.Locked;
                }
                else
                {
                    if (login.IsActive)
                    {
                        resetAttempt = ResetAttemptType.Success;
                    }
                    else
                    {
                        resetAttempt = ResetAttemptType.Deactivated;
                    }
                }
            }

            return resetAttempt;
        }

        public bool ResetPassword(string userName)
        {
            var login = loginRepository.Find(userName);
            if (login != null && login.IsAxxessAdmin == false && login.IsAxxessSupport == false)
            {
                var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}&type=password", login.Id)));
                var bodyText = MessageBuilder.PrepareTextFrom(
                    "PasswordResetInstructions",
                    "firstname", login.DisplayName,
                    "encryptedQueryString", encryptedQueryString);
                Notify.User(CoreSettings.NoReplyEmail, userName, "Reset Password - Axxess Home Health Management Software", bodyText);
                return true;
            }
            return false;
        }

        public bool UpdatePassword(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string newPasswordSalt = string.Empty;
                string newPasswordHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Password, out newPasswordHash, out newPasswordSalt);
                login.PasswordSalt = newPasswordSalt;
                login.PasswordHash = newPasswordHash;
                if (loginRepository.Update(login))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region Private Methods

        private List<Guid> GetPhysicianIdentifiers(IList<AgencyPhysician> physicianList)
        {
            var idList = new List<Guid>();

            physicianList.ForEach(p =>
            {
                idList.Add(p.Id);
            });

            return idList;
        }

        #endregion

    }
}
