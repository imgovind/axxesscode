﻿namespace Axxess.Physician.App
{
    using System;
    using System.Web.Mvc;

    using StructureMap;
    using StructureMap.Configuration.DSL;

    using Axxess.Membership.Logging;

    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Repositories;

    using Axxess.OasisC.Repositories;

    using Security;

    using Axxess.LookUp.Repositories;

    public class PhysicianApplicationRegistry : Registry
    {
        public PhysicianApplicationRegistry()
        {
            Scan(x =>
            {
                x.TheCallingAssembly();
                x.AddAllTypesOf<IController>();
                x.WithDefaultConventions();
            });

            For<ILog>().Use<DatabaseLog>();
            For<IMembershipDataProvider>().Use<MembershipDataProvider>();
            For<ILookUpDataProvider>().Use<LookUpDataProvider>();
            For<IAgencyManagementDataProvider>().Use<AgencyManagementDataProvider>();
            For<IOasisCDataProvider>().Use<OasisCDataProvider>();
            For<IPhysicianMembershipService>().Use<PhysicianMembershipService>();
            For<IFormsAuthenticationService>().Use<FormsAuthenticationService>();
        }
    }
}
