﻿namespace Axxess.Physician.App
{
    using System;

    using Axxess.Core.Infrastructure;

    public class AccountViewData : JsonViewData
    {
        public bool isLocked { get; set; }
        public string redirectUrl { get; set; }
    }
}
