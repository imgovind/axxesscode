﻿namespace Axxess.Api.Services
{
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;

    public class ReportService : BaseService, IReportService
    {
        #region IReportService Members
        
        public List<Dictionary<string, string>> CahpsExport(Guid agencyId, int sampleMonth, int sampleYear)
        {
            return CahpsExportByPaymentSources(agencyId, sampleMonth, sampleYear, new List<int>());
        }

        public List<Dictionary<string, string>> CahpsExportByPaymentSources(Guid agencyId, int sampleMonth, int sampleYear, List<int> paymentSources)
        {
            var collection = new List<Dictionary<string, string>>();
            IDataSpecification dataSpecification = null;

            try
            {
                var agency = Reports.GetAgency(agencyId);
                if (agency != null)
                {
                    var vendorName = agency.CahpsVendor.ToEnum<CahpsVendors>(CahpsVendors.DSSResearch).GetDescription();
                    Windows.EventLog.WriteEntry(string.Format("Pulling CAHPS for {0} with {1}", agency.Name, vendorName), System.Diagnostics.EventLogEntryType.Information);

                    switch (agency.CahpsVendor)
                    {
                        case (int)CahpsVendors.DSSResearch: // DSS Research
                            dataSpecification = new DssResearchDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)CahpsVendors.ArkansasFoundationMedicalCare: // Arkansas
                            break;
                        case (int)CahpsVendors.Novaetus: // Novaetus
                            dataSpecification = new NovaetusDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)CahpsVendors.Deyta: // Deyta
                        case (int)CahpsVendors.FieldsResearch: // Fields Research
                            dataSpecification = new DeytaDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)CahpsVendors.Pinnacle: // Pinnacle
                            dataSpecification = new PinnacleDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)CahpsVendors.PressGaney: // PressGaney
                            dataSpecification = new PressGaneyDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)CahpsVendors.Ocs: // Ocs
                            dataSpecification = new OcsDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)CahpsVendors.Fazzi: // Fazzi
                            dataSpecification = new FazziDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        case (int)CahpsVendors.Shp: // Strategic Healthcare
                            dataSpecification = new ShpDataSpecification(agency, sampleMonth, sampleYear);
                            collection = dataSpecification.GetItems(paymentSources);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Windows.EventLog.WriteEntry(ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                throw ex;
            }

            return collection;
        }

        #endregion
    }
}