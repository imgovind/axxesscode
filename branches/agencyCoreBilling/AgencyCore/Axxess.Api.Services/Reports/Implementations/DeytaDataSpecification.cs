﻿namespace Axxess.Api.Services
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    using Axxess.AgencyManagement.Enums;

    internal class DeytaDataSpecification : IDataSpecification
    {
        #region Constructor And Internal Members

        private int sampleYear;
        private int sampleMonth;
        private AgencyData agency;

        internal DeytaDataSpecification(AgencyData agency, int sampleMonth, int sampleYear)
        {
            this.agency = agency;
            this.sampleYear = sampleYear;
            this.sampleMonth = sampleMonth;
        }

        #endregion

        #region IDataSpecification Members

        public List<Dictionary<string, string>> GetItems(List<int> paymentSources)
        {
            var result = new List<Dictionary<string, string>>();
            if (agency != null)
            {
                var sampleMonthLast = Reports.GetEndOfMonth(sampleMonth, sampleYear);
                var sampleMonthFirst = Reports.GetStartOfMonth(sampleMonth, sampleYear);
                var sampleLastMonthFirst = Reports.GetStartOfLastMonth(sampleMonth, sampleYear);

                List<EpisodeData> servedEpisodes = null;
                if (paymentSources != null && paymentSources.Count > 0)
                {
                    servedEpisodes = Reports.GetEpisodesBetween(agency.Id, sampleMonthFirst, sampleMonthLast, paymentSources);
                }
                else
                {
                    servedEpisodes = Reports.GetEpisodesBetween(agency.Id, sampleMonthFirst, sampleMonthLast);
                }
                if (servedEpisodes != null && servedEpisodes.Count > 0)
                {
                    Windows.EventLog.WriteEntry(string.Format("Patient Episodes for Agency: {0}", servedEpisodes.Count), System.Diagnostics.EventLogEntryType.Information);
                    var patientIds = servedEpisodes.Select(e => e.PatientId).Distinct().ToList();
                    if (patientIds != null && patientIds.Count > 0)
                    {
                        var totalServed = patientIds.Count;
                        Windows.EventLog.WriteEntry(string.Format("Total Patients Served: {0}", totalServed), System.Diagnostics.EventLogEntryType.Information);
                        patientIds.ForEach(patientId =>
                        {
                            var episodes = servedEpisodes.Where(e => e.PatientId == patientId).ToList();
                            if (episodes != null && episodes.Count > 0)
                            {
                                var skilledVisitCount = Reports.SkilledVisitCount(episodes, sampleMonthFirst, sampleMonthLast);
                                var lookbackVisitCount = Reports.SkilledVisitCount(agency.Id, patientId, sampleLastMonthFirst, sampleMonthLast);

                                var patientData = episodes.FirstOrDefault();
                                var assessment = Reports.GetEpisodeAssessment(patientData, sampleMonthLast);
                                if (assessment != null)
                                {
                                    var assessmentData = assessment.ToDictionary();
                                    var collection = new Dictionary<string, string>();

                                    collection.Add("Provider Name", (agency.Name.IsNotNullOrEmpty() ? agency.Name : string.Empty));
                                    collection.Add("Provider Id", assessmentData.AnswerOrEmptyString("M0010CertificationNumber"));
                                    collection.Add("Unit", string.Empty);
                                    collection.Add("Team", string.Empty);
                                    collection.Add("NPI", assessmentData.AnswerOrDefault("M0018NationalProviderId", agency.NationalProviderNumber.IsNotNullOrEmpty() ? agency.NationalProviderNumber : string.Empty));
                                    collection.Add("Branch ID Number", assessmentData.AnswerOrEmptyString("M0016BranchId"));
                                    collection.Add("Patient ID Number", assessmentData.AnswerOrEmptyString("M0020PatientIdNumber"));
                                    collection.Add("Patient First Name", assessmentData.AnswerOrEmptyString("M0040FirstName"));
                                    collection.Add("Patient Middle Initial", assessmentData.AnswerOrEmptyString("M0040MI"));
                                    collection.Add("Patient Last Name", assessmentData.AnswerOrEmptyString("M0040LastName"));
                                    collection.Add("Patient Suffix", assessmentData.AnswerOrEmptyString("M0040Suffix"));
                                    collection.Add("Patient Mailing Address1", patientData.AddressLine1.IsNotNullOrEmpty() ? patientData.AddressLine1 : string.Empty);
                                    collection.Add("Patient Mailing Address2", patientData.AddressLine2.IsNotNullOrEmpty() ? patientData.AddressLine2 : string.Empty);
                                    collection.Add("Patient Mailing City", patientData.AddressCity.IsNotNullOrEmpty() ? patientData.AddressCity : string.Empty);
                                    collection.Add("Patient Mailing State", assessmentData.AnswerOrDefault("M0050PatientState", patientData.AddressStateCode.IsNotNullOrEmpty() ? patientData.AddressStateCode : string.Empty));
                                    collection.Add("Patient Mailing Zip Code", assessmentData.AnswerOrDefault("M0060PatientZipCode", patientData.AddressZipCode.IsNotNullOrEmpty() ? patientData.AddressZipCode : string.Empty));
                                    collection.Add("Patient Phone Number", patientData.PhoneHome.IsNotNullOrEmpty() ? patientData.PhoneHome : string.Empty);
                                    collection.Add("Medicare Number", assessmentData.AnswerOrDefault("M0063PatientMedicareNumber", patientData.MedicareNumber.IsNotNullOrEmpty() ? patientData.MedicareNumber : string.Empty));
                                    collection.Add("Medicaid Number", assessmentData.AnswerOrDefault("M0065PatientMedicaidNumber", patientData.MedicaidNumber.IsNotNullOrEmpty() ? patientData.MedicaidNumber : string.Empty));
                                    collection.Add("Patient Gender", assessmentData.AnswerOrDefault("M0069Gender", string.Empty));
                                    collection.Add("Birth Date", assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsDate() && assessmentData.AnswerOrEmptyString("M0066PatientDoB").ToDateTime().IsValid() ? assessmentData.AnswerOrEmptyString("M0066PatientDoB").ToDateTime().ToString("MMddyyyy") : string.Empty);
                                    collection.Add("Discharge Date", patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate.ToString("MMddyyyy") : string.Empty);
                                    collection.Add("Sample Month", sampleMonthFirst.ToString("MM"));
                                    collection.Add("Sample Year", sampleYear.ToString());
                                    collection.Add("Month_1_Visits", skilledVisitCount);
                                    collection.Add("Month_2_Visits", lookbackVisitCount);
                                    collection.Add("Admission Source1", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").IsEqual("1") ? "1" : "0");
                                    collection.Add("Admission Source2", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").IsEqual("1") ? "1" : "0");
                                    collection.Add("Admission Source3", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIPPS").IsEqual("1") ? "1" : "0");
                                    collection.Add("Admission Source4", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").IsEqual("1") ? "1" : "0");
                                    collection.Add("Admission Source5", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").IsEqual("1") ? "1" : "0");
                                    collection.Add("Admission Source6", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesPhych").IsEqual("1") ? "1" : "0");
                                    collection.Add("Admission Source7", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").IsEqual("1") ? "1" : "0");
                                    collection.Add("Admission Source8", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesNone").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer1", assessmentData.AnswerOrEmptyString("M0150PaymentSourceNone").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer2", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer3", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer4", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer5", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer6", assessmentData.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer7", assessmentData.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer8", assessmentData.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer9", assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVINS").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer10", assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer11", assessmentData.AnswerOrEmptyString("M0150PaymentSourceSelfPay").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer12", assessmentData.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer13", assessmentData.AnswerOrEmptyString("M0150PaymentSourceUnknown").IsEqual("1") ? "1" : "0");
                                    collection.Add("Primary Diagnosis", assessmentData.AnswerOrDefault("M1020ICD9M", string.Empty));
                                    collection.Add("ICD9-1", assessmentData.AnswerOrDefault("M1022ICD9M1", string.Empty));
                                    collection.Add("ICD9-2", assessmentData.AnswerOrDefault("M1022ICD9M2", string.Empty));
                                    collection.Add("ICD9-3", assessmentData.AnswerOrDefault("M1022ICD9M3", string.Empty));
                                    collection.Add("ICD9-4", assessmentData.AnswerOrDefault("M1022ICD9M4", string.Empty));
                                    collection.Add("ICD9-5", assessmentData.AnswerOrDefault("M1022ICD9M5", string.Empty));
                                    collection.Add("Surgical Discharge", assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode1").IsNotNullOrEmpty() || assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode2").IsNotNullOrEmpty() ? "1" : "0");
                                    collection.Add("ESRD", CheckForEndStageRenalDisiease(assessmentData));
                                    collection.Add("ADL Deficits", CheckForADLDeficits(assessmentData));
                                    collection.Add("ADL Upper", assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper")).ToString() : "M");
                                    collection.Add("ADL Lower", assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower")).ToString() : "M");
                                    collection.Add("ADL Bathe", assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody")).ToString() : "M");
                                    collection.Add("ADL Toilet", assessmentData.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1840ToiletTransferring")).ToString() : "M");
                                    collection.Add("ADL Transferring", assessmentData.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1850Transferring")).ToString() : "M");
                                    collection.Add("ADL Feeding", assessmentData.AnswerOrEmptyString("M1870FeedingOrEating").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1870FeedingOrEating")).ToString() : "M");
                                    collection.Add("Death Date", "M");
                                    collection.Add("Maternity Only", "M");
                                    collection.Add("Hospice patient", assessmentData.AnswerOrEmptyString("M2410TypeOfInpatientFacility4").IsEqual("1") ? "Yes" : (assessmentData.AnswerOrEmptyString("M2410TypeOfInpatientFacility4").IsEqual("0") ? "No" : "M"));
                                    collection.Add("Primary Language", GetLanguage(assessmentData.AnswerOrEmptyString("GenericPrimaryLanguage")));
                                    collection.Add("Patients served", totalServed.ToString());
                                    collection.Add("No publicity", "M");

                                    result.Add(collection);
                                }
                            }
                        });
                    }
                }
                else
                {
                    Windows.EventLog.WriteEntry(string.Format("No Patients found for AgencyId: {0}", agency.Name), System.Diagnostics.EventLogEntryType.Warning);
                }
            }
            else
            {
                Windows.EventLog.WriteEntry("Agency is null.", System.Diagnostics.EventLogEntryType.Error);
            }

            return result;
        }

        #endregion

        #region Private Methods

        private string GetLanguage(string language)
        {
            var result = "M";
            if (language.IsNotNullOrEmpty())
            {
                if (language.ToLowerInvariant().Contains("english"))
                {
                    result = "1";
                }
                else if (language.ToLowerInvariant().Contains("spanish"))
                {
                    result = "2";
                }
                else if (language.ToLowerInvariant().Contains("chinese"))
                {
                    result = "3";
                }
                else if (language.ToLowerInvariant().Contains("russian"))
                {
                    result = "4";
                }
                else if (language.ToLowerInvariant().Contains("vietnamese"))
                {
                    result = "5";
                }
            }
            return result;
        }

        private string CheckForADLDeficits(IDictionary<string, Question> data)
        {
            var result = 0;

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty())
                {
                    var dressUpperArray = data["M1810CurrentAbilityToDressUpper"].Answer.Split(',');
                    if (dressUpperArray != null && dressUpperArray.Length > 0)
                    {
                        dressUpperArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty())
                {
                    var dressLowerArray = data["M1820CurrentAbilityToDressLower"].Answer.Split(',');
                    if (dressLowerArray != null && dressLowerArray.Length > 0)
                    {
                        dressLowerArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty())
                {
                    var bathArray = data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',');
                    if (bathArray != null && bathArray.Length > 0)
                    {
                        bathArray.ForEach(b =>
                        {
                            if (b.IsEqual("02") || b.IsEqual("03") || b.IsEqual("04") || b.IsEqual("05") || b.IsEqual("06"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty())
                {
                    var toiletArray = data["M1840ToiletTransferring"].Answer.Split(',');
                    if (toiletArray != null && toiletArray.Length > 0)
                    {
                        toiletArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty())
                {
                    var bedArray = data["M1850Transferring"].Answer.Split(',');
                    if (bedArray != null && bedArray.Length > 0)
                    {
                        bedArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04") || t.IsEqual("05"))
                            {
                                result++;
                            }
                        });
                    }
                }
            }
            return result.ToString();
        }

        private string CheckForEndStageRenalDisiease(IDictionary<string, Question> data)
        {
            var result = "2";

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1020ICD9M").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA3").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA4").IsEqual("585.6"))
                {
                    result = "1";
                }
                else
                {
                    int icd9counter = 1;
                    do
                    {
                        var key = "M1022ICD9M" + icd9counter;
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "3";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "4";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        icd9counter++;
                    }
                    while (icd9counter < 12);
                }
            }

            return result;
        }

        #endregion
    }
}
