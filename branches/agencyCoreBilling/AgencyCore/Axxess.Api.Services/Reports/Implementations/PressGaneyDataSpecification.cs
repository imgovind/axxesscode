﻿namespace Axxess.Api.Services
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;

    internal class PressGaneyDataSpecification : IDataSpecification
    {
        #region Constructor And Internal Members

        private int sampleYear;
        private int sampleMonth;
        private AgencyData agency;

        internal PressGaneyDataSpecification(AgencyData agency, int sampleMonth, int sampleYear)
        {
            this.agency = agency;
            this.sampleYear = sampleYear;
            this.sampleMonth = sampleMonth;
        }

        #endregion

        #region IDataSpecification Members

        public List<Dictionary<string, string>> GetItems(List<int> paymentSources)
        {
            var result = new List<Dictionary<string, string>>();
            if (agency != null)
            {
                var sampleMonthLast = Reports.GetEndOfMonth(sampleMonth, sampleYear);
                var sampleMonthFirst = Reports.GetStartOfMonth(sampleMonth, sampleYear);
                var sampleLastMonthFirst = Reports.GetStartOfLastMonth(sampleMonth, sampleYear);

                List<EpisodeData> servedEpisodes = null;
                if (paymentSources != null && paymentSources.Count > 0)
                {
                    servedEpisodes = Reports.GetEpisodesBetween(agency.Id, sampleMonthFirst, sampleMonthLast, paymentSources);
                }
                else
                {
                    servedEpisodes = Reports.GetEpisodesBetween(agency.Id, sampleMonthFirst, sampleMonthLast);
                }
                if (servedEpisodes != null && servedEpisodes.Count > 0)
                {
                    Windows.EventLog.WriteEntry(string.Format("Patient Episodes for Agency: {0}", servedEpisodes.Count), System.Diagnostics.EventLogEntryType.Information);
                    var patientIds = servedEpisodes.Select(e => e.PatientId).Distinct().ToList();
                    if (patientIds != null && patientIds.Count > 0)
                    {
                        var totalServed = patientIds.Count;
                        Windows.EventLog.WriteEntry(string.Format("Total Patients Served: {0}", totalServed), System.Diagnostics.EventLogEntryType.Information);
                        patientIds.ForEach(patientId =>
                        {
                            var episodes = servedEpisodes.Where(e => e.PatientId == patientId).ToList();
                            if (episodes != null && episodes.Count > 0)
                            {
                                var scheduleEvents = Reports.AllSkilledVisits(episodes, sampleMonthFirst, sampleMonthLast);
                                var skilledVisitCount = Reports.SkilledVisitCount(episodes, sampleMonthFirst, sampleMonthLast);
                                var lookbackVisitCount = Reports.SkilledVisitCount(agency.Id, patientId, sampleLastMonthFirst, sampleMonthLast);
                                
                                var patientData = episodes.FirstOrDefault();
                                var assessment = Reports.GetEpisodeAssessment(patientData, sampleMonthLast);
                                if (assessment != null)
                                {
                                    var assessmentData = assessment.ToDictionary();
                                    var collection = new Dictionary<string, string>();

                                    collection.Add("Survey_Designator", agency.CahpsSurveyDesignator);
                                    collection.Add("Client_Number", agency.CahpsVendorClientId);
                                    collection.Add("Pat_First_Name", assessmentData.AnswerOrDefault("M0040FirstName", string.Empty));
                                    collection.Add("Pat_MI", assessmentData.AnswerOrEmptyString("M0040MI"));
                                    collection.Add("Pat_Last_Name", assessmentData.AnswerOrDefault("M0040LastName", string.Empty));
                                    collection.Add("Pat_Mailing_Address_1", patientData.AddressLine1.IsNotNullOrEmpty() ? patientData.AddressLine1 : string.Empty);
                                    collection.Add("Pat_Mailing_Address_2", patientData.AddressLine2.IsNotNullOrEmpty() ? patientData.AddressLine2 : string.Empty);
                                    collection.Add("Pat_Address_City", patientData.AddressCity.IsNotNullOrEmpty() ? patientData.AddressCity : string.Empty);
                                    collection.Add("Pat_Address_State", assessmentData.AnswerOrDefault("M0050PatientState", string.Empty));
                                    collection.Add("Pat_Address_Zip", assessmentData.AnswerOrDefault("M0060PatientZipCode", string.Empty));
                                    collection.Add("Pat_Phone", patientData.PhoneHome.IsNotNullOrEmpty() ? patientData.PhoneHome : string.Empty);

                                    collection.Add("HH_Visit_Type", string.Empty);

                                    collection.Add("SOC", assessmentData.AnswerOrEmptyString("M0030SocDate").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0030SocDate").IsDate() ? assessmentData.AnswerOrEmptyString("M0030SocDate").ToDateTime().ToString("MM/dd/yyyy") : string.Empty);
                                    collection.Add("RFA", assessmentData.AnswerOrDefault("M0100AssessmentType", string.Empty).Replace(",on", ""));
                                    collection.Add("Discharge_Date", patientData.Status == (int)PatientStatus.Discharged ? patientData.DischargeDate.ToString("MM/dd/yyyy") : string.Empty);
                                    collection.Add("DOB", assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsNotNullOrEmpty() && assessmentData.AnswerOrEmptyString("M0066PatientDoB").IsDate() ? assessmentData.AnswerOrEmptyString("M0066PatientDoB").ToDateTime().ToString("MM/dd/yyyy") : string.Empty);

                                    collection.Add("Language", GetLanguage(assessmentData.AnswerOrEmptyString("GenericPrimaryLanguage")));
                                    collection.Add("Gender", assessmentData.AnswerOrDefault("M0069Gender", string.Empty));
                                    collection.Add("Pat_Med_Rec", assessmentData.AnswerOrEmptyString("M0020PatientIdNumber"));
                                    collection.Add("Surgical_DC", assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode1").IsNotNullOrEmpty() || assessmentData.AnswerOrEmptyString("485SurgicalProcedureCode2").IsNotNullOrEmpty() ? "1" : "M");
                                    collection.Add("ESRD", CheckForEndStageRenalDisiease(assessmentData));

                                    collection.Add("Payer_None", assessmentData.AnswerOrEmptyString("M0150PaymentSourceNone").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Medicare_FFS", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Medicare_HMO", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Medicaid_FFS", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Medicaid_HMO", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Workers_Comp", assessmentData.AnswerOrEmptyString("M0150PaymentSourceWRKCOMP").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Title_Programs", assessmentData.AnswerOrEmptyString("M0150PaymentSourceTITLPRO").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Other_Government", assessmentData.AnswerOrEmptyString("M0150PaymentSourceOTHGOVT").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Private_Ins", assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVINS").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Private_HMO", assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_SelfPay", assessmentData.AnswerOrEmptyString("M0150PaymentSourceSelfPay").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Other", assessmentData.AnswerOrEmptyString("M0150PaymentSourceOtherSRS").IsEqual("1") ? "1" : "0");
                                    collection.Add("Payer_Unknown", assessmentData.AnswerOrEmptyString("M0150PaymentSourceUnknown").IsEqual("1") ? "1" : "0");

                                    collection.Add("HMO_Indicator", assessmentData.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1") || assessmentData.AnswerOrEmptyString("M0150PaymentSourcePRVHMO").IsEqual("1") ? "1" : "2");

                                    collection.Add("Adm_NF", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTC").IsEqual("1") ? "1" : "0");
                                    collection.Add("Adm_SNF", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesSNF").IsEqual("1") ? "1" : "0");
                                    collection.Add("Adm_IPP_S", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIPPS").IsEqual("1") ? "1" : "0");
                                    collection.Add("Adm_LTCH", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesLTCH").IsEqual("1") ? "1" : "0");
                                    collection.Add("Adm_IRF", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesIRF").IsEqual("1") ? "1" : "0");
                                    collection.Add("Adm_Psych", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesPhych").IsEqual("1") ? "1" : "0");
                                    collection.Add("Adm_Other", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesOTHR").IsEqual("1") ? "1" : "0");
                                    collection.Add("Adm_None", assessmentData.AnswerOrEmptyString("M1000InpatientFacilitiesNone").IsEqual("1") ? "1" : "0");

                                    collection.Add("Dual_Eligible", CheckForDualElligibility(assessmentData));

                                    collection.Add("Primary_Diagnosis", assessmentData.AnswerOrDefault("M1020ICD9M", string.Empty).Replace(".", ""));
                                    collection.Add("Other_Diagnosis_1", assessmentData.AnswerOrDefault("M1022ICD9M1", string.Empty).Replace(".", ""));
                                    collection.Add("Other_Diagnosis_2", assessmentData.AnswerOrDefault("M1022ICD9M2", string.Empty).Replace(".", ""));
                                    collection.Add("Other_Diagnosis_3", assessmentData.AnswerOrDefault("M1022ICD9M3", string.Empty).Replace(".", ""));
                                    collection.Add("Other_Diagnosis_4", assessmentData.AnswerOrDefault("M1022ICD9M4", string.Empty).Replace(".", ""));
                                    collection.Add("Other_Diagnosis_5", assessmentData.AnswerOrDefault("M1022ICD9M5", string.Empty).Replace(".", ""));

                                    collection.Add("Referral_Source", "M");

                                    collection.Add("PT", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 && scheduleEvents.Where(s => s.Discipline == Disciplines.PT.ToString()).ToList().Count > 0 ? 1 : 2));
                                    collection.Add("HHA", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 && scheduleEvents.Where(s => s.Discipline == Disciplines.HHA.ToString()).ToList().Count > 0 ? 1 : 2));
                                    collection.Add("SS", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 && scheduleEvents.Where(s => s.Discipline == Disciplines.MSW.ToString()).ToList().Count > 0 ? 1 : 2));
                                    collection.Add("OT", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 && scheduleEvents.Where(s => s.Discipline == Disciplines.OT.ToString()).ToList().Count > 0 ? 1 : 2));
                                    collection.Add("Comp_Homemaker", string.Format("{0}", 2));
                                    collection.Add("ST", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 && scheduleEvents.Where(s => s.Discipline == Disciplines.ST.ToString()).ToList().Count > 0 ? 1 : 2));

                                    collection.Add("ADL_Dress_Upper", assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper")).ToString() : string.Empty);
                                    collection.Add("ADL_Dress_Lower", assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1820CurrentAbilityToDressLower")).ToString() : string.Empty);
                                    collection.Add("ADL_Bathing", assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody")).ToString() : string.Empty);
                                    collection.Add("ADL_Toileting", assessmentData.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1840ToiletTransferring")).ToString() : string.Empty);
                                    collection.Add("ADL_Transferring", assessmentData.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1850Transferring")).ToString() : string.Empty);
                                    collection.Add("ADL_Feed", assessmentData.AnswerOrEmptyString("M1870FeedingOrEating").IsNotNullOrEmpty() ? int.Parse(assessmentData.AnswerOrEmptyString("M1870FeedingOrEating")).ToString() : string.Empty);

                                    collection.Add("Num_Skilled_Visits", string.Format("{0}", scheduleEvents != null && scheduleEvents.Count > 0 ? scheduleEvents.Where(s => s.IsSkilledCare()).ToList().Count.ToString() : ""));
                                    collection.Add("Lookback_Period_Visits", lookbackVisitCount);
                                    collection.Add("Sample_Month", sampleMonthFirst.ToString("MM"));
                                    collection.Add("Sample_Year", sampleYear.ToString());

                                    collection.Add("Branch_ID", agency.Name.IsNotNullOrEmpty() ? agency.Name : string.Empty);
                                    collection.Add("Team", string.Empty);
                                    collection.Add("Number_Pat_Served", totalServed.ToString());
                                    collection.Add("EOR", "$");

                                    result.Add(collection);
                                }
                            }
                        });
                    }
                }
                else
                {
                    Windows.EventLog.WriteEntry(string.Format("No Patients found for AgencyId: {0}", agency.Name), System.Diagnostics.EventLogEntryType.Warning);
                }
            }
            else
            {
                Windows.EventLog.WriteEntry("Agency is null.", System.Diagnostics.EventLogEntryType.Error);
            }

            return result;
        }
        
        #endregion

        #region Private Methods

        private string GetLanguage(string language)
        {
            var result = "M";
            if (language.IsNotNullOrEmpty())
            {
                if (language.ToLowerInvariant().Contains("english"))
                {
                    result = "1";
                }
                else if (language.ToLowerInvariant().Contains("spanish"))
                {
                    result = "2";
                }
            }
            return result;
        }

        private string CheckForADLDeficits(IDictionary<string, Question> data)
        {
            var result = 0;

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1810CurrentAbilityToDressUpper").IsNotNullOrEmpty())
                {
                    var dressUpperArray = data["M1810CurrentAbilityToDressUpper"].Answer.Split(',');
                    if (dressUpperArray != null && dressUpperArray.Length > 0)
                    {
                        dressUpperArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1820CurrentAbilityToDressLower").IsNotNullOrEmpty())
                {
                    var dressLowerArray = data["M1820CurrentAbilityToDressLower"].Answer.Split(',');
                    if (dressLowerArray != null && dressLowerArray.Length > 0)
                    {
                        dressLowerArray.ForEach(d =>
                        {
                            if (d.IsEqual("02") || d.IsEqual("03"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1830CurrentAbilityToBatheEntireBody").IsNotNullOrEmpty())
                {
                    var bathArray = data["M1830CurrentAbilityToBatheEntireBody"].Answer.Split(',');
                    if (bathArray != null && bathArray.Length > 0)
                    {
                        bathArray.ForEach(b =>
                        {
                            if (b.IsEqual("02") || b.IsEqual("03") || b.IsEqual("04") || b.IsEqual("05") || b.IsEqual("06"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1840ToiletTransferring").IsNotNullOrEmpty())
                {
                    var toiletArray = data["M1840ToiletTransferring"].Answer.Split(',');
                    if (toiletArray != null && toiletArray.Length > 0)
                    {
                        toiletArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04"))
                            {
                                result++;
                            }
                        });
                    }
                }

                if (data.AnswerOrEmptyString("M1850Transferring").IsNotNullOrEmpty())
                {
                    var bedArray = data["M1850Transferring"].Answer.Split(',');
                    if (bedArray != null && bedArray.Length > 0)
                    {
                        bedArray.ForEach(t =>
                        {
                            if (t.IsEqual("01") || t.IsEqual("02") || t.IsEqual("03") || t.IsEqual("04") || t.IsEqual("05"))
                            {
                                result++;
                            }
                        });
                    }
                }
            }
            return result.ToString();
        }

        private string CheckForEndStageRenalDisiease(IDictionary<string, Question> data)
        {
            var result = "2";

            if (data != null)
            {
                if (data.AnswerOrEmptyString("M1020ICD9M").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA3").IsEqual("585.6"))
                {
                    result = "1";
                }
                else if (data.AnswerOrEmptyString("M1024ICD9MA4").IsEqual("585.6"))
                {
                    result = "1";
                }
                else
                {
                    int icd9counter = 1;
                    do
                    {
                        var key = "M1022ICD9M" + icd9counter;
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "3";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        key = "M1024ICD9M" + (char)(icd9counter + 65) + "4";
                        if (data.AnswerOrEmptyString(key).IsEqual("585.6"))
                        {
                            result = "1";
                            break;
                        }
                        icd9counter++;
                    }
                    while (icd9counter < 12);
                }
            }

            return result;
        }

        private string CheckForDualElligibility(IDictionary<string, Question> data)
        {
            var result = "2";

            if (data != null)
            {
                if ((data.AnswerOrEmptyString("M0150PaymentSourceMCREFFS").IsEqual("1") || data.AnswerOrEmptyString("M0150PaymentSourceMCREHMO").IsEqual("1"))
                   && (data.AnswerOrEmptyString("M0150PaymentSourceMCAIDFFS").IsEqual("1") || data.AnswerOrEmptyString("M0150PaymentSourceMACIDHMO").IsEqual("1")))
                {
                    result = "1";
                }
            }

            return result;
        }

        #endregion
    }
}
