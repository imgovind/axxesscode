﻿namespace Axxess.Api.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.Api.Contracts;
    using Axxess.Core.Extension;

    public class AuthenticationService : BaseService, IAuthenticationService
    {
        #region IAuthenticationService Members

        public int Count()
        {
            return ActivityMonitor.Instance.Count;
        }

        public List<SingleUser> ToList()
        {
            return ActivityMonitor.Instance.AsList();
        }

        public void SignIn(SingleUser user)
        {
            ActivityMonitor.Instance.Add(user);
        }

        public void Log(SingleUser user)
        {
            ActivityMonitor.Instance.Log(user);
        }

        public SingleUser Get(Guid loginId)
        {
            return ActivityMonitor.Instance.Get(loginId);
        }

        public void SignOut(string emailAddress)
        {
            ActivityMonitor.Instance.Expire(emailAddress);
        }

        public bool Verify(SingleUser user)
        {
            var result = false;
            if (user != null)
            {
                var existingUser = ActivityMonitor.Instance.Get(user.LoginId);
                if (existingUser != null)
                {
                    if (existingUser.IsAuthenticated)
                    {
                        if (existingUser.SessionId.IsEqual(user.SessionId))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        existingUser.FullName = user.FullName;
                        existingUser.SessionId = user.SessionId;
                        existingUser.IpAddress = user.IpAddress;
                        existingUser.AgencyName = user.AgencyName;
                        existingUser.EmailAddress = user.EmailAddress;
                        existingUser.LastSecureActivity = DateTime.Now;
                        existingUser.IsAuthenticated = user.IsAuthenticated;
                        result = true;
                    }
                }
                else
                {
                    ActivityMonitor.Instance.Add(user);
                    result = true;
                }
            }
            return result;
        }

        #endregion
    }
}