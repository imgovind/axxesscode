﻿namespace Axxess.Api.Services
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Contracts;

    public sealed class PhysicianEngine
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
            }

            internal static readonly PhysicianEngine instance = new PhysicianEngine();
        }

        #endregion

        #region Public Instance

        public static PhysicianEngine Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Members

        private SafeList<UniqueEntity> physicians;
        private static readonly IAgencyManagementDataProvider dataProvider = new AgencyManagementDataProvider();

        #endregion

        #region Private Constructor / Methods

        private PhysicianEngine()
        {
            this.Load();
        }

        private void Load()
        {
            this.physicians = new SafeList<UniqueEntity>();
            var physicianList = dataProvider.PhysicianRepository.GetAllPhysicians();
            if (physicianList != null && physicianList.Count > 0)
            {
                physicianList.ForEach(physician =>
                {
                    this.physicians.Add(new UniqueEntity { Id = physician.Id, AgencyId = physician.AgencyId, Xml = physician.ToXml() });
                });
            }
        }

        private void Load(Guid agencyId)
        {
            var physicianList = dataProvider.PhysicianRepository.GetAgencyPhysicians(agencyId);
            if (physicianList != null && physicianList.Count > 0)
            {
                physicianList.ForEach(physician =>
                {
                    var item = this.physicians.Single(p => p.Id.ToString().IsEqual(physician.Id.ToString()) && p.AgencyId.ToString().IsEqual(physician.AgencyId.ToString()));
                    if (item == null)
                    {
                        this.physicians.Add(new UniqueEntity { Id = physician.Id, AgencyId = physician.AgencyId, Xml = physician.ToXml() });
                    }
                    else
                    {
                        item.Xml = physician.ToXml();
                    }
                });
            }
        }

        #endregion

        #region Public Methods

        public string Get(Guid physicianId, Guid agencyId)
        {
            var physician = this.physicians.Single(p => p.Id.ToString().IsEqual(physicianId.ToString()) && p.AgencyId.ToString().IsEqual(agencyId.ToString()));
            if (physician != null)
            {
                return physician.Xml;
            }
            return string.Empty;
        }

        public void Refresh(Guid agencyId)
        {
            Load(agencyId);
        }

        public List<string> GetPhysicians(Guid agencyId)
        {
            var items = new List<string>();
            var physicianList = this.physicians.Find(p => p.AgencyId.ToString().IsEqual(agencyId.ToString()));
            if (physicianList != null && physicianList.Count > 0)
            {
                physicianList.ForEach(p =>
                {
                    items.Add(p.Xml);
                });
                return items;
            }

            return new List<string>();
        }

        #endregion

    }
}
