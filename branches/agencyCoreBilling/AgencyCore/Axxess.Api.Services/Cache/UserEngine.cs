﻿namespace Axxess.Api.Services
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Contracts;

    public sealed class UserEngine
    {
        #region Nested Class for Singleton

        class Nested
        {
            static Nested()
            {
            }

            internal static readonly UserEngine instance = new UserEngine();
        }

        #endregion

        #region Public Instance

        public static UserEngine Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        #endregion

        #region Private Members

        private SafeList<UniqueEntity> users;
        private static readonly IAgencyManagementDataProvider dataProvider = new AgencyManagementDataProvider();

        #endregion

        #region Private Constructor / Methods

        private UserEngine()
        {
            this.Load();
        }

        private void Load()
        {
            this.users = new SafeList<UniqueEntity>();
            var userList = dataProvider.AgencyRepository.GetUserNames();
            if (userList != null && userList.Count > 0)
            {
                userList.ForEach(user =>
                {
                    this.users.Add(new UniqueEntity { Id = user.UserId, AgencyId = user.AgencyId, Name = user.DisplayName });
                });
            }
        }

        private void Load(Guid agencyId)
        {
            var userList = dataProvider.AgencyRepository.GetUserNames(agencyId);
            if (userList != null && userList.Count > 0)
            {
                userList.ForEach(user =>
                {
                    var item = this.users.Single(u => u.Id.ToString().IsEqual(user.UserId.ToString()) && u.AgencyId.ToString().IsEqual(user.AgencyId.ToString()));
                    if (item == null)
                    {
                        this.users.Add(new UniqueEntity { Id = user.UserId, AgencyId = user.AgencyId, Name = user.DisplayName });
                    }
                    else
                    {
                        item.Xml = user.ToXml();
                    }
                });
            }
        }

        #endregion

        #region Public Methods

        public string GetName(Guid userId, Guid agencyId)
        {
            var user = this.users.Single(p => p.Id.ToString().IsEqual(userId.ToString()) && p.AgencyId.ToString().IsEqual(agencyId.ToString()));
            if (user != null)
            {
                return user.Name;
            }
            return string.Empty;
        }

        public void Refresh(Guid agencyId)
        {
            Load(agencyId);
        }

        #endregion

    }
}
