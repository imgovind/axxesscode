﻿namespace Axxess.ConsoleApp
{
    using System;

    public class PecosData
    {
        #region Members

        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", this.Id, this.FirstName, this.LastName);
        }
        #endregion
    }
}
