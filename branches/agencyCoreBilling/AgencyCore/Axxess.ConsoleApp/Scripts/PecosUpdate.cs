﻿using System;
using System.IO;
using System.Data;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;
using Axxess.Core.Infrastructure;

using System.Threading;

namespace Axxess.ConsoleApp.Tests
{
    public static class PecosUpdate
    {
        private static string input = @"C:\\Projects\\PECOS_20110921\\OrderReferring_20110919.csv";
        private static string output = string.Format(@"C:\\Projects\\PECOS_20110921\\OrderReferring_20110919_errorlog_{0}.txt", DateTime.Now.Ticks.ToString());

        public static void Run()
        {
            int counter = 1;
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                    {
                        using (var csvReader = new CsvReader(fileStream))
                        {
                            if (csvReader != null)
                            {
                                foreach (var record in csvReader.DataRecords)
                                {
                                    var pecos = new PecosData();
                                    pecos.Id = record.GetValue(0);
                                    pecos.LastName = record.GetValue(1).Replace("(", "").Replace(")", "").Replace("'", "`");
                                    pecos.FirstName = record.GetValue(2).Replace("(", "").Replace(")", "").Replace("'", "`");

                                    Console.WriteLine("Inserting Row {0}: {1}", counter.ToString(), pecos.ToString());
                                    var sql = new PecosScript(pecos).ToString();
                                    using (var cmd = new FluentCommand<int>(sql))
                                    {
                                        cmd.SetConnection("AxxessLookupConnectionString");
                                        if (cmd.AsNonQuery() > 0)
                                        {
                                            Console.WriteLine("Inserted Row {0}: {1}", counter.ToString(), pecos.ToString());
                                        }
                                        else
                                        {
                                            Console.WriteLine("Failed to insert Row {0}: {1}", counter.ToString(), pecos.ToString());
                                            textWriter.WriteLine("Failed to insert Row {0}: {1}", counter.ToString(), pecos.ToString());
                                        }
                                    }
                                    Console.WriteLine();
                                    counter++;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    textWriter.WriteLine(ex.ToString());
                }
            }
        }
    }
}
