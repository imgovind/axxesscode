﻿namespace Axxess.ConsoleApp.Tests
{
    using System;
    using System.IO;
    using System.Data;

    using Excel;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;

    using Axxess.Core.Extension;

    public static class KinnserPatientIntake
    {
        private static Sheet sheet;
        private static HSSFWorkbook workBook;
        private static string input = Path.Combine(App.Root, "Files\\avelina.xls");
        private static string insertoutput = Path.Combine(App.Root, string.Format("Files\\avelina_Insert_{0}.txt", DateTime.Now.Ticks.ToString()));
        private static string npioutput = Path.Combine(App.Root, string.Format("Files\\avelina_Physician_{0}.xls", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            Initialize();
            using (TextWriter textWriter = new StreamWriter(insertoutput, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                int i = 1;
                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    var patientData = new PatientData();
                                    patientData.AgencyId = "6d80ae1f-410d-408f-b342-051e956b037f";
                                    patientData.AgencyLocationId = "c4c12e9e-4443-42f5-aab4-03fdb876bd41";
                                    patientData.PatientStatusId = "1";
                                    patientData.PatientNumber = dataRow.GetValue(0);
                                    patientData.LastName = dataRow.GetValue(1).ToTitleCase();
                                    patientData.FirstName = dataRow.GetValue(2).ToTitleCase();
                                    patientData.Gender = dataRow.GetValue(3);
                                    patientData.MedicareNumber = dataRow.GetValue(4);
                                    patientData.MedicaidNumber = dataRow.GetValue(5);
                                    patientData.BirthDate = dataRow.GetValue(6).ToMySqlDate(0);
                                    patientData.Phone = dataRow.GetValue(7).ToPhoneDB();
                                    patientData.AddressLine1 = dataRow.GetValue(8).ToTitleCase();
                                    patientData.AddressCity = dataRow.GetValue(9).ToTitleCase();
                                    patientData.AddressState = dataRow.GetValue(10);
                                    patientData.AddressZipCode = dataRow.GetValue(11).Substring(0, 5);
                                    patientData.StartofCareDate = dataRow.GetValue(12).ToMySqlDate(0);

                                    var episodeEndDate = dataRow.GetValue(14).ToDateTime();
                                    if (episodeEndDate.Year < 2011)
                                    {
                                        patientData.PatientStatusId = "2";
                                    }

                                    if (dataRow.GetValue(26).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Insurance: {0}. ", dataRow.GetValue(26));
                                    }
                                    if (dataRow.GetValue(19).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Triage Level: {0}. ", dataRow.GetValue(19));
                                    }
                                    if (dataRow.GetValue(13).IsNotNullOrEmpty() && dataRow.GetValue(14).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Episode Range: {0} - {1} .", dataRow.GetValue(13), dataRow.GetValue(14));
                                    }
                                    if (dataRow.GetValue(15).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(15));
                                    }
                                    if (dataRow.GetValue(16).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Secondary Diagnosis: {0}. ", dataRow.GetValue(16));
                                    }
                                    if (dataRow.GetValue(17).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Disciplines: {0}. ", dataRow.GetValue(17));
                                    }
                                    if (dataRow.GetValue(18).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Frequencies: {0}. ", dataRow.GetValue(18));
                                    }
                                    if (dataRow.GetValue(20).IsNotNullOrEmpty() && dataRow.GetValue(21).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Physician & NPI: {0} {1}. ", dataRow.GetValue(20), dataRow.GetValue(21));
                                    }
                                    if (dataRow.GetValue(24).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Clinician: {0}. ", dataRow.GetValue(24));
                                    }
                                    if (dataRow.GetValue(32).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Case Manager: {0}. ", dataRow.GetValue(32));
                                    }
                                    if (dataRow.GetValue(30).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("Therapist: {0}. ", dataRow.GetValue(30));
                                    }
                                    if (dataRow.GetValue(31).IsNotNullOrEmpty())
                                    {
                                        patientData.Comments += string.Format("External Referral: {0}. ", dataRow.GetValue(31));
                                    }

                                    patientData.Comments = patientData.Comments.Replace("'", "");

                                    textWriter.WriteLine(new PatientScript(patientData).ToString());
                                    textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                    textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());
                                    textWriter.Write(textWriter.NewLine);

                                    Row excelRow = sheet.CreateRow(i);
                                    excelRow.CreateCell(0).SetCellValue(patientData.PatientId.ToString());
                                    excelRow.CreateCell(1).SetCellValue(patientData.PatientNumber);
                                    excelRow.CreateCell(2).SetCellValue(patientData.LastName);
                                    excelRow.CreateCell(3).SetCellValue(patientData.FirstName);
                                    excelRow.CreateCell(4).SetCellValue(dataRow.GetValue(21));
                                    excelRow.CreateCell(5).SetCellValue("");
                                    excelRow.CreateCell(6).SetCellValue(dataRow.GetValue(20).IsNotNullOrEmpty() && dataRow.GetValue(20).Split(' ').Length > 1 ? dataRow.GetValue(20).Split(' ')[1] : string.Empty);
                                    excelRow.CreateCell(7).SetCellValue(dataRow.GetValue(20).IsNotNullOrEmpty() && dataRow.GetValue(20).Split(' ').Length > 1 ? dataRow.GetValue(20).Split(' ')[0] : string.Empty);
                                    excelRow.CreateCell(8).SetCellValue(dataRow.GetValue(20));
                                    excelRow.CreateCell(9).SetCellValue("");
                                    excelRow.CreateCell(10).SetCellValue("");
                                    excelRow.CreateCell(11).SetCellValue("");
                                    excelRow.CreateCell(12).SetCellValue("");
                                    excelRow.CreateCell(13).SetCellValue(dataRow.GetValue(22));
                                    excelRow.CreateCell(14).SetCellValue(dataRow.GetValue(23));

                                    i++;
                                }
                                Write();
                            }
                        }
                    }
                }
            }
        }

        private static void Initialize()
        {
            workBook = new HSSFWorkbook();

            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = dsi;

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export";
            workBook.SummaryInformation = si;

            sheet = workBook.CreateSheet("NPI");

            Font headerFont = workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            CellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Row headerRow = sheet.CreateRow(0);
            headerRow.CreateCell(0).SetCellValue("PatientGUID");
            headerRow.CreateCell(1).SetCellValue("PatientHIC");
            headerRow.CreateCell(2).SetCellValue("PatientLastName");
            headerRow.CreateCell(3).SetCellValue("PatientFirstName");
            headerRow.CreateCell(4).SetCellValue("PhysicianNPI");
            headerRow.CreateCell(5).SetCellValue("Physiciantitle");
            headerRow.CreateCell(6).SetCellValue("Physicianlname");
            headerRow.CreateCell(7).SetCellValue("Physicianfname");
            headerRow.CreateCell(8).SetCellValue("Physicianname");
            headerRow.CreateCell(9).SetCellValue("Physicianaddr");
            headerRow.CreateCell(10).SetCellValue("Physiciancity");
            headerRow.CreateCell(11).SetCellValue("Physicianstate");
            headerRow.CreateCell(12).SetCellValue("Physicianzip");
            headerRow.CreateCell(13).SetCellValue("Physicianphone");
            headerRow.CreateCell(14).SetCellValue("Physicianfax");
            headerRow.RowStyle = headerStyle;
        }

        private static void Write()
        {
            int columnCounter = 0;
            do
            {
                sheet.AutoSizeColumn(columnCounter);
                columnCounter++;
            }
            while (columnCounter < 15);

            using (FileStream fileStream = new FileStream(npioutput, FileMode.OpenOrCreate, FileAccess.Write))
            {
                workBook.Write(fileStream);
            }
        }
    }
}
