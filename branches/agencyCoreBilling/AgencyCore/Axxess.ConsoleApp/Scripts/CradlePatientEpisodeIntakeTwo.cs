﻿using System;
using System.IO;
using System.Data;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class CradlePatientEpisodeIntakeTwo
    {
        private static string input = Path.Combine(App.Root, "Files\\fhs.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\FHS_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (var csvReader = new CsvReader(fileStream))
                    {
                        if (csvReader != null)
                        {
                            csvReader.ReadHeaderRecord();
                            foreach (var record in csvReader.DataRecords)
                            {
                                var patientData = new PatientData();
                                patientData.AgencyId = "7544a391-09de-4fd4-8e78-b3b844e999da";
                                patientData.AgencyLocationId = "1912a6f0-0b5a-4a27-83a0-905aa0893e31";
                                patientData.PatientNumber = record.GetValue(4);
                                patientData.Gender = record.GetValue(14).IsEqual("01") || record.GetValue(14).IsEqual("1") ? "Female" : "Male";
                                patientData.MedicareNumber = record.GetValue(0);
                                patientData.FirstName = record.GetValue(6).ToTitleCase();
                                patientData.LastName = record.GetValue(7).ToTitleCase();
                                patientData.MiddleInitial = "";
                                patientData.BirthDate = record.GetValue(13).ToMySqlDate(0);
                                patientData.AddressLine1 = record.GetValue(8).ToTitleCase();
                                patientData.AddressLine2 = "";
                                patientData.AddressCity = record.GetValue(9).ToTitleCase();
                                patientData.AddressState = record.GetValue(10).Replace("_","").Substring(0, 2);
                                patientData.AddressZipCode = record.GetValue(11).Substring(0, 5);
                                patientData.StartofCareDate = record.GetValue(1).ToMySqlDate(0);
                                patientData.Phone = !record.GetValue(12).Contains("_") ? record.GetValue(12).ToPhoneDB() : "";
                                patientData.PatientStatusId = record.GetValue(34).IsEqual("1") ? "1" : "2";

                                if (record.GetValue(36).IsNotNullOrEmpty())
                                {
                                    patientData.DischargeDate = record.GetValue(36).ToMySqlDate(0);
                                }
                                if (record.GetValue(42).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Reason for Discharge: {0}. ", record.GetValue(42));
                                }
                                if (record.GetValue(16).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Emergency/Triage Level: {0}. ", record.GetValue(16));
                                }
                                if (record.GetValue(2).IsNotNullOrEmpty() && record.GetValue(3).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Episode Range: {0} - {1}. ", record.GetValue(2).ToDateTime().ToShortDateString(), record.GetValue(3).ToDateTime().ToShortDateString());
                                }
                                if (record.GetValue(17).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Episode Frequency: {0}. ", record.GetValue(17));
                                }
                                if (record.GetValue(18).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Physician/NPI/Address/Phone/Fax: {0} {1} {2} {3} {4} {5} {6} {7} {8}. "
                                        , record.GetValue(23)
                                        , record.GetValue(19)
                                        , record.GetValue(18)
                                        , record.GetValue(24)
                                        , record.GetValue(25)
                                        , record.GetValue(26)
                                        , record.GetValue(27)
                                        , record.GetValue(28).ToPhoneDB()
                                        , record.GetValue(29).ToPhoneDB());
                                }
                                if (record.GetValue(30).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Assessor: {0} {1}. ", record.GetValue(30), record.GetValue(31));
                                }
                                if (record.GetValue(32).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Primary Diagnosis/Code: {0} {1}. ", record.GetValue(33), record.GetValue(32));
                                }
                                if (record.GetValue(39).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Payer Info: {0}. ", record.GetValue(39));
                                }
                                if (record.GetValue(37).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Patient Id: {0}.", record.GetValue(37));
                                }

                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());
                                textWriter.WriteLine(new PatientAllergyProfileScript(patientData).ToString());

                                //if (patientData.PatientStatusId.IsEqual("1"))
                                //{
                                //    if (record.GetValue(2).IsNotNullOrEmpty() && record.GetValue(3).IsNotNullOrEmpty())
                                //    {
                                //        patientData.EpisodeStart = record.GetValue(2).ToMySqlDate(0);
                                //        patientData.EpisodeEnd = record.GetValue(3).ToMySqlDate(0);
                                //        if (record.GetValue(32).IsNotNullOrEmpty())
                                //        {
                                //            patientData.Diagnosis = string.Format("<DiagonasisCodes><code1>{0}</code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>", record.GetValue(32));
                                //        }
                                //        textWriter.WriteLine(new PatientEpisodeScript(patientData).ToString());

                                //        patientData.EpisodeId = Guid.NewGuid();
                                //        patientData.EpisodeStart = record.GetValue(3).ToMySqlDate(1);
                                //        patientData.EpisodeEnd = record.GetValue(3).ToMySqlDate(60);

                                //        var nextEpisodeScript = new PatientEpisodeScript(patientData);
                                //        nextEpisodeScript.CreateRapAndFinal = false;
                                //        textWriter.WriteLine(nextEpisodeScript.ToString());
                                //    }
                                //}

                                textWriter.Write(textWriter.NewLine);
                            }
                        }
                    }
                }
            }
        }
    }
}
