﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Text;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class AloraHealthIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\Divine.xls");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Divine_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        if (excelReader != null && excelReader.IsValid)
                        {
                            excelReader.IsFirstRowAsColumnNames = false;
                            DataTable dataTable = excelReader.AsDataSet().Tables[0];
                            if (dataTable != null && dataTable.Rows.Count > 0)
                            {
                                int rowCounter = 0;
                                int dataCounter = 0;
                                PatientData patientData = null;

                                foreach (DataRow dataRow in dataTable.Rows)
                                {
                                    if (!dataRow.IsEmpty())
                                    {
                                        switch (dataCounter)
                                        {
                                            case 0:
                                                patientData = new PatientData();
                                                patientData.AgencyId = "ec83060f-0518-4431-9294-4f167194ea5c";
                                                patientData.AgencyLocationId = "576832bc-95e9-43d4-8bad-72b746397ab5";
                                                var nameArray = dataRow.GetValue(2).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (nameArray != null && nameArray.Length > 1)
                                                {
                                                    patientData.LastName = nameArray[0].Trim();
                                                    patientData.FirstName = nameArray[1].Trim();
                                                }
                                                if (dataRow.GetValue(9).IsNotNullOrEmpty())
                                                {
                                                    patientData.StartofCareDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(9))).ToString("yyyy-M-d");
                                                }
                                                patientData.PatientNumber = dataRow.GetValue(16);
                                                if (dataRow.GetValue(20).IsNotNullOrEmpty())
                                                {
                                                    patientData.BirthDate = DateTime.FromOADate(double.Parse(dataRow.GetValue(20))).ToString("yyyy-M-d");
                                                }
                                                patientData.Gender = dataRow.GetValue(24).IsEqual("f") ? "Female" : "Male";
                                                if (dataRow.GetValue(33).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Current Certification Period: {0}. ", dataRow.GetValue(33));
                                                }
                                                if (dataRow.GetValue(39).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Physician: {0}. ", dataRow.GetValue(39));
                                                }
                                                break;
                                            case 1:
                                                patientData.Phone = dataRow.GetValue(2).IsNotNullOrEmpty() ? dataRow.GetValue(2).ToPhoneDB() : string.Empty;
                                                if (dataRow.GetValue(6).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Primary Diagnosis: {0}. ", dataRow.GetValue(6));
                                                }
                                                if (dataRow.GetValue(23).IsNotNullOrEmpty())
                                                {
                                                    patientData.Comments += string.Format("Evacuation {0}. ", dataRow.GetValue(23));
                                                }
                                                break;
                                            case 2:
                                                if (dataRow.GetValue(2).IsNotNullOrEmpty())
                                                {
                                                    var addressArray = dataRow.GetValue(2).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (addressArray != null)
                                                    {
                                                        if (addressArray.Length == 3)
                                                        {
                                                            patientData.AddressLine1 = addressArray[0].Replace("'", "");
                                                            patientData.AddressLine2 = "";
                                                            patientData.AddressCity = addressArray[1].Trim();

                                                            var locationArray = addressArray[2].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (locationArray != null && locationArray.Length == 2)
                                                            {
                                                                patientData.AddressState = locationArray[0];
                                                                patientData.AddressZipCode = locationArray[1];
                                                            }
                                                        }

                                                        if (addressArray.Length == 4)
                                                        {
                                                            patientData.AddressLine1 = addressArray[0].Replace("'", "");
                                                            patientData.AddressLine2 = addressArray[1].Replace("'", "");
                                                            patientData.AddressCity = addressArray[2].Trim();

                                                            var locationArray = addressArray[3].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                            if (locationArray != null && locationArray.Length == 2)
                                                            {
                                                                patientData.AddressState = locationArray[0];
                                                                patientData.AddressZipCode = locationArray[1];
                                                            }
                                                        }
                                                    }
                                                }

                                                if (dataRow.GetValue(23).IsNotNullOrEmpty() && dataRow.GetValue(23).ToLower().Contains("medicare"))
                                                {
                                                    var medicareNumberArray = dataRow.GetValue(23).Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                                    if (medicareNumberArray != null)
                                                    {
                                                        patientData.MedicareNumber = medicareNumberArray[2].Replace(")", "");
                                                    }
                                                }

                                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());

                                                textWriter.Write(textWriter.NewLine);

                                                break;
                                        }
                                        for (int columnCounter = 0; columnCounter < dataRow.ItemArray.Length; columnCounter++)
                                        {
                                            if (dataRow.GetValue(columnCounter).IsNotNullOrEmpty())
                                            {
                                                Console.WriteLine("{0}) Data: {1}", dataCounter, dataRow.GetValue(columnCounter));
                                                Console.WriteLine("Row x Column: [{0} x {1}]", rowCounter, columnCounter);
                                                Console.WriteLine();
                                            }
                                        }
                                        dataCounter++;
                                        
                                    }
                                    else
                                    {
                                        dataCounter = 0;
                                    }
                                    rowCounter++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
