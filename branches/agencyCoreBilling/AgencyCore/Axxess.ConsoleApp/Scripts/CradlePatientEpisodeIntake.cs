﻿using System;
using System.IO;
using System.Data;

using Excel;
using Kent.Boogaart.KBCsv;

using Axxess.Core.Extension;

namespace Axxess.ConsoleApp.Tests
{
    public static class CradlePatientEpisodeIntake
    {
        private static string input = Path.Combine(App.Root, "Files\\Emangy.csv");
        private static string output = Path.Combine(App.Root, string.Format("Files\\Emangy_{0}.txt", DateTime.Now.Ticks.ToString()));

        public static void Run()
        {
            using (TextWriter textWriter = new StreamWriter(output, true))
            {
                using (FileStream fileStream = new FileStream(input, FileMode.Open, FileAccess.Read))
                {
                    using (var csvReader = new CsvReader(fileStream))
                    {
                        if (csvReader != null)
                        {
                            csvReader.ReadHeaderRecord();
                            foreach (var record in csvReader.DataRecords)
                            {
                                var patientData = new PatientData();
                                patientData.AgencyId = "df9db542-a1aa-45a2-a110-d6f9580b0170";
                                patientData.AgencyLocationId = "6c3410ea-1299-4349-9cb4-9ed71bdf7e42";
                                patientData.PatientNumber = record.GetValue(4);
                                patientData.Gender = record.GetValue(13).IsEqual("01") || record.GetValue(13).IsEqual("1") ? "Female" : "Male";
                                patientData.MedicareNumber = record.GetValue(0);
                                patientData.FirstName = record.GetValue(6).ToTitleCase();
                                patientData.LastName = record.GetValue(7).ToTitleCase();
                                patientData.MiddleInitial = "";
                                patientData.BirthDate = record.GetValue(14).ToMySqlDate(0);
                                patientData.AddressLine1 = record.GetValue(8).ToTitleCase();
                                patientData.AddressLine2 = "";
                                patientData.AddressCity = record.GetValue(9).ToTitleCase();
                                patientData.AddressState = record.GetValue(10);
                                patientData.AddressZipCode = record.GetValue(11).Substring(0, 5);
                                patientData.StartofCareDate = record.GetValue(1).ToMySqlDate(0);
                                patientData.Phone = !record.GetValue(12).Contains("_") ? record.GetValue(12).ToPhoneDB() : "";
                                patientData.PatientStatusId = record.GetValue(33).IsEqual("true") ? "1" : "2";

                                if (record.GetValue(45).IsNotNullOrEmpty())
                                {
                                    patientData.DischargeDate = record.GetValue(45).ToMySqlDate(0);
                                }
                                if (record.GetValue(15).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Emergency/Triage Level: {0}. ", record.GetValue(15));
                                }
                                if (record.GetValue(16).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Episode Frequency: {0}. ", record.GetValue(16));
                                }
                                if (record.GetValue(17).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Physician/NPI/Address/Phone/Fax: {0} {1} {2} {3} {4} {5} {6} {7} {8}. "
                                        , record.GetValue(22)
                                        , record.GetValue(18)
                                        , record.GetValue(17)
                                        , record.GetValue(23)
                                        , record.GetValue(24)
                                        , record.GetValue(25)
                                        , record.GetValue(26)
                                        , record.GetValue(27).ToPhone()
                                        , record.GetValue(28).ToPhone());
                                }
                                if (record.GetValue(29).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Assessor: {0} {1}. ", record.GetValue(29), record.GetValue(30));
                                }
                                if (record.GetValue(31).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Primary Diagnosis/Code: {0} {1}. ", record.GetValue(31), record.GetValue(32));
                                }
                                if (record.GetValue(43).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Payer Info: {0}. ", record.GetValue(43));
                                }
                                if (record.GetValue(41).IsNotNullOrEmpty())
                                {
                                    patientData.Comments += string.Format("Patient Id: {0}.", record.GetValue(41));
                                }

                                textWriter.WriteLine(new PatientScript(patientData).ToString());
                                textWriter.WriteLine(new PatientMedProfileScript(patientData).ToString());

                                if (patientData.PatientStatusId.IsEqual("1"))
                                {
                                    if (record.GetValue(2).IsNotNullOrEmpty() && record.GetValue(3).IsNotNullOrEmpty())
                                    {
                                        patientData.EpisodeStart = record.GetValue(2).ToMySqlDate(0);
                                        patientData.EpisodeEnd = record.GetValue(3).ToMySqlDate(0);
                                        if (record.GetValue(31).IsNotNullOrEmpty())
                                        {
                                            patientData.Diagnosis = string.Format("<DiagonasisCodes><code1>{0}</code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>", record.GetValue(31));
                                        }
                                        textWriter.WriteLine(new PatientEpisodeScript(patientData).ToString());

                                        patientData.EpisodeId = Guid.NewGuid();
                                        patientData.EpisodeStart = record.GetValue(3).ToMySqlDate(1);
                                        patientData.EpisodeEnd = record.GetValue(3).ToMySqlDate(60);

                                        var nextEpisodeScript = new PatientEpisodeScript(patientData);
                                        nextEpisodeScript.CreateRapAndFinal = true;
                                        textWriter.WriteLine(nextEpisodeScript.ToString());
                                    }
                                }

                                textWriter.Write(textWriter.NewLine);
                            }
                        }
                    }
                }
            }
        }
    }
}
