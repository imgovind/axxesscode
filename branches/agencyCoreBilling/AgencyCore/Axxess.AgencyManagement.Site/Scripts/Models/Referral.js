﻿var Referral = {
    Init: function() {
        $('#txtAdd_Referral_HomePhone1').autotab({ target: 'txtAdd_Referral_HomePhone2', format: 'numeric' });
        $('#txtAdd_Referral_HomePhone2').autotab({ target: 'txtAdd_Referral_HomePhone3', format: 'numeric', previous: 'txtAdd_Referral_HomePhone1' });
        $('#txtAdd_Referral_HomePhone3').autotab({ target: 'txtAdd_Referral_Email', format: 'numeric', previous: 'txtAdd_Referral_HomePhone2' });
        $('#txtAdd_Referral_PhysicianPhone1').autotab({ target: 'txtAdd_Referral_PhysicianPhone2', format: 'numeric' });
        $('#txtAdd_Referral_PhysicianPhone2').autotab({ target: 'txtAdd_Referral_PhysicianPhone3', format: 'numeric', previous: 'txtAdd_Referral_PhysicianPhone1' });
        $('#txtAdd_Referral_PhysicianPhone3').autotab({ target: 'txtAdd_Referral_PhysicianFax', format: 'numeric', previous: 'txtAdd_Referral_PhysicianPhone2' });

        $(".names").alpha({ nocaps: false });
        $(".numeric").numeric();
        $("#newReferralForm").click(function() {
            $("#newReferralValidaton").empty();
            $("#newReferralValidaton").hide();
        });

        $("#Add_DME_Other").click(function() {
            var q = $('#Add_DME_Other:checked').is(':checked');
            if (!q) {
                $("#txtAdd_Referral_OtherDME").hide();
            }
            else {
                $("#txtAdd_Referral_OtherDME").show();
            }
        });

        $("#Edit_DME_Other").click(function() {
            var q = $('#Edit_DME_Other:checked').is(':checked');
            if (q != true) {
                $("#txtEdit_Referral_OtherDME").hide();
            }
            else {
                $("#txtEdit_Referral_OtherDME").show();
            }
        });

        var q = $('#Edit_DME_Other:checked').is(':checked');
        if (q != true) {
            $("#txtEdit_Referral_OtherDME").hide();
        }
        else {
            $("#txtEdit_Referral_OtherDME").show();
        }

        $("#newReferralForm").validate({
            messages: {
                FirstName: "",
                LastName: "",
                DateOfBirth: "",
                Gender: "",
                HomePhoneArray: "",
                AddressLine1: "",
                AddressCity: "",
                AddressStateCode: "",
                AddressZipCode: ""
            },
            submitHandler: function(form) {
                var options = {
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function(values, form, options) {
                    },
                    success: function(result) {
                        var resultObject = eval(result);
                        if (resultObject.isSuccessful) {
                            $("#newReferralValidaton").empty();
                            $("#newReferralValidaton").hide();
                            var text = "<div style=\"color:green;\"><ul class='system_messages'><li class='green sent' ><span class='ico'></span><strong class='system_title'>Your data is successfully saved</strong></li></ul></div>";
                            $("#newReferralValidaton").append(text);
                            $("#newReferralValidaton").show();
                            Referral.RebindList();
                            Referral.Close($("#newReferralForm"));
                        }
                        else {
                            $("#newReferralValidaton").empty();
                            $("#newReferralValidaton").hide();
                            var text = "<div style=\"color:Red;\"><ul class='system_messages'><li class='red errorMessage' ><span class='ico'></span><strong class='system_title'>There are some fields missing in the form . Please see below  </strong></li> <li ><span class='ico'></span>" + resultObject.errorMessage + "</li></ul> </div>";
                            $("#newReferralValidaton").append(text);
                            $("#newReferralValidaton").show();
                        }
                    }
                };
                $(form).ajaxSubmit(options);
                return false;
            }
        });



        $("#txtNew_Referral_PhysicianDropDown").change(function() {
            var data = 'PhysicianContactId=' + $(this).val();
            $.ajax({
                url: '/Patient/GetPhysicianContact',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result) {
                    $('tbody', $(this).closest('table')).clearForm();
                    $("#txtNew_Referral_NpiNumber").val('');
                    var data = eval(result);
                    $("#txtAdd_Referral_PhysicianFirstName").val(data.FirstName != null ? data.FirstName : '');
                    $("#txtAdd_Referral_PhysicianLastName").val(data.LastName != null ? data.LastName : '');
                    $("#txtAdd_Referral_PhysicianNPI").val(data.NPI != null ? data.NPI : '');
                    if (data.PhoneWork !== null) {
                        var primaryPhone = data.PhoneWork;
                        $("#txtAdd_Referral_PhysicianPhone1").val(primaryPhone.substring(0, 3));
                        $("#txtAdd_Referral_PhysicianPhone2").val(primaryPhone.substring(3, 6));
                        $("#txtAdd_Referral_PhysicianPhone3").val(primaryPhone.substring(6, 10));
                    }
                    if (data.FaxNumber !== null) {
                        var ProviderBusinessPracticeLocationAddressFaxNumber = data.FaxNumber;
                        $("#txtAdd_Referral_PhysicianFax").val(ProviderBusinessPracticeLocationAddressFaxNumber);

                    }
                    $("#txtAdd_Referral_PhysicianEmail").val(data.EmailAddress != null ? data.EmailAddress : '');
                }
            });

        });

        $("#txtNew_Referral_NpiNumber").autocomplete('/LookUp/Npis', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Id, result: data[i].Id };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Id + '-' + row.ProviderFirstName + '  ' + row.ProviderLastName;
            },
            width: 400
        }).result(function(event, row, formatted) {
            $('tbody', $(this).closest('table')).clearForm();
            if (row) {
                $("#txtAdd_Referral_PhysicianFirstName").val(row.ProviderFirstName != null ? row.ProviderFirstName : '');
                $("#txtAdd_Referral_PhysicianLastName").val(row.ProviderLastName != null ? row.ProviderLastName : '');
                $("#txtAdd_Referral_PhysicianNPI").val(row.Id != null ? row.Id : '');

                if (row.ProviderBusinessPracticeLocationAddressTelephoneNumber !== null) {
                    var primaryPhone = row.ProviderBusinessPracticeLocationAddressTelephoneNumber;
                    $("#txtAdd_Referral_PhysicianPhone1").val(primaryPhone.substring(0, 3));
                    $("#txtAdd_Referral_PhysicianPhone2").val(primaryPhone.substring(3, 6));
                    $("#txtAdd_Referral_PhysicianPhone3").val(primaryPhone.substring(6, 10));
                }
                if (row.ProviderBusinessPracticeLocationAddressFaxNumber !== null) {
                    var ProviderBusinessPracticeLocationAddressFaxNumber = row.ProviderBusinessPracticeLocationAddressFaxNumber;
                    $("#txtAdd_Referral_PhysicianFax").val(ProviderBusinessPracticeLocationAddressFaxNumber);

                }
                $("#txtNew_Referral_PhysicianDropDown").val("0");
            }
        });



        $("#txtEdit_Referral_NpiNumber").autocomplete('/LookUp/Npis', {
            max: 20,
            minChars: 1,
            dataType: 'json',
            parse: function(data) {

                var rows = new Array();
                for (var i = 0; i < data.length; i++) {
                    rows[i] = { data: data[i], value: data[i].Id, result: data[i].Id };
                }
                return rows;
            },
            formatItem: function(row, i, n) {

                return row.Id + '-' + row.ProviderFirstName + '  ' + row.ProviderLastName;
            },
            width: 400
        }).result(function(event, row, formatted) {
            $('tbody', $(this).closest('table')).clearForm();
            if (row) {
                $("#txtEdit_Referral_PhysicianFirstName").val(row.ProviderFirstName != null ? row.ProviderFirstName : '');
                $("#txtEdit_Referral_PhysicianLastName").val(row.ProviderLastName != null ? row.ProviderLastName : '');
                $("#txtEdit_Referral_PhysicianNPI").val(row.Id != null ? row.Id : '');

                if (row.ProviderBusinessPracticeLocationAddressTelephoneNumber !== null) {
                    var primaryPhone = row.ProviderBusinessPracticeLocationAddressTelephoneNumber;
                    $("#txtEdit_Referral_PhysicianPhone1").val(primaryPhone.substring(0, 3));
                    $("#txtEdit_Referral_PhysicianPhone2").val(primaryPhone.substring(3, 6));
                    $("#txtEdit_Referral_PhysicianPhone3").val(primaryPhone.substring(6, 10));
                }
                if (row.ProviderBusinessPracticeLocationAddressFaxNumber !== null) {
                    var ProviderBusinessPracticeLocationAddressFaxNumber = row.ProviderBusinessPracticeLocationAddressFaxNumber;
                    $("#txtEdit_Referral_PhysicianFax").val(ProviderBusinessPracticeLocationAddressFaxNumber);

                }
                $("#txtEdit_Referral_PhysicianDropDown").val("0");
            }
        });


    },
    New: function() {
        $("#newReferralValidaton").empty();
        $("#newReferralValidaton").hide();
        $("#newReferral label.error").hide();
        $("#newReferral").clearForm();
        $("#txtNew_Referral_Date").data("tDatePicker").value(new Date());
    }
    ,
    Admit: function(id) {
        $("#admitPatientForm label.error").hide();
        $("#admitPatientForm").clearForm();
        $("#admitPatientValidaton").empty();
        $("#admitPatientValidaton").hide();
        var data = 'id=' + id;
        $.ajax({
            url: '/Referral/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var reportObject = eval(result);
                getNewRows(reportObject);
            }
        });
        getNewRows = function(data) {
            $("#txtAdmit_Patient_FirstName").val((data.FirstName !== null ? data.FirstName : ""));
            $("#txtAdmit_Patient_LastName").val((data.LastName !== null ? data.LastName : ""));
            $('input[name=Gender][value=' + data.Gender.toString() + ']').attr('checked', true);
            $("#txtAdmit_Patient_DOB").val((data.DateOfBirth !== null ? new Date(data.DateOfBirth).getMonth() + "/" + new Date(data.DateOfBirth).getDate() + "/" + new Date(data.DateOfBirth).getFullYear() : ""));
            $("#txtAdmit_Patient_MedicareNumber").val((data.MedicareNo !== null ? data.MedicareNo : ""));
            $("#txtAdmit_Patient_MedicaidNumber").val((data.MedicaidNo !== null ? data.MedicaidNo : ""));
            $("#txtAdmit_Patient_SSN").val((data.SSN !== null ? data.SSN : ""));
            $("#txtAdmit_Patient_AddressLine1").val((data.AddressLine1 !== null ? data.AddressLine1 : ""));
            $("#txtAdmit_Patient_AddressLine2").val((data.AddressLine2 !== null ? data.AddressLine2 : ""));
            $("#txtAdmit_Patient_AddressCity").val((data.AddressCity !== null ? data.AddressCity : ""));
            $("#txtAdmit_Patient_AddressStateCode").val((data.AddressStateCode !== null ? data.AddressStateCode : ""));
            $("#txtAdmit_Patient_AddressZipCode").val((data.AddressZipCode !== null ? data.AddressZipCode : ""));
            $("#txtAdmit_Patient_HomePhone1").val((data.HomePhone !== null ? data.HomePhone.substring(0, 3) : ""));
            $("#txtAdmit_Patient_HomePhone2").val((data.HomePhone !== null ? data.HomePhone.substring(3, 6) : ""));
            $("#txtAdmit_Patient_HomePhone3").val((data.HomePhone !== null ? data.HomePhone.substring(6) : ""));
            $("#txtAdmit_Patient_Email").val((data.Email !== null ? data.Email : ""));

            $("#txtAdmit_Patient_FirstNamePhysicianContact").val((data.PhysicianFirstName !== null ? data.PhysicianFirstName : ""));
            $("#txtAdmit_Patient_LastNamePhysicianContact").val((data.PhysicianLastName !== null ? data.PhysicianLastName : ""));
            if (data.PhysicianPhone !== null) {
                var physicianPhone = data.PhysicianPhone;
                $("#txtAdmit_Patient_PhysicianContactPrimaryPhoneArray1").val(physicianPhone.substring(0, 3));
                $("#txtAdmit_Patient_PhysicianContactPrimaryPhoneArray2").val(physicianPhone.substring(3, 6));
                $("#txtAdmit_Patient_PhysicianContactPrimaryPhoneArray3").val(physicianPhone.substring(6, 10));
            }
            if (data.PhysicianFax !== null) {
                var physicianFax = data.PhysicianFax;
                $("#txtAdmit_Patient_PhysicianContactFax1").val(physicianFax.substring(0, 3));
                $("#txtAdmit_Patient_PhysicianContactFax2").val(physicianFax.substring(3, 6));
                $("#txtAdmit_Patient_PhysicianContactFax3").val(physicianFax.substring(6, 10));
            }
            $("#txtAdmit_Patient_PhysicianContactEmail").val((data.PhysicianEmail !== null ? data.PhysicianEmail : " "));
            $("#txtAdmit_Patient_PhysicianContactNPINo").val((data.PhysicianNPI !== null ? data.PhysicianNPI : " "));


            $("#txtAdmit_Patient_ReferralPhysicianFirstName").val((data.PhysicianReferral !== null ? data.PhysicianReferral : ""));
            $("#txtAdmit_Patient_PatientReferralDate").val((data.PhysicianReferralDate !== null ? new Date(data.PhysicianReferralDate).getMonth() + "/" + new Date(data.PhysicianReferralDate).getDate() + "/" + new Date(data.PhysicianReferralDate).getFullYear() : ""));
            $("#txtAdmit_Patient_OtherReferralSource").val((data.ReferralSource !== null ? data.ReferralSource : ""));
            $("#txtAdmit_Patient_OtherSourceFirstName").val((data.ReferrerFirstName !== null ? data.ReferrerFirstName : ""));
            $("#txtAdmit_Patient_OtherSourceLastName").val((data.ReferrerLastName !== null ? data.ReferrerLastName : ""));
        };
    },
    loadAdmit: function(id) {

        $('#admitpatientResult').load('Patient/Admit', { Id: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#admitpatientResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#admitpatient');
            }
            else if (textStatus == "success") {

                JQD.open_window('#admitpatient');
                Referral.Init();
                Lookup.loadAdmissionSources();
                Lookup.loadStates();
                Lookup.loadRaces();
                Lookup.loadPhysicians();
                Lookup.loadUsers();
                Lookup.loadReferralSources();
            }
        }
);
    },
    Delete: function(id) {
        if (confirm("Are you sure you want to delete this referral?")) {

            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Referral/Delete",
                data: "id=" + id,
                success: function(result) {
                    var resultObject = eval(result);
                    if (resultObject.isSuccessful) {
                        Referral.RebindList();
                    }
                }
            });
        }
    },

    Edit: function(id) {

        $("#editReferral label.error").hide();
        $("#editReferral").clearForm();
        $("#editReferralValidaton").empty();
        $("#editReferralValidaton").hide();
        var data = 'id=' + id;
        $.ajax({
            url: '/Referral/Get',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(result) {
                var reportObject = eval(result);
                getNewRows(reportObject);
            }
        });

        getNewRows = function(data) {
            $("#txtEdit_ReferralID").val(id);
            $("#txtEdit_PhysicianReferral").val((data.PhysicianReferral !== null ? data.PhysicianReferral : " "));
            $("#txtEdit_Referral_Date").val((data.PhysicianReferralDate !== null ? new Date(data.PhysicianReferralDate).getMonth() + "/" + new Date(data.PhysicianReferralDate).getDate() + "/" + new Date(data.PhysicianReferralDate).getFullYear() : " "));
            $("#txtEdit_Referral_ReferralSource").val((data.ReferralSource !== null ? data.ReferralSource : " "));
            $("#txtEdit_Referrer_FirstName").val((data.ReferrerFirstName !== null ? data.ReferrerFirstName : " "));
            $("#txtEdit_Referrer_LastName").val((data.ReferrerLastName !== null ? data.ReferrerLastName : " "));
            $('input[name=Gender][value=' + data.Gender.toString() + ']').attr('checked', true);

            if (data.DME !== null) {

                var dmeArray = (data.DME).split(';');
                var i = 0;
                for (i = 0; i < dmeArray.length; i++) {

                    $('input[name=DMECollection][value=' + dmeArray[i].toString() + ']').attr('checked', true);
                    if (dmeArray[i] == 10) {

                        $("#txtEdit_Referral_OtherDME").val((data.OtherDME !== null ? data.OtherDME : " ")).show();
                    }
                }
            }
            if (data.ServicesRequired !== null) {

                var ServicesRequiredArray = (data.ServicesRequired).split(';');
                var i = 0;
                for (i = 0; i < ServicesRequiredArray.length; i++) {

                    $('input[name=ServicesRequiredCollection][value=' + ServicesRequiredArray[i].toString() + ']').attr('checked', true);
                }
            }
            $("#txtEdit_Referral_FirstName").val((data.FirstName !== null ? data.FirstName : " "));
            $("#txtEdit_Referral_LastName").val((data.LastName !== null ? data.LastName : " "));
            $("#txtEdit_Referral_MedicareNo").val((data.MedicareNo !== null ? data.MedicareNo : " "));
            $("#txtEdit_Referral_MedicaidNo").val((data.MedicaidNo !== null ? data.MedicaidNo : " "));
            $("#txtEdit_Referral_SSN").val((data.SSN !== null ? data.SSN : " "));
            $("#txtEdit_Referral_DateOfBirth").val((data.DateOfBirth !== null ? new Date(data.DateOfBirth).getMonth() + "/" + new Date(data.DateOfBirth).getDate() + "/" + new Date(data.DateOfBirth).getFullYear() : " "));
            $("input[name=Gender").val(data.Gender);
            $("#txtEdit_Referral_HomePhone1").val((data.HomePhone !== null ? data.HomePhone.substring(0, 3) : " "));
            $("#txtEdit_Referral_HomePhone2").val((data.HomePhone !== null ? data.HomePhone.substring(3, 6) : " "));
            $("#txtEdit_Referral_HomePhone3").val((data.HomePhone !== null ? data.HomePhone.substring(6) : " "));
            $("#txtEdit_Referral_Email").val((data.Email != null ? data.Email : " "));
            $("#txtEdit_Referral_AddressLine1").val((data.AddressLine1 !== null ? data.AddressLine1 : " "));
            $("#txtEdit_Referral_AddressLine2").val((data.AddressLine2 !== null ? data.AddressLine2 : " "));
            $("#txtEdit_Referral_AddressCity").val((data.AddressCity !== null ? data.AddressCity : " "));
            $("#txtEdit_Referral_AddressStateCode").val((data.AddressStateCode !== null ? data.AddressStateCode : " "));
            $("#txtEdit_Referral_AddressZipCode").val((data.AddressZipCode !== null ? data.AddressZipCode : " "));
            $("#txtEdit_Referral_PhysicianFirstName").val((data.PhysicianFirstName !== null ? data.PhysicianFirstName : " "));
            $("#txtEdit_Referral_PhysicianLastName").val((data.PhysicianLastName !== null ? data.PhysicianLastName : " "));
            $("#txtEdit_Referral_PhysicianNPI").val((data.PhysicianNPI !== null ? data.PhysicianNPI : " "));
            $("#txtEdit_Referral_PhysicianPhone1").val((data.PhysicianPhone !== null ? data.PhysicianPhone.substring(0, 3) : " "));
            $("#txtEdit_Referral_PhysicianPhone2").val((data.PhysicianPhone !== null ? data.PhysicianPhone.substring(3, 6) : " "));
            $("#txtEdit_Referral_PhysicianPhone3").val((data.PhysicianPhone !== null ? data.PhysicianPhone.substring(6) : " "));
            $("#txtEdit_Referral_PhysicianFax").val((data.PhysicianFax !== null ? data.PhysicianFax : " "));
            $("#txtEdit_Referral_PhysicianEmail").val((data.PhysicianEmail !== null ? data.PhysicianEmail : " "));
        };
    },
    NewReferral: function() {
        var refer = $('#List_Referral_NewButton');
        href = "javascript:void(0);";
        refer.attr('href', href);
    },
    RebindList: function() {
        var referralGrid = $('#ExistingReferralGrid').data('tGrid');
        referralGrid.rebind();
    },
    Close: function(control) {
        control.closest('div.window').hide();
    }
    ,
    loadEditReferral: function(id) {
        $('#editReferralResult').load('Referral/EditView', { id: id }, function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#editReferralResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#editReferral');
            }
            else if (textStatus == "success") {
                JQD.open_window('#editReferral');
                Referral.Init();
                Lookup.loadAdmissionSources();
                Lookup.loadStates();
                Lookup.loadRaces();
                Lookup.loadPhysicians();
                Lookup.loadUsers();
                Lookup.loadReferralSources();
                $("#txtEdit_Referral_PhysicianDropDown").change(function() {
                    var data = 'physicianId=' + $(this).val();
                    $.ajax({
                        url: '/Agency/GetPhysician',
                        type: 'POST',
                        dataType: 'json',
                        data: data,
                        success: function(result) {
                            $('tbody', $(this).closest('table')).clearForm();
                            $("#txtEditt_Referral_NpiNumber").val('');
                            var data = eval(result);
                            $("#txtEdit_Referral_PhysicianFirstName").val(data.FirstName != null ? data.FirstName : '');
                            $("#txtEdit_Referral_PhysicianLastName").val(data.LastName != null ? data.LastName : '');
                            $("#txtEdit_Referral_PhysicianNPI").val(data.NPI != null ? data.NPI : '');
                            if (data.PhoneWork !== null) {
                                var primaryPhone = data.PhoneWork;
                                $("#txtEdit_Referral_PhysicianPhone1").val(primaryPhone.substring(0, 3));
                                $("#txtEdit_Referral_PhysicianPhone2").val(primaryPhone.substring(3, 6));
                                $("#txtEdit_Referral_PhysicianPhone3").val(primaryPhone.substring(6, 10));
                            }
                            if (data.FaxNumber !== null) {
                                var ProviderBusinessPracticeLocationAddressFaxNumber = data.FaxNumber;
                                $("#txtEdit_Referral_PhysicianFax").val(ProviderBusinessPracticeLocationAddressFaxNumber);
                            }
                            $("#txtEdit_Referral_PhysicianEmail").val(data.EmailAddress != null ? data.EmailAddress : '');
                        }
                    });

                });
            }
        }
);
    },
    loadNewReferral: function() {

        $('#newReferralResult').load('Referral/NewContent', function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#newReferralResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#newReferral');
            }
            else if (textStatus == "success") {

                JQD.open_window('#newReferral');
                Referral.Init();
                Lookup.loadAdmissionSources();
                Lookup.loadStates();
                Lookup.loadRaces();
                Lookup.loadPhysicians();
                Lookup.loadUsers();
                Lookup.loadReferralSources();
            }
        }
);
    },
    loadExistingReferral: function() {
        $('#existingreferralResult').load('Referral/Grid', function(responseText, textStatus, XMLHttpRequest) {
            if (textStatus == 'error') {
                $('#existingreferralResult').html('<p>There was an error making the AJAX request</p>');
                JQD.open_window('#existingreferral');
            }
            else if (textStatus == "success") {
                JQD.open_window('#existingreferral');
                Referral.Init();
            }
        }
);
    }
}