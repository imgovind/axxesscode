﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="Billing_Center" class="abs window">
    <div style="background-color: Transparent;" class="abs window_inner">
        <div class="window_top">
            <span class="float_left">Billing Center </span><span class="float_right"><a class="window_min"
                href="javascript:void(0);"></a><a class="window_resize" href="javascript:void(0);">
                </a><a class="window_close" href="javascript:void(0);"></a></span>
        </div>
        <div id="billingCenterResult" style="display: block;" class="abs window_content general_form">
        </div>
        <div class="abs window_bottom">
            Billing Center
        </div>
        <span class="abs ui-resizable-handle ui-resizable-se"></span>
    </div>
</div>
