﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MessageViewData>" %>
<div class="messageList" id="messageList">
    <div class="inboxSubHeader">
        <div>
            Messages<span class="float_right"><a id="composeNewMessage" href="javascript:void(0);"><img
                alt="New Message" src="/Images/icons/new.gif" /></a></span></div>
    </div>
    <div class="messagePanel">
        <div id="9cbc6735-a69f-4255-a919-4f236bcc3c42" class="selectedMessage message">
            <div>
                <span class="float_right bold">12:49 PM</span></div>
            <div>
                <span class="bold">Niyi Olajide </span>
                <br />
                <span>Introducing Axxess&trade; AgencyCore</span>
            </div>
        </div>
        <div id="723a786c-0f57-4e0b-8b1f-5cf50f722fe0" class="message">
            <div>
                <span class="float_right bold">8:37 AM</span></div>
            <div>
                <span class="bold">Plaxo </span>
                <br />
                <span>Welcome to Plaxo</span>
            </div>
        </div>
        <div id="98a1d449-d6b7-423b-9310-c4bddf4c3504" class="message">
            <div>
                <span class="float_right bold">Tue 5:06 PM</span></div>
            <div>
                <span class="bold">careerhomecare.com </span>
                <br />
                <span>Home Health and Hospice Jobs vel nisl</span>
            </div>
        </div>
        <div id="b84936ad-c838-4f80-88b4-c92383819c31" class="message">
            <div>
                <span class="float_right bold">12:49 PM</span></div>
            <div>
                <span class="bold">Niyi Olajide </span>
                <br />
                <span>Introducing Axxess&trade; AgencyCore</span>
            </div>
        </div>
        <div id="61ed37ce-894e-42d1-afb0-c21ccf942203" class="message">
            <div>
                <span class="float_right bold">8:37 PM</span></div>
            <div>
                <span class="bold">Plaxo </span>
                <br />
                <span>Welcome to Plaxo</span>
            </div>
        </div>
    </div>
</div>
<div class="messageContent" id="messageContent">
    <div class="messageContentPanel" id="messageContentPanel">
        <div class="messageHeaderContainer">
            <strong>Introducing Axxess&trade; AgencyCore</strong><br />
            <div class="messageHeaderRow">
                <label for="messageSender">
                    From:</label><span id="messageSender">Niyi Olajide</span></div>
            <div class="messageHeaderRow">
                <label for="messageDate">
                    Sent:</label><span id="messageDate">Wed 7/21/2010 12:49 PM</span></div>
            <div class="messageHeaderRow" style="margin-top: 10px;">
                <input type="button" value="Reply" style="font-size: 9px; width: 50px; height: 18px;
                    font-weight: bold;" />&nbsp;<input type="button" value="Forward" style="font-size: 9px; width: 55px; height: 18px;
                    font-weight: bold;" />&nbsp;<input type="button" value="Delete" style="font-size: 9px;
                        width: 50px; height: 18px; font-weight: bold;" /></div>
        </div>
        <div class="clear">
        </div>
        <div class="messageBodyContainer">
            <h4>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            </h4>
            <p>
                Etiam at est et purus tincidunt pulvinar. Pellentesque pellentesque sapien vel nisl.
                Ut sed mauris nec ante dignissim egestas. Etiam tortor magna, tincidunt at, rhoncus
                eget, ornare et, lorem. Pellentesque habitant morbi tristique senectus et netus
                et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus
                et netus et malesuada fames ac turpis egestas. Mauris faucibus risus in nibh. Aliquam
                purus tortor, sodales ut, blandit sit amet, euismod ut, nunc. Vivamus pulvinar rutrum
                eros. Cras luctus, dui eu mattis adipiscing, risus mauris sagittis eros, sed tincidunt
                velit magna vel felis. Fusce adipiscing ligula ac mi. Proin quis libero. Proin purus
                purus, lacinia eget, bibendum quis, condimentum eget, eros. Integer dapibus vestibulum
                turpis. Quisque orci tellus, pretium vel, tincidunt ut, interdum id, velit.
            </p>
            <p>
                Nunc et leo. Aenean quis enim. Suspendisse ac odio a nisi fringilla porta. Donec
                in lacus volutpat nibh tristique placerat. Nam fermentum molestie velit. Integer
                sed lorem. Praesent posuere augue ac urna. Curabitur vitae velit. In elementum,
                pede volutpat blandit eleifend, quam turpis semper libero, a imperdiet quam nulla
                adipiscing ipsum. Quisque leo arcu, laoreet ac, tincidunt ac, dictum eu, nisl. Nam
                nec ante vitae orci suscipit gravida. Proin et dolor vitae augue pretium tempor.
                Nullam sollicitudin massa sit amet nibh. Phasellus ac eros ac arcu sodales tristique.
                Pellentesque commodo arcu et est. Vivamus quam purus, facilisis non, ultrices a,
                facilisis in, eros.
            </p>
        </div>
    </div>
</div>
<div class="clear">
</div>
<div class="recipientList" id="recipientList">
    <div class="inboxSubHeader">
        <div>
            Contact List</div>
    </div>
    <div class="recipientPanel">
        <div class="recipient">
            <div>
                <span class="float_right">[<a href="javascript:void(0);" onclick="Message.AddContacts();">insert
                    recipient(s)</a>]</span></div>
            <div>
                <input type="checkbox" id="selectAlRecipients" />&nbsp;<span class="bold">Select All</span></div>
        </div>
        <div class="recipient">
            <div>
                <input class="recipients" type="checkbox" id="62op37ce-894e-42d1-afb0-c21ccf942203" value="Niyi Olajide" />&nbsp;<span>Niyi Olajide</span></div>
        </div>
        <div class="recipient">
            <div>
                <input class="recipients" type="checkbox" id="83yd37ce-894e-42d1-afb0-c21ccf942203" value="Birhanu Abagaro" />&nbsp;<span>Birhanu Abagaro</span></div>
        </div>
        <div class="recipient">
            <div>
                <input class="recipients" type="checkbox" id="97af37ce-894e-42d1-afb0-c21ccf942203" value="Tom Petty" />&nbsp; <span>Tom Petty</span></div>
        </div>
    </div>
</div>
<div class="newMessageContent" id="newMessageContent">
    <div class="inboxSubHeader">
        <div>
            New Message</div>
    </div>
    <div class="newMessageContentPanel" id="newMessageContentPanel">
        <div class="newMessageContainer">
            <% using (Html.BeginForm("Compose", "Message", FormMethod.Post, new { @id = "newMessageForm" }))%>
            <%  { %>
            <div style="margin-left: 130px;">
                <input type="button" value="Cancel" style="float: left; font-size: 9px; width: 50px;
                    height: 18px; font-weight: bold;" id="newMessageCancelButton" /></div>
            <div class="clear">
            </div>
            <div class="float_left" style="width: 70px;">
                <input type="submit" value="Send" style="font-weight: bold; height: 60px; width: 50px;
                    background-color: #C3D8F1;" /><br />
            </div>
            <div class="float_left">
                <div class="newMessageRow">
                    <label for="newMessageRecipents">
                        To:</label><input type="text" id="newMessageRecipents" name="newMessageRecipents" />
                    <input type="hidden" name="newMessageRecipientIds" id="newMessageRecipientIds" />
                </div>
                <div class="newMessageRow">
                    <label for="newMessageSubject">
                        Subject:</label><input type="text" id="newMessageSubject" name="newMessageSubject" /></div>
                <div class="newMessageRow">
                    <label for="newMessageRegarding">
                        Regarding:</label><select name="newMessageRegarding"></select></div>
            </div>
            <div class="clear">
            </div>
            <div class="newMessageRow">
                <textarea id="newMessageBody" name="newMessageBody"></textarea></div>
            <%} %>
        </div>
    </div>
</div>
<div class="clear">
</div>
