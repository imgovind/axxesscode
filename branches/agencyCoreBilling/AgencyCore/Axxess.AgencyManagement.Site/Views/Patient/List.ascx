﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="existingPatientList" class="abs window">
<div class="abs window_inner">
    <div class="window_top">
        <span class="float_left">Patient List</span> <span class="float_right"><a href="javascript:void(0);"
            class="window_min"></a><a href="javascript:void(0);" class="window_resize"></a><a
                href="javascript:void(0);" class="window_close"></a></span>
    </div>
    <div class="abs window_content general_form">
        <div class="submitoasisCont">
            
                <!--[if !IE]>start table_wrapper<![endif]-->
                <%= Html.Telerik().Grid<Patient>()
                        .Name("ExistingPatientGrid")
                        .ToolBar(commnds => commnds.Custom().HtmlAttributes(new { @id = "add_New_Patient", @href = "javascript:void(0);", @onclick = "Patient.New(); JQD.open_window('#newpatient');" }).Text("Add New Patient"))                                                                                                    
                        .Columns(columns =>
		                {
                            columns.Bound(p => p.PatientIdNumber).Title("Patient Id");
                            columns.Bound(p => p.Created).Format("{0:MM/dd/yyyy}").Title("Date Created");
                            columns.Bound(p => p.Modified).Format("{0:MM/dd/yyyy}").Title("Date Modified");
                            columns.Bound(p => p.DisplayName).Title("Patient Name");                                 
                            columns.Bound(p =>p.Id)
                                   .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"Patient.EditFromGrid('<#=Id#>');JQD.open_window('#editPatient');\">Edit</a>")
                                   .Title("Action");
                        })
                        .DataBinding(dataBinding => dataBinding.Ajax().Select("List", "Patient"))
                        .ClientEvents(evets => evets.OnDataBound("Patient.NewPatient"))
                        .Pageable(paging => paging.PageSize(13))
                        .Sortable()
                        .Scrollable(scrolling => scrolling.Enabled(true).Height(369))
                %>
                <!--[if !IE]>end table_wrapper<![endif]-->
            </div>
    </div>
    <div class="abs window_bottom">
        List of Patient
    </div>
</div>
<span class="abs ui-resizable-handle ui-resizable-se"></span></div>
