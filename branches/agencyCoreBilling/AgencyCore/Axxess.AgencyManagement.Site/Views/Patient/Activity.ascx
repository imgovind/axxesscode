﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<ScheduleEvent>>" %>
<div class="submenu">
    <div style="margin-left: 10px; float: left;">
        <label>
            Show&nbsp;&nbsp;&nbsp;</label></div>
    <div style="float: left; margin-left: 10px; margin-right: 10px;">
        <select name="patientActivityDropDown" class="patientActivityDropDown">
            <option value="All" selected="selected">All</option>
            <option value="Orders">Orders</option>
            <option value="Nursing">Nursing</option>
            <option value="PT">PT</option>
            <option value="OT">OT</option>
            <option value="ST">ST</option>
            <option value="HHA">HHA</option>
            <option value="MSW">MSW</option>
            <option value="Dietician">Dietician</option>
        </select>
    </div>
    <div style="float: left;">
        <label>
            Date&nbsp;&nbsp;&nbsp;</label></div>
    <div style="float: left; margin-left: 10px; margin-right: 10px;">
        <select name="patientActivityDateDropdown" class="patientActivityDateDropDown">
            <option value="All" selected="selected">All</option>
            <option disabled="disabled">---------------------------------------------</option>
            <option value="DateRange">Date Range</option>
            <option disabled="disabled">---------------------------------------------</option>
            <option value="ThisEpisode">This Episode</option>
            <option value="LastEpisode">Last Episode</option>
            <option value="NextEpisode">Next Episode</option>
            <option disabled="disabled">---------------------------------------------</option>
            <option value="Today">Today</option>
            <option value="ThisWeek">This Week</option>
            <option value="ThisWeekToDate">This Week-to-date</option>
            <option value="LastMonth">Last Month</option>
            <option value="ThisMonth">This Month</option>
            <option value="ThisMonthToDate">This Month-to-date</option>
            <option value="ThisFiscalQuarter">This Fiscal Quarter</option>
            <option value="ThisFiscalQuarterToDate">This Fiscal Quarter-to-date</option>
            <option value="ThisFiscalYear">This Fiscal Year</option>
            <option value="ThisFiscalYearToDate">This Fiscal Year-to-date</option>
            <option value="Yesterday">Yesterday</option>
            <option value="LastWeek">Last Week</option>
            <option value="LastWeekToDate">Last Week-to-date</option>
            <option value="LastMonthToDate">Last Month-to-date</option>
            <option value="LastFiscalQuarter">Last Fiscal Quarter</option>
            <option value="LastFiscalQuarterToDate">Last Fiscal Quarter-to-date</option>
            <option value="LastFiscalYear">Last Fiscal Year</option>
            <option value="LastFiscalYearToDate">Last Fiscal Year-to-date</option>
            <option value="NextWeek">Next Week</option>
            <option value="Next4Weeks">Next 4 Weeks</option>
            <option value="NextMonth">Next Month</option>
            <option value="NextFiscalQuarter">Next Fiscal Quarter</option>
            <option value="NextFiscalYear">Next Fiscal Year</option>
        </select>
    </div>
    <div id="dateRangeText" style="display: none;">
    </div>
    <div class="CustomDateRange" style="display: none;">
        <div style="float: left;">
            <label>
                From&nbsp;&nbsp;&nbsp;</label></div>
        <div style="float: left; margin-left: 10px; margin-right: 10px;">
            <%= Html.Telerik().DatePicker()
                                          .Name("From")
                                        .Value(DateTime.Now.Subtract(TimeSpan.FromDays(60)))
                                        .HtmlAttributes(new {@id = "patientActivityFromDate", @class = "date" })
            %>
        </div>
        <div style="float: left;">
            <label>
                To&nbsp;&nbsp;&nbsp;</label></div>
        <div style="float: left;">
            <%= Html.Telerik().DatePicker()
                                          .Name("To")
                                        .Value(DateTime.Today)
                                        .HtmlAttributes(new {@id = "patientActivityToDate", @class = "date" })
            %>
        </div>
        <div style="float: left;">
            &nbsp;&nbsp;&nbsp;<input type="button" value="Search" onclick="Patient.CustomDateRange();" />
        </div>
    </div>
</div>
<%
    var val = Model != null && Model.Count > 0 ? Model.First().PatientId : Guid.Empty;
%>
<%Html.Telerik().Grid<ScheduleEvent>()
                                                .Name("PatientActivityGrid")
                                                .Columns(columns =>
                                                {
                                                    columns.Bound(o => o.EventId)
                                              .ClientTemplate("<input type='checkbox' name='checkedRecords' value='<#= EventId #>' />")
                                              .Title("Select")
                                              .Width(50)
                                              .HtmlAttributes(new { style = "text-align:center" });
                                                    columns.Bound(p => p.DisciplineTaskName)
                                                        .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('<#=EpisodeId#>');<#=Url#>\"><#=DisciplineTaskName#></a>")
                                                        .Title("Task");
                                                    columns.Bound(p => p.EventDate).Format("{0:MM/dd/yyyy}").Title("Target Date").Width(100);
                                                    columns.Bound(p => p.UserName);
                                                    columns.Bound(p => p.StatusName);
                                                    columns.Bound(p => p.Url)
                             .ClientTemplate("<a href=\"javascript:void(0);\" onclick=\"OasisValidation.SetEpisodeId('<#=EpisodeId#>'); <#=Url#>\">Edit</a> | <a href=\"javascript:void(0);\" onclick=\"Patient.DeleteSchedule($(this),'<#=EventId#>','<#=UserId#>');\" >Delete</a> ")
                             .Title("Action").Width(180);
                                                    columns.Bound(p => p.EpisodeId).HeaderHtmlAttributes(new { style = "display:none" }).HtmlAttributes(new { style = "display:none" }).Width(0);
                                                })
                                                .DataBinding(dataBinding => dataBinding.Ajax().Select("Activity", "Patient", new { patientId = val, discipline = "All", dateRangeId = "All", rangeStartDate = DateTime.Now.Subtract(TimeSpan.FromDays(60)), rangeEndDate = DateTime.Now }))
                                                .Sortable()
                                                .Selectable()
                                                .Scrollable()
                                                .Footer(false).HtmlAttributes(new { Style = "min-width:100px;" })
                                                .Render();                                                
                                                                                                
%>