﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="newagency" class="abs window">
    <div class="abs window_inner">
        <div class="window_top">
            <span class="float_left">New Agency Registration</span> <span class="float_right"><a
                href="javascript:void(0);" class="window_min"></a><a href="javascript:void(0);" class="window_resize">
                </a><a href="javascript:void(0);" class="window_close"></a></span>
        </div>
        <div class="abs window_content general_form">
            <!--[if !IE]>start forms<![endif]-->
            <% using (Html.BeginForm("New", "Agency", FormMethod.Post, new { @id = "newAgencyForm" }))%>
            <%  { %>
            <div id="newAgencyValidaton" class="marginBreak " style="display: none">
            </div>
            <div class="marginBreak">
                <b>Admin Information</b>
                <div class="rowBreak">
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <label for="AgencyAdminUsername">
                                        E-mail:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AgencyAdminUsername", "", new { @id = "txtAdd_Agency_AdminUsername", @class = "text required input_wrapper", @maxlength = "40", @tabindex = "1" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="AgencyAdminPassword">
                                        Password:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AgencyAdminPassword", "", new { @id = "txtAdd_Agency_AdminPassword", @class = "text required input_wrapper", @maxlength = "20", @tabindex = "2" })%>
                                        <input type="button" id="btnAdd_Agency_GeneratePassword" value="Generate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <label for="AgencyAdminFirstName">
                                        First Name:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AgencyAdminFirstName", "", new { @id = "txtAdd_Agency_AdminFirstName", @class = "text required names input_wrapper", @maxlength = "20", @tabindex = "3" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="AgencyAdminLastName">
                                        Last Name:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AgencyAdminLastName", "", new { @id = "txtAdd_Agency_AdminLastName", @class = "text required names input_wrapper", @maxlength = "20", @tabindex = "4" })%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="marginBreak">
                <b>Company Information</b>
                <div class="rowBreak">
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <label for="Name">
                                        Company Name:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("Name", "", new { @id = "txtAdd_Agency_CompanyName", @maxlength = "50", @class = "text required input_wrapper", @tabindex = "5" })%>
                                    </div>
                                </div>
                                <div class="row" style="vertical-align: middle;">
                                    <label for="TaxId">
                                        Tax Id:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("TaxId", "", new { @id = "txtAdd_Agency_TaxId", @maxlength = "10", @class = "text required input_wrapper", @tabindex = "6" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="TaxIdTypeId">
                                        Tax Id Type:</label>
                                    <div class="inputs">
                                        <select id="txtAdd_Agency_TaxIdType" name="TaxIdType" tabindex="7" class="input_wrapper">
                                            <option value="0">Choose Tax ID Type</option>
                                            <option value="1">EIN (Employer Identification Number)</option>
                                            <option value="2">SSN (Social Security Number)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="Payor">
                                        Payor:</label>
                                    <div class="inputs">
                                        <select id="txtAdd_Agency_Payor" name="Payor" tabindex="8" class="input_wrapper">
                                            <option value="0">Choose Payor</option>
                                            <option value="1">Palmetto GBA</option>
                                            <option value="2">National Government Services (formerly known as United Government
                                                Services)</option>
                                            <option value="3">Blue Cross Blue Shield of Alabama (AKA Chaba GBA)</option>
                                            <option value="4">Anthem Health Plans of Maine</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <label for="NationalProviderNumber">
                                        National Provider Number:</label>
                                    <%=Html.TextBox("NationalProviderNumber", " ", new { @id = "txtAdd_Agency_NationalProviderNo", @maxlength = "10", @class = "text input_wrapper", @tabindex = "9" })%>
                                </div>
                                <div class="row">
                                    <label for="MedicareProviderNumber">
                                        Medicare Provider Number:</label>
                                    <%=Html.TextBox("MedicareProviderNumber", " ", new { @id = "txtAdd_Agency_MedicareProviderNo", @maxlength = "10", @class = "text input_wrapper", @tabindex = "10" })%>
                                </div>
                                <div class="row">
                                    <label for="MedicaidProviderNumber">
                                        Medicaid Provider Number:</label>
                                    <%=Html.TextBox("MedicaidProviderNumber", " ", new { @id = "txtAdd_Agency_MedicaidProviderNo", @maxlength = "10", @class = "text input_wrapper", @tabindex = "11" })%>
                                </div>
                                <div class="row">
                                    <label for="HomeHealthAgencyId">
                                        Unique HHA Agency ID Code:</label>
                                    <%=Html.TextBox("HomeHealthAgencyId", " ", new { @id = "txtAdd_Agency_HomeHealthAgencyId", @maxlength = "10", @class = "text input_wrapper", @tabindex = "12" })%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <input type="checkbox" name="txtAdd_Agency_SameAsAdmin" id="txtAdd_Agency_SameAsAdmin" tabindex="13" />&nbsp;Check here if the
                                    Admin is same as the Contact Person
                                </div>
                                <div class="row">
                                    <label for="ContactPersonEmail">
                                        Contact Person E-mail:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("ContactPersonEmail", "", new { @id = "txtAdd_Agency_ContactPersonEmail", @class = "text required input_wrapper", @maxlength = "20", @tabindex = "14" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="ContactPhoneArray">
                                        Contact Person Phone:</label>
                                    <div class="inputs">
                                        <input type="text" class="autotext required numeric" style="width: 51.5px; padding: 0px;
                                            margin: 0px;" name="ContactPhoneArray" id="txtAdd_Agency_ContactPhone1" maxlength="3" size="3" tabindex="15" />
                                        -
                                        <input type="text" class="autotext required numeric" style="width: 51.5px; padding: 0px;
                                            margin: 0px;" name="ContactPhoneArray" id="txtAdd_Agency_ContactPhone2" maxlength="3" size="3" tabindex="16" />
                                        -
                                        <input type="text" class="autotext required numeric" style="width: 52px; padding: 0px;
                                            margin: 0px;" name="ContactPhoneArray" id="txtAdd_Agency_ContactPhone3" maxlength="4" size="5" tabindex="17" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    &nbsp;
                                </div>
                                <div class="row">
                                    <div class="row">
                                        <label for="ContactPersonFirstName">
                                            Contact Person First Name:</label>
                                        <div class="inputs">
                                            <%=Html.TextBox("ContactPersonFirstName", "", new { @id = "txtAdd_Agency_ContactPersonFirstName", @maxlength = "50", @class = "text required names input_wrapper", @tabindex = "18" })%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="ContactPersonLastName">
                                        Contact Person Last Name:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("ContactPersonLastName", "", new { @id = "txtAdd_Agency_ContactPersonLastName", @class = "text required names input_wrapper", @maxlength = "20", @tabindex = "19" })%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="marginBreak">
                <b>Location Information</b>
                <div class="rowBreak">
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <label for="AddressLine1">
                                        Address Line 1:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AddressLine1", "", new { @id = "txtAdd_Agency_AddressLine1", @maxlength = "20", @class = "text required input_wrapper", @tabindex = "20" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="AddressCity">
                                        City:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AddressCity", "", new { @id = "txtAdd_Agency_AddressCity", @maxlength = "20", @class = "text required input_wrapper", @tabindex = "21" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="AddressZipCode">
                                        Zip Code :</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AddressZipCode", "", new { @id = "txtAdd_Agency_AddressZipCode", @class = "text numeric required input_wrapper", @size = "5", @maxlength = "5", @tabindex = "22" })%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contentDivider">
                        <div class="patientfieldset">
                            <div class="fix">
                                <div class="row">
                                    <label for="AddressLine2">
                                        Address Line 2:</label>
                                    <div class="inputs">
                                        <%=Html.TextBox("AddressLine2", "", new { @id = "txtAdd_Agency_AddressLine2", @maxlength = "20", @class = "text input_wrapper", @tabindex = "23" })%>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="AddressStateCode">
                                        State :</label>
                                    <div class="inputs">
                                        <select id="txtAdd_Agency_StateCode" name="AddressStateCode" tabindex="24" class="AddressStateCode selectDropDown input_wrapper">
                                            <option value="0" selected="selected">** Select State **</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="PhoneArray">
                                        Phone:</label>
                                    <div class="inputs">
                                        <input type="text" class="autotext numeric required" style="width: 51.5px; padding: 0px;
                                            margin: 0px;" name="PhoneArray" id="txtAdd_Agency_Phone1" maxlength="3" size="3" tabindex="25" />
                                        -
                                        <input type="text" class="autotext numeric required" style="width: 51.5px; padding: 0px;
                                            margin: 0px;" name="PhoneArray" id="txtAdd_Agency_Phone2" maxlength="3" size="3" tabindex="26" />
                                        -
                                        <input type="text" class="autotext numeric required" style="width: 52px; padding: 0px;
                                            margin: 0px;" name="PhoneArray" id="txtAdd_Agency_Phone3" maxlength="4" size="5" tabindex="27" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="buttons">
                    <ul>
                        <li>
                            <input type="submit" value="Save" /></li>
                        <li>
                            <input type="button" value="Cancel" onclick="Agency.Close($(this));" /></li>
                        <li>
                            <input type="reset" value="Reset" /></li>
                    </ul>
                </div>
            </div>
            <%} %>
            <!--[if !IE]>end forms<![endif]-->
        </div>
        <div class="abs window_bottom">
        </div>
    </div>
    <span class="abs ui-resizable-handle ui-resizable-se"></span>
</div>
<% Html.Telerik().ScriptRegistrar().Scripts(script => script.Add("/Models/Agency.js"))
.OnDocumentReady(() =>
{%>
Agency.Init();
<%}); %>