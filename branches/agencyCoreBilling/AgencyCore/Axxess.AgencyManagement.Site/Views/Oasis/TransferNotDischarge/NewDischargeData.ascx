﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "editOasisTransferInPatientNotDischargedOrdersForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("TransferInPatientNotDischarged_Id", Model.Id)%>
<%= Html.Hidden("TransferInPatientNotDischarged_Action", "Edit")%>
<%= Html.Hidden("TransferInPatientNotDischarged_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "TransferInPatientNotDischarged")%>
<div class="insideColFull">
    <div class="insiderow title">
        <div class="margin">
            (M2400) Intervention Synopsis: (Check only one box in each row.) Since the previous
            OASIS assessment, were the following interventions BOTH included in the physician-ordered
            plan of care AND implemented?
        </div>
    </div>
    <div class="margin">
        <table class="agency-data-table" id="Table7">
            <thead>
                <tr>
                    <th>
                        Plan / Intervention
                    </th>
                    <th>
                        No
                    </th>
                    <th>
                        Yes
                    </th>
                    <th colspan="2">
                        Not Applicable
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        a. Diabetic foot care including monitoring for the presence of skin lesions on the
                        lower extremities and patient/caregiver education on proper foot care
                    </td>
                    <td>
                        <%=Html.Hidden("TransferInPatientNotDischarged_M2400DiabeticFootCare", " ", new { @id = "" })%>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400DiabeticFootCare", "00", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400DiabeticFootCare", "01", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400DiabeticFootCare", "NA", data.ContainsKey("M2400DiabeticFootCare") && data["M2400DiabeticFootCare"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Patient is not diabetic or is bilateral amputee
                    </td>
                </tr>
                <tr>
                    <td>
                        b. Falls prevention interventions
                    </td>
                    <td>
                        <%=Html.Hidden("TransferInPatientNotDischarged_M2400FallsPreventionInterventions", " ", new { @id = "" })%>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400FallsPreventionInterventions", "00", data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400FallsPreventionInterventions", "01", data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400FallsPreventionInterventions", "NA", data.ContainsKey("M2400FallsPreventionInterventions") && data["M2400FallsPreventionInterventions"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Formal multi-factor Fall Risk Assessment indicates the patient was not at risk for
                        falls since the last OASIS assessment
                    </td>
                </tr>
                <tr>
                    <td>
                        c. Depression intervention(s) such as medication, referral for other treatment,
                        or a monitoring plan for current treatment
                    </td>
                    <td>
                        <%=Html.Hidden("TransferInPatientNotDischarged_M2400DepressionIntervention", " ", new { @id = "" })%>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400DepressionIntervention", "00", data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400DepressionIntervention", "01", data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400DepressionIntervention", "NA", data.ContainsKey("M2400DepressionIntervention") && data["M2400DepressionIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Formal assessment indicates patient did not meet criteria for depression AND patient
                        did not have diagnosis of depression since the last OASIS assessment
                    </td>
                </tr>
                <tr>
                    <td>
                        d. Intervention(s) to monitor and mitigate pain
                    </td>
                    <td>
                        <%=Html.Hidden("TransferInPatientNotDischarged_M2400PainIntervention", " ", new { @id = "" })%>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400PainIntervention", "00", data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400PainIntervention", "01", data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400PainIntervention", "NA", data.ContainsKey("M2400PainIntervention") && data["M2400PainIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Formal assessment did not indicate pain since the last OASIS assessment
                    </td>
                </tr>
                <tr>
                    <td>
                        e. Intervention(s) to prevent pressure ulcers
                    </td>
                    <td>
                        <%=Html.Hidden("TransferInPatientNotDischarged_M2400PressureUlcerIntervention", " ", new { @id = "" })%>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400PressureUlcerIntervention", "00", data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400PressureUlcerIntervention", "01", data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400PressureUlcerIntervention", "NA", data.ContainsKey("M2400PressureUlcerIntervention") && data["M2400PressureUlcerIntervention"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Formal assessment indicates the patient was not at risk of pressure ulcers since
                        the last OASIS assessment
                    </td>
                </tr>
                <tr>
                    <td>
                        f. Pressure ulcer treatment based on principles of moist wound healing
                    </td>
                    <td>
                        <%=Html.Hidden("TransferInPatientNotDischarged_M2400PressureUlcerTreatment", " ", new { @id = "" })%>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400PressureUlcerTreatment", "00", data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400PressureUlcerTreatment", "01", data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                    </td>
                    <td>
                        <%=Html.RadioButton("TransferInPatientNotDischarged_M2400PressureUlcerTreatment", "NA", data.ContainsKey("M2400PressureUlcerTreatment") && data["M2400PressureUlcerTreatment"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;na
                    </td>
                    <td>
                        Dressings that support the principles of moist wound healing not indicated for this
                        patient’s pressure ulcers OR patient has no pressure ulcers with need for moist
                        wound healing
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="rowOasis">
    <div class="insiderow">
        <div class="insiderow title">
            <div class="margin">
                (M2410) To which Inpatient Facility has the patient been admitted?
            </div>
        </div>
        <div class="margin">
            <%=Html.Hidden("TransferInPatientNotDischarged_M2410TypeOfInpatientFacility", " ", new { @id = "" })%>
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2410TypeOfInpatientFacility", "01", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
            - Hospital [ Go to M2430 ]<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2410TypeOfInpatientFacility", "02", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
            - Rehabilitation facility [ Go to M0903 ]<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2410TypeOfInpatientFacility", "03", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
            - Nursing home [ Go to M2440 ]<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2410TypeOfInpatientFacility", "04", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
            - Hospice [ Go to M0903 ]<br />
            <%=Html.RadioButton("TransferInPatientNotDischarged_M2410TypeOfInpatientFacility", "NA", data.ContainsKey("M2410TypeOfInpatientFacility") && data["M2410TypeOfInpatientFacility"].Answer == "NA" ? true : false, new { @id = "" })%>&nbsp;NA
            - No inpatient facility admission [Omit "NA" option on TRN]<br />
        </div>
    </div>
</div>
<div class="rowOasis" id="transferNotDischarge_M2430">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="margin">
                (M2430) Reason for Hospitalization: For what reason(s) did the patient require hospitalization?
                (Mark all that apply.)
            </div>
        </div>
        <div class="insideCol">
            <div class="margin">
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMed" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMed" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationMed") && data["M2430ReasonForHospitalizationMed"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;1
                - Improper medication administration, medication side effects, toxicity, anaphylaxis<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationFall" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationFall" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationFall") && data["M2430ReasonForHospitalizationFall"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;2
                - Injury caused by fall<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationInfection"
                    type="hidden" value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationInfection"
                    type="checkbox" value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationInfection") && data["M2430ReasonForHospitalizationInfection"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;3
                - Respiratory infection (e.g., pneumonia, bronchitis)<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationOtherRP" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationOtherRP" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationOtherRP") && data["M2430ReasonForHospitalizationOtherRP"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;4
                - Other respiratory problem<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHeartFail"
                    type="hidden" value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHeartFail"
                    type="checkbox" value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationHeartFail") && data["M2430ReasonForHospitalizationHeartFail"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;5
                - Heart failure (e.g., fluid overload)<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationCardiac" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationCardiac" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationCardiac") && data["M2430ReasonForHospitalizationCardiac"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;6
                - Cardiac dysrhythmia (irregular heartbeat)<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMyocardial"
                    type="hidden" value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMyocardial"
                    type="checkbox" value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationMyocardial") && data["M2430ReasonForHospitalizationMyocardial"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;7
                - Myocardial infarction or chest pain<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHeartDisease"
                    type="hidden" value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHeartDisease"
                    type="checkbox" value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationHeartDisease") && data["M2430ReasonForHospitalizationHeartDisease"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;8
                - Other heart disease<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationStroke" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationStroke" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationStroke") && data["M2430ReasonForHospitalizationStroke"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;9
                - Stroke (CVA) or TIA<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHypo" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationHypo" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationHypo") && data["M2430ReasonForHospitalizationHypo"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;10
                - Hypo/Hyperglycemia, diabetes out of control
            </div>
        </div>
        <div class="insideCol">
            <div class="margin">
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationGI" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationGI" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationGI") && data["M2430ReasonForHospitalizationGI"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;11
                - GI bleeding, obstruction, constipation, impaction<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationDehMal" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationDehMal" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationDehMal") && data["M2430ReasonForHospitalizationDehMal"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;12
                - Dehydration, malnutrition<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUrinaryInf"
                    type="hidden" value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUrinaryInf"
                    type="checkbox" value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationUrinaryInf") && data["M2430ReasonForHospitalizationUrinaryInf"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;13
                - Urinary tract infection<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationIV" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationIV" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationIV") && data["M2430ReasonForHospitalizationIV"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;14
                - IV catheter-related infection or complication<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationWoundInf" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationWoundInf" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationWoundInf") && data["M2430ReasonForHospitalizationWoundInf"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;15
                - Wound infection or deterioration<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUncontrolledPain"
                    type="hidden" value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUncontrolledPain"
                    type="checkbox" value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationUncontrolledPain") && data["M2430ReasonForHospitalizationUncontrolledPain"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;16
                - Uncontrolled pain<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMental" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationMental" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationMental") && data["M2430ReasonForHospitalizationMental"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;17
                - Acute mental/behavioral health problem<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationDVT" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationDVT" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationDVT") && data["M2430ReasonForHospitalizationDVT"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;18
                - Deep vein thrombosis, pulmonary embolus<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationScheduled"
                    type="hidden" value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationScheduled"
                    type="checkbox" value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationScheduled") && data["M2430ReasonForHospitalizationScheduled"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;19
                - Scheduled treatment or procedure<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationOther" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationOther" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationOther") && data["M2430ReasonForHospitalizationOther"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;20
                - Other than above reasons<br />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUK" type="hidden"
                    value=" " />
                <input name="TransferInPatientNotDischarged_M2430ReasonForHospitalizationUK" type="checkbox"
                    value="1" '<% if(data.ContainsKey("M2430ReasonForHospitalizationUK") && data["M2430ReasonForHospitalizationUK"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;UK
                - Reason unknown<br />
            </div>
        </div>
    </div>
</div>
<div class="rowOasis" id="transferNotDischarge_M2440">
    <div class="insideColFull title">
        <div class="padding">
            (M2440) For what Reason(s) was the patient Admitted to a Nursing Home? (Mark all
            that apply.)</div>
    </div>
    <div class="insideCol ">
        <div class="padding">
            <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedTherapy" type="hidden"
                value=" " />
            <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedTherapy" type="checkbox"
                value="1" '<% if(data.ContainsKey("M2440ReasonPatientAdmittedTherapy") && data["M2440ReasonPatientAdmittedTherapy"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;1
            - Therapy services<br />
            <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedRespite" type="hidden"
                value=" " />
            <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedRespite" type="checkbox"
                value="1" '<% if(data.ContainsKey("M2440ReasonPatientAdmittedRespite") && data["M2440ReasonPatientAdmittedRespite"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;2
            - Respite care<br />
            <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedHospice" type="hidden"
                value=" " />
            <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedHospice" type="checkbox"
                value="1" '<% if(data.ContainsKey("M2440ReasonPatientAdmittedHospice") && data["M2440ReasonPatientAdmittedHospice"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;3
            - Hospice care<br />
            <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedPermanent" type="hidden"
                value=" " />
            <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedPermanent" type="checkbox"
                value="1" '<% if(data.ContainsKey("M2440ReasonPatientAdmittedPermanent") && data["M2440ReasonPatientAdmittedPermanent"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;4
            - Permanent placement
        </div>
    </div>
    <div class="insideCol adjust">
        <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedUnsafe" type="hidden"
            value=" " />
        <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedUnsafe" type="checkbox"
            value="1" '<% if(data.ContainsKey("M2440ReasonPatientAdmittedUnsafe") && data["M2440ReasonPatientAdmittedUnsafe"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;5
        - Unsafe for care at home<br />
        <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedOther" type="hidden"
            value=" " />
        <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedOther" type="checkbox"
            value="1" '<% if(data.ContainsKey("M2440ReasonPatientAdmittedOther") && data["M2440ReasonPatientAdmittedOther"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;6
        - Other<br />
        <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedUnknown" type="hidden"
            value=" " />
        <input name="TransferInPatientNotDischarged_M2440ReasonPatientAdmittedUnknown" type="checkbox"
            value="1" '<% if(data.ContainsKey("M2440ReasonPatientAdmittedUnknown") && data["M2440ReasonPatientAdmittedUnknown"].Answer == "1"){ %>checked="checked"<% }%>'" />&nbsp;UK
        - Unknown<br />
    </div>
</div>
<div class="rowOasis">
    <div class="insideCol">
        <div class="insiderow title">
            <div class="margin">
                (M0903) Date of Last (Most Recent) Home Visit:
            </div>
        </div>
        <div class="margin">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0903LastHomeVisitDate", data.ContainsKey("M0903LastHomeVisitDate") ? data["M0903LastHomeVisitDate"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0903LastHomeVisitDate" })%>
        </div>
    </div>
    <div class="insideCol">
        <div class="insiderow title">
            <div class="margin">
                (M0906) Discharge/Transfer/Death Date: Enter the date of the discharge, transfer,
                or death (at home) of the patient.
            </div>
        </div>
        <div class="margin">
            <%=Html.TextBox("TransferInPatientNotDischarged_M0906DischargeDate", data.ContainsKey("M0906DischargeDate") ? data["M0906DischargeDate"].Answer : "", new { @id = "TransferInPatientNotDischarged_M0906DischargeDate" })%>
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save" onclick="TransferNotDischarge.FormSubmit($(this),'Edit');" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="TransferNotDischarge.FormSubmit($(this),'Edit');" /></li>
    </ul>
</div>
<%  } %>
