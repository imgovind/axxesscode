﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisFollowUpAdlForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("FollowUp_Id", Model.Id)%>
<%= Html.Hidden("FollowUp_Action", "Edit")%>
<%= Html.Hidden("FollowUp_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "FollowUp")%>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1810) Current Ability to Dress Upper Body safely (with or without dressing aids)
                including undergarments, pullovers, front-opening shirts and blouses, managing zippers,
                buttons, and snaps:
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("FollowUp_M1810CurrentAbilityToDressUpper", " ", new { @id = "" })%>
                <%=Html.RadioButton("FollowUp_M1810CurrentAbilityToDressUpper", "00", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to get clothes out of closets and drawers, put them on and remove them from
                the upper body without assistance.<br />
                <%=Html.RadioButton("FollowUp_M1810CurrentAbilityToDressUpper", "01", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to dress upper body without assistance if clothing is laid out or handed
                to the patient.<br />
                <%=Html.RadioButton("FollowUp_M1810CurrentAbilityToDressUpper", "02", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Someone must help the patient put on upper body clothing.<br />
                <%=Html.RadioButton("FollowUp_M1810CurrentAbilityToDressUpper", "03", data.ContainsKey("M1810CurrentAbilityToDressUpper") && data["M1810CurrentAbilityToDressUpper"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Patient depends entirely upon another person to dress the upper body.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1820) Current Ability to Dress Lower Body safely (with or without dressing aids)
                including undergarments, slacks, socks or nylons, shoes:
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("FollowUp_M1820CurrentAbilityToDressLower", " ", new { @id = "" })%>
                <%=Html.RadioButton("FollowUp_M1820CurrentAbilityToDressLower", "00", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to obtain, put on, and remove clothing and shoes without assistance.<br />
                <%=Html.RadioButton("FollowUp_M1820CurrentAbilityToDressLower", "01", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to dress lower body without assistance if clothing and shoes are laid out
                or handed to the patient.<br />
                <%=Html.RadioButton("FollowUp_M1820CurrentAbilityToDressLower", "02", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Someone must help the patient put on undergarments, slacks, socks or nylons, and
                shoes.<br />
                <%=Html.RadioButton("FollowUp_M1820CurrentAbilityToDressLower", "03", data.ContainsKey("M1820CurrentAbilityToDressLower") && data["M1820CurrentAbilityToDressLower"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Patient depends entirely upon another person to dress lower body.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1830) Bathing: Current ability to wash entire body safely. Excludes grooming (washing
                face, washing hands, and shampooing hair).
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("FollowUp_M1830CurrentAbilityToBatheEntireBody", " ", new { @id = "" })%>
                <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "00", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to bathe self in shower or tub independently, including getting in and out
                of tub/shower.<br />
                <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "01", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - With the use of devices, is able to bathe self in shower or tub independently,
                including getting in and out of the tub/shower.<br />
                <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "02", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Able to bathe in shower or tub with the intermittent assistance of another person:<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) for intermittent supervision or encouragement
                or reminders, OR
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) to get in and out of the shower or
                tub, OR<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(c) for washing difficult to reach areas.<br />
                <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "03", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Able to participate in bathing self in shower or tub, but requires presence of
                another person throughout the bath for assistance or supervision.<br />
                <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "04", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Unable to use the shower or tub, but able to bathe self independently with or
                without the use of devices at the sink, in chair, or on commode.<br />
                <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "05", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Unable to use the shower or tub, but able to participate in bathing self in bed,
                at the sink, in bedside chair, or on commode, with the assistance or supervision
                of another person throughout the bath.<br />
                <%=Html.RadioButton("FollowUp_M1830CurrentAbilityToBatheEntireBody", "06", data.ContainsKey("M1830CurrentAbilityToBatheEntireBody") && data["M1830CurrentAbilityToBatheEntireBody"].Answer == "06" ? true : false, new { @id = "" })%>&nbsp;6
                - Unable to participate effectively in bathing and is bathed totally by another
                person.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1840) Toilet Transferring: Current ability to get to and from the toilet or bedside
                commode safely and transfer on and off toilet/commode.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("FollowUp_M1840ToiletTransferring", " ", new { @id = "" })%>
                <%=Html.RadioButton("FollowUp_M1840ToiletTransferring", "00", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to get to and from the toilet and transfer independently with or without
                a device.<br />
                <%=Html.RadioButton("FollowUp_M1840ToiletTransferring", "01", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - When reminded, assisted, or supervised by another person, able to get to and from
                the toilet and transfer.<br />
                <%=Html.RadioButton("FollowUp_M1840ToiletTransferring", "02", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Unable to get to and from the toilet but is able to use a bedside commode (with
                or without assistance).<br />
                <%=Html.RadioButton("FollowUp_M1840ToiletTransferring", "03", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Unable to get to and from the toilet or bedside commode but is able to use a bedpan/urinal
                independently.<br />
                <%=Html.RadioButton("FollowUp_M1840ToiletTransferring", "04", data.ContainsKey("M1840ToiletTransferring") && data["M1840ToiletTransferring"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Is totally dependent in toileting.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1850) Transferring: Current ability to move safely from bed to chair, or ability
                to turn and position self in bed if patient is bedfast.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("FollowUp_M1850Transferring", " ", new { @id = "" })%>
                <%=Html.RadioButton("FollowUp_M1850Transferring", "00", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to independently transfer.<br />
                <%=Html.RadioButton("FollowUp_M1850Transferring", "01", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - Able to transfer with minimal human assistance or with use of an assistive device.<br />
                <%=Html.RadioButton("FollowUp_M1850Transferring", "02", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Able to bear weight and pivot during the transfer process but unable to transfer
                self.<br />
                <%=Html.RadioButton("FollowUp_M1850Transferring", "03", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Unable to transfer self and is unable to bear weight or pivot when transferred
                by another person.<br />
                <%=Html.RadioButton("FollowUp_M1850Transferring", "04", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Bedfast, unable to transfer but is able to turn and position self in bed.<br />
                <%=Html.RadioButton("FollowUp_M1850Transferring", "05", data.ContainsKey("M1850Transferring") && data["M1850Transferring"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Bedfast, unable to transfer and is unable to turn and position self.
            </div>
        </div>
    </div>
</div>
<div class="rowOasis">
    <div class="insideColFull">
        <div class="insiderow title">
            <div class="padding">
                (M1860) Ambulation/Locomotion: Current ability to walk safely, once in a standing
                position, or use a wheelchair, once in a seated position, on a variety of surfaces.
            </div>
        </div>
        <div class="insiderow">
            <div class="padding">
                <%=Html.Hidden("FollowUp_M1860AmbulationLocomotion", " ", new { @id = "" })%>
                <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "00", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "00" ? true : false, new { @id = "" })%>&nbsp;0
                - Able to independently walk on even and uneven surfaces and negotiate stairs with
                or without railings (i.e., needs no human assistance or assistive device).<br />
                <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "01", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "01" ? true : false, new { @id = "" })%>&nbsp;1
                - With the use of a one-handed device (e.g. cane, single crutch, hemi-walker), able
                to independently walk on even and uneven surfaces and negotiate stairs with or without
                railings.<br />
                <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "02", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "02" ? true : false, new { @id = "" })%>&nbsp;2
                - Requires use of a two-handed device (e.g., walker or crutches) to walk alone on
                a level surface and/or requires human supervision or assistance to negotiate stairs
                or steps or uneven surfaces.<br />
                <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "03", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "03" ? true : false, new { @id = "" })%>&nbsp;3
                - Able to walk only with the supervision or assistance of another person at all
                times.<br />
                <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "04", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "04" ? true : false, new { @id = "" })%>&nbsp;4
                - Chairfast, unable to ambulate but is able to wheel self independently.<br />
                <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "05", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "05" ? true : false, new { @id = "" })%>&nbsp;5
                - Chairfast, unable to ambulate and is unable to wheel self.<br />
                <%=Html.RadioButton("FollowUp_M1860AmbulationLocomotion", "06", data.ContainsKey("M1860AmbulationLocomotion") && data["M1860AmbulationLocomotion"].Answer == "06" ? true : false, new { @id = "" })%>&nbsp;6
                - Bedfast, unable to ambulate or be up in a chair.
            </div>
        </div>
    </div>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save/Continue" class="SaveContinue" onclick="FollowUp.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="FollowUp.FormSubmit($(this));" /></li>
    </ul>
</div>
<%} %>
