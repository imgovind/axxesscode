﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Assessment>" %>
<% using (Html.BeginForm("Assessment", "Oasis", FormMethod.Post, new { @id = "newOasisStartOfCareOrdersDisciplineTreatmentForm" }))%>
<%  { %>
<%var data = Model.ToDictionary(); %>
<%= Html.Hidden("StartOfCare_Id", Model.Id)%>
<%= Html.Hidden("StartOfCare_Action", "Edit")%>
<%= Html.Hidden("StartOfCare_PatientGuid", Model.PatientId)%>
<%= Html.Hidden("assessment", "StartOfCare")%>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th colspan="2">
                Orders for Discipline and Treatments
            </th>
        </tr>
        <tr>
            <td width="20%">
                SN Frequency
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485SNFrequency", data.ContainsKey("485SNFrequency") ? data["485SNFrequency"].Answer : "", new { @id = "StartOfCare_485SNFrequency", @maxlength = "70", @size = "70" })%>
            </td>
        </tr>
        <tr>
            <td width="20%">
                PT Frequency
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485PTFrequency", data.ContainsKey("485PTFrequency") ? data["485PTFrequency"].Answer : "", new { @id = "StartOfCare_485PTFrequency", @maxlength = "70", @size = "70" })%>
            </td>
        </tr>
        <tr>
            <td width="20%">
                OT Frequency
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485OTFrequency", data.ContainsKey("485OTFrequency") ? data["485OTFrequency"].Answer : "", new { @id = "StartOfCare_485OTFrequency", @maxlength = "70", @size = "70" })%>
            </td>
        </tr>
        <tr>
            <td width="20%">
                ST Frequency
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485STFrequency", data.ContainsKey("485STFrequency") ? data["485STFrequency"].Answer : "", new { @id = "StartOfCare_485STFrequency", @maxlength = "70", @size = "70" })%>
            </td>
        </tr>
        <tr>
            <td width="20%">
                MSW Frequency
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485MSWFrequency", data.ContainsKey("485MSWFrequency") ? data["485MSWFrequency"].Answer : "", new { @id = "StartOfCare_485MSWFrequency", @maxlength = "70", @size = "70" })%>
            </td>
        </tr>
        <tr>
            <td width="20%">
                HHA Frequency
            </td>
            <td>
                <%=Html.TextBox("StartOfCare_485HHAFrequency", data.ContainsKey("485HHAFrequency") ? data["485HHAFrequency"].Answer : "", new { @id = "StartOfCare_485HHAFrequency", @maxlength = "70", @size = "70" })%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="hidden" name="StartOfCare_485Dietician" value=" " />
                <input name="StartOfCare_485Dietician" value="1" type="checkbox" '<% if(data.ContainsKey("485Dietician") && data["485Dietician"].Answer == "1"){ %>checked="checked"<% }%>'" />
                Dietician
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Additional Orders: &nbsp;
                <%var ordersDisciplineInterventionTemplates = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "-----------", Value = "-2" },
                   new SelectListItem { Text = "Erase", Value = "-1"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485OrdersDisciplineInterventionTemplates") && data["485OrdersDisciplineInterventionTemplates"].Answer != "" ? data["485OrdersDisciplineInterventionTemplates"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485OrdersDisciplineInterventionTemplates", ordersDisciplineInterventionTemplates)%>
                <%=Html.TextArea("StartOfCare_485OrdersDisciplineInterventionComments", data.ContainsKey("485OrdersDisciplineInterventionComments") ? data["485OrdersDisciplineInterventionComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485OrdersDisciplineInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] rehabilitationPotential = data.ContainsKey("485RehabilitationPotential") && data["485RehabilitationPotential"].Answer != "" ? data["485RehabilitationPotential"].Answer.Split(',') : null; %>
        <tr>
            <th>
                Rehabilitation Potential
            </th>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="StartOfCare_485RehabilitationPotential" value="" type="hidden" />
                        <input name="StartOfCare_485RehabilitationPotential" value="1" type="checkbox" '<% if( rehabilitationPotential!=null && rehabilitationPotential.Contains("1") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Good to achieve stated goals with skilled intervention and patient's compliance
                        with the plan of care </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="StartOfCare_485RehabilitationPotential" value="2" type="checkbox" '<% if( rehabilitationPotential!=null && rehabilitationPotential.Contains("2") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Fair to achieve stated goals with skilled intervention and patient's compliance
                        with the plan of care </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="StartOfCare_485RehabilitationPotential" value="3" type="checkbox" '<% if( rehabilitationPotential!=null && rehabilitationPotential.Contains("3") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Poor to achieve stated goals with skilled intervention and patient's compliance
                        with the plan of care </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>Other rehabilitation potential: &nbsp;
                        <%var achieveGoalsTemplates = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "-----------", Value = "-2" },
                   new SelectListItem { Text = "Erase", Value = "-1"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485AchieveGoalsTemplates") && data["485AchieveGoalsTemplates"].Answer != "" ? data["485AchieveGoalsTemplates"].Answer : "0");%>
                        <%= Html.DropDownList("StartOfCare_485AchieveGoalsTemplates", achieveGoalsTemplates)%>
                        <%=Html.TextArea("StartOfCare_485AchieveGoalsComments", data.ContainsKey("485AchieveGoalsComments") ? data["485AchieveGoalsComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485AchieveGoalsComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table cellspacing="0" cellpadding="0" border="0">
        <%string[] dischargePlans = data.ContainsKey("485DischargePlans") && data["485DischargePlans"].Answer != "" ? data["485DischargePlans"].Answer.Split(',') : null; %>
        <tr>
            <th>
                Discharge Plans
            </th>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input type="hidden" name="StartOfCare_485DischargePlans" value="" />
                        <input name="StartOfCare_485DischargePlans" value="1" type="checkbox" '<% if( dischargePlans!=null && dischargePlans.Contains("1") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Discharge to care of physician </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="StartOfCare_485DischargePlans" value="2" type="checkbox" '<% if( dischargePlans!=null && dischargePlans.Contains("2") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Discharge to caregiver </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="StartOfCare_485DischargePlans" value="3" type="checkbox" '<% if( dischargePlans!=null && dischargePlans.Contains("3") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Discharge patient to self care </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="StartOfCare_485DischargePlans" value="4" type="checkbox" '<% if( dischargePlans!=null && dischargePlans.Contains("4") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Discharge when caregiver willing and able to manage all aspects of patient's care
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="StartOfCare_485DischargePlans" value="5" type="checkbox" '<% if( dischargePlans!=null && dischargePlans.Contains("5") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Discharge when goals met </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="StartOfCare_485DischargePlans" value="6" type="checkbox" '<% if( dischargePlans!=null && dischargePlans.Contains("6") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Discharge when wound(s) healed </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="StartOfCare_485DischargePlans" value="7" type="checkbox" '<% if( dischargePlans!=null && dischargePlans.Contains("7") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Discharge when reliable caregiver available to assist with patient's medical needs
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input name="StartOfCare_485DischargePlans" value="8" type="checkbox" '<% if( dischargePlans!=null && dischargePlans.Contains("8") ){ %>checked="checked"<% }%>'" />
                    </li>
                    <li>Discharge when patient is independent in management of medical needs </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>Additional discharge plans: &nbsp;
                        <%var dischargePlanTemplates = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "-----------", Value = "-2" },
                   new SelectListItem { Text = "Erase", Value = "-1"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485DischargePlanTemplates") && data["485DischargePlanTemplates"].Answer != "" ? data["485DischargePlanTemplates"].Answer : "0");%>
                        <%= Html.DropDownList("StartOfCare_485DischargePlanTemplates", dischargePlanTemplates)%>
                        <%=Html.TextArea("StartOfCare_485DischargePlanComments", data.ContainsKey("485DischargePlanComments") ? data["485DischargePlanComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485DischargePlanComments", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <%string[] patientStrengths = data.ContainsKey("485PatientStrengths") && data["485PatientStrengths"].Answer != "" ? data["485PatientStrengths"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="3">
                Patient Strengths
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="StartOfCare_485PatientStrengths" value="" />
                <input name="StartOfCare_485PatientStrengths" value="1" type="checkbox" '<% if( patientStrengths!=null && patientStrengths.Contains("1") ){ %>checked="checked"<% }%>'" />
                Motivated Learner
            </td>
            <td>
                <input name="StartOfCare_485PatientStrengths" value="2" type="checkbox" '<% if( patientStrengths!=null && patientStrengths.Contains("2") ){ %>checked="checked"<% }%>'" />
                Strong Support System
            </td>
            <td>
                <input name="StartOfCare_485PatientStrengths" value="3" type="checkbox" '<% if( patientStrengths!=null && patientStrengths.Contains("3") ){ %>checked="checked"<% }%>'" />
                Absence of Multiple Diagnosis
            </td>
        </tr>
        <tr>
            <td>
                <input name="StartOfCare_485PatientStrengths" value="4" type="checkbox" '<% if( patientStrengths!=null && patientStrengths.Contains("4") ){ %>checked="checked"<% }%>'" />
                Enhanced Socioeconomic Status
            </td>
            <td colspan="2">
                Other:
                <%=Html.TextBox("StartOfCare_485PatientStrengthOther", data.ContainsKey("485PatientStrengthOther") ? data["485PatientStrengthOther"].Answer : "", new { @id = "StartOfCare_485PatientStrengthOther", @size = "70", @maxlength = "70" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table id="Table1" cellspacing="0" cellpadding="0">
        <%string[] conclusions = data.ContainsKey("485Conclusions") && data["485Conclusions"].Answer != "" ? data["485Conclusions"].Answer.Split(',') : null; %>
        <tr>
            <th colspan="3">
                Conclusions
            </th>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="StartOfCare_485Conclusions" value="" />
                <input name="StartOfCare_485Conclusions" value="1" type="checkbox" '<% if( conclusions!=null && conclusions.Contains("1") ){ %>checked="checked"<% }%>'" />
                Skilled Intervention Needed
            </td>
            <td>
                <input name="StartOfCare_485Conclusions" value="2" type="checkbox" '<% if( conclusions!=null && conclusions.Contains("2") ){ %>checked="checked"<% }%>'" />
                Skilled Instruction Needed
            </td>
            <td>
                <input name="StartOfCare_485Conclusions" value="3" type="checkbox" '<% if( conclusions!=null && conclusions.Contains("3") ){ %>checked="checked"<% }%>'" />
                No Skilled Service Needed
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Other:
                <%=Html.TextBox("StartOfCare_485ConclusionOther", data.ContainsKey("485ConclusionOther") ? data["485ConclusionOther"].Answer : "", new { @id = "StartOfCare_485ConclusionOther", @size = "70", @maxlength = "70" })%>
            </td>
        </tr>
    </table>
</div>
<div class="row485">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th>
                Skilled Intervention
            </th>
        </tr>
        <tr>
            <td colspan="3" class="bold">
                Assessment/ Instruction/ Performance:&nbsp;
                <%var skilledInterventionTemplate = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "-----------", Value = "-2" },
                   new SelectListItem { Text = "Erase", Value = "-1"}
                   
               }
                   , "Value", "Text", data.ContainsKey("485SkilledInterventionTemplate") && data["485SkilledInterventionTemplate"].Answer != "" ? data["485SkilledInterventionTemplate"].Answer : "0");%>
                <%= Html.DropDownList("StartOfCare_485SkilledInterventionTemplate", skilledInterventionTemplate)%>
                <%=Html.TextArea("StartOfCare_485SkilledInterventionComments", data.ContainsKey("485SkilledInterventionComments") ? data["485SkilledInterventionComments"].Answer : "", 5, 70, new { @id = "StartOfCare_485SkilledInterventionComments", @style = "width: 99%;" })%>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li>
                        <input type="hidden" name="StartOfCare_485SIResponse" id="StartOfCare_485SIResponse" />
                        <input name="StartOfCare_485SIResponse" value="1" type="checkbox" '<% if(data.ContainsKey("485SIResponse") && data["485SIResponse"].Answer == "1"){ %>checked="checked"<% }%>'" />
                        Response to Skilled Intervention </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp;</li>
                    <li class="spacer">Verbalized Understanding </li>
                    <li class="littleSpacer">
                        <input type="hidden" name="StartOfCare_485SIVerbalizedUnderstandingPT" value=" " />
                        <input name="StartOfCare_485SIVerbalizedUnderstandingPT" value="1" type="checkbox" '<% if(data.ContainsKey("485SIVerbalizedUnderstandingPT") && data["485SIVerbalizedUnderstandingPT"].Answer == "1"){ %>checked="checked"<% }%>'" />
                        Pt </li>
                    <li class="littleSpacer">
                        <input type="text" name="StartOfCare_485SIVerbalizedUnderstandingPTPercent" id="StartOfCare_485SIVerbalizedUnderstandingPTPercent"
                            size="4" maxlength="4" value="" />
                        % </li>
                    <li class="littleSpacer">
                        <input type="hidden" name="StartOfCare_485SIVerbalizedUnderstandingCG" value=" " />
                        <input name="StartOfCare_485SIVerbalizedUnderstandingCG" value="1" type="checkbox" '<% if(data.ContainsKey("485SIVerbalizedUnderstandingCG") && data["485SIVerbalizedUnderstandingCG"].Answer == "1"){ %>checked="checked"<% }%>'" />
                        CG </li>
                    <li>
                        <input type="text" name="StartOfCare_485SIVerbalizedUnderstandingCGPercent" id="StartOfCare_485SIVerbalizedUnderstandingCGPercent"
                            size="4" maxlength="4" value="" />
                        % </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp;</li>
                    <li class="spacer">Return Demonstration </li>
                    <li class="littleSpacer">
                        <input type="hidden" name="StartOfCare_485SIReturnDemonstrationPT" value=" " />
                        <input name="StartOfCare_485SIReturnDemonstrationPT" value="1" type="checkbox" '<% if(data.ContainsKey("485SIReturnDemonstrationPT") && data["485SIReturnDemonstrationPT"].Answer == "1"){ %>checked="checked"<% }%>'" />
                        Pt </li>
                    <li class="littleSpacer">
                        <%=Html.TextBox("StartOfCare_485SIReturnDemonstrationPTPercent", data.ContainsKey("485SIReturnDemonstrationPTPercent") ? data["485SIReturnDemonstrationPTPercent"].Answer : "", new { @id = "StartOfCare_485SIReturnDemonstrationPTPercent", @size = "4", @maxlength = "4" })%>
                        % </li>
                    <li class="littleSpacer">
                        <input type="hidden" name="StartOfCare_485SIReturnDemonstrationCG" value=" " />
                        <input name="StartOfCare_485SIReturnDemonstrationCG" value="1" type="checkbox" '<% if(data.ContainsKey("485SIReturnDemonstrationCG") && data["485SIReturnDemonstrationCG"].Answer == "1"){ %>checked="checked"<% }%>'" />
                        CG </li>
                    <li>
                        <%=Html.TextBox("StartOfCare_485SIReturnDemonstrationCGPercent", data.ContainsKey("485SIReturnDemonstrationCGPercent") ? data["485SIReturnDemonstrationCGPercent"].Answer : "", new { @id = "StartOfCare_485SIReturnDemonstrationCGPercent", @size = "4", @maxlength = "4" })%>
                        % </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp;</li>
                    <li class="spacer">Require Further Teaching </li>
                    <li class="littleSpacer">
                        <input type="hidden" name="StartOfCare_485SIFurtherTeachReqPT" value=" " />
                        <input name="StartOfCare_485SIFurtherTeachReqPT" value="1" type="checkbox" '<% if(data.ContainsKey("485SIFurtherTeachReqPT") && data["485SIFurtherTeachReqPT"].Answer == "1"){ %>checked="checked"<% }%>'" />
                        Pt </li>
                    <li>
                        <input type="hidden" name="StartOfCare_485SIFurtherTeachReqCG" value=" " />
                        <input name="StartOfCare_485SIFurtherTeachReqCG" value="1" type="checkbox" '<% if(data.ContainsKey("485SIFurtherTeachReqCG") && data["485SIFurtherTeachReqCG"].Answer == "1"){ %>checked="checked"<% }%>'" />
                        CG </li>
                </ul>
                <ul class="columns">
                    <li class="littleSpacer">&nbsp;</li>
                    <li class="spacer">Comments: </li>
                    <li>
                        <%=Html.TextBox("StartOfCare_485SIResponseComments", data.ContainsKey("485SIResponseComments") ? data["485SIResponseComments"].Answer : "", new { @id = "StartOfCare_485SIResponseComments", @size = "90", @maxlength = "90" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Title of Teaching Tool Used/Given: </li>
                    <li>
                        <%=Html.TextBox("StartOfCare_485TeachingToolUsed", data.ContainsKey("485TeachingToolUsed") ? data["485TeachingToolUsed"].Answer : "", new { @id = "StartOfCare_485TeachingToolUsed", @size = "40", @maxlength = "40" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="bigSpacer">Progress To Goals: </li>
                    <li>
                        <%=Html.TextBox("StartOfCare_485ProgressToGoals", data.ContainsKey("485ProgressToGoals") ? data["485ProgressToGoals"].Answer : "", new { @id = "StartOfCare_485ProgressToGoals", @size = "40", @maxlength = "40" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Conferenced With: </li>
                    <li>
                        <%var conferencedWith = new SelectList(new[]
               { 
                   new SelectListItem { Text = "", Value = "0" },
                   new SelectListItem { Text = "MD", Value = "MD" },
                   new SelectListItem { Text = "SN", Value = "SN"},
                   new SelectListItem { Text = "PT", Value = "PT" },
                   new SelectListItem { Text = "OT", Value = "OT" },
                    new SelectListItem { Text = "ST", Value = "ST" },
                   new SelectListItem { Text = "MSW", Value = "MSW" },
                   new SelectListItem { Text = "HHA", Value = "HHA"}
                   
                   
               }
                   , "Value", "Text", data.ContainsKey("485ConferencedWith") && data["485ConferencedWith"].Answer != "" ? data["485ConferencedWith"].Answer : "0");%>
                        <%= Html.DropDownList("StartOfCare_485ConferencedWith", conferencedWith)%>
                    </li>
                </ul>
                <ul class="columns">
                    <li class="spacer">Name: </li>
                    <li>
                        <%=Html.TextBox("StartOfCare_485ConferencedWithName", data.ContainsKey("485ConferencedWithName") ? data["485ConferencedWithName"].Answer : "", new { @id = "StartOfCare_485ConferencedWithName", @size = "30", @maxlength = "30" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <ul class="columns">
                    <li class="spacer">Regarding: </li>
                    <li>
                        <%=Html.TextArea("StartOfCare_485SkilledInterventionRegarding", data.ContainsKey("485SkilledInterventionRegarding") ? data["485SkilledInterventionRegarding"].Answer : "", 5, 70, new { @id = "StartOfCare_485SkilledInterventionRegarding", @style = "width: 99%;" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Physician Contacted Re: </li>
                    <li>
                        <%=Html.TextBox("StartOfCare_485SkilledInterventionPhysicianContacted", data.ContainsKey("485SkilledInterventionPhysicianContacted") ? data["485SkilledInterventionPhysicianContacted"].Answer : "", new { @id = "StartOfCare_485SkilledInterventionPhysicianContacted", @size = "90", @maxlength = "90" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Order Changes: </li>
                    <li>
                        <%=Html.TextBox("StartOfCare_485SkilledInterventionOrderChanges", data.ContainsKey("485SkilledInterventionOrderChanges") ? data["485SkilledInterventionOrderChanges"].Answer : "", new { @id = "StartOfCare_485SkilledInterventionOrderChanges", @size = "90", @maxlength = "90" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Plan for Next Visit: </li>
                    <li>
                        <%=Html.TextBox("StartOfCare_485SkilledInterventionNextVisit", data.ContainsKey("485SkilledInterventionNextVisit") ? data["485SkilledInterventionNextVisit"].Answer : "", new { @id = "StartOfCare_485SkilledInterventionNextVisit", @size = "90", @maxlength = "90" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Next Physician Visit: </li>
                    <li>
                        <%=Html.TextBox("StartOfCare_485SkilledInterventionNextPhysicianVisit", data.ContainsKey("485SkilledInterventionNextPhysicianVisit") ? data["485SkilledInterventionNextPhysicianVisit"].Answer : "", new { @id = "StartOfCare_485SkilledInterventionNextPhysicianVisit", @size = "10", @maxlength = "10" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">Discharge Planning: </li>
                    <li>
                        <%=Html.TextBox("StartOfCare_485SkilledInterventionDischargePlanning", data.ContainsKey("485SkilledInterventionDischargePlanning") ? data["485SkilledInterventionDischargePlanning"].Answer : "", new { @id = "StartOfCare_485SkilledInterventionDischargePlanning", @size = "90", @maxlength = "90" })%>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <ul class="columns">
                    <li class="spacer">&nbsp;</li>
                    <li>
                        <input type="hidden" name="StartOfCare_485SkilledInterventionWrittenNotice" value=" " />
                        <input name="StartOfCare_485SkilledInterventionWrittenNotice" value="1" type="checkbox" '<% if(data.ContainsKey("485SkilledInterventionWrittenNotice") && data["485SkilledInterventionWrittenNotice"].Answer == "1"){ %>checked="checked"<% }%>'" />
                        Written notice of discharge provided to patient. </li>
                    <li>Discharge scheduled for:
                        <%=Html.TextBox("StartOfCare_485SkilledInterventionWrittenNoticeDate", data.ContainsKey("485SkilledInterventionWrittenNoticeDate") ? data["485SkilledInterventionWrittenNoticeDate"].Answer : "", new { @id = "StartOfCare_485SkilledInterventionWrittenNoticeDate", @size = "10", @maxlength = "10" })%>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</div>
<div class="rowOasisButtons">
    <ul>
        <li style="float: left">
            <input type="button" value="Save" class="SaveContinue" onclick="SOC.FormSubmit($(this));" /></li>
        <li style="float: left">
            <input type="button" value="Save/Exit" onclick="SOC.FormSubmit($(this));" /></li>
    </ul>
</div>
<%  } %>
