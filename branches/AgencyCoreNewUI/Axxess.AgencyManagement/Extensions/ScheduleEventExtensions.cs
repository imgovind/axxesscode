﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;


    public static class ScheduleEventExtensions
    {
        public static string[] Discipline(this List<ScheduleEvent> scheduleEvent)
        {
            var disciplines = new List<string>();
            if (scheduleEvent != null && scheduleEvent.Count > 0)
            {
                if (scheduleEvent.Exists(s => s.Discipline == "Nursing"))
                {
                    disciplines.Add("SN");
                }
                if (scheduleEvent.Exists(s => s.Discipline == "PT"))
                {
                    disciplines.Add("PT");
                }
                if (scheduleEvent.Exists(s => s.Discipline == "OT"))
                {
                    disciplines.Add("OT");
                }
                if (scheduleEvent.Exists(s => s.Discipline == "ST"))
                {
                    disciplines.Add("ST");
                }
                if (scheduleEvent.Exists(s => s.Discipline == "HHA"))
                {
                    disciplines.Add("HHA");
                }
                if (scheduleEvent.Exists(s => s.Discipline == "MSW"))
                {
                    disciplines.Add("MSW");
                }
            }
            return disciplines.ToArray();
        }

        public static string GIdentify(this ScheduleEvent schedule)
        {
            if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
            {
                var type = ((DisciplineTasks)schedule.DisciplineTask);
                switch (type)
                {
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.NonOASISDischarge:
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.NonOASISRecertification:
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.NonOASISStartofCare:
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.LVNSupervisoryVisit:
                    case DisciplineTasks.DieticianVisit:
                    case DisciplineTasks.DischargeSummary:
                    case DisciplineTasks.SixtyDaySummary:
                    case DisciplineTasks.TransferSummary:
                    case DisciplineTasks.CoordinationOfCare:
                        return "SN";

                    case DisciplineTasks.SNVTeachingTraining:
                        return "SNT";

                    case DisciplineTasks.SNVManagementAndEvaluation:
                        return "SNM";

                    case DisciplineTasks.SNVObservationAndAssessment:
                        return "SNO";

                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.PTReEvaluation:
                        return "PT";

                    case DisciplineTasks.PTAVisit:
                        return "PTA";

                    case DisciplineTasks.PTMaintenance:
                        return "PTM";

                    case DisciplineTasks.OASISCStartofCareOT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                    case DisciplineTasks.OASISCRecertificationOT:
                    case DisciplineTasks.OASISCFollowupOT:
                    case DisciplineTasks.OASISCTransferOT:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDeathOT:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTVisit:
                        return "OT";

                    case DisciplineTasks.OTMaintenance:
                        return "OTM";

                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STDischarge:
                        return "ST";

                    case DisciplineTasks.STMaintenance:
                        return "STM";

                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWVisit:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.MSWProgressNote:
                        return "MSW";

                    case DisciplineTasks.HHAideSupervisoryVisit:
                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.HHAideCarePlan:
                        return "HHA";

                    case DisciplineTasks.COTAVisit:
                        return "OTA";

                    case DisciplineTasks.PhysicianOrder:
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                    case DisciplineTasks.FaceToFaceEncounter:
                    case DisciplineTasks.IncidentAccidentReport:
                    case DisciplineTasks.InfectionReport:
                    case DisciplineTasks.CommunicationNote:
                        return string.Empty;
                }
            }
            return string.Empty;
        }

        public static string TypeOfEvent(this ScheduleEvent schedule)
        {
            if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
            {
                var type = (DisciplineTasks)schedule.DisciplineTask;
                switch (type)
                {
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathOT:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.NonOASISDischarge:
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                    case DisciplineTasks.NonOASISRecertification:
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                    case DisciplineTasks.NonOASISStartofCare:
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNAssessmentRecert:
                        return "OASIS";
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.LVNSupervisoryVisit:
                    case DisciplineTasks.DieticianVisit:
                    case DisciplineTasks.DischargeSummary:
                    case DisciplineTasks.SixtyDaySummary:
                    case DisciplineTasks.TransferSummary:
                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTAVisit:
                    case DisciplineTasks.PTMaintenance:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.OTMaintenance:
                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STDischarge:
                    case DisciplineTasks.STMaintenance:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWVisit:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.MSWProgressNote:
                    case DisciplineTasks.HHAideSupervisoryVisit:
                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.HHAideCarePlan:
                    case DisciplineTasks.COTAVisit:
                    case DisciplineTasks.PASVisit:
                    case DisciplineTasks.PASCarePlan:
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        return "Notes";
                    case DisciplineTasks.PhysicianOrder:
                        return "PhysicianOrder";
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                    case DisciplineTasks.HCFA485StandAlone:
                        return "PlanOfCare";
                    case DisciplineTasks.FaceToFaceEncounter:
                        return "FaceToFaceEncounter";
                    case DisciplineTasks.IncidentAccidentReport:
                        return "IncidentAccident";
                    case DisciplineTasks.InfectionReport:
                        return "Infection";
                    case DisciplineTasks.CommunicationNote:
                        return "CommunicationNote";
                }
            }
            return string.Empty;
        }
    }
}
