﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.Core.Extension;


    public static class ClaimExtensions
    {
        public static bool IsRapDischage(this Rap rap)
        {
            var result = false;
            if (rap != null && rap.UB4PatientStatus.IsNotNullOrEmpty() && rap.UB4PatientStatus.IsInteger() && Enum.IsDefined(typeof(UB4PatientStatus), rap.UB4PatientStatus.ToInteger()))
            {
                var status = (UB4PatientStatus)rap.UB4PatientStatus.ToInteger();

                switch (status)
                {
                    case UB4PatientStatus.StillPatient:
                        result = false;
                        break;
                    case UB4PatientStatus.DischargeToHomeOrSelfCare:
                    case UB4PatientStatus.DischargeToHospiceHome:
                    case UB4PatientStatus.DischargeToHospiceMedicareFacility:
                    case UB4PatientStatus.DischargeToShortTermHospital:
                    case UB4PatientStatus.DischargeToSNF:
                        result = true;
                        break;
                }
            }
            return result;
        }

        public static bool IsFinalDischage(this Final final)
        {
            var result = false;
            if (final != null && final.UB4PatientStatus.IsNotNullOrEmpty() && final.UB4PatientStatus.IsInteger() && Enum.IsDefined(typeof(UB4PatientStatus), final.UB4PatientStatus.ToInteger()))
            {
                var status = (UB4PatientStatus)final.UB4PatientStatus.ToInteger();

                switch (status)
                {
                    case UB4PatientStatus.StillPatient:
                        result = false;
                        break;
                    case UB4PatientStatus.DischargeToHomeOrSelfCare:
                    case UB4PatientStatus.DischargeToHospiceHome:
                    case UB4PatientStatus.DischargeToHospiceMedicareFacility:
                    case UB4PatientStatus.DischargeToShortTermHospital:
                    case UB4PatientStatus.DischargeToSNF:
                        result = true;
                        break;
                }
            }
            return result;
        }

        public static bool IsManagedClamDischage(this ManagedClaim managedClaim)
        {
            var result = false;
            if (managedClaim != null && managedClaim.UB4PatientStatus.IsNotNullOrEmpty() && managedClaim.UB4PatientStatus.IsInteger() && Enum.IsDefined(typeof(UB4PatientStatus), managedClaim.UB4PatientStatus.ToInteger()))
            {
                var status = (UB4PatientStatus)managedClaim.UB4PatientStatus.ToInteger();

                switch (status)
                {
                    case UB4PatientStatus.StillPatient:
                        result = false;
                        break;
                    case UB4PatientStatus.DischargeToHomeOrSelfCare:
                    case UB4PatientStatus.DischargeToHospiceHome:
                    case UB4PatientStatus.DischargeToHospiceMedicareFacility:
                    case UB4PatientStatus.DischargeToShortTermHospital:
                    case UB4PatientStatus.DischargeToSNF:
                        result = true;
                        break;
                }
            }
            return result;
        }

        public static IDictionary<string, Locator> ToUb04Locator81Dictionary(this string ub04Locator81cca)
        {
            var questions = new Dictionary<string, Locator>();
            if (ub04Locator81cca.IsNotNullOrEmpty())
            {
                var locatorQuestions = ub04Locator81cca.ToObject<List<Locator>>();
                if (locatorQuestions != null && locatorQuestions.Count > 0)
                {
                    locatorQuestions.ForEach(n =>
                    {
                        questions.Add(n.LocatorId, n);
                    });
                }
            }
            return questions;
        }

        public static Dictionary<int, ChargeRate> ToInsurancBillDataDictionary(this  AgencyInsurance agencyInsurance)
        {
            var chargeRates = new Dictionary<int, ChargeRate>();
            if (agencyInsurance!=null && agencyInsurance.BillData.IsNotNullOrEmpty())
            {
                var rates = agencyInsurance.BillData.ToObject<List<ChargeRate>>();
                if (rates != null && rates.Count > 0)
                {
                    rates.ForEach(n =>
                    {
                        if (!chargeRates.ContainsKey(n.Id))
                        {
                            chargeRates.Add(n.Id, n);
                        }
                    });
                }
            }
            return chargeRates;
        }

        public static Dictionary<int, ChargeRate> ToInsurancBillDataDictionary(this  Final final)
        {
            var chargeRates = new Dictionary<int, ChargeRate>();
            if (final != null && final.Insurance.IsNotNullOrEmpty())
            {
                var insurance = final.Insurance.ToObject<AgencyInsurance>();
                if (insurance != null )
                {
                    chargeRates = insurance.ToInsurancBillDataDictionary();
                }
            }
            return chargeRates;
        }

        public static Dictionary<int, ChargeRate> ToInsurancBillDataDictionary(this  ManagedClaim managedClaim)
        {
            var chargeRates = new Dictionary<int, ChargeRate>();
            if (managedClaim != null && managedClaim.Insurance.IsNotNullOrEmpty())
            {
                var insurance = managedClaim.Insurance.ToObject<AgencyInsurance>();
                if (insurance != null)
                {
                    chargeRates = insurance.ToInsurancBillDataDictionary();
                }
            }
            return chargeRates;
        }

        public static IDictionary<string, Locator> ToUb04LocatorDictionary(this string ub04Locator)
        {
            var questions = new Dictionary<string, Locator>();
            if (ub04Locator.IsNotNullOrEmpty())
            {
                var locatorQuestions = ub04Locator.ToObject<List<Locator>>();
                if (locatorQuestions != null && locatorQuestions.Count > 0)
                {
                    locatorQuestions.ForEach(n =>
                    {
                        questions.Add(n.LocatorId, n);
                    });
                }
            }
            return questions;
        }

    }
}
