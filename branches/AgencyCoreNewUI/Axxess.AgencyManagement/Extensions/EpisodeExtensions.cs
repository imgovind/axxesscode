﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Axxess.Core.Extension;

namespace Axxess.AgencyManagement.Extensions
{
    public static class EpisodeExtensions
    {
        public static Dictionary<string, string> ParseFrequency(this string target)
        {
            Dictionary<string, string> freqs = new Dictionary<string, string>();
            if (target.IsNotNullOrEmpty())
            {
                Regex reg = new Regex("([\\w]* [\\w]*):[ ]*([1-7]w[1-9])([,]?[ ]?[1-7]w[1-9])*");
                MatchCollection matchCollection = reg.Matches(target);
                foreach (Match match in matchCollection)
                {
                    string freq = "";
                    string value = "";
                    if (match.Value.IsNotNullOrEmpty())
                    {
                        for (int i = 0; i < match.Groups.Count; i++)
                        {
                            if (match.Groups[i].Value.IsNotNullOrEmpty())
                            {
                                if (i == 1)
                                {
                                    freq = match.Groups[i].Value.Replace(" Frequency", "");
                                }
                                else if (i > 1)
                                {
                                    value += match.Groups[i].Value;
                                }
                            }
                        }
                        freqs.Add(freq, value);
                    }
                }
            }
            if (!freqs.ContainsKey("SN")) freqs.Add("SN", "");
            if (!freqs.ContainsKey("HHA")) freqs.Add("HHA", "");
            if (!freqs.ContainsKey("PT")) freqs.Add("PT", "");
            if (!freqs.ContainsKey("OT")) freqs.Add("OT", "");
            if (!freqs.ContainsKey("ST")) freqs.Add("ST", "");
            if (!freqs.ContainsKey("MSW")) freqs.Add("MSW", "");
            return freqs;
        }
    }
}
