﻿namespace Axxess.AgencyManagement.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Enums;

    public static class AgencyServicesExtensions
    {
        public static bool Has(this AgencyServices service, AgencyServices value)
        {
            try
            {
                return (((int)(AgencyServices)service & (int)(AgencyServices)value) == (int)(AgencyServices)value);
            }
            catch
            {
                return false;
            }
        }
    }
}
