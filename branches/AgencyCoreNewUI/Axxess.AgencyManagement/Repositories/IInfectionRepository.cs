﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IInfectionRepository 
    {
        bool AddInfection(Infection infection);
        bool UpdateInfection(Infection infection);
        bool UpdateInfectionModal(Infection infection);
        bool MarkInfectionsAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
        bool ReassignInfectionsUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        bool RemoveInfection(Guid Id);
        Infection GetInfectionReport(Guid agencyId, Guid id);
        IList<Infection> GetInfections(Guid agencyId);
    }
}
