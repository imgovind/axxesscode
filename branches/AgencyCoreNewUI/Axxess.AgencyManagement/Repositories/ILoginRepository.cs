﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using Enums;
    using Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.Core;

    public interface ILoginRepository
    {
        Login Find(Guid id);
        bool Add(Login login);
        bool Update(Login login);
        Login Find(string emailAddress);
        bool AddImpersonationLink(ImpersonationLink link);

        bool AddUserSnapshot(UserSnapshot userSnapshot);

        bool AddAgencySnapshot(AgencySnapshot agencySnapshot);
        bool UpdateAgencySnapshot(AgencySnapshot agencySnapshot);

        bool ToggleReadOnly(Guid id, DateTime readOnlyDate, bool isFrozen);
        bool ToggleDelete(Guid id);

        UserSnapshot GetUserSnapshot(Guid userId);
        UserSnapshot GetUserSnapshot(Guid agencyId, Guid userId);
        List<UserSnapshot> GetUserSnapshotByLoginIdWithClusterid(Guid loginId);
        List<UserSnapshot> GetUserSnapshotByLoginId(Guid loginId);
        int Cluster(Guid agencyId);
        AgencySnapshot GetAgencySnapshot(Guid agencyId);
        bool RemoveAgencySnapshot(Guid Id);
        List<AgencySnapshot>  GetAgencySnapshotNames(List<Guid> agencyIds);
        IList<UserSelection> GetUsersSnapShotWithLoginInfo(Guid agencyId);
        IList<AgencySnapshot> All();
        bool IsAtLeastOneUserExist(Guid loginId);
        bool IsAtLeastOnePhysicianExist(Guid loginId);

        AgencyAccountData GetReadOnlyAcccountId(Guid agencyId);
        AgencySpecialist GetImplementationSpecialist(Guid agencyId);
        bool UpdateImplementationSpecialist(Guid agencyId, Guid implementationSpecialistId);
        IList<AgencySpecialist> GetImplementationSpecialists(Guid implementationSpecialistId, DateTime startDate, DateTime endDate);


    }
}
