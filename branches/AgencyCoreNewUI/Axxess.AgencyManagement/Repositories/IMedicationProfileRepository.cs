﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IMedicationProfileRepository
    {
        bool AddNewMedicationProfile(MedicationProfile medication);
        bool SaveMedicationProfile(MedicationProfile medicationProfile);
        bool UpdateMedication(MedicationProfile medicationProfile);
        bool UpdateMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType);
        bool UpdateMedicationForDischarge(Guid MedId, Guid agencyId, Guid Id, DateTime DischargeDate);
        bool DeleteMedicationProfile(Guid Id, Guid agencyId);
       
        MedicationProfile InsertMedication(Guid Id, Guid agencyId, Medication medication, string MedicationType);
        MedicationProfile DeleteMedication(Guid MedId, Guid agencyId, Medication medication);
        MedicationProfile GetMedicationProfileByPatient(Guid PatientId, Guid AgencyId);
        MedicationProfile GetMedicationProfile(Guid Id, Guid AgencyId);
    }
}
