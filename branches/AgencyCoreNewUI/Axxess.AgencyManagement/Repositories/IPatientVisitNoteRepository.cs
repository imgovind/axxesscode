﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Domain;
using Axxess.Core.Enums;

namespace Axxess.AgencyManagement.Repositories
{
    public interface IPatientVisitNoteRepository
    {
        bool AddVisitNote(PatientVisitNote patientVisitNote);
        bool UpdateVisitNote(PatientVisitNote patientVisitNote);
        bool MarkVisitNoteAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated);
        bool ReassignNotesUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        PatientVisitNote GetVisitNote(Guid agencyId, Guid patientId, Guid noteId);
        PatientVisitNote GetVisitNote(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId);
        PatientVisitNote GetHHAPlanOfCareVisitNote(Guid episodeId, Guid patientId);
        PatientVisitNote GetVisitNoteByType(Guid episodeId, Guid patientId, DisciplineTasks disciplineTask);
        List<PatientVisitNote> GetVisitNotesByDisciplineTaskWithStatus(Guid agencyId, Guid patientId, int[] scheduleStatus, int[] disciplineTasks);
        List<PatientVisitNote> GetEvalOrders(Guid agencyId, string evalIds, DateTime startDate, DateTime endDate);
        List<PatientVisitNote> GetEvalOrders(Guid agencyId, List<int> status, string evalIds, DateTime startDate, DateTime endDate);
    }
}
