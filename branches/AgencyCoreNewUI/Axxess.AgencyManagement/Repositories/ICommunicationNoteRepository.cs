﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface ICommunicationNoteRepository
    {
        bool AddCommunicationNote(CommunicationNote communicationNote);
        bool EditCommunicationNote(CommunicationNote communicationNote);
        bool UpdateCommunicationNoteModal(CommunicationNote communicationNote);
        bool DeleteCommunicationNote(Guid agencyId, Guid Id, Guid patientId, bool isDeprecated);
        bool ReassignCommunicationNoteUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        CommunicationNote GetCommunicationNote(Guid Id, Guid patientId, Guid agencyId);
        List<CommunicationNote> GetCommunicationNotes(Guid agencyId, Guid patientId);
        List<CommunicationNote> GetCommunicationNoteByIds(Guid agencyId, string orderIds);
        List<CommunicationNote> GetAllCommunicationNotes();
    }
}
