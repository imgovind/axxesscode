﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using Domain;

    public interface IUserRepository
    {
        User GetUserOnly(Guid id, Guid agencyId, int clusterId);
        User Get(Guid id, Guid agencyId, int clusterId);
        User GetUserForName(Guid agencyId, Guid userId, int clusterId);
       
        IList<User> GetUsersByLoginId(Guid loginId, int clusterId);
        bool AddUserOnly(User user, int clusterId);
        bool Refresh(User user, int clusterId);
        IList<User> GetAgencyUsers(Guid agencyId, int clusterId);

        List<User> GetUsersWithLoginInfo(List<Guid> userIds, int clusterId);
        IList<User> GetUsersWithLoginInfo(Guid agencyId, List<Guid> userIds, int clusterId);

        IList<User> GetUsersByRoleOrTitle(Guid agencyId, int agencyRoleId, string titleType, string stateCode, int clusterId);
        User GetPrimaryUser(Guid agencyId, int clusterId);
        bool TogglePrimaryUser(Guid id, Guid agencyId, int clusterId);
        User GetUserProfileData(Guid agencyId, Guid loginId, int clusterId);
        bool UpdateUserProfileData(Guid agencyId, Guid userId, string profileData, int clusterId);

        bool LinkUserLocation(UserLocation userLocation, int clusterId);


        //
        //
        //
        //
        //
        //IList<UserSelection> GetUsersWithLoginInfo(Guid agencyId, int clusterId);
        //
        //
        //
     
    }
}
