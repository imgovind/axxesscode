﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

   public interface IScheduleRepository
    {
       bool AddScheduleEvent(ScheduleEvent scheduleEvent);

       bool UpdateScheduleEventNew(ScheduleEvent scheduleEvent);

       bool RemoveScheduleEventNew(ScheduleEvent scheduleEvent);
       bool RemoveScheduleEventNew(Guid agencyId, Guid patientId, Guid episodeId, Guid eventId);

       ScheduleEvent GetScheduleEventNew(Guid agencyId, Guid patientId, Guid eventId);
       ScheduleEvent GetScheduleEventNew(Guid agencyId, Guid patientId, Guid episodeId, Guid eventId);
       ScheduleEvent GetLastScheduledEvents(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks);
       ScheduleEvent GetFirstScheduledEvents(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks);
       ScheduleEvent FirstBillableEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate);

      // List<ScheduleEvent> GetPatientScheduledEventsNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline, bool isReportAndNotesIncluded);
       List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded);
       List<ScheduleEvent> GetScheduledEventsOnlyLean(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded);
       List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate);
       List<ScheduleEvent> GetPastDueRecertsLeanByDateRange(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate);
       List<ScheduleEvent> GetUpcomingRecertsLean(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate);
       List<ScheduleEvent> GetPlanOfCareOrderScheduleEvents(Guid agencyId, DateTime startDate, DateTime endDate, int[] disciplineTasks, int[] status);
       List<ScheduleEvent> GetTherapyExceptionScheduleEvents(Guid agencyId, Guid branchId, DateTime fromDate, DateTime toDate);
       List<ScheduleEvent> GetPhysicianOrderScheduleEvents(Guid agencyId, DateTime startDate, DateTime endDate, int[] status);
       List<ScheduleEvent> GetOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> status);
       List<ScheduleEvent> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
       List<ScheduleEvent> GetPendingSignatureOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate);
       List<ScheduleEvent> GetEpisodesForSurveyCensesAndPatientRoster(Guid agencyId, string patientIds);
       List<ScheduleEvent> GetPatientScheduledEventsOnlyNew(Guid agencyId, Guid episodeId, Guid patientId);
       //List<ScheduleEvent> GetPatientScheduledEventsWithDateRangeOrNotNew(Guid agencyId, Guid episodeId, Guid patientId, bool IsDateRange, DateTime startDate, DateTime endDate);
      // List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange);
       List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange);
       IList<ScheduleEvent> GetScheduledEventsByEmployeeAssignedNew(Guid agencyId, Guid patientId, Guid employeeId);
       IList<ScheduleEvent> GetScheduledEventsByEmployeeAssignedNew(Guid agencyId, Guid employeeId);
       List<ScheduleEvent> GetDeletedItemsNew(Guid agencyId, Guid patientId);
       List<ScheduleEvent> GetPreviousNotes(Guid agencyId, ScheduleEvent scheduledEvent, int[] disciplineTasks, int[] status);
       List<ScheduleEvent> GetPreviousNotes(Guid agencyId, ScheduleEvent scheduledEvent, string[] disciplines, int[] disciplineTasks, int[] status);
       List<ScheduleEvent> GetCurrentAndPerviousOrders(Guid agencyId, PatientEpisode episode, int[] disciplineTasks);
       List<ScheduleEvent> GetScheduleByBranchAndStatus(Guid agencyId, Guid branchId, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded);
       List<ScheduleEvent> GetScheduleByBranchDateRangeAndStatusLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded);
       List<ScheduleEvent> GetScheduleByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded);
       List<ScheduleEvent> GetMissedVisitSchedulesLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus);
       List<ScheduleEvent> GetScheduleDeviations(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded);
       List<ScheduleEvent> GetPrintQueueEvents(Guid agencyId);
       List<ScheduleEvent> GetScheduledEventsForOASISAndNotesByPatient(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] notesScheduleStatus, int[] notesDisciplineTasks, int[] oasisScheduleStatus, int[] oasisDisciplineTasks);
       List<ScheduleEvent> GetScheduledEventsForOASISAndNotesByEpisode(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, int[] notesScheduleStatus, int[] notesDisciplineTasks, int[] oasisScheduleStatus, int[] oasisDisciplineTasks);
       List<ScheduleEvent> GetScheduleEventsVeryLean(Guid agencyId, Guid branchId, Guid patientId, Guid episodeId, int[] scheduleStatus, int[] disciplineTasks, int patientStatus, DateTime startDate, DateTime endDate, bool IsDateRange, bool IsMissedVisitIncluded);
       List<ScheduleEvent> GetPatientScheduledEventsLeanWithUserId(Guid agencyId, Guid patientId, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit);


       List<ScheduleEvent> GetPatientScheduledEventsNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline, bool isReportAndNotesIncluded);
       List<ScheduleEvent> GetPatientScheduledEventsWithDateRangeOrNotNew(Guid agencyId, Guid episodeId, Guid patientId, bool IsDateRange, DateTime startDate, DateTime endDate);
       List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange);

       List<UserVisit> GetUserVisitLean(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded);
       List<UserVisit> GetUserVisitLeanByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded);
       List<UserVisit> GetPayrollSummmaryVisits(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid);
       List<VisitSummary> GetPayrollSummmaryLean(Guid agencyId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid);


       bool UpdateScheduleEventsForIsBillable(Guid agencyId, List<ScheduleEvent> scheduleEvents);
       bool IsFirstBillableVisit(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate);


       bool AddMissedVisit(MissedVisit missedVisit);
       bool UpdateMissedVisit(MissedVisit missedVisit);
       MissedVisit GetMissedVisit(Guid agencyId, Guid id);

    }
}
