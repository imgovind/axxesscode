﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IAgencyContactRepository
    {
        bool AddContact(AgencyContact contact);
        bool EditContact(AgencyContact contact);
        bool DeleteContact(Guid agencyId, Guid id);
        AgencyContact FindContact(Guid agencyId, Guid Id);
        IList<AgencyContact> GetContacts(Guid agencyId);
    }
}
