﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IReportRepository
    {
        bool AddReport(Report report);
        bool UpdateReport(Report report);
        Report GetReport(Guid agencyId, Guid reportId);
        IList<ReportLite> GetReports(Guid agencyId);
    }
}
