﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IMedicationProfileHistoryRepository
    {
        bool AddNewMedicationHistory(MedicationProfileHistory medicationHistory);
        bool UpdateMedicationProfileHistory(MedicationProfileHistory medicationProfile);
        MedicationProfileHistory GetMedicationProfileHistory(Guid Id, Guid AgencyId);
        IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId, Guid agencyId);
        IList<MedicationProfileHistory> GetAllMedicationProfileHistory();
    }
}
