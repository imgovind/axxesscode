﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IPatientAdmissionRepository
    {
        bool AddPatientAdmissionDate(PatientAdmissionDate managedDate);
        bool PatientAdmissionEdit(Patient patient);
        bool PatientAdmissionAdd(Patient patient);
        bool UpdatePatientAdmissionDate(PatientAdmissionDate managedDate);
        bool UpdatePatientAdmissionDateModal(PatientAdmissionDate managedDate);
        bool DeletePatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id);
        bool DeletePatientAdmissionDates(Guid agencyId, Guid patientId);
        bool DeprecatedPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id);
        bool DeprecatedPatientAdmissionDates(Guid agencyId, Guid patientId);

        PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id);
        PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid admissionId);
        PatientAdmissionDate GetPatientLatestAdmissionDate(Guid agencyId, Guid patientId, DateTime date);
        IList<PatientAdmissionDate> GetPatientAdmissionDates(Guid agencyId, Guid patientId);
    }
}
