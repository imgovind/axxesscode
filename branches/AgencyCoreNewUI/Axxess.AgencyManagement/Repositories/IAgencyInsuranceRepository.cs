﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IAgencyInsuranceRepository
    {
        bool AddInsurance(AgencyInsurance insurance);
        bool EditInsurance(AgencyInsurance insurance);
        bool EditInsuranceModal(AgencyInsurance insurance);
        bool DeleteInsurance(Guid agencyId, int Id);
        bool IsMedicareHMO(Guid agencyId, int Id);
        AgencyInsurance GetInsurance(int insuranceId, Guid agencyId);
        AgencyInsurance FindInsurance(Guid agencyId, int Id);
        IList<AgencyInsurance> GetInsurances(Guid agencyId);
        List<InsuranceLean> GetLeanInsurances(Guid agencyId, int[] insuranceIds);
    }
}
