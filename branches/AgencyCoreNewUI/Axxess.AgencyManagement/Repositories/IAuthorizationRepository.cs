﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IAuthorizationRepository
    {
        bool AddAuthorization(Authorization authorization);
        bool EditAuthorization(Authorization authorization);
        bool DeleteAuthorization(Guid agencyId, Guid patientId, Guid Id);
        Authorization GetAuthorization(Guid agencyId, Guid Id);
        Authorization GetAuthorization(Guid agencyId, Guid patientId, Guid Id);
        IList<Authorization> GetAuthorizations(Guid agencyId, Guid patientId);
        IList<Authorization> GetActiveAuthorizations(Guid agencyId, Guid patientId, string insuranceId, string status, DateTime startDate, DateTime endDate);

    }
}
