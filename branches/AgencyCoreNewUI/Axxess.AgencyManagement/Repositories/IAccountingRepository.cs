﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using Domain;
    using Axxess.Core.Infrastructure;

    public interface IAccountingRepository
    {
        List<Information> GetAllAccountingInformation(DateTime start, DateTime end);
        List<Information> GetAllAccountingInformation(DateTime start, DateTime end, DateTime lastYearStart, DateTime lastYearEnd);
        List<Information> GetOneAccountingInformation(string accountId);
        bool UpdateAccountingInformation(string account, string name, string city, string state, int zip, string implementationFee, string paymentSource, DateTime initialSignDate, string specialNotes, int status, int startUp, string salesPerson);
        bool UpdateAccountingpayments(int Id, DateTime start, DateTime end, decimal amount, int package);
        List<Payment> GetAccountingPayments(string accountId);
        bool AddAccountingpayments(string account, int sequence, DateTime start, decimal amount, int package);
        List<Payment> GetAllFirstPayment();
        List<Payment> GetAllPayments();
        bool UpdateLastPayment(int Id, DateTime endDate);
        bool UpdateLastPaymentStartDate(int Id, decimal amount, DateTime startDate);
        bool UpdateReadOnlyInformation(string account, string specialNotes, int status);
        Payment GetLastPayments(string account);
        Payment GetPrePayments(string account, int sequence);
        bool UpdateReadOnlyLastPayment(int Id, DateTime endDate);
        bool UpdateUndoReadOnlyInformation(string account, int status);

        List<AccountInfo> GetAccountInfo(DateTime start, DateTime end);

        bool UpdateAgencyUpgrade(Guid Id, Guid agencyId, double amount, double previousAmount, DateTime effectiveDate, string comments, bool isComplete);
        bool UpdateAgencyUpgrade(AgencyUpgrade agencyUpgrade);
        AgencyUpgrade GetAgencyUpgradeOnly(Guid id, Guid agencyId);
        IList<AgencyUpgrade> GetAgencyUpgrades(string isCompleted, DateTime startDate, DateTime endDate, int clusterId);
        IList<AgencyUpgrade> GetExistingAgencyUpgrades(List<Guid> agencyIds);

        IList<AgencyUpgrade> GetAgencyAnnualPlan(string isCompleted, DateTime startDate, DateTime endDate, int clusterId);


        AgencyChange GetAgencyChangePrev(Guid Id, Guid agencyId);

        IList<AgencyChange> GetAgencyChanges(string isComplete, DateTime startDate, DateTime endDate);
        bool UpdateAgencyChange(Guid Id, Guid agencyId, string comments, bool isComplete);

        bool AddSubscriptionPlan(AgencySubscriptionPlan plan);
        bool UpdateSubscriptionPlan(AgencySubscriptionPlan plan);
        AgencySubscriptionPlan GetSubscriptionPlan(Guid agencyId, Guid locationId);

        IList<AgencySubscriptionPlan> GetLocationSubscriptionPlanByLocationId(Guid agencyId, List<Guid> locationIds);

        bool AddAgencyProfile(Guid AgencyId, bool IsSuspendPayment, DateTime ReadOnlyDate, string FrozenReason, string AssetId);

        IList<AgencyMarkAsReadOnly> GetAgencyMarkAsReadOnly(DateTime startDate, DateTime endDate);

        bool ToggleAccuntAgency(Guid id);
        bool AddAccountAgency(Guid agencyId, Guid branchId, int clusterId);
        bool RemoveAccountAgency(Guid Id);

        bool Add(Asset asset);
        bool removeAccountingpayments(string account, int sequence);

    }
}
