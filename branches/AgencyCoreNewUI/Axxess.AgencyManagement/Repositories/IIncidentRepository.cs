﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IIncidentRepository
    {
        bool AddIncident(Incident incident);
        bool UpdateIncident(Incident incident);
        bool UpdateIncidentModal(Incident incident);
        bool MarkIncidentAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
        bool ReassignIncidentUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        bool RemoveIncidentReport(Guid Id);
        Incident GetIncidentReport(Guid agencyId, Guid id);
        IList<Incident> GetIncidents(Guid agencyId);
    }
}
