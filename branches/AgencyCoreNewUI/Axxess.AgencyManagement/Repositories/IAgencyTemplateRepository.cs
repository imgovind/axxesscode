﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IAgencyTemplateRepository
    {
        bool AddTemplate(AgencyTemplate template);
        void InsertTemplates(Guid agencyId);
        bool UpdateTemplate(AgencyTemplate template);
        bool DeleteTemplate(Guid agencyId, Guid id);
        AgencyTemplate GetTemplate(Guid agencyId, Guid id);
        IList<AgencyTemplate> GetTemplates(Guid agencyId);
    }
}
