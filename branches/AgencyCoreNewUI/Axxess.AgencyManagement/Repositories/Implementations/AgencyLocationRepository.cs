﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;

    public class AgencyLocationRepository : IAgencyLocationRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyLocationRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion


        public bool AddLocation(AgencyLocation agencyLocation)
        {
            if (agencyLocation != null)
            {
                if (agencyLocation.PhoneArray != null && agencyLocation.PhoneArray.Count > 0)
                {
                    agencyLocation.PhoneWork = agencyLocation.PhoneArray.ToArray().PhoneEncode();
                }
                if (agencyLocation.FaxNumberArray != null && agencyLocation.FaxNumberArray.Count > 0)
                {
                    agencyLocation.FaxNumber = agencyLocation.FaxNumberArray.ToArray().PhoneEncode();
                }

                agencyLocation.Created = DateTime.Now;
                agencyLocation.Modified = DateTime.Now;

                database.Add<AgencyLocation>(agencyLocation);
                return true;
            }

            return false;
        }

        public bool EditLocationModal(AgencyLocation location)
        {
            var result = false;
            if (location != null)
            {
                try
                {
                    location.Modified = DateTime.Now;
                    database.Update<AgencyLocation>(location);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool EditBranchCost(AgencyLocation location)
        {
            var result = false;
            if (location != null)
            {
                var existingLocation = database.Single<AgencyLocation>(l => l.AgencyId == location.AgencyId && l.Id == location.Id);
                if (existingLocation != null)
                {
                    existingLocation.Cost = location.Cost;
                    existingLocation.Ub04Locator81cca = location.Ub04Locator81cca;
                    existingLocation.Modified = DateTime.Now;
                    database.Update<AgencyLocation>(existingLocation);
                    result = true;
                }
            }
            return result;
        }

        public AgencyLocation GetMainLocation(Guid agencyId)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.IsMainOffice == true && l.IsDeprecated == false);
        }

        public AgencyLocation FindLocation(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == Id);
        }

        public IList<AgencyLocation> GetBranches(Guid agencyId)
        {
            return database.GetPaged<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false, "Created", 0, 20).ToList();
        }

        public IList<AgencyLocation> GetAllBranches(Guid agencyId)
        {
            return database.Find<AgencyLocation>(l => l.AgencyId == agencyId && l.IsDeprecated == false).ToList();
        }

        public List<AgencyLocation> AgencyLocations(Guid agencyId, string locations)
        {
            var script = string.Format(@"SELECT 
                                Id as Id,
                                Name as Name
                                    FROM 
                                        agencylocations 
                                                WHERE
                                                    AgencyId = @agencyid AND 
                                                    IsDeprecated = 0 AND
                                                    Id IN ( {0} ) ", locations);

            var list = new List<AgencyLocation>();
            using (var cmd = new FluentCommand<AgencyLocation>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new AgencyLocation
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetString("Name").ToUpperCase()
                }).AsList();
            }
            return list;
        }

    }
}
