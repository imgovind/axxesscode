﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;

    public class EmergencyContactRepository : IEmergencyContactRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public EmergencyContactRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        public bool AddEmergencyContact(PatientEmergencyContact emergencyContactInfo)
        {
            var result = false;
            try
            {
                if (emergencyContactInfo != null)
                {
                    emergencyContactInfo.Id = Guid.NewGuid();
                    if (emergencyContactInfo.PhonePrimaryArray.Count >= 2)
                    {
                        emergencyContactInfo.PrimaryPhone = emergencyContactInfo.PhonePrimaryArray.ToArray().PhoneEncode();
                    }
                    if (emergencyContactInfo.PhoneAlternateArray.Count >= 2)
                    {
                        emergencyContactInfo.AlternatePhone = emergencyContactInfo.PhoneAlternateArray.ToArray().PhoneEncode();
                    }
                    database.Add<PatientEmergencyContact>(emergencyContactInfo);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool EditEmergencyContact(Guid agencyId, PatientEmergencyContact emergencyContact)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            bool result = false;
            try
            {
                if (emergencyContact != null)
                {
                    if (emergencyContact.PhonePrimaryArray.Count >= 2)
                    {
                        emergencyContact.PrimaryPhone = emergencyContact.PhonePrimaryArray.ToArray().PhoneEncode();
                    }
                    if (emergencyContact.PhoneAlternateArray.Count >= 2)
                    {
                        emergencyContact.AlternatePhone = emergencyContact.PhoneAlternateArray.ToArray().PhoneEncode();
                    }
                    var emergencyContactToEdit = database.Single<PatientEmergencyContact>(e => e.PatientId == emergencyContact.PatientId && e.Id == emergencyContact.Id);
                    if (emergencyContactToEdit != null)
                    {
                        emergencyContactToEdit.FirstName = emergencyContact.FirstName;
                        emergencyContactToEdit.LastName = emergencyContact.LastName;
                        emergencyContactToEdit.PrimaryPhone = emergencyContact.PrimaryPhone;
                        emergencyContactToEdit.AlternatePhone = emergencyContact.AlternatePhone;
                        emergencyContactToEdit.EmailAddress = emergencyContact.EmailAddress;
                        emergencyContactToEdit.Relationship = emergencyContact.Relationship;
                        if (emergencyContact.IsPrimary)
                        {
                            if (SetPrimaryEmergencyContact(agencyId, emergencyContact.PatientId, emergencyContact.Id))
                            {
                                emergencyContactToEdit.IsPrimary = true;
                            }
                        }
                        database.Update<PatientEmergencyContact>(emergencyContactToEdit);
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteEmergencyContact(Guid Id, Guid patientId)
        {
            bool result = false;
            try
            {
                if (!Id.IsEmpty() && !patientId.IsEmpty())
                {
                    var emergencyContact = database.Single<PatientEmergencyContact>(r => r.PatientId == patientId && r.Id == Id);
                    if (emergencyContact != null)
                    {
                        database.Delete<PatientEmergencyContact>(emergencyContact.Id);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteEmergencyContacts(Guid patientId)
        {
            bool result = false;
            try
            {
                if (!patientId.IsEmpty())
                {
                    database.DeleteMany<PatientEmergencyContact>(pec => pec.PatientId == patientId);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }


        public bool SetPrimaryEmergencyContact(Guid agencyId, Guid patientId, Guid emergencyContactId)
        {
            bool result = false;
            try
            {
                var patient = database.Single<Patient>(p => p.Id == patientId);
                if (patient != null)
                {
                    var emergencyContacts = database.Find<PatientEmergencyContact>(p => p.PatientId == patientId);
                    bool flag = false; ;
                    foreach (PatientEmergencyContact contat in emergencyContacts)
                    {
                        if (contat.Id == emergencyContactId)
                        {
                            contat.IsPrimary = true;
                            flag = true;
                        }
                        else
                        {
                            contat.IsPrimary = false;
                        }
                    }
                    if (flag)
                    {
                        database.UpdateMany<PatientEmergencyContact>(emergencyContacts);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid emergencyContactId)
        {
            Check.Argument.IsNotEmpty(emergencyContactId, "emergencyContactId");
            return database.Single<PatientEmergencyContact>(e => e.PatientId == patientId && e.Id == emergencyContactId);
        }

        public IList<PatientEmergencyContact> GetEmergencyContacts(Guid patientId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Find<PatientEmergencyContact>(p => p.PatientId == patientId).OrderBy(p => p.IsPrimary == false).ToList();
        }

        public List<EmergencyContactInfo> GetEmergencyContactInfos(Guid agencyId, Guid branchId, int status)
        {
            var emergencyContactInfos = new List<EmergencyContactInfo>();
            var script = string.Format(@"SELECT 
                            patients.FirstName as FirstName , 
                            patients.LastName as LastName ,
                            patients.PatientIdNumber as PatientIdNumber ,
                            patients.Triage as Triage , 
                            patientemergencycontacts.FirstName as ContactFirstName,
                            patientemergencycontacts.LastName as ContactLastName ,
                            patientemergencycontacts.Relationship as Relationship ,
                            patientemergencycontacts.PrimaryPhone as PrimaryPhone ,
                            patientemergencycontacts.EmailAddress as EmailAddress 
                                FROM 
                                    patientemergencycontacts
                                        INNER JOIN patients ON patientemergencycontacts.PatientId = patients.Id 
                                            WHERE 
                                                patients.AgencyId = @agencyId  AND
                                                patientemergencycontacts.IsPrimary = 1 {0} {1} AND 
                                                patients.IsDeprecated = 0 
                                                 ORDER BY patients.LastName ASC , patients.FirstName ASC ", status == 0 ? " AND patients.Status IN ( 1,2 )  " : " AND patients.Status = " + status, !branchId.IsEmpty() ? "AND patients.AgencyLocationId = @branchId" : string.Empty);
            using (var cmd = new FluentCommand<EmergencyContactInfo>(script))
            {
                emergencyContactInfos = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("branchId", branchId)
                    .SetMap(reader => new EmergencyContactInfo
                    {
                        PatientName = string.Format("{0}, {1}", reader.GetStringNullable("LastName").ToUpperCase(), reader.GetStringNullable("FirstName").ToUpperCase()),
                        Triage = reader.GetInt("Triage"),
                        PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                        ContactName = string.Format("{0}, {1}", reader.GetStringNullable("ContactLastName").ToUpperCase(), reader.GetStringNullable("ContactFirstName").ToUpperCase()),
                        ContactRelation = reader.GetStringNullable("Relationship"),
                        ContactPhoneHome = reader.GetStringNullable("PrimaryPhone").ToPhone(),
                        ContactEmailAddress = reader.GetStringNullable("EmailAddress")
                    })
                    .AsList();
            }

            return emergencyContactInfos;

        }


    }
}
