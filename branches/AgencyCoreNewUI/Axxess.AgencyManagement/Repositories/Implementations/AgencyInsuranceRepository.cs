﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;

    public class AgencyInsuranceRepository : IAgencyInsuranceRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyInsuranceRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region Insurance

        public bool AddInsurance(AgencyInsurance insurance)
        {
            var result = false;
            if (insurance != null)
            {
                if (!insurance.AgencyId.IsEmpty() && insurance.OldInsuranceId > 0)
                {
                    var oldInsurance = GetInsurance(insurance.OldInsuranceId, insurance.AgencyId);
                    if (oldInsurance != null && oldInsurance.BillData.IsNotNullOrEmpty())
                    {
                        insurance.BillData = oldInsurance.BillData;
                    }
                }
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    insurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.SubmitterPhoneArray.Count > 0)
                {
                    insurance.SubmitterPhone = insurance.SubmitterPhoneArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    insurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                insurance.Created = DateTime.Now;
                insurance.Modified = DateTime.Now;

                database.Add<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public bool EditInsurance(AgencyInsurance insurance)
        {
            var result = false;
            var existingInsurance = database.Single<AgencyInsurance>(I => I.AgencyId == insurance.AgencyId && I.Id == insurance.Id);
            if (insurance != null && existingInsurance != null)
            {
                if (insurance.PhoneNumberArray.Count > 0)
                {
                    existingInsurance.PhoneNumber = insurance.PhoneNumberArray.ToArray().PhoneEncode();
                }
                if (insurance.SubmitterPhoneArray.Count > 0)
                {
                    insurance.SubmitterPhone = insurance.SubmitterPhoneArray.ToArray().PhoneEncode();
                }
                if (insurance.FaxNumberArray.Count > 0)
                {
                    existingInsurance.FaxNumber = insurance.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingInsurance.PayorType = insurance.PayorType;
                existingInsurance.InvoiceType = insurance.InvoiceType;
                existingInsurance.ChargeGrouping = insurance.ChargeGrouping;
                existingInsurance.ChargeType = insurance.ChargeType;
                existingInsurance.ParentInsurance = insurance.ParentInsurance;
                existingInsurance.Name = insurance.Name;
                existingInsurance.AddressLine1 = insurance.AddressLine1;
                existingInsurance.AddressLine2 = insurance.AddressLine2;
                existingInsurance.AddressCity = insurance.AddressCity;
                existingInsurance.AddressStateCode = insurance.AddressStateCode;
                existingInsurance.AddressZipCode = insurance.AddressZipCode;
                existingInsurance.ProviderId = insurance.ProviderId;
                existingInsurance.ProviderSubscriberId = insurance.ProviderSubscriberId;
                existingInsurance.OtherProviderId = insurance.OtherProviderId;
                existingInsurance.Ub04Locator81cca = insurance.Ub04Locator81cca;
                existingInsurance.PayorId = insurance.PayorId;
                existingInsurance.ContactPersonFirstName = insurance.ContactPersonFirstName;
                existingInsurance.ContactPersonLastName = insurance.ContactPersonLastName;
                existingInsurance.ContactEmailAddress = insurance.ContactEmailAddress;
                existingInsurance.CurrentBalance = insurance.CurrentBalance;
                existingInsurance.WorkWeekStartDay = insurance.WorkWeekStartDay;
                existingInsurance.IsVisitAuthorizationRequired = insurance.IsVisitAuthorizationRequired;
                existingInsurance.Charge = insurance.Charge;
                existingInsurance.SubmitterId = insurance.SubmitterId;
                existingInsurance.BillType = insurance.BillType;
                existingInsurance.SubmitterPhone = insurance.SubmitterPhone;
                existingInsurance.IsAxxessTheBiller = insurance.IsAxxessTheBiller;
                existingInsurance.ClearingHouse = insurance.ClearingHouse;
                existingInsurance.InterchangeReceiverId = insurance.InterchangeReceiverId;
                existingInsurance.ClearingHouseSubmitterId = insurance.ClearingHouseSubmitterId;
                existingInsurance.SubmitterName = insurance.SubmitterName;
                existingInsurance.SubmitterPhone = insurance.SubmitterPhone;
                existingInsurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(existingInsurance);
                result = true;
            }
            return result;
        }

        public bool EditInsuranceModal(AgencyInsurance insurance)
        {
            var result = false;

            if (insurance != null)
            {
                insurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(insurance);
                result = true;
            }
            return result;
        }

        public bool DeleteInsurance(Guid agencyId, int Id)
        {
            var insurance = database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == Id);
            if (insurance != null)
            {
                insurance.IsDeprecated = true;
                insurance.Modified = DateTime.Now;
                database.Update<AgencyInsurance>(insurance);
                return true;
            }
            return false;
        }

        public bool IsMedicareHMO(Guid agencyId, int Id)
        {
            var result = false;
            var insurance = database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == Id);
            if (insurance != null)
            {
                result = (insurance.PayorType == 2);
            }
            return result;
        }

        public AgencyInsurance GetInsurance(int insuranceId, Guid agencyId)
        {
            return database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == insuranceId);
        }

        public AgencyInsurance FindInsurance(Guid agencyId, int Id)
        {
            return database.Single<AgencyInsurance>(i => i.AgencyId == agencyId && i.Id == Id);
        }

        public IList<AgencyInsurance> GetInsurances(Guid agencyId)
        {
            return database.Find<AgencyInsurance>(i => i.AgencyId == agencyId && i.IsDeprecated == false).ToList();
        }

        public List<InsuranceLean> GetLeanInsurances(Guid agencyId, int[] insuranceIds)
        {
            var additionalScript = string.Empty;
            if (insuranceIds != null && insuranceIds.Length > 0)
            {
                additionalScript += string.Format(" AND agencyinsurances.Id IN ( {0} ) ", insuranceIds.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT agencyinsurances.Id,
                        agencyinsurances.PayorType, 
                        agencyinsurances.Name,
                        agencyinsurances.InvoiceType, 
                        agencyinsurances.ContactPersonFirstName, 
                        agencyinsurances.ContactPersonLastName, 
                        agencyinsurances.PhoneNumber,
                        agencyinsurances.PayorId 
                                FROM agencyinsurances 
                                        WHERE agencyinsurances.AgencyId = @agencyid AND agencyinsurances.IsDeprecated = 0 {0}", additionalScript);

            return new FluentCommand<InsuranceLean>(script)
                .SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new InsuranceLean
                {
                    Id = reader.GetInt("Id"),
                    Name = reader.GetStringNullable("Name"),
                    PayorType = reader.GetInt("PayorType"),
                    InvoiceType = reader.GetInt("InvoiceType"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName").ToUpperCase(),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName").ToUpperCase(),
                    PhoneNumber = reader.GetStringNullable("PhoneNumber").ToPhone(),
                    PayorId = reader.GetStringNullable("PayorId"),
                    IsTradtionalMedicare = false
                })
                .AsList();
        }


        #endregion
    }
}
