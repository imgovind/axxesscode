﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;

    public class MedicationProfileRepository : IMedicationProfileRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public MedicationProfileRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion


        public bool AddNewMedicationProfile(MedicationProfile medication)
        {
            var result = false;
            try
            {
                database.Add<MedicationProfile>(medication);
                result = true;
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public bool SaveMedicationProfile(MedicationProfile medicationProfile)
        {
            try
            {
                var medication = this.GetMedicationProfile(medicationProfile.Id, medicationProfile.AgencyId);
                if (medication != null)
                {
                    medication.PharmacyName = medicationProfile.PharmacyName;
                    medication.PharmacyPhone = medicationProfile.PharmacyPhone;
                    medication.Modified = DateTime.Now;
                    database.Update<MedicationProfile>(medication);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool UpdateMedication(MedicationProfile medicationProfile)
        {
            var result = false;
            try
            {
                if (medicationProfile != null)
                {
                    database.Update<MedicationProfile>(medicationProfile);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateMedication(Guid Id, Guid agencyId, Medication medication, string medicationType)
        {
            try
            {
                var medicationProfile = this.GetMedicationProfile(Id, agencyId);
                string medicationString = string.Empty;
                if (medicationProfile != null)
                {
                    medicationString = medicationProfile.Medication;
                }
                if (medicationString.IsNotNullOrEmpty() && medicationString != "")
                {
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    if (existingList != null && existingList.Count > 0 && existingList.Exists(m => m.Id == medication.Id))
                    {
                        var med = existingList.Single(m => m.Id == medication.Id);
                        if (med.StartDate.Date != medication.StartDate.Date || med.Route != medication.Route || med.Frequency != medication.Frequency || med.MedicationDosage != medication.MedicationDosage)
                        {
                            medication.Id = Guid.NewGuid();
                            medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                            medication.MedicationType = new MedicationType { Value = "C", Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                            existingList.Add(medication);
                            med.MedicationCategory = "DC";
                            med.DCDate = DateTime.Now;
                            medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                            database.Update<MedicationProfile>(medicationProfile);
                            return true;
                        }
                        else
                        {
                            med.IsLongStanding = medication.IsLongStanding;
                            med.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() }; ;
                            med.Classification = medication.Classification;
                            if (med.MedicationCategory == "DC")
                            {
                                med.DCDate = medication.DCDate;
                            }
                            medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                            database.Update<MedicationProfile>(medicationProfile);
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool UpdateMedicationForDischarge(Guid medId, Guid agencyId, Guid Id, DateTime dischargeDate)
        {
            bool result = false;
            try
            {
                var medicationProfile = this.GetMedicationProfile(medId, agencyId);
                string medicationString = "";
                if (medicationProfile != null)
                {
                    medicationString = medicationProfile.Medication;
                }
                if (medicationString.IsNotNullOrEmpty() && medicationString != "")
                {
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    if (existingList.Exists(m => m.Id == Id))
                    {
                        var med = existingList.Single(m => m.Id == Id);
                        med.DCDate = dischargeDate;
                        med.LastChangedDate = DateTime.Now;
                        med.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                        medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                        database.Update<MedicationProfile>(medicationProfile);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public bool DeleteMedicationProfile(Guid Id, Guid agencyId)
        {
            try
            {
                var medication = this.GetMedicationProfile(Id, agencyId);
                if (medication != null)
                {
                    database.Delete<MedicationProfile>(medication.Id);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public MedicationProfile InsertMedication(Guid Id, Guid agencyId, Medication medication, string medicationType)
        {
            var medicationProfile = this.GetMedicationProfile(Id, agencyId);
            if (medicationProfile != null)
            {
                medication.Id = Guid.NewGuid();
                medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                if (medicationProfile.Medication.IsNullOrEmpty())
                {
                    var newList = new List<Medication>() { medication };
                    medicationProfile.Medication = newList.ToXml();
                    database.Update<MedicationProfile>(medicationProfile);
                }
                else
                {
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    existingList.Add(medication);
                    medicationProfile.Medication = existingList.ToXml();
                    database.Update<MedicationProfile>(medicationProfile);
                }
            }
            return medicationProfile;
        }

        public MedicationProfile DeleteMedication(Guid medId, Guid agencyId, Medication medication)
        {
            var medicationProfile = this.GetMedicationProfile(medId, agencyId);
            string medicationString = "";
            if (medicationProfile != null)
            {
                medicationString = medicationProfile.Medication;
            }
            if (medicationProfile != null)
            {
                medicationString = medicationProfile.Medication;
            }
            if (!medicationString.IsNotNullOrEmpty())
            {
                return medicationProfile;
            }
            else
            {
                var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medication.Id))
                {
                    existingList.RemoveAll(m => m.Id == medication.Id);
                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                    database.Update<MedicationProfile>(medicationProfile);
                }
            }
            return medicationProfile;
        }

        public MedicationProfile GetMedicationProfileByPatient(Guid patientId, Guid AgencyId)
        {
            var medProfiles = database.Find<MedicationProfile>(m => m.AgencyId == AgencyId && m.PatientId == patientId);
            if (medProfiles != null)
            {
                return medProfiles.FirstOrDefault();
            }
            return null;
        }

        public MedicationProfile GetMedicationProfile(Guid Id, Guid agencyId)
        {
            return database.Single<MedicationProfile>(m => m.AgencyId == agencyId && m.Id == Id); ;
        }
    }
}
