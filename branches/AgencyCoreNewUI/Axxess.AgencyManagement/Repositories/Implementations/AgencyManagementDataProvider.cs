﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Data.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class AgencyManagementDataProvider : IAgencyManagementDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;
        
        public AgencyManagementDataProvider()
        {
            this.database = new SimpleRepository("AgencyManagementConnectionString1", SimpleRepositoryOptions.None);
        }

        #endregion

        #region AgencyCoreDataProvider Members

       
        private IPhysicianRepository physicianRepository;
        public IPhysicianRepository PhysicianRepository
        {
            get
            {
                if (physicianRepository == null)
                {
                    physicianRepository = new PhysicianRepository(this.database);
                }
                return physicianRepository;
            }
        }

        private IUserRepository employeeRepository;
        public IUserRepository UserRepository
        {
            get
            {
                if (employeeRepository == null)
                {
                    employeeRepository = new UserRepository(this.database);
                }
                return employeeRepository;
            }
        }

        private IAgencyRepository agencyRepository;
        public IAgencyRepository AgencyRepository
        {
            get
            {
                if (agencyRepository == null)
                {
                    agencyRepository = new AgencyRepository(this.database);
                }
                return agencyRepository;
            }
        }



        private IMessageRepository messageRepository;
        public IMessageRepository MessageRepository
        {
            get
            {
                if (messageRepository == null)
                {
                    messageRepository = new MessageRepository(this.database);
                }
                return messageRepository;
            }
        }

       
        #endregion
    }

}

