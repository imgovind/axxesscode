﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;

    public class ReportRepository : IReportRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public ReportRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region Reports

        public bool AddReport(Report report)
        {
            bool result = false;
            if (report != null)
            {
                report.Id = Guid.NewGuid();
                report.Created = DateTime.Now;
                database.Add<Report>(report);
                result = true;
            }

            return result;
        }

        public bool UpdateReport(Report report)
        {
            var result = false;
            if (report != null)
            {
                database.Update<Report>(report);
                result = true;
            }
            return result;
        }

        public Report GetReport(Guid agencyId, Guid reportId)
        {
            return database.Single<Report>(r => r.Id == reportId && r.AgencyId == agencyId && r.IsDeprecated == false);
        }

        public IList<ReportLite> GetReports(Guid agencyId)
        {
            var reports = new List<ReportLite>();
            var reportSQL = @"SELECT
                            `Id`, 
                            `UserId`,
                            `AssetId`, 
                            `Type`, 
                            `Format`,
                            `Status`,
                            `IsDeprecated`, 
                            `Created`, 
                            `Completed` 
                                FROM 
                                    `agencymanagement`.`reports` 
                                        WHERE 
                                            `AgencyId` = @agencyid AND
                                            `IsDeprecated` = 0 
                                                 ORDER BY `Created` DESC LIMIT 0, 100;";

            using (var cmd = new FluentCommand<ReportLite>(reportSQL))
            {
                reports = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new ReportLite
                    {
                        Id = reader.GetGuid("Id"),
                        UserId = reader.GetGuid("UserId"),
                        AssetId = reader.GetGuid("AssetId"),
                        Name = reader.GetString("Type"),
                        Format = reader.GetString("Format"),
                        Status = reader.GetString("Status"),
                        Created = reader.GetDateTime("Created") != DateTime.MinValue ? reader.GetDateTime("Created").ToString("MM/dd/yyyy hh:mm:ss tt") : "",
                        Completed = reader.GetDateTime("Completed") != DateTime.MinValue ? reader.GetDateTime("Completed").ToString("MM/dd/yyyy hh:mm:ss tt") : "",
                        UserName = UserEngine.GetName(reader.GetGuid("UserId"), agencyId)
                    })
                    .AsList();
            }
            return reports;
        }

        #endregion

    }
}
