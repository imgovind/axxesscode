﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    public class HospitalizationLogRepository : IHospitalizationLogRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public HospitalizationLogRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region Hospitalization Log

        public HospitalizationLog GetHospitalizationLog(Guid agencyId, Guid patientId, Guid transferLogId)
        {
            return database.Single<HospitalizationLog>(t => t.AgencyId == agencyId && t.PatientId == patientId && t.Id == transferLogId);
        }

        public bool UpdateHospitalizationLog(HospitalizationLog transferLog)
        {
            var result = false;
            if (transferLog != null)
            {
                database.Update<HospitalizationLog>(transferLog);
                result = true;
            }
            return result;
        }

        public bool AddHospitalizationLog(HospitalizationLog transferLog)
        {
            var result = false;
            if (transferLog != null)
            {
                database.Add<HospitalizationLog>(transferLog);
                result = true;
            }
            return result;
        }

        public List<HospitalizationLog> GetHospitalizationLogs(Guid patientId, Guid agencyId)
        {
            return database.Find<HospitalizationLog>(m => m.AgencyId == agencyId && m.PatientId == patientId).ToList();
        }

        #endregion


    }
}
