﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;

    public class AuthorizationRepository : IAuthorizationRepository
    {

        #region Constructor

        private readonly SimpleRepository database;

        public AuthorizationRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        public bool AddAuthorization(Authorization authorization)
        {
            var result = false;
            try
            {
                if (authorization != null)
                {
                    authorization.Created = DateTime.Now;
                    authorization.Modified = DateTime.Now;
                    database.Add<Authorization>(authorization);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool EditAuthorization(Authorization authorization)
        {
            var result = false;
            try
            {
                var authorizationToEdit = database.Single<Authorization>(a => a.AgencyId == authorization.AgencyId && a.PatientId == authorization.PatientId && a.Id == authorization.Id);
                if (authorizationToEdit != null)
                {
                    authorizationToEdit.StartDate = authorization.StartDate;
                    authorizationToEdit.EndDate = authorization.EndDate;
                    authorizationToEdit.AgencyLocationId = authorization.AgencyLocationId;
                    authorizationToEdit.Status = authorization.Status;
                    authorizationToEdit.Insurance = authorization.Insurance;
                    authorizationToEdit.Number1 = authorization.Number1;
                    authorizationToEdit.Number2 = authorization.Number2;
                    authorizationToEdit.Number3 = authorization.Number3;
                    authorizationToEdit.SNVisit = authorization.SNVisit;
                    authorization.SNVisitCountType = authorization.SNVisitCountType;
                    authorizationToEdit.PTVisit = authorization.PTVisit;
                    authorizationToEdit.PTVisitCountType = authorization.PTVisitCountType;
                    authorizationToEdit.OTVisit = authorization.OTVisit;
                    authorizationToEdit.OTVisitCountType = authorization.OTVisitCountType;
                    authorizationToEdit.STVisit = authorization.STVisit;
                    authorizationToEdit.STVisitCountType = authorization.STVisitCountType;
                    authorizationToEdit.MSWVisit = authorization.MSWVisit;
                    authorizationToEdit.MSWVisitCountType = authorization.MSWVisitCountType;
                    authorizationToEdit.HHAVisit = authorization.HHAVisit;
                    authorizationToEdit.HHAVisitCountType = authorization.HHAVisitCountType;

                    authorizationToEdit.DieticianVisit = authorization.DieticianVisit;
                    authorizationToEdit.DieticianVisitCountType = authorization.DieticianVisitCountType;

                    authorizationToEdit.RNVisit = authorization.RNVisit;
                    authorizationToEdit.RNVisitCountType = authorization.RNVisitCountType;

                    authorizationToEdit.LVNVisit = authorization.LVNVisit;
                    authorizationToEdit.LVNVisitCountType = authorization.LVNVisitCountType;
                    authorizationToEdit.Comments = authorization.Comments;

                    authorizationToEdit.Modified = DateTime.Now;
                    database.Update<Authorization>(authorizationToEdit);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteAuthorization(Guid agencyId, Guid patientId, Guid Id)
        {
            var result = false;
            try
            {
                var authorization = database.Single<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Id == Id);
                if (authorization != null)
                {

                    authorization.IsDeprecated = true;
                    authorization.Modified = DateTime.Now;
                    database.Update<Authorization>(authorization);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public Authorization GetAuthorization(Guid agencyId, Guid Id)
        {
            return database.Single<Authorization>(a => a.AgencyId == agencyId && a.Id == Id);
        }

        public Authorization GetAuthorization(Guid agencyId, Guid patientId, Guid Id)
        {
            return database.Single<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.Id == Id);
        }

        public IList<Authorization> GetAuthorizations(Guid agencyId, Guid patientId)
        {
            var authorizations = database.Find<Authorization>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.IsDeprecated == false);
            if (authorizations != null && authorizations.Count > 0)
            {
                authorizations.ForEach(a =>
                {
                    //a.Branch = GetBranchName(agencyId, a.AgencyLocationId);
                });
            }
            return authorizations.OrderBy(a => a.StartDate).ToList();
        }

        public IList<Authorization> GetActiveAuthorizations(Guid agencyId, Guid patientId, string insuranceId, string status, DateTime startDate, DateTime endDate)
        {
            var authorizations = new List<Authorization>();
            var script = string.Format(@"SELECT 
                                        Id ,
                                        Number1 ,
                                        Number2 ,  
                                        Number3 ,
                                        StartDate ,
                                        EndDate 
                                            FROM 
                                                authorizations
                                                    WHERE  
                                                        AgencyId = @agencyId AND 
                                                        PatientId = @patientId  AND 
                                                        Insurance = @insuranceId  AND 
                                                        Status = @status  AND
                                                        ((DATE( @startdate) between DATE(StartDate) and Date (EndDate)) OR ( DATE(@enddate) between DATE(StartDate) and Date (EndDate)) OR (DATE(StartDate) between  DATE(@startdate) and Date (@enddate)) OR (DATE(EndDate) between  DATE(@startdate) and Date (@enddate)))
                                                            ORDER BY StartDate ASC");
            using (var cmd = new FluentCommand<Authorization>(script))
            {
                authorizations = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddString("insuranceId", insuranceId)
                    .AddString("status", status)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new Authorization
                    {
                        Id = reader.GetGuid("Id"),
                        Number1 = reader.GetStringNullable("Number1"),
                        Number2 = reader.GetStringNullable("Number2"),
                        Number3 = reader.GetStringNullable("Number3"),
                        StartDate = reader.GetDateTime("StartDate"),
                        EndDate = reader.GetDateTime("EndDate")
                    })
                    .AsList();
            }
            return authorizations;
        }

       
    }
}
