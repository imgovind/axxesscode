﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Domain;

    using AutoMapper;

    using SubSonic.Repository;
    using Axxess.Core.Infrastructure;
    using SubSonic.DataProviders;

    public class AssetRepository : IAssetRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private readonly string connectionString;

        public AssetRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            this.connectionString = "SupportMembershipConnectionString";
        }

        #endregion

        #region IAssetRepository Methods

        public Asset Get(Guid id, Guid agencyId)
        {
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider(this.connectionString)))
            {
                return database.Single<Asset>(a => a.Id == id && a.AgencyId == agencyId);
            }
        }

        public bool Delete(Guid id)
        {
            //var asset = database.Single<Asset>(a => a.Id == id);
            //if (asset != null)
            //{
            //    asset.IsDeprecated = true;
            //    asset.Modified = DateTime.Now;
            //    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString")))
            //    {
            //        database.Update<Asset>(asset);
            //    }
            //    return true;
            //}
            try
            {
                var sql = @"UPDATE
                              assets 
                                SET 
                                    IsDeprecated = @isdeprecated,
                                    Modified = @modified
                                         WHERE
                                              Id = @id";


                using (var cmd = new FluentCommand<Agency>(sql))
                {
                    return cmd.SetConnection(this.connectionString)
                         .AddInt("isdeprecated", 1)
                         .AddDateTime("modified", DateTime.Now)
                         .AddGuid("id", id)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            //return false;
        }

        public bool Add(Asset asset)
        {
            bool result = false;

            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;
                using (new SharedDbConnectionScope(ProviderFactory.GetProvider(this.connectionString)))
                {
                    database.Add<Asset>(asset);
                }
                result = true;
            }

            return result;
        }

        public bool RemoveAsset(Guid Id)
        {
            try
            {
                var sql = @"DELETE
                             from  assets 
                                         WHERE
                                              Id = @id";


                using (var cmd = new FluentCommand<Agency>(sql))
                {
                    return cmd.SetConnection(this.connectionString)
                         .AddGuid("id", Id)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            //try
            //{
            //    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString")))
            //    {
            //        database.Delete<Asset>(Id);
            //    }
            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //} 
        }
        #endregion
    }
}
