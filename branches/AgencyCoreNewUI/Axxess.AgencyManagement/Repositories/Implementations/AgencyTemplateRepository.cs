﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;

    public class AgencyTemplateRepository : IAgencyTemplateRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyTemplateRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region Template Methods

        public bool AddTemplate(AgencyTemplate template)
        {
            var result = false;
            if (template != null)
            {

                template.Created = DateTime.Now;
                template.Modified = DateTime.Now;

                database.Add<AgencyTemplate>(template);
                result = true;
            }
            return result;
        }

        public void InsertTemplates(Guid agencyId)
        {
            var templateSQL = MessageBuilder.ReadTextFrom("Templates");

            if (templateSQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(templateSQL.Replace("{0}", agencyId.ToString())))
                {
                    cmd.SetConnection("AgencyManagementConnectionString")
                    .AsNonQuery();
                }
            }
        }

        public bool UpdateTemplate(AgencyTemplate template)
        {
            var result = false;
            if (template != null)
            {
                template.Modified = DateTime.Now;
                database.Update<AgencyTemplate>(template);
                result = true;
            }
            return result;
        }

        public bool DeleteTemplate(Guid agencyId, Guid id)
        {
            var template = database.Single<AgencyTemplate>(c => c.AgencyId == agencyId && c.Id == id);
            if (template != null)
            {
                template.IsDeprecated = true;
                template.Modified = DateTime.Now;
                database.Update<AgencyTemplate>(template);
                return true;
            }
            return false;
        }

        public AgencyTemplate GetTemplate(Guid agencyId, Guid id)
        {
            return database.Single<AgencyTemplate>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public IList<AgencyTemplate> GetTemplates(Guid agencyId)
        {
            return database.Find<AgencyTemplate>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Title).ToList();
        }

        #endregion
    }
}
