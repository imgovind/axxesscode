﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;

    public class OrderRepository : IOrderRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public OrderRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        public bool AddOrder(PhysicianOrder order)
        {
            var result = false;
            try
            {
                if (order != null)
                {
                    order.Modified = DateTime.Now;
                    database.Add<PhysicianOrder>(order);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateOrder(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            Check.Argument.IsNotEmpty(order.Id, "order.Id");
            bool result = false;
            try
            {
                var editOrder = database.Single<PhysicianOrder>(o => o.Id == order.Id && o.PatientId == order.PatientId);
                if (order != null && editOrder != null)
                {
                    editOrder.Modified = DateTime.Now;
                    editOrder.OrderDate = order.OrderDate;
                    editOrder.Summary = order.Summary;
                    editOrder.Text = order.Text;
                    editOrder.PhysicianId = order.PhysicianId;
                    editOrder.Status = order.Status;
                    editOrder.IsOrderForNextEpisode = order.IsOrderForNextEpisode;
                    editOrder.IsOrderReadAndVerified = order.IsOrderReadAndVerified;
                    editOrder.SignatureDate = order.SignatureDate;
                    editOrder.SignatureText = order.SignatureText;
                    editOrder.IsOrderForNextEpisode = order.IsOrderForNextEpisode;
                    if (order.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature)
                    {
                        editOrder.PhysicianSignatureText = order.PhysicianSignatureText;
                        editOrder.ReceivedDate = order.ReceivedDate;
                    }
                    editOrder.PhysicianData = order.PhysicianData;
                    database.Update<PhysicianOrder>(editOrder);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateOrderModel(PhysicianOrder order)
        {
            Check.Argument.IsNotNull(order, "order");
            Check.Argument.IsNotEmpty(order.Id, "order.Id");
            bool result = false;
            try
            {
                if (order != null)
                {
                    order.Modified = DateTime.Now;
                    database.Update<PhysicianOrder>(order);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived, DateTime dateSend)
        {
            bool result = false;
            try
            {
                var order = database.Single<PhysicianOrder>(o => o.Id == orderId && o.AgencyId == agencyId);
                if (order != null)
                {
                    order.ReceivedDate = dateReceived;
                    order.SentDate = dateSend;
                    order.Status = status;
                    database.Update<PhysicianOrder>(order);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool MarkOrderAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            try
            {
                var physicianOrder = database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.Id == Id);
                if (physicianOrder != null)
                {
                    physicianOrder.IsDeprecated = isDeprecated;
                    database.Update<PhysicianOrder>(physicianOrder);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool ReassignOrdersUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var physicianOrder = database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.Id == Id);
            if (physicianOrder != null)
            {
                try
                {
                    physicianOrder.UserId = employeeId;
                    database.Update<PhysicianOrder>(physicianOrder);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
       
        public PhysicianOrder GetOrderOnly(Guid Id, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            return database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.Id == Id);
        }

        public PhysicianOrder GetOrderOnly(Guid Id, Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Single<PhysicianOrder>(o => o.AgencyId == agencyId && o.PatientId == patientId && o.Id == Id);
        }

        public List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT
                                    physicianorders.Id as Id,
                                    physicianorders.PhysicianId as PhysicianId,
                                    physicianorders.EpisodeId as EpisodeId,
                                    physicianorders.OrderNumber as OrderNumber, 
                                    patients.Id as PatientId , 
                                    patients.FirstName as FirstName,
                                    patients.LastName as LastName, 
                                    agencyphysicians.FirstName as PhysicianFirstName,
                                    agencyphysicians.LastName as PhysicianLastName,
                                    physicianorders.Status as Status,
                                    physicianorders.OrderDate as OrderDate,
                                    physicianorders.ReceivedDate as ReceivedDate,
                                    physicianorders.SentDate as SentDate,
                                    agencyphysicians.Credentials as PhysicianCredentials , 
                                    physicianorders.PhysicianSignatureDate as PhysicianSignatureDate 
                                        FROM 
                                            physicianorders 
                                                INNER JOIN patients ON physicianorders.PatientId = patients.Id 
                                                INNER JOIN agencyphysicians ON physicianorders.PhysicianId = agencyphysicians.Id  
                                                    WHERE
                                                        physicianorders.AgencyId = @agencyid AND
                                                        patients.IsDeprecated = 0 AND 
                                                        patients.Status IN (1,2) AND 
                                                        physicianorders.IsDeprecated = 0 AND
                                                        physicianorders.Id IN ( {0} )  AND 
                                                        physicianorders.OrderDate >= @startDate AND
                                                        physicianorders.OrderDate <= @endDate", orderIds); //AND  physicianorders.Status = @status

            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                    PhysicianName = string.Format("{0}, {1} {2}", reader.GetStringNullable("PhysicianLastName").ToTitleCase(), reader.GetStringNullable("PhysicianFirstName").ToTitleCase(), reader.GetStringNullable("PhysicianCredentials")),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT
                                    physicianorders.Id as Id,
                                    physicianorders.PhysicianId as PhysicianId,
                                    physicianorders.EpisodeId as EpisodeId, 
                                    physicianorders.OrderNumber as OrderNumber, 
                                    patients.Id as PatientId ,
                                    patients.FirstName as FirstName, 
                                    patients.LastName as LastName,
                                    agencyphysicians.FirstName as PhysicianFirstName,
                                    agencyphysicians.LastName as PhysicianLastName,
                                    physicianorders.Status as Status,
                                    physicianorders.OrderDate as OrderDate,
                                    physicianorders.ReceivedDate as ReceivedDate,
                                    physicianorders.SentDate as SentDate,
                                    agencyphysicians.Credentials as PhysicianCredentials 
                                        FROM
                                            physicianorders
                                                INNER JOIN patients ON physicianorders.PatientId = patients.Id 
                                                INNER JOIN agencyphysicians ON physicianorders.PhysicianId = agencyphysicians.Id 
                                                    WHERE 
                                                        physicianorders.AgencyId = @agencyid AND
                                                        patients.IsDeprecated = 0 AND 
                                                        patients.Status IN (1,2) AND 
                                                        physicianorders.IsDeprecated = 0 AND 
                                                        physicianorders.Id IN ( {0} )  AND
                                                        physicianorders.OrderDate >= @startDate AND 
                                                        physicianorders.OrderDate <= @endDate", orderIds);

            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PhysicianName = reader.GetString("PhysicianLastName").ToUpperCase() + ", " + reader.GetString("PhysicianFirstName").ToUpperCase()

                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPatientPhysicianOrders(Guid agencyId, Guid patientId, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                                    physicianorders.Id as Id,
                                    physicianorders.PhysicianId,
                                    physicianorders.EpisodeId as EpisodeId,
                                    physicianorders.OrderNumber as OrderNumber,
                                    patients.Id as PatientId ,
                                    patients.FirstName as FirstName,
                                    patients.LastName as LastName, 
                                    agencyphysicians.FirstName as PhysicianFirstName,
                                    agencyphysicians.LastName as PhysicianLastName,
                                    physicianorders.Status as Status , 
                                    physicianorders.OrderDate as OrderDate ,
                                    physicianorders.ReceivedDate as ReceivedDate, 
                                    physicianorders.SentDate as SentDate 
                                        FROM 
                                            physicianorders
                                                INNER JOIN patients ON physicianorders.PatientId = patients.Id 
                                                INNER JOIN agencyphysicians ON physicianorders.PhysicianId = agencyphysicians.Id 
                                                    WHERE 
                                                        physicianorders.AgencyId = @agencyid AND
                                                        physicianorders.PatientId = @patientId  AND
                                                        physicianorders.IsDeprecated = 0 AND
                                                        physicianorders.Id IN ( {0} )  AND
                                                        physicianorders.OrderDate >= @startDate AND
                                                        physicianorders.OrderDate <= @endDate", orderIds);

            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PhysicianName = reader.GetString("PhysicianLastName").ToUpperCase() + ", " + reader.GetString("PhysicianFirstName").ToUpperCase()

                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPendingPhysicianSignatureOrders(Guid agencyId, string orderIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                                physicianorders.Id as Id, 
                                physicianorders.PhysicianId,
                                physicianorders.EpisodeId as EpisodeId,
                                physicianorders.OrderNumber as OrderNumber,
                                patients.Id as PatientId ,
                                patients.FirstName as FirstName, 
                                patients.LastName as LastName,
                                agencyphysicians.FirstName as PhysicianFirstName,
                                agencyphysicians.LastName as PhysicianLastName,
                                physicianorders.Status as Status ,
                                physicianorders.OrderDate as OrderDate ,
                                physicianorders.ReceivedDate as ReceivedDate,
                                physicianorders.SentDate as SentDate , 
                                physicianorders.PhysicianSignatureDate as PhysicianSignatureDate 
                                    FROM
                                        physicianorders 
                                            INNER JOIN patients ON physicianorders.PatientId = patients.Id 
                                            INNER JOIN agencyphysicians ON physicianorders.PhysicianId = agencyphysicians.Id 
                                                WHERE 
                                                    physicianorders.AgencyId = @agencyid AND 
                                                    patients.IsDeprecated = 0 AND
                                                    patients.Status IN (1,2) AND 
                                                    physicianorders.IsDeprecated = 0 AND 
                                                    physicianorders.Id IN ( {0} ) AND
                                                    physicianorders.OrderDate >= @startDate AND
                                                    physicianorders.OrderDate <= @endDate", orderIds);// AND  (physicianorders.Status = 130 || physicianorders.Status = 145 )

            var list = new List<PhysicianOrder>();
            using (var cmd = new FluentCommand<PhysicianOrder>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PhysicianOrder
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PhysicianName = reader.GetString("PhysicianLastName").ToUpperCase() + ", " + reader.GetString("PhysicianFirstName").ToUpperCase(),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")

                })
                .AsList();
            }
            return list;
        }

        public List<PhysicianOrder> GetPhysicianOrdersByPhysician(List<Guid> physicianIdentifiers, int status)
        {
            var ids = new StringBuilder();
            var list = new List<PhysicianOrder>();

            physicianIdentifiers.ForEach(i =>
            {
                ids.AppendFormat("'{0}',", i.ToString());
            });

            var query = new QueryBuilder("select * from `physicianorders`")
                .Where(string.Format("`physicianorders`.Status = {0}", status))
                .And("`physicianorders`.IsDeprecated = 0")
                .And(string.Format("`physicianorders`.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

            using (var cmd = new FluentCommand<PhysicianOrder>(query.Build()))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString").AsList();
            }

            return list;
        }

        public List<PhysicianOrder> GetAllPhysicianOrders()
        {
            return database.All<PhysicianOrder>().ToList();
        }



        public bool AddFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter)
        {
            try
            {
                if (faceToFaceEncounter != null)
                {
                    database.Add<FaceToFaceEncounter>(faceToFaceEncounter);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool UpdateFaceToFaceEncounterForRequest(Guid agencyId, Guid orderId, int status, DateTime dateRequested)
        {
            Check.Argument.IsNotEmpty(orderId, "orderId");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            bool result = false;
            try
            {
                var order = database.Single<FaceToFaceEncounter>(o => o.Id == orderId && o.AgencyId == agencyId && o.IsDeprecated == false);
                if (order != null)
                {
                    order.Status = status;
                    order.RequestDate = dateRequested;
                    database.Update<FaceToFaceEncounter>(order);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter)
        {
            var result = false;
            try
            {
                if (faceToFaceEncounter != null)
                {
                    try
                    {
                        faceToFaceEncounter.Modified = DateTime.Now;
                        database.Update<FaceToFaceEncounter>(faceToFaceEncounter);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteFaceToFaceEncounter(Guid agencyId, Guid patientId, Guid Id, bool IsDeprecated)
        {
            var result = false;
            try
            {
                var faceToFaceEncounter = database.Single<FaceToFaceEncounter>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == Id);
                if (faceToFaceEncounter != null)
                {
                    faceToFaceEncounter.IsDeprecated = IsDeprecated;
                    faceToFaceEncounter.Modified = DateTime.Now;
                    database.Update<FaceToFaceEncounter>(faceToFaceEncounter);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool ReassignFaceToFaceEncounterUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            var faceToFaceEncounter = database.Single<FaceToFaceEncounter>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id && c.IsDeprecated == false);
            if (faceToFaceEncounter != null)
            {
                try
                {
                    faceToFaceEncounter.UserId = employeeId;
                    database.Update<FaceToFaceEncounter>(faceToFaceEncounter);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid patientId, Guid agencyId)
        {
            return database.Single<FaceToFaceEncounter>(f => f.AgencyId == agencyId && f.PatientId == patientId && f.Id == Id);
        }

        public FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid agencyId)
        {
            return database.Single<FaceToFaceEncounter>(f => f.AgencyId == agencyId && f.Id == Id);
        }
        
        public List<FaceToFaceEncounter> GetFaceToFaceEncounterOrders(Guid agencyId, int status, string orderIds)
        {
            var script = string.Format(@"SELECT 
                        facetofaceencounters.Id as Id, 
                        facetofaceencounters.EpisodeId as EpisodeId,
                        facetofaceencounters.OrderNumber as OrderNumber, 
                        facetofaceencounters.PhysicianId as PhysicianId ,
                        patients.Id as PatientId ,
                        patients.FirstName as FirstName, 
                        patients.LastName as LastName,
                        facetofaceencounters.Status as Status , 
                        facetofaceencounters.ReceivedDate as ReceivedDate,
                        facetofaceencounters.RequestDate as RequestDate , 
                        facetofaceencounters.SignatureDate as SignatureDate 
                            FROM 
                                facetofaceencounters 
                                    INNER JOIN patients ON facetofaceencounters.PatientId = patients.Id  
                                        WHERE 
                                            facetofaceencounters.AgencyId = @agencyid AND
                                            patients.IsDeprecated = 0 AND
                                            patients.Status IN (1,2) AND
                                            facetofaceencounters.IsDeprecated = 0 AND 
                                            facetofaceencounters.Id IN ( {0} ) AND 
                                            facetofaceencounters.Status = @status ", orderIds);

            var list = new List<FaceToFaceEncounter>();
            using (var cmd = new FluentCommand<FaceToFaceEncounter>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddInt("status", status)
                .SetMap(reader => new FaceToFaceEncounter
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    RequestDate = reader.GetDateTime("RequestDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    SignatureDate = reader.GetDateTime("SignatureDate"),
                })
                .AsList();
            }
            return list;
        }

        public List<FaceToFaceEncounter> GetPatientFaceToFaceEncounterOrders(Guid agencyId, Guid patientId, string orderIds)
        {
            var script = string.Format(@"SELECT 
                                    facetofaceencounters.Id as Id, 
                                    facetofaceencounters.EpisodeId as EpisodeId,
                                    facetofaceencounters.OrderNumber as OrderNumber,
                                    facetofaceencounters.PhysicianId as PhysicianId , 
                                    patients.Id as PatientId , 
                                    facetofaceencounters.Status as Status , 
                                    facetofaceencounters.ReceivedDate as ReceivedDate,
                                    facetofaceencounters.RequestDate as RequestDate 
                                        FROM
                                            facetofaceencounters
                                                INNER JOIN patients ON facetofaceencounters.PatientId = patients.Id  
                                                    WHERE 
                                                        facetofaceencounters.AgencyId = @agencyid AND
                                                        facetofaceencounters.PatientId = @patientId AND 
                                                        facetofaceencounters.IsDeprecated = 0 AND 
                                                        facetofaceencounters.Id IN ( {0} ) ", orderIds);

            var list = new List<FaceToFaceEncounter>();
            using (var cmd = new FluentCommand<FaceToFaceEncounter>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .SetMap(reader => new FaceToFaceEncounter
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    RequestDate = reader.GetDateTime("RequestDate"),
                    PhysicianId = reader.GetGuid("PhysicianId")
                })
                .AsList();
            }
            return list;
        }

        public List<FaceToFaceEncounter> GetPendingSignatureFaceToFaceEncounterOrders(Guid agencyId, string orderIds)
        {
            var script = string.Format(@"SELECT 
                                facetofaceencounters.Id as Id,
                                facetofaceencounters.EpisodeId as EpisodeId, 
                                facetofaceencounters.OrderNumber as OrderNumber,
                                facetofaceencounters.PhysicianId as PhysicianId , 
                                patients.Id as PatientId ,
                                patients.FirstName as FirstName, 
                                patients.LastName as LastName,
                                facetofaceencounters.Status as Status ,  
                                facetofaceencounters.ReceivedDate as ReceivedDate, 
                                facetofaceencounters.RequestDate as RequestDate 
                                    FROM 
                                        facetofaceencounters 
                                            INNER JOIN patients ON facetofaceencounters.PatientId = patients.Id  
                                                WHERE
                                                    facetofaceencounters.AgencyId = @agencyid AND 
                                                    patients.IsDeprecated = 0 AND 
                                                    patients.Status IN (1,2) AND 
                                                    facetofaceencounters.IsDeprecated = 0 AND
                                                    facetofaceencounters.Id IN ( {0} ) AND
                                                    ( facetofaceencounters.Status = 130 || facetofaceencounters.Status = 145 )", orderIds);

            var list = new List<FaceToFaceEncounter>();
            using (var cmd = new FluentCommand<FaceToFaceEncounter>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new FaceToFaceEncounter
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    RequestDate = reader.GetDateTime("RequestDate"),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PhysicianId = reader.GetGuid("PhysicianId")
                }).AsList();
            }
            return list;
        }

        public List<FaceToFaceEncounter> GetFaceToFaceEncountersByPhysician(List<Guid> physicianIdentifiers, int status)
        {
            var ids = new StringBuilder();
            var list = new List<FaceToFaceEncounter>();

            physicianIdentifiers.ForEach(i =>
            {
                ids.AppendFormat("'{0}',", i.ToString());
            });

            var query = new QueryBuilder("select * from `facetofaceencounters`")
                .Where(string.Format("(`facetofaceencounters`.Status = {0} || `facetofaceencounters`.Status = {1})"
                , (int)ScheduleStatus.OrderSentToPhysicianElectronically, status))
                .And("`facetofaceencounters`.IsDeprecated = 0")
                .And(string.Format("`facetofaceencounters`.PhysicianId in ({0})", ids.ToString().Remove(ids.ToString().Length - 1, 1)));

            using (var cmd = new FluentCommand<FaceToFaceEncounter>(query.Build()))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString").AsList();
            }

            return list;
        }

        public List<FaceToFaceEncounter> GetAllFaceToFaceEncounters()
        {
            return database.All<FaceToFaceEncounter>().ToList();
        }


    }
}
