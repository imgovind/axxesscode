﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;

    public class PatientAdmissionRepository : IPatientAdmissionRepository
    {
         #region Constructor

        private readonly SimpleRepository database;

        public PatientAdmissionRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        public bool AddPatientAdmissionDate(PatientAdmissionDate managedDate)
        {
            var result = false;
            try
            {
                if (managedDate != null)
                {
                    managedDate.Created = DateTime.Now;
                    managedDate.Modified = DateTime.Now;
                    database.Add<PatientAdmissionDate>(managedDate);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool PatientAdmissionEdit(Patient patient)
        {
            bool result = false;
            try
            {
                if (patient != null)
                {
                    var admissionToEdit = database.Single<PatientAdmissionDate>(p => p.AgencyId == patient.AgencyId && p.Id == patient.AdmissionId && p.PatientId == patient.Id);

                    if (admissionToEdit != null)
                    {
                        var patientToEdit = admissionToEdit.PatientData.ToObject<Patient>() ?? new Patient();
                        if (patientToEdit != null)
                        {
                            patientToEdit.FirstName = patient.FirstName;
                            patientToEdit.MiddleInitial = patient.MiddleInitial;
                            patientToEdit.LastName = patient.LastName;
                            patientToEdit.Gender = patient.Gender;
                            patientToEdit.DOB = patient.DOB;
                            patientToEdit.MaritalStatus = patient.MaritalStatus;
                            patientToEdit.Height = patient.Height;
                            patientToEdit.HeightMetric = patient.HeightMetric;
                            patientToEdit.Weight = patient.Weight;
                            patientToEdit.WeightMetric = patient.WeightMetric;
                            patientToEdit.AgencyLocationId = patient.AgencyLocationId;
                            patientToEdit.PatientIdNumber = patient.PatientIdNumber;
                            patientToEdit.MedicareNumber = patient.MedicareNumber;
                            patientToEdit.MedicaidNumber = patient.MedicaidNumber;
                            patientToEdit.SSN = patient.SSN;
                            patientToEdit.StartofCareDate = patient.StartofCareDate;
                            patientToEdit.DischargeDate = patient.DischargeDate;
                            patientToEdit.CaseManagerId = patient.CaseManagerId;
                            patientToEdit.UserId = patient.UserId;
                            patientToEdit.Ethnicities = patient.Ethnicities;
                            patientToEdit.PaymentSource = patient.PaymentSource;
                            patientToEdit.OtherPaymentSource = patient.OtherPaymentSource;
                            patientToEdit.AddressLine1 = patient.AddressLine1;
                            patientToEdit.AddressLine2 = patient.AddressLine2;
                            patientToEdit.AddressCity = patient.AddressCity;
                            patientToEdit.AddressStateCode = patient.AddressStateCode;
                            patientToEdit.AddressZipCode = patient.AddressZipCode;
                            patientToEdit.PhoneHome = patient.PhoneHome;
                            patientToEdit.PhoneMobile = patient.PhoneMobile;
                            patientToEdit.EmailAddress = patient.EmailAddress;
                            patientToEdit.PharmacyName = patient.PharmacyName;
                            patientToEdit.PharmacyPhone = patient.PharmacyPhone;
                            patientToEdit.PrimaryInsurance = patient.PrimaryInsurance;
                            if (patient.PrimaryInsurance >= 1000)
                            {
                                patientToEdit.PrimaryHealthPlanId = patient.PrimaryHealthPlanId;
                                patientToEdit.PrimaryGroupName = patient.PrimaryGroupName;
                                patientToEdit.PrimaryGroupId = patient.PrimaryGroupId;
                            }
                            patientToEdit.SecondaryInsurance = patient.SecondaryInsurance;
                            if (patient.SecondaryInsurance >= 1000)
                            {
                                patientToEdit.SecondaryHealthPlanId = patient.SecondaryHealthPlanId;
                                patientToEdit.SecondaryGroupName = patient.SecondaryGroupName;
                                patientToEdit.SecondaryGroupId = patient.SecondaryGroupId;
                            }
                            patientToEdit.TertiaryInsurance = patient.TertiaryInsurance;
                            if (patient.TertiaryInsurance >= 1000)
                            {
                                patientToEdit.TertiaryHealthPlanId = patient.TertiaryHealthPlanId;
                                patientToEdit.TertiaryGroupName = patient.TertiaryGroupName;
                                patientToEdit.TertiaryGroupId = patient.TertiaryGroupId;
                            }
                            patientToEdit.Triage = patient.Triage;
                            patientToEdit.EvacuationZone = patient.EvacuationZone;
                            patientToEdit.ServicesRequired = patient.ServicesRequired;
                            patientToEdit.DME = patient.DME;
                            patientToEdit.ReferrerPhysician = patient.ReferrerPhysician;
                            patientToEdit.AdmissionSource = patient.AdmissionSource;
                            patientToEdit.OtherReferralSource = patient.OtherReferralSource;
                            patientToEdit.ReferralDate = patient.ReferralDate;
                            patientToEdit.InternalReferral = patient.InternalReferral;

                            patientToEdit.Comments = patient.Comments;
                            patientToEdit.DischargeReason = patient.DischargeReason;
                            patientToEdit.AdmissionId = patient.AdmissionId;
                            patientToEdit.Modified = DateTime.Now;
                            admissionToEdit.PatientData = patientToEdit.ToXml();
                            admissionToEdit.StartOfCareDate = patient.StartofCareDate;
                            admissionToEdit.DischargedDate = patient.DischargeDate;
                            database.Update<PatientAdmissionDate>(admissionToEdit);
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public bool PatientAdmissionAdd(Patient patient)
        {
            bool result = false;
            try
            {
                if (patient != null)
                {
                    var admissionToAdd = new PatientAdmissionDate { Id = Guid.NewGuid(), PatientId = patient.Id, AgencyId = patient.AgencyId, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, Created = DateTime.Now, Modified = DateTime.Now, IsDeprecated = false, IsActive = true, Status = patient.Status };
                    var patientToEdit = database.Single<Patient>(p => p.AgencyId == patient.AgencyId && p.Id == patient.Id) ?? new Patient();
                    if (patientToEdit != null)
                    {
                        patientToEdit.FirstName = patient.FirstName;
                        patientToEdit.MiddleInitial = patient.MiddleInitial;
                        patientToEdit.LastName = patient.LastName;
                        patientToEdit.Gender = patient.Gender;
                        patientToEdit.DOB = patient.DOB;
                        patientToEdit.MaritalStatus = patient.MaritalStatus;
                        patientToEdit.Height = patient.Height;
                        patientToEdit.HeightMetric = patient.HeightMetric;
                        patientToEdit.Weight = patient.Weight;
                        patientToEdit.WeightMetric = patient.WeightMetric;
                        patientToEdit.AgencyLocationId = patient.AgencyLocationId;
                        patientToEdit.PatientIdNumber = patient.PatientIdNumber;
                        patientToEdit.MedicareNumber = patient.MedicareNumber;
                        patientToEdit.MedicaidNumber = patient.MedicaidNumber;
                        patientToEdit.SSN = patient.SSN;
                        patientToEdit.StartofCareDate = patient.StartofCareDate;
                        patientToEdit.DischargeDate = patient.DischargeDate;
                        patientToEdit.DischargeReason = patient.DischargeReason;
                        patientToEdit.CaseManagerId = patient.CaseManagerId;
                        patientToEdit.UserId = patient.UserId;
                        patientToEdit.Ethnicities = patient.Ethnicities;
                        patientToEdit.PaymentSource = patient.PaymentSource;
                        patientToEdit.OtherPaymentSource = patient.OtherPaymentSource;
                        patientToEdit.AddressLine1 = patient.AddressLine1;
                        patientToEdit.AddressLine2 = patient.AddressLine2;
                        patientToEdit.AddressCity = patient.AddressCity;
                        patientToEdit.AddressStateCode = patient.AddressStateCode;
                        patientToEdit.AddressZipCode = patient.AddressZipCode;
                        patientToEdit.PhoneHome = patient.PhoneHome;
                        patientToEdit.PhoneMobile = patient.PhoneMobile;
                        patientToEdit.EmailAddress = patient.EmailAddress;
                        patientToEdit.PharmacyName = patient.PharmacyName;
                        patientToEdit.PharmacyPhone = patient.PharmacyPhone;
                        patientToEdit.PrimaryInsurance = patient.PrimaryInsurance;
                        if (patient.PrimaryInsurance >= 1000)
                        {
                            patientToEdit.PrimaryHealthPlanId = patient.PrimaryHealthPlanId;
                            patientToEdit.PrimaryGroupName = patient.PrimaryGroupName;
                            patientToEdit.PrimaryGroupId = patient.PrimaryGroupId;
                        }
                        patientToEdit.SecondaryInsurance = patient.SecondaryInsurance;
                        if (patient.SecondaryInsurance >= 1000)
                        {
                            patientToEdit.SecondaryHealthPlanId = patient.SecondaryHealthPlanId;
                            patientToEdit.SecondaryGroupName = patient.SecondaryGroupName;
                            patientToEdit.SecondaryGroupId = patient.SecondaryGroupId;
                        }
                        patientToEdit.TertiaryInsurance = patient.TertiaryInsurance;
                        if (patient.TertiaryInsurance >= 1000)
                        {
                            patientToEdit.TertiaryHealthPlanId = patient.TertiaryHealthPlanId;
                            patientToEdit.TertiaryGroupName = patient.TertiaryGroupName;
                            patientToEdit.TertiaryGroupId = patient.TertiaryGroupId;
                        }
                        patientToEdit.Triage = patient.Triage;
                        patientToEdit.EvacuationZone = patient.EvacuationZone;
                        patientToEdit.ServicesRequired = patient.ServicesRequired;
                        patientToEdit.DME = patient.DME;
                        patientToEdit.ReferrerPhysician = patient.ReferrerPhysician;
                        patientToEdit.AdmissionSource = patient.AdmissionSource;
                        patientToEdit.OtherReferralSource = patient.OtherReferralSource;
                        patientToEdit.ReferralDate = patient.ReferralDate;
                        patientToEdit.InternalReferral = patient.InternalReferral;
                        patientToEdit.Comments = patient.Comments;
                        patientToEdit.AdmissionId = admissionToAdd.Id;
                        admissionToAdd.PatientData = patientToEdit.ToXml();
                        admissionToAdd.StartOfCareDate = patient.StartofCareDate;
                        admissionToAdd.DischargedDate = patient.DischargeDate;
                        database.Add<PatientAdmissionDate>(admissionToAdd);
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public bool UpdatePatientAdmissionDate(PatientAdmissionDate managedDate)
        {
            var result = false;
            var managedDateToEdit = database.Single<PatientAdmissionDate>(s => s.AgencyId == managedDate.AgencyId && s.PatientId == managedDate.PatientId && s.Id == managedDate.Id);
            try
            {
                if (managedDateToEdit != null && managedDate != null)
                {
                    managedDateToEdit.StartOfCareDate = managedDate.StartOfCareDate;
                    managedDateToEdit.DischargedDate = managedDate.DischargedDate;
                    managedDateToEdit.PatientData = managedDate.PatientData;
                    managedDateToEdit.Status = managedDate.Status;
                    managedDateToEdit.Reason = managedDate.Reason;
                    managedDateToEdit.Modified = DateTime.Now;
                    database.Update<PatientAdmissionDate>(managedDateToEdit);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdatePatientAdmissionDateModal(PatientAdmissionDate managedDate)
        {
            var result = false;
            if (managedDate != null)
            {
                try
                {
                    managedDate.Modified = DateTime.Now;
                    database.Update<PatientAdmissionDate>(managedDate);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool DeprecatedPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id)
        {
            var result = false;
            var managedDate = database.Single<PatientAdmissionDate>(s => s.AgencyId == agencyId && s.PatientId == patientId && s.Id == Id);
            try
            {
                if (managedDate != null)
                {
                    managedDate.IsDeprecated = true;
                    managedDate.Modified = DateTime.Now;
                    database.Update<PatientAdmissionDate>(managedDate);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeletePatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id)
        {
            var result = false;
            var managedDate = database.Single<PatientAdmissionDate>(s => s.AgencyId == agencyId && s.PatientId == patientId && s.Id == Id);
            try
            {
                if (managedDate != null)
                {
                    database.Delete<PatientAdmissionDate>(managedDate.Id);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeletePatientAdmissionDates(Guid agencyId, Guid patientId)
        {
            var result = false;
            var managedDates = database.Find<PatientAdmissionDate>(s => s.AgencyId == agencyId && s.PatientId == patientId);
            try
            {
                if (managedDates != null && managedDates.Count > 0)
                {
                    database.DeleteMany<PatientAdmissionDate>(managedDates);
                    result = true;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeprecatedPatientAdmissionDates(Guid agencyId, Guid patientId)
        {
            var result = false;
            var managedDates = database.Find<PatientAdmissionDate>(s => s.AgencyId == agencyId && s.PatientId == patientId);
            try
            {
                if (managedDates != null && managedDates.Count > 0)
                {
                    managedDates.ForEach(m => { m.IsDeprecated = true; m.Modified = DateTime.Now; });
                    database.UpdateMany<PatientAdmissionDate>(managedDates);
                    result = true;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid patientId, Guid Id)
        {
            return database.Single<PatientAdmissionDate>(m => m.AgencyId == agencyId && m.PatientId == patientId && m.Id == Id);
        }

        public PatientAdmissionDate GetPatientAdmissionDate(Guid agencyId, Guid admissionId)
        {
            return database.Single<PatientAdmissionDate>(a => a.AgencyId == agencyId && a.Id == admissionId);
        }

        public PatientAdmissionDate GetPatientLatestAdmissionDate(Guid agencyId, Guid patientId, DateTime date)
        {
            PatientAdmissionDate managedDate = null;
            var script = @"SELECT * FROM patientadmissiondates WHERE patientadmissiondates.AgencyId = @agencyId AND patientadmissiondates.PatientId = @patientId  AND patientadmissiondates.StartOfCareDate <= @date AND  patientadmissiondates.IsDeprecated = 0  ORDER BY patientadmissiondates.StartOfCareDate DESC LIMIT 1";
            using (var cmd = new FluentCommand<PatientAdmissionDate>(script))
            {
                managedDate = cmd.SetConnection("AgencyManagementConnectionString")
                    .AddGuid("agencyId", agencyId)
                    .AddGuid("patientId", patientId)
                    .AddDateTime("date", date)
                .SetMap(reader => new PatientAdmissionDate
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    DischargedDate = reader.GetDateTime("DischargedDate"),
                    StartOfCareDate = reader.GetDateTime("StartOfCareDate")
                })
                .AsSingle();
            }

            return managedDate;
        }

        public IList<PatientAdmissionDate> GetPatientAdmissionDates(Guid agencyId, Guid patientId)
        {
            return database.Find<PatientAdmissionDate>(d => d.AgencyId == agencyId && d.PatientId == patientId && d.IsDeprecated == false).OrderByDescending(d => d.StartOfCareDate.Date).ToList();
        }


    }
}
