﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;

   public class CommunicationNoteRepository : ICommunicationNoteRepository
    {
         #region Constructor

        private readonly SimpleRepository database;

        public CommunicationNoteRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        public bool AddCommunicationNote(CommunicationNote communicationNote)
        {
            try
            {
                if (communicationNote != null)
                {
                    database.Add<CommunicationNote>(communicationNote);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool EditCommunicationNote(CommunicationNote communicationNote)
        {
            Check.Argument.IsNotNull(communicationNote, "communicationNote");
            Check.Argument.IsNotEmpty(communicationNote.Id, "communicationNote.Id");

            bool result = false;
            try
            {
                var communicationNoteToEdit = database.Single<CommunicationNote>(c => c.AgencyId == communicationNote.AgencyId && c.Id == communicationNote.Id && c.PatientId == communicationNote.PatientId);
                if (communicationNoteToEdit != null)
                {
                    communicationNoteToEdit.Created = communicationNote.Created;
                    communicationNoteToEdit.Text = communicationNote.Text;
                    communicationNoteToEdit.EpisodeId = communicationNote.EpisodeId;
                    communicationNoteToEdit.PhysicianId = communicationNote.PhysicianId;
                    communicationNoteToEdit.SignatureText = communicationNote.SignatureText;
                    communicationNoteToEdit.SignatureDate = communicationNote.SignatureDate;
                    communicationNoteToEdit.Status = communicationNote.Status;
                    communicationNoteToEdit.PhysicianData = communicationNote.PhysicianData;
                    database.Update<CommunicationNote>(communicationNoteToEdit);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool UpdateCommunicationNoteModal(CommunicationNote communicationNote)
        {
            bool result = false;
            try
            {
                if (communicationNote != null)
                {
                    database.Update<CommunicationNote>(communicationNote);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool DeleteCommunicationNote(Guid agencyId, Guid Id, Guid patientId, bool isDeprecated)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            try
            {
                var commNote = database.Single<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id);
                if (commNote != null)
                {
                    commNote.IsDeprecated = isDeprecated;
                    database.Update<CommunicationNote>(commNote);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool ReassignCommunicationNoteUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            bool result = false;
            var commNote = database.Single<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id && c.IsDeprecated == false);
            if (commNote != null)
            {
                try
                {
                    commNote.UserId = employeeId;
                    database.Update<CommunicationNote>(commNote);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public CommunicationNote GetCommunicationNote(Guid Id, Guid patientId, Guid agencyId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            return database.Single<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.Id == Id && c.IsDeprecated == false);
        }
      
        public List<CommunicationNote> GetCommunicationNotes(Guid agencyId, Guid patientId)
        {
            return database.Find<CommunicationNote>(c => c.AgencyId == agencyId && c.PatientId == patientId && c.IsDeprecated == false).ToList();
        }

        public List<CommunicationNote> GetCommunicationNoteByIds(Guid agencyId, string orderIds)
        {
            var script = string.Format(@"SELECT
                                communicationnotes.Id as Id, 
                                communicationnotes.EpisodeId as EpisodeId,
                                communicationnotes.UserId as UserId,
                                communicationnotes.Status as Status, 
                                communicationnotes.Created as Created , 
                                patients.Id as PatientId ,
                                patients.FirstName as FirstName,
                                patients.LastName as LastName  
                                    FROM 
                                        communicationnotes 
                                            INNER JOIN patients ON communicationnotes.PatientId = patients.Id 
                                                WHERE
                                                    communicationnotes.AgencyId = @agencyid AND 
                                                    patients.IsDeprecated = 0 AND 
                                                    patients.Status IN (1,2) AND 
                                                    communicationnotes.IsDeprecated = 0 AND 
                                                    communicationnotes.Id IN ( {0} ) ", orderIds);

            var list = new List<CommunicationNote>();
            using (var cmd = new FluentCommand<CommunicationNote>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new CommunicationNote
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    UserDisplayName = UserEngine.GetName(reader.GetGuid("UserId"), agencyId),
                    DisplayName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    Created = reader.GetDateTime("Created")
                })
                .AsList();
            }
            return list;
        }

        public List<CommunicationNote> GetAllCommunicationNotes()
        {
            return database.All<CommunicationNote>().ToList();
        }
    }
}
