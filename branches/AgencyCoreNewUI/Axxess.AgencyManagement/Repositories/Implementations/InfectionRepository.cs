﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    public class InfectionRepository : IInfectionRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public InfectionRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region Infection Report

        public bool AddInfection(Infection infection)
        {
            var result = false;
            if (infection != null)
            {
                if (infection.InfectionTypeArray != null && infection.InfectionTypeArray.Count > 0)
                {
                    infection.InfectionType = infection.InfectionTypeArray.ToArray().AddColons();
                }
                infection.Created = DateTime.Now;
                infection.Modified = DateTime.Now;

                database.Add<Infection>(infection);
                result = true;
            }
            return result;
        }

        public bool UpdateInfection(Infection infection)
        {
            var result = false;
            var existingInfection = GetInfectionReport(infection.AgencyId, infection.Id);
            if (existingInfection != null)
            {
                existingInfection.Orders = infection.Orders;
                existingInfection.Treatment = infection.Treatment;
                existingInfection.MDNotified = infection.MDNotified;
                existingInfection.NewOrdersCreated = infection.NewOrdersCreated;
                existingInfection.TreatmentPrescribed = infection.TreatmentPrescribed;

                existingInfection.PhysicianId = infection.PhysicianId;
                existingInfection.InfectionDate = infection.InfectionDate;
                existingInfection.Status = infection.Status;
                if (infection.InfectionTypeArray != null && infection.InfectionTypeArray.Count > 0)
                {
                    existingInfection.InfectionType = infection.InfectionTypeArray.ToArray().AddColons();
                }
                existingInfection.InfectionTypeOther = infection.InfectionTypeOther;
                existingInfection.FollowUp = infection.FollowUp;
                existingInfection.SignatureDate = infection.SignatureDate;
                existingInfection.SignatureText = infection.SignatureText;
                existingInfection.Modified = DateTime.Now;

                database.Update<Infection>(existingInfection);
                result = true;
            }
            return result;
        }

        public bool UpdateInfectionModal(Infection infection)
        {
            var result = false;
            if (infection != null)
            {
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        public bool RemoveInfection(Guid Id)
        {
            var result = false;
            try
            {
                database.Delete<Infection>(Id);
                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool MarkInfectionsAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (infection != null)
            {
                infection.IsDeprecated = isDeprecated;
                infection.Modified = DateTime.Now;
                database.Update<Infection>(infection);
                result = true;
            }
            return result;
        }

        public bool ReassignInfectionsUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var infection = database.Single<Infection>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (infection != null)
            {
                try
                {
                    infection.UserId = employeeId;
                    infection.Modified = DateTime.Now;
                    database.Update<Infection>(infection);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public Infection GetInfectionReport(Guid agencyId, Guid infectionId)
        {
            return database.Single<Infection>(i => i.AgencyId == agencyId && i.Id == infectionId);
        }

        public IList<Infection> GetInfections(Guid agencyId)
        {
            return database.Find<Infection>(i => i.AgencyId == agencyId).ToList();
        }


        #endregion
    }
}
