﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
   

   public class PatientVisitNoteRepository : IPatientVisitNoteRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PatientVisitNoteRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        public bool AddVisitNote(PatientVisitNote patientVisitNote)
        {
            try
            {
                database.Add<PatientVisitNote>(patientVisitNote);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateVisitNote(PatientVisitNote patientVisitNote)
        {
            var result = false;
            try
            {
                if (patientVisitNote != null)
                {
                    patientVisitNote.Modified = DateTime.Now;
                    database.Update<PatientVisitNote>(patientVisitNote);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public bool MarkVisitNoteAsDeleted(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, bool isDeprecated)
        {
            bool result = false;
            try
            {
                var visitNote = database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == eventId && p.PatientId == patientId && p.EpisodeId == episodeId);
                if (visitNote != null)
                {
                    visitNote.IsDeprecated = isDeprecated;
                    visitNote.Modified = DateTime.Now;
                    database.Update<PatientVisitNote>(visitNote);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool ReassignNotesUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId)
        {
            bool result = false;
            try
            {
                var visitNote = database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == eventId && p.PatientId == patientId && p.EpisodeId == episodeId);
                if (visitNote != null)
                {
                    try
                    {
                        visitNote.UserId = employeeId;
                        visitNote.Modified = DateTime.Now;
                        database.Update<PatientVisitNote>(visitNote);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public PatientVisitNote GetVisitNote(Guid agencyId, Guid patientId, Guid noteId)
        {
            return database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == noteId && p.PatientId == patientId);
        }

        public PatientVisitNote GetVisitNote(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId)
        {
            return database.Single<PatientVisitNote>(p => p.AgencyId == agencyId && p.Id == eventId && p.PatientId == patientId && p.EpisodeId == episodeId);
        }
      
        public PatientVisitNote GetHHAPlanOfCareVisitNote(Guid episodeId, Guid patientId)
        {
            var noteType = DisciplineTasks.HHAideCarePlan.ToString();
            return database.Find<PatientVisitNote>(p => p.EpisodeId == episodeId && p.PatientId == patientId && p.NoteType == noteType).LastOrDefault();
        }

        public PatientVisitNote GetVisitNoteByType(Guid episodeId, Guid patientId, DisciplineTasks disciplineTask)
        {
            var noteType = disciplineTask.ToString();
            return database.Find<PatientVisitNote>(p => p.EpisodeId == episodeId && p.PatientId == patientId && p.NoteType == noteType).LastOrDefault();
        }

        public List<PatientVisitNote> GetVisitNotesByDisciplineTaskWithStatus(Guid agencyId, Guid patientId, int[] scheduleStatus, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;

            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        patientvisitnotes.Id as Id ,
                        patientvisitnotes.PatientId as PatientId ,
                        patientvisitnotes.EpisodeId as EpisodeId ,
                        patientvisitnotes.UserId as UserId ,
                        patientvisitnotes.PhysicianId as PhysicianId , 
                        patientvisitnotes.NoteType as NoteType ,
                        patientvisitnotes.Note as Note ,
                        patientvisitnotes.Status as Status ,
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate 
                            FROM 
                                patientvisitnotes 
                                    INNER JOIN scheduleevents  ON patientvisitnotes.Id = scheduleevents.EventId 
                                    INNER JOIN patientepisodes ON scheduleevents.EpisodeId = patientepisodes.Id 
                                        WHERE 
                                            patientepisodes.AgencyId = @agencyid AND 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.IsMissedVisit = 0  AND 
                                            scheduleevents.IsDeprecated = 0 AND
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0} 
                                             ORDER BY  DATE(scheduleevents.EventDate) DESC ", additionalScript);

            var list = new List<PatientVisitNote>();
            using (var cmd = new FluentCommand<PatientVisitNote>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    NoteType = reader.GetStringNullable("NoteType"),
                    Note = reader.GetStringNullable("Note"),
                    Status = reader.GetInt("Status"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                })
                .AsList();
            }
            return list;
        }

        public List<PatientVisitNote> GetEvalOrders(Guid agencyId, string evalIds, DateTime startDate, DateTime endDate)
        {
            var script = string.Format(@"SELECT 
                                    patientvisitnotes.Id as Id,
                                    patientvisitnotes.PhysicianId as PhysicianId,
                                    patientvisitnotes.EpisodeId as EpisodeId, 
                                    patientvisitnotes.OrderNumber as OrderNumber, 
                                    patients.Id as PatientId,
                                    patients.FirstName as FirstName,
                                    patients.LastName as LastName,
                                    agencyphysicians.FirstName as PhysicianFirstName,
                                    agencyphysicians.LastName as PhysicianLastName,
                                    patientvisitnotes.NoteType as NoteType, 
                                    patientvisitnotes.Status as Status,
                                    patientvisitnotes.Created as OrderDate,
                                    patientvisitnotes.ReceivedDate as ReceivedDate,
                                    patientvisitnotes.SentDate as SentDate, 
                                    agencyphysicians.Credentials as PhysicianCredentials ,
                                    patientvisitnotes.PhysicianSignatureDate as PhysicianSignatureDate 
                                        FROM
                                            patientvisitnotes
                                                INNER JOIN patients ON patientvisitnotes.PatientId = patients.Id 
                                                INNER JOIN agencyphysicians ON patientvisitnotes.PhysicianId = agencyphysicians.Id
                                                    WHERE
                                                        patientvisitnotes.AgencyId = @agencyid AND 
                                                        patients.IsDeprecated = 0 AND 
                                                        patients.Status IN (1,2) AND
                                                        patientvisitnotes.IsDeprecated = 0 AND
                                                        patientvisitnotes.Id IN ({0}) AND 
                                                        patientvisitnotes.Created >= @startDate AND 
                                                        patientvisitnotes.Created <= @endDate", evalIds);

            var list = new List<PatientVisitNote>();
            using (var cmd = new FluentCommand<PatientVisitNote>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startDate", startDate)
                .AddDateTime("endDate", endDate)
                .SetMap(reader => new PatientVisitNote
                {
                    Id = reader.GetGuid("Id"),
                    OrderNumber = (long)reader.GetDecimalNullable("OrderNumber"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    Status = reader.GetInt("Status"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    ReceivedDate = reader.GetDateTime("ReceivedDate"),
                    NoteType = reader.GetStringNullable("NoteType"),
                    SentDate = reader.GetDateTime("SentDate"),
                    DisplayName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                    PhysicianName = string.Format("{0}, {1} {2}", reader.GetStringNullable("PhysicianLastName").ToTitleCase(), reader.GetStringNullable("PhysicianFirstName").ToTitleCase(), reader.GetStringNullable("PhysicianCredentials")),
                    PhysicianId = reader.GetGuid("PhysicianId"),
                    PhysicianSignatureDate = reader.GetDateTime("PhysicianSignatureDate")
                })
                .AsList();
            }
            return list;
        }

        public List<PatientVisitNote> GetEvalOrders(Guid agencyId, List<int> status, string evalIds, DateTime startDate, DateTime endDate)
        {
            var list = GetEvalOrders(agencyId, evalIds, startDate, endDate);
            return list.Where(p => status.Contains(p.Status)).ToList();
        }
    }
}
