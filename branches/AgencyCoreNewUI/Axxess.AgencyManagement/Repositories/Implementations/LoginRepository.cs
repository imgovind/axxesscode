﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using System.Text;

    public class LoginRepository : ILoginRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private string connectionString;

        public LoginRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            this.connectionString = "AxxessMembershipConnectionString";
        }

        #endregion

        #region Login Members

        public bool Add(Login login)
        {
            if (login != null)
            {
                login.Id = Guid.NewGuid();
                login.Created = DateTime.Now;
                database.Add<Login>(login);
                return true;
            }
            return false;
        }

        public Login Find(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "id");

            return database.Single<Login>(l => l.Id == id);
        }

        public Login Find(string emailAddress)
        {
            Check.Argument.IsNotInvalidEmail(emailAddress, "emailAddress");

            return database.Single<Login>(l => l.EmailAddress == emailAddress.Trim());
        }

        public bool Update(Login login)
        {
            try
            {

                if (login != null)
                {
                    return database.Update<Login>(login) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        #endregion

        #region ISupportRepository Members

        public bool AddImpersonationLink(ImpersonationLink link)
        {
            try
            {
                if (link != null)
                {
                    link.Id = Guid.NewGuid();
                    link.Created = DateTime.Now;
                    database.Add<ImpersonationLink>(link);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        #endregion

        #region agency snapshot

        public bool AddAgencySnapshot(AgencySnapshot agencySnapshot)
        {
            try
            {
                if (agencySnapshot != null)
                {
                    database.Add<AgencySnapshot>(agencySnapshot);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        public bool UpdateAgencySnapshot(AgencySnapshot agencySnapshot)
        {
            try
            {

                if (agencySnapshot != null)
                {
                    return database.Update<AgencySnapshot>(agencySnapshot) > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public bool ToggleReadOnly(Guid id, DateTime readOnlyDate, bool isFrozen)
        {
            var sql = @"UPDATE
                          agencysnapshots as ag
                            SET 
                                ag.IsFrozen = @isFrozen,
                                ag.FrozenDate = @readOnlyDate
                                     WHERE
                                          ag.Id = @id";


            using (var cmd = new FluentCommand<int>(sql))
            {
                return cmd.SetConnection(this.connectionString)
                     .AddBoolean("isFrozen", isFrozen)
                     .AddDateTime("readOnlyDate", readOnlyDate)
                     .AddGuid("id", id)
                     .AsNonQuery() > 0;
            }
        }

        public bool ToggleDelete(Guid id)
        {
            try
            {
                var sql = @"UPDATE
                          agencysnapshots as ag
                            SET 
                                ag.IsDeprecated = !ag.IsDeprecated,
                                ag.IsSuspended = !ag.IsSuspended
                                     WHERE
                                          ag.Id = @id";


                using (var cmd = new FluentCommand<int>(sql))
                {
                    return cmd.SetConnection(this.connectionString)
                         .AddGuid("id", id)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool RemoveAgencySnapshot(Guid Id)
        {
            try
            {
                var sql = @"DELETE
                             from  agencysnapshots 
                                         WHERE
                                              Id = @id";

                using (var cmd = new FluentCommand<Agency>(sql))
                {
                    return cmd.SetConnection(this.connectionString)
                         .AddGuid("id", Id)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public AgencySnapshot GetAgencySnapshot(Guid agencyId)
        {

            return database.Single<AgencySnapshot>(l => l.Id == agencyId);
        }

        public AgencyAccountData GetReadOnlyAcccountId(Guid agencyId)
        {
            var account = new AgencyAccountData();

            var sql = @"SELECT 
                        ag.AccountId as Account,
                        ag.Id as AgencyId,
                        ag.IsFrozen as IsFrozen,
                        ag.ClusterId as ClusterId
                             FROM 
                                 agencysnapshots ag
                                    WHERE 
                                       ag.Id = @agencyId";


            using (var cmd = new FluentCommand<AgencyAccountData>(sql))
            {
                account = cmd.SetConnection(this.connectionString)
                .AddGuid("agencyId", agencyId)
                .SetMap(reader => new AgencyAccountData
                {
                    Account = reader.GetStringNullable("Account"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    IsFrozen = reader.GetBoolean("IsFrozen"),
                    ClusterId = reader.GetInt("ClusterId")
                })
                .AsSingle();
            }
            return account;
        }

        public List<AgencySnapshot> GetAgencySnapshotNames(List<Guid> agencyIds)
        {
            var list = new List<AgencySnapshot>();
            if (agencyIds != null && agencyIds.Count > 0)
            {

                var ids = agencyIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
                var script = string.Format(@"SELECT Id, Name from agencysnapshots where Id IN ({0});", ids);
                using (var cmd = new FluentCommand<AgencySnapshot>(script))
                {
                    list = cmd.SetConnection(connectionString)
                         .SetMap(reader => new AgencySnapshot
                         {
                             Id = reader.GetGuid("Id"),
                             Name = reader.GetString("Name")
                         }).AsList();


                }
            }
            return list;
        }

        public IList<AgencySnapshot> All()
        {
            var agencySnapshot = new List<AgencySnapshot>();
            var script = @"
                           SELECT 
                                Id,
                                Name,
                                IsSuspended, 
                                IsDeprecated,
                                IsFrozen,
                                FrozenDate,
                                ClusterId
                                    FROM agencysnapshots";
            using (var cmd = new FluentCommand<AgencySnapshot>(script))
            {
                agencySnapshot = cmd.SetConnection(this.connectionString)
                .SetMap(reader => new AgencySnapshot
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetStringNullable("Name"),
                    IsSuspended = reader.GetBoolean("IsSuspended"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsFrozen = reader.GetBoolean("IsFrozen"),
                    FrozenDate = reader.GetDateTime("FrozenDate"),
                    ClusterId = reader.GetInt("ClusterId"),

                })
                .AsList();
            }

            return agencySnapshot;
        }

        public int Cluster(Guid agencyId)
        {
            try
            {
                var sql = @"SELECT 
                                ClusterId
                                    FROM 
                                        agencysnapshots
                                            WHERE 
                                                Id = @agencyid limit 0,1 ;";


                using (var cmd = new FluentCommand<int>(sql))
                {
                    return cmd.SetConnection(this.connectionString)
                         .AddGuid("agencyId", agencyId)
                         .SetMap(
                           reader =>
                           reader.GetInt("ClusterId")
                           )
                       .AsSingle();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        #endregion

        #region user snapshot

        public bool AddUserSnapshot(UserSnapshot userSnapshot)
        {
            try
            {
                if (userSnapshot != null)
                {
                    database.Add<UserSnapshot>(userSnapshot);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        public bool IsAtLeastOneUserExist(Guid loginId)
        {
            try
            {
                var script = @"SELECT 
                              1
                                FROM usersnapshots as us 
                                    WHERE 
                                        us.LoginId = @loginid AND
                                         us.Status = @status AND
                                          us.IsDeprecated = 0 limit 0, 1;";

                using (var cmd = new FluentCommand<UserSnapshot>(script))
                {
                    return cmd.SetConnection(this.connectionString)
                        .AddGuid("loginid", loginId)
                        .AddInt("status", (int)UserStatus.Active)
                        .AsScalar() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsAtLeastOnePhysicianExist(Guid loginId)
        {
            try
            {
                var script = @"SELECT 
                              1
                                FROM physiciansnapshots as us 
                                    WHERE 
                                        us.LoginId = @loginid AND
                                          us.IsDeprecated = 0 limit 0, 1;";

                using (var cmd = new FluentCommand<UserSnapshot>(script))
                {
                    return cmd.SetConnection(this.connectionString)
                        .AddGuid("loginid", loginId)
                        .AddInt("status", (int)UserStatus.Active)
                        .AsScalar() > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public UserSnapshot GetUserSnapshot( Guid userId)
        {

            return database.Single<UserSnapshot>(l => l.Id == userId);
        }

        public UserSnapshot GetUserSnapshot(Guid agencyId, Guid userId)
        {

            return database.Single<UserSnapshot>(l => l.AgencyId == agencyId && l.Id==userId);
        }

        public List<UserSnapshot> GetUserSnapshotByLoginIdWithClusterid(Guid loginId)
        {
            var result = new List<UserSnapshot>();

            var script = @"
                            SELECT 
                            us.Id,
                            us.LoginId,
                            us.AgencyId,
                            us.TitleType,
                            us.`Status`,
                            us.Created,
                            us.IsDeprecated,
                            agn.ClusterId as ClusterId
                                FROM usersnapshots as us INNER JOIN agencysnapshots agn ON us.AgencyId = agn.Id
                                    WHERE 
                                        us.LoginId = @loginid;";

            using (var cmd = new FluentCommand<UserSnapshot>(script))
            {
                result = cmd.SetConnection("AxxessMembershipConnectionString")
                    .AddGuid("loginid", loginId)
                    .SetMap(reader => new UserSnapshot
                    {
                        Id = reader.GetGuid("Id"),
                        LoginId = reader.GetGuid("LoginId"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        TitleType = reader.GetStringNullable("TitleType"),
                        Status = reader.GetInt("Status"),
                        Created = reader.GetDateTime("Created"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        ClusterId = reader.GetInt("ClusterId")
                    }).AsList();
            }
            return result;
        }

        public List<UserSnapshot> GetUserSnapshotByLoginId(Guid loginId)
        {
            var result = new List<UserSnapshot>();

            var script = @"
                            SELECT 
                            us.Id,
                            us.LoginId,
                            us.AgencyId,
                            us.TitleType,
                            us.`Status`,
                            us.Created,
                            us.IsDeprecated
                                FROM usersnapshots as us 
                                    WHERE 
                                        us.LoginId = @loginid;";

            using (var cmd = new FluentCommand<UserSnapshot>(script))
            {
                result = cmd.SetConnection("AxxessMembershipConnectionString")
                    .AddGuid("loginid", loginId)
                    .SetMap(reader => new UserSnapshot
                    {
                        Id = reader.GetGuid("Id"),
                        LoginId = reader.GetGuid("LoginId"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        TitleType = reader.GetStringNullable("TitleType"),
                        Status = reader.GetInt("Status"),
                        Created = reader.GetDateTime("Created"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated")
                    }).AsList();
            }
            return result;
        }

        public IList<UserSelection> GetUsersSnapShotWithLoginInfo(Guid agencyId)
        {
            var users = new List<UserSelection>();

            var script = string.Format(@"SELECT 
                                urs.Id as Id,
                                urs.LoginId as LoginId,
                                lgn.EmailAddress as EmailAddress,
                                lgn.IsActive as IsActive,
                                urs.Created as Created
                                    FROM 
                                        usersnapshots as urs INNER JOIN logins lgn ON urs.LoginId = lgn.Id
                                            WHERE 
                                                urs.AgencyId = @agencyid AND
                                                urs.Status =@status AND  
                                                urs.IsDeprecated = 0 ");
            using (var cmd = new FluentCommand<UserSelection>(script))
            {
                users =
                    cmd.SetConnection(this.connectionString)
                       .AddGuid("agencyid", agencyId)
                       .AddInt("status", (int)UserStatus.Active)
                       .SetMap(
                           reader =>
                           new UserSelection
                           {
                               Id = reader.GetGuid("Id"),
                               LoginId = reader.GetGuid("LoginId"),
                               EmailAddress = reader.GetStringNullable("EmailAddress"),
                               LoginCreated = reader.GetDateTime("Created"),
                               IsLoginActive = reader.GetBoolean("IsActive")
                           })
                       .AsList();
            }
            return users;
        }

        #endregion

        #region Implementation Specialists

        public bool UpdateImplementationSpecialist(Guid agencyId, Guid implementationSpecialistId)
        {
            bool result = false;

            try
            {
                var sql = @"UPDATE
                              agencysnapshots as ag
                                SET 
                                    ag.ImplementationSpecialist = @implementationSpecialistId
                                         WHERE
                                              ag.Id = @agencyId";


                using (var cmd = new FluentCommand<int>(sql))
                {
                    return cmd.SetConnection(this.connectionString)
                         .AddDateTime("modified", DateTime.Now)
                         .AddGuid("implementationSpecialistId", implementationSpecialistId)
                         .AddGuid("agencyId", agencyId)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return result;
        }

        public AgencySpecialist GetImplementationSpecialist(Guid agencyId)
        {
            AgencySpecialist specialist = null;
            var sql = new StringBuilder(@"SELECT 
                 ags.Id as AgencyId, 
                 ags.Name as AgencyName,
                 ags.Created,
                 ags.ImplementationSpecialist
                    from
                        agencysnapshots  ags
                                where
                                    ags.Id = @agencyid limit 0, 1;")
            .ToString();

            using (var cmd = new FluentCommand<AgencySpecialist>(sql))
            {
                specialist = cmd.SetConnection(this.connectionString)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new AgencySpecialist
                    {
                        AgencyId = reader.GetGuid("AgencyId"),
                        AgencyName = reader.GetString("AgencyName"),
                        AgencyCreated = reader.GetDateTime("Created"),
                        SpecialistId = reader.GetGuidIncludeEmpty("ImplementationSpecialist")
                    })
                    .AsSingle();
            }

            return specialist;
        }

        public IList<AgencySpecialist> GetImplementationSpecialists(Guid implementationSpecialistId, DateTime startDate, DateTime endDate)
        {
            var list = new List<AgencySpecialist>();
            var implementationSpecialistSql = string.Empty;
            if (!implementationSpecialistId.IsEmpty())
            {
                implementationSpecialistSql = string.Format(" and ags.ImplementationSpecialist = '{0}' ", implementationSpecialistId);
            }

            var sql = new StringBuilder(@"SELECT 
                        ags.Id as AgencyId, 
                        ags.Name as AgencyName,
                        ags.ClusterId as ClusterId, 
                        ags.Created, 
                        ags.ImplementationSpecialist
                            from agencysnapshots ags")
                                            .AppendFormat(" where ags.IsDeprecated = 0 {0} ", implementationSpecialistSql)
                                            .Append(" and Date(ags.created) between Date(@startdate) and Date(@enddate) ")
                                           
                                            .ToString();

            using (var cmd = new FluentCommand<AgencySpecialist>(sql))
            {
                list = cmd.SetConnection(this.connectionString)
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new AgencySpecialist
                    {
                        AgencyId = reader.GetGuid("AgencyId"),
                        AgencyName = reader.GetString("AgencyName"),
                        AgencyCreated = reader.GetDateTime("Created"),
                        SpecialistId = reader.GetGuidIncludeEmpty("ImplementationSpecialist"),
                        ClusterId = reader.GetInt("ClusterId")
                    })
                    .AsList();
                if (list != null && list.Count > 0)
                {
                    list = list.OrderByDescending(l => l.AgencyCreated).ToList();
                }
            }

            return list;
        }

        #endregion
    }
}
