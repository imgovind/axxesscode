﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using Enums;
    using Domain;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class MessageRepository : IMessageRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public MessageRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region IMessageRepository Members

        public bool AddSystemMessage(SystemMessage message)
        {
            bool result = false;
            if (message != null)
            {
                message.Id = Guid.NewGuid();
                message.Created = DateTime.Now;
                database.Add<SystemMessage>(message);
                result = true;

                if (message.AgencyId != Guid.Empty)
                {
                    database.Add<AgencySystemMessage>(new AgencySystemMessage
                    {
                        SystemMessageId = message.Id,
                        AgencyId = message.AgencyId
                    });
                }
            }

            return result;
        }

        public bool AddSystemMessageFluent(SystemMessage message, int clusterId)
        {
            bool result = default(bool);

            var script = @"
                            INSERT INTO `systemmessages` 
                            (
	                            `Id`,
	                            `Subject`,
	                            `Body`,
	                            `Created`,
	                            `CreatedBy`
                            ) 
                            VALUES 
                            (
	                            @systemmessageid,
	                            @subject,
	                            @body,
	                            @created,
	                            @createdby
                            );";

            using (var cmd = new FluentCommand<int>(script))
            { 
                if(cmd.SetConnection("AgencyManagementConnectionString"+clusterId)
                    .AddGuid("systemmessageid", message.Id)
                    .AddString("subject", message.Subject)
                    .AddString("body", message.Body)
                    .AddDateTime("created", message.Created)
                    .AddString("createdby", message.CreatedBy)
                    .AsNonQuery() > 0)
                {
                    result = true;
                }
            }

            return result;
        }


        public bool AddUserMessage(UserMessage userMessage)
        {
            bool result = false;

            if (userMessage != null)
            {
                database.Add<UserMessage>(userMessage);
                result = true;
            }

            return result;
        }

        public bool AddUserMessages(List<UserMessage> userMessages)
        {
            bool result = false;

            if (userMessages != null && userMessages.Count > 0)
            {
                database.AddMany<UserMessage>(userMessages);
                result = true;
            }

            return result;
        }

        public bool AddUserMessageFluent(UserMessage userMessage, int clusterId)
        {
            bool result = default(bool);

            var script = @"
                            INSERT INTO `usermessages` 
                            (
	                            `Id`,
	                            `AgencyId`,
	                            `MessageId`,
	                            `UserId`,
	                            `MessageType`,
	                            `FolderId`,
	                            `IsRead`,
	                            `IsDeprecated`,
	                            `ThreadId`
                            ) 
                            VALUES 
                            (
	                            @usermessageid,
	                            @agencyid,
	                            @messageid,
	                            @userid,
	                            @messagetype,
	                            @folderid,
	                            @isread, 
	                            @isdeprecated, 
	                            @threadid
                            );";

            using (var cmd = new FluentCommand<int>(script))
            {
                if (cmd.SetConnection("AgencyManagementConnectionString"+clusterId)
                    .AddGuid("usermessageid", userMessage.Id)
                    .AddGuid("agencyid", userMessage.AgencyId)
                    .AddGuid("messageid", userMessage.MessageId)
                    .AddGuid("userid", userMessage.UserId)
                    .AddInt("messagetype", userMessage.MessageType)
                    .AddGuid("folderid", userMessage.FolderId)
                    .AddBoolean("isread", userMessage.IsRead)
                    .AddBoolean("isdeprecated", userMessage.IsDeprecated)
                    .AsNonQuery() > 0)
                {
                    result = true;
                }
            }

            return result;
        }

        public bool AddDashboardMessage(DashboardMessage message)
        {
            bool result = false;
            if (message != null)
            {
                message.Id = Guid.NewGuid();
                message.Created = DateTime.Now;
                database.Add<DashboardMessage>(message);
                result = true;
            }

            return result;
        }

        public bool AddDashboardMessageFluent(DashboardMessage message, int clusterId)
        {
            bool result = false;

            var script = @"
                            INSERT INTO `dashboardmessages` 
                            (
	                            `Id`,
	                            `Title`,
	                            `Text`,
	                            `Created`,
	                            `CreatedBy`
                            ) 
                            VALUES 
                            (
	                            @dashboardmessageid,
	                            @title,
	                            @text,
	                            @created,
	                            @createdby
                            );
                            ";

            using (var cmd = new FluentCommand<int>(script))
            {
                if (cmd.SetConnection("AgencyManagementConnectionString"+clusterId)
                    .AddGuid("dashboardmessageid", message.Id)
                    .AddString("title", message.Title)
                    .AddString("text", message.Text)
                    .AddDateTime("created", message.Created)
                    .AddString("createdby", message.CreatedBy)
                    .AsNonQuery() > 0)
                {
                    result = true;
                }
            }

            return result;
        }

        #endregion
    }
}
