﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Domain;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;
    using SubSonic.DataProviders;

    public class CustomerNotesRepository : ICustomerNotesRepository
    {

        #region Constructor
        private readonly SimpleRepository database;
        
        private readonly string selectNotes = string.Format(@"SELECT
                                                        Id as Id,
                                                        AgencyId as AgencyId,
                                                        LoginId as LoginId,
                                                        Comments as Comments,
                                                        Created as Created,
                                                        Modified as Modified,
                                                        IsDeprecated as IsDeprecated,
                                                        Recipients as Recipients,
                                                        NoteType as NoteType,
                                                        Summary as Summary,
                                                        CallerId as CallerId,
                                                        IssueId as IssueId,
                                                        AttachmentId as AttachmentId,
                                                        IsCompleted as IsCompleted,
                                                        DevUpdates as DevUpdates,
                                                        AdditionalInformation as AdditionalInformation,
                                                        ModifiedBy as ModifiedBy,
                                                        Status as Status
                                                            FROM
                                                                customernotes ");
        //private readonly string issueIdCheck = "IssueId!=0";
        private readonly string selectNonDeprecatedNotes;
        public CustomerNotesRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
            selectNonDeprecatedNotes = selectNotes + "WHERE IsDeprecated=false ";
        }
        #endregion


        #region ICustomerNotesRepository Members

        public IList<CustomerNote> GetRecentCustomerNotes(DateTime startDate, DateTime endDate)
        {
            var result = new List<CustomerNote>();

            var sql = new StringBuilder("SELECT agencymanagement.agencies.Name as AgencyName, customermanagement.logins.DisplayName as RepName,  customermanagement.customernotes.Id,customermanagement.customernotes.AgencyId, customermanagement.customernotes.LoginId, customermanagement.customernotes.Summary, ")
            .Append("customermanagement.customernotes.Created, customermanagement.customernotes.IsDeprecated, customermanagement.customernotes.NoteType, customermanagement.customernotes.Status, customermanagement.customernotes.ModifiedBy ")
            .Append("FROM customermanagement.customernotes INNER JOIN agencymanagement.agencies on agencymanagement.agencies.Id = customermanagement.customernotes.AgencyId ")
            .Append("INNER JOIN customermanagement.logins on customermanagement.customernotes.LoginId = customermanagement.logins.Id ")
            .Append("WHERE customermanagement.customernotes.IsDeprecated = 0 AND Date(customermanagement.customernotes.Created) BETWEEN DATE(@startDate) AND DATE(@endDate) ;")
            .ToString();
            using (var cmd = new FluentCommand<CustomerNote>(sql))
            {
                result = cmd.SetConnection("SupportMembershipConnectionString")
                    .AddDateTime("startDate", startDate)
                    .AddDateTime("endDate", endDate)
                    .SetMap(reader => new CustomerNote
                    {
                        Id = reader.GetGuid("Id"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        AgencyName = reader.GetString("AgencyName"),
                        RepName = reader.GetString("RepName"),
                        LoginId = reader.GetGuid("LoginId"),
                        Summary = reader.GetString("Summary"),
                        Created = reader.GetDateTime("Created"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        NoteType = reader.GetInt("NoteType"),
                        Status = reader.GetInt("Status"),
                        ModifiedBy = reader.GetGuid("ModifiedBy"),
                    }).AsList();
            }
            return result;
        }

        public IList<CustomerNote> GetCurrentUserNotes(Guid id, DateTime startDate, DateTime endDate)
        {
            var result = new List<CustomerNote>();
            var sql = new StringBuilder("SELECT agencymanagement.agencies.Name as AgencyName, customermanagement.logins.DisplayName as RepName,  customermanagement.customernotes.Id,customermanagement.customernotes.AgencyId, customermanagement.customernotes.LoginId, customermanagement.customernotes.Summary, ")
           .Append("customermanagement.customernotes.Created, customermanagement.customernotes.IsDeprecated, customermanagement.customernotes.NoteType, customermanagement.customernotes.Status, customermanagement.customernotes.ModifiedBy ")
           .Append("FROM customermanagement.customernotes INNER JOIN agencymanagement.agencies on agencymanagement.agencies.Id = customermanagement.customernotes.AgencyId ")
           .Append("INNER JOIN customermanagement.logins on customermanagement.customernotes.LoginId = customermanagement.logins.Id ")
           .Append("WHERE customermanagement.customernotes.IsDeprecated = 0 AND Date(customermanagement.customernotes.Created) BETWEEN DATE(@startDate) AND DATE(@endDate) AND customermanagement.customernotes.LoginId = @id;")
           .ToString();
            using (var cmd = new FluentCommand<CustomerNote>(sql))
            {
                result = cmd.SetConnection("SupportMembershipConnectionString")
                    .AddDateTime("startDate", startDate)
                    .AddDateTime("endDate", endDate)
                    .AddGuid("id", id)
                    .SetMap(reader => new CustomerNote
                    {
                        Id = reader.GetGuid("Id"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        AgencyName = reader.GetString("AgencyName"),
                        RepName = reader.GetString("RepName"),
                        LoginId = reader.GetGuid("LoginId"),
                        Summary = reader.GetString("Summary"),
                        Created = reader.GetDateTime("Created"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        NoteType = reader.GetInt("NoteType"),
                        Status = reader.GetInt("Status"),
                        ModifiedBy = reader.GetGuid("ModifiedBy"),
                    }).AsList();
            }
            return result;
        }

        public IList<CustomerNote> GetActiveCustomerNotes(int limit)
        {
            var result = new List<CustomerNote>();
            var script = string.Format(selectNonDeprecatedNotes + "ORDER BY Created DESC LIMIT 0, {0}", limit);
            result = GetQueryResultNotes(script).AsList();
            return result;
        }

        public IList<CustomerNote> GetActiveCustomerNotes()
        {
            return GetActiveCustomerNotes(50);
        }

        public IList<CustomerNote> GetUpdatedCustomerNotes(int limit)
        {
            var result = new List<CustomerNote>();
            var script = string.Format(selectNonDeprecatedNotes+"AND Created < Modified ORDER BY Modified DESC LIMIT 0, {0}", limit);
            result = GetQueryResultNotes(script).AsList();
            return result;
        }

        public IList<CustomerNote> GetUpdatedCustomerNotes()
        {
            return GetUpdatedCustomerNotes(75);
        }

        public IList<CustomerNote> GetCurrentUserNotes(Guid id, int limit)
        {
            var script = string.Format(selectNonDeprecatedNotes + "AND LoginId='{0}' ORDER BY Created DESC, Modified DESC Limit 0, {1}", id, limit);
            var result = GetQueryResultNotes(script).AsList();
            return result;
        }

        public IList<CustomerNote> GetCurrentUserNotes(Guid id)
        {
            return GetCurrentUserNotes(id, 75);
        }

        public DateTime GetLastAddedTime()
        {
            var script = string.Format(@"SELECT Created as Created FROM customernotes WHERE IsDeprecated=false ORDER BY Created DESC LIMIT {0}", 1);
            var result = new FluentCommand<DateTime>(script)
            .SetConnection("SupportMembershipConnectionString")
            .SetMap(reader => reader.GetDateTime("Created")).AsList().First();
            return result;
        }

        public DateTime GetLastUpdatedTime()
        {
            var script = string.Format(@"SELECT Modified as Modified FROM customernotes WHERE IsDeprecated=false ORDER BY Modified DESC LIMIT {0}", 1);
            var result = new FluentCommand<DateTime>(script)
            .SetConnection("SupportMembershipConnectionString")
            .SetMap(reader => reader.GetDateTime("Modified")).AsList().First();
            return result;
        }

        #endregion

        #region CustomerNotes

        public bool AddCustomerNote(CustomerNote note)
        {
            bool result = default(bool);

            note.Id = Guid.NewGuid();
            note.Created = DateTime.Now;
            note.Modified = DateTime.Now;
            note.IsDeprecated = false;

            var script = @"INSERT INTO 
	                        `customernotes` 
	                        (
		                        `Id`,
		                        `AgencyId`,
		                        `LoginId`,
		                        `Comments`,
		                        `Created`,
		                        `Modified`,
		                        `IsDeprecated`,
		                        `Recipients`,
		                        `NoteType`,
		                        `Summary`,
		                        `CallerId`,
		                        `IssueId`,
		                        `AttachmentId`,
		                        `IsCompleted`,
		                        `DevUpdates`,
		                        `AdditionalInformation`,
		                        `ModifiedBy`,
		                        `Status`,
		                        `Saved`
	                        ) 
	                        VALUES 
	                        (
		                        @id,
		                        @agencyid,
		                        @loginid,
		                        @comments,
		                        @created,
		                        @modified,
		                        @isdeprecated,
		                        @recipients,
		                        @notetype,
		                        @summary,
		                        @callerid,
		                        @issueid,
		                        @attachmentid,
		                        @iscompleted,
		                        @devupdates,
		                        @additionalinformation,
		                        @modifiedby,
		                        @status,
		                        @saved
	                        );";
            using (var cmd = new FluentCommand<CustomerNote>(script))
            {
                if (cmd.SetConnection("SupportMembershipConnectionString")
                    .AddGuid("id", note.Id)
                    .AddGuid("agencyid", note.AgencyId)
                    .AddGuid("loginid", note.LoginId)
                    .AddString("comments", note.Comments)
                    .AddDateTime("created", note.Created)
                    .AddDateTime("modified", note.Modified)
                    .AddBoolean("isdeprecated", note.IsDeprecated)
                    .AddString("recipients", note.Recipients)
                    .AddInt("notetype", note.NoteType)
                    .AddString("summary", note.Summary)
                    .AddGuid("callerid", note.CallerId)
                    .AddInt("issueid", note.IssueId)
                    .AddGuid("attachmentid", note.AttachmentId)
                    .AddBoolean("iscompleted", note.IsCompleted)
                    .AddString("devupdates", note.DevUpdates)
                    .AddString("additionalinformation", note.AdditionalInformation)
                    .AddGuid("modifiedby", note.ModifiedBy)
                    .AddInt("status", note.Status)
                    .AddBoolean("saved", note.Saved)
                    .AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public CustomerNote GetCustomerNote(Guid noteId)
        {
            CustomerNote result = new CustomerNote();
            var script = @"SELECT
	                        `Id`,
	                        `AgencyId`,
	                        `LoginId`,
	                        `Comments`,
	                        `Created`,
	                        `Modified`,
	                        `IsDeprecated`,
	                        `Recipients`,
	                        `NoteType`,
	                        `Summary`,
	                        `CallerId`,
	                        `IssueId`,
	                        `AttachmentId`,
	                        `IsCompleted`,
	                        `DevUpdates`,
	                        `AdditionalInformation`,
	                        `ModifiedBy`,
	                        `Status`,
	                        `Saved`
                        FROM `customernotes` 
                        WHERE Id = @noteid
                        AND IsDeprecated = 0";

            using (var cmd = new FluentCommand<CustomerNote>(script))
            {
                result = cmd.SetConnection("SupportMembershipConnectionString")
                    .AddGuid("noteid", noteId)
                    .SetMap(reader => new CustomerNote 
                    {
                        Id = reader.GetGuid("Id"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        LoginId = reader.GetGuid("LoginId"),
                        Comments = reader.GetStringNullable("Comments"),
                        Created = reader.GetDateTime("Created"),
                        Modified = reader.GetDateTime("Modified"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        Recipients = reader.GetStringNullable("Recipients"),
                        NoteType = reader.GetInt("NoteType"),
                        Summary = reader.GetStringNullable("Summary"),
                        CallerId = reader.GetGuid("CallerId"),
                        IssueId = reader.GetInt("IssueId"),
                        AttachmentId = reader.GetGuid("AttachmentId"),
                        IsCompleted = reader.GetBoolean("IsCompleted"),
                        DevUpdates = reader.GetStringNullable("DevUpdates"),
                        AdditionalInformation = reader.GetStringNullable("AdditionalInformation"),
                        ModifiedBy = reader.GetGuid("ModifiedBy"),
                        Status = reader.GetInt("Status"),
                        Saved = reader.GetBoolean("Saved")
                    }).AsSingle();
            }

            return result;
        }

        public CustomerNote GetCustomerNote(Guid agencyId, Guid noteId)
        {
            CustomerNote result = new CustomerNote();
            var script = @"SELECT
	                        `Id`,
	                        `AgencyId`,
	                        `LoginId`,
	                        `Comments`,
	                        `Created`,
	                        `Modified`,
	                        `IsDeprecated`,
	                        `Recipients`,
	                        `NoteType`,
	                        `Summary`,
	                        `CallerId`,
	                        `IssueId`,
	                        `AttachmentId`,
	                        `IsCompleted`,
	                        `DevUpdates`,
	                        `AdditionalInformation`,
	                        `ModifiedBy`,
	                        `Status`,
	                        `Saved`
                        FROM `customernotes` 
                        WHERE AgencyId = @agencyid
                        AND Id = @noteid
                        AND IsDeprecated = 0";
            try
            {
                using (var cmd = new FluentCommand<CustomerNote>(script))
                {
                    result = cmd.SetConnection("SupportMembershipConnectionString")
                        .AddGuid("noteid", noteId)
                        .AddGuid("agencyid", agencyId)
                        .SetMap(reader => new CustomerNote
                        {
                            Id = reader.GetGuid("Id"),
                            AgencyId = reader.GetGuid("AgencyId"),
                            LoginId = reader.GetGuid("LoginId"),
                            Comments = reader.GetStringNullable("Comments"),
                            Created = reader.GetDateTime("Created"),
                            Modified = reader.GetDateTime("Modified"),
                            IsDeprecated = reader.GetBoolean("IsDeprecated"),
                            Recipients = reader.GetStringNullable("Recipients"),
                            NoteType = reader.GetInt("NoteType"),
                            Summary = reader.GetStringNullable("Summary"),
                            CallerId = reader.GetGuid("CallerId"),
                            IssueId = reader.GetInt("IssueId"),
                            AttachmentId = reader.GetGuid("AttachmentId"),
                            IsCompleted = reader.GetBoolean("IsCompleted"),
                            DevUpdates = reader.GetStringNullable("DevUpdates"),
                            AdditionalInformation = reader.GetStringNullable("AdditionalInformation"),
                            ModifiedBy = reader.GetGuid("ModifiedBy"),
                            Status = reader.GetInt("Status"),
                            Saved = reader.GetBoolean("Saved")
                        }).AsSingle();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }

        public bool UpdateCustomerNote(CustomerNote note)
        {
            var result = false;
            if (note != null)
            {
                using (new SharedDbConnectionScope(ProviderFactory.GetProvider("SupportMembershipConnectionString")))
                {
                    database.Update<CustomerNote>(note);
                }
                result = true;
            }
            return result;
        }

        public bool DeleteCustomerNote(CustomerNote note)
        {
            bool result = default(bool);
            var script = @"
                            UPDATE `customernotes` 
                            SET 
	                            `IsDeprecated`= 1
                            WHERE  `Id`=@customernoteid;";
            using (var cmd = new FluentCommand<CustomerNote>(script))
            {
                if (cmd.SetConnection("SupportMembershipConnectionString")
                    .AddGuid("customernoteid", note.Id)
                    .AsNonQuery() > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public IList<CustomerNote> GetCustomerNotes(Guid agencyId)
        {
            var result = new List<CustomerNote>();
            var script = @"SELECT
	                        `Id`,
	                        `AgencyId`,
	                        `LoginId`,
	                        `Comments`,
	                        `Created`,
	                        `Modified`,
	                        `IsDeprecated`,
	                        `Recipients`,
	                        `NoteType`,
	                        `Summary`,
	                        `CallerId`,
	                        `IssueId`,
	                        `AttachmentId`,
	                        `IsCompleted`,
	                        `DevUpdates`,
	                        `AdditionalInformation`,
	                        `ModifiedBy`,
	                        `Status`,
	                        `Saved`
                        FROM `customernotes` 
                        WHERE AgencyId = @agencyid
                        AND IsDeprecated = 0";

            using (var cmd = new FluentCommand<CustomerNote>(script))
            {
                result = cmd.SetConnection("SupportMembershipConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new CustomerNote
                    {
                        Id = reader.GetGuid("Id"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        LoginId = reader.GetGuid("LoginId"),
                        Comments = reader.GetStringNullable("Comments"),
                        Created = reader.GetDateTime("Created"),
                        Modified = reader.GetDateTime("Modified"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        Recipients = reader.GetStringNullable("Recipients"),
                        NoteType = reader.GetInt("NoteType"),
                        Summary = reader.GetStringNullable("Summary"),
                        CallerId = reader.GetGuid("CallerId"),
                        IssueId = reader.GetInt("IssueId"),
                        AttachmentId = reader.GetGuid("AttachmentId"),
                        IsCompleted = reader.GetBoolean("IsCompleted"),
                        DevUpdates = reader.GetStringNullable("DevUpdates"),
                        AdditionalInformation = reader.GetStringNullable("AdditionalInformation"),
                        ModifiedBy = reader.GetGuid("ModifiedBy"),
                        Status = reader.GetInt("Status"),
                        Saved = reader.GetBoolean("Saved")
                    }).AsList();
            }

            return result;
        }

        public bool UpdateCustomerNoteIssueAndStatus(Guid agencyId, Guid noteId, int issueId, int statusId)
        {
            var count = 0;

            var sql = @"Update customernotes  set IssueId=@IssueId, Status=@statusId where  AgencyId= @agencyid AND Id=@id ";
            using (var cmd = new FluentCommand<CustomerNote>(sql))
            {
                count = cmd.SetConnection("SupportMembershipConnectionString")
                    .AddGuid("agencyid", agencyId)
                    .AddGuid("Id", noteId)
                    .AddInt("IssueId", issueId)
                    .AddInt("statusId", statusId).AsNonQuery();
            }
            return count > 0;
        }

        #endregion

        private FluentCommand<CustomerNote> GetQueryResultNotes(string script)
        {
            var result = new FluentCommand<CustomerNote>(script)
              .SetConnection("SupportMembershipConnectionString")
              .SetMap(reader => new CustomerNote
              {
                  Id = reader.GetGuid("Id"),
                  AgencyId = reader.GetGuid("AgencyId"),
                  LoginId = reader.GetGuid("LoginId"),
                  Comments = reader.GetString("Comments"),
                  Created = reader.GetDateTime("Created"),
                  Modified = reader.GetDateTime("Modified"),
                  IsDeprecated = reader.GetBoolean("IsDeprecated"),
                  Recipients = reader.GetStringNullable("Recipients"),
                  NoteType = reader.GetInt("NoteType"),
                  Summary = reader.GetString("Summary"),
                  CallerId = reader.GetGuid("CallerId"),
                  IssueId = (reader.GetIntNullable("IssueId") == null) ? 0 : reader.GetInt("IssueId"),
                  AttachmentId = reader.GetGuid("AttachmentId"),
                  IsCompleted = reader.GetBoolean("IsCompleted"),
                  DevUpdates = reader.GetStringNullable("DevUpdates"),
                  AdditionalInformation = reader.GetStringNullable("AdditionalInformation"),
                  ModifiedBy = reader.GetGuid("ModifiedBy"),
                  Status = reader.GetInt("Status")
              });
            return result;
        }
    }
}
