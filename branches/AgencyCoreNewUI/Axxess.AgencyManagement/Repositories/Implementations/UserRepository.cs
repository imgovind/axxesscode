﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Domain;
    using Extensions;

    using SubSonic.Repository;
    using SubSonic.DataProviders;

    public class UserRepository : IUserRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public UserRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region IUserRepository Members

        public User GetPrimaryUser(Guid agencyId, int clusterId)
        {
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                return database.Single<User>(u => u.IsPrimary == true && u.AgencyId == agencyId && u.IsDeprecated == false);
            }
        }

        public bool TogglePrimaryUser(Guid id, Guid agencyId, int clusterId)
        {
            var editUser = new User();
            bool result = false;
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                editUser = database.Single<User>(u => u.Id == id);
            }
            if (editUser != null)
            {
                editUser.Modified = DateTime.Now;
                editUser.IsPrimary = !editUser.IsPrimary;
                database.Update<User>(editUser);
                result = true;
            }
            return result;
        }

        public User GetUserOnly(Guid id, Guid agencyId, int clusterId)
        {
            User user = null;
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                user = database.Single<User>(u => u.Id == id && u.AgencyId == agencyId);
            }
            return user;
        }

        public User Get(Guid id, Guid agencyId, int clusterId)
        {
            var user = new User();
            Check.Argument.IsNotEmpty(id, "id");
            Check.Argument.IsNotEmpty(agencyId, "agencyId");
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                user = database.Single<User>(u => u.Id == id && u.AgencyId == agencyId);
            }
            if (user != null)
            {
                user.Profile = user.ProfileData.ToObject<UserProfile>();

                if (user.Permissions.IsNotNullOrEmpty())
                {
                    user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                }
                else
                {
                    user.PermissionsArray = new List<string>();
                }
                if (user.Licenses.IsNotNullOrEmpty())
                {
                    user.LicensesArray = user.Licenses.ToObject<List<License>>();
                }
                else
                {
                    user.LicensesArray = new List<License>();
                }
            }
            return user;
        }

        public IList<User> GetUsersByRoleOrTitle(Guid agencyId, int agencyRoleId, string titleType, string stateCode, int clusterId)
        {
            var users = new List<User>();
            var script = new StringBuilder(@"SELECT 
                agencymanagement.users.Id,
                agencymanagement.users.AgencyId, 
                agencymanagement.users.LoginId,
                agencymanagement.users.FirstName,
                axxessmembership.logins.EmailAddress 
                FROM 
                agencymanagement.users 
                INNER JOIN axxessmembership.logins on agencymanagement.users.LoginId = axxessmembership.logins.Id 
                INNER JOIN agencymanagement.agencies on agencymanagement.users.AgencyId = agencymanagement.agencies.Id ");
               
            if(stateCode.IsNotNullOrEmpty())
            {
                script.Append("INNER JOIN agencymanagement.userlocations on agencymanagement.users.Id = agencymanagement.userlocations.UserId ")
                    .Append("INNER JOIN agencymanagement.agencylocations on agencymanagement.userlocations.AgencyLocationId = agencymanagement.agencylocations.Id ");
            }
              script.Append("WHERE agencymanagement.users.IsDeprecated = 0 ")
                .Append("AND agencymanagement.users.Status = 1 ")
                .Append("AND agencymanagement.agencies.IsDeprecated = 0 ")
                .Append("AND agencymanagement.agencies.IsFrozen = 0 ")
                .Append("AND agencymanagement.agencies.IsSuspended = 0 ")
                .Append("AND axxessmembership.logins.IsActive = 1 ");

            if (!agencyId.IsEmpty())
            {
                script.AppendFormat("AND agencymanagement.users.AgencyId = '{0}' ", agencyId);
            }

            if (agencyRoleId > 0)
            {
                script.AppendFormat("AND (agencymanagement.users.Roles like '{0};%' OR agencymanagement.users.Roles like '%;{0};%') ", agencyRoleId);
            }

            if (titleType.IsNotNullOrEmpty())
            {
                script.AppendFormat("AND agencymanagement.users.TitleType = '{0}' ", titleType);
            }

            if (stateCode.IsNotNullOrEmpty())
            {
                script.AppendFormat("AND agencymanagement.agencylocations.AddressStateCode = '{0}' ", stateCode);
            }
            
            using (var cmd = new FluentCommand<User>(script.ToString()))
            {
                users = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .SetMap(reader => new User
                    {
                        Id = reader.GetGuid("Id"),
                        LoginId = reader.GetGuid("LoginId"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                        FirstName = reader.GetStringNullable("FirstName").ToTitleCase()
                    })
                    .AsList().Distinct().ToList();
            }
            return users;
        }

        public IList<User> GetAgencyUsers(Guid agencyId, int clusterId)
        {
            var users = new List<User>();
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                users = database.Find<User>(u => u.AgencyId == agencyId && u.IsDeprecated == false && u.Status == (int)UserStatus.Active).ToList();
            }
            users.ForEach(user =>
            {
                if (user.ProfileData.IsNotNullOrEmpty())
                {
                    user.Profile = user.ProfileData.ToObject<UserProfile>();
                    user.EmailAddress = user.Profile.EmailWork;
                }
                if (user.Permissions.IsNotNullOrEmpty())
                {
                    user.PermissionsArray = user.Permissions.ToObject<List<string>>();
                }
            });
            return users.OrderBy(u => u.FirstName).ToList();
        }

        public List<User> GetUsersWithLoginInfo(List<Guid> userIds, int clusterId)
        {
            var users = new List<User>();
            if (userIds != null && userIds.Count > 0)
            {
                var script = string.Format(@"SELECT 
                                urs.Id as Id ,
                                urs.LoginId as LoginId,
                                urs.FirstName as FirstName ,
                                urs.LastName as LastName ,
                                urs.Suffix as Suffix,
                                urs.Credentials as Credentials ,
                                urs.CredentialsOther as CredentialsOther ,
                                urs.IsDeprecated as IsDeprecated ,
                                urs.TitleTypeOther as TitleTypeOther,
                                urs.TitleType as TitleType
                                    FROM 
                                        agencymanagement.users urs
                                            WHERE 
                                                urs.Id IN ({0})", userIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<User>(script))
                {
                    users =
                        cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                           .SetMap(
                               reader =>
                               new User
                               {
                                   Id = reader.GetGuid("Id"),
                                   LoginId = reader.GetGuid("LoginId"),
                                   LastName = reader.GetStringNullable("LastName"),
                                   FirstName = reader.GetStringNullable("FirstName"),
                                   TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
                                   TitleType = reader.GetStringNullable("TitleType"),
                                   Suffix = reader.GetStringNullable("Suffix"),
                                   Credentials = reader.GetStringNullable("Credentials"),
                                   CredentialsOther = reader.GetStringNullable("CredentialsOther")

                               })
                           .AsList();
                }
            }
            return users;
        }

        public IList<User> GetUsersWithLoginInfo(Guid agencyId, List<Guid> userIds, int clusterId)
        {
            var users = new List<User>();
            var ids = userIds.Select(s => string.Format("'{0}'", s)).ToArray().Join(", ");
            var script = string.Format(@"SELECT 
                                urs.Id as Id ,
                                urs.LoginId as LoginId,
                                urs.FirstName as FirstName ,
                                urs.LastName as LastName ,
                                urs.Suffix as Suffix,
                                urs.Credentials as Credentials ,
                                urs.CredentialsOther as CredentialsOther ,
                                urs.IsDeprecated as IsDeprecated ,
                                urs.TitleTypeOther as TitleTypeOther,
                                urs.TitleType as TitleType
                                    FROM 
                                        agencymanagement.users urs
                                            WHERE 
                                                urs.AgencyId = @agencyid AND
                                                urs.Id IN ({0})", ids);
            using (var cmd = new FluentCommand<User>(script))
            {
                users =
                    cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                       .AddGuid("agencyid", agencyId)
                       .SetMap(
                           reader =>
                           new User
                           {
                               Id = reader.GetGuid("Id"),
                               LoginId = reader.GetGuid("LoginId"),
                               LastName = reader.GetStringNullable("LastName"),
                               FirstName = reader.GetStringNullable("FirstName"),
                               TitleTypeOther = reader.GetStringNullable("TitleTypeOther"),
                               TitleType = reader.GetStringNullable("TitleType"),
                               Suffix = reader.GetStringNullable("Suffix"),
                               Credentials = reader.GetStringNullable("Credentials"),
                               CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                               IsDeprecated = reader.GetBoolean("IsDeprecated")

                           })
                       .AsList();
            }

            return users;
        }

        public IList<User> GetUsersByLoginId(Guid loginId, int clusterId)
        {
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                return database.Find<User>(u => u.LoginId == loginId && u.IsDeprecated == false && u.Status == (int)UserStatus.Active);
            }
        }

        public User GetUserProfileData(Guid agencyId, Guid userId, int clusterId)
        {
            User user = null;
            var script = string.Format(@"SELECT 
                                urs.ProfileData as ProfileData
                                    FROM 
                                        agencymanagement.users urs
                                            WHERE 
                                                urs.AgencyId = @agencyid AND
                                                urs.Id = @id limit 0, 1");
            using (var cmd = new FluentCommand<User>(script))
            {
                user =
                    cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                       .AddGuid("agencyid", agencyId)
                       .AddGuid("id", userId)
                       .SetMap(
                           reader =>
                           new User
                           {
                               Id = userId,
                               AgencyId = agencyId,
                               ProfileData = reader.GetStringNullable("ProfileData")

                           })
                       .AsSingle();
            }

            return user;
        }

        public User GetUserForName(Guid agencyId, Guid userId, int clusterId)
        {
            User user = null;
            var script = string.Format(@"SELECT Id, 
                                    FirstName, 
                                    LastName,
                                    MiddleName, 
                                    Suffix, 
                                    Credentials, 
                                    IsDeprecated, CredentialsOther 
                                    FROM 
                                        users
                                            WHERE 
                                                users.AgencyId = @agencyid AND
                                                users.Id = @id limit 0, 1");
            using (var cmd = new FluentCommand<User>(script))
            {
                user = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                 .AddGuid("agencyid", agencyId)
                  .AddGuid("id", userId)
                 .SetMap(reader => new User
                 {
                     Id = reader.GetGuid("Id"),
                     FirstName = reader.GetStringNullable("FirstName"),
                     LastName = reader.GetStringNullable("LastName"),
                     MiddleName = reader.GetStringNullable("MiddleName"),
                     Suffix = reader.GetStringNullable("Suffix"),
                     Credentials = reader.GetStringNullable("Credentials"),
                     CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                     IsDeprecated = reader.GetBoolean("IsDeprecated")
                 }).AsSingle();
            }
            return user;
        }

        public bool UpdateUserProfileData(Guid agencyId, Guid userId, string profileData, int clusterId)
        {
            var sql = @"UPDATE
                          users as ag
                            SET 
                                ag.ProfileData = @profiledata
                                     WHERE
                                          ag.AgencyId= @agencyid AND
                                          ag.Id = @id";


            using (var cmd = new FluentCommand<int>(sql))
            {
                return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                     .AddString("profiledata", profileData)
                     .AddGuid("agencyid", agencyId)
                     .AddGuid("id", userId)
                     .AsNonQuery() > 0;
            }
        }

        public bool AddUserOnly(User user, int clusterId)
        {
            if (user != null)
            {
                using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                {
                    database.Add<User>(user);
                }

                return true;
            }
            return false;
        }

        public bool LinkUserLocation(UserLocation userLocation, int clusterId)
        {
            try
            {
                using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                {
                    database.Add<UserLocation>(userLocation);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool Refresh(User user, int clusterId)
        {
            bool result = false;
            if (user != null)
            {
                user.Modified = DateTime.Now;
                using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                {
                    database.Update<User>(user);
                }
                // UserEngine.Refresh(user.AgencyId);
                result = true;
            }
            return result;
        }


        #endregion
    }
}
