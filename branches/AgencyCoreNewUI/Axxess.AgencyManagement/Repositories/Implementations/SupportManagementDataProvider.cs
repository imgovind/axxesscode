﻿
namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;


  public  class SupportManagementDataProvider : ISupportManagementDataProvider
    {
        private readonly SimpleRepository database;
        
        public SupportManagementDataProvider()
        {
            this.database = new SimpleRepository("SupportMembershipConnectionString", SimpleRepositoryOptions.None);
        }

        private ICustomerNotesRepository customerNotesRepository;
        public ICustomerNotesRepository CustomerNotesRepository
        {
            get
            {
                if (customerNotesRepository == null)
                {
                    customerNotesRepository = new CustomerNotesRepository(this.database);
                }
                return customerNotesRepository;
            }
        }

        private IAssetRepository assetRepository;
        public IAssetRepository AssetRepository
        {
            get
            {
                if (assetRepository == null)
                {
                    assetRepository = new AssetRepository(this.database);
                }
                return assetRepository;
            }
        }
    }
}
