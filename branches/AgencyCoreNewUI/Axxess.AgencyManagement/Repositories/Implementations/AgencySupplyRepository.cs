﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;

    public class AgencySupplyRepository : IAgencySupplyRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencySupplyRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region Supply Methods

        public bool AddSupply(AgencySupply supply)
        {
            var result = false;
            if (supply != null)
            {
                supply.IsDeprecated = false;
                supply.Created = DateTime.Now;
                supply.Modified = DateTime.Now;

                database.Add<AgencySupply>(supply);
                result = true;
            }
            return result;
        }

        public void InsertSupplies(Guid agencyId)
        {
            var supplySQL = MessageBuilder.ReadTextFrom("Supplies");

            if (supplySQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(supplySQL.Replace("{0}", agencyId.ToString())))
                {
                    cmd.SetConnection("AgencyManagementConnectionString")
                    .AsNonQuery();
                }
            }
        }

        public bool UpdateSupply(AgencySupply supply)
        {
            var result = false;
            if (supply != null)
            {
                supply.Modified = DateTime.Now;
                database.Update<AgencySupply>(supply);
                result = true;
            }
            return result;
        }

        public bool DeleteSupply(Guid agencyId, Guid id)
        {
            var supply = database.Single<AgencySupply>(c => c.AgencyId == agencyId && c.Id == id);
            if (supply != null)
            {
                supply.IsDeprecated = true;
                supply.Modified = DateTime.Now;
                database.Update<AgencySupply>(supply);
                return true;
            }
            return false;
        }

        public AgencySupply GetSupply(Guid agencyId, Guid id)
        {
            return database.Single<AgencySupply>(c => c.Id == id && c.AgencyId == agencyId && c.IsDeprecated == false);
        }

        public IList<AgencySupply> GetSupplies(Guid agencyId)
        {
            return database.Find<AgencySupply>(c => c.AgencyId == agencyId && c.IsDeprecated == false).OrderBy(c => c.Description).ToList();
        }

        public IList<AgencySupply> GetSupplies(Guid agencyId, string searchTerm, int searchLimit)
        {
            return database.GetPaged<AgencySupply>(c => c.AgencyId == agencyId && c.IsDeprecated == false && c.Description.Contains(searchTerm), "Description", 0, searchLimit);
        }

        #endregion

    }
}
