﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    public class AgencyContactRepository : IAgencyContactRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyContactRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region Contact Methods

        public bool AddContact(AgencyContact contact)
        {
            var result = false;
            if (contact != null)
            {

                if (contact.PhonePrimaryArray.Count > 0)
                {
                    contact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    contact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                if (contact.FaxNumberArray.Count > 0)
                {
                    contact.FaxNumber = contact.FaxNumberArray.ToArray().PhoneEncode();
                }
                contact.Created = DateTime.Now;
                contact.Modified = DateTime.Now;

                database.Add<AgencyContact>(contact);
                result = true;
            }
            return result;
        }

        public bool EditContact(AgencyContact contact)
        {
            var result = false;
            var existingContact = database.Single<AgencyContact>(c => c.Id == contact.Id && c.AgencyId == contact.AgencyId && c.IsDeprecated == false);
            if (contact != null)
            {
                if (contact.PhonePrimaryArray.Count > 0)
                {
                    existingContact.PhonePrimary = contact.PhonePrimaryArray.ToArray().PhoneEncode();
                }
                if (contact.PhoneAlternateArray.Count > 0)
                {
                    existingContact.PhoneAlternate = contact.PhoneAlternateArray.ToArray().PhoneEncode();
                }
                if (contact.FaxNumberArray.Count > 0)
                {
                    existingContact.FaxNumber = contact.FaxNumberArray.ToArray().PhoneEncode();
                }
                existingContact.FirstName = contact.FirstName;
                existingContact.LastName = contact.LastName;
                existingContact.CompanyName = contact.CompanyName;
                existingContact.AddressLine1 = contact.AddressLine1;
                existingContact.AddressLine2 = contact.AddressLine2;
                existingContact.AddressCity = contact.AddressCity;
                existingContact.AddressStateCode = contact.AddressStateCode;
                existingContact.AddressZipCode = contact.AddressZipCode;
                existingContact.EmailAddress = contact.EmailAddress;
                existingContact.ContactType = contact.ContactType;
                existingContact.ContactTypeOther = contact.ContactTypeOther;
                existingContact.Modified = DateTime.Now;
                database.Update<AgencyContact>(existingContact);
                result = true;
            }
            return result;
        }

        public bool DeleteContact(Guid agencyId, Guid id)
        {
            var contact = database.Single<AgencyContact>(c => c.Id == id && c.AgencyId == agencyId);
            if (contact != null)
            {
                contact.IsDeprecated = true;
                contact.Modified = DateTime.Now;
                database.Update<AgencyContact>(contact);
                return true;
            }
            return false;
        }

        public AgencyContact FindContact(Guid agencyId, Guid Id)
        {
            return database.Single<AgencyContact>(c => c.AgencyId == agencyId && c.Id == Id);
        }

        public IList<AgencyContact> GetContacts(Guid agencyId)
        {
            return database.Find<AgencyContact>(c => c.AgencyId == agencyId && c.IsDeprecated == false).ToList();
        }

     
        #endregion
    }
}
