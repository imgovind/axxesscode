﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Enums;

    using SubSonic.Repository;
    using SubSonic.DataProviders;

    public class AccountingRepository : IAccountingRepository
    {
        #region Constructor

        private readonly SimpleRepository database;
        private readonly string connectionString;

        public AccountingRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
            this.connectionString = "AccountingConnectionString";
        }


        #endregion

        #region IAccountingRepository Members

        public List<Information> GetAllAccountingInformation(DateTime start, DateTime end)
        {
            var list = new List<Information>();

            var sql = @"SELECT 
                        inf.id,
                        inf.Account,
                        inf.Name,
                        inf.City, 
                        inf.State,
                        inf.Zip,
                        inf.ImplementationFee, 
                        inf.PaymentSource, 
                        inf.InitialSignDate, 
                        inf.SpecialNotes,
                        inf.Status, 
                        inf.SalesPerson,
                        inf.StartUp,
                        py.Id as PaymentId,
                        py.Start as Start,
                        py.End as End,
                        py.Sequence as Sequence,
                        py.Amount as Amount,
                        py.Package as Package
                             FROM 
                                information inf INNER JOIN payment py ON inf.Account = py.Account 
                                     WHERE
                                        DATE(py.End) >= DATE(@start) AND DATE(py.Start) <= DATE(@end)";


            using (var cmd = new FluentCommand<Information>(sql))
            {
                list = cmd.SetConnection(this.connectionString)
                    .AddDateTime("start", start)
                    .AddDateTime("end", end)
                    .SetMap(reader => new Information
                    {
                        Id = reader.GetInt("id"),
                        Account = reader.GetString("Account"),
                        Name = reader.GetString("Name"),
                        City = reader.GetString("City"),
                        State = reader.GetString("State"),
                        Zip = reader.GetInt("Zip"),
                        ImplementationFee = reader.GetStringNullable("ImplementationFee"),
                        PaymentSource = reader.GetStringNullable("PaymentSource"),
                        InitialSignDate = reader.GetDateTime("InitialSignDate"),
                        SpecialNotes = reader.GetStringNullable("SpecialNotes"),
                        Status = reader.GetInt("Status"),
                        SalesPerson = reader.GetStringNullable("SalesPerson"),
                        StartUp = reader.GetInt("StartUp"),
                        PaymentId = reader.GetInt("PaymentId"),
                        Start = reader.GetDateTime("Start"),
                        End = reader.GetDateTime("End"),
                        Sequence = reader.GetInt("Sequence"),
                        Package = reader.GetInt("Package"),
                        Amount = reader.GetDouble("Amount")

                    })
                    .AsList();
                if (list != null && list.Count > 0)
                {
                    list = list.OrderBy(l => l.Name).ToList();
                }
            }

            return list;
        }

        public List<Information> GetAllAccountingInformation(DateTime start, DateTime end, DateTime lastYearStart, DateTime lastYearEnd)
        {
            var list = new List<Information>();

            var sql = @"SELECT 
                        inf.id,
                        inf.Account,
                        inf.Name,
                        inf.City, 
                        inf.State,
                        inf.Zip,
                        inf.ImplementationFee, 
                        inf.PaymentSource, 
                        inf.InitialSignDate, 
                        inf.SpecialNotes,
                        inf.Status, 
                        inf.SalesPerson,
                        inf.StartUp,
                        py.Id as PaymentId,
                        py.Start as Start,
                        py.End as End,
                        py.Sequence as Sequence,
                        py.Amount as Amount,
                        py.Package as Package
                             FROM 
                                information inf INNER JOIN payment py ON inf.Account = py.Account 
                                     WHERE
                                         (DATE(py.End) >= DATE(@start) AND DATE(py.Start) <= DATE(@end))
                                          OR
                                         (DATE(py.End) >= DATE(@lastyearstart) AND DATE(py.Start) <= DATE(@lastyearend))";


            using (var cmd = new FluentCommand<Information>(sql))
            {
                list = cmd.SetConnection(this.connectionString)
                    .AddDateTime("start", start)
                    .AddDateTime("end", end)
                    .AddDateTime("lastyearstart", lastYearStart)
                    .AddDateTime("lastyearend", lastYearEnd)
                    .SetMap(reader => new Information
                    {
                        Id = reader.GetInt("id"),
                        Account = reader.GetString("Account"),
                        Name = reader.GetString("Name"),
                        City = reader.GetString("City"),
                        State = reader.GetString("State"),
                        Zip = reader.GetInt("Zip"),
                        ImplementationFee = reader.GetStringNullable("ImplementationFee"),
                        PaymentSource = reader.GetStringNullable("PaymentSource"),
                        InitialSignDate = reader.GetDateTime("InitialSignDate"),
                        SpecialNotes = reader.GetStringNullable("SpecialNotes"),
                        Status = reader.GetInt("Status"),
                        SalesPerson = reader.GetStringNullable("SalesPerson"),
                        StartUp = reader.GetInt("StartUp"),
                        PaymentId = reader.GetInt("PaymentId"),
                        Start = reader.GetDateTime("Start"),
                        End = reader.GetDateTime("End"),
                        Sequence = reader.GetInt("Sequence"),
                        Package = reader.GetInt("Package"),
                        Amount = reader.GetDouble("Amount")

                    })
                    .AsList();
                if (list != null && list.Count > 0)
                {
                    list = list.OrderBy(l => l.Name).ToList();
                }
            }

            return list;
        }

        public List<AccountInfo> GetAccountInfo(DateTime start, DateTime end)
        {
            var list = new List<AccountInfo>();

            var sql = @"SELECT 
                        inf.id,
                        inf.Account,
                        inf.Name,
                        inf.InitialSignDate
                             FROM 
                                information inf
                                     WHERE
                                        DATE(inf.InitialSignDate) >= DATE(@start) AND DATE(inf.InitialSignDate) <= DATE(@end)";


            using (var cmd = new FluentCommand<AccountInfo>(sql))
            {
                list = cmd.SetConnection(this.connectionString)
                    .AddDateTime("start", start)
                    .AddDateTime("end", end)
                    .SetMap(reader => new AccountInfo
                    {
                        Id = reader.GetInt("id"),
                        Account = reader.GetString("Account"),
                        Name = reader.GetString("Name"),
                        InitialSignDate = reader.GetDateTime("InitialSignDate"),


                    })
                    .AsList();
            }

            return list;
        }


        public List<Payment> GetAccountingPayments(string account)
        {
            var info = new List<Payment>();

            var sql = @"SELECT 
                        py.Account as Account,
                        py.Id as Id,
                        py.Start as Start,
                        py.End as End,
                        py.Amount as Amount,
                        py.Package as Package
                             FROM 
                                 payment py 
                                     WHERE
                                          py.Account = @account";


            using (var cmd = new FluentCommand<Payment>(sql))
            {
                info = cmd.SetConnection(this.connectionString)
                    .AddString("account", account)
                    .SetMap(reader => new Payment
                    {
                        Account = reader.GetString("Account"),
                        Id = reader.GetInt("Id"),
                        Start = reader.GetDateTime("Start"),
                        End = reader.GetDateTime("End"),
                        Package = reader.GetInt("Package"),
                        Amount = reader.GetDouble("Amount")

                    })
                    .AsList();
            }

            return info;
        }

        public List<Payment> GetAllFirstPayment()
        {
            var payment = new List<Payment>();

            var sql = @"SELECT 
                        py.Account as Account,
                        py.Id as Id,
                        py.Start as Start,
                        py.End as End,
                        py.Amount as Amount,
                        py.Sequence as Sequence,
                        py.Package as Package
                             FROM 
                                 payment py 
                                     WHERE
                                          py.Sequence = 1";


            using (var cmd = new FluentCommand<Payment>(sql))
            {
                payment = cmd.SetConnection(this.connectionString)
                .SetMap(reader => new Payment
                {
                    Account = reader.GetString("Account"),
                    Id = reader.GetInt("Id"),
                    Start = reader.GetDateTime("Start"),
                    End = reader.GetDateTime("End"),
                    Package = reader.GetInt("Package"),
                    Sequence = reader.GetInt("Sequence"),
                    Amount = reader.GetDouble("Amount")

                })
                .AsList();
            }
            return payment;
        }

        public List<Payment> GetAllPayments()
        {
            var payment = new List<Payment>();

            var sql = @"SELECT 
                        py.Account as Account,
                        py.Id as Id,
                        py.Start as Start,
                        py.End as End,
                        py.Amount as Amount,
                        py.Sequence as Sequence,
                        py.Package as Package
                             FROM 
                                 payment py";


            using (var cmd = new FluentCommand<Payment>(sql))
            {
                payment = cmd.SetConnection(this.connectionString)
                .SetMap(reader => new Payment
                {
                    Account = reader.GetString("Account"),
                    Id = reader.GetInt("Id"),
                    Start = reader.GetDateTime("Start"),
                    End = reader.GetDateTime("End"),
                    Package = reader.GetInt("Package"),
                    Sequence = reader.GetInt("Sequence"),
                    Amount = reader.GetDouble("Amount")

                })
                .AsList();
            }
            return payment;
        }

        public bool UpdateLastPaymentStartDate(int Id, decimal amount, DateTime startDate)
        {
            try
            {
                var sql = @"UPDATE
                          payment as py
                            SET 
                                py.Amount = @amount, 
                                py.Start = @startDate
                                     WHERE
                                        py.Id = @Id";

                using (var cmd = new FluentCommand<Payment>(sql))
                {
                    return cmd.SetConnection(this.connectionString)
                         .AddInt("Id", Id)
                         .AddDecimal("amount", amount)
                         .AddDateTime("startDate", startDate)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateLastPayment(int Id, DateTime endDate)
        {
            try
            {
                var sql = @"UPDATE
                          payment as py
                            SET  
                                py.End = @endDate
                                     WHERE
                                        py.Id = @Id";

                using (var cmd = new FluentCommand<Payment>(sql))
                {
                    return cmd.SetConnection(this.connectionString)
                         .AddInt("Id", Id)
                         .AddDateTime("endDate", endDate)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Information> GetOneAccountingInformation(string accountId)
        {
            var info = new List<Information>();

            var sql = @"SELECT 
                        inf.id,
                        inf.Account,
                        inf.Name,
                        inf.City, 
                        inf.State,
                        inf.Zip,
                        inf.ImplementationFee, 
                        inf.PaymentSource, 
                        inf.InitialSignDate, 
                        inf.SpecialNotes,
                        inf.Status, 
                        inf.SalesPerson,
                        inf.StartUp,
                        py.Id as PaymentId,
                        py.Start as Start,
                        py.End as End,
                        py.Amount as Amount,
                        py.Package as Package
                             FROM 
                                information inf INNER JOIN payment py ON inf.Account = py.Account 
                                     WHERE
                                          inf.Account = @accountId";


            using (var cmd = new FluentCommand<Information>(sql))
            {
                info = cmd.SetConnection(this.connectionString)
                    .AddString("accountId", accountId)
                    .SetMap(reader => new Information
                    {
                        Id = reader.GetInt("id"),
                        Account = reader.GetString("Account"),
                        Name = reader.GetString("Name"),
                        City = reader.GetString("City"),
                        State = reader.GetString("State"),
                        Zip = reader.GetInt("Zip"),
                        ImplementationFee = reader.GetStringNullable("ImplementationFee"),
                        PaymentSource = reader.GetStringNullable("PaymentSource"),
                        InitialSignDate = reader.GetDateTime("InitialSignDate"),
                        SpecialNotes = reader.GetStringNullable("SpecialNotes"),
                        Status = reader.GetInt("Status"),
                        SalesPerson = reader.GetStringNullable("SalesPerson"),
                        StartUp = reader.GetInt("StartUp"),
                        PaymentId = reader.GetInt("PaymentId"),
                        Start = reader.GetDateTime("Start"),
                        End = reader.GetDateTime("End"),
                        Package = reader.GetInt("Package"),
                        Amount = reader.GetDouble("Amount")

                    })
                    .AsList();
            }

            return info;
        }

        public bool UpdateAccountingInformation(string account, string name, string city, string state, int zip, string implementationFee, string paymentSource, DateTime initialSignDate, string specialNotes, int status, int startUp, string salesPerson)
        {
            var sql = @"UPDATE
                          information as inf
                            SET 
                                inf.Name = @name,
                                inf.City = @city, 
                                inf.State = @state,
                                inf.Zip = @zip,
                                inf.ImplementationFee = @implementationFee, 
                                inf.PaymentSource = @paymentSource, 
                                inf.InitialSignDate = @InitialSignDate, 
                                inf.SpecialNotes = @specialNotes,
                                inf.Status = @status, 
                                inf.SalesPerson = @salesPerson,
                                inf.StartUp = @startUp
                                     WHERE
                                          inf.Account = @account";


            using (var cmd = new FluentCommand<Information>(sql))
            {
                return cmd.SetConnection(this.connectionString)
                     .AddString("account", account)
                     .AddString("name", name)
                     .AddString("city", city)
                     .AddString("state", state)
                     .AddInt("zip", zip)
                     .AddString("implementationFee", implementationFee)
                     .AddString("paymentSource", paymentSource)
                     .AddDateTime("initialSignDate", initialSignDate)
                     .AddString("specialNotes", specialNotes)
                     .AddInt("status", status)
                     .AddInt("startUp", startUp)
                     .AddString("salesPerson", salesPerson)
                     .AsNonQuery() > 0;
            }
        }

        public bool UpdateAccountingpayments(int Id, DateTime start, DateTime end, decimal amount, int package)
        {
            var sql = @"UPDATE
                          payment as py
                            SET 
                                py.Start = @start,
                                py.End = @end,
                                py.Amount = @amount, 
                                py.Package = @package
                                     WHERE
                                        py.Id = @Id";


            using (var cmd = new FluentCommand<Payment>(sql))
            {
                return cmd.SetConnection(this.connectionString)
                     .AddInt("Id", Id)
                     .AddDateTime("start", start)
                     .AddDateTime("end", end)
                     .AddDecimal("amount", amount)
                     .AddInt("package", package)
                     .AsNonQuery() > 0;
            }
        }

        public bool AddAccountingpayments(string account, int sequence, DateTime start, decimal amount, int package)
        {
            var sql = @"INSERT INTO
                          payment
                            (account,start,sequence,amount,package) 
                            VALUES 
                                (@account,@start,@sequence,@amount, @package)";

            using (var cmd = new FluentCommand<Payment>(sql))
            {
                return cmd.SetConnection(this.connectionString)
                     .AddInt("sequence", sequence)
                     .AddDateTime("start", start)
                     .AddString("account", account)
                     .AddDecimal("amount", amount)
                     .AddInt("package", package)
                     .AsNonQuery() > 0;
            }
        }

        public bool UpdateReadOnlyInformation(string account, string specialNotes, int status)
        {
            var sql = @"UPDATE
                          information as inf
                            SET 
                                inf.SpecialNotes = @specialNotes,
                                inf.Status = @status
                                     WHERE
                                          inf.Account = @account";


            using (var cmd = new FluentCommand<Information>(sql))
            {
                return cmd.SetConnection(this.connectionString)
                     .AddString("account", account)
                     .AddString("specialNotes", specialNotes)
                     .AddInt("status", status)
                     .AsNonQuery() > 0;
            }
        }

        public Payment GetLastPayments(string account)
        {
            var payment = new Payment();

            var sql = @"SELECT 
                        py.Account as Account,
                        py.Id as Id,
                        py.Start as Start,
                        py.End as End,
                        py.Amount as Amount,
                        py.Sequence as Sequence,
                        py.Package as Package
                             FROM 
                                 payment py
                                   WHERE
                                        py.Account = @account
                                          ORDER BY py.Sequence DESC LIMIT 1";


            using (var cmd = new FluentCommand<Payment>(sql))
            {
                payment = cmd.SetConnection(this.connectionString)
                .AddString("account", account)
                .SetMap(reader => new Payment
                {
                    Account = reader.GetString("Account"),
                    Id = reader.GetInt("Id"),
                    Start = reader.GetDateTime("Start"),
                    End = reader.GetDateTime("End"),
                    Package = reader.GetInt("Package"),
                    Sequence = reader.GetInt("Sequence"),
                    Amount = reader.GetDouble("Amount")
                })
                .AsSingle();
            }
            return payment;
        }

        public Payment GetPrePayments(string account, int sequence)
        {
            var payment = new Payment();

            var sql = @"SELECT 
                        py.Account,
                        py.Id,
                        py.Start,
                        py.End,
                        py.Amount,
                        py.Sequence,
                        py.Package
                             FROM 
                                 payment py
                                   WHERE
                                        py.Account = @account
                                            AND 
                                              py.Sequence = @sequence";


            using (var cmd = new FluentCommand<Payment>(sql))
            {
                payment = cmd.SetConnection(this.connectionString)
                .AddString("account", account)
                .AddInt("sequence", sequence)
                .SetMap(reader => new Payment
                {
                    Account = reader.GetString("Account"),
                    Id = reader.GetInt("Id"),
                    Start = reader.GetDateTime("Start"),
                    End = reader.GetDateTime("End"),
                    Package = reader.GetInt("Package"),
                    Sequence = reader.GetInt("Sequence"),
                    Amount = reader.GetDouble("Amount")
                })
                .AsSingle();
            }
            return payment;
        }

        public bool UpdateReadOnlyLastPayment(int Id, DateTime endDate)
        {
            var sql = @"UPDATE
                          payment as py
                            SET 
                                py.End = @endDate
                                     WHERE
                                        py.Id = @Id";

            using (var cmd = new FluentCommand<Payment>(sql))
            {
                return cmd.SetConnection(this.connectionString)
                     .AddInt("Id", Id)
                     .AddDateTime("endDate", endDate)
                     .AsNonQuery() > 0;
            }
        }

        public bool UpdateUndoReadOnlyInformation(string account, int status)
        {
            var sql = @"UPDATE
                          information as inf
                            SET 
                                inf.Status = @status
                                     WHERE
                                          inf.Account = @account";


            using (var cmd = new FluentCommand<Information>(sql))
            {
                return cmd.SetConnection(this.connectionString)
                     .AddString("account", account)
                     .AddInt("status", status)
                     .AsNonQuery() > 0;
            }
        }

        #endregion



        #region Agency Upgrades

        public bool UpdateAgencyUpgrade(AgencyUpgrade agencyUpgrade)
        {
            if (agencyUpgrade != null)
            {
                agencyUpgrade.Modified = DateTime.Now;
                try
                {
                    var script = @"UPDATE 
                                         agencyupgrades
                                            SET
                                                AgencyId = @AgencyId,
                                                AgencyLocationId = @AgencyLocationId, 
                                                RequestedById = @RequestedById,
                                                AccountId = @AccountId,
                                                PreviousAmount = @PreviousAmount,
                                                Amount = @Amount, 
                                                PreviousPackageId = @PreviousPackageId,
                                                RequestedPackageId = @RequestedPackageId,
                                                AnnualPlanId = @AnnualPlanId,
                                                IsComplete = @IsComplete, 
                                                IsUserPlan = @IsUserPlan,
                                                Comments = @Comments, 
                                                EffectiveDate = @EffectiveDate,
                                                Created = @Created,
                                                Modified = @Modified
                                                    WHERE 
                                                        Id = @Id";
                    using (var cmd = new FluentCommand<AgencyUpgrade>(script))
                    {
                        return cmd.SetConnection(this.connectionString)
                        .AddGuid("Id", agencyUpgrade.Id)
                        .AddGuid("AgencyId", agencyUpgrade.AgencyId)
                        .AddGuid("AgencyLocationId", agencyUpgrade.AgencyLocationId)
                        .AddGuid("RequestedById", agencyUpgrade.RequestedById)
                        .AddString("AccountId", agencyUpgrade.AccountId)
                        .AddDecimal("PreviousAmount", (decimal)agencyUpgrade.PreviousAmount)
                        .AddDecimal("Amount", (decimal)agencyUpgrade.Amount)
                        .AddInt("PreviousPackageId", agencyUpgrade.PreviousPackageId)
                        .AddInt("RequestedPackageId", agencyUpgrade.RequestedPackageId)
                        .AddInt("AnnualPlanId", agencyUpgrade.AnnualPlanId)
                        .AddBoolean("IsComplete", agencyUpgrade.IsComplete)
                        .AddBoolean("IsUserPlan", agencyUpgrade.IsUserPlan)
                        .AddString("Comments", agencyUpgrade.Comments)
                        .AddDateTime("EffectiveDate", agencyUpgrade.EffectiveDate)
                        .AddDateTime("Created", agencyUpgrade.Created)
                        .AddDateTime("Modified", agencyUpgrade.Modified)
                        .AsNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public bool UpdateAgencyUpgrade(Guid Id, Guid agencyId, double amount, double previousAmount, DateTime effectiveDate, string comments, bool isComplete)
        {
            try
            {
                var script = @"UPDATE 
                                    agencyupgrades
                                            SET
                                                Amount = @amount, 
                                                PreviousAmount = @previousAmount,
                                                IsComplete = @isComplete, 
                                                Comments = @comments, 
                                                EffectiveDate = @effectiveDate,
                                                Modified = @modified
                                                    WHERE 
                                                        Id = @Id 
                                                            AND 
                                                               AgencyId = @agencyId";
                using (var cmd = new FluentCommand<AgencyUpgrade>(script))
                {
                    return cmd.SetConnection(this.connectionString)
                    .AddGuid("Id", Id)
                    .AddGuid("agencyId", agencyId)
                    .AddDecimal("amount", (decimal)amount)
                    .AddDecimal("previousAmount", (decimal)previousAmount)
                    .AddBoolean("isComplete", isComplete)
                    .AddString("comments", comments)
                    .AddDateTime("effectiveDate", effectiveDate)
                    .AddDateTime("modified", DateTime.Now)
                    .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public AgencyUpgrade GetAgencyUpgradeOnly(Guid id, Guid agencyId)
        {

            return database.Single<AgencyUpgrade>(u => u.AgencyId == agencyId && u.Id == id);
            //            AgencyUpgrade upgrade = null;
            //            var sql = string.Format(@"SELECT
            //                        agencies.Name as AgencyName,
            //                        agencyupgrades.AgencyId as AgencyId,
            //                        agencyupgrades.AccountId as AccountId,
            //                        agencyupgrades.Id as AgencyUpgradeId, 
            //                        agencyupgrades.AgencyLocationId as AgencyLocationId, 
            //                        agencyupgrades.RequestedById,
            //                        agencyupgrades.PreviousPackageId, 
            //                        agencyupgrades.RequestedPackageId, 
            //                        agencyupgrades.AnnualPlanId, 
            //                        agencyupgrades.Comments, 
            //                        agencyupgrades.Amount,
            //                        agencyupgrades.PreviousAmount,
            //                        agencyupgrades.EffectiveDate,
            //                        agencyupgrades.IsComplete,
            //                        agencyupgrades.IsUserPlan,
            //                        agencyupgrades.Created, 
            //                                    from 
            //                                       agencyupgrades 
            //                                                    where 
            //                                                        agencyupgrades.Id = @id and 
            //                                                        agencyupgrades.AgencyId = @agencyid limit 0, 1;");

            //            using (var cmd = new FluentCommand<AgencyUpgrade>(sql))
            //            {
            //                upgrade = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
            //                    .AddGuid("id", id)
            //                    .AddGuid("agencyid", agencyId)
            //                    .SetMap(reader => new AgencyUpgrade
            //                    {
            //                        AgencyId = reader.GetGuid("AgencyId"),
            //                        Id = reader.GetGuid("AgencyUpgradeId"),
            //                        AgencyLocationId = reader.GetGuidIncludeEmpty("AgencyLocationId"),
            //                        Created = reader.GetDateTime("Created"),
            //                        AnnualPlanId = reader.GetInt("AnnualPlanId"),
            //                        AgencyName = reader.GetString("AgencyName"),
            //                        IsComplete = reader.GetBoolean("IsComplete"),
            //                        IsUserPlan = reader.GetBoolean("IsUserPlan"),
            //                        Comments = reader.GetStringNullable("Comments"),
            //                        RequestedById = reader.GetGuid("RequestedById"),
            //                        PreviousPackageId = reader.GetInt("PreviousPackageId"),
            //                        RequestedPackageId = reader.GetInt("RequestedPackageId"),
            //                        AccountId = reader.GetStringNullable("AccountId"),
            //                        Amount = reader.GetDouble("Amount"),
            //                        PreviousAmount = reader.GetDouble("PreviousAmount"),
            //                        EffectiveDate = reader.GetDateTime("EffectiveDate")                    })
            //                    .AsSingle();
            //            }

            //            return upgrade;
        }

        public IList<AgencyUpgrade> GetAgencyUpgrades(string isCompleted, DateTime startDate, DateTime endDate, int clusterId)
        {
            var list = new List<AgencyUpgrade>();
            var filter = isCompleted.IsEqual("all") ? string.Empty
                : " and aup.IsComplete = @iscompleted ";
            var sql = new StringBuilder(@"SELECT 
                                aup.AgencyId as AgencyId, 
                                aup.AgencyLocationId as LocationId,
                                aup.Id as AgencyUpgradeId, 
                                aup.RequestedById,
                                aup.PreviousPackageId, 
                                aup.RequestedPackageId,
                                aup.IsUserPlan,
                                aup.IsComplete,
                                aup.Amount, 
                                aup.PreviousAmount,
                                aup.Created, 
                                ag.ClusterId as ClusterId
                                    from 
                                       agency ag INNER JOIN  agencyupgrades aup ON aup.AgencyId = ag.AgencyId ")
                                              .AppendFormat("where ag.IsDeprecated = 0 {0} {1}", filter, clusterId > 0 ? " AND ag.ClusterId = @clusterid " : string.Empty)
                                              .Append("and Date(aup.created) between Date(@startdate) and Date(@enddate) ")
                                              .Append("and aup.PreviousPackageId !=0 ")
                                              .ToString();

            using (var cmd = new FluentCommand<AgencyUpgrade>(sql))
            {
                list.AddRange(cmd.SetConnection(this.connectionString)
                    .AddBoolean("iscompleted", isCompleted.ToBoolean())
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .AddInt("clusterid", clusterId)
                    .SetMap(reader => new AgencyUpgrade
                    {
                        AgencyId = reader.GetGuid("AgencyId"),
                        Id = reader.GetGuid("AgencyUpgradeId"),
                        Created = reader.GetDateTime("Created"),
                        IsComplete = reader.GetBoolean("IsComplete"),
                        IsUserPlan = reader.GetBoolean("IsUserPlan"),
                        RequestedById = reader.GetGuid("RequestedById"),
                        PreviousPackageId = reader.GetInt("PreviousPackageId"),
                        RequestedPackageId = reader.GetInt("RequestedPackageId"),
                        AgencyLocationId = reader.GetGuidIncludeEmpty("LocationId"),
                        Amount = reader.GetDouble("Amount") == -1 ? 0 : reader.GetDouble("Amount"),
                        PreviousAmount = reader.GetDouble("PreviousAmount") == -1 ? 0 : reader.GetDouble("PreviousAmount"),
                        ClusterId = reader.GetInt("ClusterId")
                    })
                    .AsList());

            }
            if (list != null && list.Count > 0)
            {
                list = list.OrderByDescending(l => l.Created).ToList();
            }
            return list;
        }

        public IList<AgencyUpgrade> GetExistingAgencyUpgrades(List<Guid> agencyIds)
        {
            var list = new List<AgencyUpgrade>();
            var sql = new StringBuilder(@"SELECT AgencyId as AgencyId from agencyupgrades ")
                              .AppendFormat("where AgencyId in ({0});", agencyIds.ToCommaSeperatedList())
                              .ToString();

            using (var cmd = new FluentCommand<AgencyUpgrade>(sql))
            {
                list.AddRange(cmd.SetConnection(this.connectionString)

                    .SetMap(reader => new AgencyUpgrade
                    {
                        AgencyId = reader.GetGuid("AgencyId")
                    })
                    .AsList());

            }
            return list;
        }

        #endregion

        #region Annual Plan

        public IList<AgencyUpgrade> GetAgencyAnnualPlan(string isCompleted, DateTime startDate, DateTime endDate, int clusterId)
        {
            var list = new List<AgencyUpgrade>();
            //var clusterIds = GetClusterIds();
            //if (clusterIds != null)
            //{
            //    foreach (var clusterId in clusterIds)
            //    {
            var filter = isCompleted.IsEqual("all") ? string.Empty
                : " and aup.IsComplete = @iscompleted ";
            var sql = new StringBuilder(@"SELECT
                            aup.Id as AgencyUpgradeId, 
                            aup.AgencyId as AgencyId, 
                            aup.AgencyLocationId as LocationId,
                            aup.RequestedById,
                            aup.PreviousPackageId,
                            aup.AnnualPlanId, 
                            aup.RequestedPackageId, 
                            aup.IsComplete,
                            aup.Amount,
                            aup.PreviousAmount,
                            aup.Created, 
                            ag.ClusterId as ClusterId
                                    from 
                                      agency ag INNER JOIN   agencyupgrades aup ON  aup.AgencyId = ag.AgencyId ")
                                            .AppendFormat(" where ag.IsDeprecated = 0 {0} {1}", filter, clusterId > 0 ? " AND ag.ClusterId = @clusterid " : string.Empty)
                                            .Append(" and Date(aup.created) between Date(@startdate) and Date(@enddate) ")
                                            .Append(" and aup.AnnualPlanId !=0 ")
                                            .Append(" and aup.PreviousPackageId=0 ")
                                            .ToString();

            using (var cmd = new FluentCommand<AgencyUpgrade>(sql))
            {
                list.AddRange(cmd.SetConnection(this.connectionString)//+ clusterId)
                    .AddBoolean("iscompleted", isCompleted.ToBoolean())
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .AddInt("clusterid", clusterId)
                    .SetMap(reader => new AgencyUpgrade
                    {
                        AgencyId = reader.GetGuid("AgencyId"),
                        Id = reader.GetGuid("AgencyUpgradeId"),
                        Created = reader.GetDateTime("Created"),
                        AgencyLocationId = reader.GetGuidIncludeEmpty("LocationId"),
                        IsComplete = reader.GetBoolean("IsComplete"),
                        Amount = reader.GetDouble("Amount") == -1 ? 0 : reader.GetDouble("Amount"),
                        PreviousAmount = reader.GetDouble("PreviousAmount") == -1 ? 0 : reader.GetDouble("PreviousAmount"),
                        RequestedById = reader.GetGuid("RequestedById"),
                        AnnualPlanId = reader.GetInt("AnnualPlanId"),
                        PreviousPackageId = reader.GetInt("PreviousPackageId"),
                        RequestedPackageId = reader.GetInt("RequestedPackageId"),
                        ClusterId = reader.GetInt("ClusterId")
                    })
                    .AsList());
            }
            //    }
            //}
            if (list != null && list.Count > 0)
            {
                list = list.OrderByDescending(l => l.Created).ToList();
            }
            return list;
        }

        #endregion

        #region Change

        public bool UpdateAgencyChange(Guid Id, Guid agencyId, string comments, bool isComplete)
        {
            var script = @"UPDATE agencychanges
                           SET 
                            agencychanges.IsComplete = @isComplete,
                            agencychanges.Comments = @comments
                                                       WHERE agencychanges.Id = @Id AND agencychanges.AgencyId = @agencyId";
            using (var cmd = new FluentCommand<AgencyChange>(script))
            {
                return cmd.SetConnection(this.connectionString)
                    .AddString("comments", comments)
                    .AddBoolean("isComplete", isComplete)
                    .AddGuid("Id", Id)
                    .AddGuid("agencyId", agencyId)
                    .AsNonQuery() > 0;
            }
        }

        public AgencyChange GetAgencyChangePrev(Guid Id, Guid agencyId)
        {
            var script = @"SELECT
                        acg.Id as Id,
                        acg.AgencyId as AgencyId,
                        acg.Name as AgencyName,
                        acg.TaxId,
                        acg.TaxIdType,
                        acg.ContactPersonEmail,
                        acg.ContactPersonPhone,
                        acg.CahpsVendor,
                        acg.NationalProviderNumber,
                        acg.MedicareProviderNumber,
                        acg.MedicaidProviderNumber,
                        acg.HomeHealthAgencyId,
                        acg.ContactPersonFirstName,
                        acg.ContactPersonLastName,
                        acg.LocationName,
                        acg.LocationAddressLine1, 
                        acg.LocationAddressLine2,
                        acg.LocationAddressCity, 
                        acg.LocationAddressStateCode, 
                        acg.LocationAddressZipCode,
                        acg.LocationPhoneWork,
                        acg.LocationFaxNumber,
                        acg.FullName,
                        acg.Created,
                        acg.IsComplete,
                        acg.Comments
                            FROM
                                agencychanges acg
                                        Where 
                                             acg.agencyId = @agencyId and acg.Id = @Id limit 0,1";


            return new FluentCommand<AgencyChange>(script)
                .SetConnection(this.connectionString)
                .AddGuid("@agencyid", agencyId)
                .AddGuid("@Id", Id)
                .SetMap(reader => new AgencyChange
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Name = reader.GetString("AgencyName"),
                    TaxId = reader.GetStringNullable("TaxId"),
                    TaxIdType = reader.GetStringNullable("TaxIdType"),
                    ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                    ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    HomeHealthAgencyId = reader.GetStringNullable("HomeHealthAgencyId"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
                    LocationName = reader.GetStringNullable("LocationName"),
                    LocationAddressLine1 = reader.GetStringNullable("LocationAddressLine1"),
                    LocationAddressLine2 = reader.GetStringNullable("LocationAddressLine2"),
                    LocationAddressCity = reader.GetStringNullable("LocationAddressCity"),
                    LocationAddressStateCode = reader.GetStringNullable("LocationAddressStateCode"),
                    LocationAddressZipCode = reader.GetStringNullable("LocationAddressZipCode"),
                    LocationPhoneWork = reader.GetStringNullable("LocationPhoneWork"),
                    LocationFaxNumber = reader.GetStringNullable("LocationFaxNumber"),
                    FullName = reader.GetStringNullable("FullName"),
                    Created = reader.GetDateTime("Created"),
                    IsComplete = reader.GetBoolean("IsComplete"),
                    Comments = reader.GetStringNullable("Comments")

                }).AsSingle();
        }

        public IList<AgencyChange> GetAgencyChanges(string isCompleted, DateTime startDate, DateTime endDate)
        {
            var list = new List<AgencyChange>();
            var filter = isCompleted.IsEqual("all") ? string.Empty
                : " and acg.IsComplete = @iscompleted ";
            var sql = new StringBuilder(@"SELECT
                                acg.Id as AgencyChangeId,
                                acg.Name, 
                                acg.TaxId, 
                                acg.TaxIdType, 
                                acg.ContactPersonEmail,
                                acg.ContactPersonPhone, 
                                acg.CahpsVendor, 
                                acg.NationalProviderNumber, 
                                acg.MedicareProviderNumber,
                                acg.MedicaidProviderNumber, 
                                acg.HomeHealthAgencyId, 
                                acg.ContactPersonFirstName, 
                                acg.ContactPersonLastName,
                                acg.LocationName, 
                                acg.LocationAddressLine1, 
                                acg.LocationAddressLine2,
                                acg.LocationAddressCity, 
                                acg.LocationAddressStateCode, 
                                acg.LocationAddressZipCode, 
                                acg.LocationPhoneWork, 
                                acg.LocationFaxNumber, 
                                acg.Created, 
                                acg.IsComplete,
                                acg.Comments, 
                                acg.AgencyId,
                                acg.FullName 
                                    from agencychanges acg")
                                                .AppendFormat(" where Date(acg.Created) between Date(@startdate) and Date(@enddate) {0}", filter)
                                                .ToString();

            using (var cmd = new FluentCommand<AgencyChange>(sql))
            {
                list = cmd.SetConnection(this.connectionString)
                    .AddBoolean("iscompleted", isCompleted.ToBoolean())
                    .AddDateTime("startdate", startDate)
                    .AddDateTime("enddate", endDate)
                    .SetMap(reader => new AgencyChange
                    {
                        Id = reader.GetGuid("AgencyChangeId"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        Name = reader.GetStringNullable("Name"),
                        TaxId = reader.GetStringNullable("TaxId"),
                        TaxIdType = reader.GetStringNullable("TaxIdType"),
                        ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                        ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone"),
                        CahpsVendor = reader.GetInt("CahpsVendor"),
                        NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                        MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                        MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                        HomeHealthAgencyId = reader.GetStringNullable("HomeHealthAgencyId"),
                        ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
                        ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
                        LocationName = reader.GetStringNullable("LocationName"),
                        LocationAddressLine1 = reader.GetStringNullable("LocationAddressLine1"),
                        LocationAddressLine2 = reader.GetStringNullable("LocationAddressLine2"),
                        LocationAddressCity = reader.GetStringNullable("LocationAddressCity"),
                        LocationAddressStateCode = reader.GetStringNullable("LocationAddressStateCode"),
                        LocationAddressZipCode = reader.GetStringNullable("LocationAddressZipCode"),
                        LocationPhoneWork = reader.GetStringNullable("LocationPhoneWork"),
                        LocationFaxNumber = reader.GetStringNullable("LocationFaxNumber"),
                        FullName = reader.GetStringNullable("FullName"),
                        Created = reader.GetDateTime("Created"),
                        IsComplete = reader.GetBoolean("IsComplete"),
                        Comments = reader.GetStringNullable("Comments")
                    })
                    .AsList();
                if (list != null && list.Count > 0)
                {
                    list = list.OrderByDescending(l => l.Created).ToList();
                }
            }
            return list;
        }

        #endregion

        #region Subscription Plan

        public bool AddSubscriptionPlan(AgencySubscriptionPlan plan)
        {
            if (plan != null)
            {
                plan.Id = Guid.NewGuid();
                try
                {
                    var script = @"INSERT INTO 
                                         agencysubscriptionplans
                                            (
                                            Id,
                                            AgencyId,
                                            AgencyLocationId, 
                                            IsUserPlan,
                                            PlanLimit
                                          ) 
                                          VALUES 
                                          (
                                            @Id,
                                            @AgencyId,
                                            @AgencyLocationId, 
                                            @IsUserPlan,
                                            @PlanLimit
                                          );";
                    using (var cmd = new FluentCommand<Agency>(script))
                    {
                        return cmd.SetConnection(this.connectionString)
                        .AddGuid("Id", plan.Id)
                        .AddGuid("AgencyId", plan.AgencyId)
                        .AddGuid("AgencyLocationId", plan.AgencyLocationId)
                        .AddBoolean("IsUserPlan", plan.IsUserPlan)
                        .AddInt("PlanLimit", plan.PlanLimit)
                        .AsNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool UpdateSubscriptionPlan(AgencySubscriptionPlan plan)
        {
            if (plan != null)
            {
                try
                {
                    var script = @"UPDATE 
                                         agencysubscriptionplans
                                            SET
                                                AgencyId = @AgencyId,
                                                AgencyLocationId = @AgencyLocationId, 
                                                IsUserPlan = @IsUserPlan,
                                                PlanLimit = @PlanLimit
                                                    WHERE 
                                                        Id = @Id";
                    using (var cmd = new FluentCommand<Agency>(script))
                    {
                        return cmd.SetConnection(this.connectionString)
                        .AddGuid("Id", plan.Id)
                        .AddGuid("AgencyId", plan.AgencyId)
                        .AddGuid("AgencyLocationId", plan.AgencyLocationId)
                        .AddBoolean("IsUserPlan", plan.IsUserPlan)
                        .AddInt("PlanLimit", plan.PlanLimit)
                        .AsNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public AgencySubscriptionPlan GetSubscriptionPlan(Guid agencyId, Guid locationId)
        {
            var agencySubscriptionPlan = new AgencySubscriptionPlan();
            var script = @"SELECT 
                                Id,
                                AgencyId,
                                AgencyLocationId, 
                                IsUserPlan,
                                PlanLimit
                                    FROM agencysubscriptionplans
                                        WHERE 
                                            AgencyId = @agencyId AND 
                                            AgencyLocationId = @locationId Limit 1";
            using (var cmd = new FluentCommand<AgencySubscriptionPlan>(script))
            {
                agencySubscriptionPlan = cmd.SetConnection(this.connectionString)
                .AddGuid("agencyId", agencyId)
                .AddGuid("locationId", locationId)
                .SetMap(reader => new AgencySubscriptionPlan
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                    IsUserPlan = reader.GetBoolean("IsUserPlan"),
                    PlanLimit = reader.GetInt("PlanLimit")
                })
                .AsSingle();
            }
            return agencySubscriptionPlan;
        }

        public IList<AgencySubscriptionPlan> GetLocationSubscriptionPlanByLocationId(Guid agencyId, List<Guid> locationIds)
        {
            var list = new List<AgencySubscriptionPlan>();
            if (locationIds != null && locationIds.Count > 0)
            {
                var sql = string.Format(@"SELECT
                                    ap.Id as Id,                                   
                                    ap.AgencyLocationId  as AgencyLocationId, 
                                    ap.IsUserPlan,
                                    ap.PlanLimit 
                                        FROM 
                                             agencysubscriptionplans ap
                                                WHERE 
                                                    ap.AgencyId = @agencyid AND 
                                                    ap.AgencyLocationId IN ({0});", locationIds.ToCommaSeperatedList());
                using (var cmd = new FluentCommand<AgencySubscriptionPlan>(sql))
                {
                    list = cmd.SetConnection(this.connectionString)
                        .AddGuid("agencyid", agencyId)
                        .SetMap(reader => new AgencySubscriptionPlan
                        {
                            Id = reader.GetGuid("Id"),
                            AgencyLocationId = reader.GetGuid("AgencyLocationId"),
                            AgencyId = agencyId,
                            PlanLimit = reader.GetInt("PlanLimit"),
                            IsUserPlan = reader.GetBoolean("IsUserPlan")
                        }).AsList();
                }
            }
            return list;
        }


        #endregion


        public bool AddAgencyProfile(Guid agencyId, bool isSuspendPayment, DateTime readOnlyDate, string frozenReason, string assetId)
        {
            var agencyProfile = new AgencyProfile();
            agencyProfile.Id = Guid.NewGuid();
            agencyProfile.AgencyId = agencyId;
            agencyProfile.FrozenReason = frozenReason;
            agencyProfile.EffectiveDate = readOnlyDate;
            agencyProfile.IsSuspendPayment = isSuspendPayment;
            agencyProfile.AssetId = assetId.ToGuid();
            agencyProfile.Created = DateTime.Now;
            try
            {
                var script = @"INSERT INTO 
                                        agencyprofiles
                                            (
                                                Id,
                                                AgencyId,
                                                AssetId, 
                                                IsSuspendPayment,
                                                FrozenReason,
                                                EffectiveDate,
                                                Created
                                          ) 
                                          VALUES 
                                            (
                                                @Id,
                                                @AgencyId,
                                                @AssetId, 
                                                @IsSuspendPayment,
                                                @FrozenReason,
                                                @EffectiveDate,
                                                @Created
                                          );";
                using (var cmd = new FluentCommand<AgencyProfile>(script))
                {
                    return cmd.SetConnection(this.connectionString)
                    .AddGuid("Id", agencyProfile.Id)
                    .AddGuid("AgencyId", agencyProfile.AgencyId)
                    .AddBoolean("IsSuspendPayment", agencyProfile.IsSuspendPayment)
                    .AddString("FrozenReason", agencyProfile.FrozenReason)
                    .AddDateTime("EffectiveDate", agencyProfile.EffectiveDate)
                    .AddDateTime("Created", agencyProfile.Created)
                    .AddGuid("AssetId", agencyProfile.AssetId)
                    .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IList<AgencyMarkAsReadOnly> GetAgencyMarkAsReadOnly(DateTime startDate, DateTime endDate)
        {
            var account = new List<AgencyMarkAsReadOnly>();

            var sql = @"SELECT 
                        ag.AgencyId as AgencyId,
                        agp.Created,
                        agp.EffectiveDate,
                        agp.IsSuspendPayment,
                        agp.FrozenReason,
                        ag.ClusterId as ClusterId
                             FROM 
                                 agency ag INNER JOIN agencyprofiles agp on ag.AgencyId = agp.agencyId
                                    WHERE 
                                       agp.EffectiveDate BETWEEN @startDate AND @endDate
                                          ORDER BY agp.EffectiveDate";


            using (var cmd = new FluentCommand<AgencyMarkAsReadOnly>(sql))
            {
                account = cmd.SetConnection(this.connectionString)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new AgencyMarkAsReadOnly
                {

                    AgencyId = reader.GetGuid("AgencyId"),
                    EffectiveDate = reader.GetDateTime("EffectiveDate"),
                    Created = reader.GetDateTime("Created"),
                    IsSuspendPayment = reader.GetBoolean("IsSuspendPayment"),
                    FrozenReason = reader.GetStringNullable("FrozenReason"),
                    ClusterId = reader.GetInt("ClusterId")
                })
                .AsList();
            }
            return account;
        }

        public bool ToggleAccuntAgency(Guid id)
        {
            try
            {
                var sql = @"UPDATE
                          agency as ag
                            SET 
                                ag.IsDeprecated = !ag.IsDeprecated,
                                     WHERE
                                          ag.Id = @id";


                using (var cmd = new FluentCommand<int>(sql))
                {
                    return cmd.SetConnection(this.connectionString)
                         .AddGuid("id", id)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public bool AddAccountAgency(Guid agencyId, Guid branchId, int clusterId)
        {
            try
            {
                var script = @"INSERT INTO 
                                        agency
                                            (
                                                AgencyId,
                                                BranchId,
                                                ClusterId
                                          ) 
                                          VALUES 
                                            (
                                                @AgencyId,
                                                @BranchId,
                                                @ClusterId
                                          );";
                using (var cmd = new FluentCommand<int>(script))
                {
                    return cmd.SetConnection(this.connectionString)
                    .AddGuid("AgencyId", agencyId)
                    .AddGuid("BranchId", branchId)
                    .AddInt("ClusterId", clusterId)
                    .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool RemoveAccountAgency(Guid Id)
        {
            try
            {
                var sql = @"DELETE
                             from  agency 
                                         WHERE
                                              AgencyId = @id";


                using (var cmd = new FluentCommand<Agency>(sql))
                {
                    return cmd.SetConnection(this.connectionString)
                         .AddGuid("id", Id)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
           
        }

        public bool Add(Asset asset)
        {
            bool result = false;

            if (asset != null)
            {
                asset.Id = Guid.NewGuid();
                asset.Created = DateTime.Now;
                asset.Modified = DateTime.Now;
                using (new SharedDbConnectionScope(ProviderFactory.GetProvider(this.connectionString)))
                {
                    database.Add<Asset>(asset);
                }
                result = true;
            }

            return result;
        }

        public bool removeAccountingpayments(string account, int sequence)
        {
            try
            {
                var sql = @"DELETE
                             from  payment 
                                         WHERE
                                              account = @account AND sequence = @sequence";

                using (var cmd = new FluentCommand<AgencyUpgrade>(sql))
                {
                    return cmd.SetConnection("AccountingConnectionString")
                         .AddString("account", account)
                         .AddInt("sequence", sequence)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
