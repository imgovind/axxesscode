﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    public class MedicationProfileHistoryRepository : IMedicationProfileHistoryRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public MedicationProfileHistoryRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        public bool AddNewMedicationHistory(MedicationProfileHistory medicationHistory)
        {
            try
            {
                database.Add<MedicationProfileHistory>(medicationHistory);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateMedicationProfileHistory(MedicationProfileHistory medicationProfileHistory)
        {
            try
            {
                if (medicationProfileHistory != null)
                {
                    medicationProfileHistory.Modified = DateTime.Now;
                    database.Update<MedicationProfileHistory>(medicationProfileHistory);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public MedicationProfileHistory GetMedicationProfileHistory(Guid Id, Guid AgencyId)
        {
            return database.Single<MedicationProfileHistory>(m => m.AgencyId == AgencyId && m.Id == Id);
        }

        public IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId, Guid agencyId)
        {
            return database.Find<MedicationProfileHistory>(m => m.PatientId == patientId && m.AgencyId == agencyId && m.IsDeprecated == false);
        }

        public IList<MedicationProfileHistory> GetAllMedicationProfileHistory()
        {
            return database.All<MedicationProfileHistory>().ToList();
        }
    }
}
