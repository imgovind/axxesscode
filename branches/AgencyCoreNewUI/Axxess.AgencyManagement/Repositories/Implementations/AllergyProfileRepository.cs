﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    public class AllergyProfileRepository : IAllergyProfileRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AllergyProfileRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region Allergies

        public bool UpdateAllergyProfile(AllergyProfile allergyProfile)
        {
            if (allergyProfile != null)
            {
                allergyProfile.Modified = DateTime.Now;
                database.Update<AllergyProfile>(allergyProfile);
                return true;
            }
            return false;
        }

        public AllergyProfile GetAllergyProfileByPatient(Guid patientId, Guid agencyId)
        {
            var profile = database.Single<AllergyProfile>(m => m.AgencyId == agencyId && m.PatientId == patientId);

            if (profile == null)
            {
                profile = new AllergyProfile
                {
                    Id = Guid.NewGuid(),
                    AgencyId = agencyId,
                    PatientId = patientId,
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Allergies = new List<Allergy>().ToXml()
                };
                database.Add<AllergyProfile>(profile);
            }

            return profile;
        }

        public AllergyProfile GetAllergyProfile(Guid profileId, Guid agencyId)
        {
            return database.Single<AllergyProfile>(a => a.AgencyId == agencyId && a.Id == profileId); ;
        }
       
        #endregion
    }
}
