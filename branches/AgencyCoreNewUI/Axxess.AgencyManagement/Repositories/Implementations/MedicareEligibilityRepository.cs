﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    public class MedicareEligibilityRepository : IMedicareEligibilityRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public MedicareEligibilityRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion


        #region Medicare Eligibility

        public bool UpdateMedicareEligibility(MedicareEligibility medicareEligibility)
        {
            var result = false;
            if (medicareEligibility != null)
            {
                database.Update<MedicareEligibility>(medicareEligibility);
                result = true;
            }
            return result;
        }


        public MedicareEligibility GetMedicareEligibility(Guid agencyId, Guid patientId, Guid id)
        {
            return database.Single<MedicareEligibility>(m => m.AgencyId == agencyId && m.PatientId == patientId && m.Id == id);
        }

        public MedicareEligibility GetMedicareEligibility(Guid agencyId, Guid episodeId, Guid patientId, Guid id)
        {
            return database.Single<MedicareEligibility>(m => m.AgencyId == agencyId && m.EpisodeId == episodeId && m.PatientId == patientId && m.Id == id);
        }

      
        public List<MedicareEligibility> GetMedicareEligibilities(Guid agencyId, Guid patientId)
        {
            return database.Find<MedicareEligibility>(m => m.AgencyId == agencyId && m.PatientId == patientId).ToList();
        }

        #endregion
    }
}
