﻿
namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
   
    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;
   

   public class PatientEpisodeRepository : IPatientEpisodeRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PatientEpisodeRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

       public bool AddEpisode(PatientEpisode patientEpisode)
       {

           var result = false;
           if (patientEpisode != null)
           {
               try
               {
                   patientEpisode.Details = patientEpisode.Detail.ToXml();
                   patientEpisode.Created = DateTime.Now;
                   patientEpisode.Modified = DateTime.Now;
                   database.Add<PatientEpisode>(patientEpisode);
                   result = true;
               }
               catch (Exception ex)
               {
                   return false;
               }
           }
           return result;
       }
       public bool UpdateEpisode(Guid agencyId, PatientEpisode patientEpisode)
       {
           var result = false;
           try
           {
               var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientEpisode.PatientId && e.Id == patientEpisode.Id);
               if (episode != null)
               {
                   episode.StartDate = patientEpisode.StartDate;
                   episode.EndDate = patientEpisode.EndDate;
                   //episode.Schedule = patientEpisode.Schedule;
                   episode.IsActive = patientEpisode.IsActive;
                   episode.Details = patientEpisode.Details;
                   episode.AdmissionId = patientEpisode.AdmissionId;
                   episode.Modified = DateTime.Now;
                   database.Update<PatientEpisode>(episode);
                   result = true;
               }
           }
           catch (Exception ex)
           {
               return false;
           }
           return result;
       }
       public bool UpdateEpisode(PatientEpisode episode)
       {
           var result = false;
           try
           {
               if (episode != null)
               {
                   database.Update<PatientEpisode>(episode);
                   result = true;
               }
           }
           catch (Exception ex)
           {
               return false;
           }
           return result;
       }
       public bool UpdateEpisodeForDischarge(List<PatientEpisode> patientEpisodes)
       {
           var result = false;
           try
           {
               if (patientEpisodes.Count > 0)
               {
                   database.UpdateMany<PatientEpisode>(patientEpisodes);
                   result = true;
               }
           }
           catch (Exception ex)
           {
               return false;
           }
           return result;
       }
       public bool DeleteEpisode(Guid agencyId, Patient patient, out PatientEpisode episodeDeleted)
       {
           try
           {
               var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patient.Id && e.StartDate <= DateTime.Now && e.EndDate > DateTime.Now);
               episodeDeleted = episode;
               if (episode != null)
               {
                   database.Delete<PatientEpisode>(episode.Id);
               }
           }
           catch (Exception ex)
           {
               episodeDeleted = null;
               return false;
           }
           return true;
       }
       public bool DeleteEpisode(Guid agencyId, Guid patientId, Guid episodeId)
       {
           try
           {
               var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
               if (episode != null)
               {
                   database.Delete<PatientEpisode>(episode.Id);
                   return true;
               }
           }
           catch (Exception ex)
           {
               return false;
           }
           return false;
       }
       public PatientEpisode GetEpisodeById(Guid agencyId, Guid episodeId, Guid patientId)
       {
           return database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
       }
       public PatientEpisode GetEpisodeByIdWithSOC(Guid agencyId, Guid episodeId, Guid patientId)
       {
           var script = @"SELECT 
                        patientepisodes.Id as Id ,
                        patientepisodes.PatientId as PatientId ,
                        patientepisodes.Details as Details ,
                        patientepisodes.StartDate as StartDate ,
                        patientepisodes.EndDate as EndDate , 
                        patientepisodes.IsActive as IsActive ,
                        patientepisodes.IsRecertCompleted as IsRecertCompleted , 
                        patientepisodes.IsDischarged as IsDischarged , 
                        patientepisodes.IsLinkedToDischarge as IsLinkedToDischarge ,
                        patientepisodes.AssessmentId as AssessmentId , 
                        patientepisodes.AssessmentType as AssessmentType ,
                        patientepisodes.Created as Created ,
                        patientepisodes.Modified as Modified ,
                        patientepisodes.AdmissionId as AdmissionId ,
                        patientadmissiondates.StartOfCareDate as StartOfCareDate 
                            FROM 
                                patientepisodes
                                    INNER JOIN patientadmissiondates ON patientepisodes.AdmissionId = patientadmissiondates.Id 
                                        WHERE 
                                            patientepisodes.AgencyId = @agencyid AND 
                                            patientadmissiondates.AgencyId = @agencyid AND
                                            patientepisodes.Id = @episodeid AND 
                                            patientepisodes.PatientId = @patientid LIMIT 1 ";

           var patientEpisode = new PatientEpisode();
           using (var cmd = new FluentCommand<PatientEpisode>(script))
           {
               patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientid", patientId)
               .AddGuid("episodeid", episodeId)
               .SetMap(reader => new PatientEpisode
               {
                   Id = reader.GetGuid("Id"),
                   PatientId = reader.GetGuid("PatientId"),
                   Details = reader.GetStringNullable("Details"),
                   StartDate = reader.GetDateTime("StartDate"),
                   EndDate = reader.GetDateTime("EndDate"),
                   IsActive = reader.GetBoolean("IsActive"),
                   IsRecertCompleted = reader.GetBoolean("IsRecertCompleted"),
                   IsDischarged = reader.GetBoolean("IsDischarged"),
                   IsLinkedToDischarge = reader.GetBoolean("IsLinkedToDischarge"),
                   AssessmentId = reader.GetGuid("AssessmentId"),
                   AssessmentType = reader.GetStringNullable("AssessmentType"),
                   Created = reader.GetDateTime("Created"),
                   Modified = reader.GetDateTime("Modified"),
                   AdmissionId = reader.GetGuid("AdmissionId"),
                   StartOfCareDate = reader.GetDateTime("StartOfCareDate")
               })
               .AsSingle();
           }
           return patientEpisode;
       }
       //public PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date, string discipline)
       //{
       //    Check.Argument.IsNotEmpty(patientId, "patientId");
       //    Check.Argument.IsNotNull(discipline, "discipline");
       //    var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.StartDate);

       //    var episode = allEpisodes.Where(e => e.StartDate.Date <= date.Date).FirstOrDefault();
       //    if (episode != null)
       //    {
       //        var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId && p.IsDeprecated == false);
       //        if (patient != null)
       //        {
       //            episode.DisplayName = patient.DisplayName;
       //        }
       //        var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < episode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
       //        var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > episode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
       //        episode.ScheduleEvents = this.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(episode.AgencyId, episode.Id, episode.PatientId, episode.StartDate, episode.EndDate, discipline, true, true);
       //        if (episode.Details.IsNotNullOrEmpty())
       //        {
       //            episode.Detail = episode.Details.ToObject<EpisodeDetail>();
       //        }
       //        if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
       //        {
       //            episode.NextEpisode = nextEpisode;
       //            if (episode.NextEpisode.Details.IsNotNullOrEmpty())
       //            {
       //                episode.NextEpisode.Detail = episode.NextEpisode.Details.ToObject<EpisodeDetail>();
       //            }
       //        }
       //        if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
       //        {
       //            episode.PreviousEpisode = previousEpisode;
       //            if (episode.PreviousEpisode.Details.IsNotNullOrEmpty())
       //            {
       //                episode.PreviousEpisode.Detail = episode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
       //            }
       //        }
       //    }
       //    else if (allEpisodes != null)
       //    {
       //        var futureEpisode = allEpisodes.Where(e => e.IsActive).LastOrDefault();
       //        if (futureEpisode != null)
       //        {
       //            var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId && p.IsDeprecated == false);
       //            if (patient != null)
       //            {
       //                futureEpisode.DisplayName = patient.DisplayName;
       //            }
       //            var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < futureEpisode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
       //            var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > futureEpisode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
       //            futureEpisode.ScheduleEvents = this.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(futureEpisode.AgencyId, futureEpisode.Id, futureEpisode.PatientId, futureEpisode.StartDate, futureEpisode.EndDate, discipline, true, true);
       //            if (futureEpisode.Details.IsNotNullOrEmpty())
       //            {
       //                futureEpisode.Detail = futureEpisode.Details.ToObject<EpisodeDetail>();
       //            }
       //            if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
       //            {
       //                futureEpisode.NextEpisode = nextEpisode;
       //                if (futureEpisode.NextEpisode.Details.IsNotNullOrEmpty())
       //                {
       //                    futureEpisode.NextEpisode.Detail = futureEpisode.NextEpisode.Details.ToObject<EpisodeDetail>();
       //                }
       //            }
       //            if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
       //            {
       //                futureEpisode.PreviousEpisode = previousEpisode;
       //                if (futureEpisode.PreviousEpisode.Details.IsNotNullOrEmpty())
       //                {
       //                    futureEpisode.PreviousEpisode.Detail = futureEpisode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
       //                }
       //            }
       //            episode = futureEpisode;
       //        }
       //    }
       //    return episode;
       //}

       //public PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId, string discipline)
       //{
       //    PatientEpisode episode = null;
       //    if (!patientId.IsEmpty() && !episodeId.IsEmpty() && discipline.IsNotNullOrEmpty())
       //    {
       //        try
       //        {
       //            var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate);
       //            if (allEpisodes != null)
       //            {
       //                episode = allEpisodes.Where(e => e.Id == episodeId).SingleOrDefault();
       //                if (episode != null)
       //                {
       //                    var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
       //                    if (patient != null)
       //                    {
       //                        episode.DisplayName = patient.DisplayName;
       //                    }
       //                    var previousEpisode = allEpisodes.Where(e => (e.StartDate < episode.StartDate) && e.IsActive == true).OrderByDescending(e => e.StartDate).FirstOrDefault();
       //                    var nextEpisode = allEpisodes.Where(e => (e.StartDate > episode.StartDate) && e.IsActive == true).OrderBy(e => e.StartDate).FirstOrDefault();

       //                    episode.ScheduleEvents = this.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(episode.AgencyId, episode.Id, episode.PatientId, episode.StartDate, episode.EndDate, discipline, false, true);
       //                    if (episode.Details.IsNotNullOrEmpty())
       //                    {
       //                        episode.Detail = episode.Details.ToObject<EpisodeDetail>();
       //                    }

       //                    if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
       //                    {
       //                        episode.NextEpisode = nextEpisode;
       //                        if (episode.NextEpisode.Details.IsNotNullOrEmpty())
       //                        {
       //                            episode.NextEpisode.Detail = episode.NextEpisode.Details.ToObject<EpisodeDetail>();
       //                        }
       //                    }
       //                    if (previousEpisode != null && !previousEpisode.Id.IsEmpty())
       //                    {
       //                        episode.PreviousEpisode = previousEpisode;
       //                        if (episode.PreviousEpisode.Details.IsNotNullOrEmpty())
       //                        {
       //                            episode.PreviousEpisode.Detail = episode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
       //                        }
       //                    }
       //                }
       //            }
       //        }
       //        catch (Exception e)
       //        {
       //            return null;
       //        }
       //    }
       //    return episode;

       //}

       //public PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId)
       //{
       //    PatientEpisode patientEpisode = null;
       //    if (!patientId.IsEmpty() && !episodeId.IsEmpty())
       //    {
       //        patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
       //        if (patientEpisode != null)
       //        {
       //            var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate);
       //            if (allEpisodes != null)
       //            {
       //                var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < patientEpisode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
       //                var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > patientEpisode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
       //                if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
       //                {
       //                    patientEpisode.NextEpisode = nextEpisode;
       //                }
       //                if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
       //                {
       //                    patientEpisode.PreviousEpisode = previousEpisode;

       //                }
       //            }
       //            patientEpisode.ScheduleEvents = this.GetPatientScheduledEventsWithDateRangeOrNotNew(patientEpisode.AgencyId, patientEpisode.Id, patientEpisode.PatientId, true, patientEpisode.StartDate, patientEpisode.EndDate);
       //            if (patientEpisode.Details.IsNotNullOrEmpty())
       //            {
       //                patientEpisode.Detail = patientEpisode.Details.ToObject<EpisodeDetail>();
       //            }
       //        }
       //    }
       //    return patientEpisode;
       //}
       public PatientEpisode GetEpisodeOnlyWithPreviousAndAfter(Guid agencyId, Guid episodeId, Guid patientId)
       {
           PatientEpisode patientEpisode = null;
           if (!patientId.IsEmpty() && !episodeId.IsEmpty())
           {
               patientEpisode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
               if (patientEpisode != null)
               {
                   var patient = database.Single<Patient>(p => p.AgencyId == agencyId && p.Id == patientId);
                   if (patient != null)
                   {
                       patientEpisode.DisplayName = patient.DisplayName;
                       patientEpisode.StartOfCareDateFormatted = patient.StartOfCareDateFormatted;
                   }
                   var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate);
                   if (allEpisodes != null)
                   {
                       var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < patientEpisode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
                       var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > patientEpisode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
                       if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                       {
                           patientEpisode.NextEpisode = nextEpisode;
                       }
                       if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                       {
                           patientEpisode.PreviousEpisode = previousEpisode;
                       }
                   }
               }
           }
           return patientEpisode;
       }
       public PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date)
       {
           Check.Argument.IsNotEmpty(patientId, "patientId");
           PatientEpisode episode = null;
           try
           {
               episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).Where(se => se.StartDate.Date <= date.Date && se.EndDate.Date >= date.Date).OrderByDescending(se => se.StartDate.Date).ThenByDescending(se => se.EndDate.Date).FirstOrDefault();
           }
           catch (Exception e)
           {
               return null;
           }
           return episode;
       }
       public PatientEpisode GetEpisodeOnly(Guid agencyId, Guid episodeId, Guid patientId)
       {
           return database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
       }
       public PatientEpisode GetNextEpisodeByStartDate(Guid agencyId, Guid patientId, DateTime startDate)
       {
           var script = @"SELECT
                            Id, 
                            PatientId,
                            EndDate,
                            StartDate, 
                            Details 
                                FROM
                                    patientepisodes 
                                        WHERE
                                            AgencyId = @agencyid  AND 
                                            PatientId = @patientid AND 
                                            IsActive = 1 AND
                                            IsDischarged = 0 AND
                                            DATE(StartDate) = DATE(@startdate) 
                                                 ORDER BY DATE(StartDate) ASC LIMIT 1";

           var episode = new PatientEpisode();
           using (var cmd = new FluentCommand<PatientEpisode>(script))
           {
               episode = cmd.SetConnection("AgencyManagementConnectionString")
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientid", patientId)
               .AddDateTime("startdate", startDate)
               .SetMap(reader => new PatientEpisode
               {
                   Id = reader.GetGuid("Id"),
                   PatientId = reader.GetGuid("PatientId"),
                   Details = reader.GetStringNullable("Details"),
                   StartDate = reader.GetDateTime("StartDate"),
                   EndDate = reader.GetDateTime("EndDate")
               })
               .AsSingle();
           }
           return episode;
       }
       public PatientEpisode GetLastEpisodeByEndDate(Guid agencyId, Guid patientId, DateTime endDate)
       {
           var script = @"SELECT 
                            Id, 
                            PatientId,
                            EndDate, 
                            StartDate, 
                            Details 
                                FROM 
                                    patientepisodes 
                                        WHERE 
                                            AgencyId = @agencyid  AND
                                            PatientId = @patientid AND
                                            IsActive = 1 AND 
                                            IsDischarged = 0 AND
                                            DATE(EndDate) = DATE(@enddate) 
                                                ORDER BY DATE(EndDate) DESC LIMIT 1";

           var episode = new PatientEpisode();
           using (var cmd = new FluentCommand<PatientEpisode>(script))
           {
               episode = cmd.SetConnection("AgencyManagementConnectionString")
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientid", patientId)
               .AddDateTime("enddate", endDate)
               .SetMap(reader => new PatientEpisode
               {
                   Id = reader.GetGuid("Id"),
                   PatientId = reader.GetGuid("PatientId"),
                   Details = reader.GetStringNullable("Details"),
                   StartDate = reader.GetDateTime("StartDate"),
                   EndDate = reader.GetDateTime("EndDate")
               })
               .AsSingle();
           }
           return episode;
       }
       //public PatientEpisode GetEpisodeNew(Guid agencyId, Guid patientId, DateTime date, string discipline)
       //{
       //    Check.Argument.IsNotEmpty(patientId, "patientId");
       //    Check.Argument.IsNotNull(discipline, "discipline");
       //    var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.StartDate);
       //    var episode = allEpisodes.Where(e => e.StartDate.Date <= date.Date).FirstOrDefault();
       //    if (episode != null)
       //    {
       //        var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < episode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
       //        var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > episode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
       //        episode.ScheduleEvents = this.GetPatientScheduledEventsNew(agencyId, episode.Id, episode.PatientId, discipline, false);
       //        if (episode.Details.IsNotNullOrEmpty())
       //        {
       //            episode.Detail = episode.Details.ToObject<EpisodeDetail>();
       //        }
       //        if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
       //        {
       //            episode.NextEpisode = nextEpisode;
       //        }
       //        if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
       //        {
       //            episode.PreviousEpisode = previousEpisode;
       //        }
       //    }
       //    else if (allEpisodes != null)
       //    {
       //        var futureEpisode = allEpisodes.Where(e => e.IsActive).LastOrDefault();
       //        if (futureEpisode != null)
       //        {
       //            var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < futureEpisode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
       //            var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > futureEpisode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
       //            futureEpisode.ScheduleEvents = this.GetPatientScheduledEventsNew(agencyId, futureEpisode.Id, futureEpisode.PatientId, discipline, false);
       //            if (futureEpisode.Details.IsNotNullOrEmpty())
       //            {
       //                futureEpisode.Detail = futureEpisode.Details.ToObject<EpisodeDetail>();
       //            }
       //            if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
       //            {
       //                futureEpisode.NextEpisode = nextEpisode;
       //            }
       //            if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
       //            {
       //                futureEpisode.PreviousEpisode = previousEpisode;
       //            }
       //            episode = futureEpisode;
       //        }
       //    }
       //    return episode;
       //}

       //public PatientEpisode GetEpisodeNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline)
       //{
       //    PatientEpisode episode = null;
       //    if (!patientId.IsEmpty() && !episodeId.IsEmpty() && discipline.IsNotNullOrEmpty())
       //    {
       //        try
       //        {
       //            var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate);
       //            if (allEpisodes != null)
       //            {
       //                episode = allEpisodes.Where(e => e.Id == episodeId).SingleOrDefault();
       //                if (episode != null)
       //                {
       //                    var previousEpisode = allEpisodes.Where(e => (e.StartDate < episode.StartDate) && e.IsActive == true).OrderByDescending(e => e.StartDate).FirstOrDefault();
       //                    var nextEpisode = allEpisodes.Where(e => (e.StartDate > episode.StartDate) && e.IsActive == true).OrderBy(e => e.StartDate).FirstOrDefault();
       //                    episode.ScheduleEvents = this.GetPatientScheduledEventsNew(agencyId, episodeId, patientId, discipline, false);
       //                    if (episode.Details.IsNotNullOrEmpty())
       //                    {
       //                        episode.Detail = episode.Details.ToObject<EpisodeDetail>();
       //                    }

       //                    if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
       //                    {
       //                        episode.NextEpisode = nextEpisode;

       //                    }
       //                    if (previousEpisode != null && !previousEpisode.Id.IsEmpty())
       //                    {
       //                        episode.PreviousEpisode = previousEpisode;
       //                    }
       //                }
       //            }
       //        }
       //        catch (Exception e)
       //        {
       //            return null;
       //        }
       //    }
       //    return episode;

       //}
       public PatientEpisode GetPatientEpisodeFluent(Guid agencyId, Guid episodeId, Guid patientId)
       {
           var script = @"SELECT
                        patientepisodes.Id as Id ,
                        patients.Id as PatientId,
                        patients.FirstName, 
                        patients.PatientIdNumber ,
                        patients.LastName,
                        patientepisodes.EndDate,
                        patientepisodes.StartDate 
                            FROM
                                patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id
                                    WHERE 
                                        patientepisodes.IsDischarged = 0 AND 
                                        patientepisodes.IsActive = 1 AND
                                        patientepisodes.AgencyId = @agencyid AND 
                                        patientepisodes.PatientId = @patientId AND
                                        patientepisodes.Id = @episodeId ";
           var patientEpisode = new PatientEpisode();
           using (var cmd = new FluentCommand<PatientEpisode>(script))
           {
               patientEpisode = cmd.SetConnection("AgencyManagementConnectionString")
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientid", patientId)
               .AddGuid("episodeId", episodeId)
               .SetMap(reader => new PatientEpisode
               {
                   Id = reader.GetGuid("Id"),
                   PatientId = reader.GetGuid("PatientId"),
                   StartDate = reader.GetDateTime("StartDate"),
                   EndDate = reader.GetDateTime("EndDate"),
                   DisplayName = string.Format("{0} ,{1}", reader.GetStringNullable("LastName"), reader.GetStringNullable("FirstName"))

               })
               .AsSingle();
           }
           return patientEpisode;
       }
       public PatientEpisode GetCurrentEpisode(Guid agencyId, Guid patientId)
       {
           PatientEpisode episode = null;
           if (!patientId.IsEmpty())
           {
               var allEpisodes = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.StartDate);
               episode = allEpisodes.Where(e => e.StartDate <= DateTime.Now).FirstOrDefault();
               if (episode != null)
               {
                   var previousEpisode = allEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                   var nextEpisode = allEpisodes.Where(e => e.StartDate > episode.StartDate).OrderBy(e => e.StartDate).FirstOrDefault();
                   if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                   {
                       episode.NextEpisode = nextEpisode;
                   }
                   if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                   {
                       episode.PreviousEpisode = previousEpisode;
                   }
               }
               else if (allEpisodes != null)
               {
                   var futureEpisode = allEpisodes.LastOrDefault();
                   if (futureEpisode != null)
                   {
                       var previousEpisode = allEpisodes.Where(e => e.StartDate < futureEpisode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                       var nextEpisode = allEpisodes.Where(e => e.StartDate > futureEpisode.StartDate).OrderBy(e => e.StartDate).FirstOrDefault();
                       if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                       {
                           futureEpisode.NextEpisode = nextEpisode;
                       }
                       if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                       {
                           futureEpisode.PreviousEpisode = previousEpisode;
                       }
                   }
                   episode = futureEpisode;
               }
           }
           return episode;
       }
       public List<PatientEpisode> GetPatientAllEpisodes(Guid agencyId, Guid patientId)
       {
           Check.Argument.IsNotEmpty(patientId, "patientId");
           return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).ToList();
       }
       public List<PatientEpisode> GetPatientActiveEpisodes(Guid agencyId, Guid patientId)
       {
           Check.Argument.IsNotEmpty(patientId, "patientId");
           return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).ToList();
       }
       public List<PatientEpisode> GetPatientAllEpisodesWithNoException(Guid agencyId, Guid patientId)
       {
           Check.Argument.IsNotEmpty(patientId, "patientId");
           return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId).ToList();
       }
       public List<PatientEpisode> EpisodesToDischarge(Guid agencyId, Guid patientId, DateTime dischargeDate)
       {
           return database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId).Where(e => e.StartDate.Date >= dischargeDate.Date).ToList();
       }


       public PatientEpisodeData GetCurrentEpisodeLean(Guid agencyId, Guid patientId)
       {
           var script = @"SELECT 
                            Id , 
                            AgencyId , 
                            PatientId , 
                            EndDate,
                            StartDate ,
                            Details  
                                FROM
                                    patientepisodes
                                        WHERE 
                                            AgencyId = @agencyid AND
                                            IsActive = 1 AND
                                            IsDischarged = 0 AND
                                            PatientId = @patientid AND
                                            DATE(curdate()) BETWEEN DATE(StartDate) AND DATE(EndDate)
                                                limit 0 , 1";

           var currentEpisode = new PatientEpisodeData();
           using (var cmd = new FluentCommand<PatientEpisodeData>(script))
           {
               currentEpisode = cmd.SetConnection("AgencyManagementConnectionString")
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientid", patientId)
               .SetMap(reader => new PatientEpisodeData
               {
                   Id = reader.GetGuid("Id"),
                   AgencyId = reader.GetGuid("AgencyId"),
                   PatientId = reader.GetGuid("PatientId"),
                   EndDate = reader.GetDateTime("EndDate"),
                   StartDate = reader.GetDateTime("StartDate"),
                   Details = reader.GetStringNullable("Details")
               })
               .AsSingle();
           }
           return currentEpisode;
       }

       public List<PatientEpisodeData> GetEpisodesBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
       {
           var list = new List<PatientEpisodeData>();
           var script = @"SELECT 
                            patientepisodes.Id as EpisodeId,
                            patients.Id as PatientId,
                            patients.FirstName as FirstName,
                            patients.LastName as LastName,
                            patients.Status as Status,
                            patients.DischargeDate as DischargeDate, 
                            patientepisodes.EndDate as EndDate,
                            patientepisodes.StartDate as StartDate
                                   FROM patientepisodes INNER JOIN patients ON patientepisodes.PatientId = patients.Id 
                                         WHERE  patients.Status IN (1,2) AND 
                                                patientepisodes.AgencyId = @agencyid AND 
                                                patients.IsDeprecated = 0 AND
                                                patients.Id = @patientid AND 
                                                patientepisodes.IsActive = 1 AND 
                                                patientepisodes.IsDischarged = 0 AND 
                                                (patientepisodes.StartDate BETWEEN @startdate AND @enddate || patientepisodes.EndDate BETWEEN @startdate AND @enddate || @startdate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || @enddate BETWEEN patientepisodes.StartDate AND patientepisodes.EndDate || (patientepisodes.StartDate BETWEEN @startdate AND @enddate AND patientepisodes.EndDate BETWEEN @startdate AND @enddate))
                                                        ORDER BY patientepisodes.StartDate ASC";

           using (var cmd = new FluentCommand<PatientEpisodeData>(script))
           {
               list = cmd.SetConnection("AgencyManagementConnectionString")
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientId", patientId)
               .AddDateTime("enddate", endDate)
               .AddDateTime("startdate", startDate)
               .SetMap(reader => new PatientEpisodeData
               {
                   Id = reader.GetGuid("EpisodeId"),
                   Status = reader.GetInt("Status"),
                   PatientId = reader.GetGuid("PatientId"),
                   PatientDischargeDate = reader.GetDateTimeNullable("DischargeDate"),
                   EndDate = reader.GetDateTime("EndDate"),
                   StartDate = reader.GetDateTime("StartDate"),
                   PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
               })
               .AsList();
           }
           return list.ToList();
       }

       public NewEpisodeData GetLastEpisode(Guid agencyId, Guid patientId)
       {
           NewEpisodeData newEpisodeDate = null;
           if (!patientId.IsEmpty())
           {
               var script = @"SELECT
                            patientepisodes.Id as Id ,
                            patients.Id as PatientId ,
                            patients.FirstName as FirstName ,
                            patients.LastName as LastName , 
                            patients.CaseManagerId as CaseManagerId ,
                            patients.PrimaryInsurance as PrimaryInsurance ,
                            patients.SecondaryInsurance as SecondaryInsurance , 
                            patientepisodes.EndDate as EndDate ,
                            patientepisodes.StartDate as StartDate , 
                            patientepisodes.StartOfCareDate as StartOfCareDate ,
                            patientepisodes.AdmissionId as AdmissionId  
                                FROM
                                    patientepisodes 
                                        INNER JOIN patients ON patients.Id = patientepisodes.PatientId 
                                            WHERE
                                                patientepisodes.AgencyId = @agencyid  AND
                                                patientepisodes.PatientId = @patientid AND 
                                                patientepisodes.IsActive = 1  AND 
                                                patientepisodes.IsDischarged = 0 
                                                    ORDER BY 
                                                        patientepisodes.StartDate DESC LIMIT 1 ";
               using (var cmd = new FluentCommand<NewEpisodeData>(script))
               {
                   newEpisodeDate = cmd.SetConnection("AgencyManagementConnectionString")
                   .AddGuid("agencyid", agencyId)
                   .AddGuid("patientid", patientId)
                   .SetMap(reader => new NewEpisodeData
                   {
                       PatientId = reader.GetGuid("PatientId"),
                       CaseManager = reader.GetStringNullable("CaseManagerId"),
                       PrimaryInsurance = reader.GetInt("PrimaryInsurance"),
                       SecondaryInsurance = reader.GetInt("SecondaryInsurance"),
                       EndDate = reader.GetDateTime("EndDate"),
                       StartDate = reader.GetDateTime("StartDate"),
                       DisplayName = reader.GetStringNullable("LastName").ToUpperCase() + ", " + reader.GetStringNullable("FirstName").ToUpperCase(),
                       StartOfCareDate = reader.GetDateTime("StartOfCareDate"),
                       AdmissionId = reader.GetGuid("AdmissionId")

                   })
                   .AsSingle();
               }
           }
           return newEpisodeDate;
       }

       public DateRange GetCurrentEpisodeDate(Guid agencyId, Guid patientId)
       {
           DateRange dateRange = new DateRange();
           if (!patientId.IsEmpty())
           {
               var episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.StartDate <= DateTime.Today && e.EndDate >= DateTime.Today && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate).FirstOrDefault();
               if (episode != null)
               {
                   dateRange.StartDate = (DateTime)episode.StartDate;
                   dateRange.EndDate = (DateTime)episode.EndDate;
               }
           }
           return dateRange;
       }

       public DateRange GetNextEpisode(Guid agencyId, Guid patientId)
       {
           var dateRange = new DateRange();
           if (!patientId.IsEmpty())
           {
               var episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.StartDate >= DateTime.Now && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate).FirstOrDefault();
               if (episode != null)
               {
                   dateRange.StartDate = episode.StartDate;
                   dateRange.EndDate = episode.EndDate;
               }
           }
           return dateRange;
       }

       public DateRange GetPreviousEpisode(Guid agencyId, Guid patientId)
       {
           DateRange dateRange = new DateRange();
           if (!patientId.IsEmpty())
           {
               var episode = database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.EndDate < DateTime.Now && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.EndDate).FirstOrDefault();
               if (episode != null)
               {
                   dateRange.StartDate = (DateTime)episode.StartDate;
                   dateRange.EndDate = (DateTime)episode.EndDate;
               }
           }
           return dateRange;
       }

       public List<EpisodeLean> GetPatientDeactivatedAndDischargedEpisodes(Guid agencyId, Guid patientId)
       {
           var script = @"SELECT
                        patientepisodes.Id,
                        patientepisodes.PatientId,
                        patientepisodes.IsActive,
                        patientepisodes.IsDischarged,
                        patientepisodes.EndDate, 
                        patientepisodes.StartDate 
                        FROM 
                            patientepisodes 
                                WHERE 
                                    patientepisodes.AgencyId = @agencyid AND
                                    patientepisodes.PatientId = @patientId AND
                                    (patientepisodes.IsDischarged = 1 || patientepisodes.IsActive = 0)";

           return new FluentCommand<EpisodeLean>(script)
               .SetConnection("AgencyManagementConnectionString")
               .AddGuid("agencyid", agencyId)
               .AddGuid("patientId", patientId)
               .SetMap(reader => new EpisodeLean
               {
                   Id = reader.GetGuid("Id"),
                   PatientId = reader.GetGuid("PatientId"),
                   IsActive = reader.GetBoolean("IsActive"),
                   IsDischarged = reader.GetBoolean("IsDischarged"),
                   EndDate = reader.GetDateTime("EndDate"),
                   StartDate = reader.GetDateTime("StartDate")
               })
               .AsList();
       }



       public bool IsEpisodeExist(Guid agencyId, Guid episodeId)
       {
           var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.Id == episodeId);
           if (episode != null)
           {
               return true;
           }
           return false;
       }

       public bool SetRecertFlag(Guid agencyId, Guid episodeId, Guid patientId, bool isRecertComplete)
       {
           var result = false;
           try
           {
               var episode = database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
               if (episode != null)
               {
                   episode.IsRecertCompleted = isRecertComplete;
                   episode.Modified = DateTime.Now;
                   database.Update<PatientEpisode>(episode);
                   result = true;
               }
           }
           catch (Exception ex)
           {
               return false;
           }
           return result;
       }

    }
}
