﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Enums;

    using SubSonic.Repository;
    using SubSonic.DataProviders;

    public class AgencyRepository : IAgencyRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public AgencyRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");

            this.database = database;
        }

        #endregion

        #region Agency Methods

        public bool ToggleDelete(Guid id, DateTime modified, int clusterId)
        {
            try
            {
                var sql = @"UPDATE
                              agencies as ag
                                SET 
                                    ag.IsDeprecated = !ag.IsDeprecated,
                                    ag.IsSuspended = !ag.IsSuspended,
                                    ag.Modified = @modified
                                         WHERE
                                              ag.Id = @id";


                using (var cmd = new FluentCommand<Agency>(sql))
                {
                    return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                         .AddDateTime("modified", modified)
                         .AddGuid("id", id)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool ToggleReadOnly(Guid id, DateTime readOnlyDate, bool isFrozen, DateTime modified, int clusterId)
        {
            try
            {
                var sql = @"UPDATE
                              agencies as ag
                                SET 
                                    ag.IsFrozen = @isFrozen,
                                    ag.FrozenDate = @readOnlyDate,
                                    ag.Modified = @modified
                                         WHERE
                                              ag.Id = @id";


                using (var cmd = new FluentCommand<Agency>(sql))
                {
                    return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                         .AddBoolean("isFrozen", isFrozen)
                         .AddDateTime("readOnlyDate", readOnlyDate)
                         .AddDateTime("modified", modified)
                         .AddGuid("id", id)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Agency Get(Guid id, int clusterId)
        {
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                return database.Single<Agency>(u =>  u.Id == id);
            }
//            var agency = new Agency();
//            var script = @"SELECT 
//                                ag.Id,
//                                ag.SalesPerson,
//                                ag.Trainer, 
//                                ag.BackupTrainer,
//                                ag.ImplementationSpecialist,
//                                ag.Name, 
//                                ag.TaxId,
//                                ag.TaxIdType,
//                                ag.NationalProviderNumber,
//                                ag.MedicareProviderNumber, 
//                                ag.MedicaidProviderNumber, 
//                                ag.HomeHealthAgencyId, 
//                                ag.SubmitterId, 
//                                ag.SubmitterName, 
//                                ag.SubmitterPhone, 
//                                ag.SubmitterFax, 
//                                ag.IsAgreementSigned, 
//                                ag.IsSuspended, 
//                                ag.TrialPeriod, 
//                                ag.Package, 
//                                ag.AnnualPlanId, 
//                                ag.ContactPersonFirstName, 
//                                ag.ContactPersonLastName, 
//                                ag.ContactPersonEmail, 
//                                ag.ContactPersonPhone, 
//                                ag.Payor, 
//                                ag.Comments, 
//                                CustomFields, 
//                                ag.Created, 
//                                ag.Modified, 
//                                ag.IsDeprecated, 
//                                ag.CahpsVendor, 
//                                ag.CahpsVendorId, 
//                                ag.CahpsSurveyDesignator, 
//                                ag.IsAxxessTheBiller, 
//                                ag.OasisAuditVendor, 
//                                ag.OasisAuditVendorApiKey, 
//                                ag.FrozenDate, 
//                                ag.IsFrozen, 
//                                ag.OasisAuditApiKey, 
//                                PreviousSoftware, 
//                                ag.Test, 
//                                ag.DBServerIp, 
//                                ag.Services, 
//                                ag.AccountId, 
//                                ag.MigrationStatus, 
//                                ag.ClusterId,
//                                al.AddressLine1,
//                                al.AddressLine2,
//                                al.AddressCity,  
//                                al.AddressStateCode,
//                                al.AddressZipCode,
//                                al.AddressZipCodeFour
//                                    FROM agencies as ag INNER JOIN agencylocations as al ON ag.Id = al.agencyId
//                                    WHERE ag.Id = @id";

          
//            using (var cmd = new FluentCommand<Agency>(script))
//            {
//                agency = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
//                .AddGuid("id", id)
//                .SetMap(reader => new Agency
//                {
//                    Id = reader.GetGuid("Id"),
//                    SalesPerson = reader.GetGuid("SalesPerson"),
//                    Trainer = reader.GetGuid("Trainer"),
//                    BackupTrainer = reader.GetGuid("BackupTrainer"),
//                    ImplementationSpecialist = reader.GetGuid("ImplementationSpecialist"),
//                    Name = reader.GetStringNullable("Name"),
//                    TaxId = reader.GetStringNullable("TaxId"),
//                    TaxIdType = reader.GetStringNullable("TaxIdType"),
//                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
//                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
//                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
//                    HomeHealthAgencyId = reader.GetStringNullable("HomeHealthAgencyId"),
//                    SubmitterId = reader.GetStringNullable("SubmitterId"),
//                    SubmitterName = reader.GetStringNullable("SubmitterName"),
//                    SubmitterPhone = reader.GetStringNullable("SubmitterPhone"),
//                    SubmitterFax = reader.GetStringNullable("SubmitterFax"),
//                    IsAgreementSigned = reader.GetBoolean("IsAgreementSigned"),
//                    IsSuspended = reader.GetBoolean("IsSuspended"),
//                    TrialPeriod = reader.GetInt("TrialPeriod"),
//                    Package = reader.GetInt("Package"),
//                    AnnualPlanId = reader.GetInt("AnnualPlanId"),
//                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
//                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
//                    ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
//                    ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone"),
//                    Payor = reader.GetStringNullable("Payor"),
//                    Comments = reader.GetStringNullable("Comments"),
//                    CustomFields = reader.GetStringNullable("CustomFields"),
//                    Created = reader.GetDateTime("Created"),
//                    Modified = reader.GetDateTime("Modified"),
//                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
//                    CahpsVendor = reader.GetInt("CahpsVendor"),
//                    CahpsVendorId = reader.GetStringNullable("CahpsVendorId"),
//                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator"),
//                    IsAxxessTheBiller = reader.GetBoolean("IsAxxessTheBiller"),
//                    OasisAuditVendor = reader.GetInt("OasisAuditVendor"),
//                    OasisAuditVendorApiKey = reader.GetStringNullable("OasisAuditVendorApiKey"),
//                    FrozenDate = reader.GetDateTime("FrozenDate"),
//                    IsFrozen = reader.GetBoolean("IsFrozen"),
//                    OasisAuditApiKey = reader.GetStringNullable("OasisAuditApiKey"),
//                    PreviousSoftware = reader.GetStringNullable("PreviousSoftware"),
//                    Test = reader.GetStringNullable("Test"),
//                    DBServerIp = reader.GetStringNullable("DBServerIp"),
//                    Services = (AgencyServices)reader.GetInt("Services"),
//                    AccountId = reader.GetStringNullable("AccountId"),
//                    MigrationStatus = reader.GetInt("MigrationStatus"),
//                    ClusterId = reader.GetInt("ClusterId"),
//                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
//                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
//                    AddressCity = reader.GetStringNullable("AddressCity"),
//                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
//                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
//                    AddressZipCodeFour = reader.GetStringNullable("AddressZipCodeFour")
//                })
//                .AsSingle();
//            }

            //return agency;
        }

        public Agency GetAgencyOnly(Guid id, int clusterId)
        {
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                return database.Single<Agency>(u => u.Id == id);
            }
//            var agency = new Agency();
//            var script = @"SELECT 
//                                ag.Id,
//                                ag.SalesPerson,
//                                ag.Trainer, 
//                                ag.BackupTrainer,
//                                ag.ImplementationSpecialist,
//                                ag.Name, 
//                                ag.TaxId,
//                                ag.TaxIdType,
//                                ag.NationalProviderNumber,
//                                ag.MedicareProviderNumber, 
//                                ag.MedicaidProviderNumber, 
//                                ag.HomeHealthAgencyId, 
//                                ag.SubmitterId, 
//                                ag.SubmitterName, 
//                                ag.SubmitterPhone, 
//                                ag.SubmitterFax, 
//                                ag.IsAgreementSigned, 
//                                ag.IsSuspended, 
//                                ag.TrialPeriod, 
//                                ag.Package, 
//                                ag.AnnualPlanId, 
//                                ag.ContactPersonFirstName, 
//                                ag.ContactPersonLastName, 
//                                ag.ContactPersonEmail, 
//                                ag.ContactPersonPhone, 
//                                ag.Payor, 
//                                ag.Comments, 
//                                ag.CustomFields, 
//                                ag.Created, 
//                                ag.Modified, 
//                                ag.IsDeprecated, 
//                                ag.CahpsVendor, 
//                                ag.CahpsVendorId, 
//                                ag.CahpsSurveyDesignator, 
//                                ag.IsAxxessTheBiller, 
//                                ag.OasisAuditVendor, 
//                                ag.OasisAuditVendorApiKey, 
//                                ag.FrozenDate, 
//                                ag.IsFrozen, 
//                                ag.OasisAuditApiKey, 
//                                ag.PreviousSoftware, 
//                                ag.Test, 
//                                ag.DBServerIp, 
//                                ag.Services, 
//                                ag.AccountId, 
//                                ag.MigrationStatus, 
//                                ag.ClusterId
//                                    FROM agencies as ag
//                                     WHERE ag.Id = @id";
//            using (var cmd = new FluentCommand<Agency>(script))
//            {
//                agency = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
//                .AddGuid("id", id)
//                .SetMap(reader => new Agency
//                {
//                    Id = reader.GetGuid("Id"),
//                    SalesPerson = reader.GetGuid("SalesPerson"),
//                    Trainer = reader.GetGuid("Trainer"),
//                    BackupTrainer = reader.GetGuid("BackupTrainer"),
//                    ImplementationSpecialist = reader.GetGuid("ImplementationSpecialist"),
//                    Name = reader.GetStringNullable("Name"),
//                    TaxId = reader.GetStringNullable("TaxId"),
//                    TaxIdType = reader.GetStringNullable("TaxIdType"),
//                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
//                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
//                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
//                    HomeHealthAgencyId = reader.GetStringNullable("HomeHealthAgencyId"),
//                    SubmitterId = reader.GetStringNullable("SubmitterId"),
//                    SubmitterName = reader.GetStringNullable("SubmitterName"),
//                    SubmitterPhone = reader.GetStringNullable("SubmitterPhone"),
//                    SubmitterFax = reader.GetStringNullable("SubmitterFax"),
//                    IsAgreementSigned = reader.GetBoolean("IsAgreementSigned"),
//                    IsSuspended = reader.GetBoolean("IsSuspended"),
//                    TrialPeriod = reader.GetInt("TrialPeriod"),
//                    Package = reader.GetInt("Package"),
//                    AnnualPlanId = reader.GetInt("AnnualPlanId"),
//                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
//                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
//                    ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
//                    ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone"),
//                    Payor = reader.GetStringNullable("Payor"),
//                    Comments = reader.GetStringNullable("Comments"),
//                    CustomFields = reader.GetStringNullable("CustomFields"),
//                    Created = reader.GetDateTime("Created"),
//                    Modified = reader.GetDateTime("Modified"),
//                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
//                    CahpsVendor = reader.GetInt("CahpsVendor"),
//                    CahpsVendorId = reader.GetStringNullable("CahpsVendorId"),
//                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator"),
//                    IsAxxessTheBiller = reader.GetBoolean("IsAxxessTheBiller"),
//                    OasisAuditVendor = reader.GetInt("OasisAuditVendor"),
//                    OasisAuditVendorApiKey = reader.GetStringNullable("OasisAuditVendorApiKey"),
//                    FrozenDate = reader.GetDateTime("FrozenDate"),
//                    IsFrozen = reader.GetBoolean("IsFrozen"),
//                    OasisAuditApiKey = reader.GetStringNullable("OasisAuditApiKey"),
//                    PreviousSoftware = reader.GetStringNullable("PreviousSoftware"),
//                    Test = reader.GetStringNullable("Test"),
//                    DBServerIp = reader.GetStringNullable("DBServerIp"),
//                    Services = (AgencyServices)reader.GetInt("Services"),
//                    AccountId = reader.GetStringNullable("AccountId"),
//                    MigrationStatus = reader.GetInt("MigrationStatus"),
//                    ClusterId = reader.GetInt("ClusterId")
//                })
//                .AsSingle();
//            }

//            return agency;
        }

        public AgencyLocation GetMainLocation(Guid agencyId, int clusterId)
        {
            using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
            {
                return database.Single<AgencyLocation>(u => u.AgencyId == agencyId && u.IsDeprecated == false && u.IsMainOffice == true);
            }
//            var agencyLocation = new AgencyLocation();
//            var script = @"SELECT 
//                                Id, 
//                                AgencyId, 
//                                Name, 
//                                FullName,
//                                CustomId, 
//                                CBSA,
//                                MedicareProviderNumber,
//                                AddressLine1,
//                                AddressLine2,
//                                AddressCity,
//                                AddressStateCode, 
//                                AddressZipCode,
//                                AddressZipCodeFour,
//                                ZipNineStatus,
//                                PhoneWork,
//                                FaxNumber,
//                                Comments, 
//                                IsMainOffice, 
//                                Cost,
//                                BillData, 
//                                IsDeprecated,
//                                IsSubmitterInfoTheSame, 
//                                SubmitterId, 
//                                SubmitterName, 
//                                SubmitterPhone, 
//                                SubmitterFax,
//                                Payor, 
//                                BranchId, 
//                                BranchIdOther, 
//                                Ub04Locator81cca, 
//                                TaxId,
//                                TaxIdType,
//                                NationalProviderNumber, 
//                                MedicaidProviderNumber,
//                                HomeHealthAgencyId, 
//                                ContactPersonFirstName, 
//                                ContactPersonLastName, 
//                                ContactPersonEmail, 
//                                ContactPersonPhone, 
//                                CahpsVendor, 
//                                CahpsVendorId, 
//                                CahpsSurveyDesignator,
//                                IsAxxessTheBiller, 
//                                OasisAuditVendor, 
//                                IsLocationStandAlone, 
//                                Modified, 
//                                Created
//                                    FROM agencylocations as al
//                                        WHERE al.agencyId = @agencyId AND al.IsDeprecated=0 AND al.IsMainOffice=1";
//            using (var cmd = new FluentCommand<AgencyLocation>(script))
//            {
//                agencyLocation = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
//                .AddGuid("agencyId", agencyId)
//                .SetMap(reader => new AgencyLocation
//                {
//                    Id = reader.GetGuid("Id"),
//                    AgencyId = reader.GetGuid("AgencyId"),
//                    Name = reader.GetStringNullable("Name"),
//                    FullName = reader.GetStringNullable("FullName"),
//                    CustomId = reader.GetStringNullable("CustomId"),
//                    CBSA = reader.GetStringNullable("CBSA"),
//                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
//                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
//                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
//                    AddressCity = reader.GetStringNullable("AddressCity"),
//                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
//                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
//                    AddressZipCodeFour = reader.GetStringNullable("AddressZipCodeFour"),
//                    ZipNineStatus = reader.GetStringNullable("ZipNineStatus"),
//                    PhoneWork = reader.GetStringNullable("PhoneWork"),
//                    FaxNumber = reader.GetStringNullable("FaxNumber"),
//                    Comments = reader.GetStringNullable("Comments"),
//                    IsMainOffice = reader.GetBoolean("IsMainOffice"),
//                    Cost = reader.GetStringNullable("Cost"),
//                    BillData = reader.GetStringNullable("BillData"),
//                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
//                    IsSubmitterInfoTheSame = reader.GetBoolean("IsSubmitterInfoTheSame"),
//                    SubmitterId = reader.GetStringNullable("SubmitterId"),
//                    SubmitterName = reader.GetStringNullable("SubmitterName"),
//                    SubmitterPhone = reader.GetStringNullable("SubmitterPhone"),
//                    SubmitterFax = reader.GetStringNullable("SubmitterFax"),
//                    Payor = reader.GetStringNullable("Payor"),
//                    BranchId = reader.GetStringNullable("BranchId"),
//                    BranchIdOther = reader.GetStringNullable("BranchIdOther"),
//                    Ub04Locator81cca = reader.GetStringNullable("Ub04Locator81cca"),
//                    TaxId = reader.GetStringNullable("TaxId"),
//                    TaxIdType = reader.GetStringNullable("TaxIdType"),
//                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
//                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
//                    HomeHealthAgencyId = reader.GetStringNullable("HomeHealthAgencyId"),
//                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
//                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
//                    ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone"),
//                    ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
//                    CahpsVendor = reader.GetInt("CahpsVendor"),
//                    CahpsVendorId = reader.GetStringNullable("CahpsVendorId"),
//                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator"),
//                    IsAxxessTheBiller = reader.GetBoolean("IsAxxessTheBiller"),
//                    OasisAuditVendor = reader.GetInt("OasisAuditVendor"),
//                    IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
//                    Modified = reader.GetDateTime("Modified"),
//                    Created = reader.GetDateTime("Created")
//                })
//                .AsSingle();
//            }

//            return agencyLocation;
        }

        public IList<AgencyLocation> GetBranches(Guid agencyId, int clusterId)
        {
            var agencyLocation = new List<AgencyLocation>();
            var script = @"SELECT 
                                Id, 
                                AgencyId, 
                                Name, 
                                FullName,
                                CustomId, 
                                CBSA,
                                MedicareProviderNumber,
                                AddressLine1,
                                AddressLine2,
                                AddressCity,
                                AddressStateCode, 
                                AddressZipCode,
                                AddressZipCodeFour,
                                ZipNineStatus,
                                PhoneWork,
                                FaxNumber,
                                Comments, 
                                IsMainOffice, 
                                Cost,
                                BillData, 
                                IsDeprecated,
                                IsSubmitterInfoTheSame, 
                                SubmitterId, 
                                SubmitterName, 
                                SubmitterPhone, 
                                SubmitterFax,
                                Payor, 
                                BranchId, 
                                BranchIdOther, 
                                Ub04Locator81cca, 
                                TaxId,
                                TaxIdType,
                                NationalProviderNumber, 
                                MedicaidProviderNumber,
                                HomeHealthAgencyId, 
                                ContactPersonFirstName, 
                                ContactPersonLastName, 
                                ContactPersonEmail, 
                                ContactPersonPhone, 
                                CahpsVendor, 
                                CahpsVendorId, 
                                CahpsSurveyDesignator,
                                IsAxxessTheBiller, 
                                OasisAuditVendor, 
                                IsLocationStandAlone, 
                                Modified, 
                                Created
                                    FROM agencylocations as al
                                        WHERE al.agencyId = @agencyId AND al.IsDeprecated=0 
                                            ORDER BY al.Created";
            using (var cmd = new FluentCommand<AgencyLocation>(script))
            {
                agencyLocation = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("agencyId", agencyId)
                .SetMap(reader => new AgencyLocation
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Name = reader.GetStringNullable("Name"),
                    FullName = reader.GetStringNullable("FullName"),
                    CustomId = reader.GetStringNullable("CustomId"),
                    CBSA = reader.GetStringNullable("CBSA"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    AddressZipCodeFour = reader.GetStringNullable("AddressZipCodeFour"),
                    ZipNineStatus = reader.GetStringNullable("ZipNineStatus"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    FaxNumber = reader.GetStringNullable("FaxNumber"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsMainOffice = reader.GetBoolean("IsMainOffice"),
                    Cost = reader.GetStringNullable("Cost"),
                    BillData = reader.GetStringNullable("BillData"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsSubmitterInfoTheSame = reader.GetBoolean("IsSubmitterInfoTheSame"),
                    SubmitterId = reader.GetStringNullable("SubmitterId"),
                    SubmitterName = reader.GetStringNullable("SubmitterName"),
                    SubmitterPhone = reader.GetStringNullable("SubmitterPhone"),
                    SubmitterFax = reader.GetStringNullable("SubmitterFax"),
                    Payor = reader.GetStringNullable("Payor"),
                    BranchId = reader.GetStringNullable("BranchId"),
                    BranchIdOther = reader.GetStringNullable("BranchIdOther"),
                    Ub04Locator81cca = reader.GetStringNullable("Ub04Locator81cca"),
                    TaxId = reader.GetStringNullable("TaxId"),
                    TaxIdType = reader.GetStringNullable("TaxIdType"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    HomeHealthAgencyId = reader.GetStringNullable("HomeHealthAgencyId"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
                    ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone"),
                    ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    CahpsVendorId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator"),
                    IsAxxessTheBiller = reader.GetBoolean("IsAxxessTheBiller"),
                    OasisAuditVendor = reader.GetInt("OasisAuditVendor"),
                    IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                    Modified = reader.GetDateTime("Modified"),
                    Created = reader.GetDateTime("Created")
                })
                .AsList();
            }

            return agencyLocation;
        }

        public IList<AgencyLocation> GetBranchList(Guid agencyId, int clusterId)
        {
            var list = new List<AgencyLocation>();
            var sql = new StringBuilder(@"SELECT
                                agencylocations.Id,
                                agencylocations.Name,
                                agencylocations.AgencyId as AgencyId,
                                agencylocations.FaxNumber, 
                                agencylocations.PhoneWork, 
                                agencylocations.AddressLine1, 
                                agencylocations.AddressLine2,
                                agencylocations.AddressCity,
                                agencylocations.AddressStateCode, 
                                agencylocations.AddressZipCode,
                                agencylocations.MedicareProviderNumber
                                    FROM 
                                        agencies 
                                            INNER JOIN agencylocations on agencies.Id = agencylocations.AgencyId 
                                            WHERE agencies.Id = @agencyid;").ToString();
            using (var cmd = new FluentCommand<AgencyLocation>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new AgencyLocation
                    {
                        Id = reader.GetGuid("Id"),
                        Name = reader.GetString("Name"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        FaxNumber = reader.GetStringNullable("FaxNumber"),
                        PhoneWork = reader.GetStringNullable("PhoneWork"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    }).AsList();
            }
            return list;
        }

        public AgencyLocation FindLocation(Guid agencyId, Guid Id, int clusterId)
        {
            var agencyLocation = new AgencyLocation();
            var script = @"SELECT 
                                Id, 
                                AgencyId, 
                                Name, 
                                FullName,
                                CustomId, 
                                CBSA,
                                MedicareProviderNumber,
                                AddressLine1,
                                AddressLine2,
                                AddressCity,
                                AddressStateCode, 
                                AddressZipCode,
                                AddressZipCodeFour,
                                ZipNineStatus,
                                PhoneWork,
                                FaxNumber,
                                Comments, 
                                IsMainOffice, 
                                Cost,
                                BillData, 
                                IsDeprecated,
                                IsSubmitterInfoTheSame, 
                                SubmitterId, 
                                SubmitterName, 
                                SubmitterPhone, 
                                SubmitterFax,
                                Payor, 
                                BranchId, 
                                BranchIdOther, 
                                Ub04Locator81cca, 
                                TaxId,
                                TaxIdType,
                                NationalProviderNumber, 
                                MedicaidProviderNumber,
                                HomeHealthAgencyId, 
                                ContactPersonFirstName, 
                                ContactPersonLastName, 
                                ContactPersonEmail, 
                                ContactPersonPhone, 
                                CahpsVendor, 
                                CahpsVendorId, 
                                CahpsSurveyDesignator,
                                IsAxxessTheBiller, 
                                OasisAuditVendor, 
                                IsLocationStandAlone, 
                                Modified, 
                                Created
                                    FROM
                                            agencylocations as al
                                                    WHERE al.Id = @id AND al.agencyId = @agencyId";
            using (var cmd = new FluentCommand<AgencyLocation>(script))
            {
                agencyLocation = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("Id", Id)
                .AddGuid("agencyId", agencyId)
                .SetMap(reader => new AgencyLocation
                {
                    Id = reader.GetGuid("Id"),
                    AgencyId = reader.GetGuid("AgencyId"),
                    Name = reader.GetStringNullable("Name"),
                    FullName = reader.GetStringNullable("FullName"),
                    CustomId = reader.GetStringNullable("CustomId"),
                    CBSA = reader.GetStringNullable("CBSA"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    AddressLine1 = reader.GetStringNullable("AddressLine1"),
                    AddressLine2 = reader.GetStringNullable("AddressLine2"),
                    AddressCity = reader.GetStringNullable("AddressCity"),
                    AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                    AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                    AddressZipCodeFour = reader.GetStringNullable("AddressZipCodeFour"),
                    ZipNineStatus = reader.GetStringNullable("ZipNineStatus"),
                    PhoneWork = reader.GetStringNullable("PhoneWork"),
                    FaxNumber = reader.GetStringNullable("FaxNumber"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsMainOffice = reader.GetBoolean("IsMainOffice"),
                    Cost = reader.GetStringNullable("Cost"),
                    BillData = reader.GetStringNullable("BillData"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsSubmitterInfoTheSame = reader.GetBoolean("IsSubmitterInfoTheSame"),
                    SubmitterId = reader.GetStringNullable("SubmitterId"),
                    SubmitterName = reader.GetStringNullable("SubmitterName"),
                    SubmitterPhone = reader.GetStringNullable("SubmitterPhone"),
                    SubmitterFax = reader.GetStringNullable("SubmitterFax"),
                    Payor = reader.GetStringNullable("Payor"),
                    BranchId = reader.GetStringNullable("BranchId"),
                    BranchIdOther = reader.GetStringNullable("BranchIdOther"),
                    Ub04Locator81cca = reader.GetStringNullable("Ub04Locator81cca"),
                    TaxId = reader.GetStringNullable("TaxId"),
                    TaxIdType = reader.GetStringNullable("TaxIdType"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    HomeHealthAgencyId = reader.GetStringNullable("HomeHealthAgencyId"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
                    ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone"),
                    ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    CahpsVendorId = reader.GetStringNullable("CahpsVendorId"),
                    CahpsSurveyDesignator = reader.GetStringNullable("CahpsSurveyDesignator"),
                    IsAxxessTheBiller = reader.GetBoolean("IsAxxessTheBiller"),
                    OasisAuditVendor = reader.GetInt("OasisAuditVendor"),
                    IsLocationStandAlone = reader.GetBoolean("IsLocationStandAlone"),
                    Modified = reader.GetDateTime("Modified"),
                    Created = reader.GetDateTime("Created")
                })
                .AsSingle();
            }

            return agencyLocation;
        }

        public List<LocationInfo> GetLocationNameWithAgencyName(List<Guid> locationIds, int clusterId)
        {
            var list = new List<LocationInfo>();
            try
            {
                if (locationIds != null && locationIds.Count > 0)
                {
                    var sql = new StringBuilder(@"SELECT 
                                agencies.Name as AgencyName, 
                                agencylocations.Name as LocationName, 
                                agencies.Id as AgencyId, 
                                agencylocations.Id as LocationId
                                    from 
                                        agencies 
                                            inner join agencylocations on agencies.Id = agencylocations.AgencyId ")
                                                                .AppendFormat("where agencylocations.Id IN({0})", locationIds.ToCommaSeperatedList())
                                                                .ToString();

                    using (var cmd = new FluentCommand<LocationInfo>(sql))
                    {
                        list = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                            .SetMap(reader => new LocationInfo
                            {
                                AgencyId = reader.GetGuid("AgencyId"),
                                Id = reader.GetGuid("LocationId"),
                                AgencyName = reader.GetString("AgencyName"),
                                Name = reader.GetStringNullable("LocationName")
                            })
                            .AsList();
                    }
                    //}
                    // }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return list;
        }


        public bool Add(Agency agency, int clusterId)
        {
            if (agency != null)
            {
                var AgencyServices = (int)agency.Services;
                try
                {
                    agency.Created = DateTime.Now;
                    agency.Modified = DateTime.Now;
                    agency.OasisAuditVendor = (int)OasisAuditVendors.HHG;
                    agency.OasisAuditVendorApiKey = agency.OasisAuditVendorApiKey != null ? agency.OasisAuditVendorApiKey : string.Empty;

                    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                    {
                        database.Add<Agency>(agency);
                        return true;
                    }
                    //                    var script = @"INSERT INTO 
                    //                                    agencies
                    //                                        (
                    //                                            Id,
                    //                                            SalesPerson,
                    //                                            Trainer, 
                    //                                            BackupTrainer,
                    //                                            ImplementationSpecialist,
                    //                                            Name, 
                    //                                            TaxId,
                    //                                            TaxIdType,
                    //                                            NationalProviderNumber,
                    //                                            MedicareProviderNumber, 
                    //                                            MedicaidProviderNumber, 
                    //                                            HomeHealthAgencyId, 
                    //                                            SubmitterId, 
                    //                                            SubmitterName, 
                    //                                            SubmitterPhone, 
                    //                                            SubmitterFax, 
                    //                                            IsAgreementSigned, 
                    //                                            IsSuspended, 
                    //                                            TrialPeriod, 
                    //                                            Package, 
                    //                                            AnnualPlanId, 
                    //                                            ContactPersonFirstName, 
                    //                                            ContactPersonLastName, 
                    //                                            ContactPersonEmail, 
                    //                                            ContactPersonPhone, 
                    //                                            Payor, 
                    //                                            Comments, 
                    //                                            CustomFields, 
                    //                                            Created, 
                    //                                            Modified, 
                    //                                            IsDeprecated, 
                    //                                            CahpsVendor, 
                    //                                            CahpsVendorId, 
                    //                                            CahpsSurveyDesignator, 
                    //                                            IsAxxessTheBiller, 
                    //                                            OasisAuditVendor, 
                    //                                            OasisAuditVendorApiKey, 
                    //                                            FrozenDate, 
                    //                                            IsFrozen, 
                    //                                            PreviousSoftware, 
                    //                                            Services, 
                    //                                            AccountId, 
                    //                                            MigrationStatus, 
                    //                                            ClusterId
                    //                                          ) 
                    //                                          VALUES 
                    //                                          (
                    //                                            @Id,
                    //                                            @SalesPerson,
                    //                                            @Trainer, 
                    //                                            @BackupTrainer,
                    //                                            @ImplementationSpecialist,
                    //                                            @Name, 
                    //                                            @TaxId,
                    //                                            @TaxIdType,
                    //                                            @NationalProviderNumber,
                    //                                            @MedicareProviderNumber, 
                    //                                            @MedicaidProviderNumber, 
                    //                                            @HomeHealthAgencyId, 
                    //                                            @SubmitterId, 
                    //                                            @SubmitterName, 
                    //                                            @SubmitterPhone, 
                    //                                            @SubmitterFax, 
                    //                                            @IsAgreementSigned, 
                    //                                            @IsSuspended, 
                    //                                            @TrialPeriod, 
                    //                                            @Package, 
                    //                                            @AnnualPlanId, 
                    //                                            @ContactPersonFirstName, 
                    //                                            @ContactPersonLastName, 
                    //                                            @ContactPersonEmail, 
                    //                                            @ContactPersonPhone, 
                    //                                            @Payor, 
                    //                                            @Comments, 
                    //                                            @CustomFields, 
                    //                                            @Created, 
                    //                                            @Modified, 
                    //                                            @IsDeprecated, 
                    //                                            @CahpsVendor, 
                    //                                            @CahpsVendorId, 
                    //                                            @CahpsSurveyDesignator, 
                    //                                            @IsAxxessTheBiller, 
                    //                                            @OasisAuditVendor, 
                    //                                            @OasisAuditVendorApiKey, 
                    //                                            @FrozenDate, 
                    //                                            @IsFrozen, 
                    //                                            @PreviousSoftware, 
                    //                                            @Services, 
                    //                                            @AccountId, 
                    //                                            @MigrationStatus, 
                    //                                            @ClusterId
                    //                                          );";
                    //                    using (var cmd = new FluentCommand<Agency>(script))
                    //                    {
                    //                        return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    //                        .AddGuid("Id", agency.Id)
                    //                        .AddGuid("SalesPerson", agency.SalesPerson)
                    //                        .AddGuid("Trainer", agency.Trainer)
                    //                        .AddGuid("BackupTrainer", agency.BackupTrainer)
                    //                        .AddGuid("ImplementationSpecialist", agency.ImplementationSpecialist)
                    //                        .AddString("Name", agency.Name)
                    //                        .AddString("TaxId", agency.TaxId)
                    //                        .AddString("TaxIdType", agency.TaxIdType)
                    //                        .AddString("NationalProviderNumber", agency.NationalProviderNumber)
                    //                        .AddString("MedicareProviderNumber", agency.MedicareProviderNumber)
                    //                        .AddString("MedicaidProviderNumber", agency.MedicaidProviderNumber)
                    //                        .AddString("HomeHealthAgencyId", agency.HomeHealthAgencyId)
                    //                        .AddString("SubmitterId", agency.SubmitterId)
                    //                        .AddString("SubmitterName", agency.SubmitterName)
                    //                        .AddString("SubmitterPhone", agency.SubmitterPhone)
                    //                        .AddString("SubmitterFax", agency.SubmitterFax)
                    //                        .AddBoolean("IsAgreementSigned", agency.IsAgreementSigned)
                    //                        .AddBoolean("IsSuspended", agency.IsSuspended)
                    //                        .AddInt("TrialPeriod", agency.TrialPeriod)
                    //                        .AddInt("Package", agency.Package)
                    //                        .AddInt("AnnualPlanId", agency.AnnualPlanId)
                    //                        .AddString("ContactPersonFirstName", agency.ContactPersonFirstName)
                    //                        .AddString("ContactPersonLastName", agency.ContactPersonLastName)
                    //                        .AddString("ContactPersonEmail", agency.ContactPersonEmail)
                    //                        .AddString("ContactPersonPhone", agency.ContactPersonPhone)
                    //                        .AddString("Payor", agency.Payor)
                    //                        .AddString("Comments", agency.Comments)
                    //                        .AddString("CustomFields", agency.CustomFields)
                    //                        .AddDateTime("Created", DateTime.Now)
                    //                        .AddDateTime("Modified", DateTime.Now)
                    //                        .AddBoolean("IsDeprecated", agency.IsDeprecated)
                    //                        .AddInt("CahpsVendor", agency.CahpsVendor)
                    //                        .AddString("CahpsVendorId", agency.CahpsVendorId)
                    //                        .AddString("CahpsSurveyDesignator", agency.CahpsSurveyDesignator)
                    //                        .AddBoolean("IsAxxessTheBiller", agency.IsAxxessTheBiller)
                    //                        .AddInt("OasisAuditVendor", (int)OasisAuditVendors.HHG)
                    //                        .AddString("OasisAuditVendorApiKey", agency.OasisAuditVendorApiKey != null ? agency.OasisAuditVendorApiKey : string.Empty)
                    //                        .AddDateTime("FrozenDate", agency.FrozenDate)
                    //                        .AddBoolean("IsFrozen", agency.IsFrozen)
                    //                        .AddString("PreviousSoftware", agency.PreviousSoftware)
                    //                        .AddInt("Services", AgencyServices)
                    //                        .AddString("AccountId", agency.AccountId)
                    //                            //.AddInt("MigrationStatus", agency.MigrationStatus)
                    //                        .AddInt("ClusterId", agency.ClusterId)
                    //                        .AsNonQuery() > 0;
                    //                    }
                }

                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool AddLocation(AgencyLocation agencyLocation, int clusterId)
        {
            if (agencyLocation != null)
            {
                if (agencyLocation.PhoneArray != null && agencyLocation.PhoneArray.Count > 0)
                {
                    agencyLocation.PhoneWork = agencyLocation.PhoneArray.ToArray().PhoneEncode();
                }
                if (agencyLocation.FaxNumberArray != null && agencyLocation.FaxNumberArray.Count > 0)
                {
                    agencyLocation.FaxNumber = agencyLocation.FaxNumberArray.ToArray().PhoneEncode();
                }

                agencyLocation.Created = DateTime.Now;
                agencyLocation.Modified = DateTime.Now;

                if (agencyLocation != null)
                {

                        using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                        {
                            database.Add<AgencyLocation>(agencyLocation);
                            return true;
                        }

                        //                        var script = @"INSERT INTO 
                        //                                        agencylocations
                        //                                            (
                        //                                                Id, 
                        //                                                AgencyId, 
                        //                                                Name, 
                        //                                                FullName,
                        //                                                CustomId, 
                        //                                                CBSA,
                        //                                                MedicareProviderNumber,
                        //                                                AddressLine1,
                        //                                                AddressLine2,
                        //                                                AddressCity,
                        //                                                AddressStateCode, 
                        //                                                AddressZipCode,
                        //                                                AddressZipCodeFour,
                        //                                                ZipNineStatus,
                        //                                                PhoneWork,
                        //                                                FaxNumber,
                        //                                                Comments, 
                        //                                                IsMainOffice, 
                        //                                                Cost,
                        //                                                BillData, 
                        //                                                IsDeprecated,
                        //                                                IsSubmitterInfoTheSame, 
                        //                                                SubmitterId, 
                        //                                                SubmitterName, 
                        //                                                SubmitterPhone, 
                        //                                                SubmitterFax,
                        //                                                Payor, 
                        //                                                BranchId, 
                        //                                                BranchIdOther, 
                        //                                                Ub04Locator81cca, 
                        //                                                TaxId,
                        //                                                TaxIdType,
                        //                                                NationalProviderNumber, 
                        //                                                MedicaidProviderNumber,
                        //                                                HomeHealthAgencyId, 
                        //                                                ContactPersonFirstName, 
                        //                                                ContactPersonLastName, 
                        //                                                ContactPersonEmail, 
                        //                                                ContactPersonPhone, 
                        //                                                CahpsVendor, 
                        //                                                CahpsVendorId, 
                        //                                                CahpsSurveyDesignator,
                        //                                                IsAxxessTheBiller, 
                        //                                                OasisAuditVendor, 
                        //                                                IsLocationStandAlone, 
                        //                                                Modified, 
                        //                                                Created
                        //                                          ) 
                        //                                          VALUES 
                        //                                          (
                        //                                            @Id, 
                        //                                            @AgencyId, 
                        //                                            @Name, 
                        //                                            @FullName,
                        //                                            @CustomId, 
                        //                                            @CBSA,
                        //                                            @MedicareProviderNumber,
                        //                                            @AddressLine1,
                        //                                            @AddressLine2,
                        //                                            @AddressCity,
                        //                                            @AddressStateCode, 
                        //                                            @AddressZipCode,
                        //                                            @AddressZipCodeFour,
                        //                                            @ZipNineStatus,
                        //                                            @PhoneWork,
                        //                                            @FaxNumber,
                        //                                            @Comments, 
                        //                                            @IsMainOffice, 
                        //                                            @Cost,
                        //                                            @BillData, 
                        //                                            @IsDeprecated,
                        //                                            @IsSubmitterInfoTheSame, 
                        //                                            @SubmitterId, 
                        //                                            @SubmitterName, 
                        //                                            @SubmitterPhone, 
                        //                                            @SubmitterFax,
                        //                                            @Payor, 
                        //                                            @BranchId, 
                        //                                            @BranchIdOther, 
                        //                                            @Ub04Locator81cca, 
                        //                                            @TaxId,
                        //                                            @TaxIdType,
                        //                                            @NationalProviderNumber, 
                        //                                            @MedicaidProviderNumber,
                        //                                            @HomeHealthAgencyId, 
                        //                                            @ContactPersonFirstName, 
                        //                                            @ContactPersonLastName, 
                        //                                            @ContactPersonEmail, 
                        //                                            @ContactPersonPhone, 
                        //                                            @CahpsVendor, 
                        //                                            @CahpsVendorId, 
                        //                                            @CahpsSurveyDesignator,
                        //                                            @IsAxxessTheBiller, 
                        //                                            @OasisAuditVendor, 
                        //                                            @IsLocationStandAlone, 
                        //                                            @Modified, 
                        //                                            @Created
                        //                                          );";
                        //                        using (var cmd = new FluentCommand<Agency>(script))
                        //                        {
                        //                            return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                        //                            .AddGuid("Id", agencyLocation.Id)
                        //                            .AddGuid("AgencyId", agencyLocation.AgencyId)
                        //                            .AddString("Name", agencyLocation.Name)
                        //                            .AddString("FullName", agencyLocation.FullName)
                        //                            .AddString("CustomId", agencyLocation.CustomId)
                        //                            .AddString("CBSA", agencyLocation.CBSA)
                        //                            .AddString("MedicareProviderNumber", agencyLocation.MedicareProviderNumber)
                        //                            .AddString("AddressLine1", agencyLocation.AddressLine1)
                        //                            .AddString("AddressLine2", agencyLocation.AddressLine2)
                        //                            .AddString("AddressCity", agencyLocation.AddressCity)
                        //                            .AddString("AddressStateCode", agencyLocation.AddressStateCode)
                        //                            .AddString("AddressZipCode", agencyLocation.AddressZipCode)
                        //                            .AddString("AddressZipCodeFour", agencyLocation.AddressZipCodeFour)
                        //                            .AddString("ZipNineStatus", agencyLocation.ZipNineStatus)
                        //                            .AddString("PhoneWork", agencyLocation.PhoneWork)
                        //                            .AddString("FaxNumber", agencyLocation.FaxNumber)
                        //                            .AddString("Comments", agencyLocation.Comments)
                        //                            .AddBoolean("IsMainOffice", agencyLocation.IsMainOffice)
                        //                            .AddString("Cost", agencyLocation.Cost)
                        //                            .AddString("BillData", agencyLocation.BillData)
                        //                            .AddBoolean("IsDeprecated", agencyLocation.IsDeprecated)
                        //                            .AddBoolean("IsSubmitterInfoTheSame", agencyLocation.IsSubmitterInfoTheSame)
                        //                            .AddString("SubmitterId", agencyLocation.SubmitterId)
                        //                            .AddString("SubmitterName", agencyLocation.SubmitterName)
                        //                            .AddString("SubmitterPhone", agencyLocation.SubmitterPhone)
                        //                            .AddString("SubmitterFax", agencyLocation.SubmitterFax)
                        //                            .AddString("Payor", agencyLocation.Payor)
                        //                            .AddString("BranchId", agencyLocation.BranchId)
                        //                            .AddString("BranchIdOther", agencyLocation.BranchIdOther)
                        //                            .AddString("Ub04Locator81cca", agencyLocation.Ub04Locator81cca)
                        //                            .AddString("TaxId", agencyLocation.TaxId)
                        //                            .AddString("TaxIdType", agencyLocation.TaxIdType)
                        //                            .AddString("NationalProviderNumber", agencyLocation.NationalProviderNumber)
                        //                            .AddString("MedicaidProviderNumber", agencyLocation.MedicaidProviderNumber)
                        //                            .AddString("HomeHealthAgencyId", agencyLocation.HomeHealthAgencyId)
                        //                            .AddString("ContactPersonFirstName", agencyLocation.ContactPersonFirstName)
                        //                            .AddString("ContactPersonLastName", agencyLocation.ContactPersonLastName)
                        //                            .AddString("ContactPersonEmail", agencyLocation.ContactPersonEmail)
                        //                            .AddString("ContactPersonPhone", agencyLocation.ContactPersonPhone)
                        //                            .AddInt("CahpsVendor", agencyLocation.CahpsVendor)
                        //                            .AddString("CahpsVendorId", agencyLocation.CahpsVendorId)
                        //                            .AddString("CahpsSurveyDesignator", agencyLocation.CahpsSurveyDesignator)
                        //                            .AddBoolean("IsAxxessTheBiller", agencyLocation.IsAxxessTheBiller)
                        //                            .AddInt("OasisAuditVendor", agencyLocation.OasisAuditVendor)
                        //                            .AddBoolean("IsLocationStandAlone", agencyLocation.IsLocationStandAlone)
                        //                            .AddDateTime("Modified", agencyLocation.Modified)
                        //                            .AddDateTime("Created", agencyLocation.Created)
                        //                            .AsNonQuery() > 0;
                        //                        }
                   
                }
                else
                {
                    
                    return false;
                }
            }

            return false;
        }

        public bool UpdateLocation(string medicareProviderNumber, Guid Id, int clusterId)
        {
            try
            {
                var sql = @"UPDATE
                            agencylocations as al
                                SET 
                                    al.MedicareProviderNumber = @medicareProviderNumber
                                        WHERE
                                            al.Id = @Id";

                using (var cmd = new FluentCommand<Agency>(sql))
                {
                    return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                         .AddGuid("Id", Id)
                         .AddString("medicareProviderNumber", medicareProviderNumber)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool EditLocationModal(AgencyLocation location, int clusterId)
        {
            if (location != null)
            {
                try
                {
                    location.Modified = DateTime.Now;
                    try
                    {
                        using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                        {
                            return database.Update<AgencyLocation>(location) > 0;
                        }
                    }
                    catch (Exception ex)
                    {

                        return false;
                    }
//                    var script = @" 
//                                    UPDATE 
//                                        agencylocations
//                                            SET
//                                                AgencyId = @AgencyId, 
//                                                Name = @Name, 
//                                                FullName = @FullName,
//                                                CustomId = @CustomId, 
//                                                CBSA = @CBSA,
//                                                MedicareProviderNumber = @MedicareProviderNumber,
//                                                AddressLine1 = @AddressLine1,
//                                                AddressLine2 = @AddressLine2,
//                                                AddressCity = @AddressCity,
//                                                AddressStateCode = @AddressStateCode, 
//                                                AddressZipCode = @AddressZipCode,
//                                                AddressZipCodeFour = @AddressZipCodeFour,
//                                                ZipNineStatus = @ZipNineStatus,
//                                                PhoneWork = @PhoneWork,
//                                                FaxNumber = @FaxNumber,
//                                                Comments = @Comments, 
//                                                IsMainOffice = @IsMainOffice, 
//                                                Cost = @Cost,
//                                                BillData = @BillData, 
//                                                IsDeprecated = @IsDeprecated,
//                                                IsSubmitterInfoTheSame = @IsSubmitterInfoTheSame, 
//                                                SubmitterId = @SubmitterId, 
//                                                SubmitterName = @SubmitterName, 
//                                                SubmitterPhone = @SubmitterPhone, 
//                                                SubmitterFax = @SubmitterFax,
//                                                Payor = @Payor, 
//                                                BranchId = @BranchId, 
//                                                BranchIdOther = @BranchIdOther, 
//                                                Ub04Locator81cca = @Ub04Locator81cca, 
//                                                TaxId = @TaxId,
//                                                TaxIdType = @TaxIdType,
//                                                NationalProviderNumber = @NationalProviderNumber, 
//                                                MedicaidProviderNumber = @MedicaidProviderNumber,
//                                                HomeHealthAgencyId = @HomeHealthAgencyId, 
//                                                ContactPersonFirstName = @ContactPersonFirstName, 
//                                                ContactPersonLastName = @ContactPersonLastName, 
//                                                ContactPersonEmail = @ContactPersonEmail, 
//                                                ContactPersonPhone = @ContactPersonPhone, 
//                                                CahpsVendor = @CahpsVendor, 
//                                                CahpsVendorId = @CahpsVendorId, 
//                                                CahpsSurveyDesignator = @CahpsSurveyDesignator,
//                                                IsAxxessTheBiller = @IsAxxessTheBiller, 
//                                                OasisAuditVendor = @OasisAuditVendor, 
//                                                IsLocationStandAlone = @IsLocationStandAlone, 
//                                                Modified = @Modified, 
//                                                Created = @Created
//                                                    WHERE Id = @Id";
//                    using (var cmd = new FluentCommand<AgencyLocation>(script))
//                    {
//                        return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
//                        .AddGuid("Id", location.Id)
//                        .AddGuid("AgencyId", location.AgencyId)
//                        .AddString("Name", location.Name)
//                        .AddString("FullName", location.FullName)
//                        .AddString("CustomId", location.CustomId)
//                        .AddString("CBSA", location.CBSA)
//                        .AddString("MedicareProviderNumber", location.MedicareProviderNumber)
//                        .AddString("AddressLine1", location.AddressLine1)
//                        .AddString("AddressLine2", location.AddressLine2)
//                        .AddString("AddressCity", location.AddressCity)
//                        .AddString("AddressStateCode", location.AddressStateCode)
//                        .AddString("AddressZipCode", location.AddressZipCode)
//                        .AddString("AddressZipCodeFour", location.AddressZipCodeFour)
//                        .AddString("ZipNineStatus", location.ZipNineStatus)
//                        .AddString("PhoneWork", location.PhoneWork)
//                        .AddString("FaxNumber", location.FaxNumber)
//                        .AddString("Comments", location.Comments)
//                        .AddBoolean("IsMainOffice", location.IsMainOffice)
//                        .AddString("Cost", location.Cost)
//                        .AddString("BillData", location.BillData)
//                        .AddBoolean("IsDeprecated", location.IsDeprecated)
//                        .AddBoolean("IsSubmitterInfoTheSame", location.IsSubmitterInfoTheSame)
//                        .AddString("SubmitterId", location.SubmitterId)
//                        .AddString("SubmitterName", location.SubmitterName)
//                        .AddString("SubmitterPhone", location.SubmitterPhone)
//                        .AddString("SubmitterFax", location.SubmitterFax)
//                        .AddString("Payor", location.Payor)
//                        .AddString("BranchId", location.BranchId)
//                        .AddString("BranchIdOther", location.BranchIdOther)
//                        .AddString("Ub04Locator81cca", location.Ub04Locator81cca)
//                        .AddString("TaxId", location.TaxId)
//                        .AddString("TaxIdType", location.TaxIdType)
//                        .AddString("NationalProviderNumber", location.NationalProviderNumber)
//                        .AddString("MedicaidProviderNumber", location.MedicaidProviderNumber)
//                        .AddString("HomeHealthAgencyId", location.HomeHealthAgencyId)
//                        .AddString("ContactPersonFirstName", location.ContactPersonFirstName)
//                        .AddString("ContactPersonLastName", location.ContactPersonLastName)
//                        .AddString("ContactPersonPhone", location.ContactPersonPhone)
//                        .AddString("ContactPersonEmail", location.ContactPersonEmail)
//                        .AddInt("CahpsVendor", location.CahpsVendor)
//                        .AddString("CahpsVendorId", location.CahpsVendorId)
//                        .AddString("CahpsSurveyDesignator", location.CahpsSurveyDesignator)
//                        .AddBoolean("IsAxxessTheBiller", location.IsAxxessTheBiller)
//                        .AddInt("OasisAuditVendor", location.OasisAuditVendor)
//                        .AddBoolean("IsLocationStandAlone", location.IsLocationStandAlone)
//                        .AddDateTime("Modified", DateTime.Now)
//                        .AddDateTime("Created", location.Created)
//                        .AsNonQuery() > 0;
//                    }

                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
      

        //public List<AgencyLite> GetAllAgencies()
        //{
        //    var list = new List<AgencyLite>();
        //    var sql = new StringBuilder()
        //        .Append("SELECT agencymanagement.agencies.Id, agencymanagement.agencies.Name, agencymanagement.agencies.Created, ")
        //        .Append("agencymanagement.agencies.IsFrozen, agencymanagement.agencies.IsSuspended, agencymanagement.agencies.IsDeprecated, ")
        //        .Append("L1.DisplayName AS SalesPerson, L2.DisplayName AS Trainer, agencymanagement.agencies.ContactPersonEmail, ")
        //        .Append("CONCAT_WS(' ', IFNULL(agencymanagement.agencies.ContactPersonFirstName, ''), IFNULL(agencymanagement.agencies.ContactPersonLastName, '')) AS ContactPerson, ")
        //        .Append("agencymanagement.agencies.ContactPersonPhone, agencymanagement.agencylocations.AddressCity, agencymanagement.agencylocations.AddressStateCode ")
        //        .Append("FROM agencymanagement.agencies ")
        //        .Append("INNER JOIN agencymanagement.agencylocations on agencymanagement.agencies.Id = agencymanagement.agencylocations.AgencyId ")
        //        .Append("LEFT JOIN axxessmembership.logins L1 on agencymanagement.agencies.SalesPerson = L1.Id ")
        //        .Append("LEFT JOIN axxessmembership.logins L2 on agencymanagement.agencies.Trainer = L2.Id ")
        //        .Append("WHERE agencymanagement.agencylocations.IsMainOffice = 1 ORDER BY agencymanagement.agencies.Created asc")
        //        .ToString();

        //    using (var cmd = new FluentCommand<AgencyLite>(sql))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString")
        //        .SetMap(reader => new AgencyLite
        //        {
        //            Id = reader.GetGuid("Id"),
        //            Name = reader.GetStringNullable("Name"),
        //            City = reader.GetStringNullable("AddressCity"),
        //            Created = reader.GetDateTime("Created"),
        //            IsFrozen = reader.GetBoolean("IsFrozen"),
        //            State = reader.GetStringNullable("AddressStateCode"),
        //            Trainer = reader.GetStringNullable("Trainer"),
        //            IsSuspended = reader.GetBoolean("IsSuspended"),
        //            IsDeprecated = reader.GetBoolean("IsDeprecated"),
        //            SalesPerson = reader.GetStringNullable("SalesPerson"),
        //            ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
        //            ContactPersonDisplayName = reader.GetStringNullable("ContactPerson"),
        //            ContactPersonPhoneFormatted = reader.GetStringNullable("ContactPersonPhone").ToPhone()
        //        }).AsList();
        //    }

        //    return list;
        //}

        public List<AgencyLite> GetAllAgencies(string stateFilter, DateTime? startDate, DateTime? endDate, int clusterId)
        {
            var list = new List<AgencyLite>();
            var filter = stateFilter.IsEqual("all") ? string.Empty
                  : " and loc.AddressStateCode = @stateFilter ";
            var date = startDate.Value.Equals(DateTime.MinValue) || endDate.Value.Equals(DateTime.MaxValue) ? string.Empty : "and Date(agn.Created) between Date(@startDate) and Date(@endDate) ";
            var sql = string.Format(@"SELECT 
                            agn.Id,
                            agn.Name,
                            agn.Created,
                            agn.IsFrozen, 
                            agn.IsSuspended, 
                            agn.IsDeprecated, 
                            agn.SalesPerson AS SalesPerson,
                            agn.Trainer AS Trainer, 
                            agn.ContactPersonEmail, 
                            CONCAT_WS(' ', IFNULL(agn.ContactPersonFirstName, ''),
                            IFNULL(agn.ContactPersonLastName, '')) AS ContactPerson, 
                            agn.ContactPersonPhone,
                            loc.AddressCity, 
                            loc.AddressStateCode
                                FROM 
                                    agencymanagement.agencies agn
                                        INNER JOIN agencymanagement.agencylocations loc on agn.Id = loc.AgencyId 
                                            WHERE 
                                            loc.IsMainOffice = 1 AND 
                                            agn.ClusterId = @clusterId  {0}{1};", filter, date);

            //LEFT JOIN axxessmembership.logins log on (agn.SalesPerson = log.Id ||  agn.Trainer = log.Id )
            //                           LEFT JOIN axxessmembership.logins L2 on agencymanagement.agencies.Trainer = L2.Id 

            using (var cmd = new FluentCommand<AgencyLite>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString"+ clusterId)
                    .AddString("stateFilter", stateFilter)
                    .AddDateTime("startDate", startDate)
                    .AddDateTime("endDate", endDate)
                    .AddInt("clusterId", clusterId)
                    .SetMap(reader => new AgencyLite
                    {
                        Id = reader.GetGuid("Id"),
                        Name = reader.GetStringNullable("Name"),
                        City = reader.GetStringNullable("AddressCity"),
                        Created = reader.GetDateTime("Created"),
                        IsFrozen = reader.GetBoolean("IsFrozen"),
                        State = reader.GetStringNullable("AddressStateCode"),
                        TrainerId = reader.GetGuid("Trainer"),
                        IsSuspended = reader.GetBoolean("IsSuspended"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        SalesPersonId = reader.GetGuid("SalesPerson"),
                        ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                        ContactPersonDisplayName = reader.GetStringNullable("ContactPerson"),
                        ContactPersonPhoneFormatted = reader.GetStringNullable("ContactPersonPhone").ToPhone()
                    }).AsList();
            }
            if (list != null && list.Count > 0)
            {
                list = list.OrderBy(l => l.Created).ToList();
            }
            return list;
        }

        public List<AgencyLite> GetAgencyContactInformations(List<Guid> agencyIds, int clusterId)
        {
            var list = new List<AgencyLite>();
            var sql = string.Format(@"SELECT 
                            agn.Id,
                            agn.Name,
                            agn.IsFrozen ,
                            agn.ContactPersonEmail, 
                            CONCAT_WS(' ', IFNULL(agn.ContactPersonFirstName, ''),IFNULL(agn.ContactPersonLastName, '')) AS ContactPerson, 
                            agn.ContactPersonPhone
                                FROM 
                                    agencymanagement.agencies agn
                                            WHERE 
                                            agn.Id IN({0});", agencyIds.ToCommaSeperatedList());

            //LEFT JOIN axxessmembership.logins log on (agn.SalesPerson = log.Id ||  agn.Trainer = log.Id )
            //                           LEFT JOIN axxessmembership.logins L2 on agencymanagement.agencies.Trainer = L2.Id 

            using (var cmd = new FluentCommand<AgencyLite>(sql))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddInt("clusterId", clusterId)
                    .SetMap(reader => new AgencyLite
                    {
                        Id = reader.GetGuid("Id"),
                        Name = reader.GetStringNullable("Name"),
                        IsFrozen = reader.GetBoolean("IsFrozen"),
                        ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                        ContactPersonDisplayName = reader.GetStringNullable("ContactPerson"),
                        ContactPersonPhoneFormatted = reader.GetStringNullable("ContactPersonPhone").ToPhone()
                    }).AsList();
            }
           
            return list;
        }

        public bool Update(Agency agency, int clusterId)
        {
            var AgencyServices = (int)agency.Services;
            agency.Modified = DateTime.Now;
            try
            {
                using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString" + clusterId)))
                {
                    return database.Update<Agency>(agency) > 0;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
//            if (agency != null)
//            {
//                try
//                {
//                    var script = @"UPDATE 
//                                        agencies
//                                            SET
//                                                SalesPerson = @SalesPerson,
//                                                Trainer = @Trainer, 
//                                                BackupTrainer = @BackupTrainer,
//                                                ImplementationSpecialist = @ImplementationSpecialist,
//                                                Name = @Name, 
//                                                TaxId = @TaxId,
//                                                TaxIdType = @TaxIdType,
//                                                NationalProviderNumber = @NationalProviderNumber,
//                                                MedicareProviderNumber = @MedicareProviderNumber, 
//                                                MedicaidProviderNumber = @MedicaidProviderNumber, 
//                                                HomeHealthAgencyId = @HomeHealthAgencyId, 
//                                                SubmitterId = @SubmitterId, 
//                                                SubmitterName = @SubmitterName, 
//                                                SubmitterPhone = @SubmitterPhone, 
//                                                SubmitterFax = @SubmitterFax, 
//                                                IsAgreementSigned = @IsAgreementSigned, 
//                                                IsSuspended = @IsSuspended, 
//                                                TrialPeriod = @TrialPeriod, 
//                                                Package = @Package, 
//                                                AnnualPlanId = @AnnualPlanId, 
//                                                ContactPersonFirstName = @ContactPersonFirstName, 
//                                                ContactPersonLastName = @ContactPersonLastName, 
//                                                ContactPersonEmail = @ContactPersonEmail, 
//                                                ContactPersonPhone = @ContactPersonPhone, 
//                                                Payor = @Payor, 
//                                                Comments = @Comments, 
//                                                CustomFields = @CustomFields, 
//                                                Created = @Created, 
//                                                Modified = @Modified, 
//                                                IsDeprecated = @IsDeprecated, 
//                                                CahpsVendor = @CahpsVendor, 
//                                                CahpsVendorId = @CahpsVendorId, 
//                                                CahpsSurveyDesignator = @CahpsSurveyDesignator, 
//                                                IsAxxessTheBiller = @IsAxxessTheBiller, 
//                                                OasisAuditVendor = @OasisAuditVendor, 
//                                                OasisAuditVendorApiKey = @OasisAuditVendorApiKey, 
//                                                FrozenDate = @FrozenDate, 
//                                                IsFrozen = @IsFrozen, 
//                                                PreviousSoftware = @PreviousSoftware, 
//                                                Services = @Services, 
//                                                AccountId = @AccountId
//                                                    WHERE Id = @Id";
//                    using (var cmd = new FluentCommand<Agency>(script))
//                    {
//                        return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
//                        .AddGuid("Id", agency.Id)
//                        .AddGuid("SalesPerson", agency.SalesPerson)
//                        .AddGuid("Trainer", agency.Trainer)
//                        .AddGuid("BackupTrainer", agency.BackupTrainer)
//                        .AddGuid("ImplementationSpecialist", agency.ImplementationSpecialist)
//                        .AddString("Name", agency.Name)
//                        .AddString("TaxId", agency.TaxId)
//                        .AddString("TaxIdType", agency.TaxIdType)
//                        .AddString("NationalProviderNumber", agency.NationalProviderNumber)
//                        .AddString("MedicareProviderNumber", agency.MedicareProviderNumber)
//                        .AddString("MedicaidProviderNumber", agency.MedicaidProviderNumber)
//                        .AddString("HomeHealthAgencyId", agency.HomeHealthAgencyId)
//                        .AddString("SubmitterId", agency.SubmitterId)
//                        .AddString("SubmitterName", agency.SubmitterName)
//                        .AddString("SubmitterPhone", agency.SubmitterPhone)
//                        .AddString("SubmitterFax", agency.SubmitterFax)
//                        .AddBoolean("IsAgreementSigned", agency.IsAgreementSigned)
//                        .AddBoolean("IsSuspended", agency.IsSuspended)
//                        .AddInt("TrialPeriod", agency.TrialPeriod)
//                        .AddInt("Package", agency.Package)
//                        .AddInt("AnnualPlanId", agency.AnnualPlanId)
//                        .AddString("ContactPersonFirstName", agency.ContactPersonFirstName)
//                        .AddString("ContactPersonLastName", agency.ContactPersonLastName)
//                        .AddString("ContactPersonEmail", agency.ContactPersonEmail)
//                        .AddString("ContactPersonPhone", agency.ContactPersonPhone)
//                        .AddString("Payor", agency.Payor)
//                        .AddString("Comments", agency.Comments)
//                        .AddString("CustomFields", agency.CustomFields)
//                        .AddDateTime("Modified", DateTime.Now)
//                        .AddBoolean("IsDeprecated", agency.IsDeprecated)
//                        .AddInt("CahpsVendor", agency.CahpsVendor)
//                        .AddString("CahpsVendorId", agency.CahpsVendorId)
//                        .AddString("CahpsSurveyDesignator", agency.CahpsSurveyDesignator)
//                        .AddBoolean("IsAxxessTheBiller", agency.IsAxxessTheBiller)
//                        .AddInt("OasisAuditVendor", agency.OasisAuditVendor)
//                        .AddString("OasisAuditVendorApiKey", agency.OasisAuditVendorApiKey)
//                        .AddDateTime("FrozenDate", agency.FrozenDate)
//                        .AddBoolean("IsFrozen", agency.IsFrozen)
//                        .AddString("PreviousSoftware", agency.PreviousSoftware)
//                        .AddInt("Services", AgencyServices)
//                        .AddString("AccountId", agency.AccountId)
//                        .AsNonQuery() > 0;
//                    }
//                }
//                catch (Exception ex)
//                {
//                    return false;
//                }
//            }
//            else
//            {
//                return false;
//            }

        }

        public bool UpdateImplementationSpecialist(Guid agencyId, Guid implementationSpecialistId, int clusterId)
        {
                try
                {
                    var script = @"UPDATE 
                                        agencies
                                            SET
                                                ImplementationSpecialist = @ImplementationSpecialist,
                                                Modified = @Modified
                                                    WHERE Id = @Id";
                    using (var cmd = new FluentCommand<Agency>(script))
                    {
                        return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                        .AddGuid("Id", agencyId)
                        .AddGuid("ImplementationSpecialist", implementationSpecialistId)
                        .AddDateTime("Modified", DateTime.Now)
                        .AsNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
           

        }

        public List<AgencySelection> GetAgencySelection(bool onlyDeleted, int clusterId)
        {
            var selection = new List<AgencySelection>();
            var sql = @"SELECT 
                            agencies.Id, 
                            agencies.Name, 
                            agencies.Created, 
                            agencylocations.AddressStateCode 
                                    FROM 
                                        agencies 
                                            INNER JOIN agencylocations on agencies.Id = agencylocations.AgencyId 
                                                WHERE 
                                                    agencies.IsDeprecated = @isdeprecated AND
                                                    agencylocations.IsMainOffice = 1 AND 
                                                    agencies.ClusterId = @clusterid;";
            using (var cmd = new FluentCommand<AgencySelection>(sql))
            {
                selection = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddInt("clusterId", clusterId)
                    .AddInt("isdeprecated", onlyDeleted ? 1 : 0)
                .SetMap(reader => new AgencySelection
                {
                    Id = reader.GetGuid("Id"),
                    Name = reader.GetString("Name"),
                    Created = reader.GetDateTime("Created"),
                    StateCode = reader.GetString("AddressStateCode")
                }).AsList();
            }
            if (selection != null && selection.Count > 0)
            {
                selection = selection.OrderBy(s => s.Name).ToList();
            }

            return selection;
        }


        public bool RemoveAgency(Guid Id, int clusterId)
        {
            try
            {
                var sql = @"DELETE
                             from  agencies 
                                         WHERE
                                              Id = @id";


                using (var cmd = new FluentCommand<Agency>(sql))
                {
                    return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                         .AddGuid("id", Id)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            //try
            //{
            //    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString")))
            //    {
            //        database.Delete<Asset>(Id);
            //    }
            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //} 
        }


        public bool RemoveLocation(Guid agencyId, Guid Id, int clusterId)
        {
            try
            {
                var sql = @"DELETE
                             from  agencylocations 
                                         WHERE
                                              AgencyId = @agencyid AND
                                              Id = @id";


                using (var cmd = new FluentCommand<Agency>(sql))
                {
                    return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                         .AddGuid("id", Id)
                         .AddGuid("agencyid", agencyId)
                         .AsNonQuery() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            //try
            //{
            //    using (new SharedDbConnectionScope(ProviderFactory.GetProvider("AgencyManagementConnectionString")))
            //    {
            //        database.Delete<Asset>(Id);
            //    }
            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //} 
        }

        //public int GetAgencyCount(int clusterId)
        //{
        //    var count = 0;
        //    var sql = "SELECT COUNT(Id) FROM agencies;";
        //    using (var cmd = new FluentCommand<AgencySelection>(sql))
        //    {
        //        count = cmd.SetConnection("AgencyManagementConnectionString" + clusterId).AsScalar();
        //    }

        //    return count;
        //}

     
        #endregion

        #region Template Methods

        public bool InsertTemplates(Guid agencyId, int clusterId)
        {
            var templateSQL = MessageBuilder.ReadTextFrom("Templates");

            if (templateSQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(templateSQL.Replace("{0}", agencyId.ToString())))
                {
                    return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                     .AsNonQuery() > 0;
                }
            }
            return false;
        }

        #endregion

        #region Supply Methods

        public bool InsertSupplies(Guid agencyId, int clusterId)
        {
            var supplySQL = MessageBuilder.ReadTextFrom("Supplies");

            if (supplySQL.IsNotNullOrEmpty())
            {
                using (var cmd = new FluentCommand<int>(supplySQL.Replace("{0}", agencyId.ToString())))
                {
                 return   cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AsNonQuery() > 0;
                }
            }
            return false;
        }

        #endregion

        #region Adjustment Codes

        public bool AddAdjustmentCode(AgencyAdjustmentCode code, int clusterId)
        {
            if (code != null)
            {
                code.Created = DateTime.Now;
                code.Modified = DateTime.Now;
                if (code.Id.IsEmpty())
                {
                    code.Id = Guid.NewGuid();
                }
                try
                {
                    var script = @"INSERT INTO 
                                        uploadtypes
                                         (
                                            Id,
                                            AgencyId,
                                            Code, 
                                            Description,
                                            IsDeprecated,
                                            Created,
                                            Modified
                                          ) 
                                          VALUES 
                                          (
                                            @Id,
                                            @AgencyId,
                                            @Code,
                                            @Description, 
                                            @IsDeprecated,
                                            @Created,
                                            @Modified
                                          );";
                    using (var cmd = new FluentCommand<AgencyAdjustmentCode>(script))
                    {
                        return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                        .AddGuid("Id", code.Id)
                        .AddGuid("AgencyId", code.AgencyId)
                        .AddString("Code", code.Code)
                        .AddString("Description", code.Description)
                        .AddBoolean("IsDeprecated", code.IsDeprecated)
                        .AddDateTime("Created", code.Created)
                        .AddDateTime("Modified", code.Modified)
                        .AsNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Upload Type

        public void InsertUploadTypes(Guid agencyId, int clusterId)
        {
            List<UploadType> types = new List<UploadType>();
            types.Add(new UploadType
            {
                AgencyId = agencyId,
                Type = "Admission Documents"
            });

            types.Add(new UploadType
            {
                AgencyId = agencyId,
                Type = "Consent to Photograph"
            });

            types.Add(new UploadType
            {
                AgencyId = agencyId,
                Type = "Lab Results"
            });

            types.Add(new UploadType
            {
                AgencyId = agencyId,
                Type = "Advanced Directives"
            });

            types.Add(new UploadType
            {
                AgencyId = agencyId,
                Type = "Consent Form"
            });

            types.Add(new UploadType
            {
                AgencyId = agencyId,
                Type = "Patient Choice Statment"
            });

            types.Add(new UploadType
            {
                AgencyId = agencyId,
                Type = "Encounter Details"
            });

            types.Add(new UploadType
            {
                AgencyId = agencyId,
                Type = "Family Medical History"
            });

            types.Add(new UploadType
            {
                AgencyId = agencyId,
                Type = "Individual Medical History"
            });

            types.Add(new UploadType
            {
                AgencyId = agencyId,
                Type = "Hospital Discharge Summary"
            });
            AddManyUploadTypes(types, clusterId);
        }

        public bool AddUploadType(UploadType uploadType, int clusterId)
        {
            if (uploadType != null)
            {
                try
                {
                uploadType.Created = DateTime.Now;
                uploadType.Modified = DateTime.Now;
                uploadType.IsDeprecated = false;
                if (uploadType.Id.IsEmpty())
                {
                    uploadType.Id = Guid.NewGuid();
                }

                
                    var script = @"INSERT INTO 
                                        uploadtypes
                                         (
                                            Id,
                                            AgencyId,
                                            Type, 
                                            IsDeprecated,
                                            Created,
                                            Modified
                                          ) 
                                          VALUES 
                                          (
                                            @Id,
                                            @AgencyId,
                                            @Type, 
                                            @IsDeprecated,
                                            @Created,
                                            @Modified
                                          );";
                    using (var cmd = new FluentCommand<UploadType>(script))
                    {
                        return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                        .AddGuid("Id", uploadType.Id)
                        .AddGuid("AgencyId", uploadType.AgencyId)
                        .AddString("Type", uploadType.Type)
                        .AddBoolean("IsDeprecated", uploadType.IsDeprecated)
                        .AddDateTime("Created", uploadType.Created)
                        .AddDateTime("Modified", uploadType.Modified)
                        .AsNonQuery() > 0;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }

        public bool AddManyUploadTypes(List<UploadType> uploadTypes, int clusterId)
        {
            var count = 0;
            foreach (var uploadType in uploadTypes)
            {
                if (AddUploadType(uploadType, clusterId))
                {
                    count++;
                }
            }
            return count == uploadTypes.Count;
        }

        #endregion

        #region Agency Upgrades

//        public bool UpdateAgencyUpgrade(AgencyUpgrade agencyUpgrade, int clusterId)
//        {
//            if (agencyUpgrade != null)
//            {
//                agencyUpgrade.Modified = DateTime.Now;
//                try
//                {
//                    var script = @"UPDATE 
//                                         agencyupgrades
//                                            SET
//                                                AgencyId = @AgencyId,
//                                                AgencyLocationId = @AgencyLocationId, 
//                                                RequestedById = @RequestedById,
//                                                AccountId = @AccountId,
//                                                PreviousAmount = @PreviousAmount,
//                                                Amount = @Amount, 
//                                                PreviousPackageId = @PreviousPackageId,
//                                                RequestedPackageId = @RequestedPackageId,
//                                                AnnualPlanId = @AnnualPlanId,
//                                                IsComplete = @IsComplete, 
//                                                IsUserPlan = @IsUserPlan,
//                                                Type = @Type,
//                                                Comments = @Comments, 
//                                                EffectiveDate = @EffectiveDate,
//                                                Created = @Created,
//                                                Modified = @Modified
//                                                    WHERE 
//                                                        Id = @Id";
//                    using (var cmd = new FluentCommand<AgencyUpgrade>(script))
//                    {
//                        return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
//                        .AddGuid("Id", agencyUpgrade.Id)
//                        .AddGuid("AgencyId", agencyUpgrade.AgencyId)
//                        .AddGuid("AgencyLocationId", agencyUpgrade.AgencyLocationId)
//                        .AddGuid("RequestedById", agencyUpgrade.RequestedById)
//                        .AddString("AccountId", agencyUpgrade.AccountId)
//                        .AddDecimal("PreviousAmount", (decimal)agencyUpgrade.PreviousAmount)
//                        .AddDecimal("Amount", (decimal)agencyUpgrade.Amount)
//                        .AddInt("PreviousPackageId", agencyUpgrade.PreviousPackageId)
//                        .AddInt("RequestedPackageId", agencyUpgrade.RequestedPackageId)
//                        .AddInt("AnnualPlanId", agencyUpgrade.AnnualPlanId)
//                        .AddBoolean("IsComplete", agencyUpgrade.IsComplete)
//                        .AddBoolean("IsUserPlan", agencyUpgrade.IsUserPlan)
//                        .AddString("Type", agencyUpgrade.Type)
//                        .AddString("Comments", agencyUpgrade.Comments)
//                        .AddDateTime("EffectiveDate", agencyUpgrade.EffectiveDate)
//                        .AddDateTime("Created", agencyUpgrade.Created)
//                        .AddDateTime("Modified", agencyUpgrade.Modified)
//                        .AsNonQuery() > 0;
//                    }
//                }
//                catch (Exception ex)
//                {
//                    return false;
//                }
//            }
//            else
//            {
//                return false;
//            }
//        }


      


        //public IList<AgencyUpgrade> GetAgencyUpgrades(string isCompleted, DateTime startDate, DateTime endDate, int clusterId)
        //{
        //    var list = new List<AgencyUpgrade>();
        //    var filter = isCompleted.IsEqual("all") ? string.Empty
        //        : " and agencyupgrades.IsComplete = @iscompleted ";
        //    var sql = new StringBuilder("SELECT agencyupgrades.Id as AgencyUpgradeId, agencies.Id as AgencyId, agencies.Name as AgencyName, agencylocations.Id as LocationId, ")
        //    .Append("agencyupgrades.RequestedById, agencyupgrades.PreviousPackageId, agencyupgrades.RequestedPackageId, agencylocations.Name as LocationName, ")
        //    .Append("agencyupgrades.IsUserPlan, agencyupgrades.IsComplete,agencyupgrades.Amount, agencyupgrades.PreviousAmount, agencyupgrades.Created, users.FirstName, users.LastName, users.TitleType, users.TitleTypeOther ")
        //    .Append("from agencyupgrades inner join agencies on agencyupgrades.AgencyId = agencies.Id ")
        //    .Append("inner join users on agencyupgrades.RequestedById = users.Id ")
        //    .Append("left join agencylocations on agencyupgrades.AgencyLocationId = agencylocations.Id ")
        //    .AppendFormat("where agencies.IsDeprecated = 0 {0}", filter)
        //    .Append("and Date(agencyupgrades.created) between Date(@startdate) and Date(@enddate) ")
        //    .Append("and agencyupgrades.PreviousPackageId !=0 ")
        //    .Append("and users.IsDeprecated = 0 order by agencyupgrades.Created desc;")
        //    .ToString();

        //    using (var cmd = new FluentCommand<AgencyUpgrade>(sql))
        //    {
        //        list.AddRange(cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
        //            .AddBoolean("iscompleted", isCompleted.ToBoolean())
        //            .AddDateTime("startdate", startDate)
        //            .AddDateTime("enddate", endDate)
        //            .SetMap(reader => new AgencyUpgrade
        //            {
        //                AgencyId = reader.GetGuid("AgencyId"),
        //                Id = reader.GetGuid("AgencyUpgradeId"),
        //                Created = reader.GetDateTime("Created"),
        //                AgencyName = reader.GetString("AgencyName"),
        //                IsComplete = reader.GetBoolean("IsComplete"),
        //                IsUserPlan = reader.GetBoolean("IsUserPlan"),
        //                RequestedById = reader.GetGuid("RequestedById"),
        //                PreviousPackageId = reader.GetInt("PreviousPackageId"),
        //                LocationName = reader.GetStringNullable("LocationName"),
        //                RequestedPackageId = reader.GetInt("RequestedPackageId"),
        //                AgencyLocationId = reader.GetGuidIncludeEmpty("LocationId"),
        //                Amount = reader.GetDouble("Amount") == -1 ? 0 : reader.GetDouble("Amount"),
        //                PreviousAmount = reader.GetDouble("PreviousAmount") == -1 ? 0 : reader.GetDouble("PreviousAmount"),
        //                RequestedBy = string.Format("{0} {1} {2}", reader.GetStringNullable("FirstName"), reader.GetStringNullable("LastName"), reader.GetStringNullable("TitleType"))
        //            })
        //            .AsList());
        //    }
        //    return list;
        //}


        #endregion

        #region Annual Plan

        //public IList<AgencyUpgrade> GetAgencyAnnualPlan(string isCompleted, DateTime startDate, DateTime endDate, int clusterId)
        //{
        //    var list = new List<AgencyUpgrade>();
           
        //            var filter = isCompleted.IsEqual("all") ? string.Empty
        //                : " and agencyupgrades.IsComplete = @iscompleted ";
        //            var sql = new StringBuilder("SELECT agencyupgrades.Id as AgencyUpgradeId, agencies.Id as AgencyId, agencies.Name as AgencyName, ")
        //            .Append("agencyupgrades.RequestedById, agencyupgrades.PreviousPackageId, agencyupgrades.AnnualPlanId, agencyupgrades.RequestedPackageId, ")
        //            .Append("agencyupgrades.IsComplete,agencyupgrades.Amount, agencyupgrades.PreviousAmount, agencyupgrades.Created, agencylocations.Id as LocationId, agencylocations.Name as LocationName, users.FirstName, users.LastName, users.TitleType, users.TitleTypeOther ")
        //            .Append("from agencyupgrades inner join agencies on agencyupgrades.AgencyId = agencies.Id ")
        //            .Append("inner join users on agencyupgrades.RequestedById = users.Id ")
        //            .Append("left join agencylocations on agencyupgrades.AgencyLocationId = agencylocations.Id ")
        //            .AppendFormat("where agencies.IsDeprecated = 0 {0}", filter)
        //            .Append("and Date(agencyupgrades.created) between Date(@startdate) and Date(@enddate) ")
        //            .Append("and agencyupgrades.AnnualPlanId !=0 ")
        //            .Append("and agencyupgrades.PreviousPackageId=0 ")
        //            .Append("and users.IsDeprecated = 0 order by agencyupgrades.Created desc;")
        //            .ToString();

        //            using (var cmd = new FluentCommand<AgencyUpgrade>(sql))
        //            {
        //                list.AddRange(cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
        //                    .AddBoolean("iscompleted", isCompleted.ToBoolean())
        //                    .AddDateTime("startdate", startDate)
        //                    .AddDateTime("enddate", endDate)
        //                    .SetMap(reader => new AgencyUpgrade
        //                    {
        //                        AgencyId = reader.GetGuid("AgencyId"),
        //                        Id = reader.GetGuid("AgencyUpgradeId"),
        //                        Created = reader.GetDateTime("Created"),
        //                        LocationName = reader.GetStringNullable("LocationName"),
        //                        AgencyLocationId = reader.GetGuidIncludeEmpty("LocationId"),
        //                        AgencyName = reader.GetString("AgencyName"),
        //                        IsComplete = reader.GetBoolean("IsComplete"),
        //                        Amount = reader.GetDouble("Amount") == -1 ? 0 : reader.GetDouble("Amount"),
        //                        PreviousAmount = reader.GetDouble("PreviousAmount") == -1 ? 0 : reader.GetDouble("PreviousAmount"),
        //                        RequestedById = reader.GetGuid("RequestedById"),
        //                        AnnualPlanId = reader.GetInt("AnnualPlanId"),
        //                        PreviousPackageId = reader.GetInt("PreviousPackageId"),
        //                        RequestedPackageId = reader.GetInt("RequestedPackageId"),
        //                        RequestedBy = string.Format("{0} {1} {2}", reader.GetStringNullable("FirstName"), reader.GetStringNullable("LastName"), reader.GetStringNullable("TitleType"))
        //                    })
        //                    .AsList());
                    
        //    }
        //    return list;
        //}
        #endregion

        #region Change

        //public IList<AgencyChange> GetAgencyChanges(string isCompleted, DateTime startDate, DateTime endDate, int clusterId)
        //{
        //    var list = new List<AgencyChange>();
        //    var filter = isCompleted.IsEqual("all") ? string.Empty
        //        : " and agencychanges.IsComplete = @iscompleted ";
        //    var sql = new StringBuilder("SELECT agencychanges.Id as AgencyChangeId, agencychanges.Name, ")
        //    .Append("agencychanges.TaxId, agencychanges.TaxIdType, agencychanges.ContactPersonEmail, agencychanges.ContactPersonPhone, ")
        //    .Append("agencychanges.CahpsVendor, agencychanges.NationalProviderNumber, agencychanges.MedicareProviderNumber, agencychanges.MedicaidProviderNumber, agencychanges.HomeHealthAgencyId, agencychanges.ContactPersonFirstName, ")
        //    .Append("agencychanges.ContactPersonLastName, agencychanges.LocationName, agencychanges.LocationAddressLine1, agencychanges.LocationAddressLine2, agencychanges.LocationAddressCity, agencychanges.LocationAddressStateCode, ")
        //    .Append("agencychanges.LocationAddressZipCode, agencychanges.LocationPhoneWork, agencychanges.LocationFaxNumber, agencychanges.Created, agencychanges.IsComplete,agencychanges.Comments, agencychanges.AgencyId, agencychanges.FullName ")
        //    .Append("from agencychanges ")
        //    .AppendFormat("where Date(agencychanges.Created) between Date(@startdate) and Date(@enddate) {0}", filter)
        //    .Append("order by agencychanges.Created desc;")
        //    .ToString();

        //    using (var cmd = new FluentCommand<AgencyChange>(sql))
        //    {
        //        list = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
        //            .AddBoolean("iscompleted", isCompleted.ToBoolean())
        //            .AddDateTime("startdate", startDate)
        //            .AddDateTime("enddate", endDate)
        //            .SetMap(reader => new AgencyChange
        //            {
        //                Id = reader.GetGuid("AgencyChangeId"),
        //                AgencyId = reader.GetGuid("AgencyId"),
        //                Name = reader.GetStringNullable("Name"),
        //                TaxId = reader.GetStringNullable("TaxId"),
        //                TaxIdType = reader.GetStringNullable("TaxIdType"),
        //                ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
        //                ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone"),
        //                CahpsVendor = reader.GetInt("CahpsVendor"),
        //                NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
        //                MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
        //                MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
        //                HomeHealthAgencyId = reader.GetStringNullable("HomeHealthAgencyId"),
        //                ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
        //                ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
        //                LocationName = reader.GetStringNullable("LocationName"),
        //                LocationAddressLine1 = reader.GetStringNullable("LocationAddressLine1"),
        //                LocationAddressLine2 = reader.GetStringNullable("LocationAddressLine2"),
        //                LocationAddressCity = reader.GetStringNullable("LocationAddressCity"),
        //                LocationAddressStateCode = reader.GetStringNullable("LocationAddressStateCode"),
        //                LocationAddressZipCode = reader.GetStringNullable("LocationAddressZipCode"),
        //                LocationPhoneWork = reader.GetStringNullable("LocationPhoneWork"),
        //                LocationFaxNumber = reader.GetStringNullable("LocationFaxNumber"),
        //                FullName = reader.GetStringNullable("FullName"),
        //                Created = reader.GetDateTime("Created"),
        //                IsComplete = reader.GetBoolean("IsComplete"),
        //                Comments = reader.GetStringNullable("Comments")
        //            })
        //            .AsList();
        //    }
        //    return list;
        //}


        public AgencyChange GetAgencyChangeCur(Guid agencyId, int clusterId)
        {
            var script = @"SELECT
                        agencies.Id as AgencyId,
                        agencies.Name as AgencyName,
                        agencies.TaxId as TaxId,
                        agencies.TaxIdType as TaxIdType,
                        agencies.ContactPersonEmail as ContactPersonEmail,
                        agencies.ContactPersonPhone as ContactPersonPhone,
                        agencies.CahpsVendor as CahpsVendor,
                        agencies.NationalProviderNumber as NationalProviderNumber,
                        agencies.MedicareProviderNumber as MedicareProviderNumber,
                        agencies.MedicaidProviderNumber as MedicaidProviderNumber,
                        agencies.HomeHealthAgencyId as HomeHealthAgencyId,
                        agencies.ContactPersonFirstName as ContactPersonFirstName,
                        agencies.ContactPersonLastName as ContactPersonLastName,
                        agencylocations.Name as LocationName,
                        agencylocations.AddressLine1 as LocationAddressLine1, 
                        agencylocations.AddressLine2 as LocationAddressLine2,
                        agencylocations.AddressCity as LocationAddressCity, 
                        agencylocations.AddressStateCode as LocationAddressStateCode, 
                        agencylocations.AddressZipCode as LocationAddressZipCode,
                        agencylocations.PhoneWork as LocationPhoneWork,
                        agencylocations.FaxNumber as LocationFaxNumber,
                        agencylocations.IsMainOffice as IsMainOffice
                            FROM
                                agencies 
                                    INNER JOIN agencylocations ON agencies.Id = agencylocations.AgencyId 
                                        Where 
                                             agencies.Id = @agencyid and agencylocations.IsMainOffice = 1";


            return new FluentCommand<AgencyChange>(script)
                .SetConnection("AgencyManagementConnectionString" + clusterId)
                .AddGuid("@agencyid", agencyId)
                .SetMap(reader => new AgencyChange
                {
                    AgencyId= reader.GetGuid("AgencyId"),
                    Name = reader.GetStringNullable("AgencyName"),
                    TaxId = reader.GetStringNullable("TaxId"),
                    TaxIdType = reader.GetStringNullable("TaxIdType"),
                    ContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
                    ContactPersonPhone = reader.GetStringNullable("ContactPersonPhone"),
                    CahpsVendor = reader.GetInt("CahpsVendor"),
                    NationalProviderNumber = reader.GetStringNullable("NationalProviderNumber"),
                    MedicareProviderNumber = reader.GetStringNullable("MedicareProviderNumber"),
                    MedicaidProviderNumber = reader.GetStringNullable("MedicaidProviderNumber"),
                    HomeHealthAgencyId = reader.GetStringNullable("HomeHealthAgencyId"),
                    ContactPersonFirstName = reader.GetStringNullable("ContactPersonFirstName"),
                    ContactPersonLastName = reader.GetStringNullable("ContactPersonLastName"),
                    LocationName = reader.GetStringNullable("LocationName"),
                    LocationAddressLine1 = reader.GetStringNullable("LocationAddressLine1"),
                    LocationAddressLine2 = reader.GetStringNullable("LocationAddressLine2"),
                    LocationAddressCity = reader.GetStringNullable("LocationAddressCity"),
                    LocationAddressStateCode = reader.GetStringNullable("LocationAddressStateCode"),
                    LocationAddressZipCode = reader.GetStringNullable("LocationAddressZipCode"),
                    LocationPhoneWork = reader.GetStringNullable("LocationPhoneWork"),
                    LocationFaxNumber = reader.GetStringNullable("LocationFaxNumber")
                       
                }).AsSingle();
        }

        #endregion

      

//        #region Implementation Specialists

//        public bool UpdateImplementationSpecialist(Guid agencyId, Guid implementationSpecialistId, int clusterId)
//        {
//            bool result = false;
//            var agency = new Agency();
           
//                try
//                {
//                    var sql = @"UPDATE
//                          agencies as ag
//                            SET 
//                                ag.ImplementationSpecialist = @implementationSpecialistId,
//                                ag.Modified = @modified
//                                     WHERE
//                                          ag.Id = @agencyId";


//                    using (var cmd = new FluentCommand<Agency>(sql))
//                    {
//                        return cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
//                             .AddDateTime("modified", DateTime.Now)
//                             .AddGuid("implementationSpecialistId", implementationSpecialistId)
//                             .AddGuid("agencyId", agencyId)
//                             .AsNonQuery() > 0;
//                    }
//                }
//                catch (Exception ex)
//                {
//                    return false;
//                }
            
//            return result;
//        }

//        public IList<AgencySpecialist> GetImplementationSpecialists(Guid implementationSpecialistId, DateTime startDate, DateTime endDate, int clusterId)
//        {
//            var list = new List<AgencySpecialist>();
//            var implementationSpecialistSql = string.Empty;
//            if (!implementationSpecialistId.IsEmpty())
//            {
//                implementationSpecialistSql = string.Format("and agencymanagement.agencies.ImplementationSpecialist = '{0}' ", implementationSpecialistId);
//            }

//            var sql = new StringBuilder(@"SELECT 
//agencymanagement.agencies.Id as AgencyId, 
//agencymanagement.agencies.Name as AgencyName, 
//agencymanagement.agencies.ContactPersonPhone, 
//agencymanagement.agencies.ContactPersonEmail,
//agencymanagement.agencies.ContactPersonFirstName,
//agencymanagement.agencies.ContactPersonLastName, 
//agencymanagement.agencies.Created, 
//agencymanagement.agencies.ImplementationSpecialist,
//customermanagement.logins.EmailAddress,
//customermanagement.logins.DisplayName 
//from agencymanagement.agencies 
//join customermanagement.logins on agencymanagement.agencies.ImplementationSpecialist = customermanagement.logins.Id ")
//            .AppendFormat("where agencymanagement.agencies.IsDeprecated = 0 {0}", implementationSpecialistSql)
//            .Append("and Date(agencymanagement.agencies.created) between Date(@startdate) and Date(@enddate) ")
//            .Append("order by agencymanagement.agencies.Created desc;")
//            .ToString();

//            using (var cmd = new FluentCommand<AgencySpecialist>(sql))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
//                    .AddDateTime("startdate", startDate)
//                    .AddDateTime("enddate", endDate)
//                    .SetMap(reader => new AgencySpecialist
//                    {
//                        AgencyId = reader.GetGuid("AgencyId"),
//                        AgencyName = reader.GetString("AgencyName"),
//                        AgencyCreated = reader.GetDateTime("Created"),
//                        SpecialistName = reader.GetStringNullable("DisplayName"),
//                        SpecialistEmailAddress = reader.GetStringNullable("EmailAddress"),
//                        SpecialistId = reader.GetGuidIncludeEmpty("ImplementationSpecialist"),
//                        AgencyContactPersonPhone = reader.GetStringNullable("ContactPersonPhone").ToPhone(),
//                        AgencyContactPersonEmail = reader.GetStringNullable("ContactPersonEmail"),
//                        AgencyContactDisplayName = string.Format("{0} {1}", reader.GetStringNullable("ContactPersonFirstName"), reader.GetStringNullable("ContactPersonLastName"))
//                    })
//                    .AsList();
//            }

//            return list;
//        }

//        public AgencySpecialist GetImplementationSpecialist(Guid agencyId, int clusterId)
//        {
//            AgencySpecialist specialist = null;
//            var sql = new StringBuilder(@"SELECT 
//                 agencymanagement.agencies.Id as AgencyId, 
//                 agencymanagement.agencies.Name as AgencyName,
//                 agencymanagement.agencies.Created,
//                 agencymanagement.agencies.ImplementationSpecialist,
//                 axxessmembership.logins.DisplayName 
//                    from
//                        agencymanagement.agencies  
//                            join axxessmembership.logins on agencymanagement.agencies.ImplementationSpecialist = axxessmembership.logins.Id 
//                                where
//                                    agencymanagement.agencies.IsDeprecated = 0 and
//                                    agencymanagement.agencies.Id = @agencyid;")
//            .ToString();

//            using (var cmd = new FluentCommand<AgencySpecialist>(sql))
//            {
//                specialist = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
//                    .AddGuid("agencyid", agencyId)
//                    .SetMap(reader => new AgencySpecialist
//                    {
//                        AgencyId = reader.GetGuid("AgencyId"),
//                        AgencyName = reader.GetString("AgencyName"),
//                        AgencyCreated = reader.GetDateTime("Created"),
//                        SpecialistName = reader.GetStringNullable("DisplayName"),
//                        SpecialistId = reader.GetGuidIncludeEmpty("ImplementationSpecialist")
//                    })
//                    .AsSingle();
//            }

//            return specialist;
//        }

//        public IList<StringVariable> GetAgencyUpgrades(int clusterId)
//        {
//            List<StringVariable> list = new List<StringVariable>();
//            string sql = "SELECT Id, Name FROM `agencies` WHERE AnnualPlanId > 0;";
//            using (var fluentCommand = new FluentCommand<StringVariable>(sql))
//            {
//                list.AddRange(fluentCommand
//                    .SetConnection("AgencyManagementConnectionString" + clusterId)
//                    .SetMap(reader => new StringVariable
//                    {
//                        Value = reader.GetStringNullable("Id"),
//                        Name = reader.GetStringNullable("Name")
//                    }).AsList());

//            }
//            return list;
//        }


//        #endregion


    }
}
