﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Enums;
   


   public class ScheduleRepository : IScheduleRepository
    {
         #region Constructor

        private readonly SimpleRepository database;

        public ScheduleRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region Add Schedule

        public bool AddScheduleEvent(ScheduleEvent scheduleEvent)
        {
            bool result = false;
            if (scheduleEvent != null)
            {
                try
                {
                    database.Add<ScheduleEvent>(scheduleEvent);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        #endregion

        #region Update Schedule

        public bool UpdateScheduleEventNew(ScheduleEvent scheduleEvent)
        {
            bool result = false;
            if (scheduleEvent != null)
            {
                try
                {
                    database.Update<ScheduleEvent>(scheduleEvent);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        #endregion

        #region Remove Schedule

        public bool RemoveScheduleEventNew(ScheduleEvent scheduleEvent)
        {
            var result = 0;
            var script = "DELETE FROM  scheduleevents WHERE AgencyId = @agencyid AND PatientId = @patientid AND EpisodeId = @episodeid AND EventId = @eventId ";
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", scheduleEvent.AgencyId)
                .AddGuid("patientid", scheduleEvent.PatientId)
                .AddGuid("episodeid", scheduleEvent.EpisodeId)
                .AddGuid("eventId", scheduleEvent.EventId).AsNonQuery();

            }
            return result > 0;
        }

        public bool RemoveScheduleEventNew(Guid agencyId, Guid patientId, Guid episodeId, Guid eventId)
        {
            var result = 0;
            var script = "DELETE FROM  scheduleevents WHERE AgencyId = @agencyid AND PatientId = @patientid AND EpisodeId = @episodeid AND EventId = @eventId ";
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddGuid("eventId", eventId).AsNonQuery();

            }
            return result > 0;
        }

        #endregion

        #region Get Schedule

        public ScheduleEvent GetScheduleEventNew(Guid agencyId, Guid patientId, Guid eventId)
        {
            return database.Single<ScheduleEvent>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.EventId == eventId);
        }

        public ScheduleEvent GetScheduleEventNew(Guid agencyId, Guid patientId, Guid episodeId, Guid eventId)
        {
            return database.Single<ScheduleEvent>(a => a.AgencyId == agencyId && a.PatientId == patientId && a.EpisodeId == episodeId && a.EventId == eventId);
        }

        public ScheduleEvent GetLastScheduledEvents(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        EventId as EventId ,
                        PatientId as PatientId ,
                        EpisodeId as EpisodeId ,
                        UserId as UserId ,
                        DisciplineTask as DisciplineTask , 
                        EventDate as EventDate ,
                        VisitDate as VisitDate ,
                        Status as Status ,
                        Discipline as Discipline ,
                        IsBillable as IsBillable ,
                        IsMissedVisit as IsMissedVisit , 
                        TimeIn as TimeIn , 
                        TimeOut as TimeOut ,
                        IsDeprecated as IsDeprecated , 
                        IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        IsVisitPaid as IsVisitPaid ,
                        InPrintQueue as InPrintQueue  
                            FROM
                                scheduleevents 
                                    WHERE
                                        AgencyId = @agencyid AND 
                                        PatientId = @patientid AND
                                        EpisodeId = @episodeid AND
                                        IsDeprecated = 0 AND
                                        DisciplineTask > 0 AND
                                        IsMissedVisit = 0  AND
                                        DATE(EventDate) between  DATE(@episodestartdate) and DATE(@episodeenddate) AND 
                                        DATE(EventDate) between DATE(@startdate) and DATE(@enddate) {0}  
                                            ORDER BY DATE(EventDate) DESC LIMIT 1 ", additionalScript);


            var scheduleEvent = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("episodestartdate", episodeStartDate)
                .AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = startDate,
                    EndDate = endDate
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public ScheduleEvent GetFirstScheduledEvents(Guid agencyId, Guid episodeId, Guid patientId, DateTime episodeStartDate, DateTime episodeEndDate, DateTime startDate, DateTime endDate, int[] disciplineTasks)
        {
            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        EventId as EventId ,
                        PatientId as PatientId ,
                        EpisodeId as EpisodeId ,
                        UserId as UserId ,
                        DisciplineTask as DisciplineTask , 
                        EventDate as EventDate ,
                        VisitDate as VisitDate ,
                        Status as Status ,
                        Discipline as Discipline ,
                        IsBillable as IsBillable ,
                        IsMissedVisit as IsMissedVisit , 
                        TimeIn as TimeIn , 
                        TimeOut as TimeOut ,
                        IsVisitPaid as IsVisitPaid  
                            FROM 
                                scheduleevents  
                                    WHERE 
                                        AgencyId = @agencyid AND 
                                        PatientId = @patientid AND
                                        EpisodeId = @episodeid AND
                                        IsDeprecated = 0 AND 
                                        DisciplineTask > 0 AND 
                                        IsMissedVisit = 0  AND 
                                        DATE(EventDate) between  DATE(@episodestartdate) and DATE(@episodeenddate) AND
                                        DATE(EventDate) between DATE(@startdate) and DATE(@enddate) {0} 
                                            ORDER BY DATE(EventDate) ASC LIMIT 1 ", additionalScript);


            var scheduleEvent = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleEvent = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddDateTime("episodestartdate", episodeStartDate)
                .AddDateTime("episodeenddate", episodeEndDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    StartDate = episodeStartDate,
                    EndDate = episodeEndDate
                })
                .AsSingle();
            }
            return scheduleEvent;
        }

        public ScheduleEvent FirstBillableEvent(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var status = ScheduleStatusFatory.OASISAndNurseNotesAfterQA().Select(s => s.ToString()).ToArray();// new string[] { ((int)ScheduleStatus.NoteCompleted).ToString(), ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), ((int)ScheduleStatus.OasisExported).ToString(), ((int)ScheduleStatus.OasisCompletedNotExported).ToString() };
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline  
                            FROM 
                                scheduleevents
                                    WHERE 
                                        scheduleevents.AgencyId = @agencyid AND 
                                        scheduleevents.PatientId = @patientid  AND
                                        scheduleevents.EpisodeId = @episodeid AND 
                                        scheduleevents.IsDeprecated = 0 AND 
                                        scheduleevents.IsBillable = 1 AND  
                                        scheduleevents.IsMissedVisit = 0  AND
                                        DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                        scheduleevents.Status IN ( {0} ) 
                                            ORDER BY 
                                                 DATE(scheduleevents.VisitDate) ASC LIMIT 1", status.Join(","));

            var schedule = new ScheduleEvent();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                schedule = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsSingle();
            }
            return schedule;
        }

        #endregion

        #region Get Schedules

//        public List<ScheduleEvent> GetPatientScheduledEventsNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline, bool isReportAndNotesIncluded)
//        {
//            var disciplineScript = string.Empty;
//            if (discipline.IsEqual("all"))
//            {

//            }
//            else if (discipline.IsEqual("Therapy"))
//            {
//                disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'PT') = 1 OR STRCMP( scheduleevents.Discipline, 'OT') = 1 OR STRCMP( scheduleevents.Discipline , 'ST') = 1 ) ";
//            }
//            else
//            {
//                if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
//                {
//                    disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'Nursing') = 1 OR STRCMP( scheduleevents.Discipline, 'ReportsAndNotes') = 1) ";
//                }
//                else
//                {
//                    disciplineScript = " AND scheduleevents.Discipline = @discipline ";
//                }
//            }
//            var script = string.Format(@"SELECT 
//                        scheduleevents.EventId as EventId ,
//                        scheduleevents.PatientId as PatientId ,
//                        scheduleevents.EpisodeId as EpisodeId ,
//                        scheduleevents.UserId as UserId ,
//                        scheduleevents.DisciplineTask as DisciplineTask , 
//                        scheduleevents.EventDate as EventDate ,
//                        scheduleevents.VisitDate as VisitDate ,
//                        scheduleevents.Status as Status ,
//                        scheduleevents.Discipline as Discipline ,
//                        scheduleevents.IsMissedVisit as IsMissedVisit , 
//                        scheduleevents.ReturnReason as ReturnReason ,
//                        scheduleevents.Comments as Comments ,
//                        scheduleevents.StartDate as StartDate ,
//                        scheduleevents.EndDate as EndDate ,
//                        scheduleevents.Asset as Asset ,
//                        scheduleevents.Version  as Version ,
//                        scheduleevents.UserName  as UserName ,
//                        users.FirstName as FirstName , 
//                        users.LastName as LastName ,
//                        users.Suffix as Suffix , 
//                        users.Credentials as Credentials ,
//                        users.CredentialsOther as CredentialsOther ,
//                        users.IsDeprecated as UserIsDeprecated
//                            FROM 
//                                scheduleevents 
//                                    LEFT JOIN users ON scheduleevents.UserId = users.Id 
//                                        WHERE 
//                                            scheduleevents.AgencyId = @agencyid AND
//                                            scheduleevents.PatientId = @patientid AND 
//                                            scheduleevents.EpisodeId = @episodeid AND 
//                                            scheduleevents.IsDeprecated = 0 AND
//                                            scheduleevents.DisciplineTask > 0 {0}  
//                                            ORDER BY DATE(scheduleevents.EventDate) DESC  ", disciplineScript);

//            var list = new List<ScheduleEvent>();
//            using (var cmd = new FluentCommand<ScheduleEvent>(script))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("patientid", patientId)
//                .AddGuid("episodeid", episodeId)
//                .AddString("discipline", discipline)
//                .SetMap(reader => new ScheduleEvent
//                {
//                    EventId = reader.GetGuid("EventId"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    EpisodeId = reader.GetGuid("EpisodeId"),
//                    UserId = reader.GetGuid("UserId"),
//                    DisciplineTask = reader.GetInt("DisciplineTask"),
//                    EventDate = reader.GetDateTime("EventDate"),
//                    VisitDate = reader.GetDateTime("VisitDate"),
//                    Status = reader.GetIntOrZero("Status"),
//                    Discipline = reader.GetStringNullable("Discipline"),
//                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
//                    ReturnReason = reader.GetStringNullable("ReturnReason"),
//                    Comments = reader.GetStringNullable("Comments"),
//                    StartDate = reader.GetDateTime("StartDate"),
//                    EndDate = reader.GetDateTime("EndDate"),
//                    Asset = reader.GetStringNullable("Asset"),
//                    Version = reader.GetInt("Version"),
//                    UserName = reader.GetStringNullable("UserName"),
//                    UserFirstName = reader.GetStringNullable("FirstName"),
//                    UserLastName = reader.GetStringNullable("LastName"),
//                    UserSuffix = reader.GetStringNullable("Suffix"),
//                    Credentials = reader.GetStringNullable("Credentials"),
//                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
//                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
//                })
//                .AsList();
//            }
//            return list;
//        }

        public List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty; ;

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsBillable as IsBillable ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.TimeIn as TimeIn , 
                        scheduleevents.TimeOut as TimeOut ,
                        scheduleevents.Surcharge as Surcharge ,
                        scheduleevents.AssociatedMileage as AssociatedMileage ,
                        scheduleevents.ReturnReason as ReturnReason ,
                        scheduleevents.Comments as Comments ,
                        scheduleevents.IsDeprecated as IsDeprecated , 
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        scheduleevents.IsVisitPaid as IsVisitPaid ,
                        scheduleevents.InPrintQueue as InPrintQueue ,
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate ,
                        scheduleevents.Version  as Version ,
                        scheduleevents.UserName  as UserName ,
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 AND 
                                            patientepisodes.IsActive = 1  AND 
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0}
                                                ORDER BY DATE(scheduleevents.EventDate) DESC ", additionalScript); //AND ( DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) OR ( DATE(@startdate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND DATE(@enddate)  between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)) OR ( DATE(patientepisodes.StartDate)  between  DATE(@startdate) and  DATE(@enddate)  AND DATE(patientepisodes.EndDate)  between DATE(@startdate) and  DATE(@enddate) ))

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Surcharge = reader.GetStringNullable("Surcharge"),
                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName"),
                })
                .AsList();
            }
            return list;
        }
        public List<ScheduleEvent> GetScheduledEventsOnlyLean(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] scheduleStatus, int[] disciplineTasks, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty; ;

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 AND 
                                            patientepisodes.IsActive = 1  AND 
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0}
                                                ORDER BY DATE(scheduleevents.EventDate) DESC ", additionalScript); //AND ( DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) OR DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) OR ( DATE(@startdate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND DATE(@enddate)  between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)) OR ( DATE(patientepisodes.StartDate)  between  DATE(@startdate) and  DATE(@enddate)  AND DATE(patientepisodes.EndDate)  between DATE(@startdate) and  DATE(@enddate) ))

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                })
                .AsList();
            }
            return list;
        }
        public List<ScheduleEvent> GetScheduledEventsOnly(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate)
        {
            return database.Find<ScheduleEvent>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.EpisodeId == episodeId && e.IsDeprecated == false && e.DisciplineTask > 0).Where(e => e.VisitDate.Date >= startDate.Date && e.VisitDate.Date <= endDate.Date).OrderBy(s => s.VisitDate.Date).ToList();

        }
        public List<ScheduleEvent> GetPastDueRecertsLeanByDateRange(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var insurance = string.Empty;
            var list = new List<ScheduleEvent>();
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", location.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                    else
                    {
                        var agency = database.Single<Agency>(l => l.Id == agencyId);
                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", agency.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} )", insuranceId);
            }

            var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks().ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
            var dischargeStatus = ScheduleStatusFatory.OASISCompleted().ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };

            var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments().ToArray();// new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISBResumptionofCare };
            var recertAndROCStatus = ScheduleStatusFatory.OASISCompleted().ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

            var script1 = string.Format(@"SELECT 
                    '{4}' as EventId ,
                    patientepisodes.PatientId as PatientId ,
                    patientepisodes.Id as EpisodeId ,
                    '{4}' as UserId ,
                    {6} as DisciplineTask , 
                    '{5}' as EventDate ,
                    '{5}' as VisitDate ,
                    '{7}' as Status ,
                    patientepisodes.StartDate as StartDate, 
                    patientepisodes.EndDate as EndDate , 
                    patients.FirstName as FirstName ,
                    patients.LastName as LastName ,
                    patients.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes 
                                INNER JOIN patients ON patientepisodes.PatientId = patients.Id  
                                    WHERE 
                                           patients.AgencyId = @agencyid  AND
                                           patients.Status = 1 {0} {1} AND 
                                           patients.IsDeprecated = 0 AND 
                                           patientepisodes.IsActive = 1 AND 
                                           patientepisodes.IsDischarged = 0 AND 
                                           DATEDIFF( patientepisodes.EndDate , patientepisodes.StartDate ) = 59 AND
                                           DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           Not Exists ( SELECT * from scheduleevents WHERE scheduleevents.PatientId = patients.Id AND scheduleevents.EpisodeId = patientepisodes.Id AND DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND scheduleevents.DisciplineTask > 0 AND scheduleevents.IsDeprecated = 0 AND  scheduleevents.IsMissedVisit = 0 AND ( {2} OR  {3} ) )",
                                                                                   insurance,
                                                                                   !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchid " : string.Empty,
                                                                                   string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                   Guid.Empty.ToString(),
                                                                                   DateTime.MinValue,
                                                                                   (int)DisciplineTasks.OASISCRecertification,
                                                                                   -1);
            var script2 = string.Format(@"SELECT 
                    scheduleevents.EventId as EventId ,
                    scheduleevents.PatientId as PatientId ,
                    scheduleevents.EpisodeId as EpisodeId ,
                    scheduleevents.UserId as UserId ,
                    scheduleevents.DisciplineTask as DisciplineTask , 
                    scheduleevents.EventDate as EventDate ,
                    scheduleevents.VisitDate as VisitDate ,
                    scheduleevents.Status as Status ,
                    patientepisodes.StartDate as StartDate, 
                    patientepisodes.EndDate as EndDate , 
                    patients.FirstName as FirstName ,
                    patients.LastName as LastName ,
                    patients.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes 
                                INNER JOIN patients ON patientepisodes.PatientId = patients.Id  
                                LEFT JOIN scheduleevents ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    WHERE 
                                           patients.AgencyId = @agencyid  AND
                                           patients.Status = 1 {0} {1} AND 
                                           patients.IsDeprecated = 0 AND 
                                           patientepisodes.IsActive = 1 AND 
                                           patientepisodes.IsDischarged = 0 AND 
                                           DATE(patientepisodes.EndDate) < DATE(curdate()) AND 
                                           DATEDIFF( patientepisodes.EndDate , patientepisodes.StartDate ) = 59 AND
                                           DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                           scheduleevents.DisciplineTask > 0 AND
                                           scheduleevents.IsDeprecated = 0 AND 
                                           scheduleevents.IsMissedVisit = 0 AND
                                           ( {2} OR  {3} )", insurance, !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchid " : string.Empty, string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")), string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")));

            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1} ORDER BY EndDate DESC", script1, script2)))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                 .AddGuid("branchid", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }
        public List<ScheduleEvent> GetUpcomingRecertsLean(Guid agencyId, Guid branchId, int insuranceId, DateTime startDate, DateTime endDate)
        {
            var insurance = string.Empty;
            var list = new List<ScheduleEvent>();
            if (insuranceId < 0)
            {
                return list;
            }
            else if (insuranceId == 0)
            {
                if (!branchId.IsEmpty())
                {
                    var location = database.Single<AgencyLocation>(l => l.AgencyId == agencyId && l.Id == branchId);
                    if (location != null && location.IsLocationStandAlone)
                    {
                        if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", location.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                    else
                    {
                        var agency = database.Single<Agency>(l => l.Id == agencyId);
                        if (agency != null && agency.Payor.IsNotNullOrEmpty() && agency.Payor.IsInteger())
                        {
                            insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance = {0} || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance = {0} ||  patients.TertiaryInsurance >= 1000 )", agency.Payor);
                        }
                        else
                        {
                            insurance = "AND ( patients.PrimaryInsurance >= 1000 || patients.SecondaryInsurance >= 1000 || patients.TertiaryInsurance >= 1000 )";
                        }
                    }
                }
            }
            else
            {
                insurance = string.Format("AND ( patients.PrimaryInsurance = {0} || patients.SecondaryInsurance = {0} || patients.TertiaryInsurance = {0} )", insuranceId);
            }

            var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks().ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
            var dischargeStatus = ScheduleStatusFatory.OASISCompleted().ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };

            var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments().ToArray(); //new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISBResumptionofCare };
            var recertAndROCStatus = ScheduleStatusFatory.OASISCompleted().ToArray(); //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };

            var script1 = string.Format(@"SELECT 
                    '{4}' as EventId ,
                    patientepisodes.PatientId as PatientId ,
                    patientepisodes.Id as EpisodeId ,
                    '{4}' as UserId ,
                    {6} as DisciplineTask , 
                    '{5}' as EventDate ,
                    '{5}' as VisitDate ,
                    '{7}' as Status ,
                    patientepisodes.StartDate as StartDate, 
                    patientepisodes.EndDate as EndDate , 
                    patients.FirstName as FirstName ,
                    patients.LastName as LastName ,
                    patients.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes 
                                INNER JOIN patients ON patientepisodes.PatientId = patients.Id  
                                    WHERE 
                                           patients.AgencyId = @agencyid  AND
                                           patients.Status = 1 {0} {1} AND 
                                           patients.IsDeprecated = 0 AND 
                                           patientepisodes.IsActive = 1 AND 
                                           patientepisodes.IsDischarged = 0 AND 
                                           DATEDIFF( patientepisodes.EndDate , patientepisodes.StartDate ) = 59 AND
                                           DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           Not Exists ( SELECT * from scheduleevents WHERE scheduleevents.PatientId = patients.Id AND scheduleevents.EpisodeId = patientepisodes.Id AND DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND scheduleevents.DisciplineTask > 0 AND scheduleevents.IsDeprecated = 0 AND  scheduleevents.IsMissedVisit = 0 AND ( {2} OR  {3} ) )",
                                                                                     insurance,
                                                                                     !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchid " : string.Empty,
                                                                                     string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                     string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")),
                                                                                     Guid.Empty.ToString(),
                                                                                     DateTime.MinValue,
                                                                                     (int)DisciplineTasks.OASISCRecertification,
                                                                                     -1);
            var script2 = string.Format(@"SELECT 
                    scheduleevents.EventId as EventId ,
                    scheduleevents.PatientId as PatientId ,
                    scheduleevents.EpisodeId as EpisodeId ,
                    scheduleevents.UserId as UserId ,
                    scheduleevents.DisciplineTask as DisciplineTask , 
                    scheduleevents.EventDate as EventDate ,
                    scheduleevents.VisitDate as VisitDate ,
                    scheduleevents.Status as Status ,
                    patientepisodes.StartDate as StartDate, 
                    patientepisodes.EndDate as EndDate , 
                    patients.FirstName as FirstName ,
                    patients.LastName as LastName ,
                    patients.PatientIdNumber as PatientIdNumber
                        FROM patientepisodes 
                                INNER JOIN patients ON patientepisodes.PatientId = patients.Id  
                                LEFT JOIN scheduleevents ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    WHERE 
                                           patients.AgencyId = @agencyid  AND
                                           patients.Status = 1 {0} {1} AND 
                                           patients.IsDeprecated = 0 AND 
                                           patientepisodes.IsActive = 1 AND 
                                           patientepisodes.IsDischarged = 0 AND 
                                           DATEDIFF( patientepisodes.EndDate , patientepisodes.StartDate ) = 59 AND
                                           DATE(patientepisodes.EndDate) between DATE(@startdate) and DATE(@enddate) AND
                                           DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                           scheduleevents.DisciplineTask > 0 AND
                                           scheduleevents.IsDeprecated = 0 AND 
                                           scheduleevents.IsMissedVisit = 0 AND
                                           ( {2} OR  {3} ) ", insurance, !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchid " : string.Empty, string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND  DATE(scheduleevents.EventDate) between DATE(DATE_SUB(patientepisodes.EndDate, INTERVAL 5 DAY)) and DATE(patientepisodes.EndDate) ) ", recertAndROCDisciplineTasks.Select(d => d.ToString()).ToArray().Join(",")), string.Format(" ( scheduleevents.DisciplineTask IN ( {0} ) AND scheduleevents.Status  IN ( {1} ) )", dischargeDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","), dischargeStatus.Select(d => d.ToString()).ToArray().Join(",")));

            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1} ORDER BY EndDate DESC", script1, script2)))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                 .AddGuid("branchid", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return list;
        }
        public List<ScheduleEvent> GetPlanOfCareOrderScheduleEvents(Guid agencyId, DateTime startDate, DateTime endDate, int[] disciplineTasks, int[] status)
        {

            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} )", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (status != null && status.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Status IN ( {0} )", status.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            patients.IsDeprecated = 0 AND
                                            patients.Status IN (1,2) AND
                                            scheduleevents.IsDeprecated = 0  AND 
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} ", additionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }
        public List<ScheduleEvent> GetTherapyExceptionScheduleEvents(Guid agencyId, Guid branchId, DateTime fromDate, DateTime toDate)
        {
            var disciplines = DisciplineFactory.TherapyDisciplines().Select(d => d.ToString()).ToArray();// new string[] { Disciplines.PT.ToString(), Disciplines.OT.ToString(), Disciplines.ST.ToString() };
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        patientepisodes.EndDate as EndDate,
                        patientepisodes.StartDate as StartDate,
                        patients.FirstName as FirstName, 
                        patients.LastName as LastName, 
                        patients.PatientIdNumber as PatientIdNumber 
                                 FROM scheduleevents INNER JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id INNER JOIN patients ON scheduleevents.PatientId = patients.Id    
                                      WHERE 
                                            scheduleevents.AgencyId = @agencyid {0} AND
                                            patients.IsDeprecated = 0 AND 
                                            patients.Status = 1 AND
                                            patientepisodes.IsActive = 1 AND 
                                            patientepisodes.IsDischarged = 0 AND 
                                            DATE(patientepisodes.StartDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            scheduleevents.IsDeprecated = 0  AND
                                            scheduleevents.IsMissedVisit = 0 AND 
                                            scheduleevents.Discipline IN ( {1} )", !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId" : string.Empty, disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", fromDate)
                .AddDateTime("enddate", toDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;
        }
        public List<ScheduleEvent> GetPhysicianOrderScheduleEvents(Guid agencyId, DateTime startDate, DateTime endDate, int[] status)
        {
            var disciplineTasks = new int[] { (int)DisciplineTasks.PhysicianOrder };

            var additionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} )", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (status != null && status.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Status IN ( {0} ) ", status.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            patients.IsDeprecated = 0 AND 
                                            patients.Status IN (1,2) AND 
                                            scheduleevents.IsDeprecated = 0  AND 
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0} ", additionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }
        public List<ScheduleEvent> GetOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, List<int> status)
        {
            var disciplineTasks = DisciplineTaskFactory.AllOrders().ToArray();//new int[] { (int)DisciplineTasks.FaceToFaceEncounter, (int)DisciplineTasks.HCFA485, (int)DisciplineTasks.NonOasisHCFA485, (int)DisciplineTasks.PhysicianOrder, (int)DisciplineTasks.PTEvaluation, (int)DisciplineTasks.OTEvaluation, (int)DisciplineTasks.STEvaluation, (int)DisciplineTasks.PTReEvaluation, (int)DisciplineTasks.OTReEvaluation, (int)DisciplineTasks.STReEvaluation };
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND
                                            patients.IsDeprecated = 0 AND 
                                            patients.Status IN (1,2) AND
                                            scheduleevents.IsDeprecated = 0  AND
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            scheduleevents.DisciplineTask IN ( {0} ) AND
                                            scheduleevents.Status IN ( {1} ) {2}", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","), !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId" : string.Empty);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }
        public List<ScheduleEvent> GetPatientOrderScheduleEvents(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var disciplineTasks = DisciplineTaskFactory.AllOrders().Select(s => s.ToString()).ToArray();// new string[] { ((int)DisciplineTasks.FaceToFaceEncounter).ToString(), ((int)DisciplineTasks.HCFA485StandAlone).ToString(), ((int)DisciplineTasks.HCFA485).ToString(), ((int)DisciplineTasks.NonOasisHCFA485).ToString(), ((int)DisciplineTasks.PhysicianOrder).ToString(), ((int)DisciplineTasks.PTEvaluation).ToString(), ((int)DisciplineTasks.OTEvaluation).ToString(), ((int)DisciplineTasks.STEvaluation).ToString(), ((int)DisciplineTasks.PTReEvaluation).ToString(), ((int)DisciplineTasks.OTReEvaluation).ToString(), ((int)DisciplineTasks.STReEvaluation).ToString() };
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid  AND
                                            scheduleevents.IsDeprecated = 0  AND 
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            scheduleevents.DisciplineTask IN ( {0} )", disciplineTasks.Join(","));

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientId", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }
        public List<ScheduleEvent> GetPendingSignatureOrderScheduleEvents(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
        {
            var disciplineTasks = DisciplineTaskFactory.AllOrders().Select(s => s.ToString()).ToArray();// new string[] { ((int)DisciplineTasks.FaceToFaceEncounter).ToString(), ((int)DisciplineTasks.HCFA485).ToString(), ((int)DisciplineTasks.NonOasisHCFA485).ToString(), ((int)DisciplineTasks.PhysicianOrder).ToString(), ((int)DisciplineTasks.HCFA485StandAlone).ToString(), ((int)DisciplineTasks.PTEvaluation).ToString(), ((int)DisciplineTasks.OTEvaluation).ToString(), ((int)DisciplineTasks.STEvaluation).ToString(), ((int)DisciplineTasks.PTReEvaluation).ToString(), ((int)DisciplineTasks.OTReEvaluation).ToString(), ((int)DisciplineTasks.STReEvaluation).ToString() };
            var status = ScheduleStatusFatory.OrdersPendingPhysicianSignature().Select(s => s.ToString()).ToArray();// new string[] { ((int)ScheduleStatus.OrderSentToPhysicianElectronically).ToString(), ((int)ScheduleStatus.OrderSentToPhysician).ToString(), ((int)ScheduleStatus.EvalSentToPhysician).ToString(), ((int)ScheduleStatus.EvalSentToPhysicianElectronically).ToString() };

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid  AND
                                            patients.IsDeprecated = 0 AND 
                                            patients.Status IN (1,2)  AND
                                            scheduleevents.IsDeprecated = 0  AND 
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            scheduleevents.DisciplineTask IN ( {0} ) AND
                                            scheduleevents.Status IN ( {1} ) {2}", disciplineTasks.Join(","), status.Join(","), !branchId.IsEmpty() ? " AND patients.AgencyLocationId = @branchId" : string.Empty);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode")
                })
                .AsList();
            }
            return scheduleList;
        }
        public List<ScheduleEvent> GetEpisodesForSurveyCensesAndPatientRoster(Guid agencyId, string patientIds)
        {
            var disciplineTasksOASISList = DisciplineTaskFactory.EpisodeAllAssessments().ToArray();
            var list = new List<ScheduleEvent>();
            var script = string.Format(@"SELECT 
                                            agencymanagement.scheduleevents.EventId as EventId ,
                                            agencymanagement.patientepisodes.PatientId as PatientId,
                                            agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                                            agencymanagement.patientepisodes.StartDate as StartDate, 
                                            agencymanagement.patientepisodes.EndDate as EndDate,
                                            agencymanagement.scheduleevents.UserId as UserId ,
                                            agencymanagement.scheduleevents.DisciplineTask as DisciplineTask , 
                                            agencymanagement.scheduleevents.EventDate as EventDate ,
                                            agencymanagement.scheduleevents.VisitDate as VisitDate ,
                                            agencymanagement.scheduleevents.Status as Status ,
                                            agencymanagement.scheduleevents.Discipline as Discipline ,
                                            oasisc.assessments.OasisData as Note 
                                                    FROM  agencymanagement.patientepisodes 
                                                           LEFT JOIN agencymanagement.scheduleevents ON agencymanagement.scheduleevents.EpisodeId = agencymanagement.patientepisodes.Id 
                                                           LEFT JOIN oasisc.assessments ON oasisc.assessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.assessments.EpisodeId = agencymanagement.scheduleevents.EpisodeId AND oasisc.assessments.Id = agencymanagement.scheduleevents.EventId AND agencymanagement.scheduleevents.DisciplineTask IN ( {1} )
                                                                WHERE 
                                                                     agencymanagement.patientepisodes.AgencyId = @agencyid AND 
                                                                     agencymanagement.patientepisodes.PatientId IN ( {0} ) AND
                                                                     agencymanagement.patientepisodes.IsActive = 1 AND 
                                                                     agencymanagement.patientepisodes.IsDischarged = 0 AND 
                                                                     agencymanagement.scheduleevents.DisciplineTask > 0 AND
                                                                     agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                                                     agencymanagement.scheduleevents.IsMissedVisit = 0 AND
                                                                     DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND
                                                                     DATE(agencymanagement.patientepisodes.StartDate) < Date(@startdate)", patientIds, disciplineTasksOASISList.Select(d => d.ToString()).ToArray().Join(","));

            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", DateTime.Now)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    Note = reader.GetStringNullable("Note")

                }).AsList();
            }
            return list;
        }
        public List<ScheduleEvent> GetPatientScheduledEventsOnlyNew(Guid agencyId, Guid episodeId, Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        EventId as EventId ,
                        PatientId as PatientId ,
                        EpisodeId as EpisodeId ,
                        UserId as UserId ,
                        DisciplineTask as DisciplineTask , 
                        EventDate as EventDate ,
                        VisitDate as VisitDate ,
                        Status as Status ,
                        Discipline as Discipline ,
                        IsBillable as IsBillable ,
                        IsMissedVisit as IsMissedVisit , 
                        TimeIn as TimeIn , 
                        TimeOut as TimeOut ,
                        IsDeprecated as IsDeprecated , 
                        IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        IsVisitPaid as IsVisitPaid ,
                        InPrintQueue as InPrintQueue ,
                        StartDate as StartDate ,
                        EndDate as EndDate  
                            FROM 
                                scheduleevents 
                                    WHERE 
                                        AgencyId = @agencyid AND
                                        PatientId = @patientid AND 
                                        EpisodeId = @episodeid AND
                                        IsDeprecated = 0 AND
                                        DisciplineTask > 0 
                                            ORDER BY DATE(EventDate) DESC ");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate")
                })
                .AsList();
            }
            return list;
        }
//        public List<ScheduleEvent> GetPatientScheduledEventsWithDateRangeOrNotNew(Guid agencyId, Guid episodeId, Guid patientId, bool IsDateRange, DateTime startDate, DateTime endDate)
//        {
//            var dateRange = IsDateRange ? " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
//            var script = string.Format(@"SELECT 
//                        scheduleevents.EventId as EventId ,
//                        scheduleevents.PatientId as PatientId ,
//                        scheduleevents.EpisodeId as EpisodeId ,
//                        scheduleevents.UserId as UserId ,
//                        scheduleevents.DisciplineTask as DisciplineTask , 
//                        scheduleevents.EventDate as EventDate ,
//                        scheduleevents.VisitDate as VisitDate ,
//                        scheduleevents.Status as Status ,
//                        scheduleevents.Discipline as Discipline ,
//                        scheduleevents.IsBillable as IsBillable ,
//                        scheduleevents.IsMissedVisit as IsMissedVisit , 
//                        scheduleevents.TimeIn as TimeIn , 
//                        scheduleevents.TimeOut as TimeOut ,
//                        scheduleevents.Surcharge as Surcharge ,
//                        scheduleevents.AssociatedMileage as AssociatedMileage ,
//                        scheduleevents.ReturnReason as ReturnReason ,
//                        scheduleevents.Comments as Comments ,
//                        scheduleevents.IsDeprecated as IsDeprecated , 
//                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode , 
//                        scheduleevents.IsVisitPaid as IsVisitPaid ,
//                        scheduleevents.InPrintQueue as InPrintQueue ,
//                        scheduleevents.StartDate as StartDate ,
//                        scheduleevents.EndDate as EndDate ,
//                        scheduleevents.Asset as Asset ,
//                        scheduleevents.Version  as Version ,
//                        scheduleevents.UserName  as UserName ,
//                        users.FirstName as FirstName , 
//                        users.LastName as LastName ,
//                        users.Suffix as Suffix , 
//                        users.Credentials as Credentials ,
//                        users.CredentialsOther as CredentialsOther ,
//                        users.IsDeprecated as UserIsDeprecated 
//                            FROM
//                                scheduleevents 
//                                    LEFT JOIN users ON scheduleevents.UserId = users.Id
//                                        WHERE 
//                                            scheduleevents.AgencyId = @agencyid AND 
//                                            scheduleevents.PatientId = @patientid AND 
//                                            scheduleevents.EpisodeId = @episodeid AND 
//                                            scheduleevents.IsDeprecated = 0 AND 
//                                            scheduleevents.DisciplineTask > 0 {0} 
//                                                ORDER BY DATE(scheduleevents.EventDate) DESC ", dateRange);

//            var list = new List<ScheduleEvent>();
//            using (var cmd = new FluentCommand<ScheduleEvent>(script))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("patientid", patientId)
//                .AddGuid("episodeid", episodeId)
//                .AddDateTime("startdate", startDate)
//                .AddDateTime("enddate", endDate)
//                .SetMap(reader => new ScheduleEvent
//                {
//                    EventId = reader.GetGuid("EventId"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    EpisodeId = reader.GetGuid("EpisodeId"),
//                    UserId = reader.GetGuid("UserId"),
//                    DisciplineTask = reader.GetInt("DisciplineTask"),
//                    EventDate = reader.GetDateTime("EventDate"),
//                    VisitDate = reader.GetDateTime("VisitDate"),
//                    Status = reader.GetIntOrZero("Status"),
//                    Discipline = reader.GetStringNullable("Discipline"),
//                    IsBillable = reader.GetBoolean("IsBillable"),
//                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
//                    TimeIn = reader.GetStringNullable("TimeIn"),
//                    TimeOut = reader.GetStringNullable("TimeOut"),
//                    Surcharge = reader.GetStringNullable("Surcharge"),
//                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
//                    ReturnReason = reader.GetStringNullable("ReturnReason"),
//                    Comments = reader.GetStringNullable("Comments"),
//                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
//                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
//                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
//                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
//                    StartDate = reader.GetDateTime("StartDate"),
//                    EndDate = reader.GetDateTime("EndDate"),
//                    Asset = reader.GetStringNullable("Asset"),
//                    Version = reader.GetInt("Version"),
//                    UserName = reader.GetStringNullable("UserName"),
//                    UserFirstName = reader.GetStringNullable("FirstName"),
//                    UserLastName = reader.GetStringNullable("LastName"),
//                    UserSuffix = reader.GetStringNullable("Suffix"),
//                    Credentials = reader.GetStringNullable("Credentials"),
//                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
//                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
//                })
//                .AsList();
//            }
//            return list;
//        }
//        public List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange)
//        {
//            var dateRange = IsDateRange ? " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
//            var disciplineScript = string.Empty;
//            if (discipline.IsEqual("all"))
//            {

//            }
//            else if (discipline.IsEqual("Therapy"))
//            {
//                disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'PT') = 1 OR STRCMP( scheduleevents.Discipline, 'OT') = 1 OR STRCMP( scheduleevents.Discipline , 'ST') = 1 ) ";
//            }
//            else
//            {
//                if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
//                {
//                    disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'Nursing') = 1 OR STRCMP( scheduleevents.Discipline, 'ReportsAndNotes') = 1) ";
//                }
//                else
//                {
//                    disciplineScript = " AND scheduleevents.Discipline = @discipline ";
//                }
//            }
//            var script = string.Format(@"SELECT 
//                        scheduleevents.EventId as EventId ,
//                        scheduleevents.PatientId as PatientId ,
//                        scheduleevents.EpisodeId as EpisodeId ,
//                        scheduleevents.UserId as UserId ,
//                        scheduleevents.EventDate as EventDate ,
//                        scheduleevents.VisitDate as VisitDate ,
//                        scheduleevents.Discipline as Discipline ,
//                        scheduleevents.DisciplineTask as DisciplineTask , 
//                        scheduleevents.Status as Status ,
//                        scheduleevents.IsBillable as IsBillable ,
//                        scheduleevents.IsMissedVisit as IsMissedVisit , 
//                        scheduleevents.ReturnReason as ReturnReason ,
//                        scheduleevents.Comments as Comments ,
//                        scheduleevents.Asset as Asset ,
//                        scheduleevents.UserName  as UserName ,
//                        users.FirstName as FirstName , 
//                        users.LastName as LastName ,
//                        users.Suffix as Suffix , 
//                        users.Credentials as Credentials ,
//                        users.CredentialsOther as CredentialsOther ,
//                        users.IsDeprecated as UserIsDeprecated 
//                            FROM 
//                                scheduleevents
//                                    LEFT JOIN users ON scheduleevents.UserId = users.Id
//                                        WHERE 
//                                            scheduleevents.AgencyId = @agencyid AND 
//                                            scheduleevents.PatientId = @patientid AND 
//                                            scheduleevents.EpisodeId = @episodeid AND 
//                                            scheduleevents.IsDeprecated = 0 AND 
//                                            scheduleevents.DisciplineTask > 0 {0} {1} 
//                                                ORDER BY 
//                                                    DATE(scheduleevents.EventDate) DESC ", disciplineScript, dateRange);

//            var list = new List<ScheduleEvent>();
//            using (var cmd = new FluentCommand<ScheduleEvent>(script))
//            {
//                list = cmd.SetConnection("AgencyManagementConnectionString")
//                .AddGuid("agencyid", agencyId)
//                .AddGuid("patientid", patientId)
//                .AddGuid("episodeid", episodeId)
//                .AddString("discipline", discipline)
//                .AddDateTime("startdate", startDate)
//                .AddDateTime("enddate", endDate)
//                .SetMap(reader => new ScheduleEvent
//                {
//                    EventId = reader.GetGuid("EventId"),
//                    PatientId = reader.GetGuid("PatientId"),
//                    EpisodeId = reader.GetGuid("EpisodeId"),
//                    UserId = reader.GetGuid("UserId"),
//                    EventDate = reader.GetDateTime("EventDate"),
//                    VisitDate = reader.GetDateTime("VisitDate"),
//                    Discipline = reader.GetStringNullable("Discipline"),
//                    DisciplineTask = reader.GetInt("DisciplineTask"),
//                    Status = reader.GetIntOrZero("Status"),
//                    IsBillable = reader.GetBoolean("IsBillable"),
//                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
//                    ReturnReason = reader.GetStringNullable("ReturnReason"),
//                    Comments = reader.GetStringNullable("Comments"),
//                    Asset = reader.GetStringNullable("Asset"),
//                    UserName = reader.GetStringNullable("UserName"),
//                    UserFirstName = reader.GetStringNullable("FirstName"),
//                    UserLastName = reader.GetStringNullable("LastName"),
//                    UserSuffix = reader.GetStringNullable("Suffix"),
//                    Credentials = reader.GetStringNullable("Credentials"),
//                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
//                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
//                })
//                .AsList();
//            }
//            return list;
//        }
        public List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange)
        {
            var dateRange = IsDateRange ? " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
            var disciplineScript = string.Empty;
            if (discipline.IsEqual("all")) disciplineScript = string.Empty;
            else if (discipline.IsEqual("Therapy")) disciplineScript = " AND (STRCMP(scheduleevents.Discipline ,'PT') = 1 OR STRCMP(scheduleevents.Discipline, 'OT') = 1 OR STRCMP(scheduleevents.Discipline , 'ST') = 1) ";
            else if (isReportAndNotesIncluded && discipline.IsEqual("Nursing")) disciplineScript = " AND (STRCMP(scheduleevents.Discipline ,'Nursing') = 1 OR STRCMP(scheduleevents.Discipline, 'ReportsAndNotes') = 1) ";
            else disciplineScript = " AND scheduleevents.Discipline = @discipline ";
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.ReturnReason as ReturnReason ,
                        scheduleevents.Comments as Comments ,
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate ,
                        scheduleevents.Asset as Asset ,
                        scheduleevents.Version  as Version ,
                        scheduleevents.UserName  as UserName ,
                        users.FirstName as FirstName , 
                        users.LastName as LastName ,
                        users.Suffix as Suffix , 
                        users.Credentials as Credentials ,
                        users.CredentialsOther as CredentialsOther ,
                        users.IsDeprecated as UserIsDeprecated
                            FROM 
                                scheduleevents 
                                    LEFT JOIN users ON scheduleevents.UserId = users.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.IsDeprecated = 0 AND
                                            scheduleevents.DisciplineTask > 0 {0} {1} 
                                            ORDER BY DATE(scheduleevents.EventDate) DESC  ", disciplineScript, dateRange);
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddString("discipline", discipline)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName"),
                    UserFirstName = reader.GetStringNullable("FirstName"),
                    UserLastName = reader.GetStringNullable("LastName"),
                    UserSuffix = reader.GetStringNullable("Suffix"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
                })
                .AsList();
            }
            return list;
        }
        public IList<ScheduleEvent> GetScheduledEventsByEmployeeAssignedNew(Guid agencyId, Guid patientId, Guid employeeId)
        {
            return database.Find<ScheduleEvent>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.UserId == employeeId && e.IsDeprecated == false && e.IsMissedVisit == false);
        }
        public IList<ScheduleEvent> GetScheduledEventsByEmployeeAssignedNew(Guid agencyId, Guid employeeId)
        {
            return database.Find<ScheduleEvent>(e => e.AgencyId == agencyId && e.UserId == employeeId && e.IsDeprecated == false && e.IsMissedVisit == false);
        }
        public List<ScheduleEvent> GetDeletedItemsNew(Guid agencyId, Guid patientId)
        {
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsBillable as IsBillable ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate ,
                        scheduleevents.Asset as Asset ,
                        scheduleevents.Version  as Version ,
                        scheduleevents.UserName  as UserName ,
                        users.FirstName as FirstName , 
                        users.LastName as LastName ,
                        users.Suffix as Suffix , 
                        users.Credentials as Credentials ,
                        users.CredentialsOther as CredentialsOther ,
                        users.IsDeprecated as UserIsDeprecated 
                            FROM
                                scheduleevents
                                    LEFT JOIN patientepisodes ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN users ON scheduleevents.UserId = users.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.IsDeprecated = 1 AND
                                            patientepisodes.IsActive = 1 AND 
                                            patientepisodes.IsDischarged = 0 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC");

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName"),
                    UserFirstName = reader.GetStringNullable("FirstName"),
                    UserLastName = reader.GetStringNullable("LastName"),
                    UserSuffix = reader.GetStringNullable("Suffix"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
                })
                .AsList();
            }
            return list;
        }
        public List<ScheduleEvent> GetPreviousNotes(Guid agencyId, ScheduleEvent scheduledEvent, int[] disciplineTasks, int[] status)
        {
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            scheduleevents.PatientId = @patientid  AND
                                            scheduleevents.EventId != @eventid  AND
                                            scheduleevents.IsMissedVisit = 0  AND
                                            scheduleevents.IsDeprecated = 0  AND
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            scheduleevents.DisciplineTask IN ({0}) AND
                                            scheduleevents.Status IN ({1}) 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC LIMIT 5 ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","));
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("eventid", scheduledEvent.EventId)
                .AddGuid("patientid", scheduledEvent.PatientId)
                .AddDateTime("startdate", scheduledEvent.EventDate.AddMonths(-4))
                .AddDateTime("enddate", scheduledEvent.EventDate)
                .SetDictonaryId("EventId")
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;
        }
        public List<ScheduleEvent> GetPreviousNotes(Guid agencyId, ScheduleEvent scheduledEvent, string[] disciplines, int[] disciplineTasks, int[] status)
        {
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id  
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            scheduleevents.PatientId = @patientid  AND
                                            scheduleevents.EventId != @eventid  AND
                                            scheduleevents.IsMissedVisit = 0 AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            patientepisodes.IsActive = 1 AND 
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  AND
                                            scheduleevents.DisciplineTask IN ({0}) AND
                                            scheduleevents.Status IN ({1}) AND
                                            scheduleevents.Discipline IN ({2}) 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC LIMIT 5 ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","), status.Select(s => s.ToString()).ToArray().Join(","), disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("eventid", scheduledEvent.EventId)
                .AddGuid("patientid", scheduledEvent.PatientId)
                .AddDateTime("startdate", scheduledEvent.EventDate.AddMonths(-4))
                .AddDateTime("enddate", scheduledEvent.EventDate)
                .SetDictonaryId("EventId")
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;
        }
        public List<ScheduleEvent> GetCurrentAndPerviousOrders(Guid agencyId, PatientEpisode episode, int[] disciplineTasks)
        {
            var episodeScript = " SELECT Id FROM patientepisodes WHERE PatientId = @patientid AND ( Id = @episodeid OR (Id != @episodeid AND DATE(EndDate) = DATE(@previousenddate))  AND IsActive = 1  AND IsDischarged = 0 ORDER BY DATE(StartDate) DESC LIMIT 2 ) ";
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate  
                            FROM
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id AND patientepisodes.Id IN ( {0} )
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid AND
                                            scheduleevents.IsMissedVisit = 0 AND
                                            scheduleevents.IsDeprecated = 0 AND 
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND 
                                            ( (scheduleevents.EpisodeId = @episodeid AND scheduleevents.IsOrderForNextEpisode = 0 ) OR (scheduleevents.EpisodeId != @episodeid AND scheduleevents.IsOrderForNextEpisode = 1 ) ) AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            scheduleevents.DisciplineTask IN ( {1} ) 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC  ", episodeScript, disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", episode.PatientId)
                .AddGuid("episodeid", episode.Id)
                .AddDateTime("previousenddate", episode.StartDate.AddDays(-1))
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate")
                })
                .AsList();
            }
            return list;

        }
        public List<ScheduleEvent> GetScheduleByBranchAndStatus(Guid agencyId, Guid branchId, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var status = string.Empty; ;
            if (patientStatus <= 0)
            {
                status = " AND  patients.Status IN (1,2)";
            }
            else
            {
                status = " AND patients.Status = @statusid";
            }
            var branch = string.Empty;

            if (!branchId.IsEmpty())
            {
                branch = " AND patients.AgencyLocationId = @branchId";
            }
            var missedVisit = string.Empty;
            if (!IsMissedVisitIncluded)
            {
                missedVisit = " AND scheduleevents.IsMissedVisit = 0 ";
            }
            var schedulleStatusScript = string.Empty;
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                schedulleStatusScript = string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.ReturnReason as ReturnReason ,
                        scheduleevents.Comments as Comments ,
                        patientepisodes.Details as Details ,
                        patients.LastName as LastName ,
                        patients.FirstName as FirstName 
 
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND
                                            patients.IsDeprecated = 0  {2} AND
                                            scheduleevents.IsDeprecated = 0  {3} AND
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0}  {1}", schedulleStatusScript, branch, status, missedVisit);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    EpisodeNotes = reader.GetStringNullable("Details"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;

        }
        public List<ScheduleEvent> GetScheduleByBranchDateRangeAndStatusLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var status = string.Empty; ;
            if (patientStatus <= 0)
            {
                status = " AND  patients.Status IN (1,2)";
            }
            else
            {
                status = " AND patients.Status = @statusid";
            }
            var branch = string.Empty;

            if (!branchId.IsEmpty())
            {
                branch = " AND patients.AgencyLocationId = @branchId";
            }
            var missedVisit = string.Empty;
            if (!IsMissedVisitIncluded)
            {
                missedVisit = " AND scheduleevents.IsMissedVisit = 0 ";
            }
            var schedulleStatusScript = string.Empty;
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                schedulleStatusScript = string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        patients.LastName as LastName ,
                        patients.FirstName as FirstName ,
                        patients.PatientIdNumber as PatientIdNumber
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND
                                            patients.IsDeprecated = 0  {2} AND
                                            scheduleevents.IsDeprecated = 0  {3} AND
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0  AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0}  {1}", schedulleStatusScript, branch, status, missedVisit);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader => new ScheduleEvent
                {
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return scheduleList;

        }
        public List<ScheduleEvent> GetScheduleByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND patients.Status = @statusid ";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND patients.AgencyLocationId = @branchId ";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        patients.PatientIdNumber as PatientIdNumber ,
                        patients.LastName as LastName ,
                        patients.FirstName as FirstName  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            patients.IsDeprecated = 0 AND 
                                            scheduleevents.IsDeprecated = 0 AND
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return scheduleList;

        }
        public List<ScheduleEvent> GetMissedVisitSchedulesLean(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND patients.Status IN (1,2)";
            }
            else
            {
                additonalScript += " AND patients.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND patients.AgencyLocationId = @branchId";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplineTasks.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            var script = string.Format(@"SELECT 
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        patients.PatientIdNumber as PatientIdNumber ,
                        patients.LastName as LastName ,
                        patients.FirstName as FirstName 
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND
                                            patients.IsDeprecated = 0 AND
                                            scheduleevents.IsDeprecated = 0 AND
                                            scheduleevents.IsMissedVisit = 1 AND 
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return scheduleList;

        }
        public List<ScheduleEvent> GetScheduleDeviations(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND patients.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND patients.AgencyLocationId = @branchId";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        patients.PatientIdNumber as PatientIdNumber ,
                        patients.LastName as LastName ,
                        patients.FirstName as FirstName  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            patients.IsDeprecated = 0  AND 
                                            scheduleevents.IsDeprecated = 0  AND 
                                            patientepisodes.IsActive = 1 AND 
                                            patientepisodes.IsDischarged = 0  AND 
                                            DATE(scheduleevents.EventDate) <> DATE(scheduleevents.VisitDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} 
                                            ORDER BY patients.LastName ASC , patients.FirstName ASC ", additonalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    PatientIdNumber = reader.GetStringNullable("PatientIdNumber")
                })
                .AsList();
            }
            return scheduleList;

        }
        public List<ScheduleEvent> GetPrintQueueEvents(Guid agencyId)
        {
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        patients.FirstName as FirstName, 
                        patients.LastName as LastName
                                FROM scheduleevents INNER JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id
                                        INNER JOIN patients ON scheduleevents.PatientId = patients.Id  
                                                WHERE scheduleevents.AgencyId = @agencyid  AND 
                                                        scheduleevents.IsMissedVisit = 0 AND
                                                        scheduleevents.IsDeprecated = 0 AND
                                                        scheduleevents.InPrintQueue = 1 AND
                                                        patients.IsDeprecated = 0  AND 
                                                        patients.Status IN (1,2) AND 
                                                        scheduleevents.IsDeprecated = 0  AND 
                                                        patientepisodes.IsActive = 1 AND 
                                                        patientepisodes.IsDischarged = 0  AND 
                                                        DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate)");

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;

        }
        public List<ScheduleEvent> GetScheduledEventsForOASISAndNotesByPatient(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate, int[] notesScheduleStatus, int[] notesDisciplineTasks, int[] oasisScheduleStatus, int[] oasisDisciplineTasks)
        {
            var additionalScript1 = string.Empty;

            if (notesDisciplineTasks != null && notesDisciplineTasks.Length > 0)
            {
                additionalScript1 += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", notesDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (notesScheduleStatus != null && notesScheduleStatus.Length > 0)
            {
                additionalScript1 += string.Format("  AND scheduleevents.Status IN ( {0} ) ", notesScheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script1 = string.Format(@"SELECT 
                        scheduleevents.EventId as Id ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        patientvisitnotes.Note as Note ,
                        scheduleevents.Status as Status ,
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        patientepisodes.StartDate as StartDate ,
                        patientepisodes.EndDate as EndDate ,
                        scheduleevents.DisciplineTask as DisciplineTask  
                                        FROM patientvisitnotes 
                                            INNER JOIN scheduleevents ON patientvisitnotes.Id = scheduleevents.EventId 
                                            INNER JOIN patientepisodes ON  patientvisitnotes.EpisodeId = patientepisodes.Id  AND patientvisitnotes.EpisodeId = scheduleevents.EpisodeId
                                                        WHERE 
                                                              patientepisodes.AgencyId = @agencyid AND 
                                                              scheduleevents.AgencyId = @agencyid AND 
                                                              scheduleevents.PatientId = @patientid AND 
                                                              scheduleevents.IsMissedVisit = 0 AND 
                                                              scheduleevents.IsDeprecated = 0 AND 
                                                              patientepisodes.IsActive = 1 AND 
                                                              patientepisodes.IsDischarged = 0 AND 
                                                              DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND 
                                                              DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0} ", additionalScript1);

            var additionalScript2 = string.Empty;

            if (oasisDisciplineTasks != null && oasisDisciplineTasks.Length > 0)
            {
                additionalScript2 += string.Format(" AND agencymanagement.scheduleevents.DisciplineTask IN ( {0} ) ", oasisDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (oasisScheduleStatus != null && oasisScheduleStatus.Length > 0)
            {
                additionalScript2 += string.Format("  AND agencymanagement.scheduleevents.Status IN ( {0} ) ", oasisScheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script2 = string.Format(@"SELECT 
                        agencymanagement.scheduleevents.EventId as Id ,
                        agencymanagement.scheduleevents.PatientId as PatientId ,
                        agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                        agencymanagement.scheduleevents.UserId as UserId ,
                        oasisc.assessments.OasisData as Note ,
                        agencymanagement.scheduleevents.Status as Status ,
                        agencymanagement.scheduleevents.EventDate as EventDate ,
                        agencymanagement.scheduleevents.VisitDate as VisitDate ,
                        agencymanagement.patientepisodes.StartDate as StartDate ,
                        agencymanagement.patientepisodes.EndDate as EndDate ,
                        agencymanagement.scheduleevents.DisciplineTask as DisciplineTask
                                            FROM oasisc.assessments 
                                                                INNER JOIN  agencymanagement.scheduleevents  ON oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                                                INNER JOIN agencymanagement.patientepisodes ON oasisc.assessments.EpisodeId = agencymanagement.patientepisodes.Id AND oasisc.assessments.EpisodeId = agencymanagement.scheduleevents.EpisodeId 
                                                                        WHERE  
                                                                          agencymanagement.patientepisodes.AgencyId = @agencyid AND 
                                                                          agencymanagement.scheduleevents.AgencyId = @agencyid AND 
                                                                          agencymanagement.scheduleevents.PatientId = @patientid AND 
                                                                          agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                                                          agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                                                          agencymanagement.patientepisodes.IsActive = 1 AND 
                                                                          agencymanagement.patientepisodes.IsDischarged = 0 AND 
                                                                          DATE(agencymanagement.scheduleevents.EventDate) between DATE(agencymanagement.patientepisodes.StartDate) and DATE(agencymanagement.patientepisodes.EndDate) AND 
                                                                          DATE(agencymanagement.scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0}", additionalScript2);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1}", script1, script2)))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    Note = reader.GetStringNullable("Note"),
                    Status = reader.GetIntOrZero("Status"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    DisciplineTask = reader.GetInt("DisciplineTask")
                })
                .AsList();
            }
            return list;
        }
        public List<ScheduleEvent> GetScheduledEventsForOASISAndNotesByEpisode(Guid agencyId, Guid patientId, Guid episodeId, DateTime startDate, DateTime endDate, int[] notesScheduleStatus, int[] notesDisciplineTasks, int[] oasisScheduleStatus, int[] oasisDisciplineTasks)
        {
            var additionalScript1 = string.Empty;

            if (notesDisciplineTasks != null && notesDisciplineTasks.Length > 0)
            {
                additionalScript1 += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", notesDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (notesScheduleStatus != null && notesScheduleStatus.Length > 0)
            {
                additionalScript1 += string.Format("  AND scheduleevents.Status IN ( {0} ) ", notesScheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script1 = string.Format(@"SELECT 
                        scheduleevents.EventId as Id ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        patientvisitnotes.Note as Note ,
                        scheduleevents.Status as Status ,
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.DisciplineTask as DisciplineTask  
                                        FROM patientvisitnotes 
                                            INNER JOIN scheduleevents ON patientvisitnotes.Id = scheduleevents.EventId 
                                                        WHERE 
                                                              scheduleevents.AgencyId = @agencyid AND 
                                                              scheduleevents.PatientId = @patientid AND 
                                                              scheduleevents.EpisodeId = @episodeid AND 
                                                              scheduleevents.IsMissedVisit = 0 AND 
                                                              scheduleevents.IsDeprecated = 0 AND 
                                                              DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0} ", additionalScript1);

            var additionalScript2 = string.Empty;

            if (oasisDisciplineTasks != null && oasisDisciplineTasks.Length > 0)
            {
                additionalScript2 += string.Format(" AND agencymanagement.scheduleevents.DisciplineTask IN ( {0} ) ", oasisDisciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (oasisScheduleStatus != null && oasisScheduleStatus.Length > 0)
            {
                additionalScript2 += string.Format("  AND agencymanagement.scheduleevents.Status IN ( {0} ) ", oasisScheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script2 = string.Format(@"SELECT 
                        agencymanagement.scheduleevents.EventId as Id ,
                        agencymanagement.scheduleevents.PatientId as PatientId ,
                        agencymanagement.scheduleevents.EpisodeId as EpisodeId ,
                        agencymanagement.scheduleevents.UserId as UserId ,
                        oasisc.assessments.OasisData as Note ,
                        agencymanagement.scheduleevents.Status as Status ,
                        agencymanagement.scheduleevents.EventDate as EventDate ,
                        agencymanagement.scheduleevents.VisitDate as VisitDate ,
                        agencymanagement.scheduleevents.DisciplineTask as DisciplineTask
                                            FROM oasisc.assessments 
                                                                INNER JOIN  agencymanagement.scheduleevents  ON oasisc.assessments.Id = agencymanagement.scheduleevents.EventId 
                                                                        WHERE  
                                                                          agencymanagement.scheduleevents.AgencyId = @agencyid AND 
                                                                          agencymanagement.scheduleevents.PatientId = @patientid AND 
                                                                          agencymanagement.scheduleevents.EpisodeId = @episodeid AND 
                                                                          agencymanagement.scheduleevents.IsMissedVisit = 0 AND 
                                                                          agencymanagement.scheduleevents.IsDeprecated = 0 AND 
                                                                          DATE(agencymanagement.scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) {0}", additionalScript2);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(string.Format("{0} UNION {1}", script1, script2)))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("Id"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    Note = reader.GetStringNullable("Note"),
                    Status = reader.GetIntOrZero("Status"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    DisciplineTask = reader.GetInt("DisciplineTask")
                })
                .AsList();
            }
            return list;
        }
        public List<ScheduleEvent> GetScheduleEventsVeryLean(Guid agencyId, Guid branchId, Guid patientId, Guid episodeId, int[] scheduleStatus, int[] disciplineTasks, int patientStatus, DateTime startDate, DateTime endDate, bool IsDateRange, bool IsMissedVisitIncluded)
        {
            var branchAdditionalScript = string.Empty;
            if (!branchId.IsEmpty()) branchAdditionalScript = " AND patients.AgencyLocationId = @branchId ";
            var patientAdditionalScript = string.Empty;
            if (!patientId.IsEmpty())
            {
                patientAdditionalScript += " AND scheduleevents.PatientId = @patientId ";
            }
            else
            {
                patientAdditionalScript = string.Format(" AND patients.IsDeprecated = 0 {0} ", patientStatus <= 0 ? " AND patients.Status IN (1,2) " : " AND patients.Status = " + patientStatus);
            }
            var episodeAdditionalScript = string.Empty;
            if (!episodeId.IsEmpty()) episodeAdditionalScript = " AND scheduleevents.EpisodeId = @episodeid ";
            else episodeAdditionalScript = " AND patientepisodes.IsActive = 1  AND patientepisodes.IsDischarged = 0  ";
            var scheduleAdditionalScript = string.Empty;
            if (disciplineTasks != null && disciplineTasks.Length > 0) scheduleAdditionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            if (scheduleStatus != null && scheduleStatus.Length > 0) scheduleAdditionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            if (!IsMissedVisitIncluded) scheduleAdditionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            if (IsDateRange) scheduleAdditionalScript += " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline  
                            FROM 
                                scheduleevents 
                                    INNER JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    INNER JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE
                                            scheduleevents.AgencyId = @agencyId {0} {1} {2} AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {3} ", branchAdditionalScript, patientAdditionalScript, episodeAdditionalScript, scheduleAdditionalScript);

            var scheduleList = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddGuid("patientId", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline")
                })
                .AsList();
            }
            return scheduleList;
        }
        public List<ScheduleEvent> GetPatientScheduledEventsLeanWithUserId(Guid agencyId, Guid patientId, int[] scheduleStatus, string[] disciplines, int[] disciplineTasks, bool IsDateRange, DateTime startDate, DateTime endDate, bool IsMissedVisitIncluded, bool IsASC, int limit)
        {
            var additionalScript = string.Empty;
            if (IsDateRange)
            {
                additionalScript += " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) ";
            }
            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            var limitScript = string.Empty;
            if (IsASC)
            {
                limitScript += "  ORDER BY DATE(scheduleevents.EventDate) ASC  ";
            }
            else
            {
                limitScript += "  ORDER BY DATE(scheduleevents.EventDate) DESC  ";
            }
            if (limit > 0)
            {
                limitScript += string.Format(" LIMIT {0} ", limit);
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline  
                            FROM 
                                scheduleevents 
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid  AND
                                            scheduleevents.IsDeprecated = 0 AND
                                            scheduleevents.DisciplineTask > 0  AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0} {1} ", additionalScript, limitScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline")

                })
                .AsList();
            }
            return list;
        }


        public List<ScheduleEvent> GetPatientScheduledEventsNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline, bool isReportAndNotesIncluded)
        {
            var disciplineScript = string.Empty;
            if (discipline.IsEqual("all"))
            {

            }
            else if (discipline.IsEqual("Therapy"))
            {
                disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'PT') = 1 OR STRCMP( scheduleevents.Discipline, 'OT') = 1 OR STRCMP( scheduleevents.Discipline , 'ST') = 1 ) ";
            }
            else
            {
                if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
                {
                    disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'Nursing') = 1 OR STRCMP( scheduleevents.Discipline, 'ReportsAndNotes') = 1) ";
                }
                else
                {
                    disciplineScript = " AND scheduleevents.Discipline = @discipline ";
                }
            }
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.ReturnReason as ReturnReason ,
                        scheduleevents.Comments as Comments ,
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate ,
                        scheduleevents.Asset as Asset ,
                        scheduleevents.Version  as Version ,
                        scheduleevents.UserName  as UserName ,
                        users.FirstName as FirstName , 
                        users.LastName as LastName ,
                        users.Suffix as Suffix , 
                        users.Credentials as Credentials ,
                        users.CredentialsOther as CredentialsOther ,
                        users.IsDeprecated as UserIsDeprecated
                            FROM 
                                scheduleevents 
                                    LEFT JOIN users ON scheduleevents.UserId = users.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.EpisodeId = @episodeid AND 
                                            scheduleevents.IsDeprecated = 0 AND
                                            scheduleevents.DisciplineTask > 0 {0}  
                                            ORDER BY DATE(scheduleevents.EventDate) DESC  ", disciplineScript);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddString("discipline", discipline)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName"),
                    UserFirstName = reader.GetStringNullable("FirstName"),
                    UserLastName = reader.GetStringNullable("LastName"),
                    UserSuffix = reader.GetStringNullable("Suffix"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsWithDateRangeOrNotNew(Guid agencyId, Guid episodeId, Guid patientId, bool IsDateRange, DateTime startDate, DateTime endDate)
        {
            var dateRange = IsDateRange ? " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.IsBillable as IsBillable ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.TimeIn as TimeIn , 
                        scheduleevents.TimeOut as TimeOut ,
                        scheduleevents.Surcharge as Surcharge ,
                        scheduleevents.AssociatedMileage as AssociatedMileage ,
                        scheduleevents.ReturnReason as ReturnReason ,
                        scheduleevents.Comments as Comments ,
                        scheduleevents.IsDeprecated as IsDeprecated , 
                        scheduleevents.IsOrderForNextEpisode as IsOrderForNextEpisode , 
                        scheduleevents.IsVisitPaid as IsVisitPaid ,
                        scheduleevents.InPrintQueue as InPrintQueue ,
                        scheduleevents.StartDate as StartDate ,
                        scheduleevents.EndDate as EndDate ,
                        scheduleevents.Asset as Asset ,
                        scheduleevents.Version  as Version ,
                        scheduleevents.UserName  as UserName ,
                        users.FirstName as FirstName , 
                        users.LastName as LastName ,
                        users.Suffix as Suffix , 
                        users.Credentials as Credentials ,
                        users.CredentialsOther as CredentialsOther ,
                        users.IsDeprecated as UserIsDeprecated 
                            FROM
                                scheduleevents 
                                    LEFT JOIN users ON scheduleevents.UserId = users.Id
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.EpisodeId = @episodeid AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 {0} 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC ", dateRange);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Surcharge = reader.GetStringNullable("Surcharge"),
                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    IsDeprecated = reader.GetBoolean("IsDeprecated"),
                    IsOrderForNextEpisode = reader.GetBoolean("IsOrderForNextEpisode"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    InPrintQueue = reader.GetBoolean("InPrintQueue"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Asset = reader.GetStringNullable("Asset"),
                    Version = reader.GetInt("Version"),
                    UserName = reader.GetStringNullable("UserName"),
                    UserFirstName = reader.GetStringNullable("FirstName"),
                    UserLastName = reader.GetStringNullable("LastName"),
                    UserSuffix = reader.GetStringNullable("Suffix"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
                })
                .AsList();
            }
            return list;
        }

        public List<ScheduleEvent> GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate, string discipline, bool isReportAndNotesIncluded, bool IsDateRange)
        {
            var dateRange = IsDateRange ? " AND DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)" : string.Empty;
            var disciplineScript = string.Empty;
            if (discipline.IsEqual("all"))
            {

            }
            else if (discipline.IsEqual("Therapy"))
            {
                disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'PT') = 1 OR STRCMP( scheduleevents.Discipline, 'OT') = 1 OR STRCMP( scheduleevents.Discipline , 'ST') = 1 ) ";
            }
            else
            {
                if (isReportAndNotesIncluded && discipline.IsEqual("Nursing"))
                {
                    disciplineScript = " AND ( STRCMP(scheduleevents.Discipline ,'Nursing') = 1 OR STRCMP( scheduleevents.Discipline, 'ReportsAndNotes') = 1) ";
                }
                else
                {
                    disciplineScript = " AND scheduleevents.Discipline = @discipline ";
                }
            }
            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Discipline as Discipline ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.Status as Status ,
                        scheduleevents.IsBillable as IsBillable ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.ReturnReason as ReturnReason ,
                        scheduleevents.Comments as Comments ,
                        scheduleevents.Asset as Asset ,
                        scheduleevents.UserName  as UserName ,
                        users.FirstName as FirstName , 
                        users.LastName as LastName ,
                        users.Suffix as Suffix , 
                        users.Credentials as Credentials ,
                        users.CredentialsOther as CredentialsOther ,
                        users.IsDeprecated as UserIsDeprecated 
                            FROM 
                                scheduleevents
                                    LEFT JOIN users ON scheduleevents.UserId = users.Id
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.PatientId = @patientid AND 
                                            scheduleevents.EpisodeId = @episodeid AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 {0} {1} 
                                                ORDER BY 
                                                    DATE(scheduleevents.EventDate) DESC ", disciplineScript, dateRange);

            var list = new List<ScheduleEvent>();
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddString("discipline", discipline)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new ScheduleEvent
                {
                    EventId = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    EventDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Discipline = reader.GetStringNullable("Discipline"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    Status = reader.GetIntOrZero("Status"),
                    IsBillable = reader.GetBoolean("IsBillable"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    ReturnReason = reader.GetStringNullable("ReturnReason"),
                    Comments = reader.GetStringNullable("Comments"),
                    Asset = reader.GetStringNullable("Asset"),
                    UserName = reader.GetStringNullable("UserName"),
                    UserFirstName = reader.GetStringNullable("FirstName"),
                    UserLastName = reader.GetStringNullable("LastName"),
                    UserSuffix = reader.GetStringNullable("Suffix"),
                    Credentials = reader.GetStringNullable("Credentials"),
                    CredentialsOther = reader.GetStringNullable("CredentialsOther"),
                    IsUserDeprecated = reader.GetStringNullable("UserIsDeprecated").IsNotNullOrEmpty() && reader.GetStringNullable("UserIsDeprecated").IsBoolean() ? reader.GetBoolean("UserIsDeprecated") : false
                })
                .AsList();
            }
            return list;
        }
        
        #endregion


        public List<UserVisit> GetUserVisitLean(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additionalScript = string.Empty; ;
            if (patientStatus <= 0)
            {
                additionalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND patients.Status = @statusid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        patients.FirstName as FirstName , 
                        patients.LastName as LastName  
                            FROM
                                scheduleevents
                                    LEFT JOIN patientepisodes ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.UserId = @userid AND 
                                            patientepisodes.IsActive = 1  AND 
                                            patientepisodes.IsDischarged = 0 AND
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 AND 
                                            patients.IsDeprecated = 0 AND 
                                            patients.Status IN (1,2) AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0} 
                                                ORDER BY patients.LastName ASC , patients.FirstName DESC ", additionalScript);

            var list = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("userid", userId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader => new UserVisit
                {
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    ScheduleDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    Status = reader.GetIntOrZero("Status"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return list;
        }

        public List<UserVisit> GetUserVisitLeanByBranchStatusDisciplineAndDateRange(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate, int patientStatus, string[] disciplines, int[] disciplineTasks, int[] scheduleStatus, bool IsMissedVisitIncluded)
        {
            var additonalScript = string.Empty;
            if (patientStatus <= 0)
            {
                additonalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additonalScript += " AND patients.Status = @statusid";
            }
            if (!branchId.IsEmpty())
            {
                additonalScript += " AND patients.AgencyLocationId = @branchId";
            }
            if (!IsMissedVisitIncluded)
            {
                additonalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additonalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplineTasks != null && disciplineTasks.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.DisciplineTask IN ( {0} ) ", disciplineTasks.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additonalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        patients.FirstName as FirstName , 
                        patients.LastName as LastName  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON  scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id
                                        WHERE
                                            scheduleevents.AgencyId = @agencyid  AND 
                                            patients.IsDeprecated = 0 AND 
                                            scheduleevents.IsDeprecated = 0 AND
                                            patientepisodes.IsActive = 1 AND
                                            patientepisodes.IsDischarged = 0 AND
                                            DATE(scheduleevents.EventDate) between DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) AND
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate)  {0} ", additonalScript);

            var scheduleList = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                scheduleList = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("branchId", branchId)
                .AddInt("statusid", patientStatus)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .SetMap(reader => new UserVisit
                {
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    ScheduleDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    Status = reader.GetIntOrZero("Status"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase()
                })
                .AsList();
            }
            return scheduleList;

        }

        public List<UserVisit> GetPayrollSummmaryVisits(Guid agencyId, Guid userId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid)
        {
            var additionalScript = string.Empty; ;
            if (patientStatus <= 0)
            {
                additionalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND patients.Status = @statusid";
            }

            if (!userId.IsEmpty())
            {
                additionalScript += " AND scheduleevents.UserId = @userid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }

            if (disciplineTasksNotIncluded != null && disciplineTasksNotIncluded.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask NOT IN ( {0} ) ", disciplineTasksNotIncluded.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (IsVisitPaid.IsEqual("true"))
            {
                additionalScript += " AND scheduleevents.IsVisitPaid = 1 ";
            }
            else if (IsVisitPaid.IsEqual("false"))
            {
                additionalScript += " AND scheduleevents.IsVisitPaid = 0 ";
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.EventId as EventId ,
                        scheduleevents.PatientId as PatientId ,
                        scheduleevents.EpisodeId as EpisodeId ,
                        scheduleevents.UserId as UserId ,
                        scheduleevents.DisciplineTask as DisciplineTask , 
                        scheduleevents.EventDate as EventDate ,
                        scheduleevents.VisitDate as VisitDate ,
                        scheduleevents.Status as Status ,
                        scheduleevents.IsMissedVisit as IsMissedVisit , 
                        scheduleevents.TimeIn as TimeIn , 
                        scheduleevents.TimeOut as TimeOut ,
                        scheduleevents.Surcharge as Surcharge ,
                        scheduleevents.AssociatedMileage as AssociatedMileage ,
                        scheduleevents.IsVisitPaid as IsVisitPaid ,
                        patients.FirstName as FirstName , 
                        patients.LastName as LastName  
                            FROM 
                                scheduleevents
                                    LEFT JOIN patientepisodes ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    LEFT JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            patientepisodes.IsActive = 1  AND 
                                            patientepisodes.IsDischarged = 0 AND
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 AND
                                            patients.IsDeprecated = 0 AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0} 
                                                ORDER BY DATE(scheduleevents.EventDate) DESC ", additionalScript);

            var list = new List<UserVisit>();
            using (var cmd = new FluentCommand<UserVisit>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .AddGuid("userid", userId)
                .SetMap(reader => new UserVisit
                {
                    Id = reader.GetGuid("EventId"),
                    PatientId = reader.GetGuid("PatientId"),
                    EpisodeId = reader.GetGuid("EpisodeId"),
                    UserId = reader.GetGuid("UserId"),
                    DisciplineTask = reader.GetInt("DisciplineTask"),
                    ScheduleDate = reader.GetDateTime("EventDate"),
                    VisitDate = reader.GetDateTime("VisitDate"),
                    Status = reader.GetIntOrZero("Status"),
                    IsMissedVisit = reader.GetBoolean("IsMissedVisit"),
                    TimeIn = reader.GetStringNullable("TimeIn"),
                    TimeOut = reader.GetStringNullable("TimeOut"),
                    Surcharge = reader.GetStringNullable("Surcharge"),
                    AssociatedMileage = reader.GetStringNullable("AssociatedMileage"),
                    IsVisitPaid = reader.GetBoolean("IsVisitPaid"),
                    PatientName = reader.GetString("LastName").ToUpperCase() + ", " + reader.GetString("FirstName").ToUpperCase(),
                    VisitRate = "$0.00"
                })
                .AsList();
            }
            return list;
        }

        public List<VisitSummary> GetPayrollSummmaryLean(Guid agencyId, DateTime startDate, DateTime endDate, int patientStatus, int[] scheduleStatus, string[] disciplines, int[] disciplineTasksNotIncluded, bool IsMissedVisitIncluded, string IsVisitPaid)
        {
            var additionalScript = string.Empty; ;
            if (patientStatus <= 0)
            {
                additionalScript += " AND patients.Status IN (1,2) ";
            }
            else
            {
                additionalScript += " AND patients.Status = @statusid ";
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }
            if (scheduleStatus != null && scheduleStatus.Length > 0)
            {
                additionalScript += string.Format("  AND scheduleevents.Status IN ( {0} ) ", scheduleStatus.Select(d => d.ToString()).ToArray().Join(","));
            }

            if (!IsMissedVisitIncluded)
            {
                additionalScript += " AND scheduleevents.IsMissedVisit = 0 ";
            }

            if (disciplineTasksNotIncluded != null && disciplineTasksNotIncluded.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.DisciplineTask NOT IN ( {0} ) ", disciplineTasksNotIncluded.Select(d => d.ToString()).ToArray().Join(","));
            }
            if (disciplines != null && disciplines.Length > 0)
            {
                additionalScript += string.Format(" AND scheduleevents.Discipline IN ( {0} ) ", disciplines.Select(d => "\'" + d.ToString() + "\'").ToArray().Join(","));
            }
            if (IsVisitPaid.IsEqual("true"))
            {
                additionalScript += " AND scheduleevents.IsVisitPaid = 1 ";
            }
            else if (IsVisitPaid.IsEqual("false"))
            {
                additionalScript += " AND scheduleevents.IsVisitPaid = 0 ";
            }

            var script = string.Format(@"SELECT 
                        scheduleevents.UserId as UserId ,
                        COUNT(*) as VisitCount  
                            FROM 
                                scheduleevents 
                                    INNER JOIN patientepisodes ON scheduleevents.EpisodeId = patientepisodes.Id 
                                    INNER JOIN patients ON scheduleevents.PatientId = patients.Id 
                                        WHERE 
                                            scheduleevents.AgencyId = @agencyid AND 
                                            scheduleevents.UserId != '{1}' AND
                                            patientepisodes.IsActive = 1  AND
                                            patientepisodes.IsDischarged = 0 AND 
                                            scheduleevents.IsDeprecated = 0 AND 
                                            scheduleevents.DisciplineTask > 0 AND
                                            patients.IsDeprecated = 0 AND 
                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND
                                            DATE(scheduleevents.EventDate) between  DATE(patientepisodes.StartDate) and DATE(patientepisodes.EndDate) {0} 
                                                GROUP BY scheduleevents.UserId ", additionalScript, Guid.Empty.ToString());

            var list = new List<VisitSummary>();
            using (var cmd = new FluentCommand<VisitSummary>(script))
            {
                list = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AddInt("statusid", patientStatus)
                .SetMap(reader => new VisitSummary
                {
                    UserId = reader.GetGuid("UserId"),
                    VisitCount = reader.GetInt("VisitCount")
                })
                .AsList();
            }
            return list;
        }


        #region Process Schedule

        public bool UpdateScheduleEventsForIsBillable(Guid agencyId, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                try
                {
                    scheduleEvents.ForEach(scheduleEvent =>
                    {
                        var evnt = database.Single<ScheduleEvent>(e => e.AgencyId == agencyId && e.EpisodeId == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId && e.EventId == scheduleEvent.EventId);
                        if (evnt != null)
                        {
                            evnt.IsBillable = scheduleEvent.IsBillable;
                            database.Update<ScheduleEvent>(evnt);
                        }
                    }
                    );
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return result;
        }

        public bool IsFirstBillableVisit(Guid agencyId, Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var status = ScheduleStatusFatory.OASISAndNurseNotesAfterQA().Select(s => s.ToString()).ToArray();// new string[] { ((int)ScheduleStatus.NoteCompleted).ToString(), ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), ((int)ScheduleStatus.OasisExported).ToString(), ((int)ScheduleStatus.OasisCompletedNotExported).ToString() };
            var script = string.Format(@"SELECT count(*)  
                                                FROM 
                                                    scheduleevents
                                                        WHERE 
                                                            scheduleevents.AgencyId = @agencyid AND 
                                                            scheduleevents.PatientId = @patientid  AND 
                                                            scheduleevents.EpisodeId = @episodeid AND 
                                                            scheduleevents.IsDeprecated = 0  AND
                                                            scheduleevents.IsBillable = 1 AND 
                                                            scheduleevents.IsMissedVisit = 0 AND 
                                                            DATE(scheduleevents.EventDate) between DATE(@startdate) and DATE(@enddate) AND 
                                                            scheduleevents.Status IN ( {0} ) 
                                                                ORDER BY 
                                                                    DATE(scheduleevents.VisitDate) ASC LIMIT 1", status.Join(","));

            var count = 0;
            using (var cmd = new FluentCommand<ScheduleEvent>(script))
            {
                count = cmd.SetConnection("AgencyManagementConnectionString")
                .AddGuid("agencyid", agencyId)
                .AddGuid("patientid", patientId)
                .AddGuid("episodeid", episodeId)
                .AddDateTime("startdate", startDate)
                .AddDateTime("enddate", endDate)
                .AsScalar();
            }
            return count > 0;
        }

        #endregion

        #region Missed Visit

        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            try
            {
                if (missedVisit != null)
                {
                    missedVisit.Date = DateTime.Now;
                    database.Add<MissedVisit>(missedVisit);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public bool UpdateMissedVisit(MissedVisit missedVisit)
        {
            try
            {
                if (missedVisit != null)
                {
                    missedVisit.Date = DateTime.Now;
                    database.Update<MissedVisit>(missedVisit);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public MissedVisit GetMissedVisit(Guid agencyId, Guid Id)
        {
            var missedVisit = database.Single<MissedVisit>(m => m.AgencyId == agencyId && m.Id == Id);
            if (missedVisit != null)
            {
                var patient = database.Single<Patient>(p => p.Id == missedVisit.PatientId && p.AgencyId == agencyId);
                if (patient != null)
                {
                    missedVisit.PatientName = patient.DisplayName;
                }
                var scheduledEvent = GetScheduleEventNew(agencyId, missedVisit.PatientId, missedVisit.EpisodeId, Id);
                if (scheduledEvent != null)
                {
                    missedVisit.VisitType = scheduledEvent.DisciplineTaskName;
                    var user = database.Single<User>(u => u.AgencyId == agencyId && u.Id == scheduledEvent.UserId);
                    if (user != null)
                    {
                        missedVisit.UserName = user.DisplayName;
                    }
                }
            }
            return missedVisit;
        }

        #endregion



    }
}
