﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SubSonic.Repository;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;


    public class IncidentRepository : IIncidentRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public IncidentRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

        #region Incident Report

        public bool AddIncident(Incident incident)
        {
            var result = false;
            if (incident != null)
            {
                if (incident.IndividualInvolvedArray != null && incident.IndividualInvolvedArray.Count > 0)
                {
                    incident.IndividualInvolved = incident.IndividualInvolvedArray.ToArray().AddColons();
                }
                incident.Created = DateTime.Now;
                incident.Modified = DateTime.Now;

                database.Add<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool UpdateIncident(Incident incident)
        {
            var result = false;
            var existingIncident = GetIncidentReport(incident.AgencyId, incident.Id);
            if (existingIncident != null)
            {
                existingIncident.Orders = incident.Orders;
                existingIncident.Description = incident.Description;
                existingIncident.ActionTaken = incident.ActionTaken;

                existingIncident.MDNotified = incident.MDNotified;
                existingIncident.NewOrdersCreated = incident.NewOrdersCreated;
                existingIncident.FamilyNotified = incident.FamilyNotified;

                existingIncident.PhysicianId = incident.PhysicianId;
                existingIncident.IncidentDate = incident.IncidentDate;
                existingIncident.IndividualInvolvedOther = incident.IndividualInvolvedOther;
                existingIncident.Status = incident.Status;

                if (incident.IndividualInvolvedArray != null && incident.IndividualInvolvedArray.Count > 0)
                {
                    existingIncident.IndividualInvolved = incident.IndividualInvolvedArray.ToArray().AddColons();
                }
                existingIncident.IndividualInvolvedOther = incident.IndividualInvolvedOther;
                existingIncident.FollowUp = incident.FollowUp;
                existingIncident.SignatureDate = incident.SignatureDate;
                existingIncident.SignatureText = incident.SignatureText;
                existingIncident.Modified = DateTime.Now;

                database.Update<Incident>(existingIncident);
                result = true;
            }
            return result;
        }

        public bool UpdateIncidentModal(Incident incident)
        {
            var result = false;
            if (incident != null)
            {
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool RemoveIncidentReport(Guid Id)
        {
            var result = false;
            try
            {
                database.Delete<Incident>(Id);
                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool MarkIncidentAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (incident != null)
            {
                incident.IsDeprecated = isDeprecated;
                incident.Modified = DateTime.Now;
                database.Update<Incident>(incident);
                result = true;
            }
            return result;
        }

        public bool ReassignIncidentUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId)
        {
            bool result = false;
            var incident = database.Single<Incident>(i => i.AgencyId == agencyId && i.PatientId == patientId && i.Id == Id);
            if (incident != null)
            {
                try
                {
                    incident.UserId = employeeId;
                    incident.Modified = DateTime.Now;
                    database.Update<Incident>(incident);
                    result = true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public Incident GetIncidentReport(Guid agencyId, Guid incidentId)
        {
            return database.Single<Incident>(i => i.AgencyId == agencyId && i.Id == incidentId);
        }

        public IList<Incident> GetIncidents(Guid agencyId)
        {
            return database.Find<Incident>(i => i.AgencyId == agencyId).ToList();
        }


        #endregion
    }
}
