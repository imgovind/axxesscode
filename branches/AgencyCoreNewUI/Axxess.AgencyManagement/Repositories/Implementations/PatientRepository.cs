﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using System.Transactions;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Domain;
    using Extensions;

    using AutoMapper;

    using SubSonic.Repository;
    using SubSonic.DataProviders;

    public class PatientRepository : IPatientRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PatientRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }

        #endregion

    }
}
