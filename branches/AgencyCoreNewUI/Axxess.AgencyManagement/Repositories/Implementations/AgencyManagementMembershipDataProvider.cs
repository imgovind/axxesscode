﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Data.Linq;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;

    using SubSonic.Repository;

    public class AgencyManagementMembershipDataProvider : IAgencyManagementMembershipDataProvider
    {
        #region Members and Properties

        private readonly SimpleRepository database;

        public AgencyManagementMembershipDataProvider()
        {
            this.database = new SimpleRepository("AxxessMembershipConnectionString", SimpleRepositoryOptions.None);
        }

        #endregion

        private ILoginRepository loginRepository;
        public ILoginRepository LoginRepository
        {
            get
            {
                if (loginRepository == null)
                    loginRepository = new LoginRepository(this.database);

                return loginRepository;
            }
        }
    }

}

