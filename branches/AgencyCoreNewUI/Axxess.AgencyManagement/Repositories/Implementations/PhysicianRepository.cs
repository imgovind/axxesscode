﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    using SubSonic.Repository;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Domain;
    using Axxess.Core.Infrastructure;

    public class PhysicianRepository : IPhysicianRepository
    {
        #region Constructor

        private readonly SimpleRepository database;

        public PhysicianRepository(SimpleRepository database)
        {
            Check.Argument.IsNotNull(database, "database");
            this.database = database;
        }
        #endregion

        #region IPhysicianRepository Members
       
        public AgencyPhysician Get(Guid physicianId, Guid agencyId, int clusterId)
        {
            AgencyPhysician result = new AgencyPhysician();

            var script = @"SELECT
	                            `Id`,
	                            `AgencyId`,
	                            `LoginId`,
	                            `NPI`,
	                            `UPIN`,
	                            `FirstName`,
	                            `LastName`,
	                            `MiddleName`,
	                            `Gender`,
	                            `Credentials`,
	                            `AddressLine1`,
	                            `AddressLine2`,
	                            `AddressCity`,
	                            `AddressStateCode`,
	                            `AddressZipCode`,
	                            `PhoneWork`,
	                            `PhoneAlternate`,
	                            `FaxNumber`,
	                            `EmailAddress`,
	                            `LicenseNumber`,
	                            `LicenseStateCode`,
	                            `Comments`,
	                            `Created`,
	                            `Modified`,
	                            `IsDeprecated`,
	                            `PhysicianAccess`,
	                            `Licenses`
                                    FROM 
                                        `agencymanagement`.`agencyphysicians` 
                                            WHERE 
                                                `agencyphysicians`.AgencyId = @agencyid AND
                                                `agencyphysicians`.Id = @physicianid";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("physicianid", physicianId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new AgencyPhysician
                    {
                        Id = reader.GetGuid("Id"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        LoginId = reader.GetGuid("LoginId"),
                        NPI = reader.GetStringNullable("NPI"),
                        UPIN = reader.GetStringNullable("UPIN"),
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        MiddleName = reader.GetStringNullable("MiddleName"),
                        Gender = reader.GetStringNullable("Gender"),
                        Credentials = reader.GetStringNullable("Credentials"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PhoneWork = reader.GetStringNullable("PhoneWork"),
                        PhoneAlternate = reader.GetStringNullable("PhoneAlternate"),
                        FaxNumber = reader.GetStringNullable("FaxNumber"),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                        LicenseNumber = reader.GetStringNullable("LicenseNumber"),
                        LicenseStateCode = reader.GetStringNullable("LicenseStateCode"),
                        Comments = reader.GetStringNullable("Comments"),
                        Created = reader.GetDateTime("Created"),
                        Modified = reader.GetDateTime("Modified"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        PhysicianAccess = reader.GetBoolean("PhysicianAccess"),
                        Licenses = reader.GetStringNullable("Licenses")
                    }).AsSingle();
            }

            return result;
        }
    
        public IList<AgencyPhysician> GetAgencyPhysicians(Guid agencyId, int clusterId)
        {
            List<AgencyPhysician> result = new List<AgencyPhysician>();

            var script = @"SELECT
	                            `Id`,
	                            `AgencyId`,
	                            `LoginId`,
	                            `NPI`,
	                            `UPIN`,
	                            `FirstName`,
	                            `LastName`,
	                            `MiddleName`,
	                            `Gender`,
	                            `Credentials`,
	                            `AddressLine1`,
	                            `AddressLine2`,
	                            `AddressCity`,
	                            `AddressStateCode`,
	                            `AddressZipCode`,
	                            `PhoneWork`,
	                            `PhoneAlternate`,
	                            `FaxNumber`,
	                            `EmailAddress`,
	                            `LicenseNumber`,
	                            `LicenseStateCode`,
	                            `Comments`,
	                            `Created`,
	                            `Modified`,
	                            `IsDeprecated`,
	                            `PhysicianAccess`,
	                            `Licenses`
                                    FROM    
                                       `agencymanagement`.`agencyphysicians` 
                                            WHERE 
                                                `agencyphysicians`.AgencyId = @agencyid";

            using (var cmd = new FluentCommand<AgencyPhysician>(script))
            {
                result = cmd.SetConnection("AgencyManagementConnectionString" + clusterId)
                    .AddGuid("agencyid", agencyId)
                    .SetMap(reader => new AgencyPhysician
                    {
                        Id = reader.GetGuid("Id"),
                        AgencyId = reader.GetGuid("AgencyId"),
                        LoginId = reader.GetGuidIncludeEmpty("LoginId"),
                        NPI = reader.GetStringNullable("NPI"),
                        UPIN = reader.GetStringNullable("UPIN"),
                        FirstName = reader.GetStringNullable("FirstName"),
                        LastName = reader.GetStringNullable("LastName"),
                        MiddleName = reader.GetStringNullable("MiddleName"),
                        Gender = reader.GetStringNullable("Gender"),
                        Credentials = reader.GetStringNullable("Credentials"),
                        AddressLine1 = reader.GetStringNullable("AddressLine1"),
                        AddressLine2 = reader.GetStringNullable("AddressLine2"),
                        AddressCity = reader.GetStringNullable("AddressCity"),
                        AddressStateCode = reader.GetStringNullable("AddressStateCode"),
                        AddressZipCode = reader.GetStringNullable("AddressZipCode"),
                        PhoneWork = reader.GetStringNullable("PhoneWork"),
                        PhoneAlternate = reader.GetStringNullable("PhoneAlternate"),
                        FaxNumber = reader.GetStringNullable("FaxNumber"),
                        EmailAddress = reader.GetStringNullable("EmailAddress"),
                        LicenseNumber = reader.GetStringNullable("LicenseNumber"),
                        LicenseStateCode = reader.GetStringNullable("LicenseStateCode"),
                        Comments = reader.GetStringNullable("Comments"),
                        Created = reader.GetDateTime("Created"),
                        Modified = reader.GetDateTime("Modified"),
                        IsDeprecated = reader.GetBoolean("IsDeprecated"),
                        PhysicianAccess = reader.GetBoolean("PhysicianAccess"),
                        Licenses = reader.GetStringNullable("Licenses")
                    }).AsList();
            }

            return result;
        }
     
        #endregion
    }
}
