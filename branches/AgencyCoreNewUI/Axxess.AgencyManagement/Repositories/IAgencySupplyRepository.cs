﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IAgencySupplyRepository
    {
        bool AddSupply(AgencySupply supply);
        void InsertSupplies(Guid agencyId);
        bool UpdateSupply(AgencySupply supply);
        bool DeleteSupply(Guid agencyId, Guid id);
        AgencySupply GetSupply(Guid agencyId, Guid id);
        IList<AgencySupply> GetSupplies(Guid agencyId);
        IList<AgencySupply> GetSupplies(Guid agencyId, string searchTerm, int searchLimit);
    }
}
