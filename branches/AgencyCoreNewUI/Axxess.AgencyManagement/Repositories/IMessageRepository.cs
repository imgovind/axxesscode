﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core;

    using Domain;

    public interface IMessageRepository
    {
        bool AddSystemMessage(SystemMessage message);
        bool AddDashboardMessage(DashboardMessage message);
        bool AddUserMessage(UserMessage userMessage);
        bool AddUserMessages(List<UserMessage> userMessages);
    }
}
