﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IAgencyHospitalRepository
    {
        bool AddHospital(AgencyHospital hospital);
        bool EditHospital(AgencyHospital hospital);
        bool DeleteHospital(Guid agencyId, Guid Id);
        AgencyHospital FindHospital(Guid agencyId, Guid Id);
        IList<AgencyHospital> GetHospitals(Guid agencyId);
    }
}
