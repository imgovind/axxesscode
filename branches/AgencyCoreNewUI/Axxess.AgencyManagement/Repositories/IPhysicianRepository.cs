﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface IPhysicianRepository
    {
        AgencyPhysician Get(Guid physicianId, Guid agencyId, int clusterId);
        IList<AgencyPhysician> GetAgencyPhysicians(Guid agencyId, int clusterId);
    }
}
