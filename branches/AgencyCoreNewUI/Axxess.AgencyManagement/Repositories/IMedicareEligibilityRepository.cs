﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Domain;

namespace Axxess.AgencyManagement.Repositories
{
    public interface IMedicareEligibilityRepository
    {
        bool UpdateMedicareEligibility(MedicareEligibility medicareEligibility);
        MedicareEligibility GetMedicareEligibility(Guid agencyId, Guid patientId, Guid id);
        MedicareEligibility GetMedicareEligibility(Guid agencyId, Guid EpisodeId, Guid patientId, Guid id);
        List<MedicareEligibility> GetMedicareEligibilities(Guid agencyId, Guid patientId);
    }
}
