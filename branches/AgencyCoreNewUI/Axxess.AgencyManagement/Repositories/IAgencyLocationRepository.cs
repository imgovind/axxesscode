﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IAgencyLocationRepository
    {
        bool AddLocation(AgencyLocation agencyLocation);
        bool EditLocationModal(AgencyLocation location);
        bool EditBranchCost(AgencyLocation location);
        AgencyLocation GetMainLocation(Guid agencyId);
        AgencyLocation FindLocation(Guid agencyId, Guid Id);
        IList<AgencyLocation> GetBranches(Guid agencyId);
        IList<AgencyLocation> GetAllBranches(Guid agencyId);
        List<AgencyLocation> AgencyLocations(Guid agencyId, string locations);

    }
}
