﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain;

    public interface ICustomerNotesRepository
    {
        IList<CustomerNote> GetActiveCustomerNotes(int limit);
        IList<CustomerNote> GetActiveCustomerNotes();
        IList<CustomerNote> GetUpdatedCustomerNotes(int limit);
        IList<CustomerNote> GetUpdatedCustomerNotes();
        IList<CustomerNote> GetCurrentUserNotes(Guid id, int limit);
        IList<CustomerNote> GetCurrentUserNotes(Guid id);
        DateTime GetLastAddedTime();
        DateTime GetLastUpdatedTime();


        bool AddCustomerNote(CustomerNote note);
        bool UpdateCustomerNote(CustomerNote note);
        CustomerNote GetCustomerNote(Guid noteId);
        CustomerNote GetCustomerNote(Guid agencyId, Guid noteId);
        IList<CustomerNote> GetCustomerNotes(Guid agencyId);
        bool UpdateCustomerNoteIssueAndStatus(Guid agencyId, Guid noteId, int issueId, int statusId);
        IList<CustomerNote> GetRecentCustomerNotes(DateTime startDate, DateTime endDate);
        IList<CustomerNote> GetCurrentUserNotes(Guid id, DateTime startDate, DateTime endDate);
        bool DeleteCustomerNote(CustomerNote note);
    }
}
