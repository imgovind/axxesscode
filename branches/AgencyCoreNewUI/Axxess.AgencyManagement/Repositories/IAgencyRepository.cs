﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using Domain;
    using Axxess.Core.Infrastructure;

    public interface IAgencyRepository
    {
        bool ToggleDelete(Guid id, DateTime modified, int clusterId);
        bool ToggleReadOnly(Guid id, DateTime readOnlyDate, bool isFrozen, DateTime modified, int clusterId);
        Agency Get(Guid id, int clusterId);
        Agency GetAgencyOnly(Guid id, int clusterId);
      
        //List<AgencyLite> GetAllAgencies(int clusterId);
        List<AgencyLite> GetAllAgencies(string stateFilter, DateTime? startDate, DateTime? endDate, int clusterId);
        bool Add(Agency agency, int clusterId);
        bool Update(Agency agency, int clusterId);
        bool UpdateImplementationSpecialist(Guid agencyId, Guid implementationSpecialistId, int clusterId);
        bool AddLocation(AgencyLocation agencyLocation, int clusterId);
        AgencyLocation GetMainLocation(Guid agencyId, int clusterId);
        IList<AgencyLocation> GetBranches(Guid agencyId, int clusterId);
        AgencyLocation FindLocation(Guid agencyId, Guid Id, int clusterId);
        bool EditLocationModal(AgencyLocation location, int clusterId);
        bool UpdateLocation(string medicareProviderNumber, Guid Id, int clusterId);
        bool RemoveLocation(Guid agencyId, Guid Id, int clusterId);
        List<AgencySelection> GetAgencySelection(bool onlyDeleted, int clusterId);
        bool InsertTemplates(Guid agencyId, int clusterId);
        bool InsertSupplies(Guid agencyId, int clusterId);
       
        void InsertUploadTypes(Guid agencyId, int clusterId);
        bool AddAdjustmentCode(AgencyAdjustmentCode code, int clusterId);
        //int GetAgencyCount(int clusterId);
       
      
        //IList<AgencyUpgrade> GetAgencyUpgrades(string isCompleted, DateTime startDate, DateTime endDate, int clusterId);
        //IList<StringVariable> GetAgencyUpgrades(int clusterId);

       // IList<AgencyUpgrade> GetAgencyAnnualPlan(string isCompleted, DateTime startDate, DateTime endDate, int clusterId);

        AgencyChange GetAgencyChangeCur(Guid agencyId, int clusterId);
        //IList<AgencyChange> GetAgencyChanges(string isComplete, DateTime startDate, DateTime endDate, int clusterId);
      

       

      

        IList<AgencyLocation> GetBranchList(Guid agencyId, int clusterId);

        List<LocationInfo> GetLocationNameWithAgencyName(List<Guid> locationIds, int clusterId);

        List<AgencyLite> GetAgencyContactInformations(List<Guid> agencyIds, int clusterId);

        bool RemoveAgency(Guid Id, int clusterId);
    }
}
