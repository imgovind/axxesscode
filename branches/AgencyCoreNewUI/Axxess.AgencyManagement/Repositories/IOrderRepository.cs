﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IOrderRepository
    {

        bool AddOrder(PhysicianOrder order);
        bool UpdateOrder(PhysicianOrder order);
        bool UpdateOrderModel(PhysicianOrder order);
        bool UpdateOrderStatus(Guid agencyId, Guid orderId, int status, DateTime dateReceived, DateTime dateSend);
        bool MarkOrderAsDeleted(Guid Id, Guid patientId, Guid agencyId, bool isDeprecated);
        bool ReassignOrdersUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        PhysicianOrder GetOrderOnly(Guid Id, Guid agencyId);
        PhysicianOrder GetOrderOnly(Guid Id, Guid patientId, Guid agencyId);
        List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, int status, string orderIds, DateTime startDate, DateTime endDate);
        List<PhysicianOrder> GetPhysicianOrders(Guid agencyId, string orderIds, DateTime startDate, DateTime endDate);
        List<PhysicianOrder> GetPatientPhysicianOrders(Guid agencyId, Guid patientId, string orderIds, DateTime startDate, DateTime endDate);
        List<PhysicianOrder> GetPendingPhysicianSignatureOrders(Guid agencyId, string orderIds, DateTime startDate, DateTime endDate);
        List<PhysicianOrder> GetPhysicianOrdersByPhysician(List<Guid> physicianIdentifiers, int status);
        List<PhysicianOrder> GetAllPhysicianOrders();

        bool AddFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter);
        bool UpdateFaceToFaceEncounterForRequest(Guid agencyId, Guid orderId, int status, DateTime dateRequested);
        bool UpdateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter);
        bool DeleteFaceToFaceEncounter(Guid agencyId, Guid patientId, Guid Id, bool IsDeprecated);
        bool ReassignFaceToFaceEncounterUser(Guid agencyId, Guid patientId, Guid Id, Guid employeeId);
        FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid patientId, Guid agencyId);
        FaceToFaceEncounter GetFaceToFaceEncounter(Guid Id, Guid agencyId);
        List<FaceToFaceEncounter> GetFaceToFaceEncounterOrders(Guid agencyId, int status, string orderIds);
        List<FaceToFaceEncounter> GetPatientFaceToFaceEncounterOrders(Guid agencyId, Guid patientId, string orderIds);
        List<FaceToFaceEncounter> GetPendingSignatureFaceToFaceEncounterOrders(Guid agencyId, string orderIds);
        List<FaceToFaceEncounter> GetFaceToFaceEncountersByPhysician(List<Guid> physicianIdentifiers, int status);
        List<FaceToFaceEncounter> GetAllFaceToFaceEncounters();



    }
}
