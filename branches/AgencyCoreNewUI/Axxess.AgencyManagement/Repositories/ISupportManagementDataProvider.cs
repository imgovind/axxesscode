﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public interface ISupportManagementDataProvider
    {


       ICustomerNotesRepository CustomerNotesRepository
       {
           get;
       }

       IAssetRepository AssetRepository
       {
           get;
       }
    }
}
