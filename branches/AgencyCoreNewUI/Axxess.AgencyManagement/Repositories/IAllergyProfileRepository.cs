﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IAllergyProfileRepository
    {
        bool UpdateAllergyProfile(AllergyProfile allergyProfile);
        AllergyProfile GetAllergyProfileByPatient(Guid patientId, Guid agencyId);
        AllergyProfile GetAllergyProfile(Guid profileId, Guid agencyId);
    }
}
