﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;

    using Repositories;

    public interface IAgencyManagementDataProvider
    {

       
        IPhysicianRepository PhysicianRepository
        {
            get;
        }
        IUserRepository UserRepository
        {
            get;
        }
        IAgencyRepository AgencyRepository
        {
            get;
        }
      
       
        IMessageRepository MessageRepository
        {
            get;
        }

       
    }
}
