﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core;

    public interface IPatientEpisodeRepository
    {
        bool AddEpisode(PatientEpisode patientEpisode);
        bool UpdateEpisode(Guid agencyId, PatientEpisode episode);
        bool UpdateEpisode(PatientEpisode episode);
        bool UpdateEpisodeForDischarge(List<PatientEpisode> episodes);
        bool DeleteEpisode(Guid agencyId, Patient patient, out PatientEpisode episodeDeleted);
        bool DeleteEpisode(Guid agencyId, Guid patientId, Guid episodeId);
        PatientEpisode GetEpisodeById(Guid agencyId, Guid episodeId, Guid patientId);
        PatientEpisode GetEpisodeByIdWithSOC(Guid agencyId, Guid episodeId, Guid patientId);
       // PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date, string discipline);
        //PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId, string discipline);
        //PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId);
        PatientEpisode GetEpisodeOnlyWithPreviousAndAfter(Guid agencyId, Guid episodeId, Guid patientId);
        PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date);
        PatientEpisode GetEpisodeOnly(Guid agencyId, Guid episodeId, Guid patientId);
        PatientEpisode GetNextEpisodeByStartDate(Guid agencyId, Guid patientId, DateTime startDate);
        PatientEpisode GetLastEpisodeByEndDate(Guid agencyId, Guid patientId, DateTime endDate);
       // PatientEpisode GetEpisodeNew(Guid agencyId, Guid patientId, DateTime date, string discipline);
       // PatientEpisode GetEpisodeNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline);
        PatientEpisode GetPatientEpisodeFluent(Guid agencyId, Guid episodeId, Guid patientId);
        PatientEpisode GetCurrentEpisode(Guid agencyId, Guid patientId);
        List<PatientEpisode> GetPatientAllEpisodes(Guid agencyId, Guid patientId);
        List<PatientEpisode> GetPatientActiveEpisodes(Guid agencyId, Guid patientId);
        List<PatientEpisode> GetPatientAllEpisodesWithNoException(Guid agencyId, Guid patientId);
        List<PatientEpisode> EpisodesToDischarge(Guid agencyId, Guid patientId, DateTime dischargeDate);

        PatientEpisodeData GetCurrentEpisodeLean(Guid agencyId, Guid patientId);
        List<PatientEpisodeData> GetEpisodesBetween(Guid agencyId, Guid patientId, DateTime startDate, DateTime endDate);
        NewEpisodeData GetLastEpisode(Guid agencyId, Guid patientId);
        DateRange GetCurrentEpisodeDate(Guid agencyId, Guid patientId);
        DateRange GetNextEpisode(Guid agencyId, Guid patientId);
        DateRange GetPreviousEpisode(Guid agencyId, Guid patientId);
        List<EpisodeLean> GetPatientDeactivatedAndDischargedEpisodes(Guid agencyId, Guid patientId);


        bool IsEpisodeExist(Guid agencyId, Guid episodeId);
        bool SetRecertFlag(Guid agencyId, Guid episodeId, Guid patientId, bool isRecertComplete);
    }
}
