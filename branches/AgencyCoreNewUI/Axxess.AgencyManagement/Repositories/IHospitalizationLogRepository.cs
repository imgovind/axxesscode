﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Domain;

namespace Axxess.AgencyManagement.Repositories
{
    public interface IHospitalizationLogRepository
    {
        bool AddHospitalizationLog(HospitalizationLog transferLog);
        bool UpdateHospitalizationLog(HospitalizationLog transferLog);
        HospitalizationLog GetHospitalizationLog(Guid agencyId, Guid patientId, Guid transferLogId);
        List<HospitalizationLog> GetHospitalizationLogs(Guid patientId, Guid agencyId);
    }
}
