﻿namespace Axxess.AgencyManagement.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain;

    public interface IEmergencyContactRepository
    {
        bool AddEmergencyContact(PatientEmergencyContact emergencyContact);
        bool EditEmergencyContact(Guid agencyId, PatientEmergencyContact emergencyContact);
        bool DeleteEmergencyContacts(Guid patientId);
        bool DeleteEmergencyContact(Guid Id, Guid patientId);
        bool SetPrimaryEmergencyContact(Guid agencyId, Guid patientId, Guid emergencyContactId);
        PatientEmergencyContact GetEmergencyContact(Guid patientId, Guid emergencyContactId);
        IList<PatientEmergencyContact> GetEmergencyContacts(Guid patientId);
        List<EmergencyContactInfo> GetEmergencyContactInfos(Guid agencyId, Guid branchId, int status);

    }
}
