﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

   public class RemitQueue
    {
       public Guid Id { get; set; }
       public Guid AgencyId { get; set; }
       public string Data { get; set; }
       public string Error { get; set; }
       public int Priority { get; set; }
       public string Status { get; set; }
       public bool IsUpload { get; set; }
       public DateTime Created { get; set; }
       public DateTime Modified { get; set; }
    }
}
