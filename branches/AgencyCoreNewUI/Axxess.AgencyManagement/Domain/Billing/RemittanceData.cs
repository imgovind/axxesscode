﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using System.Xml.Serialization;

    [XmlRoot("Remittance")]
    public class RemittanceData
    {
        [XmlElement]
        public string PaymentTotal { get; set; }
        [XmlElement]
        public string CheckNo { get; set; }
        [XmlElement]
        public Payer Payer { get; set; }
        [XmlElement]
        public Payee Payee { get; set; }
        [XmlElement]
        public List<ClaimInfoData> Claim { get; set; }
        [XmlElement]
        public string RemittanceDate { get; set; }
    }

    [XmlRoot()]
    public class Payer
    {
        [XmlElement]
        public string Name { get; set; }
        [XmlElement]
        public string Add1 { get; set; }
        [XmlElement]
        public string Add2 { get; set; }
        [XmlElement]
        public string City { get; set; }
        [XmlElement]
        public string State { get; set; }
        [XmlElement]
        public string Zip { get; set; }
        [XmlElement]
        public string RefType { get; set; }
        [XmlElement]
        public string RefNum { get; set; }
    }
    [XmlRoot()]
    public class Payee
    {
        [XmlElement]
        public string Name { get; set; }
        [XmlElement]
        public string Add1 { get; set; }
        [XmlElement]
        public string Add2 { get; set; }
        [XmlElement]
        public string City { get; set; }
        [XmlElement]
        public string State { get; set; }
        [XmlElement]
        public string Zip { get; set; }
        [XmlElement]
        public string RefType { get; set; }
        [XmlElement]
        public string RefNum { get; set; }
        [XmlElement]
        public string IdType { get; set; }
        [XmlElement]
        public string Id { get; set; }
    }

    [XmlRoot("Claim")]
    public class ClaimInfoData
    {
        [XmlElement]
        public  List<PaymentInformation> ClaimPaymentInformation { get; set; }
        [XmlElement]
        public ProviderLevelAdjustment ProviderLevelAdjustment { get; set; }
    }

    [XmlRoot("ProviderLevelAdjustment")]
    public class ProviderLevelAdjustment
    {
        [XmlElement]
        public string ProviderIdentifier { get; set; }
        [XmlElement]
        public string FiscalPeriodDate { get; set; }
        [XmlElement]
        public string AdjustmentReasonCode { get; set; }
        [XmlElement]
        public string AdjustmentReasonDesc { get; set; }
        [XmlElement]
        public string ProviderAdjustmentIdentifier { get; set; }
        [XmlElement]
        public string ProviderAdjustmentAmount { get; set; }
    }


    [XmlRoot("ClaimPaymentInformation")]
    public class PaymentInformation
    {
        [XmlElement]
        public ServicePaymentInformation ServicePaymentInformation { get; set; }
        [XmlElement]
        public string PatientControlNumber { get; set; }
        [XmlElement]
        public string ClaimStatusCode { get; set; }
        [XmlElement]
        public string ClaimStatusDescription { get; set; }
        [XmlElement]
        public string TotalClaimChargeAmount { get; set; }
        [XmlElement]
        public string ClaimPaymentAmount { get; set; }
        [XmlElement]
        public string TypeOfBill { get; set; }
        [XmlElement]
        public string PatientResponsibilityAmount { get; set; }
        [XmlElement]
        public string ClaimFilingIndicatorCode { get; set; }
        [XmlElement]
        public string PayerClaimControlNumber { get; set; }
        [XmlElement]
        public string FacilityTypeCode { get; set; }
        [XmlElement]
        public string ClaimFrequencyCode { get; set; }
        [XmlElement]
        public OutpatientAdjuctionInformation OutpatientAdjuctionInformation { get; set; }
        [XmlElement]
        public string ReceivedDate { get; set; }
        [XmlElement]
        public string PaymentDate { get; set; }
        [XmlElement]
        public string ClaimStatementPeriodStartDate { get; set; }
        [XmlElement]
        public string ClaimStatementPeriodEndDate { get; set; }
        [XmlElement]
        public string PerDayLimit { get; set; }
        [XmlElement]
        public PatientInfo Patient { get; set; }
        [XmlElement]
        public ClaimAdjustment ClaimAdjustment { get; set; }
        [XmlElement]
        public string ServiceAdjustmentTotal { get; set; }
        [XmlIgnore]
        public List<ClaimLean> AssociatedClaims { get; set; }
        [XmlElement]
        public bool IsPosted { get; set; }
        [XmlElement]
        public DateTime RemittanceDate { get; set; }
        [XmlElement]
        public Guid Id { get; set; }
        [XmlElement]
        public string RemitId { get; set; }
        [XmlElement]
        public long BatchId { get; set; }


    }

    [XmlRoot()]
    public class ServicePaymentInformation
    {
        [XmlElement]
        public ServiceAdjustment ServiceAdjustment { get; set; }
    }
    [XmlRoot()]
    public class ServiceAdjustment
    {
        [XmlElement]
        public string AdjGroup { get; set; }
        [XmlElement]
        public AdjData AdjData { get; set; }
    }
    [XmlRoot()]
    public class ClaimAdjustment
    {
        [XmlElement]
        public string AdjGroup { get; set; }
        [XmlElement]
        public AdjData AdjData { get; set; }
    }
    [XmlRoot()]
    public class AdjData
    {
        [XmlElement]
        public string AdjReason { get; set; }
        [XmlElement]
        public string AdjAmount { get; set; }
        [XmlElement]
        public string AdjQuantity { get; set; }
    }

    [XmlRoot()]
    public class OutpatientAdjuctionInformation
    {
        [XmlElement]
        public string ReimbursementRate { get; set; }
        [XmlElement]
        public string ClaimPayableAmount { get; set; }
        [XmlElement]
        public string RemarkCode { get; set; }
    }

    [XmlRoot("Patient")]
    public class PatientInfo
    {
        [XmlElement]
        public string LastName { get; set; }
        [XmlElement]
        public string FirstName { get; set; }
        [XmlElement]
        public string IdQualifier { get; set; }
        [XmlElement]
        public string IdQualifierName { get; set; }
        [XmlElement]
        public string Id { get; set; }
    }
}
