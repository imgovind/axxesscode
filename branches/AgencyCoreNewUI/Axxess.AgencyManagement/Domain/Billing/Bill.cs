﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    public class Bill
    {
        public Bill()
        {
            this.Claims = new List<ClaimBill>();
        }

        public IList<ClaimBill> Claims { get; set; }

        public bool IsElectronicSubmssion { get; set; }
        public Guid BranchId { get; set; }
        public int Insurance { get; set; }
        public bool IsMedicareHMO { get; set; }
        public string ClaimType { get; set; }

        public Agency Agency { get; set; }

    }
}
