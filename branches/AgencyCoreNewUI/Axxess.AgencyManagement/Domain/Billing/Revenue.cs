﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.Core.Enums;

    public class Revenue
    {
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public string ClaimDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
       // public string Schedule { get; set; }
        public string AssessmentType { get; set; }
        public int Status { get; set; }
        public string HippsCode { get; set; }
        public string AddressZipCode { get; set; }
        public string ProspectivePayment { get; set; }
        public double RapAmount { get; set; }
        public double UnitAmount { get; set; }
        public double UnearnedRevenueAmount { get; set; }
        public double EarnedRevenueAmount { get; set; }
        public double UnbilledRevenueAmount { get; set; }
        public int UnearnedVisitCount { get; set; }
        public int EpisodeDays
        {
            get
            {
                var count = 0;
                if (this.EpisodeStartDate.IsValid() && this.EpisodeEndDate.IsValid() && this.EpisodeEndDate > this.EpisodeStartDate)
                {
                    count = this.EpisodeEndDate.Subtract(this.EpisodeStartDate).Days;
                }
                return count;
            }
        }
        public int BillableVisitCount { get; set; }
        public int CompletedVisitCount { get; set; }
        public int BillableDayCount { get { return 60; } }
        public int CompletedDayCount { get; set; }
        public int RapVisitCount { get; set; }
        public int UnbilledVisitCount { get; set; }
        public int BilledVisitCount { get; set; }
        public string AssessmentTypeName { get; set; }
        public string StatusName
        {
            get
            {
                return Enum.IsDefined(typeof(BillingStatus), this.Status) ? ((BillingStatus)this.Status).GetDescription() : string.Empty;

            }
        }
        public string EpisodeRange
        {
            get
            {
                return string.Format("{0} - {1}", this.EpisodeStartDate != null ? this.EpisodeStartDate.ToString("MM/dd/yyyy") : "", this.EpisodeEndDate != null ? this.EpisodeEndDate.ToString("MM/dd/yyyy") : "");
            }
        }
        public string DisplayName
        {
            get
            {
                return string.Format("{0}, {1}", this.LastName, this.FirstName);
            }
        }
    }
}
