﻿
namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class BillLean
    {

        public BillLean()
        {
            Claims = new List<MedicareClaimLean>();
        }
        public IList<MedicareClaimLean> Claims { get; set; }
        public bool IsElectronicSubmssion { get; set; }
        public Guid BranchId { get; set; }
        public int Insurance { get; set; }
        public bool IsMedicareHMO { get; set; }
        public string Type { get; set; }

        public Agency Agency { get; set; }
    }
}