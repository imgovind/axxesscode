﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic.SqlGeneration.Schema;

namespace Axxess.AgencyManagement.Domain
{
   public class ManagedBill
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public string PatientIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public bool AreVisitsComplete { get; set; }
        public bool IsGenerated { get; set; }
        public string VerifiedVisits { get; set; }
        public bool IsVisitVerified { get; set; }
        public bool IsSupplyVerified { get; set; }
        public bool IsInfoVerified { get; set; }


        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }
        [SubSonicIgnore]
        public string ClaimDateRange
        {
            get
            {
                return string.Format("{0} - {1}", this.EpisodeStartDate.ToString("MM/dd/yyyy"), this.EpisodeEndDate.ToString("MM/dd/yyyy"));
            }
        }

       

    }
}
