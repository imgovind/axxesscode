﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain
{
   public class BillExchange
    {
       public string Status { get; set; }
       public string Message { get; set; }
       public string Result { get; set; }
       public string ClaimId { get; set; }
       public string ClaimType { get; set; }

       public bool isSuccessful { get; set;  }
    }
}
