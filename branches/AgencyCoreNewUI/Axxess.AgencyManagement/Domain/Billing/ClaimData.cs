﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Enums;

    public class ClaimData
    {
        public long Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Data { get; set; }
        public string BillIdentifers { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ClaimType {get; set;}
        public string Response { get; set; }
    }
}
