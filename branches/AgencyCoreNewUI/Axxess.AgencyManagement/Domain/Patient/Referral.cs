﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;

    public class Referral : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid ReferrerPhysician { get; set; }
        public int AdmissionSource { get; set; }
        public DateTime ReferralDate { get; set; }
        public Guid InternalReferral { get; set; }
        public string OtherReferralSource { get; set; }      
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [SubSonicNullString()]
        [SubSonicStringLength(1)]
        public string MiddleInitial { get; set; }
        public string MaritalStatus { get; set; }
        public float Height { get; set; }
        public int HeightMetric { get; set; }
        public float Weight { get; set; }
        public int WeightMetric { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public string SSN { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string PhoneHome { get; set; }
        public string EmailAddress { get; set; }
        public string Physicians { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string ServicesRequired { get; set; }
        public string DME { get; set; }
        public string OtherDME { get; set; }
        public Guid UserId { get; set; }
        public Guid CreatedById { get; set; }
        public int Status { get; set; }
        public string VerificationDate { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string Comments { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime NonAdmissionDate { get; set; }
        public string NonAdmissionReason { get; set; }

        #endregion

        #region Domain
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((ReferralStatus)Enum.ToObject(typeof(ReferralStatus), this.Status));
            }
        }
        [SubSonicIgnore]
        public string ReferralSourceName { get; set; }
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                if (this.AddressLine1 != null)
                {
                    return string.Format("{0} {1} {2}", this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}, {2}, {3} {4}", this.AddressLine1.Trim(), this.AddressLine2.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return string.Format("{0}, {1}, {2} {3}", this.AddressLine1.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public List<string> PhoneHomeArray { get; set; }       
        [SubSonicIgnore]
        public List<string> ServicesRequiredCollection { get; set; }       
        [SubSonicIgnore]
        public List<string> DMECollection { get; set; }  
        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName.ToUpperCase(), ", ", this.FirstName.ToUpperCase());
            }
        }
        [SubSonicIgnore]
        public string DOBFormatted { get { return this.DOB.ToShortDateString(); } }
        [SubSonicIgnore]
        public List<Guid> AgencyPhysicians { get; set; }
        [SubSonicIgnore]
        public List<Physician> PhysicianContacts { get; set; }
        [SubSonicIgnore]
        public string CreatedBy { get; set; }
        [SubSonicIgnore]
        public string PhoneHomeFormatted { get { return this.PhoneHome.ToPhone(); } }
        [SubSonicIgnore]
        public string NonAdmissionDateFormatted { get { return this.NonAdmissionDate.ToShortDateString().ToZeroFilled(); } }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Referral first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Referral last name is required."));
            AddValidationRule(new Validation(() => this.DOB == DateTime.MinValue, "Referral date of birth is required."));
            AddValidationRule(new Validation(() => !this.DOB.ToString().IsValidDate(), "Date of birth is not a valid date."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Gender), "Referral gender has to be selected."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Referral address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Referral city is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Referral state is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Referral zip is required."));
            AddValidationRule(new Validation(() => this.ReferralDate == DateTime.MinValue, "Referral Date is required."));

            if (this.PhoneHome.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhoneHomeArray.Count == 0, "Referral phone is required."));
            }
        }

        #endregion
    }
}
