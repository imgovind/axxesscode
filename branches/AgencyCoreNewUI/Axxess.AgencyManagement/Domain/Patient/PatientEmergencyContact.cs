﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    public class PatientEmergencyContact : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryPhone { get; set; }
        public string AlternatePhone { get; set; }
        public string EmailAddress { get; set; }
        public string Relationship { get; set; }
        public bool IsPrimary { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public List<string> PhonePrimaryArray { get; set; }
        [SubSonicIgnore]
        public List<string> PhoneAlternateArray { get; set; }

        [SubSonicIgnore]
        public string DisplayName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }

        [SubSonicIgnore]
        public string PrimaryPhoneFormatted { get { return this.PrimaryPhone.ToPhone(); } }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.FirstName), "Emergency contact first name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.LastName), "Emergency contact last name is required."));

            if (this.PrimaryPhone.IsNullOrEmpty())
            {
                AddValidationRule(new Validation(() => this.PhonePrimaryArray.Count == 0, "Emergency contact primary phone is required."));
            }
        }

        #endregion

    }
}
