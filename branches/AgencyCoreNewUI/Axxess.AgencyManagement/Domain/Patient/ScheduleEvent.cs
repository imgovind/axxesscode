﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;
    using System.Web.Script.Serialization;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;
    using Axxess.Core.Enums;

    using Extensions;
    using Axxess.Core;

    [XmlRoot()]
    [DataContract]
    public class ScheduleEvent
    {
        public ScheduleEvent()
        {
            this.UserId = Guid.Empty;
            this.EventId = Guid.Empty;
            this.Assets = new List<Guid>();
        }

        [XmlIgnore]
        public Guid AgencyId { get; set; }
        
        [XmlElement]
        [DataMember]
        [SubSonicPrimaryKey]
        public Guid EventId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid PatientId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid EpisodeId { get; set; }

        [XmlElement]
        [DataMember]
        public Guid UserId { get; set; }

        [XmlElement]
        [DataMember]
        public int DisciplineTask { get; set; }

        [XmlElement]
        [DataMember]
        public string UserName { get; set; }

        [XmlElement("EventDate")]
        [SubSonicIgnore]
        public string EventDateXml { get { return EventDate.ToString("MM/dd/yyyy"); } set { EventDate = value.IsNotNullOrEmpty() && value.IsValidDate() ? value.ToDateTime() : DateTime.MinValue; } }

        [XmlElement("VisitDate")]
        [SubSonicIgnore]
        public string VisitDateXml { get { return VisitDate.ToString("MM/dd/yyyy"); } set { VisitDate = value.IsNotNullOrEmpty() && value.IsValidDate() ? value.ToDateTime() : DateTime.MinValue; } }

        [XmlIgnore]
        [DataMember]
        public DateTime EventDate { get; set; }

        [XmlIgnore]
        [DataMember]
        public DateTime VisitDate { get; set; }

        [XmlElement]
        [DataMember]
        public int Status { get; set; }

        [XmlElement]
        [DataMember]
        public string Discipline { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsBillable { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsMissedVisit { get; set; }

        [XmlElement]
        [DataMember]
        public string TimeIn { get; set; }

        [XmlElement]
        [DataMember]
        public string TimeOut { get; set; }
       
        [XmlElement]
        [DataMember]
        public string Surcharge { get; set; }

        [XmlElement]
        [DataMember]
        public string AssociatedMileage { get; set; }

        [XmlElement]
        [DataMember]
        public string ReturnReason { get; set; }

        [XmlElement]
        [DataMember]
        public string Comments { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsDeprecated { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsOrderForNextEpisode { get; set; }

        [XmlElement]
        [DataMember]
        public bool IsVisitPaid { get; set; }

        [XmlElement]
        [DataMember]
        public int Version { get; set; }

        [XmlElement]
        [DataMember]
        public bool InPrintQueue { get; set; }

        public List<Guid> Assets { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public List<NotesQuestion> Questions { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string StatusName
        {
            get
            {
                if (this.Status > 0)
                {
                    if (this.IsMissedVisit)
                    {
                        return "Missed Visit";
                    }
                    else
                    {
                        var status = Enum.IsDefined(typeof(ScheduleStatus), this.Status) ? (ScheduleStatus)this.Status : ScheduleStatus.NoStatus;
                        if ((status == ScheduleStatus.OasisNotYetDue || status == ScheduleStatus.NoteNotYetDue || status == ScheduleStatus.OrderNotYetDue) && this.EventDate.IsValid() && this.EventDate.Date <= DateTime.Now.Date)
                        {
                            return ScheduleStatus.CommonNotStarted.GetDescription();
                        }
                        else
                        {
                            return status.GetDescription();
                        }
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string DisciplineTaskName
        {
            get
            {
                if (Enum.IsDefined(typeof(DisciplineTasks), this.DisciplineTask)) { return ((DisciplineTasks) this.DisciplineTask).GetDescription(); } else { return string.Empty; };
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string PatientName { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string Url { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string PrintUrl { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string ActionUrl { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public Guid PhysicianId { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string AttachmentUrl
        {
            get
            {
                if (this.Asset.IsNotNullOrEmpty())
                {
                    var assets = this.Asset.ToObject<List<Guid>>();
                    if (assets != null && assets.Count > 0)
                    {
                        return "<span class=\"img icon paperclip\"></span>";
                    }
                }
                return string.Empty;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string MissedVisitComments { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string EpisodeNotes { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string StatusComment
        {
            get
            {
                var result = string.Empty;
                if (this.IsMissedVisit && this.MissedVisitComments.IsNotNullOrEmpty()) result += this.MissedVisitComments.Clean() + "\r\n";
                if (this.ReturnReason.IsNotNullOrEmpty() && this.Status != (int)ScheduleStatus.NoteCompleted) result += this.ReturnReason;
                return result;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsPastDue
        {
            get
            {
                return ( this.EventDate.IsValid() && EventDate.Date < DateTime.Now.Date && (this.Status == (int)ScheduleStatus.NoteNotYetDue || this.Status == (int)ScheduleStatus.OasisNotYetDue || this.Status == (int)ScheduleStatus.OrderNotYetDue));
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsComplete
        {
            get
            {
                return ScheduleStatusFatory.OnAndAfterQAStatus().Exists(s => s == this.Status);
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsOrphaned
        {
            get
            {
                var result = false;
                if (this.EventDate.IsValid())
                {
                    if (this.EventDate.Date < this.StartDate.Date || this.EventDate.Date > this.EndDate.Date)
                    {
                        if (this.DisciplineTask != 102)
                        {
                            result = true;
                        }
                    }
                }
                return result;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public string PatientIdNumber { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string EventDateSortable
        {
            get
            {
                //return EventDate.IsNotNullOrEmpty() ? "<div class='float-left'><span class='float-right'>" + EventDate.Split('/')[2] + "</span><span class='float-right'>/</span><span class='float-right'>" + EventDate.Split('/')[0] + "/" + EventDate.Split('/')[1] + "</span></div>" : "";
                return string.Format("<div class='fl'><span class='fr'>{0:0000}</span><span class='fr'>/</span><span class='fr'>{1:00}/{2:00}</span></div>", this.EventDate.Year, this.EventDate.Month, this.EventDate.Day);
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public int Unit
        {
            get
            {
                var timeIn = DateTime.Now;
                var timeOut = DateTime.Now;
                if (this.TimeOut.IsNotNullOrEmpty() && this.TimeOut.HourToDateTime(ref timeOut) && this.TimeIn.IsNotNullOrEmpty() && this.TimeIn.HourToDateTime(ref timeIn) && timeOut >= timeIn)
                {
                    var min = (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
                    if (min > 0)
                    {
                        return (int)Math.Ceiling((double)min / 15);
                    }
                    return 0;
                }
                return 0;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public int MinSpent
        {
            get
            {
                var timeIn = DateTime.Now;
                var timeOut = DateTime.Now;
                if (this.TimeOut.IsNotNullOrEmpty() && this.TimeOut.HourToDateTime(ref timeOut) && this.TimeIn.IsNotNullOrEmpty() && this.TimeIn.HourToDateTime(ref timeIn))
                {
                    if (!timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")))
                    {
                        var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                        var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                        return outi + outO;
                    }
                    else if (timeOut.ToString("tt").IsEqual(timeIn.ToString("tt")) && (timeOut.Hour * 60 + timeOut.Minute) < (timeIn.Hour * 60 + timeIn.Minute))
                    {
                        var outi = 12 * 60 - timeIn.Hour * 60 - (timeIn.Minute);
                        var outO = ((timeOut.Hour >= 12 ? Math.Abs(timeOut.Hour - 12) : timeOut.Hour)) * 60 + (timeOut.Minute);
                        return outi + outO + 12 * 60;
                    }
                    else
                    {
                        if (timeOut >= timeIn)
                        {
                            return (timeOut.Hour - timeIn.Hour) * 60 + (timeOut.Minute - timeIn.Minute);
                        }
                    }
                }
                return 0;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public Guid NewEpisodeId { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsEpisodeReassiged { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string OasisProfileUrl { get; set; }

        [XmlIgnore]
        public string Asset { get; set; }

        #region User Data

        [XmlIgnore]
        [SubSonicIgnore]
        [ScriptIgnore]
        public string UserFirstName { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        [ScriptIgnore]
        public string UserLastName { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        [ScriptIgnore]
        public string UserSuffix { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        [ScriptIgnore]
        public string Credentials { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        [ScriptIgnore]
        public string CredentialsOther { get; set; }

        [XmlIgnore]
        [ScriptIgnore]
        [SubSonicIgnore]
        public bool IsUserDeprecated { get; set; }
       
        [XmlIgnore]
        [SubSonicIgnore]
        public string UserDisplayName
        {
            get
            {
                if (this.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport)
                {
                    return this.UserName;
                }
                else
                {
                    var credential = string.Empty;
                    if (Credentials.IsEqual("None"))
                    {
                    }
                    else
                    {
                        if (CredentialsOther.IsNotNullOrEmpty())
                        {
                            credential = this.CredentialsOther;
                        }
                        else
                        {
                            credential = this.Credentials;
                        }
                    }
                    return string.Format("{0} {1} {2} {3} {4}", this.UserFirstName, this.UserLastName, this.UserSuffix, credential, this.IsUserDeprecated ? " [deleted]" : string.Empty);
                }
            }
        }

        #endregion

        [XmlIgnore]
        [SubSonicIgnore]
        public string VisitType { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string Note { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public Guid NextEpisodeId { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string Icons {
            get {
                string icons = string.Empty;
                if (this.OasisProfileUrl.IsNotNullOrEmpty()) icons += this.OasisProfileUrl + " | ";
                if (this.StatusComment.IsNotNullOrEmpty()) icons += "<a class=\"sticky-note-icon red\" tooltip=\"" + this.StatusComment + "\"></a> | ";
                if (this.Comments.IsNotNullOrEmpty()) icons += "<a class=\"sticky-note-icon\" tooltip=\"" + this.Comments + "\"></a> | ";
                if (this.EpisodeNotes.IsNotNullOrEmpty()) icons += "<a class=\"sticky-note-icon blue\" tooltip=\"" + this.EpisodeNotes + "\"></a> | ";
                if (this.PrintUrl.IsNotNullOrEmpty()) icons += this.PrintUrl + " | ";
                if (this.AttachmentUrl.IsNotNullOrEmpty()) icons += this.AttachmentUrl + " | ";
                if (icons.IsNotNullOrEmpty()) icons = icons.Substring(0, icons.Length - 3);
                return icons;
            }
        }
    }
}
