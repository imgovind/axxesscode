﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Enums;

namespace Axxess.AgencyManagement.Domain
{
    public class OASISAndNote
    {
        public Guid EventId { get; set; }

        public Guid PatientId { get; set; }

        public Guid EpisodeId { get; set; }

        public Guid UserId { get; set; }

        public int DisciplineTask { get; set; }

        public string Status { get; set; }

        public string Discipline { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public List<NotesQuestion> Questions { get; set; }

        public string PatientName { get; set; }

        public DateTime EventDate { get; set; }

        public DateTime VisitDate { get; set; }

      
    }
}
