﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain
{
    public class MedicationCategory
    {
     
        public string Text { get; set; }
      
        public string Value { get; set; }
    }
}
