﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Domain
{
    public class VisitData
    {
        public string Frequency { get; set; }
        public int Count { get; set; }

        public VisitData(string freq, int count) 
        {
            Frequency = freq;
            Count = count;
        }

        public VisitData(string freq)
            : this(freq, 0) { }
       
        public VisitData() : this("", 0) {}
    }
}
