﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;

    public class PatientOnCallListing
    {
        public Guid Id { get; set; }
        public string SocCertPeriod { get; set; }
        public string SocEndDate { get; set; }
        public string PatientSoC { get; set; }
        public string respEmp { get; set; }
        public string PatientPatientID { get; set; }
        public string PatientLastName { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientGender { get; set; }
        public string PatientMedicareNumber { get; set; }
        public DateTime PatientDOB { get; set; }
        public string PatientPhone { get; set; }
        public string PatientAddressLine1 { get; set; }
        public string PatientAddressLine2 { get; set; }
        public string PatientAddressCity { get; set; }
        public string PatientAddressStateCode { get; set; }
        public string PatientAddressZipCode { get; set; }
        public string PatientPrimaryDiagnosis { get; set; }
        public string PatientSecondaryDiagnosis { get; set; }
        public string PatientDiagnosis3 { get; set; }
        public string PatientDiagnosis4 { get; set; }
        public string PatientDiagnosis5 { get; set; }
        public string PatientDiagnosis6 { get; set; }
        public string PatientPrimaryDiagnosisCode { get; set; }
        public string PatientSecondaryDiagnosisCode { get; set; }
        public string PatientDiagnosis3Code { get; set; }
        public string PatientDiagnosis4Code { get; set; }
        public string PatientDiagnosis5Code { get; set; }
        public string PatientDiagnosis6Code { get; set; }
        public string PhysicianName { get; set; }
        public string PhysicianPhone { get; set; }
        public string PhysicianFacsimile { get; set; }
        public string PatientInsurance { get; set; }
        public string PatientCaseManager { get; set; }
        public string PhysicianPhoneHome { get; set; }
        public string PhysicianPhoneMobile { get; set; }
        public string PhysicianEmailAddress { get; set; }
        public string PatientPriority { get; set; }
        public string ContactName { get; set; }
        public string ContactRelation { get; set; }
        public string ContactPhoneHome { get; set; }
        public string ContactEmailAddress { get; set; }
        public string PatientAllergies { get; set; }
        public string PatientMeds { get; set; }

    }

}
