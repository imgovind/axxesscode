﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
using SubSonic.SqlGeneration.Schema;

    class AgencySystemMessage : EntityBase
    {
        [SubSonicPrimaryKey]
        public Guid SystemMessageId { get; set; }
        public Guid AgencyId { get; set; }

        protected override void AddValidationRules()
        {
            
        }
    }
}
