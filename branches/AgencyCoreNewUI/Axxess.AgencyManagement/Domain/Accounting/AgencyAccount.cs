﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;

    public class AgencyAccount
    {
        public string AgencyName { get; set; }
        public string AccountId { get; set; }
        public string PaymentSource { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string StateFilter { get; set; }
        public int ZipCode { get; set; }
        public string SalesPerson { get; set; }
        public string ImplementationFee { get; set; }
        public DateTime InitialSignDate { get; set; }
        public string SpecialNotes { get; set; }
        public int Status { get; set; }
        public int StartUp { get; set; }
        public List<PaymentHistory> Payments { get; set; }
        public PaymentHistory UpdatePayment { get; set; }

    }
}
