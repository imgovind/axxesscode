﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;


    public class AccountingPaymentViewData
    {
        public string PrevViewDate { get; set; }
        public string CurrentViewDate { get; set; }
        public string NextViewDate { get; set; }
        public List<string> Titles { get; set; }
        public List<AgencyPayments> AgencyPaymentData { get; set; }
        public List<string> ListOfTotalAmounts { get; set; }
        public string BigTotal { get; set; }
    }
}
