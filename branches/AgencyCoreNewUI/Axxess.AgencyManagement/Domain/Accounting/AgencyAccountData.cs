﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core.Extension;

    public class AgencyAccountData
    {
        #region Members

        public Guid AgencyId { get; set; }
        public string Account { get; set; }
        public bool IsFrozen { get; set; }

        public int ClusterId { get; set; }

        #endregion
    }
}
