﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;


    public class ChartRows
    {
        public string name { get; set; }
        public List<double> data { get; set; }
    }
}