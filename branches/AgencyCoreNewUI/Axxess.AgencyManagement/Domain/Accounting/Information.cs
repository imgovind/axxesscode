﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;


    public class Information
    {

        #region Members

        public int Id { get; set; }
        public string Account { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
        public string ImplementationFee { get; set; }
        public string PaymentSource { get; set; }
        public DateTime InitialSignDate { get; set; }
        public string SpecialNotes { get; set; }
        public int Status { get; set; }
        public string SalesPerson { get; set; }
        public int StartUp { get; set; }
        public int Sequence { get; set; }
        
        [SubSonicIgnore]
        public List<Payment> Payment { get; set; }

        [SubSonicIgnore]
        public int PaymentId { get; set; }

        [SubSonicIgnore]
        public DateTime Start { get; set; }

        [SubSonicIgnore]
        public DateTime End { get; set; }

        [SubSonicIgnore]
        public int Package { get; set; }

        [SubSonicIgnore]
        public double Amount { get; set; }

        #endregion
    }
}
