﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;


    public class AgencyPayments
    {
        public string AgencyName { get; set; }
        public string AccountId { get; set; }
        public string PaymentSource { get; set; }
        public string InitialSignDate { get; set; }
        public string FirstBillableDate { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int ZipCode { get; set; }
        public int StartUp { get; set; }
        public int Status { get; set; }
        public string SalesPerson { get; set; }
        public string SpecialNotes { get; set; }
        public List<string> Payments { get; set; }
        public string Total { get; set; }
    }
}
