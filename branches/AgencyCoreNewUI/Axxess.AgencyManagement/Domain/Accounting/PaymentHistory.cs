﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core.Extension;

    public class PaymentHistory
    {
        #region Members

        public int Id { get; set; }
        public string Amount { get; set; }
        public string Account { get; set; }
        public int Sequence { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Package { get; set; }

        #endregion
    }
}
