﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;


    public class MetricsRows
    {
        public string Name { get; set; }
        public List<string> Data { get; set; }
        public string Total { get; set; }
    }
}