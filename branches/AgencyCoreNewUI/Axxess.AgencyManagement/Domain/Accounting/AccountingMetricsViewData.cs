﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;


    public class AccountingMetricsViewData
    {
        public List<string> Months { get; set; }
        public List<MetricsRows> MetricsRows { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
        public string PrevViewDate { get; set; }
        public string NextViewDate { get; set; }
    }
}
