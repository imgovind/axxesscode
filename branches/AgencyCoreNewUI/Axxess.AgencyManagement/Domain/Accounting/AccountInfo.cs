﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;


    public class AccountInfo
    {

        #region Members

        public int Id { get; set; }
        public string Account { get; set; }
        public string Name { get; set; }
        public DateTime InitialSignDate { get; set; }
        
        #endregion
    }
}
