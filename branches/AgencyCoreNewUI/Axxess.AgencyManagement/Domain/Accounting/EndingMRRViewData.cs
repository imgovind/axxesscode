﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;
    using SubSonic.SqlGeneration.Schema;


    public class EndingMRRViewData
    {
       public string Caption { get; set; }
       public List<string> Column { get; set; }
       public List<ChartRows> Rows { get; set; }
       public DateTime StartDate { get; set; }
       public DateTime EndDate { get; set; }
 
    }
}
