﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;

    public class UserSnapshot
    {
        public Guid Id { get; set; }
        public Guid LoginId { get; set; }
        public Guid AgencyId { get; set; }
        //public string DisplayName { get; set; }
        public string TitleType { get; set; }
        public int Status { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }

        [SubSonicIgnore]
        public int ClusterId { get; set; }
    }

}
