﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class UserLocation
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid AgencyId { get; set; }
        public Guid AgencyLocationId { get; set; }
    }
}
