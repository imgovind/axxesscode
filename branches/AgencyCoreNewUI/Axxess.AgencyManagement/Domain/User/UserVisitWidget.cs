﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class UserVisitWidget
    {
        public string TaskName { get; set; }
        public string EventDate { get; set; }
        public string PatientName { get; set; }
    }
}
