﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel.DataAnnotations;

    using Axxess.AgencyManagement.Enums;
   
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    [XmlRoot()]
    public class UserRate : EntityBase
    {
        [XmlElement]
        [UIHint("DisciplineTask")]
        public int Id { get; set; }
        [XmlElement]
        public double Rate { get; set; }
        [XmlElement]
        public double MileageRate { get; set; }
        [XmlIgnore]
        public Guid UserId { get; set; }
        [XmlIgnore]
        public string DisciplineTaskName
        {
            get
            {
                if (Enum.IsDefined(typeof(DisciplineTasks), this.Id)) { return ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), this.Id)).GetActualDescription(); } else { return string.Empty; }
            }
        }

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => this.Id <= 0, "Task is required."));
            AddValidationRule(new Validation(() => this.Rate < 0, "Rate is required."));
        }

        #endregion

    }
}
