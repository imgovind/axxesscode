﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using System.Xml.Serialization;

    public class AgencyLocation : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Name { get; set; }
        public string CustomId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public string AddressZipCodeFour{ get; set; }
        public string PhoneWork { get; set; }
        public string FaxNumber { get; set; }
        public string Comments { get; set; }
        public bool IsMainOffice { get; set; }
        public bool IsDeprecated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string MedicareProviderNumber { get; set; }
        public string CBSA { get; set; }
        public string Cost { get; set; }
        public string BillData { get; set; }
        public bool IsSubmitterInfoTheSame { get; set; }
        public string SubmitterId { get; set; }
        public string SubmitterName { get; set; }
        public string SubmitterPhone { get; set; }
        public string SubmitterFax { get; set; }
        public string Payor { get; set; }
        public string BranchId { get; set; }
        public string BranchIdOther { get; set; }
        public string Ub04Locator81cca { get; set; }

        public bool IsLocationStandAlone { get; set; }
        public string TaxId { get; set; }
        public string TaxIdType { get; set; }
        public string NationalProviderNumber { get; set; }
        public string MedicaidProviderNumber { get; set; }
        public string HomeHealthAgencyId { get; set; }

        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonPhone { get; set; }
        public int CahpsVendor { get; set; }
        public string CahpsVendorId { get; set; }
        public string CahpsSurveyDesignator { get; set; }
        public bool IsAxxessTheBiller { get; set; }
        public int OasisAuditVendor { get; set; }
        public string FullName { get; set; }
        public string ZipNineStatus { get; set; }
        

        #endregion

        #region Domain
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> PhoneArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> FaxNumberArray { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> SubmitterPhoneArray { get; set; }
        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> SubmitterFaxArray { get; set; }
       
        [SubSonicIgnore]
        public string PhoneWorkFormatted { get { return PhoneWork.ToPhone(); } }
        [SubSonicIgnore]
        public string FaxNumberFormatted { get { return FaxNumber.ToPhone(); } }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressSecondRow
        {
            get
            {
                return string.Format("{0} {1} {2}{3}", this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim(), this.AddressZipCodeFour.IsNotNullOrEmpty() ? string.Format("-{0}", this.AddressZipCodeFour.Trim()) : string.Empty);

            }
        }
        [XmlIgnore]
        [SubSonicIgnore]
        public string AddressFull
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1} {2}, {3} {4}", this.AddressLine1.Trim(), this.AddressLine2.Trim(), this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return string.Format("{0} {1}, {2} {3}", this.AddressLine1.Trim(), this.AddressCity.Trim(), this.AddressStateCode.Trim(), this.AddressZipCode.Trim());
                }
                return string.Empty;
            }
        }

        [XmlIgnore]
        [SubSonicIgnore]
        public List<string> ContactPhoneArray { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public string InsuranceName { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public bool IsUserPlan { get; set; }

        [XmlIgnore]
        [SubSonicIgnore]
        public int PlanLimit { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Name), "Location Name is required."));
           // AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.MedicareProviderNumber), "Location Medicare Provider Number is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressLine1), "Location Address line is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressCity), "Location City is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressStateCode), "Location State is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.AddressZipCode), "Location Zipcode is required."));
            if (this.IsLocationStandAlone)
            {
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.TaxId), "Tax Id is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Payor), "Payor is required."));
                AddValidationRule(new Validation(() => this.Payor.IsNotNullOrEmpty()&& !this.Payor.IsInteger(), "Payor is not in the right format."));
                AddValidationRule(new Validation(() => this.Payor.IsNotNullOrEmpty() && this.Payor.IsInteger() && this.Payor.ToInteger() <= 0, "Payor is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.NationalProviderNumber), "National Provider Number is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.MedicareProviderNumber), "Medicare Provider Number is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.HomeHealthAgencyId), "Unique Agency OASIS Id Code is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.ContactPersonFirstName), "Contact Person First Name is required."));
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.ContactPersonLastName), "Contact Person Last Name is required."));
                if (!this.IsAxxessTheBiller)
                {
                    AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.SubmitterId), "Submitter Id is required."));
                    AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.SubmitterName), "Submitter Name is required."));
                }
            }
            AddValidationRule(new Validation(() => this.BranchId != "N" && this.BranchId != "P" && this.BranchId != "Other", "Branch Id Number is required."));
            if (this.BranchId == "Other")
            {
                AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.BranchIdOther), "Custom Branch Id Number is required."));
            }

        }

        #endregion
    }
}
