﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Collections.Generic;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Enums;
    using System.Xml.Serialization;

    [XmlRoot()]
    public class AgencyInsurance : EntityBase
    {
        #region Members

        [XmlElement]
        public int Id { get; set; }
        [XmlElement]
        public Guid AgencyId { get; set; }
        [XmlElement]
        public int PayorType { get; set; }
        [XmlElement]
        public int InvoiceType { get; set; }
        [XmlElement]
        public int ChargeGrouping { get; set; }
        [XmlElement]
        public int ChargeType { get; set; }
        [XmlElement]
        public int ParentInsurance { get; set; }
        [XmlElement]
        public string ClearingHouse { get; set; }
        [XmlElement]
        public string InterchangeReceiverId { get; set; }
        [XmlElement]
        public string PayorId { get; set; }
        [XmlElement]
        public string Name { get; set; }
        [XmlElement]
        public string AddressLine1 { get; set; }
        [XmlElement]
        public string AddressLine2 { get; set; }
        [XmlElement]
        public string AddressCity { get; set; }
        [XmlElement]
        public string AddressStateCode { get; set; }
        [XmlElement]
        public string AddressZipCode { get; set; }
        [XmlElement]
        public string ProviderId { get; set; }
        [XmlElement]
        public string OtherProviderId { get; set; }
        [XmlElement]
        public string SubmitterId { get; set; }
        [XmlElement]
        public string HealthPlanId { get; set; }
        [XmlElement]
        public string ProviderSubscriberId { get; set; }
        [XmlElement]
        public string Ub04Locator81cca { get; set; }
        [XmlElement]
        public string Charge { get; set; }
        [XmlElement]
        public string BillData { get; set; }
        [XmlElement]
        public string PhoneNumber { get; set; }
        [XmlElement]
        public string FaxNumber { get; set; }
        [XmlElement]
        public string ContactPersonFirstName { get; set; }
        [XmlElement]
        public string ContactPersonLastName { get; set; }
        [XmlElement]
        public string ContactEmailAddress { get; set; }
        [XmlElement]
        public bool IsAxxessTheBiller { get; set; }
        [XmlElement]
        public bool IsInsuranceRateUsed { get; set; }
        [XmlElement]
        public decimal CurrentBalance { get; set; }
        [XmlElement]
        public int WorkWeekStartDay { get; set; }
        [XmlElement]
        public bool IsVisitAuthorizationRequired { get; set; }
        [XmlElement]
        public bool DefaultFiscalIntermediary { get; set; }
        [XmlElement]
        public bool IsDeprecated { get; set; }
        [XmlElement]
        public string SubmitterName { get; set; }
        [XmlElement]
        public string SubmitterPhone { get; set; }
        [XmlElement]
        public string ClearingHouseSubmitterId { get; set; }
        [XmlElement]
        public string BillType { get; set; }
        [XmlElement]
        public DateTime Created { get; set; }
        [XmlElement]
        public DateTime Modified { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        [XmlIgnore]
        public string ContactPerson
        {
            get
            {
                return string.Format("{0} {1}", this.ContactPersonFirstName, this.ContactPersonLastName);
            }
        }


        [SubSonicIgnore]
        [XmlIgnore]
        public string AddressFirstRow
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}", this.AddressLine1.Trim(), this.AddressLine2.Trim());
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return this.AddressLine1.Trim();
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        [XmlIgnore]
        public string AddressSecondRow
        {
            get
            {
                if (this.AddressLine1 != null)
                {
                    return string.Format("{0} {1} {2}", this.AddressCity.IsNotNullOrEmpty()? this.AddressCity.Trim() :string.Empty,this.AddressStateCode.IsNotNullOrEmpty()? this.AddressStateCode.Trim():string.Empty, this.AddressZipCode.IsNotNullOrEmpty()? this.AddressZipCode.Trim():string.Empty);
                }
                return string.Empty;
            }
        }

        [SubSonicIgnore]
        [XmlIgnore]
        public string AddressFull
        {
            get
            {
                if (this.AddressLine1.IsNotNullOrEmpty() && this.AddressLine2.IsNotNullOrEmpty())
                {
                    return string.Format("{0} {1}, {2} ,{3}  {4}", this.AddressLine1.Trim(), this.AddressLine2.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                if (this.AddressLine1.IsNotNullOrEmpty() && string.IsNullOrEmpty(this.AddressLine2))
                {
                    return string.Format("{0} , {1} ,{2}  {3}", this.AddressLine1.Trim(), this.AddressCity, this.AddressStateCode, this.AddressZipCode);
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        [XmlIgnore]
        public List<string> SubmitterPhoneArray { get; set; }
        [SubSonicIgnore]
        [XmlIgnore]
        public List<string> PhoneNumberArray { get; set; }
        [SubSonicIgnore]
        [XmlIgnore]
        public List<string> FaxNumberArray { get; set; }

        [SubSonicIgnore]
        [XmlIgnore]
        public string InvoiceTypeName
        {
            get
            {
                return ((InvoiceType)Enum.ToObject(typeof(InvoiceType), this.InvoiceType)).GetDescription();
            }
        }

        [SubSonicIgnore]
        [XmlIgnore]
        public string PayerTypeName
        {
            get
            {
                return ((PayerTypes)Enum.ToObject(typeof(PayerTypes), this.PayorType)).GetDescription();
            }
        }

        [SubSonicIgnore]
        [XmlIgnore]
        public int OldInsuranceId { get; set; }
        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Name), "Insurance Name is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.PayorId), "Payor Id is required."));
        }

        #endregion

    }
}
