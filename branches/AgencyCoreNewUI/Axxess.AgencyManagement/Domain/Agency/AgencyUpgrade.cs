﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using SubSonic.SqlGeneration.Schema;

    using Axxess.Core.Extension;

    using Extensions;

    public class AgencyUpgrade
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid RequestedById { get; set; }
        public string AccountId { get; set; }
        public double PreviousAmount { get; set; }
        public double Amount { get; set; }
        public int PreviousPackageId { get; set; }
        public int RequestedPackageId { get; set; }
        public int AnnualPlanId { get; set; }
        public string Comments { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        public bool IsComplete { get; set; }
        public bool IsUserPlan { get; set; }
        public Guid AgencyLocationId { get; set; }

        [SubSonicIgnore]
        public string AgencyName { get; set; }

        [SubSonicIgnore]
        public string LocationName { get; set; }

        [SubSonicIgnore]
        public string RequestedBy { get; set; }

        [SubSonicIgnore]
        public string RequestedPackageDescription
        {
            get
            {
                if (this.PreviousPackageId != this.RequestedPackageId)
                {
                    if (this.IsUserPlan)
                    {

                        return RequestedPackageId.UserSubscriptionPlanName();
                    }
                    else
                    {
                        return RequestedPackageId.PatientSubscriptionPlanName();
                    }
                }
                return string.Empty;
            }
        }

        [SubSonicIgnore]
        public string PreviousPackageDescription
        {
            get
            {
                if (this.IsUserPlan)
                {

                    return PreviousPackageId.UserSubscriptionPlanName();
                }
                else
                {
                    return PreviousPackageId.PatientSubscriptionPlanName();
                }
            }
        }

        [SubSonicIgnore]
        public string AnnualPlanDescription
        {
            get
            {
                if (this.AnnualPlanId == 1)
                {
                    return "5% Discount for 6 month payment";
                }
                else if (this.AnnualPlanId == 2)
                {
                    return "10% Discount for full year payment";
                }
                else if (this.AnnualPlanId == 3)
                {
                    return "15% Discount for 2 years payment";
                }
                return string.Empty;
            }
        }

        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public string Complete { get { return this.IsComplete? "Yes" : "No"; } }


        [SubSonicIgnore]
        public int ClusterId { get; set; }
    }
}
