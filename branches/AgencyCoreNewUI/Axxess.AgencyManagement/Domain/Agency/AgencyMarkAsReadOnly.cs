﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;
    using Extensions;

    public class AgencyMarkAsReadOnly
    {
        public Guid AgencyId { get; set; }
        public string AgencyName { get; set; }
        public bool IsFrozen { get; set; }
        public string FrozenReason { get; set; }
        public bool IsSuspendPayment { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime Created { get; set; }
       // public string ContactPersonLastName { get; set; }
        //public string ContactPersonFirstName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonPhone { get; set; }
       


        [SubSonicIgnore]
        public string ContactPersonDisplayName {set; get;}
        //{
        //    get
        //    {
        //        return string.Concat(this.ContactPersonFirstName, " ", this.ContactPersonLastName);
        //    }
        //}

        [SubSonicIgnore]
        public string EffectiveDateFormatted { get { return this.EffectiveDate.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public string CreateDateFormatted { get { return this.Created.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public string SuspendPayment { get { return this.IsSuspendPayment ? "Yes" : "No"; } }

        [SubSonicIgnore]
        public string Frozen { get { return this.IsFrozen ? "Yes" : "No"; } }

        [SubSonicIgnore]
        public int ClusterId { get; set; }
    }
}
