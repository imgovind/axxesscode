﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;

    public class AgencyProfile
    {
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public bool IsSuspendPayment { get; set; }
        public string FrozenReason { get; set; }
        public Guid AssetId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime Created { get; set; }

    }
}
