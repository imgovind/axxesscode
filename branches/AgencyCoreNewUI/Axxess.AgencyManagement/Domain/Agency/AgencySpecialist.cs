﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;
    using SubSonic.SqlGeneration.Schema;
    
    public class AgencySpecialist
    {
        public Guid AgencyId { get; set; }
        public Guid SpecialistId { get; set; }
        public string AgencyName { get; set; }
        public string AgencyContactDisplayName { get; set; }
        public string AgencyContactPersonEmail { get; set; }
        public string AgencyContactPersonPhone { get; set; }
        public string SpecialistName { get; set; }
        public string SpecialistEmailAddress { get; set; }
        public DateTime AgencyCreated { get; set; }
        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.AgencyCreated.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public int ClusterId { get; set; }
      
    }
}
