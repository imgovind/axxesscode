﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using Axxess.Core.Extension;

    public class AgencySelection
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string StateCode { get; set; }
        public DateTime Created { get; set; }
        public string AttentionLevel 
        { 
            get 
            {
                var attentionLevel = string.Empty;
                if (Created >= DateTime.Now.AddDays(-90))
                {
                    attentionLevel = "red";
                }
                else if (Created >= DateTime.Now.AddDays(-180) && Created <= DateTime.Now.AddDays(-90))
                {
                    attentionLevel = "blue";
                }
                return attentionLevel; 
            } 
        }
    }
}
