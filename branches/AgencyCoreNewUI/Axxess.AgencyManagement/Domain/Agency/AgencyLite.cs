﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;
    
    public class AgencyLite
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Title { get; set; }
        public bool IsFrozen { get; set; }
        public string Trainer { get; set; }
        public Guid TrainerId { get; set; }
        public DateTime Created { get; set; }
        public bool IsSuspended { get; set; }
        public bool IsDeprecated { get; set; }
        public string SalesPerson { get; set; }
        public Guid SalesPersonId { get; set; }
        public string BackupTrainer { get; set; }
        public Guid BackupTrainerId { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonDisplayName { get; set; }
        public string ContactPersonPhoneFormatted { get; set; }

        public string Date
        {
            get
            {
                return this.Created.ToString("MM/dd/yyyy");
            }
        }
        public string Location
        {
            get
            {
                var location = string.Empty;
                if (City.IsNotNullOrEmpty() && State.IsNotNullOrEmpty())
                {
                    location = string.Format("{0}, {1}", City.Trim(), State.Trim());
                }
                return location;
            }
        }
        public string ActionText
        {
            get
            {
                return this.IsDeprecated ? "Restore" : "Suspend";
            }
        }
        public string ReadOnlyText
        {
            get
            {
                return this.IsFrozen ? "UndoReadOnly" : "ReadOnly";
            }
        }
        public string DateSortable
        {
            get
            {
                return Date.IsNotNullOrEmpty() ? "<div class='float-left'><span class='fr'>" + Date.Split('/')[2] + "</span><span class='fr'>/</span><span class='fr'>" + Date.Split('/')[0] + "/" + Date.Split('/')[1] + "</span></div>" : "";
            }
        }
        public string PopUpContent
        {
            get
            {
                return new StringBuilder()
                .AppendFormat("<strong>Name:</strong> {0}<br />", this.Name)
                .AppendFormat("<strong>Trainer:</strong> {0}<br />", this.Trainer)
                //.AppendFormat("<strong>Sales Person:</strong> {0}<br />", this.SalesPerson)
                .ToString();
            }
        }
        public string AttentionLevel
        {
            get
            {
                var attentionLevel = string.Empty;
                if (this.Created.IsValid())
                {
                    if (this.Created >= DateTime.Now.AddDays(-90))
                    {
                        attentionLevel = "red";
                    }
                    else if (this.Created >= DateTime.Now.AddDays(-180) && this.Created <= DateTime.Now.AddDays(-90))
                    {
                        attentionLevel = "blue";
                    }
                }
                return attentionLevel;
            }
        }
    }
}
