﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.SqlGeneration.Schema;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Enums;

    [Serializable]
    public class CustomerNote : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public Guid LoginId { get; set; }
        public string Comments { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsDeprecated { get; set; }
        public string Recipients { get; set; }
        public int NoteType { get; set; }
        public string Summary { get; set; }
        public Guid CallerId { get; set; }
        public int IssueId { get; set; }
        public Guid AttachmentId { get; set; }
        public bool IsCompleted { get; set; }
        public string DevUpdates { get; set; }
        public string AdditionalInformation { get; set; }
        public Guid ModifiedBy { get; set; }
        public int Status { get; set; }
        public bool Saved { get; set; }

        #endregion

        #region Domain

        [SubSonicIgnore]
        public string AgencyName { get; set; }

        [SubSonicIgnore]
        public string CreatedFormatted
        {
            get {
                return this.Created.ToString("MM/dd/yyy hh:mm tt");
            }
        }
        [SubSonicIgnore]
        public string ModifiedFormatted
        {
            get
            {
                return this.Modified.ToString("MM/dd/yyy hh:mm tt");
            }
        }
        [SubSonicIgnore]
        public string RepName { get; set; }
        [SubSonicIgnore]
        public string SalesPersonName { get; set; }
        [SubSonicIgnore]
        public string TrainerName { get; set; }
        [SubSonicIgnore]
        public string BackupTrainerName { get; set; }
        [SubSonicIgnore]
        public string CallerName { get; set; }
        [SubSonicIgnore]
        public string ContactNumber { get; set; }
        [SubSonicIgnore]
        public string Preview
        {
            get
            {
                if (this.Comments.IsNotNullOrEmpty())
                {
                    return this.Comments.Length >= 75 ? string.Format("{0}...", this.Comments.Substring(0, 74)) : this.Comments;
                }
                return string.Empty;
            }
        }
        [SubSonicIgnore]
        public string NoteTypeName
        {
            get
            {
                if (this.NoteType != 0)
                {
                    return ((CustomerNoteType)this.NoteType).GetDescription();
                }
                return string.Empty;
            }
        }

        [SubSonicIgnore]
        public List<Guid> RecipientArray { get; set; }

        [SubSonicIgnore]
        public bool SendAsMessage { get; set; }

        [SubSonicIgnore]
        public string IssueStatus { get; set; }

        [SubSonicIgnore]
        public bool HasAttachment{ get; set; } 

        [SubSonicIgnore]
        public string Completed
        {
            get
            {
                if (IssueId!=0)
                {
                    return "Yes";
                }
                else
                {
                    return "No";
                }
            }
        }

        [SubSonicIgnore]
        public string PatientName { get; set; }

        [SubSonicIgnore]
        public string EpisodePeriod { get; set; }

        [SubSonicIgnore]
        public string VisitType { get; set; }

        [SubSonicIgnore]
        public string DateofVisit { get; set; }

        [SubSonicIgnore]
        public string ModifiedByName { get; set; }

        [SubSonicIgnore]
        public string PreviousSoftware { get; set; }

        [SubSonicIgnore]
        public string TextStatus
        {
            get
            {
                var result = "NA";
                if (IssueId != 0)
                {
                    if (Enum.IsDefined(typeof(NoteStatus),this.Status))
                    {
                        return ((NoteStatus)this.Status).GetDescription();
                    }
                }
                return result;
            }
        }

        [SubSonicIgnore]
        public string AttachmentName { get; set; }

        [SubSonicIgnore]
        public bool IsEditable { get; set; } 

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Comments), "Note Comments is required."));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Summary), "Note Summary is required."));
            AddValidationRule(new Validation(() => this.NoteType == 0, "Note Type is required."));
        }

        #endregion
    }
}
