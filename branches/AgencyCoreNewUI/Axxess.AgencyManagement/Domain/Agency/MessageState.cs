﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    public class MessageState
    {
        public Guid Id { get; set; }
        public bool IsRead { get; set; }
        public bool IsDeprecated { get; set; }
    }
}
