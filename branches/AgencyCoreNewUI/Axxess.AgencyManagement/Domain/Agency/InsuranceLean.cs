﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Enums;
using Axxess.Core.Extension;

namespace Axxess.AgencyManagement.Domain
{
   public class InsuranceLean
    {
        public int Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Name { get; set; }
        public int PayorType { get; set; }
        public int InvoiceType { get; set; }
        public string PayorId { get; set; }
        public string PhoneNumber { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }

        public bool IsTradtionalMedicare { get; set; }

        public string ContactPerson
        {
            get
            {
                return string.Format("{0} {1}", this.ContactPersonFirstName, this.ContactPersonLastName);
            }
        }
        public string InvoiceTypeName
        {
            get
            {
                return Enum.IsDefined(typeof(InvoiceType),this.InvoiceType)? ((InvoiceType)Enum.ToObject(typeof(InvoiceType), this.InvoiceType)).GetDescription():string.Empty;
            }
        }

        public string PayerTypeName
        {
            get
            {
                return Enum.IsDefined(typeof(PayerTypes), this.PayorType) ? ((PayerTypes)Enum.ToObject(typeof(PayerTypes), this.PayorType)).GetDescription() : string.Empty;
            }
        }

        public string Action
        {
            get { return IsTradtionalMedicare? "<a class=\"link\" onclick=\"Agency.Insurance.VisitRates.Edit();return false\">Edit Visit Rates</a>" : string.Format("<a class=\"link\" onclick=\"Agency.Insurance.Edit('{0}');return false\">Edit</a> | <a class=\"link\" onclick=\"Agency.Insurance.Delete('{0}',$(this).closest('.t-grid'));return false\">Delete</a>", this.Id); }
        }
    }
}
