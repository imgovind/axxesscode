﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using SubSonic.SqlGeneration.Schema;

    public class MessageDetail : EntityBase
    {
        #region Members

        public Guid Id { get; set; }
        public Guid FromId { get; set; }
        public string Body { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public string Subject { get; set; }
        public string FromName { get; set; }
        public Guid AttachmentId { get; set; }
        public DateTime Created { get; set; }
        public string RecipientNames { get; set; }
        public string CarbonCopyNames { get; set; }

        #endregion

        #region Validation Rules

        protected override void AddValidationRules()
        {
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Body), "Body is required. <br />"));
            AddValidationRule(new Validation(() => string.IsNullOrEmpty(this.Subject), "Subject is required. <br />"));
        }

        #endregion
    }
}
