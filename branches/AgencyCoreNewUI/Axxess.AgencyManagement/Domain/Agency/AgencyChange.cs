﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using SubSonic.SqlGeneration.Schema;
    using Axxess.Core.Extension;

    public class AgencyChange
    {
        #region Members

        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
        public string Name { get; set; }
        public string TaxId { get; set; }
        public string TaxIdType { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonPhone { get; set; }
        public int CahpsVendor { get; set; }
        public string NationalProviderNumber { get; set; }
        public string MedicareProviderNumber { get; set; }
        public string MedicaidProviderNumber { get; set; }
        public string HomeHealthAgencyId { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }

        public string LocationName { get; set; }
        public string LocationAddressLine1 { get; set; }
        public string LocationAddressLine2 { get; set; }
        public string LocationAddressCity { get; set; }
        public string LocationAddressStateCode { get; set; }
        public string LocationAddressZipCode { get; set; }
        public string LocationPhoneWork { get; set; }
        public string LocationFaxNumber { get; set; }
        public string FullName { get; set; }
        public DateTime Created { get; set; }
        public bool IsComplete { get; set; }
        public string Comments { get; set; }

        #endregion

        [SubSonicIgnore]
        public string CreatedDateFormatted { get { return this.Created.ToShortDateString().ToZeroFilled(); } }

        [SubSonicIgnore]
        public string Complete { get { return this.IsComplete ? "Yes" : "No"; } }
    }
}
