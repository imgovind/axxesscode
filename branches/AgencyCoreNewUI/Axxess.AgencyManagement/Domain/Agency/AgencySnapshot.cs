﻿namespace Axxess.AgencyManagement.Domain
{
    using System;
    using System.Text;

    using Axxess.Core.Extension;

    public class AgencySnapshot
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsSuspended { get; set; }
        public bool IsDeprecated { get; set; }
        public bool IsFrozen { get; set; }
        public DateTime FrozenDate { get; set; }
        public int ClusterId { get; set; }
       
        public string AccountId { get; set; }
        public Guid Trainer { get; set; }
        public Guid BackupTrainer { get; set; }
        public Guid SalesPerson { get; set; }
        public Guid ImplementationSpecialist { get; set; }
        public DateTime Created { get; set; }

    }

}
