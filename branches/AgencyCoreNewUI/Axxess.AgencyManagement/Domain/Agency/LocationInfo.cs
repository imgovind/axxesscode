﻿namespace Axxess.AgencyManagement.Domain
{
    using System;

    using Axxess.Core.Extension;
    public class LocationInfo
    {
        public string Name { get; set; }
        public string AgencyName { get; set; }
        public Guid Id { get; set; }
        public Guid AgencyId { get; set; }
    }
}
