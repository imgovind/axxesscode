﻿namespace Axxess.AgencyManagement
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Threading;
    using System.Collections;
    using System.Collections.Generic;

    using Axxess.Api;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Domain;
    using Repositories;

    public static class PhysicianEngine
    {
        #region Private Members

        private static readonly CacheAgent cacheAgent = new CacheAgent();
        private static IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();

        #endregion

        #region Public Methods

        //public static AgencyPhysician Get(Guid physicianId, Guid agencyId)
        //{
        //    AgencyPhysician physician = null;
        //    //var physicianInfo = cacheAgent.GetPhysicianXml(physicianId, agencyId);
        //    //if (physicianInfo.IsNotNullOrEmpty())
        //    //{
        //    //    physician = physicianInfo.ToObject<AgencyPhysician>();
        //    //}
        //    //else
        //    //{
        //        var agencySnapShot = dataProvider.AgencyRepository.GetAgencySnapshot(agencyId);
        //        physician = dataProvider.PhysicianRepository.Get(physicianId, agencyId, agencySnapShot.ClusterId);
        //    //}
        //    return physician;
        //}

        //public static string GetName(Guid physicianId, Guid agencyId)
        //{
        //    var name = string.Empty;
        //    var physician = Get(physicianId, agencyId);
        //    if (physician != null)
        //    {
        //        name = physician.DisplayName;
        //    }
        //    return name;
        //}

        //public static void Refresh(Guid agencyId)
        //{
        //    //cacheAgent.RefreshPhysicians(agencyId);
        //}

        //public static IList<AgencyPhysician> AsList(Guid agencyId)
        //{
        //    IList<AgencyPhysician> physicianList = new List<AgencyPhysician>();

        //    //var physicianInfoList = cacheAgent.GetPhysicians(agencyId);
        //    //if (physicianInfoList == null || physicianInfoList.Count == 0)
        //    //{
        //        var agencySnapShot = dataProvider.AgencyRepository.GetAgencySnapshot(agencyId);
        //        physicianList = dataProvider.PhysicianRepository.GetAgencyPhysicians(agencyId,agencySnapShot.ClusterId);
        //    //}
        //    //else
        //    //{
        //    //    physicianInfoList.ForEach(p =>
        //    //    {
        //    //        if (p.IsNotNullOrEmpty())
        //    //        {
        //    //            physicianList.Add(p.ToObject<AgencyPhysician>());
        //    //        }
        //    //    });
        //    //}

        //    return physicianList;
        //}

        #endregion
    }
}
