﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum RedMineProjects
    {
        [Description("AgencyCoreV4")]
        AgencyCoreV4,
        [Description("AgencyCoreV5")]
        AgencyCoreV5,
        [Description("Axxessconsult.com")]
        Axxessconsult,
        [Description("Axxessweb.com")]
        Axxessweb,
        [Description("PhysicianPortalV4")]
        PhysicianPortalV4,
        [Description("SupportAppV5")]
        SupportAppV5,
        [Description("Test Project")]
        TestProject,
        [Description("iOS APP")]
        IOSApp,
        [Description("Android App")]
        AndroidApp
    }
}