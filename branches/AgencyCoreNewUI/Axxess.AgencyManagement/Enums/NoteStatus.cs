﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Axxess.AgencyManagement.Enums
{
   public enum NoteStatus
    {
       [Description("New")]
       New=0,
        [Description("In Progress")]
       InProgress=10,
        [Description("Feedback")]
       Feedback=40,
        [Description("Resolved")]
       Resolved=80,
        [Description("Rejected")]
       Rejected=90,
         [Description("Closed")]
       Closed=100
       //,
       //  Closed = 255
    }
}
