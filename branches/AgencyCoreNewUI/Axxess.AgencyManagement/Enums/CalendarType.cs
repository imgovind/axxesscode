﻿namespace Axxess.AgencyManagement.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public enum CalendarType
    {
        Daily = 1,
        Weekly = 2,
        Monthly = 3
    }
}
