﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum Nursing
    {
        [Description("Oasis-C Start Of Care")]
        soc,
        [Description("Oasis-C Resumption Of Care")]
        Roc,
        [Description("Oasis-C Discharge From Agency")]
        DischargeFromAgency,
        [Description("Oasis-C Recertification")]
        Recertification,
        [Description("Oasis-C Other Follow-up")]
        OtherFollowUp,
        [Description("Oasis-C Death at Home")]
        DeathAtHome,
        [Description("Oasis-C Transfer to inpatient facility - Not Discharged")]
        TransferNotDischarged,
        [Description("Oasis-C Transfer to inpatient facility - Discharged")]
        TransferDischarged,
        [Description("Discharge Summary")]
        DischargeSummary,
        [Description("Skilled Nurse Visit")]
        SkilledNurseVisit,
        [Description("60 Day Summary")]
        SixtyDaySummary,
        [Description("LVN Supervisory Visit")]
        LVNSupervisoryVisit,
    }
}
