﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.Enums
{
    [Flags]
    public enum AgencyServices : int
    {
        HomeHealth = 1,
        PrivateDuty = 2,
        Therapy = 4
    }
}
