﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum DynamicBalance : byte
    {
        [Description("N = Maintain dynamic sitting/standing balance through maximum excursions of active trunk movement.")]
        N = 1,
        [Description("G = Maintain dynamic sitting/standing balance through moderate excursions of active trunk movement.")]
        G = 2,
        [Description("F+ = Maintain dynamic sitting/standing balance through minimal excursions of active trunk movement.")]
        FPlus = 3,
        [Description("F = Maintain dynamic sitting/standing balance through minimal excursions of active trunk movement with supervision.")]
        F = 4,
        [Description("F- = Maintain dynamic sitting/standing balance through minimal excursions of active trunk movement with CG assist. ")]
        FMinus = 5,
        [Description("P+ = Maintain dynamic sitting/standing balance through minimal excursions of active trunk movement with minimal assist.")]
        PPlus = 6,
        [Description("P = Maintain dynamic sitting/standing balance through minimal excursions of active trunk movement with moderate assist.")]
        P = 7,
        [Description("P- = Maintain dynamic sitting/standing balance through minimal excursions of active trunk movement with maximum assist.")]
        PMinus = 8,
        [Description("U = Unable to attempt.")]
        U = 9
    }
}
