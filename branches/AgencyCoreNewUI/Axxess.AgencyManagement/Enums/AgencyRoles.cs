﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;

    public enum AgencyRoles : byte
    {
        [Description("Administrator")]
        Administrator = 1,
        [Description("Director of Nursing")]
        DoN = 2,
        [Description("Case Manager")]
        CaseManager = 3,
        [Description("Nurse")]
        Nurse = 4,
        [Description("Clerk")]
        Clerk = 5,
        [Description("Physical Therapist")]
        PhysicalTherapist = 6,
        [Description("Occupational Therapist")]
        OccupationalTherapist = 7,
        [Description("Speech Therapist")]
        SpeechTherapist = 8,
        [Description("Medical Social Worker")]
        MedicalSocialWorker = 9,
        [Description("Home Health Aide")]
        HHA = 10,
        [Description("Scheduler")]
        Scheduler = 11,
        [Description("Biller")]
        Biller = 12,
        [Description("QA")]
        QA = 13,
        [Description("Physician")]
        Physician = 14,
        [Description("Office Manager")]
        OfficeManager = 15,
        [Description("Community Liason Officer")]
        CommunityLiasonOfficer = 16,
        [Description("External Referral Source")]
        ExternalReferralSource = 17,
        [Description("Driver And Transportation")]
        DriverAndTransportation = 18,
        [Description("OfficeStaff")]
        OfficeStaff = 19,
        [Description("Auditor")]
        Auditor = 20
    }
}
