﻿namespace Axxess.AgencyManagement.Enums
{
    using System.ComponentModel;
    public enum CustomerNoteType
    {
        [Description("Bug")]
        Bug = 1,
        [Description("Feature")]
        Feature = 2,
        [Description("Enhancement")]
        Enhancement = 3,
        [Description("Online Training")]
        OnlineTraining = 4,
        [Description("Onsite Visit")]
        OnsiteVisit = 5,
        [Description("Recalled")]
        Recalled = 6,
        [Description("Complaint")]
        Complaint = 7,
        [Description("Data Load")]
        DataLoad = 8,
        [Description("Print Discrepancy")]
        PrintDiscrepancy = 9,
        [Description("Remittance Advice")]
        RemitAdvice = 10,
        [Description("Implementation Call")]
        ImplementationCall = 11,
        [Description("Tablet")]
        Tablet = 12,
        [Description("Mobile Application(Apple)")]
        AppleMobileApp = 13,
        [Description("Mobile Application(Android)")]
        AndroidMobileApp = 14,
        [Description("Private Duty")]
        PrivateDuty =15



    }
}