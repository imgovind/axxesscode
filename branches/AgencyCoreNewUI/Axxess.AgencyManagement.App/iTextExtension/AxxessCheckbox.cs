﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    class AxxessCheckbox : Paragraph {
        public static Font unicode = new Font(BaseFont.CreateFont("C:\\Windows\\Fonts\\arialuni.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, true, null, null));
        public AxxessCheckbox(String label, bool check) : base(String.Format("{0} {1}", check ? "\u2612" : "\u2610", label), AxxessCheckbox.unicode) {
            AxxessCheckbox.unicode.Size = 10F;
            AxxessCheckbox.unicode.BaseFont.CompressionLevel = 9;
        }
        public AxxessCheckbox(String label, bool check, Font font) : base(String.Format("{0} {1}", check ? "\u2612" : "\u2610", label), AxxessCheckbox.unicode) {
            AxxessCheckbox.unicode.Size = font.Size;
            AxxessCheckbox.unicode.BaseFont.CompressionLevel = 9;
        }
        public AxxessCheckbox(String label, bool check, float fontSize) : base(String.Format("{0} {1}", check ? "\u2612" : "\u2610", label), AxxessCheckbox.unicode) {
            AxxessCheckbox.unicode.Size = fontSize;
            AxxessCheckbox.unicode.BaseFont.CompressionLevel = 9;
        }
        public AxxessCheckbox(bool check, float fontSize) : base(String.Format("{0}", check ? "\u2612" : "\u2610"), AxxessCheckbox.unicode) {
            this.SetAlignment("Center");
            AxxessCheckbox.unicode.Size = fontSize;
            AxxessCheckbox.unicode.BaseFont.CompressionLevel = 9;
        }
    }
}