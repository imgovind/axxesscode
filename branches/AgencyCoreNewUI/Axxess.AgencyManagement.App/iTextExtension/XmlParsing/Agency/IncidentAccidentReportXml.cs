﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using System.ComponentModel;
    using System.Reflection;
    class IncidentAccidentReportXml : BaseXml {
        private Incident Data = new Incident();
        public IncidentAccidentReportXml(Incident data) : base(PdfDocs.IncidentAccidentReport)
        {
            this.Data = data;
            this.Init();
            this.NotaFilter();
            if (this.Layout.Count == 0) this.Layout.Add(new XmlPrintSection(this, null));
        }
        public override String GetData(String Index) {
            foreach (PropertyDescriptor p in TypeDescriptor.GetProperties(this.Data)) if (p.Name == Index) return p.GetValue(this.Data) != null ? p.GetValue(this.Data).ToString() : string.Empty;
            return string.Empty;
        }
    }
}