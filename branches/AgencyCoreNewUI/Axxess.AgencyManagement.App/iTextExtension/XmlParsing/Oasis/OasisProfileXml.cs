﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.OasisC.Domain;
    using Axxess.Core.Extension;
    using Axxess.OasisC.Extensions;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.Services;
    using Axxess.LookUp.Repositories;
    using Axxess.LookUp.Domain;
    using Axxess.Core.Infrastructure;
    class OasisProfileXml : BaseXml {
        private readonly IBillingService billingService = Container.Resolve<IBillingService>();
        private readonly IAssessmentService assessmentService = Container.Resolve<IAssessmentService>();
        private readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;
        private readonly IPatientRepository patientRepository = Container.Resolve<IAgencyManagementDataProvider>().PatientRepository;
        private readonly IPhysicianRepository physicianRepository = Container.Resolve<IAgencyManagementDataProvider>().PhysicianRepository;
        private readonly ILookupRepository lookupRepository = Container.Resolve<ILookUpDataProvider>().LookUpRepository;
        private IDictionary<String, Question> Data = null;
        public OasisProfileXml(Assessment data)
            : base(PdfDocs.OasisProfile)
        {
            this.Data = data.ToDictionary();
            var patient = patientRepository.GetPatientOnly(data.PatientId, Current.AgencyId) ?? new Patient();
            var location = agencyRepository.FindLocation(Current.AgencyId, patient.AgencyLocationId) ?? new AgencyLocation();
            var episodeRange = assessmentService.GetPlanofCareCertPeriod(data.EpisodeId, data.PatientId, data.Id);
            var prospectivePayment = lookupRepository.GetProspectivePayment(data.HippsCode, episodeRange.StartDate, patient.AddressZipCode.IsNotNullOrEmpty() ? patient.AddressZipCode : location.AddressZipCode);
            if (!this.Data.ContainsKey("M0100AssessmentType")) this.Data.Add(new KeyValuePair<string, Question>("M0100AssessmentType", new Question()));
            this.Data["M0100AssessmentType"].Answer = data.TypeDescription;
            if (!this.Data.ContainsKey("EpisodeStartDate")) this.Data.Add(new KeyValuePair<string, Question>("EpisodeStartDate", new Question()));
            this.Data["EpisodeStartDate"].Answer = episodeRange.StartDate.ToShortDateString();
            if (!this.Data.ContainsKey("EpisodeEndDate")) this.Data.Add(new KeyValuePair<string, Question>("EpisodeEndDate", new Question()));
            this.Data["EpisodeEndDate"].Answer = episodeRange.EndDate.ToShortDateString();
            if (!this.Data.ContainsKey("CBSACode")) this.Data.Add(new KeyValuePair<string, Question>("CBSACode", new Question()));
            this.Data["CBSACode"].Answer = prospectivePayment.CbsaCode;
            if (!this.Data.ContainsKey("HHRG")) this.Data.Add(new KeyValuePair<string, Question>("HHRG", new Question()));
            this.Data["HHRG"].Answer = prospectivePayment.Hhrg;
            if (!this.Data.ContainsKey("Weight")) this.Data.Add(new KeyValuePair<string, Question>("Weight", new Question()));
            this.Data["Weight"].Answer = prospectivePayment.Weight;
            if (!this.Data.ContainsKey("WageIndex")) this.Data.Add(new KeyValuePair<string, Question>("WageIndex", new Question()));
            this.Data["WageIndex"].Answer = prospectivePayment.WageIndex;
            if (!this.Data.ContainsKey("LaborPortion")) this.Data.Add(new KeyValuePair<string, Question>("LaborPortion", new Question()));
            this.Data["LaborPortion"].Answer = prospectivePayment.LaborAmount;
            if (!this.Data.ContainsKey("NonLabor")) this.Data.Add(new KeyValuePair<string, Question>("NonLabor", new Question()));
            this.Data["NonLabor"].Answer = prospectivePayment.NonLaborAmount;
            if (!this.Data.ContainsKey("NonRoutineSuppliesAmount")) this.Data.Add(new KeyValuePair<string, Question>("NonRoutineSuppliesAmount", new Question()));
            this.Data["NonRoutineSuppliesAmount"].Answer = prospectivePayment.NonRoutineSuppliesAmount;
            if (!this.Data.ContainsKey("TotalPayment")) this.Data.Add(new KeyValuePair<string, Question>("TotalPayment", new Question()));
            this.Data["TotalPayment"].Answer = prospectivePayment.TotalProspectiveAmount;
            if (!this.Data.ContainsKey("HIPPS")) this.Data.Add(new KeyValuePair<string, Question>("HIPPS", new Question()));
            this.Data["HIPPS"].Answer = data.HippsCode;
            if (!this.Data.ContainsKey("OASISMatchingKey")) this.Data.Add(new KeyValuePair<string, Question>("OASISMatchingKey", new Question()));
            this.Data["OASISMatchingKey"].Answer = data.ClaimKey;
            this.Init();
            this.FilterEmptyDiagnoses();
        }
        public override String GetData(String Index) {
            return this.Data != null && this.Data.ContainsKey(Index) && this.Data[Index].Answer.IsNotNullOrEmpty() ? this.Data[Index].Answer : string.Empty;
        }
        private double WageIndex(CBSACode cbsaCode, DateTime time) {
            double wageIndex = 0;
            if (time.Year == 2012) wageIndex = cbsaCode.WITwoTwelve;
            else if (time.Year == 2011) wageIndex = cbsaCode.WITwoEleven;
            else if (time.Year == 2010) wageIndex = cbsaCode.WITwoTen;
            else if (time.Year == 2009) wageIndex = cbsaCode.WITwoNine;
            else if (time.Year == 2008) wageIndex = cbsaCode.WITwoEight;
            else if (time.Year == 2007) wageIndex = cbsaCode.WITwoSeven;
            return wageIndex;
        }
        private void FilterEmptyDiagnoses() {
            for (int sectionI = 0; sectionI < this.Layout.Count(); sectionI++)
                for (int questionI = 0; questionI < this.Layout[sectionI].Question.Count(); questionI++)
                    for (int subQuestionI = 0; subQuestionI < this.Layout[sectionI].Question[questionI].Subquestion.Count(); subQuestionI++)
                        if (this.Layout[sectionI].Question[questionI].Subquestion[subQuestionI].Data.Trim().Equals("()")) this.Layout[sectionI].Question[questionI].Subquestion.RemoveRange(--subQuestionI, 2);
                        else if (this.Layout[sectionI].Question[questionI].Subquestion[subQuestionI].Type.Equals("text") && this.Layout[sectionI].Question[questionI].Subquestion[subQuestionI].Data.IsNullOrEmpty()) this.Layout[sectionI].Question[questionI].Subquestion[subQuestionI].Data = " ";
        }
    }
}