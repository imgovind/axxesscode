﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    class ReferralXml : BaseXml {
        private Dictionary<String, NotesQuestion> Data = new Dictionary<String, NotesQuestion>();
        public ReferralXml(Referral data) : base(PdfDocs.Referral) {
            this.Data.Add("ReferrerPhysician", new NotesQuestion());
            this.Data["ReferrerPhysician"].Answer = data.ReferrerPhysician.ToString();
            this.Data.Add("AdmissionSource", new NotesQuestion());
            this.Data["AdmissionSource"].Answer = data.AdmissionSource.ToString();
            this.Data.Add("OtherReferralSource", new NotesQuestion());
            this.Data["OtherReferralSource"].Answer = data.OtherReferralSource;
            this.Data.Add("ReferralDate", new NotesQuestion());
            this.Data["ReferralDate"].Answer = data.ReferralDate.ToShortDateString();
            this.Data.Add("InternalReferral", new NotesQuestion());
            this.Data["InternalReferral"].Answer = data.InternalReferral.ToString();
            this.Data.Add("FirstName", new NotesQuestion());
            this.Data["FirstName"].Answer = data.FirstName;
            this.Data.Add("MiddleInitial", new NotesQuestion());
            this.Data["MiddleInitial"].Answer = data.MiddleInitial;
            this.Data.Add("LastName", new NotesQuestion());
            this.Data["LastName"].Answer = data.LastName;
            this.Data.Add("Gender", new NotesQuestion());
            this.Data["Gender"].Answer = data.Gender;
            this.Data.Add("DOB", new NotesQuestion());
            this.Data["DOB"].Answer = data.DOB.ToShortDateString();
            this.Data.Add("MaritalStatus", new NotesQuestion());
            this.Data["MaritalStatus"].Answer = data.MaritalStatus;
            this.Data.Add("Height", new NotesQuestion());
            this.Data["Height"].Answer = data.Height.ToString();
            this.Data.Add("HeightMetric", new NotesQuestion());
            this.Data["HeightMetric"].Answer = data.HeightMetric.ToString();
            this.Data.Add("Weight", new NotesQuestion());
            this.Data["Weight"].Answer = data.Weight.ToString();
            this.Data.Add("WeightMetric", new NotesQuestion());
            this.Data["WeightMetric"].Answer = data.WeightMetric.ToString();
            this.Data.Add("UserId", new NotesQuestion());
            this.Data["UserId"].Answer = data.UserId.ToString();
            this.Data.Add("MedicareNumber", new NotesQuestion());
            this.Data["MedicareNumber"].Answer = data.MedicareNumber;
            this.Data.Add("MedicaidNumber", new NotesQuestion());
            this.Data["MedicaidNumber"].Answer = data.MedicaidNumber;
            this.Data.Add("SSN", new NotesQuestion());
            this.Data["SSN"].Answer = data.SSN;
            this.Data.Add("AddressLine1", new NotesQuestion());
            this.Data["AddressLine1"].Answer = data.AddressLine1;
            this.Data.Add("AddressLine2", new NotesQuestion());
            this.Data["AddressLine2"].Answer = data.AddressLine2;
            this.Data.Add("AddressCity", new NotesQuestion());
            this.Data["AddressCity"].Answer = data.AddressCity;
            this.Data.Add("AddressStateCode", new NotesQuestion());
            this.Data["AddressStateCode"].Answer = data.AddressStateCode;
            this.Data.Add("AddressZipCode", new NotesQuestion());
            this.Data["AddressZipCode"].Answer = data.AddressZipCode;
            this.Data.Add("PhoneHome", new NotesQuestion());
            this.Data["PhoneHome"].Answer = data.PhoneHome.Substring(0,3) + "-" + data.PhoneHome.Substring(3,3) + "-" + data.PhoneHome.Substring(6,4);
            this.Data.Add("EmailAddress", new NotesQuestion());
            this.Data["EmailAddress"].Answer = data.EmailAddress;
            this.Data.Add("ServicesRequired", new NotesQuestion());
            this.Data["ServicesRequired"].Answer = data.ServicesRequired;
            this.Data.Add("DME", new NotesQuestion());
            this.Data["DME"].Answer = data.DME;
            this.Data.Add("Comments", new NotesQuestion());
            this.Data["Comments"].Answer = data.Comments;
            this.Init();
            if (this.Layout.Count == 0) this.Layout.Add(new XmlPrintSection(this, null));
        }
        public override String GetData(String Index) {
            return this.Data != null && this.Data.ContainsKey(Index) && this.Data[Index].Answer.IsNotNullOrEmpty() ? this.Data[Index].Answer : string.Empty;
        }
    }
}