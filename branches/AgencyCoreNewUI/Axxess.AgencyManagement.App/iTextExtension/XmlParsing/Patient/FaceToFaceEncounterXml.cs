﻿namespace Axxess.AgencyManagement.App.iTextExtension.XmlParsing {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    class FaceToFaceEncounterXml : BaseXml {
        private Dictionary<String, NotesQuestion> Data = new Dictionary<String, NotesQuestion>();
        public FaceToFaceEncounterXml(FaceToFaceEncounter data) : base(PdfDocs.FaceToFaceEncounter)
        {
            this.Data.Add("Certification", new NotesQuestion());
            this.Data.Add("EncounterDate", new NotesQuestion());
            this.Data.Add("MedicalReason", new NotesQuestion());
            this.Data.Add("ClinicalFinding", new NotesQuestion());
            this.Data.Add("Services", new NotesQuestion());
            this.Data.Add("ServicesOther", new NotesQuestion());
            this.Data["Certification"].Answer = data.Certification;
            this.Data["EncounterDate"].Answer = data.EncounterDate.IsValid() ? data.EncounterDate.ToShortDateString() : string.Empty;
            this.Data["MedicalReason"].Answer = data.MedicalReason;
            this.Data["ClinicalFinding"].Answer = data.ClinicalFinding;
            this.Data["Services"].Answer = data.Services;
            this.Data["ServicesOther"].Answer = data.ServicesOther;
            this.Init();
            if (this.Layout.Count == 0) this.Layout.Add(new XmlPrintSection(this, null));
        }
        public override String GetData(String Index) {
            return this.Data != null && this.Data.ContainsKey(Index) && this.Data[Index].Answer.IsNotNullOrEmpty() ? this.Data[Index].Answer : string.Empty;
        }
    }
}