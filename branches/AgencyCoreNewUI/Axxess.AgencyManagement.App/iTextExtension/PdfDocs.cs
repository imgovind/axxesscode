﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    public class PdfDocs {
        public static PdfDoc IncidentAccidentReport = new PdfDoc(AppSettings.IncidentAccidentReportPdf, AppSettings.IncidentAccidentReportXml);
        public static PdfDoc InfectionReport = new PdfDoc(AppSettings.InfectionReportPdf, AppSettings.InfectionReportXml);
        public static PdfDoc BillingClaims = new PdfDoc(AppSettings.BillingClaimsPdf);
        public static PdfDoc BillingFinal = new PdfDoc(AppSettings.BillingFinalPdf);
        public static PdfDoc BillingRap = new PdfDoc(AppSettings.BillingRapPdf);
        public static PdfDoc HCFA1500 = new PdfDoc(AppSettings.HCFA1500Pdf);
        public static PdfDoc Remittance = new PdfDoc(AppSettings.RemittancePdf);
        public static PdfDoc Remittances = new PdfDoc(AppSettings.RemittancesPdf);
        public static PdfDoc UB04 = new PdfDoc(AppSettings.UB04Pdf);
        public static PdfDoc Message = new PdfDoc(AppSettings.MessagePdf);
        public static PdfDoc BillingSupplies = new PdfDoc(AppSettings.BillingSuppliesPdf);
        public static PdfDoc HCFA485 = new PdfDoc(AppSettings.HCFA485Pdf);
        public static PdfDoc HCFA487 = new PdfDoc(AppSettings.HCFA487Pdf);
        public static PdfDoc Oasis = new PdfDoc(AppSettings.OasisPdf, AppSettings.OasisXml);
        public static PdfDoc OasisAudit = new PdfDoc(AppSettings.OasisAuditPdf);
        public static PdfDoc OasisProfile = new PdfDoc(AppSettings.OasisProfilePdf, AppSettings.OasisProfileXml);
        public static PdfDoc AllergyProfile = new PdfDoc(AppSettings.AllergyProfilePdf);
        public static PdfDoc Authorization = new PdfDoc(AppSettings.AuthorizationPdf);
        public static PdfDoc CommunicationNote = new PdfDoc(AppSettings.CommunicationNotePdf);
        public static PdfDoc DrugDrugInteraction = new PdfDoc(AppSettings.DrugDrugInteractionPdf);
        public static PdfDoc FaceToFaceEncounter = new PdfDoc(AppSettings.FaceToFaceEncounterPdf, AppSettings.FaceToFaceEncounterXml);
        public static PdfDoc HospitalizationLog = new PdfDoc(AppSettings.HospitalizationLogPdf, AppSettings.HospitalizationLogXml);
        public static PdfDoc MedicareEligibilityReport = new PdfDoc(AppSettings.MedicareEligibilityReportPdf);
        public static PdfDoc MedicationProfile = new PdfDoc(AppSettings.MedicationProfilePdf);
        public static PdfDoc PatientProfile = new PdfDoc(AppSettings.PatientProfilePdf);
        public static PdfDoc PhysicianOrder = new PdfDoc(AppSettings.PhysicianOrderPdf);
        public static PdfDoc Referral = new PdfDoc(AppSettings.ReferralPdf, AppSettings.ReferralXml);
        public static PdfDoc TriageClassification = new PdfDoc(AppSettings.TriageClassificationPdf);
        public static PdfDoc PayrollDetail = new PdfDoc(AppSettings.PayrollDetailPdf);
        public static PdfDoc PayrollSummary = new PdfDoc(AppSettings.PayrollSummaryPdf);
        public static PdfDoc AnnualCalendar = new PdfDoc(AppSettings.AnnualCalendarPdf);
        public static PdfDoc MasterCalendar = new PdfDoc(AppSettings.MasterCalendarPdf);
        public static PdfDoc MonthlyCalendar = new PdfDoc(AppSettings.MonthlyCalendarPdf);
        public static PdfDoc HHAideCarePlan = new PdfDoc(AppSettings.HHAideCarePlanPdf, AppSettings.HHAideCarePlanXml);
        public static PdfDoc HHAideSupervisoryVisit = new PdfDoc(AppSettings.HHAideSupervisoryVisitPdf, AppSettings.HHAideSupervisoryVisitXml);
        public static PdfDoc HHAideVisit = new PdfDoc(AppSettings.HHAideVisitPdf, AppSettings.HHAideVisitXml);
        public static PdfDoc MissedVisit = new PdfDoc(AppSettings.MissedVisitPdf);
        public static PdfDoc DriverOrTransportationNote = new PdfDoc(AppSettings.DriverOrTransportationNotePdf, AppSettings.DriverOrTransportationNoteXml);
        public static PdfDoc MSWEvaluationAssessment = new PdfDoc(AppSettings.MSWEvaluationAssessmentPdf, AppSettings.MSWEvaluationAssessmentXml);
        public static PdfDoc MSWProgressNote = new PdfDoc(AppSettings.MSWProgressNotePdf, AppSettings.MSWProgressNoteXml);
        public static PdfDoc MSWVisit = new PdfDoc(AppSettings.MSWVisitPdf, AppSettings.MSWVisitXml);
        public static PdfDoc DischargeSummary = new PdfDoc(AppSettings.DischargeSummaryPdf, AppSettings.DischargeSummaryXml);
        public static PdfDoc LVNSupervisoryVisit = new PdfDoc(AppSettings.LVNSupervisoryVisitPdf, AppSettings.LVNSupervisoryVisitXml);
        public static PdfDoc SixtyDaySummary = new PdfDoc(AppSettings.SixtyDaySummaryPdf, AppSettings.SixtyDaySummaryXml);
        public static PdfDoc SkilledNurseVisit = new PdfDoc(AppSettings.SkilledNurseVisitPdf, AppSettings.SkilledNurseVisitXml);
        public static PdfDoc SNDiabeticDailyVisit = new PdfDoc(AppSettings.SNDiabeticDailyVisitPdf, AppSettings.SNDiabeticDailyVisitXml);
        public static PdfDoc TransferSummary = new PdfDoc(AppSettings.TransferSummaryPdf, AppSettings.TransferSummaryXml);
        public static PdfDoc WoundCare = new PdfDoc(AppSettings.WoundCarePdf, AppSettings.WoundCareXml);
        public static PdfDoc PASCarePlan = new PdfDoc(AppSettings.PASCarePlanPdf, AppSettings.PASCarePlanXml);
        public static PdfDoc PASVisit = new PdfDoc(AppSettings.PASVisitPdf, AppSettings.PASVisitXml);
        public static PdfDoc OTEvaluation = new PdfDoc(AppSettings.OTEvaluationPdf, AppSettings.OTEvaluationXml);
        public static PdfDoc OTVisit = new PdfDoc(AppSettings.OTVisitPdf, AppSettings.OTVisitXml);
        public static PdfDoc PTDischarge = new PdfDoc(AppSettings.PTDischargePdf, AppSettings.PTDischargeXml);
        public static PdfDoc PTEvaluation = new PdfDoc(AppSettings.PTEvaluationPdf, AppSettings.PTEvaluationXml);
        public static PdfDoc PTVisit = new PdfDoc(AppSettings.PTVisitPdf, AppSettings.PTVisitXml);
        public static PdfDoc STEvaluation = new PdfDoc(AppSettings.STEvaluationPdf, AppSettings.STEvaluationXml);
        public static PdfDoc STVisit = new PdfDoc(AppSettings.STVisitPdf, AppSettings.STVisitXml);
        public static PdfDoc UAPInsulinPrepAdminVisit = new PdfDoc(AppSettings.UAPInsulinPrepAdminVisitPdf, AppSettings.UAPInsulinPrepAdminVisitXml);
        public static PdfDoc UAPWoundCareVisit = new PdfDoc(AppSettings.UAPWoundCareVisitPdf, AppSettings.UAPWoundCareVisitXml);

    }
}
