﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using Axxess.Membership.Logging;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    abstract public class AxxessPdf {
        public bool IsOasis = false;
        public static Font sans = new Font(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED, true, null, null));
        public static Font sansbold = new Font(BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED, true, null, null));
        private MemoryStream stream;
        private List<Dictionary<String, String>> fieldmap;
        private IElement[] content;
        private List<Font> font;
        private Rectangle pagesize;
        private float[] margins;
        private PdfDoc type;
        public AxxessPdf() {
            AxxessPdf.sans.BaseFont.CompressionLevel = 9;
            AxxessPdf.sansbold.BaseFont.CompressionLevel = 9;
        }
        public MemoryStream GetSwf()
        {
            try {
                this.GeneratePdf(null, true, true);
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return null;
            }
            return this.stream;
        }
        public MemoryStream GetStream()
        {
            try
            {
                this.GeneratePdf(null, true, false);
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return null;
            }
            return this.stream;
        }
        public MemoryStream GetStream(bool PageNumbering)
        {
            try
            {
                this.GeneratePdf(null, PageNumbering, false);
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return null;
            }
            return this.stream;
        }
        public MemoryStream GetStream(AxxessPdf append)
        {
            try
            {
                this.GeneratePdf(append, true, false);
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return null;
            }
            return this.stream;
        }
        protected void SetContent(IElement[] c) { this.content = c; }
        protected void SetFields(List<Dictionary<String, String>> f) { this.fieldmap = f; }
        protected List<Font> GetFonts() { return this.font; }
        protected void SetFonts(List<Font> f) { this.font = f; }
        protected void SetMargins(float[] m) { this.margins = m; }
        protected void SetPageSize(Rectangle p) { this.pagesize = p; }
        protected void SetType(PdfDoc t) { this.type = t; }
        private void GeneratePdf(AxxessPdf append, bool PageNumbering, bool convertToSwf)
        {
            if (this.pagesize == null) this.pagesize = PageSize.LETTER;
            PdfContentByte swap;
            PdfImportedPage swapPage;
            AxxessReader swapReader;
            MemoryStream swapMem = new MemoryStream();
            AxxessDoc swapDoc = new AxxessDoc(this.pagesize, this.margins);
            PdfWriter swapWriter = PdfWriter.GetInstance(swapDoc, swapMem);
            List<AxxessReader> readerList = new List<AxxessReader>();
            List<PdfStamper> writerList = new List<PdfStamper>();
            List<AcroFields> fieldsList = new List<AcroFields>();
            List<MemoryStream> streamList = new List<MemoryStream>();
            swapDoc.Open();
            swapDoc.PopulateContent(this.content);
            swapDoc.Close();
            swapReader = new AxxessReader(swapMem);
            for (int i = 0; i < swapReader.NumberOfPages; i++)
            {
                streamList.Add(new MemoryStream());
                readerList.Add(new AxxessReader(this.type));
                writerList.Add(new PdfStamper(readerList[i], streamList[i]));
                fieldsList.Add(writerList[i].AcroFields);
                for (int j = 0; j < this.fieldmap.Count(); j++) foreach (KeyValuePair<String, String> field in fieldmap[j])
                    {
                        if (this.font[j] != null) fieldsList[i].SetFieldProperty(field.Key, "textfont", this.font[j].BaseFont, null);
                        fieldsList[i].SetField(field.Key, field.Value);
                    }
                swap = writerList[i].GetOverContent(1);
                swapPage = writerList[i].Writer.GetImportedPage(swapReader, i + 1);
                swap.AddTemplate(swapPage, 0, 0);
                writerList[i].FormFlattening = true;
                writerList[i].Close();
            }
            if (append != null) streamList.Add(append.GetStream(false));
            int numPages = 0;
            int pageNum = 1;
            foreach (MemoryStream stream in streamList)
            {
                swapReader = new AxxessReader(stream);
                numPages += swapReader.NumberOfPages;
            }
            swapDoc = new AxxessDoc(this.pagesize);
            this.stream = new MemoryStream();
            swapWriter = PdfWriter.GetInstance(swapDoc, this.stream);
            swapDoc.Open();
            swap = new PdfContentByte(swapWriter);
            swap = swapWriter.DirectContent;
            for (int i = 0; i < streamList.Count; i++)
            {
                swapReader = new AxxessReader(streamList[i]);
                for (int j = 0; j < swapReader.NumberOfPages; j++)
                {
                    swapDoc.NewPage();
                    swapPage = swapWriter.GetImportedPage(swapReader, j + 1);
                    swap.AddTemplate(swapPage, 0, 0);
                    if (PageNumbering) this.AddPageNumber(swap, pageNum, numPages);
                    this.AddWatermark(swap);
                    pageNum++;
                }
            }
            swapWriter.SetFullCompression();
            swapWriter.CloseStream = false;
            swapDoc.Close();
        }
        protected virtual void AddPageNumber(PdfContentByte swap, int pageNum, int numPages) {
            swap.BeginText();
            swap.SetFontAndSize(this.font[0].BaseFont, 7.5F);
            swap.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Page " + pageNum + " of " + numPages, this.pagesize.Width - 32, 18, 0);
            swap.EndText();
        }
        protected virtual void AddWatermark(PdfContentByte swap) { }
        private string SavePdf() {
            AxxessReader swapReader = new AxxessReader(this.stream);
            String PdfPath = System.IO.Path.GetTempFileName();
            FileStream PdfFileStream = File.OpenWrite(PdfPath);
            this.stream.WriteTo(PdfFileStream);
            PdfFileStream.Flush();
            PdfFileStream.Close();
            return PdfPath;
        }
    }
}