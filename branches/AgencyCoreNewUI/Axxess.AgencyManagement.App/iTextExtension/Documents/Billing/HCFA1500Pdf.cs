﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using Axxess.LookUp.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.App.Services;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.AgencyManagement.App.Enums;
    using Axxess.AgencyManagement.Enums;
    class HCFA1500Pdf : AxxessPdf
    {
        protected readonly IBillingService billingService;
        private HCFA1500ViewData data;

        public HCFA1500Pdf(HCFA1500ViewData data, IBillingService billingService)
        {
            this.data = data;
            this.billingService = billingService;
            this.SetType(PdfDocs.HCFA1500);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            this.SetContent(this.BuildContent(data));
            float[] margins = new float[] { 522, 29, 130, 29 };
            this.SetMargins(margins);
            this.SetFields(this.BuildFieldMap(data));
        }

        protected virtual List<Dictionary<String, String>> BuildFieldMap(HCFA1500ViewData data)
        {
            var fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<string, string>() { });
            if (data.Claim != null && (data.Claim.ClaimType == ClaimType.MAN || data.Claim.ClaimType == ClaimType.HMO))
            {
                fieldmap[0].Add("1-group", "X");
                fieldmap[0].Add("1a", data.Claim != null && data.Claim.HealthPlanId.IsNotNullOrEmpty() ? data.Claim.HealthPlanId : string.Empty);
            }
            fieldmap[0].Add("2",
                (data.Claim != null && data.Claim.LastName.IsNotNullOrEmpty() ? data.Claim.LastName.ToUpper() + ", " : string.Empty) +
                (data.Claim != null && data.Claim.FirstName.IsNotNullOrEmpty() ? data.Claim.FirstName.ToUpper() : string.Empty));
            fieldmap[0].Add("3-month", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("MM") : string.Empty);
            fieldmap[0].Add("3-day", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("dd") : string.Empty);
            fieldmap[0].Add("3-year", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("yyyy") : string.Empty);
            fieldmap[0].Add("3-male", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("M") ? "X" : string.Empty);
            fieldmap[0].Add("3-female", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("F") ? "X" : string.Empty);
            fieldmap[0].Add("4",
                (data.Claim != null && data.Claim.LastName.IsNotNullOrEmpty() ? data.Claim.LastName.ToUpper() + ", " : string.Empty) +
                (data.Claim != null && data.Claim.FirstName.IsNotNullOrEmpty() ? data.Claim.FirstName.ToUpper() : string.Empty));
            fieldmap[0].Add("5", data.Claim != null && data.Claim.AddressLine1.IsNotNullOrEmpty() ? data.Claim.AddressLine1.ToUpper() + (data.Claim.AddressLine2.IsNotNullOrEmpty() ? " " + data.Claim.AddressLine2.ToUpper() : string.Empty) : string.Empty);
            fieldmap[0].Add("5-city", data.Claim != null && data.Claim.AddressCity.IsNotNullOrEmpty() ? data.Claim.AddressCity.ToUpper() : string.Empty);
            fieldmap[0].Add("5-state", data.Claim != null && data.Claim.AddressStateCode.IsNotNullOrEmpty() ? data.Claim.AddressStateCode.ToUpper() : string.Empty);
            fieldmap[0].Add("5-zip", data.Claim != null && data.Claim.AddressZipCode.IsNotNullOrEmpty() ? data.Claim.AddressZipCode.ToUpper() : string.Empty);
            fieldmap[0].Add("5-areacode", data.PatientTelephoneNum.IsNotNullOrEmpty() && data.PatientTelephoneNum.Length == 10 ? data.PatientTelephoneNum.Substring(0, 3) : string.Empty);
            fieldmap[0].Add("5-telephone", data.PatientTelephoneNum.IsNotNullOrEmpty() && data.PatientTelephoneNum.Length == 10 ? data.PatientTelephoneNum.Substring(3, 3) + "-" + data.PatientTelephoneNum.Substring(6, 4) : string.Empty);
            fieldmap[0].Add("6-self", "X");
            fieldmap[0].Add("7", data.Claim != null && data.Claim.AddressLine1.IsNotNullOrEmpty() ? data.Claim.AddressLine1.ToUpper() + (data.Claim.AddressLine2.IsNotNullOrEmpty() ? " " + data.Claim.AddressLine2.ToUpper() : string.Empty) : string.Empty);
            fieldmap[0].Add("7-city", data.Claim != null && data.Claim.AddressCity.IsNotNullOrEmpty() ? data.Claim.AddressCity.ToUpper() : string.Empty);
            fieldmap[0].Add("7-state", data.Claim != null && data.Claim.AddressStateCode.IsNotNullOrEmpty() ? data.Claim.AddressStateCode.ToUpper() : string.Empty);
            fieldmap[0].Add("7-zip", data.Claim != null && data.Claim.AddressZipCode.IsNotNullOrEmpty() ? data.Claim.AddressZipCode.ToUpper() : string.Empty);
            if (data.PatientMaritalStatus.IsNotNullOrEmpty()) switch (data.PatientMaritalStatus)
                {
                    case "Single": fieldmap[0].Add("8-single", "X"); break;
                    case "Married": fieldmap[0].Add("8-married", "X"); break;
                    default: fieldmap[0].Add("8-other", "X"); break;
                }
            if (data.Claim != null && data.Claim.OtherProviderId.IsNotNullOrEmpty())
            {
                fieldmap[0].Add("9",
                    (data.Claim != null && data.Claim.LastName.IsNotNullOrEmpty() ? data.Claim.LastName.ToUpper() + ", " : string.Empty) +
                    (data.Claim != null && data.Claim.FirstName.IsNotNullOrEmpty() ? data.Claim.FirstName.ToUpper() : string.Empty));
                fieldmap[0].Add("9a", data.Claim.OtherProviderId);
                fieldmap[0].Add("9b-month", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("MM") : string.Empty);
                fieldmap[0].Add("9b-day", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("dd") : string.Empty);
                fieldmap[0].Add("9b-year", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("yyyy") : string.Empty);
                fieldmap[0].Add("9b-male", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("M") ? "X" : string.Empty);
                fieldmap[0].Add("9b-female", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("F") ? "X" : string.Empty);
            }
            fieldmap[0].Add("11a-month", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("MM") : string.Empty);
            fieldmap[0].Add("11a-day", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("dd") : string.Empty);
            fieldmap[0].Add("11a-year", data.Claim != null && data.Claim.DOB.IsValid() ? data.Claim.DOB.ToString("yyyy") : string.Empty);
            fieldmap[0].Add("11a-male", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("M") ? "X" : string.Empty);
            fieldmap[0].Add("11a-female", data.Claim != null && data.Claim.Gender.IsNotNullOrEmpty() && data.Claim.Gender.Substring(0, 1).ToUpper().Equals("F") ? "X" : string.Empty);
            fieldmap[0].Add("11c", (data.Claim != null && data.Claim.PayorName.IsNotNullOrEmpty() ? data.Claim.PayorName : string.Empty) + (data.Claim.IsHMO ? ("\n" + data.Claim.PayorAddressLine1 + "\n" + data.Claim.PayorAddressLine2) : string.Empty));
            if (data.Claim != null && data.Claim.OtherProviderId.IsNotNullOrEmpty()) fieldmap[0].Add("11d-yes", "X");
            else if (data.Claim != null) fieldmap[0].Add("11d-no", "X");
            fieldmap[0].Add("17",
                (data.Claim != null && data.Claim.PhysicianLastName.IsNotNullOrEmpty() ? data.Claim.PhysicianLastName.ToUpper() + ", " : string.Empty) +
                (data.Claim != null && data.Claim.PhysicianFirstName.IsNotNullOrEmpty() ? data.Claim.PhysicianFirstName.ToUpper() : string.Empty));
            fieldmap[0].Add("17b", data.Claim != null && data.Claim.PhysicianNPI.IsNotNullOrEmpty() ? data.Claim.PhysicianNPI : string.Empty);
            if (data.Claim != null && data.Claim.DiagnosisCode.IsNotNullOrEmpty())
            {
                var diagnosis = XElement.Parse(data.Claim.DiagnosisCode);
                fieldmap[0].Add("21-1", diagnosis != null && diagnosis.Element("code1") != null ? diagnosis.Element("code1").Value : string.Empty);
                fieldmap[0].Add("21-2", diagnosis != null && diagnosis.Element("code2") != null ? diagnosis.Element("code2").Value : string.Empty);
                fieldmap[0].Add("21-3", diagnosis != null && diagnosis.Element("code3") != null ? diagnosis.Element("code3").Value : string.Empty);
                fieldmap[0].Add("21-4", diagnosis != null && diagnosis.Element("code4") != null ? diagnosis.Element("code4").Value : string.Empty);
            }
            fieldmap[0].Add("23", data.Claim != null && data.Claim.AuthorizationNumber.IsNotNullOrEmpty() ? data.Claim.AuthorizationNumber : string.Empty);
            fieldmap[0].Add("25", data.Agency != null && data.Agency.TaxId.IsNotNullOrEmpty() ? data.Agency.TaxId : string.Empty);
            fieldmap[0].Add("25-ein", "X");
            fieldmap[0].Add("31-sign", data.Agency != null && data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name : string.Empty);
            fieldmap[0].Add("33-areacode", data.Agency != null && data.Agency.Phone.IsNotNullOrEmpty() ? data.Agency.Phone.Substring(0, 3) : string.Empty);
            fieldmap[0].Add("33-telephone", data.Agency != null && data.Agency.Phone.IsNotNullOrEmpty() ? data.Agency.Phone.Substring(3, 3) + "-" + data.Agency.Phone.Substring(6, 4) : string.Empty);
            fieldmap[0].Add("33",
                (data.Agency != null && data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToUpper() + "\n" : string.Empty) +
                (data.AgencyLocation != null && data.AgencyLocation.AddressFirstRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressFirstRow.ToUpper() + "\n" : string.Empty) +
                (data.AgencyLocation != null && data.AgencyLocation.AddressSecondRow.IsNotNullOrEmpty() ? data.AgencyLocation.AddressSecondRow.ToUpper() : string.Empty));
            fieldmap[0].Add("33a", data.Agency != null && data.Agency.NationalProviderNumber.IsNotNullOrEmpty() ? data.Agency.NationalProviderNumber : string.Empty);
            return fieldmap;
        }

        protected virtual IElement[] BuildContent(HCFA1500ViewData data)
        {
            var content = new AxxessTable[] { new AxxessTable(new float[] { 19, 21, 21, 21, 23, 21, 23, 22, 51, 26, 21, 21, 23, 36, 44, 19, 28, 15, 22, 86 }, false) };
            var font = AxxessPdf.sans;
            font.Size = 10;
            float[] padding = new float[] { 7, 1, 1, 1 }, borders = new float[] { 0, 0, 0, 0 };
            int count = 0;
            var schedules = data.Claim != null && data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ToList() : new List<ScheduleEvent>();
            if (schedules != null && schedules.Count > 0)
            {
                var visits = billingService.BillableVisitSummary(data.Claim.AgencyLocationId, schedules, ClaimType.MAN, data.Claim.ChargeRates, true);
                if (visits != null && visits.Count > 0)
                {
                    foreach (var visit in visits)
                    {
                        content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM") : string.Empty, font, padding, borders);
                        content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("dd") : string.Empty, font, padding, borders);
                        content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("yy") : string.Empty, font, padding, borders);
                        content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("MM") : string.Empty, font, padding, borders);
                        content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("dd") : string.Empty, font, padding, borders);
                        content[0].AddCell(visit.VisitDate != null && visit.VisitDate.IsValidDate() ? visit.VisitDate.ToDateTime().ToString("yy") : string.Empty, font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(visit.HCPCSCode != null && visit.HCPCSCode.IsNotNullOrEmpty() ? visit.HCPCSCode : string.Empty, font, padding, borders);
                        content[0].AddCell(visit.Modifier, font, padding, borders);
                        content[0].AddCell(visit.Modifier2, font, padding, borders);
                        content[0].AddCell(visit.Modifier3, font, padding, borders);
                        content[0].AddCell(visit.Modifier4, font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(Convert.ToInt32(visit.Charge).ToString(), font, "Right", padding, borders);
                        content[0].AddCell(Convert.ToInt32((visit.Charge % 1) * 100).ToString().PadLeft(2, '0'), font, padding, borders);
                        content[0].AddCell(visit.Unit > 0 ? visit.Unit.ToString() : string.Empty, font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(string.Empty, font, padding, borders);
                        content[0].AddCell(data.Agency != null && data.Agency.NationalProviderNumber.IsNotNullOrEmpty() ? data.Agency.NationalProviderNumber : string.Empty, font, padding, borders);
                        count++;
                    }
                }
                else return new Paragraph[] { new Paragraph(" ") };
            }
            if (count <= 0) return new Paragraph[] { new Paragraph(" ") };
            return content;
        }

        protected override void AddPageNumber(PdfContentByte swap, int pageNum, int numPages)
        {
            pageNum--;
            double total = 0;
            var schedules = this.data.Claim != null && this.data.Claim.VerifiedVisit.IsNotNullOrEmpty() ? this.data.Claim.VerifiedVisit.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValid()).OrderBy(s => s.VisitDate.Date).ToList() : new List<ScheduleEvent>();
            var visits = billingService.BillableVisitSummary(this.data.Claim.AgencyLocationId, schedules, this.data.Claim.ClaimType, this.data.Claim.ChargeRates, true);
            if (visits != null && visits.Count > pageNum * 6) for (int i = pageNum * 6; i < pageNum * 6 + 6 && i < visits.Count; i++) total += visits[i].Charge;
            string totalString = Convert.ToInt32(total).ToString() + " " + Convert.ToInt32((total % 1) * 100).ToString().PadLeft(2, '0');
            swap.BeginText();
            swap.SetFontAndSize(AxxessPdf.sans.BaseFont, 10F);
            swap.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, totalString, 443, 110, 0);
            swap.EndText();
        }

        protected double SupplyValue { get; set; }
        protected double VisitValue { get; set; }
    }
}