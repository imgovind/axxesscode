﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using iTextSharp.text;
    class RemittancePdf : AxxessPdf {
        private RemittanceData RemData;
        public RemittancePdf(Remittance data) {
            this.SetType(PdfDocs.Remittance);
            this.RemData = data.Data.ToObject<RemittanceData>();
            if (this.RemData == null) this.RemData = new RemittanceData();
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            if (this.RemData.Claim != null && this.RemData.Claim.Count > 0) {
                int count = 0;
                AxxessTable ClaimTable = new AxxessTable(1);
                foreach (var claim in this.RemData.Claim) {
                    if (claim.ProviderLevelAdjustment != null) {
                        AxxessContentTable Row = new AxxessContentTable(new AxxessContentSection[,] {
                            {
                                new AxxessContentSection("Provider Identifier", fonts[1], claim.ProviderLevelAdjustment.ProviderIdentifier, fonts[0], 10), 
                                new AxxessContentSection("Fiscal Period Date", fonts[1], (claim.ProviderLevelAdjustment.FiscalPeriodDate.IsValidPHPDate() ? claim.ProviderLevelAdjustment.FiscalPeriodDate.ToDateTimePHP().ToShortDateString() : string.Empty), fonts[0], 10)
                            },{
                                new AxxessContentSection("Adjustment Reason", fonts[1], claim.ProviderLevelAdjustment.AdjustmentReasonDesc, fonts[0], 10),
                                new AxxessContentSection("Adjustment Amount", fonts[1], String.Format("${0:#,0.00}", claim.ProviderLevelAdjustment.ProviderAdjustmentAmount), fonts[0], 10)
                            }
                        }, false);
                        ClaimTable.AddCell(new AxxessContentSection("Provider Level Adjustment", Row, fonts[1], 10));
                    }
                    if (claim.ClaimPaymentInformation != null && claim.ClaimPaymentInformation.Count > 0) {
                        foreach (var claimPaymentInfo in claim.ClaimPaymentInformation) {
                            AxxessContentTable Row = new AxxessContentTable(new AxxessContentSection[,] {
                                {
                                    new AxxessContentSection("Patient Name", fonts[1], (claimPaymentInfo.Patient != null ? String.Format("{0} {1}", claimPaymentInfo.Patient.FirstName, claimPaymentInfo.Patient.LastName) : string.Empty), fonts[0], 10), 
                                    new AxxessContentSection((claimPaymentInfo.Patient != null && claimPaymentInfo.Patient.IdQualifierName.IsNotNullOrEmpty() ? claimPaymentInfo.Patient.IdQualifierName : string.Empty), fonts[1], (claimPaymentInfo.Patient != null && claimPaymentInfo.Patient.IdQualifierName.IsNotNullOrEmpty() ? claimPaymentInfo.Patient.Id : string.Empty), fonts[0], 10)
                                },{
                                    new AxxessContentSection("Patient Control Number", fonts[1], claimPaymentInfo.PatientControlNumber, fonts[0], 10), 
                                    new AxxessContentSection("ICN Number", fonts[1], claimPaymentInfo.PayerClaimControlNumber, fonts[0], 10), 
                                },{
                                    new AxxessContentSection("Start Date", fonts[1], (claimPaymentInfo.ClaimStatementPeriodStartDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodStartDate.ToDateTimePHP().ToShortDateString() : string.Empty), fonts[0], 10), 
                                    new AxxessContentSection("End Date", fonts[1], (claimPaymentInfo.ClaimStatementPeriodEndDate.IsValidPHPDate() ? claimPaymentInfo.ClaimStatementPeriodEndDate.ToDateTimePHP().ToShortDateString() : string.Empty), fonts[0], 10)
                                },{
                                    new AxxessContentSection("Type Of Bill", fonts[1], claimPaymentInfo.TypeOfBill, fonts[0], 10), 
                                    new AxxessContentSection("Claim Status", fonts[1], String.Format("{0} ({1})", claimPaymentInfo.ClaimStatusDescription, claimPaymentInfo.ClaimStatusCode), fonts[0], 10), 
                                },{
                                    new AxxessContentSection("Claim Number", fonts[1], (count + 1).ToString(), fonts[0], 10), 
                                    new AxxessContentSection("Reported Charge", fonts[1], String.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount), fonts[0], 10)
                                },{
                                    new AxxessContentSection("Remittance", fonts[1], String.Format("${0:#,0.00}", claimPaymentInfo.TotalClaimChargeAmount), fonts[0], 10), 
                                    new AxxessContentSection("Line Adjustment Amount", fonts[1], String.Format("${0:#,0.00}", claimPaymentInfo.ServiceAdjustmentTotal), fonts[0], 10)
                                },{
                                    new AxxessContentSection(),
                                    new AxxessContentSection("Paid Amount", fonts[1], string.Format("${0:#,0.00}", claimPaymentInfo.ClaimPaymentAmount), fonts[0], 10)
                                }
                            }, false);
                            ClaimTable.AddCell(Row);
                        }
                    }
                }
                this.SetContent(new AxxessTable[] { ClaimTable });
            } else this.SetContent(new AxxessTitle[] { new AxxessTitle("No Remittances Found", fonts[1]) });
            this.SetMargins(new float[] { 244, 28, 28, 28 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : "") +
                    (data.Agency.MainLocation != null ?
                        (data.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? data.Agency.MainLocation.AddressLine1.ToTitleCase() : "") +
                        (data.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? data.Agency.MainLocation.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (data.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? data.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : "") +
                        (data.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? data.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "  " : "") +
                        (data.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? data.Agency.MainLocation.AddressZipCode : "") +
                        (data.Agency.MainLocation.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + data.Agency.MainLocation.PhoneWorkFormatted : "") +
                        (data.Agency.MainLocation.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + data.Agency.MainLocation.FaxNumberFormatted : "")
                    : "")
                : ""));
            fieldmap[0].Add("checknum", this.RemData.CheckNo.IsNotNullOrEmpty() ? this.RemData.CheckNo : string.Empty);
            fieldmap[0].Add("total", string.Format("${0:#,0.00}", data.PaymentAmount));
            fieldmap[0].Add("claims", data.TotalClaims.ToString());
            fieldmap[0].Add("date", data.RemittanceDate.IsValid() ? data.RemittanceDate.ToShortDateString() : string.Empty);
            fieldmap[0].Add("PayerName", this.RemData.Payer != null && this.RemData.Payer.Name.IsNotNullOrEmpty() ? this.RemData.Payer.Name : string.Empty);
            fieldmap[0].Add("PayerAddress1", this.RemData.Payer != null && this.RemData.Payer.Add1.IsNotNullOrEmpty() ? this.RemData.Payer.Add1 : string.Empty);
            fieldmap[0].Add("PayerAddress2", this.RemData.Payer != null && this.RemData.Payer.Add2.IsNotNullOrEmpty() ? this.RemData.Payer.Add2 : string.Empty);
            fieldmap[0].Add("PayerCity", this.RemData.Payer != null && this.RemData.Payer.City.IsNotNullOrEmpty() ? this.RemData.Payer.City : string.Empty);
            fieldmap[0].Add("PayerState", this.RemData.Payer != null && this.RemData.Payer.State.IsNotNullOrEmpty() ? this.RemData.Payer.State : string.Empty);
            fieldmap[0].Add("PayerZip", this.RemData.Payer != null && this.RemData.Payer.Zip.IsNotNullOrEmpty() ? this.RemData.Payer.Zip : string.Empty);
            if (this.RemData.Payer != null && this.RemData.Payer.RefType.IsNotNullOrEmpty()) {
                fieldmap[1].Add("PayerLabel1", this.RemData.Payer.RefType + ": ");
                fieldmap[0].Add("PayerAnswer1", this.RemData.Payer.RefNum.IsNotNullOrEmpty() ? this.RemData.Payer.RefNum : string.Empty);
            }
            fieldmap[0].Add("PayeeName", this.RemData.Payee != null && this.RemData.Payee.Name.IsNotNullOrEmpty() ? this.RemData.Payee.Name : string.Empty);
            fieldmap[0].Add("PayeeAddress1", this.RemData.Payee != null && this.RemData.Payee.Add1.IsNotNullOrEmpty() ? this.RemData.Payee.Add1 : string.Empty);
            fieldmap[0].Add("PayeeAddress2", this.RemData.Payee != null && this.RemData.Payee.Add2.IsNotNullOrEmpty() ? this.RemData.Payee.Add2 : string.Empty);
            fieldmap[0].Add("PayeeCity", this.RemData.Payee != null && this.RemData.Payee.City.IsNotNullOrEmpty() ? this.RemData.Payee.City : string.Empty);
            fieldmap[0].Add("PayeeState", this.RemData.Payee != null && this.RemData.Payee.State.IsNotNullOrEmpty() ? this.RemData.Payee.State : string.Empty);
            fieldmap[0].Add("PayeeZip", this.RemData.Payee != null && this.RemData.Payee.Zip.IsNotNullOrEmpty() ? this.RemData.Payee.Zip : string.Empty);
            if (this.RemData.Payee != null && this.RemData.Payee.RefType.IsNotNullOrEmpty()) {
                fieldmap[1].Add("PayeeLabel1", this.RemData.Payee.RefType + ": ");
                fieldmap[0].Add("PayeeAnswer1", this.RemData.Payee.RefNum.IsNotNullOrEmpty() ? this.RemData.Payee.RefNum : string.Empty);
            }
            if (this.RemData.Payee != null && this.RemData.Payee.IdType.IsNotNullOrEmpty()) {
                fieldmap[1].Add("PayeeLabel2", this.RemData.Payee.IdType + ": ");
                fieldmap[0].Add("PayeeAnswer2", this.RemData.Payee.Id.IsNotNullOrEmpty() ? this.RemData.Payee.Id : string.Empty);
            }
            this.SetFields(fieldmap);
        }
    }
}