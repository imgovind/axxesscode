﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.ViewData;
    using iTextSharp.text;
    class RemittancesPdf : AxxessPdf {
        public RemittancesPdf(RemittanceListViewData data) {
            this.SetType(PdfDocs.Remittances);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            if (data.Remittances.Count == 0) {
                Paragraph[] content = new Paragraph[] { new Paragraph(" ") };
                this.SetContent(content);
            } else {
                AxxessTable[] content = new AxxessTable[1];
                float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { 0, 0, .2F, 0 };
                content[0] = new AxxessTable(new float[] { 10, 61, 54, 53, 42 }, false);
                int count = 0;
                foreach (var remittance in data.Remittances) {
                    content[0].AddCell((++count).ToString(), fonts[0], padding, borders);
                    content[0].AddCell(remittance.RemitId, fonts[0], padding, borders);
                    content[0].AddCell(remittance.RemittanceDate > DateTime.MinValue ? remittance.RemittanceDate.ToString("MM/dd/yyyy") : string.Empty, fonts[0], padding, borders);
                    content[0].AddCell(string.Format("${0:#,0.00}", remittance.PaymentAmount), fonts[0], padding, borders);
                    content[0].AddCell(remittance.TotalClaims.ToString(), fonts[0], padding, borders);
                }
                this.SetContent(content);
            }
            this.SetMargins(new float[] { 100, 28, 28, 28 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : "") +
                    (data.Agency.MainLocation != null ?
                        (data.Agency.MainLocation.AddressLine1.IsNotNullOrEmpty() ? data.Agency.MainLocation.AddressLine1.ToTitleCase() : "") +
                        (data.Agency.MainLocation.AddressLine2.IsNotNullOrEmpty() ? data.Agency.MainLocation.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (data.Agency.MainLocation.AddressCity.IsNotNullOrEmpty() ? data.Agency.MainLocation.AddressCity.ToTitleCase() + ", " : "") +
                        (data.Agency.MainLocation.AddressStateCode.IsNotNullOrEmpty() ? data.Agency.MainLocation.AddressStateCode.ToString().ToUpper() + "  " : "") +
                        (data.Agency.MainLocation.AddressZipCode.IsNotNullOrEmpty() ? data.Agency.MainLocation.AddressZipCode : "") +
                        (data.Agency.MainLocation.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + data.Agency.MainLocation.PhoneWorkFormatted : "") +
                        (data.Agency.MainLocation.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + data.Agency.MainLocation.FaxNumberFormatted : "")
                    : "")
                : ""));
            this.SetFields(fieldmap);
        }
    }
}