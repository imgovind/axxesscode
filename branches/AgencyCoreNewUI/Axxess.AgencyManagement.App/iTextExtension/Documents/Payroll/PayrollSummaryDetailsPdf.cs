﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class PayrollSummaryDetailsPdf : AxxessPdf {
        private List<PayrollDetail> data;
        private Agency agency;
        private DateTime startDate;
        private DateTime endDate;
        public PayrollSummaryDetailsPdf(PayrollDetail data, Agency agency, DateTime startDate, DateTime endDate) {
            this.SetPageSize(AxxessDoc.Landscape);
            this.data = new List<PayrollDetail>();
            this.data.Add(data);
            this.agency = agency;
            this.startDate = startDate;
            this.endDate = endDate;
            this.BuildLayout();
        }
        public PayrollSummaryDetailsPdf(List<PayrollDetail> data, Agency agency, DateTime startDate, DateTime endDate) {
            this.data = data;
            this.agency = agency;
            this.startDate = startDate;
            this.endDate = endDate;
            this.BuildLayout();
        }
        private void BuildLayout() {
            this.SetType(PdfDocs.PayrollDetail);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 8F;
            fonts[2].Size = 10F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[this.data.Count];
            int counter = 0;
            foreach (var table in data) {
                AxxessTable usertable = new AxxessTable(new float[] { 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, false);
                AxxessCell user = new AxxessCell(padding, borders);
                user.Colspan = 13;
                user.AddElement(new Paragraph("Employee: " + table.Name, fonts[2]));
                usertable.AddCell(user);
                usertable.AddCell("Date", fonts[1], padding, borders);
                usertable.AddCell("Time In", fonts[1], padding, borders);
                usertable.AddCell("Time Out", fonts[1], padding, borders);
                usertable.AddCell("Patient Name", fonts[1], padding, borders);
                usertable.AddCell("MRN", fonts[1], padding, borders);
                usertable.AddCell("Input Date", fonts[1], padding, borders);
                usertable.AddCell("Miles", fonts[1], padding, borders);
                usertable.AddCell("Hours", fonts[1], padding, borders);
                usertable.AddCell("Expected Pay Rate", fonts[1], padding, borders);
                usertable.AddCell("Expected Pay", fonts[1], padding, borders);
                usertable.AddCell("Actual Pay Rate", fonts[1], padding, borders);
                usertable.AddCell("Actual Paid", fonts[1], padding, borders);
                usertable.AddCell("Paid Date", fonts[1], padding, borders);
                usertable.HeaderRows = 2;
                var totalTime=0;
                foreach (UserVisit row in table.Visits) {
                    usertable.AddCell(row.VisitDate.ToString("MM/dd/yyyy"), fonts[0], padding, borders);
                    usertable.AddCell(row.TimeIn, fonts[0], padding, borders);
                    usertable.AddCell(row.TimeOut, fonts[0], padding, borders);
                    usertable.AddCell(row.PatientName, fonts[0], padding, borders);
                    usertable.AddCell(row.PatientIdNumber, fonts[0], padding, borders);
                    usertable.AddCell(string.Empty, fonts[0], padding, borders);
                    usertable.AddCell(row.AssociatedMileage, fonts[0], padding, borders);
                    usertable.AddCell(String.Format("{0:#0.00}", (double)row.MinSpent / 60), fonts[0], padding, borders);
                    usertable.AddCell(row.VisitRate, fonts[0], padding, borders);
                    usertable.AddCell(string.Empty, fonts[0], padding, borders);
                    usertable.AddCell(string.Empty, fonts[0], padding, borders);
                    usertable.AddCell(string.Empty, fonts[0], padding, borders);
                    usertable.AddCell(string.Empty, fonts[0], padding, borders);
                    totalTime += row.MinSpent;
                }
                content[counter++] = usertable;
                AxxessCell userFooter = new AxxessCell(padding, borders);
                userFooter.Colspan = 13;
                userFooter.AddElement(new AxxessTitle(string.Format("Total Number of Payroll Detail: {0}, Total Time for this employee: {1}", table.Visits.Count, string.Format(" {0} min = {1:#0.00} hour(s) ", totalTime, (double)totalTime / 60)), fonts[1]));
                usertable.AddCell(userFooter);

            }
            this.SetContent(content);
            float[] margins = new float[] { 57, 28, 28, 28 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[0].Add("agency", data != null && agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() : string.Empty);
            fieldmap[0].Add("date", this.startDate.ToShortDateString() + " - " + this.endDate.ToShortDateString());
            this.SetFields(fieldmap);
        }
    }
}