﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.AgencyManagement.App.iTextExtension.XmlParsing;
    using Axxess.AgencyManagement.App.Enums;
    abstract class VisitNotePdf : AxxessPdf {
        private VisitNoteXml xml;
        protected String DocType;
        public VisitNotePdf(VisitNoteViewData data, int rev) : base() {
            PdfDoc type;
            switch (data.Type) {
                case "MSWAssessment": case "MSWDischarge": case "MSWEvaluationAssessment":
                    type = PdfDocs.MSWEvaluationAssessment;
                    this.Init(data, type, rev);
                    break;
                case "MSWProgressNote":
                    type = PdfDocs.MSWProgressNote;
                    this.Init(data, type, rev);
                    break;
                case "MSWVisit":
                    type = PdfDocs.MSWVisit;
                    this.Init(data, type, rev);
                    break;
                case "PTDischarge":
                    type = PdfDocs.PTDischarge;
                    this.Init(data, type, rev);
                    break;
                case "PTEvaluation": case "PTReEvaluation": case "PTMaintenance":
                    type = PdfDocs.PTEvaluation;
                    this.Init(data, type, rev);
                    break;
                case "PTVisit": case "PTAVisit":
                    type = PdfDocs.PTVisit;
                    this.Init(data, type, rev);
                    break;
                case "OTEvaluation": case "OTReEvaluation": case "OTDischarge": case "OTMaintenance":
                    type = PdfDocs.OTEvaluation;
                    this.Init(data, type, rev);
                    break;
                case "OTVisit": case "COTAVisit":
                    type = PdfDocs.OTVisit;
                    this.Init(data, type, rev);
                    break;
                case "STEvaluation": case "STReEvaluation": case "STMaintenance": case "STDischarge":
                    type = PdfDocs.STEvaluation;
                    this.Init(data, type, rev);
                    break;
                case "STVisit":
                    type = PdfDocs.STVisit;
                    this.Init(data, type, rev);
                    break;
                case "DriverOrTransportationNote":
                    type = PdfDocs.DriverOrTransportationNote;
                    this.Init(data, type, rev);
                    break;
            }
        }
        public VisitNotePdf(VisitNoteViewData data, PdfDoc type, int rev) : base() {
            this.Init(data, type, rev);
        }
        private void Init(VisitNoteViewData data, PdfDoc type, int rev) {
            if (rev > 0) this.xml = new VisitNoteXml(data, type, rev);
            else this.xml = new VisitNoteXml(data, type);
            this.SetType(type);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            this.SetContent(this.Content(this.xml));
            this.SetMargins(this.Margins(data));
            this.SetFields(this.FieldMap(data));
        }
        protected virtual IElement[] Content(VisitNoteXml xml) {
            AxxessContentSection[] content = new AxxessContentSection[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection section in this.xml.GetLayout()) {
                content[count] = new AxxessContentSection(section, this.GetFonts(), true, 10, this.IsOasis);
                count++;
            }
            return content;
        }
        protected virtual List<Dictionary<String, String>> FieldMap(VisitNoteViewData data) {
            return new List<Dictionary<string, string>>();
        }
        protected virtual float[] Margins(VisitNoteViewData data) {
            return new float[] { 0, 0, 0, 0 };
        }
    }
}