﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.Domain;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    using iTextSharp.text;
    using Axxess.AgencyManagement.Domain;
    class TriageClassificationPdf : AxxessPdf
    {
        public TriageClassificationPdf(PatientProfile data)
        {
            this.SetType(PdfDocs.TriageClassification);
            var isDataExist = data != null && data.Patient != null;
            var patient = data != null ? data.Patient : new Patient();
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(" ") };
            this.SetContent(content);
            this.SetMargins(new float[] { 0, 0, 0, 0 });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(isDataExist != null ? patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : string.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : string.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : string.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : string.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : string.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : string.Empty)
                    : string.Empty)
                : string.Empty));
            fieldmap[0].Add("patientname", isDataExist ? (patient.LastName.IsNotNullOrEmpty() ? patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) + (patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) + (patient.MiddleInitial.IsNotNullOrEmpty() ? patient.MiddleInitial.ToUpper() + "\n" : "\n") : string.Empty);
            fieldmap[0].Add("patient", (
                isDataExist ?
                    (patient.AddressLine1.IsNotNullOrEmpty() ? patient.AddressLine1.ToTitleCase() : string.Empty) +
                    (patient.AddressLine2.IsNotNullOrEmpty() ? patient.AddressLine2.ToTitleCase() + "\n" : "\n") +
                    (patient.AddressCity.IsNotNullOrEmpty() ? patient.AddressCity.ToTitleCase() + ", " : string.Empty) +
                    (patient.AddressStateCode.IsNotNullOrEmpty() ? patient.AddressStateCode.ToString().ToUpper() + "  " : string.Empty) +
                    (patient.AddressZipCode.IsNotNullOrEmpty() ? patient.AddressZipCode : string.Empty) +
                    (patient.PhoneHome.IsNotNullOrEmpty() ? "\nPhone: " + patient.PhoneHome.ToPhone() : string.Empty)
                : string.Empty));
            fieldmap[0].Add("mr", isDataExist && patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("dob", isDataExist && patient.DOBFormatted.IsNotNullOrEmpty() ? patient.DOBFormatted : string.Empty);
            fieldmap[0].Add("ecname", isDataExist && patient.EmergencyContacts.Count > 0 && patient.EmergencyContacts[0].DisplayName.IsNotNullOrEmpty() ? patient.EmergencyContacts[0].DisplayName.Clean() : string.Empty);
            fieldmap[0].Add("ecphone", isDataExist && patient.EmergencyContacts.Count > 0 && patient.EmergencyContacts[0].PrimaryPhone.IsNotNullOrEmpty() ? patient.EmergencyContacts[0].PrimaryPhone.ToPhone().Clean() : string.Empty);
            fieldmap[0].Add("tri1", isDataExist && patient.Triage == 1 ? "Yes" : string.Empty);
            fieldmap[0].Add("tri2", isDataExist && patient.Triage == 2 ? "Yes" : string.Empty);
            fieldmap[0].Add("tri3", isDataExist && patient.Triage == 3 ? "Yes" : string.Empty);
            fieldmap[0].Add("tri4", isDataExist && patient.Triage == 4 ? "Yes" : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}