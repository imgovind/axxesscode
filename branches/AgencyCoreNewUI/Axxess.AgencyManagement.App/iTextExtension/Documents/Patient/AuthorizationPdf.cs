﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class AuthorizationPdf : AxxessPdf {
        public AuthorizationPdf(Authorization data) {
            this.SetType(PdfDocs.Authorization);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            Paragraph[] content = new Paragraph[] { new Paragraph(data != null && data.Comments.IsNotNullOrEmpty() ? data.Comments : " ") };
            this.SetContent(content);
            this.SetMargins(new float[] { 130, 28.3F, 90.5F, 28.3F });
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency",
                data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty);
            fieldmap[0].Add("patientname",
                data.Patient != null ?
                    (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToLower().ToTitleCase() + ", " : string.Empty) +
                    (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToLower().ToTitleCase() + " " : string.Empty) +
                    (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n")
                : string.Empty);
            fieldmap[0].Add("mr", data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            fieldmap[0].Add("dob", data.Patient != null && data.Patient.DOBFormatted.IsNotNullOrEmpty() ? data.Patient.DOBFormatted : string.Empty);
            fieldmap[0].Add("start", data != null && data.StartDateFormatted.IsNotNullOrEmpty() ? data.StartDateFormatted : string.Empty);
            fieldmap[0].Add("end", data != null && data.EndDateFormatted.IsNotNullOrEmpty() ? data.EndDateFormatted : string.Empty);
            fieldmap[0].Add("branch", data != null && data.Branch.IsNotNullOrEmpty() ? data.Branch : string.Empty);
            fieldmap[0].Add("status", data != null && data.Status.IsNotNullOrEmpty() ? data.Status : string.Empty);
            fieldmap[0].Add("insurance", data != null && data.Insurance.IsNotNullOrEmpty() ? data.Insurance : string.Empty);
            fieldmap[0].Add("auth1", data != null && data.Number1.IsNotNullOrEmpty() ? data.Number1 : string.Empty);
            fieldmap[0].Add("auth2", data != null && data.Number2.IsNotNullOrEmpty() ? data.Number2 : string.Empty);
            fieldmap[0].Add("auth3", data != null && data.Number3.IsNotNullOrEmpty() ? data.Number3 : string.Empty);
            fieldmap[0].Add("sn", data != null && data.SNVisit.IsNotNullOrEmpty() ? data.SNVisit + " " + (data.SNVisitCountType.Equals("1") ? "Visits" : string.Empty) + (data.SNVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("pt", data != null && data.PTVisit.IsNotNullOrEmpty() ? data.PTVisit + " " + (data.PTVisitCountType.Equals("1") ? "Visits" : string.Empty) + (data.PTVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("ot", data != null && data.OTVisit.IsNotNullOrEmpty() ? data.OTVisit + " " + (data.OTVisitCountType.Equals("1") ? "Visits" : string.Empty) + (data.OTVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("st", data != null && data.STVisit.IsNotNullOrEmpty() ? data.STVisit + " " + (data.STVisitCountType.Equals("1") ? "Visits" : string.Empty) + (data.STVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("msw", data != null && data.MSWVisit.IsNotNullOrEmpty() ? data.MSWVisit + " " + (data.MSWVisitCountType.Equals("1") ? "Visits" : string.Empty) + (data.MSWVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("hha", data != null && data.HHAVisit.IsNotNullOrEmpty() ? data.HHAVisit + " " + (data.HHAVisitCountType.Equals("1") ? "Visits" : string.Empty) + (data.HHAVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("dietician", data != null && data.DieticianVisit.IsNotNullOrEmpty() ? data.DieticianVisit + " " + (data.DieticianVisitCountType.Equals("1") ? "Visits" : string.Empty) + (data.DieticianVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("rn", data != null && data.RNVisit.IsNotNullOrEmpty() ? data.RNVisit + " " + (data.RNVisitCountType.Equals("1") ? "Visits" : string.Empty) + (data.RNVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            fieldmap[0].Add("lvn", data != null && data.LVNVisit.IsNotNullOrEmpty() ? data.LVNVisit + " " + (data.LVNVisitCountType.Equals("1") ? "Visits" : string.Empty) + (data.LVNVisitCountType.Equals("2") ? "Hours" : string.Empty) : string.Empty);
            this.SetFields(fieldmap);
        }
    }
}