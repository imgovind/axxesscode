﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Net;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    class MessagePdf : AxxessPdf {
        public MessagePdf(Message data, Agency agency) {
            this.SetType(PdfDocs.Message);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 12F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 6, 2 }, noborder = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[3];
            content[0] = new AxxessTable(1);
            AxxessCell name = new AxxessCell(padding, new float[] { 0, 0, 1, 0 });
            name.AddElement(new Paragraph(Current.UserFullName, fonts[1]));
            content[0].AddCell(name);
            content[1] = new AxxessTable(new float[] { 1, 4 }, false);
            AxxessCell fromLabel = new AxxessCell(padding, noborder),
                from = new AxxessCell(padding, noborder),
                sentLabel = new AxxessCell(padding, noborder),
                sent = new AxxessCell(padding, noborder),
                toLabel = new AxxessCell(padding, noborder),
                to = new AxxessCell(padding, noborder),
                subjectLabel = new AxxessCell(padding, noborder),
                subject = new AxxessCell(padding, noborder),
                regardingLabel = new AxxessCell(padding, noborder),
                regarding = new AxxessCell(padding, noborder);
            fromLabel.AddElement(new Paragraph("From:", fonts[1]));
            sentLabel.AddElement(new Paragraph("Sent:", fonts[1]));
            toLabel.AddElement(new Paragraph("To:", fonts[1]));
            subjectLabel.AddElement(new Paragraph("Subject:", fonts[1]));
            regardingLabel.AddElement(new Paragraph("Regarding:", fonts[1]));
            from.AddElement(new Paragraph(data.FromName));
            sent.AddElement(new Paragraph(data.MessageDate));
            to.AddElement(new Paragraph(data.RecipientNames));
            subject.AddElement(new Paragraph(data.Subject));
            regarding.AddElement(new Paragraph(data.PatientName));
            content[1].AddCell(fromLabel);
            content[1].AddCell(from);
            content[1].AddCell(sentLabel);
            content[1].AddCell(sent);
            content[1].AddCell(toLabel);
            content[1].AddCell(to);
            content[1].AddCell(subjectLabel);
            content[1].AddCell(subject);
            content[1].AddCell(regardingLabel);
            content[1].AddCell(regarding);
            AxxessCell message = new AxxessCell(padding, noborder);
            message.AddElement(new Paragraph(data.Body));
            content[2] = new AxxessTable(1);
            content[2].AddCell(message);
            this.SetContent(content);
            float[] margins = new float[] { 83, 28, 25, 28 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = agency.GetMainOffice();
            fieldmap[0].Add("agency", (
                agency != null ?
                    (agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            this.SetFields(fieldmap);
        }
    }
}