﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System.IO;
    using System.Linq;
    using iTextSharp.text;
    using Axxess.OasisC.Domain;
    class PlanOfCarePdf : HCFA485Pdf {
        private HCFA487Pdf cms487;
        public PlanOfCarePdf(PlanofCare planOfCare) : base(planOfCare) {
            IElement[] content = this.get487content();
            if (content != null && content.Count() > 0) this.cms487 = new HCFA487Pdf(content, this.get487fieldmap());
        }
        public MemoryStream GetPlanOfCareStream() {
            return this.GetStream(this.cms487);
        }
    }
}