﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using Axxess.OasisC.Domain;
    using Axxess.Core.Extension;
    using Axxess.OasisC.Extensions;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.Core.Infrastructure;
    internal class PlanOfCare485Pdf : AxxessPdf
    {
        private Dictionary<String, String> cms487, fieldmap487;
        private readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;
        private readonly IPatientRepository patientRepository = Container.Resolve<IAgencyManagementDataProvider>().PatientRepository;
        private readonly IPhysicianRepository physicianRepository = Container.Resolve<IAgencyManagementDataProvider>().PhysicianRepository;
        public PlanOfCare485Pdf(PlanofCare planOfCare)
        {
            this.SetType(PdfDocs.PlanOfCare485);
            this.cms487 = new Dictionary<String, String>();
            this.fieldmap487 = new Dictionary<String, String>();
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            this.SetContent(new IElement[] { new Chunk(planOfCare.AssessmentType == "StartOfCare" || planOfCare.AssessmentType == "Recertification" ? "_____" : "") });
            float[] margins = new float[] { 35, 35, 35, 35 };
            if (planOfCare.AssessmentType == "StartOfCare") margins = new float[] { 673, 266, 117, 321 };
            if (planOfCare.AssessmentType == "Recertification" || planOfCare.AssessmentType == "ResumptionOfCare") margins = new float[] { 673, 237, 117, 343 };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            planOfCare.Questions = planOfCare.Data.ToObject<List<Question>>();
            var data = planOfCare.ToDictionary();
            var medicationList = planOfCare.MedicationProfileData != null ? planOfCare.MedicationProfileData.ToObject<List<Medication>>() : new List<Medication>();
            var patient = planOfCare.PatientData.IsNotNullOrEmpty() ? planOfCare.PatientData.ToObject<Patient>() : new Patient();
            var agency = agencyRepository.GetWithBranches(Current.AgencyId) ?? new Agency();
            var location = agency.GetBranch(patient != null ? patient.AgencyLocationId : Guid.Empty);
            if (location == null) location = agency.GetMainOffice();
            var physician = physicianRepository.Get(planOfCare.PhysicianId, Current.AgencyId);
            if (physician == null) physician = planOfCare.PhysicianData != null ? planOfCare.PhysicianData.ToObject<AgencyPhysician>() : new AgencyPhysician();
            string loc13a = string.Empty, loc13b = string.Empty, loc13boe = string.Empty, loc13c = string.Empty;
            for (int i = 1; i < 5; i++)
            {
                loc13a += data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() ? (i > 1 ? "\n" : "") + data["M1022ICD9M" + i].Answer : "";
                loc13b += data.ContainsKey("M1022PrimaryDiagnosis" + i) && data["M1022PrimaryDiagnosis" + i].Answer.IsNotNullOrEmpty() ? (i > 1 ? "\n" : "") + data["M1022PrimaryDiagnosis" + i].Answer : "";
                loc13boe += (data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis" + i) ? (data["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer.Equals("1") ? "E" : "") + (data["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer.Equals("2") ? "O" : "") : "") + "\n";
                loc13c += data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() && data.ContainsKey("M1022PrimaryDiagnosis" + i + "Date") ? (i > 1 ? "\n" : "") + data["M1022PrimaryDiagnosis" + i + "Date"].Answer : "";
            }
            string[] functionLimitations = data.ContainsKey("485FunctionLimitations") && data["485FunctionLimitations"].Answer != "" ? data["485FunctionLimitations"].Answer.Split(',') : null;
            string[] activitiesPermitted = data.ContainsKey("485ActivitiesPermitted") && data["485ActivitiesPermitted"].Answer != "" ? data["485ActivitiesPermitted"].Answer.Split(',') : null;
            string[] mentalStatus = data.ContainsKey("485MentalStatus") && data["485MentalStatus"].Answer != "" ? data["485MentalStatus"].Answer.Split(',') : null;
            fieldmap[0].Add("485-01", patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber : patient.MedicaidNumber.IsNotNullOrEmpty() ? patient.MedicaidNumber.Clean() : string.Empty);
            fieldmap[0].Add("485-02", patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? patient.StartOfCareDateFormatted : "");
            fieldmap[0].Add("485-03a", planOfCare.EpisodeStart);
            fieldmap[0].Add("485-03b", planOfCare.EpisodeEnd);
            fieldmap[0].Add("485-04", patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber : "");
            fieldmap[0].Add("485-05", location.MedicareProviderNumber.IsNotNullOrEmpty() ? location.MedicareProviderNumber : "");
            fieldmap[0].Add("485-06", (patient.LastName.IsNotNullOrEmpty() ? patient.LastName + ", " : "") + (patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName : "") + (patient.MiddleInitial.IsNotNullOrEmpty() ? " " + patient.MiddleInitial + ".\n" : "\n") + (patient.AddressLine1.IsNotNullOrEmpty() ? patient.AddressLine1.ToTitleCase() : "") + (patient.AddressLine2.IsNotNullOrEmpty() ? " " + patient.AddressLine2.ToTitleCase() : "") + (patient.AddressCity.IsNotNullOrEmpty() ? "\n" + patient.AddressCity.ToTitleCase() : "") + (patient.AddressStateCode.IsNotNullOrEmpty() ? ", " + patient.AddressStateCode.ToUpper() : "") + (patient.AddressZipCode.IsNotNullOrEmpty() ? "  " + patient.AddressZipCode : "") + (patient.PhoneHomeFormatted.IsNotNullOrEmpty() ? "\n" + patient.PhoneHomeFormatted : ""));
            fieldmap[0].Add("485-07", (agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + "\n" : "") + (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : "") + (location.AddressLine2.IsNotNullOrEmpty() ? " " + location.AddressLine2.ToTitleCase() : "") + (location.AddressCity.IsNotNullOrEmpty() ? "\n" + location.AddressCity.ToTitleCase() : "") + (location.AddressStateCode.IsNotNullOrEmpty() ? ", " + location.AddressStateCode : "") + (location.AddressZipCode.IsNotNullOrEmpty() ? "  " + location.AddressZipCode : "") + (location.PhoneWork.IsNotNullOrEmpty() ? "\nPhone: (" + location.PhoneWork.Substring(0, 3) + ") " + location.PhoneWork.Substring(3, 3) + "-" + location.PhoneWork.Substring(6, 4) : "") + (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : "") + (agency.ContactPersonEmail.IsNotNullOrEmpty() ? "\nEmail: " + agency.ContactPersonEmail : ""));
            fieldmap[0].Add("485-08", patient.DOBFormatted.IsNotNullOrEmpty() ? patient.DOBFormatted : "");
            if (patient.Gender.IsNotNullOrEmpty() && patient.Gender == "Male") fieldmap[0].Add("485-09m", "Yes");
            if (patient.Gender.IsNotNullOrEmpty() && patient.Gender == "Female") fieldmap[0].Add("485-09f", "Yes");
            fieldmap[0].Add("485-10", this.splitData(data.ContainsKey("485Medications") ? data["485Medications"].Answer.Split('\n') : new String[0], "\n", 380, 120, "10. Medications"));
            fieldmap[0].Add("485-11a", data.ContainsKey("M1020ICD9M") ? data["M1020ICD9M"].Answer : "");
            fieldmap[0].Add("485-11b", data.ContainsKey("M1020PrimaryDiagnosis") ? data["M1020PrimaryDiagnosis"].Answer : "");
            fieldmap[0].Add("485-11boe", data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis") ? (data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer.Equals("1") ? "E" : "") + (data["485ExacerbationOrOnsetPrimaryDiagnosis"].Answer.Equals("2") ? "O" : "") : "");
            fieldmap[0].Add("485-11c", data.ContainsKey("M1020PrimaryDiagnosisDate") ? data["M1020PrimaryDiagnosisDate"].Answer : "");
            fieldmap[0].Add("485-12a", data.ContainsKey("485SurgicalProcedureCode1") ? data["485SurgicalProcedureCode1"].Answer : "");
            fieldmap[0].Add("485-12b", data.ContainsKey("485SurgicalProcedureDescription1") ? data["485SurgicalProcedureDescription1"].Answer : "");
            fieldmap[0].Add("485-12c", data.ContainsKey("485SurgicalProcedureCode1Date") ? data["485SurgicalProcedureCode1Date"].Answer : "");
            if (data.ContainsKey("485SurgicalProcedureCode2") && data["485SurgicalProcedureCode2"].Answer.IsNotNullOrEmpty())
            {
                this.cms487.Add("12. Surgical Procedure",
                    data["485SurgicalProcedureCode2"].Answer +
                    (data.ContainsKey("485SurgicalProcedureDescription2") && data["485SurgicalProcedureDescription2"].Answer.IsNotNullOrEmpty() ? " / " + data["485SurgicalProcedureDescription2"].Answer : "") +
                    (data.ContainsKey("485SurgicalProcedureCode2Date") && data["485SurgicalProcedureCode2Date"].Answer.IsNotNullOrEmpty() ? " / " + data["485SurgicalProcedureCode2Date"].Answer : ""));
            }
            fieldmap[0].Add("485-13a", loc13a);
            fieldmap[0].Add("485-13b", loc13b);
            fieldmap[0].Add("485-13boe", loc13boe);
            fieldmap[0].Add("485-13c", loc13c);
            if (data.ContainsKey("M1022ICD9M5") && data["M1022ICD9M5"].Answer.IsNotNullOrEmpty())
            {
                String diag487 = string.Empty;
                for (int i = 5; i < 27; i++) diag487 +=
                    (data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() ? (i > 5 ? "\n" : "") + data["M1022ICD9M" + i].Answer : "") +
                    (data.ContainsKey("M1022PrimaryDiagnosis" + i) && data["M1022PrimaryDiagnosis" + i].Answer.IsNotNullOrEmpty() ? " / " + data["M1022PrimaryDiagnosis" + i].Answer : "") +
                    (data.ContainsKey("485ExacerbationOrOnsetPrimaryDiagnosis" + i) ? (data["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer.Equals("1") ? " (E)" : "") + (data["485ExacerbationOrOnsetPrimaryDiagnosis" + i].Answer.Equals("2") ? " (O)" : "") : "") +
                    (data.ContainsKey("M1022ICD9M" + i) && data["M1022ICD9M" + i].Answer.IsNotNullOrEmpty() && data.ContainsKey("M1022PrimaryDiagnosis" + i + "Date") ? " / " + data["M1022PrimaryDiagnosis" + i + "Date"].Answer : "");
                this.cms487.Add("13. Diagnoses", diag487);
            }
            fieldmap[0].Add("485-14", this.splitData((
                (data.ContainsKey("485DME") && data["485DME"].Answer.IsNotNullOrEmpty() ? PlanofCareXml.LookupText("DME", data["485DME"].Answer) : "") +
                (data.ContainsKey("485DMEComments") && data["485DMEComments"].Answer.IsNotNullOrEmpty() ? ", " + data["485DMEComments"].Answer : "") +
                (data.ContainsKey("485Supplies") && data["485Supplies"].Answer.IsNotNullOrEmpty() ? ", " + PlanofCareXml.LookupText("Supplies", data["485Supplies"].Answer) : "") +
                (data.ContainsKey("485SuppliesComment") && data["485SuppliesComment"].Answer.IsNotNullOrEmpty() ? ", " + data["485SuppliesComment"].Answer : "")).Split(' '), " ", 380, 15, "14. DME and Supplies"));
            fieldmap[0].Add("485-15", this.splitData((
                (data.ContainsKey("485SafetyMeasures") && data["485SafetyMeasures"].Answer.IsNotNullOrEmpty() ? PlanofCareXml.LookupText("SafetyMeasures", data["485SafetyMeasures"].Answer) : "") +
                (data.ContainsKey("485OtherSafetyMeasures") && data["485OtherSafetyMeasures"].Answer.IsNotNullOrEmpty() ? ", " + data["485OtherSafetyMeasures"].Answer : "")).Split(' '), " ", 380, 15, "15. Safety Measures"));
            fieldmap[0].Add("485-16", this.splitData(data.ContainsKey("485NutritionalReqs") ? data["485NutritionalReqs"].Answer.Split(' ') : new String[0], " ", 270, 15, "16. Nutritional Requirements"));
            fieldmap[0].Add("485-17", this.splitData(data.ContainsKey("485Allergies") && data["485Allergies"].Answer == "No" ? new String[] { "NKA", "(Food/Drugs/Latex)" } : (data.ContainsKey("485AllergiesDescription") ? data["485AllergiesDescription"].Answer.Split(' ') : new String[] { "Allergies", "Not", "Specified" }), " ", 300, 15, "17. Allergies"));
            if (functionLimitations != null && functionLimitations.Contains("1")) fieldmap[0].Add("485-18a-1", "Yes");
            if (functionLimitations != null && functionLimitations.Contains("2")) fieldmap[0].Add("485-18a-2", "Yes");
            if (functionLimitations != null && functionLimitations.Contains("3")) fieldmap[0].Add("485-18a-3", "Yes");
            if (functionLimitations != null && functionLimitations.Contains("4")) fieldmap[0].Add("485-18a-4", "Yes");
            if (functionLimitations != null && functionLimitations.Contains("5")) fieldmap[0].Add("485-18a-5", "Yes");
            if (functionLimitations != null && functionLimitations.Contains("6")) fieldmap[0].Add("485-18a-6", "Yes");
            if (functionLimitations != null && functionLimitations.Contains("7")) fieldmap[0].Add("485-18a-7", "Yes");
            if (functionLimitations != null && functionLimitations.Contains("8")) fieldmap[0].Add("485-18a-8", "Yes");
            if (functionLimitations != null && functionLimitations.Contains("9")) fieldmap[0].Add("485-18a-9", "Yes");
            if (functionLimitations != null && functionLimitations.Contains("A")) fieldmap[0].Add("485-18a-A", "Yes");
            if (functionLimitations != null && functionLimitations.Contains("B"))
            {
                fieldmap[0].Add("485-18a-B", "Yes");
                if (data.ContainsKey("485FunctionLimitationsOther")) fieldmap[0].Add("485-18a-B-text", this.splitData(data["485FunctionLimitationsOther"].Answer.Split(' '), " ", 150, 30, "18.A. Functional Limitations"));
            }
            if (activitiesPermitted != null && activitiesPermitted.Contains("1")) fieldmap[0].Add("485-18b-1", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("2")) fieldmap[0].Add("485-18b-2", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("3")) fieldmap[0].Add("485-18b-3", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("4")) fieldmap[0].Add("485-18b-4", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("5")) fieldmap[0].Add("485-18b-5", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("6")) fieldmap[0].Add("485-18b-6", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("7")) fieldmap[0].Add("485-18b-7", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("8")) fieldmap[0].Add("485-18b-8", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("9")) fieldmap[0].Add("485-18b-9", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("A")) fieldmap[0].Add("485-18b-A", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("B")) fieldmap[0].Add("485-18b-B", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("C")) fieldmap[0].Add("485-18b-C", "Yes");
            if (activitiesPermitted != null && activitiesPermitted.Contains("D"))
            {
                fieldmap[0].Add("485-18b-D", "Yes");
                if (data.ContainsKey("485ActivitiesPermittedOther")) fieldmap[0].Add("485-18b-D-text", this.splitData(data["485ActivitiesPermittedOther"].Answer.Split(' '), " ", 250, 15, "18.B. Activities Permitted"));
            }
            if (mentalStatus != null && mentalStatus.Contains("1")) fieldmap[0].Add("485-19-1", "Yes");
            if (mentalStatus != null && mentalStatus.Contains("2")) fieldmap[0].Add("485-19-2", "Yes");
            if (mentalStatus != null && mentalStatus.Contains("3")) fieldmap[0].Add("485-19-3", "Yes");
            if (mentalStatus != null && mentalStatus.Contains("4")) fieldmap[0].Add("485-19-4", "Yes");
            if (mentalStatus != null && mentalStatus.Contains("5")) fieldmap[0].Add("485-19-5", "Yes");
            if (mentalStatus != null && mentalStatus.Contains("6")) fieldmap[0].Add("485-19-6", "Yes");
            if (mentalStatus != null && mentalStatus.Contains("7")) fieldmap[0].Add("485-19-7", "Yes");
            if (mentalStatus != null && mentalStatus.Contains("8"))
            {
                fieldmap[0].Add("485-19-8", "Yes");
                if (data.ContainsKey("485MentalStatusOther")) fieldmap[0].Add("485-19-8-text", this.splitData(data["485MentalStatusOther"].Answer.Split(' '), " ", 150, 30, "19. Mental Status"));
            }
            if (data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Poor") fieldmap[0].Add("485-20-1", "Yes");
            if (data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Guarded") fieldmap[0].Add("485-20-2", "Yes");
            if (data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Fair") fieldmap[0].Add("485-20-3", "Yes");
            if (data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Good") fieldmap[0].Add("485-20-4", "Yes");
            if (data.ContainsKey("485Prognosis") && data["485Prognosis"].Answer == "Excellent") fieldmap[0].Add("485-20-5", "Yes");
            fieldmap[0].Add("485-21", this.splitData(data.ContainsKey("485Interventions") ? data["485Interventions"].Answer.Replace("&#38;", "&").Split(' ') : new String[0], " ", 755, 180, "21. Orders"));
            fieldmap[0].Add("485-22", this.splitData(data.ContainsKey("485Goals") ? data["485Goals"].Answer.Replace("&#38;", "&").Split(' ') : new String[0], " ", 755, 50, "22. Goals"));
            fieldmap[0].Add("485-23", planOfCare.SignatureText + " " + (data.ContainsKey("M0102PhysicianOrderedDate") ? data["M0102PhysicianOrderedDate"].Answer : ""));
            fieldmap[0].Add("485-24", (physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "") + (physician.NPI.IsNotNullOrEmpty() ? "     NPI: " + physician.NPI.ToString() + "\n" : "\n") + (physician.AddressFirstRow.IsNotNullOrEmpty() ? physician.AddressFirstRow + "\n" : "") + (physician.AddressSecondRow.IsNotNullOrEmpty() ? physician.AddressSecondRow + "\n" : "") + (physician.PhoneWorkFormatted.IsNotNullOrEmpty() ? "Phone: " + physician.PhoneWorkFormatted : "") + (physician.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + physician.FaxNumberFormatted : ""));
            fieldmap[0].Add("485-27", (planOfCare.PhysicianSignatureText.IsNotNullOrEmpty() ? planOfCare.PhysicianSignatureText : string.Empty) + " " + (planOfCare.ReceivedDateFormatted.IsNotNullOrEmpty() && planOfCare.PhysicianSignatureText.IsNotNullOrEmpty() ? planOfCare.ReceivedDateFormatted : string.Empty));
            this.SetFields(fieldmap);
            this.fieldmap487.Add("487-01", patient.MedicareNumber.IsNotNullOrEmpty() ? patient.MedicareNumber : patient.MedicaidNumber.IsNotNullOrEmpty() ? patient.MedicaidNumber.Clean() : string.Empty);
            this.fieldmap487.Add("487-02", patient.StartOfCareDateFormatted.IsNotNullOrEmpty() ? patient.StartOfCareDateFormatted : "");
            this.fieldmap487.Add("487-03a", planOfCare.EpisodeStart);
            this.fieldmap487.Add("487-03b", planOfCare.EpisodeEnd);
            this.fieldmap487.Add("487-04", patient.PatientIdNumber.IsNotNullOrEmpty() ? patient.PatientIdNumber : "");
            this.fieldmap487.Add("487-05", location.MedicareProviderNumber.IsNotNullOrEmpty() ? location.MedicareProviderNumber : "");
            this.fieldmap487.Add("487-06", (patient.LastName.IsNotNullOrEmpty() ? patient.LastName + ", " : "") + (patient.FirstName.IsNotNullOrEmpty() ? patient.FirstName : "") + (patient.MiddleInitial.IsNotNullOrEmpty() ? " " + patient.MiddleInitial + "." : ""));
            this.fieldmap487.Add("487-07", (agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() : ""));
            this.fieldmap487.Add("487-09", planOfCare.PhysicianSignatureText.IsNotNullOrEmpty() ? planOfCare.PhysicianSignatureText : string.Empty);
            this.fieldmap487.Add("487-10", planOfCare.PhysicianSignatureDate.Date > DateTime.MinValue.Date && planOfCare.PhysicianSignatureText.IsNotNullOrEmpty() ? planOfCare.PhysicianSignatureDate.ToString("MM/dd/yyyy") : string.Empty);
            this.fieldmap487.Add("487-11", planOfCare.SignatureText);
            this.fieldmap487.Add("487-12", (planOfCare.SignatureDate.Date > DateTime.MinValue.Date ? planOfCare.SignatureDate.ToString("MM/dd/yyyy") : string.Empty));
        }

        private string splitData(String[] content, String delimiter, float x, float y, String title487)
        {
            MemoryStream swapStream = new MemoryStream();
            AxxessDoc swapDoc = new AxxessDoc(new float[] { 0, 0, x, y });
            PdfWriter swapWriter = PdfWriter.GetInstance(swapDoc, swapStream);
            swapDoc.Open();
            PdfContentByte swap = swapWriter.DirectContent;
            string content485 = string.Empty, content487 = string.Empty;
            if (content != null && content.Count() > 0)
            {
                for (int i = 0; i < content.Count(); i++)
                {
                    ColumnText col = new ColumnText(swap);
                    col.SetSimpleColumn(new Phrase(content485 + (i == 0 ? "" : delimiter) + content[i]), 0, 0, x, y, 11, Element.ALIGN_LEFT);
                    if (col.Go(true) == ColumnText.NO_MORE_TEXT) content485 += (i == 0 ? "" : delimiter) + content[i];
                    else
                    {
                        for (int j = i; j < content.Count(); j++) content487 += delimiter + content[j];
                        break;
                    }
                }
                if (content487.Length > 0) this.cms487.Add(title487, content487.Substring(delimiter.Length));
            }
            return content485;
        }

        protected IElement[] get487content()
        {
            PdfPTable[] data = new PdfPTable[this.cms487.Count];
            int count = 0;
            foreach (KeyValuePair<String, String> loc in cms487)
            {
                data[count] = new PdfPTable(1);
                data[count].SplitLate = false;
                data[count].WidthPercentage = 100;
                PdfPCell cell = new PdfPCell(), headercell = new PdfPCell();
                headercell.BorderWidth = cell.BorderWidth = 0;
                cell.BorderWidthBottom = .5F;
                cell.Padding = headercell.Padding = 2;
                cell.PaddingBottom = 8;
                headercell.PaddingTop = headercell.PaddingBottom = cell.PaddingTop = 0;
                headercell.AddElement(new Paragraph(loc.Key, new Font(AxxessPdf.sans.BaseFont, 7.5F)));
                Paragraph content = new Paragraph(loc.Value, new Font(AxxessPdf.sans.BaseFont, 10F));
                content.SetLeading(0, 1);
                cell.AddElement(content);
                data[count].AddCell(headercell);
                data[count].AddCell(cell);
                data[count].HeaderRows = 1;
                count++;
            }
            return data;
        }

        protected List<Dictionary<String, String>> get487fieldmap()
        {
            List<Dictionary<String, String>> cms487 = new List<Dictionary<String, String>>();
            cms487.Add(this.fieldmap487);
            return cms487;
        }
    }
}