﻿namespace Axxess.AgencyManagement.App.iTextExtension
{
    using System;
    using System.IO;
    using System.Collections.Generic;

    using Axxess.OasisC.Domain;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    
    using iTextSharp.text;
    using iTextSharp.text.pdf;

    using XmlParsing;
    using ViewData;

    using Axxess.Api.Contracts;

    public class OasisAuditPdf : AxxessPdf
    {
        public OasisAuditPdf(OasisAudit data)
        {
            this.SetType(PdfDocs.OasisAudit);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 11F;
            this.SetFonts(fonts);
            float[] padding = new float[] { 2, 2, 8, 2 }, borders = new float[] { .2F, .2F, .2F, .2F }, none = new float[] { 0, 0, 0, 0 };
            AxxessTable[] content = new AxxessTable[] { new AxxessTable(1) };
            if (data == null || data.LogicalErrors == null)
            {
                AxxessCell contentcell = new AxxessCell(padding, borders);
                contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], "The OASIS Audit Service is down at this moment. Please try again later.", fonts[0], 12));
                content[0].AddCell(contentcell);
            }
            else
            {
                if (data.LogicalErrors.Count > 0)
                {
                    data.LogicalErrors.ForEach(d =>
                    {
                        AxxessCell contentcell = new AxxessCell(padding, borders);
                        contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], d.Header, fonts[0], 12));
                        contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], d.Reason, fonts[0], 12));
                        if (d.OasisItems.Count > 0)
                        {
                            d.OasisItems.ForEach(o =>
                            {
                                contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], o, fonts[0], 10));
                            });
                        }
                        content[0].AddCell(contentcell);
                    });
                }
                else
                {
                    AxxessCell contentcell = new AxxessCell(padding, borders);
                    contentcell.AddElement(new AxxessContentSection(string.Empty, fonts[1], "No Logical/Clinical Inconsistency Errors Found", fonts[0], 12));
                    content[0].AddCell(contentcell);
                }
            }
            this.SetContent(content);
            float[] margins = new float[] { 118, 28.3F, 28.3F, 28.3F };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = data.Agency.GetBranch(data.Patient != null ? data.Patient.AgencyLocationId : Guid.Empty);
            fieldmap[0].Add("agency", (
                data != null && data.Agency != null ?
                    (data.Agency.Name.IsNotNullOrEmpty() ? data.Agency.Name.ToTitleCase() + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap[0].Add("assessmenttypedate", string.Format("{0} assessed on {1}", data.Assessment.TypeDescription, data.Assessment.AssessmentDate.ToShortDateString().ToZeroFilled()));
            fieldmap[0].Add("auditvendor", data.Agency.OasisAuditVendor.ToEnum<OasisAuditVendors>(OasisAuditVendors.None).GetDescription());
            fieldmap[0].Add("patientname", data.Patient != null ? (data.Patient.LastName.IsNotNullOrEmpty() ? data.Patient.LastName.ToTitleCase() + ", " : "") + (data.Patient.FirstName.IsNotNullOrEmpty() ? data.Patient.FirstName.ToTitleCase() + " " : "") + (data.Patient.MiddleInitial.IsNotNullOrEmpty() ? data.Patient.MiddleInitial.ToUpper() + "\n" : "\n") : "");
            fieldmap[0].Add("mr", data.Patient != null && data.Patient.PatientIdNumber.IsNotNullOrEmpty() ? data.Patient.PatientIdNumber : string.Empty);
            this.SetFields(fieldmap);
        }
        protected override void AddWatermark(PdfContentByte swap) {
            string ImagePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppSettings.HomeHealthGoldLogo);
            using (Stream ImageStream = new FileStream(ImagePath, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                Image image = Image.GetInstance(ImageStream);
                image.SetAbsolutePosition(275, 16);
                swap.AddImage(image);
            }
        }

    }
}
