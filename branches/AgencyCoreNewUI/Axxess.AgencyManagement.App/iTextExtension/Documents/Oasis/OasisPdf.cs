﻿namespace Axxess.AgencyManagement.App.iTextExtension {
    using System;
    using System.Collections.Generic;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.Core.Extension;
    using iTextSharp.text;
    using Axxess.OasisC.Domain;
    using XmlParsing;
    class OasisPdf : AxxessPdf {
        private OasisXml xml;
        private static String[] DocumentType = new String[] { "START OF CARE", string.Empty, "RESUMPTION OF CARE", "RECERTIFICATION", "FOLLOW-UP", "TRANSFER NOT DISCHARGED", "TRANSFER DISCHARGE", "DEATH AT HOME", "DISCHARGE FROM AGENCY" };
        public OasisPdf(Assessment data) {
            this.CreatePdf(data);
        }
        public OasisPdf(Assessment data, string Discipline) {
            data.Discipline = Discipline;
            this.CreatePdf(data);
        }
        private void CreatePdf(Assessment data) {
            var agency = data.AgencyData != null ? data.AgencyData.ToObject<Agency>() : new Agency();
            var patient = data.PatientData != null ? data.PatientData.ToObject<Patient>() : new Patient();
            this.xml = new OasisXml(data);
            this.IsOasis = this.xml.GetOasisGeneration() == "0" ? false : true;
            this.SetType(PdfDocs.Oasis);
            List<Font> fonts = new List<Font>();
            fonts.Add(AxxessPdf.sans);
            fonts.Add(AxxessPdf.sansbold);
            fonts.Add(AxxessPdf.sansbold);
            for (int i = 0; i < fonts.Count; i++) fonts[i].Size = 10F;
            this.SetFonts(fonts);
            AxxessTable[] content = new AxxessTable[this.xml.SectionCount()];
            int count = 0;
            foreach (XmlPrintSection tab in this.xml.GetLayout()) {
                content[count] = new AxxessTable(1);
                AxxessCell sectionCell = new AxxessCell(new float[] { 0, 0, 0, 0 }, new float[] { .5F, 0, 0, 0 }), tabTitleCell = new AxxessCell(new float[] { 0, 0, 4, 0 }, new float[] { .5F, 0, 0, 0 });
                tabTitleCell.AddElement(new AxxessTitle(tab.Label, AxxessPdf.sansbold));
                foreach (XmlPrintSection section in tab.Subsection) {
                    AxxessContentSection contentSection = new AxxessContentSection(section, fonts, true, 8.5F, this.IsOasis);
                    sectionCell.AddElement(contentSection);
                }
                if (tab.Label.IsNotNullOrEmpty()) content[count].AddCell(tabTitleCell);
                content[count].AddCell(sectionCell);
                content[count].HeaderRows = 1;
                count++;
            }
            this.SetContent(content);
            float[] margins = new float[] { 100F, 28.5F, 25, 28.5F };
            this.SetMargins(margins);
            List<Dictionary<String, String>> fieldmap = new List<Dictionary<string, string>>();
            fieldmap.Add(new Dictionary<String, String>() { });
            var location = agency.GetBranch(patient != null ? patient.AgencyLocationId : Guid.Empty);
            if (location == null) location = agency.GetMainOffice();
            fieldmap[0].Add("agency", (
                data != null && agency != null ?
                    (agency.Name.IsNotNullOrEmpty() ? agency.Name.ToTitleCase() + "\n" : String.Empty) +
                    (location != null ?
                        (location.AddressLine1.IsNotNullOrEmpty() ? location.AddressLine1.ToTitleCase() : String.Empty) +
                        (location.AddressLine2.IsNotNullOrEmpty() ? location.AddressLine2.ToTitleCase() + "\n" : "\n") +
                        (location.AddressCity.IsNotNullOrEmpty() ? location.AddressCity.ToTitleCase() + ", " : String.Empty) +
                        (location.AddressStateCode.IsNotNullOrEmpty() ? location.AddressStateCode.ToString().ToUpper() + "  " : String.Empty) +
                        (location.AddressZipCode.IsNotNullOrEmpty() ? location.AddressZipCode : String.Empty) +
                        (location.PhoneWorkFormatted.IsNotNullOrEmpty() ? "\nPhone: " + location.PhoneWorkFormatted : String.Empty) +
                        (location.FaxNumberFormatted.IsNotNullOrEmpty() ? " | Fax: " + location.FaxNumberFormatted : String.Empty)
                    : String.Empty)
                : String.Empty));
            fieldmap.Add(new Dictionary<String, String>() { });
            fieldmap[1].Add("patientname", patient != null && patient.DisplayName.Trim() != "," ? patient.DisplayName : string.Empty);
            fieldmap[1].Add("AssessmentType", (this.IsOasis ? "OASIS-" + this.xml.GetOasisGeneration() : "NON-OASIS") + this.xml.GetDiscipline() + "\n" + OasisPdf.DocumentType[this.xml.GetAssessmentType()]);
            this.SetFields(fieldmap);
        }
    }
}