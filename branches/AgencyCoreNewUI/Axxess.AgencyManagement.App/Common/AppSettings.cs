﻿namespace Axxess.AgencyManagement.App {
    using System;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    public static class AppSettings {
        private static IWebConfiguration configuration = Container.Resolve<IWebConfiguration>();
        public static string IdentifyAgencyCookie { get { return configuration.AppSettings["IdentifyAgencyCookie"]; } }
        public static string RememberMeCookie { get { return configuration.AppSettings["RememberMeCookie"]; } }
        public static int RememberMeForTheseDays { get { return configuration.AppSettings["RememberMeForTheseDays"].ToInteger(); } }
        public static string AuthenticationType { get { return configuration.AppSettings["AuthenticationType"]; } }
        public static string GoogleAPIKey { get { return configuration.AppSettings["GoogleApiKey"]; } }
        public static double FormsAuthenticationTimeoutInMinutes { get { return configuration.AppSettings["FormsAuthenticationTimeoutInMinutes"].ToDouble(); } }
        public static bool UsePersistentCookies { get { return configuration.AppSettings["UsePersistentCookies"].ToBoolean(); } }
        public static bool UseMinifiedJs { get { return configuration.AppSettings["UseMinifiedJs"].ToBoolean(); } }
        public static string NewsFeedUrl { get { return configuration.AppSettings["NewsFeedUrl"]; } }
        public static string AllowedImageExtensions { get { return configuration.AppSettings["AllowedImageExtensions"]; } }
        public static string ANSIGeneratorUrl { get { return configuration.AppSettings["ANSIGeneratorUrl"]; } }
        public static string PatientEligibilityUrl { get { return configuration.AppSettings["PatientEligibilityUrl"]; } }
        public static string PdfBasePath { get { return configuration.AppSettings["PdfBasePath"]; } }
        public static string XmlBasePath { get { return configuration.AppSettings["XmlBasePath"]; } }
        public static string IncidentAccidentReportPdf { get { return configuration.AppSettings["IncidentAccidentReportPdf"]; } }
        public static string IncidentAccidentReportXml { get { return configuration.AppSettings["IncidentAccidentReportXml"]; } }
        public static string InfectionReportPdf { get { return configuration.AppSettings["InfectionReportPdf"]; } }
        public static string InfectionReportXml { get { return configuration.AppSettings["InfectionReportXml"]; } }
        public static string BillingClaimsPdf { get { return configuration.AppSettings["BillingClaimsPdf"]; } }
        public static string BillingFinalPdf { get { return configuration.AppSettings["BillingFinalPdf"]; } }
        public static string BillingRapPdf { get { return configuration.AppSettings["BillingRapPdf"]; } }
        public static string HCFA1500Pdf { get { return configuration.AppSettings["HCFA1500Pdf"]; } }
        public static string RemittancesPdf { get { return configuration.AppSettings["RemittancesPdf"]; } }
        public static string RemittancePdf { get { return configuration.AppSettings["RemittancePdf"]; } }
        public static string UB04Pdf { get { return configuration.AppSettings["UB04Pdf"]; } }
        public static string MessagePdf { get { return configuration.AppSettings["MessagePdf"]; } }
        public static string BillingSuppliesPdf { get { return configuration.AppSettings["BillingSuppliesPdf"]; } }
        public static string HCFA485Pdf { get { return configuration.AppSettings["HCFA485Pdf"]; } }
        public static string HCFA487Pdf { get { return configuration.AppSettings["HCFA487Pdf"]; } }
        public static string OasisPdf { get { return configuration.AppSettings["OasisPdf"]; } }
        public static string OasisXml { get { return configuration.AppSettings["OasisXml"]; } }
        public static string OasisAuditPdf { get { return configuration.AppSettings["OasisAuditPdf"]; } }
        public static string OasisProfilePdf { get { return configuration.AppSettings["OasisProfilePdf"]; } }
        public static string OasisProfileXml { get { return configuration.AppSettings["OasisProfileXml"]; } }
        public static string AllergyProfilePdf { get { return configuration.AppSettings["AllergyProfilePdf"]; } }
        public static string AuthorizationPdf { get { return configuration.AppSettings["AuthorizationPdf"]; } }
        public static string CommunicationNotePdf { get { return configuration.AppSettings["CommunicationNotePdf"]; } }
        public static string DrugDrugInteractionPdf { get { return configuration.AppSettings["DrugDrugInteractionPdf"]; } }
        public static string FaceToFaceEncounterPdf { get { return configuration.AppSettings["FaceToFaceEncounterPdf"]; } }
        public static string FaceToFaceEncounterXml { get { return configuration.AppSettings["FaceToFaceEncounterXml"]; } }
        public static string HospitalizationLogPdf { get { return configuration.AppSettings["HospitalizationLogPdf"]; } }
        public static string HospitalizationLogXml { get { return configuration.AppSettings["HospitalizationLogXml"]; } }
        public static string MedicareEligibilityReportPdf { get { return configuration.AppSettings["MedicareEligibilityReportPdf"]; } }
        public static string MedicationProfilePdf { get { return configuration.AppSettings["MedicationProfilePdf"]; } }
        public static string PatientProfilePdf { get { return configuration.AppSettings["PatientProfilePdf"]; } }
        public static string PhysicianOrderPdf { get { return configuration.AppSettings["PhysicianOrderPdf"]; } }
        public static string ReferralPdf { get { return configuration.AppSettings["ReferralPdf"]; } }
        public static string ReferralXml { get { return configuration.AppSettings["ReferralXml"]; } }
        public static string TriageClassificationPdf { get { return configuration.AppSettings["TriageClassificationPdf"]; } }
        public static string PayrollDetailPdf { get { return configuration.AppSettings["PayrollDetailPdf"]; } }
        public static string PayrollSummaryPdf { get { return configuration.AppSettings["PayrollSummaryPdf"]; } }
        public static string AnnualCalendarPdf { get { return configuration.AppSettings["AnnualCalendarPdf"]; } }
        public static string MasterCalendarPdf { get { return configuration.AppSettings["MasterCalendarPdf"]; } }
        public static string MonthlyCalendarPdf { get { return configuration.AppSettings["MonthlyCalendarPdf"]; } }
        public static string HHAideCarePlanPdf { get { return configuration.AppSettings["HHAideCarePlanPdf"]; } }
        public static string HHAideCarePlanXml { get { return configuration.AppSettings["HHAideCarePlanXml"]; } }
        public static string HHAideSupervisoryVisitPdf { get { return configuration.AppSettings["HHAideSupervisoryVisitPdf"]; } }
        public static string HHAideSupervisoryVisitXml { get { return configuration.AppSettings["HHAideSupervisoryVisitXml"]; } }
        public static string HHAideVisitPdf { get { return configuration.AppSettings["HHAideVisitPdf"]; } }
        public static string HHAideVisitXml { get { return configuration.AppSettings["HHAideVisitXml"]; } }
        public static string MissedVisitPdf { get { return configuration.AppSettings["MissedVisitPdf"]; } }
        public static string DriverOrTransportationNotePdf { get { return configuration.AppSettings["DriverOrTransportationNotePdf"]; } }
        public static string DriverOrTransportationNoteXml { get { return configuration.AppSettings["DriverOrTransportationNoteXml"]; } }
        public static string MSWEvaluationAssessmentPdf { get { return configuration.AppSettings["MSWEvaluationAssessmentPdf"]; } }
        public static string MSWEvaluationAssessmentXml { get { return configuration.AppSettings["MSWEvaluationAssessmentXml"]; } }
        public static string MSWProgressNotePdf { get { return configuration.AppSettings["MSWProgressNotePdf"]; } }
        public static string MSWProgressNoteXml { get { return configuration.AppSettings["MSWProgressNoteXml"]; } }
        public static string MSWVisitPdf { get { return configuration.AppSettings["MSWVisitPdf"]; } }
        public static string MSWVisitXml { get { return configuration.AppSettings["MSWVisitXml"]; } }
        public static string DischargeSummaryPdf { get { return configuration.AppSettings["DischargeSummaryPdf"]; } }
        public static string DischargeSummaryXml { get { return configuration.AppSettings["DischargeSummaryXml"]; } }
        public static string LVNSupervisoryVisitPdf { get { return configuration.AppSettings["LVNSupervisoryVisitPdf"]; } }
        public static string LVNSupervisoryVisitXml { get { return configuration.AppSettings["LVNSupervisoryVisitXml"]; } }
        public static string SixtyDaySummaryPdf { get { return configuration.AppSettings["SixtyDaySummaryPdf"]; } }
        public static string SixtyDaySummaryXml { get { return configuration.AppSettings["SixtyDaySummaryXml"]; } }
        public static string SkilledNurseVisitPdf { get { return configuration.AppSettings["SkilledNurseVisitPdf"]; } }
        public static string SkilledNurseVisitXml { get { return configuration.AppSettings["SkilledNurseVisitXml"]; } }
        public static string SNDiabeticDailyVisitPdf { get { return configuration.AppSettings["SNDiabeticDailyVisitPdf"]; } }
        public static string SNDiabeticDailyVisitXml { get { return configuration.AppSettings["SNDiabeticDailyVisitXml"]; } }
        public static string TransferSummaryPdf { get { return configuration.AppSettings["TransferSummaryPdf"]; } }
        public static string TransferSummaryXml { get { return configuration.AppSettings["TransferSummaryXml"]; } }
        public static string WoundCarePdf { get { return configuration.AppSettings["WoundCarePdf"]; } }
        public static string WoundCareXml { get { return configuration.AppSettings["WoundCareXml"]; } }
        public static string PASCarePlanPdf { get { return configuration.AppSettings["PASCarePlanPdf"]; } }
        public static string PASCarePlanXml { get { return configuration.AppSettings["PASCarePlanXml"]; } }
        public static string PASVisitPdf { get { return configuration.AppSettings["PASVisitPdf"]; } }
        public static string PASVisitXml { get { return configuration.AppSettings["PASVisitXml"]; } }
        public static string OTEvaluationPdf { get { return configuration.AppSettings["OTEvaluationPdf"]; } }
        public static string OTEvaluationXml { get { return configuration.AppSettings["OTEvaluationXml"]; } }
        public static string OTVisitPdf { get { return configuration.AppSettings["OTVisitPdf"]; } }
        public static string OTVisitXml { get { return configuration.AppSettings["OTVisitXml"]; } }
        public static string PTDischargePdf { get { return configuration.AppSettings["PTDischargePdf"]; } }
        public static string PTDischargeXml { get { return configuration.AppSettings["PTDischargeXml"]; } }
        public static string PTEvaluationPdf { get { return configuration.AppSettings["PTEvaluationPdf"]; } }
        public static string PTEvaluationXml { get { return configuration.AppSettings["PTEvaluationXml"]; } }
        public static string PTVisitPdf { get { return configuration.AppSettings["PTVisitPdf"]; } }
        public static string PTVisitXml { get { return configuration.AppSettings["PTVisitXml"]; } }
        public static string STEvaluationPdf { get { return configuration.AppSettings["STEvaluationPdf"]; } }
        public static string STEvaluationXml { get { return configuration.AppSettings["STEvaluationXml"]; } }
        public static string STVisitPdf { get { return configuration.AppSettings["STVisitPdf"]; } }
        public static string STVisitXml { get { return configuration.AppSettings["STVisitXml"]; } }
        public static string UAPInsulinPrepAdminVisitPdf { get { return configuration.AppSettings["UAPInsulinPrepAdminVisitPdf"]; } }
        public static string UAPInsulinPrepAdminVisitXml { get { return configuration.AppSettings["UAPInsulinPrepAdminVisitXml"]; } }
        public static string UAPWoundCareVisitPdf { get { return configuration.AppSettings["UAPWoundCareVisitPdf"]; } }
        public static string UAPWoundCareVisitXml { get { return configuration.AppSettings["UAPWoundCareVisitXml"]; } }
        public static string HomeHealthGoldLogo { get { return configuration.AppSettings["HomeHealthGoldLogo"]; } }
        public static string BrowserCompatibilityCheck { get { return configuration.AppSettings["BrowserCompatibilityCheck"]; } }
        public static string WeatherImageUriFormat { get { return configuration.AppSettings["WeatherImageUriFormat"]; } }
        public static string WeatherForecastImageUriFormat { get { return configuration.AppSettings["WeatherForecastImageUriFormat"]; } }
        public static string WeatherImageThumbnailUriFormat { get { return configuration.AppSettings["WeatherImageThumbnailUriFormat"]; } }
        public static string YahooWeatherUri { get { return configuration.AppSettings["YahooWeatherUri"]; } }
        public static string GetRemoteContent { get { return configuration.AppSettings["GetRemoteContent"]; } }
        public static bool IsSingleUserMode { get { return configuration.AppSettings["IsSingleUserMode"].ToBoolean(); } }
        public static string RemittanceSchedulerUrl { get { return configuration.AppSettings["RemittanceSchedulerUrl"]; } }
    }
}
