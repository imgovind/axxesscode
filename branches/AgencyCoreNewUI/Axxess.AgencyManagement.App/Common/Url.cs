﻿namespace Axxess.AgencyManagement.App.Common
{
    using System;


    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Enums;

    public static class Url
    {
        public static void Set(ScheduleEvent scheduleEvent, bool addReassignLink, bool usePrintIcon)
        {
            if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0 && !scheduleEvent.EpisodeId.IsEmpty() && !scheduleEvent.PatientId.IsEmpty() && !scheduleEvent.EventId.IsEmpty())
            {
                string onclick = string.Empty;
                string reopenUrl = string.Empty;
                string printUrl = Print(scheduleEvent, usePrintIcon);
                string detailUrl = string.Format("<a class=\"link\" onclick=\"Schedule.Details('{0}', '{1}', '{2}');return false\">Details</a>", scheduleEvent.EventId, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
                string deleteUrl = string.Format("<a class=\"link\" onclick=\"Schedule.Delete('{0}','{1}','{2}','{3}');return false\" >Delete</a>", scheduleEvent.EventId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.UserId);
                string reassignUrl = string.Format("<a class=\"link\" onclick=\"Schedule.Reassign('{0}','{1}','{2}','{3}');return false\">Reassign</a>", scheduleEvent.EventId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.UserId);
                string oasisProfileUrl = string.Empty;
                DisciplineTasks task = (DisciplineTasks)scheduleEvent.DisciplineTask;

                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                        if (ScheduleStatusFatory.OASISCompleted().Contains(scheduleEvent.Status))
                        {
                            oasisProfileUrl = "<a onclick=\"OASIS.Profile.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.DisciplineTaskName + "');return false\"><span class=\"img icon money\"></span></a>";
                        }
                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.StartOfCare.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;


                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                        if (ScheduleStatusFatory.OASISCompleted().Contains(scheduleEvent.Status))
                        {
                            oasisProfileUrl = "<a onclick=\"OASIS.Profile.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.DisciplineTaskName + "');return false\"><span class=\"img icon money\"></span></a>";
                        }
                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.ResumptionOfCare.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                        if (ScheduleStatusFatory.OASISCompleted().Contains(scheduleEvent.Status))
                        {
                            oasisProfileUrl = "<a onclick=\"OASIS.Profile.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.DisciplineTaskName + "');return false\"><span class=\"img icon money\"></span></a>";
                        }
                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.Recertification.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.FollowUp.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:

                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.TransferInPatientNotDischarged.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.OASISCTransferDischarge:
                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.TransferInPatientDischarged.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.DischargeFromAgencyDeath.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.DischargeFromAgency.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.NonOASISDischarge:
                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.NonOasisDischarge.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.NonOASISStartofCare:
                    case DisciplineTasks.SNAssessment:
                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.NonOasisStartOfCare.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;

                    case DisciplineTasks.NonOASISRecertification:
                    case DisciplineTasks.SNAssessmentRecert:
                        onclick = string.Format("OASIS.Assessment.Load('{0}','{1}','{2}');", scheduleEvent.EventId, scheduleEvent.PatientId, AssessmentType.NonOasisRecertification.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"Patient.Order.Edit('{0}');return false\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        scheduleEvent.Url = string.Format("{0}", scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HCFA485:
                        onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        onclick = string.Format("UserInterface.ShowEditPlanofCareStandAlone('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.NonOasisHCFA485:
                        onclick = string.Format("UserInterface.ShowEditPlanofCare('{0}','{1}','{2}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.CommunicationNote:
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"Patient.CommunicationNote.Edit('{0}','{1}');return false\">{2}</a>", scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"Agency.Incident.Edit('{0}');return false\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    case DisciplineTasks.InfectionReport:
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"Agency.Infection.Edit('{0}');return false\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                        break;
                    default:
                        onclick = string.Format("Schedule.Visit.Shared.Load('{0}','{1}','{2}','{3}');", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, task.ToString());
                        scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"{0}return false\">{1}</a>", onclick, scheduleEvent.DisciplineTaskName);
                        break;
                }

                if (Current.HasRight(Permissions.EditTaskDetails)) scheduleEvent.ActionUrl = detailUrl;
                if (Current.HasRight(Permissions.DeleteTasks))
                {
                    if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + deleteUrl;
                    else scheduleEvent.ActionUrl = deleteUrl;
                }
                if (addReassignLink && Current.HasRight(Permissions.ScheduleVisits))
                {
                    if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + reassignUrl;
                    else scheduleEvent.ActionUrl = reassignUrl;
                }
                scheduleEvent.PrintUrl = printUrl;
                if (ScheduleStatusFatory.OnAndAfterQAStatus().Exists(s=>s==scheduleEvent.Status))
                {
                    scheduleEvent.ActionUrl = string.Empty;
                    if (Current.HasRight(Permissions.EditTaskDetails)) scheduleEvent.ActionUrl = detailUrl;
                    scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                }
                if (ScheduleStatusFatory.AfterQAStatus().Exists(s=>s==scheduleEvent.Status))
                {
                    if (Current.HasRight(Permissions.EditTaskDetails))
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty() && !scheduleEvent.ActionUrl.Contains(detailUrl)) scheduleEvent.ActionUrl += " | " + detailUrl;
                        else scheduleEvent.ActionUrl = detailUrl;
                    }
                    if (Current.HasRight(Permissions.DeleteTasks))
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + deleteUrl;
                        else scheduleEvent.ActionUrl = deleteUrl;
                    }
                    if (Current.HasRight(Permissions.ReopenDocuments))
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty())
                        {
                            reopenUrl = string.Format("<a class=\"link\" onclick=\"Schedule.Reopen('{0}','{1}','{2}');{3}return false\">Reopen Task</a>", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, onclick);
                            scheduleEvent.ActionUrl += " | " + reopenUrl;
                        }
                        else scheduleEvent.ActionUrl = reopenUrl;
                    }
                    if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.MedicareEligibilityReport) scheduleEvent.ActionUrl = string.Empty;
                }
                if (scheduleEvent.IsMissedVisit)
                {
                    scheduleEvent.Url = string.Format("<a class=\"link\" onclick=\"Schedule.MissedVisit.New('{0}');return false\">{1}</a>", scheduleEvent.EventId, scheduleEvent.DisciplineTaskName);
                    if (Current.HasRight(Permissions.EditTaskDetails)) scheduleEvent.ActionUrl = detailUrl;
                    if (Current.HasRight(Permissions.DeleteTasks))
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + deleteUrl;
                        else scheduleEvent.ActionUrl = deleteUrl;
                    }
                }

                if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
                {
                }
                else if (Current.IsClinicianOrHHA)
                {
                    var patient = Container.Resolve<IAgencyManagementDataProvider>().PatientRepository.GetPatientOnly(scheduleEvent.PatientId, Current.AgencyId);
                    if (Current.UserId != scheduleEvent.UserId && (patient != null && !patient.UserId.IsEmpty() && patient.UserId != Current.UserId))
                    {
                        scheduleEvent.PrintUrl = string.Empty;
                        scheduleEvent.ActionUrl = string.Empty;
                        scheduleEvent.Status = 0;
                        scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                    }
                }
                else
                {
                    scheduleEvent.PrintUrl = string.Empty;
                    scheduleEvent.ActionUrl = string.Empty;
                    scheduleEvent.Status = 0;
                }
                if (scheduleEvent.IsOrphaned)
                {
                    scheduleEvent.Url = scheduleEvent.DisciplineTaskName;
                    if (Current.HasRight(Permissions.EditTaskDetails)) scheduleEvent.ActionUrl = detailUrl;
                    if (Current.HasRight(Permissions.DeleteTasks))
                    {
                        if (scheduleEvent.ActionUrl.IsNotNullOrEmpty()) scheduleEvent.ActionUrl += " | " + deleteUrl;
                        else scheduleEvent.ActionUrl = deleteUrl;
                    }
                    if (scheduleEvent.UserName.IsEqual("Axxess")) scheduleEvent.ActionUrl = string.Empty;
                }
                if (Current.HasRight(Permissions.ViewHHRGCalculations)) scheduleEvent.OasisProfileUrl = oasisProfileUrl;
            }
        }

        public static string Print(Guid episodeId, Guid patientId, Guid eventId, DisciplineTasks task, int status, bool usePrintIcon)
        {
            var scheduleEvent = new ScheduleEvent
            {
                EpisodeId = episodeId,
                PatientId = patientId,
                EventId = eventId,
                DisciplineTask = (int)task,
                Status = status
            };

            return Print(scheduleEvent, usePrintIcon);
        }

        public static string Print(ScheduleEvent scheduleEvent, bool usePrintIcon)
        {
            string printUrl = string.Empty;
            string linkText = usePrintIcon ? "<span class=\"img icon print\"></span>" : scheduleEvent.DisciplineTaskName;
            string cssClass = usePrintIcon ? "" : " class=\"link\"";
            if (scheduleEvent.IsMissedVisit) printUrl = "<a" + cssClass + " onclick=\"Schedule.MissedVisit.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.PatientId + "');return false\">" + linkText + "</a>";
            else
            {
                var task = (DisciplineTasks)scheduleEvent.DisciplineTask;
                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                    case DisciplineTasks.NonOASISStartofCare:

                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:

                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:

                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                    case DisciplineTasks.NonOASISRecertification:

                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:

                    case DisciplineTasks.OASISCTransferDischarge:

                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:

                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.NonOASISDischarge:

                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNAssessmentRecert:
                        printUrl = "<a" + cssClass + " onclick=\"OASIS.Assessment.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "', " + (!usePrintIcon).ToString().ToLower() + ");return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                    case DisciplineTasks.HCFA485StandAlone:
                        printUrl = "<a" + cssClass + " onclick=\"OASIS.PlanOfCare.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "', " + (!usePrintIcon).ToString().ToLower() + ");return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        printUrl = "<a" + cssClass + " onclick=\"Patient.FaceToFaceEncounter.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.PatientId + "');return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        printUrl = "<a" + cssClass + " onclick=\"Patient.Order.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "', " + (!usePrintIcon).ToString().ToLower() + ");return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CommunicationNote:
                        printUrl = "<a" + cssClass + " onclick=\"Patient.CommunicationNote.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.PatientId + "', '" + scheduleEvent.EpisodeId + "', " + (!usePrintIcon).ToString().ToLower() + ");return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.InfectionReport:
                        printUrl = "<a" + cssClass + " onclick=\"Agency.Infection.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.PatientId + "', '" + scheduleEvent.EpisodeId + "', " + (!usePrintIcon).ToString().ToLower() + ");return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        printUrl = "<a" + cssClass + " onclick=\"Agency.Incident.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.PatientId + "', '" + scheduleEvent.EpisodeId + "', " + (!usePrintIcon).ToString().ToLower() + ");return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.MedicareEligibilityReport:
                        printUrl = "<a" + cssClass + " onclick=\"Patient.Eligibility.Print('" + scheduleEvent.EventId + "', '" + scheduleEvent.PatientId + "');return false\">" + (usePrintIcon ? "<span class='img icon print'></span>" : linkText) + "</a>";
                        break;
                    default:
                        printUrl = "<a" + cssClass + " onclick=\"Schedule.Visit.Shared.Print('" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "', '" + scheduleEvent.EventId + "', " + (!usePrintIcon).ToString().ToLower() + ");return false\">" + linkText + "</a>";
                        break;
                }
            }
            return printUrl;
        }

        public static string Download(ScheduleEvent scheduleEvent, bool usePrintIcon)
        {
            string printUrl = string.Empty;
            string linkText = usePrintIcon ? "<span class=\"img icon print\"></span>" : scheduleEvent.DisciplineTaskName;
            string cssClass = usePrintIcon ? "" : " class=\"link\"";
            if (scheduleEvent.IsMissedVisit) printUrl = "<a" + cssClass + " onclick=\"Schedule.MissedVisit.Download('" + scheduleEvent.EventId + "', '" + scheduleEvent.PatientId + "');return false\">" + linkText + "</a>";
            else
            {
                var task = (DisciplineTasks)scheduleEvent.DisciplineTask;
                switch (task)
                {
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.NonOASISStartofCare:
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                    case DisciplineTasks.SNAssessmentRecert:
                    case DisciplineTasks.NonOASISRecertification:
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDeathOT:
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.NonOASISDischarge:
                        printUrl = "<a" + cssClass + " onclick=\"OASIS.Assessment." + task.ToString() + ".Download('" + scheduleEvent.EventId + "', '" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "');return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                    case DisciplineTasks.HCFA485StandAlone:
                        printUrl = "<a" + cssClass + " onclick=\"OASIS.PlanOfCare.Download('" + scheduleEvent.EventId + "', '" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "');return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        printUrl = "<a" + cssClass + " onclick=\"Patient.FaceToFaceEncounter.Download('" + scheduleEvent.EventId + "', '" + scheduleEvent.PatientId + "');return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        printUrl = "<a" + cssClass + " onclick=\"Patient.Order.Download('" + scheduleEvent.EventId + "', '" + scheduleEvent.PatientId + "');return false\">" + linkText + "</a>";
                        break;
                    case DisciplineTasks.CommunicationNote:
                        printUrl = "<a" + cssClass + " onclick=\"Patient.CommunicationNote.Download('" + scheduleEvent.EventId + "', '" + scheduleEvent.PatientId + "');return false\">" + linkText + "</a>";
                        break;
                    default:
                        printUrl = "<a" + cssClass + " onclick=\"Schedule.Visit." + task.ToString() + ".Download('" + scheduleEvent.EventId + "', '" + scheduleEvent.EpisodeId + "', '" + scheduleEvent.PatientId + "');return false\">" + linkText + "</a>";
                        break;
                }
            }
            return printUrl;
        }
    }
}