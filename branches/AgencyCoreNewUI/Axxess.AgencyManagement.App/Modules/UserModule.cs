﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class UserModule : Module
    {
        public override string Name
        {
            get { return "User"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region Calendar
            routes.MapRoute(
                "CalendarNavigate",
                "User/Calendar/Navigate",
                new { controller = this.Name, action = "CalendarNavigate", id = UrlParameter.Optional });
            routes.MapRoute(
                "CalendarPdf",
                "User/Calendar/Pdf",
                new { controller = this.Name, action = "CalendarPdf", id = UrlParameter.Optional });
            routes.MapRoute(
                "CalendarGrid",
                "User/Calendar/Grid",
                new { controller = this.Name, action = "CalendarGrid", id = UrlParameter.Optional });
            #endregion

            #region License
            routes.MapRoute(
                "LicenseNew",
                "User/License/New",
                new { controller = this.Name, action = "LicenseNew", id = UrlParameter.Optional });
            routes.MapRoute(
                "LicenseCreate",
                "User/License/Create",
                new { controller = this.Name, action = "LicenseCreate", id = UrlParameter.Optional });
            routes.MapRoute(
                "LicenseGrid",
                "User/License/Grid",
                new { controller = this.Name, action = "LicenseGrid", id = UrlParameter.Optional });
            routes.MapRoute(
                "LicenseEdit",
                "User/License/Edit",
                new { controller = this.Name, action = "LicenseEdit", id = UrlParameter.Optional });
            routes.MapRoute(
                "LicenseUpdate",
                "User/License/Update",
                new { controller = this.Name, action = "LicenseUpdate", id = UrlParameter.Optional });
            routes.MapRoute(
                "LicenseDelete",
                "User/License/Delete",
                new { controller = this.Name, action = "LicenseDelete", id = UrlParameter.Optional });
            #endregion

            #region LicenseItem
            routes.MapRoute(
                "LicenseItemNew",
                "User/LicenseItem/New",
                new { controller = this.Name, action = "LicenseItemNew", id = UrlParameter.Optional });
            routes.MapRoute(
                "LicenseItemAdd",
                "User/LicenseItem/Add",
                new { controller = this.Name, action = "LicenseItemAdd", id = UrlParameter.Optional });
            routes.MapRoute(
                "LicenseItemManager",
                "User/LicenseItem/Manager",
                new { controller = this.Name, action = "LicenseItemManager", id = UrlParameter.Optional });
            routes.MapRoute(
                "LicenseItemList",
                "User/LicenseItem/List",
                new { controller = this.Name, action = "LicenseItemList", id = UrlParameter.Optional });
            routes.MapRoute(
                "LicenseItemEdit",
                "User/LicenseItem/Edit",
                new { controller = this.Name, action = "LicenseItemEdit", id = UrlParameter.Optional });
            routes.MapRoute(
                "LicenseItemUpdate",
                "User/LicenseItem/Update",
                new { controller = this.Name, action = "LicenseItemUpdate", id = UrlParameter.Optional });
            #endregion
            
            #region PayRate
            routes.MapRoute(
                "PayRateNew",
                "User/PayRate/New",
                new { controller = this.Name, action = "PayRateNew", id = UrlParameter.Optional });
            routes.MapRoute(
                "PayRateCreate",
                "User/PayRate/Create",
                new { controller = this.Name, action = "PayRateCreate", id = UrlParameter.Optional });
            routes.MapRoute(
                "PayRateGrid",
                "User/PayRate/Grid",
                new { controller = this.Name, action = "PayRateGrid", id = UrlParameter.Optional });
            routes.MapRoute(
                "PayRateEdit",
                "User/PayRate/Edit",
                new { controller = this.Name, action = "PayRateEdit", id = UrlParameter.Optional });
            routes.MapRoute(
                "PayRateUpdate",
                "User/PayRate/Update",
                new { controller = this.Name, action = "PayRateUpdate", id = UrlParameter.Optional });
            routes.MapRoute(
                "PayRateDelete",
                "User/PayRate/Delete",
                new { controller = this.Name, action = "PayRateDelete", id = UrlParameter.Optional });
            #endregion

            #region Profile
            routes.MapRoute(
                "ProfileEdit",
                "User/Profile/Edit",
                new { controller = this.Name, action = "ProfileEdit", id = UrlParameter.Optional });
            routes.MapRoute(
                "ProfileUpdate",
                "User/Profile/Update",
                new { controller = this.Name, action = "ProfileUpdate", id = UrlParameter.Optional });
            #endregion

            #region Schedule
            routes.MapRoute(
                "ScheduleList",
                "User/Schedule/List",
                new { controller = this.Name, action = "ScheduleList", id = UrlParameter.Optional });
            routes.MapRoute(
                "ScheduleGrid",
                "User/Schedule/Grid",
                new { controller = this.Name, action = "ScheduleGrid", id = UrlParameter.Optional });
            routes.MapRoute(
                "ScheduleWidget",
                "User/Schedule/Widget",
                new { controller = this.Name, action = "ScheduleWidget", id = UrlParameter.Optional });
            routes.MapRoute(
                "ScheduleExport",
                "User/Schedule/Export",
                new { controller = "Export", action = "UserSchedule", id = UrlParameter.Optional });
            #endregion

            #region Signature
            routes.MapRoute(
                "ForgotSignature",
                "User/Signature/Reset",
                new { controller = this.Name, action = "SignatureReset", id = UrlParameter.Optional });
            routes.MapRoute(
                "EmailSignature",
                "User/Signature/Email",
                new { controller = this.Name, action = "SignatureEmail", id = UrlParameter.Optional });
            #endregion

            #region Export
            routes.MapRoute(
                "ExportUserInactive",
                "User/ExportInactive",
                new { controller = "Export", action = "UserInactive", id = UrlParameter.Optional });
            routes.MapRoute(
                "ExportUserActive",
                "User/ExportActive",
                new { controller = "Export", action = "UserActive", id = UrlParameter.Optional });
            #endregion
        }
    }
}
