﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class MessageModule : Module
    {
        public override string Name
        {
            get { return "Message"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {

        }
    }
}
