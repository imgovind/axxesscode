﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class OasisModule : Module
    {
        public override string Name
        {
            get { return "Oasis"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                "PlanOfCarePrintPreview",
                "485/PrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PlanOfCarePrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute(
                "485Medication",
                "485/Medication/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "PlanOfCareMedication", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute(
                "OASISPrintPreview",
                "OASISPrintPreview/{episodeId}/{patientId}/{eventId}",
                new { controller = this.Name, action = "OASISPrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute(
                "Validate",
                "Validate/{Id}/{patientId}/{episodeId}/{assessmentType}",
                new { controller = this.Name, action = "Validate", Id = new IsGuid(), patientId = new IsGuid(), episodeId = new IsGuid(), assessmentType = UrlParameter.Optional });
            routes.MapRoute(
                "Profile",
                "Oasis/Profile/{Id}/{type}",
                new { controller = this.Name, action = "OasisProfilePrint", Id = new IsGuid(), type = UrlParameter.Optional });
            routes.MapRoute(
               "ExportGrid",
               "Oasis/Export/Grid",
               new { controller = this.Name, action = "ExportGrid" });
            routes.MapRoute(
               "ExportExport",
               "Oasis/Export/Export",
               new { controller = "Export", action = "OasisExport" });
            routes.MapRoute(
               "ExportedGrid",
               "Oasis/Exported/Grid",
               new { controller = this.Name, action = "ExportGrid" });
            routes.MapRoute(
               "ExportedReopen",
               "Oasis/Exported/Reopen",
               new { controller = this.Name, action = "ExportedReopen" });
            routes.MapRoute(
               "ExportedExport",
               "Oasis/Exported/Export",
               new { controller = "Export", action = "ExportedOasis" });
        }
    }
}
