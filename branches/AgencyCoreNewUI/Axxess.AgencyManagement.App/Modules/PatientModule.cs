﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Domain;

    public class PatientModule : Module
    {
        public override string Name
        {
            get { return "Patient"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region Photo
            routes.MapRoute(
                "PhotoEdit",
                "Patient/Photo/Edit",
                new { controller = this.Name, action = "PhotoEdit", id = new IsGuid() });
            routes.MapRoute(
                "PhotoUpdate",
                "Patient/Photo/Update",
                new { controller = this.Name, action = "PhotoUpdate", id = new IsGuid() });
            routes.MapRoute(
                "PhotoDelete",
                "Patient/Photo/Delete",
                new { controller = this.Name, action = "PhotoDelete", id = new IsGuid() });
            #endregion

            #region Charts
            routes.MapRoute(
                "ChartsContent",
                "Patient/Charts/Content",
                new { controller = this.Name, action = "ChartsContent", patientId = new IsGuid() });
            routes.MapRoute(
                "ChartsInfo",
                "Patient/Charts/Info",
                new { controller = this.Name, action = "ChartsInfo", patientId = new IsGuid() });
            routes.MapRoute(
                "ChartsActivitiesGrid",
                "Patient/Charts/ActivitiesGrid",
                new { controller = this.Name, action = "ChartsActivitiesGrid", patientId = new IsGuid(), discipline = UrlParameter.Optional, dateRangeId = UrlParameter.Optional, rangeStartDate = UrlParameter.Optional, rangeEndDate = UrlParameter.Optional });
            routes.MapRoute(
                "Map",
                "Patient/Map/{id}",
                new { controller = this.Name, action = "Map", id = new IsGuid() });
            #endregion

            #region Authorization
            routes.MapRoute(
               "AuthorizationNew",
               "Patient/Authorization/New",
               new { controller = this.Name, action = "AuthorizationNew", patientId = UrlParameter.Optional });
            routes.MapRoute(
               "AuthorizationCreate",
               "Patient/Authorization/Create",
               new { controller = this.Name, action = "AuthorizationCreate", authorization = UrlParameter.Optional });
            routes.MapRoute(
               "AuthorizationList",
               "Patient/Authorization/List",
               new { controller = this.Name, action = "AuthorizationList", patientId = new IsGuid() });
            routes.MapRoute(
               "AuthorizationGrid",
               "Patient/Authorization/Grid",
               new { controller = this.Name, action = "AuthorizationGrid", patientId = new IsGuid() });
            routes.MapRoute(
                "AuthorizationPrintPreview",
                "Patient/Authorization/PrintPreview/{id}/{patientId}",
                new { controller = this.Name, action = "AuthorizationPrintPreview", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "AuthorizationPrint",
                "Patient/Authorization/Print/{id}/{patientId}",
                new { controller = this.Name, action = "AuthorizationPrint", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
               "AuthorizationEdit",
               "Patient/Authorization/Edit",
               new { controller = this.Name, action = "AuthorizationEdit", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
               "AuthorizationUpdate",
               "Patient/Authorization/Update",
               new { controller = this.Name, action = "AuthorizationUpdate", authorization = UrlParameter.Optional });
            routes.MapRoute(
               "AuthorizationDelete",
               "Patient/Authorization/Delete",
               new { controller = this.Name, action = "AuthorizationDelete", id = new IsGuid(), patientId = new IsGuid() });
            #endregion

            #region Communication Note
            routes.MapRoute(
                "CommunicationNoteNew",
                "Patient/CommunicationNote/New",
                new { controller = this.Name, action = "CommunicationNoteNew", patientId = UrlParameter.Optional });
            routes.MapRoute(
                "CommunicationNoteCreate",
                "Patient/CommunicationNote/Create",
                new { controller = this.Name, action = "CommunicationNoteCreate", communicationNote = UrlParameter.Optional });
            routes.MapRoute(
                "CommunicationNoteList",
                "Patient/CommunicationNote/List",
                new { controller = this.Name, action = "CommunicationNoteList", patientId = UrlParameter.Optional });
            routes.MapRoute(
                "CommunicationNoteGrid",
                "Patient/CommunicationNote/Grid",
                new { controller = this.Name, action = "CommunicationNoteGrid", patientId = UrlParameter.Optional });
            routes.MapRoute(
                "CommunicationNoteExport",
                "Patient/CommunicationNote/Export",
                new { controller = "Export", action = "CommunicationNote" });
            routes.MapRoute(
                "CommunicationNotePrintPreview",
                "Patient/CommunicationNote/PrintPreview",
                new { controller = this.Name, action = "CommunicationNotePrintPreview", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "CommunicationNotePrint",
                "Patient/CommunicationNote/Print",
                new { controller = this.Name, action = "CommunicationNotePrint", id = UrlParameter.Optional, patientId = UrlParameter.Optional });
            routes.MapRoute(
                "CommunicationNoteEdit",
                "Patient/CommunicationNote/Edit",
                new { controller = this.Name, action = "CommunicationNoteEdit", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "CommunicationNoteUpdate",
                "Patient/CommunicationNote/Update",
                new { controller = this.Name, action = "CommunicationNoteUpdate", communicationNote = UrlParameter.Optional });
            routes.MapRoute(
                "CommunicationNoteDelete",
                "Patient/CommunicationNote/Delete",
                new { controller = this.Name, action = "CommunicationNoteDelete", id = new IsGuid(), patientId = new IsGuid() });
            #endregion

            #region FaceToFaceEncounter
            routes.MapRoute(
              "FaceToFaceEncounterNew",
              "Patient/FaceToFaceEncounter/New",
              new { controller = this.Name, action = "FaceToFaceEncounterNew" });
            routes.MapRoute(
              "FaceToFaceEncounterCreate",
              "Patient/FaceToFaceEncounter/Create",
              new { controller = this.Name, action = "FaceToFaceEncounterCreate", faceToFaceEncounter = UrlParameter.Optional });
            routes.MapRoute(
              "FaceToFaceEncounterPrintPreview",
              "Patient/FaceToFaceEncounter/PrintPreview",
              new { controller = this.Name, action = "FaceToFaceEncounterPrintPreview", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
              "FaceToFaceEncounterPrint",
              "Patient/FaceToFaceEncounter/Print",
              new { controller = this.Name, action = "FaceToFaceEncounterPrint", id = UrlParameter.Optional, patientId = UrlParameter.Optional });
            #endregion

            #region Order
            routes.MapRoute(
                "OrderNew",
                "Patient/Order/New",
                new { controller = this.Name, action = "OrderNew", patientId = UrlParameter.Optional });
            routes.MapRoute(
                "OrderCreate",
                "Patient/Order/Create",
                new { controller = this.Name, action = "OrderCreate", order = UrlParameter.Optional });
            routes.MapRoute(
                "OrderHistory",
                "Patient/Order/History",
                new { controller = this.Name, action = "OrderHistory", patientId = new IsGuid() });
            routes.MapRoute(
                "OrderHistoryGrid",
                "Patient/Order/History/Grid",
                new { controller = this.Name, action = "OrderHistoryGrid", patientId = new IsGuid() });
            routes.MapRoute(
                "OrderHistoryExport",
                "Patient/Order/History/Export",
                new { controller = "Export", action = "PatientOrdersHistory", patientId = new IsGuid() });
            routes.MapRoute(
                "OrderPrintPreview",
                "Patient/Order/PrintPreview/{id}/{patientId}",
                new { controller = this.Name, action = "OrderPrintPreview", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "OrderPrint",
                "Patient/Order/Print/{id}/{patientId}",
                new { controller = this.Name, action = "OrderPrint", id = UrlParameter.Optional, patientId = UrlParameter.Optional });
            routes.MapRoute(
                "OrderEdit",
                "Patient/Order/Edit",
                new { controller = this.Name, action = "OrderEdit", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "OrderUpdate",
                "Patient/Order/Update",
                new { controller = this.Name, action = "OrderUpdate", order = UrlParameter.Optional });
            #endregion

            #region EmergencyContact
            routes.MapRoute(
                "EmergencyContactNew",
                "Patient/EmergencyContact/New",
                new { controller = this.Name, action = "EmergencyContactNew", patientId = new IsGuid() });
            routes.MapRoute(
                "EmergencyContactCreate",
                "Patient/EmergencyContact/Create",
                new { controller = this.Name, action = "EmergencyContactCreate" });
            routes.MapRoute(
                "EmergencyContactGet",
                "Patient/EmergencyContact/Get",
                new { controller = this.Name, action = "EmergencyContactGet", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "EmergencyContactGrid",
                "Patient/EmergencyContact/Grid",
                new { controller = this.Name, action = "EmergencyContactGrid", patientId = new IsGuid() });
            routes.MapRoute(
                "EmergencyContactEdit",
                "Patient/EmergencyContact/Edit",
                new { controller = this.Name, action = "EmergencyContactEdit", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "EmergencyContactUpdate",
                "Patient/EmergencyContact/Update",
                new { controller = this.Name, action = "EmergencyContactUpdate" });
            routes.MapRoute(
                "EmergencyContactDelete",
                "Patient/EmergencyContact/Delete",
                new { controller = this.Name, action = "EmergencyContactDelete", id = new IsGuid(), patientId = new IsGuid() });
            #endregion

            #region Physician
            routes.MapRoute(
                "PatientPhysicianAdd",
                "Patient/Physician/Add",
                new { controller = this.Name, action = "PhysicianAdd", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "PatientPhysicianGet",
                "Patient/Physician/Get",
                new { controller = this.Name, action = "PhysicianGet", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "PatientPhysicianGrid",
                "Patient/Physician/Grid",
                new { controller = this.Name, action = "PhysicianGrid", patientId = new IsGuid() });
            routes.MapRoute(
                "PatientPhysicianSetPrimary",
                "Patient/Physician/SetPrimary",
                new { controller = this.Name, action = "PhysicianSetPrimary", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "PatientPhysicianDelete",
                "Patient/Physician/Delete",
                new { controller = this.Name, action = "PhysicianDelete", id = new IsGuid(), patientId = new IsGuid() });
            #endregion

            #region Profile
            routes.MapRoute(
                "ProfilePrintPreview",
                "Patient/Profile/PrintPreview/{id}",
                new { controller = this.Name, action = "ProfilePrintPreview", id = new IsGuid() });
            routes.MapRoute(
                "ProfilePrint",
                "Patient/Profile/Print/{id}",
                new { controller = this.Name, action = "ProfilePrint", id = new IsGuid() });
            routes.MapRoute(
                "TriageClassificationPrintPreview",
                "Patient/TriageClassification/PrintPreview/{id}",
                new { controller = this.Name, action = "TriageClassificationPrintPreview", id = new IsGuid() });
            routes.MapRoute(
                "TriageClassificationPrint",
                "Patient/TriageClassification/Print/{id}",
                new { controller = this.Name, action = "TriageClassificationPrint", id = new IsGuid() });
            #endregion

            #region Medication
            routes.MapRoute(
                "MedicationNew",
                "Patient/Medication/New",
                new { controller = this.Name, action = "MedicationNew", profileId = new IsGuid() });
            routes.MapRoute(
                "MedicationCreate",
                "Patient/Medication/Create",
                new { controller = this.Name, action = "MedicationCreate", profileId = new IsGuid() });
            routes.MapRoute(
                "MedicationProfile",
                "Patient/Medication/Profile",
                new { controller = this.Name, action = "MedicationProfile", patientId = new IsGuid() });
            routes.MapRoute(
                "MedicationProfileGrid",
                "Patient/Medication/Profile/Grid",
                new { controller = this.Name, action = "MedicationProfileGrid", profileId = new IsGuid() });
            routes.MapRoute(
                "MedicationProfilePrintPreview",
                "Patient/Medication/Profile/PrintPreview/{patientId}",
                new { controller = this.Name, action = "MedicationProfilePrintPreview", patientId = new IsGuid() });
            routes.MapRoute(
                "MedicationProfilePrint",
                "Patient/Medication/Profile/Print/{patientId}",
                new { controller = this.Name, action = "MedicationProfilePrint", patientId = new IsGuid() });
            routes.MapRoute(
                "MedicationInteractions",
                "Patient/Medication/Interactions",
                new { controller = this.Name, action = "MedicationInteractions", profileId = new IsGuid() });
            routes.MapRoute(
                "MedicationInteractionsPrint",
                "Patient/Medication/Interactions/Print",
                new { controller = this.Name, action = "MedicationInteractionsPrint", profileId = new IsGuid() });
            routes.MapRoute(
                "MedicationEdit",
                "Patient/Medication/Edit",
                new { controller = this.Name, action = "MedicationEdit", id = new IsGuid(), profileId = new IsGuid() });
            routes.MapRoute(
                "MedicationUpdate",
                "Patient/Medication/Update",
                new { controller = this.Name, action = "MedicationUpdate" });
            routes.MapRoute(
                "MedicationDiscontinue",
                "Patient/Medication/Discontinue",
                new { controller = this.Name, action = "MedicationDiscontinue", id = new IsGuid(), profileId = new IsGuid() });
            routes.MapRoute(
                "MedicationUpdateStatus",
                "Patient/Medication/UpdateStatus",
                new { controller = this.Name, action = "MedicationUpdateStatus", id = new IsGuid(), profileId = new IsGuid() });
            routes.MapRoute(
                "MedicationDelete",
                "Patient/Medication/Delete",
                new { controller = this.Name, action = "MedicationDelete", id = new IsGuid(), profileId = new IsGuid() });
            routes.MapRoute(
                "MedicationLogs",
                "Patient/Medication/Logs",
                new { controller = this.Name, action = "MedicationLogs", id = new IsGuid(), patientId = new IsGuid() });
            #endregion

            #region Medication Snapshot
            routes.MapRoute(
                "MedicationSnapshotNew",
                "Patient/Medication/Snapshot/New",
                new { controller = this.Name, action = "MedicationSnapshotNew", patientId = new IsGuid() });
            routes.MapRoute(
                "MedicationSnapshotCreate",
                "Patient/Medication/Snapshot/Create",
                new { controller = this.Name, action = "MedicationSnapshotCreate" });
            routes.MapRoute(
                "MedicationSnapshotList",
                "Patient/Medication/Snapshot/List",
                new { controller = this.Name, action = "MedicationSnapshotList", patientId = new IsGuid() });
            routes.MapRoute(
                "MedicationSnapshotGrid",
                "Patient/Medication/Snapshot/Grid",
                new { controller = this.Name, action = "MedicationSnapshotGrid" });
            routes.MapRoute(
                "MedicationSnapshotPrintPreview",
                "Patient/Medication/Snapshot/PrintPreview/{id}",
                new { controller = this.Name, action = "MedicationSnapshotPrintPreview", id = new IsGuid() });
            routes.MapRoute(
                "MedicationSnapshotPrint",
                "Patient/Medication/Snapshot/Print/{id}",
                new { controller = this.Name, action = "MedicationSnapshotPrint", id = new IsGuid() });
            routes.MapRoute(
                "MedicationSnapshotEdit",
                "Patient/Medication/Snapshot/Edit",
                new { controller = this.Name, action = "MedicationSnapshotEdit", id = new IsGuid() });
            routes.MapRoute(
                "MedicationSnapshotUpdate",
                "Patient/Medication/Snapshot/Update",
                new { controller = this.Name, action = "MedicationSnapshotUpdate" });
            routes.MapRoute(
                "MedicationSnapshotDelete",
                "Patient/Medication/Snapshot/Delete",
                new { controller = this.Name, action = "MedicationSnapshotDelete", id = new IsGuid() });
            #endregion

            #region Allergy
            routes.MapRoute(
                "AllergyNew",
                "Patient/Allergy/New",
                new { controller = this.Name, action = "AllergyNew", profileId = new IsGuid() });
            routes.MapRoute(
                "AllergyCreate",
                "Patient/Allergy/Create",
                new { controller = this.Name, action = "AllergyCreate", profileId = new IsGuid() });
            routes.MapRoute(
                "AllergyProfile",
                "Patient/Allergy/Profile",
                new { controller = this.Name, action = "AllergyProfile", patientId = new IsGuid() });
            routes.MapRoute(
                "AllergyProfileGrid",
                "Patient/Allergy/Profile/Grid",
                new { controller = this.Name, action = "AllergyProfileGrid", profileId = new IsGuid() });
            routes.MapRoute(
                "AllergyProfilePrintPreview",
                "Patient/Allergy/Profile/PrintPreview/{id}",
                new { controller = this.Name, action = "AllergyProfilePrintPreview", id = new IsGuid() });
            routes.MapRoute(
                "AllergyProfilePrint",
                "Patient/Allergy/Profile/Print/{id}",
                new { controller = this.Name, action = "AllergyProfilePrint", id = new IsGuid() });
            routes.MapRoute(
                "AllergyEdit",
                "Patient/Allergy/Edit",
                new { controller = this.Name, action = "AllergyEdit", id = new IsGuid(), profileId = new IsGuid() });
            routes.MapRoute(
                "AllergyUpdate",
                "Patient/Allergy/Update",
                new { controller = this.Name, action = "AllergyUpdate" });
            routes.MapRoute(
                "AllergyUpdateStatus",
                "Patient/Allergy/UpdateStatus",
                new { controller = this.Name, action = "AllergyUpdateStatus", id = new IsGuid(), profileId = new IsGuid() });
            routes.MapRoute(
                "AllergyDelete",
                "Patient/Allergy/Delete",
                new { controller = this.Name, action = "AllergyDelete", id = new IsGuid(), profileId = new IsGuid() });
            #endregion

            #region Status
            routes.MapRoute(
                "StatusEdit",
                "Patient/Status/Edit",
                new { controller = this.Name, action = "StatusEdit", id = new IsGuid() });
            routes.MapRoute(
                "StatusUpdate",
                "Patient/Status/Update",
                new { controller = this.Name, action = "StatusUpdate" });
            #endregion

            #region Sixty Day Summary
            routes.MapRoute(
                "SixtyDaySummaryList",
                "Patient/SixtyDaySummary/List",
                new { controller = this.Name, action = "SixtyDaySummaryList", id = new IsGuid() });
            routes.MapRoute(
                "SixtyDaySummaryGrid",
                "Patient/SixtyDaySummary/Grid",
                new { controller = this.Name, action = "SixtyDaySummaryGrid", id = new IsGuid() });
            #endregion

            #region Vital Signs
            routes.MapRoute(
                "VitalSignsList",
                "Patient/VitalSigns/List",
                new { controller = this.Name, action = "VitalSignsList", id = new IsGuid() });
            routes.MapRoute(
                "VitalSignsGrid",
                "Patient/VitalSigns/Grid",
                new { controller = this.Name, action = "VitalSignsGrid", id = new IsGuid() });
            routes.MapRoute(
                "VitalSignsExport",
                "Patient/VitalSigns/Export",
                new { controller = "Export", action = "VitalSigns", id = new IsGuid() });
            #endregion

            #region Pending Admissions
            routes.MapRoute(
                "PendingAdmissionList",
                "Patient/PendingAdmission/List",
                new { controller = this.Name, action = "PendingAdmissionList" });
            routes.MapRoute(
                "PendingAdmissionGrid",
                "Patient/PendingAdmission/Grid",
                new { controller = this.Name, action = "PendingAdmissionGrid" });
            routes.MapRoute(
                "PendingAdmissionExport",
                "Patient/PendingAdmission/Export",
                new { controller = "Export", action = "PendingAdmission" });
            routes.MapRoute(
                "AdmitNew",
                "Patient/Admit/New",
                new { controller = this.Name, action = "AdmitNew" });
            routes.MapRoute(
                "AdmitCreate",
                "Patient/Admit/Create",
                new { controller = this.Name, action = "AdmitCreate" });
            routes.MapRoute(
                "NonAdmitNew",
                "Patient/NonAdmit/New",
                new { controller = this.Name, action = "NonAdmitNew" });
            routes.MapRoute(
                "NonAdmitCreate",
                "Patient/NonAdmit/Create",
                new { controller = this.Name, action = "NonAdmitCreate" });
            routes.MapRoute(
                "NonAdmissionList",
                "Patient/NonAdmission/List",
                new { controller = this.Name, action = "NonAdmissionList" });
            routes.MapRoute(
                "NonAdmissionGrid",
                "Patient/NonAdmission/Grid",
                new { controller = this.Name, action = "NonAdmissionGrid" });
            routes.MapRoute(
                "DeletedList",
                "Patient/Deleted/List",
                new { controller = this.Name, action = "DeletedList" });
            routes.MapRoute(
                "DeletedGrid",
                "Patient/Deleted/Grid",
                new { controller = this.Name, action = "DeletedGrid" });
            routes.MapRoute(
                "DeletedExport",
                "Patient/Deleted/Export",
                new { controller = "Export", action = "DeletedPatients" });
            #endregion

            #region Hospitalization
            routes.MapRoute(
                "HospitalizationList",
                "Patient/Hospitalization/List",
                new { controller = this.Name, action = "HospitalizationList" });
            routes.MapRoute(
                "HospitalizationGrid",
                "Patient/Hospitalization/Grid",
                new { controller = this.Name, action = "HospitalizationGrid" });
            routes.MapRoute(
                "HospitalizationLogs",
                "Patient/Hospitalization/Logs",
                new { controller = this.Name, action = "HospitalizationLogs" });
            routes.MapRoute(
                "HospitalizationNew",
                "Patient/Hospitalization/New",
                new { controller = this.Name, action = "HospitalizationNew" });
            routes.MapRoute(
                "HospitalizationCreate",
                "Patient/Hospitalization/Create",
                new { controller = this.Name, action = "HospitalizationCreate" });
            routes.MapRoute(
                "HospitalizationEdit",
                "Patient/Hospitalization/Edit",
                new { controller = this.Name, action = "HospitalizationEdit" });
            routes.MapRoute(
                "HospitalizationUpdate",
                "Patient/Hospitalization/Update",
                new { controller = this.Name, action = "HospitalizationUpdate" });
            routes.MapRoute(
                "HospitalizationDelete",
                "Patient/Hospitalization/Delete",
                new { controller = this.Name, action = "HospitalizationDelete" });
            routes.MapRoute(
                "HospitalizationLogsGrid",
                "Patient/Hospitalization/LogsGrid",
                new { controller = this.Name, action = "HospitalizationLogsGrid" });
            routes.MapRoute(
                "HospitalizationPrint",
                "Patient/Hospitalization/Print/{id}/{patientId}",
                new { controller = this.Name, action = "HospitalizationPrint", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "HospitalizationPrintPreview",
                "Patient/Hospitalization/PrintPreview/{id}/{patientId}",
                new { controller = this.Name, action = "HospitalizationPrintPreview", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "HospitalizationExport",
                "Patient/Hospitalization/Export",
                new { controller = "Export", action = "HospitalizationLog" });
            #endregion
        }
    }
}