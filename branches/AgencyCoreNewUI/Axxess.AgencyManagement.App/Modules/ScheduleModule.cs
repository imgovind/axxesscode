﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class ScheduleModule : Module
    {
        public override string Name
        {
            get { return "Schedule"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region Episode
            routes.MapRoute(
                "EpisodeNew",
                "Schedule/Episode/New",
                new { controller = this.Name, action = "EpisodeNew", id = UrlParameter.Optional });
            routes.MapRoute(
                "EpisodeCreate",
                "Schedule/Episode/Create",
                new { controller = this.Name, action = "EpisodeCreate", id = UrlParameter.Optional });
            routes.MapRoute(
                "EpisodeEdit",
                "Schedule/Episode/Edit",
                new { controller = this.Name, action = "EpisodeEdit", id = UrlParameter.Optional });
            routes.MapRoute(
                "EpisodeUpdate",
                "Schedule/Episode/Update",
                new { controller = this.Name, action = "EpisodeUpdate", id = UrlParameter.Optional });
            routes.MapRoute(
                "EpisodeInactiveList",
                "Schedule/Episode/InactiveList",
                new { controller = this.Name, action = "EpisodeInactiveList", patientId = new IsGuid() });
            routes.MapRoute(
                "EpisodeInactiveGrid",
                "Schedule/Episode/InactiveGrid",
                new { controller = this.Name, action = "EpisodeInactiveGrid", patientId = new IsGuid() });
            routes.MapRoute(
                "EpisodeFrequencyList",
                "Schedule/Episode/FrequencyList",
                new { controller = this.Name, action = "EpisodeFrequencyList", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "EpisodeActivate",
                "Schedule/Episode/Activate",
                new { controller = this.Name, action = "EpisodeActivate", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "EpisodeLogs",
                "Schedule/Episode/Logs",
                new { controller = this.Name, action = "EpisodeLogs", id = new IsGuid(), patientId = new IsGuid() });
            #endregion

            #region MissedVisit
            routes.MapRoute(
                "MissedVisitNew",
                "Schedule/MissedVisit/New",
                new { controller = this.Name, action = "MissedVisitNew", id = UrlParameter.Optional });
            routes.MapRoute(
                "MissedVisitCreate",
                "Schedule/MissedVisit/Create",
                new { controller = this.Name, action = "MissedVisitCreate", id = UrlParameter.Optional });
            routes.MapRoute(
                "MissedVisitPrintPreview",
                "Schedule/MissedVisit/PrintPreview",
                new { controller = this.Name, action = "MissedVisitPrintPreview", patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute(
                "MissedVisitPrint",
                "Schedule/MissedVisit/Print",
                new { controller = this.Name, action = "MissedVisitPrint", patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute(
                "MissedVisitView",
                "Schedule/MissedVisit/View",
                new { controller = this.Name, action = "MissedVisitView", patientId = new IsGuid(), eventId = new IsGuid() });
            #endregion

            #region Center
            routes.MapRoute(
                "CenterContent",
                "Schedule/Center/Content",
                new { controller = this.Name, action = "CenterContent", patientId = new IsGuid(), episodeId = UrlParameter.Optional });
            routes.MapRoute(
                "CenterCalendar",
                "Schedule/Center/Calendar",
                new { controller = this.Name, action = "CenterCalendar", patientId = new IsGuid(), episodeId = UrlParameter.Optional, discipline = UrlParameter.Optional });
            routes.MapRoute(
                "CenterActivities",
                "Schedule/Center/Activities",
                new { controller = this.Name, action = "CenterActivities", patientId = new IsGuid(), episodeId = UrlParameter.Optional, discipline = UrlParameter.Optional });
            routes.MapRoute(
                "CenterActivitiesGrid",
                "Schedule/Center/ActivitiesGrid",
                new { controller = this.Name, action = "CenterActivitiesGrid", patientId = new IsGuid(), episodeId = UrlParameter.Optional, discipline = UrlParameter.Optional });
            #endregion

            #region Details
            routes.MapRoute(
                "DetailsEdit",
                "Schedule/Details/Edit",
                new { controller = this.Name, action = "DetailsEdit", eventId = new IsGuid(), episodeId = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "DetailsUpdate",
                "Schedule/Details/Update",
                new { controller = this.Name, action = "DetailsUpdate" });
            #endregion

            #region Master Calendar
            routes.MapRoute(
                "MasterCalendarContent",
                "Schedule/MasterCalendar/Content",
                new { controller = this.Name, action = "MasterCalendarContent", episodeId = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "MasterCalendarPrint",
                "Schedule/MasterCalendar/Print",
                new { controller = this.Name, action = "MasterCalendarPrint", episodeId = new IsGuid(), patientId = new IsGuid() });
            #endregion

            #region Private Duty
            routes.MapRoute(
                "PDCenter",
                "Schedule/PD/Center",
                new { controller = this.Name, action = "PDCenter" });
            #endregion

            routes.MapRoute(
              "DeviationGrid",
              "Schedule/Deviation/Grid",
              new { controller = this.Name, action = "DeviationGrid" });
            routes.MapRoute(
              "DeviationExport",
              "Schedule/Deviation/Export",
              new { controller = "Export", action = "ScheduleDeviations" });
            
            
            routes.MapRoute(
              "NotePrintPreview",
              "Schedule/NotePrintPreview/{episodeId}/{patientId}/{eventId}",
              new { controller = this.Name, action = "NotePrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
        }
    }
}
