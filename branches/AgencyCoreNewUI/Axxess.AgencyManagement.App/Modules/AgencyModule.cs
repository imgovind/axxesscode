﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Domain;

    public class AgencyModule : Module
    {
        public override string Name
        {
            get { return "Agency"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region QA Center
            routes.MapRoute(
                "QA",
                "Agency/QA",
                new { controller = this.Name, action = "QA", id = UrlParameter.Optional });
            routes.MapRoute(
                "QAGrid",
                "Agency/QA/Grid",
                new { controller = this.Name, action = "QAGrid", id = UrlParameter.Optional });
            routes.MapRoute(
                "QAExport",
                "Agency/QA/Export",
                new { controller = "Export", action = "QA", id = UrlParameter.Optional });
            #endregion

            #region Contact
            routes.MapRoute(
                "ContactNew",
                "Agency/Contact/New",
                new { controller = this.Name, action = "ContactNew" });
            routes.MapRoute(
                "ContactCreate",
                "Agency/Contact/Create",
                new { controller = this.Name, action = "ContactCreate", contact = UrlParameter.Optional });
            routes.MapRoute(
                "ContactList",
                "Agency/Contact/List",
                new { controller = this.Name, action = "ContactList" });
            routes.MapRoute(
                "ContactGrid",
                "Agency/Contact/Grid",
                new { controller = this.Name, action = "ContactGrid" });
            routes.MapRoute(
                "ContactEdit",
                "Agency/Contact/Edit",
                new { controller = this.Name, action = "ContactEdit", id = new IsGuid() });
            routes.MapRoute(
                "ContactUpdate",
                "Agency/Contact/Update",
                new { controller = this.Name, action = "ContactUpdate", contact = UrlParameter.Optional });
            routes.MapRoute(
                "ContactDelete",
                "Agency/Contact/Delete",
                new { controller = this.Name, action = "ContactDelete", id = new IsGuid() });
            routes.MapRoute(
                "ContactLogs",
                "Agency/Contact/Logs",
                new { controller = this.Name, action = "ContactLogs", id = new IsGuid() });
            routes.MapRoute(
                "ContactExport",
                "Agency/Contact/Export",
                new { controller = "Export", action = "Contact", id = new IsGuid() });
            #endregion

            #region Hospital
            routes.MapRoute(
                "HospitalNew",
                "Agency/Hospital/New",
                new { controller = this.Name, action = "HospitalNew" });
            routes.MapRoute(
                "HospitalCreate",
                "Agency/Hospital/Create",
                new { controller = this.Name, action = "HospitalCreate", hospital = UrlParameter.Optional });
            routes.MapRoute(
                "HospitalList",
                "Agency/Hospital/List",
                new { controller = this.Name, action = "HospitalList" });
            routes.MapRoute(
                "HospitalGrid",
                "Agency/Hospital/Grid",
                new { controller = this.Name, action = "HospitalGrid" });
            routes.MapRoute(
                "HospitalEdit",
                "Agency/Hospital/Edit",
                new { controller = this.Name, action = "HospitalEdit", id = new IsGuid() });
            routes.MapRoute(
                "HospitalUpdate",
                "Agency/Hospital/Update",
                new { controller = this.Name, action = "HospitalUpdate", hospital = UrlParameter.Optional });
            routes.MapRoute(
                "HospitalDelete",
                "Agency/Hospital/Delete",
                new { controller = this.Name, action = "HospitalDelete", id = new IsGuid() });
            routes.MapRoute(
                "HospitalLogs",
                "Agency/Hospital/Logs",
                new { controller = this.Name, action = "HospitalLogs", id = new IsGuid() });
            routes.MapRoute(
                "HospitalExport",
                "Agency/Hospital/Export",
                new { controller = "Export", action = "Hospital", id = new IsGuid() });
            #endregion

            #region Physician
            routes.MapRoute(
                "PhysicianNew",
                "Agency/Physician/New",
                new { controller = this.Name, action = "PhysicianNew" });
            routes.MapRoute(
                "PhysicianCreate",
                "Agency/Physician/Create",
                new { controller = this.Name, action = "PhysicianCreate", physician = UrlParameter.Optional });
            routes.MapRoute(
                "PhysicianGet",
                "Agency/Physician/Get",
                new { controller = this.Name, action = "PhysicianGet", id = UrlParameter.Optional });
            routes.MapRoute(
                "PhysicianList",
                "Agency/Physician/List",
                new { controller = this.Name, action = "PhysicianList" });
            routes.MapRoute(
                "PhysicianGrid",
                "Agency/Physician/Grid",
                new { controller = this.Name, action = "PhysicianGrid" });
            routes.MapRoute(
                "PhysicianEdit",
                "Agency/Physician/Edit",
                new { controller = this.Name, action = "PhysicianEdit", id = new IsGuid() });
            routes.MapRoute(
                "PhysicianUpdate",
                "Agency/Physician/Update",
                new { controller = this.Name, action = "PhysicianUpdate", physician = UrlParameter.Optional });
            routes.MapRoute(
                "PhysicianDelete",
                "Agency/Physician/Delete",
                new { controller = this.Name, action = "PhysicianDelete", id = new IsGuid() });
            routes.MapRoute(
                "PhysicianLogs",
                "Agency/Physician/Logs",
                new { controller = this.Name, action = "PhysicianLogs", id = new IsGuid() });
            routes.MapRoute(
                "PhysicianExport",
                "Agency/Physician/Export",
                new { controller = "Export", action = "Physician", id = new IsGuid() });
            #endregion

            #region PhysicianLicense
            routes.MapRoute(
                "PhysicianLicenseNew",
                "Agency/Physician/License/New",
                new { controller = this.Name, action = "PhysicianLicenseNew" });
            routes.MapRoute(
                "PhysicianLicenseCreate",
                "Agency/Physician/License/Create",
                new { controller = this.Name, action = "PhysicianLicenseCreate", physicianLicense = UrlParameter.Optional });
            routes.MapRoute(
                "PhysicianLicenseList",
                "Agency/Physician/License/List",
                new { controller = this.Name, action = "PhysicianLicenseList" });
            routes.MapRoute(
                "PhysicianLicenseGrid",
                "Agency/Physician/License/Grid",
                new { controller = this.Name, action = "PhysicianLicenseGrid" });
            routes.MapRoute(
                "PhysicianLicenseEdit",
                "Agency/Physician/License/Edit",
                new { controller = this.Name, action = "PhysicianLicenseEdit", id = new IsGuid() });
            routes.MapRoute(
                "PhysicianLicenseUpdate",
                "Agency/Physician/License/Update",
                new { controller = this.Name, action = "PhysicianLicenseUpdate", physicianLicense = UrlParameter.Optional });
            routes.MapRoute(
                "PhysicianLicenseDelete",
                "Agency/Physician/License/Delete",
                new { controller = this.Name, action = "PhysicianLicenseDelete", id = new IsGuid() });
            #endregion

            #region Location
            routes.MapRoute(
                "LocationNew",
                "Agency/Location/New",
                new { controller = this.Name, action = "LocationNew" });
            routes.MapRoute(
                "LocationCreate",
                "Agency/Location/Create",
                new { controller = this.Name, action = "LocationCreate", location = UrlParameter.Optional });
            routes.MapRoute(
                "LocationList",
                "Agency/Location/List",
                new { controller = this.Name, action = "LocationList" });
            routes.MapRoute(
                "LocationGrid",
                "Agency/Location/Grid",
                new { controller = this.Name, action = "LocationGrid" });
            routes.MapRoute(
                "LocationEdit",
                "Agency/Location/Edit",
                new { controller = this.Name, action = "LocationEdit", id = new IsGuid() });
            routes.MapRoute(
                "LocationUpdate",
                "Agency/Location/Update",
                new { controller = this.Name, action = "LocationUpdate", location = UrlParameter.Optional });
            routes.MapRoute(
                "LocationDelete",
                "Agency/Location/Delete",
                new { controller = this.Name, action = "LocationDelete", id = new IsGuid() });
            routes.MapRoute(
                "LocationLogs",
                "Agency/Location/Logs",
                new { controller = this.Name, action = "LocationLogs", id = new IsGuid() });
            routes.MapRoute(
                "LocationExport",
                "Agency/Location/Export",
                new { controller = "Export", action = "Location", id = new IsGuid() });
            #endregion

            #region Insurance
            routes.MapRoute(
                "InsuranceNew",
                "Agency/Insurance/New",
                new { controller = this.Name, action = "InsuranceNew" });
            routes.MapRoute(
                "InsuranceCreate",
                "Agency/Insurance/Create",
                new { controller = this.Name, action = "InsuranceCreate", insurance = UrlParameter.Optional, formCollection = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceList",
                "Agency/Insurance/List",
                new { controller = this.Name, action = "InsuranceList" });
            routes.MapRoute(
                "InsuranceGrid",
                "Agency/Insurance/Grid",
                new { controller = this.Name, action = "InsuranceGrid" });
            routes.MapRoute(
                "InsuranceEdit",
                "Agency/Insurance/Edit",
                new { controller = this.Name, action = "InsuranceEdit", id = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceUpdate",
                "Agency/Insurance/Update",
                new { controller = this.Name, action = "InsuranceUpdate", insurance = UrlParameter.Optional, formCollection = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceDelete",
                "Agency/Insurance/Delete",
                new { controller = this.Name, action = "InsuranceDelete", id = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceLogs",
                "Agency/Insurance/Logs",
                new { controller = this.Name, action = "InsuranceLogs", id = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceSelectList",
                "Agency/Insurance/SelectList",
                new { controller = this.Name, action = "InsuranceSelectList", branchId = new IsGuid() });
            routes.MapRoute(
                "InsuranceMedicareSelectList",
                "Agency/Insurance/MedicareSelectList",
                new { controller = this.Name, action = "InsuranceMedicareSelectList", branchId = new IsGuid() });
            routes.MapRoute(
                "InsuranceMedicareWithHMOSelectList",
                "Agency/Insurance/MedicareWithHMOSelectList",
                new { controller = this.Name, action = "InsuranceMedicareWithHMOSelectList", branchId = new IsGuid() });
            routes.MapRoute(
                "InsurancePatientSelectList",
                "Agency/Insurance/PatientSelectList",
                new { controller = this.Name, action = "InsurancePatientSelectList", branchId = new IsGuid() });
            routes.MapRoute(
                "InsuranceDuplicateVisitRates",
                "Agency/Insurance/DuplicateVisitRates",
                new { controller = this.Name, action = "InsuranceDuplicateVisitRates", id = UrlParameter.Optional, replacedId = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceExport",
                "Agency/Insurance/Export",
                new { controller = "Export", action = "Insurance", id = new IsGuid() });
            #endregion

            #region InsuranceBillData
            routes.MapRoute(
                "InsuranceBillDataNew",
                "Agency/Insurance/BillData/New",
                new { controller = this.Name, action = "InsuranceBillDataNew", insuranceId = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceBillDataCreate",
                "Agency/Insurance/BillData/Create",
                new { controller = this.Name, action = "InsuranceBillDataCreate", insuranceId = UrlParameter.Optional, chargeRate = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceBillDataGrid",
                "Agency/Insurance/BillData/Grid",
                new { controller = this.Name, action = "InsuranceBillDataGrid", insuranceId = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceBillDataEdit",
                "Agency/Insurance/BillData/Edit",
                new { controller = this.Name, action = "InsuranceBillDataEdit", id = UrlParameter.Optional, insuranceId = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceBillDataUpdate",
                "Agency/Insurance/BillData/Update",
                new { controller = this.Name, action = "InsuranceBillDataUpdate", chargeRate = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceBillDataDelete",
                "Agency/Insurance/BillData/Delete",
                new { controller = this.Name, action = "InsuranceBillDataDelete", id = UrlParameter.Optional, insuranceId = UrlParameter.Optional });
            #endregion

            #region InsuranceVisitRates
            routes.MapRoute(
                "InsuranceVisitRatesEdit",
                "Agency/Insurance/VisitRates/Edit",
                new { controller = this.Name, action = "InsuranceVisitRatesEdit", id = UrlParameter.Optional, insuranceId = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceVisitRatesUpdate",
                "Agency/Insurance/VisitRates/Update",
                new { controller = this.Name, action = "InsuranceVisitRatesUpdate", chargeRate = UrlParameter.Optional });
            routes.MapRoute(
                "InsuranceVisitRatesContent",
                "Agency/Insurance/VisitRates/Content",
                new { controller = this.Name, action = "InsuranceVisitRatesContent", id = UrlParameter.Optional, insuranceId = UrlParameter.Optional });
            #endregion

            #region Infection
            routes.MapRoute(
                "InfectionNew",
                "Agency/Infection/New",
                new { controller = this.Name, action = "InfectionNew", patientId = UrlParameter.Optional });
            routes.MapRoute(
                "InfectionCreate",
                "Agency/Infection/Create",
                new { controller = this.Name, action = "InfectionCreate", infection = UrlParameter.Optional });
            routes.MapRoute(
                "InfectionList",
                "Agency/Infection/List",
                new { controller = this.Name, action = "InfectionList" });
            routes.MapRoute(
                "InfectionGrid",
                "Agency/Infection/Grid",
                new { controller = this.Name, action = "InfectionGrid" });
            routes.MapRoute(
                "InfectionEdit",
                "Agency/Infection/Edit",
                new { controller = this.Name, action = "InfectionEdit", id = new IsGuid() });
            routes.MapRoute(
                "InfectionUpdate",
                "Agency/Infection/Update",
                new { controller = this.Name, action = "InfectionUpdate", infection = UrlParameter.Optional });
            routes.MapRoute(
                "InfectionPrintPreview",
                "Agency/Infection/PrintPreview",
                new { controller = this.Name, action = "InfectionPrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute(
                "InfectionPrint",
                "Agency/Infection/Print",
                new { controller = this.Name, action = "InfectionPrint", episodeId = UrlParameter.Optional, patientId = UrlParameter.Optional, eventId = UrlParameter.Optional });
            routes.MapRoute(
                "InfectionProcess",
                "Agency/Infection/Process",
                new { controller = this.Name, action = "InfectionProcess", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "InfectionExport",
                "Agency/Infection/Export",
                new { controller = "Export", action = "Infection", id = new IsGuid() });
            #endregion

            #region Incident
            routes.MapRoute(
                "IncidentNew",
                "Agency/Incident/New",
                new { controller = this.Name, action = "IncidentNew", patientId = UrlParameter.Optional });
            routes.MapRoute(
                "IncidentCreate",
                "Agency/Incident/Create",
                new { controller = this.Name, action = "IncidentCreate", infection = UrlParameter.Optional });
            routes.MapRoute(
                "IncidentList",
                "Agency/Incident/List",
                new { controller = this.Name, action = "IncidentList" });
            routes.MapRoute(
                "IncidentGrid",
                "Agency/Incident/Grid",
                new { controller = this.Name, action = "IncidentGrid" });
            routes.MapRoute(
                "IncidentEdit",
                "Agency/Incident/Edit",
                new { controller = this.Name, action = "IncidentEdit", id = new IsGuid() });
            routes.MapRoute(
                "IncidentUpdate",
                "Agency/Incident/Update",
                new { controller = this.Name, action = "IncidentUpdate", infection = UrlParameter.Optional });
            routes.MapRoute(
                "IncidentPrintPreview",
                "Agency/Incident/PrintPreview",
                new { controller = this.Name, action = "IncidentPrintPreview", episodeId = new IsGuid(), patientId = new IsGuid(), eventId = new IsGuid() });
            routes.MapRoute(
                "IncidentPrint",
                "Agency/Incident/Print",
                new { controller = this.Name, action = "IncidentPrint", episodeId = UrlParameter.Optional, patientId = UrlParameter.Optional, eventId = UrlParameter.Optional });
            routes.MapRoute(
                "IncidentProcess",
                "Agency/Incident/Process",
                new { controller = this.Name, action = "IncidentProcess", id = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "IncidentExport",
                "Agency/Incident/Export",
                new { controller = "Export", action = "Incident", id = new IsGuid() });
            #endregion

            #region Template
            routes.MapRoute(
                "TemplateNew",
                "Agency/Template/New",
                new { controller = this.Name, action = "TemplateNew" });
            routes.MapRoute(
                "TemplateCreate",
                "Agency/Template/Create",
                new { controller = this.Name, action = "TemplateCreate", template = UrlParameter.Optional });
            routes.MapRoute(
                "TemplateGet",
                "Agency/Template/Get",
                new { controller = this.Name, action = "TemplateGet", id = new IsGuid() });
            routes.MapRoute(
                "TemplateList",
                "Agency/Template/List",
                new { controller = this.Name, action = "TemplateList" });
            routes.MapRoute(
                "TemplateGrid",
                "Agency/Template/Grid",
                new { controller = this.Name, action = "TemplateGrid" });
            routes.MapRoute(
                "TemplateEdit",
                "Agency/Template/Edit",
                new { controller = this.Name, action = "TemplateEdit", id = new IsGuid() });
            routes.MapRoute(
                "TemplateUpdate",
                "Agency/Template/Update",
                new { controller = this.Name, action = "TemplateUpdate", template = UrlParameter.Optional });
            routes.MapRoute(
                "TemplateDelete",
                "Agency/Template/Delete",
                new { controller = this.Name, action = "TemplateDelete", id = new IsGuid() });
            routes.MapRoute(
                "TemplateLogs",
                "Agency/Template/Logs",
                new { controller = this.Name, action = "TemplateLogs", id = new IsGuid() });
            routes.MapRoute(
                "TemplateExport",
                "Agency/Template/Export",
                new { controller = "Export", action = "Template", id = new IsGuid() });
            #endregion

            #region Supply
            routes.MapRoute(
                "SupplyNew",
                "Agency/Supply/New",
                new { controller = this.Name, action = "SupplyNew" });
            routes.MapRoute(
                "SupplyCreate",
                "Agency/Supply/Create",
                new { controller = this.Name, action = "SupplyCreate", supply = UrlParameter.Optional });
            routes.MapRoute(
                "SupplyGet",
                "Agency/Supply/Get",
                new { controller = this.Name, action = "SupplyGet", id = new IsGuid() });
            routes.MapRoute(
                "SupplySearch",
                "Agency/Supply/Search",
                new { controller = this.Name, action = "SupplySearch", id = UrlParameter.Optional });
            routes.MapRoute(
                "SupplyList",
                "Agency/Supply/List",
                new { controller = this.Name, action = "SupplyList" });
            routes.MapRoute(
                "SupplyGrid",
                "Agency/Supply/Grid",
                new { controller = this.Name, action = "SupplyGrid" });
            routes.MapRoute(
                "SupplyEdit",
                "Agency/Supply/Edit",
                new { controller = this.Name, action = "SupplyEdit", id = new IsGuid() });
            routes.MapRoute(
                "SupplyUpdate",
                "Agency/Supply/Update",
                new { controller = this.Name, action = "SupplyUpdate", supply = UrlParameter.Optional });
            routes.MapRoute(
                "SupplyDelete",
                "Agency/Supply/Delete",
                new { controller = this.Name, action = "SupplyDelete", id = new IsGuid() });
            routes.MapRoute(
                "SupplyLogs",
                "Agency/Supply/Logs",
                new { controller = this.Name, action = "SupplyLogs", id = new IsGuid() });
            routes.MapRoute(
                "SupplyExport",
                "Agency/Supply/Export",
                new { controller = "Export", action = "Supply", id = new IsGuid() });
            #endregion

            #region Orders
            routes.MapRoute(
                "OrdersToBeSentList",
                "Agency/Orders/ToBeSent/List",
                new { controller = this.Name, action = "OrdersToBeSentList" });
            routes.MapRoute(
                "OrdersToBeSentGrid",
                "Agency/Orders/ToBeSent/Grid",
                new { controller = this.Name, action = "OrdersToBeSentGrid" });
            routes.MapRoute(
                "OrdersHistoryList",
                "Agency/Orders/History/List",
                new { controller = this.Name, action = "OrdersHistoryList" });
            routes.MapRoute(
                "OrdersHistoryGrid",
                "Agency/Orders/History/Grid",
                new { controller = this.Name, action = "OrdersHistoryGrid" });
            routes.MapRoute(
                "OrdersPendingSignatureList",
                "Agency/Orders/PendingSignature/List",
                new { controller = this.Name, action = "OrdersPendingSignatureList" });
            routes.MapRoute(
                "OrdersPendingSignatureGrid",
                "Agency/Orders/PendingSignature/Grid",
                new { controller = this.Name, action = "OrdersPendingSignatureGrid" });
            routes.MapRoute(
                "OrdersSend",
                "Agency/Orders/Send",
                new { controller = this.Name, action = "OrdersSend" });
            routes.MapRoute(
                "OrdersHistoryEdit",
                "Agency/Orders/History/Edit",
                new { controller = this.Name, action = "OrdersHistoryEdit" });
            routes.MapRoute(
                "OrdersHistoryUpdate",
                "Agency/Orders/History/Update",
                new { controller = this.Name, action = "OrdersHistoryUpdate" });
            routes.MapRoute(
                "OrdersPendingSignatureReceive",
                "Agency/Orders/PendingSignature/Receive",
                new { controller = this.Name, action = "OrdersPendingSignatureReceive" });
            routes.MapRoute(
                "OrdersReceive",
                "Agency/Orders/Receive",
                new { controller = this.Name, action = "OrdersReceive" });
            routes.MapRoute(
                "OrdersToBeSentExport",
                "Agency/Orders/ToBeSent/Export",
                new { controller = "Export", action = "OrdersToBeSent", id = new IsGuid() });
            routes.MapRoute(
                "OrdersPendingSignatureExport",
                "Agency/Orders/PendingSignature/Export",
                new { controller = "Export", action = "OrdersPendingSignature", id = new IsGuid() });
            routes.MapRoute(
                "OrdersHistoryExport",
                "Agency/Orders/History/Export",
                new { controller = "Export", action = "OrdersHistory", id = new IsGuid() });
            #endregion

            #region Recerts
            routes.MapRoute(
                "RecertsPastDueWidget",
                "Agency/Recerts/PastDue/Widget",
                new { controller = this.Name, action = "RecertsPastDueWidget" });
            routes.MapRoute(
                "RecertsPastDueList",
                "Agency/Recerts/PastDue/List",
                new { controller = this.Name, action = "RecertsPastDueList" });
            routes.MapRoute(
                "RecertsPastDueGrid",
                "Agency/Recerts/PastDue/Grid",
                new { controller = this.Name, action = "RecertsPastDueGrid" });
            routes.MapRoute(
                "RecertsPastDueExport",
                "Agency/Recerts/PastDue/Export",
                new { controller = "Export", action = "RecertsPastDue", id = new IsGuid() });
            routes.MapRoute(
                "RecertsUpcomingWidget",
                "Agency/Recerts/Upcoming/Widget",
                new { controller = this.Name, action = "RecertsUpcomingWidget" });
            routes.MapRoute(
                "RecertsUpcomingList",
                "Agency/Recerts/Upcoming/List",
                new { controller = this.Name, action = "RecertsUpcomingList" });
            routes.MapRoute(
                "RecertsUpcomingGrid",
                "Agency/Recerts/Upcoming/Grid",
                new { controller = this.Name, action = "RecertsUpcomingGrid" });
            routes.MapRoute(
                "RecertsUpcomingExport",
                "Agency/Recerts/Upcoming/Export",
                new { controller = "Export", action = "RecertsUpcoming", id = new IsGuid() });
            #endregion

            #region Print Queue
            routes.MapRoute(
                "PrintQueueGrid",
                "Agency/PrintQueue/Grid",
                new { controller = this.Name, action = "PrintQueueGrid", id = UrlParameter.Optional });
            routes.MapRoute(
                "PrintQueueExport",
                "Agency/PrintQueue/Export",
                new { controller = "Export", action = "PrintQueue" });
            #endregion
        }
    }
}
