﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;
    using System.Web.Routing;
    using System.Web.Mvc;

    public class BillingModule : Module
    {

        public override string Name
        {
            get { return "Billing"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {
            #region Claim
            routes.MapRoute(
                "ClaimPrintPreview",
                "Billing/Claim/PrintPreview/{branchId}/{insuranceId}/{sortType}/{claimType}",
                new { controller = this.Name, action = "ClaimPrintPreview", branchId = new IsGuid() });
            routes.MapRoute(
                "ClaimPrint",
                "Billing/Claim/Print/{branchId}/{insuranceId}/{sortType}/{claimType}",
                new { controller = this.Name, action = "ClaimPrint", branchId = new IsGuid() });
            routes.MapRoute(
                "ClaimExport",
                "Billing/Claim/Export",
                new { controller = this.Name, action = "ClaimExport" });
            routes.MapRoute(
                "ClaimGenerate",
                "Billing/Claim/Generate",
                new { controller = this.Name, action = "ClaimGenerate" });
            routes.MapRoute(
                "ClaimCreateANSI",
                "Billing/Claim/CreateANSI",
                new { controller = this.Name, action = "ClaimCreateANSI" });
            routes.MapRoute(
                "ClaimSubmitDirectly",
                "Billing/Claim/SubmitDirectly",
                new { controller = this.Name, action = "ClaimSubmitDirectly" });
            routes.MapRoute(
                "ClaimUpdateStatus",
                "Billing/Claim/UpdateStatus",
                new { controller = this.Name, action = "ClaimUpdateStatus" });
            routes.MapRoute(
                "ClaimSummary",
                "Billing/Claim/Summary",
                new { controller = this.Name, action = "ClaimSummary" });
            #endregion
            #region Claim RAP
            routes.MapRoute(
                "ClaimRAPCenter",
                "Billing/Claim/RAP/Center",
                new { controller = this.Name, action = "ClaimRAPCenter" });
            routes.MapRoute(
                "ClaimRAPGrid",
                "Billing/Claim/RAP/Grid",
                new { controller = this.Name, action = "ClaimRAPGrid" });
            routes.MapRoute(
                "ClaimRAP",
                "Billing/Claim/RAP",
                new { controller = this.Name, action = "ClaimRAP" });
            routes.MapRoute(
                "ClaimRAPPrint",
                "Billing/Claim/RAP/Print/{episodeId}/{patientId}",
                new { controller = this.Name, action = "ClaimRAPPrint", episodeId = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "ClaimRAPPrintPreview",
                "Billing/Claim/RAP/PrintPreview/{episodeId}/{patientId}",
                new { controller = this.Name, action = "ClaimRAPPrintPreview", episodeId = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "ClaimRAPVerify",
                "Billing/Claim/RAP/Verify",
                new { controller = this.Name, action = "ClaimRAPVerify" });
            routes.MapRoute(
                "ClaimRAPSnapshotGrid",
                "Billing/Claim/RAP/Snapshot/Grid",
                new { controller = this.Name, action = "ClaimRAPSnapshotGrid" });
            routes.MapRoute(
                "ClaimRAPSnapshotEdit",
                "Billing/Claim/RAP/Snapshot/Edit",
                new { controller = this.Name, action = "ClaimRAPSnapshotEdit" });
            routes.MapRoute(
                "ClaimRAPSnapshotUpdate",
                "Billing/Claim/RAP/Snapshot/Update",
                new { controller = this.Name, action = "ClaimRAPSnapshotUpdate" });
            #endregion
            #region Claim Final
            routes.MapRoute(
                "ClaimFinalCenter",
                "Billing/Claim/Final/Center",
                new { controller = this.Name, action = "ClaimFinalCenter" });
            routes.MapRoute(
                "ClaimFinalGrid",
                "Billing/Claim/Final/Grid",
                new { controller = this.Name, action = "ClaimFinalGrid" });
            routes.MapRoute(
                "ClaimFinal",
                "Billing/Claim/Final",
                new { controller = this.Name, action = "ClaimFinal" });
            routes.MapRoute(
                "ClaimFinalPrint",
                "Billing/Claim/Final/Print/{episodeId}/{patientId}",
                new { controller = this.Name, action = "ClaimFinalPrint", episodeId = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "ClaimFinalPrintPreview",
                "Billing/Claim/Final/PrintPreview/{episodeId}/{patientId}",
                new { controller = this.Name, action = "ClaimFinalPrintPreview", episodeId = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "ClaimFinalCategory",
                "Billing/Claim/Final/Category",
                new { controller = this.Name, action = "ClaimFinalCategory" });
            routes.MapRoute(
                "ClaimFinalVerifyDemographics",
                "Billing/Claim/Final/VerifyDemographics",
                new { controller = this.Name, action = "ClaimFinalVerifyDemographics" });
            routes.MapRoute(
                "ClaimFinalVerifySupplies",
                "Billing/Claim/Final/VerifySupplies",
                new { controller = this.Name, action = "ClaimFinalVerifySupplies" });
            routes.MapRoute(
                "ClaimFinalVerifyVisits",
                "Billing/Claim/Final/VerifyVisits",
                new { controller = this.Name, action = "ClaimFinalVerifyVisits" });
            routes.MapRoute(
                "ClaimFinalComplete",
                "Billing/Claim/Final/Complete",
                new { controller = this.Name, action = "ClaimFinalComplete" });
            routes.MapRoute(
                "ClaimFinalSnapshotGrid",
                "Billing/Claim/Final/Snapshot/Grid",
                new { controller = this.Name, action = "ClaimFinalSnapshotGrid" });
            routes.MapRoute(
                "ClaimFinalSnapshotEdit",
                "Billing/Claim/Final/Snapshot/Edit",
                new { controller = this.Name, action = "ClaimFinalSnapshotEdit" });
            routes.MapRoute(
                "ClaimFinalSnapshotUpdate",
                "Billing/Claim/Final/Snapshot/Update",
                new { controller = this.Name, action = "ClaimFinalSnapshotUpdate" });
            #endregion
            #region Claim Final Supply
            routes.MapRoute(
                "ClaimFinalSupplyNew",
                "Billing/Claim/Final/Supply/New",
                new { controller = this.Name, action = "ClaimFinalSupplyNew" });
            routes.MapRoute(
                "ClaimFinalSupplyCreate",
                "Billing/Claim/Final/Supply/Create",
                new { controller = this.Name, action = "ClaimFinalSupplyCreate" });
            routes.MapRoute(
                "ClaimFinalSupplyBillableGrid",
                "Billing/Claim/Final/Supply/BillableGrid",
                new { controller = this.Name, action = "ClaimFinalSupplyBillableGrid" });
            routes.MapRoute(
                "ClaimFinalSupplyNonBillableGrid",
                "Billing/Claim/Final/Supply/NonBillableGrid",
                new { controller = this.Name, action = "ClaimFinalSupplyNonBillableGrid" });
            routes.MapRoute(
                "ClaimFinalSupplyChangeStatus",
                "Billing/Claim/Final/Supply/ChangeStatus",
                new { controller = this.Name, action = "ClaimFinalSupplyChangeStatus" });
            routes.MapRoute(
                "ClaimFinalSupplyEdit",
                "Billing/Claim/Final/Supply/Edit",
                new { controller = this.Name, action = "ClaimFinalSupplyEdit" });
            routes.MapRoute(
                "ClaimFinalSupplyUpdate",
                "Billing/Claim/Final/Supply/Update",
                new { controller = this.Name, action = "ClaimFinalSupplyUpdate" });
            routes.MapRoute(
                "ClaimFinalSupplyDelete",
                "Billing/Claim/Final/Supply/Delete",
                new { controller = this.Name, action = "ClaimFinalSupplyDelete" });
            #endregion
            #region Claim Managed
            routes.MapRoute(
                "ClaimManaged",
                "Billing/Claim/Managed",
                new { controller = this.Name, action = "ClaimManaged" });
            routes.MapRoute(
                "ClaimManagedCategory",
                "Billing/Claim/Managed/Category",
                new { controller = this.Name, action = "ClaimManagedCategory" });
            routes.MapRoute(
                "ClaimManagedCenter",
                "Billing/Claim/Managed/Center",
                new { controller = this.Name, action = "ClaimManagedCenter" });
            routes.MapRoute(
                "ClaimManagedGrid",
                "Billing/Claim/Managed/Grid",
                new { controller = this.Name, action = "ClaimManagedGrid" });
            routes.MapRoute(
                "ClaimManagedPrint",
                "Billing/Claim/Managed/Print/{episodeId}/{patientId}",
                new { controller = this.Name, action = "ClaimManagedPrint", episodeId = new IsGuid(), patientId = new IsGuid() });
            routes.MapRoute(
                "ClaimManagedPrintPreview",
                "Billing/Claim/Managed/PrintPreview/{episodeId}/{patientId}",
                new { controller = this.Name, action = "ClaimManagedPrintPreview", episodeId = new IsGuid(), patientId = new IsGuid() });
            #endregion
            #region Claim Pending
            routes.MapRoute(
                "ClaimPending",
                "Billing/Claim/Pending",
                new { controller = this.Name, action = "ClaimPending" });
            routes.MapRoute(
                "ClaimPendingEdit",
                "Billing/Claim/Pending/Edit",
                new { controller = this.Name, action = "ClaimPendingEdit" });
            routes.MapRoute(
                "ClaimPendingUpdate",
                "Billing/Claim/Pending/Update",
                new { controller = this.Name, action = "ClaimPendingUpdate" });
            routes.MapRoute(
                "ClaimPendingRAPGrid",
                "Billing/Claim/Pending/RAPGrid",
                new { controller = this.Name, action = "ClaimPendingRAPGrid" });
            routes.MapRoute(
                "ClaimPendingFinalGrid",
                "Billing/Claim/Pending/FinalGrid",
                new { controller = this.Name, action = "ClaimPendingFinalGrid" });
            #endregion
            #region History Medicare
            routes.MapRoute(
                "HistoryMedicare",
                "Billing/History/Medicare",
                new { controller = this.Name, action = "HistoryMedicare" });
            routes.MapRoute(
               "HistoryMedicareActivity",
               "Billing/History/Medicare/Activity",
               new { controller = this.Name, action = "HistoryMedicareActivity" });
            routes.MapRoute(
               "HistoryMedicareActivityGrid",
               "Billing/History/Medicare/Activity/Grid",
               new { controller = this.Name, action = "HistoryMedicareActivityGrid" });
            routes.MapRoute(
                "HistoryMedicareCenter",
                "Billing/History/Medicare/Content",
                new { controller = this.Name, action = "HistoryMedicareContent" });
             routes.MapRoute(
                "HistoryMedicareInfo",
                "Billing/History/Medicare/Info",
                new { controller = this.Name, action = "HistoryMedicareInfo" });
            #endregion
            #region Remittance
            routes.MapRoute(
                "RemittanceList",
                "Billing/Remittance/List",
                new { controller = this.Name, action = "RemittanceList" });
            routes.MapRoute(
                "RemittanceListGrid",
                "Billing/Remittance/List/Grid",
                new { controller = this.Name, action = "RemittanceListGrid" });
            routes.MapRoute(
                "RemittanceListPrint",
                "Billing/Remittance/List/Print/{startDate}/{endDate}",
                new { controller = this.Name, action = "RemittanceListPrint", startDate = UrlParameter.Optional, endDate = UrlParameter.Optional });
            routes.MapRoute(
                "RemittanceListPrintPreview",
                "Billing/Remittance/List/PrintPreview/{startDate}/{endDate}",
                new { controller = this.Name, action = "RemittanceListPrintPreview" });
            routes.MapRoute(
                "RemittanceDetail",
                "Billing/Remittance/Detail",
                new { controller = this.Name, action = "RemittanceDetail" });
            routes.MapRoute(
                "RemittanceDetailGrid",
                "Billing/Remittance/Detail/Grid",
                new { controller = this.Name, action = "RemittanceDetailGrid" });
            routes.MapRoute(
                "RemittanceDetailPrint",
                "Billing/Remittance/Detail/Print/{id}",
                new { controller = this.Name, action = "RemittanceDetailPrint", id = new IsGuid() });
            routes.MapRoute(
                "RemittanceDetailPrintPreview",
                "Billing/Remittance/Detail/PrintPreview/{id}",
                new { controller = this.Name, action = "RemittanceDetailPrintPreview", id = new IsGuid() });
            routes.MapRoute(
                "RemittanceDetailUpload",
                "Billing/Remittance/Detail/Upload",
                new { controller = this.Name, action = "RemittanceDetailUpload" });
            routes.MapRoute(
                "RemittanceDetailPost",
                "Billing/Remittance/Detail/Post",
                new { controller = this.Name, action = "RemittanceDetailPost" });
            routes.MapRoute(
                "RemittanceDetailDelete",
                "Billing/Remittance/Detail/Delete",
                new { controller = this.Name, action = "RemittanceDetailDelete" });
            #endregion
            #region Medicare Eligibility
            routes.MapRoute(
                "MedicareEligibilityContent",
                "Billing/MedicareEligibility/Content",
                new { controller = this.Name, action = "MedicareEligibilityContent" });
            #endregion
        }
    }
}
