﻿
namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Infrastructure;
    using System.Web.Routing;
    using System.Web.Mvc;


    public class PDScheduleModule : Module
    {

        public override string Name
        {
            get { return "PDSchedule"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {

            #region Center
            routes.MapRoute(
                "PDCenterContent",
                "PDSchedule/Center/Content",
                new { controller = this.Name, action = "CenterContent", patientId = new IsGuid(), episodeId = UrlParameter.Optional });
            routes.MapRoute(
                "PDCenterCalendar",
                "PDSchedule/Center/Calendar",
                new { controller = this.Name, action = "CenterCalendar", patientId = new IsGuid(), episodeId = UrlParameter.Optional, discipline = UrlParameter.Optional });
            routes.MapRoute(
                "PDCenterActivities",
                "PDSchedule/Center/Activities",
                new { controller = this.Name, action = "CenterActivities", patientId = new IsGuid(), episodeId = UrlParameter.Optional, discipline = UrlParameter.Optional });
            routes.MapRoute(
                "PDCenterActivitiesGrid",
                "PDSchedule/Center/ActivitiesGrid",
                new { controller = this.Name, action = "CenterActivitiesGrid", patientId = new IsGuid(), episodeId = UrlParameter.Optional, discipline = UrlParameter.Optional });
            #endregion

        }

    }
}
