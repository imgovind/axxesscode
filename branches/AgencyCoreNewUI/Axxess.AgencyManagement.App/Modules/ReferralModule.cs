﻿namespace Axxess.AgencyManagement.App.Modules
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Axxess.Core.Infrastructure;

    public class ReferralModule : Module
    {
        public override string Name
        {
            get { return "Referral"; }
        }

        public override void RegisterRoutes(RouteCollection routes)
        {

            #region Physician
            routes.MapRoute(
               "ReferralPhysicianAdd",
               "Referral/Physician/Add",
               new { controller = this.Name, action = "PhysicianAdd", referralId = new IsGuid() });
            routes.MapRoute(
               "ReferralPhysicianGrid",
               "Referral/Physician/Grid",
               new { controller = this.Name, action = "PhysicianGrid", referralId = new IsGuid() });
            routes.MapRoute(
               "ReferralPhysicianDelete",
               "Referral/Physician/Delete",
               new { controller = this.Name, action = "PhysicianDelete", id = new IsGuid(), referralId = new IsGuid() });
            routes.MapRoute(
               "ReferralPhysicianSetPrimary",
               "Referral/Physician/SetPrimary",
               new { controller = this.Name, action = "PhysicianSetPrimary", id = new IsGuid(), referralId = new IsGuid() });
            #endregion

            routes.MapRoute(
               "ReferralExport",
               "Referral/Export",
               new { controller = "Export", action = "Referral" });

        }
    }
}
