﻿namespace Axxess.AgencyManagement.App
{
    using System;
    using System.Text;
    using System.Linq;
    using System.Xml;
    using System.Xml.Xsl;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Reflection;
    using System.Collections.Generic;

    using Axxess.LookUp.Repositories;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Extensions;

    public static class HtmlHelperExtensions
    {
        #region Data Repositories

        private static IUserRepository userRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.UserRepository;
            }
        }

        private static IAssetRepository assetRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AssetRepository;
            }
        }

        private static IPatientRepository patientRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.PatientRepository;
            }
        }

        private static IAgencyRepository agencyRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.AgencyRepository;
            }
        }

        private static IPhysicianRepository physicianRepository
        {
            get
            {
                IAgencyManagementDataProvider dataProvider = Container.Resolve<IAgencyManagementDataProvider>();
                return dataProvider.PhysicianRepository;
            }
        }

        private static ILookupRepository lookupRepository
        {
            get
            {
                ILookUpDataProvider dataProvider = Container.Resolve<ILookUpDataProvider>();
                return dataProvider.LookUpRepository;
            }
        }

        private static IAssessmentRepository assessmentRepository
        {
            get
            {
                IOasisCDataProvider dataProvider = Container.Resolve<IOasisCDataProvider>();
                return dataProvider.OasisAssessmentRepository;
            }
        }

        private static ICachedDataRepository cachedDataRepository
        {
            get
            {
                IOasisCDataProvider dataProvider = Container.Resolve<IOasisCDataProvider>();
                return dataProvider.CachedDataRepository;
            }
        }

        #endregion

        #region HtmlHelper Extensions

        public static MvcHtmlString AssemblyVersion(this HtmlHelper html)
        {
            System.Version version = Assembly.GetExecutingAssembly().GetName().Version;
            var versionText = string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
            return MvcHtmlString.Create(versionText);
        }

        public static MvcHtmlString ZipCode(this HtmlHelper html)
        {
            var zipCode = "75243";
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null && agency.MainLocation != null)
            {
                zipCode = agency.MainLocation.AddressZipCode;
            }
            return MvcHtmlString.Create(zipCode);
        }

        public static MvcHtmlString Asset(this HtmlHelper html, Guid assetId)
        {
            var sb = new StringBuilder();
            if (!assetId.IsEmpty())
            {
                var asset = assetRepository.Get(assetId, Current.AgencyId);
                if (asset != null)
                {
                    sb.AppendFormat("<a class=\"link\" href=\"/Asset/{0}\">{1}</a>&#160;", asset.Id.ToString(), asset.FileName);
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Recipients(this HtmlHelper html)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"recipient-list\" id=\"recipient-list\">");
            sb.AppendLine("<div class=\"compose-header\">Recipient List</div>");
            sb.AppendLine("<div class=\"recipient-panel\">");
            sb.AppendLine("<div class=\"recipient\">");
            sb.AppendLine("<input type=\"checkbox\" class=\"contact\" id=\"NewMessage_SelectAllRecipients\" />");
            sb.AppendLine("<label for=\"NewMessage_SelectAllRecipients\" class=\"strong\">Select All</label>");
            sb.AppendLine("</div>"); // recipient
            var recipients = userRepository.GetAgencyUsers(Current.AgencyId);
            if (recipients.Count > 0) {
                int counter = 1;
                recipients.ForEach(r => {
                    if (r.Id != Current.UserId) {
                        sb.AppendFormat("<div class=\"recipient{0}\">", counter % 2 == 1 ? " t-alt" : string.Empty);
                        sb.AppendFormat("<input name=\"Recipients\" type=\"checkbox\" id=\"NewMessage_Recipient_{0}\" value=\"{1}\" title=\"{2}\" />", counter.ToString(), r.Id.ToString(), r.DisplayName);
                        sb.AppendFormat("<label for=\"NewMessage_Recipient_{0}\">{1}</label>", counter.ToString(), r.DisplayName);
                        sb.AppendLine("</div>");
                        sb.AppendLine();
                        counter++;
                    }
                });
            }
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Recipients(this HtmlHelper html, string prefix, List<Guid> selectedRecipients) {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"checkgroup\">");
            var recipients = userRepository.GetAgencyUsers(Current.AgencyId);
            if (recipients.Count > 0) for (int i = 0; i < recipients.Count; i++) sb.AppendFormat("<div class=\"option\"><input name=\"RecipientArray\" type=\"checkbox\" id=\"{0}_Recipient{1}\" value=\"{2}\" title=\"{3}\"{4} /><label for=\"{0}_Recipient{1}\">{3}</label></div>", prefix, i.ToString(), recipients[i].Id.ToString(), recipients[i].DisplayName, selectedRecipients.Contains(recipients[i].Id) ? " checked='checked'" : string.Empty);
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Templates(this HtmlHelper html, string name, object htmlAttributes)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"template-selector\"><label class=\"strong\">Templates</label>");
            List<SelectListItem> items = new List<SelectListItem>();
            items.Insert(0, new SelectListItem { Text = "-- Select Template --", Value = "" });
            items.Insert(items.Count, new SelectListItem { Text = "-- Erase --", Value = "empty" });
            var templates = agencyRepository.GetTemplates(Current.AgencyId);
            if (templates != null && templates.Count > 0)
            {
                items.Insert(items.Count, new SelectListItem { Text = "------------------------------", Value = "spacer" });
                templates.ForEach(t => {
                    items.Add(new SelectListItem { Text = t.Title, Value = t.Id.ToString() });
                });
            }
            sb.AppendLine(html.DropDownList(name, items.AsEnumerable(), htmlAttributes).ToString());
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Templates(this HtmlHelper html, string name)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div class=\"template-selector\"><label class=\"strong\"> Templates</label>");
            List<SelectListItem> items = new List<SelectListItem>();
            items.Insert(0, new SelectListItem { Text = "-- Select Template --", Value = "" });
            items.Insert(items.Count, new SelectListItem { Text = "-- Erase --", Value = "empty" });
            var templates = agencyRepository.GetTemplates(Current.AgencyId);
            if (templates != null && templates.Count > 0)
            {
                items.Insert(items.Count, new SelectListItem { Text = "------------------------------", Value = "spacer" });
                templates.ForEach(t =>
                {
                    items.Add(new SelectListItem { Text = t.Title, Value = t.Id.ToString() });
                });
            }
            sb.AppendLine(html.DropDownList(name, items.AsEnumerable()).ToString());
            sb.AppendLine("</div>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString Status(this HtmlHelper html, string name, string status, int disciplineTask, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (disciplineTask > 0)
            {
                DisciplineTasks task = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), disciplineTask.ToString());
                var taskGroup = task.GetCustomGroup();

                Array scheduleStatusValues = Enum.GetValues(typeof(ScheduleStatus));
                foreach (ScheduleStatus scheduleStatus in scheduleStatusValues)
                {
                    if (scheduleStatus.GetGroup().IsEqual(taskGroup))
                    {
                        int statusId = (int)scheduleStatus;
                        if (status.IsEqual(statusId.ToString()))
                        {
                            items.Add(new SelectListItem
                            {
                                Text = scheduleStatus.GetDescription(),
                                Value = statusId.ToString(),
                                Selected = true
                            });
                        }
                    }
                }

                string discipline = task.GetCustomCategory();
                switch (discipline.ToLower())
                {
                    case "sn":
                    case "pt":
                    case "ot":
                    case "st":
                    case "hha":
                    case "notes":
                    case "msw":
                        if (disciplineTask == (int)DisciplineTasks.PTEvaluation || disciplineTask == (int)DisciplineTasks.OTEvaluation || disciplineTask == (int)DisciplineTasks.STEvaluation || disciplineTask == (int)DisciplineTasks.PTReEvaluation || disciplineTask == (int)DisciplineTasks.OTReEvaluation || disciplineTask == (int)DisciplineTasks.STReEvaluation)
                        {
                            if (status != ((int)ScheduleStatus.NoteCompleted).ToString() && status != ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString())
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = string.Format("Completed - {0}", (ScheduleStatus.EvalReturnedWPhysicianSignature).GetDescription()),
                                    Value = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString(),
                                    Selected = false
                                });
                                items.Add(new SelectListItem
                                {
                                    Text = (ScheduleStatus.NoteCompleted).GetDescription(),
                                    Value = ((int)ScheduleStatus.NoteCompleted).ToString(),
                                    Selected = false
                                });
                            }
                            else
                            {
                                if (status != ((int)ScheduleStatus.NoteCompleted).ToString() && status == ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString())
                                {
                                    items.Add(new SelectListItem
                                    {
                                        Text = (ScheduleStatus.NoteCompleted).GetDescription(),
                                        Value = ((int)ScheduleStatus.NoteCompleted).ToString(),
                                        Selected = false
                                    });
                                }
                                if (status == ((int)ScheduleStatus.NoteCompleted).ToString() && status != ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString())
                                {
                                    items.Add(new SelectListItem
                                    {
                                        Text = string.Format("Completed - {0}", (ScheduleStatus.EvalReturnedWPhysicianSignature).GetDescription()),
                                        Value = ((int)ScheduleStatus.EvalReturnedWPhysicianSignature).ToString(),
                                        Selected = false
                                    });
                                }
                            }
                        }
                        else
                        {
                            if (status != ((int)ScheduleStatus.NoteCompleted).ToString())
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = (ScheduleStatus.NoteCompleted).GetDescription(),
                                    Value = ((int)ScheduleStatus.NoteCompleted).ToString(),
                                    Selected = false
                                });
                            }
                        }
                        break;
                    case "485":
                    case "486":
                    case "order":
                    case "nonoasis485":
                        if (status != ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString())
                        {
                            items.Add(new SelectListItem
                            {
                                Text = string.Format("Completed - {0}", (ScheduleStatus.OrderReturnedWPhysicianSignature).GetDescription()),
                                Value = ((int)ScheduleStatus.OrderReturnedWPhysicianSignature).ToString(),
                                Selected = false
                            });
                        }
                        break;
                    case "reportsandnotes":
                        if (status != ((int)ScheduleStatus.ReportAndNotesCompleted).ToString())
                        {
                            items.Add(new SelectListItem
                            {
                                Text = (ScheduleStatus.ReportAndNotesCompleted).GetDescription(),
                                Value = ((int)ScheduleStatus.ReportAndNotesCompleted).ToString(),
                                Selected = false
                            });
                        }
                        break;
                    case "oasis":
                    case "nonoasis":
                        if (status != ((int)ScheduleStatus.OasisExported).ToString() && status != ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                        {
                            items.Add(new SelectListItem
                            {
                                Text = string.Format("Completed - {0}", (ScheduleStatus.OasisExported).GetDescription()),
                                Value = ((int)ScheduleStatus.OasisExported).ToString(),
                                Selected = false
                            });
                            items.Add(new SelectListItem
                            {
                                Text = (ScheduleStatus.OasisCompletedNotExported).GetDescription(),
                                Value = ((int)ScheduleStatus.OasisCompletedNotExported).ToString(),
                                Selected = false
                            });
                        }
                        else
                        {
                            if (status == ((int)ScheduleStatus.OasisExported).ToString() && status != ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                            {
                                items.Add(new SelectListItem
                                   {
                                       Text = (ScheduleStatus.OasisCompletedNotExported).GetDescription(),
                                       Value = ((int)ScheduleStatus.OasisCompletedNotExported).ToString(),
                                       Selected = false
                                   });
                            }
                            else if (status != ((int)ScheduleStatus.OasisExported).ToString() && status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = string.Format("Completed - {0}", (ScheduleStatus.OasisExported).GetDescription()),
                                    Value = ((int)ScheduleStatus.OasisExported).ToString(),
                                    Selected = false
                                });
                            }

                        }
                        break;
                }
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Agencies(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var agencies = agencyRepository.All();
            tempItems = from agency in agencies
                        select new SelectListItem
                        {
                            Text = agency.Name,
                            Value = agency.Id.ToString(),
                            Selected = (agency.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Agency --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PreviousNotes(this HtmlHelper html, List<ScheduleEvent> previousNotes, object htmlAttributes)
        {
            var items = previousNotes.Select(e => new SelectListItem
                        {
                            Text = string.Format("{0} {1}", e.DisciplineTaskName, e.EventDate.ToString("MM/dd/yyyy")),
                            Value = e.EventId.ToString()
                        }).ToList() ?? new List<SelectListItem>();

            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Previous Notes --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList("PreviousNotes", items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PreviousAssessments(this HtmlHelper html, Guid patientId, Guid assessmentId, int assessmentType, DateTime scheduleDate, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var scheduleEvents = new List<ScheduleEvent>();
            if (assessmentType > 0 && assessmentType < 10)
            {
                var disciplineTasks = assessmentType < 5 ? new int[] { (int)DisciplineTasks.OASISCStartofCare, (int)DisciplineTasks.OASISCStartofCarePT, (int)DisciplineTasks.OASISCStartofCareOT, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.OASISCRecertificationPT } : new int[] { (int)DisciplineTasks.NonOASISStartofCare, (int)DisciplineTasks.NonOASISRecertification };
                scheduleEvents = patientRepository.GetPatientScheduledEventsLeanWithUserId(Current.AgencyId, patientId, new int[] { (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedNotExported }, new string[] { }, disciplineTasks, true, scheduleDate.AddMonths(-12), scheduleDate.AddDays(-1), false, false, 5);
                //var episodes = patientRepository.GetEpisodeData(Current.AgencyId, patientId, scheduleDate.AddMonths(-12), scheduleDate);
                //if (episodes != null && episodes.Count > 0)
                //{
                //    episodes.ForEach(e =>
                //    {
                //        var evets = e.Schedule.ToObject<List<ScheduleEvent>>().Where(v => !v.IsMissedVisit && !v.IsDeprecated && v.EventDate.IsValidDate() && v.EventDate.ToDateTime().Date < scheduleDate.Date && (assessmentType < 5 ? (v.IsOASISSOC() || v.IsOASISROC() || v.IsOASISRecert()) : (v.IsNONOASISSOC() || v.IsNONOASISRecert())) && (v.Status == ((int)ScheduleStatus.OasisCompletedPendingReview).ToString() || v.Status == ((int)ScheduleStatus.OasisCompletedExportReady).ToString() || v.Status == ((int)ScheduleStatus.OasisExported).ToString() || v.Status == ((int)ScheduleStatus.OasisCompletedNotExported).ToString())).ToList();
                //        if (evets != null && evets.Count > 0)
                //        {
                //            scheduleEvents.AddRange(evets);
                //        }
                //    });
                //}
            }
            //var shortList = scheduleEvents.OrderByDescending(v => v.EventDate.ToDateTime().Date).Take(5).ToList();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                scheduleEvents.ForEach(v =>
                {
                    if (Enum.IsDefined(typeof(DisciplineTasks), v.DisciplineTask))
                    {
                        var user = users.SingleOrDefault(u => u.Id == v.UserId);
                        items.Add(new SelectListItem { Text = string.Format("{0} {1} by {2}", ((DisciplineTasks)v.DisciplineTask).GetDescription(), v.EventDate.ToString("MM/dd/yyyy"), user != null ? user.DisplayName : string.Empty), Value = string.Format("{0}_{1}", v.EventId, ((DisciplineTasks)v.DisciplineTask).ToString()) });
                    }
                });
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select --",
                Value = string.Empty
            });
            return html.DropDownList("PreviousAssessments", items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem
                         {
                             Text = standardInsurance.Name,
                             Value = standardInsurance.Id.ToString(),
                             Selected = (standardInsurance.Id.ToString().IsEqual(value))
                         });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "0"
            });
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesFilter(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem
                        {
                            Text = standardInsurance.Name,
                            Value = standardInsurance.Id.ToString(),
                            Selected = (standardInsurance.Id.ToString().IsEqual(value))
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "0"
            });
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            items.Add(new SelectListItem()
                {
                    Text = "No Insurance assigned",
                    Value = "-1"
                });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesMedicare(this HtmlHelper html, string name, string value, bool isSelectAll, string title, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId))
                {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null)
                    {
                        items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = (standardInsurance.Id.ToString().IsEqual(value)) });
                    }
                }
            }

            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                if (i.PayorType == 2 || i.PayorType == 3)
                {
                    items.Add(new SelectListItem() { Text = i.Name, Value = i.Id.ToString(), Selected = (i.Id.ToString().IsEqual(value)) });
                }
            });
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem { Text = title, Value = "0" });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesNoneMedicare(this HtmlHelper html, string name, string value, bool isSelectAll, string title, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = title,
                    Value = "0"
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, bool addNew, object htmlAttributes) {
            List<SelectListItem> items = new List<SelectListItem>();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null) {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId)) {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null) {
                        items.Add(new SelectListItem {
                            Text = standardInsurance.Name,
                            Value = standardInsurance.Id.ToString(),
                            Selected = (standardInsurance.Id.ToString().IsEqual(value))
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem {
                Text = "-- Select Insurance --",
                Value = "0"
            });
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i => {
                items.Add(new SelectListItem() {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            if (addNew) {
                items.Insert(1, new SelectListItem {
                    Text = "",
                    Value = "spacer"
                });
                items.Insert(2, new SelectListItem {
                    Text = "** Add New Insurance **",
                    Value = "new"
                });
                items.Insert(3, new SelectListItem {
                    Text = "",
                    Value = "spacer"
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Insurances(this HtmlHelper html, string name, string value, bool addNew, IDictionary<string, string> htmlAttributes) {
            var attributes = new StringBuilder();
            var selectList = new StringBuilder();
            if (htmlAttributes != null && htmlAttributes.Count > 0) htmlAttributes.ForEach(kvp => attributes.AppendFormat("{0}='{1}' ", kvp.Key, kvp.Value));
            selectList.AppendFormat("<select name={0} {1}><option value='{2}' IsHmo='{3}'>-- Select Insurance --</option>", name, attributes.ToString(), "0", 0);
            if (addNew) {
                selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>{2}</option>", "spacer", 0, "");
                selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>{2}</option>", "new", 0, "** Add New Insurance **");
                selectList.AppendFormat("<option value='{0}' IsHmo='{1}'>{2}</option>", "spacer", 0, "");
            }
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null) {
                var payorId = 0;
                if (int.TryParse(agency.Payor, out payorId)) {
                    var standardInsurance = lookupRepository.GetInsurance(payorId);
                    if (standardInsurance != null) selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", standardInsurance.Id.ToString(), 0, ((standardInsurance.Id.ToString().IsEqual(value)) ? " selected" : ""), standardInsurance.Name);
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(insurance => {
                selectList.AppendFormat("<option value='{0}' IsHmo='{1}'{2}>{3}</option>", insurance.Id.ToString(), 1, ((insurance.Id.ToString().IsEqual(value)) ? " selected=\"selected\"" : ""), insurance.Name);
            });
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString ReportBranchList(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var branches = agencyRepository.GetBranches(Current.AgencyId);
            tempItems = from branch in branches
                        select new SelectListItem
                        {
                            Text = branch.Name,
                            Value = branch.Id.ToString(),
                            Selected = (branch.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- All Branches --",
                Value = Guid.Empty.ToString(),
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString BranchOnlyList(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var branches = agencyRepository.GetBranches(Current.AgencyId);
            tempItems = from branch in branches
                        select new SelectListItem
                        {
                            Text = branch.Name,
                            Value = branch.Id.ToString(),
                            Selected = (branch.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString MedicationTypes(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();

            items.Add(new SelectListItem { Text = "New", Value = "N", Selected = value.IsEqual("N") });
            items.Add(new SelectListItem { Text = "Changed", Value = "C", Selected = value.IsEqual("C") });
            items.Add(new SelectListItem { Text = "Unchanged", Value = "U", Selected = value.IsEqual("U") });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DisciplineTaskList(this HtmlHelper html, string discipline, string name, string value, IDictionary<string, string> htmlAttributes)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder attributes = new StringBuilder();
            if (htmlAttributes != null && htmlAttributes.Count > 0) htmlAttributes.ForEach(kvp => attributes.AppendFormat("{0}='{1}' ", kvp.Key, kvp.Value));
            sb.AppendFormat("<select name=\"{0}\" id=\"{0}\"{1}>", name, attributes.ToString());
            sb.Append("<option value=\"\">-- Select Task --</option>");
            var TaskList = lookupRepository.DisciplineTasks(discipline);
            TaskList.ForEach(task => {
                var typeofTask = task.BillDisciplineIdentify();
                var rate = new ChargeRate();
                BillingService.SetBillRateValue(rate, typeofTask);
                sb.AppendFormat("<option value=\"{1}\" isbillable=\"{2}\"{3}>{0}</option>", task.Task, task.Id.ToString(), task.IsBillable.ToString().ToLower(), task.Id.ToString().IsEqual(value) ? " selected=\"selected\"" : string.Empty);
            });
            sb.Append("</select>");
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString DisciplineTaskList(this HtmlHelper html, string discipline, string name)
        {
            return html.DisciplineTaskList(discipline, name, string.Empty, null);
        }

        public static MvcHtmlString MultipleDisciplineTasks(this HtmlHelper html, string name, string value, IDictionary<string, string> htmlAttributes)
        {
            var tasks = lookupRepository.DisciplineTasks()
                .Where(d => d.Discipline == "Nursing" || d.Discipline == "PT" || d.Discipline == "ST" || d.Discipline == "OT" || d.Discipline == "HHA" || d.Discipline == "MSW" || d.Discipline == "Orders" || d.Discipline == "ReportsAndNotes")
                .OrderBy(d => d.Task)
                .ToList();

            var attributes = new StringBuilder();
            var selectList = new StringBuilder();
            if (htmlAttributes != null && htmlAttributes.Count > 0)
            {
                htmlAttributes.ForEach(kvp => attributes.AppendFormat("{0}='{1}' ", kvp.Key, kvp.Value));
            }
            selectList.AppendFormat("<select name={0} {1}><option value=''>-- Select Discipline --</option>", name, attributes.ToString());
            tasks.ForEach(task =>
            {
                selectList.AppendFormat("<option value='{0}' IsBillable='{1}' discipline='{2}'{3}>{4}</option>", task.Id.ToString(), task.IsBillable.ToString().ToLower(), task.Discipline, ((task.Id.ToString().IsEqual(value)) ? " selected" : ""), task.Task);
            });
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString MultipleDisciplineTasks(this HtmlHelper html, string name) {
            return html.MultipleDisciplineTasks(name, string.Empty, null);
        }
    
        public static MvcHtmlString StatusPatients(this HtmlHelper html, string name, string excludedValue, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            Array patientStatusValues = Enum.GetValues(typeof(PatientStatus));
            foreach (PatientStatus patientStatus in patientStatusValues)
            {
                var statusId = (int)patientStatus;
                if (statusId.ToString() != excludedValue && statusId != (int)PatientStatus.Deprecated)
                {
                    items.Add(new SelectListItem
                    {
                        Text = patientStatus.GetDescription(),
                        Value = statusId.ToString()
                    });
                }
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LookupSelectList(this HtmlHelper html, SelectListTypes listType, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            switch (listType)
            {
                case SelectListTypes.States:
                    var states = lookupRepository.States();
                    tempItems = from state in states
                                select new SelectListItem
                                {
                                    Text = state.Name,
                                    Value = state.Code,
                                    Selected = (state.Code.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select State --",
                        Value = " "
                    });
                    break;
                case SelectListTypes.Insurance:
                    var insurances = agencyRepository.GetInsurances(Current.AgencyId);
                    tempItems = from insurance in insurances
                                select new SelectListItem
                                {
                                    Text = insurance.Name,
                                    Value = insurance.Id.ToString(),
                                    Selected = (insurance.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Insurance --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.AuthorizationStatus:
                    var authStatusTypes = new List<string>();
                    Array authStatusValues = Enum.GetValues(typeof(AuthorizationStatusTypes));
                    foreach (AuthorizationStatusTypes authStatusType in authStatusValues)
                    {
                        authStatusTypes.Add(authStatusType.GetDescription());
                    }
                    tempItems = from type in authStatusTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;
                case SelectListTypes.CahpsVendors:
                    var cahpsVendors = new Dictionary<string, int>();
                    Array cahpsVendorValues = Enum.GetValues(typeof(CahpsVendors));
                    foreach (CahpsVendors cahpsVendor in cahpsVendorValues)
                    {
                        cahpsVendors.Add(cahpsVendor.GetDescription(), (int)cahpsVendor);
                    }

                    tempItems = from type in cahpsVendors
                                select new SelectListItem
                                {
                                    Text = type.Key,
                                    Value = type.Value.ToString(),
                                    Selected = (type.Value.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;
                case SelectListTypes.ContactTypes:
                    var contactTypes = new List<string>();
                    Array contactTypeValues = Enum.GetValues(typeof(ContactTypes));
                    foreach (ContactTypes contactType in contactTypeValues)
                    {
                        contactTypes.Add(contactType.GetDescription());
                    }

                    tempItems = from type in contactTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Contact Type --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.LicenseTypes:
                    var licenseTypes = new List<string>();
                    Array licenseTypeValues = Enum.GetValues(typeof(LicenseTypes));
                    foreach (LicenseTypes licenseType in licenseTypeValues)
                    {
                        licenseTypes.Add(licenseType.GetDescription());
                    }

                    tempItems = from type in licenseTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select License Type --",
                        Value = " ",
                    });
                    break;
                case SelectListTypes.CredentialTypes:
                    var credentialTypes = new List<string>();
                    Array credentialValues = Enum.GetValues(typeof(CredentialTypes));
                    foreach (CredentialTypes credentialType in credentialValues)
                    {
                        credentialTypes.Add(credentialType.GetDescription());
                    }

                    tempItems = from type in credentialTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Unknown --",
                        Value = ""
                    });
                    break;
                case SelectListTypes.TitleTypes:
                    var titleTypes = new List<string>();
                    Array titleValues = Enum.GetValues(typeof(TitleTypes));
                    foreach (TitleTypes titleType in titleValues)
                    {
                        titleTypes.Add(titleType.GetDescription());
                    }

                    tempItems = from type in titleTypes
                                select new SelectListItem
                                {
                                    Text = type,
                                    Value = type,
                                    Selected = (type.IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Title Type --",
                        Value = "",
                    });
                    break;
                case SelectListTypes.AdmissionSources:
                    var adminSources = lookupRepository.AdmissionSources();
                    tempItems = from source in adminSources
                                select new SelectListItem
                                {
                                    Text = string.Format("({0}) {1}", source.Code, source.Description),
                                    Value = source.Id.ToString(),
                                    Selected = (source.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Admission Source --",
                        Value = "0",
                    });
                    break;
                case SelectListTypes.Users:
                    var users = userRepository.GetUsersOnly(Current.AgencyId, (int)UserStatus.Active);
                    users = users.OrderBy(u => u.DisplayName).ToList();
                    tempItems = from user in users
                                select new SelectListItem
                                {
                                    Text = user.DisplayName,
                                    Value = user.Id.ToString(),
                                    Selected = (user.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select User --",
                        Value = Guid.Empty.ToString(),
                    });
                    break;
                case SelectListTypes.Branches:
                    var branches = agencyRepository.GetBranches(Current.AgencyId);
                    tempItems = from branch in branches
                                select new SelectListItem
                                {
                                    Text = branch.Name,
                                    Value = branch.Id.ToString(),
                                    Selected = (branch.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    break;

                case SelectListTypes.BranchesReport:
                    var branchesReport = agencyRepository.GetBranches(Current.AgencyId);
                    tempItems = from branch in branchesReport
                                select new SelectListItem
                                {
                                    Text = branch.Name,
                                    Value = branch.Id.ToString(),
                                    Selected = (branch.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "All",
                        Value = Guid.Empty.ToString()
                    });
                    break;
                case SelectListTypes.Patients:
                    if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
                    {
                        var patients = patientRepository.FindPatientOnly((int)PatientStatus.Active, Current.AgencyId).ToList();

                        tempItems = from patient in patients
                                    select new SelectListItem
                                    {
                                        Text = patient.DisplayName.Trim().ToTitleCase(),
                                        Value = patient.Id.ToString(),
                                        Selected = (patient.Id.ToString().IsEqual(value))
                                    };
                    }
                    else if (Current.IsClinicianOrHHA)
                    {
                        var patients = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active);

                        tempItems = from patient in patients
                                    select new SelectListItem
                                    {
                                        Text = patient.DisplayName.Trim().ToTitleCase(),
                                        Value = patient.Id.ToString(),
                                        Selected = (patient.Id.ToString().IsEqual(value))
                                    };
                    }
                    items = tempItems.OrderBy(i => i.Text).ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Patient --",
                        Value = Guid.Empty.ToString()
                    });
                    break;
                case SelectListTypes.Physicians:
                    var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId).OrderBy(p => p.DisplayName);
                    tempItems = from physician in physicians
                                select new SelectListItem
                                {
                                    Text = string.Format("{0} ({1})", physician.DisplayName, physician.NPI),
                                    Value = physician.Id.ToString(),
                                    Selected = (physician.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Physician --",
                        Value = Guid.Empty.ToString()
                    });

                    break;
                case SelectListTypes.PaymentSource:
                    var paymentSources = lookupRepository.PaymentSources();
                    tempItems = from paymentSource in paymentSources
                                select new SelectListItem
                                {
                                    Text = paymentSource.Name,
                                    Value = paymentSource.Id.ToString(),
                                    Selected = (paymentSource.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                  {
                      Text = "-- Select Payment Source --",
                      Value = "0"
                  });
                    items.Insert(11, new SelectListItem
                    {
                        Text = "Contract",
                        Value = "13"
                    });
                    break;
                default:
                    break;

                case SelectListTypes.MapAddress:
                    var addresses = agencyRepository.GetBranches(Current.AgencyId);
                    tempItems = from address in addresses
                                select new SelectListItem
                                {
                                    Text = address.Name,
                                    Value = string.Format("{0} {1}, {2}, {3} {4}", address.AddressLine1, address.AddressLine2, address.AddressCity, address.AddressStateCode, address.AddressZipCode)
                                };
                    items = tempItems.ToList();
                    if (Current.UserAddress.IsNotNullOrEmpty())
                    {
                        items.Insert(0, new SelectListItem
                        {
                            Text = "Your Address",
                            Value = Current.UserAddress
                        });
                    }
                    items.Add(new SelectListItem
                    {
                        Text = "Specify Address",
                        Value = "specify"
                    });
                    break;
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PaymentSourceWithOutMedicareTradition(this HtmlHelper html,  string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var paymentSources = lookupRepository.PaymentSources();
            tempItems = from paymentSource in paymentSources where paymentSource.Id!=3
                        select new SelectListItem
                        {
                            Text = paymentSource.Name,
                            Value = paymentSource.Id.ToString(),
                            Selected = (paymentSource.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Payment Source --",
                Value = "0"
            });
            items.Insert(11, new SelectListItem
            {
                Text = "Contract",
                Value = "13"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LookupSelectListWithBranchAndStatus(this HtmlHelper html, SelectListTypes listType, string name, string value, Guid branchId, int status, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            switch (listType)
            {
                case SelectListTypes.Users:
                    var users = new List<User>();
                    if (status == 0)
                    {
                        if (branchId.IsEmpty())
                        {
                            users = userRepository.GetUsersOnly(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
                        }
                        else
                        {
                            users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
                        }
                    }
                    else
                    {
                        if (branchId.IsEmpty())
                        {
                            users = userRepository.GetUsersByStatus(Current.AgencyId, status).OrderBy(u => u.DisplayName).ToList();
                        }
                        else
                        {
                            users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId, status).OrderBy(u => u.DisplayName).ToList();
                        }

                    }
                    tempItems = from user in users
                                select new SelectListItem
                                {
                                    Text = user.DisplayName,
                                    Value = user.Id.ToString(),
                                    Selected = (user.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select User --",
                        Value = Guid.Empty.ToString(),
                    });
                    break;

                case SelectListTypes.Patients:
                    var patients = patientRepository.Find(status, branchId, Current.AgencyId);
                    tempItems = from patient in patients
                                select new SelectListItem
                                {
                                    Text = patient.DisplayName.Trim().ToUpperCase(),
                                    Value = patient.Id.ToString(),
                                    Selected = (patient.Id.ToString().IsEqual(value))
                                };
                    items = tempItems.OrderBy(i => i.Text).ToList();
                    items.Insert(0, new SelectListItem
                    {
                        Text = "-- Select Patient --",
                        Value = Guid.Empty.ToString()
                    });
                    break;

            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Physicians(this HtmlHelper html, string name, string value, bool addNew, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var physicians = physicianRepository.GetAgencyPhysicians(Current.AgencyId).OrderBy(p => p.DisplayName);
            tempItems = from physician in physicians
                        select new SelectListItem
                        {
                            Text = string.Format("{0} ({1})", physician.DisplayName, physician.NPI),
                            Value = physician.Id.ToString(),
                            Selected = (physician.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Physician --",
                Value = Guid.Empty.ToString()
            });

            if (addNew)
            {
                items.Insert(1, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
                items.Insert(2, new SelectListItem
                {
                    Text = "** Add New Physician **",
                    Value = "new"
                });
                items.Insert(3, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PermissionList(this HtmlHelper html, string category, List<string> userPermissions)
        {
            var sb = new StringBuilder();
            sb.Append("<div class=\"row\">");
            sb.AppendFormat("<label class=\"fl strong tall-line\">{0} &#8211; </label>", category);
            sb.Append("<div class=\"checkgroup fr\">");
            sb.Append("<div class=\"option\">");
            sb.AppendFormat("<input id=\"{1}_{0}Permissions\" type=\"checkbox\" class=\"select-all-group\" group=\"{0}\" value=\"\" />", category.Replace(" ", ""), userPermissions != null && userPermissions.Count > 0 ? "EditUser" : "NewUser");
            sb.AppendFormat("<label for=\"{2}_{0}Permissions\">Select all {1}</label>", category.Replace(" ", ""), category, userPermissions != null && userPermissions.Count > 0 ? "EditUser" : "NewUser");
            sb.Append("</div>"); // Option
            sb.Append("</div>"); // Checkgroup
            sb.Append("<div class=\"wide checkgroup\">");
            Permissions[] permissions = (Permissions[])Enum.GetValues(typeof(Permissions));
            foreach (Permissions permission in permissions) {
                Permission p = permission.GetPermission();
                if (p.Category == category) {
                    sb.AppendFormat("<div class=\"option\" tooltip=\"{0}\">", p.Tip);
                    ulong id = (ulong)permission;
                    string controlChecked = string.Empty;
                    string controlId = string.Format("NewUser_Permission_{0}", id.ToString());
                    if (userPermissions != null && userPermissions.Count > 0) {
                        controlId = string.Format("EditUser_Permission_{0}", id.ToString());
                        if (userPermissions.Contains(id.ToString())) controlChecked = " Checked=\"Checked\"";
                    }
                    sb.AppendFormat("<input id=\"{0}\" type=\"checkbox\" value=\"{1}\" name=\"PermissionsArray\" class=\"{2} required\"{3} />", controlId.ToString(), id.ToString(), category.Replace(" ", ""), controlChecked);
                    sb.AppendFormat("<label for=\"{0}\"><strong>{1}</strong> &#8211; {2}</label>", controlId.ToString(), p.Description, p.LongDescription);
                    sb.AppendLine("</div>"); // Option
                }
            }
            sb.Append("</div>"); // Checkgroup
            sb.Append("</div>"); // Row
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString CaseManagers(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetCaseManagerUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Case Manager --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString HHAides(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetHHAUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Home Health Aide --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString LVNs(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetLVNUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select LVN --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Clinicians(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetClinicalUsers(Current.AgencyId).OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Clinician --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Users(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            var users = userRepository.GetUsersOnly(Current.AgencyId, (int)UserStatus.Active);
            users = users.OrderBy(u => u.DisplayName).ToList();
            tempItems = from user in users
                        select new SelectListItem
                        {
                            Text = user.DisplayName,
                            Value = user.Id.ToString(),
                            Selected = (user.Id.ToString().IsEqual(value))
                        };

            items = tempItems.ToList();
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Users --",
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Patients(this HtmlHelper html, string name, string value, int status, string title, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;

            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                var patients = patientRepository.Find(status, Guid.Empty, Current.AgencyId);
                tempItems = from patient in patients
                            select new SelectListItem
                            {
                                Text = patient.DisplayName.Trim().ToTitleCase(),
                                Value = patient.Id.ToString(),
                                Selected = (patient.Id.ToString().IsEqual(value))
                            };
            }
            else if (Current.IsClinicianOrHHA)
            {
                var patients = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)status);
                tempItems = from patient in patients
                            select new SelectListItem
                            {
                                Text = patient.DisplayName.Trim().ToTitleCase(),
                                Value = patient.Id.ToString(),
                                Selected = (patient.Id.ToString().IsEqual(value))
                            };
            }
           
            items = tempItems.OrderBy(i => i.Text).ToList();
            items.Insert(0, new SelectListItem
            {
                Text = title,
                Value = Guid.Empty.ToString()
            });

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PatientEpisodes(this HtmlHelper html, string name, string value, Guid patientId, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var episodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patientId).OrderByDescending(e => e.StartDate.ToString("yyyyMMdd")).ToList();
            if (episodes != null && episodes.Count > 0)
            {
                items = episodes.Select(e => new SelectListItem
                {
                    Text = string.Format("{0} - {1}", e.StartDate.ToString("MM/dd/yyyy"), e.EndDate.ToString("MM/dd/yyyy")),
                    Value = e.Id.ToString(),
                    Selected = (e.Id.ToString().IsEqual(value))
                }).ToList();
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString PatientEpisodes(this HtmlHelper html, string name, string value, Guid patientId, string title, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            if (!patientId.IsEmpty())
            {
                var episodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patientId).OrderByDescending(e => e.StartDate.ToString("yyyyMMdd")).ToList();
                if (episodes != null && episodes.Count > 0)
                {
                    items = episodes.Select(e => new SelectListItem
                    {
                        Text = string.Format("{0} - {1}", e.StartDate.ToString("MM/dd/yyyy"), e.EndDate.ToString("MM/dd/yyyy")),
                        Value = e.Id.ToString(),
                        Selected = (e.Id.ToString().IsEqual(value))
                    }).ToList();
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = title,
                Value = Guid.Empty.ToString()
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString Months(this HtmlHelper html, string name, string value, int start, object htmlAttributes)
        {
            List<SelectListItem> items = null;
            List<SelectListItem> tempItems = new List<SelectListItem>();

            int currentYear = DateTime.Now.Year;
            if (start < currentYear)
            {
                for (int val = start; val <= currentYear; val++)
                {
                    tempItems.Add(new SelectListItem
                    {
                        Text = val.ToString(),
                        Value = val.ToString(),
                        Selected = (val.ToString().IsEqual(value))
                    });
                }
            }
            tempItems.Reverse();
            items = tempItems.ToList();
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DisciplineTypes(this HtmlHelper html, string name, int disciplineTask, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            if (Enum.IsDefined(typeof(DisciplineTasks), disciplineTask))
            {
                var task = (DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), disciplineTask.ToString());
                var formGroup = task.GetFormGroup();
                if (formGroup.IsNotNullOrEmpty())
                {
                    Array disciplineTaskArray = Enum.GetValues(typeof(DisciplineTasks));
                    foreach (DisciplineTasks disciplineTaskItem in disciplineTaskArray)
                    {
                        if (formGroup.IsEqual(disciplineTaskItem.GetFormGroup()))
                        {
                            var data = lookupRepository.GetDisciplineTask((int)disciplineTaskItem);
                            if (data != null)
                            {
                                var typeofTask = data.BillDisciplineIdentify();
                                var rate = new ChargeRate();
                                BillingService.SetBillRateValue(rate, typeofTask);
                                items.Add(new SelectListItem
                                {
                                    Text = string.Format("{0} ({1})", data.Task, rate.Code),
                                    Value = ((int)data.Id).ToString(),
                                    Selected = ((int)data.Id == disciplineTask ? true : false)
                                });
                            }
                        }
                    }
                }
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DischargeReasons(this HtmlHelper html, string name, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            Array dischargeReasonsValues = Enum.GetValues(typeof(DischargeReasons));
            foreach (DischargeReasons dischargeReasons in dischargeReasonsValues)
            {
                items.Add(new SelectListItem
                {
                    Text = dischargeReasons.GetDescription(),
                    Value = ((int)dischargeReasons).ToString()
                });
            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Reason --",
                Value = ""
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }
        
        public static void RenderXML(this HtmlHelper html, XmlDocument xmlDocument, string XSLTPath, Dictionary<string, string> xslArgParams)
        {
            ViewContext context = html.ViewContext;
            XsltArgumentList xslArgs = new XsltArgumentList();
            if (xslArgParams != null)
            {
                foreach (string key in xslArgParams.Keys)
                {
                    xslArgs.AddParam(key, null, xslArgParams[key]);
                }
            }
            XslCompiledTransform t = new XslCompiledTransform();
            t.Load(XSLTPath);
            t.Transform(xmlDocument, xslArgs, context.HttpContext.Response.Output);
        }

        public static MvcHtmlString ClaimTypes(this HtmlHelper html, string name, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array claimTypes = Enum.GetValues(typeof(ClaimType));
            foreach (ClaimType claimType in claimTypes)
            {
                items.Add(new SelectListItem
                {
                    Text = claimType.GetDescription(),
                    Value = claimType.ToString(),
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "All",
                Value = "All ",
                Selected=true
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString UB4PatientStatus(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var ub4PatientStatusValues = Enum.GetValues(typeof(UB4PatientStatus));
            if (ub4PatientStatusValues != null && ub4PatientStatusValues.Length > 0)
            {
                foreach (UB4PatientStatus status in ub4PatientStatusValues)
                {
                        items.Add(new SelectListItem
                        {
                            Text = status.GetDescription(),
                            Value = ((int)status).ToString().PadLeft(2,'0'),
                            Selected = value == ((int)status).ToString().PadLeft(2, '0')
                        });
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Patient Status --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString BillType(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var billType = Enum.GetValues(typeof(BillType));
            if (billType != null && billType.Length > 0)
            {
                foreach (BillType bill in billType)
                {
                    items.Add(new SelectListItem
                    {
                        Text = bill.GetDescription(),
                        Value = ((int)bill).ToString(),
                        Selected = value == ((int)bill).ToString()
                    });
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Bill Type --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString UnitType(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var billUnitType = Enum.GetValues(typeof(BillUnitType));
            if (billUnitType != null && billUnitType.Length > 0)
            {
                foreach (BillUnitType unit in billUnitType)
                {
                    items.Add(new SelectListItem
                    {
                        Text = unit.GetDescription(),
                        Value = ((int)unit).ToString(),
                        Selected = value == ((int)unit).ToString()
                    });
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Unit Type --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsuranceDisciplineTask(this HtmlHelper html, string name, int disciplineTask, int insuranceId, bool isTaskIncluded ,object htmlAttributes)
        {
            var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
            var disciplines = new List<int>();
            if (insurance != null && insurance.BillData.IsNotNullOrEmpty())
            {
                disciplines = insurance.BillData.ToObject<List<ChargeRate>>().Select(i => i.Id).ToList();
            }
            var items = new List<SelectListItem>();

            var disciplineTaskArray = lookupRepository.DisciplineTasks();
            if (disciplineTaskArray != null && disciplineTaskArray.Count > 0)
            {
                foreach (var disciplineTaskItem in disciplineTaskArray)
                {
                    if ((disciplineTaskItem.Discipline == Disciplines.Nursing.ToString() || disciplineTaskItem.Discipline == Disciplines.PT.ToString() || disciplineTaskItem.Discipline == Disciplines.ST.ToString() || disciplineTaskItem.Discipline == Disciplines.OT.ToString() || disciplineTaskItem.Discipline == Disciplines.HHA.ToString() || disciplineTaskItem.Discipline == Disciplines.MSW.ToString() ) && disciplineTaskItem.Id != (int)DisciplineTasks.CommunicationNote )
                    {
                        if (isTaskIncluded && disciplines.Contains(disciplineTaskItem.Id) && disciplineTaskItem.Id != disciplineTask)
                        {
                            continue;
                        }
                        items.Add(new SelectListItem
                        {
                            Text = disciplineTaskItem.Task,
                            Value = disciplineTaskItem.Id.ToString(),
                            Selected = (disciplineTaskItem.Id == disciplineTask)
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Task --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString UserDisciplineTask(this HtmlHelper html, string name, int disciplineTask, Guid userId, bool isTaskIncluded, object htmlAttributes)
        {
            var disciplines = new List<int>();
            var items = new List<SelectListItem>();

            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null && user.Rates.IsNotNullOrEmpty())
            {
                disciplines = user.Rates.ToObject<List<UserRate>>().Select(i => i.Id).ToList();
            }

            var disciplineTaskArray = lookupRepository.DisciplineTasks();
            if (disciplineTaskArray != null && disciplineTaskArray.Count > 0)
            {
                foreach (var disciplineTaskItem in disciplineTaskArray)
                {
                    if ((disciplineTaskItem.Discipline == Disciplines.Nursing.ToString() || disciplineTaskItem.Discipline == Disciplines.PT.ToString() || disciplineTaskItem.Discipline == Disciplines.ST.ToString() || disciplineTaskItem.Discipline == Disciplines.OT.ToString() || disciplineTaskItem.Discipline == Disciplines.HHA.ToString() || disciplineTaskItem.Discipline == Disciplines.MSW.ToString()) && disciplineTaskItem.Id != (int)DisciplineTasks.CommunicationNote)
                    {
                        if (isTaskIncluded && disciplines.Contains(disciplineTaskItem.Id) && disciplineTaskItem.Id != disciplineTask)
                        {
                            continue;
                        }
                        items.Add(new SelectListItem
                        {
                            Text = disciplineTaskItem.Task,
                            Value = disciplineTaskItem.Id.ToString(),
                            Selected = (disciplineTaskItem.Id == disciplineTask)
                        });
                    }
                }
            }
            items.Insert(0, new SelectListItem
            {
                Text = " -- Select Task --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesMedicareByBranch(this HtmlHelper html, string name, string value,Guid branchId, bool isSelectAll, string title, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = (standardInsurance.Id.ToString().IsEqual(value)) });
                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = (standardInsurance.Id.ToString().IsEqual(value)) });
                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem { Text = standardInsurance.Name, Value = standardInsurance.Id.ToString(), Selected = (standardInsurance.Id.ToString().IsEqual(value)) });
                        }
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            if (agencyInsurances != null && agencyInsurances.Count > 0)
            {
                agencyInsurances.ForEach(i =>
                {
                    if (i.PayorType == 2)
                    {
                        items.Add(new SelectListItem() { Text = i.Name, Value = i.Id.ToString(), Selected = (i.Id.ToString().IsEqual(value)) });
                    }
                });
            }
           
            if (isSelectAll)
            {
                items.Insert(0, new SelectListItem { Text = title, Value = "0" });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString InsurancesByBranch(this HtmlHelper html, string name, string value, Guid branchId, bool addNew, IDictionary<string, string> htmlAttributes)
        {
            var attributes = new StringBuilder();
            var selectList = new StringBuilder();
            if (htmlAttributes != null && htmlAttributes.Count > 0)
            {
                htmlAttributes.ForEach(kvp => attributes.AppendFormat("{0}='{1}' ", kvp.Key, kvp.Value));
            }
            selectList.AppendFormat("<select name={0} {1}><option value='{2}' IsHmo='{3}'>-- Select Insurance --</option>", name, attributes.ToString(), "0", 0);
            if (addNew)
            {
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "new", 0, "** Add New Insurance **");
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'>{2}</option>", "spacer", 0, "");
            }
          

            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", standardInsurance.Id.ToString(), 0, ((standardInsurance.Id.ToString().IsEqual(value)) ? " selected" : ""), standardInsurance.Name);
                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", standardInsurance.Id.ToString(), 0, ((standardInsurance.Id.ToString().IsEqual(value)) ? " selected" : ""), standardInsurance.Name);
                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", standardInsurance.Id.ToString(), 0, ((standardInsurance.Id.ToString().IsEqual(value)) ? " selected" : ""), standardInsurance.Name);
                        }
                    }
                }
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(insurance =>
            {
                selectList.AppendFormat("<option value='{0}'  IsHmo='{1}'{2}>{3}</option>", insurance.Id.ToString(), 1, ((insurance.Id.ToString().IsEqual(value)) ? " selected" : ""), insurance.Name);
            });
            selectList.Append("</select>");
            return MvcHtmlString.Create(selectList.ToString());
        }

        public static MvcHtmlString InsurancesByBranch(this HtmlHelper html, string name, string value, Guid branchId, bool addNew, object htmlAttributes)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (!branchId.IsEmpty())
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null && location.IsLocationStandAlone)
                {
                    if (location.Payor.IsNotNullOrEmpty() && location.Payor.IsInteger())
                    {
                        var standardInsurance = lookupRepository.GetInsurance(location.Payor.ToInteger());
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem
                            {
                                Text = standardInsurance.Name,
                                Value = standardInsurance.Id.ToString(),
                                Selected = (standardInsurance.Id.ToString().IsEqual(value))
                            });

                        }
                    }
                }
                else
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        var payorId = 0;
                        if (int.TryParse(agency.Payor, out payorId))
                        {
                            var standardInsurance = lookupRepository.GetInsurance(payorId);
                            if (standardInsurance != null)
                            {
                                items.Add(new SelectListItem
                                {
                                    Text = standardInsurance.Name,
                                    Value = standardInsurance.Id.ToString(),
                                    Selected = (standardInsurance.Id.ToString().IsEqual(value))
                                });

                            }
                        }
                    }
                }
            }
            else
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem
                            {
                                Text = standardInsurance.Name,
                                Value = standardInsurance.Id.ToString(),
                                Selected = (standardInsurance.Id.ToString().IsEqual(value))
                            });

                        }
                    }
                }
            }

            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Insurance --",
                Value = "0"
            });

            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id.ToString().IsEqual(value))
                });
            });
            if (addNew)
            {
                items.Insert(1, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
                items.Insert(2, new SelectListItem
                {
                    Text = "** Add New Insurance **",
                    Value = "new"
                });
                items.Insert(3, new SelectListItem
                {
                    Text = "",
                    Value = "spacer"
                });
            }

            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString BillStatus(this HtmlHelper html, string name, string value, bool isSelectIncluded ,object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            var billStatus = Enum.GetValues(typeof(BillingStatus));
            if (billStatus != null && billStatus.Length > 0)
            {
                foreach (BillingStatus status in billStatus)
                {
                    items.Add(new SelectListItem
                    {
                        Text = status.GetDescription(),
                        Value = ((int)status).ToString(),
                        Selected = value == ((int)status).ToString()
                    });
                }
            }
            if (isSelectIncluded)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = " -- Select Bill Status --",
                    Value = "0"
                });
            }
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString TherapyAssistance(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array list = Enum.GetValues(typeof(TherapyAssistance));
            foreach (TherapyAssistance item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GetDescription(),
                    Value = ((int)item).ToString(),
                    Selected = value == ((int)item).ToString()
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Assistance --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString StaticBalance(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array list = Enum.GetValues(typeof(StaticBalance));
            foreach (StaticBalance item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GetDescription(),
                    Value = ((int)item).ToString(),
                    Selected = value == ((int)item).ToString()
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Balance --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        public static MvcHtmlString DynamicBalance(this HtmlHelper html, string name, string value, object htmlAttributes)
        {
            var items = new List<SelectListItem>();
            Array list = Enum.GetValues(typeof(DynamicBalance));
            foreach (DynamicBalance item in list)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GetDescription(),
                    Value = ((int)item).ToString(),
                    Selected = value == ((int)item).ToString()
                });

            }
            items.Insert(0, new SelectListItem
            {
                Text = "-- Select Balance --",
                Value = "0"
            });
            return html.DropDownList(name, items.AsEnumerable(), htmlAttributes);
        }

        #endregion
    }
}
