﻿namespace Axxess.AgencyManagement.App.ViewData
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using System.Runtime.Serialization;
    using System.ComponentModel.DataAnnotations;
    using Axxess.AgencyManagement.App.Enums;

    [KnownType(typeof(ClaimViewData))]
    public class ClaimViewData
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public Guid AgencyId { get; set; }
        public Guid PatientId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid AgencyLocationId { get; set; }
        public string PatientIdNumber { get; set; }
        public DateTime EpisodeStartDate { get; set; }
        public DateTime EpisodeEndDate { get; set; }
        public bool IsOasisComplete { get; set; }
        public bool IsFirstBillableVisit { get; set; }
        public DateTime FirstBillableVisitDate { get; set; }
        public bool IsGenerated { get; set; }
        public bool IsVerified { get; set; }
        public string MedicareNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string AdmissionSource { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressStateCode { get; set; }
        public string AddressZipCode { get; set; }
        public DateTime StartofCareDate { get; set; }
        public string PhysicianNPI { get; set; }
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public string DiagnosisCode { get; set; }
        public string ConditionCodes { get; set; }
        public string HippsCode { get; set; }
        public string ClaimKey { get; set; }
        public string Ub04Locator81cca { get; set; }
        public string CBSA { get; set; }
        public string VerifiedVisit { get; set; }
        public string Supply { get; set; }
        public bool IsSupplyNotBillable { get; set; }
        public bool AreOrdersComplete { get; set; }
        public int PrimaryInsuranceId { get; set; }
        [UIHint("Status")]
        public int Status { get; set; }
        public string UB4PatientStatus { get; set; }
        [DataType(DataType.Date)]
        public DateTime PaymentDate { get; set; }
        public string PaymentDateFormatted
        {
            get
            {
                return PaymentDate != DateTime.MinValue ? PaymentDate.ToShortDateString().ToZeroFilled() : string.Empty;
            }
        }
        public string ClaimDate { get; set; }
        public double PaymentAmount { get; set; }
        public double ClaimAmount { get; set; }
        public double SupplyTotal { get; set; }
        public string SupplyCode { get; set; }
        public string Remark { get; set; }
        public string Comment { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
       
        public string TypeName
        {
            get
            {
                return this.Type.ToUpperCase();
            }
            
        }
        public string StatusName
        {
            get
            {
                return EnumExtensions.GetDescription((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), this.Status));
            }
        }
        public string EpisodeRange
        {
            get
            {
                return string.Format("{0}-{1}", this.EpisodeStartDate != null ? this.EpisodeStartDate.ToString("MM/dd/yyyy") : "", this.EpisodeEndDate != null ? this.EpisodeEndDate.ToString("MM/dd/yyyy") : "");
            }
        }
        public string DisplayName
        {
            get
            {
                return string.Concat(this.LastName, ", ", this.FirstName);
            }
        }

        public ClaimType ClaimType { get; set; }
        public bool IsHMO { get; set; }

        public string HealthPlanId { get; set; }
        public string GroupName { get; set; }
        public string GroupId { get; set; }
        public string PayerId { get; set; }
        public string PayorName { get; set; }
        public int PayorType { get; set; }
        public string AuthorizationNumber { get; set; }
        public string AuthorizationNumber2 { get; set; }
        public string AuthorizationNumber3 { get; set; }
        public string OtherProviderId { get; set; }
        public string ProviderId { get; set; }
        public string ProviderSubscriberId { get; set; }

        public double Amount30 { get { return this.DateDifference <= 30 ? this.ClaimAmount : 0; } }
        public double Amount60 { get { return this.DateDifference <= 60 && this.DateDifference >= 31 ? this.ClaimAmount : 0; } }
        public double Amount90 { get { return this.DateDifference <= 90 && this.DateDifference >= 61 ? this.ClaimAmount : 0; } }
        public double AmountOver90 { get { return this.DateDifference >= 90 ? this.ClaimAmount : 0; } }

        public int DateDifference { get; set; }

        public string PayorAddressLine1 { get; set; }
        public string PayorAddressLine2 { get; set; }

        public bool IsHomeHealthServiceIncluded { get; set; }

        public Dictionary<int, ChargeRate> ChargeRates { get; set; }
    }
}
