﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axxess.AgencyManagement.Domain;

namespace Axxess.AgencyManagement.App.ViewData
{
    public class UserCalendarViewData
    {
        public List<UserVisit> UserEvents { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set;}
    }
}
