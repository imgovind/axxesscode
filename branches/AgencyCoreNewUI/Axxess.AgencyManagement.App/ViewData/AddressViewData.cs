﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.ViewData
{
   public class AddressViewData
    {
       public string Name { get; set; }
       public string FullAddress { get; set; }
    }
}
