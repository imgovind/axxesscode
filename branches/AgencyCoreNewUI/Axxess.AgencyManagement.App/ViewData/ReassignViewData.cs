﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Axxess.AgencyManagement.App.ViewData
{
    public class ReassignViewData
    {
        public Guid EventId { get; set; }
        public Guid EpisodeId { get; set; }
        public Guid PatientId { get; set; }
        public Guid UserId { get; set; }
    }
}
