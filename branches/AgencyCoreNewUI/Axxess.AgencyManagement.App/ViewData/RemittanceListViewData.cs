﻿
namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;

    public class RemittanceListViewData
    {
        public RemittanceListViewData(List<RemittanceLean> remittances, Agency agency)
        {
            this.Remittances = remittances;
            this.Agency = agency;
        }
        public Agency Agency { get; set; }
        public List<RemittanceLean> Remittances { get; set; }
    }
}
