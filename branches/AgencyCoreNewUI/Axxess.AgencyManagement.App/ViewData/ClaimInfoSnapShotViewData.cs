﻿namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using Axxess.LookUp.Domain;

    public class ClaimInfoSnapShotViewData
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public string PatientName { get; set; }
        public string PatientIdNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string PayorName { get; set; }
        public string HIPPS { get; set; }
        public string ClaimKey { get; set; }
        public ProspectivePayment ProspectivePayment { get; set; }
        public string Type { get; set; }
        public bool Visible { get; set; }
    }
}
