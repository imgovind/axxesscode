﻿
namespace Axxess.AgencyManagement.App.ViewData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;


   public class NewManagedClaimViewData
    {
        public List<SelectListItem> Insurances { get; set; }
        public int SelecetdInsurance { get; set; }
        public Guid PatientId { get; set; }
    }
}
