﻿namespace Axxess.AgencyManagement.App.Workflows
{
    using System;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;


    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.App.Services;

    public class CreatePatientWorkflow : IWorkflow
    {
        #region CreatePatientWorkflow Members

        private Patient patient { get; set; }

        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;

        public CreatePatientWorkflow(Patient patient)
        {
            Check.Argument.IsNotNull(patient, "patient");

            this.patient = patient;

            this.patientService = Container.Resolve<IPatientService>();
            this.userRepository = Container.Resolve<IAgencyManagementDataProvider>().UserRepository;
            this.patientRepository = Container.Resolve<IAgencyManagementDataProvider>().PatientRepository;
            this.billingRepository = Container.Resolve<IAgencyManagementDataProvider>().BillingRepository;
            this.physicianRepository = Container.Resolve<IAgencyManagementDataProvider>().PhysicianRepository;

            this.Process();
        }

        #endregion

        #region IWorkflow Members

        private string message { get; set; }
        public string Message { get { return message; } }

        private bool isCommitted { get; set; }
        public bool IsCommitted { get { return isCommitted; } }

        public void Process()
        {
            var work = new WorkSequence();
            work.Complete += (sequence) =>
            {
                this.isCommitted = this.message.IsNullOrEmpty();
            };

            work.Error += (sequence, item, index) =>
            {
                this.isCommitted = false;
                this.message = item.Description;
            };
            work.Add(
                () =>
                {
                    patient.AgencyId = Current.AgencyId;
                    patient.Status = (patient.ShouldCreateEpisode && patient.UserId != Guid.Empty) ? patient.Status : (int)PatientStatus.Pending;
                    return patientService.AddPatient(patient);
                },
                () =>
                {
                    patientRepository.Delete(Current.AgencyId, patient.Id);
                    patientRepository.DeletePatientAdmissionDates(Current.AgencyId, patient.Id);
                },
                "System could not save the Patient information.");
            if (patient.EmergencyContact != null && patient.EmergencyContact.PhonePrimaryArray != null && patient.EmergencyContact.PhonePrimaryArray.Count >= 2)
            {
                patient.EmergencyContact.PrimaryPhone = patient.EmergencyContact.PhonePrimaryArray.ToArray().PhoneEncode();
            }
            if (patient.EmergencyContact.PhoneAlternateArray != null && patient.EmergencyContact.PhoneAlternateArray.Count >= 2)
            {
                patient.EmergencyContact.AlternatePhone = patient.EmergencyContact.PhoneAlternateArray.ToArray().PhoneEncode();
            }
            if (patient.EmergencyContact.FirstName.IsNotNullOrEmpty() && patient.EmergencyContact.LastName.IsNotNullOrEmpty() && patient.EmergencyContact.PrimaryPhone.IsNotNullOrEmpty())
            {
                work.Add(
                    () =>
                    {
                        patient.EmergencyContact.PatientId = patient.Id;
                        patient.EmergencyContact.AgencyId = Current.AgencyId;
                        return patientService.AddPrimaryEmergencyContact(patient);
                    },
                    () =>
                    {
                        patientRepository.DeleteEmergencyContacts(patient.Id);
                    },
                    "System could not save the Emergency Contact information.");
            }
           
            work.Add(() => { return patientService.LinkPhysicians(patient); },
                     () => { physicianRepository.UnlinkAll(patient.Id); },
                     "System could not link the Patient to the Physician.");

            if (patient.ShouldCreateEpisode && patient.UserId != Guid.Empty)
            {
                var medId = Guid.NewGuid();
                work.Add(() => { return patientService.CreateMedicationProfile(patient, medId); },
                         () => { patientRepository.DeleteMedicationProfile(patient.Id, medId); },
                         "System could not create the patient's medication profile.");

                work.Add(() => { return patientService.CreateEpisodeAndClaims(this.patient); },
                         () => { patientService.DeleteEpisodeAndClaims(this.patient); },
                         "System could not create the patient's episode information.");

            }
            else
            {
                work.Add(() => { return patientRepository.SetStatus(Current.AgencyId, this.patient.Id, PatientStatus.Pending); });
            }
            work.Perform();
        }

        #endregion
    }
}
