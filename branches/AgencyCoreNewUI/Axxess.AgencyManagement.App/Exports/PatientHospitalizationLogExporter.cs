﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Extensions;
    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using System;

    class PatientHospitalizationLogExporter:BaseExporter
    {
        private IList<HospitalizationLog> hospitalizationLog;
        private string patientName;

        public PatientHospitalizationLogExporter(IList<HospitalizationLog> hospitalizationLog, string patientName)
            : base()
        {
            this.hospitalizationLog = hospitalizationLog;
            this.patientName = patientName;
        }
        protected override void InitializeExcel()
        {
            base.InitializeExcel();
            
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - "+this.patientName+" Hospitalization Log";
            base.workBook.SummaryInformation = si;
        }
        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("PatientHospitalizationLog");

            CellStyle dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue(this.patientName+" Hospitalization Log");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Source");
            headerRow.CreateCell(1).SetCellValue("User");
            headerRow.CreateCell(2).SetCellValue("Date");

            if (this.hospitalizationLog.Count > 0)
            {
                int i = 2;
                this.hospitalizationLog.ForEach(h =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(h.Source);
                    dataRow.CreateCell(1).SetCellValue(h.User);
                    if (h.HospitalizationDate!=null&&h.HospitalizationDate!= DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(2);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(h.HospitalizationDate);
                    }
                    else
                    {
                        dataRow.CreateCell(2).SetCellValue(string.Empty);
                    }
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Hospitalization Log: {0}", hospitalizationLog.Count));
            }
            workBook.FinishWritingToExcelSpreadsheet(3);
        }
    }
}
