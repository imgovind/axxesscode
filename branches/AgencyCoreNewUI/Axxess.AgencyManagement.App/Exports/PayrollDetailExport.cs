﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.App.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

   public class PayrollDetailExport : BaseExporter
    {
       private PayrollDetail payrollDetail;
       public PayrollDetailExport(PayrollDetail payrollDetail)
            : base()
        {
            this.payrollDetail = payrollDetail;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Payroll Detail";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Payroll Detail");
            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Payroll Detail");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", payrollDetail.StartDate.ToString("MM/dd/yyyy"), payrollDetail.EndDate.ToString("MM/dd/yyyy"))));
            titleRow.CreateCell(4).SetCellValue(string.Format("Employee Name: {0}", payrollDetail.Name));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Schedule Date");
            headerRow.CreateCell(1).SetCellValue("Visit Date");
            headerRow.CreateCell(2).SetCellValue("Patient Name");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Time");
            headerRow.CreateCell(5).SetCellValue("Total Min.");
            headerRow.CreateCell(6).SetCellValue("Associated Mileage");
            headerRow.CreateCell(7).SetCellValue("Status");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.payrollDetail != null && this.payrollDetail.Visits!=null && this.payrollDetail.Visits.Count > 0)
            {
                int i = 2;
                var totalTime = 0;
                this.payrollDetail.Visits.ForEach(visit =>
                {
                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(visit.ScheduleDateFormatted);
                    row.CreateCell(1).SetCellValue(visit.VisitDateFormatted);
                    row.CreateCell(2).SetCellValue(visit.PatientName);
                    row.CreateCell(3).SetCellValue(visit.TaskName);
                    row.CreateCell(4).SetCellValue(visit.TimeIn.IsNotNullOrEmpty() && visit.TimeOut.IsNotNullOrEmpty() ?string.Format("{0} - {1}", visit.TimeIn, visit.TimeOut):string.Empty);
                    row.CreateCell(5).SetCellValue(visit.MinSpent);
                    row.CreateCell(6).SetCellValue(visit.AssociatedMileage);
                    row.CreateCell(7).SetCellValue(visit.StatusName);
                    i++;
                    totalTime += visit.MinSpent;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Payroll Detail: {0}", this.payrollDetail.Visits.Count));
                totalRow.CreateCell(1).SetCellValue(string.Format("Total Time for this employee: {0}", string.Format(" {0} min =  {1:#0.00} hour(s)", totalTime, (double)totalTime / 60)));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            
        }
    }
}
