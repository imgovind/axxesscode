﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using System;
    
    public class HospitalizationLogExporter:BaseExporter
    {
        private IList<PatientHospitalizationData> patientHospitalizationData;
        public HospitalizationLogExporter(IList<PatientHospitalizationData> patientHospitalizationData)
            : base()
        {
            this.patientHospitalizationData = patientHospitalizationData;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Hospitalization Log";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("HospitalizationLog");
          
            CellStyle dateStyle = base.workBook.CreateCellStyle();
            dateStyle.DataFormat = base.workBook.CreateDataFormat().GetFormat("MM/dd/yyyy");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Hospitalization Log");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Source");
            headerRow.CreateCell(3).SetCellValue("Hospitalization Date");
            headerRow.CreateCell(4).SetCellValue("Last Home Visit Date");
            headerRow.CreateCell(5).SetCellValue("User");
            if (this.patientHospitalizationData.Count > 0)
            {
                int i = 2;
                this.patientHospitalizationData.ForEach(p =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(p.PatientIdNumber);
                    dataRow.CreateCell(1).SetCellValue(p.DisplayName);
                    dataRow.CreateCell(2).SetCellValue(p.Source);
                    DateTime eventDate;
                    DateTime.TryParse(p.HospitalizationDate,out eventDate);
                    if (eventDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(eventDate);
                    }
                    else
                    {
                        dataRow.CreateCell(3).SetCellValue(string.Empty);
                    }
                    DateTime.TryParse(p.LastHomeVisitDate, out eventDate);
                    if (eventDate != DateTime.MinValue)
                    {
                        var createdDateCell = dataRow.CreateCell(4);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(eventDate);
                    }
                    else
                    {
                        dataRow.CreateCell(4).SetCellValue(string.Empty);
                    }
                    dataRow.CreateCell(5).SetCellValue(p.User);
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Hospitalization Log: {0}", patientHospitalizationData.Count));
            
            }
            workBook.FinishWritingToExcelSpreadsheet(6);
        }
    }
}
