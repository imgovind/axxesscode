﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using Axxess.AgencyManagement.Domain;


    public class PastDueRecertExporter : BaseExporter
    {
         private IList<RecertEvent> recertEvents;
         public PastDueRecertExporter(IList<RecertEvent> recertEvents)
            : base()
        {
            this.recertEvents = recertEvents;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Past Due Recert. OASIS";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("PastDueRecertOASIS");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Past Due Recert. OASIS");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("MR #");
            headerRow.CreateCell(2).SetCellValue("Employee Responsible");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Due Date");
            headerRow.CreateCell(5).SetCellValue("Past Date");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.recertEvents.Count > 0)
            {
                int i = 2;
                this.recertEvents.ForEach(a =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.PatientName);
                    dataRow.CreateCell(1).SetCellValue(a.PatientIdNumber);
                    dataRow.CreateCell(2).SetCellValue(a.AssignedTo);
                    dataRow.CreateCell(3).SetCellValue(a.StatusName);
                    dataRow.CreateCell(4).SetCellValue(a.TargetDate.ToString("MM/dd/yyyy"));
                    dataRow.CreateCell(5).SetCellValue(a.DateDifference);
                    
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Recert. OASIS: {0}", recertEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
           
        }
    }
}
