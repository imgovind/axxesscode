﻿namespace Axxess.AgencyManagement.App.Exports
{
     using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

   public class ScheduleDeviationExporter : BaseExporter
    {
        private IList<ScheduleEvent> scheduleEvents;
        public ScheduleDeviationExporter(IList<ScheduleEvent> scheduleEvents)
            : base()
        {
            this.scheduleEvents = scheduleEvents;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Schedule Deviations";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("ScheduleDeviations");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Deviations");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Patient Name");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Employee");
            headerRow.CreateCell(5).SetCellValue("Schedule Date");
            headerRow.CreateCell(6).SetCellValue("Visit Date");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.scheduleEvents.Count > 0)
            {
                int i = 2;
                this.scheduleEvents.ForEach(evnt =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    dataRow.CreateCell(1).SetCellValue(evnt.DisciplineTaskName);
                    dataRow.CreateCell(2).SetCellValue(evnt.PatientName);
                    dataRow.CreateCell(3).SetCellValue(evnt.StatusName);
                    dataRow.CreateCell(4).SetCellValue(evnt.UserName);
                    dataRow.CreateCell(5).SetCellValue(evnt.EventDate.ToString("MM/dd/yyyy"));
                    dataRow.CreateCell(6).SetCellValue(evnt.VisitDate.ToString("MM/dd/yyyy"));
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Deviations: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
           
        }
    }
}
