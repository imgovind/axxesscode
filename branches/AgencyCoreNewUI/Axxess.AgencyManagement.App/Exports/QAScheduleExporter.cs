﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System.Collections.Generic;


    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;


    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    using System;

   public class QAScheduleExporter : BaseExporter
    {
       private IList<PatientEpisodeEvent> patientEpisodeEvent;
       public QAScheduleExporter(IList<PatientEpisodeEvent> patientEpisodeEvent)
            : base()
        {
            this.patientEpisodeEvent = patientEpisodeEvent;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - QA Schedule Tasks";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("QAScheduleTasks");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("QA Schedule Tasks");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Date");
            headerRow.CreateCell(2).SetCellValue("Task");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("User Name");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.patientEpisodeEvent.Count > 0)
            {
                int i = 2;
                this.patientEpisodeEvent.ForEach(u =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(u.PatientName);
                    dataRow.CreateCell(1).SetCellValue(u.EventDate);
                    dataRow.CreateCell(2).SetCellValue(u.TaskName);
                    dataRow.CreateCell(3).SetCellValue(u.Status);
                    dataRow.CreateCell(4).SetCellValue(u.UserName);
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of QA Schedule Tasks: {0}", patientEpisodeEvent.Count));
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
        }
    }
}
