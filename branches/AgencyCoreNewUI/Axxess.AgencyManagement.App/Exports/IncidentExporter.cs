﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;


   public class IncidentExporter : BaseExporter
    {
       private IList<Incident> incidents;
       public IncidentExporter(IList<Incident> incidents)
            : base()
        {
            this.incidents = incidents;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export -  Incident";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Incident");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Incident");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Physician");
            headerRow.CreateCell(2).SetCellValue("Type Of Incident");
            headerRow.CreateCell(3).SetCellValue("Incident Date");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.incidents.Count > 0)
            {
                int i = 2;
                this.incidents.ForEach(incident =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(incident.PatientName);
                    dataRow.CreateCell(1).SetCellValue(incident.PhysicianName);
                    dataRow.CreateCell(2).SetCellValue(incident.IncidentType);
                    dataRow.CreateCell(3).SetCellValue(incident.IncidentDateFormatted);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Incidents: {0}", incidents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
           
        }
    }
}
