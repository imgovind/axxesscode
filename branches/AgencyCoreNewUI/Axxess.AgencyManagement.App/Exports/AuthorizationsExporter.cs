﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class AuthorizationsExporter : BaseExporter
    {
        private IList<Authorization> authorizations;
        public AuthorizationsExporter(IList<Authorization> authorizations) : base() {
            this.authorizations = authorizations;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Authorizations";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Authorizations");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Authorizations");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Authorization #1");
            headerRow.CreateCell(1).SetCellValue("Authorization #2");
            headerRow.CreateCell(2).SetCellValue("Authorization #3");
            headerRow.CreateCell(3).SetCellValue("Branch");
            headerRow.CreateCell(4).SetCellValue("Start Date");
            headerRow.CreateCell(5).SetCellValue("End Date");
            headerRow.CreateCell(6).SetCellValue("Status");
            headerRow.CreateCell(7).SetCellValue("Insurance");
            headerRow.CreateCell(8).SetCellValue("SN Visit");
            headerRow.CreateCell(9).SetCellValue("PT Visit");
            headerRow.CreateCell(10).SetCellValue("OT Visit");
            headerRow.CreateCell(11).SetCellValue("ST Visit");
            headerRow.CreateCell(12).SetCellValue("MSW Visit");
            headerRow.CreateCell(13).SetCellValue("HHA Visit");
            headerRow.CreateCell(14).SetCellValue("Dietician Visit");
            headerRow.CreateCell(15).SetCellValue("RN Visit");
            headerRow.CreateCell(16).SetCellValue("LVN Visit");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.authorizations.Count > 0) {
                int i = 2;
                this.authorizations.ForEach(a =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.Number1);
                    dataRow.CreateCell(1).SetCellValue(a.Number2);
                    dataRow.CreateCell(2).SetCellValue(a.Number3);
                    dataRow.CreateCell(3).SetCellValue(a.Branch);
                    dataRow.CreateCell(4).SetCellValue(a.StartDate.ToShortDateString());
                    dataRow.CreateCell(5).SetCellValue(a.EndDate.ToShortDateString());
                    dataRow.CreateCell(6).SetCellValue(a.Status);
                    dataRow.CreateCell(7).SetCellValue(a.Insurance);
                    dataRow.CreateCell(8).SetCellValue(a.SNVisit.IsNotNullOrEmpty() ? a.SNVisit + " " + (a.SNVisitCountType == "1" ? "Visits" : string.Empty) + (a.SNVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(9).SetCellValue(a.PTVisit.IsNotNullOrEmpty() ? a.PTVisit + " " + (a.PTVisitCountType == "1" ? "Visits" : string.Empty) + (a.PTVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(10).SetCellValue(a.OTVisit.IsNotNullOrEmpty() ? a.OTVisit + " " + (a.OTVisitCountType == "1" ? "Visits" : string.Empty) + (a.OTVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(11).SetCellValue(a.STVisit.IsNotNullOrEmpty() ? a.STVisit + " " + (a.STVisitCountType == "1" ? "Visits" : string.Empty) + (a.STVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(12).SetCellValue(a.MSWVisit.IsNotNullOrEmpty() ? a.MSWVisit + " " + (a.MSWVisitCountType == "1" ? "Visits" : string.Empty) + (a.MSWVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(13).SetCellValue(a.HHAVisit.IsNotNullOrEmpty() ? a.HHAVisit + " " + (a.HHAVisitCountType == "1" ? "Visits" : string.Empty) + (a.HHAVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(14).SetCellValue(a.DieticianVisit.IsNotNullOrEmpty() ? a.DieticianVisit + " " + (a.DieticianVisitCountType == "1" ? "Visits" : string.Empty) + (a.DieticianVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(15).SetCellValue(a.RNVisit.IsNotNullOrEmpty() ? a.RNVisit + " " + (a.RNVisitCountType == "1" ? "Visits" : string.Empty) + (a.RNVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    dataRow.CreateCell(16).SetCellValue(a.LVNVisit.IsNotNullOrEmpty() ? a.LVNVisit + " " + (a.LVNVisitCountType == "1" ? "Visits" : string.Empty) + (a.LVNVisitCountType == "2" ? "Hours" : string.Empty) : string.Empty);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Authorizations: {0}", authorizations.Count));
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
            sheet.AutoSizeColumn(12);
            sheet.AutoSizeColumn(13);
            sheet.AutoSizeColumn(14);
            sheet.AutoSizeColumn(15);
            sheet.AutoSizeColumn(16);
            sheet.AutoSizeColumn(17);
            sheet.AutoSizeColumn(18);
            sheet.AutoSizeColumn(19);
            sheet.AutoSizeColumn(20);
            sheet.AutoSizeColumn(21);
        }
    }
}
