﻿namespace Axxess.AgencyManagement.App.Exports {
    using System;
    using System.Collections.Generic;
    using Extensions;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class PendingAdmissionsExporter : BaseExporter {
        private IList<PendingPatient> admissions;
        public PendingAdmissionsExporter(IList<PendingPatient> admissions) : base() {
            this.admissions = admissions;
        }

        protected override void InitializeExcel() {
            base.InitializeExcel();
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Pending Admissions";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet() {
            Sheet sheet = base.workBook.CreateSheet("Pending Admissions");
            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 10;
            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Pending Admissions");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("MR#");
            headerRow.CreateCell(2).SetCellValue("Primary Insurance");
            headerRow.CreateCell(3).SetCellValue("Branch");
            headerRow.CreateCell(4).SetCellValue("Date Added");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (this.admissions.Count > 0) {
                int i = 2;
                this.admissions.ForEach(a => {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(a.DisplayName);
                    dataRow.CreateCell(1).SetCellValue(a.PatientIdNumber);
                    dataRow.CreateCell(2).SetCellValue(a.PrimaryInsuranceName);
                    dataRow.CreateCell(3).SetCellValue(a.Branch);
                    dataRow.CreateCell(4).SetCellValue(a.CreatedDateFormatted);
                    i++;
                });
                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number of Pending Admissions: {0}", admissions.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
        }
    }
}
