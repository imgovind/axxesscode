﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    using Kent.Boogaart.KBCsv;

    public class CostReportExporter : BaseExporter
    {
        #region Private Members and Constructor

        private Guid branchId;
        private Guid agencyId;
        private DateTime endDate;
        private DateTime startDate;
        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public CostReportExporter(Guid agencyId, Guid branchId, DateTime startDate, DateTime endDate)
            : base()
        {
            this.branchId = branchId;
            this.agencyId = agencyId;
            this.endDate = endDate;
            this.startDate = startDate;

            this.FormatType = ExportFormatType.XLS;
            this.FileName = "CostReport.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, CoreSettings.ExcelDirectory);
            var file = new FileStream(Path.Combine(path, "CostReport.xls"), FileMode.Open, FileAccess.Read);

            base.workBook = new HSSFWorkbook(file);

            DocumentSummaryInformation docSummaryInformation = PropertySetFactory.CreateDocumentSummaryInformation();
            docSummaryInformation.Company = "Axxess Technology Solutions, Inc";
            workBook.DocumentSummaryInformation = docSummaryInformation;

            SummaryInformation summaryInformation = PropertySetFactory.CreateSummaryInformation();
            summaryInformation.Subject = "Axxess Data Export - Cost Report";
            summaryInformation.Author = "Axxess";
            base.workBook.SummaryInformation = summaryInformation;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet1 = base.workBook.GetSheet("Sheet1");

            var agency = agencyRepository.GetAgencyOnly(agencyId);
            var agencyName = agency != null && agency.Name.IsNotNullOrEmpty() ? agency.Name : string.Empty;

            var report = reportAgent.CostReport(agencyId, branchId, startDate, endDate);

            if (report != null)
            {
                sheet1.GetRow(6).GetCell(1).SetCellValue(report.SNMedicareVisits);
                sheet1.GetRow(6).GetCell(2).SetCellValue(report.SNMedicarePatients);
                sheet1.GetRow(6).GetCell(3).SetCellValue(report.SNNonMedicareVisits);
                sheet1.GetRow(6).GetCell(4).SetCellValue(report.SNNonMedicarePatients);

                sheet1.GetRow(7).GetCell(1).SetCellValue(report.PTMedicareVisits);
                sheet1.GetRow(7).GetCell(2).SetCellValue(report.PTMedicarePatients);
                sheet1.GetRow(7).GetCell(3).SetCellValue(report.PTNonMedicareVisits);
                sheet1.GetRow(7).GetCell(4).SetCellValue(report.PTNonMedicarePatients);

                sheet1.GetRow(8).GetCell(1).SetCellValue(report.OTMedicareVisits);
                sheet1.GetRow(8).GetCell(2).SetCellValue(report.OTMedicarePatients);
                sheet1.GetRow(8).GetCell(3).SetCellValue(report.OTNonMedicareVisits);
                sheet1.GetRow(8).GetCell(4).SetCellValue(report.OTNonMedicarePatients);

                sheet1.GetRow(9).GetCell(1).SetCellValue(report.STMedicareVisits);
                sheet1.GetRow(9).GetCell(2).SetCellValue(report.STMedicarePatients);
                sheet1.GetRow(9).GetCell(3).SetCellValue(report.STNonMedicareVisits);
                sheet1.GetRow(9).GetCell(4).SetCellValue(report.STNonMedicarePatients);

                sheet1.GetRow(10).GetCell(1).SetCellValue(report.MSWMedicareVisits);
                sheet1.GetRow(10).GetCell(2).SetCellValue(report.MSWMedicarePatients);
                sheet1.GetRow(10).GetCell(3).SetCellValue(report.MSWNonMedicareVisits);
                sheet1.GetRow(10).GetCell(4).SetCellValue(report.MSWNonMedicarePatients);

                sheet1.GetRow(11).GetCell(1).SetCellValue(report.HHAMedicareVisits);
                sheet1.GetRow(11).GetCell(2).SetCellValue(report.HHAMedicarePatients);
                sheet1.GetRow(11).GetCell(3).SetCellValue(report.HHANonMedicareVisits);
                sheet1.GetRow(11).GetCell(4).SetCellValue(report.HHANonMedicareVisits);

                sheet1.GetRow(18).GetCell(1).SetCellValue(report.SNTotalPatients);
                sheet1.GetRow(19).GetCell(1).SetCellValue(report.PTTotalPatients);
                sheet1.GetRow(20).GetCell(1).SetCellValue(report.OTTotalPatients);
                sheet1.GetRow(21).GetCell(1).SetCellValue(report.STTotalPatients);
                sheet1.GetRow(22).GetCell(1).SetCellValue(report.MSWTotalPatients);
                sheet1.GetRow(23).GetCell(1).SetCellValue(report.HHATotalPatients);

                sheet1.GetRow(26).GetCell(1).SetCellValue(report.MedicareUnduplicated);
                sheet1.GetRow(27).GetCell(1).SetCellValue(report.NonMedicareUnduplicated);
                sheet1.GetRow(28).GetCell(1).SetCellValue(report.TotalUnduplicated);


                sheet1.GetRow(31).GetCell(1).SetCellValue(report.HHAMedicareHours);
                sheet1.GetRow(32).GetCell(1).SetCellValue(report.HHANonMedicareHours);

            }
        }

        #endregion

    }
}
