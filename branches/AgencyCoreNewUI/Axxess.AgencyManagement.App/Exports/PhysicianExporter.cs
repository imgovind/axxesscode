﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;

    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    public class PhysicianExporter : BaseExporter
    {
        private IList<AgencyPhysician> physicians;
        public PhysicianExporter(IList<AgencyPhysician> physicians)
            : base()
        {
            this.physicians = physicians;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Agency Physicians";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Physicians");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = 1;
            headerFont.FontHeightInPoints = 11;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Physicians");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("NPI");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Email");
            headerRow.CreateCell(3).SetCellValue("Address");
            headerRow.CreateCell(4).SetCellValue("Phone Number");
            headerRow.CreateCell(5).SetCellValue("Fax Phone");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);

            if (this.physicians.Count > 0)
            {
                int i = 2;
                this.physicians.ForEach(p =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(p.NPI);
                    dataRow.CreateCell(1).SetCellValue(p.DisplayName);
                    dataRow.CreateCell(2).SetCellValue(p.EmailAddress);
                    dataRow.CreateCell(3).SetCellValue(p.AddressFull);
                    dataRow.CreateCell(4).SetCellValue(p.PhoneWork.ToPhone());
                    dataRow.CreateCell(5).SetCellValue(p.FaxNumber.ToPhone());
                    i++;
                });

                var totalRow = sheet.CreateRow(i + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Physicians: {0}", physicians.Count));
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
        }
    }
}
