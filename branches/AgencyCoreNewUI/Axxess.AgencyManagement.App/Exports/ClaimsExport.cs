﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;
    using NPOI.HPSF;
    using NPOI.SS.UserModel;
    public class ClaimsExport : BaseExporter
    {
        private Bill data;
        public ClaimsExport(Bill data)
            : base()
        {
            this.data = data;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Claims Summary";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("Claims");
            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;
            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Claims Summary");
            titleRow.CreateCell(2).SetCellValue(string.Format("Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            if (this.data != null && this.data.ClaimType.IsEqual("rap"))
            {
                var titleRow2 = sheet.CreateRow(2);
                titleRow2.CreateCell(0).SetCellValue("RAPs");
                Row headerRow = sheet.CreateRow(3);
                headerRow.CreateCell(0).SetCellValue("Patient Name");
                headerRow.CreateCell(1).SetCellValue("MR#");
                headerRow.CreateCell(2).SetCellValue("Episode Date");
                headerRow.CreateCell(3).SetCellValue("OASIS");
                headerRow.CreateCell(4).SetCellValue("Billable Visit");
                headerRow.CreateCell(5).SetCellValue("Verfied");
                headerRow.RowStyle = headerStyle;
                int i = 4;
                if (this.data.Claims.Count > 0)
                {
                    this.data.Claims.ForEach(rap =>
                    {
                        var row = sheet.CreateRow(i);
                        row.CreateCell(0).SetCellValue(string.Format("{0}, {1}", rap.LastName, rap.FirstName));
                        row.CreateCell(1).SetCellValue(rap.PatientIdNumber);
                        row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                        row.CreateCell(3).SetCellValue(rap.IsOasisComplete ? "X" : "");
                        row.CreateCell(4).SetCellValue(rap.IsFirstBillableVisit ? "X" : "");
                        row.CreateCell(5).SetCellValue(rap.IsOasisComplete && rap.IsFirstBillableVisit && rap.IsVerified ? "X" : "");
                        i++;
                    });
                }
            }
            else if (this.data != null && this.data.ClaimType.IsEqual("final"))
            {
                var titleRow3 = sheet.CreateRow(2);
                titleRow3.CreateCell(0).SetCellValue("Finals");
                Row headerRow2 = sheet.CreateRow(3);
                headerRow2.CreateCell(0).SetCellValue("Patient Name");
                headerRow2.CreateCell(1).SetCellValue("MR#");
                headerRow2.CreateCell(2).SetCellValue("Episode Date");
                headerRow2.CreateCell(3).SetCellValue("RAP");
                headerRow2.CreateCell(4).SetCellValue("Visit");
                headerRow2.CreateCell(5).SetCellValue("Order");
                headerRow2.CreateCell(6).SetCellValue("Verfied");
                headerRow2.RowStyle = headerStyle;
                int i = 4;
                if (this.data.Claims.Count > 0)
                {
                    this.data.Claims.ForEach(final =>
                    {
                        var row = sheet.CreateRow(i);
                        row.CreateCell(0).SetCellValue(string.Format("{0}, {1}", final.LastName, final.FirstName));
                        row.CreateCell(1).SetCellValue(final.PatientIdNumber);
                        row.CreateCell(2).SetCellValue(final.EpisodeRange);
                        row.CreateCell(3).SetCellValue(final.IsRapGenerated ? "X" : "");
                        row.CreateCell(4).SetCellValue(final.AreVisitsComplete ? "X" : "");
                        row.CreateCell(5).SetCellValue(final.AreOrdersComplete ? "X" : "");
                        row.CreateCell(6).SetCellValue(final.IsVisitVerified && final.IsSupplyVerified && final.IsFinalInfoVerified ? "X" : "");
                        i++;
                    });
                }
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
        }
    }
}