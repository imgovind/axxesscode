﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    using Enums;
    using Services;
    using Extensions;

    using Axxess.Api;
    using Axxess.Api.Contracts;

    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;
    using NPOI.POIFS.FileSystem;
    using NPOI.SS.UserModel;
    using NPOI.HSSF.Util;

    using Kent.Boogaart.KBCsv;

    public class PpsChargeExporter : BaseExporter
    {
        #region Private Members and Constructor

        private Guid branchId;
        private DateTime endDate;
        private DateTime startDate;
        private static readonly ReportAgent reportAgent = new ReportAgent();
        private static readonly IAgencyRepository agencyRepository = Container.Resolve<IAgencyManagementDataProvider>().AgencyRepository;

        public PpsChargeExporter(Guid branchId, DateTime startDate, DateTime endDate)
            : base()
        {
            this.branchId = branchId;
            this.endDate = endDate;
            this.startDate = startDate;

            this.FormatType = ExportFormatType.XLS;
            this.FileName = "PPSLogChargeInformation.xls";
        }

        #endregion

        #region Excel Output

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PPS Log Charge Information";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("ChargeInformation");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 13;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            List<Dictionary<string, string>> data = reportAgent.PPSChargeInformation(Current.AgencyId, branchId, startDate, endDate);

            if (data != null && data.Count > 0)
            {
                var rowIndex = 0;
                var colIndex = 0;
                var columnSize = 0;
                var dictionary = data.FirstOrDefault();

                Row headerRow = sheet.CreateRow(rowIndex);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    headerRow.CreateCell(colIndex).SetCellValue(kvp.Key);
                    colIndex++;
                }
                headerRow.RowStyle = headerStyle;

                rowIndex++;
                columnSize = colIndex + 1;
                colIndex = 0;

                data.ForEach(list =>
                {
                    Row dataRow = sheet.CreateRow(rowIndex);
                    foreach (KeyValuePair<string, string> kvp in list)
                    {
                        dataRow.CreateCell(colIndex).SetCellValue(kvp.Value.Trim());
                        colIndex++;
                    }
                    rowIndex++;
                    colIndex = 0;
                });

                int resizeColCounter = 0;
                do
                {
                    sheet.AutoSizeColumn(resizeColCounter);
                    resizeColCounter++;
                }
                while (resizeColCounter < columnSize);
            }
            else
            {
                Row errorRow = sheet.CreateRow(0);
                errorRow.CreateCell(0).SetCellValue("No Charge Information found!");
            }
        }

        #endregion

    }
}
