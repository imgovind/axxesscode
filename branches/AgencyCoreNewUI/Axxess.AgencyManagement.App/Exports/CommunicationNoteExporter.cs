﻿namespace Axxess.AgencyManagement.App.Exports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Axxess.Core.Extension;
    using Axxess.AgencyManagement.Domain;

    using NPOI.HPSF;
    using NPOI.SS.UserModel;

    public class CommunicationNoteExporter : BaseExporter
    {
        private IList<CommunicationNote> communicationNotes;
        private DateTime StartDate;
        private DateTime EndDate;
        public CommunicationNoteExporter(IList<CommunicationNote> communicationNotes, DateTime StartDate,DateTime EndDate)
            : base()
        {
            this.communicationNotes = communicationNotes;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }

        protected override void InitializeExcel()
        {
            base.InitializeExcel();

            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Communication Notes";
            base.workBook.SummaryInformation = si;
        }

        protected override void WriteToExcelSpreadsheet()
        {
            Sheet sheet = base.workBook.CreateSheet("CommunicationNotes");

            Font headerFont = base.workBook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 13;

            CellStyle headerStyle = base.workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Communication Notes");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(this.StartDate.IsValid() ? string.Format("Date Range : {0} - {1}", this.StartDate.ToString("MM/dd/yyyy"), this.EndDate.ToString("MM/dd/yyyy")) : string.Empty);

            Row headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Employee Name");
            headerRow.CreateCell(2).SetCellValue("Date");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.RowStyle = headerStyle;
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (this.communicationNotes.Count > 0)
            {
                int i = 2;
                this.communicationNotes.ForEach(note =>
                {
                    Row dataRow = sheet.CreateRow(i);
                    dataRow.CreateCell(0).SetCellValue(note.DisplayName);
                    dataRow.CreateCell(1).SetCellValue(note.UserDisplayName);
                    dataRow.CreateCell(2).SetCellValue(note.Created.ToString("MM/dd/yyyy"));
                    dataRow.CreateCell(3).SetCellValue(note.StatusName);
                    i++;
                });
                var totalRow = sheet.CreateRow(i+2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Communication Notes: {0}", communicationNotes.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
           
        }
    }
}
