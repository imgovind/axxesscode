﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Domain;
    using ViewData; 

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.LookUp.Domain;
    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Log.Enums;
    using Axxess.Log.Domain;
    using Axxess.OasisC.Domain;

    public interface IPatientService
    {
        bool AddPatient(Patient patient);
        bool EditPatient(Patient patient);
        bool DeletePatient(Guid id);
        bool AdmitPatient(PendingPatient pending);
        bool NonAdmitPatient(PendingPatient pending);
        bool SetPatientPending(Guid patientId);
        bool DischargePatient(Guid patientId, DateTime dischargeDate, string dischargeReason);
        bool DischargePatient(Guid patientId, Guid episodeId, DateTime dischargeDate, string dischargeReason);
        bool ActivatePatient(Guid patientId);
        bool ActivatePatient(Guid patientId, DateTime startOfCareDate);
        bool AddPhoto(Guid patientId, HttpFileCollectionBase httpFiles);
        bool UpdatePatientForPhotoRemove(Patient patient);

        bool AddPrimaryEmergencyContact(Patient patient);
        PatientAdmissionDate GetIfExitOrCreate(Guid patientId);
        bool NewEmergencyContact(PatientEmergencyContact emergencyContact, Guid patientId);
        bool EditEmergencyContact(PatientEmergencyContact emergencyContact);
        bool DeleteEmergencyContact(Guid id, Guid patientId);

        PatientProfile GetProfile(Guid id);
        string GetAllergies(Guid patientId);

        bool CreateMedicationProfile(Patient patient, Guid medId);
        bool AddMedication(Guid medicationProfileId, Medication medication, string medicationType);
        bool UpdateMedication(Guid medicationProfileId, Medication medication, string medicationType);
        bool DeleteMedication(Guid medicationProfileId, Guid medicationId);
        bool UpdateMedicationStatus(Guid medicationProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate);
        bool SignMedicationHistory(MedicationProfileHistory medicationProfileHistory);
        IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId);
        List<Medication> GetCurrentMedicationsByCategory(Guid patientId, string medicationCategory);
        MedicationProfileSnapshotViewData GetMedicationProfilePrint(Guid id);
        MedicationProfileSnapshotViewData GetMedicationSnapshotPrint(Guid id);
        AllergyProfileViewData GetAllergyProfilePrint(Guid id);
        DrugDrugInteractionsViewData GetDrugDrugInteractionsPrint(Guid patientId, List<string> drugsSelected);
       
        bool LinkPhysicians(Patient patient);
        bool LinkPhysician(Guid patientId, Guid physicianId, bool isPrimary);
        bool UnlinkPhysician(Guid patientId, Guid physicianId);

        bool ProcessPhysicianOrder(Guid id, Guid episodeId, Guid patientId, string actionType, string reason);
        List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate);
        List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId);
        bool DeletePhysicianOrder(Guid orderId, Guid patientId);
        PhysicianOrder GetOrderPrint();
        PhysicianOrder GetOrderPrint(Guid patientId, Guid orderId);

        bool ProcessCommunicationNotes(string button, Guid patientId, Guid eventId, string reason);
        List<CommunicationNote> GetCommunicationNotes(Guid patientId, Guid branchId, int patientStatus, DateTime startDate, DateTime endDate);
        bool DeleteCommunicationNote(Guid Id, Guid patientId);
        CommunicationNote GetCommunicationNotePrint(Guid eventId, Guid patientId);
        CommunicationNote GetCommunicationNotePrint();

        bool CreateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter);
        FaceToFaceEncounter GetFaceToFacePrint();
        FaceToFaceEncounter GetFaceToFacePrint(Guid patientId, Guid orderId);

        PatientEpisode CreateEpisode(Guid patientId, DateTime startDate, ScheduleEvent scheduleEvent);
        PatientEpisode CreateEpisode(Guid patientId, PatientEpisode episode);
        bool AddEpisode(PatientEpisode patientEpisode);
        bool UpdateEpisode(PatientEpisode episode);
        bool IsValidEpisode(Guid patientId, DateTime startDate, DateTime endDate);
        bool IsValidEpisode(Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate);

        bool CreateEpisodeAndClaims(Patient patient);
        void DeleteEpisodeAndClaims(Patient patient);

        bool UpdateScheduleEvent(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles);
        bool UpdateScheduleEventDetail(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles);
        List<ScheduleEvent> GetScheduledEvents(Guid episodeId, Guid patientId, string discipline);
       
        List<ScheduleEvent> GetScheduledEvents(Guid patientId, string discipline, DateRange range);
        ScheduleEvent GetScheduledEvent(Guid episodeId, Guid patientId, Guid eventId);

        bool ReassignSchedules(Guid employeeOldId, Guid employeeId);
        bool ReassignSchedules(Guid patientId, Guid employeeOldId, Guid employeeId);

        PatientEpisode GetPatientEpisodeWithFrequencyForMasterCalendar(Guid episodeId, Guid patientId);

        bool SaveWoundCare(FormCollection formCollection, HttpFileCollectionBase httpFiles);
      
        Rap CreateRap(Patient patient, PatientEpisode episode);
        Final CreateFinal(Patient patient, PatientEpisode episode);
        bool AddClaim(Guid patientId, Guid episodeId, string type);
        bool DeleteClaim(Guid patientId, Guid Id, string type);

        ManagedClaim CreateManagedClaim(Patient patient, DateTime startDate, DateTime endDate, int insuranceId);
        bool AddManagedClaim(Guid patientId, DateTime startDate, DateTime endDate, int insuranceId);

        bool AddMissedVisit(MissedVisit missedVisit);

        bool IsValidImage(HttpFileCollectionBase httpFiles);

        bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        List<Supply> GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId);
       
        PatientEligibility VerifyEligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender);
        List<MedicareEligibility> GetMedicareEligibilityLists(Guid patientId);

        string GetScheduledEventUrl(ScheduleEvent evnt, DisciplineTasks task);

        PlanofCareViewData GetPatientAndAgencyInfo(Guid episodeId, Guid patientId, Guid eventId);
       
       
        List<VisitNoteViewData> GetSixtyDaySummary(Guid patientId);
        List<VitalSign> GetPatientVitalSigns(Guid patientId, DateTime startDate, DateTime endDate);
        List<VitalSign> GetVitalSignsForSixtyDaySummary(PatientEpisode episode, DateTime date);

        DisciplineTask GetDisciplineTask(int disciplineTaskId);

        List<PatientEpisodeTherapyException> GetTherapyException(Guid branchId, DateTime startDate, DateTime endDate);
        List<PatientEpisodeTherapyException> GetTherapyReevaluationException(Guid branchId, int count, DateTime startDate, DateTime endDate);
       
        List<TaskLog> GetTaskLogs(Guid patientId, Guid eventId, int task);
        List<AppAudit> GetGeneralLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId);
        List<AppAudit> GetMedicationLogs(LogDomain logDomain, LogType logType, Guid domainId);

        List<ScheduleEvent> GetPreviousNotes(Guid patientId, ScheduleEvent scheduledEvent);

        bool AddAllergy(Allergy allergy);
        bool UpdateAllergy(Allergy allergy);
        bool UpdateAllergy(Guid allergyProfileId, Guid allergyId, bool isDeleted);

        bool AddHospitalizationLog(FormCollection formCollection);
        bool UpdateHospitalizationLog(FormCollection formCollection);
        List<HospitalizationLog> GetHospitalizationLogs(Guid agencyId, Guid patientId);
        HospitalizationLog GetHospitalizationLog(Guid patientId, Guid hospitalizationLogId);

        List<NonAdmit> GetNonAdmits();
        List<PendingPatient> GetPendingPatients();
        PatientInsuranceInfoViewData PatientInsuranceInfo(Guid patientId,int insuranceId , string insuranceType);
        bool IsValidAdmissionPeriod(Guid admissionId, Guid patientId, DateTime startOfCareDate, DateTime dischargeDate);
        bool IsValidAdmissionPeriod(Guid patientId, DateTime startOfCareDate, DateTime dischargeDate);
        bool MarkPatientAdmissionCurrent(Guid patientId, Guid Id);

        bool SaveNotesNew(string button, FormCollection formCollection);
        bool ProcessNotesNew(string button, Guid episodeId, Guid patientId, Guid eventId, string reason);
        bool UpdateReturnReason(Guid episodeId, Guid patientId, Guid eventId, string reason);
        bool DeleteWoundCareAssetNew(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId);
        bool DeleteScheduleEventAssetNew(Guid episodeId, Guid patientId, Guid eventId, Guid assetId);
        List<ScheduleEvent> GetScheduledEventsNew(Guid episodeId, Guid patientId, string discipline);
        List<ScheduleEvent> GetScheduledEventsNew(Guid patientId, string discipline, DateRange dateRange);
        bool ReopenNew(Guid episodeId, Guid patientId, Guid eventId);
        bool ReassignNew(Guid episodeId, Guid patientId, Guid eventId, Guid oldEmployeeId, Guid employeeId);
        bool ToggleScheduleStatusNew(Guid episodeId, Guid patientId, Guid eventId , bool isDelete);
        bool UpdateEpisodeNew(PatientEpisode episode, List<ScheduleEvent> scheduleEvents);
        bool AddMultiDateRangeScheduleNew(PatientEpisode episode, int disciplineTask, string discipline, Guid userId, bool isBillable, DateTime startDate, DateTime endDate);
        bool AddMultiDayScheduleNew(PatientEpisode episode, Guid userId, int disciplineTaskId, string visitDates);
        bool AddSchedules(PatientEpisode episode, List<ScheduleEvent> scheduleEvents);
        VisitNoteViewData GetVisitNote(ScheduleEvent scheduleEvent, NoteArguments arguments);
        VisitNoteViewData GetVisitNoteForContent(Guid patientId, Guid noteId,Guid previousNoteId, string type);
        VisitNoteViewData GetVisitNotePrint();
        VisitNoteViewData GetVisitNotePrint(int type);
        VisitNoteViewData GetVisitNotePrint(ScheduleEvent scheduledEvent, NoteArguments arguments);
        Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId);
        Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId, DateTime eventDate);
        MissedVisit GetMissedVisitPrint();
        MissedVisit GetMissedVisitPrint(Guid patientId, Guid eventId);
        VisitNoteViewData GetTransportationNote(Guid episodeId, Guid patientId, Guid eventId);


    }
}
