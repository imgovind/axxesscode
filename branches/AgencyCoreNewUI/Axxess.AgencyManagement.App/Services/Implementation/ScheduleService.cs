﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Log.Repositories;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.LookUp.Repositories;
    using Axxess.OasisC.Repositories;
    using Axxess.AgencyManagement.Domain;

    public class ScheduleService : IScheduleService
    {
        #region Private Members

        private readonly IDrugService drugService;
        private readonly ILogRepository logRepository;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly IReferralRepository referralRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IPatientEpisodeRepository patientEpisodeRepository;
        private readonly IPatientVisitNoteRepository patientVisitNoteRepository;
        private readonly ICommunicationNoteRepository communicationNoteRepository;

        #endregion

        #region Constructor

        public ScheduleService(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, ILogDataProvider logDataProvider, IOasisCDataProvider oasisDataProvider, IAssessmentService assessmentService, IDrugService drugService)
        {
            Check.Argument.IsNotNull(drugService, "drugService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.drugService = drugService;
            this.assessmentService = assessmentService;
            this.logRepository = logDataProvider.LogRepository;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetRepository = agencyManagementDataProvider.AssetRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.assessmentRepository = oasisDataProvider.OasisAssessmentRepository;
            this.scheduleRepository = agencyManagementDataProvider.ScheduleRepository;
            this.patientEpisodeRepository = agencyManagementDataProvider.PatientEpisodeRepository;
            this.patientVisitNoteRepository = agencyManagementDataProvider.PatientVisitNoteRepository;
            this.communicationNoteRepository = agencyManagementDataProvider.CommunicationNoteRepository;
        }

        #endregion


        public PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId)
        {
            PatientEpisode patientEpisode = null;
            if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            {
                patientEpisode = patientEpisodeRepository.GetEpisodeOnly(agencyId, episodeId, patientId);// database.Single<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.Id == episodeId);
                if (patientEpisode != null)
                {
                    var allEpisodes = patientEpisodeRepository.GetPatientAllEpisodes(agencyId, patientId);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate);
                    if (allEpisodes != null && allEpisodes.Count>0)
                    {
                        allEpisodes = allEpisodes.OrderBy(e => e.StartDate).ToList();
                        var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < patientEpisode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
                        var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > patientEpisode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
                        if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                        {
                            patientEpisode.NextEpisode = nextEpisode;
                        }
                        if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                        {
                            patientEpisode.PreviousEpisode = previousEpisode;

                        }
                    }
                    patientEpisode.ScheduleEvents = scheduleRepository.GetPatientScheduledEventsWithDateRangeOrNotNew(patientEpisode.AgencyId, patientEpisode.Id, patientEpisode.PatientId, true, patientEpisode.StartDate, patientEpisode.EndDate);
                    if (patientEpisode.Details.IsNotNullOrEmpty())
                    {
                        patientEpisode.Detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                    }
                }
            }
            return patientEpisode;
        }

        public PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            PatientEpisode episode = null;
            var allEpisodes = patientEpisodeRepository.GetPatientAllEpisodes(agencyId, patientId);//database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.StartDate);
            if (allEpisodes != null && allEpisodes.Count > 0)
            {
                allEpisodes = allEpisodes.OrderByDescending(e => e.StartDate).ToList();
                 episode = allEpisodes.Where(e => e.StartDate.Date <= date.Date).FirstOrDefault();
                if (episode != null)
                {
                    var patient = patientRepository.GetPatientOnly(patientId, agencyId);
                    if (patient != null)
                    {
                        episode.DisplayName = patient.DisplayName;
                    }
                    var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < episode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
                    var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > episode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
                    episode.ScheduleEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(episode.AgencyId, episode.Id, episode.PatientId, episode.StartDate, episode.EndDate, discipline, true, true);
                    if (episode.Details.IsNotNullOrEmpty())
                    {
                        episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                    }
                    if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                    {
                        episode.NextEpisode = nextEpisode;
                        if (episode.NextEpisode.Details.IsNotNullOrEmpty())
                        {
                            episode.NextEpisode.Detail = episode.NextEpisode.Details.ToObject<EpisodeDetail>();
                        }
                    }
                    if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                    {
                        episode.PreviousEpisode = previousEpisode;
                        if (episode.PreviousEpisode.Details.IsNotNullOrEmpty())
                        {
                            episode.PreviousEpisode.Detail = episode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                        }
                    }
                }
                else if (allEpisodes != null)
                {
                    var futureEpisode = allEpisodes.Where(e => e.IsActive).LastOrDefault();
                    if (futureEpisode != null)
                    {
                        var patient = patientRepository.GetPatientOnly(patientId, agencyId);
                        if (patient != null)
                        {
                            futureEpisode.DisplayName = patient.DisplayName;
                        }
                        var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < futureEpisode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
                        var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > futureEpisode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
                        futureEpisode.ScheduleEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(futureEpisode.AgencyId, futureEpisode.Id, futureEpisode.PatientId, futureEpisode.StartDate, futureEpisode.EndDate, discipline, true, true);
                        if (futureEpisode.Details.IsNotNullOrEmpty())
                        {
                            futureEpisode.Detail = futureEpisode.Details.ToObject<EpisodeDetail>();
                        }
                        if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                        {
                            futureEpisode.NextEpisode = nextEpisode;
                            if (futureEpisode.NextEpisode.Details.IsNotNullOrEmpty())
                            {
                                futureEpisode.NextEpisode.Detail = futureEpisode.NextEpisode.Details.ToObject<EpisodeDetail>();
                            }
                        }
                        if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                        {
                            futureEpisode.PreviousEpisode = previousEpisode;
                            if (futureEpisode.PreviousEpisode.Details.IsNotNullOrEmpty())
                            {
                                futureEpisode.PreviousEpisode.Detail = futureEpisode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                            }
                        }
                        episode = futureEpisode;
                    }
                }
            }
            return episode;
        }

        public PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId, string discipline)
        {
            PatientEpisode episode = null;
            if (!patientId.IsEmpty() && !episodeId.IsEmpty() && discipline.IsNotNullOrEmpty())
            {
                try
                {
                    var allEpisodes = patientEpisodeRepository.GetPatientAllEpisodes(agencyId, patientId);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate);
                    if (allEpisodes != null && allEpisodes.Count>0)
                    {
                        allEpisodes = allEpisodes.OrderBy(e => e.StartDate).ToList();
                        episode = allEpisodes.Where(e => e.Id == episodeId).SingleOrDefault();
                        if (episode != null)
                        {
                            var patient = patientRepository.GetPatientOnly(patientId, agencyId); 
                            if (patient != null)
                            {
                                episode.DisplayName = patient.DisplayName;
                            }
                            var previousEpisode = allEpisodes.Where(e => (e.StartDate < episode.StartDate) && e.IsActive == true).OrderByDescending(e => e.StartDate).FirstOrDefault();
                            var nextEpisode = allEpisodes.Where(e => (e.StartDate > episode.StartDate) && e.IsActive == true).OrderBy(e => e.StartDate).FirstOrDefault();

                            episode.ScheduleEvents = scheduleRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(episode.AgencyId, episode.Id, episode.PatientId, episode.StartDate, episode.EndDate, discipline, false, true);
                            if (episode.Details.IsNotNullOrEmpty())
                            {
                                episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                            }

                            if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                            {
                                episode.NextEpisode = nextEpisode;
                                if (episode.NextEpisode.Details.IsNotNullOrEmpty())
                                {
                                    episode.NextEpisode.Detail = episode.NextEpisode.Details.ToObject<EpisodeDetail>();
                                }
                            }
                            if (previousEpisode != null && !previousEpisode.Id.IsEmpty())
                            {
                                episode.PreviousEpisode = previousEpisode;
                                if (episode.PreviousEpisode.Details.IsNotNullOrEmpty())
                                {
                                    episode.PreviousEpisode.Detail = episode.PreviousEpisode.Details.ToObject<EpisodeDetail>();
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            return episode;

        }


        public PatientEpisode GetEpisodeNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline)
        {
            PatientEpisode episode = null;
            if (!patientId.IsEmpty() && !episodeId.IsEmpty() && discipline.IsNotNullOrEmpty())
            {
                try
                {
                    var allEpisodes = patientEpisodeRepository.GetPatientAllEpisodes(agencyId, patientId);//database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderBy(e => e.StartDate);
                    if (allEpisodes != null )
                    {
                        allEpisodes = allEpisodes.OrderBy(e => e.StartDate).ToList();
                        episode = allEpisodes.Where(e => e.Id == episodeId).SingleOrDefault();
                        if (episode != null)
                        {
                            var previousEpisode = allEpisodes.Where(e => (e.StartDate < episode.StartDate) && e.IsActive == true).OrderByDescending(e => e.StartDate).FirstOrDefault();
                            var nextEpisode = allEpisodes.Where(e => (e.StartDate > episode.StartDate) && e.IsActive == true).OrderBy(e => e.StartDate).FirstOrDefault();
                            episode.ScheduleEvents = scheduleRepository.GetPatientScheduledEventsNew(agencyId, episodeId, patientId, discipline, false);
                            if (episode.Details.IsNotNullOrEmpty())
                            {
                                episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                            }

                            if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                            {
                                episode.NextEpisode = nextEpisode;

                            }
                            if (previousEpisode != null && !previousEpisode.Id.IsEmpty())
                            {
                                episode.PreviousEpisode = previousEpisode;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            return episode;

        }

        public PatientEpisode GetEpisodeNew(Guid agencyId, Guid patientId, DateTime date, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            PatientEpisode episode = null;
            var allEpisodes = patientEpisodeRepository.GetPatientAllEpisodes(agencyId, patientId);// database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.IsActive == true && e.IsDischarged == false).OrderByDescending(e => e.StartDate);
            if (allEpisodes != null)
            {
                 episode = allEpisodes.Where(e => e.StartDate.Date <= date.Date).FirstOrDefault();
                if (episode != null)
                {
                    var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < episode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
                    var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > episode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
                    episode.ScheduleEvents = scheduleRepository.GetPatientScheduledEventsNew(agencyId, episode.Id, episode.PatientId, discipline, false);
                    if (episode.Details.IsNotNullOrEmpty())
                    {
                        episode.Detail = episode.Details.ToObject<EpisodeDetail>();
                    }
                    if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                    {
                        episode.NextEpisode = nextEpisode;
                    }
                    if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                    {
                        episode.PreviousEpisode = previousEpisode;
                    }
                }
                else if (allEpisodes != null)
                {
                    var futureEpisode = allEpisodes.Where(e => e.IsActive).LastOrDefault();
                    if (futureEpisode != null)
                    {
                        var previousEpisode = allEpisodes.Where(e => e.StartDate.Date < futureEpisode.StartDate.Date).OrderByDescending(e => e.StartDate).FirstOrDefault();
                        var nextEpisode = allEpisodes.Where(e => e.StartDate.Date > futureEpisode.StartDate.Date).OrderBy(e => e.StartDate).FirstOrDefault();
                        futureEpisode.ScheduleEvents = scheduleRepository.GetPatientScheduledEventsNew(agencyId, futureEpisode.Id, futureEpisode.PatientId, discipline, false);
                        if (futureEpisode.Details.IsNotNullOrEmpty())
                        {
                            futureEpisode.Detail = futureEpisode.Details.ToObject<EpisodeDetail>();
                        }
                        if (nextEpisode != null && nextEpisode.Id != Guid.Empty)
                        {
                            futureEpisode.NextEpisode = nextEpisode;
                        }
                        if (previousEpisode != null && previousEpisode.Id != Guid.Empty)
                        {
                            futureEpisode.PreviousEpisode = previousEpisode;
                        }
                        episode = futureEpisode;
                    }
                }
            }
            return episode;
        }

       
    }
}
