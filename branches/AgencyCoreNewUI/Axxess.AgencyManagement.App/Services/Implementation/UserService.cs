﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;


    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;
    using Axxess.Log.Enums;
    using Axxess.Log.Common;

    public class UserService : IUserService
    {
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IPatientRepository patientRepository;

        public UserService(IAgencyManagementDataProvider agencyManagmentDataProvider, IMembershipDataProvider membershipDataProvider, IPatientService patientService)
        {
            Check.Argument.IsNotNull(patientService, "patientService");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagmentDataProvider, "agencyManagmentDataProvider");

            this.patientService = patientService;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagmentDataProvider.UserRepository;
            this.assetRepository = agencyManagmentDataProvider.AssetRepository;
            this.patientRepository = agencyManagmentDataProvider.PatientRepository;
        }

        public bool IsEmailAddressInUse(string emailAddress)
        {
            var result = false;
            var login = loginRepository.Find(emailAddress);
            if (login != null && login.IsActive)
            {
                var users = userRepository.GetUsersByLoginId(login.Id, Current.AgencyId);
                if (users.Count > 0)
                {
                    result = true;
                }
            }
            return  result;
        }

        public bool IsPasswordCorrect(Guid userId, string password)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(password, login.PasswordHash, login.PasswordSalt))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsSignatureCorrect(Guid userId, string signature)
        {
            if (!userId.IsEmpty() && signature.IsNotNullOrEmpty())
            {
                var user = userRepository.Get(userId, Current.AgencyId);
                if (user != null)
                {
                    var login = loginRepository.Find(user.LoginId);
                    if (login != null)
                    {
                        var saltedHash = new SaltedHash();
                        if (saltedHash.VerifyHashAndSalt(signature, login.SignatureHash, login.SignatureSalt))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool CreateUser(User user) {
            try {
                var isNewLogin = false;
                var login = loginRepository.Find(user.EmailAddress);
                if (login == null) {
                    login = new Login();
                    login.DisplayName = user.FirstName;
                    login.EmailAddress = user.EmailAddress;
                    login.Role = Roles.ApplicationUser.ToString();
                    login.IsActive = true;
                    login.IsLocked = false;
                    login.IsAxxessAdmin = false;
                    login.IsAxxessSupport = false;
                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Add(login)) isNewLogin = true;
                }
                user.LoginId = login.Id;
                user.Profile = user.Profile;
                user.Profile.EmailWork = user.EmailAddress;
                if (user.HomePhoneArray != null && user.HomePhoneArray.Count > 0) user.Profile.PhoneHome = user.HomePhoneArray.ToArray().PhoneEncode();
                if (user.MobilePhoneArray != null && user.MobilePhoneArray.Count > 0) user.Profile.PhoneMobile = user.MobilePhoneArray.ToArray().PhoneEncode();
                if (user.FaxPhoneArray != null && user.FaxPhoneArray.Count > 0) user.Profile.PhoneFax = user.FaxPhoneArray.ToArray().PhoneEncode();
                if (userRepository.Add(user)) {
                    string bodyText = string.Empty;
                    string subject = string.Format("{0} - Invitation to use AgencyCore - Home Health Software by Axxess", user.AgencyName);
                    if (isNewLogin) {
                        var parameters = string.Format("id={0}&agencyid={1}", user.Id, user.AgencyId);
                        var encryptedParameters = string.Format("?enc={0}", Crypto.Encrypt(parameters));
                        bodyText = MessageBuilder.PrepareTextFrom("NewUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName, "encryptedQueryString", encryptedParameters);
                    } else bodyText = MessageBuilder.PrepareTextFrom("ExistingUserConfirmation", "firstname", user.FirstName, "agencyname", user.AgencyName);
                    UserEngine.Refresh(Current.AgencyId);
                    Notify.User(CoreSettings.NoReplyEmail, user.EmailAddress, subject, bodyText);
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserAdded, string.Empty);
                    return true;
                }
            } catch (Exception ex) {
                Logger.Exception(ex);
                return false;
            }
            return false;
        }

        public bool DeleteUser(Guid userId)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            var result = false;
            if (user != null)
            {
                var accounts = userRepository.GetUsersByLoginId(user.LoginId);
                if (accounts != null)
                {
                    if (userRepository.Delete(Current.AgencyId, userId))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId,LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserDeleted, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public List<UserVisit> GetScheduleLean(Guid userId, DateTime from, DateTime to)
        {
            var userVisits = new List<UserVisit>();
            var status = ScheduleStatusFatory.OnAndAfterQAStatus().ToArray(); 
            var aditionalFilter = string.Format(" AND scheduleevents.Status NOT IN ( {0} ) ", status.Select(s => s.ToString()).ToArray().Join(","));
            var scheduledEvents = userRepository.GetScheduleLean(Current.AgencyId, userId, from, to, false, aditionalFilter);
            if (scheduledEvents != null && scheduledEvents.Count > 0)
            {
                scheduledEvents.ForEach(scheduledEvent =>
                {
                    var visitNote = string.Empty;
                    var statusComments = string.Empty;

                    var episodeDetail = scheduledEvent.EpisodeNotes.IsNotNullOrEmpty() ? scheduledEvent.EpisodeNotes.ToObject<EpisodeDetail>() : new EpisodeDetail();
                    if (scheduledEvent.Comments.IsNotNullOrEmpty())
                    {
                        visitNote = scheduledEvent.Comments.Clean();
                    }
                    if (scheduledEvent.StatusComment.IsNotNullOrEmpty())
                    {
                        statusComments = scheduledEvent.StatusComment;
                    }

                    Common.Url.Set(scheduledEvent, false, false);
                    userVisits.Add(new UserVisit
                    {
                        Id = scheduledEvent.EventId,
                        VisitNotes = visitNote,
                        Url = scheduledEvent.Url,
                        Status = scheduledEvent.Status,
                        PatientName = scheduledEvent.PatientName,
                        StatusComment = statusComments,
                        DisciplineTask = scheduledEvent.DisciplineTask,
                        EpisodeId = scheduledEvent.EpisodeId,
                        PatientId = scheduledEvent.PatientId,
                        EpisodeNotes = episodeDetail.Comments.Clean(),
                        VisitDate = scheduledEvent.VisitDate,
                        ScheduleDate = scheduledEvent.EventDate,
                        IsMissedVisit = scheduledEvent.IsMissedVisit
                    });
                });
            }
            return userVisits;
        }

        public List<UserVisit> GetScheduleLeanAll(Guid userId, DateTime from, DateTime to)
        {
            var userVisits = new List<UserVisit>();
            var scheduledEvents = userRepository.GetScheduleLean(Current.AgencyId, userId, from, to, false, string.Empty);
            if (scheduledEvents != null && scheduledEvents.Count > 0)
            {
                scheduledEvents.ForEach(scheduledEvent =>
                {
                    var visitNote = string.Empty;
                    var statusComments = string.Empty;

                    var episodeDetail = scheduledEvent.EpisodeNotes.IsNotNullOrEmpty() ? scheduledEvent.EpisodeNotes.ToObject<EpisodeDetail>() : new EpisodeDetail();
                    if (scheduledEvent.Comments.IsNotNullOrEmpty())
                    {
                        visitNote = scheduledEvent.Comments.Clean();
                    }
                    if (scheduledEvent.StatusComment.IsNotNullOrEmpty())
                    {
                        statusComments = scheduledEvent.StatusComment;
                    }

                    Common.Url.Set(scheduledEvent, false, false);
                    userVisits.Add(new UserVisit
                    {
                        Id = scheduledEvent.EventId,
                        VisitNotes = visitNote,
                        Url = scheduledEvent.Url,
                        Status = scheduledEvent.Status,
                        PatientName = scheduledEvent.PatientName,
                        StatusComment = statusComments,
                        DisciplineTask = scheduledEvent.DisciplineTask,
                        EpisodeId = scheduledEvent.EpisodeId,
                        PatientId = scheduledEvent.PatientId,
                        EpisodeNotes = episodeDetail.Comments.Clean(),
                        VisitDate = scheduledEvent.VisitDate,
                        ScheduleDate = scheduledEvent.EventDate,
                        IsMissedVisit = scheduledEvent.IsMissedVisit
                    });
                });
            }
            return userVisits;
        }

        public bool UpdateProfile(User user)
        {
            var result = false;

            if (userRepository.UpdateProfile(user))
            {
                result = true;
                if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty() || user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                {
                    User userInfo = userRepository.Get(user.Id, Current.AgencyId);
                    Login login = loginRepository.Find(userInfo.LoginId);
                    if (userInfo != null && login != null)
                    {
                        if (user.PasswordChanger.CurrentPassword.IsNotNullOrEmpty())
                        {
                            string passwordsalt = string.Empty;
                            string passwordHash = string.Empty;

                            var saltedHash = new SaltedHash();
                            saltedHash.GetHashAndSalt(user.PasswordChanger.NewPassword, out passwordHash, out passwordsalt);
                            login.PasswordSalt = passwordsalt;
                            login.PasswordHash = passwordHash;
                        }

                        if (user.SignatureChanger.CurrentSignature.IsNotNullOrEmpty())
                        {
                            string signaturesalt = string.Empty;
                            string signatureHash = string.Empty;

                            var saltedHash = new SaltedHash();
                            saltedHash.GetHashAndSalt(user.SignatureChanger.NewSignature, out signatureHash, out signaturesalt);
                            login.SignatureSalt = signaturesalt;
                            login.SignatureHash = signatureHash;
                        }

                        if (!loginRepository.Update(login))
                        {
                            result = false;
                        }
                        else
                        {
                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserProfileUpdated, string.Empty);
                        }
                    }
                }
                
            }

            return result;
        }

        public bool AddLicense(License license, System.Web.HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var isAssetSaved = true;
            var user = userRepository.Get(license.UserId, Current.AgencyId);
            if (user != null)
            {
                if (httpFiles.Count > 0)
                {
                    foreach (string key in httpFiles.AllKeys)
                    {
                        HttpPostedFileBase file = httpFiles.Get(key);
                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                        {
                            var binaryReader = new BinaryReader(file.InputStream);

                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                license.AssetId = asset.Id;
                            }
                            else
                            {
                                isAssetSaved = false;
                                break;
                            }
                        }
                    }
                }
                if (isAssetSaved)
                {
                    license.Id = Guid.NewGuid();
                    license.Created = DateTime.Now;
                    if (license.OtherLicenseType.IsNotNullOrEmpty())
                    {
                        license.LicenseType = license.OtherLicenseType;
                    }
                    if (user.LicensesArray != null )
                    {
                        user.LicensesArray.Add(license);
                        user.Licenses = user.LicensesArray.ToXml();
                    }
                    else
                    {
                        user.LicensesArray = new List<License>();
                        user.LicensesArray.Add(license);
                        user.Licenses = user.LicensesArray.ToXml();
                    }
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateLicense(License license)
        {
            var user = userRepository.Get(license.UserId, Current.AgencyId);
            if (user != null) {
                var License = user.LicensesArray.Find(l => l.Id == license.Id);
                if (License != null) {
                    License.InitiationDate = license.InitiationDate;
                    License.ExpirationDate = license.ExpirationDate;
                    user.Licenses = user.LicensesArray.ToXml();
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool UpdateLicense(Guid id, Guid userId, DateTime initiationDate, DateTime expirationDate)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                var license = user.LicensesArray.Find(l => l.Id == id);
                if (license != null)
                {
                    license.InitiationDate = initiationDate;
                    license.ExpirationDate = expirationDate;
                    user.Licenses = user.LicensesArray.ToXml();
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool DeleteLicense(Guid id, Guid userId)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                var license = user.LicensesArray.Find(l => l.Id == id);
                if (license != null)
                {
                    if (!license.AssetId.IsEmpty())
                    {
                        assetRepository.Delete(license.AssetId);
                    }
                    user.LicensesArray.RemoveAll(l => l.Id == id);
                    user.Licenses = user.LicensesArray.ToXml();
                    if (userRepository.UpdateModel(user))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserLicenseDeleted, string.Empty);
                        return true;
                    }

                }
            }
            return false;
        }

        public bool UpdatePermissions(FormCollection formCollection)
        {
            var result = false;
            var userId = formCollection["UserId"] != null ? formCollection["UserId"].ToGuid() : Guid.Empty;
            var permissionArray = formCollection["PermissionsArray"] != null ? formCollection["PermissionsArray"].ToArray().ToList() : null;
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null && permissionArray != null && permissionArray.Count > 0)
            {
                user.PermissionsArray = permissionArray;
                if (user.PermissionsArray != null && user.PermissionsArray.Count > 0)
                {
                    user.Permissions = user.PermissionsArray.ToXml();
                }
                if (userRepository.UpdateModel(user))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserPermissionsUpdated, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public List<User> GetUserByBranchAndStatus(Guid branchId, int status)
        {
            var users = new List<User>();
            if (status == 0)
            {
                if (branchId.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId).ToList();
                }
            }
            else
            {
                if (branchId.IsEmpty())
                {
                    users = userRepository.GetUsersByStatus(Current.AgencyId,status).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchId, Current.AgencyId,status).ToList();
                }

            }
            return users.OrderBy(u=>u.FirstName).ThenBy(u=>u.LastName).ToList();
        }

        public IList<LicenseItem> GetUserLicenses()
        {
            var list = new List<LicenseItem>();

            var nonuserLicenses = userRepository.GetNonUserLicenses(Current.AgencyId);
            if (nonuserLicenses != null && nonuserLicenses.Count > 0)
            {
                nonuserLicenses.ForEach(license =>
                {
                    list.Add(license);
                });
            }

            var userLicenses = userRepository.GetSoftwareUserLicenses(Current.AgencyId);
            if (userLicenses != null && userLicenses.Count > 0)
            {
                userLicenses.ForEach(license =>
                {
                    license.DisplayName = UserEngine.GetName(license.UserId, license.AgencyId);
                    list.Add(license);
                });
            }
            return list.OrderBy(l => l.FirstName).ToList();
        }

        public IList<License> GetUserLicenses(Guid branchId, int status)
        {
            var list = new List<License>();

            var users = userRepository.GetAllUsers(Current.AgencyId, branchId, status);
            if (users != null && users.Count > 0)
            {
                users.ForEach(u =>
                {
                    if (u.Licenses.IsNotNullOrEmpty())
                    {
                        u.LicensesArray = u.Licenses.ToObject<List<License>>();
                        if (u.LicensesArray != null && u.LicensesArray.Count > 0)
                        {
                            u.LicensesArray.ForEach(l =>
                            {
                                l.UserDisplayName = string.Format("{0}, {1}", u.LastName, u.FirstName);
                                list.Add(l);
                            }
                                );
                        }
                    }
                });
            }
            return list;
        }

        public bool AddLicenseItem(LicenseItem licenseItem, System.Web.HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var isAssetSaved = true;
            if (httpFiles.Count > 0)
            {
                foreach (string key in httpFiles.AllKeys)
                {
                    HttpPostedFileBase file = httpFiles.Get(key);
                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        var binaryReader = new BinaryReader(file.InputStream);

                        var asset = new Asset
                        {
                            FileName = file.FileName,
                            AgencyId = Current.AgencyId,
                            ContentType = file.ContentType,
                            FileSize = file.ContentLength.ToString(),
                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                        };
                        if (assetRepository.Add(asset))
                        {
                            licenseItem.AssetId = asset.Id;
                        }
                        else
                        {
                            isAssetSaved = false;
                            break;
                        }
                    }
                }
            }
            if (isAssetSaved)
            {
                licenseItem.Id = Guid.NewGuid();
                licenseItem.Created = DateTime.Now;
                licenseItem.Modified = DateTime.Now;
                licenseItem.IsDeprecated = false;
                licenseItem.AgencyId = Current.AgencyId;

                if (licenseItem.OtherLicenseType.IsNotNullOrEmpty())
                {
                    licenseItem.LicenseType = licenseItem.OtherLicenseType;
                }
                if (userRepository.AddNonUserLicense(licenseItem))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, licenseItem.Id.ToString(), LogType.NonUserLicense, LogAction.UserLicenseAdded, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public IList<UserRate> GetUserRates(Guid userId)
        {
            var list = new List<UserRate>();
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null && user.Rates.IsNotNullOrEmpty())
            {
                list = user.Rates.ToObject<List<UserRate>>();
            }
            return list;
        }
    }
}
