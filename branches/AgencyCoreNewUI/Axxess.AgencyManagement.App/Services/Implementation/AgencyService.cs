﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Repositories;

    using Axxess.Log.Enums;
    using Axxess.Log.Common;

    using Enums;
    using Domain;
    using Common;
    using ViewData;

    public class AgencyService : IAgencyService
    {
        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;

        public AgencyService(IAgencyManagementDataProvider agencyManagementDataProvider, IMembershipDataProvider membershipDataProvider, ILookUpDataProvider lookUpDataProvider, IOasisCDataProvider oasisDataprovider)
        {
            Check.Argument.IsNotNull(oasisDataprovider, "oasisDataprovider");
            Check.Argument.IsNotNull(lookUpDataProvider, "lookUpDataProvider");
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.lookupRepository = lookUpDataProvider.LookUpRepository;
            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.planofCareRepository = oasisDataprovider.PlanofCareRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
        }

        public bool CreateAgency(Agency agency)
        {
            try
            {
                agency.IsSuspended = false;
                if (agency.ContactPhoneArray != null && agency.ContactPhoneArray.Count == 3)
                {
                    agency.ContactPersonPhone = agency.ContactPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterPhoneArray != null && agency.SubmitterPhoneArray.Count == 3)
                {
                    agency.SubmitterPhone = agency.SubmitterPhoneArray.ToArray().PhoneEncode();
                }

                if (agency.SubmitterFaxArray != null && agency.SubmitterFaxArray.Count == 3)
                {
                    agency.SubmitterFax = agency.SubmitterFaxArray.ToArray().PhoneEncode();
                }

                if (agencyRepository.Add(agency))
                {
                    var location = new AgencyLocation();
                    location.Name = agency.LocationName;
                    location.AddressLine1 = agency.AddressLine1;
                    location.AddressLine2 = agency.AddressLine2;
                    location.AddressCity = agency.AddressCity;
                    location.AddressStateCode = agency.AddressStateCode;
                    location.AddressZipCode = agency.AddressZipCode;
                    location.AgencyId = agency.Id;
                    location.IsMainOffice = true;
                    location.IsDeprecated = false;

                    if (agency.PhoneArray != null && agency.PhoneArray.Count > 0)
                    {
                        location.PhoneWork = agency.PhoneArray.ToArray().PhoneEncode();
                    }
                    if (agency.FaxArray != null && agency.FaxArray.Count > 0)
                    {
                        location.FaxNumber = agency.FaxArray.ToArray().PhoneEncode();
                    }

                    var zipCode = lookupRepository.GetZipCode(agency.AddressZipCode);
                    if (zipCode != null)
                    {
                        location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
                        location.MedicareProviderNumber = agency.MedicareProviderNumber;
                    }
                    location.Cost = "<ArrayOfCostRate><CostRate><RateDiscipline>SkilledNurse</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseTeaching</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseObservation</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>SkilledNurseManagement</RateDiscipline><PerUnit>200</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>PhysicalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyAssistance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>OccupationalTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapy</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>SpeechTherapyMaintenance</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>MedicareSocialWorker</RateDiscipline><PerUnit>250</PerUnit></CostRate><CostRate><RateDiscipline>HomeHealthAide</RateDiscipline><PerUnit>120</PerUnit></CostRate><CostRate><RateDiscipline>Attendant</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>CompanionCare</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>HomemakerServices</RateDiscipline><PerUnit>0</PerUnit></CostRate><CostRate><RateDiscipline>PrivateDutySitter</RateDiscipline><PerUnit>0</PerUnit></CostRate></ArrayOfCostRate>";
                    location.Id = Guid.NewGuid();
                    if (agencyRepository.AddLocation(location))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                        var user = new User
                        {
                            AgencyId = agency.Id,
                            AllowWeekendAccess = true,
                            AgencyName = agency.Name,
                            AgencyLocationId = location.Id,
                            Status = (int)UserStatus.Active,
                            LastName = agency.AgencyAdminLastName,
                            FirstName = agency.AgencyAdminFirstName,
                            PermissionsArray = GeneratePermissions(),
                            EmailAddress = agency.AgencyAdminUsername,
                            TitleType = TitleTypes.Administrator.GetDescription(),
                            Profile = new UserProfile() { EmailWork = agency.AgencyAdminUsername },
                            AgencyRoleList = new List<string>() { ((int)AgencyRoles.Administrator).ToString() }
                        };

                        IUserService userService = Container.Resolve<IUserService>();
                        return userService.CreateUser(user);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            return false;
        }

        private List<string> GeneratePermissions()
        {
            var list = new List<string>();

            var permissions = (Permissions[])Enum.GetValues(typeof(Permissions));

            foreach (Permissions permission in permissions)
            {
                ulong permissionId = (ulong)permission;
                if (permissionId != 0)
                {
                    list.Add((permissionId).ToString());
                }
            }

            return list;
        }

        public bool CreateLocation(AgencyLocation location)
        {
            var result = false;
            var zipCode = lookupRepository.GetZipCode(location.AddressZipCode);
            location.CBSA = zipCode != null ? zipCode.CBSA : string.Empty;
            location.AgencyId = Current.AgencyId;
            location.Id = Guid.NewGuid();
            if (agencyRepository.AddLocation(location))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId,LogDomain.Agency, Current.AgencyId, location.Id.ToString(), LogType.AgencyLocation, LogAction.AgencyLocationAdded, string.Empty);
                result = true;
            }

            return result;
        }

        public Agency GetAgency(Guid Id)
        {
            Agency agencyViewData = agencyRepository.Get(Id);
            if (agencyViewData != null)
            {
                return agencyViewData;
            }
            return null;
        }

        public bool CreateContact(AgencyContact contact)
        {
            contact.AgencyId = Current.AgencyId;
            contact.Id = Guid.NewGuid();
            try
            {
                if (agencyRepository.AddContact(contact))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, contact.AgencyId, contact.Id.ToString(), LogType.AgencyContact, LogAction.AgencyContactAdded, string.Empty);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public List<PatientEpisodeEvent> GetQASchedule(Guid branchId, int status)
        {
            var events = new List<PatientEpisodeEvent>();
            var schedules = patientRepository.GetScheduleByBranchAndStatus(Current.AgencyId, branchId, status, ScheduleStatusFatory.CaseManagerStatus().ToArray(), false);
            if (schedules != null && schedules.Count > 0)
            {
                var userIds = schedules.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                schedules.ForEach(s =>
                {
                    var details = s.EpisodeNotes.IsNotNullOrEmpty() ? s.EpisodeNotes.ToObject<EpisodeDetail>() : new EpisodeDetail();
                    Common.Url.Set(s, false, false);
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    events.Add(new PatientEpisodeEvent
                    {
                        PrintUrl = s.PrintUrl,
                        Status = s.StatusName,
                        PatientName = s.PatientName,
                        TaskName = s.DisciplineTaskName,
                        UserName = user != null ? user.DisplayName : string.Empty,// UserEngine.GetName(s.UserId, Current.AgencyId),
                        YellowNote = s.Comments.IsNotNullOrEmpty() ? s.Comments.Clean() : string.Empty,
                        RedNote = s.StatusComment.IsNotNullOrEmpty() ? s.StatusComment : string.Empty,
                        BlueNote = details != null && details.Comments.IsNotNullOrEmpty() ? details.Comments.Clean() : string.Empty,
                        EventDate = s.EventDate.IsValid() ? s.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                        CustomValue = string.Format("{0}|{1}|{2}|{3}", s.EpisodeId, s.PatientId, s.EventId, s.DisciplineTask)
                    });
                });
            }
            return events;
        }

        public List<RecertEvent> GetRecertsPastDue(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate, bool isLimitForWidget, int limit)
        {
            var pastDueRecerts = new List<RecertEvent>();
            var scheduleEvents = patientRepository.GetPastDueRecertsLeanByDateRange(Current.AgencyId, branchId, insuranceId, startDate, endDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var episodeIds = scheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                if (episodeIds != null && episodeIds.Count > 0)
                {
                    var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks().ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
                    var dischargeStatus = ScheduleStatusFatory.OASISCompleted().ToArray();//new int[] { (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OasisReopened, (int)ScheduleStatus.OasisSaved };
                    var recertAndROCStatus = dischargeStatus;
                    var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments().ToArray();

                    var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                    episodeIds.ForEach(e =>
                    {
                        var schedules = scheduleEvents.Where(s => s.EpisodeId == e).OrderByDescending(s => s.EventDate.Date).ToList();
                        if (schedules != null)
                        {
                            if (schedules.Count == 1)
                            {
                                var recert = new RecertEvent();
                                var evnt = schedules.FirstOrDefault();
                                if ((recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask) && recertAndROCStatus.Contains(evnt.Status)) || (dischargeDisciplineTasks.Contains(evnt.DisciplineTask) && dischargeStatus.Contains(evnt.Status)))
                                {
                                }
                                else if (recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask))
                                {
                                    if (!evnt.UserId.IsEmpty())
                                    {
                                        var user = users.SingleOrDefault(u => u.Id == evnt.UserId);
                                        if (user != null)
                                        {
                                            recert.AssignedTo = user.DisplayName;
                                        }
                                        else
                                        {
                                            recert.AssignedTo = "Unassigned";
                                        }
                                    }
                                    else
                                    {
                                        recert.AssignedTo = "Unassigned";
                                    }
                                    if (evnt.EventDate.Date <= DateTime.MinValue.Date)
                                    {
                                        recert.DateDifference = DateTime.Now.Subtract(evnt.EndDate).Days;
                                        recert.TargetDate = evnt.EndDate;
                                    }
                                    else
                                    {
                                        recert.DateDifference = endDate.Subtract(evnt.EventDate.Date).Days;
                                        recert.TargetDate = evnt.EventDate;
                                    }
                                    recert.Status = evnt.Status;
                                    recert.PatientName = evnt.PatientName;
                                    recert.PatientIdNumber = evnt.PatientIdNumber;
                                    recert.EventDate = evnt.EventDate;
                                    pastDueRecerts.Add(recert);
                                }
                            }
                            else if (schedules.Count > 1)
                            {
                                if (schedules.Exists(s => dischargeDisciplineTasks.Contains(s.DisciplineTask) && dischargeStatus.Contains(s.Status)))
                                {
                                }
                                else
                                {
                                    var oasisROCOrRecert = schedules.Where(s => recertAndROCDisciplineTasks.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                                    if (oasisROCOrRecert != null)
                                    {
                                        if (recertAndROCStatus.Contains(oasisROCOrRecert.Status))
                                        {
                                        }
                                        else
                                        {
                                            var recert = new RecertEvent();
                                            if (!oasisROCOrRecert.UserId.IsEmpty())
                                            {
                                                var user = users.SingleOrDefault(u => u.Id == oasisROCOrRecert.UserId);
                                                if (user != null)
                                                {
                                                    recert.AssignedTo = user.DisplayName;
                                                }
                                                else
                                                {
                                                    recert.AssignedTo = "Unassigned";
                                                }
                                            }
                                            else
                                            {
                                                recert.AssignedTo = "Unassigned";
                                            }
                                            if (oasisROCOrRecert.EventDate.Date <= DateTime.MinValue.Date)
                                            {
                                                recert.DateDifference = DateTime.Now.Subtract(oasisROCOrRecert.EndDate).Days;
                                                recert.TargetDate = oasisROCOrRecert.EndDate;
                                            }
                                            else
                                            {
                                                recert.DateDifference = endDate.Subtract(oasisROCOrRecert.EventDate.Date).Days;
                                                recert.TargetDate = oasisROCOrRecert.EventDate;
                                            }
                                            recert.Status = oasisROCOrRecert.Status;
                                            recert.PatientName = oasisROCOrRecert.PatientName;
                                            recert.PatientIdNumber = oasisROCOrRecert.PatientIdNumber;
                                            recert.EventDate = oasisROCOrRecert.EventDate;
                                            pastDueRecerts.Add(recert);
                                        }
                                    }

                                }
                            }
                        }
                        if (isLimitForWidget && pastDueRecerts.Count >= limit)
                        {
                            return;
                        }
                    });

                }
            }
            return pastDueRecerts;
        }

        public List<RecertEvent> GetRecertsPastDueWidget()
        {
            return this.GetRecertsPastDue(Guid.Empty, 0, DateTime.Now.AddMonths(-4), DateTime.Now, true, 5);//new List<RecertEvent>();;
        }

        public List<RecertEvent> GetRecertsUpcoming(Guid branchId, int insuranceId, DateTime startDate, DateTime endDate , bool isLimitForWidget, int limit )
        {
            var upcomingRecets = new List<RecertEvent>();
            var scheduleEvents = patientRepository.GetUpcomingRecertsLean(Current.AgencyId, branchId, insuranceId, startDate, endDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var episodeIds = scheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                if (episodeIds != null && episodeIds.Count > 0)
                {
                    var dischargeDisciplineTasks = DisciplineTaskFactory.AllDischargingOASISDisciplineTasks().ToArray();// new int[] { (int)DisciplineTasks.OASISCDischarge, (int)DisciplineTasks.OASISCDischargeOT, (int)DisciplineTasks.OASISCDischargePT, (int)DisciplineTasks.OASISCDeath, (int)DisciplineTasks.OASISCDeathOT, (int)DisciplineTasks.OASISCDeathPT, (int)DisciplineTasks.OASISCTransferDischarge, (int)DisciplineTasks.OASISBDischarge, (int)DisciplineTasks.OASISBDeathatHome, (int)DisciplineTasks.NonOASISDischarge };
                    var dischargeStatus = ScheduleStatusFatory.OASISCompleted().ToArray();// new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };
                    var recertAndROCStatus = dischargeStatus; //new int[] { (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedPendingReview, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisCompletedNotExported };
                    var recertAndROCDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments().ToArray();//new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.NonOASISRecertification, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCarePT, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISBResumptionofCare };
                    var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                    episodeIds.ForEach(e =>
                    {
                        var schedules = scheduleEvents.Where(s => s.EpisodeId == e).OrderByDescending(s => s.EventDate.Date).ToList();
                        if (schedules != null)
                        {
                            if (schedules.Count == 1)
                            {
                                var recert = new RecertEvent();
                                var evnt = schedules.FirstOrDefault();
                                if ((recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask) && recertAndROCStatus.Contains(evnt.Status)) || (dischargeDisciplineTasks.Contains(evnt.DisciplineTask) && dischargeStatus.Contains(evnt.Status)))
                                {
                                }
                                else if(recertAndROCDisciplineTasks.Contains(evnt.DisciplineTask))
                                {
                                    if (!evnt.UserId.IsEmpty())
                                    {
                                        var user = users.SingleOrDefault(u => u.Id == evnt.UserId);
                                        if (user != null)
                                        {
                                            recert.AssignedTo = user.DisplayName;
                                        }
                                        else
                                        {
                                            recert.AssignedTo = "Unassigned";
                                        }
                                    }
                                    else
                                    {
                                        recert.AssignedTo = "Unassigned";
                                    }
                                    if (evnt.EventDate.Date <= DateTime.MinValue.Date)
                                    {
                                        recert.DateDifference = DateTime.Now.Subtract(evnt.EndDate).Days;
                                        recert.TargetDate = evnt.EndDate;
                                    }
                                    else
                                    {
                                        recert.DateDifference = endDate.Subtract(evnt.EventDate.Date).Days;
                                        recert.TargetDate = evnt.EventDate;
                                    }
                                    recert.Status = evnt.Status;
                                    recert.PatientName = evnt.PatientName;
                                    recert.PatientIdNumber = evnt.PatientIdNumber;
                                    recert.EventDate = evnt.EventDate;
                                    upcomingRecets.Add(recert);
                                }
                            }
                            else if (schedules.Count > 1)
                            {
                                if (schedules.Exists(s => dischargeDisciplineTasks.Contains(s.DisciplineTask) && dischargeStatus.Contains(s.Status)))
                                {
                                }
                                else
                                {
                                    var oasisROCOrRecert = schedules.Where(s => recertAndROCDisciplineTasks.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                                    if (oasisROCOrRecert != null)
                                    {
                                        if (recertAndROCStatus.Contains(oasisROCOrRecert.Status))
                                        {
                                        }
                                        else
                                        {
                                            var recert = new RecertEvent();
                                            if (!oasisROCOrRecert.UserId.IsEmpty())
                                            {
                                                var user = users.SingleOrDefault(u => u.Id == oasisROCOrRecert.UserId);
                                                if (user != null)
                                                {
                                                    recert.AssignedTo = user.DisplayName;
                                                }
                                                else
                                                {
                                                    recert.AssignedTo = "Unassigned";
                                                }
                                            }
                                            else
                                            {
                                                recert.AssignedTo = "Unassigned";
                                            }
                                            if (oasisROCOrRecert.EventDate.Date <= DateTime.MinValue.Date)
                                            {
                                                recert.DateDifference = DateTime.Now.Subtract(oasisROCOrRecert.EndDate).Days;
                                                recert.TargetDate = oasisROCOrRecert.EndDate;
                                            }
                                            else
                                            {
                                                recert.DateDifference = endDate.Subtract(oasisROCOrRecert.EventDate.Date).Days;
                                                recert.TargetDate = oasisROCOrRecert.EventDate;
                                            }
                                            recert.Status = oasisROCOrRecert.Status;
                                            recert.PatientName = oasisROCOrRecert.PatientName;
                                            recert.PatientIdNumber = oasisROCOrRecert.PatientIdNumber;
                                            recert.EventDate = oasisROCOrRecert.EventDate;
                                            upcomingRecets.Add(recert);
                                        }
                                    }
                                }
                            }
                        }
                        if (isLimitForWidget && upcomingRecets.Count >= limit)
                        {
                            return ;
                        }
                    });
                }
            }
            return upcomingRecets;
        }

        public List<RecertEvent> GetRecertsUpcomingWidget()
        {
            return this.GetRecertsUpcoming(Guid.Empty, 0, DateTime.Now, DateTime.Now.AddDays(24), true, 5);
        }

        public List<InsuranceViewData> GetInsurances()
        {
            var insuranceList = new List<InsuranceViewData>();
            lookupRepository.Insurances().ForEach(i =>
            {
                insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            });
            agencyRepository.GetInsurances(Current.AgencyId).ForEach(i =>
            {
                insuranceList.Add(new InsuranceViewData { Id = i.Id, Name = i.Name });
            });
            return insuranceList;
        }

        public List<Order> GetOrdersToBeSent(Guid BranchId, bool sendAutomatically, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var status = new List<int> { (int)ScheduleStatus.OrderToBeSentToPhysician, (int)ScheduleStatus.EvalToBeSentToPhysician };
            var schedules = patientRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                PatientId = po.PatientId,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                CreatedDate = po.OrderDateFormatted

                            });
                        });
                    }
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareOrdersIds);

                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    Type = OrderType.HCFA485,
                                    Text = DisciplineTasks.HCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, planofCareStandAloneOrdersIds);

                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                var patient = patientRepository.GetPatientOnly(poc.PatientId, Current.AgencyId);
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = patient != null && patient.DisplayName.IsNotNullOrEmpty() ? patient.DisplayName : "",
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, (int)ScheduleStatus.OrderToBeSentToPhysician, faceToFaceEncounterOrdersIds);
                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                        {
                            var evnt = faceToFaceEncounterSchedules.SingleOrDefault(s => s.EventId == ffe.Id);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!ffe.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (ffe.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = ffe.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    PatientId = ffe.PatientId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy")
                                });
                            }
                        });
                    }
                }
                var evalDisciplineTasks = DisciplineTaskFactory.PhysicianEvalNotes();
                var evalOrdersSchedule = schedules.Where(s => evalDisciplineTasks.Exists(d => (int)d == s.DisciplineTask)).ToList();
                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int> { status[1] }, evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = eval.Id,
                                PatientId = eval.PatientId,
                                Type = GetOrderType(eval.NoteType),
                                Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                Number = eval.OrderNumber,
                                PatientName = eval.DisplayName,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
                                SendDate = eval.SentDate
                            });
                        });
                    }
                }
            }
            return orders.Where(o => o.PhysicianAccess == sendAutomatically).OrderByDescending(o => o.CreatedDate).ToList();
        }

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                }
            }
            return orderType;
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                }
            }
            return task;
        }

        public List<Order> GetProcessedOrders(Guid BranchId, DateTime startDate, DateTime endDate, List<int> status)
        {
            var orders = new List<Order>();
            var schedules = patientRepository.GetOrderScheduleEvents(Current.AgencyId, BranchId, startDate, endDate, status);
            if (schedules != null && schedules.Count > 0 && status.Count > 1)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPhysicianOrders(Current.AgencyId, status[0], physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                PatientId = po.PatientId,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PhysicianName = po.PhysicianName.IsNotNullOrEmpty() ? po.PhysicianName : physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true),
                                CreatedDate = po.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate,
                                SendDate = po.SentDate,
                                PhysicianSignatureDate = po.PhysicianSignatureDate
                            });
                        });
                    }
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, status[0], planofCareOrdersIds);
                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.FirstOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    Type = OrderType.HCFA485,
                                    Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SendDate = poc.SentDate,
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAloneByStatus(Current.AgencyId, status[0], planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetFaceToFaceEncounterOrders(Current.AgencyId, status[0], faceToFaceEncounterOrdersIds);

                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                        {
                            var evnt = faceToFaceEncounterSchedules.SingleOrDefault(s => s.EventId == ffe.Id);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    PatientId = ffe.PatientId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate,
                                    SendDate = ffe.RequestDate,
                                    PhysicianSignatureDate = ffe.SignatureDate
                                });
                            }
                        });
                    }
                }
                var evalDisciplineTasks = DisciplineTaskFactory.PhysicianEvalNotes();
                var evalOrdersSchedule = schedules.Where(s => evalDisciplineTasks.Exists(d => (int)d == s.DisciplineTask)).ToList();
                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int> { status[1] }, evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var evnt = evalOrdersSchedule.SingleOrDefault(s => s.EventId == eval.Id);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = eval.Id,
                                    PatientId = eval.PatientId,
                                    Type = GetOrderType(eval.NoteType),
                                    Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                    Number = eval.OrderNumber,
                                    PatientName = eval.DisplayName,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate,
                                    SendDate = eval.SentDate,
                                    PhysicianSignatureDate = eval.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public Order GetOrder(Guid id, Guid patientId, string type)
        {
            var order = new Order();
           
            if (!id.IsEmpty() && !patientId.IsEmpty() && type.IsNotNullOrEmpty() && type.IsInteger() && Enum.IsDefined(typeof(OrderType), type.ToInteger()))
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var typeEnum = (OrderType)type.ToInteger();
                    switch (typeEnum)
                    {
                        case OrderType.PhysicianOrder:
                            var physicianOrder = patientRepository.GetOrderOnly(id, Current.AgencyId);
                            if (physicianOrder != null)
                            {
                                order = new Order
                                {
                                    Id = physicianOrder.Id,
                                    PatientId = physicianOrder.PatientId,
                                    Type = OrderType.PhysicianOrder,
                                    Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                    Number = physicianOrder.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    PhysicianName = physicianOrder.PhysicianName,
                                    ReceivedDate = physicianOrder.ReceivedDate > DateTime.MinValue ? physicianOrder.ReceivedDate : physicianOrder.SentDate,
                                    SendDate = physicianOrder.SentDate,
                                    PhysicianSignatureDate = physicianOrder.PhysicianSignatureDate
                                };
                            }
                            break;
                        case OrderType.FaceToFaceEncounter:
                            var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(id, Current.AgencyId);
                            if (faceToFaceEncounter != null)
                            {
                                order = new Order
                                {
                                    Id = faceToFaceEncounter.Id,
                                    PatientId = faceToFaceEncounter.PatientId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = faceToFaceEncounter.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = faceToFaceEncounter.ReceivedDate > DateTime.MinValue ? faceToFaceEncounter.ReceivedDate : faceToFaceEncounter.RequestDate,
                                    SendDate = faceToFaceEncounter.RequestDate,
                                    PhysicianSignatureDate = faceToFaceEncounter.SignatureDate
                                };
                            }
                            break;
                        case OrderType.HCFA485:
                        case OrderType.NonOasisHCFA485:
                            var planofCare = planofCareRepository.Get(Current.AgencyId, id);
                            if (planofCare != null)
                            {
                                order = new Order
                                {
                                    Id = planofCare.Id,
                                    Type = OrderType.HCFA485,
                                    PatientId = planofCare.PatientId,
                                    Text = "HCFA485" == type ? DisciplineTasks.HCFA485.GetDescription() : ("NonOasisHCFA485" == type ? DisciplineTasks.NonOasisHCFA485.GetDescription() : string.Empty),
                                    Number = planofCare.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = planofCare.ReceivedDate > DateTime.MinValue ? planofCare.ReceivedDate : planofCare.SentDate,
                                    SendDate = planofCare.SentDate,
                                    PhysicianSignatureDate = planofCare.PhysicianSignatureDate
                                };
                            }
                            break;
                        case OrderType.PtEvaluation:
                            var ptEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptEval != null)
                            {
                                order = new Order
                                {
                                    Id = ptEval.Id,
                                    PatientId = ptEval.PatientId,
                                    Type = OrderType.PtEvaluation,
                                    Text = DisciplineTasks.PTEvaluation.GetDescription(),
                                    Number = ptEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptEval.ReceivedDate > DateTime.MinValue ? ptEval.ReceivedDate : ptEval.SentDate,
                                    SendDate = ptEval.SentDate,
                                    PhysicianSignatureDate = ptEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case OrderType.PtReEvaluation:
                            var ptReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (ptReEval != null)
                            {
                                order = new Order
                                {
                                    Id = ptReEval.Id,
                                    PatientId = ptReEval.PatientId,
                                    Type = OrderType.PtReEvaluation,
                                    Text = DisciplineTasks.PTReEvaluation.GetDescription(),
                                    Number = ptReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = ptReEval.ReceivedDate > DateTime.MinValue ? ptReEval.ReceivedDate : ptReEval.SentDate,
                                    SendDate = ptReEval.SentDate,
                                    PhysicianSignatureDate = ptReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case OrderType.OtEvaluation:
                            var otEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otEval != null)
                            {
                                order = new Order
                                {
                                    Id = otEval.Id,
                                    PatientId = otEval.PatientId,
                                    Type = OrderType.OtEvaluation,
                                    Text = DisciplineTasks.OTEvaluation.GetDescription(),
                                    Number = otEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otEval.ReceivedDate > DateTime.MinValue ? otEval.ReceivedDate : otEval.SentDate,
                                    SendDate = otEval.SentDate,
                                    PhysicianSignatureDate = otEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case OrderType.OtReEvaluation:
                            var otReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (otReEval != null)
                            {
                                order = new Order
                                {
                                    Id = otReEval.Id,
                                    PatientId = otReEval.PatientId,
                                    Type = OrderType.OtReEvaluation,
                                    Text = DisciplineTasks.OTReEvaluation.GetDescription(),
                                    Number = otReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = otReEval.ReceivedDate > DateTime.MinValue ? otReEval.ReceivedDate : otReEval.SentDate,
                                    SendDate = otReEval.SentDate,
                                    PhysicianSignatureDate = otReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case OrderType.StEvaluation:
                            var stEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stEval != null)
                            {
                                order = new Order
                                {
                                    Id = stEval.Id,
                                    PatientId = stEval.PatientId,
                                    Type = OrderType.StEvaluation,
                                    Text = DisciplineTasks.STEvaluation.GetDescription(),
                                    Number = stEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stEval.ReceivedDate > DateTime.MinValue ? stEval.ReceivedDate : stEval.SentDate,
                                    SendDate = stEval.SentDate,
                                    PhysicianSignatureDate = stEval.PhysicianSignatureDate
                                };
                            }
                            break;
                        case OrderType.StReEvaluation:
                            var stReEval = patientRepository.GetVisitNote(Current.AgencyId, patientId, id);
                            if (stReEval != null)
                            {
                                order = new Order
                                {
                                    Id = stReEval.Id,
                                    PatientId = stReEval.PatientId,
                                    Type = OrderType.StReEvaluation,
                                    Text = DisciplineTasks.STReEvaluation.GetDescription(),
                                    Number = stReEval.OrderNumber,
                                    PatientName = patient != null ? patient.DisplayName : string.Empty,
                                    ReceivedDate = stReEval.ReceivedDate > DateTime.MinValue ? stReEval.ReceivedDate : stReEval.SentDate,
                                    SendDate = stReEval.SentDate,
                                    PhysicianSignatureDate = stReEval.PhysicianSignatureDate
                                };
                            }
                            break;
                    }
                }
            }

            return order;
        }

        public List<Order> GetOrdersPendingSignature(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var schedules = patientRepository.GetPendingSignatureOrderScheduleEvents(Current.AgencyId, branchId, startDate, endDate);
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPendingPhysicianSignatureOrders(Current.AgencyId, physicianOrdersIds, startDate, endDate);
                    if (physicianOrders != null && physicianOrders.Count > 0)
                    {
                        physicianOrders.ForEach(po =>
                        {
                            var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = po.Id,
                                PatientId = po.PatientId,
                                Type = OrderType.PhysicianOrder,
                                Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                                Number = po.OrderNumber,
                                PatientName = po.DisplayName,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                SentDate = po.SentDate.ToShortDateString().ToZeroFilled(),
                                CreatedDate = po.OrderDateFormatted,
                                ReceivedDate = DateTime.Today,
                                PhysicianSignatureDate = po.PhysicianSignatureDate
                            });
                        });
                    }
                }

                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPendingSignaturePlanofCares(Current.AgencyId, planofCareOrdersIds);

                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        planofCareOrders.ForEach(poc =>
                        {
                            var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    Type = OrderType.HCFA485,
                                    Text = DisciplineTasks.HCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPendingSignaturePlanofCaresStandAlone(Current.AgencyId, planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                AgencyPhysician physician = null;
                                if (!poc.PhysicianId.IsEmpty())
                                {
                                    physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                                }
                                else
                                {
                                    if (poc.PhysicianData.IsNotNullOrEmpty())
                                    {
                                        var oldPhysician = poc.PhysicianData.ToObject<AgencyPhysician>();
                                        if (oldPhysician != null && !oldPhysician.Id.IsEmpty())
                                        {
                                            physician = PhysicianEngine.Get(oldPhysician.Id, Current.AgencyId);
                                        }
                                    }
                                }
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    PatientId = poc.PatientId,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SentDate = poc.SentDate.ToShortDateString().ToZeroFilled(),
                                    PhysicianSignatureDate = poc.PhysicianSignatureDate
                                });
                            }
                        });
                    }
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetPendingSignatureFaceToFaceEncounterOrders(Current.AgencyId, faceToFaceEncounterOrdersIds);

                    if (faceToFaceEncounters != null && faceToFaceEncounters.Count > 0)
                    {
                        faceToFaceEncounters.ForEach(ffe =>
                            {
                                var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = ffe.Id,
                                    PatientId = ffe.PatientId,
                                    Type = OrderType.FaceToFaceEncounter,
                                    Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                    Number = ffe.OrderNumber,
                                    PatientName = ffe.DisplayName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    SentDate = ffe.RequestDate.ToString("MM/dd/yyyy"),
                                    CreatedDate = ffe.RequestDate.ToString("MM/dd/yyyy"),
                                    ReceivedDate = DateTime.Today,
                                    PhysicianSignatureDate = ffe.SignatureDate

                                });
                            });
                    }
                }
                var evalDisciplineTasks = DisciplineTaskFactory.PhysicianEvalNotes();
                var evalSchedules = schedules.Where(s => evalDisciplineTasks.Exists(d => (int)d == s.DisciplineTask)).ToList();
                if (evalSchedules != null && evalSchedules.Count > 0)
                {
                    var evalIds = evalSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, new List<int>() { (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically }, evalIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        evalOrders.ForEach(eval =>
                        {
                            var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = eval.Id,
                                PatientId = eval.PatientId,
                                Type = GetOrderType(eval.NoteType),
                                Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                Number = eval.OrderNumber,
                                PatientName = eval.DisplayName,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                SentDate = eval.SentDate.ToShortDateString().ToZeroFilled(),
                                CreatedDate = eval.OrderDate.ToShortDateString().ToZeroFilled(),
                                ReceivedDate = DateTime.Today,
                                PhysicianSignatureDate = eval.PhysicianSignatureDate
                            });
                        });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public bool MarkOrdersAsSent(FormCollection formCollection)
        {
            var result = true;
            var sendElectronically = formCollection.Get("SendAutomatically").ToBoolean();
            formCollection.Remove("SendAutomatically");

            var status = (int)ScheduleStatus.OrderSentToPhysician;

            foreach (var key in formCollection.AllKeys)
            {
                string answers = formCollection.GetValues(key).Join(",");
                string[] answersArray = answers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (key.IsInteger() && key.ToInteger() == (int)OrderType.PhysicianOrder)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var orderId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var order = patientRepository.GetOrderOnly(orderId, Current.AgencyId);
                        if (order != null)
                        {
                            var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, order.PatientId, order.EpisodeId, order.Id);
                            if (scheduleEvent != null)
                            {
                                var oldStatus = scheduleEvent.Status;
                                scheduleEvent.Status = status;
                                if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    if (patientRepository.UpdateOrderStatus(Current.AgencyId, orderId, status, DateTime.MinValue, DateTime.Now))
                                    {
                                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId,  scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, DisciplineTasks.PhysicianOrder, string.Empty);
                                        }
                                       
                                    }
                                    else
                                    {
                                        result = false;
                                        scheduleEvent.Status = oldStatus;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        return;
                                    }
                                }
                            }
                        }

                    });
                }
               else if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var planofCare = planofCareRepository.Get(Current.AgencyId, pocId);
                        if (planofCare != null)
                        {

                            var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.EpisodeId, planofCare.Id);
                            if (scheduleEvent != null)
                            {
                                var oldStatus = scheduleEvent.Status;
                                scheduleEvent.Status = status;
                                if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    planofCare.Status = status;
                                    planofCare.SentDate = DateTime.Now;
                                    if (planofCareRepository.Update(planofCare))
                                    {
                                        if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        }
                                       
                                    }
                                    else
                                    {
                                        result = false;
                                        scheduleEvent.Status = oldStatus;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        return; 
                                    }
                                }
                            }

                        }
                    });
                }
               else if (key.IsInteger() && key.ToInteger() == (int)OrderType.HCFA485StandAlone)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var pocOrderId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var planofCare = planofCareRepository.GetStandAlone(Current.AgencyId, pocOrderId);
                        if (planofCare != null)
                        {

                            var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.EpisodeId, planofCare.Id);
                            if (scheduleEvent != null)
                            {
                                var oldStatus = scheduleEvent.Status;
                                scheduleEvent.Status = status;
                                if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    planofCare.Status = status;
                                    planofCare.SentDate = DateTime.Now;
                                    if (planofCareRepository.UpdateStandAlone(planofCare))
                                    {
                                        if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        }
                                    }
                                    else
                                    {
                                        result = false;
                                        scheduleEvent.Status = oldStatus;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        return;
                                    }
                                }
                            }

                        }
                    });
                }
               else if (key.IsInteger() && key.ToInteger() == (int)OrderType.FaceToFaceEncounter)
                {
                    status = (int)ScheduleStatus.OrderSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.OrderSentToPhysicianElectronically;
                    }

                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var facetofaceId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length > 1 ? answerArray[1].ToGuid() : Guid.Empty;
                        var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(facetofaceId, Current.AgencyId);
                        if (faceToFaceEncounter != null)
                        {

                            var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, faceToFaceEncounter.PatientId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.Id);
                            if (scheduleEvent != null)
                            {
                                var oldStatus = scheduleEvent.Status;
                                scheduleEvent.Status = status;
                                if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    if (patientRepository.UpdateFaceToFaceEncounterForRequest(Current.AgencyId, facetofaceId, status, DateTime.Now))
                                    {
                                        if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        } 
                                    }
                                    else
                                    {
                                        result = false;
                                        scheduleEvent.Status = oldStatus;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        return;
                                       
                                    }
                                }
                            }

                        }
                    });
                }
              else  if (key.IsInteger() && (key.ToInteger() == (int)OrderType.PtEvaluation || key.ToInteger() == (int)OrderType.PtReEvaluation
                    || key.ToInteger() == (int)OrderType.OtEvaluation || key.ToInteger() == (int)OrderType.OtReEvaluation
                    || key.ToInteger() == (int)OrderType.StEvaluation || key.ToInteger() == (int)OrderType.StReEvaluation))
                {
                    status = (int)ScheduleStatus.EvalSentToPhysician;
                    if (sendElectronically)
                    {
                        status = (int)ScheduleStatus.EvalSentToPhysicianElectronically;
                    }
                    answersArray.ForEach(item =>
                    {
                        string[] answerArray = item.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        var evalId = answerArray != null && answerArray.Length > 1 ? answerArray[0].ToGuid() : Guid.Empty;
                        var patientId = answerArray != null && answerArray.Length > 1 ? answerArray[1].ToGuid() : Guid.Empty;
                        var evalOrder = patientRepository.GetVisitNote(Current.AgencyId, patientId, evalId);
                        if (evalOrder != null)
                        {
                            var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, evalOrder.PatientId, evalOrder.EpisodeId, evalOrder.Id);
                            if (scheduleEvent != null)
                            {
                                var oldStatus = scheduleEvent.Status;
                                scheduleEvent.Status = status;
                                if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    evalOrder.Status = status;
                                    evalOrder.SentDate = DateTime.Now;
                                    if (patientRepository.UpdateVisitNote(evalOrder))
                                    {
                                        if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        }
                                    }
                                    else
                                    {
                                        result = false;
                                        scheduleEvent.Status = oldStatus;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        return;
                                    }
                                }
                            }

                        }
                    });
                }
            }

            return result;
        }

        public bool MarkOrderAsReturned(Guid Id, Guid patientId, OrderType type, DateTime receivedDate, DateTime physicianSignatureDate)
        {
            if (type == OrderType.PhysicianOrder) {
                var order = patientRepository.GetOrderOnly(Id, Current.AgencyId);
                if (order != null) {
                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, order.PatientId, order.EpisodeId, order.Id);
                    if (scheduleEvent != null) {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent)) {
                            order.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            order.ReceivedDate = receivedDate;
                            order.PhysicianSignatureDate = physicianSignatureDate;
                            if (patientRepository.UpdateOrderModel(order)) {
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status)) Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                return true;
                            } else {
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                }
            } else if (type == OrderType.HCFA485) {
                var planofCare = planofCareRepository.Get(Current.AgencyId, Id);
                if (planofCare != null) {
                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, planofCare.PatientId, planofCare.EpisodeId, planofCare.Id);
                    if (scheduleEvent != null) {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent)) {
                            var physician = planofCare.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null) planofCare.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            planofCare.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            planofCare.ReceivedDate = receivedDate;
                            planofCare.PhysicianSignatureDate = physicianSignatureDate;
                            if (planofCareRepository.Update(planofCare)) {
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status)) Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                return true;
                            } else {
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                }
            } else if (type == OrderType.HCFA485StandAlone) {
                var pocStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, Id);
                if (pocStandAlone != null) {
                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, pocStandAlone.PatientId, pocStandAlone.EpisodeId, pocStandAlone.Id);
                    if (scheduleEvent != null) {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent)) {
                            var physician = pocStandAlone.PhysicianData.ToObject<AgencyPhysician>();
                            if (physician != null) pocStandAlone.PhysicianSignatureText = string.Format("Signed by: {0}", physician.DisplayName);
                            pocStandAlone.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            pocStandAlone.ReceivedDate = receivedDate;
                            pocStandAlone.PhysicianSignatureDate = physicianSignatureDate;
                            if (planofCareRepository.UpdateStandAlone(pocStandAlone)) {
                                if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status)) Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                return true;
                            } else {
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                }
            } else if (type == OrderType.FaceToFaceEncounter) {
                var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(Id, Current.AgencyId);
                if (faceToFaceEncounter != null) {
                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, faceToFaceEncounter.PatientId, faceToFaceEncounter.EpisodeId, faceToFaceEncounter.Id);
                    if (scheduleEvent != null) {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent)) {
                            faceToFaceEncounter.Status = (int)ScheduleStatus.OrderReturnedWPhysicianSignature;
                            faceToFaceEncounter.ReceivedDate = receivedDate;
                            faceToFaceEncounter.SignatureDate = physicianSignatureDate;
                            if (patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter)) {
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status)) Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                return true;
                            } else {
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                }
            } else if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation || type == OrderType.StEvaluation || type == OrderType.StReEvaluation) {
                var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, Id);
                if (eval != null) {
                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, eval.PatientId, eval.EpisodeId, eval.Id);
                    if (scheduleEvent != null) {
                        var oldStatus = scheduleEvent.Status;
                        scheduleEvent.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent)) {
                            eval.Status = (int)ScheduleStatus.EvalReturnedWPhysicianSignature;
                            eval.ReceivedDate = receivedDate;
                            eval.PhysicianSignatureDate = physicianSignatureDate;
                            if (patientRepository.UpdateVisitNote(eval)) {
                                if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status)) Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                return true;
                            } else {
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool UpdateOrderDates(Guid Id, Guid patientId, OrderType type, DateTime receivedDate, DateTime sendDate, DateTime physicianSignatureDate)
        {
            bool result = false;
            if (type == OrderType.PhysicianOrder)
            {
                var order = patientRepository.GetOrderOnly(Id, Current.AgencyId);
                if (order != null)
                {
                    order.ReceivedDate = receivedDate;
                    order.SentDate = sendDate;
                    order.PhysicianSignatureDate = physicianSignatureDate;
                    result = patientRepository.UpdateOrderModel(order);
                }
            }
            else if (type == OrderType.HCFA485)
            {
                var planofCare = planofCareRepository.Get(Current.AgencyId, Id);
                if (planofCare != null)
                {
                    planofCare.ReceivedDate = receivedDate;
                    planofCare.SentDate = sendDate;
                    planofCare.PhysicianSignatureDate = physicianSignatureDate;
                    result = planofCareRepository.Update(planofCare);
                }
            }
            else if (type == OrderType.HCFA485StandAlone)
            {
                var pocStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, Id);
                if (pocStandAlone != null)
                {
                    pocStandAlone.ReceivedDate = receivedDate;
                    pocStandAlone.SentDate = sendDate;
                    pocStandAlone.PhysicianSignatureDate = physicianSignatureDate;
                    result = planofCareRepository.UpdateStandAlone(pocStandAlone);
                }
            }
            else if (type == OrderType.FaceToFaceEncounter)
            {
                var faceToFaceEncounter = patientRepository.GetFaceToFaceEncounter(Id, Current.AgencyId);
                if (faceToFaceEncounter != null)
                {
                    faceToFaceEncounter.ReceivedDate = receivedDate;
                    faceToFaceEncounter.RequestDate = sendDate;
                    faceToFaceEncounter.SignatureDate = physicianSignatureDate;
                    result = patientRepository.UpdateFaceToFaceEncounter(faceToFaceEncounter);
                }
            }
            else if (type == OrderType.PtEvaluation || type == OrderType.PtReEvaluation
                || type == OrderType.OtEvaluation || type == OrderType.OtReEvaluation
                || type == OrderType.StEvaluation || type == OrderType.StReEvaluation)
            {
                var eval = patientRepository.GetVisitNote(Current.AgencyId, patientId, Id);
                if (eval != null)
                {
                    eval.ReceivedDate = receivedDate;
                    eval.SentDate = sendDate;
                    eval.PhysicianSignatureDate = physicianSignatureDate;
                    result = patientRepository.UpdateVisitNote(eval);
                }
            }
            return result;
        }

        public List<AddressViewData> GetAgencyFullAddress()
        {
            var listOfAddress = agencyRepository.GetBranches(Current.AgencyId);
            var address = new List<AddressViewData>();
            if (listOfAddress != null && listOfAddress.Count > 0)
            {
                listOfAddress.ForEach(l =>
                {
                    address.Add(new AddressViewData { Name = l.Name, FullAddress = string.Format("{0} {1}, {2}, {3} {4}", l.AddressLine1, l.AddressLine2, l.AddressCity, l.AddressStateCode, l.AddressZipCode) });
                });
            }
            return address;
        }

        public List<Infection> GetInfections(Guid agencyId)
        {
            var infections = agencyRepository.GetInfections(Current.AgencyId).ToList();
            if (infections != null && infections.Count > 0)
            {
                infections.ForEach(i =>
                {
                    var patient = patientRepository.GetPatientOnly(i.PatientId, i.AgencyId);
                    if (patient != null)
                    {
                        i.PatientName = patient.DisplayName;
                    }
                    var physician = physicianRepository.Get(i.PhysicianId, i.AgencyId);
                    if (physician != null)
                    {
                        i.PhysicianName = physician.DisplayName;
                    }
                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, i.PatientId, i.EpisodeId, i.Id);
                    if (scheduleEvent != null)
                    {
                        i.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return infections;
        }

        public List<Incident> GetIncidents(Guid agencyId)
        {
            var incidents = agencyRepository.GetIncidents(Current.AgencyId).ToList();
            if (incidents != null && incidents.Count > 0)
            {
                incidents.ForEach(i =>
                {
                    var patient = patientRepository.GetPatientOnly(i.PatientId, i.AgencyId);
                    if (patient != null)
                    {
                        i.PatientName = patient.DisplayName;
                    }
                    var physician = physicianRepository.Get(i.PhysicianId, i.AgencyId);
                    if (physician != null)
                    {
                        i.PhysicianName = physician.DisplayName;
                    }

                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, i.PatientId, i.EpisodeId, i.Id);
                    if (scheduleEvent != null)
                    {
                        i.PrintUrl = Url.Print(scheduleEvent, true);
                    }
                });
            }
            return incidents;
        }

        public bool ProcessInfections(string button, Guid patientId, Guid eventId, string reason)
        {
            var result = false;
            bool isNoteUpdates = true;
            bool isActionSet = false;

            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
                if (infection != null && !infection.EpisodeId.IsEmpty())
                {
                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, infection.EpisodeId, eventId);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        var oldReason = scheduleEvent.ReturnReason;
                        var oldPrintQueue = scheduleEvent.InPrintQueue;
                        var description = string.Empty;
                        if (button == ButtonAction.Approve.ToString())
                        {
                            infection.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                            scheduleEvent.InPrintQueue = true;
                            scheduleEvent.Status = infection.Status;
                            scheduleEvent.ReturnReason = string.Empty;
                            isActionSet = true;
                            description = "Approved By:" + Current.UserFullName;

                        }
                        else if (button == ButtonAction.Return.ToString())
                        {
                            infection.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                            infection.SignatureText = string.Empty;
                            infection.SignatureDate = DateTime.MinValue;
                            scheduleEvent.Status = infection.Status;
                            scheduleEvent.ReturnReason = reason;
                            isActionSet = true;
                            description = "Returned By:" + Current.UserFullName;

                        }
                        else if (button == ButtonAction.Print.ToString())
                        {
                            scheduleEvent.InPrintQueue = false;
                            isNoteUpdates = false;
                            isActionSet = true;
                        }

                        if (isActionSet)
                        {
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (isNoteUpdates)
                                {
                                    infection.Modified = DateTime.Now;
                                    if (agencyRepository.UpdateInfectionModal(infection))
                                    {
                                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                        }
                                        result = true;
                                    }
                                    else
                                    {
                                        scheduleEvent.Status = oldStatus;
                                        scheduleEvent.ReturnReason = oldReason;
                                        scheduleEvent.InPrintQueue = oldPrintQueue;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        result = false;
                                    }

                                }
                                else
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool ProcessIncidents(string button, Guid patientId, Guid eventId, string reason)
        {
            var result = false;
            bool isNoteUpdates = true;
            bool isActionSet = false;
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
                if (incident != null && !incident.EpisodeId.IsEmpty())
                {
                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, incident.EpisodeId, eventId);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        var oldReason = scheduleEvent.ReturnReason;
                        var oldPrintQueue = scheduleEvent.InPrintQueue;
                        var description = string.Empty;
                        var action = new Actions();
                        if (button == ButtonAction.Approve.ToString())
                        {
                            incident.Status = ((int)ScheduleStatus.ReportAndNotesCompleted);
                            scheduleEvent.InPrintQueue = true;
                            scheduleEvent.Status = incident.Status;
                            scheduleEvent.ReturnReason = string.Empty;
                            isActionSet = true;
                            description = "Approved by " + Current.UserFullName;
                            action = Actions.Approved;
                        }
                        else if (button == ButtonAction.Return.ToString())
                        {
                            incident.Status = ((int)ScheduleStatus.ReportAndNotesReturned);
                            incident.SignatureText = string.Empty;
                            incident.SignatureDate = DateTime.MinValue;
                            scheduleEvent.Status = incident.Status;
                            scheduleEvent.ReturnReason = reason;
                            isActionSet = true;
                            description = "Returned by " + Current.UserFullName;
                            action = Actions.Returned;

                        }
                        else if (button == ButtonAction.Print.ToString())
                        {
                            scheduleEvent.InPrintQueue = false;
                            isNoteUpdates = false;
                            isActionSet = true;
                        }

                        if (isActionSet)
                        {
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (isNoteUpdates)
                                {
                                    incident.Modified = DateTime.Now;
                                    if (agencyRepository.UpdateIncidentModal(incident))
                                    {
                                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                        }
                                        result = true;
                                    }
                                    else
                                    {
                                        scheduleEvent.Status = oldStatus;
                                        scheduleEvent.ReturnReason = oldReason;
                                        scheduleEvent.InPrintQueue = oldPrintQueue;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        result = false;
                                    }
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public List<SelectListItem> Insurances(int value, bool IsAll, bool IsMedicareTradIncluded)
        {
            var items = new List<SelectListItem>();
            if (IsMedicareTradIncluded)
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var payorId = 0;
                    if (int.TryParse(agency.Payor, out payorId))
                    {
                        var standardInsurance = lookupRepository.GetInsurance(payorId);
                        if (standardInsurance != null)
                        {
                            items.Add(new SelectListItem
                            {
                                Text = standardInsurance.Name,
                                Value = standardInsurance.Id.ToString(),
                                Selected = (standardInsurance.Id==(value))
                            });
                        }
                    }
                }
            }
            if (IsAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "All",
                    Value = "0"
                });
            }
            var agencyInsurances = agencyRepository.GetInsurances(Current.AgencyId);
            agencyInsurances.ForEach(i =>
            {
                items.Add(new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = (i.Id==(value))
                });
            });
            return items;
        }

        public List<SelectListItem> Branchs(string value, bool IsAll)
        {
            List<SelectListItem> items = null;
            IEnumerable<SelectListItem> tempItems = null;
            var branches = agencyRepository.GetBranches(Current.AgencyId);
            tempItems = from branch in branches
                        select new SelectListItem
                        {
                            Text = branch.Name,
                            Value = branch.Id.ToString(),
                            Selected = (branch.Id.ToString().IsEqual(value))
                        };
            items = tempItems.ToList();
            if (IsAll)
            {
                items.Insert(0, new SelectListItem
                {
                    Text = "-- All Branches --",
                    Value = Guid.Empty.ToString(),
                });
            }
            return items;
        }

        public Infection GetInfectionReportPrint()
        {
            var infection = new Infection();
            infection.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return infection;
        }

        public Incident GetIncidentReportPrint()
        {
            var incident = new Incident();
            incident.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return incident;
        }

        public Infection GetInfectionReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
            if (infection != null)
            {
                infection.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                infection.Patient = patientRepository.GetPatientOnly(infection.PatientId, Current.AgencyId);

                if (!infection.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, infection.EpisodeId, infection.PatientId);
                    if (episode != null)
                    {
                        infection.EpisodeEndDate = episode.EndDateFormatted;
                        infection.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
                if (!infection.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(infection.PhysicianId, Current.AgencyId);
                    if (physician != null) infection.PhysicianName = physician.DisplayName;
                }
            }
            return infection;
        }

        public Incident GetIncidentReportPrint(Guid episodeId, Guid patientId, Guid eventId)
        {
            var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
            if (incident != null)
            {
                incident.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                incident.Patient = patientRepository.GetPatientOnly(incident.PatientId, incident.AgencyId);
                if (!incident.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, incident.EpisodeId, incident.PatientId);
                    if (episode != null)
                    {
                        incident.EpisodeEndDate = episode.EndDateFormatted;
                        incident.EpisodeStartDate = episode.StartDateFormatted;
                    }
                }
                if (!incident.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(incident.PhysicianId, Current.AgencyId);
                    if (physician != null) incident.PhysicianName = physician.DisplayName;
                }
            }
            return incident;
        }

        public List<PatientEpisodeEvent> GetPrintQueue()
        {
            var events = new List<PatientEpisodeEvent>();
            var schedules = patientRepository.GetPrintQueueEvents(Current.AgencyId);
            if (schedules != null && schedules.Count > 0)
            {
                var users = new List<User>();
                var userIds = schedules.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                if (userIds != null && userIds.Count > 0)
                {
                    users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds) ?? new List<User>();
                }
                schedules.ForEach(e =>
                {
                    var user = users.FirstOrDefault(u => u.Id == e.UserId);
                    events.Add(new PatientEpisodeEvent
                    {
                        Status = e.StatusName,
                        PatientName = e.PatientName,
                        TaskName = e.DisciplineTaskName,
                        PrintUrl = Url.Download(e, false),
                        UserName = user != null ? user.DisplayName : string.Empty,
                        EventDate = e.EventDate.IsValid() ? e.EventDate.ToString("MM/dd/yyyy") : "",
                        CustomValue = string.Format("{0}|{1}|{2}|{3}", e.EpisodeId, e.PatientId, e.EventId, e.DisciplineTask)
                    });
                });
            }
            return events.OrderByDescending(e => e.EventDate.ToOrderedDate()).ToList();
        }
    }
}
