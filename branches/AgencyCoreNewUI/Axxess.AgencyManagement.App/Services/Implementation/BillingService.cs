﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using System.Web.Mvc;
    using System.Web.Script.Serialization;

    using System.Xml.Linq;
    using System.Net;
    using System.IO;
    using System.Text.RegularExpressions;

    using iTextSharp.text;

    using ViewData;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;

    using Axxess.Membership.Logging;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.Log.Enums;
    using Axxess.Log.Common;

  
    using Axxess.AgencyManagement.App.Enums;
   

    public class BillingService : IBillingService
    {
        #region Private Members

        private readonly IUserRepository userRepository;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IReferralRepository referrralRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly ILookupRepository lookUpRepository;

        #endregion

        #region Constructor

        public BillingService(IAgencyManagementDataProvider dataProvider, IOasisCDataProvider oasisCDataProvider, ILookUpDataProvider lookUpDataProvider, IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");

            this.assessmentService = assessmentService;
            this.patientRepository = dataProvider.PatientRepository;
            this.userRepository = dataProvider.UserRepository;
            this.referrralRepository = dataProvider.ReferralRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.assessmentRepository = oasisCDataProvider.OasisAssessmentRepository;
            this.lookUpRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Public Methods

        public bool AddRap(Rap rap)
        {
            var result = false;
            if (billingRepository.AddRap(rap))
            {
                Auditor.AddGeneralLog(Current.AgencyId,Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public bool AddFinal(Final final)
        {
            var result = false;
            if (billingRepository.AddFinal(final))
            {
                Auditor.AddGeneralLog(Current.AgencyId,Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public string GenerateJsonRAP(List<Guid> rapToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, out List<Rap> raps, bool isHMO, AgencyLocation branch)
        {
            string requestArr = string.Empty;
            claimInfo = new List<ClaimInfo>();
            raps = null;
            try
            {
                var rapClaim = billingRepository.GetRapsToGenerateByIds(Current.AgencyId, rapToGenerate);
                raps = rapClaim;
                #region
                if (branch != null)
                {
                    var patients = new List<object>();
                    if (rapClaim != null && rapClaim.Count > 0)
                    {
                        var zipCodes = rapClaim.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).Distinct().ToArray();
                        var cbsaCodes = lookUpRepository.CbsaCodesByZip(zipCodes) ?? new List<CBSACode>();
                        int rapCount = rapClaim.Count;
                        foreach (var rap in rapClaim)
                        {
                            var diagnosis = rap.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(rap.DiagnosisCode) : null;
                            var conditionCodes = rap.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(rap.ConditionCodes) : null;
                            var claims = new List<object>();
                            claimInfo.Add(new ClaimInfo { ClaimId = rap.Id, PatientId = rap.PatientId, EpisodeId = rap.EpisodeId, ClaimType = rap.Type == 0 ? "322" : "" });
                            var locator81Lists = rap.Ub04Locator81cca.IsNotNullOrEmpty() ? rap.Ub04Locator81cca.ToObject<List<Locator>>() : new List<Locator>();
                            var locator81 = new List<object>();
                            if (locator81Lists != null && locator81Lists.Count > 0)
                            {
                                locator81Lists.ForEach(l =>
                                {
                                    locator81.Add(new { code1 = l.Code1, code2 = l.Code2, code3 = l.Code3 });
                                });
                            }
                            var rapObj = new
                            {
                                claim_id = rap.Id,
                                claim_type = rap.Type == 1 ? "328" : "322",
                                claim_physician_upin = rap.PhysicianNPI,
                                claim_physician_last_name = rap.PhysicianLastName,
                                claim_physician_first_name = rap.PhysicianFirstName,
                                claim_first_visit_date = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
                                claim_episode_start_date = rap.EpisodeStartDate.ToString("MM/dd/yyyy"),
                                claim_episode_end_date = rap.EpisodeStartDate.ToString("MM/dd/yyyy"),
                                claim_hipps_code = rap.HippsCode,
                                claim_oasis_key = rap.ClaimKey,
                                hmo_plan_id = rap.HealthPlanId,
                                claim_group_name = rap.GroupName,
                                claim_group_Id = rap.GroupId,
                                claim_hmo_auth_key = rap.AuthorizationNumber,
                                claim_hmo_auth_key2 = rap.AuthorizationNumber2,
                                claim_hmo_auth_key3 = rap.AuthorizationNumber3,
                                claim_diagnosis_code1 = (diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : ""),
                                claim_diagnosis_code2 = (diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : ""),
                                claim_diagnosis_code3 = (diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : ""),
                                claim_diagnosis_code4 = (diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : ""),
                                claim_diagnosis_code5 = (diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : ""),
                                claim_diagnosis_code6 = (diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : ""),
                                claim_condition_code18 = (conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : ""),
                                claim_condition_code19 = (conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : ""),
                                claim_condition_code20 = (conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : ""),
                                claim_condition_code21 = (conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : ""),
                                claim_condition_code22 = (conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : ""),
                                claim_condition_code23 = (conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : ""),
                                claim_condition_code24 = (conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : ""),
                                claim_condition_code25 = (conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : ""),
                                claim_condition_code26 = (conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : ""),
                                claim_condition_code27 = (conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : ""),
                                claim_condition_code28 = (conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : ""),
                                claim_total_charge_amount = "0",
                                claim_admission_source_code = rap.AdmissionSource.IsInteger() ? rap.AdmissionSource.ToInteger().GetSplitValue() : "9",
                                claim_patient_status_code = rap.UB4PatientStatus,
                                claim_ub04locator81 = locator81
                            };
                            claims.Add(rapObj);
                            var cbsa = cbsaCodes.SingleOrDefault(c => c.Zip.IsEqual(rap.AddressZipCode));
                            var patient = new
                            {
                                patient_gender = rap.Gender.Substring(0, 1),
                                patient_medicare_num = rap.MedicareNumber,
                                patient_record_num = rap.PatientIdNumber,
                                patient_dob = rap.DOB.ToString("MM/dd/yyyy"),
                                patient_doa = rap.StartofCareDate.ToString("MM/dd/yyyy"),
                                patient_dod = rap.IsRapDischage() && rap.DischargeDate.Date > DateTime.MinValue.Date ? rap.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
                                patient_address = rap.AddressLine1,
                                patient_address2 = rap.AddressLine2,
                                patient_city = rap.AddressCity,
                                patient_state = rap.AddressStateCode,
                                patient_zip = rap.AddressZipCode,
                                patient_cbsa = cbsa!=null ? cbsa.CBSA:string.Empty,
                                patient_last_name = rap.LastName,
                                patient_first_name = rap.FirstName,
                                patient_middle_initial = "",
                                claims_arr = claims
                            };
                            patients.Add(patient);
                        }
                    }

                    if (isHMO)
                    {
                        var agencyClaim = new
                        {
                            format = "ansi837",
                            submit_type = commandType.ToString(),
                            user_login_name = Current.User.Name,
                            hmo_payer_id = payerInfo.HMOPayerId,
                            hmo_payer_name = payerInfo.HMOPayerName,
                            hmo_submitter_id = payerInfo.HMOSubmitterId,
                            hmo_provider_id = payerInfo.ProviderId,
                            hmo_other_provider_id = payerInfo.OtherProviderId,
                            hmo_provider_subscriber_id = payerInfo.ProviderSubscriberId,
                            payer_id = payerInfo.Code,
                            payer_name = payerInfo.Name,
                            submitter_id = payerInfo.SubmitterId,
                            insurance_is_axxess_biller = payerInfo.IsAxxessTheBiller,
                            clearing_house_id = payerInfo.ClearingHouseSubmitterId,
                            provider_claim_type = payerInfo.BillType,
                            interchange_receiver_id = payerInfo.InterchangeReceiverId,
                            clearing_house = payerInfo.ClearingHouse,
                            claim_billtype = ClaimType.HMO.ToString(),
                            submitter_name = payerInfo.SubmitterName,
                            submitter_phone = payerInfo.Phone,
                            submitter_fax = payerInfo.Fax,
                            user_agency_name = branch.Name,
                            user_tax_id = branch.TaxId,
                            user_national_provider_id = branch.NationalProviderNumber,
                            user_address_1 = branch.AddressLine1,
                            user_address_2 = branch.AddressLine2,
                            user_city = branch.AddressCity,
                            user_state = branch.AddressStateCode,
                            user_zip = branch.AddressZipCode + "-" + branch.AddressZipCodeFour,
                            user_phone = branch.PhoneWork,
                            user_fax = branch.FaxNumber,
                            user_CBSA_code = lookUpRepository.CbsaCodeByZip(branch.AddressZipCode),
                            ansi_837_id = claimId,
                            patients_arr = patients
                        };
                        var jss = new JavaScriptSerializer();
                        requestArr = jss.Serialize(agencyClaim);
                    }
                    else
                    {
                        var agencyClaim = new
                        {
                            format = "ansi837",
                            submit_type = commandType.ToString(),
                            user_login_name = Current.User.Name,
                            payer_id = payerInfo.Code,
                            payer_name = payerInfo.Name,
                            submitter_id = payerInfo.SubmitterId,
                            claim_billtype = ClaimType.CMS.ToString(),
                            submitter_name = payerInfo.SubmitterName,
                            submitter_phone = payerInfo.Phone,
                            submitter_fax = payerInfo.Fax,
                            user_agency_name = branch.Name,
                            user_tax_id = branch.TaxId,
                            user_national_provider_id = branch.NationalProviderNumber,
                            user_address_1 = branch.AddressLine1,
                            user_address_2 = branch.AddressLine2,
                            user_city = branch.AddressCity,
                            user_state = branch.AddressStateCode,
                            user_zip = branch.AddressZipCode + "-" + branch.AddressZipCodeFour,
                            user_phone = branch.PhoneWork,
                            user_fax = branch.FaxNumber,
                            user_CBSA_code = lookUpRepository.CbsaCodeByZip(branch.AddressZipCode),
                            ansi_837_id = claimId,
                            patients_arr = patients
                        };
                        var jss = new JavaScriptSerializer();
                        requestArr = jss.Serialize(agencyClaim);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return requestArr;
        }

        public string GenerateJsonFinal(List<Guid> finalToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, out List<Final> finals, bool isHMO, AgencyLocation branch)
        {
            string requestArr = string.Empty;
            claimInfo = new List<ClaimInfo>();
            finals = null;
            try
            {
                var finalClaim = billingRepository.GetFinalsToGenerateByIds(Current.AgencyId, finalToGenerate);
                finals = finalClaim;
                #region
                if (branch != null)
                {
                    var patients = new List<object>();
                    if (finalClaim != null && finalClaim.Count > 0)
                    {
                        var zipCodes = finalClaim.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).Distinct().ToArray();
                        var cbsaCodes = lookUpRepository.CbsaCodesByZip(zipCodes)?? new List<CBSACode>();
                        foreach (var final in finalClaim)
                        {
                            var claims = new List<object>();
                            var diagnosis = final.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(final.DiagnosisCode) : null;
                            var conditionCodes = final.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(final.ConditionCodes) : null;
                            var visitTotalAmount = 0.00;
                            var visits = final.VerifiedVisits.IsNotNullOrEmpty() ? final.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(f => f.VisitDate.IsValid()).OrderBy(f => f.VisitDate.Date).ToList() : new List<ScheduleEvent>();
                            var visitList = new List<object>();
                            if (visits.Count > 0)
                            {
                                var agencyInsurance = new AgencyInsurance();
                                var chargeRates = this.FinalToCharegRates(final, out agencyInsurance) ?? new Dictionary<int, ChargeRate>();
                                visits.ForEach(v =>
                                {
                                    var rate = chargeRates.ContainsKey(v.DisciplineTask) ? chargeRates[v.DisciplineTask] : new ChargeRate();
                                    var amount = 0.0;
                                    var addedVisits = this.BillableVisitForANSI(rate, v, out amount, isHMO ? ClaimType.HMO : ClaimType.CMS, false);
                                    if (addedVisits != null && addedVisits.Count > 0)
                                    {
                                        visitList.AddRange(addedVisits);
                                        visitTotalAmount += amount;
                                    }
                                });
                            }
                            var supplyList = new List<object>();
                            if (!final.IsSupplyNotBillable)
                            {
                                final.SupplyTotal = this.MedicareSupplyTotal(final);
                                supplyList = new List<object> { new { date = final.FirstBillableVisitDate.ToString("MM/dd/yyyy"), revenue = "0272", amount = final.SupplyTotal, units = 1 } };
                            }
                            else
                            {
                                final.SupplyTotal = 0;
                            }
                            var locator81Lists = final.Ub04Locator81cca.IsNotNullOrEmpty() ? final.Ub04Locator81cca.ToObject<List<Locator>>() : new List<Locator>();
                            var locator81 = new List<object>();
                            if (locator81Lists != null && locator81Lists.Count > 0)
                            {
                                locator81Lists.ForEach(l =>
                                {
                                    locator81.Add(new { code1 = l.Code1, code2 = l.Code2, code3 = l.Code3 });
                                });
                            }
                            claimInfo.Add(new ClaimInfo { ClaimId = final.Id, PatientId = final.PatientId, EpisodeId = final.EpisodeId, ClaimType = final.Type == 0 ? "329" : "" });
                            var finalObj = new
                            {
                                claim_id = final.Id,
                                claim_type = final.Type == 1 ? "328" : "329",
                                claim_physician_upin = final.PhysicianNPI,
                                claim_physician_last_name = final.PhysicianLastName,
                                claim_physician_first_name = final.PhysicianFirstName,
                                claim_first_visit_date = final.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
                                claim_episode_start_date = final.EpisodeStartDate.ToString("MM/dd/yyyy"),
                                claim_episode_end_date = final.EpisodeEndDate.ToString("MM/dd/yyyy"),
                                claim_hipps_code = final.HippsCode,
                                claim_oasis_key = final.ClaimKey,
                                hmo_plan_id = final.HealthPlanId,
                                claim_group_name = final.GroupName,
                                claim_group_Id = final.GroupId,
                                claim_hmo_auth_key = final.AuthorizationNumber,
                                claim_hmo_auth_key2 = final.AuthorizationNumber2,
                                claim_hmo_auth_key3 = final.AuthorizationNumber3,
                                claim_diagnosis_code1 = (diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : ""),
                                claim_diagnosis_code2 = (diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : ""),
                                claim_diagnosis_code3 = (diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : ""),
                                claim_diagnosis_code4 = (diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : ""),
                                claim_diagnosis_code5 = (diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : ""),
                                claim_diagnosis_code6 = (diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : ""),
                                claim_condition_code18 = (conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : ""),
                                claim_condition_code19 = (conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : ""),
                                claim_condition_code20 = (conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : ""),
                                claim_condition_code21 = (conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : ""),
                                claim_condition_code22 = (conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : ""),
                                claim_condition_code23 = (conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : ""),
                                claim_condition_code24 = (conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : ""),
                                claim_condition_code25 = (conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : ""),
                                claim_condition_code26 = (conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : ""),
                                claim_condition_code27 = (conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : ""),
                                claim_condition_code28 = (conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : ""),
                                claim_admission_source_code = final.AdmissionSource.IsNotNullOrEmpty() && final.AdmissionSource.IsInteger() ? final.AdmissionSource.ToInteger().GetSplitValue() : "9",
                                claim_patient_status_code = final.UB4PatientStatus,
                                claim_dob = final.IsFinalDischage() ? final.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
                                claim_ub04locator81 = locator81,
                                claim_supply_isBillable = !final.IsSupplyNotBillable,
                                claim_supply_value = Math.Round(final.SupplyTotal, 2),
                                claim_supplies = supplyList,
                                claim_total_charge_amount = Math.Round(visitTotalAmount, 2),
                                claim_visits = visitList
                            };
                            claims.Add(finalObj);
                            var cbsa = cbsaCodes.SingleOrDefault(c => c.Zip.IsEqual(final.AddressZipCode));
                            var patient = new
                            {
                                patient_gender = final.Gender.Substring(0, 1),
                                patient_medicare_num = final.MedicareNumber,
                                patient_record_num = final.PatientIdNumber,
                                patient_dob = final.DOB.ToString("MM/dd/yyyy"),
                                patient_doa = final.StartofCareDate.ToString("MM/dd/yyyy"),
                                patient_dod = final.IsFinalDischage() && final.DischargeDate.Date > DateTime.MinValue.Date ? final.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
                                patient_address = final.AddressLine1,
                                patient_address2 = final.AddressLine2,
                                patient_city = final.AddressCity,
                                patient_state = final.AddressStateCode,
                                patient_zip = final.AddressZipCode,
                                patient_cbsa = cbsa!=null ? cbsa.CBSA: string.Empty,
                                patient_last_name = final.LastName,
                                patient_first_name = final.FirstName,
                                patient_middle_initial = "",
                                claims_arr = claims
                            };
                            patients.Add(patient);
                        }
                    }
                    if (isHMO)
                    {
                        var agencyClaim = new
                        {
                            format = "ansi837",
                            submit_type = commandType.ToString(),
                            user_login_name = Current.User.Name,
                            hmo_payer_id = payerInfo.HMOPayerId,
                            hmo_payer_name = payerInfo.HMOPayerName,
                            hmo_submitter_id = payerInfo.HMOSubmitterId,
                            hmo_provider_id = payerInfo.ProviderId,
                            hmo_other_provider_id = payerInfo.OtherProviderId,
                            hmo_provider_subscriber_id = payerInfo.ProviderSubscriberId,
                            payer_id = payerInfo.Code,
                            payer_name = payerInfo.Name,
                            submitter_id = payerInfo.SubmitterId,
                            insurance_is_axxess_biller = payerInfo.IsAxxessTheBiller,
                            clearing_house_id = payerInfo.ClearingHouseSubmitterId,
                            provider_claim_type = payerInfo.BillType,
                            interchange_receiver_id = payerInfo.InterchangeReceiverId,
                            clearing_house = payerInfo.ClearingHouse,
                            claim_billtype = ClaimType.HMO.ToString(),
                            submitter_name = payerInfo.SubmitterName,
                            submitter_phone = payerInfo.Phone,
                            submitter_fax = payerInfo.Fax,
                            user_agency_name = branch.Name,
                            user_tax_id = branch.TaxId,
                            user_national_provider_id = branch.NationalProviderNumber,
                            user_address_1 = branch.AddressLine1,
                            user_address_2 = branch.AddressLine2,
                            user_city = branch.AddressCity,
                            user_state = branch.AddressStateCode,
                            user_zip = branch.AddressZipCode + "-" + branch.AddressZipCodeFour,
                            user_phone = branch.PhoneWork,
                            user_fax = branch.FaxNumber,
                            user_CBSA_code = lookUpRepository.CbsaCodeByZip(branch.AddressZipCode),
                            ansi_837_id = claimId,
                            patients_arr = patients
                        };
                        var jss = new JavaScriptSerializer();
                        requestArr = jss.Serialize(agencyClaim);
                    }
                    else
                    {
                        var agencyClaim = new
                        {
                            format = "ansi837",
                            submit_type = commandType.ToString(),
                            user_login_name = Current.User.Name,
                            payer_id = payerInfo.Code,
                            payer_name = payerInfo.Name,
                            submitter_id = payerInfo.SubmitterId,
                            claim_billtype = ClaimType.CMS.ToString(),
                            submitter_name = payerInfo.SubmitterName,
                            submitter_phone = payerInfo.Phone,
                            submitter_fax = payerInfo.Fax,
                            user_agency_name = branch.Name,
                            user_tax_id = branch.TaxId,
                            user_national_provider_id = branch.NationalProviderNumber,
                            user_address_1 = branch.AddressLine1,
                            user_address_2 = branch.AddressLine2,
                            user_city = branch.AddressCity,
                            user_state = branch.AddressStateCode,
                            user_zip = branch.AddressZipCode + "-" + branch.AddressZipCodeFour,
                            user_phone = branch.PhoneWork,
                            user_fax = branch.FaxNumber,
                            user_CBSA_code = lookUpRepository.CbsaCodeByZip(branch.AddressZipCode),
                            ansi_837_id = claimId,
                            patients_arr = patients
                        };
                        var jss = new JavaScriptSerializer();
                        requestArr = jss.Serialize(agencyClaim);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return requestArr;
        }

        public bool GenerateDirect(List<Guid> ClaimSelected, string Type, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId)
        {
            var claimData = new ClaimData { AgencyId = Current.AgencyId };
            var claimInfo = new List<ClaimInfo>();
            claimDataOut = null;
            billExchange = new BillExchange { isSuccessful = false, Message = "There is a problem creating the claim. Try Again." };
            if (Type.IsNotNullOrEmpty() && (Type.IsEqual("rap") || Type.IsEqual("final")))
            {
                var agency = agencyRepository.Get(Current.AgencyId);
                if (agency != null)
                {
                    var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, branchId);
                    if (agencyLocation != null)
                    {
                        if (!agencyLocation.IsLocationStandAlone)
                        {
                            agencyLocation.Payor = agency.Payor;
                            agencyLocation.SubmitterId = agency.SubmitterId;
                            agencyLocation.SubmitterName = agency.SubmitterName;
                            agencyLocation.SubmitterPhone = agency.SubmitterPhone;
                            agencyLocation.SubmitterFax = agency.SubmitterFax;
                            agencyLocation.Name = agency.Name;
                            agencyLocation.TaxId = agency.TaxId;
                            agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;

                        }
                        int payorId;
                        if (int.TryParse(agencyLocation.Payor, out payorId))
                        {
                            var payerInfo = agencyRepository.SubmitterInfo(payorId);
                            if (payerInfo != null)
                            {
                                if (commandType == ClaimCommandType.download)
                                {
                                    payerInfo.SubmitterId = agencyLocation.SubmitterId;
                                    payerInfo.SubmitterName = agencyLocation.SubmitterName;
                                    payerInfo.Phone = agencyLocation.SubmitterPhone;
                                    payerInfo.Fax = agencyLocation.SubmitterFax;
                                }
                                var isHMO = false;
                                if (insuranceId >= 1000)
                                {
                                    var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
                                    if (insurance != null && insurance.PayorType == 2)
                                    {
                                        isHMO = true;
                                        var checkHmoPayorId = false;
                                        var message = string.Empty;
                                        if (insurance.PayorId.IsNotNullOrEmpty() && !(insurance.PayorId == "0"))
                                        {
                                            payerInfo.HMOPayerId = insurance.PayorId;
                                            checkHmoPayorId = true;
                                        }
                                        else
                                        {
                                            checkHmoPayorId = false;
                                            message = "Payor Id information is not correct.";
                                        }
                                        payerInfo.HMOPayerName = insurance.Name;
                                        var checkHmoSubmitterId = false;
                                        if (insurance.SubmitterId.IsNotNullOrEmpty())
                                        {
                                            payerInfo.HMOSubmitterId = insurance.SubmitterId;
                                            checkHmoSubmitterId = true;
                                        }
                                        else
                                        {
                                            checkHmoSubmitterId = false;
                                            if (message.IsNullOrEmpty()) { message = "The submitter Id is not correct."; } else { message = message + "," + "The submitter Id is not correct."; }
                                        }
                                        if (!(checkHmoPayorId && checkHmoSubmitterId))
                                        {
                                            billExchange.Message = message;
                                            return false;
                                        }
                                        payerInfo.Ub04Locator81cca = insurance.Ub04Locator81cca;
                                        payerInfo.IsAxxessTheBiller = insurance.IsAxxessTheBiller;
                                        payerInfo.InterchangeReceiverId = insurance.InterchangeReceiverId;
                                        payerInfo.ClearingHouse = insurance.ClearingHouse;
                                        payerInfo.ClearingHouseSubmitterId = insurance.ClearingHouseSubmitterId;
                                        payerInfo.BillType = insurance.BillType;
                                    }
                                    else
                                    {
                                        billExchange.Message = "Medicare HMO information is not right.";
                                        return false;
                                    }
                                }
                                else if (insuranceId < 1000 && insuranceId > 0)
                                {
                                    isHMO = false;
                                }
                                else
                                {
                                    billExchange.Message = "Payor information is not right.";
                                    return false;
                                }
                                var claimId = GetNextClaimId(claimData);
                                claimData.Id = claimId;
                               
                                var requestArr = string.Empty;
                                if (Type.IsEqual("rap"))
                                {
                                    billExchange = SendRAPClaims(ClaimSelected, commandType, claimData, out claimInfo, payerInfo, isHMO,  agencyLocation , out claimDataOut);
                                }
                                else if (Type.IsEqual("final"))
                                {
                                    billExchange = SendFinalClaims(ClaimSelected, commandType, claimData, out claimInfo, payerInfo, isHMO, agencyLocation, out claimDataOut);
                                }
                                else
                                {
                                    billExchange.isSuccessful = false;
                                    billExchange.Message = "Claims type is not identified. Try again.";
                                }
                            }
                            else
                            {
                                billExchange.Message = "Payer information is not right.";
                            }
                        }
                        else
                        {
                            billExchange.Message = "Payer information is not right.";
                        }
                    }
                    else
                    {
                        billExchange.Message = "Branch information is not found. Try again.";
                    }
                }
                else
                {
                    billExchange.Message = "Claim Information is not correct. Try again.";
                }
            }
            else
            {
                billExchange.Message = "Claim type is not identified. Try again.";
            }
            return billExchange.isSuccessful;
        }

        public BillExchange SendRAPClaims(List<Guid> ClaimSelected, ClaimCommandType commandType, ClaimData claimData, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, bool isHMO, AgencyLocation agencyLocation, out ClaimData claimDataOut)
        {
            var billExchange = new BillExchange { isSuccessful = false, Message = "Error in generating the RAP calim." };
            List<Rap> rapClaims = null;
            claimDataOut = null;
            var requestArr = GenerateJsonRAP(ClaimSelected, commandType, claimData.Id, out claimInfo, payerInfo, out rapClaims, isHMO, agencyLocation );
            if (requestArr.IsNotNullOrEmpty())
            {
                if (billingRepository.AddRapSnapShots(rapClaims, claimData.Id))
                {
                    requestArr = requestArr.Replace("&", "U+0026");
                    billExchange = GenerateANSI(requestArr);

                    if (billExchange != null && billExchange.isSuccessful && billExchange.Status == "OK")
                    {
                        if (billExchange.Result.IsNotNullOrEmpty())
                        {
                            claimData.Data = billExchange.Result;
                            claimData.BillIdentifers = claimInfo.ToXml<List<ClaimInfo>>();
                            claimData.ClaimType = isHMO ? ClaimType.HMO.ToString() : ClaimType.CMS.ToString();
                            billingRepository.UpdateClaimData(claimData);
                            if (commandType == ClaimCommandType.direct)
                            {
                                if (rapClaims != null && rapClaims.Count > 0)
                                {
                                    billingRepository.MarkRapsAsSubmitted(Current.AgencyId, rapClaims);
                                    rapClaims.ForEach(rap =>
                                    {
                                        Auditor.AddGeneralLog(Current.AgencyId,Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPSubmittedElectronically, string.Empty);
                                    });
                                }
                            }
                            else if (commandType == ClaimCommandType.download)
                            {
                                if (rapClaims != null && rapClaims.Count > 0)
                                {
                                    billingRepository.MarkRapsAsGenerated(Current.AgencyId, rapClaims);
                                    rapClaims.ForEach(rap =>
                                    {
                                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPGenerated, string.Empty);
                                    });
                                }
                            }
                            claimDataOut = claimData;
                            billExchange.isSuccessful = true;
                        }
                        else
                        {
                            billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                            billingRepository.DeleteRapSnapShots(claimData.Id);
                            claimDataOut = null;
                            billExchange.isSuccessful = false;
                        }
                    }
                    else
                    {
                        billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                        billingRepository.DeleteRapSnapShots(claimData.Id);
                        billExchange = billExchange ?? new BillExchange { Message = "Error in generating the calim." };
                        billExchange.isSuccessful = false;
                    }
                }
                else
                {
                    billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                    billingRepository.DeleteRapSnapShots(claimData.Id);
                    billExchange.isSuccessful = false;
                }
            }
            else
            {
                billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                billExchange.Message = "There is a problem generating the claim data. Try Again.";
                billExchange.isSuccessful = false;
            }
            return billExchange;
        }

        public BillExchange SendFinalClaims(List<Guid> ClaimSelected, ClaimCommandType commandType, ClaimData claimData, out List<ClaimInfo> claimInfo, AxxessSubmitterInfo payerInfo, bool isHMO, AgencyLocation agencyLocation, out ClaimData claimDataOut)
        {
            var billExchange = new BillExchange { isSuccessful = false, Message = "Error in generating the Final calim." };
             List<Final> finalClaims = null;
             claimDataOut = null;
             var requestArr = GenerateJsonFinal(ClaimSelected, commandType, claimData.Id, out claimInfo, payerInfo, out finalClaims, isHMO, agencyLocation);
            if (requestArr.IsNotNullOrEmpty())
            {
                if (billingRepository.AddFinaSnapShots(finalClaims, claimData.Id))
                {
                    requestArr = requestArr.Replace("&", "U+0026");
                    billExchange = GenerateANSI(requestArr);

                    if (billExchange != null && billExchange.isSuccessful && billExchange.Status == "OK")
                    {
                        if (billExchange.Result.IsNotNullOrEmpty())
                        {
                            claimData.Data = billExchange.Result;
                            claimData.BillIdentifers = claimInfo.ToXml<List<ClaimInfo>>();
                            claimData.ClaimType = isHMO ? ClaimType.HMO.ToString() : ClaimType.CMS.ToString();
                            billingRepository.UpdateClaimData(claimData);
                            if (commandType == ClaimCommandType.direct)
                            {
                                if (finalClaims != null && finalClaims.Count > 0)
                                {
                                    billingRepository.MarkFinalsAsSubmitted(Current.AgencyId, finalClaims);
                                    finalClaims.ForEach(final =>
                                    {
                                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalSubmittedElectronically, string.Empty);
                                    });
                                }
                            }
                            else if (commandType == ClaimCommandType.download)
                            {
                                if (finalClaims != null && finalClaims.Count > 0)
                                {
                                    billingRepository.MarkFinalsAsGenerated(Current.AgencyId, finalClaims);
                                    finalClaims.ForEach(final =>
                                    {
                                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalGenerated, string.Empty);
                                    });
                                }
                            }
                            claimDataOut = claimData;
                            billExchange.isSuccessful =true;
                        }
                        else
                        {
                            billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                            billingRepository.DeleteFinaSnapShots(claimData.Id);
                            claimDataOut = null;
                            billExchange.isSuccessful =false;
                        }
                    }
                    else
                    {
                        billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                        billingRepository.DeleteFinaSnapShots(claimData.Id);
                        billExchange = billExchange ?? new BillExchange { Message = "Error in generating the calim." };
                        billExchange.isSuccessful = false;
                    }
                }
                else
                {
                    billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                    billingRepository.DeleteFinaSnapShots(claimData.Id);
                }
            }
            else
            {
                billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                billExchange.Message = "There is a problem generating the claim data. Try Again.";
                billExchange.isSuccessful = false;
            }
            return billExchange;
        }

        public BillExchange GenerateANSI(string jsonData)
        {
            var billExchange = new BillExchange();
            try
            {
                var encoding = new ASCIIEncoding();
                string postData = ("request=" + jsonData);
                byte[] data = encoding.GetBytes(postData);
                var request = (HttpWebRequest)WebRequest.Create(AppSettings.ANSIGeneratorUrl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                var newStream = request.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();
                var response = (HttpWebResponse)request.GetResponse();
                if (response!=null && response.StatusCode == HttpStatusCode.OK)
                {
                    var receiveStream = response.GetResponseStream();
                    var encode = System.Text.Encoding.GetEncoding("utf-8");
                    var readStream = new StreamReader(receiveStream, encode);
                    var strResult = readStream.ReadToEnd();
                    var jss = new JavaScriptSerializer();
                    billExchange = jss.Deserialize<BillExchange>(strResult);
                    if (billExchange.Status == "OK")
                    {
                        billExchange.isSuccessful = true;
                    }
                    else
                    {
                        billExchange.isSuccessful = false;
                    }
                }
                else
                {
                    billExchange.Message = "The is a problem processing these claim. Try Again.";
                    billExchange.isSuccessful = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                billExchange.Message = "There is system problem. Try Again.";
                billExchange.isSuccessful = false;
                return billExchange;
            }
            return billExchange;
        }

        public bool VisitVerify(Guid Id, Guid episodeId, Guid patientId, List<Guid> visit)
        {
            bool result = false;
            if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && visit != null && visit.Count > 0)
            {
                var claim = billingRepository.GetFinal(Current.AgencyId, Id);
                if (claim != null)
                {
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                        if (episode != null )
                        {
                            var scheduleEvents = patientRepository.GetScheduledEventsOnly(Current.AgencyId, episode.PatientId, episode.Id, episode.StartDate, episode.EndDate);
            
                           // var scheduleEvents = episode.Schedule.ToObject<List<ScheduleEvent>>();
                            if (scheduleEvents != null)
                            {
                                var visitList = new List<ScheduleEvent>();

                                if (claim.PrimaryInsuranceId > 0)
                                {
                                    if (!claim.IsFinalInfoVerified || (claim.IsFinalInfoVerified && claim.Insurance.IsNullOrEmpty()))
                                    {
                                        var agencyInsurance = new AgencyInsurance();
                                        if (claim.PrimaryInsuranceId >= 1000)
                                        {
                                            agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                                        }
                                        else if (claim.PrimaryInsuranceId < 1000)
                                        {
                                            agencyInsurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, claim.PrimaryInsuranceId);
                                        }
                                        claim.Insurance = agencyInsurance.ToXml();
                                    }
                                }
                                var finalVisit = claim != null && claim.VerifiedVisits.IsNotNullOrEmpty() ? claim.VerifiedVisits.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();
                                visit.ForEach(v =>
                                {
                                    var scheduleVisit = scheduleEvents.FirstOrDefault(s => s.EventId == v);
                                    if (scheduleVisit != null)
                                    {
                                        scheduleVisit.IsBillable = true;
                                        visitList.Add(scheduleVisit);
                                    }
                                });
                                if (finalVisit != null && finalVisit.Count > 0)
                                {
                                    finalVisit.ForEach(f =>
                                    {
                                        if (scheduleEvents.Exists(e => e.EventId == f.EventId) && !visit.Contains(f.EventId))
                                        {
                                            scheduleEvents.FirstOrDefault(e => e.EventId == f.EventId).IsBillable = false;
                                        }
                                    });
                                }
                                claim.IsVisitVerified = true;
                                claim.VerifiedVisits = visitList.ToXml();
                                claim.Modified = DateTime.Now;
                                //episode.Schedule = scheduleEvents.ToXml();
                                if (patientRepository.UpdateScheduleEventsForIsBillable(Current.AgencyId, scheduleEvents))
                                {
                                    var supplyList = claim.Supply.IsNotNullOrEmpty() ? claim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                                    if (visitList != null && visitList.Count > 0)
                                    {
                                        visitList.ForEach(v =>
                                        {
                                            var episodeSupply = this.GetSupply(v);
                                            if (episodeSupply != null && episodeSupply.Count > 0)
                                            {
                                                episodeSupply.ForEach(s =>
                                                {
                                                    if (!supplyList.Exists(l => l.UniqueIdentifier == s.UniqueIdentifier))
                                                    {
                                                        s.IsBillable = true;
                                                        supplyList.Add(s);
                                                    }
                                                });
                                            }
                                        });
                                        claim.Supply = supplyList.ToXml();
                                    }
                                    result = billingRepository.UpdateFinalStatus(claim);
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool VisitSupply(Guid Id, Guid episodeId, Guid patientId, bool IsSupplyNotBillable)
        {
            bool result = false;
            if (!Id.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = billingRepository.GetFinalOnly(Current.AgencyId, Id);
                if (claim != null)
                {
                    claim.SupplyTotal = this.MedicareSupplyTotal(claim);
                    claim.Modified = DateTime.Now;
                    claim.IsSupplyVerified = true;

                    if (IsSupplyNotBillable && claim.HippsCode.IsNotNullOrEmpty() && claim.HippsCode.Length == 5)
                    {
                        claim.IsSupplyNotBillable = IsSupplyNotBillable;
                        var hippsCode = GetNrsSeverityLevel(claim.HippsCode);
                        if (hippsCode.IsNotNullOrEmpty() && hippsCode.Length == 5)
                        {
                            claim.HippsCode = hippsCode;
                        }
                    }
                    else if (claim.IsSupplyNotBillable && !IsSupplyNotBillable && claim.HippsCode.Length == 5)
                    {
                        claim.IsSupplyNotBillable = IsSupplyNotBillable;
                        var hippsCode = GetReverseNrsSeverityLevel(claim.HippsCode);
                        if (hippsCode.IsNotNullOrEmpty() && hippsCode.Length == 5)
                        {
                            claim.HippsCode = hippsCode;
                        }
                    }
                    else
                    {
                        claim.IsSupplyNotBillable = IsSupplyNotBillable;
                    }
                    result = billingRepository.UpdateFinal(claim);
                }
            }
            return result;
        }

        public Bill AllUnProcessedBill(Guid branchId, int insuranceId, string sortType, string claimType)
        {
            var bill = new Bill();
            bill.ClaimType = claimType;
            if (claimType.IsNotNullOrEmpty())
            {
                if (claimType.IsEqual("rap"))
                {
                    var raps = AllUnProcessedRaps(branchId, insuranceId);
                    if (raps != null && raps.Count > 0)
                    {
                        if (sortType == "rap-name-invert") raps = raps.Reverse().ToList();
                        else if (sortType == "rap-id") raps = raps.OrderBy(rap => rap.PatientIdNumber).ToList();
                        else if (sortType == "rap-id-invert") raps = raps.OrderBy(rap => rap.PatientIdNumber).Reverse().ToList();
                        else if (sortType == "rap-episode") raps = raps.OrderBy(rap => rap.EpisodeStartDate.ToString("yyyyMMdd")).ToList();
                        else if (sortType == "rap-episode-invert") raps = raps.OrderBy(rap => rap.EpisodeStartDate.ToString("yyyyMMdd")).Reverse().ToList();
                        bill.Claims = raps != null ? raps : new List<ClaimBill>();
                    }
                }
                else if (claimType.IsEqual("final"))
                {
                    var finals = AllUnProcessedFinals(branchId, insuranceId);
                    if (finals != null && finals.Count > 0)
                    {
                        if (sortType == "rap-name-invert") finals = finals.Reverse().ToList();
                        else if (sortType == "rap-id") finals = finals.OrderBy(final => final.PatientIdNumber).ToList();
                        else if (sortType == "rap-id-invert") finals = finals.OrderBy(final => final.PatientIdNumber).Reverse().ToList();
                        else if (sortType == "rap-episode") finals = finals.OrderBy(final => final.EpisodeStartDate.ToString("yyyyMMdd")).ToList();
                        else if (sortType == "rap-episode-invert") finals = finals.OrderBy(final => final.EpisodeStartDate.ToString("yyyyMMdd")).Reverse().ToList();
                        bill.Claims = finals != null ? finals : new List<ClaimBill>();
                    }
                }
            }
            bill.BranchId = branchId;
            bill.Insurance = insuranceId;
            bill.IsMedicareHMO = agencyRepository.IsMedicareHMO(Current.AgencyId, insuranceId);
            return bill;
        }

        public IList<ClaimBill> AllUnProcessedRaps(Guid branchId, int insuranceId)
        {
            var raps = billingRepository.GetOutstandingRapClaims(Current.AgencyId, branchId, insuranceId);
            if (raps != null && raps.Count > 0)
            {
                var visitStatus = ScheduleStatusFatory.OASISAndNurseNotesAfterQA();// new int[] { (int)ScheduleStatus.NoteCompleted, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.EvalToBeSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically, (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalReturnedWPhysicianSignature };
                var socDisciplineTasks =DisciplineTaskFactory.AllSOCDisciplineTasks();// new int[] { (int)DisciplineTasks.OASISCStartofCare, (int)DisciplineTasks.OASISCStartofCarePT, (int)DisciplineTasks.OASISCStartofCareOT };
                var recertOrRocDisciplineTasks = DisciplineTaskFactory.LastFiveDayAssessments();// new int[] { (int)DisciplineTasks.OASISCRecertification, (int)DisciplineTasks.OASISCRecertificationOT, (int)DisciplineTasks.OASISCRecertificationPT, (int)DisciplineTasks.OASISCResumptionofCare, (int)DisciplineTasks.OASISCResumptionofCareOT, (int)DisciplineTasks.OASISCResumptionofCarePT };
                var oasisStatus = ScheduleStatusFatory.OASISAfterQA(); //new int[] { (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.OasisCompletedNotExported };
                var schedules = billingRepository.GetRAPClaimSchedules(Current.AgencyId, raps.Select(r => r.EpisodeId).ToList());
                raps.ForEach(rap =>
                {
                    if (rap.Status == (int)BillingStatus.ClaimCreated)
                    {
                        rap.IsFirstBillableVisit = rap.IsFirstBillableVisit || schedules.Exists(s => s.EpisodeId == rap.EpisodeId && s.IsBillable && visitStatus.Contains(s.Status));

                        if (!rap.IsOasisComplete)
                        {
                            var soc = schedules.Where(s => !s.EpisodeId.IsEmpty() && s.EpisodeId == rap.EpisodeId && socDisciplineTasks.Contains(s.DisciplineTask)).OrderBy(s => s.EventDate.Date).FirstOrDefault();
                            if (soc != null)
                            {
                                if (oasisStatus.Contains(soc.Status))
                                {
                                    rap.IsOasisComplete = true;
                                }
                            }
                            else
                            {
                                var recertOrRoc = schedules.Where(s => !s.NewEpisodeId.IsEmpty() && s.NewEpisodeId != s.EpisodeId && s.NextEpisodeId == rap.EpisodeId && recertOrRocDisciplineTasks.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate.Date).FirstOrDefault();
                                if (recertOrRoc != null)
                                {
                                    if (oasisStatus.Contains(recertOrRoc.Status))
                                    {
                                        rap.IsOasisComplete = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        rap.IsFirstBillableVisit = true;
                        rap.IsOasisComplete = true;
                        rap.IsVerified = true;
                    }
                });
            }
            return raps;
        }

        public IList<ClaimBill> AllUnProcessedFinals(Guid branchId, int insuranceId)
        {
            var finals = billingRepository.GetOutstandingFinalClaims(Current.AgencyId, branchId, insuranceId);
            if (finals != null && finals.Count > 0)
            {
                var visitDisciplines = DisciplineFactory.NonBillableDisciplines().Select(d => d.ToString());// new string[] { Disciplines.ReportsAndNotes.ToString(), Disciplines.Orders.ToString(), Disciplines.Claim.ToString() };
                var visitStatus = ScheduleStatusFatory.OASISAndNurseNotesAfterQA();// new int[] { (int)ScheduleStatus.NoteCompleted, (int)ScheduleStatus.OasisCompletedNotExported, (int)ScheduleStatus.OasisCompletedExportReady, (int)ScheduleStatus.OasisExported, (int)ScheduleStatus.EvalToBeSentToPhysician, (int)ScheduleStatus.EvalSentToPhysicianElectronically, (int)ScheduleStatus.EvalSentToPhysician, (int)ScheduleStatus.EvalReturnedWPhysicianSignature };
                var orderDisciplineTasks = DisciplineTaskFactory.PhysicianOrdersWithOutFaceToFace();// new int[] { (int)DisciplineTasks.PhysicianOrder, (int)DisciplineTasks.HCFA485, (int)DisciplineTasks.NonOasisHCFA485 };
                var ids = finals.Select(r => r.EpisodeId).ToList();
                var schedules = billingRepository.GetFinalClaimSchedules(Current.AgencyId, ids);
                var raps = billingRepository.GetRapsToGenerateByIdsLean(Current.AgencyId, ids);
                finals.ForEach(final =>
                {
                    if (final.Status == (int)BillingStatus.ClaimCreated)
                    {
                        final.AreVisitsComplete = final.IsVisitVerified || !schedules.Exists(s => s.EpisodeId == final.EpisodeId && !s.IsDeprecated && !visitDisciplines.Contains(s.Discipline) && !(s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter) && !visitStatus.Contains(s.Status) && !s.IsMissedVisit && s.IsBillable);
                        if (!final.AreOrdersComplete)
                        {
                            var isCurrentOrdersNotComplete = !schedules.Exists(s => !s.EventId.IsEmpty() && s.EpisodeId == final.EpisodeId && !s.IsDeprecated && !s.IsMissedVisit && orderDisciplineTasks.Contains(s.DisciplineTask) && !s.IsOrderForNextEpisode && s.Status != (int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                            if (isCurrentOrdersNotComplete)
                            {
                                var isPreviousOrdersComplete = !schedules.Exists(s => !s.EventId.IsEmpty() && s.NextEpisodeId == final.EpisodeId && !s.IsDeprecated && !s.IsMissedVisit && orderDisciplineTasks.Contains(s.DisciplineTask) && s.IsOrderForNextEpisode && s.Status != (int)ScheduleStatus.OrderReturnedWPhysicianSignature);
                                if (isPreviousOrdersComplete)
                                {
                                    final.AreOrdersComplete = true;
                                }
                                else
                                {
                                    final.AreOrdersComplete = false;
                                }
                            }
                            else
                            {
                                final.AreOrdersComplete = false;
                            }
                        }
                        final.IsRapGenerated = final.IsRapGenerated || raps.Exists(r => r.Id == final.Id && (r.IsGenerated || BillingStatusFactory.Processed().Contains(r.Status)));
                    }
                    else
                    {
                        final.AreOrdersComplete = true;
                        final.IsRapGenerated = true;
                    }
                });
            }
            return finals;
        }

        public BillLean ClaimToGenerate(List<Guid> claimSelected, Guid branchId, int primaryInsurance, string type)
        {
            var bill = new BillLean();
            bill.BranchId = branchId;
            bill.Insurance = primaryInsurance;
            bill.Type = type;
            if (type.IsNotNullOrEmpty())
            {
                var isElectronicSubmssion = false;
                if (primaryInsurance < 1000 && primaryInsurance > 0)
                {
                    var agency = agencyRepository.Get(Current.AgencyId);
                    if (agency != null)
                    {
                        isElectronicSubmssion = agency.IsAxxessTheBiller;
                    }
                }
                else if (primaryInsurance >= 1000)
                {
                    var insurance = agencyRepository.GetInsurance(primaryInsurance, Current.AgencyId);
                    if (insurance != null)
                    {
                        isElectronicSubmssion = insurance.IsAxxessTheBiller;
                    }
                }
                bill.IsElectronicSubmssion = isElectronicSubmssion;
                if (type.IsEqual("rap"))
                {
                    bill.Claims = billingRepository.GetRapsByIds(Current.AgencyId, claimSelected);
                }
                else if (type.IsEqual("final"))
                {
                    bill.Claims = billingRepository.GetFinalsByIds(Current.AgencyId, claimSelected);
                }
            }
            return bill;
        }

        public long GetNextClaimId(ClaimData claimData)
        {
            return billingRepository.AddClaimData(claimData);
        }

        public IList<ClaimHistoryLean> Activity(Guid patientId, int insuranceId)
        {
            var raps = billingRepository.GetRapsHistory(Current.AgencyId, patientId, insuranceId);
            var finals = billingRepository.GetFinalsHistory(Current.AgencyId, patientId, insuranceId);
            var claims = new List<ClaimHistoryLean>();
            if (raps != null && raps.Count > 0)
            {
                claims.AddRange(raps);
            }
            if (finals != null && finals.Count > 0)
            {
                claims.AddRange(finals);
            }
            return claims;
        }

        public PendingClaimLean PendingClaim(Guid id, string type)
        {
            if (type == "RAP") return billingRepository.PendingClaimRAP(Current.AgencyId, id);
            else return billingRepository.PendingClaimFinal(Current.AgencyId, id);
        }

        public IList<PendingClaimLean> PendingClaimRaps(Guid branchId, string primaryInsurance)
        {
            var claims = new List<PendingClaimLean>();
            if (primaryInsurance.IsInteger())
            {
                var claimsToAddRap = billingRepository.PendingClaimRaps(Current.AgencyId, branchId, primaryInsurance.ToInteger());
                if (claimsToAddRap != null && claimsToAddRap.Count > 0)
                {
                    var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                    var zipCodes = claimsToAddRap.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).Distinct().ToList() ?? new List<string>();
                    if (agencyLocation!=null && agencyLocation.AddressZipCode.IsNotNullOrEmpty())
                    {
                        zipCodes.Add(agencyLocation.AddressZipCode);
                    }
                    var cbsaCodes = lookUpRepository.CbsaCodesByZip(zipCodes.Distinct().ToArray()) ?? new List<CBSACode>();
                    var hippsAndYears = claimsToAddRap.Where(s => s.HippsCode.IsNotNullOrEmpty() && s.EpisodeStartDate.Date > DateTime.MinValue).Select(s => string.Format("{0}{1}", s.HippsCode, s.EpisodeStartDate.Year)).Distinct().ToArray();
                    var hippsAndHHRG = lookUpRepository.GetHhrgByHippsCodeAndYear(hippsAndYears);
                    var years = claimsToAddRap.Select(s => s.EpisodeStartDate.Year).Distinct().ToArray();
                    var ppsStandards = lookUpRepository.PPSStandards(years);
                    claimsToAddRap.ForEach(c =>
                    {
                        var cbsa = cbsaCodes.SingleOrDefault(cb => cb.Zip.IsEqual(c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty));
                        var hipps = hippsAndHHRG.SingleOrDefault(h => h.HIPPS == c.HippsCode && h.Time.Year == c.EpisodeStartDate.Year);
                        var pps = ppsStandards.SingleOrDefault(p => p.Time.Year == c.EpisodeStartDate.Year);
                        if (cbsa != null && hipps != null && pps != null)
                        {
                            var payment = this.GetProspectivePaymentAmount(pps, hipps, cbsa);
                            c.ClaimAmount = c.AssessmentType.IsNotNullOrEmpty() && (c.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || c.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || c.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString()) ? 0.6 * Math.Round(payment, 2) : 0.5 * Math.Round(payment, 2);
                        }
                    });
                    claims.AddRange(claimsToAddRap);
                }
            }
            return claims;
        }

        public IList<PendingClaimLean> PendingClaimFinals(Guid branchId, string primaryInsurance)
        {
            var claims = new List<PendingClaimLean>();
            if (primaryInsurance.IsInteger())
            {
               // var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var claimsToAddFinal = billingRepository.PendingClaimFinals(Current.AgencyId, branchId, primaryInsurance.ToInteger());
                if (claimsToAddFinal != null && claimsToAddFinal.Count > 0)
                {
                    //claimsToAddFinal.ForEach(c => { c.ClaimAmount = Math.Round(lookUpRepository.GetProspectivePaymentAmount(c.HippsCode, c.EpisodeStartDate, c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2); });
                    claims.AddRange(claimsToAddFinal);
                }
            }
            return claims;
        }

        public IList<ClaimLean> AccountsReceivables(Guid branchId, int insurance, DateTime startDate, DateTime endDate, string type)
        {
            var claims = new List<ClaimLean>();
            var allClaims = billingRepository.GetAccountsReceivables(Current.AgencyId, branchId, insurance, startDate, endDate, type);
            if (allClaims != null && allClaims.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var zipCodes = allClaims.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).Distinct().ToList() ?? new List<string>();
                if (agencyLocation != null && agencyLocation.AddressZipCode.IsNotNullOrEmpty())
                {
                    zipCodes.Add(agencyLocation.AddressZipCode);
                }
                var cbsaCodes = lookUpRepository.CbsaCodesByZip(zipCodes.Distinct().ToArray()) ?? new List<CBSACode>();
                var hippsAndYears = allClaims.Where(s => s.HippsCode.IsNotNullOrEmpty() && s.EpisodeStartDate.Date > DateTime.MinValue).Select(s => string.Format("{0}{1}", s.HippsCode, s.EpisodeStartDate.Year)).Distinct().ToArray();
                var hippsAndHHRG = lookUpRepository.GetHhrgByHippsCodeAndYear(hippsAndYears);
                var years = allClaims.Select(s => s.EpisodeStartDate.Year).Distinct().ToArray();
                var ppsStandards = lookUpRepository.PPSStandards(years);
                allClaims.ForEach(c =>
                {
                    var cbsa = cbsaCodes.SingleOrDefault(cb => cb.Zip.IsEqual(c.AddressZipCode.IsNotNullOrEmpty() ? c.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty));
                    var hipps = hippsAndHHRG.SingleOrDefault(h => h.HIPPS == c.HippsCode && h.Time.Year == c.EpisodeStartDate.Year);
                    var pps = ppsStandards.SingleOrDefault(p => p.Time.Year == c.EpisodeStartDate.Year);
                    if (cbsa != null && hipps != null && pps != null)
                    {
                        var payment = this.GetProspectivePaymentAmount(pps, hipps, cbsa);
                        if (type.IsEqual("rap"))
                        {
                            c.ClaimAmount = c.AssessmentType.IsNotNullOrEmpty() && (c.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || c.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || c.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString()) ? 0.6 * Math.Round(payment, 2) : 0.5 * Math.Round(payment, 2);
                        }
                        else
                        {
                            c.ClaimAmount = payment;
                        }
                    }
                });
                claims.AddRange(allClaims);
            }
            return claims.OrderBy(c => c.ClaimDate.ToShortDateString().ToZeroFilled()).ToList();
        }

        public IList<Revenue> GetUnearnedRevenue(Guid branchId, int insurance, DateTime endDate)
        {
            var result = new List<Revenue>();
            var revenueList = billingRepository.GetRevenue(Current.AgencyId, branchId, insurance, BillingStatusFactory.Processed(), endDate);
            if (revenueList != null && revenueList.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var zipCodes = revenueList.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).Distinct().ToList() ?? new List<string>();
                if (agencyLocation != null && agencyLocation.AddressZipCode.IsNotNullOrEmpty())
                {
                    zipCodes.Add(agencyLocation.AddressZipCode);
                }
                var cbsaCodes = lookUpRepository.CbsaCodesByZip(zipCodes.Distinct().ToArray()) ?? new List<CBSACode>();
                var hippsAndYears = revenueList.Where(s => s.HippsCode.IsNotNullOrEmpty() && s.EpisodeStartDate.Date > DateTime.MinValue).Select(s => string.Format("{0}{1}", s.HippsCode, s.EpisodeStartDate.Year)).Distinct().ToArray();
                var hippsAndHHRG = lookUpRepository.GetHhrgByHippsCodeAndYear(hippsAndYears);
                var years = revenueList.Select(s => s.EpisodeStartDate.Year).Distinct().ToArray();
                var ppsStandards = lookUpRepository.PPSStandards(years);
                var episodeIds = revenueList.Select(s => s.EpisodeId).Distinct().ToList();
                var allSchedules = billingRepository.GetRevenueScheduleEvents(Current.AgencyId, episodeIds);
                revenueList.ForEach(rap =>
                {
                    var cbsa = cbsaCodes.SingleOrDefault(cb => cb.Zip.IsEqual(rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty));
                    var hipps = hippsAndHHRG.SingleOrDefault(h => h.HIPPS == rap.HippsCode && h.Time.Year == rap.EpisodeStartDate.Year);
                    var pps = ppsStandards.SingleOrDefault(p => p.Time.Year == rap.EpisodeStartDate.Year);
                    var prospectivePayment = this.GetProspectivePayment(pps, hipps, cbsa);
                    if (prospectivePayment != null)
                    {
                        rap.AssessmentTypeName = rap.AssessmentType.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetActualDescription();
                        rap.RapAmount = rap.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                        rap.ProspectivePayment = prospectivePayment.TotalProspectiveAmount;
                        var schedules = allSchedules.Where(s => s.EpisodeId == rap.EpisodeId).ToList();
                        if (schedules != null && schedules.Count > 0)
                        {
                            rap.BillableVisitCount = schedules.Where(v => v.IsBillable && !v.IsMissedVisit).ToList().Count;
                            rap.CompletedVisitCount = schedules.Where(v => v.IsBillable && ScheduleStatusFatory.BillStatus().Contains(v.Status) && !v.IsMissedVisit && v.EventDate.IsValid() && v.EventDate.Date <= endDate.Date).ToList().Count; // (v.Status == 420 || v.Status == 425 || v.Status == 430 || v.Status == 435 || v.Status == 215 || v.Status == 220 || v.Status == 225 || v.Status == 230 || v.Status == 235) 
                        }
                        if (rap.CompletedVisitCount >= 0 && rap.BillableVisitCount > 0)
                        {
                            rap.RapVisitCount = (int)Math.Ceiling(0.5 * rap.BillableVisitCount);
                            if (rap.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString())
                            {
                                rap.RapVisitCount = (int)Math.Ceiling(0.6 * rap.BillableVisitCount);
                            }
                            if (rap.CompletedVisitCount < rap.RapVisitCount && rap.CompletedVisitCount <= rap.BillableVisitCount)
                            {
                                var unearnedVisits = rap.RapVisitCount - rap.CompletedVisitCount;
                                var unitAmount = Math.Round(prospectivePayment.TotalAmount / rap.BillableVisitCount, 2);
                                rap.UnearnedRevenueAmount = Math.Round((unearnedVisits) * unitAmount, 2);
                                rap.UnearnedVisitCount = unearnedVisits;
                                rap.UnitAmount = unitAmount;
                                result.Add(rap);
                            }
                        }
                    }
                });
            }
            return result;
        }

        public IList<Revenue> GetUnbilledRevenue(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var result = new List<Revenue>();
            var revenueList = billingRepository.GetRevenue(Current.AgencyId, branchId, insurance,BillingStatusFactory.Processed(), startDate, endDate);
            if (revenueList != null && revenueList.Count > 0)
            {

                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var zipCodes = revenueList.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).Distinct().ToList() ?? new List<string>();
                if (agencyLocation != null && agencyLocation.AddressZipCode.IsNotNullOrEmpty())
                {
                    zipCodes.Add(agencyLocation.AddressZipCode);
                }
                var cbsaCodes = lookUpRepository.CbsaCodesByZip(zipCodes.Distinct().ToArray()) ?? new List<CBSACode>();
                var hippsAndYears = revenueList.Where(s => s.HippsCode.IsNotNullOrEmpty() && s.EpisodeStartDate.Date > DateTime.MinValue).Select(s => string.Format("{0}{1}", s.HippsCode, s.EpisodeStartDate.Year)).Distinct().ToArray();
                var hippsAndHHRG = lookUpRepository.GetHhrgByHippsCodeAndYear(hippsAndYears);
                var years = revenueList.Select(s => s.EpisodeStartDate.Year).Distinct().ToArray();
                var ppsStandards = lookUpRepository.PPSStandards(years);
                var episodeIds = revenueList.Select(s => s.EpisodeId).Distinct().ToList();
                var allSchedules = billingRepository.GetRevenueScheduleEvents(Current.AgencyId, episodeIds);
                revenueList.ForEach(rap =>
                {
                    var cbsa = cbsaCodes.SingleOrDefault(cb => cb.Zip.IsEqual(rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty));
                    var hipps = hippsAndHHRG.SingleOrDefault(h => h.HIPPS == rap.HippsCode && h.Time.Year == rap.EpisodeStartDate.Year);
                    var pps = ppsStandards.SingleOrDefault(p => p.Time.Year == rap.EpisodeStartDate.Year);
                    var prospectivePayment = this.GetProspectivePayment(pps, hipps, cbsa);
                    if (prospectivePayment != null)
                    {
                        rap.AssessmentTypeName = rap.AssessmentType.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetActualDescription();
                        rap.RapAmount = rap.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                        rap.ProspectivePayment = prospectivePayment.TotalProspectiveAmount;
                        var schedules = allSchedules.Where(s => s.EpisodeId == rap.EpisodeId).ToList();
                        if (schedules != null && schedules.Count > 0)
                        {
                            rap.BillableVisitCount = schedules.Where(v => v.IsBillable && !v.IsMissedVisit).ToList().Count;
                            rap.CompletedVisitCount = schedules.Where(v => v.IsBillable && ScheduleStatusFatory.BillStatus().Contains(v.Status) && !v.IsMissedVisit && v.EventDate.IsValid() && v.EventDate.Date <= endDate.Date).ToList().Count;//(v.Status == 420 || v.Status == 425 || v.Status == 430 || v.Status == 435 || v.Status == 215 || v.Status == 220 || v.Status == 225 || v.Status == 230 || v.Status == 235)
                        }
                        if (rap.CompletedVisitCount >= 0 && rap.BillableVisitCount > 0)
                        {
                            int rapVisits = (int)Math.Ceiling(0.5 * rap.BillableVisitCount);
                            if (rap.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString())
                            {
                                rapVisits = (int)Math.Ceiling(0.6 * rap.BillableVisitCount);
                            }
                            if (rap.CompletedVisitCount > rapVisits && rap.CompletedVisitCount <= rap.BillableVisitCount)
                            {
                                var unitAmount = Math.Round(prospectivePayment.TotalAmount / rap.BillableVisitCount, 2);
                                rap.UnbilledRevenueAmount = Math.Round((rap.CompletedVisitCount - rapVisits) * unitAmount, 2);
                                rap.UnbilledVisitCount = rap.CompletedVisitCount - rapVisits;
                                rap.UnitAmount = unitAmount;
                                result.Add(rap);
                            }
                        }
                    }
                });
            }
            return result;
        }

        public IList<Revenue> GetEarnedRevenue(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var result = new List<Revenue>();
            var revenueList = billingRepository.GetRevenue(Current.AgencyId, branchId, insurance,BillingStatusFactory.Processed(), startDate, endDate);
            if (revenueList != null && revenueList.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var zipCodes = revenueList.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).Distinct().ToList() ?? new List<string>();
                if (agencyLocation != null && agencyLocation.AddressZipCode.IsNotNullOrEmpty())
                {
                    zipCodes.Add(agencyLocation.AddressZipCode);
                }
                var cbsaCodes = lookUpRepository.CbsaCodesByZip(zipCodes.Distinct().ToArray()) ?? new List<CBSACode>();
                var hippsAndYears = revenueList.Where(s => s.HippsCode.IsNotNullOrEmpty() && s.EpisodeStartDate.Date > DateTime.MinValue).Select(s => string.Format("{0}{1}", s.HippsCode, s.EpisodeStartDate.Year)).Distinct().ToArray();
                var hippsAndHHRG = lookUpRepository.GetHhrgByHippsCodeAndYear(hippsAndYears);
                var years = revenueList.Select(s => s.EpisodeStartDate.Year).Distinct().ToArray();
                var ppsStandards = lookUpRepository.PPSStandards(years);
                var episodeIds = revenueList.Select(s => s.EpisodeId).Distinct().ToList();
                var allSchedules = billingRepository.GetRevenueScheduleEvents(Current.AgencyId, episodeIds);
                revenueList.ForEach(rap =>
                {
                    var cbsa = cbsaCodes.SingleOrDefault(cb => cb.Zip.IsEqual(rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty));
                    var hipps = hippsAndHHRG.SingleOrDefault(h => h.HIPPS == rap.HippsCode && h.Time.Year == rap.EpisodeStartDate.Year);
                    var pps = ppsStandards.SingleOrDefault(p => p.Time.Year == rap.EpisodeStartDate.Year);
                    var prospectivePayment = this.GetProspectivePayment(pps, hipps, cbsa);
                    if (prospectivePayment != null)
                    {
                        rap.AssessmentTypeName = rap.AssessmentType.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetActualDescription();
                        rap.RapAmount = rap.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                        rap.ProspectivePayment = prospectivePayment.TotalProspectiveAmount;

                        var schedules = allSchedules.Where(s => s.EpisodeId == rap.EpisodeId).ToList();
                        if (schedules != null && schedules.Count > 0)
                        {
                            rap.BillableVisitCount = schedules.Where(v => v.IsBillable && !v.IsMissedVisit).ToList().Count;
                            rap.CompletedVisitCount = schedules.Where(v => v.IsBillable && ScheduleStatusFatory.BillStatus().Contains(v.Status) && !v.IsMissedVisit && v.EventDate.IsValid() && v.EventDate.Date >= startDate.Date && v.EventDate.Date <= endDate.Date).ToList().Count;//(v.Status == 420 || v.Status == 425 || v.Status == 430 || v.Status == 435 || v.Status == 215 || v.Status == 220 || v.Status == 225 || v.Status == 230 || v.Status == 235)
                        }
                        if (rap.CompletedVisitCount >= 0 && rap.BillableVisitCount > 0 && rap.CompletedVisitCount < rap.BillableVisitCount)
                        {
                            var unitAmount = Math.Round(prospectivePayment.TotalAmount / rap.BillableVisitCount, 2);
                            rap.EarnedRevenueAmount = rap.CompletedVisitCount * unitAmount;
                            rap.UnitAmount = unitAmount;
                            result.Add(rap);
                        }
                    }
                });
            }
            return result;
        }

        public IList<Revenue> GetEarnedRevenueByEpisodeDays(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var result = new List<Revenue>();
            var revenueList = billingRepository.GetRevenue(Current.AgencyId, branchId, insurance,BillingStatusFactory.PaidAndSubmitted(), startDate, endDate);
            if (revenueList != null && revenueList.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var zipCodes = revenueList.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).Distinct().ToList() ?? new List<string>();
                if (agencyLocation != null && agencyLocation.AddressZipCode.IsNotNullOrEmpty())
                {
                    zipCodes.Add(agencyLocation.AddressZipCode);
                }
                var cbsaCodes = lookUpRepository.CbsaCodesByZip(zipCodes.Distinct().ToArray()) ?? new List<CBSACode>();
                var hippsAndYears = revenueList.Where(s => s.HippsCode.IsNotNullOrEmpty() && s.EpisodeStartDate.Date > DateTime.MinValue).Select(s => string.Format("{0}{1}", s.HippsCode, s.EpisodeStartDate.Year)).Distinct().ToArray();
                var hippsAndHHRG = lookUpRepository.GetHhrgByHippsCodeAndYear(hippsAndYears);
                var years = revenueList.Select(s => s.EpisodeStartDate.Year).Distinct().ToArray();
                var ppsStandards = lookUpRepository.PPSStandards(years);
                revenueList.ForEach(rap =>
                {
                    var cbsa = cbsaCodes.SingleOrDefault(cb => cb.Zip.IsEqual(rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty));
                    var hipps = hippsAndHHRG.SingleOrDefault(h => h.HIPPS == rap.HippsCode && h.Time.Year == rap.EpisodeStartDate.Year);
                    var pps = ppsStandards.SingleOrDefault(p => p.Time.Year == rap.EpisodeStartDate.Year);
                    var prospectivePayment = this.GetProspectivePayment(pps, hipps, cbsa);
                    if (prospectivePayment != null)
                    {
                        rap.AssessmentTypeName = rap.AssessmentType.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetActualDescription();
                        rap.RapAmount = rap.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                        rap.ProspectivePayment = prospectivePayment.TotalProspectiveAmount;

                        if (rap.EpisodeStartDate.IsValid() && rap.EpisodeEndDate.IsValid() && rap.EpisodeEndDate > rap.EpisodeStartDate)
                        {
                            if (rap.EpisodeStartDate.Date >= startDate.Date)
                            {
                                if (rap.EpisodeEndDate.Date > endDate.Date)
                                {
                                    rap.CompletedDayCount = endDate.Subtract(rap.EpisodeStartDate).Days;
                                }
                                else
                                {
                                    rap.CompletedDayCount = rap.EpisodeEndDate.Subtract(rap.EpisodeStartDate).Days + 1;
                                }
                            }
                            else
                            {
                                if (rap.EpisodeEndDate.Date > endDate.Date)
                                {
                                    rap.CompletedDayCount = endDate.Subtract(startDate).Days;
                                }
                                else
                                {
                                    rap.CompletedDayCount = rap.EpisodeEndDate.Subtract(startDate).Days;
                                }
                            }

                            if (rap.CompletedDayCount >= 0 && rap.BillableDayCount >= 0)
                            {
                                var unitAmount = Math.Round(prospectivePayment.TotalAmount / rap.BillableDayCount, 2);
                                rap.EarnedRevenueAmount = rap.CompletedDayCount * unitAmount;
                                rap.UnitAmount = unitAmount;
                                result.Add(rap);
                            }
                        }
                    }
                });
            }
            return result;
        }

        public IList<Revenue> GetRevenueReport(Guid branchId, int insurance, DateTime startDate, DateTime endDate)
        {
            var result = new List<Revenue>();
            var revenueList = billingRepository.GetRevenue(Current.AgencyId, branchId, insurance, BillingStatusFactory.Processed(), startDate, endDate);
            if (revenueList != null && revenueList.Count > 0)
            {
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                var zipCodes = revenueList.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).Distinct().ToList() ?? new List<string>();
                if (agencyLocation != null && agencyLocation.AddressZipCode.IsNotNullOrEmpty())
                {
                    zipCodes.Add(agencyLocation.AddressZipCode);
                }
                var cbsaCodes = lookUpRepository.CbsaCodesByZip(zipCodes.Distinct().ToArray()) ?? new List<CBSACode>();
                var hippsAndYears = revenueList.Where(s => s.HippsCode.IsNotNullOrEmpty() && s.EpisodeStartDate.Date > DateTime.MinValue).Select(s => string.Format("{0}{1}", s.HippsCode, s.EpisodeStartDate.Year)).Distinct().ToArray();
                var hippsAndHHRG = lookUpRepository.GetHhrgByHippsCodeAndYear(hippsAndYears);
                var years = revenueList.Select(s => s.EpisodeStartDate.Year).Distinct().ToArray();
                var ppsStandards = lookUpRepository.PPSStandards(years);
                var allSchedules = billingRepository.GetRevenueScheduleEvents(Current.AgencyId, revenueList.Select(r => r.EpisodeId).Distinct().ToList());
                revenueList.ForEach(rap =>
                {
                    if (rap != null)
                    {
                        var cbsa = cbsaCodes.SingleOrDefault(cb => cb.Zip.IsEqual(rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty));
                        var hipps = hippsAndHHRG.SingleOrDefault(h => h.HIPPS == rap.HippsCode && h.Time.Year == rap.EpisodeStartDate.Year);
                        var pps = ppsStandards.SingleOrDefault(p => p.Time.Year == rap.EpisodeStartDate.Year);
                        var prospectivePayment = this.GetProspectivePayment(pps, hipps, cbsa);
                        if (prospectivePayment != null)
                        {
                            rap.AssessmentTypeName = rap.AssessmentType.ToEnum<DisciplineTasks>(DisciplineTasks.NoDiscipline).GetActualDescription();
                            rap.ProspectivePayment = prospectivePayment.TotalProspectiveAmount;
                            var schedules = allSchedules.Where(s => s.EpisodeId == rap.EpisodeId).ToList();
                            if (schedules != null && schedules.Count > 0)// rap.Schedule.IsNotNullOrEmpty())
                            {
                                rap.BillableVisitCount = schedules.Where(v => v.IsBillable && !v.IsMissedVisit).ToList().Count;
                                rap.CompletedVisitCount = schedules.Where(v => v.IsBillable && ScheduleStatusFatory.BillStatus().Contains(v.Status) && !v.IsMissedVisit && v.EventDate.IsValid() && v.EventDate.Date <= endDate.Date).ToList().Count;//(v.Status == 420 || v.Status == 425 || v.Status == 430 || v.Status == 435 || v.Status == 215 || v.Status == 220 || v.Status == 225 || v.Status == 230 || v.Status == 235) 
                            }

                            if (rap.CompletedVisitCount >= 0 && rap.BillableVisitCount > 0)
                            {
                                rap.UnitAmount = Math.Round(prospectivePayment.TotalAmount / rap.BillableVisitCount, 2);
                                rap.EarnedRevenueAmount = Math.Round((rap.CompletedVisitCount) * rap.UnitAmount, 2);

                                rap.RapVisitCount = (int)Math.Ceiling(0.5 * rap.BillableVisitCount);
                                if (rap.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString())
                                {
                                    rap.RapVisitCount = (int)Math.Ceiling(0.6 * rap.BillableVisitCount);
                                }

                                if (rap.ClaimDate.IsDate() && rap.ClaimDate.ToDateTime().Date != DateTime.MinValue.Date && rap.ClaimDate.ToDateTime().Date <= endDate.Date)
                                {
                                    rap.RapAmount = rap.RapVisitCount * rap.UnitAmount;

                                    if (rap.CompletedVisitCount <= rap.RapVisitCount)
                                    {
                                        rap.UnearnedVisitCount = rap.RapVisitCount - rap.CompletedVisitCount;
                                        rap.UnearnedRevenueAmount = rap.UnearnedVisitCount * rap.UnitAmount;
                                    }

                                    if (rap.CompletedVisitCount > rap.RapVisitCount)
                                    {
                                        rap.UnbilledVisitCount = rap.CompletedVisitCount - rap.RapVisitCount;
                                        rap.UnbilledRevenueAmount = rap.UnbilledVisitCount * rap.UnitAmount;
                                    }
                                }
                                else
                                {
                                    rap.RapAmount = 0.0;
                                    rap.RapVisitCount = 0;

                                    rap.UnbilledVisitCount = rap.CompletedVisitCount;
                                    rap.UnbilledRevenueAmount = rap.EarnedRevenueAmount;

                                    rap.UnearnedVisitCount = 0;
                                    rap.UnearnedRevenueAmount = 0.0;
                                }
                            }
                            result.Add(rap);
                        }
                    }
                });
            }
            return result;
        }

        public IList<TypeOfBill> GetAllUnProcessedBill(bool isLimit, int limit)
        {
            var claims = new List<TypeOfBill>();
            var raps = billingRepository.GetOutstandingRaps(Current.AgencyId, isLimit, limit);
            if (raps != null && raps.Count > 0)
            {
                claims.AddRange(raps);
            }
            var finals = billingRepository.GetOutstandingFinals(Current.AgencyId,  isLimit,  limit);
            if (finals != null && finals.Count > 0)
            {
                claims.AddRange(finals);
            }
            return claims.OrderBy(c => c.SortData).Take(limit).ToList();
        }

        public List<Supply> GetSupply(ScheduleEvent scheduleEvent)
        {
            var supplies = new List<Supply>();
            switch ((DisciplineTasks)scheduleEvent.DisciplineTask)
            {
                case DisciplineTasks.NonOASISRecertification:
                case DisciplineTasks.NonOASISStartofCare:
                case DisciplineTasks.OASISCStartofCare:
                case DisciplineTasks.OASISCStartofCarePT:
                case DisciplineTasks.OASISCStartofCareOT:
                case DisciplineTasks.OASISCResumptionofCare:
                case DisciplineTasks.OASISCResumptionofCarePT:
                case DisciplineTasks.OASISCResumptionofCareOT:
                case DisciplineTasks.OASISCFollowUp:
                case DisciplineTasks.OASISCFollowupPT:
                case DisciplineTasks.OASISCFollowupOT:
                case DisciplineTasks.OASISCRecertification:
                case DisciplineTasks.OASISCRecertificationPT:
                case DisciplineTasks.OASISCRecertificationOT:
                    var assessment = assessmentRepository.GetWithNoList(Current.AgencyId, scheduleEvent.EventId, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
                    if (assessment != null && assessment.Supply.IsNotNullOrEmpty())
                    {
                        supplies = assessment.Supply.ToObject<List<Supply>>();
                    }
                    break;
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                case DisciplineTasks.SNVManagementAndEvaluation:
                case DisciplineTasks.SNVObservationAndAssessment:
                case DisciplineTasks.SNVTeachingTraining:
                    var note = patientRepository.GetVisitNote(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                    if (note != null && note.Supply.IsNotNullOrEmpty())
                    {
                        supplies = note.Supply.ToObject<List<Supply>>();
                    }
                    break;
            }
            return supplies;
        }

        public Rap GetRap(Guid patientId, Guid episodeId)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            var rap = billingRepository.GetRap(Current.AgencyId, patientId, episodeId);
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (rap != null && patient != null && episode != null)
            {
                AgencyInsurance insurance = null;
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                if (rap.PrimaryInsuranceId > 0 && rap.PrimaryInsuranceId < 1000)
                {
                    rap.IsMedicareHMO = false;
                }
                else if (rap.PrimaryInsuranceId >= 1000)
                {
                    insurance = agencyRepository.GetInsurance(rap.PrimaryInsuranceId, Current.AgencyId);
                    if (insurance != null)
                    {
                        rap.IsMedicareHMO = (insurance.PayorType ==(int)PayerTypes.MedicareHMO);
                    }
                }
                if ((rap.Status == (int)BillingStatus.ClaimCreated))
                {
                    if (!rap.IsVerified)
                    {
                        
                            rap.FirstName = patient.FirstName;
                            rap.LastName = patient.LastName;
                            rap.MedicareNumber = patient.MedicareNumber;
                            rap.PatientIdNumber = patient.PatientIdNumber;
                            rap.Gender = patient.Gender;
                            rap.DOB = patient.DOB;
                            rap.AddressLine1 = patient.AddressLine1;
                            rap.AddressLine2 = patient.AddressLine2;
                            rap.AddressCity = patient.AddressCity;
                            rap.AddressStateCode = patient.AddressStateCode;
                            rap.AddressZipCode = patient.AddressZipCode;
                            rap.AdmissionSource = patient.AdmissionSource;
                            rap.PatientStatus = patient.Status;
                            rap.UB4PatientStatus =((int)UB4PatientStatus.StillPatient).ToString();
                            if (episode != null)
                            {
                                var managedDate = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                                if (managedDate != null)
                                {
                                    if (rap.IsRapDischage())
                                    {
                                        if (managedDate.DischargedDate.Date > DateTime.MinValue.Date)
                                        {
                                            rap.DischargeDate = managedDate.DischargedDate;
                                        }
                                    }
                                    rap.StartofCareDate = managedDate.StartOfCareDate;
                                }
                                rap.EpisodeStartDate = episode.StartDate;
                                rap.EpisodeEndDate = episode.EndDate;
                            }
                        
                            if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count >= 0)
                            {
                                var physician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                                if (physician != null)
                                {
                                    rap.PhysicianLastName = physician.LastName;
                                    rap.PhysicianFirstName = physician.FirstName;
                                    rap.PhysicianNPI = physician.NPI;
                                }
                                else
                                {
                                    physician = patient.PhysicianContacts.FirstOrDefault();
                                    if (physician != null)
                                    {
                                        rap.PhysicianLastName = physician.LastName;
                                        rap.PhysicianFirstName = physician.FirstName;
                                        rap.PhysicianNPI = physician.NPI;
                                    }
                                }
                            }
                        
                        if (patientRepository.IsFirstBillableVisit(Current.AgencyId, episodeId, patientId,episode.StartDate,episode.EndDate))
                        {
                            var evnt = patientRepository.FirstBillableEvent(Current.AgencyId, episodeId, patientId,episode.StartDate,episode.EndDate);
                            if (evnt != null && evnt.VisitDate.IsValid())
                            {
                                rap.FirstBillableVisitDateFormat = evnt.VisitDate.ToString("MM/dd/yyyy");
                                rap.FirstBillableVisitDate = evnt.VisitDate;
                            }
                        }
                        else
                        {
                            rap.FirstBillableVisitDateFormat = string.Empty;
                        }
                        var assessmentEvent = assessmentService.GetEpisodeAssessmentEvent(episodeId, patientId);
                        if (assessmentEvent != null)
                        {
                            var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                            var assessment = assessmentRepository.Get(assessmentEvent.EventId, patientId, assessmentEvent.EpisodeId, Current.AgencyId);
                            if (assessment != null)
                            {
                                var assessmentQuestions = assessment.ToDictionary();
                                string diagnosis = "<DiagonasisCodes>";
                                diagnosis += "<code1>" + (assessmentQuestions.ContainsKey("M1020ICD9M") ? assessmentQuestions["M1020ICD9M"].Answer : string.Empty) + "</code1>";
                                diagnosis += "<code2>" + (assessmentQuestions.ContainsKey("M1022ICD9M1") ? assessmentQuestions["M1022ICD9M1"].Answer : string.Empty) + "</code2>";
                                diagnosis += "<code3>" + (assessmentQuestions.ContainsKey("M1022ICD9M2") ? assessmentQuestions["M1022ICD9M2"].Answer : string.Empty) + "</code3>";
                                diagnosis += "<code4>" + (assessmentQuestions.ContainsKey("M1022ICD9M3") ? assessmentQuestions["M1022ICD9M3"].Answer : string.Empty) + "</code4>";
                                diagnosis += "<code5>" + (assessmentQuestions.ContainsKey("M1022ICD9M4") ? assessmentQuestions["M1022ICD9M4"].Answer : string.Empty) + "</code5>";
                                diagnosis += "<code6>" + (assessmentQuestions.ContainsKey("M1022ICD9M5") ? assessmentQuestions["M1022ICD9M5"].Answer : string.Empty) + "</code6>";
                                diagnosis += "</DiagonasisCodes>";
                                rap.DiagnosisCode = diagnosis;
                                rap.HippsCode = assessment.HippsCode;
                                rap.ClaimKey = assessment.ClaimKey;
                                rap.AssessmentType = assessmentType;

                                if (assessmentType.IsNotNullOrEmpty())
                                {
                                    if (assessmentType == DisciplineTasks.OASISCStartofCare.ToString() || assessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || assessmentType == DisciplineTasks.OASISCStartofCareOT.ToString())
                                    {
                                        rap.ProspectivePay = Math.Round(0.6 * lookUpRepository.GetProspectivePaymentAmount(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                    }
                                    else
                                    {
                                        rap.ProspectivePay = Math.Round(0.5 * lookUpRepository.GetProspectivePaymentAmount(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                    }
                                }
                            }
                        }
                        if (rap.PrimaryInsuranceId > 0 && rap.PrimaryInsuranceId < 1000)
                        {
                            rap.Ub04Locator81cca = agencyLocation.Ub04Locator81cca;
                        }
                        else if (rap.PrimaryInsuranceId >= 1000)
                        {
                            insurance = agencyRepository.GetInsurance(rap.PrimaryInsuranceId, Current.AgencyId);
                            if (insurance != null)
                            {
                                rap.Ub04Locator81cca = insurance.Ub04Locator81cca;
                            }
                            var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, rap.PrimaryInsuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), episode.StartDate, episode.EndDate);
                            if (autorizations != null && autorizations.Count > 0)
                            {
                                var autorization = autorizations.FirstOrDefault();
                                var autoId = string.Empty;
                                if (autorization != null)
                                {
                                    rap.AuthorizationNumber = autorization.Number1;
                                    rap.AuthorizationNumber2 = autorization.Number2;
                                    rap.AuthorizationNumber3 = autorization.Number3;
                                    autoId = autorization.Id.ToString();
                                }
                                rap.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id.ToString() == autoId }).ToList();
                            }
                            else
                            {
                                rap.Authorizations = new List<SelectListItem>();
                            }

                            if (rap.PrimaryInsuranceId == patient.PrimaryInsurance)
                            {
                                rap.HealthPlanId = patient.PrimaryHealthPlanId;
                                rap.GroupName = patient.PrimaryGroupName;
                                rap.GroupId = patient.PrimaryGroupId;
                            }
                            else if (rap.PrimaryInsuranceId == patient.SecondaryInsurance)
                            {
                                rap.HealthPlanId = patient.SecondaryHealthPlanId;
                                rap.GroupName = patient.SecondaryGroupName;
                                rap.GroupId = patient.SecondaryGroupId;
                            }
                            else if (rap.PrimaryInsuranceId == patient.TertiaryInsurance)
                            {
                                rap.HealthPlanId = patient.TertiaryHealthPlanId;
                                rap.GroupName = patient.TertiaryGroupName;
                                rap.GroupId = patient.TertiaryGroupId;
                            }
                        }
                    }
                    else
                    {
                        if (rap.PrimaryInsuranceId >= 1000)
                        {
                            var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, rap.PrimaryInsuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), episode.StartDate, episode.EndDate);
                            if (autorizations != null && autorizations.Count > 0)
                            {
                                rap.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id == rap.Authorization }).ToList();
                            }
                            else
                            {
                                rap.Authorizations = new List<SelectListItem>();
                            }
                        }
                        rap.FirstBillableVisitDateFormat = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                    }
                }
                else
                {
                    rap.FirstBillableVisitDateFormat = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy");

                    if (rap.PrimaryInsuranceId >= 1000)
                    {
                        var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, rap.PrimaryInsuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), episode.StartDate, episode.EndDate);
                        if (autorizations != null && autorizations.Count > 0)
                        {
                            rap.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id == rap.Authorization }).ToList();
                        }
                        else
                        {
                            rap.Authorizations = new List<SelectListItem>();
                        }
                    }
                }
               // rap.EpisodeStartDate = episode.StartDate;
                rap.EpisodeEndDate = episode.EndDate;
                rap.BranchId = patient.AgencyLocationId;
                rap.AgencyLocationId = patient.AgencyLocationId;
            }
            return rap;
        }

        public Final GetFinalInfo(Guid patientId, Guid episodeId)
        {
            var final = billingRepository.GetFinalOnly(Current.AgencyId, patientId, episodeId);
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            if (final != null && patient != null && episode != null)
            {
                AgencyInsurance insurance = null;
                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                if (final.PrimaryInsuranceId > 0 && final.PrimaryInsuranceId < 1000)
                {
                    final.IsMedicareHMO = false;
                }
                else if (final.PrimaryInsuranceId >= 1000)
                {
                    insurance = agencyRepository.GetInsurance(final.PrimaryInsuranceId, Current.AgencyId);
                    if (insurance != null)
                    {
                        final.IsMedicareHMO = (insurance.PayorType == (int)PayerTypes.MedicareHMO);
                    }
                }

                if ((final.Status == (int)BillingStatus.ClaimCreated))
                {
                    if (!final.IsFinalInfoVerified)
                    {
                        var rap = billingRepository.GetRap(Current.AgencyId, patientId, episodeId);
                        if (rap != null && (rap.IsVerified))
                        {
                            final.FirstName = rap.FirstName;
                            final.LastName = rap.LastName;
                            final.MedicareNumber = rap.MedicareNumber;
                            final.PatientIdNumber = rap.PatientIdNumber;
                            final.DiagnosisCode = rap.DiagnosisCode;
                            final.ConditionCodes = rap.ConditionCodes;
                            final.Gender = rap.Gender;
                            final.DOB = rap.DOB;
                            final.EpisodeStartDate = rap.EpisodeStartDate;
                            final.StartofCareDate = rap.StartofCareDate;
                            final.AddressLine1 = rap.AddressLine1;
                            final.AddressLine2 = rap.AddressLine2;
                            final.AddressCity = rap.AddressCity;
                            final.AddressStateCode = rap.AddressStateCode;
                            final.AddressZipCode = rap.AddressZipCode;
                            final.HippsCode = rap.HippsCode;
                            final.ClaimKey = rap.ClaimKey;
                            final.FirstBillableVisitDateFormat = rap.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                            final.PhysicianLastName = rap.PhysicianLastName;
                            final.PhysicianFirstName = rap.PhysicianFirstName;
                            final.PhysicianNPI = rap.PhysicianNPI;
                            final.AdmissionSource = rap.AdmissionSource;
                            final.AssessmentType = rap.AssessmentType;
                            final.UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty); //rap.UB4PatientStatus;
                            final.HealthPlanId = rap.HealthPlanId;
                            final.Authorization = rap.Authorization;
                            final.AuthorizationNumber = rap.AuthorizationNumber;
                            final.AuthorizationNumber2 = rap.AuthorizationNumber2;
                            final.AuthorizationNumber3= rap.AuthorizationNumber3;
                            
                            if (episode != null)
                            {
                                var managedDate = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                                if (managedDate != null)
                                {
                                    if (final.IsFinalDischage())
                                    {
                                        if (managedDate.DischargedDate.Date > DateTime.MinValue.Date)
                                        {
                                            final.DischargeDate = managedDate.DischargedDate;
                                        }
                                    }
                                }
                                final.EpisodeStartDate = episode.StartDate;
                                final.EpisodeEndDate = episode.EndDate;
                            }
                            if (final.AssessmentType.IsNotNullOrEmpty())
                            {
                                if (final.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || final.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || final.AssessmentType==DisciplineTasks.OASISCStartofCareOT.ToString())
                                {
                                    final.ProspectivePay = Math.Round(lookUpRepository.GetProspectivePaymentAmount(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                }
                                else
                                {
                                    final.ProspectivePay = Math.Round(lookUpRepository.GetProspectivePaymentAmount(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                }
                            }
                            if (final.PrimaryInsuranceId > 0 && final.PrimaryInsuranceId < 1000)
                            {
                                final.Ub04Locator81cca = agencyLocation.Ub04Locator81cca;
                            }
                            else if (final.PrimaryInsuranceId >= 1000)
                            {
                                insurance = agencyRepository.GetInsurance(final.PrimaryInsuranceId, Current.AgencyId);
                                if (insurance != null)
                                {
                                    final.Ub04Locator81cca = insurance.Ub04Locator81cca;
                                }

                                if (final.PrimaryInsuranceId >= 1000)
                                {
                                    var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, final.PrimaryInsuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), final.EpisodeStartDate, final.EpisodeEndDate);
                                    if (autorizations != null && autorizations.Count > 0)
                                    {
                                        final.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id == final.Authorization }).ToList();
                                    }
                                    else
                                    {
                                        final.Authorizations = new List<SelectListItem>();
                                    }
                                }
                            }
                           
                        }
                        else if (rap == null)
                        {
                            
                                final.FirstName = patient.FirstName;
                                final.LastName = patient.LastName;
                                final.MedicareNumber = patient.MedicareNumber;
                                final.PatientIdNumber = patient.PatientIdNumber;
                                final.Gender = patient.Gender;
                                final.DOB = patient.DOB;
                                final.AddressLine2 = patient.AddressLine2;
                                final.AddressCity = patient.AddressCity;
                                final.AddressStateCode = patient.AddressStateCode;
                                final.AddressZipCode = patient.AddressZipCode;
                                final.UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty);
                                
                                if (episode != null)
                                {
                                    var managedDate = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                                    if (managedDate != null)
                                    {
                                        if (final.IsFinalDischage())
                                        {
                                            if (managedDate.DischargedDate > DateTime.MinValue)
                                            {
                                                final.DischargeDate = managedDate.DischargedDate;
                                            }
                                        }
                                        final.StartofCareDate = managedDate.StartOfCareDate;
                                    }
                                    final.EpisodeStartDate = episode.StartDate;
                                    final.EpisodeEndDate = episode.EndDate;
                                    
                                }
                                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count >= 0)
                                {
                                    var physician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                                    if (physician != null)
                                    {
                                        final.PhysicianLastName = physician.LastName;
                                        final.PhysicianFirstName = physician.FirstName;
                                        final.PhysicianNPI = physician.NPI;
                                    }
                                    else
                                    {
                                        physician = patient.PhysicianContacts.FirstOrDefault();
                                        if (physician != null)
                                        {
                                            final.PhysicianLastName = physician.LastName;
                                            final.PhysicianFirstName = physician.FirstName;
                                            final.PhysicianNPI = physician.NPI;
                                        }
                                    }
                                }
                            
                            if (patientRepository.IsFirstBillableVisit(Current.AgencyId, episodeId, patientId,episode.StartDate,episode.EndDate))
                            {
                                var evnt = patientRepository.FirstBillableEvent(Current.AgencyId, episodeId, patientId, episode.StartDate, episode.EndDate);
                                if (evnt != null && evnt.VisitDate.IsValid())
                                {
                                    final.FirstBillableVisitDateFormat = evnt.VisitDate.ToString("MM/dd/yyyy");
                                }
                            }
                            else
                            {
                                final.FirstBillableVisitDateFormat = string.Empty;
                            }
                            var assessmentEvent = assessmentService.GetEpisodeAssessmentEvent(episodeId, patientId);
                            if (assessmentEvent != null)
                            {
                                var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                                var assessment = assessmentRepository.Get(assessmentEvent.EventId, patientId, assessmentEvent.EpisodeId, Current.AgencyId);
                                if (assessment != null)
                                {
                                    var assessmentQuestions = assessment.ToDictionary();
                                    string diagnosis = "<DiagonasisCodes>";
                                    diagnosis += "<code1>" + (assessmentQuestions.ContainsKey("M1020ICD9M") ? assessmentQuestions["M1020ICD9M"].Answer : string.Empty) + "</code1>";
                                    diagnosis += "<code2>" + (assessmentQuestions.ContainsKey("M1022ICD9M1") ? assessmentQuestions["M1022ICD9M1"].Answer : string.Empty) + "</code2>";
                                    diagnosis += "<code3>" + (assessmentQuestions.ContainsKey("M1022ICD9M2") ? assessmentQuestions["M1022ICD9M2"].Answer : string.Empty) + "</code3>";
                                    diagnosis += "<code4>" + (assessmentQuestions.ContainsKey("M1022ICD9M3") ? assessmentQuestions["M1022ICD9M3"].Answer : string.Empty) + "</code4>";
                                    diagnosis += "<code5>" + (assessmentQuestions.ContainsKey("M1022ICD9M4") ? assessmentQuestions["M1022ICD9M4"].Answer : string.Empty) + "</code5>";
                                    diagnosis += "<code6>" + (assessmentQuestions.ContainsKey("M1022ICD9M5") ? assessmentQuestions["M1022ICD9M5"].Answer : string.Empty) + "</code6>";
                                    diagnosis += "</DiagonasisCodes>";
                                    final.DiagnosisCode = diagnosis;
                                    final.HippsCode = assessment.HippsCode;
                                    final.ClaimKey = assessment.ClaimKey;
                                    final.AssessmentType = assessmentType;
                                    if (assessmentType.IsNotNullOrEmpty())
                                    {
                                        if (DisciplineTaskFactory.SOCDisciplineTasks().Contains(assessmentEvent.DisciplineTask))
                                        {
                                            final.ProspectivePay = Math.Round(lookUpRepository.GetProspectivePaymentAmount(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                        }
                                        else
                                        {
                                            final.ProspectivePay = Math.Round(lookUpRepository.GetProspectivePaymentAmount(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                        }
                                    }
                                }
                            }
                            if (final.PrimaryInsuranceId > 0 && final.PrimaryInsuranceId < 1000)
                            {
                                final.Ub04Locator81cca = agencyLocation.Ub04Locator81cca;
                            }
                            else if (final.PrimaryInsuranceId >= 1000)
                            {
                                insurance = agencyRepository.GetInsurance(final.PrimaryInsuranceId, Current.AgencyId);
                                if (insurance != null)
                                {
                                    final.Ub04Locator81cca = insurance.Ub04Locator81cca;
                                }
                                var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, final.PrimaryInsuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), final.EpisodeStartDate, final.EpisodeEndDate);
                                if (autorizations != null && autorizations.Count > 0)
                                {
                                    var autorization = autorizations.FirstOrDefault();
                                    var autoId = string.Empty;
                                    if (autorization != null)
                                    {
                                        final.AuthorizationNumber = autorization.Number1;
                                        final.AuthorizationNumber2 = autorization.Number2;
                                        final.AuthorizationNumber3 = autorization.Number3;
                                        autoId = autorization.Id.ToString();
                                    }
                                    final.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id.ToString() == autoId }).ToList();
                                }
                                else
                                {
                                    final.Authorizations = new List<SelectListItem>();
                                }
                            }
                        }
                        if (final.PrimaryInsuranceId >= 1000)
                        {
                            if (final.PrimaryInsuranceId == patient.PrimaryInsurance)
                            {
                                final.HealthPlanId = patient.PrimaryHealthPlanId;
                                final.GroupName = patient.PrimaryGroupName;
                                final.GroupId = patient.PrimaryGroupId;
                            }
                            else if (final.PrimaryInsuranceId == patient.SecondaryInsurance)
                            {
                                final.HealthPlanId = patient.SecondaryHealthPlanId;
                                final.GroupName = patient.SecondaryGroupName;
                                final.GroupId = patient.SecondaryGroupId;
                            }
                            else if (final.PrimaryInsuranceId == patient.TertiaryInsurance)
                            {
                                final.HealthPlanId = patient.TertiaryHealthPlanId;
                                final.GroupName = patient.TertiaryGroupName;
                                final.GroupId = patient.TertiaryGroupId;
                            }
                        }
                    }
                    else
                    {
                        if (final.PrimaryInsuranceId >= 1000)
                        {
                            var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, final.PrimaryInsuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), final.EpisodeStartDate, final.EpisodeEndDate);
                            if (autorizations != null && autorizations.Count > 0)
                            {
                                final.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id == final.Authorization }).ToList();
                            }
                            else
                            {
                                final.Authorizations = new List<SelectListItem>();
                            }
                        }
                        final.FirstBillableVisitDateFormat = final.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                    }
                }
                else
                {
                    if (final.PrimaryInsuranceId >= 1000)
                    {
                        var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, final.PrimaryInsuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), final.EpisodeStartDate, final.EpisodeEndDate);
                        if (autorizations != null && autorizations.Count > 0)
                        {
                            final.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id == final.Authorization }).ToList();
                        }
                        else
                        {
                            final.Authorizations = new List<SelectListItem>();
                        }
                    }
                    final.FirstBillableVisitDateFormat = final.FirstBillableVisitDate.ToString("MM/dd/yyyy");
                }
                final.BranchId = patient.AgencyLocationId;
                final.AgencyLocationId = patient.AgencyLocationId;
            }
            return final;
        }

        public bool UpdateRapStatus(List<Guid> rapToGenerate, string statusType)
        {
            bool result = false;
            if (rapToGenerate != null)
            {
                rapToGenerate.ForEach(r =>
                {
                    var rap = billingRepository.GetRap(Current.AgencyId, r);
                    if (rap != null)
                    {
                        var patient = patientRepository.GetPatientOnly(rap.PatientId, Current.AgencyId);
                        if (patient != null)
                        {
                            var oldStatus = rap.Status;
                            if (statusType == ButtonAction.Submit.ToString())
                            {
                                if (rap.Status != (int)BillingStatus.ClaimSubmitted)
                                {
                                    rap.Status = (int)BillingStatus.ClaimSubmitted;
                                    rap.ClaimDate = DateTime.Now;
                                }
                                //rap.IsGenerated = true;
                                //rap.IsVerified = true;
                            }
                            else if (statusType == ButtonAction.Cancelled.ToString())
                            {
                                rap.Status = (int)BillingStatus.ClaimCancelledClaim;
                            }
                            else if (statusType == ButtonAction.Rejected.ToString())
                            {
                                rap.Status = (int)BillingStatus.ClaimRejected;
                            }
                            else if (statusType == ButtonAction.Accepted.ToString())
                            {
                                rap.Status = (int)BillingStatus.ClaimAccepted;
                                rap.IsGenerated = true;
                            }
                            else if (statusType == ButtonAction.PaymentPending.ToString())
                            {
                                rap.Status = (int)BillingStatus.ClaimPaymentPending;
                                rap.IsGenerated = true;
                            }
                            else if (statusType == ButtonAction.Error.ToString())
                            {
                                rap.Status = (int)BillingStatus.ClaimWithErrors;
                            }
                            else if (statusType == ButtonAction.Paid.ToString())
                            {
                                rap.Status = (int)BillingStatus.ClaimPaidClaim;
                                // rap.IsGenerated = true;
                            }
                            else if (statusType == ButtonAction.ReOpen.ToString())
                            {
                                rap.Status = (int)BillingStatus.ClaimReOpen;
                                rap.ClaimDate = DateTime.MinValue;
                                rap.IsVerified = false;
                                rap.IsGenerated = false;
                            }
                            var isStatusChange = oldStatus == rap.Status;
                            if (!isStatusChange && BillingStatusFactory.UnProcessed().Contains(oldStatus))
                            {
                                if ((rap.IsVerified && rap.Insurance.IsNullOrEmpty()) || (!rap.IsVerified))
                                {
                                    if (rap.PrimaryInsuranceId > 0)
                                    {
                                        if (rap.PrimaryInsuranceId >= 1000)
                                        {
                                            var insurance = agencyRepository.FindInsurance(Current.AgencyId, rap.PrimaryInsuranceId);
                                            if (insurance != null)
                                            {
                                                rap.Insurance = insurance.ToXml();
                                            }

                                        }
                                        else if (rap.PrimaryInsuranceId < 1000)
                                        {
                                            var insurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, rap.PrimaryInsuranceId);
                                            if (insurance != null)
                                            {
                                                rap.Insurance = insurance.ToXml();
                                            }
                                        }
                                    }
                                }
                            }
                            if (billingRepository.UpdateRapStatus(rap))
                            {
                                var final = billingRepository.GetFinal(Current.AgencyId, rap.PatientId, rap.EpisodeId);
                                if (final != null)
                                {
                                    final.IsRapGenerated = rap.IsGenerated;
                                    billingRepository.UpdateFinalStatus(final);
                                }

                                if (!isStatusChange)
                                {
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPUpdatedWithStatus, ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), rap.Status) ? (" To " + ((BillingStatus)rap.Status).GetDescription()) : string.Empty)));
                                }
                            }
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool UpdateFinalStatus(List<Guid> finalToGenerate, string statusType)
        {
            bool result = false;
            if (finalToGenerate != null)
            {
                finalToGenerate.ForEach(f =>
                {
                    var final = billingRepository.GetFinal(Current.AgencyId, f);
                    if (final != null)
                    {
                        var patient = patientRepository.GetPatientOnly(final.PatientId, Current.AgencyId);
                        if (patient != null)
                        {
                            var oldStatus = final.Status;
                            if (statusType == ButtonAction.Submit.ToString())
                            {
                                if (final.Status != (int)BillingStatus.ClaimSubmitted)
                                {
                                    final.Status = (int)BillingStatus.ClaimSubmitted;
                                    final.ClaimDate = DateTime.Now;
                                }
                                //final.IsGenerated = true;
                                //final.IsFinalInfoVerified = true;
                                //final.IsVisitVerified = true;
                                //final.IsSupplyVerified = true;
                            }
                            else if (statusType == ButtonAction.Cancelled.ToString())
                            {
                                final.Status = (int)BillingStatus.ClaimCancelledClaim;
                            }
                            else if (statusType == ButtonAction.Rejected.ToString())
                            {
                                final.Status = (int)BillingStatus.ClaimRejected;
                            }
                            else if (statusType == ButtonAction.Accepted.ToString())
                            {
                                final.Status = (int)BillingStatus.ClaimAccepted;
                            }
                            else if (statusType == ButtonAction.PaymentPending.ToString())
                            {
                                final.Status = (int)BillingStatus.ClaimPaymentPending;
                            }
                            else if (statusType == ButtonAction.Error.ToString())
                            {
                                final.Status = (int)BillingStatus.ClaimWithErrors;
                            }
                            else if (statusType == ButtonAction.Paid.ToString())
                            {
                                final.Status = (int)BillingStatus.ClaimPaidClaim;
                            }
                            else if (statusType == ButtonAction.ReOpen.ToString())
                            {
                                final.Status = (int)BillingStatus.ClaimReOpen;
                                final.ClaimDate = DateTime.MinValue;
                                final.IsFinalInfoVerified = false;
                                final.IsSupplyVerified = false;
                                final.IsVisitVerified = false;
                                final.IsGenerated = false;
                            }
                            var isStatusChange = oldStatus == final.Status;
                            if (!isStatusChange && BillingStatusFactory.UnProcessed().Contains(oldStatus))
                            {
                                if (final.PrimaryInsuranceId > 0)
                                {
                                    if ((final.IsFinalInfoVerified && final.Insurance.IsNullOrEmpty()) || (!final.IsFinalInfoVerified))
                                    {
                                        if (final.PrimaryInsuranceId >= 1000)
                                        {
                                            var insurance = agencyRepository.FindInsurance(Current.AgencyId, final.PrimaryInsuranceId);
                                            if (insurance != null)
                                            {
                                                final.Insurance = insurance.ToXml();
                                            }
                                        }
                                        else if (final.PrimaryInsuranceId < 1000)
                                        {
                                            var insurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, final.PrimaryInsuranceId);
                                            if (insurance != null)
                                            {
                                                final.Insurance = insurance.ToXml();
                                            }
                                        }
                                    }
                                }
                            }
                            if (billingRepository.UpdateFinalStatus(final))
                            {
                                if (!isStatusChange)
                                {
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalUpdatedWithStatus, ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), final.Status) ? (" To " + ((BillingStatus)final.Status).GetDescription()) : string.Empty)));
                                }
                            }
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool FinalComplete(Guid Id)
        {
            var final = billingRepository.GetFinal(Current.AgencyId, Id);
            bool result = false;
            if (final != null)
            {
                if (billingRepository.UpdateFinalStatus(final))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalSummaryVerified, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public HCFA1500ViewData GetHCFA1500Info(Guid patientId, Guid claimId)
        {
            var hcfa1500ViewData = new HCFA1500ViewData();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                hcfa1500ViewData.Agency = agency;
                hcfa1500ViewData.AgencyLocation = agency.MainLocation;
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    hcfa1500ViewData.PatientMaritalStatus = patient.MaritalStatus;
                    hcfa1500ViewData.PatientTelephoneNum = patient.PhoneHome;
                    var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, claimId);
                    if (managedClaim != null)
                    {
                        var claimData = new ClaimViewData
                        {
                            UB4PatientStatus = managedClaim.UB4PatientStatus,
                            PatientIdNumber = managedClaim.PatientIdNumber,
                            Type = managedClaim.Type,
                            EpisodeStartDate = managedClaim.EpisodeStartDate,
                            EpisodeEndDate = managedClaim.EpisodeEndDate,
                            FirstName = managedClaim.FirstName,
                            LastName = managedClaim.LastName,
                            AddressLine1 = managedClaim.AddressLine1,
                            AddressLine2 = managedClaim.AddressLine2,
                            AddressCity = managedClaim.AddressCity,
                            AddressStateCode = managedClaim.AddressStateCode,
                            AddressZipCode = managedClaim.AddressZipCode,
                            DOB = managedClaim.DOB,
                            Gender = managedClaim.Gender,
                            AdmissionSource = managedClaim.AdmissionSource.IsNotNullOrEmpty() && managedClaim.AdmissionSource.IsInteger() ? managedClaim.AdmissionSource.ToInteger().GetSplitValue() : "9",
                            CBSA = lookUpRepository.CbsaCodeByZip(managedClaim.AddressZipCode),
                            HippsCode = managedClaim.HippsCode,
                            PrimaryInsuranceId = managedClaim.PrimaryInsuranceId,
                            ClaimKey = managedClaim.ClaimKey,
                            DiagnosisCode = managedClaim.DiagnosisCode,
                            PhysicianFirstName = managedClaim.PhysicianFirstName,
                            PhysicianLastName = managedClaim.PhysicianLastName,
                            PhysicianNPI = managedClaim.PhysicianNPI,
                            VerifiedVisit = managedClaim.VerifiedVisits,
                            Supply = managedClaim.Supply,
                            StartofCareDate = managedClaim.StartofCareDate,
                            MedicareNumber = managedClaim.IsuranceIdNumber,
                            SupplyTotal = managedClaim.SupplyTotal,
                            FirstBillableVisitDate = managedClaim.FirstBillableVisitDate,
                            HealthPlanId = managedClaim.HealthPlanId,
                            GroupName = managedClaim.GroupName,
                            GroupId = managedClaim.GroupId,
                            AuthorizationNumber = managedClaim.AuthorizationNumber,
                            AuthorizationNumber2 = managedClaim.AuthorizationNumber2,
                            AuthorizationNumber3 = managedClaim.AuthorizationNumber3,
                            ConditionCodes = managedClaim.ConditionCodes,
                            SupplyCode = managedClaim.SupplyCode,
                            Ub04Locator81cca = managedClaim.Ub04Locator81cca,
                            Status = managedClaim.Status,
                            AgencyLocationId = patient.AgencyLocationId
                        };
                        hcfa1500ViewData.Claim = claimData;
                        var agencyInsurance = new AgencyInsurance();
                        hcfa1500ViewData.Claim.ChargeRates = this.ManagedToCharegRates(managedClaim, out agencyInsurance);
                        if (hcfa1500ViewData.Claim != null)
                        {
                            if (hcfa1500ViewData.Claim.PrimaryInsuranceId >= 1000)
                            {
                                if (agencyInsurance != null)
                                {
                                    if (agencyInsurance.PayorId.IsNotNullOrEmpty() && !(agencyInsurance.PayorId == "0")) hcfa1500ViewData.Claim.PayerId = agencyInsurance.PayorId;
                                    if (agencyInsurance.PayorType == (int)PayerTypes.MedicareHMO) hcfa1500ViewData.Claim.IsHMO = true;
                                    hcfa1500ViewData.Claim.PayorName = agencyInsurance.Name;
                                    hcfa1500ViewData.Claim.PayorType = agencyInsurance.PayorType;
                                    hcfa1500ViewData.Claim.PayorAddressLine1 = agencyInsurance.AddressFirstRow;
                                    hcfa1500ViewData.Claim.PayorAddressLine2 = agencyInsurance.AddressSecondRow;
                                    hcfa1500ViewData.Claim.OtherProviderId = agencyInsurance.OtherProviderId;
                                    hcfa1500ViewData.Claim.ProviderId = agencyInsurance.ProviderId;
                                    hcfa1500ViewData.Claim.ProviderSubscriberId = agencyInsurance.ProviderSubscriberId;
                                }
                                hcfa1500ViewData.Claim.ClaimType = ClaimType.MAN;
                            }
                            else if (hcfa1500ViewData.Claim.PrimaryInsuranceId < 1000 && hcfa1500ViewData.Claim.PrimaryInsuranceId > 0)
                            {
                                hcfa1500ViewData.Claim.IsHMO = false;
                                hcfa1500ViewData.Claim.ClaimType = ClaimType.CMS;
                                if (agencyInsurance != null) hcfa1500ViewData.Claim.PayorName = agencyInsurance.Name;
                            }
                        }
                    }
                }
            }
            return hcfa1500ViewData;
        }

        public UBOFourViewData GetUBOFourInfo(Guid patientId, Guid claimId, string type) {
            var uboFourViewData = new UBOFourViewData();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                uboFourViewData.Agency = agency;
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    uboFourViewData.AgencyLocation = agency.MainLocation;
                    if (type.IsEqual("rap"))
                    {
                        var rap = billingRepository.GetRap(Current.AgencyId, claimId);
                        if (rap != null)
                        {
                            rap.AgencyLocationId = patient.AgencyLocationId;
                            var rapClaimData = new ClaimViewData
                            {
                                UB4PatientStatus = ((int)UB4PatientStatus.StillPatient).ToString(),
                                PatientIdNumber = rap.PatientIdNumber,
                                Type = rap.Type == 1 ? "328" : ((int)BillType.HHPPSRAP).ToString(),
                                EpisodeStartDate = rap.EpisodeStartDate,
                                EpisodeEndDate = rap.EpisodeEndDate,
                                FirstName = rap.FirstName,
                                LastName = rap.LastName,
                                AddressLine1 = rap.AddressLine1,
                                AddressLine2 = rap.AddressLine2,
                                AddressCity = rap.AddressCity,
                                AddressStateCode = rap.AddressStateCode,
                                AddressZipCode = rap.AddressZipCode,
                                DOB = rap.DOB,
                                Gender = rap.Gender,
                                AdmissionSource = rap.AdmissionSource.IsNotNullOrEmpty() && rap.AdmissionSource.IsInteger() ? rap.AdmissionSource.ToInteger().GetSplitValue() : "9",
                                CBSA = lookUpRepository.CbsaCodeByZip(rap.AddressZipCode),
                                HippsCode = rap.HippsCode,
                                PrimaryInsuranceId = rap.PrimaryInsuranceId,
                                ClaimKey = rap.ClaimKey,
                                DiagnosisCode = rap.DiagnosisCode,
                                PhysicianFirstName = rap.PhysicianFirstName,
                                PhysicianLastName = rap.PhysicianLastName,
                                PhysicianNPI = rap.PhysicianNPI,
                                StartofCareDate = rap.StartofCareDate,
                                MedicareNumber = rap.MedicareNumber,
                                FirstBillableVisitDate = rap.FirstBillableVisitDate,
                                ConditionCodes = rap.ConditionCodes,
                                Ub04Locator81cca = rap.Ub04Locator81cca,
                                AgencyLocationId = patient.AgencyLocationId
                            };
                            if (rapClaimData.PrimaryInsuranceId >= 1000)
                            {
                                rapClaimData.HealthPlanId = rap.HealthPlanId;
                                rapClaimData.GroupName = rap.GroupName;
                                rapClaimData.GroupId = rap.GroupId;
                                rapClaimData.AuthorizationNumber = rap.AuthorizationNumber;
                                rapClaimData.AuthorizationNumber2 = rap.AuthorizationNumber2;
                                rapClaimData.AuthorizationNumber3 = rap.AuthorizationNumber3;
                            }
                            uboFourViewData.Claim = rapClaimData;
                            if (uboFourViewData.Claim != null)
                            {
                                var agencyInsurance = this.RapToInsurance(rap);
                               
                                if (uboFourViewData.Claim.PrimaryInsuranceId >= 1000)
                                {
                                    if (agencyInsurance != null)
                                    {
                                        if (agencyInsurance.PayorId.IsNotNullOrEmpty() && !(agencyInsurance.PayorId == "0"))
                                        {
                                            uboFourViewData.Claim.PayerId = agencyInsurance.PayorId;
                                        }
                                        if (agencyInsurance.PayorType == (int)PayerTypes.MedicareHMO)
                                        {
                                            uboFourViewData.Claim.IsHMO = true;
                                        }
                                        uboFourViewData.Claim.PayorName = agencyInsurance.Name;
                                        uboFourViewData.Claim.PayorAddressLine1 = agencyInsurance.AddressFirstRow;
                                        uboFourViewData.Claim.PayorAddressLine2 = agencyInsurance.AddressSecondRow;
                                        uboFourViewData.Claim.OtherProviderId = agencyInsurance.OtherProviderId;
                                        uboFourViewData.Claim.ProviderId = agencyInsurance.ProviderId;
                                        uboFourViewData.Claim.ProviderSubscriberId = agencyInsurance.ProviderSubscriberId;
                                    }
                                    uboFourViewData.Claim.ClaimType = ClaimType.HMO;
                                }
                                else if (uboFourViewData.Claim.PrimaryInsuranceId < 1000 && uboFourViewData.Claim.PrimaryInsuranceId > 0)
                                {
                                    uboFourViewData.Claim.IsHMO = false;
                                    uboFourViewData.Claim.ClaimType = ClaimType.CMS;
                                    if (agencyInsurance != null)
                                    {
                                        uboFourViewData.Claim.PayorName = agencyInsurance.Name;
                                    }
                                }
                            }
                        }
                    }
                    else if (type.IsEqual("final"))
                    {
                        var final = billingRepository.GetFinalOnly(Current.AgencyId, claimId);
                        if (final != null)
                        {
                            final.AgencyLocationId = patient.AgencyLocationId;
                            var finalClaimData = new ClaimViewData
                            {
                                UB4PatientStatus = final.UB4PatientStatus,
                                PatientIdNumber = final.PatientIdNumber,
                                Type = final.Type == 1 ? "328" : ((int)BillType.HHPPSFinal).ToString(),
                                EpisodeStartDate = final.EpisodeStartDate,
                                EpisodeEndDate = final.EpisodeEndDate,
                                FirstName = final.FirstName,
                                LastName = final.LastName,
                                AddressLine1 = final.AddressLine1,
                                AddressLine2 = final.AddressLine2,
                                AddressCity = final.AddressCity,
                                AddressStateCode = final.AddressStateCode,
                                AddressZipCode = final.AddressZipCode,
                                DOB = final.DOB,
                                Gender = final.Gender,
                                AdmissionSource = final.AdmissionSource.IsNotNullOrEmpty() && final.AdmissionSource.IsInteger() ? final.AdmissionSource.ToInteger().GetSplitValue() : "9",
                                CBSA = lookUpRepository.CbsaCodeByZip(final.AddressZipCode),
                                HippsCode = final.HippsCode,
                                PrimaryInsuranceId = final.PrimaryInsuranceId,
                                ClaimKey = final.ClaimKey,
                                DiagnosisCode = final.DiagnosisCode,
                                PhysicianFirstName = final.PhysicianFirstName,
                                PhysicianLastName = final.PhysicianLastName,
                                PhysicianNPI = final.PhysicianNPI,
                                VerifiedVisit = final.VerifiedVisits,
                                StartofCareDate = final.StartofCareDate,
                                MedicareNumber = final.MedicareNumber,
                                SupplyTotal = final.SupplyTotal,
                                FirstBillableVisitDate = final.FirstBillableVisitDate,
                                ConditionCodes = final.ConditionCodes,
                                Ub04Locator81cca = final.Ub04Locator81cca,
                                Status = final.Status,
                                AgencyLocationId = patient.AgencyLocationId,
                                IsSupplyNotBillable = final.IsSupplyNotBillable
                            };
                            if (finalClaimData.PrimaryInsuranceId >= 1000)
                            {
                                finalClaimData.HealthPlanId = final.HealthPlanId;
                                finalClaimData.GroupName = final.GroupName;
                                finalClaimData.GroupId = final.GroupId;
                                finalClaimData.AuthorizationNumber = final.AuthorizationNumber;
                                finalClaimData.AuthorizationNumber2 = final.AuthorizationNumber2;
                                finalClaimData.AuthorizationNumber3 = final.AuthorizationNumber3;
                            }
                            uboFourViewData.Claim = finalClaimData;

                            if (uboFourViewData.Claim != null)
                            {

                                var agencyInsurance = new AgencyInsurance();
                                uboFourViewData.Claim.ChargeRates = this.FinalToCharegRates(final, out agencyInsurance);
                
                                if (uboFourViewData.Claim.PrimaryInsuranceId >= 1000)
                                {
                                    if (agencyInsurance != null)
                                    {
                                        if (agencyInsurance.PayorId.IsNotNullOrEmpty() && !(agencyInsurance.PayorId == "0"))
                                        {
                                            uboFourViewData.Claim.PayerId = agencyInsurance.PayorId;
                                        }
                                        if (agencyInsurance.PayorType == (int)PayerTypes.MedicareHMO)
                                        {
                                            uboFourViewData.Claim.IsHMO = true;
                                        }
                                        uboFourViewData.Claim.PayorName = agencyInsurance.Name;
                                        uboFourViewData.Claim.PayorAddressLine1 = agencyInsurance.AddressFirstRow;
                                        uboFourViewData.Claim.PayorAddressLine2 = agencyInsurance.AddressSecondRow;
                                        uboFourViewData.Claim.OtherProviderId = agencyInsurance.OtherProviderId;
                                        uboFourViewData.Claim.ProviderId = agencyInsurance.ProviderId;
                                        uboFourViewData.Claim.ProviderSubscriberId = agencyInsurance.ProviderSubscriberId;
                                    }
                                    uboFourViewData.Claim.ClaimType = ClaimType.HMO;
                                }
                                else if (uboFourViewData.Claim.PrimaryInsuranceId < 1000 && uboFourViewData.Claim.PrimaryInsuranceId > 0)
                                {
                                    uboFourViewData.Claim.IsHMO = false;
                                    uboFourViewData.Claim.ClaimType = ClaimType.CMS;
                                    if (agencyInsurance != null)
                                    {
                                        uboFourViewData.Claim.PayorName = agencyInsurance.Name;
                                    }
                                }
                                uboFourViewData.Claim.SupplyTotal = this.MedicareSupplyTotal(final);
                            }
                        }
                    }
                }
            }
            return uboFourViewData;
        }

        public ClaimInfoSnapShotViewData GetClaimSnapShotInfo(Guid patientId, Guid claimId, string type)
        {
            var claimInfo = new ClaimInfoSnapShotViewData();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                if (type.IsEqual("Rap"))
                {
                    var rap = billingRepository.GetRap(Current.AgencyId, claimId);
                    if (rap != null)
                    {
                        rap.AgencyLocationId = patient.AgencyLocationId;
                        rap.BranchId = patient.AgencyLocationId;
                        if (BillingStatusFactory.Processed().Contains(rap.Status)|| (rap.Status<=27 && rap.Status>=1))
                        {
                            claimInfo.Id = rap.Id;
                            claimInfo.PatientId = rap.PatientId;
                            claimInfo.PatientName = string.Format("{0} {1}", rap.FirstName, rap.LastName);
                            claimInfo.PatientIdNumber = rap.PatientIdNumber;
                            claimInfo.MedicareNumber = rap.MedicareNumber;
                            claimInfo.Type = "RAP";
                            claimInfo.HIPPS = rap.HippsCode;
                            claimInfo.ClaimKey = rap.ClaimKey;
                            claimInfo.ProspectivePayment = new ProspectivePayment();

                            if (rap.HippsCode.IsNotNullOrEmpty() && rap.HippsCode.Length == 5)
                            {
                                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                var prospectivePayment = lookUpRepository.GetProspectivePayment(rap.HippsCode, rap.EpisodeStartDate, rap.AddressZipCode.IsNotNullOrEmpty() ? rap.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                                if (prospectivePayment != null)
                                {
                                    var claimAmount = rap.AssessmentType == DisciplineTasks.OASISCStartofCare.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || rap.AssessmentType == DisciplineTasks.OASISCStartofCareOT.ToString() ? prospectivePayment.CalculateRapClaimAmount(true) : prospectivePayment.CalculateRapClaimAmount(false);
                                    claimInfo.ProspectivePayment = prospectivePayment;
                                }
                            }
                            if (rap.IsVerified)
                            {
                                claimInfo.Visible = true;
                            }
                        }
                        else
                        {
                            claimInfo.Id = rap.Id;
                            claimInfo.PatientId = rap.PatientId;
                            claimInfo.PatientName = string.Format("{0} {1}", patient.FirstName, patient.LastName);
                            claimInfo.PatientIdNumber = patient.PatientIdNumber;
                            claimInfo.MedicareNumber = patient.MedicareNumber;
                            claimInfo.Type = "RAP";
                            claimInfo.Visible = false;
                        }
                        var agencyInsurance = this.RapToInsurance(rap);
                        if (agencyInsurance != null)
                        {
                            claimInfo.PayorName = agencyInsurance.Name;
                        }
                    }
                }
                else if (type.IsEqual("Final"))
                {
                    var final = billingRepository.GetFinal(Current.AgencyId, claimId);
                    if (final != null)
                    {
                        final.AgencyLocationId = patient.AgencyLocationId;
                        final.BranchId = patient.AgencyLocationId;
                        if ( BillingStatusFactory.Processed().Contains(final.Status) || (final.Status <= 27 && final.Status >= 1))
                        {
                            claimInfo.Id = final.Id;
                            claimInfo.PatientId = final.PatientId;
                            claimInfo.PatientName = string.Format("{0} {1}", final.FirstName, final.LastName);
                            claimInfo.PatientIdNumber = final.PatientIdNumber;
                            claimInfo.MedicareNumber = final.MedicareNumber;
                            claimInfo.Type = "Final";

                            claimInfo.HIPPS = final.HippsCode;
                            claimInfo.ClaimKey = final.ClaimKey;

                            if (final.HippsCode.IsNotNullOrEmpty() && final.HippsCode.Length == 5)
                            {
                                var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                                var prospectivePayment = lookUpRepository.GetProspectivePayment(final.HippsCode, final.EpisodeStartDate, final.AddressZipCode.IsNotNullOrEmpty() ? final.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                                if (prospectivePayment != null)
                                {
                                    prospectivePayment.ClaimAmount = prospectivePayment.TotalProspectiveAmount;
                                    claimInfo.ProspectivePayment = prospectivePayment;
                                    claimInfo.Visible = true;
                                }
                            }
                        }
                        else
                        {
                            claimInfo.Id = final.Id;
                            claimInfo.PatientId = final.PatientId;
                            claimInfo.PatientName = string.Format("{0} {1}", patient.FirstName, patient.LastName);
                            claimInfo.PatientIdNumber = patient.PatientIdNumber;
                            claimInfo.MedicareNumber = patient.MedicareNumber;
                            claimInfo.Type = "Final";
                            claimInfo.Visible = false;
                        }
                        var agencyInsurance = this.FinalToInsurance(final);
                        if (agencyInsurance != null)
                        {
                            claimInfo.PayorName = agencyInsurance.Name;
                        }
                    }
                }
            }
            return claimInfo;
        }

        public ClaimViewData GetClaimViewData(Guid patientId, Guid Id, string type)
        {
            var claimData = new ClaimViewData();
            if (type.IsEqual("Rap"))
            {
                var rap = billingRepository.GetRap(Current.AgencyId, Id);
                if (rap != null)
                {
                    claimData.Id = rap.Id;
                    claimData.PatientId = rap.PatientId;
                    claimData.EpisodeId = rap.EpisodeId;
                    claimData.FirstName = rap.FirstName;
                    claimData.LastName = rap.LastName;
                    claimData.MedicareNumber = rap.MedicareNumber;
                    claimData.PrimaryInsuranceId = rap.PrimaryInsuranceId;
                    claimData.PaymentDate = rap.PaymentDate;
                    claimData.PaymentAmount = rap.Payment;
                    claimData.Type = "RAP";
                    claimData.Created = rap.Created;
                    claimData.Status = rap.Status;
                    claimData.EpisodeStartDate = rap.EpisodeStartDate;
                    claimData.EpisodeEndDate = rap.EpisodeEndDate;
                    claimData.ClaimDate = rap.ClaimDate.IsValid() ? rap.ClaimDate.ToString("MM/dd/yyyy") : rap.EpisodeStartDate.ToString("MM/dd/yyyy");
                    claimData.ClaimAmount = rap.ProspectivePay;
                    claimData.Comment = rap.Comment;
                }
            }
            else if (type.IsEqual("Final"))
            {
                var final = billingRepository.GetFinal(Current.AgencyId, Id);
                if (final != null)
                {
                    claimData.PatientId = final.PatientId;
                    claimData.Id = final.Id;
                    claimData.EpisodeId = final.EpisodeId;
                    claimData.Type = "Final";
                    claimData.FirstName = final.FirstName;
                    claimData.LastName = final.LastName;
                    claimData.MedicareNumber = final.MedicareNumber;
                    claimData.PrimaryInsuranceId = final.PrimaryInsuranceId;
                    claimData.PaymentDate = final.PaymentDate;
                    claimData.Created = final.Created;
                    claimData.Status = final.Status;
                    claimData.EpisodeStartDate = final.EpisodeStartDate;
                    claimData.EpisodeEndDate = final.EpisodeEndDate;
                    claimData.PaymentAmount = final.Payment;
                    claimData.ClaimDate = final.ClaimDate.IsValid() ? final.ClaimDate.ToString("MM/dd/yyyy") : final.EpisodeEndDate.ToString("MM/dd/yyyy");
                    claimData.ClaimAmount = final.ProspectivePay;
                    claimData.Comment = final.Comment;
                }
            }
            return claimData;
        }

        public double GetSupplyReimbursement(char type, int year, out PPSStandard ppsStandardOut)
        {
            var ppsStandard = PPSstandardEngine.Instance.Get(year);
            ppsStandardOut = ppsStandard;
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        return ppsStandard.S;
                    case 'T':
                        return ppsStandard.T;
                    case 'U':
                        return ppsStandard.U;
                    case 'V':
                        return ppsStandard.V;
                    case 'W':
                        return ppsStandard.W;
                    case 'X':
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        public double GetSupplyReimbursement(char type, int year)
        {
            var ppsStandard = PPSstandardEngine.Instance.Get(year);
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        return ppsStandard.S;
                    case 'T':
                        return ppsStandard.T;
                    case 'U':
                        return ppsStandard.U;
                    case 'V':
                        return ppsStandard.V;
                    case 'W':
                        return ppsStandard.W;
                    case 'X':
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        public bool UpdateProccesedClaimStatus(Patient patient, Guid Id, string type, DateTime claimDate, double paymentAmount, DateTime paymentDate, int status, string comment)
        {
            bool result = false;
            if (type.IsEqual("rap"))
            {
                var rap = billingRepository.GetRap(Current.AgencyId, Id);
                if (rap != null)
                {
                    rap.Payment = paymentAmount;
                    rap.PaymentDate = paymentDate;
                    rap.Comment = comment;
                    //rap.PrimaryInsuranceId = primaryInsuranceId;
                    var isStatusChange = rap.Status == status;
                    var oldStatus = rap.Status;
                    if (BillingStatusFactory.UnProcessed().Contains(status))
                    {
                        rap.IsVerified = false;
                        rap.IsGenerated = false;
                    }
                    if (status == (int)BillingStatus.ClaimSubmitted && rap.Status != (int)BillingStatus.ClaimSubmitted)
                    {
                        rap.ClaimDate = DateTime.Now;
                        //if (rap.IsVerified)
                        //{
                        //    rap.IsGenerated = true;
                        //}
                    }
                    rap.Status = status;
                    if (BillingStatusFactory.Processed().Contains(rap.Status))
                    {
                        // rap.IsGenerated = true;
                    }
                    if (!isStatusChange && BillingStatusFactory.UnProcessed().Contains(oldStatus))
                    {
                        if ((rap.IsVerified && rap.Insurance.IsNullOrEmpty()) || (!rap.IsVerified))
                        {
                            if (rap.PrimaryInsuranceId > 0)
                            {
                                if (rap.PrimaryInsuranceId >= 1000)
                                {
                                    var insurance = agencyRepository.FindInsurance(Current.AgencyId, rap.PrimaryInsuranceId);
                                    if (insurance != null)
                                    {
                                        rap.Insurance = insurance.ToXml();
                                    }

                                }
                                else if (rap.PrimaryInsuranceId < 1000)
                                {
                                    var insurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, rap.PrimaryInsuranceId);
                                    if (insurance != null)
                                    {
                                        rap.Insurance = insurance.ToXml();
                                    }
                                }
                            }
                        }
                    }
                    if (billingRepository.UpdateRapStatus(rap))
                    {
                        var final = billingRepository.GetFinal(Current.AgencyId, rap.PatientId, rap.EpisodeId);
                        if (final != null)
                        {
                            final.IsRapGenerated = rap.IsGenerated;
                            billingRepository.UpdateFinalStatus(final);
                        }
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, !isStatusChange ? LogAction.RAPUpdatedWithStatus : LogAction.RAPUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                        result = true;
                    }
                }
            }
            else if (type.IsEqual("Final"))
            {
                var final = billingRepository.GetFinal(Current.AgencyId, Id);
                if (final != null)
                {
                    final.Payment = paymentAmount;
                    final.PaymentDate = paymentDate;
                    final.Comment = comment;
                    // final.PrimaryInsuranceId = primaryInsuranceId;
                    var isStatusChange = final.Status == status;
                    var oldStatus = final.Status;
                    if (BillingStatusFactory.UnProcessed().Contains(status))
                    {
                        final.IsFinalInfoVerified = false;
                        final.IsSupplyVerified = false;
                        final.IsVisitVerified = false;
                        final.IsGenerated = false;
                    }
                    if (status == (int)BillingStatus.ClaimSubmitted && final.Status != (int)BillingStatus.ClaimSubmitted)
                    {
                        final.ClaimDate = DateTime.Now;
                    }
                    if (!isStatusChange && BillingStatusFactory.UnProcessed().Contains(oldStatus))
                    {
                        if (final.PrimaryInsuranceId > 0)
                        {
                            if ((final.IsFinalInfoVerified && final.Insurance.IsNullOrEmpty()) || (!final.IsFinalInfoVerified))
                            {
                                if (final.PrimaryInsuranceId >= 1000)
                                {
                                    var insurance = agencyRepository.FindInsurance(Current.AgencyId, final.PrimaryInsuranceId);
                                    if (insurance != null)
                                    {
                                        final.Insurance = insurance.ToXml();
                                    }

                                }
                                else if (final.PrimaryInsuranceId < 1000)
                                {
                                    var insurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, final.PrimaryInsuranceId);
                                    if (insurance != null)
                                    {
                                        final.Insurance = insurance.ToXml();
                                    }
                                }
                            }
                        }
                    }
                    final.Status = status;
                    if (BillingStatusFactory.Processed().Contains(final.Status))
                    {
                        // final.IsGenerated = true;
                    }
                    if (billingRepository.UpdateFinalStatus(final))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, !isStatusChange ? LogAction.FinalUpdatedWithStatus : LogAction.FinalUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdatePendingClaimStatus(FormCollection formCollection, string[] rapIds, string[] finalIds)
        {
            bool result = false;
            var keys = formCollection.AllKeys;
            if (keys != null && keys.Length > 0)
            {
                if (rapIds != null && rapIds.Length > 0)
                {
                    foreach (var id in rapIds)
                    {
                        var guidId = id.ToGuid();
                        var type = keys.Contains(string.Format("RapType_{0}", id)) && formCollection[string.Format("RapType_{0}", id)].IsNotNullOrEmpty() ? formCollection[string.Format("RapType_{0}", id)].ToString() : string.Empty;
                        var date = keys.Contains(string.Format("RapPaymentDate_{0}", id)) && formCollection[string.Format("RapPaymentDate_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("RapPaymentDate_{0}", id)].IsValidDate() ? formCollection[string.Format("RapPaymentDate_{0}", id)] : string.Empty;
                        var paymentAmount = keys.Contains(string.Format("RapPayment_{0}", id)) && formCollection[string.Format("RapPayment_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("RapPayment_{0}", id)].IsDouble() ? formCollection[string.Format("RapPayment_{0}", id)].ToDouble() : 0;
                        var status = keys.Contains(string.Format("RapStatus_{0}", id)) && formCollection[string.Format("RapStatus_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("RapStatus_{0}", id)].IsInteger() ? formCollection[string.Format("RapStatus_{0}", id)].ToInteger() : 0;
                        if (!guidId.IsEmpty() && type.IsNotNullOrEmpty())
                        {
                            if (type.IsEqual("Rap"))
                            {
                                var rap = billingRepository.GetRap(Current.AgencyId, guidId);
                                if (rap != null)
                                {
                                    var patient = patientRepository.GetPatientOnly(rap.PatientId, Current.AgencyId);
                                    if (patient != null)
                                    {
                                        rap.Payment = paymentAmount;
                                        rap.PaymentDate = date.IsNotNullOrEmpty() ? date.ToDateTime() : rap.PaymentDate;
                                        if (status == (int)BillingStatus.ClaimSubmitted && rap.Status != (int)BillingStatus.ClaimSubmitted)
                                        {
                                            rap.ClaimDate = DateTime.Now;
                                            //if (rap.IsVerified)
                                            //{
                                            //    rap.IsGenerated = true;
                                            //}
                                        }
                                        var isStatusChange = rap.Status == status;
                                        var oldStatus = rap.Status;
                                        if (!isStatusChange && BillingStatusFactory.UnProcessed().Contains(oldStatus))
                                        {
                                            if ((rap.IsVerified && rap.Insurance.IsNullOrEmpty()) || (!rap.IsVerified))
                                            {
                                                if (rap.PrimaryInsuranceId > 0)
                                                {
                                                    if (rap.PrimaryInsuranceId >= 1000)
                                                    {
                                                        var insurance = agencyRepository.FindInsurance(Current.AgencyId, rap.PrimaryInsuranceId);
                                                        if (insurance != null)
                                                        {
                                                            rap.Insurance = insurance.ToXml();
                                                        }
                                                    }
                                                    else if (rap.PrimaryInsuranceId < 1000)
                                                    {
                                                        var insurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, rap.PrimaryInsuranceId);
                                                        if (insurance != null)
                                                        {
                                                            rap.Insurance = insurance.ToXml();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        rap.Status = status;
                                        if (BillingStatusFactory.UnProcessed().Contains(rap.Status))
                                        {
                                            rap.IsVerified = false;
                                            rap.IsGenerated = false;
                                        }
                                        if (BillingStatusFactory.Processed().Contains(rap.Status))
                                        {
                                            //  rap.IsGenerated = true;
                                        }
                                        if (billingRepository.UpdateRapStatus(rap))
                                        {
                                            var final = billingRepository.GetFinal(Current.AgencyId, rap.PatientId, rap.EpisodeId);
                                            if (final != null)
                                            {
                                                final.IsRapGenerated = rap.IsGenerated;
                                                billingRepository.UpdateFinalStatus(final);
                                            }
                                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Final, !isStatusChange ? LogAction.RAPUpdatedWithStatus : LogAction.RAPUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                                            result = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (finalIds != null && finalIds.Length > 0)
                {
                    foreach (var id in finalIds)
                    {
                        var guidId = id.ToGuid();
                        var type = keys.Contains(string.Format("FinalType_{0}", id)) && formCollection[string.Format("FinalType_{0}", id)].IsNotNullOrEmpty() ? formCollection[string.Format("FinalType_{0}", id)].ToString() : string.Empty;
                        var date = keys.Contains(string.Format("FinalPaymentDate_{0}", id)) && formCollection[string.Format("FinalPaymentDate_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("FinalPaymentDate_{0}", id)].IsValidDate() ? formCollection[string.Format("FinalPaymentDate_{0}", id)] : string.Empty;
                        var paymentAmount = keys.Contains(string.Format("FinalPayment_{0}", id)) && formCollection[string.Format("FinalPayment_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("FinalPayment_{0}", id)].IsDouble() ? formCollection[string.Format("FinalPayment_{0}", id)].ToDouble() : 0;
                        var status = keys.Contains(string.Format("FinalStatus_{0}", id)) && formCollection[string.Format("FinalStatus_{0}", id)].IsNotNullOrEmpty() && formCollection[string.Format("FinalStatus_{0}", id)].IsInteger() ? formCollection[string.Format("FinalStatus_{0}", id)].ToInteger() : 0;
                        if (!guidId.IsEmpty() && type.IsNotNullOrEmpty())
                        {
                            if (type.IsEqual("Final"))
                            {
                                var final = billingRepository.GetFinal(Current.AgencyId, guidId);
                                if (final != null)
                                {
                                    var patient = patientRepository.GetPatientOnly(final.PatientId, Current.AgencyId);
                                    if (patient != null)
                                    {
                                        final.Payment = paymentAmount;
                                        final.PaymentDate = date.IsNotNullOrEmpty() ? date.ToDateTime() : final.PaymentDate;
                                        var isStatusChange = final.Status == status;
                                        var oldStatus = final.Status;
                                        if (status == (int)BillingStatus.ClaimSubmitted && final.Status != (int)BillingStatus.ClaimSubmitted)
                                        {
                                            final.ClaimDate = DateTime.Now;
                                        }
                                        if (!isStatusChange && BillingStatusFactory.UnProcessed().Contains(oldStatus))
                                        {
                                            if (final.PrimaryInsuranceId > 0)
                                            {
                                                if ((final.IsFinalInfoVerified && final.Insurance.IsNullOrEmpty()) || (!final.IsFinalInfoVerified))
                                                {
                                                    if (final.PrimaryInsuranceId >= 1000)
                                                    {
                                                        var insurance = agencyRepository.FindInsurance(Current.AgencyId, final.PrimaryInsuranceId);
                                                        if (insurance != null)
                                                        {
                                                            final.Insurance = insurance.ToXml();
                                                        }

                                                    }
                                                    else if (final.PrimaryInsuranceId < 1000)
                                                    {
                                                        var insurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, final.PrimaryInsuranceId);
                                                        if (insurance != null)
                                                        {
                                                            final.Insurance = insurance.ToXml();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        final.Status = status;
                                        if (BillingStatusFactory.UnProcessed().Contains(final.Status))
                                        {
                                            final.IsFinalInfoVerified = false;
                                            final.IsSupplyVerified = false;
                                            final.IsVisitVerified = false;
                                            final.IsGenerated = false;
                                        }
                                        if (BillingStatusFactory.Processed().Contains(final.Status))
                                        {
                                            // final.IsGenerated = true;
                                        }
                                        if (billingRepository.UpdateFinalStatus(final))
                                        {
                                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, !isStatusChange ? LogAction.FinalUpdatedWithStatus : LogAction.FinalUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                                            result = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateRapClaimStatus(Guid Id, DateTime paymentDate, double paymentAmount, int status)
        {
            bool result = false;

            var rap = billingRepository.GetRap(Current.AgencyId, Id);
            if (rap != null)
            {
                 var patient = patientRepository.GetPatientOnly(rap.PatientId, Current.AgencyId);
                 if (patient != null)
                 {
                     rap.Payment = paymentAmount;
                     rap.PaymentDate = paymentDate;
                     if (status == (int)BillingStatus.ClaimSubmitted && rap.Status != (int)BillingStatus.ClaimSubmitted)
                     {
                         rap.ClaimDate = DateTime.Now;
                         //if (rap.IsVerified)
                         //{
                         //    rap.IsGenerated = true;
                         //}
                     }
                     var isStatusChange = rap.Status == status;
                     var oldStatus = rap.Status;
                     if (!isStatusChange && BillingStatusFactory.UnProcessed().Contains(oldStatus))
                     {
                         if ((rap.IsVerified && rap.Insurance.IsNullOrEmpty()) || (!rap.IsVerified))
                         {
                             if (rap.PrimaryInsuranceId > 0)
                             {
                                 if (rap.PrimaryInsuranceId >= 1000)
                                 {
                                     var insurance = agencyRepository.FindInsurance(Current.AgencyId, rap.PrimaryInsuranceId);
                                     if (insurance != null)
                                     {
                                         rap.Insurance = insurance.ToXml();
                                     }
                                 }
                                 else if (rap.PrimaryInsuranceId < 1000)
                                 {
                                     var insurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, rap.PrimaryInsuranceId);
                                     if (insurance != null)
                                     {
                                         rap.Insurance = insurance.ToXml();
                                     }
                                 }
                             }
                         }
                     }
                     rap.Status = status;
                     if (BillingStatusFactory.Processed().Contains(rap.Status))
                     {
                         // rap.IsGenerated = true;
                     }
                     if (BillingStatusFactory.UnProcessed().Contains(rap.Status))
                     {
                         rap.IsVerified = false;
                         rap.IsGenerated = false;
                     }
                     if (billingRepository.UpdateRapStatus(rap))
                     {
                         var final = billingRepository.GetFinal(Current.AgencyId, rap.PatientId, rap.EpisodeId);
                         if (final != null)
                         {
                             final.IsRapGenerated = rap.IsGenerated;
                             billingRepository.UpdateFinalStatus(final);
                         }
                         Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, !isStatusChange ? LogAction.RAPUpdatedWithStatus : LogAction.RAPUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                         result = true;
                     }
                 }
            }
            return result;
        }

        public bool UpdateFinalClaimStatus(Guid Id, DateTime paymentDate, double paymentAmount, int status)
        {
            bool result = false;
            var final = billingRepository.GetFinal(Current.AgencyId, Id);
            if (final != null)
            {
                var patient = patientRepository.GetPatientOnly(final.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    final.Payment = paymentAmount;
                    final.PaymentDate = paymentDate;
                    if (status == (int)BillingStatus.ClaimSubmitted && final.Status != (int)BillingStatus.ClaimSubmitted)
                    {
                        final.ClaimDate = DateTime.Now;
                    }
                    var isStatusChange = final.Status == status;
                    var oldStatus = final.Status;
                    if (!isStatusChange && BillingStatusFactory.UnProcessed().Contains(oldStatus))
                    {
                        if (final.PrimaryInsuranceId > 0)
                        {
                            if ((final.IsFinalInfoVerified && final.Insurance.IsNullOrEmpty()) || (!final.IsFinalInfoVerified))
                            {
                                if (final.PrimaryInsuranceId >= 1000)
                                {
                                    var insurance = agencyRepository.FindInsurance(Current.AgencyId, final.PrimaryInsuranceId);
                                    if (insurance != null)
                                    {
                                        final.Insurance = insurance.ToXml();
                                    }
                                }
                                else if (final.PrimaryInsuranceId < 1000)
                                {
                                    var insurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, final.PrimaryInsuranceId);
                                    if (insurance != null)
                                    {
                                        final.Insurance = insurance.ToXml();
                                    }
                                }
                            }
                        }
                    }
                    final.Status = status;
                    if ( BillingStatusFactory.UnProcessed().Contains(final.Status))
                    {
                        final.IsFinalInfoVerified = false;
                        final.IsSupplyVerified = false;
                        final.IsVisitVerified = false;
                        final.IsGenerated = false;
                    }
                    if (BillingStatusFactory.Processed().Contains(final.Status))
                    {
                        // final.IsGenerated = true;
                    }
                    if (billingRepository.UpdateFinalStatus(final))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, !isStatusChange ? LogAction.FinalUpdatedWithStatus : LogAction.FinalUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(BillingStatus), oldStatus) ? ("From " + ((BillingStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(BillingStatus), status) ? (" To " + ((BillingStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool IsEpisodeHasClaim(Guid episodeId, Guid patientId, string type)
        {
            var claimIds = billingRepository.GetClaimIds(Current.AgencyId, patientId, type);
            if (claimIds != null && claimIds.Count > 0)
            {
                return claimIds.Contains(episodeId);
            }
            return false;
        }

        public IList<ClaimSnapShotViewData> ClaimSnapShots(Guid Id, string type)
        {
            var claims = new List<ClaimSnapShotViewData>();
            if (type.IsEqual("Rap"))
            {
                var rapSnapShots = billingRepository.GetRapSnapShots(Current.AgencyId, Id);
                if (rapSnapShots != null && rapSnapShots.Count > 0)
                {
                    rapSnapShots.ForEach(rapSnapShot =>
               {
                   claims.Add(
                       new ClaimSnapShotViewData
                       {
                           Id = rapSnapShot.Id,
                           BatchId = rapSnapShot.BatchId,
                           PatientId = rapSnapShot.PatientId,
                           EpisodeId = rapSnapShot.EpisodeId,
                           FirstName = rapSnapShot.FirstName,
                           LastName = rapSnapShot.LastName,
                           MedicareNumber = rapSnapShot.MedicareNumber,
                           PaymentDate = rapSnapShot.PaymentDate,
                           PaymentAmount = rapSnapShot.Payment,
                           Type = "Rap",
                           Created = rapSnapShot.Created,
                           Status = rapSnapShot.Status,
                           EpisodeStartDate = rapSnapShot.EpisodeStartDate,
                           EpisodeEndDate = rapSnapShot.EpisodeEndDate,
                           ClaimDate = rapSnapShot.ClaimDate.IsValid() ? rapSnapShot.ClaimDate.ToString("MM/dd/yyyy hh:mm tt") : rapSnapShot.EpisodeStartDate.ToString("MM/dd/yyyy hh:mm tt"),
                           ClaimAmount = rapSnapShot.ProspectivePay
                       });
               });
                }
            }
            if (type.IsEqual("Final"))
            {
                var finalSnapShots = billingRepository.GetFinalSnapShots(Current.AgencyId, Id);
                if (finalSnapShots != null && finalSnapShots.Count > 0)
                {
                    finalSnapShots.ForEach(finalSnapShot =>
               {
                   claims.Add(
                       new ClaimSnapShotViewData
                       {
                           PatientId = finalSnapShot.PatientId,
                           BatchId = finalSnapShot.BatchId,
                           Id = finalSnapShot.Id,
                           EpisodeId = finalSnapShot.EpisodeId,
                           Type = "Final",
                           FirstName = finalSnapShot.FirstName,
                           LastName = finalSnapShot.LastName,
                           MedicareNumber = finalSnapShot.MedicareNumber,
                           PaymentDate = finalSnapShot.PaymentDate,
                           Created = finalSnapShot.Created,
                           Status = finalSnapShot.Status,
                           EpisodeStartDate = finalSnapShot.EpisodeStartDate,
                           EpisodeEndDate = finalSnapShot.EpisodeEndDate,
                           PaymentAmount = finalSnapShot.Payment,
                           ClaimDate = finalSnapShot.ClaimDate.IsValid() ? finalSnapShot.ClaimDate.ToString("MM/dd/yyyy hh:mm tt") : finalSnapShot.EpisodeEndDate.ToString("{MM/dd/yyyy hh:mm tt"),
                           ClaimAmount = finalSnapShot.ProspectivePay
                       });
               });
                }
            }
            return claims;
        }

        public bool UpdateSnapShot(Guid Id, long batchId, double paymentAmount, DateTime paymentDate, int status, string type)
        {
            bool result = false;
            if (type.IsEqual("Rap"))
            {
                var rapSnapShot = billingRepository.GetRapSnapShot(Current.AgencyId, Id, batchId);
                if (rapSnapShot != null)
                {
                    rapSnapShot.Payment = paymentAmount;
                    rapSnapShot.PaymentDate = paymentDate;
                    rapSnapShot.Status = status;
                    result = billingRepository.UpdateRapSnapShots(rapSnapShot);
                    if (result && batchId == billingRepository.GetLastRapSnapShotBatchId(Current.AgencyId, Id))
                    {
                        UpdateRapClaimStatus(Id, paymentDate, paymentAmount, status);
                    }
                }
            }
            else if (type.IsEqual("Final"))
            {
                var finalSnapShot = billingRepository.GetFinalSnapShot(Current.AgencyId, Id, batchId);
                if (finalSnapShot != null)
                {
                    finalSnapShot.Payment = paymentAmount;
                    finalSnapShot.PaymentDate = paymentDate;
                    finalSnapShot.Status = status;
                    result = billingRepository.UpdateFinalSnapShots(finalSnapShot);
                    if (result && batchId == billingRepository.GetLastFinalSnapShotBatchId(Current.AgencyId, Id))
                    {
                        UpdateFinalClaimStatus(Id, paymentDate, paymentAmount, status);
                    }
                }
            }
            return result;
        }

        public ManagedClaim GetManagedClaimInfo(Guid patientId, Guid Id)
        {
            var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            if (managedClaim != null && patient != null)
            {
                if (managedClaim.Status == (int)ManagedClaimStatus.ClaimCreated)
                {
                    if (!managedClaim.IsInfoVerified)
                    {
                        var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                        if (patient != null)
                        {
                            var episodes = patientRepository.GetEpisodesBetween(Current.AgencyId, patientId, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
                            if (episodes != null && episodes.Count > 0)
                            {
                                if (episodes.Count == 1)
                                {
                                    var assessmentEvent = assessmentService.GetEpisodeAssessmentEvent(episodes[0].Id, patientId);
                                    if (assessmentEvent != null)
                                    {
                                        var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                                        var assessment = assessmentRepository.Get(assessmentEvent.EventId, patientId, assessmentEvent.EpisodeId,  Current.AgencyId);
                                        if (assessment != null)
                                        {
                                            var assessmentQuestions = assessment.ToDictionary();
                                            string diagnosis = "<DiagonasisCodes>";
                                            diagnosis += "<code1>" + (assessmentQuestions.ContainsKey("M1020ICD9M") ? assessmentQuestions["M1020ICD9M"].Answer : string.Empty) + "</code1>";
                                            diagnosis += "<code2>" + (assessmentQuestions.ContainsKey("M1022ICD9M1") ? assessmentQuestions["M1022ICD9M1"].Answer : string.Empty) + "</code2>";
                                            diagnosis += "<code3>" + (assessmentQuestions.ContainsKey("M1022ICD9M2") ? assessmentQuestions["M1022ICD9M2"].Answer : string.Empty) + "</code3>";
                                            diagnosis += "<code4>" + (assessmentQuestions.ContainsKey("M1022ICD9M3") ? assessmentQuestions["M1022ICD9M3"].Answer : string.Empty) + "</code4>";
                                            diagnosis += "<code5>" + (assessmentQuestions.ContainsKey("M1022ICD9M4") ? assessmentQuestions["M1022ICD9M4"].Answer : string.Empty) + "</code5>";
                                            diagnosis += "<code6>" + (assessmentQuestions.ContainsKey("M1022ICD9M5") ? assessmentQuestions["M1022ICD9M5"].Answer : string.Empty) + "</code6>";
                                            diagnosis += "</DiagonasisCodes>";
                                            managedClaim.DiagnosisCode = diagnosis;
                                            managedClaim.HippsCode = assessment.HippsCode;
                                            managedClaim.ClaimKey = assessment.ClaimKey;
                                            managedClaim.AssessmentType = assessmentType;
                                            if (assessmentType.IsNotNullOrEmpty())
                                            {
                                                if (assessmentType == DisciplineTasks.OASISCStartofCare.ToString() || assessmentType == DisciplineTasks.OASISCStartofCarePT.ToString() || assessmentType == DisciplineTasks.OASISCStartofCareOT.ToString())
                                                {
                                                    managedClaim.ProspectivePay = Math.Round(lookUpRepository.ProspectivePayAmount(assessment.HippsCode, episodes[0].StartDate, managedClaim.AddressZipCode.IsNotNullOrEmpty() ? managedClaim.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                                }
                                                else
                                                {
                                                    managedClaim.ProspectivePay = Math.Round(lookUpRepository.ProspectivePayAmount(assessment.HippsCode, episodes[0].StartDate, managedClaim.AddressZipCode.IsNotNullOrEmpty() ? managedClaim.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (episodes.Count > 1)
                                {
                                    managedClaim.HasMultipleEpisodes = true;
                                }
                            }
                            managedClaim.FirstName = patient.FirstName;
                            managedClaim.LastName = patient.LastName;
                            managedClaim.IsuranceIdNumber = patient.MedicareNumber;
                            managedClaim.PatientIdNumber = patient.PatientIdNumber;
                            managedClaim.Gender = patient.Gender;
                            managedClaim.DOB = patient.DOB;
                            managedClaim.PatientStatus = patient.Status;
                            managedClaim.UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty);
                            managedClaim.IsHomeHealthServiceIncluded = true;
                            var managedData = patientRepository.GetPatientLatestAdmissionDate(Current.AgencyId, patient.Id, managedClaim.EpisodeStartDate);
                            if (managedData != null)
                            {
                                if (managedClaim.IsManagedClamDischage())
                                {
                                    if (managedData.DischargedDate.Date > DateTime.MinValue.Date)
                                    {
                                        managedClaim.DischargeDate = managedData.DischargedDate;
                                    }
                                    else
                                    {
                                        managedClaim.DischargeDate = patient.DischargeDate;
                                    }
                                }

                                if (managedData.StartOfCareDate.Date > DateTime.MinValue.Date)
                                {
                                    managedClaim.StartofCareDate = managedData.StartOfCareDate;
                                }
                                else
                                {
                                    managedClaim.StartofCareDate = patient.StartofCareDate;
                                }
                            }
                            else
                            {
                                if (managedClaim.IsManagedClamDischage())
                                {
                                    managedClaim.DischargeDate = patient.DischargeDate;
                                }
                                managedClaim.StartofCareDate = patient.StartofCareDate;
                            }
                            managedClaim.AddressLine1 = patient.AddressLine1;
                            managedClaim.AddressLine2 = patient.AddressLine2;
                            managedClaim.AddressCity = patient.AddressCity;
                            managedClaim.AddressStateCode = patient.AddressStateCode;
                            managedClaim.AddressZipCode = patient.AddressZipCode;
                            managedClaim.AdmissionSource = patient.AdmissionSource;
                            if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count >= 0)
                            {
                                var physician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                                if (physician != null)
                                {
                                    managedClaim.PhysicianLastName = physician.LastName;
                                    managedClaim.PhysicianFirstName = physician.FirstName;
                                    managedClaim.PhysicianNPI = physician.NPI;
                                }
                                else
                                {
                                    physician = patient.PhysicianContacts.FirstOrDefault();
                                    if (physician != null)
                                    {
                                        managedClaim.PhysicianLastName = physician.LastName;
                                        managedClaim.PhysicianFirstName = physician.FirstName;
                                        managedClaim.PhysicianNPI = physician.NPI;
                                    }
                                }
                            }

                            if (managedClaim.PrimaryInsuranceId > 0 && managedClaim.PrimaryInsuranceId < 1000)
                            {
                                managedClaim.Ub04Locator81cca = agencyLocation.Ub04Locator81cca;
                            }
                            else if (managedClaim.PrimaryInsuranceId >= 1000)
                            {
                                var insurance = agencyRepository.GetInsurance(managedClaim.PrimaryInsuranceId, Current.AgencyId);
                                if (insurance != null)
                                {
                                    managedClaim.Ub04Locator81cca = insurance.Ub04Locator81cca;

                                }
                                managedClaim.HealthPlanId = patient.PrimaryHealthPlanId;
                                managedClaim.GroupName = patient.PrimaryGroupName;
                                managedClaim.GroupId = patient.PrimaryGroupId;

                            }
                            var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, managedClaim.PrimaryInsuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
                            if (autorizations != null && autorizations.Count > 0)
                            {
                                var autorization = autorizations.FirstOrDefault();
                                var autoId = string.Empty;
                                if (autorization != null)
                                {
                                    managedClaim.AuthorizationNumber = autorization.Number1;
                                    managedClaim.AuthorizationNumber2 = autorization.Number2;
                                    managedClaim.AuthorizationNumber3 = autorization.Number3;
                                    autoId = autorization.Id.ToString();
                                }
                                managedClaim.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id.ToString() == autoId }).ToList();
                            }
                            else
                            {
                                managedClaim.Authorizations = new List<SelectListItem>();
                            }

                        }
                        if (managedClaim.PrimaryInsuranceId > 0 && managedClaim.PrimaryInsuranceId < 1000)
                        {
                            managedClaim .Ub04Locator81cca = agencyLocation.Ub04Locator81cca;
                        }
                        else if (managedClaim.PrimaryInsuranceId >= 1000)
                        {
                           var  insurance = agencyRepository.GetInsurance(managedClaim.PrimaryInsuranceId, Current.AgencyId);
                            if (insurance != null)
                            {
                                managedClaim.Ub04Locator81cca = insurance.Ub04Locator81cca;
                            }
                        }
                    }
                    else
                    {
                        var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, managedClaim.PrimaryInsuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
                        if (autorizations != null && autorizations.Count > 0)
                        {
                            managedClaim.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id == managedClaim.Authorization }).ToList();
                        }
                        else
                        {
                            managedClaim.Authorizations = new List<SelectListItem>();
                        }
                    }
                }
                else
                {
                    var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, managedClaim.PrimaryInsuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
                    if (autorizations != null && autorizations.Count > 0)
                    {
                        managedClaim.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id == managedClaim.Authorization }).ToList();
                    }
                    else
                    {
                        managedClaim.Authorizations = new List<SelectListItem>();
                    }
                }
                managedClaim.AgencyLocationId = patient.AgencyLocationId;
            }
            return managedClaim;
        }

        public List<SelectListItem> GetManagedClaimEpisodes(Guid patientId, Guid managedClaimId)
        {
            var list = new List<SelectListItem>();
            var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, managedClaimId);
            if (managedClaim != null)
            {
                var episodes = patientRepository.GetEpisodesBetween(Current.AgencyId, patientId, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate);
                if (episodes != null)
                {
                    if (episodes.Count > 1)
                    {
                        episodes.ForEach(e =>
                        {
                            list.Add(new SelectListItem
                            {
                                Text = string.Format("{0} - {1}", e.StartDate, e.EndDate),
                                Value = string.Format("{0}_{1}", e.Id, e.PatientId)
                            });
                        });
                    }
                }
            }
            return list;
        }

        public ManagedClaimEpisodeData GetEpisodeAssessmentData(Guid episodeId, Guid patientId)
        {
            var managedClaim = new ManagedClaimEpisodeData();
          
            var assessmentEvent = assessmentService.GetEpisodeAssessmentEvent(episodeId, patientId);
            if (assessmentEvent != null)
            {
                var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), assessmentEvent.DisciplineTask)).ToString();
                var assessment = assessmentRepository.Get(assessmentEvent.EventId, patientId, assessmentEvent.EpisodeId, Current.AgencyId);
                if (assessment != null)
                {
                    var assessmentQuestions = assessment.ToDictionary();
                    managedClaim.DiagnosisCode1 = assessmentQuestions.ContainsKey("M1020ICD9M") ? assessmentQuestions["M1020ICD9M"].Answer : string.Empty;
                    managedClaim.DiagnosisCode2 = assessmentQuestions.ContainsKey("M1022ICD9M1") ? assessmentQuestions["M1022ICD9M1"].Answer : string.Empty;
                    managedClaim.DiagnosisCode3 = assessmentQuestions.ContainsKey("M1022ICD9M2") ? assessmentQuestions["M1022ICD9M2"].Answer : string.Empty;
                    managedClaim.DiagnosisCode4 = assessmentQuestions.ContainsKey("M1022ICD9M3") ? assessmentQuestions["M1022ICD9M3"].Answer : string.Empty;
                    managedClaim.DiagnosisCode5 = assessmentQuestions.ContainsKey("M1022ICD9M4") ? assessmentQuestions["M1022ICD9M4"].Answer : string.Empty;
                    managedClaim.DiagnosisCode6 = assessmentQuestions.ContainsKey("M1022ICD9M5") ? assessmentQuestions["M1022ICD9M5"].Answer : string.Empty;
                    managedClaim.HippsCode = assessment.HippsCode;
                    managedClaim.ClaimKey = assessment.ClaimKey;
                    managedClaim.AssessmentType = assessmentType;
                    if (assessmentType.IsNotNullOrEmpty())
                    {
                        var patient = patientRepository.Get(patientId, Current.AgencyId);
                        var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                        if (DisciplineTaskFactory.SOCDisciplineTasks().Contains(assessmentEvent.DisciplineTask))
                        {
                            managedClaim.ProspectivePay = Math.Round(lookUpRepository.GetProspectivePaymentAmount(assessment.HippsCode, assessmentEvent.StartDate, patient != null && patient.AddressZipCode.IsNotNullOrEmpty() ? patient.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                        }
                        else
                        {
                            managedClaim.ProspectivePay = Math.Round(lookUpRepository.GetProspectivePaymentAmount(assessment.HippsCode, assessmentEvent.StartDate, patient != null && patient.AddressZipCode.IsNotNullOrEmpty() ? patient.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty), 2);
                        }
                    }
                }
            }
            return managedClaim;
        }

        public bool ManagedVisitVerify(Guid Id, Guid patientId, List<Guid> visit)
        {
            bool result = false;

            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                    if (managedClaim != null)
                    {
                        if (managedClaim.PrimaryInsuranceId > 0)
                        {
                            if (!managedClaim.IsInfoVerified || (managedClaim.IsInfoVerified && managedClaim.Insurance.IsNullOrEmpty()))
                            {
                                var agencyInsurance = new AgencyInsurance();
                                if (managedClaim.PrimaryInsuranceId >= 1000)
                                {
                                    agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, managedClaim.PrimaryInsuranceId);
                                }
                                else if (managedClaim.PrimaryInsuranceId > 0 && managedClaim.PrimaryInsuranceId < 1000)
                                {
                                    agencyInsurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, managedClaim.PrimaryInsuranceId);
                                }
                                managedClaim.Insurance = agencyInsurance.ToXml();
                            }
                        }
                        var scheduleEvents = patientRepository.GetScheduledEventsOnly(Current.AgencyId, patientId, managedClaim.EpisodeStartDate, managedClaim.EpisodeEndDate, new int[] { }, new int[] { }, false);
                        if (scheduleEvents != null && scheduleEvents.Count > 0 && visit != null && visit.Count > 0)
                        {
                            var visitList = new List<ScheduleEvent>();
                            var managedClaimVisit = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<ScheduleEvent>>() : new List<ScheduleEvent>();

                            visit.ForEach(v =>
                            {
                                var scheduleVisit = scheduleEvents.FirstOrDefault(s => s.EventId == v);
                                if (scheduleVisit != null)
                                {
                                    scheduleVisit.IsBillable = true;
                                    visitList.Add(scheduleVisit);
                                }
                            });

                            if (managedClaimVisit != null && managedClaimVisit.Count > 0)
                            {
                                managedClaimVisit.ForEach(f =>
                                {
                                    if (scheduleEvents.Exists(e => e.EventId == f.EventId) && !visit.Contains(f.EventId))
                                    {
                                        scheduleEvents.FirstOrDefault(e => e.EventId == f.EventId).IsBillable = false;
                                    }
                                });
                            }

                            managedClaim.IsVisitVerified = true;
                            managedClaim.VerifiedVisits = visitList.ToXml();
                            managedClaim.Modified = DateTime.Now;
                            if (patientRepository.UpdateScheduleEventsForIsBillable(Current.AgencyId, scheduleEvents))
                            {
                                var supplyList = managedClaim.Supply.IsNotNullOrEmpty() ? managedClaim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                                if (visitList != null && visitList.Count > 0)
                                {
                                    visitList.ForEach(v =>
                                    {
                                        var episodeSupply = this.GetSupply(v);
                                        if (episodeSupply != null && episodeSupply.Count > 0)
                                        {
                                            episodeSupply.ForEach(s =>
                                            {
                                                if (!supplyList.Exists(l => l.UniqueIdentifier == s.UniqueIdentifier))
                                                {
                                                    s.IsBillable = true;
                                                    supplyList.Add(s);
                                                }
                                            });
                                        }
                                    });
                                    managedClaim.Supply = supplyList.ToXml();
                                }

                                if (billingRepository.UpdateManagedClaimForVisitVerify(managedClaim))
                                {
                                    result = true;
                                }
                            }
                        }
                        else
                        {
                            managedClaim.IsVisitVerified = true;
                            managedClaim.VerifiedVisits = new List<ScheduleEvent>().ToXml();
                            managedClaim.Modified = DateTime.Now;
                            result = billingRepository.UpdateManagedClaimForVisitVerify(managedClaim);
                        }
                    }
                }
            }
            return result;
        }

        public bool ManagedVisitSupply(Guid Id, Guid patientId)
        {
            bool result = false;
            if (!Id.IsEmpty() && !patientId.IsEmpty())
            {
                var claim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
                if (claim != null)
                {
                    claim.Modified = DateTime.Now;
                    claim.IsSupplyVerified = true;
                    result = billingRepository.UpdateManagedClaimForSupplyVerify(claim);
                }
            }
            return result;
        }

        public UBOFourViewData GetManagedUBOFourInfo(Guid patientId, Guid claimId)
        {
            var uboFourViewData = new UBOFourViewData();
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                uboFourViewData.Agency = agency;
                uboFourViewData.AgencyLocation = agency.MainLocation;
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, claimId);
                    if (managedClaim != null)
                    {
                        var claimData = new ClaimViewData
                        {
                            UB4PatientStatus = managedClaim.UB4PatientStatus,
                            PatientIdNumber = managedClaim.PatientIdNumber,
                            Type = managedClaim.Type,
                            EpisodeStartDate = managedClaim.EpisodeStartDate,
                            EpisodeEndDate = managedClaim.EpisodeEndDate,
                            FirstName = managedClaim.FirstName,
                            LastName = managedClaim.LastName,
                            AddressLine1 = managedClaim.AddressLine1,
                            AddressLine2 = managedClaim.AddressLine2,
                            AddressCity = managedClaim.AddressCity,
                            AddressStateCode = managedClaim.AddressStateCode,
                            AddressZipCode = managedClaim.AddressZipCode,
                            DOB = managedClaim.DOB,
                            Gender = managedClaim.Gender,
                            AdmissionSource = managedClaim.AdmissionSource.IsNotNullOrEmpty() && managedClaim.AdmissionSource.IsInteger() ? managedClaim.AdmissionSource.ToInteger().GetSplitValue() : "9",
                            CBSA = lookUpRepository.CbsaCodeByZip(managedClaim.AddressZipCode),
                            HippsCode = managedClaim.HippsCode,
                            PrimaryInsuranceId = managedClaim.PrimaryInsuranceId,
                            ClaimKey = managedClaim.ClaimKey,
                            DiagnosisCode = managedClaim.DiagnosisCode,
                            PhysicianFirstName = managedClaim.PhysicianFirstName,
                            PhysicianLastName = managedClaim.PhysicianLastName,
                            PhysicianNPI = managedClaim.PhysicianNPI,
                            VerifiedVisit = managedClaim.VerifiedVisits,
                            Supply = managedClaim.Supply,
                            StartofCareDate = managedClaim.StartofCareDate,
                            MedicareNumber = managedClaim.IsuranceIdNumber,
                            SupplyTotal = managedClaim.SupplyTotal,
                            FirstBillableVisitDate = managedClaim.FirstBillableVisitDate,
                            HealthPlanId = managedClaim.HealthPlanId,
                            GroupName = managedClaim.GroupName,
                            GroupId = managedClaim.GroupId,
                            AuthorizationNumber = managedClaim.AuthorizationNumber,
                            AuthorizationNumber2 = managedClaim.AuthorizationNumber2,
                            AuthorizationNumber3 = managedClaim.AuthorizationNumber3,
                            ConditionCodes = managedClaim.ConditionCodes,
                            SupplyCode = managedClaim.SupplyCode,
                            Ub04Locator81cca = managedClaim.Ub04Locator81cca,
                            Status = managedClaim.Status,
                            AgencyLocationId = patient.AgencyLocationId,
                            IsHomeHealthServiceIncluded = managedClaim.IsHomeHealthServiceIncluded
                        };
                        uboFourViewData.Claim = claimData;
                        var agencyInsurance = new AgencyInsurance();
                        uboFourViewData.Claim.ChargeRates = this.ManagedToCharegRates(managedClaim , out agencyInsurance);

                        if (uboFourViewData.Claim != null)
                        {
                            if (uboFourViewData.Claim.PrimaryInsuranceId >= 1000)
                            {
                                if (agencyInsurance != null)
                                {
                                    if (agencyInsurance.PayorId.IsNotNullOrEmpty() && !(agencyInsurance.PayorId == "0"))
                                    {
                                        uboFourViewData.Claim.PayerId = agencyInsurance.PayorId;
                                    }
                                    if (agencyInsurance.PayorType == (int)PayerTypes.MedicareHMO)
                                    {
                                        uboFourViewData.Claim.IsHMO = true;
                                    }
                                    
                                    uboFourViewData.Claim.PayorName = agencyInsurance.Name;
                                    uboFourViewData.Claim.PayorAddressLine1 = agencyInsurance.AddressFirstRow;
                                    uboFourViewData.Claim.PayorAddressLine2 = agencyInsurance.AddressSecondRow;
                                    uboFourViewData.Claim.OtherProviderId = agencyInsurance.OtherProviderId;
                                    uboFourViewData.Claim.ProviderId = agencyInsurance.ProviderId;
                                    uboFourViewData.Claim.ProviderSubscriberId = agencyInsurance.ProviderSubscriberId;
                                }
                                uboFourViewData.Claim.ClaimType = ClaimType.MAN;
                            }
                            else if (uboFourViewData.Claim.PrimaryInsuranceId < 1000 && uboFourViewData.Claim.PrimaryInsuranceId > 0)
                            {
                                uboFourViewData.Claim.IsHMO = false;
                                uboFourViewData.Claim.ClaimType = ClaimType.CMS;
                                if (agencyInsurance != null)
                                {
                                    uboFourViewData.Claim.PayorName = agencyInsurance.Name;
                                }
                            }
                        }
                    }
                }
            }
            return uboFourViewData;
        }

        public bool UpdateProccesedManagedClaimStatus(Patient patient, Guid Id, DateTime claimDate, double paymentAmount, DateTime paymentDate, int status, string comment)
        {
            bool result = false;
            var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patient.Id, Id);
            if (managedClaim != null)
            {
                managedClaim.Payment = paymentAmount;
                managedClaim.PaymentDate = paymentDate;
                managedClaim.Comment = comment;
                if (ManagedClaimStatusFactory.UnProcessed().Contains(status))
                {
                    managedClaim.IsInfoVerified = false;
                    managedClaim.IsSupplyVerified = false;
                    managedClaim.IsVisitVerified = false;
                    managedClaim.IsGenerated = false;
                }
                if (status == (int)ManagedClaimStatus.ClaimSubmitted && managedClaim.Status != (int)ManagedClaimStatus.ClaimSubmitted)
                {
                    managedClaim.ClaimDate = DateTime.Now;
                }
                var oldStatus = managedClaim.Status;
                var isStatusChange = managedClaim.Status == status;
                if (!isStatusChange && ManagedClaimStatusFactory.UnProcessed().Contains(oldStatus))
                {
                    if (managedClaim.PrimaryInsuranceId > 0)
                    {
                        if ((managedClaim.IsInfoVerified && managedClaim.Insurance.IsNullOrEmpty()) || (!managedClaim.IsInfoVerified))
                        {
                            if (managedClaim.PrimaryInsuranceId >= 1000)
                            {
                                var insurance = agencyRepository.FindInsurance(Current.AgencyId, managedClaim.PrimaryInsuranceId);
                                if (insurance != null)
                                {
                                    managedClaim.Insurance = insurance.ToXml();
                                }
                            }
                            else if (managedClaim.PrimaryInsuranceId < 1000)
                            {
                                var insurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, managedClaim.PrimaryInsuranceId);
                                if (insurance != null)
                                {
                                    managedClaim.Insurance = insurance.ToXml();
                                }
                            }
                        }
                    }
                }
                managedClaim.Status = status;
                if (billingRepository.UpdateManagedClaim(managedClaim))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, !isStatusChange ? LogAction.ManagedUpdatedWithStatus : LogAction.ManagedUpdated, !isStatusChange ? ((Enum.IsDefined(typeof(ManagedClaimStatus), oldStatus) ? ("From " + ((ManagedClaimStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ManagedClaimStatus), status) ? (" To " + ((ManagedClaimStatus)status).GetDescription()) : string.Empty)) : string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public ManagedClaimSnapShotViewData GetManagedClaimSnapShotInfo(Guid patientId, Guid claimId)
        {
            var claimInfo = new ManagedClaimSnapShotViewData();
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, claimId);
                if (managedClaim != null)
                {
                    claimInfo.Id = managedClaim.Id;
                    claimInfo.PatientId = patient.Id;
                    if (managedClaim.IsInfoVerified)
                    {
                        claimInfo.PatientName = string.Format("{0} {1}", managedClaim.FirstName, managedClaim.LastName);
                        claimInfo.PatientIdNumber = managedClaim.PatientIdNumber;
                        claimInfo.IsuranceIdNumber = managedClaim.IsuranceIdNumber;
                    }
                    else
                    {
                        claimInfo.PatientName = string.Format("{0} {1}", patient.FirstName, patient.LastName);
                        claimInfo.PatientIdNumber = patient.PatientIdNumber;
                        claimInfo.IsuranceIdNumber = patient.MedicareNumber;
                    }
                    claimInfo.AuthorizationNumber = managedClaim.AuthorizationNumber;
                    claimInfo.HealthPlainId = managedClaim.HealthPlanId;
                    var hhrg = lookUpRepository.GetHHRGByHIPPSCODE(managedClaim.HippsCode);
                    if (hhrg != null)
                    {
                        claimInfo.HHRG = hhrg.HHRG;
                    }
                    claimInfo.HIPPS = managedClaim.HippsCode;
                    claimInfo.ClaimKey = managedClaim.ClaimKey;

                    if (managedClaim.HippsCode.IsNotNullOrEmpty())
                    {
                        var agencyLocation = agencyRepository.GetMainLocation(Current.AgencyId);
                        PPSStandard ppsStandard;
                        claimInfo.SupplyReimbursement = GetSupplyReimbursement(managedClaim.HippsCode[managedClaim.HippsCode.Length - 1], managedClaim.EpisodeStartDate.Year, out ppsStandard);
                        //claimInfo.StandardEpisodeRate = lookUpRepository.ProspectivePayAmount(managedClaim.HippsCode, managedClaim.EpisodeStartDate, managedClaim.AddressZipCode.IsNotNullOrEmpty() ? managedClaim.AddressZipCode : string.Empty, agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                        claimInfo.StandardEpisodeRate = lookUpRepository.GetProspectivePaymentAmount(managedClaim.HippsCode, managedClaim.EpisodeStartDate, managedClaim.AddressZipCode.IsNotNullOrEmpty() ? managedClaim.AddressZipCode : agencyLocation != null ? agencyLocation.AddressZipCode : string.Empty);
                    }
                    claimInfo.ProspectivePay = claimInfo.StandardEpisodeRate;
                    var agencyInsurance = this.ManagedToInsurance(managedClaim);
                    if (agencyInsurance != null)
                    {
                        claimInfo.PayorName = agencyInsurance.Name;
                    }
                    claimInfo.Visible = true;
                }
            }
            return claimInfo;
        }

        public bool ManagedComplete(Guid Id, Guid patientId)
        {
            bool result = false;
            var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, patientId, Id);
            if (managedClaim != null)
            {
                if (billingRepository.UpdateManagedClaim(managedClaim))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedSummaryVerified, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public Bill GetClaimsPrint(Guid branchId, int insuranceId,  string sortType, string claimType)
        {
            var agency = agencyRepository.GetWithBranches(Current.AgencyId);
            Bill claims = null;
            if (agency != null) {
                claims = AllUnProcessedBill(branchId, insuranceId, sortType, claimType);
                if (claims != null) {
                    claims.Insurance = insuranceId;
                    claims.BranchId = branchId;
                    claims.Agency = agency;
                }
            }
            return claims;
        }

        public Final GetFinalPrint(Guid episodeId, Guid patientId)
        {
            var final = GetFinalInfo(patientId, episodeId);
            if (final != null)
            {
                final.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    final.SupplyTotal = this.MedicareSupplyTotal(final);
                    if (final.VerifiedVisits.IsNotNullOrEmpty())
                    {
                        final.AgencyLocationId = patient.AgencyLocationId;
                        var visits = final.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(s => s.VisitDate.IsValid() && s.EventDate.IsValid()).OrderBy(s => s.VisitDate.Date).ThenBy(s => s.EventDate.Date).ToList();
                        if (visits != null && visits.Count > 0)
                        {
                            var agencyInsurance = new AgencyInsurance();
                            var chargeRates = this.FinalToCharegRates(final, out agencyInsurance);
                            final.BillVisitSummaryDatas = this.BillableVisitSummary(patient.AgencyLocationId, visits, final.PrimaryInsuranceId > 0 && final.PrimaryInsuranceId < 1000 ? ClaimType.CMS : ClaimType.HMO, chargeRates, false);
                        }
                    }

                }
            }
            return final;
        }

        public Final GetFinalWithSupplies(Guid episodeId, Guid patientId)
        {
            Final claim = null;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                claim = billingRepository.GetFinal(Current.AgencyId, patientId, episodeId);
                if (claim != null)
                {
                    claim.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        claim.EpisodeStartDate = episode.StartDate;
                        claim.EpisodeEndDate = episode.EndDate;
                    }
                    var patient = patientRepository.Get(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        claim.BranchId = patient.AgencyLocationId;
                    }

                    if (claim.VerifiedVisits.IsNotNullOrEmpty())
                    {
                        var visits = claim.VerifiedVisits.ToObject<List<ScheduleEvent>>();
                        var supplyList = claim.Supply.IsNotNullOrEmpty() ? claim.Supply.ToObject<List<Supply>>() : new List<Supply>();
                        if (visits != null && visits.Count > 0)
                        {
                            visits.ForEach(v =>
                            {
                                var episodeSupply = GetSupply(v);
                                if (episodeSupply != null && episodeSupply.Count > 0)
                                {
                                    episodeSupply.ForEach(s =>
                                    {
                                        if (!supplyList.Exists(l => l.UniqueIdentifier == s.UniqueIdentifier))
                                        {
                                            supplyList.Add(s);
                                        }
                                    });
                                }
                            });
                            claim.Supply = supplyList.ToXml();
                        }
                    }
                }
            }
            return claim;
        }

        public Rap GetRapPrint(Guid episodeId, Guid patientId)
        {
            var rap = GetRap(patientId, episodeId);
            if (rap != null)
            {
                rap.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                rap.AdmissionSourceDisplay = rap.AdmissionSource.GetAdmissionDescription();
            }
            return rap;
        }

        public string GenerateJsonForManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, long claimId, out List<ClaimInfo> claimInfo, AgencyInsurance payerInfo, out List<ManagedClaim> managedClaims, AgencyLocation branch)
        {
            string requestArr = string.Empty;
            claimInfo = new List<ClaimInfo>();
            managedClaims = null;
            try
            {
                var managedClaimLists = billingRepository.GetManagedClaimsToGenerateByIds(Current.AgencyId, managedClaimToGenerate);
                managedClaims = managedClaimLists;
                if (branch != null)
                {
                    var patients = new List<object>();
                    if (managedClaimLists != null && managedClaimLists.Count > 0)
                    {
                        var zipCodes = managedClaimLists.Where(s => s.AddressZipCode.IsNotNullOrEmpty()).Select(s => s.AddressZipCode).Distinct().ToArray();
                        var cbsaCodes = lookUpRepository.CbsaCodesByZip(zipCodes) ?? new List<CBSACode>();
                        foreach (var managedClaim in managedClaimLists)
                        {
                            var claims = new List<object>();

                            var diagnosis = managedClaim.DiagnosisCode.IsNotNullOrEmpty() ? XElement.Parse(managedClaim.DiagnosisCode) : null;

                            var conditionCodes = managedClaim.ConditionCodes.IsNotNullOrEmpty() ? XElement.Parse(managedClaim.ConditionCodes) : null;

                            var visitTotalAmount = 0.00;
                            var visits = managedClaim.VerifiedVisits.IsNotNullOrEmpty() ? managedClaim.VerifiedVisits.ToObject<List<ScheduleEvent>>().Where(f => f.VisitDate.IsValid()).OrderBy(f => f.VisitDate.Date).ToList() : new List<ScheduleEvent>();
                            var visitList = new List<object>();
                            if (visits != null && visits.Count > 0)
                            {
                                var agencyInsurance = new AgencyInsurance();
                                var chargeRates = this.ManagedToCharegRates(managedClaim, out agencyInsurance) ?? new Dictionary<int, ChargeRate>();
                                visits.ForEach(v =>
                                {
                                    var rate = chargeRates.ContainsKey(v.DisciplineTask) ? chargeRates[v.DisciplineTask] : new ChargeRate();
                                    var amount = 0.0;
                                    var addedVisits = this.BillableVisitForANSI(rate, v, out amount, ClaimType.MAN, true);
                                    if (addedVisits != null && addedVisits.Count > 0)
                                    {
                                        visitList.AddRange(addedVisits);
                                        visitTotalAmount += amount;
                                    }
                                });
                            }

                            var supplies = managedClaim.Supply.IsNotNullOrEmpty() ? managedClaim.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && s.Date.IsValidDate() && !s.IsDeprecated).OrderBy(s => s.Date.ToDateTime().Date).ToList() : new List<Supply>();
                            var supplyList = new List<object>();
                            var supplyTotalAmount = 0.00;
                            if (supplies != null && supplies.Count > 0)
                            {
                                supplies.ForEach(v =>
                                {
                                    supplyList.Add(new { date = v.Date, revenue = v.RevenueCode.IsNotNullOrEmpty() ? v.RevenueCode : string.Empty, hcpcs = v.Code.IsNotNullOrEmpty() ? v.Code : string.Empty, units = v.Quantity, amount = v.UnitCost * v.Quantity, modifier = v.Modifier.IsNotNullOrEmpty() ? v.Modifier : string.Empty });
                                    supplyTotalAmount += v.UnitCost * v.Quantity;
                                });
                            }

                            var locator81Lists = managedClaim.Ub04Locator81cca.IsNotNullOrEmpty() ? managedClaim.Ub04Locator81cca.ToObject<List<Locator>>() : new List<Locator>();
                            var locator81 = new List<object>();
                            if (locator81Lists != null && locator81Lists.Count > 0)
                            {
                                locator81Lists.ForEach(l =>
                                {
                                    locator81.Add(new { code1 = l.Code1, code2 = l.Code2, code3 = l.Code3 });
                                });
                            }
                            claimInfo.Add(new ClaimInfo { ClaimId = managedClaim.Id, PatientId = managedClaim.PatientId, EpisodeId = managedClaim.EpisodeId, ClaimType = managedClaim.Type });
                            var finalObj = new
                            {
                                claim_id = managedClaim.Id,
                                claim_type = managedClaim.Type,
                                claim_physician_upin = managedClaim.PhysicianNPI,
                                claim_physician_last_name = managedClaim.PhysicianLastName,
                                claim_physician_first_name = managedClaim.PhysicianFirstName,
                                claim_first_visit_date = managedClaim.FirstBillableVisitDate.ToString("MM/dd/yyyy"),
                                claim_episode_start_date = managedClaim.EpisodeStartDate.ToString("MM/dd/yyyy"),
                                claim_episode_end_date = managedClaim.EpisodeEndDate.ToString("MM/dd/yyyy"),
                                claim_hipps_code = managedClaim.HippsCode,
                                claim_oasis_key = managedClaim.ClaimKey,
                                hmo_plan_id = managedClaim.HealthPlanId,
                                claim_group_name = managedClaim.GroupName,
                                claim_group_Id = managedClaim.GroupId,
                                claim_hmo_auth_key = managedClaim.AuthorizationNumber,
                                claim_hmo_auth_key2 = managedClaim.AuthorizationNumber2,
                                claim_hmo_auth_key3 = managedClaim.AuthorizationNumber3,
                                claim_diagnosis_code1 = (diagnosis != null && diagnosis.Element("code1") != null ? Regex.Replace(diagnosis.Element("code1").Value, @"[.]", "") : ""),
                                claim_diagnosis_code2 = (diagnosis != null && diagnosis.Element("code2") != null ? Regex.Replace(diagnosis.Element("code2").Value, @"[.]", "") : ""),
                                claim_diagnosis_code3 = (diagnosis != null && diagnosis.Element("code3") != null ? Regex.Replace(diagnosis.Element("code3").Value, @"[.]", "") : ""),
                                claim_diagnosis_code4 = (diagnosis != null && diagnosis.Element("code4") != null ? Regex.Replace(diagnosis.Element("code4").Value, @"[.]", "") : ""),
                                claim_diagnosis_code5 = (diagnosis != null && diagnosis.Element("code5") != null ? Regex.Replace(diagnosis.Element("code5").Value, @"[.]", "") : ""),
                                claim_diagnosis_code6 = (diagnosis != null && diagnosis.Element("code6") != null ? Regex.Replace(diagnosis.Element("code6").Value, @"[.]", "") : ""),
                                claim_condition_code18 = (conditionCodes != null && conditionCodes.Element("ConditionCode18") != null ? conditionCodes.Element("ConditionCode18").Value : ""),
                                claim_condition_code19 = (conditionCodes != null && conditionCodes.Element("ConditionCode19") != null ? conditionCodes.Element("ConditionCode19").Value : ""),
                                claim_condition_code20 = (conditionCodes != null && conditionCodes.Element("ConditionCode20") != null ? conditionCodes.Element("ConditionCode20").Value : ""),
                                claim_condition_code21 = (conditionCodes != null && conditionCodes.Element("ConditionCode21") != null ? conditionCodes.Element("ConditionCode21").Value : ""),
                                claim_condition_code22 = (conditionCodes != null && conditionCodes.Element("ConditionCode22") != null ? conditionCodes.Element("ConditionCode22").Value : ""),
                                claim_condition_code23 = (conditionCodes != null && conditionCodes.Element("ConditionCode23") != null ? conditionCodes.Element("ConditionCode23").Value : ""),
                                claim_condition_code24 = (conditionCodes != null && conditionCodes.Element("ConditionCode24") != null ? conditionCodes.Element("ConditionCode24").Value : ""),
                                claim_condition_code25 = (conditionCodes != null && conditionCodes.Element("ConditionCode25") != null ? conditionCodes.Element("ConditionCode25").Value : ""),
                                claim_condition_code26 = (conditionCodes != null && conditionCodes.Element("ConditionCode26") != null ? conditionCodes.Element("ConditionCode26").Value : ""),
                                claim_condition_code27 = (conditionCodes != null && conditionCodes.Element("ConditionCode27") != null ? conditionCodes.Element("ConditionCode27").Value : ""),
                                claim_condition_code28 = (conditionCodes != null && conditionCodes.Element("ConditionCode28") != null ? conditionCodes.Element("ConditionCode28").Value : ""),
                                claim_admission_source_code = managedClaim.AdmissionSource.IsNotNullOrEmpty() && managedClaim.AdmissionSource.IsInteger() ? managedClaim.AdmissionSource.ToInteger().GetSplitValue() : "9",
                                claim_patient_status_code = managedClaim.UB4PatientStatus,
                                claim_dob = managedClaim.IsManagedClamDischage() ? managedClaim.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
                                claim_ub04locator81 = locator81,
                                claim_supply_isBillable = true,
                                claim_supply_value = Math.Round(supplyTotalAmount, 2),
                                claim_supplies = supplyList,
                                claim_total_charge_amount = Math.Round(visitTotalAmount, 2),
                                claim_visits = visitList
                            };
                            claims.Add(finalObj);
                            var cbsa = cbsaCodes.SingleOrDefault(c => c.Zip.IsEqual(managedClaim.AddressZipCode));
                            var patient = new
                            {
                                patient_gender = managedClaim.Gender.Substring(0, 1),
                                patient_record_num = managedClaim.PatientIdNumber,
                                patient_dob = managedClaim.DOB.ToString("MM/dd/yyyy"),
                                patient_doa = managedClaim.StartofCareDate.ToString("MM/dd/yyyy"),
                                patient_dod = managedClaim.IsManagedClamDischage() && managedClaim.DischargeDate.Date > DateTime.MinValue.Date ? managedClaim.DischargeDate.ToString("MM/dd/yyyy") : string.Empty,
                                patient_address = managedClaim.AddressLine1,
                                patient_address2 = managedClaim.AddressLine2,
                                patient_city = managedClaim.AddressCity,
                                patient_state = managedClaim.AddressStateCode,
                                patient_zip = managedClaim.AddressZipCode,
                                patient_cbsa =cbsa!=null ? cbsa.CBSA:string.Empty,
                                patient_last_name = managedClaim.LastName,
                                patient_first_name = managedClaim.FirstName,
                                patient_middle_initial = "",
                                claims_arr = claims
                            };
                            patients.Add(patient);
                        }
                    }
                    var agencyClaim = new
                    {
                        format = "ansi837",
                        submit_type = commandType.ToString(),
                        user_login_name = Current.User.Name,
                        hmo_payer_id = payerInfo.PayorId,
                        hmo_payer_name = payerInfo.Name,
                        hmo_submitter_id = payerInfo.SubmitterId,
                        hmo_provider_id = payerInfo.ProviderId,
                        hmo_other_provider_id = payerInfo.OtherProviderId,
                        hmo_provider_subscriber_id = payerInfo.ProviderSubscriberId,
                        payer_name = payerInfo.Name,
                        insurance_is_axxess_biller = payerInfo.IsAxxessTheBiller,
                        clearing_house_id = payerInfo.ClearingHouseSubmitterId,
                        provider_claim_type = payerInfo.BillType,
                        interchange_receiver_id = payerInfo.InterchangeReceiverId,
                        clearing_house = payerInfo.ClearingHouse,
                        claim_billtype = ClaimType.MAN.ToString(),
                        submitter_name = payerInfo.SubmitterName,
                        submitter_phone = payerInfo.SubmitterPhone,
                        user_agency_name = branch.Name,
                        user_tax_id = branch.TaxId,
                        user_national_provider_id = branch.NationalProviderNumber,
                        user_address_1 = branch.AddressLine1,
                        user_address_2 = branch.AddressLine2,
                        user_city = branch.AddressCity,
                        user_state = branch.AddressStateCode,
                        user_zip = branch.AddressZipCode + "-" + branch.AddressZipCodeFour,
                        user_phone = branch.PhoneWork,
                        user_fax = branch.FaxNumber,
                        user_CBSA_code = lookUpRepository.CbsaCodeByZip(branch.AddressZipCode),
                        ansi_837_id = claimId,
                        patients_arr = patients
                    };
                    var jss = new JavaScriptSerializer();
                    requestArr = jss.Serialize(agencyClaim);
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return requestArr;
        }

        public bool GenerateManaged(List<Guid> managedClaimToGenerate, ClaimCommandType commandType, out ClaimData claimDataOut, out BillExchange billExchange, Guid branchId, int insuranceId)
        {
            var claimData = new ClaimData { AgencyId = Current.AgencyId };
            var claimInfo = new List<ClaimInfo>();
            bool result = false;
            claimDataOut = null;
            billExchange = new BillExchange { isSuccessful = false, Message = "There is a problem creating the claim. Try Again." };
            var agency = agencyRepository.Get(Current.AgencyId);
            if (agency != null)
            {
                var agencyLocation = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (agencyLocation != null)
                {
                    if (!agencyLocation.IsLocationStandAlone)
                    {
                        //agencyLocation.Payor = agency.Payor;
                        //agencyLocation.SubmitterId = agency.SubmitterId;
                        //agencyLocation.SubmitterName = agency.SubmitterName;
                        //agencyLocation.SubmitterPhone = agency.SubmitterPhone;
                        //agencyLocation.SubmitterFax = agency.SubmitterFax;
                        agencyLocation.Name = agency.Name;
                        agencyLocation.TaxId = agency.TaxId;
                        agencyLocation.NationalProviderNumber = agency.NationalProviderNumber;

                    }
                    if (insuranceId >= 1000)
                    {
                        var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
                        if (insurance != null)
                        {
                            claimData.ClaimType = ClaimType.MAN.ToString();
                            var claimId = GetNextClaimId(claimData);
                            claimData.Id = claimId;
                            List<ManagedClaim> managedClaims = null;

                            var requestArr = GenerateJsonForManaged(managedClaimToGenerate, commandType, claimId, out claimInfo, insurance, out managedClaims, agencyLocation);
                            if (requestArr.IsNotNullOrEmpty())
                            {
                                requestArr = requestArr.Replace("&", "U+0026");
                                billExchange = GenerateANSI(requestArr);
                                if (billExchange != null && billExchange.isSuccessful && billExchange.Status == "OK")
                                {
                                    if (billExchange.Result.IsNotNullOrEmpty())
                                    {
                                        claimData.Data = billExchange.Result;
                                        claimData.ClaimType = ClaimType.MAN.ToString();
                                        claimData.BillIdentifers = claimInfo.ToXml<List<ClaimInfo>>();
                                        billingRepository.UpdateClaimData(claimData);
                                        if (commandType == ClaimCommandType.direct)
                                        {
                                            if (managedClaims != null && managedClaims.Count > 0)
                                            {
                                                billingRepository.MarkManagedClaimsAsSubmitted(Current.AgencyId, managedClaims);
                                                managedClaims.ForEach(claim =>
                                                {
                                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedSubmittedElectronically, string.Empty);
                                                });
                                            }
                                        }
                                        else if (commandType == ClaimCommandType.download)
                                        {
                                            if (managedClaims != null && managedClaims.Count > 0)
                                            {
                                                billingRepository.MarkManagedClaimsAsGenerated(Current.AgencyId, managedClaims);
                                                managedClaims.ForEach(claim =>
                                                {
                                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, claim.PatientId, claim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedGenerated, string.Empty);
                                                });
                                            }
                                        }
                                        claimDataOut = claimData;
                                        result = true;
                                    }
                                    else
                                    {
                                        billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                                        claimDataOut = null;
                                        result = false;
                                    }
                                }
                                else
                                {
                                    billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                                    result = false;
                                }
                            }
                            else
                            {
                                billingRepository.DeleteClaimData(Current.AgencyId, claimData.Id);
                                billExchange.Message = "System problem. Try again.";
                                result = false;
                            }

                        }
                        else
                        {
                            billExchange.Message = "Insurance/Payer information is not right.";
                            return false;
                        }
                    }
                    else
                    {
                        billExchange.Message = "Insurance/Payer information is not right.";
                    }
                }
                else
                {
                    billExchange.Message = "Branch information is not found. Try again.";
                }
            }
            else
            {
                billExchange.Message = "Claim Information is not correct. Try again.";
            }
            return result;
        }

        public ManagedBillViewData ManagedBill(Guid branchId, int insuranceId, int status)
        {
            var manageBill = new ManagedBillViewData();
            manageBill.Bills = billingRepository.GetManagedClaims(Current.AgencyId, branchId, insuranceId, status);
            manageBill.BranchId = branchId;
            manageBill.Insurance = insuranceId;
            var insurance = agencyRepository.GetInsurance(insuranceId, Current.AgencyId);
            if (insurance != null)
            {
                manageBill.IsElectronicSubmssion = insurance.IsAxxessTheBiller;
                manageBill.InsuranceName = insurance.Name;
            }
            return manageBill;
        }

        public ManagedBillViewData ManagedClaimToGenerate(List<Guid> managedClaimToGenerate, Guid branchId, int primaryInsurance)
        {
            var isElectronicSubmssion = false;
            var insurance = agencyRepository.GetInsurance(primaryInsurance, Current.AgencyId);
            if (insurance != null)
            {
                isElectronicSubmssion = insurance.IsAxxessTheBiller;
            }
            return new ManagedBillViewData { BranchId = branchId, Insurance = primaryInsurance, Bills = billingRepository.GetManagedClaimByIds(Current.AgencyId, branchId, primaryInsurance, managedClaimToGenerate), IsElectronicSubmssion = isElectronicSubmssion };
        }

        public bool UpdateManagedClaimStatus(List<Guid> managedClaimToGenerate,  string statusType)
        {
            bool result = false;
            if (managedClaimToGenerate != null)
            {
                managedClaimToGenerate.ForEach(id =>
                {
                    var managedClaim = billingRepository.GetManagedClaim(Current.AgencyId, id);
                    if (managedClaim != null)
                    {
                        var patient = patientRepository.GetPatientOnly(managedClaim.PatientId, Current.AgencyId);
                        if (patient != null)
                        {
                            var oldStatus = managedClaim.Status;
                            if (statusType == ButtonAction.Submit.ToString())
                            {
                                if (managedClaim.Status != (int)ManagedClaimStatus.ClaimSubmitted)
                                {
                                    managedClaim.Status = (int)ManagedClaimStatus.ClaimSubmitted;
                                    managedClaim.ClaimDate = DateTime.Now;
                                }
                                managedClaim.IsGenerated = true;
                                managedClaim.IsInfoVerified = true;
                                managedClaim.IsVisitVerified = true;
                                managedClaim.IsSupplyVerified = true;
                            }
                            else if (statusType == ButtonAction.Cancelled.ToString())
                            {
                                managedClaim.Status = (int)ManagedClaimStatus.ClaimCancelledClaim;
                            }
                            else if (statusType == ButtonAction.Rejected.ToString())
                            {
                                managedClaim.Status = (int)ManagedClaimStatus.ClaimRejected;
                            }
                            else if (statusType == ButtonAction.Accepted.ToString())
                            {
                                managedClaim.Status = (int)ManagedClaimStatus.ClaimAccepted;
                            }
                            else if (statusType == ButtonAction.PaymentPending.ToString())
                            {
                                managedClaim.Status = (int)ManagedClaimStatus.ClaimPaymentPending;
                            }
                            else if (statusType == ButtonAction.Error.ToString())
                            {
                                managedClaim.Status = (int)ManagedClaimStatus.ClaimWithErrors;
                            }
                            else if (statusType == ButtonAction.Paid.ToString())
                            {
                                managedClaim.Status = (int)ManagedClaimStatus.ClaimPaidClaim;
                            }
                            else if (statusType == ButtonAction.ReOpen.ToString())
                            {
                                managedClaim.Status = (int)ManagedClaimStatus.ClaimReOpen;
                                managedClaim.ClaimDate = DateTime.MinValue;
                                managedClaim.IsInfoVerified = false;
                                managedClaim.IsSupplyVerified = false;
                                managedClaim.IsVisitVerified = false;
                                managedClaim.IsGenerated = false;
                            }
                            var isStatusChange = oldStatus == managedClaim.Status;
                            if (!isStatusChange && ManagedClaimStatusFactory.UnProcessed().Contains(oldStatus))
                            {
                                if (managedClaim.PrimaryInsuranceId > 0)
                                {
                                    if ((managedClaim.IsInfoVerified && managedClaim.Insurance.IsNullOrEmpty()) || (!managedClaim.IsInfoVerified))
                                    {
                                        if (managedClaim.PrimaryInsuranceId >= 1000)
                                        {
                                            var insurance = agencyRepository.FindInsurance(Current.AgencyId, managedClaim.PrimaryInsuranceId);
                                            if (insurance != null)
                                            {
                                                managedClaim.Insurance = insurance.ToXml();
                                            }

                                        }
                                        else if (managedClaim.PrimaryInsuranceId < 1000)
                                        {
                                            var insurance = this.CMSInsuranceToAgencyInsurance(patient.AgencyLocationId, managedClaim.PrimaryInsuranceId);
                                            if (insurance != null)
                                            {
                                                managedClaim.Insurance = insurance.ToXml();
                                            }
                                        }
                                    }
                                }
                            }
                            if (billingRepository.UpdateManagedClaim(managedClaim))
                            {
                                if (!isStatusChange)
                                {
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedUpdatedWithStatus, ((Enum.IsDefined(typeof(ManagedClaimStatus), oldStatus) ? ("From " + ((ManagedClaimStatus)oldStatus).GetDescription()) : string.Empty) + (Enum.IsDefined(typeof(ManagedClaimStatus), managedClaim.Status) ? (" To " + ((ManagedClaimStatus)managedClaim.Status).GetDescription()) : string.Empty)));
                                }
                            }
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool AddRemittanceUpload(HttpPostedFileBase file)
        {
            var result = false;
            var streamReader = new StreamReader(file.InputStream);
            var remit = new RemitQueue
            {
                Id = Guid.NewGuid(),
                AgencyId = Current.AgencyId,
                Data = streamReader.ReadToEnd(),
                IsUpload = true,
                Status = RemitQueueStatus.queued.ToString()
            };
            if (billingRepository.AddRemitQueue(remit))
            {
                try
                {
                    var encoding = new ASCIIEncoding();
                    var request = (HttpWebRequest)WebRequest.Create(AppSettings.RemittanceSchedulerUrl);
                    request.Method = "GET";
                    var response = (HttpWebResponse)request.GetResponse();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var receiveStream = response.GetResponseStream();
                        var encode = System.Text.Encoding.GetEncoding("utf-8");
                        var readStream = new StreamReader(receiveStream, encode);
                        var strResult = readStream.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {
                    return true;
                }
                result = true;
            }
            return result;
        }

        public List<ClaimInfoDetail> GetSubmittedBatchClaims(int batchId)
        {
            var claimInfos = new List<ClaimInfoDetail>();
            var claimData = billingRepository.GetClaimData(Current.AgencyId, batchId);
            if (claimData != null && claimData.ClaimType.IsNotNullOrEmpty() && claimData.BillIdentifers.IsNotNullOrEmpty())
            {
                var claims = claimData.BillIdentifers.ToObject<List<ClaimInfo>>();
                if (claims != null && claims.Count > 0)
                {
                    if (claimData.ClaimType.ToUpperCase() == ClaimType.MAN.ToString())
                    {
                        var managedClaims = billingRepository.GetManagedClaimInfoDetails(Current.AgencyId, claims.Select(c => c.ClaimId).ToList());
                        if (managedClaims != null && managedClaims.Count > 0)
                        {
                            claimInfos.AddRange(managedClaims);
                        }
                    }
                    else if (claimData.ClaimType.ToUpperCase() == ClaimType.CMS.ToString() || claimData.ClaimType.ToUpperCase() == ClaimType.HMO.ToString())
                    {
                        var raps = claims.Where(c => c.ClaimType == "322" || c.ClaimType.ToUpperCase() == "RAP").ToList();
                        if (raps != null && raps.Count > 0)
                        {
                            var medicareRapClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, raps.Select(c => c.ClaimId).ToList(), "RAP");
                            if (medicareRapClaims != null && medicareRapClaims.Count > 0)
                            {
                                claimInfos.AddRange(medicareRapClaims);
                            }
                        }
                        var finals = claims.Where(c => c.ClaimType == "329" || c.ClaimType.ToUpperCase() == "FINAL").ToList();
                        if (finals != null && finals.Count > 0)
                        {
                            var medicareFinalClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, finals.Select(c => c.ClaimId).ToList(), "Final");
                            if (medicareFinalClaims != null && medicareFinalClaims.Count > 0)
                            {
                                claimInfos.AddRange(medicareFinalClaims);
                            }
                        }
                    }
                }
            }
            return claimInfos;
        }

        public IList<ClaimInfoDetail> BillingBatch(string claimType, DateTime batchDate)
        {
            var claimInfos = new List<ClaimInfoDetail>();
            var batchList = billingRepository.GetClaimDatas(Current.AgencyId, claimType, batchDate);
            if (batchList != null && batchList.Count > 0)
            {
                batchList.ForEach(batch =>
                {
                    if ( batch.BillIdentifers.IsNotNullOrEmpty())
                    {
                        var claims = batch.BillIdentifers.ToObject<List<ClaimInfo>>();
                        if (claims != null && claims.Count > 0)
                        {
                            if (batch.ClaimType.ToUpperCase() == ClaimType.MAN.ToString())
                            {
                                var managedClaims = billingRepository.GetManagedClaimInfoDetails(Current.AgencyId, claims.Select(c => c.ClaimId).ToList());
                                if (managedClaims != null && managedClaims.Count > 0)
                                {
                                    claimInfos.AddRange(managedClaims);
                                }
                            }
                            else if (batch.ClaimType.ToUpperCase() == ClaimType.CMS.ToString() || batch.ClaimType.ToUpperCase() == ClaimType.HMO.ToString())
                            {
                                var raps = claims.Where(c => c.ClaimType == "322" || c.ClaimType.ToUpperCase() == "RAP").ToList();
                                if (raps != null && raps.Count > 0)
                                {
                                    var medicareRapClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, raps.Select(c => c.ClaimId).ToList(), "RAP");
                                    if (medicareRapClaims != null && medicareRapClaims.Count > 0)
                                    {
                                        claimInfos.AddRange(medicareRapClaims);
                                    }
                                }
                                var finals = claims.Where(c => c.ClaimType == "329" || c.ClaimType.ToUpperCase() == "FINAL").ToList();
                                if (finals != null && finals.Count > 0)
                                {
                                    var medicareFinalClaims = billingRepository.GetMedicareClaimInfoDetails(Current.AgencyId, finals.Select(c => c.ClaimId).ToList(), "Final");
                                    if (medicareFinalClaims != null && medicareFinalClaims.Count > 0)
                                    {
                                        claimInfos.AddRange(medicareFinalClaims);
                                    }
                                }
                            }
                        }
                    }
                });
            }
            return claimInfos;
        }

        public InsuranceAuthorizationViewData InsuranceWithAuthorization(Guid patientId, int insuranceId , DateTime startDate , DateTime endDate)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            var viewData = new InsuranceAuthorizationViewData();
            if (patient != null)
            {
                if (patient.PrimaryInsurance == insuranceId)
                {
                    viewData.HealthPlanId = patient.PrimaryHealthPlanId;
                    viewData.GroupName = patient.PrimaryGroupName;
                    viewData.GroupId = patient.PrimaryGroupId;
                }
                else if (patient.SecondaryInsurance == insuranceId)
                {
                    viewData.HealthPlanId = patient.SecondaryHealthPlanId;
                    viewData.GroupName = patient.SecondaryGroupName;
                    viewData.GroupId = patient.SecondaryGroupId;
                }
                else if (patient.TertiaryInsurance == insuranceId)
                {
                    viewData.HealthPlanId = patient.TertiaryHealthPlanId;
                    viewData.GroupName = patient.TertiaryGroupName;
                    viewData.GroupId = patient.TertiaryGroupId;
                }
                var autorizations = patientRepository.GetActiveAuthorizations(Current.AgencyId, patient.Id, insuranceId.ToString(), (AuthorizationStatusTypes.Active).ToString(), startDate, endDate);
                if (autorizations != null && autorizations.Count > 0)
                {
                    var autorization = autorizations.FirstOrDefault();
                    var autoId = string.Empty;
                    if (autorization != null)
                    {
                        viewData.Authorization = autorization;
                        autoId = autorization.Id.ToString();
                    }
                    viewData.Authorizations = autorizations.Select(a => new SelectListItem { Text = string.Format("{0} - {1}", a.StartDate.ToString("MM/dd/yyyy"), a.EndDate.ToString("MM/dd/yyyy")), Value = a.Id.ToString(), Selected = a.Id.ToString() == autoId }).ToList();
                }
                else
                {
                    viewData.Authorizations = new List<SelectListItem>();
                }
            }
            else
            {
                viewData.Authorizations = new List<SelectListItem>();
            }
            return viewData;
        }

        public Dictionary<BillVisitCategory, Dictionary<BillDiscipline, List<BillSchedule>>> BillableVisitsData(Guid branchId, List<ScheduleEvent> visits, ClaimType claimType, Dictionary<int, ChargeRate> chargeRates, bool isLimitApplied)
        {
            var visitDatas = new Dictionary<BillVisitCategory, Dictionary<BillDiscipline, List<BillSchedule>>>();
            if (visits != null && visits.Count > 0)
            {
                foreach (BillVisitCategory billVisitCategory in Enum.GetValues(typeof(BillVisitCategory)))
                {
                    var billCategoryVisits = this.BillCategoryVisits(billVisitCategory, visits);
                    if (billCategoryVisits != null && billCategoryVisits.Count > 0)
                    {
                        var categoryVisitsDictionary = new Dictionary<BillDiscipline, List<BillSchedule>>();
                        foreach (BillDiscipline discipline in Enum.GetValues(typeof(BillDiscipline)))
                        {
                            var disciplineVisits = billCategoryVisits.Where(v => v.Discipline == discipline.ToString()).ToList();
                            if (disciplineVisits != null && disciplineVisits.Count > 0)
                            {
                                var billVisits = new List<BillSchedule>();
                                disciplineVisits.ForEach(v =>
                                {
                                    var rate = chargeRates.ContainsKey(v.DisciplineTask) ? chargeRates[v.DisciplineTask] : null;
                                    if (rate != null)
                                    {
                                        var visitsToBeAdded = this.BillUnitTypeSchedules(rate, v,claimType, isLimitApplied);
                                        if (visitsToBeAdded != null && visitsToBeAdded.Count > 0)
                                        {
                                            billVisits.AddRange(visitsToBeAdded);
                                        }
                                    }
                                    else
                                    {
                                        billVisits.Add(new BillSchedule { EventId = v.EventId, EventDate = v.EventDate.ToString("MM/dd/yyyy"), VisitDate = v.VisitDate.ToString("MM/dd/yyyy"), DisciplineTaskName = v.DisciplineTaskName, StatusName = v.StatusName });
                                    }
                                });
                                if (billVisits != null && billVisits.Count > 0)
                                {
                                    categoryVisitsDictionary.Add(discipline, billVisits);
                                }
                            }
                        }
                        if (categoryVisitsDictionary != null && categoryVisitsDictionary.Count > 0)
                        {
                            visitDatas.Add(billVisitCategory, categoryVisitsDictionary);
                        }
                    }
                }
            }
            return visitDatas;
        }

        public List<BillSchedule> BillableVisitSummary(Guid branchId, List<ScheduleEvent> visits, ClaimType claimType, Dictionary<int, ChargeRate> chargeRates, bool isLimitApplied)
        {
            var billVisits = new List<BillSchedule>();
            if (visits != null && visits.Count > 0)
            {
               visits.ForEach(v =>
               {
                   var rate = chargeRates.ContainsKey(v.DisciplineTask) ? chargeRates[v.DisciplineTask] : null;
                   if (rate != null)
                   {
                       var visitsToBeAdded = this.BillUnitTypeSchedules(rate, v,claimType, isLimitApplied);
                       if (visitsToBeAdded != null && visitsToBeAdded.Count > 0)
                       {
                           billVisits.AddRange(visitsToBeAdded);
                       }
                   }
                   else
                   {
                       billVisits.Add(new BillSchedule { EventId = v.EventId, EventDate = v.EventDate.ToString("MM/dd/yyyy"), VisitDate = v.VisitDate.ToString("MM/dd/yyyy"), DisciplineTaskName = v.DisciplineTaskName, StatusName = v.StatusName });
                   }
               });
            }

            return billVisits;
        }

        public Dictionary<int, ChargeRate> MedicareBillRate(Dictionary<string, CostRate> disciplineRates)
        {
            var chargeRates = new Dictionary<int, ChargeRate>();
            var disciplineTaskArray = lookUpRepository.DisciplineTasks();
            if (disciplineTaskArray != null && disciplineTaskArray.Count > 0)
            {
                if (disciplineRates != null && disciplineRates.Count > 0)
                {

                    foreach (var task in disciplineTaskArray)
                    {
                        var discipline = task.BillDisciplineIdentify();
                        if (discipline.IsNotNullOrEmpty())
                        {
                            if (disciplineRates.ContainsKey(discipline))
                            {
                                var disciplineRate = disciplineRates[discipline];
                                if (disciplineRate != null)
                                {
                                    var rate = new ChargeRate();
                                    SetBillRateValue(rate, discipline);
                                    rate.Id = task.Id;
                                    rate.ChargeType = ((int)BillUnitType.PerVisit).ToString();
                                    rate.Charge = disciplineRate.PerUnit.IsNotNullOrEmpty() && disciplineRate.PerUnit.IsDouble() ? disciplineRate.PerUnit.ToDouble() : 0.0;
                                    chargeRates.Add(task.Id, rate);
                                }
                            }
                            else
                            {
                                var rate = new ChargeRate();
                                SetBillRateValue(rate, discipline);
                                rate.Id = task.Id;
                                rate.ChargeType = ((int)BillUnitType.PerVisit).ToString();
                               // rate.Charge = disciplineRate.PerUnit.IsNotNullOrEmpty() && disciplineRate.PerUnit.IsDouble() ? disciplineRate.PerUnit.ToDouble() : 0.0;
                                chargeRates.Add(task.Id, rate);
                            }
                        }
                    }
                }
                else
                {
                    foreach (var task in disciplineTaskArray)
                    {
                        var discipline = task.BillDisciplineIdentify();
                        if (discipline.IsNotNullOrEmpty())
                        {
                            var rate = new ChargeRate();
                            SetBillRateValue(rate, discipline);
                            rate.Id = task.Id;
                            rate.ChargeType = ((int)BillUnitType.PerVisit).ToString();
                            chargeRates.Add(task.Id, rate);
                        }
                    }
                }

            }
            return chargeRates;
        }

        private List<BillSchedule> BillUnitTypeSchedules(ChargeRate rate, ScheduleEvent visit, ClaimType claimType, bool isLimitApplied)
        {
            var schedules = new List<BillSchedule>();
            if (visit != null && rate != null)
            {
                var schedule = new BillSchedule();
                schedule.EventId = visit.EventId;
                schedule.EventDate = visit.EventDate.ToString("MM/dd/yyyy");
                schedule.VisitDate = visit.VisitDate.ToString("MM/dd/yyyy");
                schedule.DisciplineTaskName = visit.DisciplineTaskName;
                schedule.PereferredName = rate.PreferredDescription.IsNotNullOrEmpty()? rate.PreferredDescription:schedule.DisciplineTaskName;
                schedule.StatusName = visit.StatusName;
                schedule.HCPCSCode = rate.Code;
                schedule.RevenueCode = rate.RevenueCode;
                schedule.Modifier = rate.Modifier;
                schedule.Modifier2 = rate.Modifier2;
                schedule.Modifier3 = rate.Modifier3;
                schedule.Modifier4 = rate.Modifier4;
                if (rate.ChargeType.IsNotNullOrEmpty() && rate.ChargeType.IsInteger())
                {
                    switch (rate.ChargeType.ToInteger())
                    {
                        case (int)BillUnitType.PerVisit:
                            {
                                if (claimType == ClaimType.HMO || claimType == ClaimType.CMS)
                                {
                                    schedule.Unit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                                    schedule.Charge = rate.Charge;
                                    schedules.Add(schedule);
                                }
                                else
                                {
                                    schedule.Unit = rate.Unit;
                                    schedule.Charge = rate.Charge;
                                    schedules.Add(schedule);
                                }
                            }
                            break;
                        case (int)BillUnitType.Hourly:
                            {
                                schedule.Unit = (int)Math.Ceiling((double)visit.MinSpent / 60);
                                schedule.Charge = schedule.Unit * rate.Charge;
                                schedules.Add(schedule);
                            }
                            break;

                        case (int)BillUnitType.Per15Min:
                            {
                                if (isLimitApplied)
                                {
                                    if (rate.IsTimeLimit)
                                    {
                                        var totalMinLimit = rate.TimeLimitHour * 60 + rate.TimeLimitMin;
                                        var totalTimeEventTake = visit.MinSpent;
                                        if (totalTimeEventTake <= totalMinLimit)
                                        {
                                            schedule.Unit = (int)Math.Ceiling((double)totalTimeEventTake / 15);
                                            schedule.Charge = schedule.Unit * rate.Charge;
                                            schedules.Add(schedule);
                                        }
                                        else if (totalTimeEventTake > totalMinLimit)
                                        {
                                            schedule.Unit = (int)Math.Ceiling((double)totalMinLimit / 15);
                                            schedule.Charge = schedule.Unit * rate.Charge;
                                            schedules.Add(schedule);

                                            var addedBillSchedule = new BillSchedule { EventId = visit.EventId, EventDate = visit.EventDate.ToString("MM/dd/yyyy"), VisitDate = visit.VisitDate.ToString("MM/dd/yyyy"), DisciplineTaskName = visit.DisciplineTaskName, PereferredName = rate.SecondDescription.IsNotNullOrEmpty() ? rate.SecondDescription : rate.PreferredDescription, StatusName = visit.StatusName, HCPCSCode = rate.SecondCode.IsNotNullOrEmpty() ? rate.SecondCode : rate.Code, RevenueCode = rate.SecondRevenueCode.IsNotNullOrEmpty() ? rate.SecondRevenueCode : rate.RevenueCode, Modifier = rate.SecondModifier.IsNotNullOrEmpty() ? rate.SecondModifier : string.Empty, Modifier2 = rate.SecondModifier2.IsNotNullOrEmpty() ? rate.SecondModifier2 : string.Empty, Modifier3 = rate.SecondModifier3.IsNotNullOrEmpty() ? rate.SecondModifier3 : string.Empty, Modifier4 = rate.SecondModifier4.IsNotNullOrEmpty() ? rate.SecondModifier4 : string.Empty, IsExtraTime = true };
                                            addedBillSchedule.Unit = (int)Math.Ceiling((double)(totalTimeEventTake - totalMinLimit) / 15);
                                            addedBillSchedule.Charge = addedBillSchedule.Unit * rate.Charge;

                                            schedules.Add(addedBillSchedule);
                                        }
                                    }
                                    else
                                    {
                                        schedule.Unit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                                        schedule.Charge = schedule.Unit * rate.Charge;
                                        schedules.Add(schedule);
                                    }
                                }
                                else
                                {
                                    schedule.Unit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                                    schedule.Charge = schedule.Unit * rate.Charge;
                                    schedules.Add(schedule);
                                }
                            }
                            break;
                    }
                }
            }
            return schedules;
        }

        private List<ScheduleEvent> BillCategoryVisits(BillVisitCategory category, List<ScheduleEvent> visits)
        {
            var categoryVisits = new List<ScheduleEvent>();
            if (visits != null && visits.Count > 0)
            {
                //v.Status == (int)ScheduleStatus.NoteSubmittedWithSignature || v.Status == (int)ScheduleStatus.NoteCompleted || v.Status == (int)ScheduleStatus.NoteReturned || v.Status == (int)ScheduleStatus.NoteReopened || v.Status == (int)ScheduleStatus.OasisCompletedPendingReview || v.Status == (int)ScheduleStatus.OasisCompletedExportReady || v.Status == (int)ScheduleStatus.OasisExported || v.Status == (int)ScheduleStatus.OasisReturnedForClinicianReview || v.Status == (int)ScheduleStatus.OasisReopened || v.Status == (int)ScheduleStatus.EvalReturnedWPhysicianSignature || v.Status == (int)ScheduleStatus.EvalSentToPhysician || v.Status == (int)ScheduleStatus.EvalSentToPhysicianElectronically || v.Status == (int)ScheduleStatus.EvalToBeSentToPhysician
                switch (category)
                {
                    case BillVisitCategory.Billable:
                        categoryVisits = visits.Where(v => v.IsBillable && (ScheduleStatusFatory.BillStatus().Contains(v.Status)) && !v.IsMissedVisit).ToList();
                        break;
                    case BillVisitCategory.NonBillable:
                        categoryVisits = visits.Where(v => !v.IsBillable && (ScheduleStatusFatory.BillStatus().Contains(v.Status)) && !v.IsMissedVisit).ToList();
                        break;
                    case BillVisitCategory.MissedVisit:
                        categoryVisits = visits.Where(v => v.IsMissedVisit).ToList();
                        break;
                }
            }
            return categoryVisits;
        }

        private List<object> BillableVisitForANSI(ChargeRate rate, ScheduleEvent visit, out double totalCharge, ClaimType claimType, bool isLimitApplied)
        {
            totalCharge = 0;
            var schedules = new List<object>();
            if (visit != null && rate != null)
            {
                var discipline = visit.GIdentify();
                if ( rate.ChargeType.IsNotNullOrEmpty() && rate.ChargeType.IsInteger())
                {
                    switch (rate.ChargeType.ToInteger())
                    {
                        case (int)BillUnitType.PerVisit:
                            {
                                schedules.Add(new { date = visit.VisitDate.ToString("MM/dd/yyyy"), type = discipline, revenue = rate.RevenueCode, hcpcs = rate.Code, units = (claimType == ClaimType.HMO || claimType == ClaimType.CMS) ? (int)Math.Ceiling((double)visit.MinSpent / 15) : rate.Unit, amount = rate.Charge, modifier1 = rate.Modifier.IsNotNullOrEmpty() ? rate.Modifier : string.Empty, modifier2 = rate.Modifier2.IsNotNullOrEmpty() ? rate.Modifier2 : string.Empty, modifier3 = rate.Modifier3.IsNotNullOrEmpty() ? rate.Modifier3 : string.Empty, modifier4 = rate.Modifier4.IsNotNullOrEmpty() ? rate.Modifier4 : string.Empty });
                                totalCharge +=  rate.Charge;
                            }
                            break;
                        case (int)BillUnitType.Hourly:
                            {
                                var unit = (int)Math.Ceiling((double)visit.MinSpent / 60);
                                schedules.Add(new { date = visit.VisitDate.ToString("MM/dd/yyyy"), type = discipline, revenue = rate.RevenueCode, hcpcs = rate.Code, units = unit, amount = unit * rate.Charge, modifier = rate.Modifier.IsNotNullOrEmpty() ? rate.Modifier : string.Empty, modifier2 = rate.Modifier2.IsNotNullOrEmpty() ? rate.Modifier2 : string.Empty, modifier3 = rate.Modifier3.IsNotNullOrEmpty() ? rate.Modifier3 : string.Empty, modifier4 = rate.Modifier4.IsNotNullOrEmpty() ? rate.Modifier4 : string.Empty });
                                totalCharge += unit * rate.Charge;
                            }
                            break;

                        case (int)BillUnitType.Per15Min:
                            {
                                if (isLimitApplied)
                                {
                                    if (rate.IsTimeLimit)
                                    {
                                        var totalMinLimit = rate.TimeLimitHour * 60 + rate.TimeLimitMin;
                                        var totalTimeEventTake = visit.MinSpent;
                                        if (totalTimeEventTake <= totalMinLimit)
                                        {
                                            var unit = (int)Math.Ceiling((double)totalTimeEventTake / 15);
                                            schedules.Add(new { date = visit.VisitDate.ToString("MM/dd/yyyy"), type = discipline, revenue = rate.RevenueCode, hcpcs = rate.Code, units = unit, amount = unit * rate.Charge, modifier = rate.Modifier.IsNotNullOrEmpty() ? rate.Modifier : string.Empty, modifier2 = rate.Modifier2.IsNotNullOrEmpty() ? rate.Modifier2 : string.Empty, modifier3 = rate.Modifier3.IsNotNullOrEmpty() ? rate.Modifier3 : string.Empty, modifier4 = rate.Modifier4.IsNotNullOrEmpty() ? rate.Modifier4 : string.Empty });
                                            totalCharge += unit * rate.Charge;

                                        }
                                        else if (totalTimeEventTake > totalMinLimit)
                                        {
                                            var unit = (int)Math.Ceiling((double)totalMinLimit / 15);
                                            schedules.Add(new { date = visit.VisitDate.ToString("MM/dd/yyyy"), type = discipline, revenue = rate.RevenueCode, hcpcs = rate.Code, units = unit, amount = unit * rate.Charge, modifier = rate.Modifier.IsNotNullOrEmpty() ? rate.Modifier : string.Empty, modifier2 = rate.Modifier2.IsNotNullOrEmpty() ? rate.Modifier2 : string.Empty, modifier3 = rate.Modifier3.IsNotNullOrEmpty() ? rate.Modifier3 : string.Empty, modifier4 = rate.Modifier4.IsNotNullOrEmpty() ? rate.Modifier4 : string.Empty });
                                            totalCharge += unit * rate.Charge;

                                            var unitAdded = (int)Math.Ceiling((double)(totalTimeEventTake - totalMinLimit) / 15);
                                            schedules.Add(new { date = visit.VisitDate.ToString("MM/dd/yyyy"), type = discipline, revenue = rate.SecondRevenueCode.IsNotNullOrEmpty() ? rate.SecondRevenueCode : rate.RevenueCode, hcpcs = rate.SecondCode.IsNotNullOrEmpty() ? rate.SecondCode : rate.Code, units = unit, amount = unitAdded * rate.Charge, modifier = rate.SecondModifier.IsNotNullOrEmpty() ? rate.SecondModifier : string.Empty, modifier2 = rate.SecondModifier2.IsNotNullOrEmpty() ? rate.SecondModifier2 : string.Empty, modifier3 = rate.SecondModifier3.IsNotNullOrEmpty() ? rate.SecondModifier3 : string.Empty, modifier4 = rate.SecondModifier4.IsNotNullOrEmpty() ? rate.SecondModifier4 : string.Empty });
                                            totalCharge += unitAdded * rate.Charge;
                                        }
                                    }
                                    else
                                    {
                                        var unit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                                        schedules.Add(new { date = visit.VisitDate.ToString("MM/dd/yyyy"), type = discipline, revenue = rate.RevenueCode, hcpcs = rate.Code, units = unit, amount = unit * rate.Charge, modifier = rate.Modifier.IsNotNullOrEmpty() ? rate.Modifier : string.Empty, modifier2 = rate.Modifier2.IsNotNullOrEmpty() ? rate.Modifier2 : string.Empty, modifier3 = rate.Modifier3.IsNotNullOrEmpty() ? rate.Modifier3 : string.Empty, modifier4 = rate.Modifier4.IsNotNullOrEmpty() ? rate.Modifier4 : string.Empty });
                                        totalCharge += unit * rate.Charge;
                                    }
                                }
                                else
                                {
                                    var unit = (int)Math.Ceiling((double)visit.MinSpent / 15);
                                    schedules.Add(new { date = visit.VisitDate.ToString("MM/dd/yyyy"), type = discipline, revenue = rate.RevenueCode, hcpcs = rate.Code, units = unit, amount = unit * rate.Charge, modifier = rate.Modifier.IsNotNullOrEmpty() ? rate.Modifier : string.Empty, modifier2 = rate.Modifier2.IsNotNullOrEmpty() ? rate.Modifier2 : string.Empty, modifier3 = rate.Modifier3.IsNotNullOrEmpty() ? rate.Modifier3 : string.Empty, modifier4 = rate.Modifier4.IsNotNullOrEmpty() ? rate.Modifier4 : string.Empty });
                                    totalCharge += unit * rate.Charge;
                                }
                            }
                            break;
                    }
                }
            }
            return schedules;
        }

        public static void SetBillRateValue(ChargeRate rate, string discipline)
        {
            switch (discipline)
            {
                case "SkilledNurse":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0154";
                        rate.Charge = 200.00;
                    }
                    break;
                case "SkilledNurseManagement":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0162";
                        rate.Charge = 200.00;
                    }
                    break;
                case "SkilledNurseObservation":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0163";
                        rate.Charge = 200.00;
                    }
                    break;
                case "SkilledNurseTeaching":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0164";
                        rate.Charge = 200.00;
                    }
                    break;
                case "HomeHealthAide":
                    {
                        rate.Unit = 4;
                        rate.RevenueCode = "0571";
                        rate.Code = "G0156";
                        rate.Charge = 120.00;
                    }
                    break;
                case "PhysicalTherapy":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0421";
                        rate.Code = "G0151";
                        rate.Charge = 250.00;
                    }
                    break;
                case "PhysicalTherapyAssistance":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0421";
                        rate.Code = "G0157";
                        rate.Charge = 250.00;
                    }
                    break;
                case "PhysicalTherapyMaintenance":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0421";
                        rate.Code = "G0159";
                        rate.Charge = 250.00;
                    }
                    break;
                case "OccupationalTherapy":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0431";
                        rate.Code = "G0152";
                        rate.Charge = 250.00;
                    }
                    break;
                case "OccupationalTherapyAssistance":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0431";
                        rate.Code = "G0158";
                        rate.Charge = 250.00;
                    }
                    break;
                case "OccupationalTherapyMaintenance":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0431";
                        rate.Code = "G0160";
                        rate.Charge = 250.00;
                    }
                    break;
                case "SpeechTherapy":
                    {
                        rate.Unit = 4;
                        rate.RevenueCode = "0440";
                        rate.Code = "G0153";
                        rate.Charge = 250.00;
                    }
                    break;
                case "SpeechTherapyMaintenance":
                    {
                        rate.Unit = 4;
                        rate.RevenueCode = "0440";
                        rate.Code = "G0161";
                        rate.Charge = 250.00;
                    }
                    break;
                case "MedicareSocialWorker":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0561";
                        rate.Code = "G0155";
                        rate.Charge = 250.00;
                    }
                    break;
                case "SkilledNurseSOC":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0162";
                        rate.Charge = 200.00;
                    }
                    break;
                case "SkilledNurseRecet":
                    {
                        rate.Unit = 3;
                        rate.RevenueCode = "0551";
                        rate.Code = "G0162";
                        rate.Charge = 200.00;
                    }
                    break;
            }
        }

        public AgencyInsurance CMSInsuranceToAgencyInsurance(Guid branchId, int insuranceId)
        {
            var agencyInsurance = new AgencyInsurance();
            var agency = agencyRepository.GetById(Current.AgencyId);
            if (agency != null)
            {
                var location = agencyRepository.FindLocation(Current.AgencyId, branchId);
                if (location != null)
                {
                    var insurance = lookUpRepository.GetInsurance(insuranceId);
                    var axxessData = agencyRepository.SubmitterInfo(insuranceId);

                    var chargeRates = new List<ChargeRate>();
                    var data = location.ToCostRateDictionary();
                    if (data != null && data.Count > 0)
                    {
                        var chargeRatesDictionary = this.MedicareBillRate(data);
                        if (chargeRatesDictionary != null && chargeRatesDictionary.Count > 0)
                        {
                            chargeRates = chargeRatesDictionary.Values.ToList();
                        }
                    }
                    if (!location.IsLocationStandAlone)
                    {
                        if (agency.IsAxxessTheBiller)
                        {
                            if (axxessData != null)
                            {
                                agency.SubmitterId = axxessData.SubmitterId;
                                agency.SubmitterName = axxessData.SubmitterName;
                                agency.SubmitterPhone = axxessData.Phone;
                                agency.SubmitterFax = axxessData.Fax;
                            }
                        }
                        agencyInsurance = new AgencyInsurance
                        {
                            Id = insuranceId,
                            AgencyId = Current.AgencyId,
                            PayorType = (int)PayerTypes.MedicareTraditional,
                            InvoiceType = (int)InvoiceType.UB,
                            BillType = "institutional",
                            Name = insurance != null ? insurance.Name : string.Empty,
                            PayorId = axxessData != null ? axxessData.Code : string.Empty,
                            SubmitterId = agency.SubmitterId,
                            SubmitterName = agency.SubmitterName,
                            SubmitterPhone = agency.SubmitterPhone,
                            FaxNumber = agency.SubmitterFax,
                            Ub04Locator81cca = location.Ub04Locator81cca,
                            IsAxxessTheBiller = agency.IsAxxessTheBiller,
                            BillData = chargeRates.ToXml(),
                        };
                    }
                    else
                    {
                        if (location.IsAxxessTheBiller)
                        {
                            if (axxessData != null)
                            {
                                location.SubmitterId = axxessData.SubmitterId;
                                location.SubmitterName = axxessData.SubmitterName;
                                location.SubmitterPhone = axxessData.Phone;
                                location.SubmitterFax = axxessData.Fax;
                            }
                        }
                        agencyInsurance = new AgencyInsurance
                        {
                            Id = insuranceId,
                            AgencyId = Current.AgencyId,
                            PayorType = (int)PayerTypes.MedicareTraditional,
                            InvoiceType = (int)InvoiceType.UB,
                            BillType = "institutional",
                            Name = insurance != null ? insurance.Name : string.Empty,
                            PayorId = axxessData != null ? axxessData.Code : string.Empty,
                            SubmitterId = location.SubmitterId,
                            SubmitterName = location.SubmitterName,
                            SubmitterPhone = location.SubmitterPhone,
                            FaxNumber = location.SubmitterFax,
                            Ub04Locator81cca = location.Ub04Locator81cca,
                            IsAxxessTheBiller = axxessData != null ? axxessData.SubmitterId.IsEqual(location.SubmitterId) : false,
                            BillData = chargeRates.ToXml()
                        };
                    }
                }
            }
            return agencyInsurance;
        }

        public Dictionary<int, ChargeRate> FinalToCharegRates(Final claim, out AgencyInsurance agencyInsurance)
        {
            agencyInsurance = new AgencyInsurance();
            var chargeRates = new Dictionary<int, ChargeRate>();
            if (claim.IsFinalInfoVerified && claim.Insurance.IsNotNullOrEmpty() || (!BillingStatusFactory.UnProcessed().Contains(claim.Status) && claim.Insurance.IsNotNullOrEmpty()))
            {
                agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>() ?? new AgencyInsurance();
                chargeRates = agencyInsurance.ToInsurancBillDataDictionary();
                if (chargeRates != null && chargeRates.Count > 0)
                {
                }
                else
                {
                    if (claim.PrimaryInsuranceId > 0)
                    {
                        if (claim.PrimaryInsuranceId < 1000)
                        {
                            agencyInsurance = this.CMSInsuranceToAgencyInsurance(claim.AgencyLocationId, claim.PrimaryInsuranceId);
                        }
                        else if (claim.PrimaryInsuranceId >= 1000)
                        {
                            agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                        }
                        chargeRates = agencyInsurance.ToInsurancBillDataDictionary() ?? new Dictionary<int, ChargeRate>();
                    }
                }
            }
            else
            {
                if (claim.PrimaryInsuranceId > 0)
                {
                    if (claim.PrimaryInsuranceId < 1000)
                    {
                        agencyInsurance = this.CMSInsuranceToAgencyInsurance(claim.AgencyLocationId, claim.PrimaryInsuranceId);
                    }
                    else if (claim.PrimaryInsuranceId >= 1000)
                    {
                        agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                    }
                    chargeRates = agencyInsurance.ToInsurancBillDataDictionary() ?? new Dictionary<int, ChargeRate>();
                }
            }
            return chargeRates;
        }

        public Dictionary<int, ChargeRate> ManagedToCharegRates(ManagedClaim claim, out AgencyInsurance agencyInsurance)
        {
            agencyInsurance = new AgencyInsurance();
            var chargeRates = new Dictionary<int, ChargeRate>();
            if (claim.IsInfoVerified && claim.Insurance.IsNotNullOrEmpty() || (!ManagedClaimStatusFactory.UnProcessed().Contains(claim.Status) && claim.Insurance.IsNotNullOrEmpty())) //(claim.Status != (int)ManagedClaimStatus.ClaimCreated && claim.Status != (int)ManagedClaimStatus.ClaimReOpen)
            {
                agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>() ?? new AgencyInsurance();
                chargeRates = agencyInsurance.ToInsurancBillDataDictionary();
                if (chargeRates != null && chargeRates.Count > 0)
                {
                }
                else
                {
                    if (claim.PrimaryInsuranceId > 0)
                    {
                        if (claim.PrimaryInsuranceId < 1000)
                        {
                            agencyInsurance = this.CMSInsuranceToAgencyInsurance(claim.AgencyLocationId, claim.PrimaryInsuranceId);

                        }
                        else if (claim.PrimaryInsuranceId >= 1000)
                        {
                            agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                        }
                        chargeRates = agencyInsurance.ToInsurancBillDataDictionary() ?? new Dictionary<int, ChargeRate>();
                    }
                }
            }
            else
            {
                if (claim.PrimaryInsuranceId > 0)
                {
                    if (claim.PrimaryInsuranceId < 1000)
                    {
                        agencyInsurance = this.CMSInsuranceToAgencyInsurance(claim.AgencyLocationId, claim.PrimaryInsuranceId);

                    }
                    else if (claim.PrimaryInsuranceId >= 1000)
                    {
                        agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                    }
                    chargeRates = agencyInsurance.ToInsurancBillDataDictionary() ?? new Dictionary<int, ChargeRate>();
                }
            }
            return chargeRates;
        }

        public AgencyInsurance RapToInsurance(Rap claim)
        {
            var agencyInsurance = new AgencyInsurance();
            if (claim.IsVerified && claim.Insurance.IsNotNullOrEmpty() || (!BillingStatusFactory.UnProcessed().Contains(claim.Status) && claim.Insurance.IsNotNullOrEmpty()))
            {
                agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>() ?? new AgencyInsurance();
                if (agencyInsurance != null && agencyInsurance.Name.IsNotNullOrEmpty())
                {
                }
                else
                {
                    if (claim.PrimaryInsuranceId > 0)
                    {
                        if (claim.PrimaryInsuranceId < 1000)
                        {
                            agencyInsurance = this.CMSInsuranceToAgencyInsurance(claim.AgencyLocationId, claim.PrimaryInsuranceId);
                        }
                        else if (claim.PrimaryInsuranceId >= 1000)
                        {
                            agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                        }
                    }
                }
            }
            else
            {
                if (claim.PrimaryInsuranceId > 0)
                {
                    if (claim.PrimaryInsuranceId < 1000)
                    {
                        agencyInsurance = this.CMSInsuranceToAgencyInsurance(claim.AgencyLocationId, claim.PrimaryInsuranceId);
                    }
                    else if (claim.PrimaryInsuranceId >= 1000)
                    {
                        agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                    }
                }
            }
            return agencyInsurance;
        }

        public AgencyInsurance FinalToInsurance(Final claim)
        {
            var agencyInsurance = new AgencyInsurance();
            if (claim.IsFinalInfoVerified && claim.Insurance.IsNotNullOrEmpty() || (!BillingStatusFactory.UnProcessed().Contains(claim.Status) && claim.Insurance.IsNotNullOrEmpty()))
            {
                agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>() ?? new AgencyInsurance();
                if (agencyInsurance != null && agencyInsurance.Name.IsNotNullOrEmpty())
                {
                }
                else
                {
                    if (claim.PrimaryInsuranceId > 0)
                    {
                        if (claim.PrimaryInsuranceId < 1000)
                        {
                            agencyInsurance = this.CMSInsuranceToAgencyInsurance(claim.AgencyLocationId, claim.PrimaryInsuranceId);
                        }
                        else if (claim.PrimaryInsuranceId >= 1000)
                        {
                            agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                        }
                    }
                }
            }
            else
            {
                if (claim.PrimaryInsuranceId > 0)
                {
                    if (claim.PrimaryInsuranceId < 1000)
                    {
                        agencyInsurance = this.CMSInsuranceToAgencyInsurance(claim.AgencyLocationId, claim.PrimaryInsuranceId);
                    }
                    else if (claim.PrimaryInsuranceId >= 1000)
                    {
                        agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                    }
                }
            }
            return agencyInsurance;
        }

        public AgencyInsurance ManagedToInsurance(ManagedClaim claim)
        {
            var agencyInsurance = new AgencyInsurance();
            if (claim.IsInfoVerified && claim.Insurance.IsNotNullOrEmpty() || (!ManagedClaimStatusFactory.UnProcessed().Contains(claim.Status) && claim.Insurance.IsNotNullOrEmpty()))//(claim.Status != (int)ManagedClaimStatus.ClaimCreated && claim.Status != (int)ManagedClaimStatus.ClaimReOpen)
            {
                agencyInsurance = claim.Insurance.ToObject<AgencyInsurance>() ?? new AgencyInsurance();
                if (agencyInsurance != null && agencyInsurance.Name.IsNotNullOrEmpty())
                {
                }
                else
                {
                    if (claim.PrimaryInsuranceId > 0)
                    {
                        if (claim.PrimaryInsuranceId < 1000)
                        {
                            agencyInsurance = this.CMSInsuranceToAgencyInsurance(claim.AgencyLocationId, claim.PrimaryInsuranceId);

                        }
                        else if (claim.PrimaryInsuranceId >= 1000)
                        {
                            agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                        }
                    }
                }
            }
            else
            {
                if (claim.PrimaryInsuranceId > 0)
                {
                    if (claim.PrimaryInsuranceId < 1000)
                    {
                        agencyInsurance = this.CMSInsuranceToAgencyInsurance(claim.AgencyLocationId, claim.PrimaryInsuranceId);

                    }
                    else if (claim.PrimaryInsuranceId >= 1000)
                    {
                        agencyInsurance = agencyRepository.FindInsurance(Current.AgencyId, claim.PrimaryInsuranceId);
                    }
                }
            }
            return agencyInsurance;
        }

        public double MedicareSupplyTotal(Final final)
        {
            if (final != null)
            {
                if (final.Supply.IsNotNullOrEmpty())
                {
                    final.SupplyTotal = final.Supply.ToObject<List<Supply>>().Where(s => s.IsBillable && !s.IsDeprecated).Sum(s => s.TotalCost);
                }
                if (final.SupplyTotal <= 0)
                {
                    if (final.HippsCode.IsNotNullOrEmpty() && final.HippsCode.Length == 5)
                    {
                        final.SupplyTotal = this.GetSupplyReimbursement(final.HippsCode[4], final.EpisodeStartDate.Year);
                    }
                }
                return final.SupplyTotal;
            }
            return 0;
        }

        public bool PostRemittance(Guid Id, List<string> episodes)
        {
            var dictionary = this.RemittanceDataDictionary(episodes);
            if (dictionary != null && dictionary.Count > 0)
            {
                var remittance = billingRepository.GetRemittance(Current.AgencyId, Id);
                if (remittance != null && remittance.Data.IsNotNullOrEmpty())
                {
                    remittance.Data = remittance.Data.Replace("&", "&amp;").Replace("\r", "").Replace("\n", "");
                    var remittanceData = remittance.Data.ToObject<RemittanceData>();
                    if (remittanceData != null && remittanceData.Claim != null && remittanceData.Claim.Count > 0)
                    {
                        if (remittanceData.Claim != null && remittanceData.Claim.Count > 0)
                        {
                            remittanceData.Claim.ForEach(c =>
                            {
                                var claimInformations = c.ClaimPaymentInformation;
                                if (claimInformations != null && claimInformations.Count > 0)
                                {
                                    foreach (var info in claimInformations)
                                    {
                                        if (info.PayerClaimControlNumber.IsNotNullOrEmpty() && dictionary.ContainsKey(info.PayerClaimControlNumber) && info.ClaimStatusCode.IsInteger())
                                        {
                                            var datas = dictionary[info.PayerClaimControlNumber];
                                            if (datas != null && datas.Length == 2)
                                            {
                                                var episodeId = datas[0].IsNotNullOrEmpty() && datas[0].IsGuid() ? datas[0].ToGuid():Guid.Empty;
                                                if (!episodeId.IsEmpty())
                                                {
                                                    info.RemittanceDate = remittance.RemittanceDate;
                                                    if (info.ClaimStatementPeriodStartDate.IsEqual(info.ClaimStatementPeriodEndDate))
                                                    {
                                                        var rap = billingRepository.GetRap(Current.AgencyId, episodeId);
                                                        if (rap != null)
                                                        {
                                                            var claimInfos = rap.Remittance.IsNotNullOrEmpty() ? rap.Remittance.ToObject<List<PaymentInformation>>() : new List<PaymentInformation>();
                                                            if (!claimInfos.Exists(cif => cif.RemittanceDate.Date == remittance.RemittanceDate.Date))
                                                            {
                                                                info.IsPosted = true;
                                                                long batchId = 0;
                                                                var isValidBatchId = datas[1].IsNotNullOrEmpty() ? long.TryParse(datas[1], out batchId) : false;
                                                                var isLatePost = claimInfos.Exists(cif => cif.RemittanceDate.Date > remittance.RemittanceDate.Date);
                                                                if (!isLatePost)
                                                                {
                                                                    rap.Status = info.ClaimStatusCode.ToInteger();
                                                                }
                                                                info.RemitId = remittance.RemitId;
                                                                info.Id = remittance.Id;
                                                                info.BatchId = batchId;
                                                                claimInfos.Add(info);
                                                                var total = claimInfos.Where(ct => ct.ClaimPaymentAmount.IsNotNullOrEmpty() && ct.ClaimPaymentAmount.IsDouble()).Sum(ct => ct.ClaimPaymentAmount.ToDouble());
                                                                rap.Payment = total;
                                                                rap.Remittance = claimInfos.ToXml();
                                                                rap.PaymentDate = remittance.PaymentDate;
                                                                if (billingRepository.UpdateRap(rap))
                                                                {
                                                                    if (isValidBatchId)
                                                                    {
                                                                        var rapSnapShot = billingRepository.GetRapSnapShot(Current.AgencyId, episodeId, batchId);
                                                                        if (rapSnapShot != null)
                                                                        {
                                                                            if (!isLatePost)
                                                                            {
                                                                                rapSnapShot.Status = info.ClaimStatusCode.ToInteger();
                                                                            }
                                                                            var totalSnapShot = claimInfos.Where(ct => ct.BatchId == batchId && ct.ClaimPaymentAmount.IsNotNullOrEmpty() && ct.ClaimPaymentAmount.IsDouble()).Sum(ct => ct.ClaimPaymentAmount.ToDouble());
                                                                            rapSnapShot.Payment = totalSnapShot;
                                                                            rapSnapShot.PaymentDate = remittance.PaymentDate;
                                                                            billingRepository.UpdateRapSnapShots(rapSnapShot);
                                                                        }
                                                                    }
                                                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPRemittancePosted, string.Empty);
                                                                }
                                                                else
                                                                {
                                                                    info.IsPosted = false;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var final = billingRepository.GetFinalOnly(Current.AgencyId, episodeId);
                                                        if (final != null)
                                                        {
                                                            var claimInfos = final.Remittance.IsNotNullOrEmpty() ? final.Remittance.ToObject<List<PaymentInformation>>() : new List<PaymentInformation>();
                                                            if (!claimInfos.Exists(cif => cif.RemittanceDate.Date == remittance.RemittanceDate.Date))
                                                            {
                                                                long batchId = 0;
                                                                var isValidBatchId = datas[1].IsNotNullOrEmpty() ? long.TryParse(datas[1], out batchId) : false;
                                                                info.IsPosted = true;
                                                                info.RemitId = remittance.RemitId;
                                                                info.Id = remittance.Id;
                                                                info.BatchId = batchId;
                                                                claimInfos.Add(info);
                                                                final.Remittance = claimInfos.ToXml();
                                                                var isLatePost = claimInfos.Exists(cif => cif.RemittanceDate.Date > remittance.RemittanceDate.Date);
                                                                if (!isLatePost)
                                                                {
                                                                    final.Status = info.ClaimStatusCode.ToInteger();
                                                                }
                                                                var total = claimInfos.Where(ct => ct.ClaimPaymentAmount.IsNotNullOrEmpty() && ct.ClaimPaymentAmount.IsDouble()).Sum(ct => ct.ClaimPaymentAmount.ToDouble());
                                                                final.Payment = total;
                                                                final.PaymentDate = remittance.PaymentDate;
                                                                if (billingRepository.UpdateFinal(final))
                                                                {
                                                                    if (isValidBatchId)
                                                                    {
                                                                        var finalSnapShot = billingRepository.GetFinalSnapShot(Current.AgencyId, episodeId, batchId);
                                                                        if (finalSnapShot != null)
                                                                        {
                                                                            if (!isLatePost)
                                                                            {
                                                                                finalSnapShot.Status = info.ClaimStatusCode.ToInteger();
                                                                            }
                                                                            var totalSnapShot = claimInfos.Where(ct => ct.BatchId==batchId && ct.ClaimPaymentAmount.IsNotNullOrEmpty() && ct.ClaimPaymentAmount.IsDouble()).Sum(ct => ct.ClaimPaymentAmount.ToDouble());
                                                                            finalSnapShot.Payment = totalSnapShot;
                                                                            finalSnapShot.PaymentDate = remittance.PaymentDate;
                                                                            billingRepository.UpdateFinalSnapShots(finalSnapShot);
                                                                        }
                                                                    }
                                                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalRemittancePosted, string.Empty);
                                                                }
                                                                else
                                                                {
                                                                    info.IsPosted = false;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            });
                        }
                        remittance.Data = remittanceData.ToXml();
                    }
                    return billingRepository.UpdateRemittance(remittance);
                }
            }
            return false;
        }

        #endregion

        #region Private Methods

        private string GetNrsSeverityLevel(string hippsCode)
        {
            if (hippsCode.IsNotNullOrEmpty() && hippsCode.Length == 5)
            {
                if (hippsCode.ToLower().EndsWith("s"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "1");
                }
                if (hippsCode.ToLower().EndsWith("t"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "2");
                }
                if (hippsCode.ToLower().EndsWith("u"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "3");
                }
                if (hippsCode.ToLower().EndsWith("v"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "4");
                }
                if (hippsCode.ToLower().EndsWith("w"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "5");
                }
                if (hippsCode.ToLower().EndsWith("x"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "6");
                }
            }
            return hippsCode;
        }

        private string GetReverseNrsSeverityLevel(string hippsCode)
        {
            if (hippsCode.IsNotNullOrEmpty() && hippsCode.Length == 5)
            {
                if (hippsCode.ToLower().EndsWith("1"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "S");
                }
                if (hippsCode.ToLower().EndsWith("2"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "T");
                }
                if (hippsCode.ToLower().EndsWith("3"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "U");
                }
                if (hippsCode.ToLower().EndsWith("4"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "V");
                }
                if (hippsCode.ToLower().EndsWith("5"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "W");
                }
                if (hippsCode.ToLower().EndsWith("6"))
                {
                    return hippsCode.Remove(4, 1).Insert(4, "X");
                }
            }
            return hippsCode;
        }

        private Dictionary<string, string [] > RemittanceDataDictionary(List<string> datas)
        {
            var dictionary = new Dictionary<string, string []>();
            if (datas != null && datas.Count > 0)
            {
                datas.ForEach(data =>
                {
                    var values = data.Split('|');
                    if (values != null && values.Length == 3 && !dictionary.ContainsKey(values[2]))
                    {
                        dictionary.Add(values[2], new[] { values[0], values[1] });
                    }
                });
            }
            return dictionary;
        }

        public double ProspectivePayAmount(PPSStandard ppsStandard, HippsAndHhrg hhrg, CBSACode cbsaCode )
        {
            double amount = 0;
            if (hhrg != null && cbsaCode != null && ppsStandard != null)
            {
                var time = ppsStandard.Time;
                var ppsRate = ppsStandard.UrbanRate;
                var hhrgWeight = hhrg.HHRGWeight;
                var labor = ppsStandard.Labor;
                var nonLabor = ppsStandard.NonLabor;
                double wageIndex = 0;
                if (time.Year == 2012)
                {
                    wageIndex = cbsaCode.WITwoTwelve;
                }
                else if (time.Year == 2011)
                {
                    wageIndex = cbsaCode.WITwoEleven;
                }
                else if (time.Year == 2010)
                {
                    wageIndex = cbsaCode.WITwoTen;
                }
                else if (time.Year == 2009)
                {
                    wageIndex = cbsaCode.WITwoNine;
                }
                else if (time.Year == 2008)
                {
                    wageIndex = cbsaCode.WITwoEight;
                }
                else if (time.Year == 2007)
                {
                    wageIndex = cbsaCode.WITwoSeven;
                }
                amount = (ppsRate * hhrgWeight) * ((labor * wageIndex) + nonLabor);
            }
            return amount;
        }

        public double GetProspectivePaymentAmount(PPSStandard ppsStandard, HippsAndHhrg hhrg, CBSACode cbsaCode)
        {
            double prospectivePayment = 0;

            if (cbsaCode != null && hhrg != null && ppsStandard != null)
            {
                double rate = 0;
                var isRural = false;
                var wageIndex = this.WageIndex(cbsaCode, ppsStandard.Time);

                if (cbsaCode.CBSA.IsNotNullOrEmpty())
                {
                    if (cbsaCode.CBSA.StartsWith("9"))
                    {
                        isRural = true;
                        rate = ppsStandard.RuralRate;
                    }
                    else
                    {
                        isRural = false;
                        rate = ppsStandard.UrbanRate;
                    }
                }

                var rateTimesWeight = rate * hhrg.HHRGWeight;
                var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                var character = hhrg.HIPPS.Length == 5 ? hhrg.HIPPS[4] : ' ';
                var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);
                var totalAmount = supplyAmount + totalAmountWithoutSupplies;

                prospectivePayment = totalAmount;

            }
            return prospectivePayment;
        }

        public ProspectivePayment GetProspectivePayment(PPSStandard ppsStandard, HippsAndHhrg hhrg, CBSACode cbsaCode)
        {
            var prospectivePayment = new ProspectivePayment();
            if (cbsaCode != null && hhrg != null && ppsStandard != null)
            {
                double rate = 0;
                var isRural = false;
                var wageIndex = this.WageIndex(cbsaCode, ppsStandard.Time);

                if (cbsaCode.CBSA.IsNotNullOrEmpty())
                {
                    if (cbsaCode.CBSA.StartsWith("9"))
                    {
                        isRural = true;
                        rate = ppsStandard.RuralRate;
                    }
                    else
                    {
                        isRural = false;
                        rate = ppsStandard.UrbanRate;
                    }
                }

                var rateTimesWeight = rate * hhrg.HHRGWeight;
                var laborAmount = rateTimesWeight * ppsStandard.Labor * wageIndex;
                var nonLaborAmount = rateTimesWeight * ppsStandard.NonLabor;
                var totalAmountWithoutSupplies = laborAmount + nonLaborAmount;

                var character = hhrg.HIPPS.Length == 5 ? hhrg.HIPPS[4] : ' ';
                var supplyAmount = GetSupplyReimbursement(ppsStandard, isRural, character);
                prospectivePayment.TotalAmount = supplyAmount + totalAmountWithoutSupplies;

                prospectivePayment.Hhrg = hhrg.HHRG;
                prospectivePayment.CbsaCode = cbsaCode.CBSA;
                prospectivePayment.WageIndex = wageIndex.ToString();
                prospectivePayment.Weight = hhrg.HHRGWeight.ToString();
                prospectivePayment.LaborAmount = string.Format("${0:#0.00}", laborAmount);
                prospectivePayment.NonLaborAmount = string.Format("${0:#0.00}", nonLaborAmount);
                prospectivePayment.NonRoutineSuppliesAmount = string.Format("${0:#0.00}", supplyAmount);
                prospectivePayment.TotalAmountWithoutSupplies = string.Format("${0:#0.00}", totalAmountWithoutSupplies);
                prospectivePayment.TotalProspectiveAmount = string.Format("${0:#0.00}", prospectivePayment.TotalAmount);
            }
            return prospectivePayment;
        }

        private double WageIndex(CBSACode cbsaCode, DateTime time)
        {
            double wageIndex = 0;
            if (time.Year == 2012) wageIndex = cbsaCode.WITwoTwelve;
            else if (time.Year == 2011) wageIndex = cbsaCode.WITwoEleven;
            else if (time.Year == 2010) wageIndex = cbsaCode.WITwoTen;
            else if (time.Year == 2009) wageIndex = cbsaCode.WITwoNine;
            else if (time.Year == 2008) wageIndex = cbsaCode.WITwoEight;
            else if (time.Year == 2007) wageIndex = cbsaCode.WITwoSeven;
            return wageIndex;
        }

        private double GetSupplyReimbursement(PPSStandard ppsStandard, bool isRural, char type)
        {
            if (ppsStandard != null)
            {
                switch (type)
                {
                    case 'S':
                        if (isRural)
                        {
                            return ppsStandard.RuralS;
                        }
                        return ppsStandard.S;
                    case 'T':
                        if (isRural)
                        {
                            return ppsStandard.RuralT;
                        }
                        return ppsStandard.T;
                    case 'U':
                        if (isRural)
                        {
                            return ppsStandard.RuralU;
                        }
                        return ppsStandard.U;
                    case 'V':
                        if (isRural)
                        {
                            return ppsStandard.RuralV;
                        }
                        return ppsStandard.V;
                    case 'W':
                        if (isRural)
                        {
                            return ppsStandard.RuralW;
                        }
                        return ppsStandard.W;
                    case 'X':
                        if (isRural)
                        {
                            return ppsStandard.RuralX;
                        }
                        return ppsStandard.X;
                }
            }
            return 0;
        }

        #endregion
    }
}
