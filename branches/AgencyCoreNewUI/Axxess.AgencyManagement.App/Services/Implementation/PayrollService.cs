﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;

    using Axxess.AgencyManagement.Repositories;

    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Domain;
    using ViewData;
    using Security;
    using Axxess.Membership.Enums;
    using Axxess.Log.Enums;
    using Axxess.Log.Common;

    public class PayrollService : IPayrollService
    {
        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;

        public PayrollService(IAgencyManagementDataProvider agencyManagementDataProvider, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
        }

        public bool MarkAsPaid(List<string> itemList)
        {
            var result = true;

            if (itemList != null && itemList.Count > 0)
            {
                itemList.ForEach(visit =>
                {
                    var visitArray = visit.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                    if (visitArray != null && visitArray.Length == 3)
                    {
                        var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, visitArray[1].ToGuid(), visitArray[0].ToGuid(), visitArray[2].ToGuid());
                        if (scheduleEvent != null)
                        {
                            var oldIsVisitPaid = scheduleEvent.IsVisitPaid;
                            scheduleEvent.IsVisitPaid = true;
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask))
                                {
                                    Auditor.Log(Current.AgencyId,Current.UserId,Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.MarkedPaid, (DisciplineTasks)scheduleEvent.DisciplineTask, "This visit is marked as paid.");
                                }
                            }
                            else
                            {
                                result = false;
                                scheduleEvent.IsVisitPaid = oldIsVisitPaid;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return;
                            }
                        }
                    }
                });
            }
            return result;
        }

        public bool MarkAsUnpaid(List<string> itemList)
        {
            var result = true;

            if (itemList != null && itemList.Count > 0)
            {
                itemList.ForEach(visit =>
                {
                    var visitArray = visit.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                    if (visitArray != null && visitArray.Length == 3)
                    {
                        var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, visitArray[1].ToGuid(), visitArray[0].ToGuid(), visitArray[2].ToGuid());
                        if (scheduleEvent != null)
                        {
                            var oldIsVisitPaid = scheduleEvent.IsVisitPaid;
                            scheduleEvent.IsVisitPaid = false;
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.MarkedUnpaid, (DisciplineTasks)scheduleEvent.DisciplineTask, "The visit is marked as unpaid.");
                                }
                            }
                            else
                            {
                                result = false;
                                scheduleEvent.IsVisitPaid = oldIsVisitPaid;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return; 
                            }
                        }
                    }
                });
            }
            return result;
        }

        public List<VisitSummary> GetSummary(DateTime startDate, DateTime endDate, string Status)
        {
            var scheduleStatus = ScheduleStatusFatory.OnAndAfterQAStatus().ToArray();
            var disciplines = DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToArray();
            var list = patientRepository.GetPayrollSummmaryLean(Current.AgencyId, startDate, endDate, 0, scheduleStatus, disciplines, new int[] { (int)DisciplineTasks.FaceToFaceEncounter }, false, Status);
            if (list != null && list.Count > 0)
            {
                var userIds = list.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                list.ForEach(p =>
                {
                    var user = users.SingleOrDefault(u => u.Id == p.UserId);
                    if (user != null)
                    {
                        p.UserName = user.DisplayName;
                    }
                });
            }
            return list;
        }

        public List<PayrollDetail> GetDetails(DateTime startDate, DateTime endDate, string Status)
        {
            var list = new List<PayrollDetail>();
            if (Status.IsEqual("false") || Status.IsEqual("true"))
            {
                var scheduleStatus = ScheduleStatusFatory.OnAndAfterQAStatus().ToArray();
                var disciplines = DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToArray();
                var payrollVisits = patientRepository.GetPayrollSummmaryVisits(Current.AgencyId,Guid.Empty, startDate, endDate, 0, scheduleStatus, disciplines, new int[] { (int)DisciplineTasks.FaceToFaceEncounter }, false, Status);
                if (payrollVisits != null && payrollVisits.Count > 0)
                {
                    var userIds = payrollVisits.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    if (userIds != null && userIds.Count > 0)
                    {
                        var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                        userIds.ForEach(u =>
                        {
                            var user = users.SingleOrDefault(us => us.Id == u);
                            list.Add(new PayrollDetail
                            {
                                Id = u,
                                Name = user != null ? user.DisplayName : string.Empty,
                                Visits = payrollVisits.Where(p => p.UserId == u).ToList(),
                                StartDate = startDate,
                                EndDate = endDate,
                                PayrollStatus = Status
                            }
                            );
                        });
                    }
                }
            }
            return list;
        }

        public List<UserVisit> GetVisits(Guid userId, DateTime startDate, DateTime endDate, string Status)
        {
            var userVisits = new List<UserVisit>();
            if (Status.IsEqual("false") || Status.IsEqual("true"))
            {
                var scheduleStatus = ScheduleStatusFatory.OnAndAfterQAStatus().ToArray();
                var disciplines = DisciplineFactory.MainDisciplines().Select(d => d.ToString()).ToArray();
                userVisits = patientRepository.GetPayrollSummmaryVisits(Current.AgencyId, userId, startDate, endDate, 0, scheduleStatus, disciplines, new int[] { (int)DisciplineTasks.FaceToFaceEncounter }, false, Status);
            }
            return userVisits;
        }
    }
}
