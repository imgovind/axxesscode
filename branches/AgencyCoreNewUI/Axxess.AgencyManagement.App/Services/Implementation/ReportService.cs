﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Domain;
    using ViewData;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.LookUp.Repositories;
    using Axxess.AgencyManagement.App.Enums;
    using  Axxess.AgencyManagement.App.Extensions;
    using Axxess.LookUp.Domain;

    public class ReportService : IReportService
    {
        #region Constructor and Private Members

        private readonly IUserRepository userRepository;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly ILookupRepository lookUpRepository;


        public ReportService(IAgencyManagementDataProvider agencyManagementDataProvider, IOasisCDataProvider oasisDataProvider, ILookUpDataProvider lookUpDataProvider, IAssessmentService assessmentService)
        {
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.assessmentService = assessmentService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.assessmentRepository = oasisDataProvider.OasisAssessmentRepository;
            this.lookUpRepository = lookUpDataProvider.LookUpRepository;
        }

        #endregion

        #region Patient Reports

        public List<Birthday> GetPatientBirthdays(Guid addressBranchCode)
        {
            IList<Patient> patients = null;
            var birthdays = new List<Birthday>();
            if (addressBranchCode.IsEmpty())
            {
                patients = patientRepository.FindPatientOnly((int)PatientStatus.Active, Current.AgencyId);
            }
            else
            {
                patients = patientRepository.Find((int)PatientStatus.Active, addressBranchCode, Current.AgencyId);
            }
            patients.ForEach(patient =>
            {
                birthdays.Add(new Birthday
                {
                    Id = patient.Id,
                    Name = patient.DisplayName,
                    Date = patient.DOB,
                    AddressLine1 = patient.AddressLine1,
                    AddressLine2 = patient.AddressLine2,
                    AddressCity = patient.AddressCity,
                    AddressStateCode = patient.AddressStateCode,
                    AddressZipCode = patient.AddressZipCode,
                    PhoneHome = patient.PhoneHome,
                    PhoneMobile = patient.PhoneMobile,
                    EmailAddress = patient.EmailAddress
                });
            });
            return birthdays;
        }

        public List<Birthday> GetPatientBirthdays(Guid branchId, int month)
        {
            IList<Patient> patients = null;
            var birthdays = new List<Birthday>();
            if (branchId.IsEmpty())
            {
                patients = patientRepository.FindPatientOnly((int)PatientStatus.Active, Current.AgencyId);
            }
            else
            {
                patients = patientRepository.Find((int)PatientStatus.Active, branchId, Current.AgencyId);
            }
            if (patients != null)
            {
                patients.ForEach(patient =>
                {
                    if (patient.DOB.IsValid() && patient.DOB.Month == month)
                    {
                        birthdays.Add(new Birthday
                        {
                            Id = patient.Id,
                            Name = patient.DisplayName.ToUpperCase(),
                            IdNumber = patient.PatientIdNumber,
                            Date = patient.DOB,
                            AddressLine1 = patient.AddressLine1,
                            AddressLine2 = patient.AddressLine2,
                            AddressCity = patient.AddressCity,
                            AddressStateCode = patient.AddressStateCode,
                            AddressZipCode = patient.AddressZipCode,
                            PhoneHome = patient.PhoneHome,
                            PhoneMobile = patient.PhoneMobile,
                            EmailAddress = patient.EmailAddress
                        });
                    }
                });
            }
            return birthdays.OrderByDescending(b => b.Date.Day).ThenBy(b => b.Name).ToList();
        }

        public List<Birthday> GetCurrentBirthdays()
        {
            var birthdays = new List<Birthday>();
            var patients = new List<PatientSelection>();

            if (Current.IsAgencyAdmin || Current.IsDirectorOfNursing || Current.IsOfficeManager || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
            {
                birthdays = patientRepository.GetCurrentPatientBirthdays(Current.AgencyId);
            }
            else if (Current.IsClinicianOrHHA)
            {
                patients = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active);
                if (patients != null && patients.Count > 0)
                {
                    patients.ForEach(patient =>
                    {
                        if (patient.DOB.Month == DateTime.Now.Month)
                        {
                            birthdays.Add(new Birthday
                            {
                                Date = patient.DOB,
                                PhoneHome = patient.PhoneNumber,
                                Name = patient.DisplayName.ToUpperCase()
                            });
                        }
                    });
                }
            }
            return birthdays;
        }

        public List<AddressBookEntry> GetPatientAddressListing(Guid branchId, int statusId)
        {
            var contacts = new List<AddressBookEntry>();
            var patients = patientRepository.Find(statusId, branchId, Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(patient =>
                {
                    contacts.Add(new AddressBookEntry
                    {
                        Id = patient.Id,
                        Name = patient.DisplayName.ToUpperCase(),
                        IdNumber = patient.PatientIdNumber,
                        AddressLine1 = patient.AddressLine1.ToTitleCase(),
                        AddressLine2 = patient.AddressLine2.ToTitleCase(),
                        AddressCity = patient.AddressCity.ToTitleCase(),
                        AddressStateCode = patient.AddressStateCode,
                        AddressZipCode = patient.AddressZipCode,
                        PhoneHome = patient.PhoneHome.ToPhone(),
                        PhoneMobile = patient.PhoneMobile.ToPhone(),
                        EmailAddress = patient.EmailAddress
                    });
                });
            }

            return contacts.OrderBy(a => a.Name).ToList();
        }

        public List<EmergencyContactInfo> GetPatientEmergencyContacts(Guid branchCode, int statusId)
        {
            return patientRepository.GetEmergencyContactInfos(Current.AgencyId, branchCode, statusId);
        }

        public List<PatientSocCertPeriod> GetPatientSocCertPeriod(Guid branchId, int statusId, DateTime startDate, DateTime endDate)
        {
            var socCertPeriod = new List<PatientSocCertPeriod>();
            var patients = patientRepository.GetPatientPhysicianInfos(Current.AgencyId, branchId, statusId);
            if (patients != null && patients.Count > 0)
            {
                var ids =  patients.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                if (ids.IsNotNullOrEmpty())
                {
                    socCertPeriod = patientRepository.PatientSocCertPeriods(Current.AgencyId, ids, startDate, endDate);
                    if (socCertPeriod != null && socCertPeriod.Count > 0)
                    {
                        var userIds = patients.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                        var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);

                        var physicianIds = patients.Where(p => !p.PhysicianId.IsEmpty()).Select(p => p.PhysicianId).Distinct().ToList();
                        var physicians = physicianRepository.GetPhysiciansByIdsLean(Current.AgencyId, physicianIds);

                        socCertPeriod.ForEach(soc =>
                        {
                            var tempPatient = patients.FirstOrDefault(p => p.Id == soc.Id);
                            if (tempPatient != null)
                            {
                                if (soc.PatientData.IsNotNullOrEmpty())
                                {
                                    var patient = soc.PatientData.ToObject<Patient>();
                                    if (patient != null)
                                    {
                                        soc.PatientFirstName = patient.FirstName.ToUpperCase();
                                        soc.PatientLastName = patient.LastName.ToUpperCase();
                                        soc.PatientPatientID = patient.PatientIdNumber;
                                        soc.PatientSoC = patient.StartofCareDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        soc.PatientFirstName = tempPatient.FirstName.ToUpperCase();
                                        soc.PatientLastName = tempPatient.LastName.ToUpperCase();
                                        soc.PatientPatientID = tempPatient.PatientIdNumber;
                                    }
                                }
                                else
                                {
                                    soc.PatientFirstName = tempPatient.FirstName.ToUpperCase();
                                    soc.PatientLastName = tempPatient.LastName.ToUpperCase();
                                    soc.PatientPatientID = tempPatient.PatientIdNumber;
                                }

                                var user = users.SingleOrDefault(u => u.Id == tempPatient.UserId);
                                if (user != null)
                                {
                                    soc.respEmp = user.DisplayName;
                                }
                                var physician = physicians.SingleOrDefault(u => u.Id == tempPatient.PhysicianId);
                                if (physician != null)
                                {
                                    soc.PhysicianName = physician.DisplayName;
                                }
                            }
                        });
                    }
                }
            }
            return socCertPeriod.OrderBy(r => r.PatientLastName).ThenBy(r => r.PatientFirstName).ToList();
        }

        public List<PatientRoster> GetPatientRoster(Guid branchCode, int statusId, int insuranceId, bool isExcel)
        {
            var rosterList = patientRepository.GetPatientRoster(Current.AgencyId, branchCode, statusId, insuranceId);
            if (rosterList != null && rosterList.Count > 0)
            {
                if (isExcel)
                {
                    var physicianIds = rosterList.Where(p => !p.PhysicianId.IsEmpty()).Select(p => p.PhysicianId).Distinct().ToList();
                    var physicians = physicianRepository.GetPhysiciansByIdsLean(Current.AgencyId, physicianIds);
                //    var ids = rosterList.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                //    var rosterEpisodesWithEvents = patientRepository.GetEpisodesForSurveyCensesAndPatientRoster(Current.AgencyId, ids);
                //    var disciplineTasksOASISSOC = new int[]
                //{
                //    (int)DisciplineTasks.OASISCStartofCare,
                //    (int)DisciplineTasks.NonOASISStartofCare,
                //    (int)DisciplineTasks.OASISCStartofCarePT,
                //    (int) DisciplineTasks.OASISCStartofCareOT
                //};

                //    var disciplineTasksOASISROCAndRecert = new int[]
                //{
                //    (int)DisciplineTasks.OASISCRecertification,
                //    (int)DisciplineTasks.OASISCRecertificationOT,
                //    (int)DisciplineTasks.OASISCRecertificationPT,
                //    (int)DisciplineTasks.NonOASISRecertification,

                //    (int)DisciplineTasks.OASISCResumptionofCare,
                //    (int)DisciplineTasks.OASISCResumptionofCareOT,
                //    (int)DisciplineTasks.OASISCResumptionofCarePT
                //};
                    rosterList.ForEach(roster =>
                    {
                        //var patientRosterEpisodes = rosterEpisodesWithEvents.Where(s => s.PatientId == roster.Id).GroupBy(s => s.EpisodeId).Select(s => s.First()).ToList();
                        //if (patientRosterEpisodes != null && patientRosterEpisodes.Count > 0)
                        //{
                        //    var episode = patientRosterEpisodes.OrderByDescending(s => s.StartDate).FirstOrDefault();
                        //    if (episode != null)
                        //    {
                        //        var schedules = rosterEpisodesWithEvents.Where(s => s.PatientId == episode.PatientId && s.EpisodeId == episode.EpisodeId).ToList();
                        //        if (schedules != null && schedules.Count > 0)
                        //        {
                        //            var scheduleEvent = schedules.Where(s => s.EpisodeId == episode.EpisodeId && disciplineTasksOASISSOC.Contains(s.DisciplineTask)).OrderBy(s => s.EventDate).FirstOrDefault();
                        //            if (scheduleEvent != null && scheduleEvent.Note.IsNotNullOrEmpty())
                        //            {
                        //                var diagnosis = scheduleEvent.Diagnosis();
                        //                if (diagnosis != null && diagnosis.Count > 0)
                        //                {
                        //                    roster.PatientPrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
                        //                    roster.PatientSecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
                        //                }
                        //            }
                        //            else
                        //            {
                        //                var previousEpisode = patientRosterEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                        //                if (previousEpisode != null)
                        //                {
                        //                    var previousSchedule = rosterEpisodesWithEvents.Where(s => s.PatientId == roster.Id && s.EpisodeId == previousEpisode.EpisodeId && s.EventDate <= previousEpisode.EndDate && s.EventDate >= previousEpisode.EndDate.AddDays(-5) && disciplineTasksOASISROCAndRecert.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                        //                    if (previousSchedule != null && previousSchedule.Note.IsNotNullOrEmpty())
                        //                    {
                        //                        var diagnosis = previousSchedule.Diagnosis();
                        //                        if (diagnosis != null && diagnosis.Count > 0)
                        //                        {
                        //                            roster.PatientPrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
                        //                            roster.PatientSecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }
                        //}

                        
                        if (!roster.PhysicianId.IsEmpty())
                        {
                            var physician = physicians.SingleOrDefault(u => u.Id == roster.PhysicianId);
                            if (physician != null)
                            {
                                roster.PhysicianNpi = physician.NPI;
                                roster.PhysicianName = physician.DisplayName;
                                roster.PhysicianPhone = physician.PhoneWork.ToPhone();
                                roster.PhysicianFacsimile = physician.FaxNumber;
                                roster.PhysicianPhoneHome = physician.PhoneAlternate;
                                roster.PhysicianEmailAddress = physician.EmailAddress;
                            }
                        }
                    });
                }
            }
            return rosterList.OrderBy(o => o.PatientDisplayName).ToList();
        }

        public List<PatientRoster> GetPatientRosterByInsurance(Guid branchCode, int insurance, int statusId)
        {
            var rosterList = new List<PatientRoster>();
            if (insurance > 0)
            {
                rosterList = patientRepository.GetPatientByInsurance(Current.AgencyId, branchCode, insurance, statusId);
            }
            return rosterList.OrderBy(r => r.PatientDisplayName).ToList();
        }

        public List<Authorization> GetExpiringAuthorizaton(Guid branchId, int status)
        {
            var autorizations = new List<Authorization>();
            var patients = patientRepository.Find(status, branchId, Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(patient =>
                {
                    var allAuthorization = patientRepository.GetAuthorizations(Current.AgencyId, patient.Id);
                    if (allAuthorization != null && allAuthorization.Count > 0)
                    {
                        allAuthorization.ForEach(auto =>
                        {
                            if (auto.EndDate <= DateTime.Now.AddDays(14))
                            {
                                auto.DisplayName = patient.DisplayName;
                                autorizations.Add(auto);
                            }
                        });
                    }
                }
                );
            }
            return autorizations;
        }

        public List<PatientRoster> GetPatientByPhysician(Guid agencyPhysicianId)
        {
            var rosterList = new List<PatientRoster>();
            if (!agencyPhysicianId.IsEmpty())
            {
               var  patients = physicianRepository.GetPhysicanPatients(agencyPhysicianId, Current.AgencyId);
                if (patients != null && patients.Count > 0)
                {
                    patients.ForEach(patient =>
                    {
                        var roster = new PatientRoster();
                        roster.Id = patient.Id;
                        roster.PatientId = patient.PatientIdNumber;
                        roster.PatientLastName = patient.LastName.ToUpperCase();
                        roster.PatientFirstName = patient.FirstName.ToUpperCase();
                        roster.PatientGender = patient.Gender;
                        roster.PatientMedicareNumber = patient.MedicareNumber;
                        roster.PatientDOB = patient.DOB;
                        roster.PatientPhone = patient.PhoneHome;
                        roster.PatientAddressLine1 = patient.AddressLine1;
                        roster.PatientAddressLine2 = patient.AddressLine2;
                        roster.PatientAddressCity = patient.AddressCity;
                        roster.PatientAddressStateCode = patient.AddressStateCode;
                        roster.PatientAddressZipCode = patient.AddressZipCode;
                        roster.PatientSoC = patient.StartOfCareDateFormatted;
                        roster.PatientInsurance = patient.PaymentSource;
                        rosterList.Add(roster);

                    });
                }
            }
            return rosterList.OrderBy(r => r.PatientDisplayName).ToList();
        }

        public List<PatientRoster> GetPatientByResponsiableEmployee(Guid branchCode, Guid userId, int statusId)
        {
            var roster = new List<PatientRoster>();
            var patients = patientRepository.FindByUser(statusId, branchCode, Current.AgencyId, userId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(patient =>
                {
                    var patientRoster = new PatientRoster();
                    patientRoster.Id = patient.Id;
                    patientRoster.PatientId = patient.PatientIdNumber;
                    patientRoster.PatientLastName = patient.LastName.ToUpperCase();
                    patientRoster.PatientFirstName = patient.FirstName.ToUpperCase();
                    patientRoster.PatientAddressLine1 = patient.AddressLine1;
                    patientRoster.PatientAddressLine2 = patient.AddressLine2;
                    patientRoster.PatientAddressCity = patient.AddressCity;
                    patientRoster.PatientAddressStateCode = patient.AddressStateCode;
                    patientRoster.PatientAddressZipCode = patient.AddressZipCode;
                    patientRoster.PatientSoC = patient.StartofCareDate.IsValid() ? patient.StartofCareDate.ToString("MM/dd/yyyy") : "";
                    roster.Add(patientRoster);
                });
            }
            return roster.OrderBy(r => r.PatientLastName).ThenBy(r => r.PatientFirstName).ToList();
        }

        public List<PatientRoster> GetPatientByResponsiableByCaseManager(Guid branchCode, Guid caseManagerId, int statusId)
        {
            var roster = new List<PatientRoster>();
            var patients = patientRepository.FindByCaseManager(Current.AgencyId, branchCode, statusId, caseManagerId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(patient =>
                {
                    var patientRoster = new PatientRoster();
                    patientRoster.Id = patient.Id;
                    patientRoster.PatientId = patient.PatientIdNumber;
                    patientRoster.PatientLastName = patient.LastName.ToUpperCase();
                    patientRoster.PatientFirstName = patient.FirstName.ToUpperCase();
                    patientRoster.PatientAddressLine1 = patient.AddressLine1;
                    patientRoster.PatientAddressLine2 = patient.AddressLine2;
                    patientRoster.PatientAddressCity = patient.AddressCity;
                    patientRoster.PatientAddressStateCode = patient.AddressStateCode;
                    patientRoster.PatientAddressZipCode = patient.AddressZipCode;
                    patientRoster.PatientSoC = patient.StartofCareDate.IsValid() ? patient.StartofCareDate.ToString("MM/dd/yyyy") : "";
                    roster.Add(patientRoster);
                });
            }
            return roster.OrderBy(r => r.PatientLastName).ThenBy(r => r.PatientFirstName).ToList();
        }

        public List<SurveyCensus> GetPatientSurveyCensus(Guid branchId, int statusId, int insuranceId, bool isExcel)
        {
            var surveyCensuses = patientRepository.GetSurveyCensesByStatus(Current.AgencyId, branchId, statusId, insuranceId);
            if (surveyCensuses != null && surveyCensuses.Count > 0)
            {
                var ids = surveyCensuses.Select(s => string.Format("'{0}'", s.Id)).ToArray().Join(", ");
                var surveyCensuseEpisodesWithEvents = patientRepository.GetEpisodesForSurveyCensesAndPatientRoster(Current.AgencyId, ids);
                var users = new List<User>();
                var insurances= new List<InsuranceLean>();
                IList<Insurance> standardInsurances = new List<Insurance>();
                if (isExcel)
                {
                    var userIds = surveyCensuses.Where(s => !s.CaseManagerId.IsEmpty()).Select(s => s.CaseManagerId).Distinct().ToList();
                    users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                    insurances = agencyRepository.GetLeanInsurances(Current.AgencyId, surveyCensuses.Where(s => s.InsuranceId.IsNotNullOrEmpty() && s.InsuranceId.IsInteger() && s.InsuranceId.ToInteger() >= 1000).Select(i => i.InsuranceId.ToInteger()).Distinct().ToArray());
                    standardInsurances = lookUpRepository.Insurances();
                }
                var disciplineTasksOASISSOC = DisciplineTaskFactory.AllSOCDisciplineTasks().ToArray();
                var disciplineTasksOASISROCAndRecert = DisciplineTaskFactory.LastFiveDayAssessments().ToArray(); 
                surveyCensuses.ForEach(surveyCensus =>
                {
                    var patientSurveyCensuseEpisodes = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == surveyCensus.Id).GroupBy(s => s.EpisodeId).Select(s => s.First()).ToList();
                    if (patientSurveyCensuseEpisodes != null && patientSurveyCensuseEpisodes.Count > 0)
                    {
                        var episode = patientSurveyCensuseEpisodes.OrderByDescending(s => s.StartDate).FirstOrDefault();
                        if (episode != null)
                        {
                            surveyCensus.CertPeriod = (episode.StartDate.IsValid() ? episode.StartDate.ToString("MM/dd/yyyy") : "") + " - " + (episode.EndDate.IsValid() ? episode.EndDate.ToString("MM/dd/yyyy") : "");

                            var schedules = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == episode.PatientId && s.EpisodeId == episode.EpisodeId).ToList();
                            if (schedules != null && schedules.Count > 0)
                            {
                                var scheduleEvent = schedules.Where(s => s.EpisodeId == episode.EpisodeId && disciplineTasksOASISSOC.Contains(s.DisciplineTask)).OrderBy(s => s.EventDate).FirstOrDefault();
                                if (scheduleEvent != null && scheduleEvent.Note.IsNotNullOrEmpty())
                                {
                                    var diagnosis = scheduleEvent.Diagnosis();
                                    if (diagnosis != null && diagnosis.Count > 0)
                                    {
                                        surveyCensus.PrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
                                        surveyCensus.SecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
                                    }
                                }
                                else
                                {
                                    var previousEpisode = patientSurveyCensuseEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                                    if (previousEpisode != null)
                                    {
                                        var previousSchedule = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == surveyCensus.Id && s.EpisodeId == previousEpisode.EpisodeId && s.EventDate <= previousEpisode.EndDate && s.EventDate >= previousEpisode.EndDate.AddDays(-5) && disciplineTasksOASISROCAndRecert.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                                        if (previousSchedule != null && previousSchedule.Note.IsNotNullOrEmpty())
                                        {
                                            var diagnosis = previousSchedule.Diagnosis();
                                            if (diagnosis != null && diagnosis.Count > 0)
                                            {
                                                surveyCensus.PrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
                                                surveyCensus.SecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
                                            }
                                        }
                                    }
                                }
                                surveyCensus.Discipline = schedules.Discipline().Join(",");
                            }
                            else
                            {
                                var previousEpisode = patientSurveyCensuseEpisodes.Where(e => e.StartDate < episode.StartDate).OrderByDescending(e => e.StartDate).FirstOrDefault();
                                if (previousEpisode != null)
                                {
                                    var previousSchedule = surveyCensuseEpisodesWithEvents.Where(s => s.PatientId == surveyCensus.Id && s.EpisodeId == previousEpisode.EpisodeId && s.EventDate <= previousEpisode.EndDate && s.EventDate >= previousEpisode.EndDate.AddDays(-5) && disciplineTasksOASISROCAndRecert.Contains(s.DisciplineTask)).OrderByDescending(s => s.EventDate).FirstOrDefault();
                                    if (previousSchedule != null && previousSchedule.Note.IsNotNullOrEmpty())
                                    {
                                        var diagnosis = previousSchedule.Diagnosis();
                                        if (diagnosis != null && diagnosis.Count > 0)
                                        {
                                            surveyCensus.PrimaryDiagnosis = (diagnosis.ContainsKey("M1020PrimaryDiagnosis") && diagnosis["M1020PrimaryDiagnosis"].Answer.IsNotNullOrEmpty() ? diagnosis["M1020PrimaryDiagnosis"].Answer : string.Empty);
                                            surveyCensus.SecondaryDiagnosis = (diagnosis.ContainsKey("M1022PrimaryDiagnosis1") && diagnosis["M1022PrimaryDiagnosis1"].Answer.IsNotNullOrEmpty() ? diagnosis["M1022PrimaryDiagnosis1"].Answer : string.Empty);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (isExcel)
                    {
                        var user = users.SingleOrDefault(u => u.Id == surveyCensus.CaseManagerId);
                        if (user != null)
                        {
                            surveyCensus.CaseManagerDisplayName = user.DisplayName;
                        }
                        if (surveyCensus.InsuranceId.IsNotNullOrEmpty() && surveyCensus.InsuranceId.IsInteger())
                        {
                            if (surveyCensus.InsuranceId.ToInteger() >= 1000)
                            {
                                var insurance = insurances.SingleOrDefault(i => i.Id.ToString() == surveyCensus.InsuranceId);
                                if (insurance != null)
                                {
                                    surveyCensus.InsuranceName = insurance.Name;
                                }
                            }
                            else if (surveyCensus.InsuranceId.ToInteger() < 1000 && surveyCensus.InsuranceId.ToInteger() > 0)
                            {
                                var insurance = standardInsurances.SingleOrDefault(i => i.Id.ToString() == surveyCensus.InsuranceId);
                                if (insurance != null)
                                {
                                    surveyCensus.InsuranceName = insurance.Name;
                                }
                            }
                        }
                    }
                });
            }
            return surveyCensuses;
        }

        public List<PatientRoster> GetPatientAnnualAdmission(Guid branchCode, int statusId, int year)
        {
            var rosterList = patientRepository.GetPatientByAdmissionYear(Current.AgencyId, branchCode, statusId, year);
            return rosterList.OrderBy(o => o.PatientFirstName).ToList();
        }

        #endregion

        #region Clinical Reports

        public IList<OpenOasis> GetAllOpenOasis(Guid branchCode, DateTime startDate, DateTime endDate)
        {
            var openOasisList = new List<OpenOasis>();
            var oasis = Enum.GetValues(typeof(DisciplineTasks)).Cast<DisciplineTasks>().Where(d => d.GetCustomCategory().IsEqual("OASIS")).Select(d => (int)d).ToArray();
            var scheduleEvents = patientRepository.GetScheduleByBranchStatusDisciplineAndDateRange(Current.AgencyId, branchCode, startDate, endDate, 0, new string[] { }, oasis, ScheduleStatusFatory.OpenOASISStatus().ToArray(), false); //new List<ScheduleEvent>();
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                scheduleEvents.ForEach(e =>
                {
                    var user = users.SingleOrDefault(u => u.Id == e.UserId);
                    var openOasis = new OpenOasis();
                    openOasis.PatientIdNumber = e.PatientIdNumber;
                    openOasis.PatientName = e.PatientName;
                    openOasis.AssessmentName = e.DisciplineTaskName;
                    openOasis.Status = e.StatusName;
                    openOasis.Date = e.EventDate.ToString("MM/dd/yyyy");
                    openOasis.CurrentlyAssigned = user != null ? user.DisplayName : string.Empty;
                    openOasisList.Add(openOasis);
                });
            }
            return openOasisList.OrderBy(o => o.PatientName).ToList();
        }

        public List<MissedVisit> GetAllMissedVisit(Guid branchCode, DateTime startDate, DateTime endDate)
        {
            var missedVisitList = new List<MissedVisit>();
            var schedules = patientRepository.GetMissedVisitSchedulesLean(Current.AgencyId, branchCode, startDate, endDate, 0, new string[] { }, new int[] { }, new int[] { });
            if (schedules != null && schedules.Count > 0)
            {
                var userIds = schedules.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                schedules.ForEach(e =>
                {
                    var user = users.SingleOrDefault(u => u.Id == e.UserId);
                    var missedVisit = new MissedVisit();
                    missedVisit.PatientIdNumber = e.PatientIdNumber;
                    missedVisit.PatientName = e.PatientName;
                    missedVisit.Date = e.EventDate;
                    missedVisit.DisciplineTaskName = e.DisciplineTaskName;
                    missedVisit.UserName = user != null ? user.DisplayName : string.Empty;
                    missedVisitList.Add(missedVisit);
                });
            }
            return missedVisitList;
        }

        public IList<PhysicianOrder> GetPhysicianOrderHistory(Guid branchCode, int status, DateTime startDate, DateTime endDate)
        {
            var branchPhysicianOrder = new List<PhysicianOrder>();
            var schedules = patientRepository.GetPhysicianOrderScheduleEvents(Current.AgencyId, startDate, endDate, status > 0 ? new int[] { status } : new int[] { });
            if (schedules != null && schedules.Count > 0)
            {
                var physicianOrdersIds = schedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                branchPhysicianOrder = patientRepository.GetPhysicianOrders(Current.AgencyId, physicianOrdersIds, startDate, endDate);
            }
            return branchPhysicianOrder.OrderBy(o => o.DisplayName).ToList();
        }

        public IList<Order> GetPlanOfCareHistory(Guid branchCode, int status, DateTime startDate, DateTime endDate)
        {
            var orders = new List<Order>();
            var disciplineTasks = new int[] { (int)DisciplineTasks.HCFA485, (int)DisciplineTasks.NonOasisHCFA485, (int)DisciplineTasks.HCFA485StandAlone };
            var schedules = patientRepository.GetPlanOfCareOrderScheduleEvents(Current.AgencyId, startDate, endDate, disciplineTasks, status > 0 ? new int[] { status } : new int[] { });
            if (schedules != null && schedules.Count > 0)
            {
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPlanofCares(Current.AgencyId, planofCareOrdersIds);
                    if (planofCareOrders != null && planofCareOrders.Count > 0)
                    {
                        var physicianIds = planofCareOrders.Where(s => !s.PhysicianId.IsEmpty()).Select(s => s.PhysicianId).Distinct().ToList();
                        var physicians = physicianRepository.GetPhysiciansByIdsLean(Current.AgencyId, physicianIds);
                        planofCareOrders.ForEach(poc =>
                        {
                            var physician = physicians.SingleOrDefault(u => u.Id == poc.PhysicianId); 
                            var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485,
                                    Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate = evnt.EventDate.ToString("MM/dd/yyyy"),
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SendDate = poc.SentDate
                                });
                            }
                        });
                    }
                }
                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPlanofCaresStandAlones(Current.AgencyId, planofCareStandAloneOrdersIds);
                    if (planofCareStandAloneOrders != null && planofCareStandAloneOrders.Count > 0)
                    {
                        var physicianIds = planofCareStandAloneOrders.Where(s => !s.PhysicianId.IsEmpty()).Select(s => s.PhysicianId).Distinct().ToList();
                        var physicians = physicianRepository.GetPhysiciansByIdsLean(Current.AgencyId, physicianIds);
                        planofCareStandAloneOrders.ForEach(poc =>
                        {
                            var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                            if (evnt != null)
                            {
                                var physician = physicians.SingleOrDefault(u => u.Id == poc.PhysicianId); 
                                orders.Add(new Order
                                {
                                    Id = poc.Id,
                                    Type = OrderType.HCFA485StandAlone,
                                    Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                    Number = poc.OrderNumber,
                                    PatientName = poc.PatientName,
                                    PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                    CreatedDate = evnt.EventDate.ToString("MM/dd/yyyy"),
                                    ReceivedDate = poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate,
                                    SendDate = poc.SentDate
                                });
                            }
                        });
                    }
                }
            }
            return orders.OrderBy(o => o.PatientName).ToList();
        }

        #endregion

        #region Schedule Reports

        public List<ScheduleEvent> GetPatientScheduleEventsByDateRange(Guid patientId, DateTime fromDate, DateTime toDate)
        {
            var scheduleEvents = patientRepository.GetScheduledEventsOnlyLean(Current.AgencyId, patientId, fromDate, toDate, new int[] { }, new int[] { }, true);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                scheduleEvents.ForEach(s =>
                {
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    if (user != null)
                    {
                        s.UserFirstName = user.FirstName;
                        s.UserLastName = user.LastName;
                        s.UserSuffix = user.Suffix;
                        s.Credentials = user.Credentials;
                        s.IsUserDeprecated = user.IsDeprecated;
                    }
                });
            }
            return scheduleEvents;
        }

        public List<UserVisit> GetUserScheduleEventsByDateRange(Guid branchCode, Guid userId, DateTime from, DateTime to)
        {
            return patientRepository.GetUserVisitLean(Current.AgencyId, userId, from, to, 0, new int[] { }, false);
        }

        public List<ScheduleEvent> GetPastDueScheduleEvents(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var scheduleEvents = new List<ScheduleEvent>();
            if (endDate.Date >= startDate && startDate.Date < DateTime.Now.Date)
            {
                endDate = endDate.Date >= DateTime.Now.Date ? DateTime.Now.AddDays(-1) : endDate;
                scheduleEvents = patientRepository.GetScheduleByBranchStatusDisciplineAndDateRange(Current.AgencyId, branchId, startDate, endDate, 0, new string[] { }, new int[] { },ScheduleStatusFatory.AllNoteNotYetStarted().ToArray() , false); //new List<ScheduleEvent>();new int[] { (int)ScheduleStatus.NoteNotYetDue, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OrderNotYetDue }
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                    scheduleEvents.ForEach(s =>
                    {
                        var user = users.SingleOrDefault(u => u.Id == s.UserId);
                        if (user != null)
                        {
                            s.UserFirstName = user.FirstName;
                            s.UserLastName = user.LastName;
                            s.UserSuffix = user.Suffix;
                            s.Credentials = user.Credentials;
                            s.IsUserDeprecated = user.IsDeprecated;
                        }
                    });
                }
            }
            return scheduleEvents.OrderBy(s => s.PatientName).ToList();
        }

        public List<ScheduleEvent> GetPastDueScheduleEventsByDiscipline(Guid branchId, string discipline, DateTime startDate, DateTime endDate)
        {
            var scheduleEvents = new List<ScheduleEvent>();
            if (endDate.Date >= startDate && startDate.Date < DateTime.Now.Date)
            {
                endDate = endDate.Date >= DateTime.Now.Date ? DateTime.Now.AddDays(-1) : endDate;
                scheduleEvents = patientRepository.GetScheduleByBranchStatusDisciplineAndDateRange(Current.AgencyId, branchId, startDate, endDate, 0, new string[] { discipline }, new int[] { }, ScheduleStatusFatory.AllNoteNotYetStarted().ToArray(), false); //new int[] { (int)ScheduleStatus.NoteNotYetDue, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OrderNotYetDue }
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                    var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                    scheduleEvents.ForEach(s =>
                    {
                        var user = users.SingleOrDefault(u => u.Id == s.UserId);
                        if (user != null)
                        {
                            s.UserFirstName = user.FirstName;
                            s.UserLastName = user.LastName;
                            s.UserSuffix = user.Suffix;
                            s.Credentials = user.Credentials;
                            s.IsUserDeprecated = user.IsDeprecated;
                        }
                    });
                }
            }
            return scheduleEvents.OrderBy(s => s.PatientName).ToList();
        }

        public List<ScheduleEvent> GetScheduleEventsByDateRange(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var scheduleEvents = patientRepository.GetScheduleByBranchStatusDisciplineAndDateRange(Current.AgencyId, branchId, startDate, endDate, 0, new string[] { }, new int[] { }, new int[] { }, true);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                scheduleEvents.ForEach(s =>
                {
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    if (user != null)
                    {
                        s.UserFirstName = user.FirstName;
                        s.UserLastName = user.LastName;
                        s.UserSuffix = user.Suffix;
                        s.Credentials = user.Credentials;
                        s.IsUserDeprecated = user.IsDeprecated;
                    }
                });
            }
            return scheduleEvents;
        }

        public List<ScheduleEvent> GetCaseManagerScheduleByBranch(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var scheduleEvents = patientRepository.GetScheduleByBranchDateRangeAndStatusLean(Current.AgencyId, branchId, startDate, endDate, 0, ScheduleStatusFatory.CaseManagerStatus().ToArray(), false);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                scheduleEvents.ForEach(s =>
                {
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    if (user != null)
                    {
                        s.UserFirstName = user.FirstName;
                        s.UserLastName = user.LastName;
                        s.UserSuffix = user.Suffix;
                        s.Credentials = user.Credentials;
                        s.IsUserDeprecated = user.IsDeprecated;
                    }
                });
            }
            return scheduleEvents;
        }

        public List<ScheduleEvent> GetScheduleDeviation(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var scheduleEvents = patientRepository.GetScheduleDeviations(Current.AgencyId, branchId, startDate, endDate, 0, new string[] { }, new int[] { }, new int[] { }, true); // new List<ScheduleEvent>();;
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                scheduleEvents.ForEach(s =>
                {
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    if (user != null)
                    {
                        s.UserFirstName = user.FirstName;
                        s.UserLastName = user.LastName;
                        s.UserSuffix = user.Suffix;
                        s.Credentials = user.Credentials;
                        s.IsUserDeprecated = user.IsDeprecated;
                    }
                });
            }
            return scheduleEvents;
        }

        #endregion

        #region Billing Reports

        public List<TypeOfBill> UnProcessedBillViewData(Guid branchId, string type)
        {
            var listOfbill = new List<TypeOfBill>();
            if (type.IsEqual("RAP"))
            {
                listOfbill = billingRepository.GetRapsByStatus(Current.AgencyId, (int)BillingStatus.ClaimCreated);
            }
            else if (type.IsEqual("Final"))
            {
                listOfbill = billingRepository.GetFinalsByStatus(Current.AgencyId, (int)BillingStatus.ClaimCreated);
            }
            else
            {
                var raps = billingRepository.GetRapsByStatus(Current.AgencyId, (int)BillingStatus.ClaimCreated);
                if (raps != null && raps.Count > 0)
                {
                    listOfbill.AddRange(raps);
                }
                var finals = billingRepository.GetFinalsByStatus(Current.AgencyId, (int)BillingStatus.ClaimCreated);
                if (finals != null && finals.Count > 0)
                {
                    listOfbill.AddRange(finals);
                }
            }
            return listOfbill.OrderBy(b => b.LastName).ThenBy(b => b.FirstName).ToList();
        }

        public List<ClaimLean> BillViewDataByStatus(Guid branchId, string type, int status, DateTime startDate, DateTime endDate)
        {
            var listOfBill = new List<ClaimLean>();
            if (type.IsEqual("RAP"))
            {
                listOfBill = billingRepository.GetRapClaims(Current.AgencyId, branchId, status, startDate, endDate);
            }
            else if (type.IsEqual("Final"))
            {
                listOfBill = billingRepository.GetFinalClaims(Current.AgencyId, branchId, status, startDate, endDate);
            }
            else
            {
                var raps = billingRepository.GetRapClaims(Current.AgencyId, branchId, status, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);
                }
                var finals = billingRepository.GetFinalClaims(Current.AgencyId, branchId, status, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            return listOfBill.OrderBy(b => b.DisplayName).ToList();
        }

        public List<ClaimLean> SubmittedBillViewDataByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate)
        {
            var listOfBill = new List<ClaimLean>();
            if (type.IsEqual("RAP"))
            {
                listOfBill = billingRepository.GetRapClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
            }
            else if (type.IsEqual("Final"))
            {
                listOfBill = billingRepository.GetFinalClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
            }
            else
            {
                var raps = billingRepository.GetRapClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (raps != null && raps.Count >= 0)
                {
                    listOfBill.AddRange(raps);
                }
                var finals = billingRepository.GetFinalClaimsBySubmissionDate(Current.AgencyId, branchId, startDate, endDate);
                if (finals != null && finals.Count > 0)
                {
                    listOfBill.AddRange(finals);
                }
            }
            return listOfBill.OrderBy(b => b.DisplayName).ToList();
        }

        #endregion

        #region Employee Reports

        public List<User> GetEmployeeRoster(Guid branchCode, int status)
        {
            var users = userRepository.GetEmployeeRoster(Current.AgencyId, branchCode, status);
            return users.OrderBy(e => e.DisplayName).ToList();
        }

        public List<Birthday> GetEmployeeBirthdays(Guid branchCode, int status, int month)
        {
            var birthdays = new List<Birthday>();
            var users = new List<User>();
            if (status == 0)
            {
                if (branchCode.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchCode, Current.AgencyId).ToList();
                }
            }
            else
            {
                if (branchCode.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId, status).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchCode, Current.AgencyId, status).ToList();
                }
            }
            if (users != null)
            {
                users.ForEach(user =>
                {
                    if (user.ProfileData.IsNotNullOrEmpty())
                    {
                        user.Profile = user.ProfileData.ToObject<UserProfile>();
                        user.EmailAddress = user.Profile.EmailWork;
                    }
                    if (user.Profile != null && user.Profile.DOB.IsValid() && user.Profile.DOB.Month == month)
                    {
                        birthdays.Add(new Birthday { Name = user.DisplayName, Date = user.Profile.DOB, AddressLine1 = user.Profile.AddressLine1, AddressLine2 = user.Profile.AddressLine2, AddressCity = user.Profile.AddressCity, AddressStateCode = user.Profile.AddressZipCode, PhoneHome = user.Profile.PhoneHome });
                    }
                });
            }
            return birthdays;
        }

        public List<License> GetEmployeeExpiringLicenses(Guid branchCode, int status)
        {
            var license = new List<License>();
            var users = new List<User>();
            if (status == 0)
            {
                if (branchCode.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchCode, Current.AgencyId).ToList();
                }
            }
            else
            {
                if (branchCode.IsEmpty())
                {
                    users = userRepository.GetUsersOnly(Current.AgencyId, status).ToList();
                }
                else
                {
                    users = userRepository.GetUsersOnlyByBranch(branchCode, Current.AgencyId, status).ToList();
                }
            }
            if (users != null)
            {
                users.ForEach(user =>
                {
                    if (user.Licenses.IsNotNullOrEmpty())
                    {
                        var userLicenses = user.Licenses.ToObject<List<License>>();

                        if (userLicenses != null)
                        {
                            userLicenses.ForEach(l =>
                            {
                                if (l.ExpirationDate <= DateTime.Now.AddDays(60))
                                {
                                    l.UserDisplayName = user.DisplayName;
                                    license.Add(l);
                                }
                            });
                        }
                    }
                });
            }
            return license;
        }

        public List<UserVisit> GetEmployeeScheduleByDateRange(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var userschedules = patientRepository.GetUserVisitLeanByBranchStatusDisciplineAndDateRange(Current.AgencyId, branchId, startDate, endDate, 0, new string[] { }, new int[] { }, new int[] { }, true);
            if (userschedules != null && userschedules.Count > 0)
            {
                var userIds = userschedules.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                userschedules.ForEach(s =>
                {
                    var user = users.SingleOrDefault(u => u.Id == s.UserId);
                    if (user != null)
                    {
                        s.UserDisplayName = user.DisplayName;
                    }
                });
            }
            return userschedules.OrderBy(e => e.UserDisplayName).ToList();
        }

        #endregion

        #region Statistical Reports

        public List<UserVisit> GetEmployeeVisistList(Guid userId, DateTime startDate, DateTime endDate)
        {
            var userSchedules = patientRepository.GetUserVisitLean(Current.AgencyId, userId, startDate, endDate, 0, new int[] { }, true);
            return userSchedules;
        }

        public List<Patient> GetPatientByAdmissionUnduplicatedByDateRange(Guid branchId, int status, DateTime startDate, DateTime endDate)
        {
            var patients = new List<Patient>();
            var patientWithAllAdmission = patientRepository.GetPatientWithAdmissionsByDateRange(Current.AgencyId, branchId, status, startDate, endDate);
            if (patientWithAllAdmission != null && patientWithAllAdmission.Count > 0)
            {
                 patients = patientWithAllAdmission.GroupBy(p => p.Id).Select(s => s.OrderByDescending(se => se.StartofCareDate.Date).First()).OrderBy(s=>s.FirstName).ThenBy(s=>s.LastName).ToList();
            }
            return patients;
        }

        public List<PatientRoster> GetPatientByAdmissionMonthYear(Guid branchId, int status, int month, int year)
        {
            var patientRosters = new List<PatientRoster>();
            var patients = patientRepository.GetPatientByAdmissionMonthYear(Current.AgencyId, branchId, status, month, year);
            if (patients != null && patients.Count > 0)
            {
                var userIds = patients.Where(p => !p.InternalReferral.IsEmpty()).Select(p => p.InternalReferral).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);

                var physicianIds = patients.Where(p => !p.ReferrerPhysician.IsEmpty()).Select(p => p.ReferrerPhysician).Distinct().ToList();
                var physicians = physicianRepository.GetPhysiciansByIdsLean(Current.AgencyId, physicianIds);
                patients.ForEach(p =>
                {
                    var user = users.SingleOrDefault(u => u.Id == p.InternalReferral);
                    var physician = physicians.SingleOrDefault(u => u.Id == p.ReferrerPhysician);
                    patientRosters.Add(new PatientRoster
                    {
                        PatientStatus = p.Status,
                        PatientId = p.PatientIdNumber,
                        AdmissionSource = p.AdmissionSource,
                        PatientLastName = p.LastName,
                        PatientFirstName = p.FirstName,
                        OtherReferralSource = p.OtherReferralSource,
                        InternalReferral = user != null ? user.DisplayName : string.Empty,
                        ReferrerPhysician = physician != null ? physician.DisplayName : string.Empty,
                        ReferralDate = p.ReferralDateFormatted,
                        PatientSoC = p.StartOfCareDateFormatted
                    });

                });
            }
            return patientRosters;
        }


        #endregion

    }
}
