﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Text;
    using System.Web.Mvc;
    using System.Linq;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Drawing2D;
    using System.Collections.Generic;
    using System.Web.Script.Serialization;

    using Enums;
    using Common;
    using Domain;
    using ViewData;
    using Extensions;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Log.Enums;

    using Axxess.OasisC.Enums;
    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Extensions;
    using Axxess.OasisC.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;

    using Axxess.Log.Repositories;

    using Axxess.Membership.Logging;
    using Axxess.Log.Domain;
    using Axxess.Log.Common;

    public class PatientService : IPatientService
    {
        #region Private Members

        private readonly IDrugService drugService;
        private readonly ILogRepository logRepository;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ILookupRepository lookupRepository;
        private readonly IAssessmentService assessmentService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPlanofCareRepository planofCareRepository;
        private readonly IAssessmentRepository assessmentRepository;
        private readonly IReferralRepository referralRepository;
        #endregion

        #region Constructor

        public PatientService(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, ILogDataProvider logDataProvider, IOasisCDataProvider oasisDataProvider, IAssessmentService assessmentService, IDrugService drugService)
        {
            Check.Argument.IsNotNull(drugService, "drugService");
            Check.Argument.IsNotNull(assessmentService, "assessmentService");
            Check.Argument.IsNotNull(lookupDataProvider, "lookupDataProvider");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.drugService = drugService;
            this.assessmentService = assessmentService;
            this.logRepository = logDataProvider.LogRepository;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.assetRepository = agencyManagementDataProvider.AssetRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.billingRepository = agencyManagementDataProvider.BillingRepository;
            this.referralRepository = agencyManagementDataProvider.ReferralRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.planofCareRepository = oasisDataProvider.PlanofCareRepository;
            this.assessmentRepository = oasisDataProvider.OasisAssessmentRepository;
        }

        #endregion

        #region IPatientService Members

        public bool AddPatient(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            patient.AdmissionId = admissionDateId;
            if (patientRepository.Add(patient, out patientOut))
            {
                if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, PatientData = patientOut != null ? patientOut.ToXml() : string.Empty, StartOfCareDate = patient.StartofCareDate, IsActive = true, IsDeprecated = false, Status = patientOut.Status }))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdded, (patient.ShouldCreateEpisode && !patient.UserId.IsEmpty()) && patient.Status == (int)PatientStatus.Active ? string.Empty : "Pending for admission");
                    result = true;
                }
            }
            return result;
        }

        public bool EditPatient(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            var patientToEdit = patientRepository.GetPatientOnly(patient.Id, Current.AgencyId);
            if (patientToEdit != null)
            {
                var oldAdmissionDateId = patientToEdit.AdmissionId;
                patient.AdmissionId = patientToEdit.AdmissionId.IsEmpty() ? admissionDateId : patientToEdit.AdmissionId;
                if (patientRepository.Edit(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                            result = true;
                        }
                        else
                        {
                            patientToEdit.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patientToEdit);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientToEdit.Id, oldAdmissionDateId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.StartOfCareDate = patientOut.StartofCareDate;
                            if (patient.Status == (int)PatientStatus.Discharged)
                            {
                                admissionData.DischargedDate = patientOut.DischargeDate;
                            }
                            admissionData.PatientData = patientOut.ToXml();
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientToEdit.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patientToEdit);
                            }
                        }
                        else
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged? patient.DischargeDate:DateTime.MinValue, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientToEdit.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patientToEdit);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdatePatientForPhotoRemove(Patient patient)
        {
            var result = false;
            Patient patientOut = null;
            var photoId = patient.PhotoId;
            patient.PhotoId = Guid.Empty;
            if (patientRepository.Update(patient, out patientOut))
            {
                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientOut.Id, patientOut.AdmissionId);
                if (admissionData != null && patientOut != null)
                {
                    admissionData.PatientData = patientOut.ToXml();
                    if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
                        result = true;
                    }
                    else
                    {
                        patient.PhotoId = photoId;
                        patientRepository.Update(patient);
                    }
                }
                else
                {
                    patient.PhotoId = photoId;
                    patientRepository.Update(patient);
                }
            }
            return result;
        }

        public bool DeletePatient(Guid Id)
        {
            var result = false;
            if (patientRepository.DeprecatedPatient(Current.AgencyId, Id))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, Id, Id.ToString(), LogType.Patient, LogAction.PatientDeleted, string.Empty);
                result = true;
            }
            return result;
        }

        public bool AdmitPatient(PendingPatient pending)
        {
            var result = false;
            Patient patientOut = null;
            var admissionDateId = Guid.NewGuid();
            if (pending.Type == NonAdmitTypes.Patient)
            {
                var patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
                if (patient != null)
                {
                    patient.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;
                    var oldAdmissionDateId = patient.AdmissionId;
                    pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                    if (patientRepository.AdmitPatient(pending, out patientOut))
                    {
                        if (patientOut != null)
                        {
                            if (oldAdmissionDateId.IsEmpty())
                            {
                                if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
                                {
                                    var medId = Guid.NewGuid();
                                    patient.EpisodeStartDate = pending.EpisodeStartDate;
                                    if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                    {
                                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
                                        result = true;
                                    }
                                    else
                                    {
                                        patientRepository.DeletePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
                                        patientRepository.Update(patient);
                                    }
                                }
                                else
                                {
                                    patientRepository.Update(patient);
                                }
                            }
                            else
                            {
                                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                                if (admissionData != null)
                                {
                                    var oldPatientData = admissionData.PatientData;
                                    var oldSoc = admissionData.StartOfCareDate;
                                    admissionData.PatientData = patientOut.ToXml();
                                    admissionData.StartOfCareDate = pending.StartofCareDate;
                                    if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                    {
                                        var medId = Guid.NewGuid();
                                        patient.EpisodeStartDate = pending.EpisodeStartDate;
                                        if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                        {
                                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Removed");
                                            result = true;
                                        }
                                        else
                                        {
                                            admissionData.PatientData = oldPatientData;
                                            admissionData.StartOfCareDate = oldSoc;
                                            patientRepository.UpdatePatientAdmissionDate(admissionData);
                                            patientRepository.Update(patient);
                                        }
                                    }
                                    else
                                    {
                                        patientRepository.Update(patient);
                                    }
                                }
                                else
                                {
                                    if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Admitted.", IsDeprecated = false, IsActive = true }))
                                    {
                                        var medId = Guid.NewGuid();
                                        patient.EpisodeStartDate = pending.EpisodeStartDate;
                                        if (this.CreateMedicationProfile(patient, medId) && this.CreateEpisodeAndClaims(patient))
                                        {
                                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmitted, string.Empty);
                                            result = true;
                                        }
                                        else
                                        {
                                            patientRepository.DeletePatientAdmissionDate(Current.AgencyId, patient.Id, pending.AdmissionId);
                                            patientRepository.Update(patient);
                                        }
                                    }
                                    else
                                    {
                                        patientRepository.Update(patient);
                                    }
                                }
                            }
                        }
                        else
                        {
                            patientRepository.Update(patient);
                        }
                    }
                }
            }
            else if (pending.Type == NonAdmitTypes.Referral)
            {
                var referral = referralRepository.Get(Current.AgencyId,pending.Id);
                if (referral != null)
                {
                    pending.AdmissionId =  admissionDateId ;
                    if (patientRepository.AdmitPatient(pending, out patientOut))
                    {
                        if (patientOut != null)
                        {
                            var physicians = referral.Physicians.IsNotNullOrEmpty() ? referral.Physicians.ToObject<List<Physician>>() : new List<Physician>();
                            if (physicians != null && physicians.Count > 0)
                            {
                                physicians.ForEach(p =>
                                {
                                    physicianRepository.Link(referral.Id, p.Id, p.IsPrimary);
                                });
                            }
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = pending.Id, StartOfCareDate = pending.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Referral Admitted.", IsDeprecated = false, IsActive = true }))
                            {
                                var medId = Guid.NewGuid();
                                patientOut.EpisodeStartDate = pending.EpisodeStartDate;
                                patientOut.IsFaceToFaceEncounterCreated = pending.IsFaceToFaceEncounterCreated;
                                if (this.CreateMedicationProfile(patientOut, medId) && this.CreateEpisodeAndClaims(patientOut))
                                {
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, referral.Id, referral.Id.ToString(), LogType.Patient, LogAction.ReferralAdmitted, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    patientRepository.DeletePatientAdmissionDate(Current.AgencyId, referral.Id, pending.AdmissionId);
                                    referralRepository.UpdateModal(referral);
                                }
                            }
                            else
                            {
                                referralRepository.UpdateModal(referral);
                            }
                        }
                        else
                        {
                            referralRepository.UpdateModal(referral);
                        }
                    }
                }
            }
            return result;
        }

        public bool NonAdmitPatient(PendingPatient pending)
        {
            var result = false;
            Patient patientOut = null;
            var patient = patientRepository.GetPatientOnly(pending.Id, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                pending.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.NonAdmitPatient(pending, out patientOut))
                {
                    if (patient.AdmissionId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                            result = true;
                        }
                        else
                        {
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patientOut.Status;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {

                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patientOut.Id, StartOfCareDate = patientOut.StartofCareDate, DischargedDate = patientOut.Status == (int)PatientStatus.Discharged ? patientOut.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = "Patient Set Non-admitted.", IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetNonAdmit, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patientRepository.Update(patient);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool SetPatientPending(Guid patientId)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                Patient patientOut = null;
                var oldStatus = patient.Status;
                patient.Status = (int)PatientStatus.Pending;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.Status == (int)PatientStatus.Discharged ? patient.DischargeDate : DateTime.MinValue, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Set Pending.", IsDeprecated = false, IsActive = true }))
                        {
                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                            result = true;
                        }
                        else
                        {
                            patient.Status = oldStatus;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = oldAdmissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patientOut.StartofCareDate, PatientData = patientOut.ToXml(), Status = patientOut.Status, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientSetPending, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool DischargePatient(Guid patientId, DateTime dischargeDate, string dischargeReason)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                var oldStatus = patient.Status;
                var oldDischargeDate = patient.DischargeDate;
                var oldDischargeReason = patient.DischargeReason;
                patient.DischargeDate = dischargeDate;
                patient.Status = (int)PatientStatus.Discharged;
                patient.DischargeReason = dischargeReason;
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                Patient patientOut = null;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                        {
                            var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, dischargeDate);
                            if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                            {
                                if (episode != null)
                                {
                                    var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                    if (final != null && BillingStatusFactory.UnProcessed().Contains(final.Status))
                                    {
                                        final.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateFinal(final);
                                    }
                                    var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                    if (rap != null && BillingStatusFactory.UnProcessed().Contains(rap.Status))
                                    {
                                        rap.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateRap(rap);
                                    }
                                }
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                            }
                            result = true;
                        }
                        else
                        {
                            patient.DischargeDate = oldDischargeDate;
                            patient.Status = oldStatus;
                            patient.DischargeReason = oldDischargeReason;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            admissionData.StartOfCareDate = patient.StartofCareDate;
                            admissionData.DischargedDate = patient.DischargeDate;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, dischargeDate);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && BillingStatusFactory.UnProcessed().Contains(final.Status))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && BillingStatusFactory.UnProcessed().Contains(rap.Status))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                result = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                            {
                                var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, dischargeDate);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && BillingStatusFactory.UnProcessed().Contains(final.Status))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && BillingStatusFactory.UnProcessed().Contains(rap.Status))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                result = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }

                        }
                    }
                }
            }
            return result;
        }

        public bool DischargePatient(Guid patientId, Guid episodeId, DateTime dischargeDate, string dischargeReason)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var admissionDateId = Guid.NewGuid();
                var oldStatus = patient.Status;
                var oldDischargeDate = patient.DischargeDate;
                var oldDischargeReason = patient.DischargeReason;
                patient.DischargeDate = dischargeDate;
                patient.Status = (int)PatientStatus.Discharged;
                patient.DischargeReason = dischargeReason;
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                Patient patientOut = null;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (oldAdmissionDateId.IsEmpty())
                    {
                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                        {
                            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                            if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                            {
                                if (episode != null)
                                {
                                    var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                    if (final != null && BillingStatusFactory.UnProcessed().Contains(final.Status))
                                    {
                                        final.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateFinal(final);
                                    }
                                    var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                    if (rap != null && BillingStatusFactory.UnProcessed().Contains(rap.Status))
                                    {
                                        rap.EpisodeEndDate = dischargeDate;
                                        billingRepository.UpdateRap(rap);
                                    }
                                }
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                            }
                            result = true;
                        }
                        else
                        {
                            patient.DischargeDate = oldDischargeDate;
                            patient.Status = oldStatus;
                            patient.DischargeReason = oldDischargeReason;
                            patient.AdmissionId = oldAdmissionDateId;
                            patientRepository.Update(patient);
                        }
                    }
                    else
                    {
                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                        if (admissionData != null && patientOut != null)
                        {
                            admissionData.PatientData = patientOut.ToXml();
                            admissionData.Status = patient.Status;
                            admissionData.StartOfCareDate = patient.StartofCareDate;
                            admissionData.DischargedDate = patient.DischargeDate;
                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                            {
                                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && BillingStatusFactory.UnProcessed().Contains(final.Status))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && BillingStatusFactory.UnProcessed().Contains(rap.Status))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                result = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            if (patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, DischargedDate = patient.DischargeDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = dischargeReason, IsDeprecated = false, IsActive = true }))
                            {
                                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                                if (this.UpdateEpisodeForDischarge(patientId, dischargeDate, episode))
                                {
                                    if (episode != null)
                                    {
                                        var final = billingRepository.GetFinal(Current.AgencyId, episode.Id);
                                        if (final != null && BillingStatusFactory.UnProcessed().Contains(final.Status))
                                        {
                                            final.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateFinal(final);
                                        }
                                        var rap = billingRepository.GetRap(Current.AgencyId, episode.Id);
                                        if (rap != null && BillingStatusFactory.UnProcessed().Contains(rap.Status))
                                        {
                                            rap.EpisodeEndDate = dischargeDate;
                                            billingRepository.UpdateRap(rap);
                                        }
                                    }
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientDischarged, string.Empty);
                                }
                                result = true;
                            }
                            else
                            {
                                patient.DischargeDate = oldDischargeDate;
                                patient.Status = oldStatus;
                                patient.DischargeReason = oldDischargeReason;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool ActivatePatient(Guid patientId)
        {
            var result = false;
            Patient patientOut = null;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var oldStatus = patient.Status;
                patient.Status = (int)PatientStatus.Active;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                if (patientRepository.Update(patient, out patientOut))
                {
                    if (patientOut != null)
                    {
                        if (oldAdmissionDateId.IsEmpty())
                        {
                            if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                result = true;
                            }
                            else
                            {
                                patient.Status = oldStatus;
                                patient.AdmissionId = oldAdmissionDateId;
                                patientRepository.Update(patient);
                            }
                        }
                        else
                        {
                            var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                            if (admissionData != null)
                            {
                                admissionData.PatientData = patientOut.ToXml();
                                admissionData.Status = patient.Status;
                                admissionData.StartOfCareDate = patient.StartofCareDate;
                                if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                {
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    patient.Status = oldStatus;
                                    patient.AdmissionId = oldAdmissionDateId;
                                    patientRepository.Update(patient);
                                }
                            }
                            else
                            {
                                if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = patient.Status, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                                {
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientActivated, string.Empty);
                                    result = true;
                                }
                                else
                                {
                                    patient.Status = oldStatus;
                                    patient.AdmissionId = oldAdmissionDateId;
                                    patientRepository.Update(patient);
                                }
                            }
                        }
                    }
                    else
                    {
                        patient.Status = oldStatus;
                        patient.AdmissionId = oldAdmissionDateId;
                        patientRepository.Update(patient);
                    }
                }
            }
            return result;
        }

        public bool ActivatePatient(Guid patientId, DateTime startOfCareDate)
        {
            var result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            Patient patientOut = null;
            if (patient != null)
            {
                var oldStatus = patient.Status;
                var oldStartOfCareDate = patient.StartofCareDate;
                patient.Status = (int)PatientStatus.Active;
                patient.StartofCareDate = startOfCareDate;
                var admissionDateId = Guid.NewGuid();
                var oldAdmissionDateId = patient.AdmissionId;
                patient.AdmissionId = admissionDateId;
                if (patientRepository.Update(patient, out patientOut))
                {

                    if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = "Patient Activated.", IsDeprecated = false, IsActive = true }))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
                        result = true;
                    }
                    else
                    {
                        patient.Status = oldStatus;
                        patient.StartofCareDate = oldStartOfCareDate;
                        patient.AdmissionId = oldAdmissionDateId;
                        patientRepository.Update(patient);
                    }

                    //else
                    //{
                    //    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                    //    if (admissionData != null && patientOut != null)
                    //    {
                    //        admissionData.PatientData = patientOut.ToXml();
                    //        admissionData.Status = patient.Status;
                    //        admissionData.StartOfCareDate = patient.StartofCareDate;
                    //        if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                    //        {
                    //            Auditor.AddGeneralLog(LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientReadmitted, string.Empty);
                    //            result = true;
                    //        }
                    //        else
                    //        {
                    //            patient.Status = oldStatus;
                    //            patient.StartofCareDate = oldStartOfCareDate;
                    //            patient.AdmissionId = oldAdmissionDateId;
                    //            patientRepository.Update(patient);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        patient.Status = oldStatus;
                    //        patient.StartofCareDate = oldStartOfCareDate;
                    //        patient.AdmissionId = oldAdmissionDateId;
                    //        patientRepository.Update(patient);
                    //    }
                    //}
                }
            }
            return result;
        }

        public bool AddPhoto(Guid patientId, HttpFileCollectionBase httpFiles)
        {
            var result = false;

            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                if (httpFiles.Count > 0)
                {
                    HttpPostedFileBase file = httpFiles.Get("Photo1");
                    if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                    {
                        var photo = Image.FromStream(file.InputStream);
                        var resizedImageStream = ResizeAndEncodePhoto(photo, file.ContentType, false);
                        if (resizedImageStream != null)
                        {
                            var binaryReader = new BinaryReader(resizedImageStream);
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                Patient patientOut = null;
                                var oldPhotoId = patient.PhotoId;
                                var admissionDateId = Guid.NewGuid();
                                patient.PhotoId = asset.Id;
                                var oldAdmissionDateId = patient.AdmissionId;
                                patient.AdmissionId = patient.AdmissionId.IsEmpty() ? admissionDateId : patient.AdmissionId;
                                if (patientRepository.Update(patient, out patientOut))
                                {
                                    if (oldAdmissionDateId.IsEmpty())
                                    {
                                        if (patientOut != null && patientRepository.AddPatientAdmissionDate(new PatientAdmissionDate { Id = admissionDateId, AgencyId = Current.AgencyId, PatientId = patient.Id, StartOfCareDate = patient.StartofCareDate, PatientData = patientOut.ToXml(), Status = (int)PatientStatus.Active, Reason = string.Empty, IsDeprecated = false, IsActive = true }))
                                        {
                                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
                                            result = true;
                                        }
                                        else
                                        {
                                            patient.PhotoId = oldPhotoId;
                                            patient.AdmissionId = oldAdmissionDateId;
                                            patientRepository.Update(patient);
                                        }
                                    }
                                    else
                                    {
                                        var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patient.Id, patient.AdmissionId);
                                        if (admissionData != null && patientOut != null)
                                        {
                                            admissionData.PatientData = patientOut.ToXml();
                                            admissionData.Status = patient.Status;
                                            admissionData.StartOfCareDate = patient.StartofCareDate;
                                            if (patientRepository.UpdatePatientAdmissionDate(admissionData))
                                            {
                                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEdited, "Patient Photo Added/Updated");
                                                result = true;
                                            }
                                            else
                                            {
                                                patient.PhotoId = oldPhotoId;
                                                patient.AdmissionId = oldAdmissionDateId;
                                                patientRepository.Update(patient);
                                            }
                                        }
                                        else
                                        {
                                            patient.PhotoId = oldPhotoId;
                                            patient.AdmissionId = oldAdmissionDateId;
                                            patientRepository.Update(patient);
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool AddPrimaryEmergencyContact(Patient patient)
        {
            var result = false;
            if (patientRepository.AddEmergencyContact(patient.EmergencyContact) && patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patient.Id, patient.EmergencyContact.Id))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEmergencyContactAdded, "The Contact is set as primary.");
                result = true;
            }
            return result;
        }

        public PatientAdmissionDate GetIfExitOrCreate(Guid patientId)
        {
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            var admission = new PatientAdmissionDate();
            if (patient != null)
            {
                var admissionId = Guid.NewGuid();
                var oldAdmissionId = patient.AdmissionId;
                patient.AdmissionId = admissionId;
                admission = new PatientAdmissionDate
               {
                   Id = admissionId,
                   AgencyId = Current.AgencyId,
                   PatientId = patient.Id,
                   StartOfCareDate = patient.StartofCareDate,
                   DischargedDate = patient.DischargeDate,
                   Status = patient.Status,
                   IsActive = true,
                   IsDeprecated = false,
                   Created = DateTime.Now,
                   Modified = DateTime.Now,
                   PatientData = patient.ToXml()
               };
                if (patientRepository.Update(patient))
                {
                    if (patientRepository.AddPatientAdmissionDate(admission))
                    {
                        return admission;
                    }
                    else
                    {
                        patient.AdmissionId = oldAdmissionId;
                        patientRepository.Update(patient);
                    }
                }
            }
            return admission;
        }

        public bool NewEmergencyContact(PatientEmergencyContact emergencyContact, Guid patientId)
        {
            Check.Argument.IsNotNull(emergencyContact, "emergencyContact");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            bool result = false;
            if (patient != null)
            {
                emergencyContact.PatientId = patient.Id;
                emergencyContact.AgencyId = Current.AgencyId;
                if (patientRepository.AddEmergencyContact(emergencyContact))
                {
                    if (emergencyContact.IsPrimary)
                    {
                        patientRepository.SetPrimaryEmergencyContact(Current.AgencyId, patient.Id, emergencyContact.Id);
                    }
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientEmergencyContactAdded, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool EditEmergencyContact(PatientEmergencyContact emergencyContact)
        {
            bool result = false;
            if (patientRepository.EditEmergencyContact(Current.AgencyId, emergencyContact))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, emergencyContact.PatientId, emergencyContact.PatientId.ToString(), LogType.Patient, LogAction.PatientEmergencyContactEdited, emergencyContact.IsPrimary ? "The Contact is set as primary." : string.Empty);
                result = true;
            }
            return result;
        }

        public bool DeleteEmergencyContact(Guid Id, Guid patientId)
        {
            bool result = false;
            if (patientRepository.DeleteEmergencyContact(Id, patientId))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PatientEmergencyContactDeleted, string.Empty);
                result = true;
            }
            return result;
        }

        public PatientProfile GetProfile(Guid patientId)
        {
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            if (patient != null)
            {
                var patientProfile = new PatientProfile();
                patientProfile.Patient = patient;
                patientProfile.Agency = agencyRepository.GetWithBranches(Current.AgencyId);

                patientProfile.Allergies = GetAllergies(patientId);

                if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                {
                    patientProfile.Physician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary);
                }
                if (patient.EmergencyContacts != null && patient.EmergencyContacts.Count > 0)
                {
                    patientProfile.EmergencyContact = patient.EmergencyContacts.FirstOrDefault(p => p.IsPrimary);
                }
                if (!patient.CaseManagerId.IsEmpty())
                {
                    patient.CaseManagerName = UserEngine.GetName(patient.CaseManagerId, Current.AgencyId);
                }
                if (!patient.UserId.IsEmpty())
                {
                    patientProfile.Clinician = UserEngine.GetName(patient.UserId, Current.AgencyId);
                }

                SetInsurance(patient);

                var episode = patientRepository.GetCurrentEpisode(Current.AgencyId, patientId);
                if (episode != null)
                {
                    patientProfile.CurrentEpisode = episode;
                    var assessment = this.GetEpisodeAssessment(episode.Id, patient.Id);
                    if (assessment != null)
                    {
                       patientProfile.Frequencies = GetEpisodeFrequency(assessment);
                    }
                    else
                    {
                        patientProfile.Frequencies = string.Empty;
                    }
                    patientProfile.CurrentAssessment = assessment;
                }
                return patientProfile;
            }

            return null;
        }

        public string GetAllergies(Guid patientId)
        {
            var allergyProfile = patientRepository.GetAllergyProfileByPatient(patientId, Current.AgencyId);
            if (allergyProfile != null)
            {
                return allergyProfile.ToString();
            }
            return string.Empty;
        }

        public bool CreateMedicationProfile(Patient patient, Guid medId)
        {
            var medicationProfile = new MedicationProfile { AgencyId = Current.AgencyId, Id = medId, Medication = new List<Medication>().ToXml(), PharmacyName = patient.PharmacyName, PharmacyPhone = patient.PharmacyPhone, PatientId = patient.Id, Created = DateTime.Now, Modified = DateTime.Now };
            if (patientRepository.AddNewMedicationProfile(medicationProfile))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, medicationProfile.Id.ToString(), LogType.MedicationProfile, LogAction.MedicationProfileAdded, string.Empty);
                return true;
            }
            return false;
        }

        public bool AddMedication(Guid medicationProfileId, Medication medication, string medicationType)
        {
            bool result = false;
            if (medication != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
                if (medicationProfile != null)
                {
                    medication.Id = Guid.NewGuid();
                    medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                    medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                    if (medicationProfile.Medication.IsNullOrEmpty())
                    {
                        var newList = new List<Medication>() { medication };
                        medicationProfile.Medication = newList.ToXml();
                    }
                    else
                    {
                        var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                        existingList.Add(medication);
                        medicationProfile.Medication = existingList.ToXml();
                    }
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, LogAction.MedicationAdded, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateMedication(Guid medicationProfileId, Medication medication, string medicationType)
        {
            bool result = false;
            if (medication != null)
            {
                var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
                if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
                {
                    medication.MedicationType = new MedicationType { Value = medicationType, Text = ((MedicationTypeEnum)Enum.Parse(typeof(MedicationTypeEnum), medicationType, true)).GetDescription() };
                    var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                    if (existingList.Exists(m => m.Id == medication.Id))
                    {
                        var exisitingMedication = existingList.Single(m => m.Id == medication.Id);
                        if (exisitingMedication != null)
                        {
                            var logAction = LogAction.MedicationUpdated;
                            if (exisitingMedication.StartDate != medication.StartDate || exisitingMedication.Route != medication.Route || exisitingMedication.Frequency != medication.Frequency || exisitingMedication.MedicationDosage != medication.MedicationDosage)
                            {
                                medication.Id = Guid.NewGuid();
                                medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                                medication.MedicationType = new MedicationType { Value = "C", Text = MedicationTypeEnum.C.GetDescription() };
                                medication.MedicationCategory = exisitingMedication.MedicationCategory;
                                existingList.Add(medication);

                                exisitingMedication.MedicationCategory = "DC";
                                exisitingMedication.DCDate = DateTime.Now;
                                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                                logAction = LogAction.MedicationUpdatedWithDischarge;
                            }
                            else
                            {
                                exisitingMedication.IsLongStanding = medication.IsLongStanding;
                                exisitingMedication.MedicationType = medication.MedicationType;
                                exisitingMedication.Classification = medication.Classification;
                                if (exisitingMedication.MedicationCategory == "DC")
                                {
                                    exisitingMedication.DCDate = medication.DCDate;
                                    logAction = LogAction.MedicationDischarged;
                                }
                                medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                            }

                            if (patientRepository.UpdateMedication(medicationProfile))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateMedicationStatus(Guid medicationProfileId, Guid medicationId, string medicationCategory, DateTime dischargeDate)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingMedications = medicationProfile.Medication.ToObject<List<Medication>>();
                var medication = existingMedications != null ? existingMedications.SingleOrDefault(m => m.Id == medicationId) : null;
                if (medication != null)
                {
                    var logAction = new LogAction();
                    if (medicationCategory.IsEqual(MedicationCategoryEnum.DC.ToString()))
                    {
                        medication.DCDate = dischargeDate;
                        medication.MedicationCategory = MedicationCategoryEnum.DC.ToString();
                        logAction = LogAction.MedicationDischarged;
                    }
                    else
                    {
                        medication.MedicationCategory = MedicationCategoryEnum.Active.ToString();
                        logAction = LogAction.MedicationActivated;
                    }
                    medication.LastChangedDate = DateTime.Now;
                    medicationProfile.Medication = existingMedications.ToXml<List<Medication>>();
                    if (patientRepository.UpdateMedication(medicationProfile))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, logAction, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool DeleteMedication(Guid medicationProfileId, Guid medicationId)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                var existingList = medicationProfile.Medication.ToObject<List<Medication>>();
                if (existingList.Exists(m => m.Id == medicationId))
                {
                    existingList.RemoveAll(m => m.Id == medicationId);
                    medicationProfile.Medication = existingList.ToXml<List<Medication>>();
                }
                if (patientRepository.UpdateMedication(medicationProfile))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, medicationProfile.PatientId, medicationProfileId.ToString(), LogType.MedicationProfile, LogAction.MedicationDeleted, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool SignMedicationHistory(MedicationProfileHistory medicationProfileHistory)
        {
            bool result = false;
            var medicationProfile = patientRepository.GetMedicationProfile(medicationProfileHistory.ProfileId, Current.AgencyId);
            if (medicationProfile != null && medicationProfile.Medication.IsNotNullOrEmpty())
            {
                medicationProfileHistory.Id = Guid.NewGuid();
                medicationProfileHistory.UserId = Current.UserId;
                medicationProfileHistory.AgencyId = Current.AgencyId;
                medicationProfileHistory.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                medicationProfileHistory.Created = DateTime.Now;
                medicationProfileHistory.Modified = DateTime.Now;
                medicationProfileHistory.Medication = medicationProfile.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == "Active").ToList().ToXml();
                if (medicationProfileHistory.PharmacyPhoneArray != null && medicationProfileHistory.PharmacyPhoneArray.Count == 3)
                {
                    medicationProfileHistory.PharmacyPhone = medicationProfileHistory.PharmacyPhoneArray.ToArray().PhoneEncode();
                }
                var physician = physicianRepository.Get(medicationProfileHistory.PhysicianId, Current.AgencyId);
                if (physician != null)
                {
                    medicationProfileHistory.PhysicianData = physician.ToXml();
                }
                if (patientRepository.AddNewMedicationHistory(medicationProfileHistory))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, medicationProfile.PatientId, medicationProfile.Id.ToString(), LogType.MedicationProfileHistory, LogAction.MedicationProfileSigned, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public IList<MedicationProfileHistory> GetMedicationHistoryForPatient(Guid patientId)
        {
            var medProfiles = patientRepository.GetMedicationHistoryForPatient(patientId, Current.AgencyId);
            if (medProfiles != null && medProfiles.Count>0)
            {
                var userIds = medProfiles.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                medProfiles.ForEach(m =>
                {
                    if (!m.UserId.IsEmpty())
                    {
                        var user = users.SingleOrDefault(u => u.Id == m.UserId);
                        // var userName = UserEngine.GetName(c.UserId, Current.AgencyId);
                        if (user != null)
                        {
                            m.UserName = user.DisplayName;
                        }
                    }
                    //if (m.UserId != Guid.Empty)
                    //{
                    //    m.UserName = UserEngine.GetName(m.UserId, Current.AgencyId);
                    //}
                }
                );
            }
            return medProfiles.OrderByDescending(m => m.SignedDate.ToShortDateString().ToOrderedDate()).ToList();
        }

        public List<Medication> GetCurrentMedicationsByCategory(Guid patientId, string medicationCategory)
        {
            var medicationHistory = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var medicationList = new List<Medication>();
            if (medicationHistory != null)
            {
                medicationList = medicationHistory.Medication.ToObject<List<Medication>>().Where(m => m.MedicationCategory == medicationCategory).ToList();
            }
            return medicationList;
        }

        public AllergyProfileViewData GetAllergyProfilePrint(Guid Id)
        {
            var profile = new AllergyProfileViewData();
            var allergies = patientRepository.GetAllergyProfileByPatient(Id, Current.AgencyId);
            if (allergies != null) profile.AllergyProfile = allergies;
            var patient = patientRepository.Get(Id, Current.AgencyId);
            if (patient != null) profile.Patient = patient;
            var agency = agencyRepository.GetWithBranches(Current.AgencyId);
            if (agency != null) profile.Agency = agency;
            return profile;
        }

        public MedicationProfileSnapshotViewData GetMedicationProfilePrint(Guid patientId)
        {
            var med = patientRepository.GetMedicationProfileByPatient(patientId, Current.AgencyId);
            var profile = new MedicationProfileSnapshotViewData();
            if (med != null)
            {
                profile.MedicationProfile = med;
                profile.Patient = patientRepository.Get(patientId, Current.AgencyId);
                profile.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                profile.Allergies = GetAllergies(patientId);
                if (profile.Patient != null)
                {
                    if (profile.Patient.PhysicianContacts != null && profile.Patient.PhysicianContacts.Count > 0)
                    {
                        var physician = profile.Patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                        if (physician != null)
                        {
                            profile.PhysicianName = physician.DisplayName;
                        }
                    }
                    if (med.PharmacyName.IsNotNullOrEmpty()) profile.PharmacyName = med.PharmacyName;
                    else profile.PharmacyName = profile.Patient.PharmacyName;
                    if (med.PharmacyPhone.IsNotNullOrEmpty()) profile.PharmacyPhone = med.PharmacyPhone;
                    else profile.PharmacyPhone = profile.Patient.PharmacyPhone;
                    var currentEpisode = patientRepository.GetCurrentEpisode(Current.AgencyId, patientId);
                    if (currentEpisode != null)
                    {
                        profile.EpisodeId = currentEpisode.Id;
                        profile.StartDate = currentEpisode.StartDate;
                        profile.EndDate = currentEpisode.EndDate;
                        if (!currentEpisode.Id.IsEmpty() && !patientId.IsEmpty())
                        {
                            var assessment = this.GetEpisodeAssessment(currentEpisode.Id, patientId);
                            if (assessment != null)
                            {
                                var questions = assessment.ToDictionary();
                                if (questions != null)
                                {
                                    if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                                    {
                                        profile.PrimaryDiagnosis = questions["M1020PrimaryDiagnosis"].Answer;
                                    }
                                    if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                                    {
                                        profile.SecondaryDiagnosis = questions["M1022PrimaryDiagnosis1"].Answer;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return profile;
        }

        public DrugDrugInteractionsViewData GetDrugDrugInteractionsPrint(Guid patientId, List<string> drugsSelected)
        {
            var viewData = new DrugDrugInteractionsViewData();
            if (drugsSelected != null && drugsSelected.Count > 0)
            {
                viewData.DrugDrugInteractions = drugService.GetDrugDrugInteractions(drugsSelected);
            }
            viewData.Patient = patientRepository.Get(patientId, Current.AgencyId);
            viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);

            return viewData;
        }

        public MedicationProfileSnapshotViewData GetMedicationSnapshotPrint(Guid Id)
        {
            var viewData = new MedicationProfileSnapshotViewData();
            var snapShot = patientRepository.GetMedicationProfileHistory(Id, Current.AgencyId);
            if (snapShot != null)
            {
                viewData.MedicationProfile = snapShot.ToProfile();
                viewData.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                viewData.Patient = patientRepository.Get(snapShot.PatientId, Current.AgencyId);
                viewData.Allergies = snapShot.Allergies;
                viewData.PrimaryDiagnosis = snapShot.PrimaryDiagnosis;
                viewData.SecondaryDiagnosis = snapShot.SecondaryDiagnosis;
                AgencyPhysician physician = null;
                if (snapShot.PhysicianData.IsNotNullOrEmpty())
                {
                    physician = snapShot.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        viewData.PhysicianName = physician.DisplayName;
                    }
                    else
                    {
                        if (!snapShot.PhysicianId.IsEmpty())
                        {
                            physician = PhysicianEngine.Get(snapShot.PhysicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                viewData.PhysicianName = physician.DisplayName;
                            }
                        }
                    }
                }
                else
                {
                    if (!snapShot.PhysicianId.IsEmpty())
                    {
                        physician = PhysicianEngine.Get(snapShot.PhysicianId, Current.AgencyId);
                        if (physician != null)
                        {
                            viewData.PhysicianName = physician.DisplayName;
                        }
                    }
                }

                if (snapShot.SignatureText.IsNotNullOrEmpty())
                {
                    viewData.SignatureText = snapShot.SignatureText;
                    viewData.SignatureDate = snapShot.SignedDate;
                }
                else
                {
                    if (!snapShot.UserId.IsEmpty())
                    {
                        var displayName = UserEngine.GetName(snapShot.UserId, Current.AgencyId);
                        if (displayName.IsNotNullOrEmpty())
                        {
                            viewData.SignatureText = string.Format("Electronically Signed by: {0}", displayName);
                            viewData.SignatureDate = snapShot.SignedDate;
                        }
                    }
                }
                if (viewData.Patient != null)
                {
                    if (snapShot.PharmacyName.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyName = snapShot.PharmacyName;
                    }
                    else
                    {
                        viewData.PharmacyName = viewData.Patient.PharmacyName;
                    }

                    if (snapShot.PharmacyPhone.IsNotNullOrEmpty())
                    {
                        viewData.PharmacyPhone = snapShot.PharmacyPhone;
                    }
                    else
                    {
                        viewData.PharmacyPhone = viewData.Patient.PharmacyPhone;
                    }

                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, snapShot.EpisodeId, snapShot.PatientId);
                    if (episode != null)
                    {
                        viewData.EpisodeId = episode.Id;
                        viewData.StartDate = episode.StartDate;
                        viewData.EndDate = episode.EndDate;
                    }
                }
            }
            return viewData;
        }

        public bool LinkPhysicians(Patient patient)
        {
            var result = true;
            if (patient != null && patient.AgencyPhysicians.Any())
            {
                int i = 0;
                bool isPrimary = false;
                foreach (var agencyPhysicianId in patient.AgencyPhysicians)
                {
                    if (i == 0)
                    {
                        isPrimary = true;
                    }
                    else
                    {
                        isPrimary = false;
                    }
                    if (!agencyPhysicianId.IsEmpty() && !physicianRepository.Link(patient.Id, agencyPhysicianId, isPrimary))
                    {
                        result = false;
                        break;
                    }
                    i++;
                }
                if (i > 0 && result)
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PhysicianLinked, "One of the physician is set as primary.");
                }
            }
            return result;
        }

        public bool LinkPhysician(Guid patientId, Guid physicianId, bool isPrimary)
        {
            if (physicianRepository.Link(patientId, physicianId, isPrimary))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PhysicianLinked, isPrimary ? "Primary" : string.Empty);
                return true;
            }
            return false;
        }

        public bool UnlinkPhysician(Guid patientId, Guid physicianId)
        {
            if (patientRepository.DeletePhysicianContact(physicianId, patientId))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, patientId.ToString(), LogType.Patient, LogAction.PhysicianUnLinked, string.Empty);
                return true;
            }
            return false;
        }
       
        public List<Order> GetOrdersFromScheduleEvents(Guid patientId, DateTime startDate, DateTime endDate, List<ScheduleEvent> schedules)
        {
            var orders = new List<Order>();
            if (schedules != null && schedules.Count > 0)
            {
                var orderStatusAfterQA = ScheduleStatusFatory.OrdersAfterQA();
                var physicianOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.PhysicianOrder).ToList();
                if (physicianOrdersSchedules != null && physicianOrdersSchedules.Count > 0)
                {
                    var physicianOrdersIds = physicianOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var physicianOrders = patientRepository.GetPatientPhysicianOrders(Current.AgencyId, patientId, physicianOrdersIds, startDate, endDate);
                    physicianOrders.ForEach(po =>
                    {
                        var physician = PhysicianEngine.Get(po.PhysicianId, Current.AgencyId);
                        orders.Add(new Order
                        {
                            Id = po.Id,
                            PatientId = po.PatientId,
                            EpisodeId = po.EpisodeId,
                            Type = OrderType.PhysicianOrder,
                            Text = DisciplineTasks.PhysicianOrder.GetDescription(),
                            Number = po.OrderNumber,
                            PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                            PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                            PrintUrl = Url.Print(po.EpisodeId, po.PatientId, po.Id, DisciplineTasks.PhysicianOrder, po.Status, true) + string.Format(" | <a class=\"link\" onclick=\"Patient.Order.Delete('{0}','{1}',$(this).closest('.t-grid'));\">Delete</a>", po.Id, po.PatientId),
                            CreatedDate = po.OrderDateFormatted.ToZeroFilled(),
                            ReceivedDate = po.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature ? po.ReceivedDate > DateTime.MinValue ? po.ReceivedDate : po.SentDate : DateTime.MinValue,
                            SendDate = orderStatusAfterQA.Contains(po.Status)? po.SentDate : DateTime.MinValue,
                            Status = po.Status
                        });
                    });
                }
                var planofCareOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485 || s.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485).ToList();
                if (planofCareOrdersSchedules != null && planofCareOrdersSchedules.Count > 0)
                {
                    var planofCareOrdersIds = planofCareOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareOrders = planofCareRepository.GetPatientPlanofCares(Current.AgencyId, patientId, planofCareOrdersIds);
                    planofCareOrders.ForEach(poc =>
                    {
                        var evnt = planofCareOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                        if (evnt != null)
                        {
                            var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = poc.Id,
                                PatientId = poc.PatientId,
                                EpisodeId = poc.EpisodeId,
                                Type = OrderType.HCFA485,
                                Text = !poc.IsNonOasis ? DisciplineTasks.HCFA485.GetDescription() : DisciplineTasks.NonOasisHCFA485.GetDescription(),
                                Number = poc.OrderNumber,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485, poc.Status, true),
                                CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = evnt.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature ? (poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate) : DateTime.MinValue,
                                SendDate = orderStatusAfterQA.Contains(evnt.Status)? poc.SentDate : DateTime.MinValue,
                                Status = poc.Status
                            });
                        }
                    });
                }

                var planofCareStandAloneOrdersSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone).ToList();
                if (planofCareStandAloneOrdersSchedules != null && planofCareStandAloneOrdersSchedules.Count > 0)
                {
                    var planofCareStandAloneOrdersIds = planofCareStandAloneOrdersSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var planofCareStandAloneOrders = planofCareRepository.GetPatientPlanofCaresStandAlones(Current.AgencyId, patientId, planofCareStandAloneOrdersIds);
                    planofCareStandAloneOrders.ForEach(poc =>
                    {
                        var evnt = planofCareStandAloneOrdersSchedules.SingleOrDefault(s => s.EventId == poc.Id);
                        if (evnt != null)
                        {
                            var physician = PhysicianEngine.Get(poc.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = poc.Id,
                                PatientId = poc.PatientId,
                                EpisodeId = poc.EpisodeId,
                                Type = OrderType.HCFA485StandAlone,
                                Text = DisciplineTasks.HCFA485StandAlone.GetDescription(),
                                Number = poc.OrderNumber,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PhysicianName = physician != null && physician.DisplayName.IsNotNullOrEmpty() ? physician.DisplayName : "",
                                PrintUrl = Url.Print(poc.EpisodeId, poc.PatientId, poc.Id, DisciplineTasks.HCFA485StandAlone, poc.Status, true),
                                CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = evnt.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature ? poc.ReceivedDate > DateTime.MinValue ? poc.ReceivedDate : poc.SentDate : DateTime.MinValue,
                                SendDate = orderStatusAfterQA.Contains(evnt.Status) ? poc.SentDate : DateTime.MinValue,
                                Status = poc.Status
                            });
                        }
                    });
                }

                var faceToFaceEncounterSchedules = schedules.Where(s => s.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter).ToList();
                if (faceToFaceEncounterSchedules != null && faceToFaceEncounterSchedules.Count > 0)
                {
                    var faceToFaceEncounterOrdersIds = faceToFaceEncounterSchedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var faceToFaceEncounters = patientRepository.GetPatientFaceToFaceEncounterOrders(Current.AgencyId, patientId, faceToFaceEncounterOrdersIds);
                    faceToFaceEncounters.ForEach(ffe =>
                    {
                        var evnt = faceToFaceEncounterSchedules.SingleOrDefault(s => s.EventId == ffe.Id);
                        if (evnt != null)
                        {
                            var physician = PhysicianEngine.Get(ffe.PhysicianId, Current.AgencyId);
                            orders.Add(new Order
                            {
                                Id = ffe.Id,
                                PatientId = ffe.PatientId,
                                EpisodeId = ffe.EpisodeId,
                                Type = OrderType.FaceToFaceEncounter,
                                Text = DisciplineTasks.FaceToFaceEncounter.GetDescription(),
                                Number = ffe.OrderNumber,
                                PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                PrintUrl = Url.Print(ffe.EpisodeId, ffe.PatientId, ffe.Id, DisciplineTasks.FaceToFaceEncounter, ffe.Status, true),
                                CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                ReceivedDate = evnt.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature ? ffe.ReceivedDate > DateTime.MinValue ? ffe.ReceivedDate : ffe.RequestDate : DateTime.MinValue,
                                SendDate = orderStatusAfterQA.Contains(evnt.Status)? ffe.RequestDate : DateTime.MinValue,
                                Status = ffe.Status
                            });
                        }
                    });
                }
                var evalDisciplineTasks = DisciplineTaskFactory.PhysicianEvalNotes();
                var evalOrdersSchedule = schedules.Where(s => evalDisciplineTasks.Exists(d => (int)d == s.DisciplineTask)).ToList();
                if (evalOrdersSchedule != null && evalOrdersSchedule.Count > 0)
                {
                    var evalOrdersIds = evalOrdersSchedule.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                    var evalOrders = patientRepository.GetEvalOrders(Current.AgencyId, evalOrdersIds, startDate, endDate);
                    if (evalOrders != null && evalOrders.Count > 0)
                    {
                        var evalOrderStatusAfterQa = ScheduleStatusFatory.EvalOrderNoteAfterQA();
                        evalOrders.ForEach(eval =>
                        {
                            var evnt = evalOrdersSchedule.SingleOrDefault(s => s.EventId == eval.Id);
                            if (evnt != null)
                            {
                                var physician = PhysicianEngine.Get(eval.PhysicianId, Current.AgencyId);
                                orders.Add(new Order
                                {
                                    Id = eval.Id,
                                    PatientId = eval.PatientId,
                                    EpisodeId = eval.EpisodeId,
                                    Type = GetOrderType(eval.NoteType),
                                    Text = GetDisciplineType(eval.NoteType).GetDescription(),
                                    Number = eval.OrderNumber,
                                    PatientName = eval.DisplayName,
                                    PhysicianName = physician != null ? physician.DisplayName : string.Empty,
                                    PhysicianAccess = physician != null ? physician.PhysicianAccess : false,
                                    PrintUrl = Url.Print(eval.EpisodeId, eval.PatientId, eval.Id, GetDisciplineType(eval.NoteType), eval.Status, true),
                                    CreatedDate =  evnt.EventDate.IsValid() ? evnt.EventDate.ToString("MM/dd/yyyy") : string.Empty,
                                    ReceivedDate = evnt.Status == (int)ScheduleStatus.EvalReturnedWPhysicianSignature ? eval.ReceivedDate > DateTime.MinValue ? eval.ReceivedDate : eval.SentDate : DateTime.MinValue,
                                    SendDate = evalOrderStatusAfterQa.Contains(evnt.Status)? eval.SentDate : DateTime.MinValue,
                                    Status = eval.Status,

                                });
                            }
                        });
                    }
                }
            }
            return orders.OrderByDescending(o => o.CreatedDate).ToList();
        }

        public List<Order> GetPatientOrders(Guid patientId, DateTime startDate, DateTime endDate)
        {
            return GetOrdersFromScheduleEvents(patientId, startDate, endDate, patientRepository.GetPatientOrderScheduleEvents(Current.AgencyId, patientId, startDate, endDate));
        }

        public List<Order> GetEpisodeOrders(Guid episodeId, Guid patientId)
        {
            var orders = new List<Order>();
          
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var disciplineTasks = DisciplineTaskFactory.AllPhysicianOrders().ToArray();
                var schedules = patientRepository.GetCurrentAndPerviousOrders(Current.AgencyId, episode, disciplineTasks);
                if (schedules != null && schedules.Count > 0)
                {
                    orders = GetOrdersFromScheduleEvents(episode.PatientId, episode.StartDate, episode.EndDate, schedules);
                }
            }
            return orders;
        }

        public bool DeletePhysicianOrder(Guid orderId, Guid patientId)
        {
            bool result = false;
            if (!orderId.IsEmpty() && !patientId.IsEmpty())
            {
                try
                {
                    var physicianOrder = patientRepository.GetOrderOnly(orderId, Current.AgencyId);
                    if (physicianOrder != null && physicianOrder.PatientId == patientId)
                    {
                        var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, physicianOrder.EpisodeId, orderId);
                        if (scheduleEvent != null)
                        {
                            var oldIsDeprecated = scheduleEvent.IsDeprecated;
                            scheduleEvent.IsDeprecated = true;
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (patientRepository.MarkOrderAsDeleted(orderId, patientId, Current.AgencyId, true))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,physicianOrder.EpisodeId, physicianOrder.PatientId, physicianOrder.Id, Actions.Deleted, DisciplineTasks.PhysicianOrder);
                                    result = true;
                                }
                                else
                                {
                                    scheduleEvent.IsDeprecated = oldIsDeprecated;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    result = false;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Exception(ex);
                    return false;
                }
            }
            return result;
        }

        public PhysicianOrder GetOrderPrint()
        {
            var order = new PhysicianOrder();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        public PhysicianOrder GetOrderPrint(Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetOrderOnly(orderId, patientId, Current.AgencyId);
            if (order == null) order = new PhysicianOrder();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            order.Patient = patientRepository.Get(order.PatientId, Current.AgencyId);
            if (ScheduleStatusFatory.OrdersCompleted().Exists(o => o == order.Status) && !order.PhysicianId.IsEmpty() && order.PhysicianData.IsNotNullOrEmpty())
            {
                var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                if (physician != null)
                {
                    order.Physician = physician;
                }
                else
                {
                    order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                }
            }
            else
            {
                order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
            }

            order.Allergies = GetAllergies(patientId);
            var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, order.EpisodeId, order.PatientId);
            if (episode != null)
            {
                if (order.IsOrderForNextEpisode)
                {
                    order.EpisodeStartDate = episode.EndDate.AddDays(1).ToShortDateString();
                    order.EpisodeEndDate = episode.EndDate.AddDays(60).ToShortDateString();
                }
                else
                {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                }
                if (order.Patient != null)
                {
                    order.Patient.StartofCareDate = episode.StartOfCareDate;
                }
            }
            return order;
        }

        public bool ProcessCommunicationNotes(string button, Guid patientId, Guid eventId, string reason)
        {
            var result = false;
            bool isNoteUpdates = true;
            bool isActionSet = false;
            if (!eventId.IsEmpty() && !patientId.IsEmpty())
            {
                var communicationNote = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                if (communicationNote != null && !communicationNote.EpisodeId.IsEmpty())
                {
                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, communicationNote.EpisodeId, eventId);
                    if (scheduleEvent != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        var oldReason = scheduleEvent.ReturnReason;
                        var oldPrintQueue = scheduleEvent.InPrintQueue;
                        var description = string.Empty;
                        var action = new Actions();
                        if (button == ButtonAction.Approve.ToString())
                        {
                            communicationNote.Status = ((int)ScheduleStatus.NoteCompleted);
                            scheduleEvent.InPrintQueue = true;
                            scheduleEvent.Status = communicationNote.Status;
                            scheduleEvent.ReturnReason = string.Empty;
                            isActionSet = true;
                            description = "Approved By:" + Current.UserFullName;
                            action = Actions.Approved;

                        }
                        else if (button == ButtonAction.Return.ToString())
                        {
                            communicationNote.Status = ((int)ScheduleStatus.NoteReturned);
                            communicationNote.SignatureText = string.Empty;
                            communicationNote.SignatureDate = DateTime.MinValue;
                            scheduleEvent.Status = communicationNote.Status;
                            scheduleEvent.ReturnReason = reason;
                            isActionSet = true;
                            description = "Returned By:" + Current.UserFullName;
                            action = Actions.Returned;
                        }
                        else if (button == ButtonAction.Print.ToString())
                        {
                            scheduleEvent.InPrintQueue = false;
                            isNoteUpdates = false;
                            isActionSet = true;
                        }

                        if (isActionSet)
                        {
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (isNoteUpdates)
                                {
                                    communicationNote.Modified = DateTime.Now;
                                    if (patientRepository.UpdateCommunicationNoteModal(communicationNote))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, DisciplineTasks.CommunicationNote, description);
                                        result = true;
                                    }
                                    else
                                    {
                                        scheduleEvent.Status = oldStatus;
                                        scheduleEvent.ReturnReason = oldReason;
                                        scheduleEvent.InPrintQueue = oldPrintQueue;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        result = false;
                                    }
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public List<CommunicationNote> GetCommunicationNotes(Guid patientId, Guid branchId, int patientStatus, DateTime startDate, DateTime endDate)
        {
            var commNotes = new List<CommunicationNote>();
            var schedules = patientRepository.GetScheduleEventsVeryLean(Current.AgencyId, branchId, patientId, Guid.Empty, new int[] { }, new int[] { (int)DisciplineTasks.CommunicationNote }, patientStatus, startDate, endDate, true, false);
            if (schedules != null && schedules.Count > 0)
            {
                var communicationNoteByIds = schedules.Select(s => string.Format("'{0}'", s.EventId)).ToArray().Join(", ");
                commNotes = patientRepository.GetCommunicationNoteByIds(Current.AgencyId, communicationNoteByIds);
                if (commNotes != null && commNotes.Count > 0)
                {
                    commNotes.ForEach(c =>
                    {
                        string delete = Current.HasRight(Permissions.DeleteTasks) ? string.Format("<a class=\"link\" onclick=\"Patient.CommunicationNote.Delete('{0}','{1}',$(this).closest('.t-grid'));return false\">Delete</a>", c.Id, c.PatientId) : string.Empty;
                        if (ScheduleStatusFatory.OnAndAfterQAStatus().Exists(s => s == c.Status)) c.ActionUrl = delete;
                        else c.ActionUrl = string.Format("<a class=\"link\" onclick=\"Patient.CommunicationNote.Edit('{0}','{1}');return false\">Edit</a> | {2}", c.Id, c.PatientId, delete);
                        if (Current.IsClinicianOrHHA && Current.UserId != c.UserId) c.ActionUrl = string.Empty;
                    });
                }
            }
            return commNotes;
        }

        public bool DeleteCommunicationNote(Guid Id, Guid patientId)
        {
            bool result = false;
            var communicationNote = patientRepository.GetCommunicationNote(Id, patientId, Current.AgencyId);
            if (communicationNote != null && !communicationNote.EpisodeId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, communicationNote.PatientId, communicationNote.EpisodeId, communicationNote.Id);
                if (scheduleEvent != null)
                {
                    var oldIsDeprecated = scheduleEvent.IsDeprecated;
                    scheduleEvent.IsDeprecated = true;
                    if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                    {
                        if (patientRepository.DeleteCommunicationNote(Current.AgencyId, Id, patientId, true))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,communicationNote.EpisodeId, communicationNote.PatientId, communicationNote.Id, Actions.Deleted, DisciplineTasks.CommunicationNote);
                            result = true;
                        }
                        else
                        {
                            scheduleEvent.IsDeprecated = oldIsDeprecated;
                            patientRepository.UpdateScheduleEventNew(scheduleEvent);
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public CommunicationNote GetCommunicationNotePrint() {
            return new CommunicationNote();
        }

        public CommunicationNote GetCommunicationNotePrint(Guid eventId, Guid patientId)
        {
            var note = new CommunicationNote();
            if (!patientId.IsEmpty() && !eventId.IsEmpty())
            {
                note = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                if (note != null)
                {
                    if (ScheduleStatusFatory.NoteCompleted().Exists(n => n == note.Status) && !note.PhysicianId.IsEmpty() && note.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = note.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            note.Physician = physician;
                        }
                        else
                        {
                            note.Physician = PhysicianEngine.Get(note.PhysicianId, Current.AgencyId);
                        }
                    }
                    else
                    {
                        note.Physician = PhysicianEngine.Get(note.PhysicianId, Current.AgencyId);
                    }

                    note.Patient = patientRepository.Get(patientId, Current.AgencyId);
                    note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                }
            }
            return note;
        }

        public bool CreateFaceToFaceEncounter(FaceToFaceEncounter faceToFaceEncounter)
        {
            var result = false;
            try
            {
                faceToFaceEncounter.UserId = Current.UserId;
                faceToFaceEncounter.AgencyId = Current.AgencyId;
                faceToFaceEncounter.Status = (int)ScheduleStatus.OrderToBeSentToPhysician;
                faceToFaceEncounter.OrderNumber = patientRepository.GetNextOrderNumber();
                faceToFaceEncounter.Modified = DateTime.Now;
                faceToFaceEncounter.Created = DateTime.Now;
                if (!faceToFaceEncounter.PhysicianId.IsEmpty())
                {
                    var physician = physicianRepository.Get(faceToFaceEncounter.PhysicianId, Current.AgencyId);
                    if (physician != null)
                    {
                        faceToFaceEncounter.PhysicianData = physician.ToXml();
                    }
                }
                var newScheduleEvent = new ScheduleEvent
                {
                    AgencyId = Current.AgencyId,
                    EventId = faceToFaceEncounter.Id,
                    UserId = faceToFaceEncounter.UserId,
                    PatientId = faceToFaceEncounter.PatientId,
                    EpisodeId = faceToFaceEncounter.EpisodeId,
                    Status = faceToFaceEncounter.Status,
                    Discipline = Disciplines.Orders.ToString(),
                    EventDate = faceToFaceEncounter.RequestDate,
                    VisitDate = faceToFaceEncounter.RequestDate,
                    DisciplineTask = (int)DisciplineTasks.FaceToFaceEncounter
                };

                if (patientRepository.AddFaceToFaceEncounter(faceToFaceEncounter))
                {
                    if (patientRepository.UpdateScheduleEventNew(newScheduleEvent))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,newScheduleEvent.EpisodeId, newScheduleEvent.PatientId, newScheduleEvent.EventId, Actions.Add, DisciplineTasks.FaceToFaceEncounter);
                        result = true;
                    }
                    else
                    {
                        patientRepository.RemoveModel<FaceToFaceEncounter>(faceToFaceEncounter.Id);
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                return false;
            }
            return result;
        }

        public FaceToFaceEncounter GetFaceToFacePrint()
        {
            var order = new FaceToFaceEncounter();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        public FaceToFaceEncounter GetFaceToFacePrint(Guid patientId, Guid orderId)
        {
            var order = patientRepository.GetFaceToFaceEncounter(orderId, patientId, Current.AgencyId);
            if (order != null)
            {
                order.Patient = patientRepository.Get(order.PatientId, Current.AgencyId);
                if (ScheduleStatusFatory.OrdersCompleted().Exists(o => o == order.Status) && !order.PhysicianId.IsEmpty() && order.PhysicianData.IsNotNullOrEmpty())
                {
                    var physician = order.PhysicianData.ToObject<AgencyPhysician>();
                    if (physician != null)
                    {
                        order.Physician = physician;
                    }
                    else
                    {
                        order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                    }
                }
                else order.Physician = PhysicianEngine.Get(order.PhysicianId, Current.AgencyId);
                var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, order.EpisodeId, order.PatientId);
                if (episode != null)
                {
                    order.EpisodeEndDate = episode.EndDateFormatted;
                    order.EpisodeStartDate = episode.StartDateFormatted;
                    if (order.Patient != null) order.Patient.StartofCareDate = episode.StartOfCareDate;
                }
            }
            else order = new FaceToFaceEncounter();
            order.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return order;
        }

        public PatientEpisode CreateEpisode(Guid patientId, DateTime startDate, ScheduleEvent scheduleEvent)
        {
            var patientEpisode = new PatientEpisode();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = startDate;
            patientEpisode.EndDate = startDate.AddDays(59);
            patientEpisode.IsActive = true;
            return patientEpisode;
        }

        public PatientEpisode CreateEpisode(Guid patientId, PatientEpisode episode)
        {
            var patientEpisode = new PatientEpisode();
            patientEpisode.Id = Guid.NewGuid();
            patientEpisode.AgencyId = Current.AgencyId;
            patientEpisode.PatientId = patientId;
            patientEpisode.StartDate = episode.StartDate;
            patientEpisode.EndDate = episode.EndDate;
            patientEpisode.IsActive = true;
            patientEpisode.Detail = episode.Detail;
            patientEpisode.StartOfCareDate = episode.StartOfCareDate;
            patientEpisode.Details = patientEpisode.Detail.ToXml();
            return patientEpisode;
        }

        public bool AddEpisode(PatientEpisode patientEpisode)
        {
            bool result = false;
            if (patientRepository.AddEpisode(patientEpisode))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientEpisode.PatientId, patientEpisode.Id.ToString(), LogType.Episode, LogAction.EpisodeAdded, string.Empty);
                result = true;
            }
            return result;
        }

        public bool UpdateEpisode(PatientEpisode episode)
        {
            var result = false;
            episode.Details = episode.Detail.ToXml();
            var episodeToEdit = patientRepository.GetEpisodeById(Current.AgencyId, episode.Id, episode.PatientId);
            if (episodeToEdit != null)
            {
                var logAction = episodeToEdit.IsActive == episode.IsActive ? LogAction.EpisodeEdited : (!episode.IsActive ? LogAction.EpisodeDeactivated : LogAction.EpisodeActivated);
                episodeToEdit.IsActive = episode.IsActive;
                episodeToEdit.StartDate = episode.StartDate;
                episodeToEdit.EndDate = episode.EndDate;
                episodeToEdit.Detail = episodeToEdit.Details.ToObject<EpisodeDetail>();
                episode.Detail.Assets.AddRange(episodeToEdit.Detail.Assets);
                episodeToEdit.StartOfCareDate = episode.StartOfCareDate;
                episodeToEdit.Details = episode.Detail.ToXml();
                episodeToEdit.AdmissionId = episode.AdmissionId;
                if (patientRepository.UpdateEpisode(Current.AgencyId, episodeToEdit))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, episodeToEdit.PatientId, episodeToEdit.Id.ToString(), LogType.Episode, logAction, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool IsValidEpisode(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var episodes = patientRepository.GetPatientAllEpisodes(Current.AgencyId, patientId);
            bool result = true;
            if (episodes != null)
            {
                foreach (var e in episodes)
                {
                    if ((startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date) || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date))
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        public bool IsValidEpisode(Guid episodeId, Guid patientId, DateTime startDate, DateTime endDate)
        {
            var episodes = patientRepository.GetPatientAllEpisodes(Current.AgencyId, patientId);
            bool result = true;
            if (episodes != null)
            {
                foreach (var e in episodes)
                {
                    if (e.Id == episodeId)
                    {
                        continue;
                    }
                    else
                    {
                        if ((startDate.Date >= e.StartDate.Date && startDate.Date <= e.EndDate.Date) || (endDate.Date >= e.StartDate.Date && endDate.Date <= e.EndDate.Date))
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateEpisodeForDischarge(Guid patientId, DateTime dischargeDate, PatientEpisode episode)
        {
            var result = false;
            try
            {
                var patientEpisodes = new List<PatientEpisode>();
                if (episode != null)
                {
                    episode.EndDate = dischargeDate;
                    episode.IsLinkedToDischarge = true;
                    episode.Modified = DateTime.Now;
                    patientEpisodes.Add(episode);
                }
                var episodes = patientRepository.EpisodesToDischarge(Current.AgencyId, patientId, dischargeDate);//  database.Find<PatientEpisode>(e => e.AgencyId == agencyId && e.PatientId == patientId && e.StartDate.Date >= dischargeDate.Date);
                if (episodes != null && episodes.Count > 0)
                {
                    episodes.ForEach(es =>
                    {
                        es.Modified = DateTime.Now;
                        es.IsDischarged = true;
                    });
                    patientEpisodes.AddRange(episodes);
                }
                if (patientEpisodes.Count > 0)
                {
                    if (patientRepository.UpdateEpisodeForDischarge(patientEpisodes))
                    {
                        if (episode != null)
                        {
                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, episode.PatientId, episode.Id.ToString(), LogType.Episode, LogAction.EpisodeEdited, "Updated for patient discharge");
                        }
                        if (episodes != null && episodes.Count > 0)
                        {
                            episodes.ForEach(e =>
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, episode.PatientId, episode.Id.ToString(), LogType.Episode, LogAction.EpisodeDeactivatedForDischarge, string.Empty);
                            });
                        }
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public bool CreateEpisodeAndClaims(Patient patient)
        {
            bool result = false;
            var patientToEdit = patientRepository.Get(patient.Id, Current.AgencyId);
            if (patientToEdit != null)
            {
                if (patient.StartofCareDate.Date == patient.EpisodeStartDate.Date)
                {
                    var newEvent = new ScheduleEvent
                    {
                        AgencyId = Current.AgencyId,
                        EventId = Guid.NewGuid(),
                        PatientId = patient.Id,
                        UserId = patient.UserId,
                        Discipline = Disciplines.Nursing.ToString(),
                        Status = (int)ScheduleStatus.OasisNotYetDue,
                        DisciplineTask = (int)DisciplineTasks.OASISCStartofCare,
                        EventDate = patient.EpisodeStartDate,
                        VisitDate = patient.EpisodeStartDate,
                        IsBillable = true
                    };
                    var episode = this.CreateEpisode(patient.Id, patient.EpisodeStartDate, newEvent);
                    episode.StartOfCareDate = patient.StartofCareDate;
                    episode.AdmissionId = patientToEdit.AdmissionId;
                    if (this.AddEpisode(episode))
                    {
                        newEvent.EpisodeId = episode.Id;
                        this.ProcessScheduleFactory(newEvent);
                        if (patientRepository.AddScheduleEvent(newEvent))
                        {
                            if (SaveScheduleModalFactory(newEvent, patient, episode))
                            {
                                if ( patient.PrimaryInsurance > 0 && patient.PrimaryInsurance < 1000)
                                {
                                    var rap = this.CreateRap(patientToEdit, episode);
                                    var final = this.CreateFinal(patientToEdit, episode);
                                    if (rap != null)
                                    {
                                        if (billingRepository.AddRap(rap))
                                        {
                                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                                        }
                                    }
                                    if (final != null)
                                    {
                                        if (billingRepository.AddFinal(final))
                                        {
                                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                                        }
                                    }
                                }

                                if (patient.IsFaceToFaceEncounterCreated)
                                {
                                    if (!patientToEdit.Id.IsEmpty() && patientToEdit.PhysicianContacts.Count > 0)
                                    {
                                        var physician = patientToEdit.PhysicianContacts.Where(p => p.Primary = true).FirstOrDefault();
                                        if (physician != null && !physician.Id.IsEmpty())
                                        {
                                            var faceToFaceEncounter = new FaceToFaceEncounter { PatientId = patient.Id, EpisodeId = episode.Id, UserId = patient.UserId, PhysicianId = physician.Id, Id = Guid.NewGuid(), RequestDate = DateTime.Now };
                                            this.CreateFaceToFaceEncounter(faceToFaceEncounter);
                                        }
                                    }
                                }
                                if (Enum.IsDefined(typeof(DisciplineTasks), newEvent.DisciplineTask))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,newEvent.EpisodeId, newEvent.PatientId, newEvent.EventId, Actions.Add, (DisciplineTasks)newEvent.DisciplineTask);
                                }
                                result = true;
                            }
                            else
                            {
                                patientRepository.RemoveScheduleEventNew(newEvent);
                            }
                        }
                    }
                }
                else if (patient.EpisodeStartDate.Date > patient.StartofCareDate.Date)
                {
                    var episode = this.CreateEpisode(patient.Id, patient.EpisodeStartDate, null);
                    episode.StartOfCareDate = patient.StartofCareDate;
                    episode.AdmissionId = patientToEdit.AdmissionId;
                    if (this.AddEpisode(episode))
                    {
                        if ( patient.PrimaryInsurance > 0 && patient.PrimaryInsurance < 1000)
                        {
                            var rap = this.CreateRap(patientToEdit, episode);
                            var final = this.CreateFinal(patientToEdit, episode);
                            if (rap != null)
                            {
                                if (billingRepository.AddRap(rap))
                                {
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                                }
                            }
                            if (final != null)
                            {
                                if (billingRepository.AddFinal(final))
                                {
                                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                                }
                            }
                        }
                        if (patient.IsFaceToFaceEncounterCreated)
                        {
                            if (!patientToEdit.Id.IsEmpty() && patientToEdit.PhysicianContacts.Count > 0)
                            {
                                var physician = patientToEdit.PhysicianContacts.Where(p => p.Primary = true).FirstOrDefault();
                                if (physician != null && !physician.Id.IsEmpty())
                                {
                                    var faceToFaceEncounter = new FaceToFaceEncounter { PatientId = patient.Id, EpisodeId = episode.Id, UserId = patient.UserId, PhysicianId = physician.Id, Id = Guid.NewGuid(), RequestDate = patient.EpisodeStartDate };
                                    this.CreateFaceToFaceEncounter(faceToFaceEncounter);
                                }
                            }
                        }
                        result = true;
                    }
                }
            }
            return result;
        }

        public void DeleteEpisodeAndClaims(Patient patient)
        {
            PatientEpisode episodeDeleted = null;
            patientRepository.DeleteEpisode(Current.AgencyId, patient, out episodeDeleted);
            if (episodeDeleted != null)
            {
                billingRepository.DeleteRap(Current.AgencyId, patient.Id, episodeDeleted.Id);
                billingRepository.DeleteFinal(Current.AgencyId, patient.Id, episodeDeleted.Id);
            }
        }

        public bool UpdateScheduleEvent(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
            var schedule = patientRepository.GetScheduleEventNew(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId);
            if (schedule != null)
            {
                var scheduleBuffer = new ScheduleEvent
                {
                    Asset = schedule.Asset,
                    UserId = schedule.UserId,
                    Comments = schedule.Comments,
                    Status = schedule.Status,
                    EventDate = schedule.EventDate,
                    VisitDate = schedule.VisitDate,
                    IsBillable = schedule.IsBillable,
                    Surcharge = schedule.Surcharge,
                    AssociatedMileage = schedule.AssociatedMileage,
                    TimeIn = schedule.TimeIn,
                    TimeOut = schedule.TimeOut,
                    IsMissedVisit = schedule.IsMissedVisit,
                    DisciplineTask = schedule.DisciplineTask,
                    PhysicianId = schedule.PhysicianId,
                };

                if (httpFiles!=null && httpFiles.Count > 0)
                {
                    schedule.Assets = (schedule.Asset.IsNotNullOrEmpty() ? schedule.Asset.ToObject<List<Guid>>() : new List<Guid>()) ?? new List<Guid>();

                    foreach (string key in httpFiles.AllKeys)
                    {
                        HttpPostedFileBase file = httpFiles.Get(key);
                        if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                        {
                            var binaryReader = new BinaryReader(file.InputStream);
                            var asset = new Asset
                            {
                                FileName = file.FileName,
                                AgencyId = Current.AgencyId,
                                ContentType = file.ContentType,
                                FileSize = file.ContentLength.ToString(),
                                Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                            };
                            if (assetRepository.Add(asset))
                            {
                                schedule.Assets.Add(asset.Id);
                            }
                            else
                            {
                                assetsSaved = false;
                                break;
                            }
                        }
                    }
                }
                
                schedule.UserId = scheduleEvent.UserId;
                schedule.Comments = scheduleEvent.Comments;
                schedule.Status = scheduleEvent.Status;
                schedule.EventDate = scheduleEvent.EventDate;
                schedule.VisitDate = scheduleEvent.VisitDate;
                schedule.IsBillable = scheduleEvent.IsBillable;
                schedule.Surcharge = scheduleEvent.Surcharge;
                schedule.AssociatedMileage = scheduleEvent.AssociatedMileage;
                schedule.TimeIn = scheduleEvent.TimeIn;
                schedule.TimeOut = scheduleEvent.TimeOut;
                schedule.DisciplineTask = scheduleEvent.DisciplineTask;
                schedule.PhysicianId = scheduleEvent.PhysicianId;

                if (assetsSaved)
                {
                    schedule.Asset = schedule.Assets.ToXml();
                }

                if (assetsSaved && patientRepository.UpdateScheduleEventNew(schedule))
                {
                    if (ProcessEditDetail(schedule))
                    {
                        if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,schedule.EpisodeId, schedule.PatientId, schedule.EventId, Actions.EditDetail, (DisciplineTasks)schedule.DisciplineTask);
                        }
                        result = true;
                    }
                    else
                    {
                        schedule.UserId = scheduleBuffer.UserId;
                        schedule.Comments = scheduleBuffer.Comments;
                        schedule.Status = scheduleBuffer.Status;
                        schedule.EventDate = scheduleBuffer.EventDate;
                        schedule.VisitDate = scheduleBuffer.VisitDate;
                        schedule.IsBillable = scheduleBuffer.IsBillable;
                        schedule.Surcharge = scheduleBuffer.Surcharge;
                        schedule.AssociatedMileage = scheduleBuffer.AssociatedMileage;
                        schedule.TimeIn = scheduleBuffer.TimeIn;
                        schedule.TimeOut = scheduleBuffer.TimeOut;
                        schedule.DisciplineTask = scheduleBuffer.DisciplineTask;
                        schedule.PhysicianId = scheduleBuffer.PhysicianId;
                        schedule.Asset = scheduleBuffer.Asset;
                        patientRepository.UpdateScheduleEventNew(schedule);
                        result = false;
                    }
                }
            }
            return result;
        }

        public bool UpdateScheduleEventDetail(ScheduleEvent scheduleEvent, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
            var newPatientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.NewEpisodeId, scheduleEvent.PatientId);
            if (scheduleEvent != null && newPatientEpisode != null)
            {
                var schedule = patientRepository.GetScheduleEventNew(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.EpisodeId, scheduleEvent.EventId);  //oldScheduleEvents.FirstOrDefault(e => e.EventId == scheduleEvent.EventId && e.EpisodeId == scheduleEvent.EpisodeId && e.PatientId == scheduleEvent.PatientId);
                if (schedule != null)
                {

                    var scheduleBuffer = new ScheduleEvent
                    {
                        Asset = schedule.Asset,
                        UserId = schedule.UserId,
                        Comments = schedule.Comments,
                        Status = schedule.Status,
                        EventDate = schedule.EventDate,
                        VisitDate = schedule.VisitDate,
                        IsBillable = schedule.IsBillable,
                        Surcharge = schedule.Surcharge,
                        AssociatedMileage = schedule.AssociatedMileage,
                        TimeIn = schedule.TimeIn,
                        TimeOut = schedule.TimeOut,
                        IsMissedVisit = schedule.IsMissedVisit,
                        DisciplineTask = schedule.DisciplineTask,
                        PhysicianId = schedule.PhysicianId,
                        EpisodeId = schedule.EpisodeId
                    };

                    if (httpFiles != null && httpFiles.Count > 0)
                    {
                        schedule.Assets = (schedule.Asset.IsNotNullOrEmpty() ? schedule.Asset.ToObject<List<Guid>>() : new List<Guid>()) ?? new List<Guid>();
                        foreach (string key in httpFiles.AllKeys)
                        {
                            HttpPostedFileBase file = httpFiles.Get(key);
                            if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                            {
                                var binaryReader = new BinaryReader(file.InputStream);
                                var asset = new Asset
                                {
                                    FileName = file.FileName,
                                    AgencyId = Current.AgencyId,
                                    ContentType = file.ContentType,
                                    FileSize = file.ContentLength.ToString(),
                                    Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                };
                                if (assetRepository.Add(asset))
                                {
                                    schedule.Assets.Add(asset.Id);
                                }
                                else
                                {
                                    assetsSaved = false;
                                    break;
                                }
                            }
                        }
                    }

                    schedule.EpisodeId = newPatientEpisode.Id;
                    schedule.StartDate = newPatientEpisode.StartDate;
                    schedule.EndDate = newPatientEpisode.EndDate;
                    schedule.UserId = scheduleEvent.UserId;
                    schedule.Comments = scheduleEvent.Comments;
                    schedule.Status = scheduleEvent.Status;
                    schedule.EventDate = scheduleEvent.EventDate;
                    schedule.VisitDate = scheduleEvent.VisitDate;
                    schedule.IsBillable = scheduleEvent.IsBillable;
                    schedule.Surcharge = scheduleEvent.Surcharge;
                    schedule.AssociatedMileage = scheduleEvent.AssociatedMileage;
                    schedule.TimeIn = scheduleEvent.TimeIn;
                    schedule.TimeOut = scheduleEvent.TimeOut;
                    schedule.DisciplineTask = scheduleEvent.DisciplineTask;
                    schedule.PhysicianId = scheduleEvent.PhysicianId;
                    if (assetsSaved)
                    {
                        schedule.Asset = schedule.Assets.ToXml();
                    }

                    if (assetsSaved && patientRepository.UpdateScheduleEventNew(schedule))
                    {
                        if (ProcessEditDetailForReassign(schedule))
                        {
                            if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,schedule.EpisodeId, schedule.PatientId, schedule.EventId, Actions.EditDetail, (DisciplineTasks)schedule.DisciplineTask, "Reassigned from other episode.");
                            }
                            result = true;
                        }
                        else
                        {
                            schedule.UserId = scheduleBuffer.UserId;
                            schedule.Comments = scheduleBuffer.Comments;
                            schedule.Status = scheduleBuffer.Status;
                            schedule.EventDate = scheduleBuffer.EventDate;
                            schedule.VisitDate = scheduleBuffer.VisitDate;
                            schedule.IsBillable = scheduleBuffer.IsBillable;
                            schedule.Surcharge = scheduleBuffer.Surcharge;
                            schedule.AssociatedMileage = scheduleBuffer.AssociatedMileage;
                            schedule.TimeIn = scheduleBuffer.TimeIn;
                            schedule.TimeOut = scheduleBuffer.TimeOut;
                            schedule.DisciplineTask = scheduleBuffer.DisciplineTask;
                            schedule.PhysicianId = scheduleBuffer.PhysicianId;
                            schedule.EpisodeId = scheduleBuffer.EpisodeId;
                            schedule.Asset = scheduleBuffer.Asset;
                            patientRepository.UpdateScheduleEventNew(schedule);
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public List<ScheduleEvent> GetScheduledEvents(Guid patientId, string discipline, DateRange range)
        {
            var patientEvents = new List<ScheduleEvent>();
            if (range.Id.IsEqual("ThisEpisode"))
            {
                var patientEpisode = patientRepository.GetCurrentEpisodeLean(Current.AgencyId, patientId);
                if (patientEpisode != null )
                {
                    var schedule = patientRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientEpisode.Id, patientEpisode.PatientId, range.StartDate, range.EndDate, discipline, false, false);
                    if (schedule != null && schedule.Count > 0)
                    {
                        var detail = new EpisodeDetail();
                        if (patientEpisode.Details.IsNotNullOrEmpty())
                        {
                            detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                        }
                        schedule.ForEach(s =>
                        {
                            s.EpisodeNotes = detail.Comments.Clean();
                            if (s.IsMissedVisit)
                            {
                                var missedVisit = patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);

                                if (missedVisit != null)
                                {
                                    s.MissedVisitComments = missedVisit.ToString();
                                }
                            }
                            s.EndDate = patientEpisode.EndDate;
                            s.StartDate = patientEpisode.StartDate;
                            Common.Url.Set(s, true, true);
                            patientEvents.Add(s);
                        });
                    }
                }
            }
            else
            {
                if ((range.StartDate.Date > DateTime.MinValue.Date && range.EndDate.Date > DateTime.MinValue.Date) || range.Id.IsEqual("all"))
                {
                    var isDateRange = range.Id.IsEqual("all") ? false : true;
                    var schedule = patientRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, range.StartDate, range.EndDate, discipline, false, isDateRange);
                    if (schedule != null && schedule.Count > 0)
                    {
                        var detail = new EpisodeDetail();
                       
                        schedule.ForEach(s =>
                        {
                            if (s.EpisodeNotes.IsNotNullOrEmpty())
                            {
                                detail = s.EpisodeNotes.ToObject<EpisodeDetail>();
                            }
                            if (detail != null)
                            {
                                s.EpisodeNotes = detail.Comments.Clean();
                            }
                            else
                            {
                                s.EpisodeNotes = string.Empty;
                            }
                            if (s.IsMissedVisit)
                            {
                                var missedVisit = patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);

                                if (missedVisit != null)
                                {
                                    s.MissedVisitComments = missedVisit.ToString();
                                }
                            }
                            Common.Url.Set(s, true, true);
                            patientEvents.Add(s);
                        });
                    }
                }
            }

            return patientEvents;
        }

        public List<ScheduleEvent> GetScheduledEvents(Guid episodeId, Guid patientId, string discipline)
        {
            var patientEvents = new List<ScheduleEvent>();
            var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null)
            {
                var schedule = patientRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, episodeId, patientId, DateTime.MinValue, DateTime.MinValue, discipline, false, false);
                var detail = new EpisodeDetail();
                if (schedule != null && schedule.Count > 0)
                {
                    if (patientEpisode.Details.IsNotNullOrEmpty())
                    {
                        detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                    }
                    schedule.ForEach(s =>
                    {
                        s.EpisodeNotes = detail.Comments;
                        if (s.IsMissedVisit)
                        {
                            var missedVisit = patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
                            if (missedVisit != null) s.MissedVisitComments = missedVisit.ToString();
                        }
                        s.EndDate = patientEpisode.EndDate;
                        s.StartDate = patientEpisode.StartDate;
                        Common.Url.Set(s, true, true);
                        patientEvents.Add(s);
                    });
                }
            }
            return patientEvents;
        }

        public ScheduleEvent GetScheduledEvent(Guid episodeId, Guid patientId, Guid eventId)
        {
            var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
            if (scheduleEvent != null && scheduleEvent.Discipline.IsEqual(Disciplines.Orders.ToString()))
            {
                if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.PhysicianOrder)
                {
                    var order = patientRepository.GetOrderOnly(eventId, patientId, Current.AgencyId);
                    if (order != null)
                    {
                        scheduleEvent.PhysicianId = order.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485
                    || scheduleEvent.DisciplineTask == (int)DisciplineTasks.NonOasisHCFA485)
                {
                    var planofcare = assessmentService.GetPlanofCare(episodeId, patientId, eventId);
                    if (planofcare != null)
                    {
                        scheduleEvent.PhysicianId = planofcare.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.HCFA485StandAlone)
                {
                    var planofcareStandalone = assessmentService.GetPlanofCareStandAlone(episodeId, patientId, eventId);
                    if (planofcareStandalone != null)
                    {
                        scheduleEvent.PhysicianId = planofcareStandalone.PhysicianId;
                    }
                }
                else if (scheduleEvent.DisciplineTask == (int)DisciplineTasks.FaceToFaceEncounter)
                {
                    var faceToFace = patientRepository.GetFaceToFaceEncounter(eventId, patientId, Current.AgencyId);
                    if (faceToFace != null)
                    {
                        scheduleEvent.PhysicianId = faceToFace.PhysicianId;
                    }
                }
            }
            return scheduleEvent;
        }

        public PatientEpisode GetPatientEpisodeWithFrequencyForMasterCalendar(Guid episodeId, Guid patientId)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var patientEpisode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null)
            {
                var assessment = this.GetEpisodeAssessment(episodeId, patientId);
                if (assessment != null) patientEpisode.Detail.FrequencyList = GetEpisodeFrequency(assessment);
                else patientEpisode.Detail.FrequencyList = string.Empty;
                var patient = patientRepository.Get(patientId, Current.AgencyId);
                if (patient != null)
                {
                    patientEpisode.PatientIdNumber = patient.PatientIdNumber;
                    patientEpisode.DisplayName = patient.DisplayName;
                    patientEpisode.DisplayNameWithMi = patient.DisplayNameWithMi;
                }
            }
            return patientEpisode;
        }
        
        public bool SaveWoundCare(FormCollection formCollection, HttpFileCollectionBase httpFiles)
        {
            var result = false;
            var assetsSaved = true;
            string type = formCollection["Type"];
            if (type.IsNotNullOrEmpty())
            {
                Guid eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
                Guid episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
                Guid patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();
                var patientVisitNote = new PatientVisitNote();
                if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                    patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                    if (patientVisitNote != null)
                    {
                        patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                        if (patientVisitNote.Questions != null)
                        {
                            if (httpFiles.Count > 0)
                            {
                                foreach (string key in httpFiles.AllKeys)
                                {
                                    var keyArray = key.Split('_');
                                    HttpPostedFileBase file = httpFiles.Get(key);
                                    if (file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                                    {
                                        BinaryReader binaryReader = new BinaryReader(file.InputStream);
                                        var asset = new Asset
                                        {
                                            FileName = file.FileName,
                                            AgencyId = Current.AgencyId,
                                            ContentType = file.ContentType,
                                            FileSize = file.ContentLength.ToString(),
                                            Bytes = binaryReader.ReadBytes(Convert.ToInt32(file.InputStream.Length))
                                        };

                                        if (assetRepository.Add(asset))
                                        {
                                            if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                            {
                                                patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = asset.Id.ToString();
                                            }
                                            else
                                            {
                                                patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = asset.Id.ToString(), Type = keyArray[0] });
                                            }
                                        }
                                        else
                                        {
                                            if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                            {
                                                patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                            }
                                            else
                                            {
                                                patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                            }
                                            assetsSaved = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (patientVisitNote.Questions.Exists(q => q.Name == keyArray[1]))
                                        {
                                            patientVisitNote.Questions.SingleOrDefault(q => q.Name == keyArray[1]).Answer = Guid.Empty.ToString();
                                        }
                                        else
                                        {
                                            patientVisitNote.Questions.Add(new NotesQuestion { Name = keyArray[1], Answer = Guid.Empty.ToString(), Type = keyArray[0] });
                                        }
                                    }
                                }
                            }
                            patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                            patientVisitNote.IsWoundCare = true;
                            if (assetsSaved && patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                                Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, DisciplineTasks.SkilledNurseVisit, "Wound Care is added/ updated.");
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public Rap CreateRap(Patient patient, PatientEpisode episode)
        {
            var rap = new Rap
             {
                 Id = episode.Id,
                 AgencyId = patient.AgencyId,
                 PatientId = patient.Id,
                 EpisodeId = episode.Id,
                 EpisodeStartDate = episode.StartDate,
                 EpisodeEndDate = episode.EndDate,
                 IsFirstBillableVisit = false,
                 IsOasisComplete = false,
                 PatientIdNumber = patient.PatientIdNumber,
                 IsGenerated = false,
                 MedicareNumber = patient.MedicareNumber,
                 FirstName = patient.FirstName,
                 LastName = patient.LastName,
                 DOB = patient.DOB,
                 Gender = patient.Gender,
                 AddressLine1 = patient.AddressLine1,
                 AddressLine2 = patient.AddressLine2,
                 AddressCity = patient.AddressCity,
                 AddressStateCode = patient.AddressStateCode,
                 AddressZipCode = patient.AddressZipCode,
                 StartofCareDate = episode.StartOfCareDate,
                 AreOrdersComplete = false,
                 AdmissionSource = patient.AdmissionSource,
                 PatientStatus = patient.Status,
                 UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                 Status = (int)BillingStatus.ClaimCreated,
                 HealthPlanId = patient.PrimaryHealthPlanId,
                 DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
                 ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
                 Created = DateTime.Now
             };
            //int primaryInsurance;
            //if (int.TryParse(patient.PrimaryInsurance, out primaryInsurance))
            //{
                rap.PrimaryInsuranceId = patient.PrimaryInsurance;
           // }
            if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
            {
                if (!patient.AgencyPhysicians[0].IsEmpty())
                {
                    var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
                    if (physician != null)
                    {
                        rap.PhysicianNPI = physician.NPI;
                        rap.PhysicianFirstName = physician.FirstName;
                        rap.PhysicianLastName = physician.LastName;
                    }
                }
            }
            return rap;
        }

        public Final CreateFinal(Patient patient, PatientEpisode episode)
        {
            var final = new Final
            {
                Id = episode.Id,
                AgencyId = patient.AgencyId,
                PatientId = patient.Id,
                EpisodeId = episode.Id,
                EpisodeStartDate = episode.StartDate,
                EpisodeEndDate = episode.EndDate,
                IsFirstBillableVisit = false,
                IsOasisComplete = false,
                PatientIdNumber = patient.PatientIdNumber,
                IsGenerated = false,
                MedicareNumber = patient.MedicareNumber,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                DOB = patient.DOB,
                Gender = patient.Gender,
                AddressLine1 = patient.AddressLine1,
                AddressLine2 = patient.AddressLine2,
                AddressCity = patient.AddressCity,
                AddressStateCode = patient.AddressStateCode,
                AddressZipCode = patient.AddressZipCode,
                StartofCareDate = episode.StartOfCareDate,
                AdmissionSource = patient.AdmissionSource,
                PatientStatus = patient.Status,
                UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                AreOrdersComplete = false,
                AreVisitsComplete = false,
                Status = (int)BillingStatus.ClaimCreated,
                IsSupplyVerified = false,
                IsFinalInfoVerified = false,
                IsVisitVerified = false,
                IsRapGenerated = false,
                HealthPlanId = patient.PrimaryHealthPlanId,
                Created = DateTime.Now,
                IsSupplyNotBillable=false,
                DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
                ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
                Modified = DateTime.Now
            };
            //int primaryInsurance;
            //if (int.TryParse(patient.PrimaryInsurance, out primaryInsurance))
            //{
                final.PrimaryInsuranceId = patient.PrimaryInsurance;
            //}
            if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
            {
                if (!patient.AgencyPhysicians[0].IsEmpty())
                {
                    var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
                    if (physician != null)
                    {
                        final.PhysicianNPI = physician.NPI;
                        final.PhysicianFirstName = physician.FirstName;
                        final.PhysicianLastName = physician.LastName;
                    }
                }
            }
            return final;
        }

        public bool AddClaim(Guid patientId, Guid episodeId, string type)
        {
            bool result = false;
            if (type.IsNotNullOrEmpty())
            {
                var patient = patientRepository.Get(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        if (type.IsEqual("rap"))
                        {
                            var rap = CreateRap(patient, episode);
                            if (billingRepository.AddRap(rap))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, rap.PatientId, rap.Id.ToString(), LogType.Rap, LogAction.RAPAdded, string.Empty);
                                result = true;
                            }
                        }
                        else if (type.IsEqual("final"))
                        {
                            var final = CreateFinal(patient, episode);
                            if (billingRepository.AddFinal(final))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, final.PatientId, final.Id.ToString(), LogType.Final, LogAction.FinalAdded, string.Empty);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool DeleteClaim(Guid patientId, Guid Id, string type)
        {
            bool result = false;
            if (type.IsNotNullOrEmpty())
            {
                if (type.IsEqual("rap"))
                {
                    if (billingRepository.DeleteRap(Current.AgencyId, patientId, Id))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, Id.ToString(), LogType.Rap, LogAction.RAPDeleted, string.Empty);
                        result = true;
                    }
                }
                else if (type.IsEqual("final"))
                {
                    if (billingRepository.DeleteFinal(Current.AgencyId, patientId, Id))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, Id.ToString(), LogType.Final, LogAction.FinalDeleted, string.Empty);
                        result = true;
                    }
                }
            }
            return result;
        }

        public ManagedClaim CreateManagedClaim(Patient patient, DateTime startDate, DateTime endDate, int insuranceId)
        {
            var managedClaim = new ManagedClaim
            {
                Id = Guid.NewGuid(),
                AgencyId = patient.AgencyId,
                PatientId = patient.Id,
                EpisodeStartDate = startDate,
                EpisodeEndDate = endDate,
                IsFirstBillableVisit = false,
                IsOasisComplete = false,
                PatientIdNumber = patient.PatientIdNumber,
                IsGenerated = false,
                IsuranceIdNumber = patient.MedicareNumber,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                DOB = patient.DOB,
                Gender = patient.Gender,
                AddressLine1 = patient.AddressLine1,
                AddressLine2 = patient.AddressLine2,
                AddressCity = patient.AddressCity,
                AddressStateCode = patient.AddressStateCode,
                AddressZipCode = patient.AddressZipCode,
                StartofCareDate = patient.StartofCareDate,
                AreOrdersComplete = false,
                AdmissionSource = patient.AdmissionSource,
                PatientStatus = patient.Status,
                UB4PatientStatus = patient.Status == 1 ? "30" : (patient.Status == 2 ? "01" : string.Empty),
                Status = (int)ManagedClaimStatus.ClaimCreated,
                HealthPlanId = patient.PrimaryHealthPlanId,
                PrimaryInsuranceId = insuranceId,
                DiagnosisCode = "<DiagonasisCodes><code1></code1><code2></code2><code3></code3><code4></code4><code5></code5></DiagonasisCodes>",
                ConditionCodes = "<ConditionCodes><ConditionCode18></ConditionCode18><ConditionCode19></ConditionCode19><ConditionCode20></ConditionCode20><ConditionCode21></ConditionCode21><ConditionCode22></ConditionCode22><ConditionCode23></ConditionCode23><ConditionCode24></ConditionCode24><ConditionCode25></ConditionCode25><ConditionCode26></ConditionCode26><ConditionCode27></ConditionCode27><ConditionCode28></ConditionCode28></ConditionCodes>",
                Created = DateTime.Now
            };

            if (patient.AgencyPhysicians != null && patient.AgencyPhysicians.Count > 0)
            {
                if (!patient.AgencyPhysicians[0].IsEmpty())
                {
                    var physician = physicianRepository.Get(patient.AgencyPhysicians[0], Current.AgencyId);
                    if (physician != null)
                    {
                        managedClaim.PhysicianNPI = physician.NPI;
                        managedClaim.PhysicianFirstName = physician.FirstName;
                        managedClaim.PhysicianLastName = physician.LastName;
                    }
                }
            }
            return managedClaim;
        }

        public bool AddManagedClaim(Guid patientId, DateTime startDate, DateTime endDate, int insuranceId)
        {
            bool result = false;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                var managedClaim = CreateManagedClaim(patient, startDate, endDate, insuranceId);
                if (billingRepository.AddManagedClaim(managedClaim))
                {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, managedClaim.PatientId, managedClaim.Id.ToString(), LogType.ManagedClaim, LogAction.ManagedClaimAdded, string.Empty);
                    result = true;
                }
            }
            return result;
        }

        public bool AddMissedVisit(MissedVisit missedVisit)
        {
            bool result = false;
            if (missedVisit != null )
            {
                missedVisit.AgencyId = Current.AgencyId;
                var scheduledEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, missedVisit.PatientId, missedVisit.EpisodeId, missedVisit.Id); //events.Find(e => e.EventId == missedVisit.Id);
                if (scheduledEvent != null)
                {
                    var oldIsMissedVisit = scheduledEvent.IsMissedVisit;
                    scheduledEvent.IsMissedVisit = true;
                    if (patientRepository.UpdateScheduleEventNew(scheduledEvent))
                    {
                        var existing = patientRepository.GetMissedVisit(Current.AgencyId, scheduledEvent.EventId);
                        if (existing != null)
                        {
                            existing.EventDate = missedVisit.EventDate;
                            existing.IsOrderGenerated = missedVisit.IsOrderGenerated;
                            existing.IsPhysicianOfficeNotified = missedVisit.IsPhysicianOfficeNotified;
                            existing.Reason = missedVisit.Reason;
                            existing.SignatureDate = missedVisit.SignatureDate;
                            existing.SignatureText = missedVisit.SignatureText;
                            existing.Comments = missedVisit.Comments;
                           result= patientRepository.UpdateMissedVisit(existing);
                        }
                        else
                        {
                            result = patientRepository.AddMissedVisit(missedVisit);
                        }

                        if (result)
                        {
                            if (Enum.IsDefined(typeof(DisciplineTasks), scheduledEvent.DisciplineTask))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId, Actions.StatusChange, (DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), scheduledEvent.DisciplineTask), "Set as a missed visit by " + Current.UserFullName);
                            }
                        }
                        else
                        {
                            scheduledEvent.IsMissedVisit = oldIsMissedVisit;
                            patientRepository.UpdateScheduleEventNew(scheduledEvent);
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public bool IsValidImage(HttpFileCollectionBase httpFiles)
        {
            var result = false;
            if (httpFiles.Count > 0)
            {
                HttpPostedFileBase file = httpFiles.Get("Photo1");
                if (file != null && file.FileName.IsNotNullOrEmpty() && file.ContentLength > 0)
                {
                    var fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
                    if (AppSettings.AllowedImageExtensions.IsNotNullOrEmpty())
                    {
                        var allowedExtensions = AppSettings.AllowedImageExtensions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        if (allowedExtensions != null && allowedExtensions.Length > 0)
                        {
                            allowedExtensions.ForEach(extension =>
                            {
                                if (fileExtension.IsEqual(extension))
                                {
                                    result = true;
                                    return;
                                }
                            });
                        }
                    }
                }
            }
            return result;
        }

        public bool AddNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                var supplies = new List<Supply>();
                if (supply.UniqueIdentifier.IsEmpty())
                {
                    supply.UniqueIdentifier = Guid.NewGuid();
                }
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies != null && supplies.Count > 0)
                    {
                        var existingSupply = supplies.Find(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (existingSupply != null)
                        {
                            existingSupply.Date = supply.Date;
                            existingSupply.Quantity = supply.Quantity;
                            existingSupply.Description = supply.Description;
                        }
                        else
                        {
                            supplies.Add(supply);
                        }
                    }
                    else
                    {
                        supplies = new List<Supply> { supply };
                    }
                }
                else
                {
                    supplies = new List<Supply> { supply };
                }
                patientVisitNote.Supply = supplies.ToXml();
                patientVisitNote.IsSupplyExist = true;
                if (patientRepository.UpdateVisitNote(patientVisitNote))
                {
                    result = true;
                }
            }
            return result;
        }

        public bool UpdateNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        var editSupply = supplies.SingleOrDefault(s => s.UniqueIdentifier == supply.UniqueIdentifier);
                        if (editSupply != null)
                        {
                            editSupply.Quantity = supply.Quantity;
                            editSupply.UnitCost = supply.UnitCost;
                            editSupply.Description = supply.Description;
                            editSupply.DateForEdit = supply.DateForEdit;
                            editSupply.Code = supply.Code;
                            patientVisitNote.Supply = supplies.ToXml();
                            patientVisitNote.IsSupplyExist = true;
                            if (patientRepository.UpdateVisitNote(patientVisitNote))
                            {
                                result = true;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public bool DeleteNoteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            var result = false;
            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientVisitNote != null)
            {
                if (patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    var supplies = patientVisitNote.Supply.ToObject<List<Supply>>();
                    if (supplies.Exists(s => s.UniqueIdentifier == supply.UniqueIdentifier))
                    {
                        supplies.ForEach(s =>
                        {
                            if (s.UniqueIdentifier == supply.UniqueIdentifier)
                            {
                                supplies.Remove(s);
                            }
                        });
                        patientVisitNote.Supply = supplies.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public List<Supply> GetNoteSupply(Guid episodeId, Guid patientId, Guid eventId)
        {
            var list = new List<Supply>();
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.Supply.IsNotNullOrEmpty())
                {
                    list = patientVisitNote.Supply.ToObject<List<Supply>>();
                }
            }
            return list;
        }

        public PatientEligibility VerifyEligibility(string medicareNumber, string lastName, string firstName, DateTime dob, string gender)
        {
            PatientEligibility patientEligibility = null;
            try
            {
                var jsonData = new
                {
                    input_medicare_number = medicareNumber,
                    input_last_name = lastName,
                    input_first_name = firstName.Substring(0, 1),
                    input_date_of_birth = dob.ToString("MM/dd/yyyy"),
                    input_gender_id = gender.Substring(0, 1)
                };

                var javaScriptSerializer = new JavaScriptSerializer();
                var jsonRequest = javaScriptSerializer.Serialize(jsonData);

                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = ("request=" + jsonRequest);
                byte[] requestData = encoding.GetBytes(postData);

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(AppSettings.PatientEligibilityUrl);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.ContentLength = requestData.Length;

                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    requestStream.Write(requestData, 0, requestData.Length);
                    requestStream.Close();
                }

                var jsonResult = string.Empty;
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        jsonResult = streamReader.ReadToEnd();
                    }

                    if (jsonResult.IsNotNullOrEmpty())
                    {
                        patientEligibility = javaScriptSerializer.Deserialize<PatientEligibility>(jsonResult);
                        if (patientEligibility != null && patientEligibility.Episode != null && patientEligibility.Episode.reference_id.IsNotNullOrEmpty())
                        {
                            var npiData = Container.Resolve<ILookUpDataProvider>().LookUpRepository.GetNpiData(patientEligibility.Episode.reference_id.Trim());
                            if (npiData != null)
                            {
                                patientEligibility.Other_Agency_Data = new OtherAgencyData()
                                {
                                    name = npiData.ProviderOrganizationName,
                                    address1 = npiData.ProviderFirstLineBusinessMailingAddress,
                                    address2 = npiData.ProviderSecondLineBusinessMailingAddress,
                                    city = npiData.ProviderBusinessMailingAddressCityName,
                                    state = npiData.ProviderBusinessMailingAddressStateName,
                                    zip = npiData.ProviderBusinessMailingAddressPostalCode,
                                    phone = npiData.ProviderBusinessMailingAddressTelephoneNumber,
                                    fax = npiData.ProviderBusinessMailingAddressFaxNumber
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }
            return patientEligibility;
        }

        public List<MedicareEligibility> GetMedicareEligibilityLists(Guid patientId)
        {
            var eligibilities = patientRepository.GetMedicareEligibilities(Current.AgencyId, patientId);
            if (eligibilities != null)
            {
                eligibilities.ForEach(e =>
                {
                    if (!e.EpisodeId.IsEmpty())
                    {
                        var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, e.EpisodeId, e.PatientId);
                        if (episode != null)
                        {
                            e.EpisodeRange = string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted);
                        }
                        else
                        {
                            e.EpisodeRange = "Not in an episode";
                        }
                    }
                    e.AssignedTo = "Axxess";
                    e.TaskName = "Medicare Eligibility Report";
                    e.PrintUrl = "<a href=\"javascript:void(0);\" onclick=\"U.GetAttachment('Patient/MedicareEligibilityReportPdf', { 'patientId': '" + e.PatientId + "', 'mcareEligibilityId': '" + e.Id + "' });\"><span class='img icon print'></span></a>";
                });
            }
            return eligibilities.OrderBy(e => e.Created.ToShortDateString().ToZeroFilled()).ToList();
        }

        public string GetScheduledEventUrl(ScheduleEvent evnt, DisciplineTasks task)
        {
            var url = string.Empty;
            var scheduleEvent = patientRepository.GetLastScheduledEvents(Current.AgencyId, evnt.EpisodeId, evnt.PatientId, evnt.StartDate, evnt.EndDate, evnt.StartDate, evnt.EventDate.AddDays(-1), new int[] { (int)task });
            if (scheduleEvent != null)// && (int)task == scheduleEvent.DisciplineTask
            {
                switch (task)
                {
                    case DisciplineTasks.HHAideCarePlan:
                        url = new StringBuilder("<a href=\"javascript:void(0);\" onclick=\"Acore.OpenPrintView({")
                        .AppendFormat("Url: '/HHACarePlan/View/{0}/{1}/{2}'", scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId)
                        .Append("})\">View Care Plan</a>").ToString();
                        break;
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.OTEvaluation:
                        url = string.Format("<a href=\"javascript:void(0);\" onclick=\"Visit.{0}.Print('{1}','{2}','{3}',false)\">View Eval</a>", task.ToString(), scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId);
                        break;
                    default:
                        break;
                }

            }
            return url;
        }

        public PlanofCareViewData GetPatientAndAgencyInfo(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new PlanofCareViewData();
            var planofcare = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, eventId);
            if (planofcare != null)
            {
                if (planofcare.Data.IsNotNullOrEmpty())
                {
                    planofcare.Questions = planofcare.Data.ToObject<List<Question>>();
                }
                viewData.EditData = planofcare.ToDictionary();

                var dictionary = new Dictionary<string, string>();
                dictionary.Add("Id", eventId.ToString());

                var patient = patientRepository.Get(patientId, Current.AgencyId);
                if (patient != null)
                {
                    dictionary.Add("PatientId", patient.Id.ToString());
                    dictionary.Add("PatientName", patient.DisplayName);
                    dictionary.Add("PatientIdNumber", patient.PatientIdNumber);
                    dictionary.Add("PatientMedicareNumber", patient.MedicareNumber);
                    dictionary.Add("PatientAddressFirstRow", patient.AddressFirstRow);
                    dictionary.Add("PatientAddressSecondRow", patient.AddressSecondRow);
                    dictionary.Add("PatientPhone", patient.PhoneHomeFormatted);
                    dictionary.Add("PatientDoB", patient.DOBFormatted);
                    dictionary.Add("PatientGender", patient.Gender);


                    if (!patient.AgencyId.IsEmpty())
                    {
                        var agency = agencyRepository.Get(patient.AgencyId);
                        if (agency != null)
                        {
                            dictionary.Add("AgencyId", agency.Id.ToString());
                            dictionary.Add("AgencyMedicareProviderNumber", agency.MedicareProviderNumber);
                            dictionary.Add("AgencyName", agency.Name);
                            dictionary.Add("AgencyAddressFirstRow", agency.MainLocation.AddressFirstRow);
                            dictionary.Add("AgencyAddressSecondRow", agency.MainLocation.AddressSecondRow);
                            dictionary.Add("AgencyPhone", agency.MainLocation.PhoneWorkFormatted);
                            dictionary.Add("AgencyFax", agency.MainLocation.FaxNumberFormatted);
                        }
                    }

                    if (planofcare.PhysicianData.IsNotNullOrEmpty())
                    {
                        var physician = planofcare.PhysicianData.ToObject<AgencyPhysician>();
                        if (physician != null)
                        {
                            dictionary.Add("PhysicianId", physician.Id.ToString());
                            dictionary.Add("PhysicianName", physician.DisplayName);
                            dictionary.Add("PhysicianNpi", physician.NPI);
                        }
                    }
                    else
                    {
                        if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        {
                            var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                            if (primaryPhysician == null)
                            {
                                primaryPhysician = patient.PhysicianContacts.First();
                            }

                            if (primaryPhysician != null)
                            {
                                dictionary.Add("PhysicianId", primaryPhysician.Id.ToString());
                                dictionary.Add("PhysicianName", primaryPhysician.DisplayName);
                                dictionary.Add("PhysicianNpi", primaryPhysician.NPI);
                            }
                        }
                    }
                }

                var episode = patientRepository.GetEpisodeByIdWithSOC(Current.AgencyId, episodeId, patientId);//change when SOC implemented
                if (episode != null)
                {
                    dictionary.Add("PatientSoC", episode.StartOfCareDate.ToString("MM/dd/yyyy"));
                    dictionary.Add("EpisodeId", episode.Id.ToString());
                    dictionary.Add("EpisodeCertPeriod", string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted));
                }
                viewData.DisplayData = dictionary;
            }

            return viewData;
        }

        public List<VisitNoteViewData> GetSixtyDaySummary(Guid patientId)
        {
            var visitNoteviewDatas = new List<VisitNoteViewData>();
            var visits = patientRepository.GetVisitNotesByDisciplineTaskWithStatus(Current.AgencyId, patientId, new int[] { (int)ScheduleStatus.NoteCompleted }, new int[] { (int)DisciplineTasks.SixtyDaySummary });
            if (visits != null && visits.Count > 0)
            {
                var userIds = visits.Where(p => !p.UserId.IsEmpty()).Select(p => p.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                visits.ForEach(v =>
                {
                    var visitNoteviewData = new VisitNoteViewData();
                    var user = users.SingleOrDefault(u => u.Id == v.UserId);
                    if (user != null)
                    {
                        visitNoteviewData.UserDisplayName = user.DisplayName;
                    }
                    var questions = v.ToDictionary();
                    if (questions != null && questions.Count > 0)
                    {
                        var physicianId = questions.ContainsKey("Physician") && questions["Physician"].Answer.IsNotNullOrEmpty() && questions["Physician"].Answer.IsGuid() ? questions["Physician"].Answer.ToGuid() : Guid.Empty;
                        visitNoteviewData.VisitDate = v.VisitDate.Date > DateTime.MinValue.Date ? v.VisitDate.ToString("MM/dd/yyyy") : v.EventDate.ToString("MM/dd/yyyy");
                        if (!physicianId.IsEmpty())
                        {
                            var physician = PhysicianEngine.Get(physicianId, Current.AgencyId);
                            if (physician != null)
                            {
                                visitNoteviewData.PhysicianDisplayName = physician.DisplayName;
                            }
                        }
                        visitNoteviewData.SignatureDate = questions.ContainsKey("SignatureDate") && questions["SignatureDate"].Answer.IsNotNullOrEmpty() && questions["SignatureDate"].Answer.IsValidDate() ? questions["SignatureDate"].Answer : string.Empty;
                    }
                    visitNoteviewData.StartDate = v.StartDate;
                    visitNoteviewData.EndDate = v.EndDate;
                    visitNoteviewData.PrintUrl = Url.Print(new ScheduleEvent { DisciplineTask = (int)DisciplineTasks.SixtyDaySummary, EventId = v.Id, EpisodeId = v.EpisodeId, PatientId = v.PatientId }, true);
                    visitNoteviewDatas.Add(visitNoteviewData);
                });
            }
            return visitNoteviewDatas;
        }

        public List<VitalSign> GetPatientVitalSigns(Guid patientId, DateTime startDate, DateTime endDate)
        {
            var vitalSigns = new List<VitalSign>();
            var disciplineTasksSNVList = DisciplineTaskFactory.SkilledNurseDisciplineTasks().ToArray();
            var disciplineTasksOASISList = DisciplineTaskFactory.EpisodeAllAssessments().ToArray(); 
            var scheduleEvents = patientRepository.GetScheduledEventsForOASISAndNotesByPatient(Current.AgencyId, patientId, startDate, endDate, new int[] { }, disciplineTasksSNVList, new int[] { }, disciplineTasksOASISList);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var userIds = scheduleEvents.Where(s => !s.UserId.IsEmpty()).Select(s => s.UserId).Distinct().ToList();
                var users = userRepository.GetUsersByIdsLean(Current.AgencyId, userIds);
                scheduleEvents.ForEach(s =>
                {
                    if (s.Note.IsNotNullOrEmpty())
                    {
                        if (disciplineTasksOASISList.Contains(s.DisciplineTask))
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var questions = s.ToOASISDictionary();// assessment.ToDictionary();
                            vitalSign.VisitDate = s.VisitDate.ToString("MM/dd/yyyy");
                            if (questions != null && questions.Count > 0)
                            {
                                vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


                                vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                                vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
                                vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
                                vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");
                                var bs = new List<double>(); // var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
                                var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                if (BSAM > 0)
                                {
                                    bs.Add(BSAM);
                                }
                                var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                if (BSNoon > 0)
                                {
                                    bs.Add(BSNoon);
                                }
                                var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                if (BSPM > 0)
                                {
                                    bs.Add(BSPM);
                                }
                                var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                if (BSHS > 0)
                                {
                                    bs.Add(BSHS);
                                }
                                var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;

                                vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty; //questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
                                vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                var user = users.SingleOrDefault(u => u.Id == s.UserId);
                                if (user != null)
                                {
                                    vitalSign.UserDisplayName = user.DisplayName;
                                }
                                vitalSigns.Add(vitalSign);
                            }
                        }

                        else if (disciplineTasksSNVList.Contains(s.DisciplineTask))
                        {
                            var vitalSign = new VitalSign();
                            vitalSign.DisciplineTask = s.DisciplineTaskName;
                            var questions = s.ToVisitNoteDictionary();//visitNote.ToDictionary();
                            vitalSign.VisitDate = s.VisitDate.ToString("MM/dd/yyyy");
                            if (questions != null && questions.Count > 0)
                            {
                                vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


                                vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;


                                vitalSign.BPLying = string.Format("{0}  {1}  ", vitalSign.BPLyingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPLyingLeft : "", vitalSign.BPLyingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPLyingRight : "");
                                vitalSign.BPSitting = string.Format("{0}  {1}  ", vitalSign.BPSittingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPSittingLeft : "", vitalSign.BPSittingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPSittingRight : "");
                                vitalSign.BPStanding = string.Format(" {0}  {1}  ", vitalSign.BPStandingLeft.IsNotNullOrEmpty() ? "L: " + vitalSign.BPStandingLeft : "", vitalSign.BPStandingRight.IsNotNullOrEmpty() ? " R: " + vitalSign.BPStandingRight : "");


                                var bs = new List<double>(); //new double[] { BSAM, BSNoon, BSPM, BSHS };
                                var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                if (BSAM > 0)
                                {
                                    bs.Add(BSAM);
                                }
                                var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                if (BSNoon > 0)
                                {
                                    bs.Add(BSNoon);
                                }
                                var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                if (BSPM > 0)
                                {
                                    bs.Add(BSPM);
                                }
                                var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                if (BSHS > 0)
                                {
                                    bs.Add(BSHS);
                                }
                                var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;
                                vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty;// questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
                                vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                            }
                            var user = users.SingleOrDefault(u => u.Id == s.UserId);
                            if (user != null)
                            {
                                vitalSign.UserDisplayName = user.DisplayName;
                            }
                            vitalSigns.Add(vitalSign);
                        }
                    }
                });
            }
            return vitalSigns;
        }

        public List<VitalSign> GetVitalSignsForSixtyDaySummary(PatientEpisode episode, DateTime date)
        {
            var vitalSigns = new List<VitalSign>();
            if (episode != null)
            {
                var disciplineTasksSNVList = DisciplineTaskFactory.SkilledNurseDisciplineTasks().ToArray();
                var disciplineTasksOASISList = DisciplineTaskFactory.EpisodeAllAssessments().ToArray();
                var scheduleEvents = patientRepository.GetScheduledEventsForOASISAndNotesByEpisode(Current.AgencyId, episode.PatientId, episode.Id, episode.StartDate, date > episode.EndDate ? episode.EndDate : date, new int[] { }, disciplineTasksSNVList, new int[] { }, disciplineTasksOASISList);
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    scheduleEvents.ForEach(s =>
                    {
                        if (s.Note.IsNotNullOrEmpty())
                        {
                            var vitalSign = new VitalSign();
                            if (disciplineTasksSNVList.Contains(s.DisciplineTask))
                            {
                                var questions = s.ToVisitNoteDictionary();//visitNote.ToDictionary();
                                vitalSign.VisitDate = s.EventDate.ToString("MM/dd/yyyy");
                                if (questions != null && questions.Count > 0)
                                {
                                    vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                    vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;

                                    vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                    vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                    vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                    vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                    vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                    vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                                    vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                    vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;
                                    var bs = new List<double>(); //var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
                                    var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSAM > 0)
                                    {
                                        bs.Add(BSAM);
                                    }
                                    var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSNoon > 0)
                                    {
                                        bs.Add(BSNoon);
                                    }
                                    var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSPM > 0)
                                    {
                                        bs.Add(BSPM);
                                    }
                                    var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSHS > 0)
                                    {
                                        bs.Add(BSHS);
                                    }

                                    var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                    var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                    vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty;//questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                    vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
                                    vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                    vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                   
                                    vitalSigns.Add(vitalSign);
                                }
                            }
                            else if (disciplineTasksOASISList.Contains(s.DisciplineTask))
                            {
                                vitalSign.DisciplineTask = s.DisciplineTaskName;
                                var questions = s.ToOASISDictionary(); //assessment.ToDictionary();
                                vitalSign.VisitDate = s.VisitDate.ToString("MM/dd/yyyy");
                                if (questions != null && questions.Count > 0)
                                {
                                    vitalSign.Temp = questions.ContainsKey("GenericTemp") ? questions["GenericTemp"].Answer : string.Empty;
                                    vitalSign.Resp = questions.ContainsKey("GenericResp") ? questions["GenericResp"].Answer : string.Empty;


                                    vitalSign.BPLyingLeft = questions.ContainsKey("GenericBPLeftLying") ? questions["GenericBPLeftLying"].Answer : string.Empty;
                                    vitalSign.BPLyingRight = questions.ContainsKey("GenericBPRightLying") ? questions["GenericBPRightLying"].Answer : string.Empty;

                                    vitalSign.BPSittingLeft = questions.ContainsKey("GenericBPLeftSitting") ? questions["GenericBPLeftSitting"].Answer : string.Empty;
                                    vitalSign.BPSittingRight = questions.ContainsKey("GenericBPRightSitting") ? questions["GenericBPRightSitting"].Answer : string.Empty;

                                    vitalSign.BPStandingLeft = questions.ContainsKey("GenericBPLeftStanding") ? questions["GenericBPLeftStanding"].Answer : string.Empty;
                                    vitalSign.BPStandingRight = questions.ContainsKey("GenericBPRightStanding") ? questions["GenericBPRightStanding"].Answer : string.Empty;

                                    var bs = new List<double>(); //var bs = new double[] { BSAM, BSNoon, BSPM, BSHS };
                                    var BSAM = questions.ContainsKey("GenericBloodSugarAMLevelText") && questions["GenericBloodSugarAMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarAMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarAMLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSAM > 0)
                                    {
                                        bs.Add(BSAM);
                                    }
                                    var BSNoon = questions.ContainsKey("GenericBloodSugarLevelText") && questions["GenericBloodSugarLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSNoon > 0)
                                    {
                                        bs.Add(BSNoon);
                                    }
                                    var BSPM = questions.ContainsKey("GenericBloodSugarPMLevelText") && questions["GenericBloodSugarPMLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarPMLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarPMLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSPM > 0)
                                    {
                                        bs.Add(BSPM);
                                    }
                                    var BSHS = questions.ContainsKey("GenericBloodSugarHSLevelText") && questions["GenericBloodSugarHSLevelText"].Answer.IsNotNullOrEmpty() && questions["GenericBloodSugarHSLevelText"].Answer.IsDouble() ? questions["GenericBloodSugarHSLevelText"].Answer.ToDouble() : 0.0;
                                    if (BSHS > 0)
                                    {
                                        bs.Add(BSHS);
                                    }

                                    var maxBs = bs != null && bs.Count > 0 ? bs.Max() : 0;
                                    var minBs = bs != null && bs.Count > 0 ? bs.Min() : 0;
                                    vitalSign.ApicalPulse = questions.ContainsKey("GenericPulseApical") ? questions["GenericPulseApical"].Answer : string.Empty;
                                    vitalSign.RadialPulse = questions.ContainsKey("GenericPulseRadial") ? questions["GenericPulseRadial"].Answer : string.Empty;

                                    vitalSign.BSMax = maxBs > 0 ? maxBs.ToString() : string.Empty; //questions.ContainsKey("GenericBloodSugarLevelText") ? questions["GenericBloodSugarLevelText"].Answer : string.Empty;
                                    vitalSign.BSMin = minBs > 0 ? minBs.ToString() : string.Empty;
                                    vitalSign.Weight = questions.ContainsKey("GenericWeight") ? questions["GenericWeight"].Answer : string.Empty;
                                    vitalSign.PainLevel = questions.ContainsKey("GenericIntensityOfPain") ? questions["GenericIntensityOfPain"].Answer : string.Empty;
                                   
                                    vitalSigns.Add(vitalSign);
                                }
                            }
                        }
                    });
                }
            }
            return vitalSigns;
        }

        public DisciplineTask GetDisciplineTask(int disciplineTaskId)
        {
            return lookupRepository.GetDisciplineTask(disciplineTaskId);
        }

        public List<PatientEpisodeTherapyException> GetTherapyException(Guid branchId, DateTime startDate, DateTime endDate)
        {
            var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            var aprilStartDate = new DateTime(2011, 04, 01);
            startDate = startDate > aprilStartDate ? startDate : aprilStartDate;
            if (endDate > startDate)
            {
                var allScheduleEvents = patientRepository.GetTherapyExceptionScheduleEvents(Current.AgencyId, branchId, new DateTime(2011, 04, 01), DateTime.Now);
                if (allScheduleEvents != null && allScheduleEvents.Count > 0)
                {
                    var episodeIds = allScheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                    if (episodeIds != null && episodeIds.Count > 0)
                    {
                        var completedNoteStatus = ScheduleStatusFatory.OASISAndNurseNotesAfterQA();
                        episodeIds.ForEach(Id =>
                        {
                            var scheduleEvents = allScheduleEvents.Where(e => e.EpisodeId == Id).OrderBy(e => e.EventDate.Date).ToList();
                            if (scheduleEvents != null && scheduleEvents.Count > 0)
                            {
                                var schedule = scheduleEvents.FirstOrDefault();
                                if (schedule != null)
                                {
                                    var episode = new PatientEpisodeTherapyException
                                    {
                                        PatientIdNumber = schedule.PatientIdNumber,
                                        EndDate = schedule.EndDate,
                                        StartDate = schedule.StartDate,
                                        PatientName = schedule.PatientName
                                    };
                                    var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
                                    if (disciplines.Length == 1)
                                    {
                                        var evnt19check = true;
                                        var evnt13check = true;
                                        if (scheduleEvents.Count >= 13)
                                        {
                                            var evnt13 = scheduleEvents[12];
                                            if (evnt13.EventDate.IsValid())
                                            {
                                                var date13 = evnt13.EventDate;
                                                var scheduleEvents13 = scheduleEvents.Where(e => (e.EventDate.Date == date13.Date)).ToList();
                                                if (scheduleEvents13 != null)
                                                {
                                                    if (evnt13.Discipline == "PT")
                                                    {
                                                        evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                    }
                                                    else if (evnt13.Discipline == "OT")
                                                    {
                                                        evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                    }
                                                    else if (evnt13.Discipline == "ST")
                                                    {
                                                        evnt13check = scheduleEvents13.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                    }
                                                }
                                            }
                                            if (evnt13 != null)
                                            {
                                                episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate > DateTime.MinValue ? evnt13.EventDate.ToString("MM/dd") : string.Empty);
                                            }
                                            if (scheduleEvents.Count >= 19)
                                            {
                                                var evnt19 = scheduleEvents[18];
                                                if (evnt19.EventDate.IsValid())
                                                {
                                                    var date19 = evnt19.EventDate;
                                                    var scheduleEvents19 = scheduleEvents.Where(e => (e.EventDate.Date == date19.Date)).ToList();
                                                    if (scheduleEvents19 != null)
                                                    {
                                                        if (evnt19.Discipline == "PT")
                                                        {
                                                            evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                        }
                                                        else if (evnt19.Discipline == "OT")
                                                        {
                                                            evnt19check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                        }
                                                        else if (evnt19.Discipline == "ST")
                                                        {
                                                            evnt13check = scheduleEvents19.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                        }
                                                    }
                                                }
                                                if (evnt19 != null)
                                                {
                                                    episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate > DateTime.MinValue ? evnt19.EventDate.ToString("MM/dd") : string.Empty);
                                                }

                                            }
                                            else
                                            {
                                                episode.NineteenVisit = "NA";
                                            }
                                            if (!evnt19check || !evnt13check)
                                            {
                                                episode.ScheduledTherapy = scheduleEvents.Count;
                                                var completedSchedules = scheduleEvents.Where(s => completedNoteStatus.Contains(s.Status)).ToList();//(s.Status == (int)ScheduleStatus.NoteCompleted || s.Status == (int)ScheduleStatus.OasisCompletedExportReady || s.Status == (int)ScheduleStatus.OasisExported || s.Status == (int)ScheduleStatus.OasisCompletedNotExported)
                                                if (completedSchedules != null)
                                                {
                                                    episode.CompletedTherapy = completedSchedules.Count;
                                                }
                                                episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                                episode.Schedule = string.Empty;
                                                therapyEpisodes.Add(episode);
                                            }
                                        }
                                    }
                                    else if (disciplines.Length > 1)
                                    {
                                        var discipline = string.Join(";", disciplines);
                                        var evnt19check = true;
                                        var evnt13check = true;
                                        var first = true;
                                        if (scheduleEvents.Count >= 13 && discipline.IsNotNullOrEmpty())
                                        {
                                            var scheduleEvents13 = scheduleEvents.Take(13).Reverse().ToList();
                                            var scheduleEvents13End = scheduleEvents13.Take(3).ToList();
                                            if (scheduleEvents13End != null)
                                            {
                                                if (discipline.Contains("PT"))
                                                {
                                                    first = false;
                                                    evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);
                                                }
                                                if (discipline.Contains("OT"))
                                                {
                                                    if (first)
                                                    {
                                                        first = false;
                                                        evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                    }
                                                    else
                                                    {
                                                        evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                    }
                                                }
                                                if (discipline.Contains("ST"))
                                                {
                                                    if (first)
                                                    {
                                                        first = false;
                                                        evnt13check = scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                    }
                                                    else
                                                    {
                                                        evnt13check = evnt13check && scheduleEvents13End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                    }
                                                }
                                            }
                                            if (!evnt13check)
                                            {
                                                var evnt13 = scheduleEvents[12];
                                                if (evnt13 != null)
                                                {
                                                    episode.ThirteenVisit = evnt13.DisciplineTaskName + ":" + (evnt13.EventDate > DateTime.MinValue ? evnt13.EventDate.ToString("MM/dd") : string.Empty);
                                                }
                                            }
                                            first = true;
                                            if (scheduleEvents.Count >= 19)
                                            {
                                                var evnt19 = scheduleEvents[18];
                                                var scheduleEvents19 = scheduleEvents.Take(19).Reverse().ToList();
                                                var scheduleEvents19End = scheduleEvents19.Take(3).ToList();
                                                if (scheduleEvents19End != null)
                                                {
                                                    if (discipline.Contains("PT"))
                                                    {
                                                        first = false;
                                                        evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.PTEvaluation);

                                                    }

                                                    if (discipline.Contains("OT"))
                                                    {
                                                        if (first)
                                                        {
                                                            first = false;
                                                            evnt19check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                        }
                                                        else
                                                        {
                                                            evnt19check = evnt19check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation || e.DisciplineTask == (int)DisciplineTasks.OTEvaluation);
                                                        }
                                                    }
                                                    if (discipline.Contains("ST"))
                                                    {
                                                        if (first)
                                                        {
                                                            first = false;
                                                            evnt13check = scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                        }
                                                        else
                                                        {
                                                            evnt13check = evnt13check && scheduleEvents19End.Exists(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation || e.DisciplineTask == (int)DisciplineTasks.STEvaluation);
                                                        }
                                                    }
                                                }
                                                if (evnt19 != null)
                                                {
                                                    episode.NineteenVisit = evnt19.DisciplineTaskName + ":" + (evnt19.EventDate > DateTime.MinValue ? evnt19.EventDate.ToString("MM/dd") : string.Empty);
                                                }
                                            }
                                            if (!evnt19check || !evnt13check)
                                            {
                                                episode.ScheduledTherapy = scheduleEvents.Count;
                                                var completedSchedules = scheduleEvents.Where(s => completedNoteStatus.Contains(s.Status)).ToList();//(s.Status == (int)ScheduleStatus.NoteCompleted || s.Status == (int)ScheduleStatus.OasisCompletedExportReady || s.Status == (int)ScheduleStatus.OasisExported || s.Status == (int)ScheduleStatus.OasisCompletedNotExported)
                                                if (completedSchedules != null)
                                                {
                                                    episode.CompletedTherapy = completedSchedules.Count;
                                                }
                                                episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                                episode.Schedule = string.Empty;
                                                therapyEpisodes.Add(episode);
                                            }
                                        }
                                    }
                                }
                            }

                        });
                    }
                }
            }
            return therapyEpisodes.OrderBy(o => o.PatientName).ToList();
        }

        public List<PatientEpisodeTherapyException> GetTherapyReevaluationException(Guid branchId, int count, DateTime startDate, DateTime endDate)
        {
            var therapyEpisodes = new List<PatientEpisodeTherapyException>();
            var aprilStartDate = new DateTime(2011, 04, 01);
            startDate = startDate > aprilStartDate ? startDate : aprilStartDate;
            if (endDate > startDate)
            {
                var allScheduleEvents = patientRepository.GetTherapyExceptionScheduleEvents(Current.AgencyId, branchId, startDate, endDate);
                if (allScheduleEvents != null && allScheduleEvents.Count > 0)
                {
                    var episodeIds = allScheduleEvents.Select(s => s.EpisodeId).Distinct().ToList();
                    if (episodeIds != null && episodeIds.Count > 0)
                    {
                        var completedNoteStatus = ScheduleStatusFatory.OASISAndNurseNotesAfterQA();
                        episodeIds.ForEach(Id =>
                        {
                            var scheduleEvents = allScheduleEvents.Where(e => e.EpisodeId == Id).OrderBy(e => e.EventDate.Date).ToList();
                            if (scheduleEvents != null && scheduleEvents.Count >= count)
                            {
                                var schedule = scheduleEvents.FirstOrDefault();
                                if (schedule != null)
                                {
                                    var episode = new PatientEpisodeTherapyException
                                    {
                                        PatientIdNumber = schedule.PatientIdNumber,
                                        EndDate = schedule.EndDate,
                                        StartDate = schedule.StartDate,
                                        PatientName = schedule.PatientName
                                    };

                                    var disciplines = scheduleEvents.Where(e => e.Discipline.IsNotNullOrEmpty()).Select(e => e.Discipline).Distinct().ToArray();
                                    if (disciplines.Length > 0)
                                    {
                                        var discipline = string.Join(";", disciplines);
                                        if (discipline.IsNotNullOrEmpty())
                                        {
                                            var evntCommon = scheduleEvents[count - 1];
                                            var scheduleEventsCommon = scheduleEvents.Take(count).Reverse().ToList();
                                            var scheduleEventsCommonEnd = scheduleEventsCommon.Take(3).ToList();
                                            if (scheduleEventsCommonEnd != null)
                                            {
                                                if (discipline.Contains("PT"))
                                                {
                                                    var ptEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.PTReEvaluation);
                                                    if (ptEval != null && ptEval.EventDate.IsValid())
                                                    {
                                                        episode.PTEval = ptEval.EventDate.ToString("MM/dd");
                                                    }
                                                }
                                                else
                                                {
                                                    episode.PTEval = "NA";
                                                }
                                                if (discipline.Contains("OT"))
                                                {
                                                    var otEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.OTReEvaluation);
                                                    if (otEval != null && otEval.EventDate.IsValid())
                                                    {
                                                        episode.OTEval = otEval.EventDate.ToString("MM/dd");
                                                    }
                                                }
                                                else
                                                {
                                                    episode.OTEval = "NA";
                                                }
                                                if (discipline.Contains("ST"))
                                                {
                                                    var stEval = scheduleEventsCommonEnd.Find(e => e.DisciplineTask == (int)DisciplineTasks.STReEvaluation);
                                                    if (stEval != null && stEval.EventDate.IsValid())
                                                    {
                                                        episode.STEval = stEval.EventDate.ToString("MM/dd");
                                                    }
                                                }
                                                else
                                                {
                                                    episode.STEval = "NA";
                                                }
                                            }
                                            episode.ScheduledTherapy = scheduleEvents.Count;
                                            var completedSchedules = scheduleEvents.Where(s => completedNoteStatus.Contains(s.Status)).ToList();//(s.Status == (int)ScheduleStatus.NoteCompleted || s.Status == (int)ScheduleStatus.OasisCompletedExportReady || s.Status == (int)ScheduleStatus.OasisExported || s.Status == (int)ScheduleStatus.OasisCompletedNotExported)
                                            if (completedSchedules != null)
                                            {
                                                episode.CompletedTherapy = completedSchedules.Count;
                                            }
                                            episode.EpisodeDay = DateTime.Now.Date.Subtract(episode.StartDate.Date).Days;
                                            therapyEpisodes.Add(episode);
                                        }
                                    }

                                }
                            }
                        });
                    }
                }
            }
            return therapyEpisodes.OrderBy(o => o.PatientName).ToList();
        }

        public List<TaskLog> GetTaskLogs(Guid patientId, Guid eventId, int task)
        {
            var taskLogs = new List<TaskLog>();
            var taskAudit = logRepository.GetTaskAudit(Current.AgencyId, patientId, eventId, task);
            if (taskAudit != null && taskAudit.Log.IsNotNullOrEmpty())
            {
                var logs = taskAudit.Log.ToObject<List<TaskLog>>();
                if (logs != null && logs.Count > 0)
                {
                    logs.ForEach(log =>
                    {
                        log.UserName = log.UserId.IsEmpty() ? log.UserName : UserEngine.GetName(log.UserId, Current.AgencyId);
                        taskLogs.Add(log);
                    });
                }
            }
            return taskLogs;
        }

        public List<AppAudit> GetGeneralLogs(LogDomain logDomain, LogType logType, Guid domainId, string entityId)
        {
            var generalLogs = new List<AppAudit>();
            var logs = logRepository.GetGeneralAudits(Current.AgencyId, logDomain.ToString(), logType.ToString(), domainId, entityId.ToString());
            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(log =>
                {
                    log.UserName = UserEngine.GetName(log.UserId, Current.AgencyId);
                    generalLogs.Add(log);
                });
            }
            return generalLogs;
        }

        public List<AppAudit> GetMedicationLogs(LogDomain logDomain, LogType logType, Guid domainId)
        {
            var generalLogs = new List<AppAudit>();
            var logs = logRepository.GetMedicationAudits(Current.AgencyId, logDomain.ToString(), domainId);
            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(log =>
                {
                    log.UserName = UserEngine.GetName(log.UserId, Current.AgencyId);
                    generalLogs.Add(log);
                });
            }
            return generalLogs;
        }

        public List<ScheduleEvent> GetPreviousNotes(Guid patientId, ScheduleEvent scheduledEvent)
        {
            var schedules = new List<ScheduleEvent>();
            if (DisciplineTaskFactory.SkilledNurseDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.SkilledNurseDisciplineTasks().ToArray(), ScheduleStatusFatory.NoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.PTNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.PTNoteDisciplineTasks().ToArray(), ScheduleStatusFatory.NoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.PTEvalDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.PTEvalDisciplineTasks().ToArray(), ScheduleStatusFatory.EvalNoteCompleted().ToArray()) ;
            }
            else if (DisciplineTaskFactory.PTDischargeDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.PTDischargeDisciplineTasks().ToArray(), ScheduleStatusFatory.NoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.OTNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.OTNoteDisciplineTasks().ToArray(), ScheduleStatusFatory.NoteCompleted().ToArray()) ;
            }
            else if (DisciplineTaskFactory.OTEvalDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.OTEvalDisciplineTasks().ToArray(), ScheduleStatusFatory.EvalNoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.STNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.STNoteDisciplineTasks().ToArray(),  ScheduleStatusFatory.NoteCompleted().ToArray()) ;
            }
            else if (DisciplineTaskFactory.STEvalDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.STEvalDisciplineTasks().ToArray(), ScheduleStatusFatory.EvalNoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().ToArray(), ScheduleStatusFatory.NoteCompleted().ToArray());
            }
            else if (DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().Exists(s => s == scheduledEvent.DisciplineTask))
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, DisciplineTaskFactory.MSWProgressNoteDisciplineTasks().ToArray(), ScheduleStatusFatory.NoteCompleted().ToArray()) ;
            }
            else if (scheduledEvent.Discipline == Disciplines.HHA.ToString())
            {
                schedules = patientRepository.GetPreviousNotes(Current.AgencyId, scheduledEvent, new string[] { Disciplines.HHA.ToString() }, new int[] { scheduledEvent.DisciplineTask }, ScheduleStatusFatory.NoteCompleted().ToArray());
            }
            return schedules ?? new List<ScheduleEvent>();
        }

        public bool UpdateAllergy(Allergy allergy)
        {
            var result = false;
            if (allergy != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfile(allergy.ProfileId, Current.AgencyId);
                if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
                {
                    var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                    if (existingList.Exists(a => a.Id == allergy.Id))
                    {
                        var exisitingAllergy = existingList.Single(m => m.Id == allergy.Id);
                        if (exisitingAllergy != null)
                        {
                            exisitingAllergy.Name = allergy.Name;
                            exisitingAllergy.Type = allergy.Type;
                            allergyProfile.Allergies = existingList.ToXml<List<Allergy>>();

                            if (patientRepository.UpdateAllergyProfile(allergyProfile))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, allergyProfile.PatientId, allergyProfile.Id.ToString(), LogType.AllergyProfile, LogAction.AllergyUpdated, string.Empty);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateAllergy(Guid allergyProfileId, Guid allergyId, bool isDeleted)
        {
            var result = false;
            var allergyProfile = patientRepository.GetAllergyProfile(allergyProfileId, Current.AgencyId);
            if (allergyProfile != null && allergyProfile.Allergies.IsNotNullOrEmpty())
            {
                var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                if (existingList.Exists(a => a.Id == allergyId))
                {
                    var allergy = existingList.Single(m => m.Id == allergyId);
                    if (allergy != null)
                    {
                        allergy.IsDeprecated = isDeleted;
                        allergyProfile.Allergies = existingList.ToXml<List<Allergy>>();

                        if (patientRepository.UpdateAllergyProfile(allergyProfile))
                        {
                            Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, allergyProfile.PatientId, allergyProfileId.ToString(), LogType.AllergyProfile, LogAction.AllergyDeleted, string.Empty);
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        public bool AddAllergy(Allergy allergy)
        {
            var result = false;
            if (allergy != null)
            {
                var allergyProfile = patientRepository.GetAllergyProfile(allergy.ProfileId, Current.AgencyId);
                if (allergyProfile != null)
                {
                    allergy.Id = Guid.NewGuid();
                    if (allergyProfile.Allergies.IsNullOrEmpty())
                    {
                        var newList = new List<Allergy>() { allergy };
                        allergyProfile.Allergies = newList.ToXml();
                    }
                    else
                    {
                        var existingList = allergyProfile.Allergies.ToObject<List<Allergy>>();
                        existingList.Add(allergy);
                        allergyProfile.Allergies = existingList.ToXml();
                    }
                    if (patientRepository.UpdateAllergyProfile(allergyProfile))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, allergyProfile.PatientId, allergyProfile.Id.ToString(), LogType.AllergyProfile, LogAction.AllergyAdded, string.Empty);
                        result = true;
                    }
                }
            }

            return result;
        }

        public bool AddHospitalizationLog(FormCollection formCollection)
        {
            var result = false;

            var hospitalizationLog = new HospitalizationLog
            {
                Id = Guid.NewGuid(),
                UserId = formCollection.GetGuid("UserId"),
                PatientId = formCollection.GetGuid("PatientId"),
                EpisodeId = formCollection.GetGuid("EpisodeId"),
                HospitalizationDate = formCollection.GetString("M0906DischargeDate").IsNotNullOrEmpty() && formCollection.GetString("M0906DischargeDate").IsDate() ? formCollection.GetString("M0906DischargeDate").ToDateTime() : DateTime.MinValue,
                LastHomeVisitDate = formCollection.GetString("M0903LastHomeVisitDate").IsNotNullOrEmpty() && formCollection.GetString("M0903LastHomeVisitDate").IsDate() ? formCollection.GetString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue,
                Data = ProcessHospitalizationData(formCollection),
                Created = DateTime.Now,
                AgencyId = Current.AgencyId,
                SourceId = (int)TransferSourceTypes.User,
                Modified = DateTime.Now
            };

            if (patientRepository.AddHospitalizationLog(hospitalizationLog))
            {
                var patient = patientRepository.Get(hospitalizationLog.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    patient.IsHospitalized = true;
                    patient.HospitalizationId = hospitalizationLog.Id;
                    if (patientRepository.Update(patient))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool UpdateHospitalizationLog(FormCollection formCollection)
        {
            var result = false;

            var logId = formCollection.GetGuid("Id");
            var patientId = formCollection.GetGuid("PatientId");
            var hospitalizationLog = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, logId);
            if (hospitalizationLog != null)
            {
                hospitalizationLog.UserId = formCollection.GetGuid("UserId");
                hospitalizationLog.EpisodeId = formCollection.GetGuid("EpisodeId");
                hospitalizationLog.Modified = DateTime.Now;
                hospitalizationLog.HospitalizationDate = formCollection.GetString("M0906DischargeDate").IsNotNullOrEmpty() && formCollection.GetString("M0906DischargeDate").IsDate() ? formCollection.GetString("M0906DischargeDate").ToDateTime() : DateTime.MinValue;
                hospitalizationLog.LastHomeVisitDate = formCollection.GetString("M0903LastHomeVisitDate").IsNotNullOrEmpty() && formCollection.GetString("M0903LastHomeVisitDate").IsDate() ? formCollection.GetString("M0903LastHomeVisitDate").ToDateTime() : DateTime.MinValue;
                hospitalizationLog.Data = ProcessHospitalizationData(formCollection);

                if (patientRepository.UpdateHospitalizationLog(hospitalizationLog))
                {
                    result = true;
                }
            }

            return result;
        }

        public List<HospitalizationLog> GetHospitalizationLogs(Guid agencyId, Guid patientId)
        {
            var logs = patientRepository.GetHospitalizationLogs(patientId, Current.AgencyId);
            if (logs != null && logs.Count > 0)
            {
                logs.ForEach(l =>
                {
                    l.PrintUrl = "<a onclick=\"Patient.Hospitalization.Print('" + l.Id + "', '" + l.PatientId + "');return false\"><span class=\"img icon print\"></span></a>";
                });
            }
            return logs;
        }

        public HospitalizationLog GetHospitalizationLog(Guid patientId, Guid hospitalizationLogId)
        {
            var log = patientRepository.GetHospitalizationLog(Current.AgencyId, patientId, hospitalizationLogId);
            if (log != null)
            {
                log.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                log.Patient = patientRepository.GetPatientOnly(log.PatientId, Current.AgencyId);

                if (!log.EpisodeId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeById(Current.AgencyId, log.EpisodeId, log.PatientId);
                    if (episode != null)
                    {
                        log.EpisodeRange = string.Format("{0} - {1}", episode.StartDateFormatted, episode.EndDateFormatted);
                    }
                }
            }
            return log;
        }

        public List<NonAdmit> GetNonAdmits()
        {
            var list = new List<NonAdmit>();
            var patients = patientRepository.FindPatientOnly((int)PatientStatus.NonAdmission, Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    list.Add(new NonAdmit
                    {
                        Id = p.Id,
                        LastName = p.LastName,
                        FirstName = p.FirstName,
                        DateOfBirth = p.DOBFormatted,
                        Phone = p.PhoneHomeFormatted,
                        Type = NonAdmitTypes.Patient,
                        MiddleInitial = p.MiddleInitial,
                        PatientIdNumber = p.PatientIdNumber,
                        NonAdmissionReason = p.NonAdmissionReason,
                        NonAdmitDate = p.NonAdmissionDateFormatted
                    });
                });
            }

            var referrals = referralRepository.GetAll(Current.AgencyId, ReferralStatus.NonAdmission).ToList();
            if (referrals != null && referrals.Count > 0)
            {
                referrals.ForEach(r =>
                {
                    list.Add(new NonAdmit
                    {
                        Id = r.Id,
                        LastName = r.LastName,
                        FirstName = r.FirstName,
                        DateOfBirth = r.DOBFormatted,
                        Phone = r.PhoneHomeFormatted,
                        Type = NonAdmitTypes.Referral,
                        PatientIdNumber = string.Empty,
                        NonAdmissionReason = r.NonAdmissionReason,
                        NonAdmitDate = r.NonAdmissionDateFormatted
                    });
                });
            }

            return list.OrderByDescending(l => l.NonAdmitDate).ToList();
        }

        public List<PendingPatient> GetPendingPatients()
        {
            var patients = patientRepository.GetPendingByAgencyId(Current.AgencyId);
            if (patients != null && patients.Count > 0)
            {
                patients.ForEach(p =>
                {
                    if (p.PrimaryInsurance > 0)
                    {
                        var insurance = InsuranceEngine.Instance.Get(p.PrimaryInsurance, Current.AgencyId);
                        if (insurance != null && insurance.Name.IsNotNullOrEmpty())
                        {
                            p.PrimaryInsuranceName = insurance.Name;
                        }
                    }
                });
            }

            return patients;
        }

        public PatientInsuranceInfoViewData PatientInsuranceInfo(Guid patientId, int insuranceId, string insuranceType)
        {
            var viewData = new PatientInsuranceInfoViewData();
            viewData.InsuranceType = insuranceType.ToTitleCase();
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    if (insuranceType.IsEqual("Primary"))
                    {
                        if (patient.PrimaryInsurance == insuranceId)
                        {
                            viewData.HealthPlanId = patient.PrimaryHealthPlanId;
                            viewData.GroupName = patient.PrimaryGroupName;
                            viewData.GroupId = patient.PrimaryGroupId;
                        }
                        viewData.InsuranceType = "Primary";
                    }
                    else if (insuranceType.IsEqual("Secondary"))
                    {
                        if (patient.SecondaryInsurance == insuranceId)
                        {
                            viewData.HealthPlanId = patient.SecondaryHealthPlanId;
                            viewData.GroupName = patient.SecondaryGroupName;
                            viewData.GroupId = patient.SecondaryGroupId;
                        }
                        viewData.InsuranceType = "Secondary";

                    }
                    else if (insuranceType.IsEqual("Tertiary"))
                    {
                        if (patient.TertiaryInsurance == insuranceId)
                        {
                            viewData.HealthPlanId = patient.TertiaryHealthPlanId;
                            viewData.GroupId = patient.TertiaryGroupId;
                            viewData.GroupName = patient.TertiaryGroupName;
                        }
                        viewData.InsuranceType = "Tertiary";
                    }
                }
            }
            return viewData;
        }

        public bool IsValidAdmissionPeriod(Guid admissionId, Guid patientId, DateTime startOfCareDate, DateTime dischargeDate)
        {
            bool result = true;
            var admissionPeriods = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
            if (admissionPeriods != null && admissionPeriods.Count>0)
            {
                foreach (var a in admissionPeriods)
                {
                    if (a.StartOfCareDate.IsValid() && a.DischargedDate.IsValid() && a.StartOfCareDate.Date <= a.DischargedDate.Date)
                    {
                        if (a.Id == admissionId)
                        {
                            continue;
                        }
                        else
                        {
                            if ((startOfCareDate.Date > a.StartOfCareDate.Date && startOfCareDate.Date < a.DischargedDate.Date) || (dischargeDate.Date > a.StartOfCareDate.Date && dischargeDate.Date < a.DischargedDate.Date) || (startOfCareDate.Date < a.StartOfCareDate.Date && dischargeDate.Date > a.StartOfCareDate.Date) || (startOfCareDate.Date < a.DischargedDate.Date && dischargeDate.Date > a.DischargedDate.Date) ||(startOfCareDate.Date==a.StartOfCareDate.Date && dischargeDate.Date==a.DischargedDate.Date))
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return result;
        }

        public bool IsValidAdmissionPeriod(Guid patientId, DateTime startOfCareDate, DateTime dischargeDate)
        {
            bool result = true;
            var admissionPeriods = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
            if (admissionPeriods != null && admissionPeriods.Count > 0)
            {
                foreach (var a in admissionPeriods)
                {
                    if (a.StartOfCareDate.IsValid() && a.DischargedDate.IsValid() && a.StartOfCareDate.Date <= a.DischargedDate.Date)
                    {
                        if ((startOfCareDate.Date > a.StartOfCareDate.Date && startOfCareDate.Date < a.DischargedDate.Date) || (dischargeDate.Date > a.StartOfCareDate.Date && dischargeDate.Date < a.DischargedDate.Date) || (startOfCareDate.Date < a.StartOfCareDate.Date && dischargeDate.Date > a.StartOfCareDate.Date) || (startOfCareDate.Date < a.DischargedDate.Date && dischargeDate.Date > a.DischargedDate.Date) || (startOfCareDate.Date == a.StartOfCareDate.Date && dischargeDate.Date == a.DischargedDate.Date))
                            {
                                result = false;
                                break;
                            }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return result;
        }

        public bool MarkPatientAdmissionCurrent(Guid patientId, Guid Id)
        {
            var result = false;
            try
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var oldAdmissionId = patient.AdmissionId;
                    var admission = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientId, Id);
                    if (admission != null)
                    {
                        patient.AdmissionId = admission.Id;
                        admission.PatientData = patient.ToXml();
                        if (patientRepository.Update(patient))
                        {
                            if (patientRepository.UpdatePatientAdmissionDateModal(admission))
                            {
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patient.Id, patient.Id.ToString(), LogType.Patient, LogAction.PatientAdmissionPeriodEdited, string.Empty);
                                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientId, Id.ToString(), LogType.ManagedDate, LogAction.AdmissionPeriodSetCurrent, string.Empty);

                                return true;
                            }
                            else
                            {
                                patient.AdmissionId = oldAdmissionId;
                                patientRepository.Update(patient);
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public VisitNoteViewData GetVisitNote(ScheduleEvent scheduledEvent, NoteArguments arguments)
        {
            var noteViewData = new VisitNoteViewData();
            noteViewData.Agency = agencyRepository.Get(Current.AgencyId);
            if (scheduledEvent != null)
            {
                var patient = patientRepository.Get(arguments.PatientId, Current.AgencyId);
                if (patient != null)
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, arguments.EpisodeId, arguments.PatientId);
                    if (episode != null)
                    {

                        var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, arguments.EpisodeId, arguments.PatientId, arguments.EventId);
                        if (patientvisitNote != null)
                        {
                            IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                            noteViewData.StatusComment = scheduledEvent.StatusComment;
                            noteViewData.UserId = scheduledEvent.UserId;
                            if (arguments.IsDiagnosisNeeded || arguments.IsPlanOfCareNeeded)
                            {
                                var assessment = this.GetEpisodeAssessment(arguments.EpisodeId, arguments.PatientId, scheduledEvent.EventDate);
                                if (assessment != null)
                                {
                                    if (arguments.IsDiagnosisNeeded)
                                    {
                                        assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                                        oasisQuestions = assessment.ToNotesQuestionDictionary();
                                    }
                                    if (arguments.IsPlanOfCareNeeded)
                                    {
                                        noteViewData.CarePlanOrEvalUrl = assessmentService.GetPlanofCareUrl(arguments.EpisodeId, arguments.PatientId, assessment.Id, assessment.Type.ToString());
                                    }
                                }
                            }

                            if (ScheduleStatusFatory.NursingNoteNotYetStarted().Exists(s => s == patientvisitNote.Status))
                            {
                                if (scheduledEvent.DisciplineTask == (int)DisciplineTasks.HHAideVisit)
                                {
                                    var pocEvent = patientRepository.GetHHAPlanOfCareVisitNote(arguments.EpisodeId, arguments.PatientId);
                                    if (pocEvent != null)
                                    {
                                        noteViewData.Questions = pocEvent.ToHHADefaults();
                                    }
                                }
                                else if (scheduledEvent.DisciplineTask == (int)DisciplineTasks.PASVisit)
                                {
                                    var pocEvent = patientRepository.GetVisitNoteByType(arguments.EpisodeId, arguments.PatientId, DisciplineTasks.PASCarePlan);
                                    if (pocEvent != null)
                                    {
                                        noteViewData.Questions = pocEvent.ToDictionary();
                                    }
                                }
                                else if (scheduledEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary)
                                {
                                    noteViewData.Questions = noteViewData.Questions ?? new Dictionary<string, NotesQuestion>();
                                    var vitalSigns = this.GetVitalSignsForSixtyDaySummary(episode, scheduledEvent.EventDate);
                                    if (vitalSigns != null && vitalSigns.Count > 0)
                                    {
                                        #region BP

                                        var bp = new List<string>();
                                        var bpSysLow = int.MinValue;
                                        var bpSysHigh = int.MaxValue;
                                        var bpDiaLow = int.MinValue;
                                        var bpDiaHigh = int.MaxValue;

                                        var bpSitLeft = vitalSigns.Where(v => v.BPSittingLeft.IsNotNullOrEmpty()).Select(v => v.BPSittingLeft).ToList();
                                        var bpSitRight = vitalSigns.Where(v => v.BPSittingRight.IsNotNullOrEmpty()).Select(v => v.BPSittingRight).ToList();
                                        var bpStandLeft = vitalSigns.Where(v => v.BPStandingLeft.IsNotNullOrEmpty()).Select(v => v.BPStandingLeft).ToList();
                                        var bpStandRight = vitalSigns.Where(v => v.BPStandingRight.IsNotNullOrEmpty()).Select(v => v.BPStandingRight).ToList();
                                        var bpLyLeft = vitalSigns.Where(v => v.BPLyingLeft.IsNotNullOrEmpty()).Select(v => v.BPLyingLeft).ToList();
                                        var bpLyRight = vitalSigns.Where(v => v.BPLyingRight.IsNotNullOrEmpty()).Select(v => v.BPLyingRight).ToList();

                                        if (bpSitLeft != null && bpSitLeft.Count > 0) bp.AddRange(bpSitLeft.AsEnumerable());
                                        if (bpSitRight != null && bpSitRight.Count > 0) bp.AddRange(bpSitRight.AsEnumerable());
                                        if (bpStandLeft != null && bpStandLeft.Count > 0) bp.AddRange(bpStandLeft.AsEnumerable());
                                        if (bpStandRight != null && bpStandRight.Count > 0) bp.AddRange(bpStandRight.AsEnumerable());
                                        if (bpLyLeft != null && bpLyLeft.Count > 0) bp.AddRange(bpLyLeft.AsEnumerable());
                                        if (bpLyRight != null && bpLyRight.Count > 0) bp.AddRange(bpLyRight.AsEnumerable());

                                        if (bp != null && bp.Count > 0)
                                        {
                                            bpSysLow = bp.Select(v =>
                                            {
                                                var min = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                                if (min != null && min.Length > 1)
                                                {
                                                    if (min[0].IsInteger())
                                                    {
                                                        return min[0].ToInteger();
                                                    }
                                                }
                                                return int.MinValue;
                                            }).Min();

                                            bpSysHigh = bp.Select(v =>
                                            {
                                                var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                                if (max != null && max.Length > 1)
                                                {
                                                    if (max[0].IsInteger())
                                                    {
                                                        return max[0].ToInteger();
                                                    }
                                                }
                                                return int.MaxValue;
                                            }).Max();

                                            bpDiaLow = bp.Select(v =>
                                            {
                                                var min = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                                if (min != null && min.Length > 1)
                                                {
                                                    if (min[1].IsInteger())
                                                    {
                                                        return min[1].ToInteger();
                                                    }
                                                }
                                                return int.MinValue;
                                            }).Min();

                                            bpDiaHigh = bp.Select(v =>
                                            {
                                                var max = v.ToDigitsOnly().Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
                                                if (max != null && max.Length > 1)
                                                {
                                                    if (max[1].IsInteger())
                                                    {
                                                        return max[1].ToInteger();
                                                    }
                                                }
                                                return int.MaxValue;
                                            }).Max();
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignBPMax"))
                                        {
                                            noteViewData.Questions["VitalSignBPMax"].Answer = bpSysHigh != int.MaxValue ? bpSysHigh.ToString() : string.Empty;
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignBPMax", new NotesQuestion { Name = "VitalSignBPMax", Answer = bpSysHigh != int.MaxValue ? bpSysHigh.ToString() : string.Empty });
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignBPMin"))
                                        {
                                            noteViewData.Questions["VitalSignBPMin"].Answer = bpSysLow != int.MinValue ? bpSysLow.ToString() : string.Empty;
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignBPMin", new NotesQuestion { Name = "VitalSignBPMin", Answer = bpSysLow != int.MinValue ? bpSysLow.ToString() : string.Empty });
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignBPDiaMax"))
                                        {
                                            noteViewData.Questions["VitalSignBPDiaMax"].Answer = bpDiaHigh != int.MaxValue ? bpDiaHigh.ToString() : string.Empty;
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignBPDiaMax", new NotesQuestion { Name = "VitalSignBPDiaMax", Answer = bpDiaHigh != int.MaxValue ? bpDiaHigh.ToString() : string.Empty });
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignBPDiaMin"))
                                        {
                                            noteViewData.Questions["VitalSignBPDiaMin"].Answer = bpDiaLow != int.MinValue ? bpDiaLow.ToString() : string.Empty;
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignBPDiaMin", new NotesQuestion { Name = "VitalSignBPDiaMin", Answer = bpDiaLow != int.MinValue ? bpDiaLow.ToString() : string.Empty });
                                        }

                                        #endregion

                                        #region HR

                                        var apicalPulseMax = int.MinValue;
                                        var apicalPulseMin = int.MaxValue;
                                        var apicalPulse = vitalSigns.Where(v => v.ApicalPulse.IsNotNullOrEmpty() && v.ApicalPulse.IsInteger()).Select(v => v.ApicalPulse.ToInteger()).ToList();
                                        if (apicalPulse != null && apicalPulse.Count > 0)
                                        {
                                            apicalPulseMax = apicalPulse.Max();
                                            apicalPulseMin = apicalPulse.Min();
                                        }

                                        var radialPulseMax = int.MinValue;
                                        var radialPulseMin = int.MaxValue;
                                        var radialPulse = vitalSigns.Where(v => v.RadialPulse.IsNotNullOrEmpty() && v.RadialPulse.IsInteger()).Select(v => v.RadialPulse.ToInteger()).ToList();
                                        if (radialPulse != null && radialPulse.Count > 0)
                                        {
                                            radialPulseMax = radialPulse.Max();
                                            radialPulseMin = radialPulse.Min();
                                        }

                                        var maxHR = Math.Max(apicalPulseMax, radialPulseMax);
                                        if (noteViewData.Questions.ContainsKey("VitalSignHRMax"))
                                        {
                                            noteViewData.Questions["VitalSignHRMax"].Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty;
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignHRMax", new NotesQuestion { Name = "VitalSignHRMax", Answer = maxHR != int.MinValue ? maxHR.ToString() : string.Empty });
                                        }

                                        var minHR = Math.Min(apicalPulseMin, radialPulseMin);

                                        if (noteViewData.Questions.ContainsKey("VitalSignHRMin"))
                                        {
                                            noteViewData.Questions["VitalSignHRMin"].Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty;
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignHRMin", new NotesQuestion { Name = "VitalSignHRMin", Answer = minHR != int.MaxValue ? minHR.ToString() : string.Empty });
                                        }
                                        #endregion

                                        #region Resp

                                        var respMax = int.MaxValue;
                                        var respMin = int.MinValue;
                                        var resp = vitalSigns.Where(v => v.Resp.IsNotNullOrEmpty() && v.Resp.IsInteger()).Select(v => v.Resp.ToInteger()).ToList();
                                        if (resp != null && resp.Count > 0)
                                        {
                                            respMin = resp.Min();
                                            respMax = resp.Max();
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignRespMax"))
                                        {
                                            noteViewData.Questions["VitalSignRespMax"].Answer = respMax != int.MaxValue ? respMax.ToString() : "";
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignRespMax", new NotesQuestion { Name = "VitalSignRespMax", Answer = respMax != int.MaxValue ? respMax.ToString() : "" });
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignRespMin"))
                                        {
                                            noteViewData.Questions["VitalSignRespMin"].Answer = respMin != int.MinValue ? respMin.ToString() : "";
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignRespMin", new NotesQuestion { Name = "VitalSignRespMin", Answer = respMin != int.MinValue ? respMin.ToString() : "" });
                                        }

                                        #endregion

                                        #region Temp

                                        var tempMax = double.MaxValue;
                                        var tempMin = double.MinValue;
                                        var temp = vitalSigns.Where(v => v.Temp.IsNotNullOrEmpty() && v.Temp.IsDouble()).Select(v => v.Temp.ToDouble()).ToList();
                                        if (temp != null && temp.Count > 0)
                                        {
                                            tempMax = temp.Max();
                                            tempMin = temp.Min();
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignTempMax"))
                                        {
                                            noteViewData.Questions["VitalSignTempMax"].Answer = tempMax != double.MaxValue ? tempMax.ToString() : "";
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignTempMax", new NotesQuestion { Name = "VitalSignTempMax", Answer = tempMax != double.MaxValue ? tempMax.ToString() : "" });
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignTempMin"))
                                        {
                                            noteViewData.Questions["VitalSignTempMin"].Answer = tempMin != double.MinValue ? tempMin.ToString() : "";
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignTempMin", new NotesQuestion { Name = "VitalSignTempMin", Answer = tempMin != double.MinValue ? tempMin.ToString() : "" });
                                        }

                                        #endregion

                                        #region BS

                                        var bsMax = int.MaxValue;
                                        var bsMin = int.MinValue;
                                        var bsAllMax = vitalSigns.Where(v => v.BSMax.IsNotNullOrEmpty() && v.BSMax.IsInteger() && v.BSMax.ToInteger() > 0).Select(v => v.BSMax.Replace("/", "").ToDigitsOnly().ToInteger()).ToList();
                                        var bsAllMin = vitalSigns.Where(v => v.BSMin.IsNotNullOrEmpty() && v.BSMin.IsInteger() && v.BSMin.ToInteger() > 0).Select(v => v.BSMin.Replace("/", "").ToDigitsOnly().ToInteger()).ToList();
                                        if (bsAllMax != null && bsAllMax.Count > 0)
                                        {
                                            bsMax = bsAllMax.Max();
                                        }
                                        if (bsAllMin != null && bsAllMin.Count > 0)
                                        {
                                            bsMin = bsAllMin.Min();
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignBGMax"))
                                        {
                                            noteViewData.Questions["VitalSignBGMax"].Answer = bsMax != int.MaxValue ? bsMax.ToString() : "";
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignBGMax", new NotesQuestion { Name = "VitalSignBGMax", Answer = bsMax != int.MaxValue ? bsMax.ToString() : "" });
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignBGMin"))
                                        {
                                            noteViewData.Questions["VitalSignBGMin"].Answer = bsMin != int.MinValue ? bsMin.ToString() : "";
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignBGMin", new NotesQuestion { Name = "VitalSignBGMin", Answer = bsMin != int.MinValue ? bsMin.ToString() : "" });
                                        }
                                        #endregion

                                        #region Weight

                                        var weightMax = double.MaxValue;
                                        var weightMin = double.MinValue;
                                        var weight = vitalSigns.Where(v => v.Weight.IsNotNullOrEmpty() && v.Weight.IsDouble()).Select(v => v.Weight.ToDouble()).ToList();
                                        if (weight != null && weight.Count > 0)
                                        {
                                            weightMin = weight.Min();
                                            weightMax = weight.Max();
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignWeightMax"))
                                        {
                                            noteViewData.Questions["VitalSignWeightMax"].Answer = weightMax != double.MaxValue ? weightMax.ToString() : "";
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignWeightMax", new NotesQuestion { Name = "VitalSignWeightMax", Answer = weightMax != double.MaxValue ? weightMax.ToString() : "" });
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignWeightMin"))
                                        {
                                            noteViewData.Questions["VitalSignWeightMin"].Answer = weightMin != double.MinValue ? weightMin.ToString() : "";
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignWeightMin", new NotesQuestion { Name = "VitalSignWeightMin", Answer = weightMin != double.MinValue ? weightMin.ToString() : "" });
                                        }

                                        #endregion

                                        #region Pain

                                        var painMax = int.MaxValue;
                                        var painMin = int.MinValue;
                                        var pain = vitalSigns.Where(v => v.PainLevel.IsNotNullOrEmpty() && v.PainLevel.IsInteger()).Select(v => v.PainLevel.ToInteger()).ToList();
                                        if (pain != null && pain.Count > 0)
                                        {
                                            painMin = pain.Min();
                                            painMax = pain.Max();
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignPainMax"))
                                        {
                                            noteViewData.Questions["VitalSignPainMax"].Answer = painMax != int.MaxValue ? painMax.ToString() : "";
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignPainMax", new NotesQuestion { Name = "VitalSignPainMax", Answer = painMax != int.MaxValue ? painMax.ToString() : "" });
                                        }

                                        if (noteViewData.Questions.ContainsKey("VitalSignPainMin"))
                                        {
                                            noteViewData.Questions["VitalSignPainMin"].Answer = painMin != int.MinValue ? painMin.ToString() : "";
                                        }
                                        else
                                        {
                                            noteViewData.Questions.Add("VitalSignPainMin", new NotesQuestion { Name = "VitalSignPainMin", Answer = painMin != int.MinValue ? painMin.ToString() : "" });
                                        }

                                        #endregion
                                    }
                                }
                                noteViewData.Questions = noteViewData.Questions ?? new Dictionary<string, NotesQuestion>();
                                if (arguments.IsPhysicainNeeded)
                                {
                                    if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                                    {
                                        var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                                        if (physician != null)
                                        {
                                            noteViewData.PhysicianId = physician.Id;
                                            noteViewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                noteViewData.Questions = patientvisitNote.ToDictionary() ?? new Dictionary<string, NotesQuestion>();
                                if (arguments.IsPhysicainNeeded)
                                {
                                    var physician = this.GetPhysicianFromNotes(scheduledEvent, patientvisitNote, noteViewData.Questions);
                                    if (physician != null)
                                    {
                                        noteViewData.PhysicianId = physician.Id;
                                        noteViewData.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                    }
                                }
                            }

                            noteViewData.EndDate = episode.EndDate;
                            noteViewData.StartDate = episode.StartDate;
                            noteViewData.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString("MM/dd/yyyy") : scheduledEvent.EventDate.ToString("MM/dd/yyyy");
                            noteViewData.IsWoundCareExist = patientvisitNote.IsWoundCare;
                            noteViewData.IsSupplyExist = patientvisitNote.IsSupplyExist;
                            noteViewData.PatientId = patientvisitNote.PatientId;
                            noteViewData.EpisodeId = patientvisitNote.EpisodeId;
                            noteViewData.EventId = patientvisitNote.Id;
                            noteViewData.DisciplineTask = scheduledEvent.DisciplineTask;
                            noteViewData.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                            noteViewData.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : "";
                            if (arguments.IsAllergyNeeded)
                            {
                                noteViewData.Allergies = this.GetAllergies(arguments.PatientId);
                            }
                            if (arguments.IsEvalNeeded)
                            {
                                noteViewData.CarePlanOrEvalUrl = this.GetScheduledEventUrl(scheduledEvent, arguments.EvalTask);
                            }
                            if (arguments.IsPreviousNoteNeeded)
                            {
                                noteViewData.PreviousNotes = this.GetPreviousNotes(arguments.PatientId, scheduledEvent);
                            }
                            if (arguments.IsDiagnosisNeeded)
                            {
                                noteViewData.Questions = this.GetDiagnosisMergedFromOASIS(noteViewData.Questions, oasisQuestions);
                            }
                            noteViewData.Patient = patient;
                            noteViewData.Version = patientvisitNote.Version;
                        }
                        else
                        {
                            noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                        }
                    }
                    else
                    {
                        noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                    }
                }
                else
                {
                    noteViewData.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                noteViewData.Questions = new Dictionary<string, NotesQuestion>();
            }

            return noteViewData;
        }

        public VisitNoteViewData GetVisitNoteForContent(Guid patientId, Guid noteId,Guid previousNoteId, string type)
        {
            var viewData = new VisitNoteViewData();
            viewData.Type = type;
             var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, noteId);
             if (scheduleEvent != null)
             {
                 var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, patientId, previousNoteId);
                 if (patientvisitNote != null)
                 {
                     viewData.PatientId = patientvisitNote.PatientId;
                     viewData.EpisodeId = scheduleEvent.EpisodeId;
                     viewData.EventId = scheduleEvent.EventId;
                     viewData.Version = scheduleEvent.Version > 0 ? scheduleEvent.Version : 1;
                     viewData.TypeName = viewData.Type.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), viewData.Type) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), viewData.Type)).GetDescription() : "";
                     if (DisciplineTaskFactory.SkilledNurseWithOutSNDiabetic().Contains(scheduleEvent.DisciplineTask))
                     {
                         viewData = SkilledNurseVisitContent(scheduleEvent, patientvisitNote, viewData);
                     }
                     else
                     {
                         viewData.Questions = patientvisitNote.ToDictionary();
                     }

                 }
             }
            return viewData;
        }

        public VisitNoteViewData GetVisitNotePrint()
        {
            var note = new VisitNoteViewData();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return note;
        }

        public VisitNoteViewData GetVisitNotePrint(int type)
        {
            var note = new VisitNoteViewData();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var task=Enum.IsDefined(typeof(DisciplineTasks), type) ? ((DisciplineTasks)type) : DisciplineTasks.NoDiscipline;
            note.Type = task.ToString();
            note.TypeName = task.GetDescription();
            return note;
        }

        public VisitNoteViewData GetVisitNotePrint(ScheduleEvent scheduledEvent, NoteArguments arguments)
        {
            var note = new VisitNoteViewData();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var patient = patientRepository.Get(scheduledEvent.PatientId, Current.AgencyId);
            if (patient != null)
            {
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, scheduledEvent.EpisodeId, scheduledEvent.PatientId);
                if (episode != null)
                {
                    if (scheduledEvent != null)
                    {
                        var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventId);
                        if (patientvisitNote != null)
                        {
                            if (arguments.IsAllergyNeeded)
                            {
                                var allergyProfile = patientRepository.GetAllergyProfileByPatient(scheduledEvent.PatientId, Current.AgencyId);
                                if (allergyProfile != null)
                                {
                                    note.Allergies = allergyProfile.ToString();
                                }
                            }
                            note.SignatureText = patientvisitNote.SignatureText;
                            note.SignatureDate = patientvisitNote.SignatureDate.ToShortDateString();
                            IDictionary<string, NotesQuestion> oasisQuestions = new Dictionary<string, NotesQuestion>();
                            if (arguments.IsAssessmentNeeded)
                            {
                                var assessment = GetEpisodeAssessment(scheduledEvent.EpisodeId, scheduledEvent.PatientId, scheduledEvent.EventDate);
                                if (assessment != null)
                                {
                                    assessment.Questions = assessment.OasisData.ToObject<List<Question>>();
                                    oasisQuestions = assessment.ToNotesQuestionDictionary();
                                }
                            }
                            if (ScheduleStatusFatory.NursingNoteNotYetStarted().Exists(s => s == patientvisitNote.Status))
                            {
                                note.Questions = note.Questions ?? new Dictionary<string, NotesQuestion>();
                                if (arguments.IsPhysicainNeeded)
                                {
                                    if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                                    {
                                        var physician = patient.PhysicianContacts.Where(p => p.Primary).SingleOrDefault();
                                        if (physician != null)
                                        {
                                            note.PhysicianId = physician.Id;
                                            note.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                note.Questions = patientvisitNote.ToDictionary() ?? new Dictionary<string, NotesQuestion>();
                                if (arguments.IsPhysicainNeeded)
                                {
                                    var physician = this.GetPhysicianFromNotes(scheduledEvent, patientvisitNote, note.Questions);
                                    if (physician != null)
                                    {
                                        note.PhysicianId = physician.Id;
                                        note.PhysicianDisplayName = physician.LastName + ", " + physician.FirstName;
                                    }
                                }
                            }
                            var selectedEpisodeId = note.Questions != null && note.Questions.ContainsKey("SelectedEpisodeId") && note.Questions["SelectedEpisodeId"].Answer.IsNotNullOrEmpty() ? note.Questions["SelectedEpisodeId"].Answer.ToGuid() : Guid.Empty;
                            if (!selectedEpisodeId.IsEmpty())
                            {
                                var episodeSelected = patientRepository.GetEpisodeById(Current.AgencyId, selectedEpisodeId, scheduledEvent.PatientId);
                                if (episodeSelected != null)
                                {
                                    note.EndDate = episodeSelected.EndDate;
                                    note.StartDate = episodeSelected.StartDate;
                                }
                            }
                            else
                            {
                                note.EndDate = episode.EndDate;
                                note.StartDate = episode.StartDate;
                            }
                            note.IsWoundCareExist = patientvisitNote.IsWoundCare;
                            if (note.IsWoundCareExist)
                            {
                                note.WoundCare = patientvisitNote.ToWoundCareDictionary();
                            }
                            note.VisitDate = scheduledEvent.VisitDate.IsValid() ? scheduledEvent.VisitDate.ToString("MM/dd/yyyy") : scheduledEvent.EventDate.ToString("MM/dd/yyyy");
                            note.PatientId = patientvisitNote.PatientId;
                            note.EpisodeId = patientvisitNote.EpisodeId;
                            note.EventId = patientvisitNote.Id;
                            note.DisciplineTask = scheduledEvent.DisciplineTask;
                            note.Type = patientvisitNote.NoteType.IsNotNullOrEmpty() ? patientvisitNote.NoteType.Trim() : string.Empty;
                            note.TypeName = patientvisitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), patientvisitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), patientvisitNote.NoteType)).GetDescription() : string.Empty;
                            note.Version = patientvisitNote.Version;
                            if (arguments.IsAssessmentNeeded)
                            {
                                note.Questions = this.GetDiagnosisMergedFromOASIS(note.Questions, oasisQuestions);
                            }
                            note.Patient = patient;
                        }
                        else
                        {
                            note.Questions = new Dictionary<string, NotesQuestion>();
                        }
                    }
                    else
                    {
                        note.Questions = new Dictionary<string, NotesQuestion>();
                    }
                }
                else
                {
                    note.Questions = new Dictionary<string, NotesQuestion>();
                }
            }
            else
            {
                note.Questions = new Dictionary<string, NotesQuestion>();
            }
            return note;
        }

        public Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId)
        {
            Assessment assessment = null;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var oasis = Enum.GetValues(typeof(DisciplineTasks)).Cast<DisciplineTasks>().Where(d => d.ToString().ToLowerInvariant().Contains("startofcare") || d.ToString().ToLowerInvariant().Contains("resumptionofcare")).Select(d => (int)d).ToArray();
                var assessmentEvent = patientRepository.GetFirstScheduledEvents(Current.AgencyId, patientId, episodeId, episode.StartDate, episode.EndDate, episode.StartDate, episode.EndDate.AddDays(-6), oasis);
                if (assessmentEvent != null)
                {
                    assessment = assessmentRepository.Get(assessmentEvent.EventId, Current.AgencyId);
                }
                else
                {
                    var previousEpisode = patientRepository.GetLastEpisodeByEndDate(Current.AgencyId, patientId, episode.StartDate.AddDays(-1));
                    if (previousEpisode != null)
                    {
                        var previousOasis = Enum.GetValues(typeof(DisciplineTasks)).Cast<DisciplineTasks>().Where(d => d.ToString().ToLowerInvariant().Contains("recertification") || d.ToString().ToLowerInvariant().Contains("resumptionofcare")).Select(d => (int)d).ToArray();
                        var previousAssessmentEvent = patientRepository.GetLastScheduledEvents(Current.AgencyId, patientId, episodeId, previousEpisode.StartDate, previousEpisode.EndDate, previousEpisode.EndDate.AddDays(-5), previousEpisode.EndDate, previousOasis);
                        if (previousAssessmentEvent != null)
                        {
                            assessment = assessmentRepository.Get(previousAssessmentEvent.EventId, Current.AgencyId);
                        }
                    }
                }
            }
            return assessment;
        }

        public Assessment GetEpisodeAssessment(Guid episodeId, Guid patientId, DateTime eventDate)
        {
            Assessment assessment = null;
            var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (episode != null)
            {
                var oasis = Enum.GetValues(typeof(DisciplineTasks)).Cast<DisciplineTasks>().Where(d => d.ToString().ToLowerInvariant().Contains("resumptionofcare") || d.ToString().ToLowerInvariant().Contains("startofcare")).Select(d => (int)d).ToArray();
                var assessmentEvent = patientRepository.GetLastScheduledEvents(Current.AgencyId, patientId, episodeId, episode.StartDate, episode.EndDate, episode.StartDate, eventDate, oasis);
                if (assessmentEvent != null)
                {
                    assessment = assessmentRepository.Get(assessmentEvent.EventId,Current.AgencyId);
                }
                else
                {
                    var previousEpisode = patientRepository.GetLastEpisodeByEndDate(Current.AgencyId, patientId, episode.StartDate.AddDays(-1));
                    if (previousEpisode != null)
                    {
                        var previousOasis = Enum.GetValues(typeof(DisciplineTasks)).Cast<DisciplineTasks>().Where(d => d.ToString().ToLowerInvariant().Contains("recertification") || d.ToString().ToLowerInvariant().Contains("resumptionofcare")).Select(d => (int)d).ToArray();
                        var previousAssessmentEvent = patientRepository.GetLastScheduledEvents(Current.AgencyId, patientId, episodeId, previousEpisode.StartDate, previousEpisode.EndDate, previousEpisode.EndDate.AddDays(-5), previousEpisode.EndDate, previousOasis);
                        if (previousAssessmentEvent != null)
                        {
                            assessment = assessmentRepository.Get(previousAssessmentEvent.EventId,Current.AgencyId);
                        }
                    }
                }
            }
            return assessment;
        }

        public MissedVisit GetMissedVisitPrint()
        {
            var note = new MissedVisit();
            note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            return note;
        }

        public MissedVisit GetMissedVisitPrint(Guid patientId, Guid eventId)
        {
            var note = patientRepository.GetMissedVisit(Current.AgencyId, eventId);
            if (note != null)
            {
                note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
                note.Patient = patientRepository.Get(patientId, Current.AgencyId);
                var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, note.EpisodeId, eventId);
                if (scheduleEvent != null)
                {
                    note.EventDate = scheduleEvent.EventDate.IsValid() ? scheduleEvent.EventDate.ToString("MM/dd/yyyy") : string.Empty;
                    note.DisciplineTaskName = scheduleEvent != null && scheduleEvent.DisciplineTaskName.IsNotNullOrEmpty() ? scheduleEvent.DisciplineTaskName : string.Empty;
                }
            }
            return note;
        }

        public VisitNoteViewData GetTransportationNote(Guid episodeId, Guid patientId, Guid eventId)
        {
            var Note = new VisitNoteViewData();
            Note.Agency = agencyRepository.GetWithBranches(Current.AgencyId);
            var patient = patientRepository.Get(patientId, Current.AgencyId);
            if (patient != null)
            {
                var visitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (visitNote != null)
                {
                    var scheduledEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                    if (scheduledEvent != null)
                    {
                        var noteQuestions = visitNote.ToDictionary();
                        Note.Questions = noteQuestions;
                        Note.EndDate = scheduledEvent.EndDate;
                        Note.StartDate = scheduledEvent.StartDate;
                        Note.VisitDate = scheduledEvent.VisitDate.ToString("MM/dd/yyyy");
                        Note.DisciplineTask = scheduledEvent.DisciplineTask;
                        Note.PatientId = visitNote.PatientId;
                        Note.EpisodeId = visitNote.EpisodeId;
                        Note.EventId = visitNote.Id;
                        Note.UserId = scheduledEvent.UserId;
                        Note.Type = visitNote.NoteType.IsNotNullOrEmpty() ? visitNote.NoteType.Trim() : string.Empty;
                        Note.TypeName = visitNote.NoteType.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), visitNote.NoteType) ? ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), visitNote.NoteType)).GetDescription() : string.Empty;
                        Note.Version = visitNote.Version;
                    }
                    else
                    {
                        Note.Questions = new Dictionary<string, NotesQuestion>();
                    }
                }
                else
                {
                    Note.Questions = new Dictionary<string, NotesQuestion>();
                }
                Note.Patient = patient;
            }
            return Note;
        }

        public static MemoryStream ResizeAndEncodePhoto(Image original, string contentType, bool resizeIfWider)
        {
            int imageWidth = 140;
            int imageHeight = 140;
            MemoryStream memoryStream = null;

            if (original != null)
            {
                int destX = 0;
                int destY = 0;
                int sourceX = 0;
                int sourceY = 0;
                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;
                int sourceWidth = original.Width;
                int sourceHeight = original.Height;

                nPercentW = ((float)imageWidth / (float)sourceWidth);
                nPercentH = ((float)imageHeight / (float)sourceHeight);

                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                }
                else
                {
                    nPercent = nPercentW;
                }

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                var newImage = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

                newImage.SetResolution(original.HorizontalResolution, original.VerticalResolution);

                using (Graphics graphics = Graphics.FromImage(newImage))
                {
                    graphics.Clear(Color.White);
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.DrawImage(original,
                        new Rectangle(destX, destY, destWidth, destHeight),
                        new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                        GraphicsUnit.Pixel);
                }
                memoryStream = new MemoryStream();

                ImageCodecInfo imageCodec = null;
                ImageCodecInfo[] imageCodecs = ImageCodecInfo.GetImageEncoders();

                foreach (ImageCodecInfo imageCodeInfo in imageCodecs)
                {
                    if (imageCodeInfo.MimeType == contentType)
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }

                    if (imageCodeInfo.MimeType == "image/jpeg" && contentType == "image/pjpeg")
                    {
                        imageCodec = imageCodeInfo;
                        break;
                    }
                }
                if (imageCodec != null)
                {
                    EncoderParameters encoderParameters = new EncoderParameters();
                    encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
                    newImage.Save(memoryStream, imageCodec, encoderParameters);
                    newImage.Dispose();
                    original.Dispose();
                }
            }

            memoryStream.Position = 0;
            return memoryStream;
        }

        #endregion

        #region 

        public bool SaveNotesNew(string button, FormCollection formCollection)
        {
            var result = false;
            var keys = formCollection.AllKeys;
            string type = keys.Contains("Type") ? formCollection["Type"] : string.Empty;
            if (type.IsNotNullOrEmpty())
            {
                var eventId = keys.Contains(string.Format("{0}_EventId", type)) ? formCollection.Get(string.Format("{0}_EventId", type)).ToGuid() : Guid.Empty;
                var episodeId = keys.Contains(string.Format("{0}_EpisodeId", type)) ? formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid() : Guid.Empty;
                var patientId = keys.Contains(string.Format("{0}_PatientId", type)) ? formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid() : Guid.Empty;
                var timeIn = keys.Contains(string.Format("{0}_TimeIn", type)) && formCollection[string.Format("{0}_TimeIn", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeIn", type)] : string.Empty;
                var timeOut = keys.Contains(string.Format("{0}_TimeOut", type)) && formCollection[string.Format("{0}_TimeOut", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_TimeOut", type)] : string.Empty;
                var date = keys.Contains(string.Format("{0}_VisitDate", type)) && formCollection[string.Format("{0}_VisitDate", type)].IsNotNullOrEmpty() && formCollection[string.Format("{0}_VisitDate", type)].IsValidDate() ? formCollection.Get(string.Format("{0}_VisitDate", type)).ToDateTime() : DateTime.MinValue;
                int disciplineTask = keys.Contains("DisciplineTask") ? formCollection.Get("DisciplineTask").ToInteger() : 0;
                bool isActionSet = false;

                if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                   var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                    if (scheduleEvent != null)
                    {
                        var  patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                        if (patientVisitNote != null)
                        {
                            patientVisitNote.Questions = ProcessNoteQuestions(formCollection);
                            patientVisitNote.Note = patientVisitNote.Questions.ToXml();

                            var oldStatus = scheduleEvent.Status;
                            var oldReason = scheduleEvent.ReturnReason;
                            var oldPrintQueue = scheduleEvent.InPrintQueue;
                            var oldTimeIn = scheduleEvent.TimeIn;
                            var oldTimeOut = scheduleEvent.TimeOut;
                            var oldDisciplineTask = scheduleEvent.DisciplineTask;
                            var oldVisitDate = scheduleEvent.VisitDate;

                            if (button == ButtonAction.Save.ToString())
                            {
                                var status = patientVisitNote.Status;
                                if (status != (int)ScheduleStatus.NoteReturned && !(Current.HasRight(Permissions.AccessCaseManagement) && status == (int)ScheduleStatus.NoteSubmittedWithSignature))
                                {
                                    patientVisitNote.Status = (int)ScheduleStatus.NoteSaved;
                                }
                                if ( DisciplineTaskFactory.PhysicianEvalNotes().Exists(d => d.ToString() == type))
                                {
                                    patientVisitNote.PhysicianId = keys.Contains(string.Format("{0}_PhysicianId", type)) ? formCollection.Get(string.Format("{0}_PhysicianId", type)).ToGuid() : Guid.Empty;
                                    var physician = PhysicianEngine.Get(patientVisitNote.PhysicianId, Current.AgencyId);
                                    if (physician != null)
                                    {
                                        patientVisitNote.PhysicianData = physician.ToXml();
                                    }
                                }
                                patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                scheduleEvent.Status = patientVisitNote.Status;
                                scheduleEvent.TimeIn = timeIn;
                                scheduleEvent.TimeOut = timeOut;
                                scheduleEvent.DisciplineTask = disciplineTask;
                                if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary) && date.Date > DateTime.MinValue.Date)
                                {
                                    scheduleEvent.VisitDate = date;
                                }
                                isActionSet = true;
                            }
                            else if (button == ButtonAction.Complete.ToString())
                            {
                                patientVisitNote.Status = (int)ScheduleStatus.NoteSubmittedWithSignature;
                                if (Current.HasRight(Permissions.BypassCaseManagement))
                                {
                                    patientVisitNote.Status = (int)ScheduleStatus.NoteCompleted;
                                    if (DisciplineTaskFactory.PhysicianEvalNotes().Exists(d => (int)d == scheduleEvent.DisciplineTask))
                                    {
                                        patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                    }
                                }

                                if (DisciplineTaskFactory.PhysicianEvalNotes().Exists(d => d.ToString() == type))
                                {
                                    patientVisitNote.PhysicianId = formCollection[type + "_PhysicianId"].ToGuid();
                                    var physician = PhysicianEngine.Get(patientVisitNote.PhysicianId, Current.AgencyId);
                                    if (physician != null)
                                    {
                                        patientVisitNote.PhysicianData = physician.ToXml();
                                    }
                                }

                                patientVisitNote.UserId = Current.UserId;
                                patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                patientVisitNote.SignatureDate = formCollection[type + "_SignatureDate"].ToDateTime();
                                patientVisitNote.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                scheduleEvent.Status = patientVisitNote.Status;
                                scheduleEvent.TimeIn = timeIn;
                                scheduleEvent.TimeOut = timeOut;
                                scheduleEvent.DisciplineTask = disciplineTask;
                                if (!(scheduleEvent.DisciplineTask == (int)DisciplineTasks.SixtyDaySummary) && date.Date > DateTime.MinValue.Date)
                                {
                                    scheduleEvent.VisitDate = date;
                                }
                                isActionSet = true;
                            }
                            else if (button == ButtonAction.Approve.ToString())
                            {
                                patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                                if (DisciplineTaskFactory.PhysicianEvalNotes().Exists(d => (int)d == scheduleEvent.DisciplineTask))
                                {
                                    patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                                }
                                patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                scheduleEvent.InPrintQueue = true;
                                scheduleEvent.Status = patientVisitNote.Status;
                                scheduleEvent.ReturnReason = string.Empty;
                                isActionSet = true;
                            }
                            else if (button == ButtonAction.Return.ToString())
                            {
                                var returnSignature = keys.Contains(string.Format("{0}_ReturnForSignature", type)) && formCollection[string.Format("{0}_ReturnForSignature", type)].IsNotNullOrEmpty() ? formCollection[string.Format("{0}_ReturnForSignature", type)] : string.Empty;
                                patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                                if (returnSignature.IsNotNullOrEmpty())
                                {
                                    patientVisitNote.Status = ((int)ScheduleStatus.NoteReturnedForClinicianSignature);
                                }
                                patientVisitNote.NoteType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), disciplineTask)).ToString();
                                scheduleEvent.Status = patientVisitNote.Status;
                                scheduleEvent.ReturnReason = string.Empty;
                                isActionSet = true;
                            }
                            if (isActionSet)
                            {
                                if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                                {
                                    patientVisitNote.Modified = DateTime.Now;
                                    if (patientRepository.UpdateVisitNote(patientVisitNote))
                                    {
                                        if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        }
                                        result = true;
                                    }
                                    else
                                    {
                                        scheduleEvent.Status = oldStatus;
                                        scheduleEvent.ReturnReason = oldReason;
                                        scheduleEvent.InPrintQueue = oldPrintQueue;

                                        scheduleEvent.TimeIn = oldTimeIn;
                                        scheduleEvent.TimeOut = oldTimeOut;
                                        scheduleEvent.DisciplineTask = oldDisciplineTask;

                                        scheduleEvent.VisitDate = oldVisitDate;

                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        result = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool ProcessPhysicianOrder(Guid id, Guid episodeId, Guid patientId, string actionType, string reason)
        {
            var result = false;
            bool isOrderUpdates = true;
            bool isActionSet = false;
            if (!id.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, id);
                if (scheduleEvent != null) {
                    var physicianOrder = patientRepository.GetOrderOnly(id, patientId, Current.AgencyId);
                    if (physicianOrder != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        var oldReason = scheduleEvent.ReturnReason;
                        var oldPrintQueue = scheduleEvent.InPrintQueue;
                        var description = string.Empty;
                        if (actionType == ButtonAction.Approve.ToString())
                        {
                            physicianOrder.Status = ((int)ScheduleStatus.OrderToBeSentToPhysician);
                            scheduleEvent.InPrintQueue = true;
                            scheduleEvent.Status = physicianOrder.Status;
                            scheduleEvent.ReturnReason = string.Empty;
                            description = "Approved By:" + Current.UserFullName;
                            isActionSet = true;
                        }
                        else if (actionType == ButtonAction.Return.ToString())
                        {
                            physicianOrder.Status = ((int)ScheduleStatus.OrderReturnedForClinicianReview);
                            scheduleEvent.Status = physicianOrder.Status;
                            scheduleEvent.ReturnReason = reason;
                            description = "Returned By: " + Current.UserFullName;
                            isActionSet = true;
                        }
                        else if (actionType == ButtonAction.Print.ToString())
                        {
                            scheduleEvent.InPrintQueue = false;
                            isOrderUpdates = false;
                            isActionSet = true;
                        }
                        if (isActionSet)
                        {
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (isOrderUpdates)
                                {
                                    physicianOrder.Modified = DateTime.Now;
                                    if (patientRepository.UpdateOrder(physicianOrder))
                                    {
                                        if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, DisciplineTasks.PhysicianOrder, description);
                                        }
                                        result = true;
                                    }
                                    else
                                    {
                                        scheduleEvent.Status = oldStatus;
                                        scheduleEvent.ReturnReason = oldReason;
                                        scheduleEvent.InPrintQueue = oldPrintQueue;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        result = false;
                                    }
                                }
                                else result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool ProcessNotesNew(string button, Guid episodeId, Guid patientId, Guid eventId, string reason)
        {
            var result = false;
            bool isNoteUpdates = true;
            bool isActionSet = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null)
                {
                    var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                    if (patientVisitNote != null)
                    {
                        var oldStatus = scheduleEvent.Status;
                        var oldReason = scheduleEvent.ReturnReason;
                        var oldPrintQueue = scheduleEvent.InPrintQueue;
                        if (button == ButtonAction.Approve.ToString())
                        {
                            patientVisitNote.Status = ((int)ScheduleStatus.NoteCompleted);
                            if (DisciplineTaskFactory.PhysicianEvalNotes().Exists(d => (int)d == scheduleEvent.DisciplineTask)) patientVisitNote.Status = (int)ScheduleStatus.EvalToBeSentToPhysician;
                            scheduleEvent.InPrintQueue = true;
                            scheduleEvent.Status = patientVisitNote.Status;
                            scheduleEvent.ReturnReason = string.Empty;
                            isActionSet = true;
                        }
                        else if (button ==ButtonAction.Return.ToString())
                        {
                            patientVisitNote.Status = ((int)ScheduleStatus.NoteReturned);
                            scheduleEvent.Status = patientVisitNote.Status;
                            scheduleEvent.ReturnReason = reason;
                            isActionSet = true;
                        }
                        else if (button ==ButtonAction.Print.ToString())
                        {
                            scheduleEvent.InPrintQueue = false;
                            isNoteUpdates = false;
                            isActionSet = true;
                        }
                        if (isActionSet)
                        {
                            if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                            {
                                if (isNoteUpdates)
                                {
                                    patientVisitNote.Modified = DateTime.Now;
                                    if (patientRepository.UpdateVisitNote(patientVisitNote))
                                    {
                                        if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                        {
                                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, string.Empty);
                                        }
                                        result = true;
                                    }
                                    else
                                    {
                                        scheduleEvent.Status = oldStatus;
                                        scheduleEvent.ReturnReason = oldReason;
                                        scheduleEvent.InPrintQueue = oldPrintQueue;
                                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                        result = false;
                                    }
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateReturnReason(Guid episodeId, Guid patientId, Guid eventId, string reason)
        {
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null)
                {
                    scheduleEvent.ReturnReason = reason;
                    if (patientRepository.UpdateScheduleEventNew(scheduleEvent)) return true;
                }
            }
            return false;
        }

        public bool DeleteWoundCareAssetNew(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            var result = false;
            var patientVisitNote = new PatientVisitNote();
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && !assetId.IsEmpty() && name.IsNotNullOrEmpty())
            {
                patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                if (patientVisitNote != null && patientVisitNote.WoundNote.IsNotNullOrEmpty())
                {
                    var oldWoundCare = patientVisitNote.WoundNote;
                    patientVisitNote.Questions = patientVisitNote.WoundNote.ToObject<List<NotesQuestion>>();
                    if (patientVisitNote.Questions.Exists(q => q.Name == name))
                    {
                        patientVisitNote.Questions.SingleOrDefault(q => q.Name == name).Answer = Guid.Empty.ToString();
                        patientVisitNote.WoundNote = patientVisitNote.Questions.ToXml();
                        if (patientRepository.UpdateVisitNote(patientVisitNote))
                        {
                            if (assetRepository.Delete(assetId))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,patientVisitNote.EpisodeId, patientVisitNote.PatientId, patientVisitNote.Id, Actions.Edit, DisciplineTasks.SkilledNurseVisit, "Wound Care Asset is deleted.");
                                result = true;
                            }
                            else
                            {
                                patientVisitNote.WoundNote = oldWoundCare;
                                patientRepository.UpdateVisitNote(patientVisitNote);
                                result = false;

                            }
                        }
                    }
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool DeleteScheduleEventAssetNew(Guid episodeId, Guid patientId, Guid eventId, Guid assetId)
        {
            var result = false;
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty() && !assetId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null && scheduleEvent.Asset.IsNotNullOrEmpty())
                {
                    var oldAsset = scheduleEvent.Asset;
                    scheduleEvent.Assets = scheduleEvent.Asset.ToObject<List<Guid>>();
                    if (scheduleEvent.Assets != null && scheduleEvent.Assets.Count > 0)
                    {
                        scheduleEvent.Assets.Remove(assetId);
                        scheduleEvent.Asset = scheduleEvent.Assets.ToXml();
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            if (assetRepository.Delete(assetId))
                            {
                                if (Enum.IsDefined(typeof(DisciplineTasks), scheduleEvent.DisciplineTask))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.EditDetail, (DisciplineTasks)scheduleEvent.DisciplineTask, "Uploaded asset is deleted.");
                                }
                                result = true;
                            }
                            else
                            {
                                scheduleEvent.Asset = oldAsset;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                result = false;
                            }

                        }
                    }
                }
            }
            else
            {
                result = false;
            }
            return result;
        }

        public bool ReopenNew(Guid episodeId, Guid patientId, Guid eventId)
        {
            var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
            if (scheduleEvent != null && Enum.IsDefined(typeof(DisciplineTasks),scheduleEvent.DisciplineTask))
            {
                var oldUserId = scheduleEvent.UserId;
                var oldStatus = scheduleEvent.Status;
                var description = string.Empty;
                if (oldUserId != Current.UserId)
                {
                    description = "( From " + UserEngine.GetName(oldUserId, Current.AgencyId) + " To " + UserEngine.GetName(Current.UserId, Current.AgencyId) + " )";
                }
                switch ((DisciplineTasks)scheduleEvent.DisciplineTask)
                {
                    case DisciplineTasks.NonOASISDischarge:
                    case DisciplineTasks.NonOASISStartofCare:
                    case DisciplineTasks.NonOASISRecertification:
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathOT:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.OASISCTransferOT:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNAssessmentRecert:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = (int)ScheduleStatus.OasisReopened;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var assessment = assessmentRepository.Get(scheduleEvent.EventId, scheduleEvent.PatientId, scheduleEvent.EpisodeId,  Current.AgencyId);
                            if (assessment != null)
                            {
                                if (assessment.Status == (int)ScheduleStatus.OasisExported)
                                {
                                    assessment.VersionNumber += 1;
                                }
                                assessment.SignatureText = string.Empty;
                                assessment.SignatureDate = DateTime.MinValue;
                                assessment.Status = (int)ScheduleStatus.OasisReopened;

                                if (scheduleEvent.EventDate.IsValid())
                                {
                                    assessment.AssessmentDate = scheduleEvent.EventDate;
                                }

                                if (assessmentRepository.Update(assessment))
                                {
                                    if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                    return true;
                                }
                                else
                                {
                                    scheduleEvent.UserId = oldUserId;
                                    scheduleEvent.Status = oldStatus;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    return false;
                                }
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return false;
                            }
                        }
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:

                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVObservationAndAssessment:

                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.HHAideCarePlan:
                    case DisciplineTasks.HHAideSupervisoryVisit:

                    case DisciplineTasks.PASVisit:
                    case DisciplineTasks.PASCarePlan:

                    case DisciplineTasks.PTAVisit:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.PTVisit:

                    case DisciplineTasks.STDischarge:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STVisit:

                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTVisit:

                    case DisciplineTasks.SixtyDaySummary:
                    case DisciplineTasks.TransferSummary:
                    case DisciplineTasks.CoordinationOfCare:
                    case DisciplineTasks.DischargeSummary:

                    case DisciplineTasks.SNDiabeticDailyVisit:

                    case DisciplineTasks.LVNSupervisoryVisit:

                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.MSWProgressNote:
                    case DisciplineTasks.MSWVisit:

                    case DisciplineTasks.UAPWoundCareVisit:
                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = (int)ScheduleStatus.NoteReopened;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                            if (patientVisitNote != null)
                            {
                                patientVisitNote.UserId = scheduleEvent.UserId;
                                patientVisitNote.Status = (int)ScheduleStatus.NoteReopened;
                                patientVisitNote.SignatureText = string.Empty;
                                patientVisitNote.SignatureDate = DateTime.MinValue;
                                patientVisitNote.PhysicianSignatureText = string.Empty;
                                patientVisitNote.ReceivedDate = DateTime.MinValue;
                                if (patientRepository.UpdateVisitNote(patientVisitNote))
                                {
                                    if (Enum.IsDefined(typeof(ScheduleStatus),scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                    return true;
                                }
                                else
                                {
                                    scheduleEvent.UserId = oldUserId;
                                    scheduleEvent.Status = oldStatus;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    return false;
                                }
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return false;
                            }
                        }
                        break;

                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReopened;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var planOfCare = planofCareRepository.Get(Current.AgencyId, episodeId, patientId, eventId);
                            if (planOfCare != null)
                            {
                                planOfCare.UserId = scheduleEvent.UserId;
                                planOfCare.Status = (int)ScheduleStatus.OrderReopened;
                                planOfCare.SignatureText = string.Empty;
                                planOfCare.SignatureDate = DateTime.MinValue;
                                planOfCare.PhysicianSignatureText = string.Empty;
                                planOfCare.PhysicianSignatureDate = DateTime.MinValue;
                                planOfCare.ReceivedDate = DateTime.MinValue;
                                if (planofCareRepository.Update(planOfCare))
                                {
                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                    return true;
                                }
                                else
                                {
                                    scheduleEvent.UserId = oldUserId;
                                    scheduleEvent.Status = oldStatus;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    return false;
                                }
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return false;
                            }
                            
                        }
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReopened;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var planOfCareStandAlone = planofCareRepository.GetStandAlone(Current.AgencyId, episodeId, patientId, eventId);
                            if (planOfCareStandAlone != null)
                            {
                                planOfCareStandAlone.UserId = scheduleEvent.UserId;
                                planOfCareStandAlone.Status = (int)ScheduleStatus.OrderReopened;
                                planOfCareStandAlone.SignatureText = string.Empty;
                                planOfCareStandAlone.SignatureDate = DateTime.MinValue;
                                planOfCareStandAlone.PhysicianSignatureText = string.Empty;
                                planOfCareStandAlone.PhysicianSignatureDate = DateTime.MinValue;
                                planOfCareStandAlone.ReceivedDate = DateTime.MinValue;
                                if (planofCareRepository.UpdateStandAlone(planOfCareStandAlone))
                                {
                                    if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                    return true;
                                }
                                else
                                {
                                    scheduleEvent.UserId = oldUserId;
                                    scheduleEvent.Status = oldStatus;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    return false;
                                }
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return false;
                            }
                        }
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReopened;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var physicianOrder = patientRepository.GetOrderOnly(eventId, patientId, Current.AgencyId);
                            if (physicianOrder != null)
                            {
                                physicianOrder.UserId = scheduleEvent.UserId;
                                physicianOrder.Status = (int)ScheduleStatus.OrderReopened;
                                physicianOrder.SignatureText = string.Empty;
                                physicianOrder.SignatureDate = DateTime.MinValue;
                                physicianOrder.PhysicianSignatureText = string.Empty;
                                physicianOrder.PhysicianSignatureDate = DateTime.MinValue;
                                physicianOrder.ReceivedDate = DateTime.MinValue;
                                if (patientRepository.UpdateOrderModel(physicianOrder))
                                {
                                    if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                    return true;
                                }
                                else
                                {
                                    scheduleEvent.UserId = oldUserId;
                                    scheduleEvent.Status = oldStatus;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    return false;
                                }
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return false;
                            }
                        }
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = (int)ScheduleStatus.OrderReopened;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var faceToFaceOrder = patientRepository.GetFaceToFaceEncounter(eventId, patientId, Current.AgencyId);
                            if (faceToFaceOrder != null)
                            {
                                faceToFaceOrder.UserId = scheduleEvent.UserId;
                                faceToFaceOrder.Status = (int)ScheduleStatus.OrderReopened;
                                faceToFaceOrder.SignatureText = string.Empty;
                                faceToFaceOrder.SignatureDate = DateTime.MinValue;
                                faceToFaceOrder.SignatureText = string.Empty;
                                faceToFaceOrder.ReceivedDate = DateTime.MinValue;
                                if (patientRepository.UpdateFaceToFaceEncounter(faceToFaceOrder))
                                {
                                    if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                    return true;
                                }
                                else
                                {
                                    scheduleEvent.UserId = oldUserId;
                                    scheduleEvent.Status = oldStatus;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    return false;
                                }
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return false;
                            }
                        }
                        break;
                    case DisciplineTasks.CommunicationNote:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = (int)ScheduleStatus.NoteReopened;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var communicationNote = patientRepository.GetCommunicationNote(eventId, patientId, Current.AgencyId);
                            if (communicationNote != null)
                            {
                                communicationNote.UserId = scheduleEvent.UserId;
                                communicationNote.Status = (int)ScheduleStatus.NoteReopened;
                                communicationNote.SignatureText = string.Empty;
                                communicationNote.SignatureDate = DateTime.MinValue;
                                communicationNote.Modified = DateTime.Now;
                                if (patientRepository.UpdateCommunicationNoteModal(communicationNote))
                                {
                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                    return true;
                                }
                                else
                                {
                                    scheduleEvent.UserId = oldUserId;
                                    scheduleEvent.Status = oldStatus;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    return false;
                                }
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return false;
                            }
                        }

                        break;

                    case DisciplineTasks.IncidentAccidentReport:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var incident = agencyRepository.GetIncidentReport(Current.AgencyId, eventId);
                            if (incident != null)
                            {
                                incident.UserId = scheduleEvent.UserId;
                                incident.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                                incident.SignatureText = string.Empty;
                                incident.SignatureDate = DateTime.MinValue;
                                incident.Modified = DateTime.Now;
                                if (agencyRepository.UpdateIncidentModal(incident))
                                {
                                    if (Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                    return true;
                                }
                                else
                                {
                                    scheduleEvent.UserId = oldUserId;
                                    scheduleEvent.Status = oldStatus;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    return false;
                                }
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return false;
                            }
                        }

                        break;

                    case DisciplineTasks.InfectionReport:
                        scheduleEvent.UserId = Current.UserId;
                        scheduleEvent.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            var infection = agencyRepository.GetInfectionReport(Current.AgencyId, eventId);
                            if (infection != null)
                            {
                                infection.UserId = scheduleEvent.UserId;
                                infection.Status = (int)ScheduleStatus.ReportAndNotesReopen;
                                infection.SignatureText = string.Empty;
                                infection.SignatureDate = DateTime.MinValue;
                                infection.Modified = DateTime.Now;
                                if (agencyRepository.UpdateInfectionModal(infection))
                                {
                                    if ( Enum.IsDefined(typeof(ScheduleStatus), scheduleEvent.Status))
                                    {
                                        Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.StatusChange, (ScheduleStatus)scheduleEvent.Status, (DisciplineTasks)scheduleEvent.DisciplineTask, description);
                                    }
                                    return true;
                                }
                                else
                                {
                                    scheduleEvent.UserId = oldUserId;
                                    scheduleEvent.Status = oldStatus;
                                    patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                    return false;
                                }
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                scheduleEvent.Status = oldStatus;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                                return false;
                            }
                        }

                        break;
                }
            }
            return false;
        }

        public bool ReassignNew(Guid episodeId, Guid patientId, Guid eventId, Guid oldEmployeeId, Guid employeeId)
        {
            bool result = false;
            try
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null)
                {
                    var oldUserId = scheduleEvent.UserId;
                    scheduleEvent.UserId = employeeId;
                    if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                    {
                        if (ReassignScheduleEntity(episodeId, patientId, eventId, employeeId, scheduleEvent.DisciplineTask))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,scheduleEvent.EpisodeId, patientId, scheduleEvent.EventId, Actions.Reassigned, (DisciplineTasks)scheduleEvent.DisciplineTask, "( From " + UserEngine.GetName(oldEmployeeId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                            result = true;
                        }
                        else
                        {
                            scheduleEvent.UserId = oldUserId;
                            patientRepository.UpdateScheduleEventNew(scheduleEvent);
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        public bool ReassignSchedules(Guid employeeOldId, Guid employeeId)
        {
            bool result = false;

            var scheduleEvents = patientRepository.GetScheduledEventsByEmployeeAssignedNew(Current.AgencyId, employeeOldId);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                var status = ScheduleStatusFatory.AllNotStarted();// new int[] { (int)ScheduleStatus.NoteNotStarted, (int)ScheduleStatus.NoteNotYetDue, (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OrderNotYetDue, (int)ScheduleStatus.OrderNotYetStarted, (int)ScheduleStatus.ReportAndNotesCreated };
                scheduleEvents.ForEach(scheduleEvent =>
                {
                    if (!scheduleEvent.IsMissedVisit && !scheduleEvent.IsDeprecated && status.Exists(s => s == scheduleEvent.Status))
                    {
                        var oldUserId = scheduleEvent.UserId;
                        scheduleEvent.UserId = employeeId;
                        if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                        {
                            if (ReassignScheduleEntity(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, employeeId, scheduleEvent.DisciplineTask))
                            {
                                Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.Reassigned, (DisciplineTasks)scheduleEvent.DisciplineTask, "( From " + UserEngine.GetName(employeeOldId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                            }
                            else
                            {
                                scheduleEvent.UserId = oldUserId;
                                patientRepository.UpdateScheduleEventNew(scheduleEvent);
                            }
                        }
                    }
                });
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public bool ReassignSchedules(Guid patientId, Guid employeeOldId, Guid employeeId)
        {
            bool result = false;
            if (!patientId.IsEmpty() && !employeeId.IsEmpty() && !employeeOldId.IsEmpty())
            {
                var scheduleEvents = patientRepository.GetScheduledEventsByEmployeeAssignedNew(Current.AgencyId, patientId, employeeOldId);
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    var status = ScheduleStatusFatory.AllNotStarted();//new int[] { (int)ScheduleStatus.NoteNotStarted, (int)ScheduleStatus.NoteNotYetDue, (int)ScheduleStatus.OasisNotStarted, (int)ScheduleStatus.OasisNotYetDue, (int)ScheduleStatus.OrderNotYetDue, (int)ScheduleStatus.OrderNotYetStarted, (int)ScheduleStatus.ReportAndNotesCreated };
                    scheduleEvents.ForEach(evnt =>
                    {
                        if (!evnt.IsMissedVisit && !evnt.IsDeprecated && status.Contains(evnt.Status))
                        {
                            var oldUserId = evnt.UserId;
                            evnt.UserId = employeeId;
                            if (patientRepository.UpdateScheduleEventNew(evnt))
                            {
                                if (ReassignScheduleEntity(evnt.EpisodeId, patientId, evnt.EventId, employeeId, evnt.DisciplineTask))
                                {
                                    Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,evnt.EpisodeId, patientId, evnt.EventId, Actions.Reassigned, (DisciplineTasks)evnt.DisciplineTask, "( From " + UserEngine.GetName(employeeOldId, Current.AgencyId) + " To " + UserEngine.GetName(employeeId, Current.AgencyId) + " )");
                                }
                                else
                                {
                                    evnt.UserId = oldUserId;
                                    patientRepository.UpdateScheduleEventNew(evnt);
                                }
                            }
                        }
                    });
                    result = true;
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        public bool ToggleScheduleStatusNew(Guid episodeId, Guid patientId, Guid eventId, bool isDelete)
        {
            bool result = false;
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
                if (scheduleEvent != null)
                {
                    var oldStatus = scheduleEvent.IsDeprecated;
                    scheduleEvent.IsDeprecated = isDelete;
                    if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                    {
                        if (UpdateScheduleEntity(eventId, episodeId, patientId, scheduleEvent.DisciplineTask, isDelete))
                        {
                            Auditor.Log(Current.AgencyId, Current.UserId,Current.UserFullName ,episodeId, patientId, eventId, isDelete ? Actions.Deleted : Actions.Restored, (DisciplineTasks)scheduleEvent.DisciplineTask);
                            result = true;
                        }
                        else
                        {
                            scheduleEvent.IsDeprecated = oldStatus;
                            patientRepository.UpdateScheduleEventNew(scheduleEvent);
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public bool UpdateEpisodeNew(PatientEpisode episode, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (episode != null)
            {
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    var patient = patientRepository.Get(episode.PatientId, Current.AgencyId);
                    if (!episode.AdmissionId.IsEmpty())
                    {
                        var admissinData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, episode.AdmissionId);
                        if (admissinData != null && admissinData.PatientData.IsNotNullOrEmpty())
                        {
                            patient = admissinData.PatientData.ToObject<Patient>();
                        }
                    }
                    if (patient != null)
                    {
                        episode.StartOfCareDate = patient.StartofCareDate;
                        scheduleEvents.ForEach(ev =>
                        {
                            if (Enum.IsDefined(typeof(DisciplineTasks), ev.DisciplineTask))
                            {
                                ProcessScheduleFactory(ev);
                                if (patientRepository.AddScheduleEvent(ev))
                                {
                                    if (SaveScheduleModalFactory(ev, patient, episode))
                                    {
                                        result = true;
                                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName,ev.EpisodeId, ev.PatientId, ev.EventId, Actions.Add, (DisciplineTasks)ev.DisciplineTask);
                                    }
                                    else
                                    {
                                        patientRepository.RemoveScheduleEventNew(ev);
                                    }
                                }
                            }
                        });
                    }
                }
            }
            return result;
        }

        public bool AddMultiDateRangeScheduleNew(PatientEpisode episode, int disciplineTask, string discipline, Guid userId, bool isBillable, DateTime startDate, DateTime endDate)
        {
            bool result = false;
            if (episode != null)
            {
                var newEvents = new List<ScheduleEvent>();
                int dateDifference = endDate.Subtract(startDate).Days + 1;
                for (int i = 0; i < dateDifference; i++)
                {
                    newEvents.Add(
                        new ScheduleEvent
                        {
                            EventId = Guid.NewGuid(),
                            PatientId = episode.PatientId,
                            EpisodeId = episode.Id,
                            AgencyId = episode.AgencyId,
                            UserId = userId,
                            DisciplineTask = disciplineTask,
                            Discipline = discipline,
                            EventDate = startDate.AddDays(i),
                            VisitDate = startDate.AddDays(i),
                            StartDate = episode.StartDate,
                            EndDate = episode.EndDate,
                            IsBillable = isBillable,
                            IsDeprecated = false
                        });
                }
                if (newEvents != null && newEvents.Count > 0)
                {
                    result = this.UpdateEpisodeNew(episode, newEvents);
                }
            }
            return result;
        }

        public bool AddMultiDayScheduleNew(PatientEpisode episode, Guid userId, int disciplineTaskId, string visitDates)
        {
            bool result = false;
            if (episode != null)
            {
                var scheduledEvents = new List<ScheduleEvent>();
                var visitDateArray = visitDates.Split(',');
                if (visitDateArray != null && visitDateArray.Length > 0)
                {
                    visitDateArray.ForEach(date =>
                    {
                        if (date.IsDate())
                        {
                            var newScheduledEvent = new ScheduleEvent
                            {
                                EventId = Guid.NewGuid(),
                                PatientId = episode.PatientId,
                                EpisodeId = episode.Id,
                                AgencyId = episode.AgencyId,
                                UserId = userId,
                                DisciplineTask = disciplineTaskId,
                                EventDate = date.ToDateTime(),
                                VisitDate = date.ToDateTime(),
                                StartDate = episode.StartDate,
                                EndDate = episode.EndDate,
                                IsDeprecated = false
                            };
                            scheduledEvents.Add(newScheduledEvent);
                        }
                    });
                }
                if (scheduledEvents != null && scheduledEvents.Count > 0)
                {
                    result = this.UpdateEpisodeNew(episode, scheduledEvents);
                }
            }
            return result;
        }

        public bool AddSchedules(PatientEpisode episode, List<ScheduleEvent> scheduleEvents)
        {
            bool result = false;
            if (episode != null)
            {
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    scheduleEvents.ForEach(ev =>
                    {
                        var date = ev.EventDate;
                        ev.EventId = Guid.NewGuid();
                        ev.PatientId = episode.PatientId;
                        ev.EpisodeId = episode.Id;
                        ev.AgencyId = episode.AgencyId;
                        ev.EventDate = date;
                        ev.VisitDate = date;
                        ev.StartDate = episode.StartDate;
                        ev.EndDate = episode.EndDate;
                        ev.IsDeprecated = false;
                    });
                    result = this.UpdateEpisodeNew(episode, scheduleEvents);
                }
            }
            return result;
        }

        public List<ScheduleEvent> GetScheduledEventsNew(Guid episodeId, Guid patientId, string discipline)
        {
            var patientEvents = patientRepository.GetPatientScheduledEventsNew(Current.AgencyId, episodeId, patientId, discipline, false);
            if (patientEvents != null && patientEvents.Count > 0)
            {
                patientEvents.ForEach(s =>
                {
                    if (s.IsMissedVisit)
                    {
                        var missedVisit = patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
                        if (missedVisit != null)
                        {
                            s.MissedVisitComments = missedVisit.ToString();
                        }
                    }
                    Common.Url.Set(s, true, true);
                });
            }
            return patientEvents;
        }

        public List<ScheduleEvent> GetScheduledEventsNew(Guid patientId, string discipline, DateRange dateRange)
        {
            var patientEvents = patientRepository.GetPatientScheduledEventsDateRangeOrNotAndDiscipline(Current.AgencyId, patientId, dateRange.StartDate, dateRange.EndDate, discipline, false, true);
            if (patientEvents != null && patientEvents.Count > 0)
            {
                patientEvents.ForEach(s =>
                {
                    if (s.IsMissedVisit)
                    {
                        var missedVisit = patientRepository.GetMissedVisit(Current.AgencyId, s.EventId);
                        if (missedVisit != null) s.MissedVisitComments = missedVisit.ToString();
                    }
                    Common.Url.Set(s, true, true);
                });
            }
            return patientEvents;
        }
      
        #endregion 

        #region Private Methods

        private OrderType GetOrderType(string disciplineTask)
        {
            var orderType = OrderType.PtEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        orderType = OrderType.PtReEvaluation;
                        break;
                    case "OTEvaluation":
                        orderType = OrderType.OtEvaluation;
                        break;
                    case "OTReEvaluation":
                        orderType = OrderType.OtReEvaluation;
                        break;
                    case "STEvaluation":
                        orderType = OrderType.StEvaluation;
                        break;
                    case "STReEvaluation":
                        orderType = OrderType.StReEvaluation;
                        break;
                }
            }
            return orderType;
        }

        private DisciplineTasks GetDisciplineType(string disciplineTask)
        {
            var task = DisciplineTasks.PTEvaluation;
            if (disciplineTask.IsNotNullOrEmpty())
            {
                switch (disciplineTask)
                {
                    case "PTReEvaluation":
                        task = DisciplineTasks.PTReEvaluation;
                        break;
                    case "OTEvaluation":
                        task = DisciplineTasks.OTEvaluation;
                        break;
                    case "OTReEvaluation":
                        task = DisciplineTasks.OTReEvaluation;
                        break;
                    case "STEvaluation":
                        task = DisciplineTasks.STEvaluation;
                        break;
                    case "STReEvaluation":
                        task = DisciplineTasks.STReEvaluation;
                        break;
                }
            }
            return task;
        }

        private void SetInsurance(Patient patient)
        {
            if (patient.PrimaryInsurance > 0)
            {
                if (patient.PrimaryInsurance < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.PrimaryInsurance);
                    if (standardInsurance != null)
                    {
                        patient.PrimaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.PrimaryInsurance);
                    if (standardInsurance != null)
                    {
                        patient.PrimaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
            if (patient.SecondaryInsurance > 0)
            {
                if (patient.SecondaryInsurance < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.SecondaryInsurance);
                    if (standardInsurance != null)
                    {
                        patient.SecondaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.SecondaryInsurance);
                    if (standardInsurance != null)
                    {
                        patient.SecondaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
            if (patient.TertiaryInsurance > 0)
            {
                if (patient.TertiaryInsurance < 1000)
                {
                    var standardInsurance = lookupRepository.GetInsurance(patient.TertiaryInsurance);
                    if (standardInsurance != null)
                    {
                        patient.TertiaryInsuranceName = standardInsurance.Name;
                    }
                }
                else
                {
                    var standardInsurance = agencyRepository.FindInsurance(Current.AgencyId, patient.TertiaryInsurance);
                    if (standardInsurance != null)
                    {
                        patient.TertiaryInsuranceName = standardInsurance.Name;
                    }
                }
            }
        }

        private bool UpdateScheduleEntity(Guid eventId, Guid episodeId, Guid patientId, int taskName, bool isDeprecated)
        {
            bool result = false;
            switch (taskName)
            {
                case (int)DisciplineTasks.OASISCDeath:
                case (int)DisciplineTasks.OASISCDeathOT:
                case (int)DisciplineTasks.OASISCDeathPT:
                case (int)DisciplineTasks.OASISCDischarge:
                case (int)DisciplineTasks.OASISCDischargeOT:
                case (int)DisciplineTasks.OASISCDischargePT:
                case (int)DisciplineTasks.NonOASISDischarge:
                case (int)DisciplineTasks.OASISCFollowUp:
                case (int)DisciplineTasks.OASISCFollowupPT:
                case (int)DisciplineTasks.OASISCFollowupOT:
                case (int)DisciplineTasks.OASISCRecertification:
                case (int)DisciplineTasks.OASISCRecertificationPT:
                case (int)DisciplineTasks.OASISCRecertificationOT:
                case (int)DisciplineTasks.NonOASISRecertification:
                case (int)DisciplineTasks.OASISCResumptionofCare:
                case (int)DisciplineTasks.OASISCResumptionofCarePT:
                case (int)DisciplineTasks.OASISCResumptionofCareOT:
                case (int)DisciplineTasks.OASISCStartofCare:
                case (int)DisciplineTasks.OASISCStartofCarePT:
                case (int)DisciplineTasks.OASISCStartofCareOT:
                case (int)DisciplineTasks.NonOASISStartofCare:
                case (int)DisciplineTasks.OASISCTransfer:
                case (int)DisciplineTasks.OASISCTransferPT:
                case (int)DisciplineTasks.OASISCTransferOT:
                case (int)DisciplineTasks.OASISCTransferDischarge:
                case (int)DisciplineTasks.SNAssessment:
                case (int)DisciplineTasks.SNAssessmentRecert:
                    result = assessmentService.MarkAsDeleted(eventId, episodeId, patientId,isDeprecated);
                    break;
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SNVTeachingTraining:
                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                case (int)DisciplineTasks.SNVObservationAndAssessment:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVPsychNurse:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.LVNSupervisoryVisit:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.STVisit:
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                case (int)DisciplineTasks.STDischarge:
                case (int)DisciplineTasks.MSWEvaluationAssessment:
                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.DieticianVisit:
                case (int)DisciplineTasks.PTAVisit:
                case (int)DisciplineTasks.PTReEvaluation:
                case (int)DisciplineTasks.COTAVisit:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWProgressNote:
                case (int)DisciplineTasks.HHAideVisit:
                case (int)DisciplineTasks.HHAideCarePlan:
                case (int)DisciplineTasks.PASVisit:
                case (int)DisciplineTasks.PASCarePlan:
                case (int)DisciplineTasks.DischargeSummary:
                case (int)DisciplineTasks.SixtyDaySummary:
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                case (int)DisciplineTasks.PTMaintenance:
                case (int)DisciplineTasks.OTMaintenance:
                case (int)DisciplineTasks.STMaintenance:
                case (int)DisciplineTasks.DriverOrTransportationNote:
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                case (int)DisciplineTasks.UAPWoundCareVisit:
                case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                    result = patientRepository.MarkVisitNoteAsDeleted(Current.AgencyId, episodeId, patientId, eventId, isDeprecated);
                    break;
                case (int)DisciplineTasks.PhysicianOrder:
                    result = patientRepository.MarkOrderAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case (int)DisciplineTasks.HCFA485:
                case (int)DisciplineTasks.NonOasisHCFA485:
                    result = assessmentService.MarkPlanOfCareAsDeleted(eventId, episodeId, patientId, isDeprecated);
                    break;
                case (int)DisciplineTasks.HCFA485StandAlone:
                    result = assessmentService.MarkPlanOfCareStandAloneAsDeleted(eventId, episodeId, patientId, isDeprecated);
                    break;
                case (int)DisciplineTasks.IncidentAccidentReport:
                    result = agencyRepository.MarkIncidentAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case (int)DisciplineTasks.InfectionReport:
                    result = agencyRepository.MarkInfectionsAsDeleted(eventId, patientId, Current.AgencyId, isDeprecated);
                    break;
                case (int)DisciplineTasks.CommunicationNote:
                    result = patientRepository.DeleteCommunicationNote(Current.AgencyId, eventId, patientId, isDeprecated);
                    break;
                case (int)DisciplineTasks.FaceToFaceEncounter:
                    result = patientRepository.DeleteFaceToFaceEncounter(Current.AgencyId, patientId, eventId, isDeprecated);
                    break;
            }
            return result;
        }

        private bool ReassignScheduleEntity(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId, int taskName)
        {
            bool result = false;
            switch (taskName)
            {
                case (int)DisciplineTasks.OASISCDeath:
                case (int)DisciplineTasks.OASISCDeathOT:
                case (int)DisciplineTasks.OASISCDeathPT:
                case (int)DisciplineTasks.OASISCDischarge:
                case (int)DisciplineTasks.OASISCDischargeOT:
                case (int)DisciplineTasks.OASISCDischargePT:
                case (int)DisciplineTasks.NonOASISDischarge:
                case (int)DisciplineTasks.OASISCFollowUp:
                case (int)DisciplineTasks.OASISCFollowupPT:
                case (int)DisciplineTasks.OASISCFollowupOT:
                case (int)DisciplineTasks.OASISCRecertification:
                case (int)DisciplineTasks.OASISCRecertificationPT:
                case (int)DisciplineTasks.OASISCRecertificationOT:
                case (int)DisciplineTasks.NonOASISRecertification:
                case (int)DisciplineTasks.OASISCResumptionofCare:
                case (int)DisciplineTasks.OASISCResumptionofCarePT:
                case (int)DisciplineTasks.OASISCResumptionofCareOT:
                case (int)DisciplineTasks.OASISCStartofCare:
                case (int)DisciplineTasks.OASISCStartofCarePT:
                case (int)DisciplineTasks.OASISCStartofCareOT:
                case (int)DisciplineTasks.NonOASISStartofCare:
                case (int)DisciplineTasks.OASISCTransfer:
                case (int)DisciplineTasks.OASISCTransferPT:
                case (int)DisciplineTasks.OASISCTransferOT:
                case (int)DisciplineTasks.OASISCTransferDischarge:
                case (int)DisciplineTasks.SNAssessment:
                case (int)DisciplineTasks.SNAssessmentRecert:
                    result = assessmentService.ReassignUser(episodeId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVPsychNurse:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                case (int)DisciplineTasks.SNVDCPlanning:

                case (int)DisciplineTasks.SNVTeachingTraining:
                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                case (int)DisciplineTasks.SNVObservationAndAssessment:

                case (int)DisciplineTasks.LVNSupervisoryVisit:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTDischarge:
                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.STVisit:
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                case (int)DisciplineTasks.STDischarge:
                case (int)DisciplineTasks.MSWEvaluationAssessment:
                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                case (int)DisciplineTasks.MSWVisit:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.DieticianVisit:
                case (int)DisciplineTasks.PTAVisit:
                case (int)DisciplineTasks.PTReEvaluation:
                case (int)DisciplineTasks.COTAVisit:
                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWProgressNote:
                case (int)DisciplineTasks.HHAideVisit:
                case (int)DisciplineTasks.HHAideCarePlan:
                case (int)DisciplineTasks.DischargeSummary:
                case (int)DisciplineTasks.SixtyDaySummary:
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                case (int)DisciplineTasks.PTMaintenance:
                case (int)DisciplineTasks.OTMaintenance:
                case (int)DisciplineTasks.STMaintenance:
                case (int)DisciplineTasks.DriverOrTransportationNote:
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                case (int)DisciplineTasks.PASVisit:
                case (int)DisciplineTasks.PASCarePlan:
                case (int)DisciplineTasks.UAPWoundCareVisit:
                case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                    result = patientRepository.ReassignNotesUser(Current.AgencyId, episodeId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.PhysicianOrder:
                    result = patientRepository.ReassignOrdersUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.HCFA485:
                case (int)DisciplineTasks.NonOasisHCFA485:
                    result = assessmentService.ReassignPlanOfCaresUser(Current.AgencyId, episodeId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.IncidentAccidentReport:
                    result = agencyRepository.ReassignIncidentUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.InfectionReport:
                    result = agencyRepository.ReassignInfectionsUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.CommunicationNote:
                    result = patientRepository.ReassignCommunicationNoteUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
                case (int)DisciplineTasks.FaceToFaceEncounter:
                    result = patientRepository.ReassignFaceToFaceEncounterUser(Current.AgencyId, patientId, eventId, employeeId);
                    break;
            }
            return result;
        }

        private bool ProcessEditDetail(ScheduleEvent schedule)
        {
            bool result = false;
            if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
            {
                var task = (DisciplineTasks)schedule.DisciplineTask;
                var type = task.ToString();
                switch (task)
                {
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathOT:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.NonOASISDischarge:
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                    case DisciplineTasks.NonOASISRecertification:
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                    case DisciplineTasks.NonOASISStartofCare:
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNAssessmentRecert:
                        if (DisciplineTaskFactory.AllRecertDisciplineTasks().Contains(schedule.DisciplineTask))
                        {
                            if (ScheduleStatusFatory.OASISCompleted().Contains(schedule.Status))
                            {
                                patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, true);
                            }
                            else
                            {
                                patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, false);
                            }
                        }
                        result = assessmentService.UpdateAssessmentForDetail(schedule);
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var notesQuestions = visitNote.ToDictionary();
                                if (notesQuestions != null)
                                {
                                    if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToShortDateString(); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToShortDateString(), Name = "VisitDate", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                    visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                    visitNote.IsDeprecated = schedule.IsDeprecated;
                                    visitNote.NoteType = type;
                                    visitNote.Status = schedule.Status > 0 ? schedule.Status : visitNote.Status;
                                    result = patientRepository.UpdateVisitNote(visitNote);
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;

                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTDischarge:
                    //case DisciplineTasks.PTSupervisoryVisit:
                    //case DisciplineTasks.PTReassessment:
                    case DisciplineTasks.PTReEvaluation:
                    //case DisciplineTasks.PTDischargeSummary:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    //case DisciplineTasks.OTReassessment:
                    //case DisciplineTasks.OTDischargeSummary:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STDischarge:
                    case DisciplineTasks.PTAVisit:
                    case DisciplineTasks.COTAVisit:
                    case DisciplineTasks.PTMaintenance:
                    case DisciplineTasks.OTMaintenance:
                    case DisciplineTasks.STMaintenance:
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var notesQuestions = visitNote.ToDictionary();
                                if (notesQuestions != null)
                                {
                                    if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToShortDateString(); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToShortDateString(), Name = "VisitDate", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                    notesQuestions.Values.ForEach(q => { q.Type = type; });
                                    visitNote.NoteType = type;
                                    visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                    visitNote.IsDeprecated = schedule.IsDeprecated;
                                    visitNote.Status = schedule.Status > 0 ? schedule.Status : visitNote.Status;
                                    result = patientRepository.UpdateVisitNote(visitNote);
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;


                    case DisciplineTasks.LVNSupervisoryVisit:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.HHAideSupervisoryVisit:
                    case DisciplineTasks.MSWVisit:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.DieticianVisit:
                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.MSWProgressNote:
                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.DriverOrTransportationNote:
                    case DisciplineTasks.PASVisit:
                    case DisciplineTasks.PASCarePlan:
                    case DisciplineTasks.UAPWoundCareVisit:
                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var notesQuestions = visitNote.ToDictionary();
                                if (notesQuestions != null)
                                {
                                    if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToShortDateString(); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToShortDateString(), Name = "VisitDate", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                    visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                    visitNote.IsDeprecated = schedule.IsDeprecated;
                                    visitNote.Status = schedule.Status > 0 ? schedule.Status : visitNote.Status;
                                    result = patientRepository.UpdateVisitNote(visitNote);
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;

                    case DisciplineTasks.HHAideCarePlan:
                    case DisciplineTasks.DischargeSummary:
                    case DisciplineTasks.SixtyDaySummary:
                    case DisciplineTasks.TransferSummary:
                    case DisciplineTasks.CoordinationOfCare:
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var notesQuestions = visitNote.ToDictionary();
                                if (notesQuestions != null)
                                {
                                    if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToShortDateString(); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToShortDateString(), Name = "VisitDate", Type = type }); }
                                    visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                    visitNote.IsDeprecated = schedule.IsDeprecated;
                                    visitNote.Status = schedule.Status > 0 ? schedule.Status : visitNote.Status;
                                    result = patientRepository.UpdateVisitNote(visitNote);
                                }
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        {
                            var physicianOrder = patientRepository.GetOrderOnly(schedule.EventId, schedule.PatientId, Current.AgencyId);
                            if (physicianOrder != null)
                            {
                                physicianOrder.Status = schedule.Status > 0 ? schedule.Status : physicianOrder.Status;
                                if (schedule.VisitDate.IsValid())
                                {
                                    physicianOrder.OrderDate = schedule.VisitDate;
                                }
                                if (physicianOrder.Status != schedule.Status && physicianOrder.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                                {
                                    var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                                    if (physician != null)
                                    {
                                        physicianOrder.PhysicianData = physician.ToXml();
                                    }
                                }
                                physicianOrder.PhysicianId = schedule.PhysicianId;
                                physicianOrder.IsDeprecated = schedule.IsDeprecated;
                                result = patientRepository.UpdateOrderModel(physicianOrder);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        result = assessmentService.UpdatePlanOfCareForDetail(schedule);
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        result = assessmentService.UpdatePlanOfCareStandAloneForDetail(schedule);
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        var faceToFace = patientRepository.GetFaceToFaceEncounter(schedule.EventId, Current.AgencyId);
                        if (faceToFace != null)
                        {
                            faceToFace.Status = schedule.Status > 0 ? schedule.Status : faceToFace.Status;
                            if (faceToFace.Status != schedule.Status && faceToFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    faceToFace.PhysicianData = physician.ToXml();
                                }
                            }
                            faceToFace.PhysicianId = schedule.PhysicianId;
                            faceToFace.IsDeprecated = schedule.IsDeprecated;
                            faceToFace.RequestDate = schedule.VisitDate;
                            if (patientRepository.UpdateFaceToFaceEncounter(faceToFace))
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        {
                            var accidentReport = agencyRepository.GetIncidentReport(Current.AgencyId, schedule.EventId);
                            if (accidentReport != null)
                            {
                                if (schedule.VisitDate.IsValid())
                                {
                                    accidentReport.IncidentDate = schedule.VisitDate;
                                }
                                accidentReport.IsDeprecated = schedule.IsDeprecated;
                                accidentReport.Status = schedule.Status > 0 ? schedule.Status : accidentReport.Status;
                                result = agencyRepository.UpdateIncidentModal(accidentReport);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.InfectionReport:
                        {
                            var infectionReport = agencyRepository.GetInfectionReport(Current.AgencyId, schedule.EventId);
                            if (infectionReport != null)
                            {
                                if (schedule.VisitDate.IsValid())
                                {
                                    infectionReport.InfectionDate = schedule.VisitDate;
                                }
                                infectionReport.IsDeprecated = schedule.IsDeprecated;
                                infectionReport.Status = schedule.Status > 0 ? schedule.Status : infectionReport.Status;
                                result = agencyRepository.UpdateInfectionModal(infectionReport);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.CommunicationNote:
                        {
                            var comNote = patientRepository.GetCommunicationNote(schedule.EventId, schedule.PatientId, Current.AgencyId);
                            if (comNote != null)
                            {
                                comNote.IsDeprecated = schedule.IsDeprecated;
                                comNote.Status = schedule.Status > 0 ? schedule.Status : comNote.Status;
                                if (schedule.VisitDate.IsValid())
                                {
                                    comNote.Created = schedule.VisitDate;
                                }
                                if (comNote.Status != schedule.Status && comNote.Status == (int)ScheduleStatus.NoteCompleted && !comNote.PhysicianId.IsEmpty())
                                {
                                    var physician = physicianRepository.Get(comNote.PhysicianId, Current.AgencyId);
                                    if (physician != null)
                                    {
                                        comNote.PhysicianData = physician.ToXml();
                                    }
                                }
                                result = patientRepository.UpdateCommunicationNoteModal(comNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                }
            }
            return result;
        }

        private bool ProcessEditDetailForReassign(ScheduleEvent schedule)
        {
            bool result = false;
            if (Enum.IsDefined(typeof(DisciplineTasks), schedule.DisciplineTask))
            {
                var task = (DisciplineTasks)schedule.DisciplineTask;
                var type = task.ToString();
                switch (task)
                {
                    case DisciplineTasks.OASISCDeath:
                    case DisciplineTasks.OASISCDeathOT:
                    case DisciplineTasks.OASISCDeathPT:
                    case DisciplineTasks.OASISCDischarge:
                    case DisciplineTasks.OASISCDischargeOT:
                    case DisciplineTasks.OASISCDischargePT:
                    case DisciplineTasks.NonOASISDischarge:
                    case DisciplineTasks.OASISCFollowUp:
                    case DisciplineTasks.OASISCFollowupPT:
                    case DisciplineTasks.OASISCFollowupOT:
                    case DisciplineTasks.OASISCRecertification:
                    case DisciplineTasks.OASISCRecertificationPT:
                    case DisciplineTasks.OASISCRecertificationOT:
                    case DisciplineTasks.NonOASISRecertification:
                    case DisciplineTasks.OASISCResumptionofCare:
                    case DisciplineTasks.OASISCResumptionofCarePT:
                    case DisciplineTasks.OASISCResumptionofCareOT:
                    case DisciplineTasks.OASISCStartofCare:
                    case DisciplineTasks.OASISCStartofCarePT:
                    case DisciplineTasks.OASISCStartofCareOT:
                    case DisciplineTasks.NonOASISStartofCare:
                    case DisciplineTasks.OASISCTransfer:
                    case DisciplineTasks.OASISCTransferPT:
                    case DisciplineTasks.OASISCTransferOT:
                    case DisciplineTasks.OASISCTransferDischarge:
                    case DisciplineTasks.SNAssessment:
                    case DisciplineTasks.SNAssessmentRecert:
                        if (DisciplineTaskFactory.AllRecertDisciplineTasks().Contains(schedule.DisciplineTask))
                        {
                            if (ScheduleStatusFatory.OASISCompleted().Contains(schedule.Status))
                            {
                                patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, true);
                            }
                            else
                            {
                                patientRepository.SetRecertFlag(Current.AgencyId, schedule.EpisodeId, schedule.PatientId, false);
                            }
                        }
                        result = assessmentService.UpdateAssessmentForDetail(schedule);
                        break;
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVwithAideSupervision:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNDiabeticDailyVisit:
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var notesQuestions = visitNote.ToDictionary();
                                if (notesQuestions != null)
                                {
                                    if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToShortDateString(); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToShortDateString(), Name = "VisitDate", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                    visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                    visitNote.IsDeprecated = schedule.IsDeprecated;
                                    visitNote.NoteType = type;
                                    visitNote.EpisodeId = schedule.EpisodeId;
                                    visitNote.Status = schedule.Status > 0 ? schedule.Status : visitNote.Status;
                                    result = patientRepository.UpdateVisitNote(visitNote);
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;


                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTDischarge:
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTDischarge:
                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.STVisit:
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STDischarge:
                    case DisciplineTasks.PTAVisit:
                    case DisciplineTasks.PTReEvaluation:
                    case DisciplineTasks.COTAVisit:
                    case DisciplineTasks.PTMaintenance:
                    case DisciplineTasks.OTMaintenance:
                    case DisciplineTasks.STMaintenance:
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var notesQuestions = visitNote.ToDictionary();
                                if (notesQuestions != null)
                                {
                                    if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToShortDateString(); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToShortDateString(), Name = "VisitDate", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                    notesQuestions.Values.ForEach(q => { q.Type = type; });
                                    visitNote.NoteType = type;
                                    visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                    visitNote.IsDeprecated = schedule.IsDeprecated;
                                    visitNote.EpisodeId = schedule.EpisodeId;
                                    visitNote.Status = schedule.Status > 0 ? schedule.Status : visitNote.Status;
                                    result = patientRepository.UpdateVisitNote(visitNote);
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.LVNSupervisoryVisit:
                    case DisciplineTasks.MSWEvaluationAssessment:
                    case DisciplineTasks.HHAideSupervisoryVisit:
                    case DisciplineTasks.MSWVisit:
                    case DisciplineTasks.MSWDischarge:
                    case DisciplineTasks.DieticianVisit:
                    case DisciplineTasks.MSWAssessment:
                    case DisciplineTasks.MSWProgressNote:
                    case DisciplineTasks.HHAideVisit:
                    case DisciplineTasks.DriverOrTransportationNote:
                    case DisciplineTasks.PASVisit:
                    case DisciplineTasks.PASCarePlan:
                    case DisciplineTasks.UAPWoundCareVisit:
                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var notesQuestions = visitNote.ToDictionary();
                                if (notesQuestions != null)
                                {
                                    if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToShortDateString(); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToShortDateString(), Name = "VisitDate", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeIn")) { notesQuestions["TimeIn"].Answer = schedule.TimeIn; } else { notesQuestions.Add("TimeIn", new NotesQuestion { Answer = schedule.TimeIn, Name = "TimeIn", Type = type }); }
                                    if (notesQuestions.ContainsKey("TimeOut")) { notesQuestions["TimeOut"].Answer = schedule.TimeOut; } else { notesQuestions.Add("TimeOut", new NotesQuestion { Answer = schedule.TimeOut, Name = "TimeOut", Type = type }); }
                                    visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                    visitNote.IsDeprecated = schedule.IsDeprecated;
                                    visitNote.EpisodeId = schedule.EpisodeId;
                                    visitNote.Status = schedule.Status > 0 ? schedule.Status : visitNote.Status;
                                    result = patientRepository.UpdateVisitNote(visitNote);
                                }
                                else
                                {
                                    result = true;
                                }
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;

                    case DisciplineTasks.HHAideCarePlan:
                    case DisciplineTasks.DischargeSummary:
                    case DisciplineTasks.SixtyDaySummary:
                    case DisciplineTasks.TransferSummary:
                    case DisciplineTasks.CoordinationOfCare:
                        {
                            var visitNote = patientRepository.GetVisitNote(Current.AgencyId, schedule.PatientId, schedule.EventId);
                            if (visitNote != null && visitNote.Note.IsNotNullOrEmpty())
                            {
                                var notesQuestions = visitNote.ToDictionary();
                                if (notesQuestions != null)
                                {
                                    if (notesQuestions.ContainsKey("VisitDate")) { notesQuestions["VisitDate"].Answer = schedule.VisitDate.ToShortDateString(); } else { notesQuestions.Add("VisitDate", new NotesQuestion { Answer = schedule.VisitDate.ToShortDateString(), Name = "VisitDate", Type = type }); }
                                    visitNote.Note = notesQuestions.Values.ToList().ToXml();
                                    visitNote.IsDeprecated = schedule.IsDeprecated;
                                    visitNote.EpisodeId = schedule.EpisodeId;
                                    visitNote.Status = schedule.Status > 0 ? schedule.Status : visitNote.Status;
                                    result = patientRepository.UpdateVisitNote(visitNote);
                                }
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.PhysicianOrder:
                        {
                            var physicianOrder = patientRepository.GetOrderOnly(schedule.EventId, schedule.PatientId, Current.AgencyId);
                            if (physicianOrder != null)
                            {
                                if (schedule.VisitDate.IsValid())
                                {
                                    physicianOrder.OrderDate = schedule.VisitDate;
                                }
                                physicianOrder.Status = schedule.Status > 0 ? schedule.Status : physicianOrder.Status;
                                if (physicianOrder.Status != schedule.Status && physicianOrder.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                                {
                                    var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                                    if (physician != null)
                                    {
                                        physicianOrder.PhysicianData = physician.ToXml();
                                    }
                                }
                                physicianOrder.PhysicianId = schedule.PhysicianId;
                                physicianOrder.IsDeprecated = schedule.IsDeprecated;
                                physicianOrder.EpisodeId = schedule.EpisodeId;
                                result = patientRepository.UpdateOrderModel(physicianOrder);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.HCFA485:
                    case DisciplineTasks.NonOasisHCFA485:
                        result = assessmentService.UpdatePlanOfCareForDetail(schedule);
                        break;
                    case DisciplineTasks.HCFA485StandAlone:
                        result = assessmentService.UpdatePlanOfCareStandAloneForDetail(schedule);
                        break;
                    case DisciplineTasks.FaceToFaceEncounter:
                        var faceToFace = patientRepository.GetFaceToFaceEncounter(schedule.EventId, Current.AgencyId);
                        if (faceToFace != null)
                        {
                            faceToFace.Status = schedule.Status > 0 ? schedule.Status : faceToFace.Status;
                            if (faceToFace.Status != schedule.Status && faceToFace.Status == (int)ScheduleStatus.OrderReturnedWPhysicianSignature && !schedule.PhysicianId.IsEmpty())
                            {
                                var physician = physicianRepository.Get(schedule.PhysicianId, Current.AgencyId);
                                if (physician != null)
                                {
                                    faceToFace.PhysicianData = physician.ToXml();
                                }
                            }
                            faceToFace.PhysicianId = schedule.PhysicianId;
                            faceToFace.IsDeprecated = schedule.IsDeprecated;
                            faceToFace.RequestDate = schedule.VisitDate;
                            faceToFace.EpisodeId = schedule.EpisodeId;

                            if (patientRepository.UpdateFaceToFaceEncounter(faceToFace))
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.IncidentAccidentReport:
                        {
                            var accidentReport = agencyRepository.GetIncidentReport(Current.AgencyId, schedule.EventId);
                            if (accidentReport != null)
                            {
                                if (schedule.VisitDate.IsValid())
                                {
                                    accidentReport.IncidentDate = schedule.VisitDate;
                                }
                                accidentReport.IsDeprecated = schedule.IsDeprecated;
                                accidentReport.EpisodeId = schedule.EpisodeId;
                                accidentReport.Status = schedule.Status > 0 ? schedule.Status : accidentReport.Status;
                                result = agencyRepository.UpdateIncidentModal(accidentReport);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.InfectionReport:
                        {
                            var infectionReport = agencyRepository.GetInfectionReport(Current.AgencyId, schedule.EventId);
                            if (infectionReport != null)
                            {
                                if (schedule.VisitDate.IsValid())
                                {
                                    infectionReport.InfectionDate = schedule.VisitDate;
                                }
                                infectionReport.IsDeprecated = schedule.IsDeprecated;
                                infectionReport.EpisodeId = schedule.EpisodeId;
                                infectionReport.Status = schedule.Status > 0 ? schedule.Status : infectionReport.Status;
                                result = agencyRepository.UpdateInfectionModal(infectionReport);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                    case DisciplineTasks.CommunicationNote:
                        {
                            var comNote = patientRepository.GetCommunicationNote(schedule.EventId, schedule.PatientId, Current.AgencyId);
                            if (comNote != null)
                            {
                                comNote.IsDeprecated = schedule.IsDeprecated;
                                if (schedule.VisitDate.IsValid())
                                {
                                    comNote.Created = schedule.VisitDate;
                                }
                                comNote.Status = schedule.Status > 0 ? schedule.Status : comNote.Status;
                                if (comNote.Status != schedule.Status && comNote.Status == (int)ScheduleStatus.NoteCompleted && !comNote.PhysicianId.IsEmpty())
                                {
                                    var physician = physicianRepository.Get(comNote.PhysicianId, Current.AgencyId);
                                    if (physician != null)
                                    {
                                        comNote.PhysicianData = physician.ToXml();
                                    }
                                }
                                comNote.EpisodeId = schedule.EpisodeId;
                                result = patientRepository.UpdateCommunicationNoteModal(comNote);
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        break;
                }
            }
            return result;
        }

        private List<NotesQuestion> ProcessNoteQuestions(FormCollection formCollection)
        {
            string type = formCollection["Type"];
            formCollection.Remove("Type");

            var questions = new Dictionary<string, NotesQuestion>();

            foreach (var key in formCollection.AllKeys)
            {
                string[] nameArray = key.Split('_');
                if (nameArray.Length > 0)
                {
                    nameArray.Reverse();
                    string name = nameArray[0];
                    if (!questions.ContainsKey(name))
                    {
                        questions.Add(name, new NotesQuestion { Name = name, Answer = formCollection.GetValues(key).Join(","), Type = type });
                    }
                }
            }
            return questions.Values.ToList();
        }

        private string ProcessHospitalizationData(FormCollection formCollection)
        {
            formCollection.Remove("Id");
            formCollection.Remove("PatientId");
            formCollection.Remove("EpisodeId");
            formCollection.Remove("UserId");
            formCollection.Remove("Templates");
            formCollection.Remove("M0903LastHomeVisitDate");
            formCollection.Remove("M0906DischargeDate");

            var questions = new List<Question>();
            foreach (var key in formCollection.AllKeys)
            {
                questions.Add(Question.Create(key, formCollection.GetValues(key).Join(",")));
            }
            return questions.ToXml();
        }

        private string GetEpisodeFrequency(Assessment assessment)
        {
            var frequency = string.Empty;

            if (assessment != null)
            {
                var frequencyArray = new List<string>();
                var assessmentQuestions = assessment.ToDictionary();
                if (assessmentQuestions.ContainsKey("485SNFrequency") && assessmentQuestions["485SNFrequency"].Answer != "")
                    frequencyArray.Add("SN Frequency:" + assessmentQuestions["485SNFrequency"].Answer);
                if (assessmentQuestions.ContainsKey("485PTFrequency") && assessmentQuestions["485PTFrequency"].Answer != "")
                    frequencyArray.Add("PT Frequency:" + assessmentQuestions["485PTFrequency"].Answer);
                if (assessmentQuestions.ContainsKey("485OTFrequency") && assessmentQuestions["485OTFrequency"].Answer != "")
                    frequencyArray.Add("OT Frequency:" + assessmentQuestions["485OTFrequency"].Answer);
                if (assessmentQuestions.ContainsKey("485STFrequency") && assessmentQuestions["485STFrequency"].Answer != "")
                    frequencyArray.Add("ST Frequency:" + assessmentQuestions["485STFrequency"].Answer);
                if (assessmentQuestions.ContainsKey("485MSWFrequency") && assessmentQuestions["485MSWFrequency"].Answer != "")
                    frequencyArray.Add("MSW Frequency:" + assessmentQuestions["485MSWFrequency"].Answer);
                if (assessmentQuestions.ContainsKey("485HHAFrequency") && assessmentQuestions["485HHAFrequency"].Answer != "")
                    frequencyArray.Add("HHA Frequency:" + assessmentQuestions["485HHAFrequency"].Answer);
                if (frequencyArray.Count > 0)
                    frequency = frequencyArray.ToArray().Join(", ") + ".";

            }
            return frequency;
        }

        private bool SaveScheduleModalFactory(ScheduleEvent scheduleEvent, Patient patient, PatientEpisode episode)
        {
            bool result = false;
            switch (((DisciplineTasks)scheduleEvent.DisciplineTask))
            {
                case DisciplineTasks.OASISCDeath:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;
                case DisciplineTasks.OASISCDeathOT:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;
                case DisciplineTasks.OASISCDeathPT:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgencyDeath, episode);
                    break;
                case DisciplineTasks.OASISCDischarge:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;
                case DisciplineTasks.OASISCDischargeOT:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;
                case DisciplineTasks.OASISCDischargePT:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.DischargeFromAgency, episode);
                    break;
                case DisciplineTasks.NonOASISDischarge:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.NonOasisDischarge, episode);
                    break;
                case DisciplineTasks.OASISCFollowUp:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.FollowUp, episode);
                    break;
                case DisciplineTasks.OASISCFollowupPT:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.FollowUp, episode);
                    break;
                case DisciplineTasks.OASISCFollowupOT:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.FollowUp, episode);
                    break;
                case DisciplineTasks.OASISCRecertification:
                    var currentMedRecertification = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertification);
                    break;
                case DisciplineTasks.OASISCRecertificationPT:
                    var currentMedRecertificationPT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertificationPT);
                    break;
                case DisciplineTasks.OASISCRecertificationOT:
                    var currentMedRecertificationOT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.Recertification, episode, currentMedRecertificationOT);
                    break;
                case DisciplineTasks.SNAssessmentRecert:
                case DisciplineTasks.NonOASISRecertification:
                    var currentMedNonOasisRecertification = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.NonOasisRecertification, episode, currentMedNonOasisRecertification);
                    break;
                case DisciplineTasks.OASISCResumptionofCare:
                    var currentMedResumptionofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCare);
                    break;
                case DisciplineTasks.OASISCResumptionofCarePT:
                    var currentMedResumptionofCarePT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCarePT);
                    break;
                case DisciplineTasks.OASISCResumptionofCareOT:
                    var currentMedResumptionofCareOT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.ResumptionOfCare, episode, currentMedResumptionofCareOT);
                    break;
                case DisciplineTasks.OASISCStartofCare:
                    var currentMedStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCare);
                    break;
                case DisciplineTasks.OASISCStartofCarePT:
                    var currentMedStartofCarePT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCarePT);
                    break;
                case DisciplineTasks.OASISCStartofCareOT:
                    var currentMedStartofCareOT = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.StartOfCare, episode, currentMedStartofCareOT);
                    break;
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.NonOASISStartofCare:
                    var currentMedNonOasisStartofCare = this.GetCurrentMedicationsByCategory(patient.Id, "Active").ToXml();
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.NonOasisStartOfCare, episode, currentMedNonOasisStartofCare);
                    break;
                case DisciplineTasks.OASISCTransferDischarge:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientDischarged, episode);
                    break;
                case DisciplineTasks.OASISCTransfer:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;
                case DisciplineTasks.OASISCTransferPT:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;
                case DisciplineTasks.OASISCTransferOT:
                    result = assessmentService.AddAssessment(scheduleEvent, AssessmentType.TransferInPatientNotDischarged, episode);
                    break;
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                case DisciplineTasks.SNVTeachingTraining:
                case DisciplineTasks.SNVManagementAndEvaluation:
                case DisciplineTasks.SNVObservationAndAssessment:
                case DisciplineTasks.SNDiabeticDailyVisit:
                    var snNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNote);
                    break;
                case DisciplineTasks.LVNSupervisoryVisit:
                case DisciplineTasks.HHAideSupervisoryVisit:
                    var snNoteNonebillableNursing = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableNursing);
                    break;
                case DisciplineTasks.PTEvaluation:
                case DisciplineTasks.PTReEvaluation:
                    var ptEval = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(ptEval);
                    break;
                case DisciplineTasks.PTVisit:
                case DisciplineTasks.PTDischarge:
                case DisciplineTasks.PTAVisit:
                case DisciplineTasks.PTMaintenance:
                    var snNoteNonebillablePT = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillablePT);
                    break;
                case DisciplineTasks.OTEvaluation:
                case DisciplineTasks.OTReEvaluation:
                    var otEval = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(otEval);
                    break;
                case DisciplineTasks.OTDischarge:
                case DisciplineTasks.OTVisit:
                case DisciplineTasks.COTAVisit:
                case DisciplineTasks.OTMaintenance:
                    var snNoteNonebillableOT = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableOT);
                    break;
                case DisciplineTasks.STEvaluation:
                case DisciplineTasks.STReEvaluation:
                    var stEval = new PatientVisitNote { AgencyId = Current.AgencyId, OrderNumber = patientRepository.GetNextOrderNumber(), Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(stEval);
                    break;
                case DisciplineTasks.STVisit:
                case DisciplineTasks.STDischarge:
                case DisciplineTasks.STMaintenance:
                    var snNoteNonebillableST = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableST);
                    break;
                case DisciplineTasks.MSWEvaluationAssessment:
                case DisciplineTasks.MSWVisit:
                case DisciplineTasks.MSWDischarge:
                case DisciplineTasks.MSWAssessment:
                case DisciplineTasks.MSWProgressNote:
                    var snNoteNonebillableMSW = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableMSW);
                    break;
                case DisciplineTasks.DriverOrTransportationNote:
                    var driverOrTransportationNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(driverOrTransportationNote);
                    break;
                case DisciplineTasks.DieticianVisit:
                    var snNoteNonebillableDietician = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(snNoteNonebillableDietician);
                    break;
                case DisciplineTasks.HHAideVisit:
                    var hhAideVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(hhAideVisit);
                    break;
                case DisciplineTasks.HHAideCarePlan:
                    var hhAideCarePlan = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(hhAideCarePlan);
                    break;
                case DisciplineTasks.PASVisit:
                    var pasVisit = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(pasVisit);
                    break;
                case DisciplineTasks.PASCarePlan:
                    var pasCarePlan = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(pasCarePlan);
                    break;
                case DisciplineTasks.DischargeSummary:
                    var physician = physicianRepository.GetPatientPhysicians(patient.Id, Current.AgencyId).SingleOrDefault(p => p.Primary);
                    var dischargeSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    if (physician != null)
                    {
                        var questions = new List<NotesQuestion>();
                        questions.Add(new NotesQuestion { Name = "Physician", Answer = Convert.ToString(physician.Id), Type = "DischargeSummary" });
                        dischargeSummary.Note = questions.ToXml();
                        result = patientRepository.AddVisitNote(dischargeSummary);
                    }
                    else
                    {
                        result = patientRepository.AddVisitNote(dischargeSummary);
                    }
                    break;

                case DisciplineTasks.PhysicianOrder:
                    var order = new PhysicianOrder { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, OrderDate = scheduleEvent.EventDate, Created = DateTime.Now, Text = "", Summary = "", Status = scheduleEvent.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
                    if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                    {
                        order.PhysicianId = patient.PhysicianContacts[0].Id;
                    }
                    result = patientRepository.AddOrder(order);
                    break;

                case DisciplineTasks.HCFA485StandAlone:
                    var planofCare = new PlanofCareStandAlone { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, Status = scheduleEvent.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
                    planofCare.Questions = new List<Question>();
                    planofCare.Data = planofCare.Questions.ToXml();
                    result = planofCareRepository.AddStandAlone(planofCare);
                    break;

                case DisciplineTasks.NonOasisHCFA485:

                    break;

                case DisciplineTasks.HCFA485:

                    break;

                case DisciplineTasks.HCFA486:
                case DisciplineTasks.PostHospitalizationOrder:
                case DisciplineTasks.MedicaidPOC:

                    break;
                case DisciplineTasks.IncidentAccidentReport:
                    var incidentReport = new Incident { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, IncidentDate = scheduleEvent.EventDate, Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.ReportAndNotesCreated), UserId = scheduleEvent.UserId };
                    result = agencyRepository.AddIncident(incidentReport);
                    break;
                case DisciplineTasks.InfectionReport:
                    var infectionReport = new Infection { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, InfectionDate = scheduleEvent.EventDate, Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.ReportAndNotesCreated), UserId = scheduleEvent.UserId };
                    result = agencyRepository.AddInfection(infectionReport);
                    break;
                case DisciplineTasks.SixtyDaySummary:
                    var sixtyDaySummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(sixtyDaySummary);
                    break;
                case DisciplineTasks.TransferSummary:
                case DisciplineTasks.CoordinationOfCare:
                    var transferSummary = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(transferSummary);
                    break;
                case DisciplineTasks.CommunicationNote:
                    var comNote = new CommunicationNote { Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, AgencyId = Current.AgencyId, UserId = scheduleEvent.UserId, Status = ((int)ScheduleStatus.NoteNotYetDue), Created = scheduleEvent.EventDate, Modified = DateTime.Now };
                    result = patientRepository.AddCommunicationNote(comNote);
                    break;
                case DisciplineTasks.FaceToFaceEncounter:
                    var faceToFaceEncounter = new FaceToFaceEncounter { Id = scheduleEvent.EventId, AgencyId = Current.AgencyId, EpisodeId = scheduleEvent.EpisodeId, PatientId = scheduleEvent.PatientId, UserId = scheduleEvent.UserId, RequestDate = scheduleEvent.EventDate, Created = DateTime.Now, Modified = DateTime.Now, Status = scheduleEvent.Status, OrderNumber = patientRepository.GetNextOrderNumber() };
                    if (patient.PhysicianContacts.Count > 0)
                    {
                        faceToFaceEncounter.PhysicianId = patient.PhysicianContacts[0].Id;
                    }
                    result = patientRepository.AddFaceToFaceEncounter(faceToFaceEncounter);
                    break;
                case DisciplineTasks.UAPWoundCareVisit:
                case DisciplineTasks.UAPInsulinPrepAdminVisit:
                    var uapNote = new PatientVisitNote { AgencyId = Current.AgencyId, Id = scheduleEvent.EventId, PatientId = patient.Id, EpisodeId = episode.Id, NoteType = ((DisciplineTasks)scheduleEvent.DisciplineTask).ToString(), Note = (new List<NotesQuestion>()).ToXml(), Created = DateTime.Now, Modified = DateTime.Now, Status = ((int)ScheduleStatus.NoteNotYetDue), UserId = scheduleEvent.UserId, IsBillable = scheduleEvent.IsBillable };
                    result = patientRepository.AddVisitNote(uapNote);
                    break;
            }
            return result;
        }

        private void ProcessScheduleFactory(ScheduleEvent scheduleEvent)
        {
            switch (((DisciplineTasks)scheduleEvent.DisciplineTask))
            {
                case DisciplineTasks.OASISCDeath:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDeathOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDeathPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDischarge:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDischargeOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCDischargePT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.NonOASISDischarge:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCFollowUp:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCFollowupPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCFollowupOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCRecertification:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCRecertificationPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCRecertificationOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.SNAssessmentRecert:
                case DisciplineTasks.NonOASISRecertification:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCResumptionofCare:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCResumptionofCarePT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCResumptionofCareOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCStartofCare:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCStartofCarePT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCStartofCareOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.SNAssessment:
                case DisciplineTasks.NonOASISStartofCare:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.OASISCTransferDischarge:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCTransfer:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCTransferPT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.OASISCTransferOT:
                    scheduleEvent.Status = (int)ScheduleStatus.OasisNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.SkilledNurseVisit:
                case DisciplineTasks.SNInsulinAM:
                case DisciplineTasks.SNInsulinPM:
                case DisciplineTasks.FoleyCathChange:
                case DisciplineTasks.SNB12INJ:
                case DisciplineTasks.SNBMP:
                case DisciplineTasks.SNCBC:
                case DisciplineTasks.SNHaldolInj:
                case DisciplineTasks.PICCMidlinePlacement:
                case DisciplineTasks.PRNFoleyChange:
                case DisciplineTasks.PRNSNV:
                case DisciplineTasks.PRNVPforCMP:
                case DisciplineTasks.PTWithINR:
                case DisciplineTasks.PTWithINRPRNSNV:
                case DisciplineTasks.SkilledNurseHomeInfusionSD:
                case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case DisciplineTasks.SNDC:
                case DisciplineTasks.SNEvaluation:
                case DisciplineTasks.SNFoleyLabs:
                case DisciplineTasks.SNFoleyChange:
                case DisciplineTasks.SNInjection:
                case DisciplineTasks.SNInjectionLabs:
                case DisciplineTasks.SNLabsSN:
                case DisciplineTasks.SNVPsychNurse:
                case DisciplineTasks.SNVwithAideSupervision:
                case DisciplineTasks.SNVDCPlanning:
                case DisciplineTasks.SNVTeachingTraining:
                case DisciplineTasks.SNVManagementAndEvaluation:
                case DisciplineTasks.SNVObservationAndAssessment:
                case DisciplineTasks.SNDiabeticDailyVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.LVNSupervisoryVisit:
                case DisciplineTasks.HHAideSupervisoryVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.PTEvaluation:
                case DisciplineTasks.PTReEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                //case DisciplineTasks.PTSupervisoryVisit:
                //    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                //    scheduleEvent.Discipline = Disciplines.PT.ToString();
                //    scheduleEvent.IsBillable = true;
                //    scheduleEvent.Version = 1;
                //    break;
                case DisciplineTasks.PTVisit:
                case DisciplineTasks.PTAVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.Version = 2;
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.PTDischarge:
                case DisciplineTasks.PTMaintenance:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.PT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                //case DisciplineTasks.OTSupervisoryVisit:
                //    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                //    scheduleEvent.Discipline = Disciplines.OT.ToString();
                //    scheduleEvent.IsBillable = true;
                //    scheduleEvent.Version = 1;
                //    break;
                case DisciplineTasks.OTEvaluation:
                case DisciplineTasks.OTReEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    break;
                //case DisciplineTasks.OTReassessment:
                //    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                //    scheduleEvent.Discipline = Disciplines.OT.ToString();
                //    scheduleEvent.IsBillable = true;
                //    break;
                case DisciplineTasks.OTVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    break;
                case DisciplineTasks.OTDischarge:
                case DisciplineTasks.COTAVisit:
                case DisciplineTasks.OTMaintenance:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.OT.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.STReEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.STEvaluation:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    scheduleEvent.Version = 2;
                    break;
                case DisciplineTasks.STVisit:
                case DisciplineTasks.STDischarge:
                case DisciplineTasks.STMaintenance:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ST.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.MSWEvaluationAssessment:
                case DisciplineTasks.MSWVisit:
                case DisciplineTasks.MSWDischarge:
                case DisciplineTasks.MSWAssessment:
                case DisciplineTasks.MSWProgressNote:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.DriverOrTransportationNote:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.MSW.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.DieticianVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Dietician.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HHAideVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.HHAideCarePlan:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.PASVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = true;
                    break;
                case DisciplineTasks.PASCarePlan:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.DischargeSummary:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.PhysicianOrder:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HCFA485StandAlone:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.NonOasisHCFA485:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HCFA485:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderSaved;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.HCFA486:
                case DisciplineTasks.PostHospitalizationOrder:
                case DisciplineTasks.MedicaidPOC:
                    scheduleEvent.IsBillable = false;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
                    break;
                case DisciplineTasks.IncidentAccidentReport:
                    scheduleEvent.Status = (int)ScheduleStatus.ReportAndNotesCreated;
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.InfectionReport:
                    scheduleEvent.Status = (int)ScheduleStatus.ReportAndNotesCreated;
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.SixtyDaySummary:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.TransferSummary:
                case DisciplineTasks.CoordinationOfCare:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Nursing.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.CommunicationNote:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.ReportsAndNotes.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.FaceToFaceEncounter:
                    scheduleEvent.Status = (int)ScheduleStatus.OrderNotYetDue;
                    scheduleEvent.Discipline = Disciplines.Orders.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
                case DisciplineTasks.UAPWoundCareVisit:
                case DisciplineTasks.UAPInsulinPrepAdminVisit:
                    scheduleEvent.Status = (int)ScheduleStatus.NoteNotYetDue;
                    scheduleEvent.Discipline = Disciplines.HHA.ToString();
                    scheduleEvent.IsBillable = false;
                    break;
            }
        }

        private AgencyPhysician GetPhysicianFromNotes(ScheduleEvent scheduleEvent, PatientVisitNote note, IDictionary<string, NotesQuestion> questions)
        {
            var physician = new AgencyPhysician();
            if (scheduleEvent != null)
            {
                var physicianId = note.PhysicianId;
                if (physicianId.IsEmpty())
                {
                    NotesQuestion physicianQuestion = null;
                    if (DisciplineTaskFactory.PhysicianEvalNotes().Exists(d => (int)d == scheduleEvent.DisciplineTask))
                    {
                        if (questions.TryGetValue("PhysicianId", out physicianQuestion))
                        {
                            if (physicianQuestion != null && physicianQuestion.Answer.IsNotNullOrEmpty() && physicianQuestion.Answer.IsGuid())
                            {
                                physicianId = physicianQuestion.Answer.ToGuid();
                            }
                        }
                    }
                    else
                    {
                        if (questions.TryGetValue("Physician", out physicianQuestion))
                        {
                            if (physicianQuestion != null && physicianQuestion.Answer.IsNotNullOrEmpty() && physicianQuestion.Answer.IsGuid())
                            {
                                physicianId = physicianQuestion.Answer.ToGuid();
                            }
                        }
                    }
                }

                if (!physicianId.IsEmpty())
                {
                    physician = physicianRepository.Get(physicianId, Current.AgencyId);
                }
            }
            return physician;
        }

        private IDictionary<string, NotesQuestion> GetDiagnosisMergedFromOASIS(IDictionary<string, NotesQuestion> noteQuestions, IDictionary<string, NotesQuestion> oasisQuestions)
        {
            NotesQuestion primaryDiagnosis = null;
            if (oasisQuestions.TryGetValue("PrimaryDiagnosis", out primaryDiagnosis))
            {
                if (primaryDiagnosis != null)
                {
                    if (noteQuestions.ContainsKey("PrimaryDiagnosis"))
                    {
                        noteQuestions["PrimaryDiagnosis"].Answer = primaryDiagnosis != null ? primaryDiagnosis.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("PrimaryDiagnosis", primaryDiagnosis);
                    }
                }
            }

            NotesQuestion ICD9M = null;
            if (oasisQuestions.TryGetValue("ICD9M", out ICD9M))
            {
                if (ICD9M != null)
                {
                    if (noteQuestions.ContainsKey("ICD9M"))
                    {
                        noteQuestions["ICD9M"].Answer = ICD9M != null ? ICD9M.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("ICD9M", ICD9M);
                    }
                }
            }

            NotesQuestion primaryDiagnosis1 = null;
            if (oasisQuestions.TryGetValue("PrimaryDiagnosis1", out primaryDiagnosis1))
            {
                if (primaryDiagnosis1 != null)
                {
                    if (noteQuestions.ContainsKey("PrimaryDiagnosis1"))
                    {
                        noteQuestions["PrimaryDiagnosis1"].Answer = primaryDiagnosis1 != null ? primaryDiagnosis1.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("PrimaryDiagnosis1", primaryDiagnosis1);
                    }
                }
            }

            NotesQuestion ICD9M1 = null;
            if (oasisQuestions.TryGetValue("ICD9M1", out ICD9M1))
            {
                if (ICD9M1 != null)
                {
                    if (noteQuestions.ContainsKey("ICD9M1"))
                    {
                        noteQuestions["ICD9M1"].Answer = ICD9M1 != null ? ICD9M1.Answer : string.Empty;
                    }
                    else
                    {
                        noteQuestions.Add("ICD9M1", ICD9M1);
                    }
                }
            }

            return noteQuestions;
        }

        private VisitNoteViewData SkilledNurseVisitContent(ScheduleEvent scheduleEvent, PatientVisitNote previousNote, VisitNoteViewData viewData)
        {
            viewData.IsWoundCareExist = previousNote.IsWoundCare;
            viewData.IsSupplyExist = previousNote.IsSupplyExist;

            var noteItems = previousNote.ToDictionary();
            var nameArray = new string[] { "PatientId", "EpisodeId", "EventId", "DisciplineTask", "VisitDate", "TimeIn", "TimeOut", "PreviousNotes", "Clinician", "SignatureDate", "button", "PrimaryDiagnosis", "ICD9M", "PrimaryDiagnosis1", "ICD9M1" };
            nameArray.ForEach(name => { noteItems.Remove(name); });
            viewData.Questions = noteItems;
            var currentNote = patientRepository.GetVisitNote(Current.AgencyId, scheduleEvent.PatientId, scheduleEvent.EventId);
            if (currentNote != null)
            {
                currentNote.Questions = noteItems.Values.ToList();
                currentNote.Note = currentNote.Questions.ToXml();
                currentNote.IsWoundCare = previousNote.IsWoundCare;
                currentNote.WoundNote = previousNote.WoundNote;
                currentNote.Modified = DateTime.Now;

                var oldStatus = scheduleEvent.Status;
                scheduleEvent.Status = (int)ScheduleStatus.NoteSaved;
                if (patientRepository.UpdateScheduleEventNew(scheduleEvent))
                {
                    if (patientRepository.UpdateVisitNote(currentNote))
                    {
                        Auditor.Log(Current.AgencyId, Current.UserId, Current.UserFullName, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, Actions.LoadPreviousNote, (DisciplineTasks)scheduleEvent.DisciplineTask, "Loaded Previous Note");
                    }
                    else
                    {
                        scheduleEvent.Status = oldStatus;
                        patientRepository.UpdateScheduleEventNew(scheduleEvent);
                    }
                }
            }
            return viewData;
        }

        #endregion
    }
}
