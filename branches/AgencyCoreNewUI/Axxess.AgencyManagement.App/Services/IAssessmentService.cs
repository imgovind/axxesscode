﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;

    using Axxess.Core;
    using Axxess.Core.Enums;

    using Axxess.OasisC.Domain;
    using Axxess.OasisC.Enums;

    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.LookUp.Domain;
   
    public interface IAssessmentService
    {
        List<Question> Get485FromAssessment(Assessment assessment);
        Assessment SaveAssessment(FormCollection formCollection, HttpFileCollectionBase httpFiles);
        bool AddAssessment(ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode);
        bool AddAssessment(ScheduleEvent oasisSchedule, AssessmentType assessmentType, PatientEpisode episode, string MedicationProfile);
        bool MarkAsDeleted(Guid assessmentId, Guid episodeId, Guid patientId,bool isDeprecated);
        bool ReassignUser(Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        Assessment GetAssessment(Guid assessmentId);
        Assessment GetAssessmentWithScheduleType(Guid assessmentId);
        OasisAudit Audit(Guid assessmentId, Guid patientId, Guid episodeId);
        ValidationInfoViewData Validate(Guid assessmentId, Guid patientId, Guid episodeId);
        ValidationInfoViewData Validate(Assessment assessment);
        ValidationInfoViewData ValidateInactivate(Guid assessmentId);
        string OasisHeader(AgencyLocation agencyLocation);
        string OasisFooter(int totalNumberOfRecord);
        List<SubmissionBodyFormat> GetOasisSubmissionFormatInstructions();
        bool UpdatePlanofCare(FormCollection formCollection);
        bool UpdatePlanOfCareForDetail(ScheduleEvent schedule);
        bool UpdatePlanofCareStandAlone(FormCollection formCollection);
        bool UpdatePlanOfCareStandAloneForDetail(ScheduleEvent schedule);
        bool CreatePlanofCare(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment, bool isNonOasis);
        void GeneratePlanofCare(ScheduleEvent scheduleEvent, Patient patient, Assessment assessment, bool isNonOasis);
        bool MarkPlanOfCareAsDeleted(Guid eventId, Guid episodeId, Guid patientId, bool isDeprecated);
        bool MarkPlanOfCareStandAloneAsDeleted(Guid eventId, Guid episodeId, Guid patientId, bool isDeprecated);
        bool ReassignPlanOfCaresUser(Guid agencyId, Guid episodeId, Guid patientId, Guid eventId, Guid employeeId);
        ScheduleEvent GetEpisodeAssessmentEvent(Guid episodeId, Guid patientId);
       
        string GetPlanofCareUrl(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType);
        ScheduleEvent GetPlanofCareScheduleEvent(Guid episodeId, Guid patientId, Guid assessmentId, string assessmentType);
        IDictionary<string, Question> Allergies(Assessment assessment);
        IDictionary<string, Question> Diagnosis(Assessment assessment);
        IDictionary<string, Question> LocatorQuestions(Assessment assessment);
        bool UpdateAssessmentStatusNew(ScheduleEvent scheduleEvent, string status, string reason);
        bool UpdateAssessmentCorrectionNumber(Guid id, Guid patientId, Guid episodeId, int CorrectionNumber);
        bool UpdateAssessmentStatusForSubmit(Guid id, Guid patientId, Guid episodeId,  string status, string signature, DateTime date, string timeIn, string timeOut);
        bool UpdateAssessmentForDetail(ScheduleEvent schedule);
        bool MarkAsExportedORCompleted(List<string> OasisSelected, int status);
        bool DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId);
        bool AddSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        List<Supply> GetAssessmentSupply(Guid episodeId, Guid patientId, Guid eventId);
        bool UpdateSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        bool DeleteSupply(Guid episodeId, Guid patientId, Guid eventId, Supply supply);
        bool UpdatePlanofCareStatus(Guid id, Guid episodeId, Guid patientId, string actionType, string reason);
        Assessment GetAssessmentPrint(string Type);
        Assessment GetAssessmentPrint(Guid episodeId, Guid patientId, Guid eventId);
        PlanofCare GetPlanOfCarePrint(Guid episodeId, Guid patientId, Guid eventId);
        DateRange GetPlanofCareCertPeriod(Guid episodeId, Guid patientId, Guid assessmentId);
       
        string GetDiagnosisData(Assessment assessment);
        PlanofCare GetPlanofCare(Guid episodeId, Guid patientId, Guid planofCareId);
        PlanofCareStandAlone GetPlanofCareStandAlone(Guid episodeId, Guid patientId, Guid planofCareId);

        List<AssessmentExport> GetAssessmentByStatus(Guid branchId, ScheduleStatus status, int patientStatus, DateTime StartDate, DateTime EndDate);
        List<AssessmentExport> GetAssessmentByStatus(Guid branchId, ScheduleStatus status, List<int> paymentSources);
    }
}
