﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.AgencyManagement.Domain; 

    public interface IScheduleService
    {
        PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId);
        PatientEpisode GetEpisode(Guid agencyId, Guid patientId, DateTime date, string discipline);
        PatientEpisode GetEpisode(Guid agencyId, Guid episodeId, Guid patientId, string discipline);

        PatientEpisode GetEpisodeNew(Guid agencyId, Guid patientId, DateTime date, string discipline);
        PatientEpisode GetEpisodeNew(Guid agencyId, Guid episodeId, Guid patientId, string discipline);

       
    }
}
