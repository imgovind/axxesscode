﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Web;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;

    public interface IMessageService
    {
        Message GetSystemMessage(Guid id);
        bool DeleteSystemMessage(Guid id);
        bool SendMessage(Message message, HttpFileCollectionBase httpFiles);
        IList<Message> GetMessages(string inboxType);
    }
}
