﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;

    using Axxess.AgencyManagement.Domain;

    public interface IPhysicianService
    {
        bool CreatePhysician(AgencyPhysician physician);
        bool UpdatePhysician(AgencyPhysician physician);
        bool AddLicense(PhysicianLicense license);
        bool UpdateLicense(PhysicianLicense license);
        bool DeleteLicense(Guid id, Guid physicianId);
    }
}
