﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.App.Domain;

    public interface IReferralService
    {
        bool AddReferral(Referral referral);
        List<ReferralData> GetPending(Guid agencyId);
    }
}
