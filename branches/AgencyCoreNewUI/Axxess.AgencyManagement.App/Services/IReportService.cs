﻿namespace Axxess.AgencyManagement.App.Services
{
    using System;
    using System.Collections.Generic;

    using Domain;
    using ViewData;
    using Axxess.AgencyManagement.Domain;
    using Axxess.OasisC.Domain;

    public interface IReportService
    {
        #region Patient Reports

        List<Birthday> GetCurrentBirthdays();
        List<Birthday> GetPatientBirthdays(Guid addressBranchCode);
        List<Birthday> GetPatientBirthdays(Guid branchId, int month);
        List<AddressBookEntry> GetPatientAddressListing(Guid BranchId, int StatusId);
        List<PatientRoster> GetPatientRoster(Guid addressBranchCode, int statusId, int InsuranceId , bool isExcel);
        List<EmergencyContactInfo> GetPatientEmergencyContacts(Guid addressBranchCode, int statusId);
        List<PatientSocCertPeriod> GetPatientSocCertPeriod(Guid branchId, int statusId, DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientByPhysician(Guid agencyPhysicianId);
        List<PatientRoster> GetPatientByResponsiableEmployee(Guid branchCode, Guid userId, int statusId);
        List<PatientRoster> GetPatientByResponsiableByCaseManager(Guid branchCode, Guid caseManagerId, int statusId);
        List<PatientRoster> GetPatientRosterByInsurance(Guid branchCode, int insurance, int statusId);
        List<Authorization> GetExpiringAuthorizaton(Guid branchId, int status);
        List<SurveyCensus> GetPatientSurveyCensus(Guid branchId, int statusId, int insuranceId, bool isExcel);
        List<PatientRoster> GetPatientAnnualAdmission(Guid branchCode, int statusId, int year);
       
        #endregion

        #region Clinician Reports

        IList<OpenOasis> GetAllOpenOasis(Guid branchCode, DateTime startDate, DateTime endDate);
        List<MissedVisit> GetAllMissedVisit(Guid branchCode, DateTime startDate, DateTime endDate);
        IList<PhysicianOrder> GetPhysicianOrderHistory(Guid branchCode, int Status, DateTime startDate, DateTime endDate);
        IList<Order> GetPlanOfCareHistory(Guid branchCode, int Status, DateTime startDate, DateTime endDate);

        #endregion

        #region Schedule Reports

        List<ScheduleEvent> GetPatientScheduleEventsByDateRange(Guid patientId, DateTime fromDate, DateTime toDate);
        List<UserVisit> GetUserScheduleEventsByDateRange(Guid branchCode, Guid userId, DateTime fromDate, DateTime toDate);
        List<ScheduleEvent> GetPastDueScheduleEvents(Guid BranchId, DateTime StartDate, DateTime EndDate);
        List<ScheduleEvent> GetPastDueScheduleEventsByDiscipline(Guid BranchId, string discipline, DateTime StartDate, DateTime EndDate);
        List<ScheduleEvent> GetScheduleEventsByDateRange(Guid BranchId, DateTime StartDate, DateTime EndDate);
        List<ScheduleEvent> GetCaseManagerScheduleByBranch(Guid branchId, DateTime startDate, DateTime endDate);
        List<ScheduleEvent> GetScheduleDeviation(Guid branchId, DateTime startDate, DateTime endDate);

        #endregion

        #region Employee Reports

        List<User> GetEmployeeRoster(Guid branchCode, int status);
        List<Birthday> GetEmployeeBirthdays(Guid branchCode, int status, int month);
        List<License> GetEmployeeExpiringLicenses(Guid branchCode, int status);
        List<UserVisit> GetEmployeeScheduleByDateRange(Guid BranchId, DateTime StartDate, DateTime EndDate);

        #endregion

        #region Billing Reports

        List<ClaimLean> BillViewDataByStatus(Guid branchId, string type, int status, DateTime startDate, DateTime endDate);
        List<ClaimLean> SubmittedBillViewDataByDateRange(Guid branchId, string type, DateTime startDate, DateTime endDate);
        List<TypeOfBill> UnProcessedBillViewData(Guid BranchId, string type);
        #endregion

        #region Statistical Reports

        List<UserVisit> GetEmployeeVisistList(Guid userId, DateTime startDate, DateTime endDate);
        List<Patient> GetPatientByAdmissionUnduplicatedByDateRange(Guid branchId, int status, DateTime startDate, DateTime endDate);
        List<PatientRoster> GetPatientByAdmissionMonthYear(Guid branchId, int status, int month, int year);

        #endregion
    }
}
