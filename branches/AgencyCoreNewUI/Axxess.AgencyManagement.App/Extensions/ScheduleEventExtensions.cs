﻿namespace Axxess.AgencyManagement.App.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Axxess.OasisC.Domain;
    using Axxess.AgencyManagement.Domain;
    using Axxess.Core.Extension;
    using Axxess.OasisC.Enums;

    public static class ScheduleEventExtensions
    {
        public static IDictionary<string, Question> ToOASISDictionary(this ScheduleEvent scheduleEvent)
        {
            IDictionary<string, Question> questions = new Dictionary<string, Question>();
            if (scheduleEvent != null && scheduleEvent.Note.IsNotNullOrEmpty())
            {
                var key = string.Empty;
                var oasisQuestions= scheduleEvent.Note.ToObject<List<Question>>();
                if (oasisQuestions != null && oasisQuestions.Count > 0)
                {
                    oasisQuestions.ForEach(question =>
                    {
                        if (question.Type == QuestionType.Moo)
                        {
                            key = string.Format("{0}{1}", question.Code, question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                        else if (question.Type == QuestionType.PlanofCare)
                        {
                            key = string.Format("485{0}", question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                        else if (question.Type == QuestionType.Generic)
                        {
                            key = string.Format("Generic{0}", question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                        else
                        {
                            key = string.Format("{0}", question.Name);
                            if (!questions.ContainsKey(key))
                            {
                                questions.Add(key, question);
                            }
                        }
                    });
                }
            }
            return questions;
        }

        public static IDictionary<string, NotesQuestion> ToVisitNoteDictionary(this ScheduleEvent scheduleEvent)
        {
            IDictionary<string, NotesQuestion> questions = new Dictionary<string, NotesQuestion>();
            if (scheduleEvent != null && scheduleEvent.Note.IsNotNullOrEmpty())
            {
                var noteQuestions = scheduleEvent.Note.ToObject<List<NotesQuestion>>();
                if (noteQuestions != null && noteQuestions.Count > 0)
                {
                    noteQuestions.ForEach(n =>
                    {
                        questions.Add(n.Name, n);
                    });
                }
            }
            return questions;
        }

        public static IDictionary<string, Question> Diagnosis(this ScheduleEvent scheduleEvent)
        {
            var diagnosis = new Dictionary<string, Question>();
            if (scheduleEvent != null)
            {
                var questions = scheduleEvent.ToOASISDictionary();

                if (questions.ContainsKey("M1020PrimaryDiagnosis") && questions["M1020PrimaryDiagnosis"] != null)
                {
                    diagnosis.Add("M1020PrimaryDiagnosis", questions["M1020PrimaryDiagnosis"]);
                }
                if (questions.ContainsKey("M1020ICD9M") && questions["M1020ICD9M"] != null)
                {
                    diagnosis.Add("M1020ICD9M", questions["M1020ICD9M"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis1") && questions["M1022PrimaryDiagnosis1"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis1", questions["M1022PrimaryDiagnosis1"]);
                }

                if (questions.ContainsKey("M1022ICD9M1") && questions["M1022ICD9M1"] != null)
                {
                    diagnosis.Add("M1022ICD9M1", questions["M1022ICD9M1"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis2") && questions["M1022PrimaryDiagnosis2"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis2", questions["M1022PrimaryDiagnosis2"]);
                }

                if (questions.ContainsKey("M1022ICD9M2") && questions["M1022ICD9M2"] != null)
                {
                    diagnosis.Add("M1022ICD9M2", questions["M1022ICD9M2"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis3") && questions["M1022PrimaryDiagnosis3"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis3", questions["M1022PrimaryDiagnosis3"]);
                }

                if (questions.ContainsKey("M1022ICD9M3") && questions["M1022ICD9M3"] != null)
                {
                    diagnosis.Add("M1022ICD9M3", questions["M1022ICD9M3"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis4") && questions["M1022PrimaryDiagnosis4"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis4", questions["M1022PrimaryDiagnosis4"]);
                }

                if (questions.ContainsKey("M1022ICD9M4") && questions["M1022ICD9M4"] != null)
                {
                    diagnosis.Add("M1022ICD9M4", questions["M1022ICD9M4"]);
                }

                if (questions.ContainsKey("M1022PrimaryDiagnosis5") && questions["M1022PrimaryDiagnosis5"] != null)
                {
                    diagnosis.Add("M1022PrimaryDiagnosis5", questions["M1022PrimaryDiagnosis5"]);
                }

                if (questions.ContainsKey("M1022ICD9M5") && questions["M1022ICD9M5"] != null)
                {
                    diagnosis.Add("M1022ICD9M5", questions["M1022ICD9M5"]);
                }

            }
            return diagnosis;
        }

    }
}
