﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;


namespace Axxess.Core.Extension
{
    public static class WorkBookExtensions
    {
        public static void FinishWritingToExcelSpreadsheet(this HSSFWorkbook workBook, int columns)
        {
            Font headerFont = workBook.CreateFont();
            headerFont.FontName = "Calibri";
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.SetFont(headerFont);

            Font normalFont = workBook.CreateFont();
            normalFont.FontName = "Calibri";
            normalFont.FontHeightInPoints = 12;

            CellStyle normalStyle = workBook.CreateCellStyle();
            normalStyle.SetFont(normalFont);

            Sheet sheet = workBook.GetSheetAt(0);
            if (sheet != null)
            {
                int numberOfRows = sheet.PhysicalNumberOfRows;
                for (int rowCount = 0; rowCount < numberOfRows; rowCount++)
                {
                    Row row = sheet.GetRow(rowCount);
                    if (row != null)
                    {
                        var cells = row.GetCellEnumerator();
                        while (cells.MoveNext())
                        {
                            var cell = ((Cell)cells.Current);
                            if (rowCount < 2)
                            {
                                cell.CellStyle = headerStyle;
                            }
                            else if (cell.CellStyle.DataFormat == 0)
                            {
                                cell.CellStyle = normalStyle;
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                            else if (cell.CellStyle.DataFormat > 0)
                            {
                                cell.CellStyle.SetFont(normalFont);
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                        }
                    }
                }

                Row totalRow = sheet.GetRow(numberOfRows + 1);
                if (totalRow != null)
                {
                    var cells = totalRow.GetCellEnumerator();
                    while (cells.MoveNext())
                    {
                        var cell = ((Cell)cells.Current);
                        if (cell.CellStyle.DataFormat == 0)
                        {
                            cell.CellStyle = normalStyle;
                        }
                    }
                }

                int resizeColCounter = 0;
                do
                {
                    sheet.AutoSizeColumn(resizeColCounter);
                    sheet.SetColumnWidth(resizeColCounter, sheet.GetColumnWidth(resizeColCounter) + 2000);
                    resizeColCounter++;
                }
                while (resizeColCounter < columns);
                sheet.CreateFreezePane(0, 2, 0, 2);
            }
        }

        public static void FinishWritingToExcelSpreadsheetWithMultipleHeaders(this HSSFWorkbook workBook, int columns, List<string> possibleHeaderStarters)
        {
            Font headerFont = workBook.CreateFont();
            headerFont.FontName = "Calibri";
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.FontHeightInPoints = 12;

            CellStyle headerStyle = workBook.CreateCellStyle();
            headerStyle.Alignment = HorizontalAlignment.LEFT;
            headerStyle.SetFont(headerFont);

            CellStyle headerStyleCentered = workBook.CreateCellStyle();
            headerStyleCentered.Alignment = HorizontalAlignment.CENTER;
            headerStyleCentered.SetFont(headerFont);

            Font normalFont = workBook.CreateFont();
            normalFont.FontName = "Calibri";
            normalFont.FontHeightInPoints = 12;

            CellStyle normalStyle = workBook.CreateCellStyle();
            normalStyle.SetFont(normalFont);

            Sheet sheet = workBook.GetSheetAt(0);
            if (sheet != null)
            {
                int numberOfRows = sheet.PhysicalNumberOfRows;
                bool isHeader = false;
                int headerCount = 0;
                for (int rowCount = 0; rowCount < numberOfRows; rowCount++)
                {
                    Row row = sheet.GetRow(rowCount);
                    if (row != null)
                    {
                        if (isHeader)
                        {
                            headerCount++;
                            if (headerCount == 1)
                            {
                                CellRangeAddress cra = new CellRangeAddress(rowCount, rowCount, 0, 2);
                                sheet.AddMergedRegion(cra);
                            }
                            else if (headerCount == 3)
                            {
                                headerCount = 0;
                                isHeader = false;
                            }
                        }

                        var cells = row.GetCellEnumerator();
                        while (cells.MoveNext())
                        {
                            var cell = ((Cell)cells.Current);
                            if (rowCount == 0)
                            {
                                cell.CellStyle = headerStyle;
                            }
                            else if (possibleHeaderStarters.Contains(cell.StringCellValue))
                            {
                                isHeader = true;
                                cell.CellStyle = headerStyleCentered;
                                CellRangeAddress cra = new CellRangeAddress(rowCount, rowCount, 0, 2);
                                sheet.AddMergedRegion(cra);
                            }
                            else if (headerCount > 0 && headerCount < 3)
                            {
                                cell.CellStyle = headerStyle;
                                if (headerCount == 1)
                                {
                                    cell.CellStyle = headerStyleCentered;
                                }
                            }
                            else if (cell.CellStyle.DataFormat == 0)
                            {
                                cell.CellStyle = normalStyle;
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                            else if (cell.CellStyle.DataFormat > 0)
                            {
                                cell.CellStyle.SetFont(normalFont);
                                cell.CellStyle.Alignment = HorizontalAlignment.LEFT;
                            }
                        }
                    }
                }

                Row totalRow = sheet.GetRow(numberOfRows + 1);
                if (totalRow != null)
                {
                    var cells = totalRow.GetCellEnumerator();
                    while (cells.MoveNext())
                    {
                        var cell = ((Cell)cells.Current);
                        if (cell.CellStyle.DataFormat == 0)
                        {
                            cell.CellStyle = normalStyle;
                        }
                    }
                }

                int resizeColCounter = 0;
                do
                {
                    sheet.AutoSizeColumn(resizeColCounter);
                    sheet.SetColumnWidth(resizeColCounter, sheet.GetColumnWidth(resizeColCounter) + 2000);
                    resizeColCounter++;
                }
                while (resizeColCounter < columns);
                sheet.CreateFreezePane(0, 1, 0, 1);
            }
        }
    }
}
