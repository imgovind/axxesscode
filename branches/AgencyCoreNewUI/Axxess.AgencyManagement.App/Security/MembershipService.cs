﻿namespace Axxess.AgencyManagement.App.Security
{
    using System;
    using System.Web;
    using System.Linq;
    using System.Threading;
    using System.Collections.Generic;

    using Extensions;

    using Axxess.Api;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.Membership.Enums;
    using Axxess.Membership.Domain;
    using Axxess.Membership.Repositories;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    public class MembershipService : IMembershipService
    {
        #region Private Members

        private readonly IUserRepository userRepository;
        private readonly ILoginRepository loginRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly ISupportRepository supportRepository;
        private static readonly AuthenticationAgent authenticationAgent = new AuthenticationAgent();

        #endregion

        #region Constructor

        public MembershipService(IMembershipDataProvider membershipDataProvider, IAgencyManagementDataProvider agencyManagementProvider)
        {
            Check.Argument.IsNotNull(membershipDataProvider, "membershipDataProvider");
            Check.Argument.IsNotNull(agencyManagementProvider, "agencyManagementProvider");

            this.loginRepository = membershipDataProvider.LoginRepository;
            this.userRepository = agencyManagementProvider.UserRepository;
            this.supportRepository = membershipDataProvider.SupportRepository;
            this.agencyRepository = agencyManagementProvider.AgencyRepository;
        }

        #endregion

        #region IMembershipService Members

        public AxxessPrincipal Get(string userName)
        {
            ImpersonationLink link = null;
            AxxessPrincipal principal = null;
            if (userName.IsNotNullOrEmpty())
            {
                principal = Cacher.Get<AxxessPrincipal>(userName);

                if (principal == null)
                {
                    string[] userNameArray = userName.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                    if (userNameArray != null && userNameArray.Length > 1)
                    {
                        userName = userNameArray[0];
                        link = supportRepository.GetImpersonationLink(userNameArray[1].ToGuid());
                    }

                    Login login = loginRepository.Find(userName);
                    if (login != null)
                    {
                        var accounts = userRepository.GetUsersByLoginId(login.Id);
                        if (accounts.Count == 1)
                        {
                            Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                            if (role == Roles.ApplicationUser)
                            {
                                var user = userRepository.GetByLoginId(login.Id);

                                if (user != null && user.Status == (int) UserStatus.Active)
                                {
                                    var agency = agencyRepository.Get(user.AgencyId);

                                    if (agency != null)
                                    {
                                        AxxessIdentity identity = new AxxessIdentity(login.Id, link != null ? string.Format("{0}:{1}", login.EmailAddress, link.Id) : login.EmailAddress);
                                        identity.Session =
                                            new UserSession
                                            {
                                                UserId = user.Id,
                                                LoginId = login.Id,
                                                FullName = user.DisplayName,
                                                DisplayName = login.DisplayName,
                                                AgencyId = agency.Id,
                                                AgencyName = agency.Name,
                                                //MaxAgencyUserCount = agency.Package == 0 ? 100000000 : agency.Package,
                                                AgencyRoles = user.Roles.IsNotNullOrEmpty() ? user.Roles : string.Empty,
                                                Address = user.Profile != null ? user.Profile.AddressFull : string.Empty,
                                                AutomaticLogoutTime = user.AllowWeekendAccess ? user.AutomaticLogoutTime : string.Empty
                                            };

                                        if (link != null)
                                        {
                                            identity.IsImpersonated = true;
                                            identity.Session.ImpersonatorName = link.RepName;
                                            userName = string.Format("{0}:{1}", login.EmailAddress, link.Id);
                                        }
                                        principal = new AxxessPrincipal(identity, role, user.PermissionsArray);
                                        Cacher.Set<AxxessPrincipal>(userName, principal);
                                    }
                                }
                            }
                        }
                        else if (accounts.Count > 1)
                        {
                            var request = HttpContext.Current.Request;
                            if ((request.Cookies.Count > 0) && (request.Cookies[AppSettings.IdentifyAgencyCookie] != null))
                            {
                                var cookie = request.Cookies[AppSettings.IdentifyAgencyCookie];
                                string info = Crypto.Decrypt(cookie.Value);
                                if (info != null)
                                {
                                    var infoArray = info.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                                    if (infoArray != null && infoArray.Length == 2)
                                    {
                                        var userId = infoArray[0].ToGuid();
                                        var agencyId = infoArray[1].ToGuid();
                                        var account = accounts.FirstOrDefault(a => a.AgencyId == agencyId);
                                        if (account != null)
                                        {
                                            if (link == null)
                                            {
                                                principal = Get(userId, agencyId);
                                            }
                                            else
                                            {
                                                principal = Get(userId, agencyId, link.Id);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return principal;
        }

        public AxxessPrincipal Get(Guid userId, Guid agencyId)
        {
            AxxessPrincipal principal = null;

            var user = userRepository.Get(userId, agencyId);

            if (user != null)
            {
                Login login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                    if (role == Roles.ApplicationUser)
                    {
                        var agency = agencyRepository.GetById(user.AgencyId);

                        if (agency != null)
                        {
                            AxxessIdentity identity = new AxxessIdentity(login.Id, login.EmailAddress);
                            identity.Session =
                                new UserSession
                                {
                                    UserId = user.Id,
                                    LoginId = login.Id,
                                    FullName = user.DisplayName,
                                    DisplayName = login.DisplayName,
                                    AgencyId = agency.Id,
                                    AgencyName = agency.Name,
                                    //MaxAgencyUserCount = agency.Package == 0 ? 100000000 : agency.Package,
                                    AgencyRoles = user.Roles.IsNotNullOrEmpty() ? user.Roles : string.Empty,
                                    Address = user.Profile != null ? user.Profile.AddressFull : string.Empty,
                                    AutomaticLogoutTime = user.AllowWeekendAccess ? user.AutomaticLogoutTime : string.Empty,
                                };

                            principal = new AxxessPrincipal(identity, role, user.PermissionsArray);
                            Cacher.Set<AxxessPrincipal>(login.EmailAddress, principal);
                        }
                    }
                }
            }
            return principal;
        }

        public AxxessPrincipal Get(Guid userId, Guid agencyId, Guid linkId)
        {
            AxxessPrincipal principal = null;

            var user = userRepository.Get(userId, agencyId);

            if (user != null)
            {
                Login login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                    if (role == Roles.ApplicationUser)
                    {
                        var agency = agencyRepository.Get(user.AgencyId);

                        if (agency != null)
                        {
                            var link = supportRepository.GetImpersonationLink(linkId);
                            if (link != null)
                            {
                                AxxessIdentity identity = new AxxessIdentity(login.Id, string.Format("{0}:{1}", login.EmailAddress, link.Id));
                                identity.Session =
                                    new UserSession
                                    {
                                        UserId = user.Id,
                                        LoginId = login.Id,
                                        FullName = user.DisplayName,
                                        DisplayName = login.DisplayName,
                                        AgencyId = agency.Id,
                                        AgencyName = agency.Name,
                                        //MaxAgencyUserCount = agency.Package == 0 ? 100000000 : agency.Package,
                                        AgencyRoles = user.Roles.IsNotNullOrEmpty() ? user.Roles : string.Empty,
                                        Address = user.Profile != null ? user.Profile.AddressFull : string.Empty,
                                        AutomaticLogoutTime = user.AllowWeekendAccess ? user.AutomaticLogoutTime : string.Empty,
                                    };

                                identity.IsImpersonated = true;
                                identity.Session.ImpersonatorName = link.RepName;
                                principal = new AxxessPrincipal(identity, role, user.PermissionsArray);
                                Cacher.Set<AxxessPrincipal>(string.Format("{0}:{1}", login.EmailAddress, link.Id), principal);
                            }
                        }
                    }
                }
            }
            return principal;
        }

        public bool Login(Account account)
        {
            var result = false;

            var login = loginRepository.Find(account.UserName);
            if (login != null)
            {
                if (login.IsActive)
                {
                    var saltedHash = new SaltedHash();
                    if (saltedHash.VerifyHashAndSalt(account.Password, login.PasswordHash, login.PasswordSalt))
                    {
                        account.LoginId = login.Id;
                        account.EmailAddress = login.EmailAddress;

                        Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);
                        if (role == Roles.ApplicationUser)
                        {
                            var accounts = userRepository.GetUsersByLoginId(login.Id);
                            if (accounts.Count == 1)
                            {
                                var user = accounts[0];
                                account.UserId = user.Id;
                                account.AgencyId = user.AgencyId;

                                var principal = Get(user.Id, user.AgencyId);

                                if (principal != null)
                                {
                                    Thread.CurrentPrincipal = principal;
                                    HttpContext.Current.User = principal;

                                    login.LastLoginDate = DateTime.Now;
                                    if (loginRepository.Update(login))
                                    {
                                        result = true;
                                    }
                                }
                            }
                            else if (accounts.Count > 1)
                            {
                                account.HasMultiple = true;
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        public LoginAttemptType Validate(Account account)
        {
            var loginAttempt = LoginAttemptType.Failed;

            var login = loginRepository.Find(account.UserName);
            if (login != null)
            {
                if (login.IsLocked)
                {
                    loginAttempt = LoginAttemptType.Locked;
                }
                else
                {
                    if (login.IsActive)
                    {
                        var canContinue = true;
                        var principal = Get(login.EmailAddress);
                        if (AppSettings.IsSingleUserMode)
                        {
                            canContinue = authenticationAgent.Verify(principal.ToSingleUser());
                        }

                        if (canContinue)
                        {
                            var user = userRepository.Get(account.UserId, account.AgencyId);
                            if (user != null)
                            {
                                if (user.Status == (int)UserStatus.Active)
                                {
                                    if (user.AllowWeekendAccess())
                                    {
                                        var agency = agencyRepository.Get(user.AgencyId);
                                        if (agency != null)
                                        {
                                            if (!agency.IsSuspended)
                                            {
                                                if (!agency.HasTrialPeriodExpired())
                                                {
                                                    loginAttempt = LoginAttemptType.Success;
                                                }
                                                else
                                                {
                                                    loginAttempt = LoginAttemptType.TrialPeriodOver;
                                                }
                                            }
                                            else
                                            {
                                                loginAttempt = LoginAttemptType.AccountSuspended;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        loginAttempt = LoginAttemptType.WeekendAccessRestricted;
                                    }
                                }
                                else
                                {
                                    loginAttempt = LoginAttemptType.Deactivated;
                                }
                            }
                        }
                        else
                        {
                           loginAttempt = LoginAttemptType.AccountInUse;
                        }
                    }
                    else
                    {
                        loginAttempt = LoginAttemptType.Deactivated;
                    }
                }
            }

            return loginAttempt;
        }

        public ResetAttemptType Validate(string userName)
        {
            var resetAttempt = ResetAttemptType.Failed;

            var login = loginRepository.Find(userName);
            if (login != null)
            {
                if (login.IsLocked)
                {
                    resetAttempt = ResetAttemptType.Locked;
                }
                else
                {
                    if (login.IsActive)
                    {
                        var accounts = userRepository.GetUsersByLoginId(login.Id);
                        var activeAccount = accounts.Where(a => a.Status == (int)UserStatus.Active).FirstOrDefault();
                        if (activeAccount != null)
                        {
                            resetAttempt = ResetAttemptType.Success;
                        }
                        else
                        {
                            resetAttempt = ResetAttemptType.Deactivated;
                        }
                    }
                    else
                    {
                        resetAttempt = ResetAttemptType.Deactivated;
                    }
                }
            }

            return resetAttempt;
        }

        public bool Impersonate(Guid linkId)
        {
            var result = false;

            var link = supportRepository.GetImpersonationLink(linkId);
            if (link != null && link.IsUsed == false && DateTime.Now < link.Created.AddMinutes(1))
            {
                var login = loginRepository.Find(link.LoginId);
                var agency = agencyRepository.GetById(link.AgencyId);
                var user = userRepository.Get(link.UserId, link.AgencyId);

                if (login != null && user != null && agency != null)
                {
                    AxxessIdentity identity = new AxxessIdentity(login.Id, string.Format("{0}:{1}", login.EmailAddress, link.Id));

                    var permissionsArray = new List<string>();
                    Roles role = login.Role.ToEnum<Roles>(Roles.ApplicationUser);

                    if (role == Roles.ApplicationUser)
                    {
                        identity.IsImpersonated = true;
                        identity.Session =
                            new UserSession
                            {
                                UserId = user.Id,
                                LoginId = login.Id,
                                AgencyId = agency.Id,
                                AgencyName = agency.Name,
                                FullName = user.DisplayName,
                                DisplayName = login.DisplayName,
                                //MaxAgencyUserCount = agency.Package == 0 ? 100000000 : agency.Package,
                                AgencyRoles = user.Roles.IsNotNullOrEmpty() ? user.Roles : string.Empty,
                                Address = user.Profile != null ? user.Profile.AddressFull : string.Empty,
                                ImpersonatorName = link.RepName
                            };
                        permissionsArray = user.PermissionsArray;
                    }

                    var principal = new AxxessPrincipal(identity, role, permissionsArray);
                    Thread.CurrentPrincipal = principal;
                    HttpContext.Current.User = principal;
                    Cacher.Set<AxxessPrincipal>(string.Format("{0}:{1}", login.EmailAddress.IsNotNullOrEmpty()?login.EmailAddress.Trim():string.Empty , link.Id), principal);

                    link.IsUsed = true;
                    if (supportRepository.UpdateImpersonationLink(link))
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        public bool ResetPassword(string userName)
        {
            var login = loginRepository.Find(userName);
            if (login != null && login.IsAxxessAdmin == false && login.IsAxxessSupport == false)
            {
                var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}&type=password", login.Id)));
                var bodyText = MessageBuilder.PrepareTextFrom(
                    "PasswordResetInstructions",
                    "firstname", login.DisplayName,
                    "encryptedQueryString", encryptedQueryString);
                Notify.User(CoreSettings.NoReplyEmail, userName, "Reset Password - Axxess Home Health Management Software", bodyText);
                return true;
            }
            return false;
        }

        public bool UpdatePassword(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string newPasswordSalt = string.Empty;
                string newPasswordHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Password, out newPasswordHash, out newPasswordSalt);
                login.PasswordSalt = newPasswordSalt;
                login.PasswordHash = newPasswordHash;
                if (loginRepository.Update(login))
                {
                    return true;
                }
            }

            return false;
        }

        public bool ResetSignature(Guid loginId)
        {
            var login = loginRepository.Find(loginId);

            if (login != null)
            {
                var encryptedQueryString = string.Format("?enc={0}", Crypto.Encrypt(string.Format("loginid={0}&type=signature", login.Id)));
                var bodyText = MessageBuilder.PrepareTextFrom(
                    "SignatureResetInstructions",
                    "firstname", login.DisplayName,
                    "encryptedQueryString", encryptedQueryString);
                Notify.User(CoreSettings.NoReplyEmail, login.EmailAddress, "Reset Signature - Axxess Home Health Management Software", bodyText);
                return true;
            }

            return false;
        }

        public bool UpdateSignature(Account account)
        {
            var login = loginRepository.Find(account.LoginId);
            if (login != null)
            {
                var saltedHash = new SaltedHash();
                string newSignatureSalt = string.Empty;
                string newSignatureHash = string.Empty;

                saltedHash.GetHashAndSalt(account.Signature, out newSignatureHash, out newSignatureSalt);
                login.SignatureSalt = newSignatureSalt;
                login.SignatureHash = newSignatureHash;

                if (loginRepository.Update(login))
                {
                    return true;
                }
            }
            return false;
        }

        public bool Deactivate(Guid userId)
        {
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    login.IsActive = false;
                    if (loginRepository.Update(login))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool Activate(Account account)
        {
            var user = userRepository.Get(account.UserId, account.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    var saltedHash = new SaltedHash();
                    string passwordSalt = string.Empty;
                    string passwordHash = string.Empty;

                    saltedHash.GetHashAndSalt(account.Password, out passwordHash, out passwordSalt);
                    login.PasswordSalt = passwordSalt;
                    login.SignatureSalt = passwordSalt;
                    login.PasswordHash = passwordHash;
                    login.SignatureHash = passwordHash;
                    login.LastLoginDate = DateTime.Now;

                    if (loginRepository.Update(login))
                    {
                        AxxessPrincipal principal = Get(login.EmailAddress);
                        if (principal != null)
                        {
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public bool InitializeWith(Account account)
        {
            var user = userRepository.Get(account.UserId, account.AgencyId);
            if (user != null)
            {
                var login = loginRepository.Find(user.LoginId);
                if (login != null)
                {
                    account.LoginId = login.Id;
                    account.UserName = login.EmailAddress;
                    account.EmailAddress = login.EmailAddress;

                    AxxessPrincipal principal = Get(account.UserId, account.AgencyId);
                    if (principal != null)
                    {
                        Thread.CurrentPrincipal = principal;
                        HttpContext.Current.User = principal;
                    }

                    var cookie = HttpContext.Current.Request.Cookies[AppSettings.IdentifyAgencyCookie];
                    if (cookie != null)
                    {
                        cookie.Expires = DateTime.Now.AddDays(AppSettings.RememberMeForTheseDays);
                        cookie.Value = Crypto.Encrypt(string.Concat(account.UserId.ToString(), "_", user.AgencyId.ToString()));
                        HttpContext.Current.Response.Cookies.Set(cookie);
                    }
                    else
                    {
                        HttpCookie newCookie = new HttpCookie(AppSettings.IdentifyAgencyCookie);
                        newCookie.Expires = DateTime.Now.AddDays(AppSettings.RememberMeForTheseDays);
                        newCookie.Value = Crypto.Encrypt(string.Concat(account.UserId.ToString(), "_", user.AgencyId.ToString()));
                        HttpContext.Current.Response.Cookies.Add(newCookie);
                    }

                    login.LastLoginDate = DateTime.Now;
                    if (loginRepository.Update(login))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void LogOff(string emailAddress)
        {
            Cacher.Remove(emailAddress);
            var principal = Get(emailAddress);
            if (principal != null && !principal.IsImpersonated())
            {
                if (AppSettings.IsSingleUserMode)
                {
                    authenticationAgent.Logout(emailAddress);
                }
            }
        }

        public bool Switch(Guid loginId)
        {
            var result = false;
            var login = loginRepository.Find(loginId);
            if (login != null)
            {
                AxxessPrincipal principal = Get(login.EmailAddress);
                if (principal != null)
                {
                    authenticationAgent.Switch(loginId, principal.ToSingleUser());
                    result = true;
                }
            }

            return result;
        }

        #endregion
    }
}
