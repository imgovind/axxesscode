﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    using ViewData;
    using Services;
    using Extensions;
    using iTextExtension;
    using iTextExtension.XmlParsing;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;

    using Axxess.AgencyManagement.App.Workflows;

    using Telerik.Web.Mvc;
    using Axxess.OasisC.Extensions;
    using Axxess.LookUp.Domain;
    using Axxess.Log.Common;
    using Axxess.Log.Enums;
    using Axxess.AgencyManagement.App.Enums;
    using Axxess.AgencyManagement.App.Domain;
    using System.IO;

    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ScheduleController : BaseController
    {
        #region Constructor

        private readonly IDateService dateService;
        private readonly IPatientService patientService;
        private readonly IAssessmentService assessmentService;
        private readonly IUserService userService;
        private readonly IAgencyService agencyService;
        private readonly IReportService reportService;
        private readonly IUserRepository userRepository;
        private readonly IAssetRepository assetRepository;
        private readonly IAgencyRepository agencyRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IPhysicianRepository physicianRepository;

        public ScheduleController(IAgencyManagementDataProvider dataProvider, IPatientService patientService, IAssessmentService assessmentService, IUserService userService, IAgencyService agencyService, IReportService reportService)
        {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(patientService, "patientService");

            this.patientService = patientService;
            this.dateService = Container.Resolve<IDateService>();
            this.userRepository = dataProvider.UserRepository;
            this.assetRepository = dataProvider.AssetRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.physicianRepository = dataProvider.PhysicianRepository;
            this.assessmentService = assessmentService;
            this.userService = userService;
            this.agencyService = agencyService;
            this.reportService = reportService;
        }

        #endregion

        #region ScheduleController Actions

        #region Episode
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeNew(Guid? patientId)
        {
            var episodeViewData = new NewEpisodeData();
            if (patientId.HasValue)
            {
                var patient = patientRepository.Get((Guid)patientId, Current.AgencyId);
                if (patient != null)
                {
                    episodeViewData.PatientId = (Guid)patientId;
                    episodeViewData.DisplayName = patient.DisplayName;
                    episodeViewData.StartOfCareDate = patient.StartofCareDate;
                    episodeViewData.CaseManager = patient.CaseManagerId.ToString();
                    episodeViewData.PrimaryInsurance = patient.PrimaryInsurance;
                    episodeViewData.SecondaryInsurance = patient.SecondaryInsurance;
                    episodeViewData.AdmissionId = patient.AdmissionId;
                    if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                    {
                        var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                        if (primaryPhysician == null) primaryPhysician = patient.PhysicianContacts.First();
                        episodeViewData.PrimaryPhysician = primaryPhysician.Id.ToString();
                    }
                    var episode = patientRepository.GetLastEpisode(Current.AgencyId, (Guid)patientId);
                    if (episode != null)
                    {
                        episodeViewData.EndDate = episode.EndDate;
                        episodeViewData.StartDate = episode.StartDate;
                    }
                    else
                    {
                        episodeViewData.EndDate = DateTime.MinValue.Date;
                        episodeViewData.StartDate = DateTime.MinValue.Date;
                    }
                    var selection = new List<SelectListItem>();
                    var admissiondates = patientRepository.GetPatientAdmissionDates(Current.AgencyId, (Guid)patientId);
                    if (admissiondates != null && admissiondates.Count > 0) selection = admissiondates.Select(a => new SelectListItem { Text = a.StartOfCareDate.ToString("MM/dd/yyy"), Value = a.Id.ToString(), Selected = a.Id == episodeViewData.AdmissionId }).ToList();
                    else
                    {
                        var admission = patientService.GetIfExitOrCreate((Guid)patientId);
                        if (admission != null) selection.Add(new SelectListItem { Text = admission.StartOfCareDate.ToString("MM/dd/yyy"), Value = admission.Id.ToString(), Selected = true });
                    }
                    episodeViewData.AdmissionDates = selection;
                }
            }
            return PartialView("Episode/New", episodeViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeNewContent(Guid patientId)
        {
            var episodeViewData = new NewEpisodeData();
            if (!patientId.IsEmpty())
            {
                var episode = patientRepository.GetLastEpisode(Current.AgencyId, patientId);
                if (episode != null)
                {
                    episodeViewData = episode;
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null) episodeViewData.PrimaryPhysician = physician.Id.ToString();
                }
                else
                {
                    var patient = patientRepository.Get(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        episodeViewData.PatientId = patientId;
                        episodeViewData.DisplayName = patient.DisplayName;
                        episodeViewData.StartOfCareDate = patient.StartofCareDate;
                        episodeViewData.CaseManager = patient.CaseManagerId.ToString();
                        episodeViewData.PrimaryInsurance = patient.PrimaryInsurance;
                        episodeViewData.SecondaryInsurance = patient.SecondaryInsurance;
                        episodeViewData.AdmissionId = patient.AdmissionId;
                        if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        {
                            var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                            if (primaryPhysician == null) primaryPhysician = patient.PhysicianContacts.First();
                            episodeViewData.PrimaryPhysician = primaryPhysician.Id.ToString();
                        }
                    }
                }
                var selection = new List<SelectListItem>();
                var admissiondates = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
                if (admissiondates != null && admissiondates.Count > 0) selection = admissiondates.Select(a => new SelectListItem { Text = a.StartOfCareDate.ToString("MM/dd/yyy"), Value = a.Id.ToString(), Selected = a.Id == episodeViewData.AdmissionId }).ToList();
                episodeViewData.AdmissionDates = selection;
            }
            return PartialView("Episode/Content", episodeViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EpisodeCreate(PatientEpisode patientEpisode, EpisodeDetail detail)
        {
            var viewData = new JsonViewData() { isSuccessful = false, errorMessage = "Episode could not be saved" };
            if (patientEpisode != null && detail != null)
            {
                patientEpisode.Detail = detail;
                var patient = patientRepository.Get(patientEpisode.PatientId, Current.AgencyId);
                var validationRules = new List<Validation>();
                validationRules.Add(new Validation(() => !(patient != null), "Patient data is not available."));
                if (patient != null)
                {
                    var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientEpisode.AdmissionId);
                    validationRules.Add(new Validation(() => !(admissionData != null), "Admission data is not available."));
                }
                validationRules.Add(new Validation(() => detail.PrimaryInsurance.IsEqual("0"), "Insurance is required."));
                validationRules.Add(new Validation(() => !patientEpisode.StartDate.IsValid(), "Episode start date is not valid date."));
                validationRules.Add(new Validation(() => !patientEpisode.EndDate.IsValid(), "Episode end date is not valid date."));
                if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
                {
                    validationRules.Add(new Validation(() => !(patientEpisode.StartDate < patientEpisode.EndDate), "Episode start date must be less than episode end date."));
                    validationRules.Add(new Validation(() => !(patientEpisode.EndDate.Subtract(patientEpisode.StartDate).Days <= 60), "Episode period can't be more than 60 days."));
                }
                validationRules.Add(new Validation(() => !patientService.IsValidEpisode(patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate), "Episode date is not in the valid date range."));
                if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
                {
                    validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate <= patientEpisode.StartDate), "Episode start date must be greater than start of care date."));
                    validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate < patientEpisode.EndDate), "Episode end date must be   greater than start of care date."));
                }
                var entityValidator = new EntityValidator(validationRules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid && patient != null)
                {
                    var workflow = new CreateEpisodeWorkflow(patient, patientEpisode);
                    if (workflow.IsCommitted)
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Episode was created successfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = workflow.Message;
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeEdit(Guid id, Guid patientId)
        {
            PatientEpisode patientEpisode;
            if (patientId.IsEmpty() || id.IsEmpty())
            {
                var episodeViewData = new PatientEpisode();
                var selection = new List<SelectListItem>();
                episodeViewData.AdmissionDates = selection;
                return PartialView("Episode/Edit", episodeViewData);
            }
            else
            {
                patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, id, patientId);
                if (patientEpisode != null)
                {
                    var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                    if (patient != null) patientEpisode.DisplayName = patient.DisplayName;
                    patientEpisode.Detail = patientEpisode.Details.ToObject<EpisodeDetail>();
                }
                var selection = new List<SelectListItem>();
                var admissiondates = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
                if (admissiondates != null && admissiondates.Count > 0) selection = admissiondates.Select(a => new SelectListItem { Text = a.StartOfCareDate.ToString("MM/dd/yyy"), Value = a.Id.ToString(), Selected = patientEpisode.AdmissionId.IsEmpty() ? patientEpisode.StartOfCareDate.Date == a.StartOfCareDate.Date : a.Id == patientEpisode.AdmissionId }).ToList();
                patientEpisode.AdmissionDates = selection;
            }
            return PartialView("Episode/Edit", patientEpisode);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeUpdate([Bind] PatientEpisode patientEpisode)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Episode could not be saved." };
            var patient = patientRepository.Get(patientEpisode.PatientId, Current.AgencyId);
            var validationRules = new List<Validation>();
            validationRules.Add(new Validation(() => !(patient != null), "Patient data is not available."));
            if (patient != null)
            {
                var admissionData = patientRepository.GetPatientAdmissionDate(Current.AgencyId, patientEpisode.AdmissionId);
                validationRules.Add(new Validation(() => !(admissionData != null), "Admission data is not available."));
            }
            validationRules.Add(new Validation(() => !patientEpisode.StartDate.IsValid(), "Episode start date is not valid date."));
            validationRules.Add(new Validation(() => !patientEpisode.EndDate.IsValid(), "Episode end date is not valid date."));
            validationRules.Add(new Validation(() => patientEpisode.Detail.PrimaryInsurance.IsEqual("0"), "Insurance is required."));
            if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
            {
                validationRules.Add(new Validation(() => !(patientEpisode.StartDate < patientEpisode.EndDate), "Episode start date must be less than episode end date."));
                validationRules.Add(new Validation(() => !(patientEpisode.EndDate.Subtract(patientEpisode.StartDate).Days <= 60), "Episode period can't be more than 60 days."));
            }
            if (!patientEpisode.IsActive) validationRules.Add(new Validation(() => !patientService.IsValidEpisode(patientEpisode.Id, patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate), "Episode date is not in the valid date range."));
            if (patientEpisode.StartDate.IsValid() && patientEpisode.EndDate.IsValid())
            {
                validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate <= patientEpisode.StartDate), "Episode start date must be greater than start of care date."));
                validationRules.Add(new Validation(() => !(patientEpisode.StartOfCareDate < patientEpisode.EndDate), "Episode end date must be   greater than start of care date."));
            }
            var entityValidator = new EntityValidator(validationRules.ToArray());
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                patientEpisode.IsActive = !patientEpisode.IsActive;
                if (patientService.UpdateEpisode(patientEpisode))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Episode has been successfully updated.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = entityValidator.Message;
            }
            return Json(viewData);
        }

        public ActionResult NewEpisodeContent(Guid patientId)
        {
            var episodeViewData = new NewEpisodeData();
            if (!patientId.IsEmpty())
            {
                var episode = patientRepository.GetLastEpisode(Current.AgencyId, patientId);
                if (episode != null)
                {
                    episodeViewData = episode;
                    var physician = physicianRepository.GetPatientPrimaryPhysician(Current.AgencyId, patientId);
                    if (physician != null) episodeViewData.PrimaryPhysician = physician.Id.ToString();
                }
                else
                {
                    var patient = patientRepository.Get(patientId, Current.AgencyId);
                    if (patient != null)
                    {
                        episodeViewData.PatientId = patientId;
                        episodeViewData.DisplayName = patient.DisplayName;
                        episodeViewData.StartOfCareDate = patient.StartofCareDate;
                        episodeViewData.CaseManager = patient.CaseManagerId.ToString();
                        episodeViewData.PrimaryInsurance = patient.PrimaryInsurance;
                        episodeViewData.SecondaryInsurance = patient.SecondaryInsurance;
                        episodeViewData.AdmissionId = patient.AdmissionId;
                        if (patient.PhysicianContacts != null && patient.PhysicianContacts.Count > 0)
                        {
                            var primaryPhysician = patient.PhysicianContacts.FirstOrDefault(p => p.Primary == true);
                            if (primaryPhysician == null) primaryPhysician = patient.PhysicianContacts.First();
                            episodeViewData.PrimaryPhysician = primaryPhysician.Id.ToString();
                        }
                    }
                }
                var selection = new List<SelectListItem>();
                var admissiondates = patientRepository.GetPatientAdmissionDates(Current.AgencyId, patientId);
                if (admissiondates != null && admissiondates.Count > 0) selection = admissiondates.Select(a => new SelectListItem { Text = a.StartOfCareDate.ToString("MM/dd/yyy"), Value = a.Id.ToString(), Selected = a.Id == episodeViewData.AdmissionId }).ToList();
                episodeViewData.AdmissionDates = selection;
            }
            return PartialView("Episode/Content", episodeViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeGet(Guid patientId, Guid episodeId, string discipline)
        {
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotNull(discipline, "discipline");
            return Json(patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId, discipline));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeRangeList(Guid patientId)
        {
            var viewData = new List<EpisodeDateViewData>();
            if (!patientId.IsEmpty())
            {
                var episodes = patientRepository.GetPatientActiveEpisodes(Current.AgencyId, patientId).OrderBy(e => e.StartDate).ToList();
                if (episodes != null && episodes.Count > 0) viewData = episodes.Select(e => new EpisodeDateViewData { Id = e.Id, Range = string.Format("{0}-{1}", e.StartDate.ToString("MM/dd/yyyy"), e.EndDate.ToString("MM/dd/yyyy")) }).ToList();
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeInactiveList(Guid patientId)
        {
            return PartialView("Episode/InactiveList", patientRepository.GetPatientDeactivatedAndDischargedEpisodes(Current.AgencyId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeInactiveGrid(Guid patientId)
        {
            return PartialView("Episode/InactiveGrid", patientRepository.GetPatientDeactivatedAndDischargedEpisodes(Current.AgencyId, patientId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeFrequencyList(Guid id, Guid patientId)
        {
            FrequenciesViewData viewData = new FrequenciesViewData();
            var episode = patientService.GetPatientEpisodeWithFrequencyForMasterCalendar(id, patientId);
            var frequencies = episode.Detail.FrequencyList.ParseFrequency();
            foreach (var pair in frequencies) viewData.Visits.Add(pair.Key, new VisitData(pair.Value));
            return PartialView("Episode/FrequencyList", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeActivate(Guid id, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Episode could not be activated. Try again." };
            var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, id, patientId);
            if (patientEpisode != null)
            {
                var validationRules = new List<Validation>();
                validationRules.Add(new Validation(() => !patientService.IsValidEpisode(patientEpisode.Id, patientEpisode.PatientId, patientEpisode.StartDate, patientEpisode.EndDate), "Episode date is not in the valid date range."));
                var entityValidator = new EntityValidator(validationRules.ToArray());
                entityValidator.Validate();
                if (entityValidator.IsValid)
                {
                    patientEpisode.IsActive = true;
                    patientEpisode.IsDischarged = false;
                    patientEpisode.Modified = DateTime.Now;
                    if (patientRepository.UpdateEpisode(patientEpisode))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Patient, patientEpisode.PatientId, patientEpisode.Id.ToString(), LogType.Episode, LogAction.EpisodeActivated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Episode has been successfully activated.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = entityValidator.Message;
                }
            }
            return Json(viewData);
        }

        #endregion

        #region Schedule Center
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Center()
        {
            var viewData = new ScheduleViewData();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA) viewData.Count = patientRepository.GetPatientStatusCount(Current.AgencyId, (int)PatientStatus.Active);
            else if (Current.IsClinicianOrHHA) viewData.Count = patientRepository.GetUserPatients(Current.AgencyId, Current.UserId, (byte)PatientStatus.Active).Count;
            else viewData.Count = 0;
            return PartialView("Center/Layout", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterContent(Guid patientId, Guid? episodeId)
        {
            var viewData = new ScheduleViewData();
            viewData.PatientId = patientId;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                if (episodeId.HasValue) viewData.Episode = patientRepository.GetEpisode(Current.AgencyId, (Guid)episodeId, patientId, "all");
                else viewData.Episode = patientRepository.GetEpisodeNew(Current.AgencyId, patientId, DateTime.Now, "all");
                if (viewData.Episode != null) viewData.Episode.DisplayName = patient.DisplayName;
                if (viewData.Episode != null) viewData.Episode.DisplayNameWithMi = patient.DisplayNameWithMi;
                viewData.IsDischarged = patient.IsDischarged;
            }
            return PartialView("Center/Content", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterCalendar(Guid patientId, Guid? episodeId, string discipline)
        {
            var viewData = new ScheduleViewData();
            viewData.PatientId = patientId;
            var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
            if (patient != null)
            {
                if (episodeId.HasValue) viewData.Episode = patientRepository.GetEpisode(Current.AgencyId, (Guid)episodeId, patientId, discipline);
                else viewData.Episode = patientRepository.GetEpisodeNew(Current.AgencyId, patientId, DateTime.Now, discipline);
                if (viewData.Episode != null) viewData.Episode.DisplayName = patient.DisplayName;
                if (viewData.Episode != null) viewData.Episode.DisplayNameWithMi = patient.DisplayNameWithMi;
                viewData.IsDischarged = patient.IsDischarged;
            }
            return PartialView("Center/Calendar", new CalendarViewData { Episode = viewData.Episode, PatientId = patientId, IsDischarged = viewData.IsDischarged });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterActivities(Guid patientId, Guid episodeId, string discipline)
        {
            return PartialView("Center/Activities", new ScheduleActivityArgument { EpisodeId = episodeId, PatientId = patientId, Discpline = discipline });
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterActivitiesGrid(Guid episodeId, Guid patientId, string discipline)
        {
            return View(new GridModel(patientService.GetScheduledEventsNew(episodeId, patientId, discipline)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CenterPatientGrid(Guid branchId, byte statusId, byte paymentSourceId)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA) patientList = patientRepository.GetPatientSelection(Current.AgencyId, branchId, statusId, paymentSourceId);
            else if (Current.IsClinicianOrHHA) patientList = patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId, paymentSourceId);
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }
        #endregion

        #region Master Calendar
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MasterCalendar(Guid episodeId, Guid patientId)
        {
            var patientEpisode = patientService.GetPatientEpisodeWithFrequencyForMasterCalendar(episodeId, patientId);
            return PartialView("MasterCalendar/Main", patientEpisode);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Calendar(Guid patientId, string discipline)
        {
            CalendarViewData calendarViewData = null;
            if (!patientId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    calendarViewData = new CalendarViewData();
                    var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline);
                    if (episode != null)
                    {
                        calendarViewData.Episode = episode;
                        calendarViewData.PatientId = patientId;
                    }
                    else
                    {
                        calendarViewData.PatientId = patientId;
                    }
                    calendarViewData.IsDischarged = patient.IsDischarged;
                }
            }
            return PartialView(calendarViewData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MasterCalendarContent(Guid episodeId, Guid patientId)
        {
            var patientEpisode = patientService.GetPatientEpisodeWithFrequencyForMasterCalendar(episodeId, patientId);
            return PartialView("MasterCalendar/Content", patientEpisode);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActivityFirstTime(Guid patientId, string discipline)
        {
            var episode = patientRepository.GetEpisode(Current.AgencyId, patientId, DateTime.Now, discipline);
            if (patientId.IsEmpty() || episode == null)
            {
                return PartialView("Activities", new ScheduleActivityArgument { EpisodeId = Guid.Empty, PatientId = Guid.Empty, Discpline = "" });
            }
            return PartialView("Activities", new ScheduleActivityArgument { EpisodeId = episode.Id, PatientId = patientId, Discpline = discipline });
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activity(Guid episodeId, Guid patientId, string discipline)
        {
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotNull(discipline, "discipline");
            // var patientActivities = patientService.GetScheduledEvents(episodeId, patientId, discipline);
            var patientActivities = patientService.GetScheduledEventsNew(episodeId, patientId, discipline);
            return View(new GridModel(patientActivities));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MasterCalendarPrint(Guid episodeId, Guid patientId)
        {
            return FileGenerator.Pdf<MasterCalendarPdf>(new MasterCalendarPdf(patientService.GetPatientEpisodeWithFrequencyForMasterCalendar(episodeId, patientId)), "MasterCalendar");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MasterCalendarMain(Guid patientId, Guid episodeId)
        {
            return PartialView("MasterCalendar", patientService.GetPatientEpisodeWithFrequencyForMasterCalendar(episodeId, patientId));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult MasterCalendar(Guid patientId, Guid episodeId)
        //{
        //    return PartialView("MasterCalendar", patientService.GetPatientEpisodeWithFrequencyForMasterCalendar(episodeId, patientId));
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MasterCalendarPdf(Guid patientId, Guid episodeId)
        {
            return FileGenerator.Pdf<MasterCalendarPdf>(new MasterCalendarPdf(patientService.GetPatientEpisodeWithFrequencyForMasterCalendar(episodeId, patientId)), "MasterCalendar");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CalendarNav(Guid patientId, Guid episodeId, string discipline)
        {
            var calendarViewData = new CalendarViewData();
            if (!patientId.IsEmpty() && !episodeId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    var episode = patientRepository.GetEpisodeNew(Current.AgencyId, episodeId, patientId, discipline);
                    if (episode != null)
                    {
                        episode.DisplayName = patient.DisplayName;
                        calendarViewData.Episode = episode;
                        calendarViewData.PatientId = patientId;
                    }
                    else
                    {
                        calendarViewData.PatientId = patientId;
                    }
                    calendarViewData.IsDischarged = patient.IsDischarged;
                }
            }
            return PartialView("Calendar", calendarViewData);
        }


        #endregion

        #region Schedule Detail
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DetailsEdit(Guid? eventId, Guid? episodeId, Guid? patientId)
        {
            if (patientId.HasValue && episodeId.HasValue && eventId.HasValue) return PartialView("Detail/Edit", patientService.GetScheduledEvent((Guid)episodeId, (Guid)patientId, (Guid)eventId));
            else return PartialView("Detail/Edit", new ScheduleEvent());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DetailsUpdate([Bind] ScheduleEvent scheduleEvent)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task details could not be saved." };
            var validationRules = new List<Validation>();
            if (scheduleEvent.IsEpisodeReassiged && scheduleEvent.EpisodeId != scheduleEvent.NewEpisodeId)
            {
                var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.NewEpisodeId, scheduleEvent.PatientId);
                if (patientEpisode != null)
                {
                    var oldEvents = patientRepository.GetPatientScheduledEventsOnlyNew(Current.AgencyId, patientEpisode.Id, patientEpisode.PatientId); //(patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).Where(s => s.EventDate.IsValidDate() && !s.IsDeprecated).OrderByDescending(o => o.EventDate.ToDateTime().Date).ToList();
                    if (oldEvents != null && oldEvents.Count > 0)
                    {
                        var recertTasks = DisciplineTaskFactory.RecertDisciplineTasks();
                        var transferTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks();
                        var rocTasks = DisciplineTaskFactory.ROCDisciplineTasks();
                        var socTasks = DisciplineTaskFactory.SOCDisciplineTasks();
                        var dischargeTasks = DisciplineTaskFactory.DischargeOASISDisciplineTasks();
                        var evnt = scheduleEvent;
                        if (recertTasks.Contains(evnt.DisciplineTask))
                        {
                            var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date < evnt.EventDate.Date));
                            ScheduleEvent roc = null;
                            if (transfer != null)
                            {
                                roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date > transfer.EventDate.Date));
                            }
                            if (transfer != null && roc == null)
                            {
                                viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                return PartialView("JsonResult", viewData);
                            }
                            else if (transfer != null && roc != null && roc.EventDate.Date <= transfer.EventDate.Date)
                            {
                                viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                return PartialView("JsonResult", viewData);
                            }
                            else if (oldEvents.Exists(oe => recertTasks.Contains(evnt.DisciplineTask)))
                            {
                                viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
                                return PartialView("JsonResult", viewData);
                            }
                            else if (evnt.EventDate.Date < patientEpisode.EndDate.AddDays(-5).Date || evnt.EventDate.Date > patientEpisode.EndDate.Date)
                            {
                                viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
                                return PartialView("JsonResult", viewData);
                            }
                        }
                        else if (socTasks.Contains(evnt.DisciplineTask))
                        {
                            if (oldEvents.Exists(oe => socTasks.Contains(oe.DisciplineTask)))
                            {
                                viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
                                return PartialView("JsonResult", viewData);
                            }
                            else if (oldEvents.Exists(oe => dischargeTasks.Contains(oe.DisciplineTask)))
                            {
                            }
                        }
                        else if (rocTasks.Contains(evnt.DisciplineTask))
                        {
                            var roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask));
                            var transfer = oldEvents.Find(oe => transferTasks.Contains(oe.DisciplineTask));
                            if (roc == null)
                            {
                                if (transfer == null)
                                {
                                    viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                    return PartialView("JsonResult", viewData);
                                }
                                else if (transfer != null && (transfer.EventDate > evnt.EventDate))
                                {
                                    viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
                                    return PartialView("JsonResult", viewData);
                                }
                            }
                            else if (roc != null)
                            {
                                if (transfer != null && (roc.EventDate.Date > transfer.EventDate.Date))
                                {
                                    viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                    return PartialView("JsonResult", viewData);
                                }
                            }
                        }
                        else if (transferTasks.Contains(evnt.DisciplineTask))
                        {
                            var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask));
                            ScheduleEvent roc = null;
                            if (transfer != null)
                            {
                                roc = oldEvents.Find(oe => (rocTasks.Contains(oe.DisciplineTask)) && (oe.EventDate.Date > transfer.EventDate.Date));
                            }
                            if (transfer != null && roc == null)
                            {
                                viewData.errorMessage = "Please Create a Resumption of Care before creating another Transfer.";
                                return PartialView("JsonResult", viewData);
                            }
                        }
                    }
                    validationRules.Add(new Validation(() => scheduleEvent.EventDate <= DateTime.MinValue, "Schedule date is required."));
                    validationRules.Add(new Validation(() => !scheduleEvent.EventDate.IsValid(), "Schedule date is not valid."));
                    validationRules.Add(new Validation(() => scheduleEvent.EventDate.IsValid() ? !(scheduleEvent.EventDate >= patientEpisode.StartDate.Date && scheduleEvent.EventDate.Date <= patientEpisode.EndDate.Date) : true, "Schedule date is not in the episode range."));
                    validationRules.Add(new Validation(() => scheduleEvent.VisitDate <= DateTime.MinValue, "Visit date is required."));
                    validationRules.Add(new Validation(() => !scheduleEvent.VisitDate.IsValid(), "Visit date is not valid."));
                    validationRules.Add(new Validation(() => scheduleEvent.VisitDate.IsValid() ? !(scheduleEvent.VisitDate >= patientEpisode.StartDate.Date && scheduleEvent.VisitDate.Date <= patientEpisode.EndDate.Date) : true, "Visit date is not in the episode range."));
                    var entityValidator = new EntityValidator(validationRules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.UpdateScheduleEventDetail(scheduleEvent, Request.Files))
                        {
                            viewData.isSuccessful = true;
                        }
                    }
                    else viewData.errorMessage = entityValidator.Message;
                }
            }
            else
            {
                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, scheduleEvent.EpisodeId, scheduleEvent.PatientId);
                if (episode != null)
                {
                    validationRules.Add(new Validation(() => scheduleEvent.EventDate <= DateTime.MinValue, "Schedule date is required."));
                    validationRules.Add(new Validation(() => !scheduleEvent.EventDate.IsValid(), "Schedule date is not valid."));
                    validationRules.Add(new Validation(() => scheduleEvent.EventDate.IsValid() ? !(scheduleEvent.EventDate >= episode.StartDate && scheduleEvent.EventDate <= episode.EndDate) : true, "Schedule date is not in the episode range."));
                    validationRules.Add(new Validation(() => scheduleEvent.VisitDate <= DateTime.MinValue, "Visit date is required."));
                    validationRules.Add(new Validation(() => !scheduleEvent.VisitDate.IsValid(), "Visit date is not valid."));
                    validationRules.Add(new Validation(() => scheduleEvent.VisitDate.IsValid() ? !(scheduleEvent.VisitDate >= episode.StartDate && scheduleEvent.VisitDate <= episode.EndDate) : true, "Visit date is not in the episode range."));
                    var entityValidator = new EntityValidator(validationRules.ToArray());
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        if (patientService.UpdateScheduleEvent(scheduleEvent, Request.Files))
                        {
                            viewData.isSuccessful = true;
                        }
                    }
                    else viewData.errorMessage = entityValidator.Message;
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Note Supply
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteSupplyWorksheet(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("Nursing/NotesSupplyWorkSheet", patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteSupplyGrid(Guid episodeId, Guid patientId, Guid eventId)
        {
            return View(new GridModel(patientService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteSupplyCreate(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");
            patientService.AddNoteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(patientService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteSupplyEdit(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");
            patientService.UpdateNoteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(patientService.GetNoteSupply(episodeId, patientId, eventId)));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteSupplyDelete(Guid episodeId, Guid patientId, Guid eventId, Supply supply)
        {
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotNull(supply, "supply");

            patientService.DeleteNoteSupply(episodeId, patientId, eventId, supply);
            return View(new GridModel(patientService.GetNoteSupply(episodeId, patientId, eventId)));
        }
        #endregion

        #region MissedVisit
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisitNew(Guid episodeId, Guid patientId, Guid eventId)
        {
            return PartialView("MissedVisit/New", patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MissedVisitCreate([Bind] MissedVisit missedVisit)
        {
            Check.Argument.IsNotNull(missedVisit, "missedVisit");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Missed Visit could not be saved." };
            if (!missedVisit.Id.IsEmpty() && !missedVisit.PatientId.IsEmpty() && !missedVisit.EpisodeId.IsEmpty())
            {
                var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, missedVisit.EpisodeId, missedVisit.PatientId);
                if (episode != null)
                {
                    var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, missedVisit.PatientId, missedVisit.EpisodeId, missedVisit.Id);
                    if (scheduleEvent != null)
                    {
                        if (scheduleEvent.EventDate.IsValid() && scheduleEvent.EventDate.Date <= DateTime.Now.Date)
                        {
                            var validationRules = new List<Validation>();
                            validationRules.Add(new Validation(() => !missedVisit.SignatureDate.IsValid(), "Signature date is not valid date."));
                            validationRules.Add(new Validation(() => string.IsNullOrEmpty(missedVisit.Signature), "User Signature can't be empty."));
                            validationRules.Add(new Validation(() => missedVisit.Signature.IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, missedVisit.Signature) : false, "User Signature is not correct."));

                            if (missedVisit.SignatureDate.IsValid())
                            {
                                validationRules.Add(new Validation(() => (missedVisit.SignatureDate < episode.StartDate), "Missed visit date must be greater or equal to the episode start date."));
                                validationRules.Add(new Validation(() => (missedVisit.SignatureDate > episode.EndDate), "Missed visit date must be must be  less than or equalt to the episode end date."));
                            }

                            var entityValidator = new EntityValidator(validationRules.ToArray());
                            entityValidator.Validate();
                            if (entityValidator.IsValid)
                            {
                                missedVisit.SignatureText = string.Format("Electronically Signed by: {0}", Current.UserFullName);
                                if (patientService.AddMissedVisit(missedVisit))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "Missed visit Successfully saved.";
                                }
                                else
                                {
                                    viewData.isSuccessful = false;
                                }
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = entityValidator.Message;
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "The schedule event don't to be future date . Try again.";
                        }
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The event isn't found. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The Episode isn't found. Try again.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MissedVisitPrintPreview(Guid patientId, Guid eventId)
        {
            return View("MissedVisit/PrintPreview", patientService.GetMissedVisitPrint(patientId, eventId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult MissedVisitPrint(Guid? patientId, Guid? eventId)
        {
            if (patientId.HasValue && eventId.HasValue)
            {
                return FileGenerator.Pdf<MissedVisitPdf>(new MissedVisitPdf(patientService.GetMissedVisitPrint((Guid)patientId, (Guid)eventId)), "MissedVisit");
            }
            else
            {
                return FileGenerator.Pdf<MissedVisitPdf>(new MissedVisitPdf(patientService.GetMissedVisitPrint()), "MissedVisit");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MissedVisitView(Guid id)
        {
            return PartialView("MissedVisit/View", patientRepository.GetMissedVisit(Current.AgencyId, id));
        }
        #endregion

        #region Note View, Content, Print

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteView(Guid episodeId, Guid patientId, Guid eventId)
        {
            var noteViewData = new VisitNoteViewData();
            var scheduledEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, patientId, episodeId, eventId);
            if (scheduledEvent != null)
            {
                var argument = GetArgumentNoteWithView(scheduledEvent,false);
                noteViewData = patientService.GetVisitNote(scheduledEvent, argument) ?? new VisitNoteViewData();
                noteViewData.Version = scheduledEvent.Version;
                return PartialView(argument.ContentPath, noteViewData);
            }
            return PartialView("ContentNotFound", "The Note you are looking for is not Found. Close this window and try again.");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NoteContent(Guid patientId, Guid noteId, Guid previousNoteId, string type)
        {
            var patientVisitNote = patientService.GetVisitNoteForContent(patientId, noteId, previousNoteId, type) ?? new VisitNoteViewData();
            if (patientVisitNote != null)
            {
                patientVisitNote.Version = patientVisitNote.Version > 0 ? patientVisitNote.Version : 1;
                var contentPath = GetViewForContent(patientVisitNote);
                return PartialView(contentPath, patientVisitNote);
            }
            return PartialView("ContentNotFound", "The Previous Note  you are looking for to load is not found. Close this window and try again.");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NotePrintPreview(Guid? episodeId, Guid? patientId, Guid? eventId)
        {
            var noteViewData = new VisitNoteViewData();
            if (episodeId.HasValue && patientId.HasValue && eventId.HasValue)
            {
                var scheduledEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, (Guid)patientId, (Guid)episodeId, (Guid)eventId);
                if (scheduledEvent != null)
                {
                    var argument = GetArgumentNoteWithView(scheduledEvent, true);
                    noteViewData = patientService.GetVisitNotePrint(scheduledEvent, argument) ?? new VisitNoteViewData();
                    noteViewData.Version = scheduledEvent.Version;
                    if (noteViewData != null && argument.IsJsonSerialize)
                    {
                        var xml = new VisitNoteXml(noteViewData, argument.pdfDoc);
                        noteViewData.PrintViewJson = xml.GetJson();
                    }
                    return View(argument.ContentPath, noteViewData);
                }
            }
            return View("MSW/MSWProgressNote", new VisitNoteViewData());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult NotePdf(Guid? episodeId, Guid? patientId, Guid? eventId)
        {
            FileResult file = new FileStreamResult(new MemoryStream(), "application/pdf");
            var noteViewData = new VisitNoteViewData();
            if (episodeId.HasValue && patientId.HasValue && eventId.HasValue)
            {
                var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, (Guid)patientId, (Guid)episodeId, (Guid)eventId);
                if (scheduleEvent != null && scheduleEvent.DisciplineTask > 0)
                {
                    var argument = GetArgumentNoteWithView(scheduleEvent, true);
                    noteViewData = patientService.GetVisitNotePrint(scheduleEvent, argument) ?? new VisitNoteViewData();
                    return GetPrintFile(noteViewData, scheduleEvent.DisciplineTask, file);
                }
            }
            return GetPrintFile(noteViewData, 0, file);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult NotePrint(int Type)
        {
            var noteViewData = patientService.GetVisitNotePrint(Type) ?? new VisitNoteViewData();
            return GetPrintFile(noteViewData, Type, new FileStreamResult(new MemoryStream(), "application/pdf"));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult NoteBlank(int Type)
        {
            var noteViewData = patientService.GetVisitNotePrint(Type) ?? new VisitNoteViewData();
            return GetPrintFile(noteViewData, Type, new FileStreamResult(new MemoryStream(), "application/pdf"));
        }

        #endregion

        #region WoundCare
        [AcceptVerbs(HttpVerbs.Get)]
        public FileResult WoundCarePrint()
        {
            return FileGenerator.Pdf<WoundCarePdf>(new WoundCarePdf(patientService.GetVisitNotePrint()), "WoundCare");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundCare(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new VisitNoteViewData();
            var patientvisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
            if (patientvisitNote != null)
            {
                viewData.Questions = patientvisitNote.ToWoundCareDictionary();
                var episode = patientRepository.GetEpisodeById(Current.AgencyId, episodeId, patientId);
                if (episode != null)
                {
                    viewData.EndDate = episode.EndDate;
                    viewData.StartDate = episode.StartDate;
                }
                viewData.PatientId = patientvisitNote.PatientId;
                viewData.EpisodeId = patientvisitNote.EpisodeId;
                viewData.EventId = patientvisitNote.Id;
                viewData.Version = patientvisitNote.Version;
            }
            else
            {
                viewData.PatientId = patientId;
                viewData.EpisodeId = episodeId;
                viewData.EventId = eventId;
                viewData.Questions = new Dictionary<string, NotesQuestion>();
            }
            return PartialView("Nursing/WoundCare", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WoundCareSave(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Wound care could not be saved" };
            if (patientService.SaveWoundCare(formCollection, Request.Files))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your wound care flowsheet has been saved successfully";
            }
            return View("JsonResult", viewData);
        }

        #endregion

        #region Helpers
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task could not be saved. Please try again." };
            var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, formCollection["episodeId"].ToGuid(), formCollection["patientId"].ToGuid());
            if (patientEpisode != null)
            {
                var oldEvents = patientRepository.GetPatientScheduledEventsOnlyNew(Current.AgencyId, patientEpisode.Id, patientEpisode.PatientId); // (patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).Where(s => !s.IsDeprecated).OrderByDescending(o => o.EventDate.ToDateTime()).ToList();
                var newEvents = JsonExtensions.FromJson<List<ScheduleEvent>>(formCollection["Patient_Schedule"]).OrderBy(e => e.EventDate.Date).ToList();
                if ((oldEvents != null && oldEvents.Count > 0) && (newEvents != null && newEvents.Count > 0))
                {
                    var recertTasks = DisciplineTaskFactory.RecertDisciplineTasks();
                    var transferTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks();
                    var rocTasks = DisciplineTaskFactory.ROCDisciplineTasks();
                    var socTasks = DisciplineTaskFactory.SOCDisciplineTasks();
                    var dischargeTasks = DisciplineTaskFactory.DischargeOASISDisciplineTasks();

                    foreach (var evnt in newEvents)
                    {
                        if (recertTasks.Contains(evnt.DisciplineTask))
                        {
                            var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date < evnt.EventDate.Date));
                            ScheduleEvent roc = null;
                            if (transfer != null)
                            {
                                roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date > transfer.EventDate.Date));
                            }
                            if (transfer != null && roc == null)
                            {
                                viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                return Json(viewData);
                            }
                            else if (transfer != null && roc != null && roc.EventDate.Date <= transfer.EventDate.Date)
                            {
                                viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                return Json(viewData);
                            }
                            else if (oldEvents.Exists(oe => recertTasks.Contains(oe.DisciplineTask)))
                            {
                                viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
                                return Json(viewData);
                            }
                            else if (evnt.EventDate.Date < patientEpisode.EndDate.AddDays(-5).Date || evnt.EventDate.Date > patientEpisode.EndDate.Date)
                            {
                                viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
                                return Json(viewData);
                            }
                        }
                        else if (socTasks.Contains(evnt.DisciplineTask))
                        {
                            if (oldEvents.Exists(oe => socTasks.Contains(oe.DisciplineTask)))
                            {
                                viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
                                return Json(viewData);
                            }
                            else if (oldEvents.Exists(oe => dischargeTasks.Contains(oe.DisciplineTask)))
                            {
                            }
                        }
                        else if (rocTasks.Contains(evnt.DisciplineTask))
                        {
                            var roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask));
                            var transfer = oldEvents.Find(oe => transferTasks.Contains(oe.DisciplineTask));
                            if (roc == null)
                            {
                                if (transfer == null)
                                {
                                    viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                    return Json(viewData);
                                }
                                else if (transfer != null && (transfer.EventDate > evnt.EventDate))
                                {
                                    viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
                                    return Json(viewData);
                                }
                            }
                            else if (roc != null)
                            {
                                if (transfer != null && (roc.EventDate.Date > transfer.EventDate.Date))
                                {
                                    viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                    return Json(viewData);
                                }
                            }
                        }
                        else if (transferTasks.Contains(evnt.DisciplineTask))
                        {
                            var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask));
                            ScheduleEvent roc = null;
                            if (transfer != null)
                            {
                                roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate.Date > transfer.EventDate.Date));
                            }
                            if (transfer != null && roc == null)
                            {
                                viewData.errorMessage = "Please create a Resumption of Care before creating another Transfer.";
                                return Json(viewData);
                            }
                        }
                    }
                }
                if (patientService.AddSchedules(patientEpisode, newEvents))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Your task has been successfully scheduled.";
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in adding " + (newEvents.Count > 1 ? "these events" : "this event") + " to the schedule.";
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Episode is not found.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMultiple(Guid episodeId, Guid patientId, string DisciplineTask, string Discipline, Guid userId, bool IsBillable, string StartDate, string EndDate)
        {
            Check.Argument.IsNotNull(userId, "userId");
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            Check.Argument.IsNotEmpty(DisciplineTask, "DisciplineTask");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Event is not Saved." };
            var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
            if (patientEpisode != null)
            {
                viewData = Validate<JsonViewData>(
                              new Validation(() => string.IsNullOrEmpty(StartDate.ToString()), ". Start date is required."),
                              new Validation(() => !StartDate.IsValidDate(), ". Start date is not in the valid range."),
                              new Validation(() => string.IsNullOrEmpty(EndDate.ToString()), ". End date is required."),
                              new Validation(() => !EndDate.IsValidDate(), ". End date is not in the valid range."),
                              new Validation(() => string.IsNullOrEmpty(DisciplineTask), ". Task is required."),
                              new Validation(() => DisciplineTask.IsNotNullOrEmpty() && DisciplineTask.IsInteger() ? false : true, ". Task is not valid.")
                              );
                if (viewData.isSuccessful)
                {
                    if (StartDate.ToDateTime().Date >= EndDate.ToDateTime().Date)
                    {
                        viewData.errorMessage = "The start date must be greater than end date.";
                        viewData.isSuccessful = false;
                        return Json(viewData);
                    }
                    else if (StartDate.ToDateTime().Date < patientEpisode.StartDate.Date || StartDate.ToDateTime().Date > patientEpisode.EndDate.Date || EndDate.ToDateTime().Date < patientEpisode.StartDate.Date || EndDate.ToDateTime().Date > patientEpisode.EndDate.Date)
                    {
                        viewData.errorMessage = "The start date and end date has to be in the current episode date range.";
                        viewData.isSuccessful = false;
                        return Json(viewData);
                    }
                    else
                    {
                        if (patientService.AddMultiDateRangeScheduleNew(patientEpisode, DisciplineTask.ToInteger(), Discipline, userId, IsBillable, StartDate.ToDateTime(), EndDate.ToDateTime()))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Events are successfully Saved.";
                            return Json(viewData);
                        }
                        else
                        {
                            return Json(viewData);
                        }
                    }
                }
                else
                {
                    return Json(viewData);
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = "Episode is not found.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MultiDay(Guid episodeId, Guid patientId)
        {
            CalendarViewData calendarViewData = null;
            if (!episodeId.IsEmpty())
            {
                var patient = patientRepository.GetPatientOnly(patientId, Current.AgencyId);
                if (patient != null)
                {
                    calendarViewData = new CalendarViewData();
                    var episode = patientRepository.GetEpisode(Current.AgencyId, episodeId, patientId);
                    if (episode != null)
                    {
                        calendarViewData.Episode = episode;
                        calendarViewData.Episode.DisplayNameWithMi = patient.DisplayNameWithMi;
                        calendarViewData.PatientId = patientId;
                    }
                    else calendarViewData.PatientId = patientId;
                    calendarViewData.IsDischarged = patient.IsDischarged;
                }
            }
            return PartialView(calendarViewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddMultiDaySchedule(Guid episodeId, Guid patientId, Guid userId, int disciplineTaskId, string visitDates)
        {
            Check.Argument.IsNotNull(userId, "userId");
            Check.Argument.IsNotNull(episodeId, "episodeId");
            Check.Argument.IsNotNull(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Pick the proper discipline task." };
            var discipline = patientService.GetDisciplineTask(disciplineTaskId);
            if (discipline != null)
            {
                var visitDateArray = visitDates.Split(',').Where(s => s.IsNotNullOrEmpty() && s.IsDate()).ToArray();
                if (!(discipline.IsMultiple == false && visitDateArray.Length > 1))
                {
                    viewData = Validate<JsonViewData>(new Validation(() => string.IsNullOrEmpty(visitDates.ToString()), "Select at least one date from the calendar."));

                    if (viewData.isSuccessful)
                    {
                        var patientEpisode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                        if (patientEpisode != null)
                        {
                            var recertTasks = DisciplineTaskFactory.RecertDisciplineTasks();
                            var transferTasks = DisciplineTaskFactory.TransferNotDischargeOASISDisciplineTasks();
                            var rocTasks = DisciplineTaskFactory.ROCDisciplineTasks();
                            var socTasks = DisciplineTaskFactory.SOCDisciplineTasks();
                            var dischargeTasks = DisciplineTaskFactory.DischargeOASISDisciplineTasks();

                            var oldEvents = patientRepository.GetPatientScheduledEventsOnlyNew(Current.AgencyId, patientEpisode.Id, patientEpisode.PatientId);//(patientEpisode.Schedule.ToObject<List<ScheduleEvent>>()).Where(s => !s.IsDeprecated).OrderByDescending(o => o.EventDate.ToDateTime()).ToList();
                            foreach (var date in visitDateArray)
                            {
                                if (date.IsDate())
                                {
                                    if (recertTasks.Contains(disciplineTaskId))
                                    {
                                        var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask) && (oe.EventDate < date.ToDateTime()));
                                        ScheduleEvent roc = null;
                                        if (transfer != null)
                                        {
                                            roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate > transfer.EventDate));
                                        }
                                        if (transfer != null && roc == null)
                                        {
                                            viewData.isSuccessful = false;
                                            viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                            return Json(viewData);
                                        }
                                        else if (transfer != null && roc != null && roc.EventDate <= transfer.EventDate)
                                        {
                                            viewData.isSuccessful = false;
                                            viewData.errorMessage = "You are not allowed to create Recertification Assessment if the patient was transfered. Please create a Resumption of Care Assessment instead.";
                                            return Json(viewData);
                                        }
                                        else if (oldEvents.Exists(oe => recertTasks.Contains(oe.DisciplineTask)))
                                        {
                                            viewData.isSuccessful = false;
                                            viewData.errorMessage = "A Recertification Assessment already exists in this episode. Please delete that one before creating a new one.";
                                            return Json(viewData);
                                        }
                                        else if (date.ToDateTime() < patientEpisode.EndDate.AddDays(-5) || date.ToDateTime() > patientEpisode.EndDate)
                                        {
                                            viewData.isSuccessful = false;
                                            viewData.errorMessage = "The Recertification date is not valid. The date has to be within the last 5 days of the current episode.";
                                            return Json(viewData);
                                        }
                                    }
                                    else if (socTasks.Contains(disciplineTaskId))
                                    {
                                        if (oldEvents.Exists(oe => socTasks.Contains(oe.DisciplineTask)))
                                        {
                                            viewData.isSuccessful = false;
                                            viewData.errorMessage = "A Start of Care Assessment already exists in this episode. Please delete that one before creating a new one.";
                                            return Json(viewData);
                                        }
                                        else if (oldEvents.Exists(oe => dischargeTasks.Contains(oe.DisciplineTask)))
                                        {
                                        }
                                    }
                                    else if (rocTasks.Contains(disciplineTaskId))
                                    {
                                        var roc = oldEvents.Find(oe => rocTasks.Contains(oe.DisciplineTask));
                                        var transfer = oldEvents.Find(oe => transferTasks.Contains(oe.DisciplineTask));
                                        if (roc == null)
                                        {
                                            if (transfer == null)
                                            {
                                                viewData.isSuccessful = false;
                                                viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                                return Json(viewData);
                                            }
                                            else if (transfer != null && (transfer.EventDate > date.ToDateTime()))
                                            {
                                                viewData.isSuccessful = false;
                                                viewData.errorMessage = "Resumption of Care date should be later that the Transfer date.";
                                                return Json(viewData);
                                            }
                                        }
                                        else if (roc != null)
                                        {
                                            if (transfer != null && (roc.EventDate > transfer.EventDate))
                                            {
                                                viewData.isSuccessful = false;
                                                viewData.errorMessage = "A Resumption of Care can not be created before a Transfer.";
                                                return Json(viewData);
                                            }
                                        }
                                    }
                                    else if (transferTasks.Contains(disciplineTaskId))
                                    {
                                        var transfer = oldEvents.FirstOrDefault(oe => transferTasks.Contains(oe.DisciplineTask));
                                        ScheduleEvent roc = null;
                                        if (transfer != null)
                                        {
                                            roc = oldEvents.Find(oe => (rocTasks.Contains(oe.DisciplineTask) && (oe.EventDate > transfer.EventDate)));
                                        }
                                        if (transfer != null && roc == null)
                                        {
                                            viewData.isSuccessful = false;
                                            viewData.errorMessage = "Please create a Resumption of Care before creating another Transfer.";
                                            return Json(viewData);
                                        }
                                    }
                                }
                                else
                                {
                                    viewData.isSuccessful = false;
                                    viewData.errorMessage = "Date is not in the right format.";
                                    return Json(viewData);
                                }
                            }

                            if (!patientService.AddMultiDayScheduleNew(patientEpisode, userId, disciplineTaskId, visitDates))
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "Unable to save this page. Please try again.";
                            }
                            else
                            {
                                viewData.errorMessage = "Task(s) scheduled for user succesfully.";
                            }
                        }
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Mutiple task is not allowed for this discipline.";
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Process Notes
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Notes(string button, FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var rules = new List<Validation>();
            var keys = formCollection.AllKeys;
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The note could not be saved." };
            string type = formCollection["Type"];
            if (type.IsNotNullOrEmpty())
            {
                var eventId = formCollection.Get(string.Format("{0}_EventId", type)).ToGuid();
                var episodeId = formCollection.Get(string.Format("{0}_EpisodeId", type)).ToGuid();
                var patientId = formCollection.Get(string.Format("{0}_PatientId", type)).ToGuid();
                if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
                {
                    var episode = patientRepository.GetEpisodeOnly(Current.AgencyId, episodeId, patientId);
                    if (keys.Contains(type + "_VisitDate"))
                    {
                        rules.Add(new Validation(() => !formCollection[type + "_VisitDate"].IsNotNullOrEmpty(), "Visit date can't be empty."));
                        rules.Add(new Validation(() => !formCollection[type + "_VisitDate"].IsValidDate(), "Visit date is not valid."));
                        rules.Add(new Validation(() => formCollection[type + "_VisitDate"].IsNotNullOrEmpty() && formCollection[type + "_VisitDate"].IsValidDate() ? !(formCollection[type + "_VisitDate"].ToDateTime().Date >= episode.StartDate.Date && formCollection[type + "_VisitDate"].ToDateTime().Date <= episode.EndDate.Date) : true, "Visit date is not in the episode range."));
                    }
                    if (formCollection[type + "_StatusComment"].IsNotNullOrEmpty()) patientService.UpdateReturnReason(episodeId, patientId, eventId, formCollection[type + "_StatusComment"]);
                    if (button == ButtonAction.Save.ToString())
                    {
                        rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsNotNullOrEmpty(), "Task can't be empty."));
                        if (formCollection["DisciplineTask"].IsNotNullOrEmpty()) rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsInteger(), "Select the right task."));
                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            if (patientService.SaveNotesNew(button, formCollection))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The note was successfully saved.";
                            }
                            else
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "The note could not be saved.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message;
                        }
                    }
                    else if (button == ButtonAction.Complete.ToString())
                    {
                        if (keys.Contains(type + "_Clinician"))
                        {
                            rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_Clinician"]), "Clinician Signature is required."));
                            rules.Add(new Validation(() => formCollection[type + "_Clinician"].IsNotNullOrEmpty() ? !userService.IsSignatureCorrect(Current.UserId, formCollection[type + "_Clinician"]) : false, "User Signature is not correct."));
                        }
                        if (keys.Contains(type + "_TimeIn"))
                        {
                            rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_TimeIn"]), "Time-In is required. "));
                        }
                        if (keys.Contains(type + "_TimeOut"))
                        {
                            rules.Add(new Validation(() => string.IsNullOrEmpty(formCollection[type + "_TimeOut"]), "Time-Out is required. "));
                        }
                        if (keys.Contains(type + "_SignatureDate"))
                        {
                            rules.Add(new Validation(() => formCollection[type + "_SignatureDate"].IsNullOrEmpty(), "Signature date can't be empty."));
                            rules.Add(new Validation(() => !formCollection[type + "_SignatureDate"].IsValidDate(), "Signature date is not valid."));
                            rules.Add(new Validation(() => formCollection[type + "_SignatureDate"].IsNotNullOrEmpty() && formCollection[type + "_SignatureDate"].IsValidDate() ? !(formCollection[type + "_SignatureDate"].ToDateTime() >= episode.StartDate && formCollection[type + "_SignatureDate"].ToDateTime() <= DateTime.Now) : true, "Signature date is not the in valid range."));
                        }
                        rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsNotNullOrEmpty(), "Task can't be empty."));
                        if (formCollection["DisciplineTask"].IsNotNullOrEmpty()) rules.Add(new Validation(() => !formCollection["DisciplineTask"].IsInteger() && !Enum.IsDefined(typeof(DisciplineTasks), formCollection["DisciplineTask"].ToInteger()), "Select the right task."));
                        if (DisciplineTaskFactory.PhysicianEvalNotes().Exists(d => d.ToString().IsEqual(type))) rules.Add(new Validation(() => formCollection[type + "_PhysicianId"].IsNullOrEmpty(), "Physician is required. "));
                        var entityValidator = new EntityValidator(rules.ToArray());
                        entityValidator.Validate();
                        if (entityValidator.IsValid)
                        {
                            if (patientService.SaveNotesNew(button, formCollection))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "The note was successfully Submited.";
                            }
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = entityValidator.Message;
                        }
                    }
                    else if (button == ButtonAction.Approve.ToString())
                    {
                        var patientVisitNote = patientRepository.GetVisitNote(Current.AgencyId, episodeId, patientId, eventId);
                        if (patientVisitNote != null)
                        {
                            if (patientVisitNote.SignatureText.IsNullOrEmpty() || patientVisitNote.SignatureDate == DateTime.MinValue)
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "The note could not be approved because the Electronic Signature is missing. Please sign this note before continuing.";
                            }
                            else
                            {
                                if (patientService.SaveNotesNew(button, formCollection))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "The note was successfully Approved.";
                                }
                                else return Json(viewData);
                            }
                        }
                    }
                    else if (button == ButtonAction.Return.ToString())
                    {
                        if (patientService.SaveNotesNew(button, formCollection))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The note was successfully returned.";
                        }
                        else
                        {
                            viewData.isSuccessful = false;
                            viewData.errorMessage = "The note could not be returned.";
                        }
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ProcessNotes(string button, Guid episodeId, Guid patientId, Guid eventId, string reason)
        {
            var rules = new List<Validation>();
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your note could not be saved." };
            if (!eventId.IsEmpty() && !episodeId.IsEmpty() && !patientId.IsEmpty())
            {
                if (button == ButtonAction.Approve.ToString())
                {
                    if (patientService.ProcessNotesNew(button, episodeId, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your note has been successfully approved.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your note could not be approved.";
                    }
                }
                else if (button == ButtonAction.Return.ToString())
                {
                    if (patientService.ProcessNotesNew(button, episodeId, patientId, eventId, reason))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Your note has been successfully returned.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Your note could not be returned.";
                    }
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BulkUpdate(List<string> CustomValue, string CommandType)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The item(s) you selected could not be updated." };
            if (CustomValue != null && CustomValue.Count > 0 && CommandType.IsNotNullOrEmpty())
            {
                int total = CustomValue.Count;
                int count = 0;
                CustomValue.ForEach(v =>
                {
                    var infos = v.Split('|');
                    if (infos.Length == 4 && infos[0].IsGuid() && infos[1].IsGuid() && infos[2].IsGuid() && infos[3].IsInteger())
                    {
                        var scheduleEvent = patientRepository.GetScheduleEventNew(Current.AgencyId, infos[1].ToGuid(), infos[0].ToGuid(), infos[2].ToGuid());
                        if (scheduleEvent != null && scheduleEvent.DisciplineTask == infos[3].ToInteger())
                        {
                            var eventType = scheduleEvent.TypeOfEvent();
                            if (eventType.IsNotNullOrEmpty())
                            {
                                switch (eventType)
                                {
                                    case "OASIS":
                                        if (Enum.IsDefined(typeof(DisciplineTasks), infos[3].ToInteger()))
                                        {
                                            var assessmentType = ((DisciplineTasks)Enum.ToObject(typeof(DisciplineTasks), infos[3].ToInteger())).ToString();
                                            if (CommandType == ButtonAction.Approve.ToString())
                                            {
                                                if (assessmentService.UpdateAssessmentStatusNew(scheduleEvent, ((int)ScheduleStatus.OasisCompletedExportReady).ToString(), string.Empty))
                                                {
                                                    if ((assessmentType == DisciplineTasks.OASISCDischargeOT.ToString() || assessmentType == DisciplineTasks.OASISCDischarge.ToString() || assessmentType == DisciplineTasks.OASISCDischargePT.ToString() || assessmentType == "DischargeFromAgency") || (assessmentType == DisciplineTasks.OASISCTransferDischarge.ToString() || assessmentType == "OASISCTransferDischarge") || (assessmentType == DisciplineTasks.OASISCDeath.ToString() || assessmentType == DisciplineTasks.OASISCDeathOT.ToString() || assessmentType == DisciplineTasks.OASISCDeathPT.ToString() || assessmentType == "OASISCDeath"))
                                                    {
                                                        var assessment = assessmentService.GetAssessment(scheduleEvent.EventId);
                                                        if (assessment != null)
                                                        {
                                                            var assessmentData = assessment.ToDictionary();
                                                            var date = DateTime.MinValue;
                                                            var dateAssessment = assessmentData["M0906DischargeDate"].Answer;
                                                            if (dateAssessment.IsNotNullOrEmpty() && dateAssessment.IsValidDate()) date = dateAssessment.ToDateTime();
                                                            var eventDateSchedule = scheduleEvent.EventDate;
                                                            if (eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                                            var visitDateSchedule = scheduleEvent.VisitDate;
                                                            if (visitDateSchedule.IsValid()) date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                                            if (date > DateTime.MinValue) patientService.DischargePatient(scheduleEvent.PatientId, scheduleEvent.EpisodeId, date, "Patient discharged due to discharge oasis.");
                                                        }
                                                        else
                                                        {
                                                            var date = DateTime.MinValue;
                                                            var eventDateSchedule = scheduleEvent.EventDate;
                                                            if (eventDateSchedule.IsValid()) date = date.Date > eventDateSchedule.Date ? date : eventDateSchedule;
                                                            var visitDateSchedule = scheduleEvent.VisitDate;
                                                            if (visitDateSchedule.IsValid()) date = date.Date > visitDateSchedule.Date ? date : visitDateSchedule;
                                                            if (date > DateTime.MinValue) patientService.DischargePatient(scheduleEvent.PatientId, scheduleEvent.EpisodeId, date, "Patient discharged due to discharge oasis.");
                                                        }
                                                    }
                                                    count++;
                                                }
                                            }
                                            else if (CommandType == ButtonAction.Return.ToString() && assessmentService.UpdateAssessmentStatusNew(scheduleEvent, ((int)ScheduleStatus.OasisReturnedForClinicianReview).ToString(), string.Empty)) count++;
                                            else if (CommandType == ButtonAction.Print.ToString() && scheduleEvent != null)
                                            {
                                                scheduleEvent.InPrintQueue = false;
                                                if (patientRepository.UpdateScheduleEventNew(scheduleEvent)) count++;
                                            }
                                        }
                                        break;
                                    case "Notes":
                                        if (patientService.ProcessNotesNew(CommandType, scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, string.Empty)) count++;
                                        break;
                                    case "PhysicianOrder":
                                        if (patientService.ProcessPhysicianOrder(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, CommandType, string.Empty)) count++;
                                        break;
                                    case "PlanOfCare":
                                        if (assessmentService.UpdatePlanofCareStatus(scheduleEvent.EpisodeId, scheduleEvent.PatientId, scheduleEvent.EventId, CommandType, string.Empty)) count++;
                                        break;
                                    case "IncidentAccident":
                                        if (agencyService.ProcessIncidents(CommandType, scheduleEvent.PatientId, scheduleEvent.EventId, string.Empty)) count++;
                                        break;
                                    case "Infection":
                                        if (agencyService.ProcessInfections(CommandType, scheduleEvent.PatientId, scheduleEvent.EventId, string.Empty)) count++;
                                        break;
                                    case "CommunicationNote":
                                        if (patientService.ProcessCommunicationNotes(CommandType, scheduleEvent.PatientId, scheduleEvent.EventId, string.Empty)) count++;
                                        break;
                                }
                            }
                        }
                    }
                });
                viewData.isSuccessful = true;
                if (count == total) viewData.errorMessage = string.Format("All ({0}) items were updated successfully.", count);
                else if (count < total) viewData.errorMessage = string.Format("{0} out of {1} items were updated successfully. Try the again.", count, total);
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ReturnReason(Guid eventId, Guid patientId)
        {
            var viewData = new JsonViewData();
            var comments = patientRepository.GetReturnReason(eventId, patientId, Current.AgencyId);
            if (comments.IsNotNullOrEmpty())
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = comments;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddReturnReason(Guid eventId, Guid patientId, string reason)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The return comments failed to save." };
            if (patientRepository.AddReturnReason(eventId, patientId, Current.AgencyId, reason, Current.UserFullName))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Return comment saved successfully";
            }
            return Json(viewData);
        }
        #endregion

        #region Note Asset Actions
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteWoundCareAsset(Guid episodeId, Guid patientId, Guid eventId, string name, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Wound care asset could not be deleted." };
            if (patientService.DeleteWoundCareAssetNew(episodeId, patientId, eventId, name, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Wound care asset successfully deleted.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteScheduleEventAsset(Guid patientId, Guid episodeId, Guid eventId, Guid assetId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your Asset Not Deleted." };
            if (patientService.DeleteScheduleEventAssetNew(episodeId, patientId, eventId, assetId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Your Asset Successfully Deleted.";
            }
            return Json(viewData);
        }
        #endregion

        #region Re-Assign Schedules
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Reassign(Guid eventId, Guid episodeId, Guid patientId, Guid userId)
        {
            return PartialView(new ReassignViewData { EventId = eventId, EpisodeId = episodeId, PatientId = patientId, UserId = userId });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignTask(Guid eventId, Guid episodeId, Guid patientId, Guid userId, Guid newUserId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "This task could not be reassigned to another user" };
            if (patientService.ReassignNew(episodeId, patientId, eventId, userId, newUserId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "This task has been reassigned sucessfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignMultiple(Guid? episodeId, Guid? patientId, string type)
        {
            if (episodeId.HasValue && patientId.HasValue)
            {
                var viewData = new ReassignMultipleViewData { EpisodeId = (Guid)episodeId, PatientId = (Guid)patientId, Type = type };
                if (!viewData.PatientId.IsEmpty())
                {
                    var patient = patientRepository.GetPatientOnly((Guid)patientId, Current.AgencyId);
                    if (patient != null) viewData.PatientDisplayName = patient.DisplayNameWithMi;
                }
                return PartialView(viewData);
            }
            else return PartialView(new ReassignMultipleViewData());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignPatient(Guid PatientId, Guid EmployeeOldId, Guid EmployeeId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Events is not reassigned." };
            if (!PatientId.IsEmpty() && !EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    if (patientService.ReassignSchedules(PatientId, EmployeeOldId, EmployeeId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Events are reassigned sucessfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Events are not reassigned. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee  has to be different from the previous one.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignEpisode(Guid PatientId, Guid EmployeeOldId, Guid EmployeeId)
        {
            JsonViewData viewData = new JsonViewData { isSuccessful = false, errorMessage = "Events is not reassigned." };
            if (!PatientId.IsEmpty() && !EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    if (patientService.ReassignSchedules(PatientId, EmployeeOldId, EmployeeId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Events are reassigned sucessfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Events are not reassigned. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee  has to be different from the previous one.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReassignAll(Guid EmployeeOldId, Guid EmployeeId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Events is not reassigned." };
            if (!EmployeeOldId.IsEmpty() && !EmployeeId.IsEmpty())
            {
                if (EmployeeOldId != EmployeeId)
                {
                    if (patientService.ReassignSchedules(EmployeeOldId, EmployeeId))
                    {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "Events are reassigned sucessfully.";
                    }
                    else
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Events are not reassigned. Try again.";
                    }
                }
                else
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The new assigned employee  has to be different from the previous one.";
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Schedule Actions
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Reopen(Guid episodeId, Guid patientId, Guid eventId)
        {
            Check.Argument.IsNotEmpty(eventId, "eventId");
            Check.Argument.IsNotEmpty(episodeId, "episodeId");
            Check.Argument.IsNotEmpty(patientId, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Task cannot be reopened." };
            if (patientService.ReopenNew(episodeId, patientId, eventId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Task has been reopened sucessfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(Guid episodeId, Guid patientId, Guid eventId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error trying to delete this task. Please try again." };
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !eventId.IsEmpty())
            {
                if (patientService.ToggleScheduleStatusNew(episodeId, patientId, eventId, true))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Task has been successfully deleted.";
                }
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Restore(Guid id, Guid episodeId, Guid patientId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error trying to restore this task. Please try again." };
            if (!episodeId.IsEmpty() && !patientId.IsEmpty() && !id.IsEmpty())
            {
                if (patientService.ToggleScheduleStatusNew(episodeId, patientId, id, false))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "Task has been successfully restored.";
                }
            }
            return Json(viewData);
        }
        #endregion
        
        #region Schedule Deviation
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Deviation()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Deviation/Main", reportService.GetScheduleDeviation(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeviationGrid(Guid branchId, DateTime startDate, DateTime endDate, string sortParams)
        {
            if (sortParams.IsNotNullOrEmpty())
            {
                var paramArray = sortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Deviation/Grid", reportService.GetScheduleDeviation(branchId, startDate, endDate));
        }
        #endregion

        #region Logs
        public ActionResult ScheduleLogs(Guid eventId, Guid patientId, int task)
        {
            return PartialView("Logs", patientService.GetTaskLogs(patientId, eventId, task));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EpisodeLogs(Guid episodeId, Guid patientId)
        {
            return PartialView("Episode/Logs", patientService.GetGeneralLogs(LogDomain.Patient, LogType.Episode, patientId, episodeId.ToString()));
        }

        #endregion

        #region Private Duty
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PDCenter()
        {
            return PartialView("PD/Center/Layout");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PDCenterPatientGrid(Guid branchId, byte statusId, byte paymentSourceId)
        {
            var patientList = new List<PatientSelection>();
            if (Current.IsAgencyAdmin || Current.IsOfficeManager || Current.IsDirectorOfNursing || Current.IsCaseManager || Current.IsBiller || Current.IsClerk || Current.IsScheduler || Current.IsQA)
                patientList = patientRepository.GetPatientSelection(Current.AgencyId, branchId, statusId, paymentSourceId);
            else if (Current.IsClinicianOrHHA)
                patientList = patientRepository.GetUserPatients(Current.AgencyId, branchId, Current.UserId, statusId, paymentSourceId);
            return View(new GridModel(patientList.OrderBy(p => p.LastName).ThenBy(p => p.ShortName)));
        }
        #endregion

        #endregion

        #region Private (Helper methods)

        private NoteArguments GetArgumentNoteWithView(ScheduleEvent scheduledEvent , bool IsPreview)
        {
            var argument = new NoteArguments();
            if (scheduledEvent != null && scheduledEvent.DisciplineTask > 0)
            {
                argument.EpisodeId = scheduledEvent.EpisodeId;
                argument.PatientId = scheduledEvent.PatientId;
                argument.EventId = scheduledEvent.EventId;
                scheduledEvent.Version = scheduledEvent.Version > 0 ? scheduledEvent.Version : 1;
                switch (scheduledEvent.DisciplineTask)
                {
                    case (int)DisciplineTasks.FoleyCathChange:
                    case (int)DisciplineTasks.PICCMidlinePlacement:
                    case (int)DisciplineTasks.PRNFoleyChange:
                    case (int)DisciplineTasks.PRNSNV:
                    case (int)DisciplineTasks.PRNVPforCMP:
                    case (int)DisciplineTasks.PTWithINR:
                    case (int)DisciplineTasks.PTWithINRPRNSNV:
                    case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case (int)DisciplineTasks.SkilledNurseVisit:
                    case (int)DisciplineTasks.SNB12INJ:
                    case (int)DisciplineTasks.SNBMP:
                    case (int)DisciplineTasks.SNCBC:
                    case (int)DisciplineTasks.SNDC:
                    case (int)DisciplineTasks.SNEvaluation:
                    case (int)DisciplineTasks.SNFoleyChange:
                    case (int)DisciplineTasks.SNFoleyLabs:
                    case (int)DisciplineTasks.SNHaldolInj:
                    case (int)DisciplineTasks.SNInsulinAM:
                    case (int)DisciplineTasks.SNInsulinPM:
                    case (int)DisciplineTasks.SNInjection:
                    case (int)DisciplineTasks.SNInjectionLabs:
                    case (int)DisciplineTasks.SNLabsSN:
                    case (int)DisciplineTasks.SNVDCPlanning:
                    case (int)DisciplineTasks.SNVManagementAndEvaluation:
                    case (int)DisciplineTasks.SNVObservationAndAssessment:
                    case (int)DisciplineTasks.SNVPsychNurse:
                    case (int)DisciplineTasks.SNVTeachingTraining:
                    case (int)DisciplineTasks.SNVwithAideSupervision:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = true;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "Nursing/SkilledNurseVisitPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = true;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "Nursing/SkilledNurseVisit";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.SNDiabeticDailyVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = false;
                                argument.ContentPath = "Nursing/SNDiabeticDailyVisitPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = false;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "Nursing/SNDiabeticDailyVisit";
                            }
                        }
                        break;
                    
                    case (int)DisciplineTasks.PTVisit:
                    case (int)DisciplineTasks.PTAVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = string.Format("Therapy/PTVisit/PrintRev{0}",scheduledEvent.Version);
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = true;
                                argument.EvalTask = DisciplineTasks.PTEvaluation;
                                argument.ContentPath = "Therapy/PTVisit/FormRev1";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.PTDischarge:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "Therapy/PTDischarge/PrintRev1";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = true;
                                argument.EvalTask = DisciplineTasks.PTEvaluation;
                                argument.ContentPath = "Therapy/PTDischarge/FormRev1";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.PTEvaluation:
                    case (int)DisciplineTasks.PTMaintenance:
                    case (int)DisciplineTasks.PTReEvaluation:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "Therapy/PTEvaluation/PrintRev1";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "Therapy/PTEvaluation/FormRev1";
                            }
                        }
                        break;

                    case (int)DisciplineTasks.OTVisit:
                    case (int)DisciplineTasks.COTAVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "Therapy/OTVisit/PrintRev1";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = true;
                                argument.EvalTask = DisciplineTasks.OTEvaluation;
                                argument.ContentPath = "Therapy/OTVisit/FormRev1";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.OTEvaluation:
                    case (int)DisciplineTasks.OTReEvaluation:
                    case (int)DisciplineTasks.OTMaintenance:
                    case (int)DisciplineTasks.OTDischarge:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "Therapy/OTEvaluation/PrintRev1";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "Therapy/OTEvaluation/FormRev1";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.STVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "Therapy/STVisit/PrintRev1";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = true;
                                argument.EvalTask = DisciplineTasks.STEvaluation;
                                argument.ContentPath = "Therapy/STVisit/FormRev1";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.STEvaluation:
                    case (int)DisciplineTasks.STReEvaluation:
                    case (int)DisciplineTasks.STMaintenance:
                    case (int)DisciplineTasks.STDischarge:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "Therapy/STEvaluation/PrintRev1";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "Therapy/STEvaluation/FormRev1";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.MSWAssessment:
                    case (int)DisciplineTasks.MSWDischarge:
                    case (int)DisciplineTasks.MSWEvaluationAssessment:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "MSW/MSWEvaluationAssessmentPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "MSW/MSWEvaluationAssessment";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.MSWProgressNote:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "MSW/MSWProgressNotePrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "MSW/MSWProgressNote";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.MSWVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "MSW/MSWVisitPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "MSW/MSWVisit";
                            }
                        }
                        break;
                   
                    case (int)DisciplineTasks.HHAideVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = false;
                                argument.ContentPath = "HHA/HHAideVisitPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = false;
                                argument.IsEvalNeeded = true;
                                argument.EvalTask = DisciplineTasks.HHAideCarePlan;
                                argument.ContentPath = "HHA/HHAideVisit";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.HHAideCarePlan:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = true;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "HHA/HHAideCarePlanPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = true;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "HHA/HHAideCarePlan";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.DischargeSummary:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = true;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = false;
                                argument.ContentPath = "Nursing/DischargeSummaryPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = true;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = false;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "Nursing/DischargeSummary";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.PASVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = false;
                                argument.ContentPath = "PAS/PASVisitPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = false;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "PAS/PASVisit";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.PASCarePlan:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = true;
                                argument.IsAllergyNeeded = true;
                                argument.IsAssessmentNeeded = false;
                                argument.ContentPath = "PAS/PASCarePlanPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = true;
                                argument.IsDiagnosisNeeded = false;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "PAS/PASCarePlan";
                            }
                        }
                        break;

                        case (int)DisciplineTasks.LVNSupervisoryVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = false;
                                argument.ContentPath = "Nursing/LVNSupervisoryVisitPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = false;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "Nursing/LVNSupervisoryVisit";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.HHAideSupervisoryVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = false;
                                argument.ContentPath = "HHA/HHAideSupervisoryVisitPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = false;
                                argument.IsEvalNeeded = true;
                                argument.EvalTask = DisciplineTasks.HHAideCarePlan;
                                argument.ContentPath = "HHA/HHAideSupervisoryVisit";
                            }
                        }
                        break;

                    case (int)DisciplineTasks.UAPWoundCareVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = false;
                                argument.IsJsonSerialize = true;
                                argument.pdfDoc = PdfDocs.UAPWoundCareVisit;
                                argument.ContentPath = "UAP/UAPWoundCareVisitPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = false;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "UAP/UAPWoundCareVisit";
                            }
                        }
                        break;

                    case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = false; 
                                argument.IsJsonSerialize = true;
                                argument.pdfDoc = PdfDocs.UAPInsulinPrepAdminVisit;
                                argument.ContentPath = "UAP/UAPInsulinPrepAdminVisitPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = true;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = false;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "UAP/UAPInsulinPrepAdminVisit";
                            }
                        }
                        break;


                    case (int)DisciplineTasks.SixtyDaySummary:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = true;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = false;
                                argument.IsJsonSerialize = true;
                                argument.pdfDoc = PdfDocs.SixtyDaySummary;
                                argument.ContentPath = "Nursing/SixtyDaySummaryPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "Nursing/60DaySummary";
                            }
                        }
                        break;
                    case (int)DisciplineTasks.TransferSummary:
                    case (int)DisciplineTasks.CoordinationOfCare:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = true;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = true;
                                argument.ContentPath = "Nursing/TransferSummaryPrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = true;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = true;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = true;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "Nursing/TransferSummary";
                            }
                        }
                        break;

                    case (int)DisciplineTasks.DriverOrTransportationNote:
                        {
                            if (IsPreview)
                            {
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsAssessmentNeeded = false;
                                argument.IsJsonSerialize = true;
                                argument.pdfDoc = PdfDocs.DriverOrTransportationNote;
                                argument.ContentPath = "MSW/DriverOrTransportationNotePrint";
                            }
                            else
                            {
                                argument.IsPlanOfCareNeeded = false;
                                argument.IsPreviousNoteNeeded = false;
                                argument.IsPhysicainNeeded = false;
                                argument.IsAllergyNeeded = false;
                                argument.IsDiagnosisNeeded = false;
                                argument.IsEvalNeeded = false;
                                argument.EvalTask = DisciplineTasks.NoDiscipline;
                                argument.ContentPath = "MSW/DriverOrTransportationNote";
                            }
                        }
                        break;
                }
            }
            return argument;
        }

        private string GetViewForContent(VisitNoteViewData noteData)
        {
            var contentPath = string.Empty;
            if (noteData != null && noteData.Type.IsNotNullOrEmpty() && Enum.IsDefined(typeof(DisciplineTasks), noteData.Type))
            {
                noteData.Version = noteData.Version > 0 ? noteData.Version : 1;
                switch ((DisciplineTasks)Enum.Parse(typeof(DisciplineTasks), noteData.Type))
                {
                    case DisciplineTasks.FoleyCathChange:
                    case DisciplineTasks.PICCMidlinePlacement:
                    case DisciplineTasks.PRNFoleyChange:
                    case DisciplineTasks.PRNSNV:
                    case DisciplineTasks.PRNVPforCMP:
                    case DisciplineTasks.PTWithINR:
                    case DisciplineTasks.PTWithINRPRNSNV:
                    case DisciplineTasks.SkilledNurseHomeInfusionSD:
                    case DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                    case DisciplineTasks.SkilledNurseVisit:
                    case DisciplineTasks.SNB12INJ:
                    case DisciplineTasks.SNBMP:
                    case DisciplineTasks.SNCBC:
                    case DisciplineTasks.SNDC:
                    case DisciplineTasks.SNEvaluation:
                    case DisciplineTasks.SNFoleyChange:
                    case DisciplineTasks.SNFoleyLabs:
                    case DisciplineTasks.SNHaldolInj:
                    case DisciplineTasks.SNInsulinAM:
                    case DisciplineTasks.SNInsulinPM:
                    case DisciplineTasks.SNInjection:
                    case DisciplineTasks.SNInjectionLabs:
                    case DisciplineTasks.SNLabsSN:
                    case DisciplineTasks.SNVDCPlanning:
                    case DisciplineTasks.SNVManagementAndEvaluation:
                    case DisciplineTasks.SNVObservationAndAssessment:
                    case DisciplineTasks.SNVPsychNurse:
                    case DisciplineTasks.SNVTeachingTraining:
                    case DisciplineTasks.SNVwithAideSupervision:
                        {
                                contentPath = "Nursing/SkilledNurseVisitContent";
                        }
                        break;

                    case DisciplineTasks.PTVisit:
                    case DisciplineTasks.PTAVisit:
                        {

                            contentPath = string.Format("Therapy/PTVisit/ContentRev{0}",  noteData.Version );
                        }
                        break;
                    case DisciplineTasks.PTDischarge:
                        {

                            contentPath = "Therapy/PTDischarge/ContentRev1";
                        }
                        break;
                    case DisciplineTasks.PTEvaluation:
                    case DisciplineTasks.PTMaintenance:
                    case DisciplineTasks.PTReEvaluation:
                        {

                            contentPath = "Therapy/PTEvaluation/ContentRev1";
                        }
                        break;

                    case DisciplineTasks.OTVisit:
                    case DisciplineTasks.COTAVisit:
                        {

                            contentPath = "Therapy/OTVisit/ContentRev1";
                        }
                        break;
                    case DisciplineTasks.OTEvaluation:
                    case DisciplineTasks.OTReEvaluation:
                    case DisciplineTasks.OTMaintenance:
                    case DisciplineTasks.OTDischarge:
                        {

                            contentPath = "Therapy/OTEvaluation/ContentRev1";
                        }
                        break;
                    case DisciplineTasks.STVisit:
                        {

                            contentPath = "Therapy/STVisit/ContentRev1";
                        }
                        break;
                    case DisciplineTasks.STEvaluation:
                    case DisciplineTasks.STReEvaluation:
                    case DisciplineTasks.STMaintenance:
                    case DisciplineTasks.STDischarge:
                        {

                            contentPath = "Therapy/STEvaluation/ContentRev1";
                        }
                        break;

                    case DisciplineTasks.MSWProgressNote:
                        {

                            contentPath = "MSW/MSWProgressNoteContent";
                        }
                        break;


                    case DisciplineTasks.HHAideVisit:
                        {

                            contentPath = "HHA/HHAideVisitContent";
                        }
                        break;

                    case DisciplineTasks.PASVisit:
                        {

                            contentPath = "PAS/PASVisitContent";
                        }
                        break;

                    case DisciplineTasks.UAPWoundCareVisit:
                        {

                            contentPath = "UAP/UAPWoundCareVisitContent";
                        }
                        break;

                    case DisciplineTasks.UAPInsulinPrepAdminVisit:
                        {

                            contentPath = "UAP/UAPInsulinPrepAdminVisitContent";
                        }
                        break;
                }
            }
            return contentPath;
        }

        private FileResult GetPrintFile(VisitNoteViewData note, int disciplineTask , FileResult deafultValue)
        {
            FileResult file = deafultValue;
            switch (disciplineTask)
            {
                case (int)DisciplineTasks.FoleyCathChange:
                case (int)DisciplineTasks.PICCMidlinePlacement:
                case (int)DisciplineTasks.PRNFoleyChange:
                case (int)DisciplineTasks.PRNSNV:
                case (int)DisciplineTasks.PRNVPforCMP:
                case (int)DisciplineTasks.PTWithINR:
                case (int)DisciplineTasks.PTWithINRPRNSNV:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSD:
                case (int)DisciplineTasks.SkilledNurseHomeInfusionSDAdditional:
                case (int)DisciplineTasks.SkilledNurseVisit:
                case (int)DisciplineTasks.SNB12INJ:
                case (int)DisciplineTasks.SNBMP:
                case (int)DisciplineTasks.SNCBC:
                case (int)DisciplineTasks.SNDC:
                case (int)DisciplineTasks.SNEvaluation:
                case (int)DisciplineTasks.SNFoleyChange:
                case (int)DisciplineTasks.SNFoleyLabs:
                case (int)DisciplineTasks.SNHaldolInj:
                case (int)DisciplineTasks.SNInsulinAM:
                case (int)DisciplineTasks.SNInsulinPM:
                case (int)DisciplineTasks.SNInjection:
                case (int)DisciplineTasks.SNInjectionLabs:
                case (int)DisciplineTasks.SNLabsSN:
                case (int)DisciplineTasks.SNVDCPlanning:
                case (int)DisciplineTasks.SNVManagementAndEvaluation:
                case (int)DisciplineTasks.SNVObservationAndAssessment:
                case (int)DisciplineTasks.SNVPsychNurse:
                case (int)DisciplineTasks.SNVTeachingTraining:
                case (int)DisciplineTasks.SNVwithAideSupervision:
                    {
                        file = FileGenerator.Pdf<SkilledNurseVisitPdf>(new SkilledNurseVisitPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.SNDiabeticDailyVisit:
                    {
                        file = FileGenerator.Pdf<SNDiabeticDailyVisitPdf>(new SNDiabeticDailyVisitPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.PTVisit:
                case (int)DisciplineTasks.PTAVisit:
                    {
                        file = FileGenerator.Pdf<TherapyPdf>(new TherapyPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.PTDischarge:
                case (int)DisciplineTasks.PTEvaluation:
                case (int)DisciplineTasks.PTMaintenance:
                case (int)DisciplineTasks.PTReEvaluation:
                    {
                        file = FileGenerator.Pdf<TherapyPdf>(new TherapyPdf(note), note.TypeName); 
                    }
                    break;

                case (int)DisciplineTasks.OTVisit:
                case (int)DisciplineTasks.COTAVisit:
                    {
                        file = FileGenerator.Pdf<TherapyPdf>(new TherapyPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.OTEvaluation:
                case (int)DisciplineTasks.OTReEvaluation:
                case (int)DisciplineTasks.OTMaintenance:
                case (int)DisciplineTasks.OTDischarge:
                    {
                        file = FileGenerator.Pdf<TherapyPdf>(new TherapyPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.STVisit:
                    {
                        file = FileGenerator.Pdf<TherapyPdf>(new TherapyPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.STEvaluation:
                case (int)DisciplineTasks.STReEvaluation:
                case (int)DisciplineTasks.STMaintenance:
                case (int)DisciplineTasks.STDischarge:
                    {
                        file = FileGenerator.Pdf<TherapyPdf>(new TherapyPdf(note), note.TypeName);
                    }
                    break;

                case (int)DisciplineTasks.MSWAssessment:
                case (int)DisciplineTasks.MSWDischarge:
                case (int)DisciplineTasks.MSWEvaluationAssessment:
                    {
                        file = FileGenerator.Pdf<MSWPdf>(new MSWPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.MSWProgressNote:
                    {
                        file = FileGenerator.Pdf<MSWPdf>(new MSWPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.MSWVisit:
                    {
                        file = FileGenerator.Pdf<MSWPdf>(new MSWPdf(note), note.TypeName);
                    }
                    break;

                case (int)DisciplineTasks.HHAideVisit:
                    {
                        file = FileGenerator.Pdf<HHAideVisitPdf>(new HHAideVisitPdf(note), note.TypeName);
                    }
                    break;

                case (int)DisciplineTasks.HHAideCarePlan:
                    {
                        file = FileGenerator.Pdf<HHAideCarePlanPdf>(new HHAideCarePlanPdf(note), note.TypeName);
                    }
                    break;

                case (int)DisciplineTasks.DischargeSummary:
                    {
                        file = FileGenerator.Pdf<DischargeSummaryPdf>(new DischargeSummaryPdf(note), note.TypeName);
                    }
                    break;

                case (int)DisciplineTasks.PASVisit:
                    {
                        file = FileGenerator.Pdf<PASVisitPdf>(new PASVisitPdf(note), note.TypeName);
                    }
                    break;

                case (int)DisciplineTasks.PASCarePlan:
                    {
                        file = FileGenerator.Pdf<PASCarePlanPdf>(new PASCarePlanPdf(note), note.TypeName);
                    }
                    break;

                case (int)DisciplineTasks.LVNSupervisoryVisit:
                    {
                        file = FileGenerator.Pdf<LVNSupervisoryVisitPdf>(new LVNSupervisoryVisitPdf(note), note.TypeName);
                    }
                    break;

                case (int)DisciplineTasks.HHAideSupervisoryVisit:
                    {
                        file = FileGenerator.Pdf<HHAideSupervisoryVisitPdf>(new HHAideSupervisoryVisitPdf(note), note.TypeName);
                    }
                    break;

                case (int)DisciplineTasks.UAPWoundCareVisit:
                    {
                        file = FileGenerator.Pdf<UAPWoundCareVisitPdf>(new UAPWoundCareVisitPdf(note), note.TypeName);
                    }
                    break;

                case (int)DisciplineTasks.UAPInsulinPrepAdminVisit:
                    {
                        file = FileGenerator.Pdf<UAPInsulinPrepAdminVisitPdf>(new UAPInsulinPrepAdminVisitPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.SixtyDaySummary:
                    {
                        file = FileGenerator.Pdf<SixtyDaySummaryPdf>(new SixtyDaySummaryPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.TransferSummary:
                case (int)DisciplineTasks.CoordinationOfCare:
                    {
                        file = FileGenerator.Pdf<TransferSummaryPdf>(new TransferSummaryPdf(note), note.TypeName);
                    }
                    break;
                case (int)DisciplineTasks.DriverOrTransportationNote:
                    {
                        file = FileGenerator.Pdf<DriverOrTransportationNotePdf>(new DriverOrTransportationNotePdf(note), note.TypeName);
                    }
                    break;
            }
            return file;
        }

        #endregion

    }

}
