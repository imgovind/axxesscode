﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Linq;

    using Telerik.Web.Mvc;

    using NPOI.HPSF;
    using NPOI.HSSF.UserModel;

    using Axxess.Core;
    using Axxess.Core.Enums;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Services;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.ViewData;
    using Axxess.AgencyManagement.Enums;
    using NPOI.SS.UserModel;


    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ReportController : BaseController
    {
        #region Constructor / Member

        private readonly IAgencyService agencyService;
        private readonly IReportService reportService;
        private readonly IPatientService patientService;
        private readonly IUserRepository userRepository;
        private readonly IBillingService billingService;
        private readonly IUserService userService;
        private readonly IPatientRepository patientRepository;
        private readonly IBillingRepository billingRepository;
        private readonly IAgencyRepository agencyRepository;
        public ReportController(IAgencyManagementDataProvider dataProvider, IReportService reportService, IPatientService patientService, IBillingService billingService, IAgencyService agencyService, IUserService userService)
        {
            Check.Argument.IsNotNull(reportService, "reportService");
            this.agencyService = agencyService;
            this.reportService = reportService;
            this.patientService = patientService;
            this.billingService = billingService;
            this.userService = userService;
            this.userRepository = dataProvider.UserRepository;
            this.patientRepository = dataProvider.PatientRepository;
            this.billingRepository = dataProvider.BillingRepository;
            this.agencyRepository = dataProvider.AgencyRepository;
        }

        #endregion

        #region ReportController Actions

        #region General Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Center()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Dashboard()
        {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Patient()
        {
            return PartialView("Patient/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Clinical()
        {
            return PartialView("Clinical/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Schedule()
        {
            return PartialView("Schedule/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Billing()
        {
            return PartialView("Billing/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Employee()
        {
            return PartialView("Employee/Home");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Statistical()
        {
            return PartialView("Statistical/Home");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Completed()
        {
            return PartialView("Completed");
        }

        [GridAction]
        public ActionResult CompletedList()
        {
            var reports = agencyRepository.GetReports(Current.AgencyId);
            reports.ForEach(r =>
            {
                r.Name = r.Status.IsEqual("Completed") && !r.AssetId.IsEmpty() ? string.Format("<a href=\"/Asset/{0}\">{1}</a>", r.AssetId, r.Name) : r.Name;
            });
            return View(new GridModel(reports));
        }

        #endregion

        #region Patient Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientRoster()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/Roster", reportService.GetPatientRoster(Guid.Empty, 1, 0,false));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientRosterContent(Guid BranchCode, int StatusId, int InsuranceId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/RosterContent", reportService.GetPatientRoster(BranchCode, StatusId, InsuranceId, false));
        }

        public ActionResult ExportPatientRoster(Guid BranchCode, int StatusId, int InsuranceId)
        {
            var patientRosters = reportService.GetPatientRoster(BranchCode, StatusId, InsuranceId, true);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Roster";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientRoster");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Roster");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR #");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Medicare #");
            headerRow.CreateCell(3).SetCellValue("Address");
            headerRow.CreateCell(4).SetCellValue("City");
            headerRow.CreateCell(5).SetCellValue("State");
            headerRow.CreateCell(6).SetCellValue("Zip Code");
            headerRow.CreateCell(7).SetCellValue("Home Phone");
            headerRow.CreateCell(8).SetCellValue("Gender");
            headerRow.CreateCell(9).SetCellValue("Triage");
            headerRow.CreateCell(10).SetCellValue("Date of Birth");
            headerRow.CreateCell(11).SetCellValue("Start of Care Date");
            headerRow.CreateCell(12).SetCellValue("Discharge Date");
            headerRow.CreateCell(13).SetCellValue("Physician");
            headerRow.CreateCell(14).SetCellValue("Physician NPI");
            headerRow.CreateCell(15).SetCellValue("Physician Phone");
            sheet.CreateFreezePane(0, 2, 0, 2);

            int rowNumber = 2;

            foreach (var patient in patientRosters)
            {
                var row = sheet.CreateRow(rowNumber++);
                row.CreateCell(0).SetCellValue(patient.PatientId);
                row.CreateCell(1).SetCellValue(patient.PatientDisplayName);
                row.CreateCell(2).SetCellValue(patient.PatientMedicareNumber);
                row.CreateCell(3).SetCellValue(patient.PatientAddressLine1);
                row.CreateCell(4).SetCellValue(patient.PatientAddressCity);
                row.CreateCell(5).SetCellValue(patient.PatientAddressStateCode);
                row.CreateCell(6).SetCellValue(patient.PatientAddressZipCode);
                row.CreateCell(7).SetCellValue(patient.PatientPhone);
                row.CreateCell(8).SetCellValue(patient.PatientGender);
                row.CreateCell(9).SetCellValue(patient.Triage);
                row.CreateCell(10).SetCellValue(patient.PatientDOB.ToString("MM/dd/yyyy"));
                row.CreateCell(11).SetCellValue(patient.PatientSoC.ToZeroFilled());
                row.CreateCell(12).SetCellValue(patient.PatientDischargeDate.ToZeroFilled());
                row.CreateCell(13).SetCellValue(patient.PhysicianName);
                row.CreateCell(14).SetCellValue(patient.PhysicianNpi);
                row.CreateCell(15).SetCellValue(patient.PhysicianPhone);
            }
            var totalRow = sheet.CreateRow(rowNumber++);
            totalRow.CreateCell(0).SetCellValue(string.Format("Total Number of Patients: {0}", patientRosters.Count));
            int columnCounter = 0;
            do
            {
                sheet.AutoSizeColumn(columnCounter);
                columnCounter++;
            }
            while (columnCounter < 16);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientRoster_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Cahps()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Patient/Cahps");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientEmergencyList()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/EmergencyList", reportService.GetPatientEmergencyContacts(Guid.Empty ,1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientEmergencyListContent(Guid BranchCode, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/EmergencyListContent", reportService.GetPatientEmergencyContacts(BranchCode ,StatusId));
        }

        public ActionResult ExportPatientEmergencyList(Guid BranchCode, int StatusId)
        {
            var emergencyContactInfos = reportService.GetPatientEmergencyContacts(BranchCode ,StatusId);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Emergency List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientEmergencyList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Emergency List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Triage");
            headerRow.CreateCell(3).SetCellValue("Contact");
            headerRow.CreateCell(4).SetCellValue("Relationship");
            headerRow.CreateCell(5).SetCellValue("Contact Phone");
            headerRow.CreateCell(6).SetCellValue("Contact E-mail");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (emergencyContactInfos != null && emergencyContactInfos.Count > 0)
            {
                int rowNumber = 2;
                foreach (var emergencyContactInfo in emergencyContactInfos)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(emergencyContactInfo.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(emergencyContactInfo.PatientName);
                    row.CreateCell(2).SetCellValue(emergencyContactInfo.Triage);
                    row.CreateCell(3).SetCellValue(emergencyContactInfo.ContactName);
                    row.CreateCell(4).SetCellValue(emergencyContactInfo.ContactRelation);
                    row.CreateCell(5).SetCellValue(emergencyContactInfo.ContactPhoneHome);
                    row.CreateCell(6).SetCellValue(emergencyContactInfo.ContactEmailAddress);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Emergency List: {0}", emergencyContactInfos.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            var output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientEmergencyList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientBirthdayList()
        {
            ViewData["SortColumn"] = "Name";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/BirthdayList", patientRepository.GetPatientBirthdays(Current.AgencyId, Guid.Empty, DateTime.Now.Month));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientBirthdayListContent(Guid BranchCode, int Month, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/BirthdayListContent", patientRepository.GetPatientBirthdays(Current.AgencyId, BranchCode, Month));
        }

        public ActionResult ExportPatientBirthdayList(Guid BranchCode, int Month)
        {
            var birthDays = patientRepository.GetPatientBirthdays(Current.AgencyId, BranchCode, Month);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Birthday List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientBirthdayList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Birthday List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (new DateTime(DateTime.Now.Year, Month, 1).ToString("MMMM"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Age");
            headerRow.CreateCell(3).SetCellValue("Birth Day");
            headerRow.CreateCell(4).SetCellValue("Address First Row");
            headerRow.CreateCell(5).SetCellValue("Address Second Row");
            headerRow.CreateCell(6).SetCellValue("Home Phone");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (birthDays != null && birthDays.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in birthDays)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.IdNumber);
                    row.CreateCell(1).SetCellValue(birthDay.Name);
                    row.CreateCell(2).SetCellValue(birthDay.Age);
                    row.CreateCell(3).SetCellValue(birthDay.BirthDay);
                    row.CreateCell(4).SetCellValue(birthDay.AddressFirstRow);
                    row.CreateCell(5).SetCellValue(birthDay.AddressSecondRow);
                    row.CreateCell(6).SetCellValue(birthDay.PhoneHome);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Birthday List: {0}", birthDays.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            var output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientBirthdayList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientAddressList()
        {
            ViewData["SortColumn"] = "Name";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/AddressList", patientRepository.GetPatientAddressListing(Current.AgencyId, Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientAddressListContent(Guid BranchCode, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/AddressListContent", patientRepository.GetPatientAddressListing(Current.AgencyId, BranchCode, StatusId));
        }

        public ActionResult ExportPatientAddressList(Guid BranchCode, int StatusId)
        {
            var addressBookEntries = patientRepository.GetPatientAddressListing(Current.AgencyId, BranchCode, StatusId);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Address List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientAddressList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Address List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Address");
            headerRow.CreateCell(3).SetCellValue("City");
            headerRow.CreateCell(4).SetCellValue("State");
            headerRow.CreateCell(5).SetCellValue("Zip Code");
            headerRow.CreateCell(6).SetCellValue("Home Phone");
            headerRow.CreateCell(7).SetCellValue("Home Phone");
            headerRow.CreateCell(8).SetCellValue("Email Address");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (addressBookEntries != null && addressBookEntries.Count > 0)
            {
                int rowNumber = 2;
                foreach (var addressBook in addressBookEntries)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(addressBook.IdNumber);
                    row.CreateCell(1).SetCellValue(addressBook.Name);
                    row.CreateCell(2).SetCellValue(addressBook.AddressFirstRow);
                    row.CreateCell(3).SetCellValue(addressBook.AddressCity);
                    row.CreateCell(4).SetCellValue(addressBook.AddressStateCode);
                    row.CreateCell(5).SetCellValue(addressBook.AddressZipCode);
                    row.CreateCell(6).SetCellValue(addressBook.PhoneHome);
                    row.CreateCell(7).SetCellValue(addressBook.PhoneMobile);
                    row.CreateCell(8).SetCellValue(addressBook.EmailAddress);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Address List: {0}", addressBookEntries.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            var output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientBirthdayList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientByPhysicians()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/Physician", new List<PatientRoster>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientByPhysiciansContent(Guid? PhysicianId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            var Id = PhysicianId.HasValue ? (Guid)PhysicianId : Guid.Empty;
            return PartialView("Patient/Content/Physician", Id.IsEmpty() ? new List<PatientRoster>() : patientRepository.GetPatientByPhysician(Current.AgencyId, Id));
        }

        public ActionResult ExportPatientByPhysicians(Guid? PhysicianId)
        {
            var Id = PhysicianId.HasValue ? (Guid)PhysicianId : Guid.Empty;
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patients By Physician";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientByPhysicians");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patients By Physician");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            if (!Id.IsEmpty())
            {
                var physician = PhysicianEngine.Get(Id, Current.AgencyId);
                if (physician != null)
                {
                    titleRow.CreateCell(3).SetCellValue(string.Format("Physician : {0}", physician.DisplayName));
                }
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient");
            headerRow.CreateCell(1).SetCellValue("Address");
            headerRow.CreateCell(2).SetCellValue("City");
            headerRow.CreateCell(3).SetCellValue("State");
            headerRow.CreateCell(4).SetCellValue("Zip Code");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            headerRow.CreateCell(6).SetCellValue("Patient Gender");
            sheet.CreateFreezePane(0, 1, 0, 1);

            var patientRosters = patientRepository.GetPatientByPhysician(Current.AgencyId, Id);
            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientDisplayName);
                    row.CreateCell(1).SetCellValue(patient.PatientAddressLine1);
                    row.CreateCell(2).SetCellValue(patient.PatientAddressCity);
                    row.CreateCell(3).SetCellValue(patient.PatientAddressStateCode);
                    row.CreateCell(4).SetCellValue(patient.PatientAddressZipCode);
                    row.CreateCell(5).SetCellValue(patient.PatientPhone);
                    row.CreateCell(6).SetCellValue(patient.PatientGender);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patients By Physician: {0}", patientRosters.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientByPhysicians_{0}.xls", DateTime.Now.Ticks));
        }

        [GridAction]
        public JsonResult PatientBirthdayWidget()
        {
            var viewData = new List<Birthday>();
            viewData = reportService.GetCurrentBirthdays();
            return Json(new GridModel(viewData));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSocCertPeriodListing()
        {
            ViewData["SortColumn"] = "PatientLastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/PatientSocCertPeriodListing", reportService.GetPatientSocCertPeriod(Guid.Empty, 1, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientSocCertPeriodListingContent(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/PatientSocCertPeriodListingContent", reportService.GetPatientSocCertPeriod(BranchCode, StatusId, StartDate, EndDate));
        }

        public ActionResult ExportPatientSocCertPeriodListing(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient SOC Cert. Period Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientSocCertPeriodListing");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient SOC Cert. Period Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("SOC Date");
            headerRow.CreateCell(4).SetCellValue("SOC Cert. Period");
            headerRow.CreateCell(5).SetCellValue("Physician Name");
            headerRow.CreateCell(6).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var patientSocCertPeriods = reportService.GetPatientSocCertPeriod(BranchCode, StatusId, StartDate, EndDate);
            if (patientSocCertPeriods != null && patientSocCertPeriods.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patientSocCertPeriod in patientSocCertPeriods)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patientSocCertPeriod.PatientPatientID);
                    row.CreateCell(1).SetCellValue(patientSocCertPeriod.PatientLastName);
                    row.CreateCell(2).SetCellValue(patientSocCertPeriod.PatientFirstName);
                    row.CreateCell(3).SetCellValue(patientSocCertPeriod.PatientSoC);
                    row.CreateCell(4).SetCellValue(patientSocCertPeriod.SocCertPeriod);
                    row.CreateCell(5).SetCellValue(patientSocCertPeriod.PhysicianName);
                    row.CreateCell(6).SetCellValue(patientSocCertPeriod.respEmp);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient SOC Cert. Period Listing: {0}", patientSocCertPeriods.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientSocCertPeriodListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientByResponsibleEmployeeListing()
        {
            ViewData["SortColumn"] = "PatientLastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/PatientByResponsibleEmployeeListing", new List<PatientRoster>());
        }

        [GridAction]
        public ActionResult PatientByResponsibleEmployeeListingContent(Guid BranchCode, Guid UserId, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/PatientByResponsibleEmployeeListingContent", UserId.IsEmpty() ? new List<PatientRoster>() : patientRepository.GetPatientByResponsiableEmployee(Current.AgencyId, UserId, BranchCode, StatusId));
        }

        public ActionResult ExportPatientByResponsibleEmployeeListing(Guid BranchCode, Guid UserId, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient By Responsible Employee Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientByResponsibleEmployeeListing");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient By Responsible Employee Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Employee : {0}", UserEngine.GetName(UserId, Current.AgencyId)));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Address");
            headerRow.CreateCell(4).SetCellValue("SOC Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (UserId.IsEmpty())
            {
                MemoryStream outputNoUser = new MemoryStream();
                workbook.Write(outputNoUser);
                return File(outputNoUser.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleEmployeeListing_{0}.xls", DateTime.Now.Ticks));
            }

            var patientRosters = patientRepository.GetPatientByResponsiableEmployee(Current.AgencyId, UserId, BranchCode, StatusId);
            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientId);
                    row.CreateCell(1).SetCellValue(patient.PatientLastName);
                    row.CreateCell(2).SetCellValue(patient.PatientFirstName);
                    row.CreateCell(3).SetCellValue(patient.AddressFull);
                    row.CreateCell(4).SetCellValue(patient.PatientSoC);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient By Responsible Employee Listing: {0}", patientRosters.Count));

            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleEmployeeListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientByResponsibleCaseManagerListing()
        {
            ViewData["SortColumn"] = "PatientLastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/PatientByResponsibleCaseManager", new List<PatientRoster>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientByResponsibleCaseManagerContent(Guid BranchCode, Guid UserId, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }

            return PartialView("Patient/Content/PatientByResponsibleCaseManagerContent", UserId.IsEmpty() ? new List<PatientRoster>() : patientRepository.GetPatientByResponsiableByCaseManager(Current.AgencyId, UserId, BranchCode, StatusId));
        }

        public ActionResult ExportPatientByResponsibleCaseManager(Guid BranchCode, Guid UserId, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient By Responsible CaseManager";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientByResponsibleCaseManager");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient By Responsible CaseManager");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Case Manager : {0}", UserEngine.GetName(UserId, Current.AgencyId)));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Address");
            headerRow.CreateCell(4).SetCellValue("SOC Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (UserId.IsEmpty())
            {
                MemoryStream outputNoUser = new MemoryStream();
                workbook.Write(outputNoUser);
                return File(outputNoUser.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleCaseManager_{0}.xls", DateTime.Now.Ticks));
            }

            var patientRosters = patientRepository.GetPatientByResponsiableByCaseManager(Current.AgencyId, UserId, BranchCode, StatusId);

            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientId);
                    row.CreateCell(1).SetCellValue(patient.PatientLastName);
                    row.CreateCell(2).SetCellValue(patient.PatientFirstName);
                    row.CreateCell(3).SetCellValue(patient.AddressFull);
                    row.CreateCell(4).SetCellValue(patient.PatientSoC);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient By Responsible CaseManager: {0}", patientRosters.Count));

            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientByResponsibleCaseManager_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientExpiringAuthorizations()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/ExpiringAuthorizations", reportService.GetExpiringAuthorizaton(Guid.Empty, 1));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpiringAuthorizationsContent(Guid BranchCode, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/ExpiringAuthorizationsContent", reportService.GetExpiringAuthorizaton(BranchCode, StatusId));
        }

        public ActionResult ExportExpiringAuthorizations(Guid BranchCode, int StatusId)
        {
            var autorizations = reportService.GetExpiringAuthorizaton(BranchCode, StatusId);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Expiring Authorizations";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ExpiringAuthorizations");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Expiring Authorizations");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Number");
            headerRow.CreateCell(3).SetCellValue("Start Date");
            headerRow.CreateCell(4).SetCellValue("End Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (autorizations != null && autorizations.Count > 0)
            {
                int rowNumber = 2;
                foreach (var auto in autorizations)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(auto.DisplayName);
                    row.CreateCell(1).SetCellValue(auto.Status);
                    row.CreateCell(2).SetCellValue(auto.Number1);
                    row.CreateCell(3).SetCellValue(auto.StartDateFormatted);
                    row.CreateCell(4).SetCellValue(auto.EndDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Expiring Authorizations: {0}", autorizations.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ExpiringAuthorizations_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSurveyCensus()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/SurveyCensus", reportService.GetPatientSurveyCensus(Guid.Empty, 1,0, false));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientSurveyCensusContent(Guid BranchCode, int StatusId, int InsuranceId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/SurveyCensusContent", reportService.GetPatientSurveyCensus(BranchCode, StatusId, InsuranceId, false));
        }

        public ActionResult ExportPatientSurveyCensus(Guid BranchCode, int StatusId, int InsuranceId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Survey Census";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientSurveyCensus");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Survey Census");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR #");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Medicare #");
            headerRow.CreateCell(3).SetCellValue("SSN");
            headerRow.CreateCell(4).SetCellValue("DOB");
            headerRow.CreateCell(5).SetCellValue("SOC Date");
            headerRow.CreateCell(6).SetCellValue("Cert. Period");
            headerRow.CreateCell(7).SetCellValue("Insurance");
            headerRow.CreateCell(8).SetCellValue("Primary Diagnosis");
            headerRow.CreateCell(9).SetCellValue("Secondary Diagnosis");
            headerRow.CreateCell(10).SetCellValue("Triage");
            headerRow.CreateCell(11).SetCellValue("Disciplines");
            headerRow.CreateCell(12).SetCellValue("Phone");
            headerRow.CreateCell(13).SetCellValue("Address Line1");
            headerRow.CreateCell(14).SetCellValue("Address Line2");
            headerRow.CreateCell(15).SetCellValue("Physician");
            headerRow.CreateCell(16).SetCellValue("NPI");
            headerRow.CreateCell(17).SetCellValue("Physician Phone");
            headerRow.CreateCell(18).SetCellValue("Physician Fax");
            headerRow.CreateCell(19).SetCellValue("Case Manager");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var surveyCensuses = reportService.GetPatientSurveyCensus(BranchCode, StatusId,InsuranceId, true);
            if (surveyCensuses != null && surveyCensuses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var surveyCensus in surveyCensuses)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(surveyCensus.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(surveyCensus.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(surveyCensus.MedicareNumber);
                    row.CreateCell(3).SetCellValue(surveyCensus.SSN);
                    row.CreateCell(4).SetCellValue(surveyCensus.DOB.ToString("MM/dd/yyyy"));
                    row.CreateCell(5).SetCellValue(surveyCensus.SOC.ToString("MM/dd/yyyy"));
                    row.CreateCell(6).SetCellValue(surveyCensus.CertPeriod);
                    row.CreateCell(7).SetCellValue(surveyCensus.InsuranceName);
                    row.CreateCell(8).SetCellValue(surveyCensus.PrimaryDiagnosis);
                    row.CreateCell(9).SetCellValue(surveyCensus.SecondaryDiagnosis);
                    row.CreateCell(10).SetCellValue(surveyCensus.Triage);
                    row.CreateCell(11).SetCellValue(surveyCensus.Discipline);
                    row.CreateCell(12).SetCellValue(surveyCensus.Phone);
                    row.CreateCell(13).SetCellValue(surveyCensus.DisplayAddressLine1);
                    row.CreateCell(14).SetCellValue(surveyCensus.DisplayAddressLine2);
                    row.CreateCell(15).SetCellValue(surveyCensus.PhysicianDisplayName);
                    row.CreateCell(16).SetCellValue(surveyCensus.PhysicianNPI);
                    row.CreateCell(17).SetCellValue(surveyCensus.PhysicianPhone );
                    row.CreateCell(18).SetCellValue(surveyCensus.PhysicianFax);
                    row.CreateCell(19).SetCellValue(surveyCensus.CaseManagerDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Survey Census: {0}", surveyCensuses.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
            sheet.AutoSizeColumn(12);
            sheet.AutoSizeColumn(13);
            sheet.AutoSizeColumn(14);
            sheet.AutoSizeColumn(15);
            sheet.AutoSizeColumn(16);
            sheet.AutoSizeColumn(17);
            sheet.AutoSizeColumn(18);
            sheet.AutoSizeColumn(19);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientSurveyCensus_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientVitalSigns()
        {
            ViewData["SortColumn"] = "VisitDate";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/VitalSigns", new List<VitalSign>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientVitalSignsContent(Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/VitalSignsContent",PatientId.IsEmpty() ? new List<VitalSign>() : patientService.GetPatientVitalSigns(PatientId, StartDate, EndDate));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PatientSixtyDaySummary()
        {
            ViewData["SortColumn"] = "UserDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/SixtyDaySummary", new List<VisitNoteViewData>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientSixtyDaySummaryContent(Guid PatientId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/SixtyDaySummaryContent", PatientId.IsEmpty() ? new List<VisitNoteViewData>() : patientService.GetSixtyDaySummary(PatientId));
        }

        public ActionResult ExportPatientSixtyDaySummary(Guid PatientId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Sixty Day Summary";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("SixtyDaySummary");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Sixty Day Summary");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee Name");
            headerRow.CreateCell(1).SetCellValue("Visit Date");
            headerRow.CreateCell(2).SetCellValue("Signature Date");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("Physician Name");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var sixtyDaySummaries = patientService.GetSixtyDaySummary(PatientId);
            if (sixtyDaySummaries != null && sixtyDaySummaries.Count > 0)
            {
                int rowNumber = 2;
                foreach (var sixtyDaySummary in sixtyDaySummaries)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(sixtyDaySummary.UserDisplayName);
                    row.CreateCell(1).SetCellValue(sixtyDaySummary.VisitDate);
                    row.CreateCell(2).SetCellValue(sixtyDaySummary.SignatureDate);
                    row.CreateCell(3).SetCellValue(sixtyDaySummary.EpisodeRange);
                    row.CreateCell(4).SetCellValue(sixtyDaySummary.PhysicianDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Sixty Day Summary: {0}", sixtyDaySummaries.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientSixtyDaySummary_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargePatients()
        {
            ViewData["SortColumn"] = "LastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Patient/DischargePatients", patientRepository.GetDischargePatients(Current.AgencyId, Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DischargePatientsContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Patient/Content/DischargePatientsContent",patientRepository.GetDischargePatients(Current.AgencyId, BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportDischargePatients(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Discharge Patients";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("DischargePatients");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Discharge Patients");
            titleRow.CreateCell(2).SetCellValue(string.Format("Date Range : {0} - {1} ", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("SOC Date");
            headerRow.CreateCell(4).SetCellValue("Discharge Date");
            headerRow.CreateCell(5).SetCellValue("Discharge Reason");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var dischargePatients = patientRepository.GetDischargePatients(Current.AgencyId, BranchCode, StartDate, EndDate);
            if (dischargePatients != null && dischargePatients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in dischargePatients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(patient.LastName);
                    row.CreateCell(2).SetCellValue(patient.FirstName);
                    row.CreateCell(3).SetCellValue(patient.StartofCareDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(4).SetCellValue(patient.DischargeDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(5).SetCellValue(patient.DischargeReason);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Discharge Patients: {0}", dischargePatients.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("DischargePatients_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Clinical Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalOpenOasis()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/OpenOasis",reportService.GetAllOpenOasis(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalOpenOasisContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/OpenOasis", reportService.GetAllOpenOasis(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportClinicalOpenOasis(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Open OASIS";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("OpenOASIS");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Open OASIS");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("MR#");
            headerRow.CreateCell(2).SetCellValue("Assessment Type");
            headerRow.CreateCell(3).SetCellValue("Date");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var openOasis = reportService.GetAllOpenOasis(BranchCode, StartDate, EndDate);
            if (openOasis != null && openOasis.Count > 0)
            {
                int rowNumber = 2;
                foreach (var oasis in openOasis)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(oasis.PatientName);
                    row.CreateCell(1).SetCellValue(oasis.PatientIdNumber);
                    row.CreateCell(2).SetCellValue(oasis.AssessmentName);
                    row.CreateCell(3).SetCellValue(oasis.Date);
                    row.CreateCell(4).SetCellValue(oasis.Status);
                    row.CreateCell(5).SetCellValue(oasis.CurrentlyAssigned);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Open OASIS: {0}", openOasis.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalOpenOASIS_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalMissedVisit()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/MissedVisit", reportService.GetAllMissedVisit(Guid.Empty,  DateTime.Now.AddDays(-60),  DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalMissedVisitContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/MissedVisit", reportService.GetAllMissedVisit(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportClinicalMissedVisit(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Missed Visit";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("MissedVisit");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Missed Visit");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var missedVisits = reportService.GetAllMissedVisit(BranchCode, StartDate, EndDate);
            if (missedVisits != null && missedVisits.Count > 0)
            {
                int rowNumber = 2;
                foreach (var missedVisit in missedVisits)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(missedVisit.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(missedVisit.PatientName);
                    row.CreateCell(2).SetCellValue(missedVisit.Date.ToShortDateString());
                    row.CreateCell(3).SetCellValue(missedVisit.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(missedVisit.UserName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Missed Visit: {0}", missedVisits.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalMissedVisit_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalPhysicianOrderHistory()
        {
            ViewData["SortColumn"] = "OrderNumber";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/OrderHistory", reportService.GetPhysicianOrderHistory(Guid.Empty, 000, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalPhysicianOrderHistoryContent(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            StartDate = StatusId == (int)ScheduleStatus.OrderNotYetDue ? DateTime.Now : StartDate;
            StatusId = StatusId == (int)ScheduleStatus.OrderNotYetStarted ? (int)ScheduleStatus.OrderNotYetDue : StatusId;
            return PartialView("Clinical/Content/OrderHistory", reportService.GetPhysicianOrderHistory(BranchCode, StatusId, StartDate, EndDate));
        }

        public ActionResult ExportClinicalPhysicianOrderHistory(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Physician Order History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PhysicianOrderHistory");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Physician Order History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Order Number");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Physician Name");
            headerRow.CreateCell(3).SetCellValue("Order Date");
            headerRow.CreateCell(4).SetCellValue("Sent Date");
            headerRow.CreateCell(5).SetCellValue("Received Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            StartDate = StatusId == (int)ScheduleStatus.OrderNotYetDue ? DateTime.Now : StartDate;
            StatusId = StatusId == (int)ScheduleStatus.OrderNotYetStarted ? (int)ScheduleStatus.OrderNotYetDue : StatusId;
            var physicianOrders = reportService.GetPhysicianOrderHistory(BranchCode, StatusId, StartDate, EndDate).ToList();
            if (physicianOrders != null && physicianOrders.Count > 0)
            {
                int rowNumber = 2;
                foreach (var physicianOrder in physicianOrders)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(physicianOrder.OrderNumber);
                    row.CreateCell(1).SetCellValue(physicianOrder.DisplayName);
                    row.CreateCell(2).SetCellValue(physicianOrder.PhysicianName);
                    row.CreateCell(3).SetCellValue(physicianOrder.OrderDateFormatted);
                    row.CreateCell(4).SetCellValue(physicianOrder.SentDateFormatted);
                    row.CreateCell(5).SetCellValue(physicianOrder.ReceivedDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Physician Order History: {0}", physicianOrders.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalPhysicianOrderHistory_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalPlanOfCareHistory()
        {
            ViewData["SortColumn"] = "Number";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/PlanOfCareHistory", reportService.GetPlanOfCareHistory(Guid.Empty, 000, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalPlanOfCareHistoryContent(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            StartDate = StatusId == (int)ScheduleStatus.OrderNotYetDue ? DateTime.Now : StartDate;
            StatusId = StatusId == (int)ScheduleStatus.OrderNotYetStarted ? (int)ScheduleStatus.OrderNotYetDue : StatusId;
            return PartialView("Clinical/Content/PlanOfCareHistory", reportService.GetPlanOfCareHistory(BranchCode, StatusId, StartDate, EndDate));
        }

        public ActionResult ExportClinicalPlanOfCareHistory(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Plan Of Care History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PlanOfCareHistory");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Plan Of Care History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Order Number");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Physician Name");
            headerRow.CreateCell(3).SetCellValue("Order Date");
            headerRow.CreateCell(4).SetCellValue("Sent Date");
            headerRow.CreateCell(5).SetCellValue("Received Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            StartDate = StatusId == (int)ScheduleStatus.OrderNotYetDue ? DateTime.Now : StartDate;
            StatusId = StatusId == (int)ScheduleStatus.OrderNotYetStarted ? (int)ScheduleStatus.OrderNotYetDue : StatusId;
            var planOfCareHistories = reportService.GetPlanOfCareHistory(BranchCode, StatusId, StartDate, EndDate).ToList();
            if (planOfCareHistories != null && planOfCareHistories.Count > 0)
            {
                int rowNumber = 2;
                CellStyle dateStyle = workbook.CreateCellStyle();
                dateStyle.DataFormat = workbook.CreateDataFormat().GetFormat("mm/dd/yyyy");
                foreach (var planOfCareHistory in planOfCareHistories)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(planOfCareHistory.Number);
                    row.CreateCell(1).SetCellValue(planOfCareHistory.PatientName);
                    row.CreateCell(2).SetCellValue(planOfCareHistory.PhysicianName);

                    if (planOfCareHistory.CreatedDate.IsNotNullOrEmpty() && planOfCareHistory.CreatedDate.IsDate())
                    {
                        var createdDateCell = row.CreateCell(3);
                        createdDateCell.CellStyle = dateStyle;
                        createdDateCell.SetCellValue(planOfCareHistory.CreatedDate.ToDateTime());
                    }
                    else
                    {
                        row.CreateCell(3).SetCellValue(string.Empty);
                    }                    
                    if (planOfCareHistory.SendDate != DateTime.MinValue)
                    {
                        var sendDateCell = row.CreateCell(4);

                        sendDateCell.CellStyle = dateStyle;
                        sendDateCell.SetCellValue(planOfCareHistory.SendDate);
                    }
                    else
                    {
                        row.CreateCell(4).SetCellValue(string.Empty);
                    }      
                    if (planOfCareHistory.ReceivedDate != DateTime.MinValue)
                    {
                        var receiveDateCell = row.CreateCell(5);
                        receiveDateCell.CellStyle = dateStyle;
                        receiveDateCell.SetCellValue(planOfCareHistory.ReceivedDate);
                    }
                    else
                    {
                        row.CreateCell(5).SetCellValue(string.Empty);
                    }                    
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Plan Of Care History: {0}", planOfCareHistories.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ClinicalPlanOfCareHistory_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalThirteenAndNineteenVisitException()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/ThirteenAndNineteenVisitException", patientService.GetTherapyException(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalThirteenAndNineteenVisitExceptionContent(Guid BranchCode, string SortParams, DateTime StartDate, DateTime EndDate)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/ThirteenAndNineteenVisitException", patientService.GetTherapyException(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportClinicalThirteenAndNineteenVisitException(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Thirteen And Nineteen Visit Exception";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ThirteenAndNineteen VisitException");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Thirteen And Nineteen Visit Exception");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Episode Start Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Scheduled");
            headerRow.CreateCell(4).SetCellValue("Completed");
            headerRow.CreateCell(5).SetCellValue("Day");
            headerRow.CreateCell(6).SetCellValue("13th Visit");
            headerRow.CreateCell(7).SetCellValue("19th Visit");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var visitExceptions = patientService.GetTherapyException(BranchCode,StartDate,EndDate);
            if (visitExceptions != null && visitExceptions.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visitException in visitExceptions)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(visitException.PatientName);
                    row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                    row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                    row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                    row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                    row.CreateCell(6).SetCellValue(visitException.ThirteenVisit);
                    row.CreateCell(7).SetCellValue(visitException.NineteenVisit);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Thirteen And Nineteen Visit Exception: {0}", visitExceptions.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ThirteenAndNineteenVisitException_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalThirteenTherapyReevaluationException()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/ThirteenTherapyReevaluationException", patientService.GetTherapyReevaluationException(Guid.Empty, 13, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalThirteenTherapyReevaluationExceptionContent(Guid BranchCode, string SortParams, DateTime StartDate, DateTime EndDate)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/ThirteenTherapyReevaluationException", patientService.GetTherapyReevaluationException(BranchCode, 13,StartDate,EndDate));
        }

        public ActionResult ExportClinicalThirteenTherapyReevaluationException(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Thirteen And Nineteen Visit Exception";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ThirteenAndNineteen VisitException");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Thirteen And Nineteen Visit Exception");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Episode Start Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Scheduled");
            headerRow.CreateCell(4).SetCellValue("Completed");
            headerRow.CreateCell(5).SetCellValue("Day");
            headerRow.CreateCell(6).SetCellValue("PT Re-eval");
            headerRow.CreateCell(7).SetCellValue("ST Re-eval");
            headerRow.CreateCell(8).SetCellValue("OT Re-eval");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var visitExceptions = patientService.GetTherapyReevaluationException(BranchCode, 13,StartDate,EndDate);
            if (visitExceptions != null && visitExceptions.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visitException in visitExceptions)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(visitException.PatientName);
                    row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                    row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                    row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                    row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                    row.CreateCell(6).SetCellValue(visitException.PTEval);
                    row.CreateCell(7).SetCellValue(visitException.STEval);
                    row.CreateCell(8).SetCellValue(visitException.OTEval);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Thirteen And Nineteen Visit Exception: {0}", visitExceptions.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ThirteenAndNineteenVisitException_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClinicalNineteenTherapyReevaluationException()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Clinical/NineteenTherapyReevaluationException", patientService.GetTherapyReevaluationException(Guid.Empty, 19, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClinicalNineteenTherapyReevaluationExceptionContent(Guid BranchCode, string SortParams, DateTime StartDate, DateTime EndDate)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Clinical/Content/NineteenTherapyReevaluationException", patientService.GetTherapyReevaluationException(BranchCode, 19,StartDate,EndDate));
        }

        public ActionResult ExportClinicalNineteenTherapyReevaluationException(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Thirteen And Nineteen Visit Exception";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ThirteenAndNineteen VisitException");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Thirteen And Nineteen Visit Exception");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Episode Start Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Name");
            headerRow.CreateCell(2).SetCellValue("Episode");
            headerRow.CreateCell(3).SetCellValue("Scheduled");
            headerRow.CreateCell(4).SetCellValue("Completed");
            headerRow.CreateCell(5).SetCellValue("Day");
            headerRow.CreateCell(6).SetCellValue("PT Re-eval");
            headerRow.CreateCell(7).SetCellValue("ST Re-eval");
            headerRow.CreateCell(8).SetCellValue("OT Re-eval");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var visitExceptions = patientService.GetTherapyReevaluationException(BranchCode, 19, StartDate, EndDate);
            if (visitExceptions != null && visitExceptions.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visitException in visitExceptions)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visitException.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(visitException.PatientName);
                    row.CreateCell(2).SetCellValue(visitException.EpisodeRange);
                    row.CreateCell(3).SetCellValue(visitException.ScheduledTherapy);
                    row.CreateCell(4).SetCellValue(visitException.CompletedTherapy);
                    row.CreateCell(5).SetCellValue(visitException.EpisodeDay);
                    row.CreateCell(6).SetCellValue(visitException.PTEval);
                    row.CreateCell(7).SetCellValue(visitException.STEval);
                    row.CreateCell(8).SetCellValue(visitException.OTEval);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Thirteen And Nineteen Visit Exception: {0}", visitExceptions.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ThirteenAndNineteenVisitException_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Schedule Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientWeeklySchedule()
        {
            ViewData["SortColumn"] = "UserName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/PatientWeeklySchedule", new List<ScheduleEvent>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientWeeklyScheduleContent(Guid PatientId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/PatientWeeklySchedule", PatientId.IsEmpty() ? new List<ScheduleEvent>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, DateTime.Today, DateTime.Today.AddDays(7)));
        }

        public ActionResult ExportPatientWeeklySchedule(Guid PatientId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Weekly Schedule";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("SchedulePatientWeekly");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Patient Weekly");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", DateTime.Today.ToString("MM/dd/yyyy"), DateTime.Today.AddDays(7).ToString("MM/dd/yyyy"))));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Employee");
            if (PatientId.IsEmpty())
            {
                MemoryStream outputEmpty = new MemoryStream();
                workbook.Write(outputEmpty);
                return File(outputEmpty.ToArray(), "application/vnd.ms-excel", string.Format("SchedulePatientWeekly_{0}.xls", DateTime.Now.Ticks));
            }
            sheet.CreateFreezePane(0, 2, 0, 2);
            var scheduleEvents = reportService.GetPatientScheduleEventsByDateRange(PatientId, DateTime.Today, DateTime.Today.AddDays(7));
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(1).SetCellValue(evnt.StatusName);
                    row.CreateCell(2).SetCellValue(evnt.EventDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(3).SetCellValue(evnt.VisitDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(4).SetCellValue(evnt.UserDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Patient Weekly: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("SchedulePatientWeekly_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientMonthlySchedule()
        {
            ViewData["SortColumn"] = "UserName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/PatientMonthlySchedule", new List<ScheduleEvent>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PatientMonthlyScheduleContent(Guid PatientId, int Month, int Year, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            if (PatientId.IsEmpty() || Month <= 0)
            {
                return PartialView("Schedule/Content/PatientMonthlySchedule", new List<ScheduleEvent>());
            }
            var fromDate = DateUtilities.GetStartOfMonth(Month, Year);
            var toDate = DateUtilities.GetEndOfMonth(Month, Year);
            return PartialView("Schedule/Content/PatientMonthlySchedule", reportService.GetPatientScheduleEventsByDateRange(PatientId, fromDate, toDate));
        }

        public ActionResult ExportPatientMonthlySchedule(Guid PatientId, int Month, int Year)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Monthly Schedule";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientMonthlySchedule");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Monthly Schedule");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (PatientId.IsEmpty() || Month <= 0)
            {
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientMonthlySchedule_{0}.xls", DateTime.Now.Ticks));
            }
            else
            {
                var fromDate = DateUtilities.GetStartOfMonth(Month, Year);
                var toDate = DateUtilities.GetEndOfMonth(Month, Year);
                var scheduleEvents = reportService.GetPatientScheduleEventsByDateRange(PatientId, fromDate, toDate);
                if (scheduleEvents != null && scheduleEvents.Count > 0)
                {
                    int rowNumber = 2;
                    foreach (var evnt in scheduleEvents)
                    {
                        var row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                        row.CreateCell(1).SetCellValue(evnt.StatusName);
                        row.CreateCell(2).SetCellValue(evnt.EventDate.ToString("MM/dd/yyyy"));
                        row.CreateCell(3).SetCellValue(evnt.VisitDate.ToString("MM/dd/yyyy"));
                        row.CreateCell(4).SetCellValue(evnt.UserDisplayName);
                        rowNumber++;
                    }
                    var totalRow = sheet.CreateRow(rowNumber + 2);
                    totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Monthly Schedule: {0}", scheduleEvents.Count));
                }
                sheet.AutoSizeColumn(0);
                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientMonthlySchedule_{0}.xls", DateTime.Now.Ticks));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeWeeklySchedule()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/EmployeeWeekly", new List<UserVisit>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeWeeklyScheduleContent(Guid BranchCode, Guid UserId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/EmployeeWeekly", UserId.IsEmpty() ? new List<UserVisit>() : reportService.GetUserScheduleEventsByDateRange(BranchCode, UserId, StartDate, EndDate));
        }

        public ActionResult ExportEmployeeWeeklySchedule(Guid BranchCode, Guid UserId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Schedule Employee Weekly";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ScheduleEmployeeWeekly");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Employee Weekly");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Status");
            headerRow.CreateCell(3).SetCellValue("Schedule Date");
            headerRow.CreateCell(4).SetCellValue("Visit Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var userEvents = reportService.GetUserScheduleEventsByDateRange(BranchCode, UserId, StartDate, EndDate);
            if (userEvents != null && userEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in userEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientName);
                    row.CreateCell(1).SetCellValue(evnt.TaskName);
                    row.CreateCell(2).SetCellValue(evnt.StatusName);
                    row.CreateCell(3).SetCellValue(evnt.ScheduleDateFormatted);
                    row.CreateCell(4).SetCellValue(evnt.VisitDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Employee Weekly: {0}", userEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleEmployeeWeekly_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult MonthlyWorkSchedule()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/MonthlyWork", new List<UserVisit>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MonthlyWorkScheduleContent(Guid BranchCode, Guid UserId, int Month, int Year, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            if (UserId.IsEmpty() || Month <= 0)
            {
                return PartialView("Schedule/Content/MonthlyWork", new List<UserVisit>());
            }
            var fromDate = DateUtilities.GetStartOfMonth(Month, Year);
            var toDate = DateUtilities.GetEndOfMonth(Month, Year);
            return PartialView("Schedule/Content/MonthlyWork", reportService.GetUserScheduleEventsByDateRange(BranchCode, UserId, fromDate, toDate));
        }

        public ActionResult ExportMonthlyWorkSchedule(Guid BranchCode, Guid UserId, int Month, int Year)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Schedule Monthly Work";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ScheduleMonthlyWork");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Monthly Work");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Status");
            headerRow.CreateCell(3).SetCellValue("Schedule Date");
            headerRow.CreateCell(4).SetCellValue("Visit Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (UserId.IsEmpty() || Month <= 0)
            {
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleMonthlyWork_{0}.xls", DateTime.Now.Ticks));
            }
            else
            {
                var fromDate = DateUtilities.GetStartOfMonth(Month, Year);
                var toDate = DateUtilities.GetEndOfMonth(Month, Year);
                var userEvents = reportService.GetUserScheduleEventsByDateRange(BranchCode, UserId, fromDate, toDate);
                if (userEvents != null && userEvents.Count > 0)
                {
                    int rowNumber = 2;
                    foreach (var evnt in userEvents)
                    {
                        var row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(evnt.PatientName);
                        row.CreateCell(1).SetCellValue(evnt.TaskName);
                        row.CreateCell(2).SetCellValue(evnt.StatusName);
                        row.CreateCell(3).SetCellValue(evnt.ScheduleDateFormatted);
                        row.CreateCell(4).SetCellValue(evnt.VisitDateFormatted);
                        rowNumber++;
                    }
                    var totalRow = sheet.CreateRow(rowNumber + 2);
                    totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Monthly Work: {0}", userEvents.Count));
                }
                sheet.AutoSizeColumn(0);
                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                MemoryStream output = new MemoryStream();
                workbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleMonthlyWork_{0}.xls", DateTime.Now.Ticks));
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PastDueVisitsSchedule()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/PastDueVisits", reportService.GetPastDueScheduleEvents(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueVisitsScheduleContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/PastDueVisits", reportService.GetPastDueScheduleEvents(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportPastDueVisitsSchedule(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Past Due Visits";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PastDueVisits");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Past Due Visits");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var scheduleEvents = reportService.GetPastDueScheduleEvents(BranchCode, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.EventDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(evnt.UserDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Visits: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("SchedulePastDueVisits_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PastDueVisitsByDisciplineSchedule()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/PastDueVisitsByDiscipline", reportService.GetPastDueScheduleEventsByDiscipline(Guid.Empty, Disciplines.Nursing.ToString(), DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueVisitsByDisciplineScheduleContent(Guid BranchCode, string Discipline, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/PastDueVisitsByDiscipline", reportService.GetPastDueScheduleEventsByDiscipline(BranchCode, Discipline, StartDate, EndDate));
        }

        public ActionResult ExportPastDueVisitsByDisciplineSchedule(Guid BranchCode, string Discipline, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Past Due Visits By Discipline";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PastDueVisitsByDiscipline");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Past Due Visits By Discipline");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy"))));
            titleRow.CreateCell(4).SetCellValue(string.Format("Discipline: {0}", Enum.IsDefined(typeof(Disciplines), Discipline) ? ((Disciplines)Enum.Parse(typeof(Disciplines), Discipline)).GetDescription() : string.Empty));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);

            var scheduleEvents = reportService.GetPastDueScheduleEventsByDiscipline(BranchCode, Discipline, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.EventDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(evnt.UserDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Visits By Discipline: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PastDueVisitsByDiscipline_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DailyWorkSchedule()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/DailyWork", reportService.GetScheduleEventsByDateRange(Guid.Empty, DateTime.Now, DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DailyWorkScheduleContent(Guid BranchCode, DateTime Date, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/DailyWork", reportService.GetScheduleEventsByDateRange(BranchCode, Date, Date));
        }

        public ActionResult ExportDailyWorkSchedule(DateTime Date, Guid BranchCode)
        {
            var scheduleEvents = reportService.GetScheduleEventsByDateRange(BranchCode, Date, Date);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Daily Work Schedule";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("DailyWorkSchedule");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Daily Work Schedule");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Task");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Schedule Date");
            headerRow.CreateCell(5).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(3).SetCellValue(evnt.StatusName);
                    row.CreateCell(4).SetCellValue(evnt.EventDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(5).SetCellValue(evnt.UserDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Daily Work Schedule: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("DailyWorkSchedule_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CaseManagerTaskSchedule()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/CaseManagerTask", reportService.GetCaseManagerScheduleByBranch(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CaseManagerTaskScheduleContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/CaseManagerTask", reportService.GetCaseManagerScheduleByBranch(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportCaseManagerTaskSchedule(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var scheduleEvents = reportService.GetCaseManagerScheduleByBranch(BranchCode, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Case Manager Task";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("CaseManagerTask");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Case Manager Task");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Task");
            headerRow.CreateCell(4).SetCellValue("Employee");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.PatientName);
                    row.CreateCell(2).SetCellValue(evnt.EventDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(3).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(4).SetCellValue(evnt.UserDisplayName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Case Manager Task: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("CaseManagerTask_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ScheduleDeviation()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/Deviation", reportService.GetScheduleDeviation(Guid.Empty, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleDeviationContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/Deviation", reportService.GetScheduleDeviation(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportScheduleDeviation(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var scheduleEvents = reportService.GetScheduleDeviation(BranchCode, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Schedule Deviation";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ScheduleDeviation");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Schedule Deviation");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR #");
            headerRow.CreateCell(1).SetCellValue("Task");
            headerRow.CreateCell(2).SetCellValue("Patient Name");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Employee");
            headerRow.CreateCell(5).SetCellValue("Schedule Date");
            headerRow.CreateCell(6).SetCellValue("Visit Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(2).SetCellValue(evnt.PatientName);
                    row.CreateCell(3).SetCellValue(evnt.StatusName);
                    row.CreateCell(4).SetCellValue(evnt.UserDisplayName);
                    row.CreateCell(5).SetCellValue(evnt.EventDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(6).SetCellValue(evnt.VisitDate.ToString("MM/dd/yyyy"));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Schedule Deviation: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("ScheduleDeviation_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PastDueRecetSchedule()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            var agency = agencyRepository.Get(Current.AgencyId);
            var locationId = Guid.Empty;
            var payorId = 0;
            if (agency != null)
            {
                locationId = agency.MainLocation != null ? agency.MainLocation.Id : Guid.Empty;
                payorId = agency != null && agency.Payor.IsInteger() ? agency.Payor.ToInteger() : 0;
            }
            ViewData["ManLocationId"] = locationId;
            ViewData["Payor"] = payorId;
            return PartialView("Schedule/PastDueRecet", agencyService.GetRecertsPastDue(locationId, payorId, DateTime.Now.AddDays(-59), DateTime.Now,false,0));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PastDueRecetScheduleContent(Guid BranchCode, int InsuranceId, DateTime StartDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
           return PartialView("Schedule/Content/PastDueRecet",agencyService.GetRecertsPastDue(BranchCode, InsuranceId, StartDate, DateTime.Now,false,0));
        }

        public ActionResult ExportPastDueRecetSchedule(Guid BranchCode, int InsuranceId, DateTime StartDate)
        {
            var recets = agencyService.GetRecertsPastDue(BranchCode, InsuranceId, StartDate, DateTime.Now,false,0);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Past Due Recet";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PastDueRecet");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Past Due Recet");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0}", string.Format("{0} - {1}", StartDate.ToString("MM/dd/yyyy"), DateTime.Now.ToString("MM/dd/yyyy"))));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("MR #");
            headerRow.CreateCell(2).SetCellValue("Employee Responsible");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Due Date");
            headerRow.CreateCell(5).SetCellValue("Past Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (recets != null && recets.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in recets)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientName);
                    row.CreateCell(1).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(2).SetCellValue(evnt.AssignedTo);
                    row.CreateCell(3).SetCellValue(evnt.StatusName);
                    row.CreateCell(4).SetCellValue(evnt.TargetDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(5).SetCellValue(evnt.DateDifference);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Past Due Recet: {0}", recets.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PastDueRecet_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UpcomingRecetSchedule()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            var agency = agencyRepository.Get(Current.AgencyId);
            var locationId = Guid.Empty;
            var payorId = 0;
            if (agency != null)
            {
                locationId = agency.MainLocation != null ? agency.MainLocation.Id : Guid.Empty;
                payorId = agency != null && agency.Payor.IsInteger()? agency.Payor.ToInteger() : 0;
            }
            ViewData["ManLocationId"] = locationId;
            ViewData["Payor"] = payorId;
            return PartialView("Schedule/UpcomingRecet", agencyService.GetRecertsUpcoming(locationId, payorId, DateTime.Now, DateTime.Now.AddDays(24),false,0));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpcomingRecetScheduleContent(Guid BranchCode, int InsuranceId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Schedule/Content/UpcomingRecet", agencyService.GetRecertsUpcoming(BranchCode, InsuranceId, DateTime.Now, DateTime.Now.AddDays(24),false,0));
        }

        public ActionResult ExportUpcomingRecetSchedule(Guid BranchCode, int InsuranceId)
        {
            var recets = agencyService.GetRecertsUpcoming(BranchCode, InsuranceId, DateTime.Now, DateTime.Now.AddDays(24),false,0);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Upcoming Recet";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UpcomingRecet");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Upcoming Recet");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("MR #");
            headerRow.CreateCell(2).SetCellValue("Employee Responsible");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Due Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (recets != null && recets.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in recets)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.PatientName);
                    row.CreateCell(1).SetCellValue(evnt.PatientIdNumber);
                    row.CreateCell(2).SetCellValue(evnt.AssignedTo);
                    row.CreateCell(3).SetCellValue(evnt.StatusName);
                    row.CreateCell(4).SetCellValue(evnt.TargetDate.ToString("MM/dd/yyyy"));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Upcoming Recet: {0}", recets.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UpcomingRecet_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Billing Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OutstandingClaims()
        {
            ViewData["SortColumn"] = "LastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/OutstandingClaims", reportService.UnProcessedBillViewData(Guid.Empty, "All"));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OutstandingClaimsContent(Guid BranchCode, string Type, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/OutstandingClaims", reportService.UnProcessedBillViewData(BranchCode, Type));
        }

        public ActionResult ExportOutstandingClaims(Guid BranchCode, string Type)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Outstanding Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("OutstandingClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Outstanding Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("Type");
            sheet.CreateFreezePane(0, 1, 0, 1);
            var bills = reportService.UnProcessedBillViewData(BranchCode, Type);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.LastName);
                    row.CreateCell(2).SetCellValue(bill.FirstName);
                    row.CreateCell(3).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(4).SetCellValue(bill.Type);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Outstanding Claims: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("OutstandingClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ClaimsByStatus()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/ClaimByStatus", reportService.BillViewDataByStatus(Guid.Empty, "All", 300, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ClaimsByStatusContent(Guid BranchCode, string Type, int Status, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/ClaimByStatus", reportService.BillViewDataByStatus(BranchCode, Type, Status, StartDate, EndDate));
        }

        public ActionResult ExportClaimsByStatus(Guid BranchCode, string Type, int Status, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Claims By Status";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ClaimsByStatus");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Claims By Status");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", Enum.IsDefined(typeof(ScheduleStatus), Status) ? ((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), Status)).GetDescription() : string.Empty));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Claim Amount");
            headerRow.CreateCell(5).SetCellValue("Claim Date");
            headerRow.CreateCell(6).SetCellValue("Payment Amount");
            headerRow.CreateCell(7).SetCellValue("Payment Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = reportService.BillViewDataByStatus(BranchCode, Type, Status, StartDate, EndDate);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    row.CreateCell(4).SetCellValue(bill.ClaimAmount);
                    row.CreateCell(5).SetCellValue(bill.ClaimDateFormatted);
                    row.CreateCell(6).SetCellValue(bill.PaymentAmount);
                    row.CreateCell(7).SetCellValue(bill.PaymentDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Claims By Status: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("OutstandingClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SubmittedClaims()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/SubmittedClaims");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmittedClaimsContent(Guid BranchCode, string Type, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/SubmittedClaims", reportService.SubmittedBillViewDataByDateRange(BranchCode, Type, StartDate, EndDate));
        }

        public ActionResult ExportSubmittedClaims(Guid BranchCode, string Type, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Claims By Status";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("ClaimsByStatus");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Claims By Status");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
            // titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", Enum.IsDefined(typeof(ScheduleStatus), Status) ? ((ScheduleStatus)Enum.ToObject(typeof(ScheduleStatus), Status)).GetDescription() : string.Empty));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Claim Amount");
            headerRow.CreateCell(5).SetCellValue("Claim Date");
            headerRow.CreateCell(6).SetCellValue("Payment Amount");
            headerRow.CreateCell(7).SetCellValue("Payment Date");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = reportService.SubmittedBillViewDataByDateRange(BranchCode, Type, StartDate, EndDate);
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    row.CreateCell(4).SetCellValue(bill.ClaimAmount);
                    row.CreateCell(5).SetCellValue(bill.ClaimDateFormatted);
                    row.CreateCell(6).SetCellValue(bill.PaymentAmount);
                    row.CreateCell(7).SetCellValue(bill.PaymentDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Claims By Status: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("OutstandingClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AccountsReceivable()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/AccountsReceivable", billingService.AccountsReceivables(Guid.Empty, 0, DateTime.Now.AddDays(-59), DateTime.Now, "All"));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AccountsReceivableContent(Guid BranchCode, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            var viewData = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate, Type).ToList() ?? new List<ClaimLean>();
            //if (Type.IsNotNullOrEmpty())
            //{
            //    if (Type.IsEqual("RAP"))
            //    {
            //        viewData = billingService.AccountsReceivableRaps(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //    else if (Type.IsEqual("Final"))
            //    {
            //        viewData = billingService.AccountsReceivableFinals(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //    else if (Type.IsEqual("All"))
            //    {
            //        viewData = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //}
            return PartialView("Billing/Content/AccountsReceivable", viewData);
        }

        public ActionResult ExportAccountsReceivable(Guid BranchCode, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Accounts Receivable";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("AccountsReceivable");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Accounts Receivable");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(5).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Type");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Claim Date");
            headerRow.CreateCell(6).SetCellValue("Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate, Type).ToList() ?? new List<ClaimLean>();
            //if (Type.IsNotNullOrEmpty())
            //{
            //    if (Type.IsEqual("RAP"))
            //    {
            //        bills = billingService.AccountsReceivableRaps(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //    else if (Type.IsEqual("Final"))
            //    {
            //        bills = billingService.AccountsReceivableFinals(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //    else if (Type.IsEqual("All"))
            //    {
            //        bills = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //}
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(3).SetCellValue(bill.Type);
                    row.CreateCell(4).SetCellValue(bill.StatusName);
                    row.CreateCell(5).SetCellValue(bill.ClaimDateFormatted);
                    row.CreateCell(6).SetCellValue(Math.Round(bill.ClaimAmount, 2));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Accounts Receivable: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("AccountsReceivable_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnearnedRevenue()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/UnearnedRevenue", billingService.GetUnearnedRevenue(Guid.Empty, 0, DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnearnedRevenueContent(Guid BranchCode, int InsuranceId, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/UnearnedRevenue", billingService.GetUnearnedRevenue(BranchCode, InsuranceId, EndDate));
        }

        public ActionResult ExportUnearnedRevenue(Guid BranchCode, int InsuranceId, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Unearned Revenue";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UnearnedRevenue");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Unearned Revenue Report");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Report Date: {0}", EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Assessment");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Episode Payment");
            headerRow.CreateCell(6).SetCellValue("Total Visits");
            headerRow.CreateCell(7).SetCellValue("Completed Visits");
            headerRow.CreateCell(8).SetCellValue("Unearned Visits");
            headerRow.CreateCell(9).SetCellValue("Unit Amount");
            headerRow.CreateCell(10).SetCellValue("Unearned Amount");
            headerRow.CreateCell(11).SetCellValue("Rap Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var revenue = billingService.GetUnearnedRevenue(BranchCode, InsuranceId, EndDate);

            if (revenue != null && revenue.Count > 0)
            {
                int rowNumber = 2;
                foreach (var rap in revenue)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.DisplayName);
                    row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                    row.CreateCell(3).SetCellValue(rap.AssessmentTypeName);
                    row.CreateCell(4).SetCellValue(rap.StatusName);
                    row.CreateCell(5).SetCellValue(rap.ProspectivePayment);
                    row.CreateCell(6).SetCellValue(rap.BillableVisitCount);
                    row.CreateCell(7).SetCellValue(rap.CompletedVisitCount);
                    row.CreateCell(8).SetCellValue(rap.UnearnedVisitCount);
                    row.CreateCell(9).SetCellValue(string.Format("${0:#0.00}", rap.UnitAmount));
                    row.CreateCell(10).SetCellValue(string.Format("${0:#0.00}", rap.UnearnedRevenueAmount));
                    row.CreateCell(11).SetCellValue(string.Format("${0:#0.00}", rap.RapAmount));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Unearned Revenue: {0}", revenue.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UnearnedRevenue_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnbilledRevenue()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/UnbilledRevenue", billingService.GetUnbilledRevenue(Guid.Empty, 0, DateTime.Now.AddDays(-29), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnbilledRevenueContent(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/UnbilledRevenue", billingService.GetUnbilledRevenue(BranchCode, InsuranceId, StartDate, EndDate));
        }

        public ActionResult ExportUnbilledRevenue(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Unbilled Revenue";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UnbilledRevenue");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Unbilled Revenue Report");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Report Date: {0}", EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Assessment");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Rap Amount");
            headerRow.CreateCell(6).SetCellValue("Episode Payment");
            headerRow.CreateCell(7).SetCellValue("Total Visits");
            headerRow.CreateCell(8).SetCellValue("Completed Visits");
            headerRow.CreateCell(9).SetCellValue("Unbilled Visits");
            headerRow.CreateCell(10).SetCellValue("Unit Amount");
            headerRow.CreateCell(11).SetCellValue("Unbilled Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var revenue = billingService.GetUnbilledRevenue(BranchCode, InsuranceId, StartDate, EndDate);

            if (revenue != null && revenue.Count > 0)
            {
                int rowNumber = 2;
                foreach (var rap in revenue)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.DisplayName);
                    row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                    row.CreateCell(3).SetCellValue(rap.AssessmentTypeName);
                    row.CreateCell(4).SetCellValue(rap.StatusName);
                    row.CreateCell(5).SetCellValue(string.Format("${0:#0.00}", rap.RapAmount));
                    row.CreateCell(6).SetCellValue(rap.ProspectivePayment);
                    row.CreateCell(7).SetCellValue(rap.BillableVisitCount);
                    row.CreateCell(8).SetCellValue(rap.CompletedVisitCount);
                    row.CreateCell(9).SetCellValue(rap.UnbilledVisitCount);
                    row.CreateCell(10).SetCellValue(string.Format("${0:#0.00}", rap.UnitAmount));
                    row.CreateCell(11).SetCellValue(string.Format("${0:#0.00}", rap.UnbilledRevenueAmount));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Unbilled Revenue: {0}", revenue.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            sheet.AutoSizeColumn(10);
            sheet.AutoSizeColumn(11);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UnbilledRevenue_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EarnedRevenue()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/EarnedRevenue", billingService.GetEarnedRevenue(Guid.Empty, 0,  DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EarnedRevenueContent(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/EarnedRevenue", billingService.GetEarnedRevenue(BranchCode, InsuranceId, StartDate, EndDate));
        }

        public ActionResult ExportEarnedRevenue(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Earned Revenue";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EarnedRevenue");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Earned Revenue");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Assessment");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Total Visits");
            headerRow.CreateCell(6).SetCellValue("Episode Payment");
            headerRow.CreateCell(7).SetCellValue("Unit Amount");
            headerRow.CreateCell(8).SetCellValue("Completed Visits");
            headerRow.CreateCell(9).SetCellValue("Earned Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var revenue = billingService.GetEarnedRevenue(BranchCode, InsuranceId, StartDate, EndDate);

            if (revenue != null && revenue.Count > 0)
            {
                int rowNumber = 2;
                foreach (var rap in revenue)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.DisplayName);
                    row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                    row.CreateCell(3).SetCellValue(rap.AssessmentTypeName);
                    row.CreateCell(4).SetCellValue(rap.StatusName);
                    row.CreateCell(5).SetCellValue(rap.BillableVisitCount);
                    row.CreateCell(6).SetCellValue(rap.ProspectivePayment);
                    row.CreateCell(7).SetCellValue(string.Format("${0:#0.00}", rap.UnitAmount));
                    row.CreateCell(8).SetCellValue(rap.CompletedVisitCount);
                    row.CreateCell(9).SetCellValue(string.Format("${0:#0.00}", rap.EarnedRevenueAmount));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Earned Revenue: {0}", revenue.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EarnedRevenue_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AgedAccountsReceivable()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/AgedAccountsReceivable", billingService.AccountsReceivables(Guid.Empty, 0, DateTime.Now.AddDays(-59), DateTime.Now, "All"));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AgedAccountsReceivableContent(Guid BranchCode, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            var viewData = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate, Type).ToList() ?? new List<ClaimLean>();
            //if (Type.IsNotNullOrEmpty())
            //{
            //    if (Type.IsEqual("RAP"))
            //    {
            //        viewData = billingService.AccountsReceivableRaps(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //    else if (Type.IsEqual("Final"))
            //    {
            //        viewData = billingService.AccountsReceivableFinals(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //    else if (Type.IsEqual("All"))
            //    {
            //        viewData = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //}
            return PartialView("Billing/Content/AgedAccountsReceivable", viewData);
        }

        public ActionResult ExportAgedAccountsReceivable(Guid BranchCode, string Type, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Aged Accounts Receivable";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("AgedAccountsReceivable");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Aged Accounts Receivable");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Type: {0}", Type.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Patient Name");
            headerRow.CreateCell(1).SetCellValue("Episode Range");
            headerRow.CreateCell(2).SetCellValue("Type");
            headerRow.CreateCell(3).SetCellValue("Status");
            headerRow.CreateCell(4).SetCellValue("Claim Date");
            headerRow.CreateCell(5).SetCellValue("1-30");
            headerRow.CreateCell(6).SetCellValue("31-60");
            headerRow.CreateCell(7).SetCellValue("61-90");
            headerRow.CreateCell(8).SetCellValue("> 90");
            headerRow.CreateCell(9).SetCellValue("Total");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills =billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate,Type).ToList()?? new List<ClaimLean>();
            //if (Type.IsNotNullOrEmpty())
            //{
            //    if (Type.IsEqual("RAP"))
            //    {
            //        bills = billingService.AccountsReceivableRaps(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //    else if (Type.IsEqual("Final"))
            //    {
            //        bills = billingService.AccountsReceivableFinals(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //    else if (Type.IsEqual("All"))
            //    {
            //        bills = billingService.AccountsReceivables(BranchCode, InsuranceId, StartDate, EndDate).ToList();
            //    }
            //}
            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.DisplayName);
                    row.CreateCell(1).SetCellValue(bill.EpisodeRange);
                    row.CreateCell(2).SetCellValue(bill.Type);
                    row.CreateCell(3).SetCellValue(bill.StatusName);
                    row.CreateCell(4).SetCellValue(bill.ClaimDateFormatted);
                    row.CreateCell(5).SetCellValue(Math.Round(bill.Amount30, 2));
                    row.CreateCell(6).SetCellValue(Math.Round(bill.Amount60, 2));
                    row.CreateCell(7).SetCellValue(Math.Round(bill.Amount90, 2));
                    row.CreateCell(8).SetCellValue(Math.Round(bill.AmountOver90, 2));
                    row.CreateCell(9).SetCellValue(Math.Round(bill.ClaimAmount, 2));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Aged Accounts Receivable: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("AgedAccountsReceivable_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSRAPClaims()
        {
            ViewData["SortColumn"] = "LastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/PPSRAPClaims", billingRepository.GetPPSClaims(Current.AgencyId, Guid.Empty, "RAP"));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PPSRAPClaimsContent(Guid BranchCode, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/PPSRAPClaims", billingRepository.GetPPSClaims(Current.AgencyId, BranchCode, "RAP"));
        }

        public ActionResult ExportPPSRAPClaims(Guid BranchCode)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PPS RAP Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PPSRAPClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("PPS RAP Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("HIPPS");
            headerRow.CreateCell(5).SetCellValue("Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var raps = billingRepository.GetPPSClaims(Current.AgencyId, BranchCode, "RAP");
            if (raps != null && raps.Count > 0)
            {
                int rowNumber = 2;
                foreach (var rap in raps)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.LastName);
                    row.CreateCell(2).SetCellValue(rap.FirstName);
                    row.CreateCell(3).SetCellValue(rap.EpisodeRange);
                    row.CreateCell(4).SetCellValue(rap.HippsCode);
                    row.CreateCell(5).SetCellValue(rap.ProspectivePay);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of PPS RAP Claims: {0}", raps.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PPSRAPClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSFinalClaims()
        {
            ViewData["SortColumn"] = "LastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/PPSFinalClaims", billingRepository.GetPPSClaims(Current.AgencyId, Guid.Empty, "Final"));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PPSFinalClaimsContent(Guid BranchCode, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/PPSFinalClaims", billingRepository.GetPPSClaims(Current.AgencyId, BranchCode, "Final"));
        }

        public ActionResult ExportPPSFinalClaims(Guid BranchCode)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - PPS Final Claims";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PPSFinalClaims");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("PPS Final Claims");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            headerRow.CreateCell(4).SetCellValue("HIPPS");
            headerRow.CreateCell(5).SetCellValue("Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var finals = billingRepository.GetPPSClaims(Current.AgencyId, BranchCode, "Final");
            if (finals != null && finals.Count > 0)
            {
                int rowNumber = 2;
                foreach (var final in finals)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(final.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(final.LastName);
                    row.CreateCell(2).SetCellValue(final.FirstName);
                    row.CreateCell(3).SetCellValue(final.EpisodeRange);
                    row.CreateCell(4).SetCellValue(final.HippsCode);
                    row.CreateCell(5).SetCellValue(final.ProspectivePay);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of PPS Final Claims: {0}", finals.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PPSFinalClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PotentialClaimAutoCancel()
        {
            ViewData["SortColumn"] = "LastName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/PotentialClaimAutoCancel", billingRepository.GetPotentialCliamAutoCancels(Current.AgencyId, Guid.Empty));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PotentialClaimAutoCancelContent(Guid BranchCode, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/PotentialClaimAutoCancel", billingRepository.GetPotentialCliamAutoCancels(Current.AgencyId, BranchCode));
        }

        public ActionResult ExportPotentialClaimAutoCancel(Guid BranchCode)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Potential Claim AutoCancel";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PotentialClaimAutoCancel");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Potential Claim AutoCancel");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));

            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("First Name");
            headerRow.CreateCell(3).SetCellValue("Episode Range");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var finals = billingRepository.GetPotentialCliamAutoCancels(Current.AgencyId, BranchCode);
            if (finals != null && finals.Count > 0)
            {
                int rowNumber = 2;
                foreach (var final in finals)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(final.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(final.LastName);
                    row.CreateCell(2).SetCellValue(final.FirstName);
                    row.CreateCell(3).SetCellValue(final.EpisodeRange);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Potential Claim AutoCancel: {0}", finals.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PPSFinalClaims_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BillingBatch()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/BillingBatch", billingService.BillingBatch("ALL", DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BillingBatchContent(string ClaimType, DateTime BatchDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/BillingBatch", billingService.BillingBatch(ClaimType, BatchDate));
        }

        public ActionResult ExportBillingBatch(string ClaimType, DateTime BatchDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Billing Batch Report";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("BillingBatch");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Billing Batch");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Claim Type: {0}", ClaimType.ToUpperCase()));
            titleRow.CreateCell(4).SetCellValue(string.Format("Date: {0}", BatchDate.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Medicare Number");
            headerRow.CreateCell(4).SetCellValue("Bill Type");
            headerRow.CreateCell(5).SetCellValue("Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var bills = billingService.BillingBatch(ClaimType, BatchDate).ToList();

            if (bills != null && bills.Count > 0)
            {
                int rowNumber = 2;
                foreach (var bill in bills)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(bill.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(bill.DisplayName);
                    row.CreateCell(2).SetCellValue(bill.Range);
                    row.CreateCell(3).SetCellValue(bill.MedicareNumber);
                    row.CreateCell(4).SetCellValue(bill.BillType);
                    row.CreateCell(5).SetCellValue(Math.Round(bill.ProspectivePay, 2));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number: {0}", bills.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("BillingBatch_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnearnedRevenueByEpisodeDays()
        {
            return PartialView("Billing/UnearnedRevenueByEpisodeDays");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EarnedRevenueByEpisodeDays()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/EarnedRevenueByEpisodeDays", billingService.GetEarnedRevenueByEpisodeDays(Guid.Empty, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EarnedRevenueByEpisodeDaysContent(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/EarnedRevenueByEpisodeDays", billingService.GetEarnedRevenueByEpisodeDays(BranchCode, InsuranceId, StartDate, EndDate));
        }

        public ActionResult ExportEarnedRevenueByEpisodeDays(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Earned Revenue By Episode Days";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EarnedRevenueEpisodeDays");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Earned Revenue By Episode Days");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Assessment");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Total Days");
            headerRow.CreateCell(6).SetCellValue("Episode Payment");
            headerRow.CreateCell(7).SetCellValue("Unit Amount");
            headerRow.CreateCell(8).SetCellValue("Completed Days");
            headerRow.CreateCell(9).SetCellValue("Earned Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var revenue = billingService.GetEarnedRevenueByEpisodeDays(BranchCode, InsuranceId, StartDate, EndDate);

            if (revenue != null && revenue.Count > 0)
            {
                int rowNumber = 2;
                foreach (var rap in revenue)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.DisplayName);
                    row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                    row.CreateCell(3).SetCellValue(rap.AssessmentTypeName);
                    row.CreateCell(4).SetCellValue(rap.StatusName);
                    row.CreateCell(5).SetCellValue(rap.BillableDayCount);
                    row.CreateCell(6).SetCellValue(rap.ProspectivePayment);
                    row.CreateCell(7).SetCellValue(string.Format("${0:#0.00}", rap.UnitAmount));
                    row.CreateCell(8).SetCellValue(rap.CompletedDayCount);
                    row.CreateCell(9).SetCellValue(string.Format("${0:#0.00}", rap.EarnedRevenueAmount));
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Earned Revenue: {0}", revenue.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            sheet.AutoSizeColumn(9);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EarnedRevenueByEpisodeDays_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult BilledRevenue()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Billing/BilledRevenue", billingService.GetRevenueReport(Guid.Empty, 0, DateTime.Now.AddDays(-59), DateTime.Now));
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BilledRevenueContent(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Billing/Content/BilledRevenue", billingService.GetRevenueReport(BranchCode, InsuranceId, StartDate, EndDate));
        }

        public ActionResult ExportBilledRevenue(Guid BranchCode, int InsuranceId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Revenue Report";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("Revenue");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Revenue Report");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var insurance = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insurance != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance: {0}", insurance.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MRN");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Episode Range");
            headerRow.CreateCell(3).SetCellValue("Assessment");
            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Episode Payment");
            headerRow.CreateCell(6).SetCellValue("Total Visits");
            headerRow.CreateCell(7).SetCellValue("Unit Amount");
            headerRow.CreateCell(8).SetCellValue("Earned Visits");
            headerRow.CreateCell(9).SetCellValue("Earned Amount");
            headerRow.CreateCell(10).SetCellValue("Unearned Visits");
            headerRow.CreateCell(11).SetCellValue("Unearned Amount");
            headerRow.CreateCell(12).SetCellValue("Unbilled Visits");
            headerRow.CreateCell(13).SetCellValue("Unbilled Amount");
            headerRow.CreateCell(14).SetCellValue("Billed Visits");
            headerRow.CreateCell(15).SetCellValue("Billed Amount");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var revenue = billingService.GetRevenueReport(BranchCode, InsuranceId, StartDate, EndDate);
            if (revenue != null && revenue.Count > 0)
            {
                int rowNumber = 2;
                Double totalEarned = 0.0;
                Double totalUnEarned = 0.0;
                Double totalUnbilled = 0.0;
                Double totalBilled = 0.0;
                foreach (var rap in revenue)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(rap.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(rap.DisplayName);
                    row.CreateCell(2).SetCellValue(rap.EpisodeRange);
                    row.CreateCell(3).SetCellValue(rap.AssessmentType);
                    row.CreateCell(4).SetCellValue(rap.StatusName);
                    row.CreateCell(5).SetCellValue(rap.ProspectivePayment);
                    row.CreateCell(6).SetCellValue(rap.BillableVisitCount);
                    row.CreateCell(7).SetCellValue(string.Format("${0:#0.00}", rap.UnitAmount));
                    row.CreateCell(8).SetCellValue(rap.CompletedVisitCount);
                    row.CreateCell(9).SetCellValue(string.Format("${0:#0.00}", rap.EarnedRevenueAmount));
                    row.CreateCell(10).SetCellValue(rap.UnearnedVisitCount);
                    row.CreateCell(11).SetCellValue(string.Format("${0:#0.00}", rap.UnearnedRevenueAmount));
                    row.CreateCell(12).SetCellValue(rap.UnbilledVisitCount);
                    row.CreateCell(13).SetCellValue(string.Format("${0:#0.00}", rap.UnbilledRevenueAmount));
                    row.CreateCell(14).SetCellValue(rap.RapVisitCount);
                    row.CreateCell(15).SetCellValue(string.Format("${0:#0.00}", rap.RapAmount));
                    rowNumber++;
                    totalEarned += rap.EarnedRevenueAmount;
                    totalUnEarned += rap.UnearnedRevenueAmount;
                    totalUnbilled += rap.UnbilledRevenueAmount;
                    totalBilled += rap.RapAmount;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Revenue Items: {0}", revenue.Count));
                totalRow.CreateCell(8).SetCellValue("Total Earned:");
                totalRow.CreateCell(9).SetCellValue(string.Format("${0:#0.00}", totalEarned));
                totalRow.CreateCell(10).SetCellValue("Total Unearned:");
                totalRow.CreateCell(11).SetCellValue(string.Format("${0:#0.00}", totalUnEarned));
                totalRow.CreateCell(12).SetCellValue("Total Unbilled:");
                totalRow.CreateCell(13).SetCellValue(string.Format("${0:#0.00}", totalUnbilled));
                totalRow.CreateCell(14).SetCellValue("Total Billed:");
                totalRow.CreateCell(15).SetCellValue(string.Format("${0:#0.00}", totalBilled));

                var sumRow = sheet.CreateRow(rowNumber + 4);
                sumRow.CreateCell(8).SetCellValue("Earned + Unearned:");
                sumRow.CreateCell(9).SetCellValue(string.Format("${0:#0.00}", totalEarned + totalUnEarned));

                sumRow.CreateCell(13).SetCellValue("Billed + Unbilled:");
                sumRow.CreateCell(14).SetCellValue(string.Format("${0:#0.00}", totalBilled + totalUnbilled));
            }

            var columnSize = 16;
            int resizeColCounter = 0;
            do
            {
                sheet.AutoSizeColumn(resizeColCounter);
                resizeColCounter++;
            }
            while (resizeColCounter < columnSize);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("RevenueReport_{0}.xls", DateTime.Now.Ticks));
        }
        #endregion

        #region Employee Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeRoster()
        {
            ViewData["SortColumn"] = "DisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/Roster", reportService.GetEmployeeRoster(Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeRosterContent(Guid BranchCode, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/Roster", reportService.GetEmployeeRoster(BranchCode, StatusId));
        }

        public ActionResult ExportEmployeeRoster(Guid BranchCode, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Roster";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeRoster");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Roster");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), StatusId) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), StatusId)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Address");
            headerRow.CreateCell(2).SetCellValue("City");
            headerRow.CreateCell(3).SetCellValue("State");
            headerRow.CreateCell(4).SetCellValue("Zip Code");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            headerRow.CreateCell(6).SetCellValue("Gender");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeRosters = reportService.GetEmployeeRoster(BranchCode, StatusId);
            if (employeeRosters != null && employeeRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var employee in employeeRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(employee.DisplayName);
                    row.CreateCell(1).SetCellValue(employee.Profile.Address);
                    row.CreateCell(2).SetCellValue(employee.Profile.AddressCity);
                    row.CreateCell(3).SetCellValue(employee.Profile.AddressStateCode);
                    row.CreateCell(4).SetCellValue(employee.Profile.AddressZipCode);
                    row.CreateCell(5).SetCellValue(employee.Profile.PhoneHome);
                    row.CreateCell(6).SetCellValue(employee.Profile.Gender);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Roster: {0}", employeeRosters.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeRoster_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeBirthdayList()
        {
            ViewData["SortColumn"] = "Name";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/BirthdayList",reportService.GetEmployeeBirthdays(Guid.Empty, 1, DateTime.Now.Month) );
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BirthdayListContent(Guid BranchCode, int StatusId, int Month, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
           return PartialView("Employee/Content/BirthdayList",reportService.GetEmployeeBirthdays(BranchCode, StatusId, Month));
        }

        public ActionResult ExportEmployeeBirthdayList(Guid BranchCode, int StatusId, int Month)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Birthday List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeBirthdayList");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Birthday List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month : {0}", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), StatusId) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), StatusId)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Age");
            headerRow.CreateCell(2).SetCellValue("Birth Day");
            headerRow.CreateCell(3).SetCellValue("Address First Row");
            headerRow.CreateCell(4).SetCellValue("Address Second Row");
            headerRow.CreateCell(5).SetCellValue("Home Phone");
            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeBirthDays = reportService.GetEmployeeBirthdays(BranchCode, StatusId, Month);
            if (employeeBirthDays != null && employeeBirthDays.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in employeeBirthDays)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.Name);
                    row.CreateCell(1).SetCellValue(birthDay.Age);
                    row.CreateCell(2).SetCellValue(birthDay.BirthDay);
                    row.CreateCell(3).SetCellValue(birthDay.AddressFirstRow);
                    row.CreateCell(4).SetCellValue(birthDay.AddressSecondRow);
                    row.CreateCell(5).SetCellValue(birthDay.PhoneHome);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Birthday List: {0}", employeeBirthDays.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeBirthdayList_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeLicenseListing()
        {
            ViewData["SortColumn"] = "InitiationDate";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/License" , new List<License>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeLicenseContent(Guid UserId, string SortParams)
        {
            
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/License", UserId.IsEmpty() ? new List<License>() : userRepository.GetUserLicenses(Current.AgencyId, UserId,false));
        }

        public ActionResult ExportEmployeeLicenseListing(Guid UserId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee License Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeLicenseListing");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee License Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Name");
            headerRow.CreateCell(1).SetCellValue("Initiation Date");
            headerRow.CreateCell(2).SetCellValue("Expiration Date");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeLicenses = userRepository.GetUserLicenses(Current.AgencyId, UserId , false);
            if (employeeLicenses != null && employeeLicenses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var employeeLicense in employeeLicenses)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(employeeLicense.LicenseType);
                    row.CreateCell(1).SetCellValue(employeeLicense.InitiationDateFormatted);
                    row.CreateCell(2).SetCellValue(employeeLicense.ExpirationDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee License Listing: {0}", employeeLicenses.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AllEmployeeLicenseListing()
        {
            ViewData["SortColumn"] = "InitiationDate";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/AllEmployeeLicense", userService.GetUserLicenses(Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AllEmployeeLicenseContent(Guid BranchId, int StatusId, string SortParams)
        {

            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/AllEmployeeLicense", userService.GetUserLicenses(BranchId, StatusId));
        }

        public ActionResult ExportAllEmployeeLicenseListing(Guid BranchId, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - All Employee License Listing";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("AllEmployeeLicenseListing");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("All Employee License Listing");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee");
            headerRow.CreateCell(1).SetCellValue("License Name");
            headerRow.CreateCell(2).SetCellValue("Initiation Date");
            headerRow.CreateCell(3).SetCellValue("Expiration Date");

            sheet.CreateFreezePane(0, 2, 0, 2);

            var users = userRepository.GetAllUsers(Current.AgencyId,BranchId, StatusId);
            if (users != null && users.Count > 0)
            {
                int rowNumber = 2;
                users.ForEach(u =>
                {
                    var row = sheet.CreateRow(rowNumber);
                   
                    var rowCellStyle = workbook.CreateCellStyle();
                    rowCellStyle.BorderTop = CellBorderType.THIN;
                    row.RowStyle = rowCellStyle;

                    var cell = row.CreateCell(0);
                    var cellStyle = workbook.CreateCellStyle();
                    cell.SetCellValue(string.Format("{0}, {1}", u.LastName, u.FirstName));
                    if (u.Licenses.IsNotNullOrEmpty())
                    {
                        u.LicensesArray = u.Licenses.ToObject<List<License>>();
                        if (u.LicensesArray != null && u.LicensesArray.Count > 0)
                        {
                            u.LicensesArray.ForEach(l =>
                            {
                                rowNumber++;
                                var licenseRow = sheet.CreateRow(rowNumber);
                                licenseRow.CreateCell(0).SetCellValue(string.Empty);
                                licenseRow.CreateCell(1).SetCellValue(l.LicenseType);
                                licenseRow.CreateCell(2).SetCellValue(l.InitiationDateFormatted);
                                licenseRow.CreateCell(3).SetCellValue(l.ExpirationDateFormatted);
                            });
                        }
                    }
                    rowNumber = rowNumber + 2;
                });
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of  Employee : {0}", users.Count));
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("AllEmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeExpiringLicense()
        {
            ViewData["SortColumn"] = "UserDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/ExpiringLicenses", reportService.GetEmployeeExpiringLicenses(Guid.Empty, 1));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExpiringLicenseContent(Guid BranchCode, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/ExpiringLicenses", reportService.GetEmployeeExpiringLicenses(BranchCode, StatusId));
        }

        public ActionResult ExportEmployeeExpiringLicense(Guid BranchCode, int StatusId)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Expiring License";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeExpiringLicense");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Expiring License");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(UserStatus), StatusId) ? ((UserStatus)Enum.ToObject(typeof(UserStatus), StatusId)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee Name");
            headerRow.CreateCell(1).SetCellValue("License Name");
            headerRow.CreateCell(2).SetCellValue("Initiation Date");
            headerRow.CreateCell(3).SetCellValue("Expiration Date");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeLicenses = reportService.GetEmployeeExpiringLicenses(BranchCode, StatusId);
            if (employeeLicenses != null && employeeLicenses.Count > 0)
            {
                int rowNumber = 2;
                foreach (var employeeLicense in employeeLicenses)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(employeeLicense.UserDisplayName);
                    row.CreateCell(1).SetCellValue(employeeLicense.LicenseType);
                    row.CreateCell(2).SetCellValue(employeeLicense.InitiationDateFormatted);
                    row.CreateCell(3).SetCellValue(employeeLicense.ExpirationDateFormatted);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Expiring License: {0}", employeeLicenses.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeExpiringLicenses_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EmployeeVisitByDateRange()
        {

            ViewData["SortColumn"] = "UserDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Employee/ScheduleByDateRange", reportService.GetEmployeeScheduleByDateRange(Guid.Empty, DateTime.Now.AddDays(-15), DateTime.Now.AddDays(15)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmployeeScheduleByDateRangeContent(Guid BranchCode, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Employee/Content/ScheduleByDateRange", reportService.GetEmployeeScheduleByDateRange(BranchCode, StartDate, EndDate));
        }

        public ActionResult ExportEmployeeVisitByDateRange(Guid BranchCode, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Visit By Date Range";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeVisitByDateRange");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Visit By Date Range");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Employee");
            headerRow.CreateCell(1).SetCellValue("Patient");
            headerRow.CreateCell(2).SetCellValue("Task");
            headerRow.CreateCell(3).SetCellValue("Schedule Date");
            headerRow.CreateCell(4).SetCellValue("Visit Date");
            headerRow.CreateCell(5).SetCellValue("Status");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var employeeVisits = reportService.GetEmployeeScheduleByDateRange(BranchCode, StartDate, EndDate);
            if (employeeVisits != null && employeeVisits.Count > 0)
            {
                int rowNumber = 2;
                foreach (var visit in employeeVisits)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(visit.UserDisplayName);
                    row.CreateCell(1).SetCellValue(visit.PatientName);
                    row.CreateCell(2).SetCellValue(visit.TaskName);
                    row.CreateCell(3).SetCellValue(visit.ScheduleDateFormatted);
                    row.CreateCell(4).SetCellValue(visit.VisitDateFormatted);
                    row.CreateCell(5).SetCellValue(visit.StatusName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Visit By Date Range: {0}", employeeVisits.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeVisitByDateRange_{0}.xls", DateTime.Now.Ticks));
        }

        #endregion

        #region Statistical Reports

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalPatientVisitHistory()
        {
            ViewData["SortColumn"] = "UserDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/PatientVisitHistory", new List<ScheduleEvent>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalPatientVisitHistoryContent(Guid PatientId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Statistical/Content/PatientVisitHistory", PatientId.IsEmpty() ? new List<ScheduleEvent>() : reportService.GetPatientScheduleEventsByDateRange(PatientId, StartDate, EndDate));
        }

        public ActionResult ExportStatisticalPatientVisitHistory(Guid PatientId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Visit History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientVisitHistory");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Visit History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Employee");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var scheduleEvents = reportService.GetPatientScheduleEventsByDateRange(PatientId, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.DisciplineTaskName);
                    row.CreateCell(1).SetCellValue(evnt.StatusName);
                    row.CreateCell(2).SetCellValue(evnt.EventDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(3).SetCellValue(evnt.VisitDate.ToString("MM/dd/yyyy"));
                    row.CreateCell(4).SetCellValue(evnt.UserDisplayName);

                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Visit History: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalEmployeeVisitHistory()
        {
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/EmployeeVisitHistory", new List<UserVisit>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalEmployeeVisitHistoryContent(Guid UserId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }

            return PartialView("Statistical/Content/EmployeeVisitHistory", UserId.IsEmpty() ? new List<UserVisit>() : reportService.GetEmployeeVisistList(UserId, StartDate, EndDate));
        }

        public ActionResult ExportStatisticalEmployeeVisitHistory(Guid UserId, DateTime StartDate, DateTime EndDate)
        {
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Employee Visit History";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("EmployeeVisitHistory");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Employee Visit History");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range: {0} - {1}", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            var headerRow = sheet.CreateRow(1);

            headerRow.CreateCell(0).SetCellValue("Task");
            headerRow.CreateCell(1).SetCellValue("Status");
            headerRow.CreateCell(2).SetCellValue("Schedule Date");
            headerRow.CreateCell(3).SetCellValue("Visit Date");
            headerRow.CreateCell(4).SetCellValue("Patient");

            sheet.CreateFreezePane(0, 2, 0, 2);
            var scheduleEvents = reportService.GetEmployeeVisistList(UserId, StartDate, EndDate);
            if (scheduleEvents != null && scheduleEvents.Count > 0)
            {
                int rowNumber = 2;
                foreach (var evnt in scheduleEvents)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(evnt.TaskName);
                    row.CreateCell(1).SetCellValue(evnt.StatusName);
                    row.CreateCell(2).SetCellValue(evnt.ScheduleDateFormatted);
                    row.CreateCell(3).SetCellValue(evnt.VisitDateFormatted);
                    row.CreateCell(4).SetCellValue(evnt.PatientName);

                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Employee Visit History: {0}", scheduleEvents.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("EmployeeLicenseListing_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalMonthlyAdmission()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/MonthlyAdmission", reportService.GetPatientByAdmissionMonthYear(Guid.Empty, 1, DateTime.Now.Month, DateTime.Now.Year));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalMonthlyAdmissionContent(Guid BranchCode, int StatusId, int Month, int Year, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Statistical/Content/MonthlyAdmission", reportService.GetPatientByAdmissionMonthYear(BranchCode, StatusId, Month, Year));
        }

        public ActionResult ExportStatisticalMonthlyAdmission(Guid BranchCode, int StatusId, int Month, int Year)
        {
            var patients = reportService.GetPatientByAdmissionMonthYear(BranchCode, StatusId, Month, Year);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Monthly Admission List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientMonthlyAdmission");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Monthly Admission List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Month/Year : {0}/{1}", (!(Month <= 0) && !(Month > 13)) ? (new DateTime(DateTime.Now.Year, Month, 1)).ToString("MMMM") : string.Empty, Year));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("First Name");
            headerRow.CreateCell(1).SetCellValue("Last Name");
            headerRow.CreateCell(2).SetCellValue("Admission Source");
            headerRow.CreateCell(3).SetCellValue("Admission Date");

            headerRow.CreateCell(4).SetCellValue("Status");
            headerRow.CreateCell(5).SetCellValue("Other Referral Source");
            headerRow.CreateCell(6).SetCellValue("Internal Referral");
            headerRow.CreateCell(7).SetCellValue("Referrer Physician");
            headerRow.CreateCell(8).SetCellValue("Referral Date");

            sheet.CreateFreezePane(0, 2, 0, 2);
            if (patients != null && patients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientFirstName);
                    row.CreateCell(1).SetCellValue(patient.PatientLastName);
                    row.CreateCell(2).SetCellValue(patient.AdmissionSourceName);
                    row.CreateCell(3).SetCellValue(patient.PatientSoC);
                    row.CreateCell(4).SetCellValue(patient.PatientStatusName);
                    row.CreateCell(5).SetCellValue(patient.OtherReferralSource);
                    row.CreateCell(6).SetCellValue(patient.InternalReferral);
                    row.CreateCell(7).SetCellValue(patient.ReferrerPhysician);
                    row.CreateCell(8).SetCellValue(patient.ReferralDate);

                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Monthly Admission List: {0}", patients.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            sheet.AutoSizeColumn(8);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientMonthlyAdmission_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalAnnualAdmission()
        {
            ViewData["SortColumn"] = "PatientFirstName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/AnnualAdmission", patientRepository.GetPatientByAdmissionYear(Current.AgencyId, Guid.Empty, 1, DateTime.Now.Year));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalAnnualAdmissionContent(Guid BranchCode, int StatusId, int Year, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Statistical/Content/AnnualAdmission", patientRepository.GetPatientByAdmissionYear(Current.AgencyId, BranchCode, StatusId, Year));
        }

        public ActionResult ExportStatisticalAnnualAdmission(Guid BranchCode, int StatusId, int Year)
        {
            var patients = patientRepository.GetPatientByAdmissionYear(Current.AgencyId, BranchCode, StatusId, Year);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Annual Admission List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("PatientAnnualAdmission");

            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Annual Admission List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Year : {0}", Year));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));

            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("First Name");
            headerRow.CreateCell(2).SetCellValue("Last Name");
            headerRow.CreateCell(3).SetCellValue("Admission Source");
            headerRow.CreateCell(4).SetCellValue("Admission Date");

            sheet.CreateFreezePane(0, 2, 0, 2);

            if (patients != null && patients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var birthDay in patients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(birthDay.PatientId);
                    row.CreateCell(1).SetCellValue(birthDay.PatientFirstName);
                    row.CreateCell(2).SetCellValue(birthDay.PatientLastName);
                    row.CreateCell(4).SetCellValue(birthDay.AdmissionSourceName);
                    row.CreateCell(5).SetCellValue(birthDay.PatientSoC);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Annual Admission List: {0}", patients.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("PatientAnnualAdmission_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult StatisticalUnduplicatedCensus()
        {
            ViewData["SortColumn"] = "FirstName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/UnduplicatedCensusReport", reportService.GetPatientByAdmissionUnduplicatedByDateRange(Guid.Empty, 1, DateTime.Now.AddDays(-59), DateTime.Now.Date));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult StatisticalUnduplicatedCensusContent(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Statistical/Content/UnduplicatedCensusReport", reportService.GetPatientByAdmissionUnduplicatedByDateRange(BranchCode, StatusId, StartDate, EndDate));
        }

        public ActionResult ExportStatisticalUnduplicatedCensus(Guid BranchCode, int StatusId, DateTime StartDate, DateTime EndDate)
        {
            var patients = reportService.GetPatientByAdmissionUnduplicatedByDateRange(BranchCode, StatusId, StartDate, EndDate);
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Patient Unduplicated Census List";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("UnduplicatedCensusReport");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Patient Unduplicated Census List");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Date Range : {0} - {1} ", StartDate.ToString("MM/dd/yyyy"), EndDate.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(4).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("MR#");
            headerRow.CreateCell(1).SetCellValue("First Name");
            headerRow.CreateCell(2).SetCellValue("Last Name");
            headerRow.CreateCell(3).SetCellValue("Admission Source");
            headerRow.CreateCell(4).SetCellValue("Admission Date");
            headerRow.CreateCell(5).SetCellValue("Discharge Date");
            headerRow.CreateCell(6).SetCellValue("Status");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (patients != null && patients.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patients)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientIdNumber);
                    row.CreateCell(1).SetCellValue(patient.FirstName);
                    row.CreateCell(2).SetCellValue(patient.LastName);
                    row.CreateCell(3).SetCellValue(patient.AdmissionSourceName);
                    row.CreateCell(4).SetCellValue(patient.StartOfCareDateFormatted);
                    row.CreateCell(5).SetCellValue(patient.DischargeDateFormatted);
                    row.CreateCell(6).SetCellValue(patient.StatusName);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Patient Unduplicated Census List: {0}", patients.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("UnduplicatedCensusReport_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CensusByPrimaryInsurance()
        {
            ViewData["SortColumn"] = "PatientDisplayName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Statistical/CensusByPrimaryInsurance", new List<PatientRoster>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CensusByPrimaryInsuranceContent(Guid BranchCode, int InsuranceId, int StatusId, string SortParams)
        {
            if (SortParams.IsNotNullOrEmpty())
            {
                var paramArray = SortParams.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            return PartialView("Statistical/Content/CensusByPrimaryInsurance", InsuranceId <= 0 ? new List<PatientRoster>() : reportService.GetPatientRosterByInsurance(BranchCode, InsuranceId, StatusId));
        }

        public ActionResult ExportCensusByPrimaryInsurance(Guid BranchCode, int InsuranceId, int StatusId)
        {
            var patientRosters = InsuranceId > 0 ? reportService.GetPatientRosterByInsurance(BranchCode, InsuranceId, StatusId) : new List<PatientRoster>();
            var workbook = new HSSFWorkbook();
            var si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "Axxess Data Export - Census By Primary Insurance";
            workbook.SummaryInformation = si;
            var sheet = workbook.CreateSheet("CensusByPrimaryInsurance");
            var titleRow = sheet.CreateRow(0);
            titleRow.CreateCell(0).SetCellValue(Current.AgencyName);
            titleRow.CreateCell(1).SetCellValue("Census By Primary Insurance");
            titleRow.CreateCell(2).SetCellValue(string.Format("Effective Date: {0}", DateTime.Today.ToString("MM/dd/yyyy")));
            titleRow.CreateCell(3).SetCellValue(string.Format("Status: {0}", StatusId == 0 ? "All" : (Enum.IsDefined(typeof(PatientStatus), StatusId) ? ((PatientStatus)Enum.ToObject(typeof(PatientStatus), StatusId)).GetDescription() : string.Empty)));
            var insuranceModel = InsuranceEngine.Instance.Get(InsuranceId, Current.AgencyId);
            if (insuranceModel != null)
            {
                titleRow.CreateCell(4).SetCellValue(string.Format("Insurance : {0}", insuranceModel.Name));
            }
            var headerRow = sheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("Patient Name");
            headerRow.CreateCell(2).SetCellValue("Address");
            headerRow.CreateCell(3).SetCellValue("City");
            headerRow.CreateCell(4).SetCellValue("State");
            headerRow.CreateCell(5).SetCellValue("Zip Code");
            headerRow.CreateCell(6).SetCellValue("Home Phone");
            headerRow.CreateCell(7).SetCellValue("Gender");
            sheet.CreateFreezePane(0, 2, 0, 2);
            if (patientRosters != null && patientRosters.Count > 0)
            {
                int rowNumber = 2;
                foreach (var patient in patientRosters)
                {
                    var row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(patient.PatientId);
                    row.CreateCell(1).SetCellValue(patient.PatientDisplayName);
                    row.CreateCell(2).SetCellValue(patient.PatientAddressLine1);
                    row.CreateCell(3).SetCellValue(patient.PatientAddressCity);
                    row.CreateCell(4).SetCellValue(patient.PatientAddressStateCode);
                    row.CreateCell(5).SetCellValue(patient.PatientAddressZipCode);
                    row.CreateCell(6).SetCellValue(patient.PatientPhone);
                    row.CreateCell(7).SetCellValue(patient.PatientGender);
                    rowNumber++;
                }
                var totalRow = sheet.CreateRow(rowNumber + 2);
                totalRow.CreateCell(0).SetCellValue(string.Format("Total Number Of Census By Primary Insurance: {0}", patientRosters.Count));
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            sheet.AutoSizeColumn(6);
            sheet.AutoSizeColumn(7);
            MemoryStream output = new MemoryStream();
            workbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", string.Format("CensusByPrimaryInsurance_{0}.xls", DateTime.Now.Ticks));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSEpisodeInformationReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PPSEpisodeInformation");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSVisitInformationReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PPSVisitInformation");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSPaymentInformationReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PPSPaymentInformation");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PPSChargeInformationReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PPSChargeInformation");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DischargesByReasonReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/DischargesByReason");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitsByPrimaryPaymentSourceReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/VisitsByPrimaryPaymentSource");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VisitsByStaffTypeReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/VisitsByStaffType");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AdmissionsByReferralSourceReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/AdmissionsByReferralSource");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientsVisitsByPrincipalDiagnosisReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PatientsVisitsByPrincipalDiagnosis");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PatientsAndVisitsByAgeReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/PatientsAndVisitsByAge");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CostReport()
        {
            var location = agencyRepository.GetMainLocation(Current.AgencyId);
            ViewData["BranchId"] = location != null ? location.Id : Guid.Empty;
            return PartialView("Statistical/CostReport");
        }

        #endregion

        #endregion
    }
}
