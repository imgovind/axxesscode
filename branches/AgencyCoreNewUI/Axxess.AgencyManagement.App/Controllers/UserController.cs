﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;

    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.Core.Extension;

    using ViewData;
    using Security;
    using Services;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Enums;
    using Axxess.AgencyManagement.Extensions;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.iTextExtension;


    using Telerik.Web.Mvc;
    using Axxess.Log.Common;
    using Axxess.Log.Enums;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class UserController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IUserRepository userRepository;
        private readonly IMembershipService membershipService;
        private readonly IPatientService patientService;

        public UserController(IAgencyManagementDataProvider dataProvider, IUserService userService, IMembershipService membershipService, IPatientService patientService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            Check.Argument.IsNotNull(membershipService, "membershipService");

            this.userService = userService;
            this.membershipService = membershipService;
            this.userRepository = dataProvider.UserRepository;
            this.patientService = patientService;
        }
        #endregion

        #region CRUD
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult New()
        {
            var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
            if (userCount >= Current.MaxAgencyUserCount)
            {
                return PartialView(true);
            }
            return PartialView(false);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([Bind] User user) {
            Check.Argument.IsNotNull(user, "user");
            var viewData = new JsonViewData();
            var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
            if (userCount >= Current.MaxAgencyUserCount) {
                viewData.isSuccessful = false;
                viewData.errorMessage = "User cannot be added because you have reached your maximum number of user accounts. Please contact Axxess about upgrading your account.";
            } else if (!userService.IsEmailAddressInUse(user.EmailAddress)) {
                if (user.IsValid) {
                    user.AgencyId = Current.AgencyId;
                    user.AgencyName = Current.AgencyName;
                    if (userService.CreateUser(user)) {
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "User was saved successfully";
                    } else {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "Error in saving the new User.";
                    }
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = user.ValidationMessage;
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = "This E-mail Address is already in use for your agency";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult List() {
            return PartialView("List");
        }

        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Grid(int status) {
            return View(new GridModel(userRepository.GetUsersByStatus(Current.AgencyId, status)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(Guid id) {
            return PartialView(userRepository.Get(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSection(Guid id, String category) {
            var cat = category.IsNotNullOrEmpty() ? category.Split('_').Last() : string.Empty;
            return PartialView("Edit/" + cat, userRepository.Get(id, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([Bind] User user) {
            Check.Argument.IsNotNull(user, "user");
            var viewData = new JsonViewData();
            if (user.IsValid) {
                if (userRepository.Update(user)) {
                    Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, user.Id.ToString(), LogType.User, LogAction.UserEdited, string.Empty);
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "User was saved successfully";
                } else {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "Error in saving the new User.";
                }
            } else {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UpdatePermissions(FormCollection formCollection)
        {
            Check.Argument.IsNotNull(formCollection, "formCollection");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Permissions could not be updated." };
            if (userService.UpdatePermissions(formCollection))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Permissions updated successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(Guid id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deleted. Try Again." };
            if (userService.DeleteUser(id))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deleted successfully.";
            }
            return Json(viewData);
        }
        #endregion

        #region Calendar
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Calendar()
        {
            var fromDate = DateUtilities.GetStartOfMonth(DateTime.Today.Month, DateTime.Now.Year);
            var toDate = DateUtilities.GetEndOfMonth(DateTime.Today.Month, DateTime.Now.Year);
            return PartialView("Calendar/Main", new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult CalendarPdf(DateTime month)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month.Month, month.Year);
            var toDate = DateUtilities.GetEndOfMonth(month.Month, month.Year);
            return FileGenerator.Pdf<MonthlyCalendarPdf>(new MonthlyCalendarPdf(new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate }), "MonthlyCalendar");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CalendarNavigate(int month, int year)
        {
            var fromDate = DateUtilities.GetStartOfMonth(month, year);
            var toDate = DateUtilities.GetEndOfMonth(month, year);
            return PartialView("Calendar/Main", new UserCalendarViewData { UserEvents = userService.GetScheduleLeanAll(Current.UserId, fromDate, toDate), FromDate = fromDate, ToDate = toDate });
        }
        #endregion

        #region License
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseNew(Guid UserId)
        {
            return PartialView("License/New", UserId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LicenseCreate([Bind] License license)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be saved." };
            if (userService.AddLicense(license, Request.Files)) {
                viewData.isSuccessful = true;
                viewData.errorMessage = "License saved successfully";
            }
            return Json(viewData);
        }

        [GridAction]
        public ActionResult LicenseGrid(Guid userId)
        {
            return View(new GridModel(userRepository.GetUserLicenses(Current.AgencyId, userId, true)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseEdit(Guid Id, Guid UserId)
        {
            return PartialView("License/Edit", userRepository.GetUserLicense(Id, UserId, Current.AgencyId));
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LicenseUpdate([Bind] License license)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be saved." };
            if (userService.UpdateLicense(license)) {
                viewData.isSuccessful = true;
                viewData.errorMessage = "License saved successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LicenseDelete(Guid Id, Guid userId)
        {
            Check.Argument.IsNotEmpty(Id, "Id");
            Check.Argument.IsNotEmpty(userId, "userId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this hospital. Please try again." };
            if (userService.DeleteLicense(Id, userId)) {
                viewData.isSuccessful = true;
                viewData.errorMessage = "User License was successfully deleted.";
            }
            return Json(viewData);
        }
        #endregion

        #region LicenseItem
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseItemNew()
        {
            return PartialView("LicenseItem/New");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseItemAdd([Bind] LicenseItem licenseItem)
        {
            Check.Argument.IsNotNull(licenseItem, "license");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be added." };

            var entityValidator = new EntityValidator(
               new Validation(() => licenseItem.FirstName.IsNullOrEmpty(), "First Name is required. "),
               new Validation(() => licenseItem.LastName.IsNullOrEmpty(), "Last Name is required."),
               new Validation(() => licenseItem.IssueDate.Date > licenseItem.ExpireDate.Date, "The Expiration Date cannot be earlier than the Issue Date.")
            );
            entityValidator.Validate();
            if (entityValidator.IsValid)
            {
                if (userService.AddLicenseItem(licenseItem, Request.Files))
                {
                    viewData.isSuccessful = true;
                    viewData.errorMessage = "License added successfully";
                }
            }
            else
            {
                viewData.errorMessage = entityValidator.Message;
            }
            return PartialView("JsonResult", viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseItemManager()
        {
            return PartialView("LicenseItem/Manager", userService.GetUserLicenses());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseItemList() {
            return PartialView("LicenseItem/List", userService.GetUserLicenses());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LicenseItemEdit(Guid licenseId, Guid userId)
        {
            var licenseItem = new LicenseItem();
            if (userId.IsEmpty())
            {
                licenseItem = userRepository.GetNonUserLicense(licenseId, Current.AgencyId);
            }
            else
            {
                licenseItem = userRepository.GetUserLicenseItem(licenseId, userId, Current.AgencyId);
            }
            return PartialView("LicenseItem/Edit", licenseItem);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult LicenseItemUpdate([Bind] LicenseItem licenseItem)
        {
            Check.Argument.IsNotNull(licenseItem, "licenseItem");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The License could not be updated. Please try again." };
            if (licenseItem != null)
            {
                if (!licenseItem.UserId.IsEmpty())
                {
                    if (userService.UpdateLicense(licenseItem.Id, licenseItem.UserId, licenseItem.IssueDate, licenseItem.ExpireDate))
                    {
                        Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, licenseItem.UserId.ToString(), LogType.User, LogAction.UserLicenseUpdated, string.Empty);
                        viewData.isSuccessful = true;
                        viewData.errorMessage = "The License was updated successfully.";
                    }
                }
                else
                {
                    var entityValidator = new EntityValidator(
                       new Validation(() => licenseItem.FirstName.IsNullOrEmpty(), "First Name is required. "),
                       new Validation(() => licenseItem.LastName.IsNullOrEmpty(), "Last Name is required."),
                       new Validation(() => licenseItem.IssueDate.Date > licenseItem.ExpireDate.Date, "The Expiration Date cannot be earlier than the Issue Date.")
                    );
                    entityValidator.Validate();
                    if (entityValidator.IsValid)
                    {
                        licenseItem.AgencyId = Current.AgencyId;
                        if (userRepository.UpdateNonUserLicense(licenseItem))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "The License was updated successfully.";
                        }
                    }
                    else
                    {
                        viewData.errorMessage = entityValidator.Message;
                    }
                }
            }
            return Json(viewData);
        }
        #endregion

        #region PayRate
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PayRateNew(Guid userId)
        {
            return PartialView("PayRate/New", userId);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PayRateCreate([Bind]UserRate userRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User rate could not be added. Please try again." };
            if (userRate.IsValid)
            {
                var user = userRepository.Get(userRate.UserId, Current.AgencyId);
                if (user != null)
                {
                    if (user.Rates.IsNotNullOrEmpty())
                    {
                        var rates = user.Rates.ToObject<List<UserRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == userRate.Id);
                            if (rate != null)
                            {
                                viewData.isSuccessful = false;
                                viewData.errorMessage = "User rate already exists.";
                            }
                            else
                            {
                                rates.Add(userRate);
                                user.Rates = rates.ToXml();
                                if (userRepository.Refresh(user))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User rate added successfully";
                                }
                            }
                        }
                        else
                        {
                            rates = new List<UserRate>();
                            rates.Add(userRate);
                            user.Rates = rates.ToXml();
                            if (userRepository.Refresh(user))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User rate added successfully";
                            }
                        }
                    }
                    else
                    {
                        var rates = new List<UserRate>();
                        rates.Add(userRate);
                        user.Rates = rates.ToXml();
                        if (userRepository.Refresh(user))
                        {
                            viewData.isSuccessful = true;
                            viewData.errorMessage = "User rate added successfully";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userRate.ValidationMessage;
            }

            return Json(viewData);
        }

        [GridAction]
        public ActionResult PayRateGrid(Guid userId)
        {
            return View(new GridModel(userService.GetUserRates(userId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PayRateEdit(Guid userId, int Id)
        {
            var userRate = new UserRate();
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null && user.Rates.IsNotNullOrEmpty())
            {
                userRate = user.Rates.ToObject<List<UserRate>>().FirstOrDefault(r => r.Id == Id);
                if (userRate != null)
                {
                    userRate.UserId = user.Id;
                }
            }
            return PartialView("PayRate/Edit", userRate);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult PayRateUpdate([Bind] UserRate userRate)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User rate could not be updated. Please try again." };
            if (userRate.IsValid)
            {
                var user = userRepository.Get(userRate.UserId, Current.AgencyId);
                if (user != null)
                {
                    if (user.Rates.IsNotNullOrEmpty())
                    {
                        var rates = user.Rates.ToObject<List<UserRate>>();
                        if (rates != null && rates.Count > 0)
                        {
                            var rate = rates.FirstOrDefault(r => r.Id == userRate.Id);
                            if (rate != null)
                            {
                                rate.Rate = userRate.Rate;
                                rate.MileageRate = userRate.MileageRate;
                                user.Rates = rates.ToXml();
                                if (userRepository.Refresh(user))
                                {
                                    viewData.isSuccessful = true;
                                    viewData.errorMessage = "User rate updated successfully";
                                }
                            }
                        }
                    }

                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = userRate.ValidationMessage;
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PayRateDelete(Guid userId, int id)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Error happened trying to delete this insurance visit rate. Please try again." };
            var user = userRepository.Get(userId, Current.AgencyId);
            if (user != null)
            {
                if (user.Rates.IsNotNullOrEmpty())
                {
                    var rates = user.Rates.ToObject<List<UserRate>>();
                    if (rates != null && rates.Count > 0)
                    {
                        var removed = rates.RemoveAll(r => r.Id == id);
                        if (removed > 0)
                        {
                            user.Rates = rates.ToXml();
                            if (userRepository.Refresh(user))
                            {
                                viewData.isSuccessful = true;
                                viewData.errorMessage = "User rate deleted successfully";
                            }
                        }
                    }
                }
            }
            return Json(viewData);
        }
        #endregion

        #region Profile
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ProfileEdit()
        {
            return PartialView("Profile/Edit", userRepository.Get(Current.UserId, Current.AgencyId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ProfileUpdate([Bind] User user)
        {
            Check.Argument.IsNotNull(user, "user");

            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Your profile could not be saved." };

            if (user.IsValid)
            {
                if (user.PasswordChanger.NewPassword.IsNotNullOrEmpty() && !userService.IsPasswordCorrect(user.Id, user.PasswordChanger.CurrentPassword))
                {
                    viewData.isSuccessful = false;
                    viewData.errorMessage = "The password provided does not match the one on file.";
                }
                else
                {
                    if (user.SignatureChanger.NewSignature.IsNotNullOrEmpty() && !userService.IsSignatureCorrect(user.Id, user.SignatureChanger.CurrentSignature))
                    {
                        viewData.isSuccessful = false;
                        viewData.errorMessage = "The signature provided does not match the one on file.";
                    }
                    else
                    {
                        if (userService.UpdateProfile(user))
                        {

                            viewData.isSuccessful = true;
                            viewData.errorMessage = "Your profile has been updated successfully.";
                        }
                    }
                }
            }
            else
            {
                viewData.isSuccessful = false;
                viewData.errorMessage = user.ValidationMessage;
            }

            return Json(viewData);
        }
        #endregion

        #region Schedule
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Schedule()
        {
            ViewData["UserScheduleGroupName"] = "VisitDate";
            ViewData["SortColumn"] = "PatientName";
            ViewData["SortDirection"] = "ASC";
            return PartialView("Schedule/Main", userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleList(string group, string sort)
        {
            if (sort.IsNotNullOrEmpty())
            {
                var paramArray = sort.Split('-');
                if (paramArray.Length >= 2)
                {
                    ViewData["SortColumn"] = paramArray[0];
                    ViewData["SortDirection"] = paramArray[1].ToUpperCase();
                }
            }
            ViewData["UserScheduleGroupName"] = group;
            return PartialView("Schedule/List", userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14)));
        }
        
        [GridAction]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ScheduleGrid()
        {
            var pageNumber = this.HttpContext.Request.Params["page"];
            return View(new GridModel(userService.GetScheduleLean(Current.UserId, DateTime.Now.AddDays(-89), DateTime.Today.AddDays(14))));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ScheduleWidget()
        {
             var to = DateTime.Today.AddDays(14);
            var from = DateTime.Now.AddDays(-89);
            //return Json(userService.GetScheduleWidget(Current.UserId));
            return Json(userRepository.GetScheduleWidget(Current.AgencyId, Current.UserId, from, to, 5, false));
        }
        #endregion

        #region Signature
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SignatureReset()
        {
            return PartialView("Signature/Reset", Current.User.Name);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult SignatureEmail()
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "Unable to email instructions on signature reset." };
            if (membershipService.ResetSignature(Current.LoginId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "Please check your email for instructions on resetting your signature.";
            }
            return Json(viewData);
        }
        #endregion

        #region Helpers
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Deactivate(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be deactivated. Try Again." };
            if (userRepository.SetUserStatus(Current.AgencyId, id, (int)UserStatus.Inactive))
            {
                Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.User, LogAction.UserDeactivated, string.Empty);
                viewData.isSuccessful = true;
                viewData.errorMessage = "User has been deactivated successfully.";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Activate(Guid id)
        {
            Check.Argument.IsNotEmpty(id, "patientId");
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User cannot be activated. Try Again." };
            
             var userCount = userRepository.GetActiveUserCount(Current.AgencyId);
             if (userCount >= Current.MaxAgencyUserCount)
             {
                 viewData.isSuccessful = false;
                 viewData.errorMessage = "User can't be activated because you have reached your maximum number of user accounts. Please contact Axxess about upgrading your account.";
             }
             else
             {
                 if (userRepository.SetUserStatus(Current.AgencyId, id, (int)UserStatus.Active))
                 {
                     Auditor.AddGeneralLog(Current.AgencyId, Current.UserId, LogDomain.Agency, Current.AgencyId, id.ToString(), LogType.User, LogAction.UserActivated, string.Empty);
                     viewData.isSuccessful = true;
                     viewData.errorMessage = "User has been activated successfully.";
                 }
             }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult All()
        {
            return Json(userRepository.GetAgencyUsers(Current.AgencyId).ForSelection());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult BranchList(Guid branchId, int status)
        {
            return Json(userService.GetUserByBranchAndStatus(branchId, status).Select(u => new { Id = u.Id, Name = u.DisplayName }).ToList());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Log(Guid id)
        {
            return PartialView("ActivityLogs", patientService.GetGeneralLogs(LogDomain.Agency, LogType.User, Current.AgencyId, id.ToString()));
        }
        #endregion
    }
}
