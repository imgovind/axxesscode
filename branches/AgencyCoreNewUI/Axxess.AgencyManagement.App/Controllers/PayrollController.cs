﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using iTextExtension;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;

    using Axxess.AgencyManagement.App.Domain;
    using Axxess.AgencyManagement.App.Services;
    using Axxess.AgencyManagement.App.ViewData;

    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;

    using Telerik.Web.Mvc;

    using Enums;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PayrollController : BaseController
    {
        #region Constructor

        private readonly IUserService userService;
        private readonly IPayrollService payrollService;
        private readonly IUserRepository userRepository;
        private readonly IAgencyRepository agencyRepository;

        public PayrollController(IAgencyManagementDataProvider agencyManagementDataProvider, IPayrollService payrollService, IUserService userService)
        {
            Check.Argument.IsNotNull(userService, "userService");
            Check.Argument.IsNotNull(payrollService, "payrollService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.userService = userService;
            this.payrollService = payrollService;
            this.userRepository = agencyManagementDataProvider.UserRepository;
            this.agencyRepository = agencyManagementDataProvider.AgencyRepository;
        }

        #endregion

        #region PayrollController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SearchView()
        {
            return View("Search",DateTime.Today.AddDays(-15));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Search(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            return View("Results", new PayrollSummaryViewData { VisitSummary = payrollService.GetSummary(payrollStartDate, payrollEndDate, payrollStatus), EndDate = payrollEndDate, StartDate = payrollStartDate , PayrollStatus=payrollStatus});
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult SummaryPdf(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            return FileGenerator.Pdf<PayrollSummaryPdf>(new PayrollSummaryPdf(payrollService.GetSummary(payrollStartDate, payrollEndDate, payrollStatus), agencyRepository.GetWithBranches(Current.AgencyId), payrollStartDate, payrollEndDate), "PayrollSummary");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult SummaryDetailPdf(Guid userId, DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var detail = new PayrollDetail();
            if (!userId.IsEmpty())
            {
                detail.Id = userId;
                detail.Name = userRepository.Get(userId, Current.AgencyId).DisplayName;
                detail.Visits = payrollService.GetVisits(userId, payrollStartDate, payrollEndDate, payrollStatus);
                detail.PayrollStatus = payrollStatus;
            }
            return FileGenerator.Pdf<PayrollSummaryDetailsPdf>(new PayrollSummaryDetailsPdf(detail, agencyRepository.Get(Current.AgencyId), payrollStartDate, payrollEndDate), "PayrollSummary");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public FileResult SummaryDetailsPdf(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            return FileGenerator.Pdf<PayrollSummaryDetailsPdf>(new PayrollSummaryDetailsPdf(payrollService.GetDetails(payrollStartDate, payrollEndDate, payrollStatus), agencyRepository.Get(Current.AgencyId), payrollStartDate, payrollEndDate), "PayrollSummary");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Detail(Guid userId, DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            var detail = new PayrollDetail();
            detail.StartDate = payrollStartDate;
            detail.EndDate = payrollEndDate;
            detail.PayrollStatus = payrollStatus;
            if (!userId.IsEmpty())
            {
                detail.Id = userId;
                detail.Name =  UserEngine.GetName(userId, Current.AgencyId);
                detail.Visits = payrollService.GetVisits(userId, payrollStartDate, payrollEndDate, payrollStatus);
            }
            return View(detail);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Details(DateTime payrollStartDate, DateTime payrollEndDate, string payrollStatus)
        {
            return View(new PayrollDetailsViewData { StartDate = payrollStartDate, EndDate = payrollEndDate, Details = payrollService.GetDetails(payrollStartDate, payrollEndDate, payrollStatus),PayrollStatus=payrollStatus });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PayVisit(List<string> visitSelected)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The visits selected could not be processed. Please try again." };

            if (payrollService.MarkAsPaid(visitSelected))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The visits have been marked as paid.";
            }

            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnpayVisit(List<string> visitSelected)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "The visits selected could not be unpaid. Please try again." };

            if (payrollService.MarkAsUnpaid(visitSelected))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "The visits have been marked as unpaid.";
            }

            return Json(viewData);
        }

        #endregion

    }
}
