﻿namespace Axxess.AgencyManagement.App.Controllers
{
    using System;
    using System.Web.Mvc;
    
    using Enums;
    using ViewData;

    using Axxess.Core;
    using Axxess.Core.Extension;
    using Axxess.Core.Infrastructure;
    
    using Axxess.AgencyManagement.Domain;
    using Axxess.AgencyManagement.Repositories;
    using Axxess.AgencyManagement.App.Services;
    
    using Axxess.LookUp.Domain;
    using Axxess.LookUp.Repositories;
    using Axxess.Log.Enums;
    using Telerik.Web.Mvc;
    using System.Collections.Generic;

    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class PhysicianController : BaseController
    {
        #region Constructor

        private readonly IPhysicianService physicianService;
        private readonly ILookupRepository lookupRepository;
        private readonly IPatientRepository patientRepository;
        private readonly IPhysicianRepository physicianRepository;
        private readonly IPatientService patientService;

        public PhysicianController(IAgencyManagementDataProvider agencyManagementDataProvider, ILookUpDataProvider lookupDataProvider, IPhysicianService physicianService, IPatientService patientService)
        {
            Check.Argument.IsNotNull(physicianService, "physicianService");
            Check.Argument.IsNotNull(agencyManagementDataProvider, "agencyManagementDataProvider");

            this.physicianService = physicianService;
            this.lookupRepository = lookupDataProvider.LookUpRepository;
            this.patientRepository = agencyManagementDataProvider.PatientRepository;
            this.physicianRepository = agencyManagementDataProvider.PhysicianRepository;
            this.patientService = patientService;
        }

        #endregion

        #region PhysicianController Actions
        
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CheckPecos(string npi)
        {
            var viewData = new JsonViewData();
            if (npi.IsNotNullOrEmpty())
            {
                if (!lookupRepository.VerifyPecos(npi))
                {
                    viewData.isSuccessful = false;
                }
                else
                {
                    viewData.isSuccessful = true;
                }
            }
            else
            {
                viewData.isSuccessful = false;
            }
            return Json(viewData);
        }

        [GridAction]
        public ActionResult LicenseList(Guid physicianId)
        {
            return View(new GridModel(physicianRepository.GeAgencyPhysicianLicenses(Current.AgencyId, physicianId)));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLicense([Bind] PhysicianLicense license)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be saved." };
            if (physicianService.AddLicense(license)) {
                viewData.isSuccessful = true;
                viewData.errorMessage = "License saved successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateLicense([Bind] PhysicianLicense license)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "License could not be saved." };
            if (physicianService.UpdateLicense(license))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "License saved successfully";
            }
            return Json(viewData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteLicense(Guid Id, Guid physicianId)
        {
            var viewData = new JsonViewData { isSuccessful = false, errorMessage = "User license cannot be deleted. Try Again." };
            if (physicianService.DeleteLicense(Id, physicianId))
            {
                viewData.isSuccessful = true;
                viewData.errorMessage = "License successfully deleted";
            }
            return Json(viewData);
        }

        #endregion

    }
}
