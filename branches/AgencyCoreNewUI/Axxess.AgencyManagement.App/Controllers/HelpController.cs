﻿namespace Axxess.AgencyManagement.App.Controllers {
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using Axxess.Core;
    using Axxess.Core.Infrastructure;
    using Axxess.AgencyManagement.Repositories;
    [Compress]
    [Authorize]
    [HandleError]
    [SslRedirect]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HelpController : BaseController {
        #region Constructor
        private readonly IUserRepository userRepository;

        public HelpController(IAgencyManagementDataProvider dataProvider) {
            Check.Argument.IsNotNull(dataProvider, "dataProvider");
            this.userRepository = dataProvider.UserRepository;
        }

        #endregion

        #region HelpController Actions

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GoToMeeting() {
            return PartialView();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Webinars() {
            return PartialView(userRepository.Get(Current.UserId, Current.AgencyId));
        }

        #endregion
    }
}